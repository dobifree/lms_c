require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe SelProcessesController do

  # This should return the minimal set of attributes required to create a valid
  # SelProcess. As you add validations to SelProcess, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "sel_template" => "" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # SelProcessesController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all sel_processes as @sel_processes" do
      sel_process = SelProcess.create! valid_attributes
      get :index, {}, valid_session
      assigns(:sel_processes).should eq([sel_process])
    end
  end

  describe "GET show" do
    it "assigns the requested sel_process as @sel_process" do
      sel_process = SelProcess.create! valid_attributes
      get :list, {:id => sel_process.to_param}, valid_session
      assigns(:sel_process).should eq(sel_process)
    end
  end

  describe "GET new" do
    it "assigns a new sel_process as @sel_process" do
      get :new, {}, valid_session
      assigns(:sel_process).should be_a_new(SelProcess)
    end
  end

  describe "GET edit" do
    it "assigns the requested sel_process as @sel_process" do
      sel_process = SelProcess.create! valid_attributes
      get :edit, {:id => sel_process.to_param}, valid_session
      assigns(:sel_process).should eq(sel_process)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new SelProcess" do
        expect {
          post :create, {:sel_process => valid_attributes}, valid_session
        }.to change(SelProcess, :count).by(1)
      end

      it "assigns a newly created sel_process as @sel_process" do
        post :create, {:sel_process => valid_attributes}, valid_session
        assigns(:sel_process).should be_a(SelProcess)
        assigns(:sel_process).should be_persisted
      end

      it "redirects to the created sel_process" do
        post :create, {:sel_process => valid_attributes}, valid_session
        response.should redirect_to(SelProcess.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved sel_process as @sel_process" do
        # Trigger the behavior that occurs when invalid params are submitted
        SelProcess.any_instance.stub(:save).and_return(false)
        post :create, {:sel_process => { "sel_template" => "invalid value" }}, valid_session
        assigns(:sel_process).should be_a_new(SelProcess)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        SelProcess.any_instance.stub(:save).and_return(false)
        post :create, {:sel_process => { "sel_template" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested sel_process" do
        sel_process = SelProcess.create! valid_attributes
        # Assuming there are no other sel_processes in the database, this
        # specifies that the SelProcess created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        SelProcess.any_instance.should_receive(:update_attributes).with({ "sel_template" => "" })
        put :update, {:id => sel_process.to_param, :sel_process => { "sel_template" => "" }}, valid_session
      end

      it "assigns the requested sel_process as @sel_process" do
        sel_process = SelProcess.create! valid_attributes
        put :update, {:id => sel_process.to_param, :sel_process => valid_attributes}, valid_session
        assigns(:sel_process).should eq(sel_process)
      end

      it "redirects to the sel_process" do
        sel_process = SelProcess.create! valid_attributes
        put :update, {:id => sel_process.to_param, :sel_process => valid_attributes}, valid_session
        response.should redirect_to(sel_process)
      end
    end

    describe "with invalid params" do
      it "assigns the sel_process as @sel_process" do
        sel_process = SelProcess.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        SelProcess.any_instance.stub(:save).and_return(false)
        put :update, {:id => sel_process.to_param, :sel_process => { "sel_template" => "invalid value" }}, valid_session
        assigns(:sel_process).should eq(sel_process)
      end

      it "re-renders the 'edit' template" do
        sel_process = SelProcess.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        SelProcess.any_instance.stub(:save).and_return(false)
        put :update, {:id => sel_process.to_param, :sel_process => { "sel_template" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested sel_process" do
      sel_process = SelProcess.create! valid_attributes
      expect {
        delete :destroy, {:id => sel_process.to_param}, valid_session
      }.to change(SelProcess, :count).by(-1)
    end

    it "redirects to the sel_processes list" do
      sel_process = SelProcess.create! valid_attributes
      delete :destroy, {:id => sel_process.to_param}, valid_session
      response.should redirect_to(sel_processes_url)
    end
  end

end
