require 'spec_helper'

describe UsersController do

  describe "GET 'edit_pass'" do
    it "returns http success" do
      get 'edit_pass'
      response.should be_success
    end
  end

  describe "GET 'update_pass'" do
    it "returns http success" do
      get 'update_pass'
      response.should be_success
    end
  end

end
