require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe DncProgramsController do

  # This should return the minimal set of attributes required to create a valid
  # DncProgram. As you add validations to DncProgram, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "name" => "MyString" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # DncProgramsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all dnc_programs as @dnc_programs" do
      dnc_program = DncProgram.create! valid_attributes
      get :index, {}, valid_session
      assigns(:dnc_programs).should eq([dnc_program])
    end
  end

  describe "GET show" do
    it "assigns the requested dnc_program as @dnc_program" do
      dnc_program = DncProgram.create! valid_attributes
      get :list, {:id => dnc_program.to_param}, valid_session
      assigns(:dnc_program).should eq(dnc_program)
    end
  end

  describe "GET new" do
    it "assigns a new dnc_program as @dnc_program" do
      get :new, {}, valid_session
      assigns(:dnc_program).should be_a_new(DncProgram)
    end
  end

  describe "GET edit" do
    it "assigns the requested dnc_program as @dnc_program" do
      dnc_program = DncProgram.create! valid_attributes
      get :edit, {:id => dnc_program.to_param}, valid_session
      assigns(:dnc_program).should eq(dnc_program)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new DncProgram" do
        expect {
          post :create, {:dnc_program => valid_attributes}, valid_session
        }.to change(DncProgram, :count).by(1)
      end

      it "assigns a newly created dnc_program as @dnc_program" do
        post :create, {:dnc_program => valid_attributes}, valid_session
        assigns(:dnc_program).should be_a(DncProgram)
        assigns(:dnc_program).should be_persisted
      end

      it "redirects to the created dnc_program" do
        post :create, {:dnc_program => valid_attributes}, valid_session
        response.should redirect_to(DncProgram.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved dnc_program as @dnc_program" do
        # Trigger the behavior that occurs when invalid params are submitted
        DncProgram.any_instance.stub(:save).and_return(false)
        post :create, {:dnc_program => { "name" => "invalid value" }}, valid_session
        assigns(:dnc_program).should be_a_new(DncProgram)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        DncProgram.any_instance.stub(:save).and_return(false)
        post :create, {:dnc_program => { "name" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested dnc_program" do
        dnc_program = DncProgram.create! valid_attributes
        # Assuming there are no other dnc_programs in the database, this
        # specifies that the DncProgram created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        DncProgram.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => dnc_program.to_param, :dnc_program => { "name" => "MyString" }}, valid_session
      end

      it "assigns the requested dnc_program as @dnc_program" do
        dnc_program = DncProgram.create! valid_attributes
        put :update, {:id => dnc_program.to_param, :dnc_program => valid_attributes}, valid_session
        assigns(:dnc_program).should eq(dnc_program)
      end

      it "redirects to the dnc_program" do
        dnc_program = DncProgram.create! valid_attributes
        put :update, {:id => dnc_program.to_param, :dnc_program => valid_attributes}, valid_session
        response.should redirect_to(dnc_program)
      end
    end

    describe "with invalid params" do
      it "assigns the dnc_program as @dnc_program" do
        dnc_program = DncProgram.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        DncProgram.any_instance.stub(:save).and_return(false)
        put :update, {:id => dnc_program.to_param, :dnc_program => { "name" => "invalid value" }}, valid_session
        assigns(:dnc_program).should eq(dnc_program)
      end

      it "re-renders the 'edit' template" do
        dnc_program = DncProgram.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        DncProgram.any_instance.stub(:save).and_return(false)
        put :update, {:id => dnc_program.to_param, :dnc_program => { "name" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested dnc_program" do
      dnc_program = DncProgram.create! valid_attributes
      expect {
        delete :destroy, {:id => dnc_program.to_param}, valid_session
      }.to change(DncProgram, :count).by(-1)
    end

    it "redirects to the dnc_programs list" do
      dnc_program = DncProgram.create! valid_attributes
      delete :destroy, {:id => dnc_program.to_param}, valid_session
      response.should redirect_to(dnc_programs_url)
    end
  end

end
