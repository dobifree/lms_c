require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe DncCategoriesController do

  # This should return the minimal set of attributes required to create a valid
  # DncCategory. As you add validations to DncCategory, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "name" => "MyString" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # DncCategoriesController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all dnc_categories as @dnc_categories" do
      dnc_category = DncCategory.create! valid_attributes
      get :index, {}, valid_session
      assigns(:dnc_categories).should eq([dnc_category])
    end
  end

  describe "GET show" do
    it "assigns the requested dnc_category as @dnc_category" do
      dnc_category = DncCategory.create! valid_attributes
      get :list, {:id => dnc_category.to_param}, valid_session
      assigns(:dnc_category).should eq(dnc_category)
    end
  end

  describe "GET new" do
    it "assigns a new dnc_category as @dnc_category" do
      get :new, {}, valid_session
      assigns(:dnc_category).should be_a_new(DncCategory)
    end
  end

  describe "GET edit" do
    it "assigns the requested dnc_category as @dnc_category" do
      dnc_category = DncCategory.create! valid_attributes
      get :edit, {:id => dnc_category.to_param}, valid_session
      assigns(:dnc_category).should eq(dnc_category)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new DncCategory" do
        expect {
          post :create, {:dnc_category => valid_attributes}, valid_session
        }.to change(DncCategory, :count).by(1)
      end

      it "assigns a newly created dnc_category as @dnc_category" do
        post :create, {:dnc_category => valid_attributes}, valid_session
        assigns(:dnc_category).should be_a(DncCategory)
        assigns(:dnc_category).should be_persisted
      end

      it "redirects to the created dnc_category" do
        post :create, {:dnc_category => valid_attributes}, valid_session
        response.should redirect_to(DncCategory.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved dnc_category as @dnc_category" do
        # Trigger the behavior that occurs when invalid params are submitted
        DncCategory.any_instance.stub(:save).and_return(false)
        post :create, {:dnc_category => { "name" => "invalid value" }}, valid_session
        assigns(:dnc_category).should be_a_new(DncCategory)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        DncCategory.any_instance.stub(:save).and_return(false)
        post :create, {:dnc_category => { "name" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested dnc_category" do
        dnc_category = DncCategory.create! valid_attributes
        # Assuming there are no other dnc_categories in the database, this
        # specifies that the DncCategory created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        DncCategory.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => dnc_category.to_param, :dnc_category => { "name" => "MyString" }}, valid_session
      end

      it "assigns the requested dnc_category as @dnc_category" do
        dnc_category = DncCategory.create! valid_attributes
        put :update, {:id => dnc_category.to_param, :dnc_category => valid_attributes}, valid_session
        assigns(:dnc_category).should eq(dnc_category)
      end

      it "redirects to the dnc_category" do
        dnc_category = DncCategory.create! valid_attributes
        put :update, {:id => dnc_category.to_param, :dnc_category => valid_attributes}, valid_session
        response.should redirect_to(dnc_category)
      end
    end

    describe "with invalid params" do
      it "assigns the dnc_category as @dnc_category" do
        dnc_category = DncCategory.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        DncCategory.any_instance.stub(:save).and_return(false)
        put :update, {:id => dnc_category.to_param, :dnc_category => { "name" => "invalid value" }}, valid_session
        assigns(:dnc_category).should eq(dnc_category)
      end

      it "re-renders the 'edit' template" do
        dnc_category = DncCategory.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        DncCategory.any_instance.stub(:save).and_return(false)
        put :update, {:id => dnc_category.to_param, :dnc_category => { "name" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested dnc_category" do
      dnc_category = DncCategory.create! valid_attributes
      expect {
        delete :destroy, {:id => dnc_category.to_param}, valid_session
      }.to change(DncCategory, :count).by(-1)
    end

    it "redirects to the dnc_categories list" do
      dnc_category = DncCategory.create! valid_attributes
      delete :destroy, {:id => dnc_category.to_param}, valid_session
      response.should redirect_to(dnc_categories_url)
    end
  end

end
