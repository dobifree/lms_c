require 'spec_helper'

describe ElecRoundExecController do

  describe "GET 'show'" do
    it "returns http success" do
      get 'show'
      response.should be_success
    end
  end

  describe "GET 'show_detail_category'" do
    it "returns http success" do
      get 'show_detail_category'
      response.should be_success
    end
  end

  describe "GET 'vote'" do
    it "returns http success" do
      get 'vote'
      response.should be_success
    end
  end

end
