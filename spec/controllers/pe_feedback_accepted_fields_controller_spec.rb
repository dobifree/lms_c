require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe PeFeedbackAcceptedFieldsController do

  # This should return the minimal set of attributes required to create a valid
  # PeFeedbackAcceptedField. As you add validations to PeFeedbackAcceptedField, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "pe_process" => "" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # PeFeedbackAcceptedFieldsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all pe_feedback_accepted_fields as @pe_feedback_accepted_fields" do
      pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
      get :index, {}, valid_session
      assigns(:pe_feedback_accepted_fields).should eq([pe_feedback_accepted_field])
    end
  end

  describe "GET show" do
    it "assigns the requested pe_feedback_accepted_field as @pe_feedback_accepted_field" do
      pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
      get :show, {:id => pe_feedback_accepted_field.to_param}, valid_session
      assigns(:pe_feedback_accepted_field).should eq(pe_feedback_accepted_field)
    end
  end

  describe "GET new" do
    it "assigns a new pe_feedback_accepted_field as @pe_feedback_accepted_field" do
      get :new, {}, valid_session
      assigns(:pe_feedback_accepted_field).should be_a_new(PeFeedbackAcceptedField)
    end
  end

  describe "GET edit" do
    it "assigns the requested pe_feedback_accepted_field as @pe_feedback_accepted_field" do
      pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
      get :edit, {:id => pe_feedback_accepted_field.to_param}, valid_session
      assigns(:pe_feedback_accepted_field).should eq(pe_feedback_accepted_field)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new PeFeedbackAcceptedField" do
        expect {
          post :create, {:pe_feedback_accepted_field => valid_attributes}, valid_session
        }.to change(PeFeedbackAcceptedField, :count).by(1)
      end

      it "assigns a newly created pe_feedback_accepted_field as @pe_feedback_accepted_field" do
        post :create, {:pe_feedback_accepted_field => valid_attributes}, valid_session
        assigns(:pe_feedback_accepted_field).should be_a(PeFeedbackAcceptedField)
        assigns(:pe_feedback_accepted_field).should be_persisted
      end

      it "redirects to the created pe_feedback_accepted_field" do
        post :create, {:pe_feedback_accepted_field => valid_attributes}, valid_session
        response.should redirect_to(PeFeedbackAcceptedField.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved pe_feedback_accepted_field as @pe_feedback_accepted_field" do
        # Trigger the behavior that occurs when invalid params are submitted
        PeFeedbackAcceptedField.any_instance.stub(:save).and_return(false)
        post :create, {:pe_feedback_accepted_field => { "pe_process" => "invalid value" }}, valid_session
        assigns(:pe_feedback_accepted_field).should be_a_new(PeFeedbackAcceptedField)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        PeFeedbackAcceptedField.any_instance.stub(:save).and_return(false)
        post :create, {:pe_feedback_accepted_field => { "pe_process" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested pe_feedback_accepted_field" do
        pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
        # Assuming there are no other pe_feedback_accepted_fields in the database, this
        # specifies that the PeFeedbackAcceptedField created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        PeFeedbackAcceptedField.any_instance.should_receive(:update_attributes).with({ "pe_process" => "" })
        put :update, {:id => pe_feedback_accepted_field.to_param, :pe_feedback_accepted_field => { "pe_process" => "" }}, valid_session
      end

      it "assigns the requested pe_feedback_accepted_field as @pe_feedback_accepted_field" do
        pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
        put :update, {:id => pe_feedback_accepted_field.to_param, :pe_feedback_accepted_field => valid_attributes}, valid_session
        assigns(:pe_feedback_accepted_field).should eq(pe_feedback_accepted_field)
      end

      it "redirects to the pe_feedback_accepted_field" do
        pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
        put :update, {:id => pe_feedback_accepted_field.to_param, :pe_feedback_accepted_field => valid_attributes}, valid_session
        response.should redirect_to(pe_feedback_accepted_field)
      end
    end

    describe "with invalid params" do
      it "assigns the pe_feedback_accepted_field as @pe_feedback_accepted_field" do
        pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        PeFeedbackAcceptedField.any_instance.stub(:save).and_return(false)
        put :update, {:id => pe_feedback_accepted_field.to_param, :pe_feedback_accepted_field => { "pe_process" => "invalid value" }}, valid_session
        assigns(:pe_feedback_accepted_field).should eq(pe_feedback_accepted_field)
      end

      it "re-renders the 'edit' template" do
        pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        PeFeedbackAcceptedField.any_instance.stub(:save).and_return(false)
        put :update, {:id => pe_feedback_accepted_field.to_param, :pe_feedback_accepted_field => { "pe_process" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested pe_feedback_accepted_field" do
      pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
      expect {
        delete :destroy, {:id => pe_feedback_accepted_field.to_param}, valid_session
      }.to change(PeFeedbackAcceptedField, :count).by(-1)
    end

    it "redirects to the pe_feedback_accepted_fields list" do
      pe_feedback_accepted_field = PeFeedbackAcceptedField.create! valid_attributes
      delete :destroy, {:id => pe_feedback_accepted_field.to_param}, valid_session
      response.should redirect_to(pe_feedback_accepted_fields_url)
    end
  end

end
