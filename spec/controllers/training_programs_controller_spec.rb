require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe TrainingProgramsController do

  # This should return the minimal set of attributes required to create a valid
  # TrainingProgram. As you add validations to TrainingProgram, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "nombre" => "MyString" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # TrainingProgramsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all training_programs as @training_programs" do
      training_program = TrainingProgram.create! valid_attributes
      get :index, {}, valid_session
      assigns(:training_programs).should eq([training_program])
    end
  end

  describe "GET show" do
    it "assigns the requested training_program as @training_program" do
      training_program = TrainingProgram.create! valid_attributes
      get :list, {:id => training_program.to_param}, valid_session
      assigns(:training_program).should eq(training_program)
    end
  end

  describe "GET new" do
    it "assigns a new training_program as @training_program" do
      get :new, {}, valid_session
      assigns(:training_program).should be_a_new(TrainingProgram)
    end
  end

  describe "GET edit" do
    it "assigns the requested training_program as @training_program" do
      training_program = TrainingProgram.create! valid_attributes
      get :edit, {:id => training_program.to_param}, valid_session
      assigns(:training_program).should eq(training_program)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new TrainingProgram" do
        expect {
          post :create, {:training_program => valid_attributes}, valid_session
        }.to change(TrainingProgram, :count).by(1)
      end

      it "assigns a newly created training_program as @training_program" do
        post :create, {:training_program => valid_attributes}, valid_session
        assigns(:training_program).should be_a(TrainingProgram)
        assigns(:training_program).should be_persisted
      end

      it "redirects to the created training_program" do
        post :create, {:training_program => valid_attributes}, valid_session
        response.should redirect_to(TrainingProgram.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved training_program as @training_program" do
        # Trigger the behavior that occurs when invalid params are submitted
        TrainingProgram.any_instance.stub(:save).and_return(false)
        post :create, {:training_program => { "nombre" => "invalid value" }}, valid_session
        assigns(:training_program).should be_a_new(TrainingProgram)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        TrainingProgram.any_instance.stub(:save).and_return(false)
        post :create, {:training_program => { "nombre" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested training_program" do
        training_program = TrainingProgram.create! valid_attributes
        # Assuming there are no other training_programs in the database, this
        # specifies that the TrainingProgram created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        TrainingProgram.any_instance.should_receive(:update_attributes).with({ "nombre" => "MyString" })
        put :update, {:id => training_program.to_param, :training_program => { "nombre" => "MyString" }}, valid_session
      end

      it "assigns the requested training_program as @training_program" do
        training_program = TrainingProgram.create! valid_attributes
        put :update, {:id => training_program.to_param, :training_program => valid_attributes}, valid_session
        assigns(:training_program).should eq(training_program)
      end

      it "redirects to the training_program" do
        training_program = TrainingProgram.create! valid_attributes
        put :update, {:id => training_program.to_param, :training_program => valid_attributes}, valid_session
        response.should redirect_to(training_program)
      end
    end

    describe "with invalid params" do
      it "assigns the training_program as @training_program" do
        training_program = TrainingProgram.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        TrainingProgram.any_instance.stub(:save).and_return(false)
        put :update, {:id => training_program.to_param, :training_program => { "nombre" => "invalid value" }}, valid_session
        assigns(:training_program).should eq(training_program)
      end

      it "re-renders the 'edit' template" do
        training_program = TrainingProgram.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        TrainingProgram.any_instance.stub(:save).and_return(false)
        put :update, {:id => training_program.to_param, :training_program => { "nombre" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested training_program" do
      training_program = TrainingProgram.create! valid_attributes
      expect {
        delete :destroy, {:id => training_program.to_param}, valid_session
      }.to change(TrainingProgram, :count).by(-1)
    end

    it "redirects to the training_programs list" do
      training_program = TrainingProgram.create! valid_attributes
      delete :destroy, {:id => training_program.to_param}, valid_session
      response.should redirect_to(training_programs_url)
    end
  end

end
