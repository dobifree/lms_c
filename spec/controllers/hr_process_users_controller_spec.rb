require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe HrProcessUsersController do

  # This should return the minimal set of attributes required to create a valid
  # HrProcessUser. As you add validations to HrProcessUser, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "hr_process" => "" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # HrProcessUsersController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all hr_process_users as @hr_process_users" do
      hr_process_user = HrProcessUser.create! valid_attributes
      get :index, {}, valid_session
      assigns(:hr_process_users).should eq([hr_process_user])
    end
  end

  describe "GET show" do
    it "assigns the requested hr_process_user as @hr_process_user" do
      hr_process_user = HrProcessUser.create! valid_attributes
      get :list, {:id => hr_process_user.to_param}, valid_session
      assigns(:hr_process_user).should eq(hr_process_user)
    end
  end

  describe "GET new" do
    it "assigns a new hr_process_user as @hr_process_user" do
      get :new, {}, valid_session
      assigns(:hr_process_user).should be_a_new(HrProcessUser)
    end
  end

  describe "GET edit" do
    it "assigns the requested hr_process_user as @hr_process_user" do
      hr_process_user = HrProcessUser.create! valid_attributes
      get :edit, {:id => hr_process_user.to_param}, valid_session
      assigns(:hr_process_user).should eq(hr_process_user)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new HrProcessUser" do
        expect {
          post :create, {:hr_process_user => valid_attributes}, valid_session
        }.to change(HrProcessUser, :count).by(1)
      end

      it "assigns a newly created hr_process_user as @hr_process_user" do
        post :create, {:hr_process_user => valid_attributes}, valid_session
        assigns(:hr_process_user).should be_a(HrProcessUser)
        assigns(:hr_process_user).should be_persisted
      end

      it "redirects to the created hr_process_user" do
        post :create, {:hr_process_user => valid_attributes}, valid_session
        response.should redirect_to(HrProcessUser.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved hr_process_user as @hr_process_user" do
        # Trigger the behavior that occurs when invalid params are submitted
        HrProcessUser.any_instance.stub(:save).and_return(false)
        post :create, {:hr_process_user => { "hr_process" => "invalid value" }}, valid_session
        assigns(:hr_process_user).should be_a_new(HrProcessUser)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        HrProcessUser.any_instance.stub(:save).and_return(false)
        post :create, {:hr_process_user => { "hr_process" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested hr_process_user" do
        hr_process_user = HrProcessUser.create! valid_attributes
        # Assuming there are no other hr_process_users in the database, this
        # specifies that the HrProcessUser created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        HrProcessUser.any_instance.should_receive(:update_attributes).with({ "hr_process" => "" })
        put :update, {:id => hr_process_user.to_param, :hr_process_user => { "hr_process" => "" }}, valid_session
      end

      it "assigns the requested hr_process_user as @hr_process_user" do
        hr_process_user = HrProcessUser.create! valid_attributes
        put :update, {:id => hr_process_user.to_param, :hr_process_user => valid_attributes}, valid_session
        assigns(:hr_process_user).should eq(hr_process_user)
      end

      it "redirects to the hr_process_user" do
        hr_process_user = HrProcessUser.create! valid_attributes
        put :update, {:id => hr_process_user.to_param, :hr_process_user => valid_attributes}, valid_session
        response.should redirect_to(hr_process_user)
      end
    end

    describe "with invalid params" do
      it "assigns the hr_process_user as @hr_process_user" do
        hr_process_user = HrProcessUser.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        HrProcessUser.any_instance.stub(:save).and_return(false)
        put :update, {:id => hr_process_user.to_param, :hr_process_user => { "hr_process" => "invalid value" }}, valid_session
        assigns(:hr_process_user).should eq(hr_process_user)
      end

      it "re-renders the 'edit' template" do
        hr_process_user = HrProcessUser.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        HrProcessUser.any_instance.stub(:save).and_return(false)
        put :update, {:id => hr_process_user.to_param, :hr_process_user => { "hr_process" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested hr_process_user" do
      hr_process_user = HrProcessUser.create! valid_attributes
      expect {
        delete :destroy, {:id => hr_process_user.to_param}, valid_session
      }.to change(HrProcessUser, :count).by(-1)
    end

    it "redirects to the hr_process_users list" do
      hr_process_user = HrProcessUser.create! valid_attributes
      delete :destroy, {:id => hr_process_user.to_param}, valid_session
      response.should redirect_to(hr_process_users_url)
    end
  end

end
