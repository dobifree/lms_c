require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe BrcMailsController do

  # This should return the minimal set of attributes required to create a valid
  # BrcMail. As you add validations to BrcMail, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "def_begin_subject" => "MyString" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # BrcMailsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all brc_mails as @brc_mails" do
      brc_mail = BrcMail.create! valid_attributes
      get :index, {}, valid_session
      assigns(:brc_mails).should eq([brc_mail])
    end
  end

  describe "GET show" do
    it "assigns the requested brc_mail as @brc_mail" do
      brc_mail = BrcMail.create! valid_attributes
      get :show, {:id => brc_mail.to_param}, valid_session
      assigns(:brc_mail).should eq(brc_mail)
    end
  end

  describe "GET new" do
    it "assigns a new brc_mail as @brc_mail" do
      get :new, {}, valid_session
      assigns(:brc_mail).should be_a_new(BrcMail)
    end
  end

  describe "GET edit" do
    it "assigns the requested brc_mail as @brc_mail" do
      brc_mail = BrcMail.create! valid_attributes
      get :edit, {:id => brc_mail.to_param}, valid_session
      assigns(:brc_mail).should eq(brc_mail)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new BrcMail" do
        expect {
          post :create, {:brc_mail => valid_attributes}, valid_session
        }.to change(BrcMail, :count).by(1)
      end

      it "assigns a newly created brc_mail as @brc_mail" do
        post :create, {:brc_mail => valid_attributes}, valid_session
        assigns(:brc_mail).should be_a(BrcMail)
        assigns(:brc_mail).should be_persisted
      end

      it "redirects to the created brc_mail" do
        post :create, {:brc_mail => valid_attributes}, valid_session
        response.should redirect_to(BrcMail.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved brc_mail as @brc_mail" do
        # Trigger the behavior that occurs when invalid params are submitted
        BrcMail.any_instance.stub(:save).and_return(false)
        post :create, {:brc_mail => { "def_begin_subject" => "invalid value" }}, valid_session
        assigns(:brc_mail).should be_a_new(BrcMail)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        BrcMail.any_instance.stub(:save).and_return(false)
        post :create, {:brc_mail => { "def_begin_subject" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested brc_mail" do
        brc_mail = BrcMail.create! valid_attributes
        # Assuming there are no other brc_mails in the database, this
        # specifies that the BrcMail created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        BrcMail.any_instance.should_receive(:update_attributes).with({ "def_begin_subject" => "MyString" })
        put :update, {:id => brc_mail.to_param, :brc_mail => { "def_begin_subject" => "MyString" }}, valid_session
      end

      it "assigns the requested brc_mail as @brc_mail" do
        brc_mail = BrcMail.create! valid_attributes
        put :update, {:id => brc_mail.to_param, :brc_mail => valid_attributes}, valid_session
        assigns(:brc_mail).should eq(brc_mail)
      end

      it "redirects to the brc_mail" do
        brc_mail = BrcMail.create! valid_attributes
        put :update, {:id => brc_mail.to_param, :brc_mail => valid_attributes}, valid_session
        response.should redirect_to(brc_mail)
      end
    end

    describe "with invalid params" do
      it "assigns the brc_mail as @brc_mail" do
        brc_mail = BrcMail.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        BrcMail.any_instance.stub(:save).and_return(false)
        put :update, {:id => brc_mail.to_param, :brc_mail => { "def_begin_subject" => "invalid value" }}, valid_session
        assigns(:brc_mail).should eq(brc_mail)
      end

      it "re-renders the 'edit' template" do
        brc_mail = BrcMail.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        BrcMail.any_instance.stub(:save).and_return(false)
        put :update, {:id => brc_mail.to_param, :brc_mail => { "def_begin_subject" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested brc_mail" do
      brc_mail = BrcMail.create! valid_attributes
      expect {
        delete :destroy, {:id => brc_mail.to_param}, valid_session
      }.to change(BrcMail, :count).by(-1)
    end

    it "redirects to the brc_mails list" do
      brc_mail = BrcMail.create! valid_attributes
      delete :destroy, {:id => brc_mail.to_param}, valid_session
      response.should redirect_to(brc_mails_url)
    end
  end

end
