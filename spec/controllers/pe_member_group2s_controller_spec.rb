require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe PeMemberGroup2sController do

  # This should return the minimal set of attributes required to create a valid
  # PeMemberGroup2. As you add validations to PeMemberGroup2, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "pe_process" => "" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # PeMemberGroup2sController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all pe_member_group2s as @pe_member_group2s" do
      pe_member_group2 = PeMemberGroup2.create! valid_attributes
      get :index, {}, valid_session
      assigns(:pe_member_group2s).should eq([pe_member_group2])
    end
  end

  describe "GET show" do
    it "assigns the requested pe_member_group2 as @pe_member_group2" do
      pe_member_group2 = PeMemberGroup2.create! valid_attributes
      get :show, {:id => pe_member_group2.to_param}, valid_session
      assigns(:pe_member_group2).should eq(pe_member_group2)
    end
  end

  describe "GET new" do
    it "assigns a new pe_member_group2 as @pe_member_group2" do
      get :new, {}, valid_session
      assigns(:pe_member_group2).should be_a_new(PeMemberGroup2)
    end
  end

  describe "GET edit" do
    it "assigns the requested pe_member_group2 as @pe_member_group2" do
      pe_member_group2 = PeMemberGroup2.create! valid_attributes
      get :edit, {:id => pe_member_group2.to_param}, valid_session
      assigns(:pe_member_group2).should eq(pe_member_group2)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new PeMemberGroup2" do
        expect {
          post :create, {:pe_member_group2 => valid_attributes}, valid_session
        }.to change(PeMemberGroup2, :count).by(1)
      end

      it "assigns a newly created pe_member_group2 as @pe_member_group2" do
        post :create, {:pe_member_group2 => valid_attributes}, valid_session
        assigns(:pe_member_group2).should be_a(PeMemberGroup2)
        assigns(:pe_member_group2).should be_persisted
      end

      it "redirects to the created pe_member_group2" do
        post :create, {:pe_member_group2 => valid_attributes}, valid_session
        response.should redirect_to(PeMemberGroup2.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved pe_member_group2 as @pe_member_group2" do
        # Trigger the behavior that occurs when invalid params are submitted
        PeMemberGroup2.any_instance.stub(:save).and_return(false)
        post :create, {:pe_member_group2 => { "pe_process" => "invalid value" }}, valid_session
        assigns(:pe_member_group2).should be_a_new(PeMemberGroup2)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        PeMemberGroup2.any_instance.stub(:save).and_return(false)
        post :create, {:pe_member_group2 => { "pe_process" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested pe_member_group2" do
        pe_member_group2 = PeMemberGroup2.create! valid_attributes
        # Assuming there are no other pe_member_group2s in the database, this
        # specifies that the PeMemberGroup2 created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        PeMemberGroup2.any_instance.should_receive(:update_attributes).with({ "pe_process" => "" })
        put :update, {:id => pe_member_group2.to_param, :pe_member_group2 => { "pe_process" => "" }}, valid_session
      end

      it "assigns the requested pe_member_group2 as @pe_member_group2" do
        pe_member_group2 = PeMemberGroup2.create! valid_attributes
        put :update, {:id => pe_member_group2.to_param, :pe_member_group2 => valid_attributes}, valid_session
        assigns(:pe_member_group2).should eq(pe_member_group2)
      end

      it "redirects to the pe_member_group2" do
        pe_member_group2 = PeMemberGroup2.create! valid_attributes
        put :update, {:id => pe_member_group2.to_param, :pe_member_group2 => valid_attributes}, valid_session
        response.should redirect_to(pe_member_group2)
      end
    end

    describe "with invalid params" do
      it "assigns the pe_member_group2 as @pe_member_group2" do
        pe_member_group2 = PeMemberGroup2.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        PeMemberGroup2.any_instance.stub(:save).and_return(false)
        put :update, {:id => pe_member_group2.to_param, :pe_member_group2 => { "pe_process" => "invalid value" }}, valid_session
        assigns(:pe_member_group2).should eq(pe_member_group2)
      end

      it "re-renders the 'edit' template" do
        pe_member_group2 = PeMemberGroup2.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        PeMemberGroup2.any_instance.stub(:save).and_return(false)
        put :update, {:id => pe_member_group2.to_param, :pe_member_group2 => { "pe_process" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested pe_member_group2" do
      pe_member_group2 = PeMemberGroup2.create! valid_attributes
      expect {
        delete :destroy, {:id => pe_member_group2.to_param}, valid_session
      }.to change(PeMemberGroup2, :count).by(-1)
    end

    it "redirects to the pe_member_group2s list" do
      pe_member_group2 = PeMemberGroup2.create! valid_attributes
      delete :destroy, {:id => pe_member_group2.to_param}, valid_session
      response.should redirect_to(pe_member_group2s_url)
    end
  end

end
