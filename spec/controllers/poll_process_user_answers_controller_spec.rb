require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe PollProcessUserAnswersController do

  # This should return the minimal set of attributes required to create a valid
  # PollProcessUserAnswer. As you add validations to PollProcessUserAnswer, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "poll_process_user" => "" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # PollProcessUserAnswersController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all poll_process_user_answers as @poll_process_user_answers" do
      poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
      get :index, {}, valid_session
      assigns(:poll_process_user_answers).should eq([poll_process_user_answer])
    end
  end

  describe "GET show" do
    it "assigns the requested poll_process_user_answer as @poll_process_user_answer" do
      poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
      get :list, {:id => poll_process_user_answer.to_param}, valid_session
      assigns(:poll_process_user_answer).should eq(poll_process_user_answer)
    end
  end

  describe "GET new" do
    it "assigns a new poll_process_user_answer as @poll_process_user_answer" do
      get :new, {}, valid_session
      assigns(:poll_process_user_answer).should be_a_new(PollProcessUserAnswer)
    end
  end

  describe "GET edit" do
    it "assigns the requested poll_process_user_answer as @poll_process_user_answer" do
      poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
      get :edit, {:id => poll_process_user_answer.to_param}, valid_session
      assigns(:poll_process_user_answer).should eq(poll_process_user_answer)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new PollProcessUserAnswer" do
        expect {
          post :create, {:poll_process_user_answer => valid_attributes}, valid_session
        }.to change(PollProcessUserAnswer, :count).by(1)
      end

      it "assigns a newly created poll_process_user_answer as @poll_process_user_answer" do
        post :create, {:poll_process_user_answer => valid_attributes}, valid_session
        assigns(:poll_process_user_answer).should be_a(PollProcessUserAnswer)
        assigns(:poll_process_user_answer).should be_persisted
      end

      it "redirects to the created poll_process_user_answer" do
        post :create, {:poll_process_user_answer => valid_attributes}, valid_session
        response.should redirect_to(PollProcessUserAnswer.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved poll_process_user_answer as @poll_process_user_answer" do
        # Trigger the behavior that occurs when invalid params are submitted
        PollProcessUserAnswer.any_instance.stub(:save).and_return(false)
        post :create, {:poll_process_user_answer => { "poll_process_user" => "invalid value" }}, valid_session
        assigns(:poll_process_user_answer).should be_a_new(PollProcessUserAnswer)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        PollProcessUserAnswer.any_instance.stub(:save).and_return(false)
        post :create, {:poll_process_user_answer => { "poll_process_user" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested poll_process_user_answer" do
        poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
        # Assuming there are no other poll_process_user_answers in the database, this
        # specifies that the PollProcessUserAnswer created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        PollProcessUserAnswer.any_instance.should_receive(:update_attributes).with({ "poll_process_user" => "" })
        put :update, {:id => poll_process_user_answer.to_param, :poll_process_user_answer => { "poll_process_user" => "" }}, valid_session
      end

      it "assigns the requested poll_process_user_answer as @poll_process_user_answer" do
        poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
        put :update, {:id => poll_process_user_answer.to_param, :poll_process_user_answer => valid_attributes}, valid_session
        assigns(:poll_process_user_answer).should eq(poll_process_user_answer)
      end

      it "redirects to the poll_process_user_answer" do
        poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
        put :update, {:id => poll_process_user_answer.to_param, :poll_process_user_answer => valid_attributes}, valid_session
        response.should redirect_to(poll_process_user_answer)
      end
    end

    describe "with invalid params" do
      it "assigns the poll_process_user_answer as @poll_process_user_answer" do
        poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        PollProcessUserAnswer.any_instance.stub(:save).and_return(false)
        put :update, {:id => poll_process_user_answer.to_param, :poll_process_user_answer => { "poll_process_user" => "invalid value" }}, valid_session
        assigns(:poll_process_user_answer).should eq(poll_process_user_answer)
      end

      it "re-renders the 'edit' template" do
        poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        PollProcessUserAnswer.any_instance.stub(:save).and_return(false)
        put :update, {:id => poll_process_user_answer.to_param, :poll_process_user_answer => { "poll_process_user" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested poll_process_user_answer" do
      poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
      expect {
        delete :destroy, {:id => poll_process_user_answer.to_param}, valid_session
      }.to change(PollProcessUserAnswer, :count).by(-1)
    end

    it "redirects to the poll_process_user_answers list" do
      poll_process_user_answer = PollProcessUserAnswer.create! valid_attributes
      delete :destroy, {:id => poll_process_user_answer.to_param}, valid_session
      response.should redirect_to(poll_process_user_answers_url)
    end
  end

end
