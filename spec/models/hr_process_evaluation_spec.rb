# == Schema Information
#
# Table name: hr_process_evaluations
#
#  id                         :integer          not null, primary key
#  hr_process_id              :integer
#  hr_process_dimension_e_id  :integer
#  fecha_inicio_eval_jefe     :datetime
#  fecha_fin_eval_jefe        :datetime
#  fecha_inicio_eval_auto     :datetime
#  fecha_fin_eval_auto        :datetime
#  fecha_inicio_eval_sub      :datetime
#  fecha_fin_eval_sub         :datetime
#  fecha_inicio_eval_par      :datetime
#  fecha_fin_eval_par         :datetime
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  vigente                    :boolean          default(TRUE)
#  orden                      :integer
#  cien_x_cien_puntos         :integer
#  visualizacion_alternativas :integer          default(1)
#  fecha_inicio_eval_cli      :datetime
#  fecha_fin_eval_cli         :datetime
#  fecha_inicio_eval_prov     :datetime
#  fecha_fin_eval_prov        :datetime
#  show_preg_nivel_1          :boolean          default(TRUE)
#  tope_no_aplica             :integer
#

require 'spec_helper'

describe HrProcessEvaluation do
  pending "add some examples to (or delete) #{__FILE__}"
end
