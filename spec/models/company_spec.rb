# == Schema Information
#
# Table name: companies
#
#  id                                 :integer          not null, primary key
#  nombre                             :string(255)
#  codigo                             :string(255)
#  url                                :string(255)
#  connection_string                  :string(255)
#  active                             :boolean          default(FALSE)
#  directorio                         :string(255)
#  email_mesa_ayuda                   :string(255)
#  modulo_sence                       :boolean          default(FALSE)
#  show_header_in_login               :boolean          default(TRUE)
#  texto_username_login               :string(255)
#  texto_cambio_password              :text
#  alias_username                     :string(255)      default("Código")
#  alias_user                         :string(255)      default("Usuario,Usuarios")
#  time_zone                          :string(255)      default("America/Lima")
#  display_code_in_user_profile       :boolean          default(FALSE)
#  header_text_login_box              :string(255)      default("Ingrese sus datos de acceso")
#  recover_pass_text_login_box        :string(255)      default("¿Olvidó su contraseña?")
#  user_profile_tab_text_dashboard    :string(255)      default("Perfil de Usuario")
#  locale                             :string(255)      default("es_PE")
#  training_type_alias                :string(255)      default("Tipo de capacitación")
#  training_type_user_courses_submenu :boolean          default(FALSE)
#  notifications_email                :string(255)      default("notificaciones@exa.pe")
#  accepts_comments                   :boolean          default(TRUE)
#  accepts_values                     :boolean          default(TRUE)
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  is_productive                      :boolean          default(TRUE)
#  use_pgp                            :boolean          default(FALSE)
#  year_user_courses_submenu          :boolean          default(FALSE)
#  active_directory                   :boolean          default(FALSE)
#  alias_unit                         :string(255)      default("Unidad,Unidades")
#  upper_course_name                  :boolean          default(FALSE)
#  jobs_extra_info                    :boolean          default(TRUE)
#  is_security                        :boolean          default(FALSE)
#  iv                                 :binary
#  second_last_name                   :boolean          default(FALSE)
#  active_directory_auto_redirection  :boolean          default(TRUE)
#  email_altas                        :string(255)
#  email_bajas                        :string(255)
#

require 'spec_helper'

describe Company do
  pending "add some examples to (or delete) #{__FILE__}"
end
