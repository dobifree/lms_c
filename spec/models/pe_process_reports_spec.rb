# == Schema Information
#
# Table name: pe_process_reports
#
#  id                                         :integer          not null, primary key
#  pe_process_id                              :integer
#  rep_evaluated_evaluators_configuration     :boolean          default(FALSE)
#  created_at                                 :datetime         not null
#  updated_at                                 :datetime         not null
#  rep_assessment_status                      :boolean          default(FALSE)
#  rep_validation_status                      :boolean          default(FALSE)
#  rep_calibration_status                     :boolean          default(FALSE)
#  rep_feedback_status                        :boolean          default(FALSE)
#  rep_assessment_consolidated_results        :boolean          default(FALSE)
#  rep_assessment_detailed_results            :boolean          default(FALSE)
#  rep_evaluations_configutation              :boolean          default(FALSE)
#  rep_general_status                         :boolean          default(FALSE)
#  rep_consolidated_feedback                  :boolean          default(FALSE)
#  rep_detailed_final_results_evaluations     :boolean          default(FALSE)
#  rep_detailed_final_results_questions       :boolean          default(FALSE)
#  rep_consolidated_feedback_accepted         :boolean          default(FALSE)
#  rep_consolidated_feedback_accepted_survey  :boolean          default(FALSE)
#  rep_detailed_auto                          :boolean          default(FALSE)
#  rep_detailed_auto_elements                 :string(255)
#  rep_detailed_text_1                        :text
#  rep_detailed_text_2                        :text
#  rep_rol_privado                            :boolean          default(FALSE)
#  rep_assessment_detailed_results_i          :boolean
#  rep_tracking_status                        :boolean          default(FALSE)
#  rep_detailed_final_results_evaluations_cal :boolean
#  rep_assign_status                          :boolean          default(FALSE)
#  rep_res_habitat_1                          :boolean          default(FALSE)
#

require 'spec_helper'

describe PeProcessReports do
  pending "add some examples to (or delete) #{__FILE__}"
end
