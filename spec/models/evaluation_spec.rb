# == Schema Information
#
# Table name: evaluations
#
#  id                           :integer          not null, primary key
#  nombre                       :string(255)
#  descripcion                  :text
#  program_id                   :integer
#  level_id                     :integer
#  course_id                    :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  libre                        :boolean          default(TRUE)
#  numero                       :integer
#  activa                       :boolean          default(TRUE)
#  antes_de_unit_id             :integer
#  despues_de_unit_id           :integer
#  antes_de_evaluation_id       :integer
#  despues_de_evaluation_id     :integer
#  antes_de_poll_id             :integer
#  despues_de_poll_id           :integer
#  orden                        :integer          default(1)
#  master_evaluation_id         :integer
#  numero_preguntas             :integer
#  visualizacion_lote           :boolean
#  visualizar_resultado_detalle :boolean
#  visualizar_resultado_resumen :boolean
#  tiempo                       :integer
#  aplicar_tiempo               :boolean
#  nota_base                    :integer          default(0)
#  can_be_paused                :boolean          default(FALSE)
#  weight                       :integer          default(1)
#  virtual                      :boolean          default(TRUE)
#

require 'spec_helper'

describe Evaluation do
  pending "add some examples to (or delete) #{__FILE__}"
end
