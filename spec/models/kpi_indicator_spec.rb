# == Schema Information
#
# Table name: kpi_indicators
#
#  id                  :integer          not null, primary key
#  position            :integer
#  name                :string(255)
#  weight              :float
#  kpi_set_id          :integer
#  goal                :float
#  unit                :string(255)
#  indicator_type      :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  value               :float
#  percentage          :float
#  final_result_method :integer          default(0)
#  kpi_subset_id       :integer
#  power               :float            default(1.0)
#  leverage            :float            default(0.0)
#  opf_annual_goal     :string(255)
#

require 'spec_helper'

describe KpiIndicator do
  pending "add some examples to (or delete) #{__FILE__}"
end
