# == Schema Information
#
# Table name: dncps
#
#  id                               :integer          not null, primary key
#  planning_process_id              :integer
#  planning_process_company_unit_id :integer
#  company_unit_area_id             :integer
#  oportunidad_mejora               :text
#  tema_capacitacion                :string(255)
#  objetivos_curso                  :text
#  costo_directo                    :float
#  costo_indirecto                  :float
#  training_mode_id                 :integer
#  training_type_id                 :integer
#  meses                            :string(255)
#  tiempo_capacitacion              :integer
#  training_provider_id             :integer
#  impacto_total                    :float
#  dnc_id                           :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  nota_minima                      :integer
#  eval_presencial                  :boolean          default(TRUE)
#  poll_presencial                  :boolean          default(TRUE)
#  asistencia_presencial            :boolean          default(TRUE)
#  course_id                        :integer
#  observaciones                    :text
#  presup_ampliado                  :boolean
#  number_students                  :integer
#  eval_eficacia                    :boolean
#  training_program_id              :integer
#

require 'spec_helper'

describe Dncp do
  pending "add some examples to (or delete) #{__FILE__}"
end
