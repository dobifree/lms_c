# == Schema Information
#
# Table name: pe_assessment_final_evaluations
#
#  id                             :integer          not null, primary key
#  pe_process_id                  :integer
#  pe_member_id                   :integer
#  pe_evaluation_id               :integer
#  points                         :float
#  percentage                     :float
#  last_update                    :datetime
#  registered_by_manager          :boolean          default(FALSE)
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  pe_dimension_group_id          :integer
#  pe_area_id                     :integer
#  pe_original_dimension_group_id :integer
#  calibration_comment            :text
#

require 'spec_helper'

describe PeAssessmentFinalEvaluation do
  pending "add some examples to (or delete) #{__FILE__}"
end
