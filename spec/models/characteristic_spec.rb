# == Schema Information
#
# Table name: characteristics
#
#  id                             :integer          not null, primary key
#  nombre                         :string(255)
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  publica                        :boolean          default(FALSE)
#  mini_ficha                     :boolean          default(FALSE)
#  orden_mini_ficha               :integer
#  planning_process               :boolean
#  orden_planning_process         :integer
#  porcentaje_participante_sence  :boolean          default(FALSE)
#  basic_report                   :boolean          default(FALSE)
#  orden_basic_report             :integer
#  hr_process_assessment          :boolean
#  orden_hr_process_assessment    :integer
#  position                       :integer
#  filter_assessment_report       :boolean          default(FALSE)
#  order_filter_assessment_report :integer
#  characteristic_type_id         :integer
#  pe_rep_consolidate_results     :boolean
#  data_type_id                   :integer          default(0)
#  updated_by_manager             :boolean          default(FALSE)
#  updated_by_user                :boolean          default(FALSE)
#  boss_characteristic_id         :integer
#  register_characteristic_id     :integer
#  parent_characteristic_id       :integer
#  register_position              :integer
#  restricted_to_managers         :boolean          default(FALSE)
#  has_history                    :boolean          default(TRUE)
#  is_company                     :boolean          default(FALSE)
#  encrypted                      :boolean          default(FALSE)
#  subase                         :boolean          default(FALSE)
#  subaso                         :boolean          default(FALSE)
#  query_by_boss                  :boolean          default(TRUE)
#  query_by_user                  :boolean          default(TRUE)
#  query_by_others                :boolean          default(TRUE)
#  currency_subaso                :boolean          default(FALSE)
#  from_date_work                 :boolean          default(FALSE)
#  from_date_progresivas          :boolean          default(FALSE)
#  from_date_r_progresivas        :boolean          default(FALSE)
#  rol_security                   :boolean
#  has_children                   :boolean
#  required_new                   :boolean
#  characteristic_value_id        :integer
#  char_value_selected            :boolean          default(TRUE)
#  contrato_plazo_secu            :boolean          default(FALSE)
#  tipo_contrato_secu             :boolean          default(FALSE)
#  required_new_position          :integer
#  required_new_c                 :boolean          default(FALSE)
#

require 'spec_helper'

describe Characteristic do
  pending "add some examples to (or delete) #{__FILE__}"
end
