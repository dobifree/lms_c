# == Schema Information
#
# Table name: hr_evaluation_type_elements
#
#  id                          :integer          not null, primary key
#  nombre                      :string(255)
#  nivel                       :integer
#  valor_minimo                :float
#  valor_maximo                :float
#  formula_promedio            :boolean          default(TRUE)
#  peso_minimo                 :integer
#  peso_maximo                 :integer
#  suma_total_pesos            :integer
#  plan_accion                 :boolean          default(FALSE)
#  porcentaje_logro            :boolean          default(FALSE)
#  indicador_logro             :boolean          default(FALSE)
#  hr_evaluation_type_id       :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  cantidad_maxima             :integer
#  carga_manual                :boolean
#  evaluacion_colaborativa     :boolean
#  tiene_clasificacion         :boolean
#  nombre_clasificacion        :string(255)
#  aplica_evaluacion           :boolean          default(TRUE)
#  indicador_logro_descripcion :boolean          default(TRUE)
#

require 'spec_helper'

describe HrEvaluationTypeElement do
  pending "add some examples to (or delete) #{__FILE__}"
end
