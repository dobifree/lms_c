# == Schema Information
#
# Table name: hr_process_dimension_es
#
#  id                      :integer          not null, primary key
#  orden                   :integer
#  porcentaje              :integer
#  hr_evaluation_type_id   :integer
#  hr_process_dimension_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  eval_jefe               :boolean          default(FALSE)
#  eval_auto               :boolean          default(FALSE)
#  eval_par                :boolean          default(FALSE)
#  eval_sub                :boolean          default(FALSE)
#  eval_jefe_peso          :integer          default(0)
#  eval_auto_peso          :integer          default(0)
#  eval_par_peso           :integer          default(0)
#  eval_sub_peso           :integer          default(0)
#  eval_cli                :boolean          default(FALSE)
#  eval_prov               :boolean          default(FALSE)
#  eval_cli_peso           :integer          default(0)
#  eval_prov_peso          :integer          default(0)
#  registro_por_gestor     :boolean          default(FALSE)
#

require 'spec_helper'

describe HrProcessDimensionE do
  pending "add some examples to (or delete) #{__FILE__}"
end
