# == Schema Information
#
# Table name: polls
#
#  id                         :integer          not null, primary key
#  nombre                     :string(255)
#  descripcion                :text
#  program_id                 :integer
#  level_id                   :integer
#  course_id                  :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  libre                      :boolean          default(TRUE)
#  numero                     :integer
#  activa                     :boolean          default(TRUE)
#  antes_de_unit_id           :integer
#  despues_de_unit_id         :integer
#  antes_de_evaluation_id     :integer
#  despues_de_evaluation_id   :integer
#  antes_de_poll_id           :integer
#  despues_de_poll_id         :integer
#  orden                      :integer          default(1)
#  obligatoria                :boolean          default(FALSE)
#  master_poll_id             :integer
#  tipo_satisfaccion          :boolean          default(FALSE)
#  program_course_instance_id :integer
#  virtual                    :boolean          default(TRUE)
#

require 'spec_helper'

describe Poll do
  pending "add some examples to (or delete) #{__FILE__}"
end
