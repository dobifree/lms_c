# == Schema Information
#
# Table name: elec_process_rounds
#
#  id                                :integer          not null, primary key
#  elec_process_id                   :integer
#  name                              :string(255)
#  description                       :text
#  position                          :integer
#  mandatory                         :boolean          default(FALSE)
#  lifespan                          :integer
#  qty_required                      :integer          default(1)
#  show_live_results                 :boolean          default(FALSE)
#  show_final_results                :boolean          default(FALSE)
#  threshold_pctg_universe           :integer          default(0)
#  threshold_pctg_voters             :integer          default(0)
#  threshold_qty_voters              :integer          default(1)
#  qty_min_podium                    :integer          default(1)
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  show_live_results_extra_time      :integer          default(0)
#  show_final_results_extra_time     :integer          default(0)
#  show_list_categories_in_dashboard :boolean          default(FALSE)
#  show_live_my_voters               :boolean          default(FALSE)
#  show_live_my_voters_extra_time    :integer          default(0)
#  show_final_my_voters              :boolean          default(FALSE)
#  show_final_my_voters_extra_time   :integer          default(0)
#  show_my_voters_since_qty_votes    :integer          default(0)
#  show_my_voters_detail             :boolean          default(FALSE)
#  list_like_minificha               :boolean          default(FALSE)
#

require 'spec_helper'

describe ElecProcessRound do
  pending "add some examples to (or delete) #{__FILE__}"
end
