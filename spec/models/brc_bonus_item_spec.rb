# == Schema Information
#
# Table name: brc_bonus_items
#
#  id              :integer          not null, primary key
#  brc_process_id  :integer
#  brc_member_id   :integer
#  brc_category_id :integer
#  p_a             :boolean          default(TRUE)
#  percentage      :float
#  amount          :decimal(20, 4)   default(0.0)
#  rejected        :boolean          default(FALSE)
#  val_comment     :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  secu_cohade_id  :integer
#  amount_c        :decimal(20, 4)   default(0.0)
#  validated       :boolean          default(FALSE)
#

require 'spec_helper'

describe BrcBonusItem do
  pending "add some examples to (or delete) #{__FILE__}"
end
