# == Schema Information
#
# Table name: pe_process_notifications
#
#  id                                            :integer          not null, primary key
#  pe_process_id                                 :integer
#  step_feedback_accepted_ask_to_accept_subject  :string(255)
#  step_feedback_accepted_ask_to_accept_body     :text
#  created_at                                    :datetime         not null
#  updated_at                                    :datetime         not null
#  step_validation_accepted_subject              :string(255)
#  step_validation_accepted_body                 :text
#  step_validation_rejected_subject              :string(255)
#  step_validation_rejected_body                 :text
#  step_assessment_to_validator_subject          :string(255)
#  step_assessment_to_validator_body             :text
#  step_feedback_accepted_to_boss_subject        :string(255)
#  step_feedback_accepted_to_boss_body           :text
#  step_query_definitions_accept_subject         :string(255)
#  step_query_definitions_accept_body            :text
#  step_query_definitions_accept_cc              :string(255)
#  step_query_definitions_accept_subject_to_boss :string(255)
#  step_query_definitions_accept_body_to_boss    :text
#

require 'spec_helper'

describe PeProcessNotification do
  pending "add some examples to (or delete) #{__FILE__}"
end
