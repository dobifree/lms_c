# == Schema Information
#
# Table name: pe_rels
#
#  id                                  :integer          not null, primary key
#  pe_process_id                       :integer
#  rel                                 :integer
#  alias                               :string(255)
#  alias_reverse                       :string(255)
#  alias_plural                        :string(255)
#  alias_reverse_plural                :string(255)
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  position                            :integer          default(0)
#  allows_shared_comments              :boolean          default(FALSE)
#  display_shared_comments_in_feedback :boolean          default(FALSE)
#  available_in_selection              :boolean          default(FALSE)
#  max_number_to_select                :integer          default(1)
#  max_num_times_to_be_selected        :integer          default(1)
#  available_in_assign                 :boolean          default(FALSE)
#  max_number_to_assign                :integer          default(1)
#  min_number_to_assign                :integer          default(1)
#  assign_total_count                  :boolean          default(FALSE)
#

require 'spec_helper'

describe PeRel do
  pending "add some examples to (or delete) #{__FILE__}"
end
