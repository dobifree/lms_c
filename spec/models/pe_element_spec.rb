# == Schema Information
#
# Table name: pe_elements
#
#  id                                  :integer          not null, primary key
#  name                                :string(255)
#  pe_evaluation_id                    :integer
#  pe_element_id                       :integer
#  assessed                            :boolean          default(TRUE)
#  visible                             :boolean          default(TRUE)
#  assessment_method                   :integer
#  calculus_method                     :integer
#  max_number                          :integer
#  min_number                          :integer
#  max_weight                          :integer
#  min_weight                          :integer
#  weight_sum                          :integer
#  element_def_by                      :integer
#  element_def_by_rol                  :string(255)
#  assessment_method_def_by            :integer
#  assessment_method_def_by_rol        :string(255)
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  display_name                        :boolean          default(TRUE)
#  alternatives_display_method         :integer
#  position                            :integer          default(1)
#  simple                              :boolean          default(TRUE)
#  kpi_dashboard_id                    :integer
#  allows_individual_comments          :boolean          default(FALSE)
#  available_indicator_types           :string(255)
#  display_weight                      :boolean          default(FALSE)
#  min_numeric_value                   :integer
#  max_numeric_value                   :integer
#  display_result                      :boolean          default(FALSE)
#  plural_name                         :string(255)      default("")
#  allow_individual_comments_with_boss :boolean          default(FALSE)
#  fix_rank_models                     :integer          default(0)
#  allow_activities                    :boolean          default(FALSE)
#  allow_action_plan                   :boolean          default(FALSE)
#  allow_note                          :boolean          default(FALSE)
#  alias_note                          :string(255)      default("Nota")
#  alias_comment                       :string(255)      default("Comentario")
#  alias_comment_2                     :string(255)      default("Comentario")
#  desc_placeholder                    :string(255)
#  note_placeholder                    :string(255)
#  default_goal                        :string(255)
#  default_unit                        :string(255)
#  note_compulsory                     :boolean          default(FALSE)
#  cant_change_goal                    :boolean          default(FALSE)
#  unit_allow_digits                   :boolean          default(TRUE)
#

require 'spec_helper'

describe PeElement do
  pending "add some examples to (or delete) #{__FILE__}"
end
