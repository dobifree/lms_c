# == Schema Information
#
# Table name: user_characteristic_records
#
#  id                      :integer          not null, primary key
#  user_id                 :integer
#  characteristic_id       :integer
#  value                   :text
#  from_date               :datetime
#  to_date                 :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  value_string            :string(255)
#  value_text              :text
#  value_date              :date
#  value_int               :integer
#  value_float             :float
#  value_file              :string(255)
#  characteristic_value_id :integer
#  created_by_user_id      :integer
#  created_by_admin_id     :integer
#  finished_by_user_id     :integer
#  finished_by_admin_id    :integer
#  value_ct_image_url      :string(255)
#  value_encrypted         :binary
#

require 'spec_helper'

describe UserCharacteristicRecord do
  pending "add some examples to (or delete) #{__FILE__}"
end
