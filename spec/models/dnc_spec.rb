# == Schema Information
#
# Table name: dncs
#
#  id                               :integer          not null, primary key
#  planning_process_id              :integer
#  planning_process_company_unit_id :integer
#  company_unit_area_id             :integer
#  oportunidad_mejora               :text
#  tema_capacitacion                :string(255)
#  objetivos_curso                  :text
#  costo_directo                    :float
#  costo_indirecto                  :float
#  training_mode_id                 :integer
#  training_type_id                 :integer
#  meses                            :string(255)
#  tiempo_capacitacion              :integer
#  training_provider_id             :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  impacto_total                    :float
#  enviado_aprobar_area             :boolean          default(FALSE)
#  requiere_modificaciones_area     :boolean          default(FALSE)
#  aprobado_area                    :boolean          default(FALSE)
#  aprobado_unidad                  :boolean          default(FALSE)
#  observaciones                    :text
#  number_students                  :integer          default(0)
#  eval_eficacia                    :boolean
#  training_program_id              :integer
#

require 'spec_helper'

describe Dnc do
  pending "add some examples to (or delete) #{__FILE__}"
end
