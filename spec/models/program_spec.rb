# == Schema Information
#
# Table name: programs
#
#  id                 :integer          not null, primary key
#  nombre             :string(255)
#  descripcion        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  nombre_corto       :string(255)
#  codigo             :string(255)
#  color              :string(255)
#  especifico         :boolean          default(TRUE)
#  plan_anual         :boolean          default(FALSE)
#  activo             :boolean          default(TRUE)
#  orden              :integer
#  managed_by_manager :boolean          default(FALSE)
#

require 'spec_helper'

describe Program do
  pending "add some examples to (or delete) #{__FILE__}"
end
