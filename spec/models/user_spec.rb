# == Schema Information
#
# Table name: users
#
#  id                      :integer          not null, primary key
#  codigo                  :string(255)
#  email                   :string(255)
#  password_digest         :string(255)
#  nombre                  :string(255)
#  apellidos               :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  codigo_rec_pass         :string(255)
#  fecha_rec_pass          :string(255)
#  jefe_capacitacion       :boolean          default(FALSE)
#  foto                    :string(255)
#  activo                  :boolean          default(TRUE)
#  requiere_cambio_pass    :boolean          default(FALSE)
#  jefe_biblioteca         :boolean          default(FALSE)
#  node_id                 :integer
#  j_job_id                :integer
#  perpetual_active        :boolean
#  employee                :boolean          default(TRUE)
#  private_key             :text
#  public_key              :text
#  active_directory_id     :string(255)
#  he_able                 :boolean          default(FALSE)
#  j_cost_center_id        :integer
#  delete_date             :date
#  user_delete_reason_id   :integer
#  delete_comment          :text
#  from_date               :date
#  last_name_1             :string(255)
#  last_name_2             :string(255)
#  user_delete_type_id     :integer
#  show_azure_landing_page :boolean          default(TRUE)
#  flexible_sc             :boolean          default(FALSE)
#  secu_external           :boolean
#

require 'spec_helper'

describe User do
  pending "add some examples to (or delete) #{__FILE__}"
end
