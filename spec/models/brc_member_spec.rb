# == Schema Information
#
# Table name: brc_members
#
#  id                      :integer          not null, primary key
#  brc_process_id          :integer
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  user_define_id          :integer
#  user_validate_id        :integer
#  status_def              :boolean          default(FALSE)
#  status_val              :boolean          default(FALSE)
#  status_bon_cal          :boolean          default(FALSE)
#  bonus_total             :decimal(20, 4)   default(0.0)
#  bonus_liquido           :decimal(20, 4)   default(0.0)
#  bonus_retencion         :decimal(20, 4)   default(0.0)
#  bonus_p_retencion       :float            default(0.0)
#  status_choose           :boolean          default(FALSE)
#  bonus_apv               :decimal(20, 4)   default(0.0)
#  bonus_convenio          :decimal(20, 4)   default(0.0)
#  status_rej              :boolean          default(FALSE)
#  bonus_cc                :boolean          default(TRUE)
#  brc_apv_institution_id  :integer
#  brc_con_institution_id  :integer
#  bonus_con_uf            :decimal(20, 4)   default(0.0)
#  choose_by_own           :boolean          default(FALSE)
#  choose_date             :datetime
#  format_rejected         :boolean          default(FALSE)
#  format_rejected_comment :string(255)
#

require 'spec_helper'

describe BrcMember do
  pending "add some examples to (or delete) #{__FILE__}"
end
