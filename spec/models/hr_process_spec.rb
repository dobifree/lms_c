# == Schema Information
#
# Table name: hr_processes
#
#  id                                    :integer          not null, primary key
#  nombre                                :string(255)
#  abierto                               :boolean          default(TRUE)
#  fecha_inicio                          :datetime
#  fecha_fin                             :datetime
#  year                                  :integer
#  created_at                            :datetime         not null
#  updated_at                            :datetime         not null
#  hr_process_template_id                :integer
#  muestra_cajas                         :boolean
#  muestra_grupos                        :boolean
#  tiene_paso_feedback                   :boolean
#  calib_cuadrante                       :boolean
#  calib_dimension                       :boolean
#  muestra_cuadrantes_calib              :boolean
#  show_quien_evaluo                     :boolean          default(TRUE)
#  jefe_consulta_resultados              :boolean          default(FALSE)
#  jefe_ve_autoeval                      :boolean          default(FALSE)
#  ae_avisa_jefe_email                   :boolean          default(FALSE)
#  alias_jefe                            :string(255)      default("Supervisor")
#  alias_jefe_inverso                    :string(255)      default("Colaborador")
#  alias_jefe_plu                        :string(255)      default("Supervisores")
#  alias_jefe_inverso_plu                :string(255)      default("Colaboradores")
#  alias_par                             :string(255)      default("Par")
#  alias_par_inverso                     :string(255)      default("Par")
#  alias_par_plu                         :string(255)      default("Pares")
#  alias_par_inverso_plu                 :string(255)      default("Pares")
#  alias_sub                             :string(255)      default("Colaborador")
#  alias_sub_inverso                     :string(255)      default("Supervisor")
#  alias_sub_plu                         :string(255)      default("Colaboradores")
#  alias_sub_inverso_plu                 :string(255)      default("Supervisores")
#  alias_cli                             :string(255)      default("Cliente")
#  alias_cli_inverso                     :string(255)      default("Proveedor")
#  alias_cli_plu                         :string(255)      default("Clientes")
#  alias_cli_inverso_plu                 :string(255)      default("Proveedores")
#  alias_prov                            :string(255)      default("Proveedor")
#  alias_prov_inverso                    :string(255)      default("Cliente")
#  alias_prov_plu                        :string(255)      default("Proveedores")
#  alias_prov_inverso_plu                :string(255)      default("Clientes")
#  jefe_cons_estado_avance               :boolean          default(FALSE)
#  jefe_cons_res_final                   :boolean          default(FALSE)
#  positions_final_results_report        :string(255)
#  boss_check_results_while_assess       :boolean
#  boss_check_final_report_on_feedback   :boolean
#  step_feedback_confirmation            :boolean
#  step_feedback_confirmation_mail       :boolean
#  boss_check_final_results_while_assess :boolean
#  message_boss_while_assess             :text
#  jefe_consulta_resultados_rec          :boolean
#  show_user_profile                     :boolean
#  tiene_paso_feedback_modo              :integer          default(1)
#  label_1_t                             :string(255)
#  label_1_c                             :text
#  label_2_t                             :string(255)
#  label_2_c                             :text
#  label_3_t                             :string(255)
#  label_3_c                             :text
#

require 'spec_helper'

describe HrProcess do
  pending "add some examples to (or delete) #{__FILE__}"
end
