# == Schema Information
#
# Table name: units
#
#  id                       :integer          not null, primary key
#  numero                   :integer
#  nombre                   :string(255)
#  descripcion              :text
#  course_id                :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  libre                    :boolean          default(TRUE)
#  activa                   :boolean          default(TRUE)
#  antes_de_unit_id         :integer
#  despues_de_unit_id       :integer
#  antes_de_evaluation_id   :integer
#  despues_de_evaluation_id :integer
#  antes_de_poll_id         :integer
#  despues_de_poll_id       :integer
#  master_unit_id           :integer
#  orden                    :integer          default(1)
#  finaliza_automaticamente :boolean          default(TRUE)
#  virtual                  :boolean          default(TRUE)
#

require 'spec_helper'

describe Unit do
  pending "add some examples to (or delete) #{__FILE__}"
end
