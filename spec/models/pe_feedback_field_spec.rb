# == Schema Information
#
# Table name: pe_feedback_fields
#
#  id                               :integer          not null, primary key
#  pe_process_id                    :integer
#  position                         :integer
#  name                             :string(255)
#  simple                           :boolean          default(TRUE)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  field_type                       :integer          default(0)
#  pe_feedback_field_list_id        :integer
#  max_number_of_compound_feedbacks :integer          default(3)
#  required                         :boolean          default(TRUE)
#  pe_feedback_field_list_item_id   :integer
#  list_item_selected               :boolean          default(TRUE)
#

require 'spec_helper'

describe PeFeedbackField do
  pending "add some examples to (or delete) #{__FILE__}"
end
