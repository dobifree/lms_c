# == Schema Information
#
# Table name: course_certificates
#
#  id                  :integer          not null, primary key
#  course_id           :integer
#  template            :string(255)
#  rango_desde         :date
#  rango_hasta         :date
#  nombre              :boolean
#  nombre_x            :integer
#  nombre_y            :integer
#  desde               :boolean
#  desde_x             :integer
#  desde_y             :integer
#  hasta               :boolean
#  hasta_x             :integer
#  hasta_y             :integer
#  inicio              :boolean
#  inicio_x            :integer
#  inicio_y            :integer
#  fin                 :boolean
#  fin_x               :integer
#  fin_y               :integer
#  nota                :boolean
#  nota_x              :integer
#  nota_y              :integer
#  fecha_source        :string(255)
#  fecha_source_delay  :integer          default(0)
#  fecha               :boolean
#  fecha_x             :integer
#  fecha_y             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  nombre_curso        :boolean
#  nombre_curso_x      :integer
#  nombre_curso_y      :integer
#  nombre_color        :string(255)      default("000000")
#  nombre_curso_color  :string(255)      default("000000")
#  fecha_color         :string(255)      default("000000")
#  desde_color         :string(255)      default("000000")
#  hasta_color         :string(255)      default("000000")
#  inicio_color        :string(255)      default("000000")
#  fin_color           :string(255)      default("000000")
#  nota_color          :string(255)      default("000000")
#  nombre_letra        :string(255)      default("Helvetica")
#  nombre_curso_letra  :string(255)      default("Helvetica")
#  fecha_letra         :string(255)      default("Helvetica")
#  desde_letra         :string(255)      default("Helvetica")
#  hasta_letra         :string(255)      default("Helvetica")
#  inicio_letra        :string(255)      default("Helvetica")
#  fin_letra           :string(255)      default("Helvetica")
#  nota_letra          :string(255)      default("Helvetica")
#  nombre_size         :integer          default(20)
#  nombre_curso_size   :integer          default(20)
#  fecha_size          :integer          default(20)
#  desde_size          :integer          default(20)
#  hasta_size          :integer          default(20)
#  inicio_size         :integer          default(20)
#  fin_size            :integer          default(20)
#  nota_size           :integer          default(20)
#  desde_formato       :string(255)      default("day_snmonth_year")
#  hasta_formato       :string(255)      default("day_snmonth_year")
#  inicio_formato      :string(255)      default("day_snmonth_year")
#  fin_formato         :string(255)      default("day_snmonth_year")
#  fecha_formato       :string(255)      default("full_date")
#  program_instance_id :integer
#  program_id          :integer
#

require 'spec_helper'

describe CourseCertificate do
  pending "add some examples to (or delete) #{__FILE__}"
end
