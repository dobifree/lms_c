# == Schema Information
#
# Table name: sel_processes
#
#  id                    :integer          not null, primary key
#  sel_template_id       :integer
#  name                  :string(255)
#  description           :text
#  qty_required          :integer
#  source                :integer
#  apply_available       :boolean
#  from_date             :date
#  to_date               :date
#  finished_at           :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  file                  :string(255)
#  claimant_area         :string(255)
#  code                  :string(255)
#  sel_requirement_id    :integer
#  registered_at         :datetime
#  registered_by_user_id :integer
#  finished_by_user_id   :integer
#  finished              :boolean          default(FALSE)
#  traceable             :boolean          default(TRUE)
#

require 'spec_helper'

describe SelProcess do
  pending "add some examples to (or delete) #{__FILE__}"
end
