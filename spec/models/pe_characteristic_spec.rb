# == Schema Information
#
# Table name: pe_characteristics
#
#  id                                    :integer          not null, primary key
#  pe_process_id                         :integer
#  characteristic_id                     :integer
#  created_at                            :datetime         not null
#  updated_at                            :datetime         not null
#  pos_rep_assessment_status             :integer
#  active_rep_assessment_status          :boolean          default(FALSE)
#  pos_gui_cal_committee                 :integer
#  active_gui_cal_committee              :boolean          default(FALSE)
#  pos_gui_cal_member                    :integer
#  active_gui_cal_member                 :boolean          default(FALSE)
#  pos_gui_feedback                      :integer
#  active_gui_feedback                   :boolean          default(FALSE)
#  pos_rep_calibration_status            :integer
#  active_rep_calibration_status         :boolean          default(FALSE)
#  pos_rep_feedback_status               :integer
#  active_rep_feedback_status            :boolean          default(FALSE)
#  pos_rep_consolidated_final_results    :integer
#  active_rep_consolidated_final_results :boolean          default(FALSE)
#  pos_gui_validation                    :integer
#  active_gui_validation                 :boolean          default(FALSE)
#  pos_rep_detailed_final_results        :integer
#  active_rep_detailed_final_results     :boolean
#  pos_gui_selection                     :integer
#  active_gui_selection                  :boolean          default(FALSE)
#  pos_reps_configuration                :integer
#  active_reps_configuration             :boolean          default(FALSE)
#  active_segmentation                   :boolean
#

require 'spec_helper'

describe PeCharacteristic do
  pending "add some examples to (or delete) #{__FILE__}"
end
