# == Schema Information
#
# Table name: user_ct_module_privileges
#
#  id                                   :integer          not null, primary key
#  user_id                              :integer
#  created_at                           :datetime         not null
#  updated_at                           :datetime         not null
#  sel_make_requirement                 :boolean          default(FALSE)
#  mpi_view_users                       :boolean          default(FALSE)
#  mpi_view_users_data                  :boolean          default(FALSE)
#  mpi_view_users_training_history      :boolean          default(FALSE)
#  mpi_view_users_pe_history            :boolean          default(FALSE)
#  mpi_view_users_informative_documents :boolean          default(FALSE)
#  um_manage_users                      :boolean          default(FALSE)
#  um_manage_users_data                 :boolean          default(FALSE)
#  um_manage_users_password             :boolean          default(FALSE)
#  sel_approve_requirements             :boolean          default(FALSE)
#  um_manage_users_email                :boolean          default(FALSE)
#  um_query_reports                     :boolean          default(FALSE)
#  lms_define                           :boolean          default(FALSE)
#  lms_enroll                           :boolean          default(FALSE)
#  lms_manage_enroll                    :boolean          default(FALSE)
#  lms_reports                          :boolean          default(FALSE)
#  um_create_users                      :boolean          default(FALSE)
#  um_delete_users                      :boolean          default(FALSE)
#  um_edit_users                        :boolean          default(FALSE)
#  um_manage_users_photo                :boolean          default(FALSE)
#

require 'spec_helper'

describe UserCtModulePrivileges do
  pending "add some examples to (or delete) #{__FILE__}"
end
