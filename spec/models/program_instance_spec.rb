# == Schema Information
#
# Table name: program_instances
#
#  id                       :integer          not null, primary key
#  name                     :string(255)
#  program_id               :integer
#  from_date                :date
#  to_date                  :date
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  minimum_grade            :float
#  percentage_minimum_grade :boolean
#  active                   :boolean          default(TRUE)
#  master_poll_id           :integer
#  num_passed_courses       :integer
#  sk_client                :string(255)
#  sk_rut_client            :string(255)
#

require 'spec_helper'

describe ProgramInstance do
  pending "add some examples to (or delete) #{__FILE__}"
end
