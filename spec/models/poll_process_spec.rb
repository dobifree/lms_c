# == Schema Information
#
# Table name: poll_processes
#
#  id                        :integer          not null, primary key
#  name                      :string(255)
#  description               :text
#  active                    :boolean
#  from_date                 :datetime
#  to_date                   :datetime
#  master_poll_id            :integer
#  finished                  :boolean
#  finished_at               :datetime
#  finished_by_user_id       :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  show_preview_results_user :boolean          default(FALSE)
#  to_everyone               :boolean          default(FALSE)
#  allow_pause               :boolean          default(FALSE)
#  grid_view                 :boolean          default(FALSE)
#  flow                      :integer          default(0)
#  show_group_names          :boolean          default(FALSE)
#

require 'spec_helper'

describe PollProcess do
  pending "add some examples to (or delete) #{__FILE__}"
end
