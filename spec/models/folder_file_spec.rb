# == Schema Information
#
# Table name: folder_files
#
#  id                    :integer          not null, primary key
#  nombre                :string(255)
#  crypted_name          :string(255)
#  folder_id             :integer
#  user_id               :integer
#  descargable           :boolean          default(TRUE)
#  extension             :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  descripcion           :text
#  size                  :integer
#  valor                 :float            default(0.0)
#  oculto                :boolean          default(FALSE)
#  folder_file_father_id :integer
#  is_last               :boolean          default(TRUE)
#  download_token        :string(255)
#

require 'spec_helper'

describe FolderFile do
  pending "add some examples to (or delete) #{__FILE__}"
end
