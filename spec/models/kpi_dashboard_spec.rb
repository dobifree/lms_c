# == Schema Information
#
# Table name: kpi_dashboards
#
#  id                          :integer          not null, primary key
#  name                        :string(255)
#  period                      :string(255)
#  from                        :date
#  to                          :date
#  frequency                   :string(255)
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  upper_limit                 :float
#  lower_limit                 :float
#  csv                         :boolean
#  kpi_dashboard_type_id       :integer
#  def_view_kpis               :boolean          default(TRUE)
#  def_view_table              :boolean          default(TRUE)
#  def_view_percentage         :boolean          default(TRUE)
#  pt_rows                     :text
#  pt_cols                     :text
#  pt_aggregator               :text
#  pt_aggregator_field         :text
#  show_indicator_final_result :boolean          default(FALSE)
#  allow_subsets               :boolean          default(FALSE)
#  subset_alias                :string(255)
#  show_set_final_result       :boolean          default(FALSE)
#  show_set_results            :boolean
#  kpi_field_annual_goal       :boolean          default(FALSE)
#  def_view_goal               :boolean          default(TRUE)
#  allow_set_colors            :boolean          default(FALSE)
#  allow_indicator_colors      :boolean          default(FALSE)
#  w_p                         :boolean          default(TRUE)
#

require 'spec_helper'

describe KpiDashboard do
  pending "add some examples to (or delete) #{__FILE__}"
end
