# == Schema Information
#
# Table name: program_course_instances
#
#  id                            :integer          not null, primary key
#  program_course_id             :integer
#  program_instance_id           :integer
#  from_date                     :datetime
#  to_date                       :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  sence_hour_value              :float
#  sence_student_value           :float
#  sence_hours                   :float
#  sence_training_type           :string(255)
#  sence_region                  :string(255)
#  sence_otec_name               :string(255)
#  sence_otec_address            :string(255)
#  sence_otec_telephone          :string(255)
#  place                         :string(255)
#  contract_type                 :string(255)
#  minimum_grade                 :integer
#  minimum_assistance            :integer
#  sence_maximum_students_number :integer
#  sence_cbipartito              :string(255)
#  sence_dnc                     :string(255)
#  sence_comment                 :string(255)
#  sence_schedule                :string(255)
#  sale_hour_value               :float
#  sencenet                      :string(255)
#  purchase_order_number         :string(255)
#  sk_financing                  :string(255)
#  sk_external_certification     :boolean
#  sence_minimum_students_number :integer
#  city                          :string(255)
#  obra                          :string(255)
#  proyecto_ci                   :string(255)
#  description                   :string(255)
#  creator_id                    :integer
#

require 'spec_helper'

describe ProgramCourseInstance do
  pending "add some examples to (or delete) #{__FILE__}"
end
