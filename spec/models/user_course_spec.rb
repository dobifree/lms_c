# == Schema Information
#
# Table name: user_courses
#
#  id                         :integer          not null, primary key
#  enrollment_id              :integer
#  program_course_id          :integer
#  desde                      :datetime
#  hasta                      :datetime
#  limite_tiempo              :boolean
#  inicio                     :datetime
#  fin                        :datetime
#  numero_oportunidad         :integer
#  iniciado                   :boolean          default(FALSE)
#  finalizado                 :boolean          default(FALSE)
#  aprobado                   :boolean          default(FALSE)
#  nota                       :float
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  course_id                  :integer
#  porcentaje_avance          :float            default(0.0)
#  dncp_id                    :integer
#  grupo                      :integer
#  asistencia                 :boolean
#  aplica_sence               :boolean          default(FALSE)
#  anulado                    :boolean          default(FALSE)
#  grupo_f                    :integer
#  program_course_instance_id :integer
#  user_id                    :integer
#  asistencia_unidades        :float            default(0.0)
#  enroll_np                  :boolean          default(FALSE)
#

require 'spec_helper'

describe UserCourse do
  pending "add some examples to (or delete) #{__FILE__}"
end
