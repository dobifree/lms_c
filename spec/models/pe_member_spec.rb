# == Schema Information
#
# Table name: pe_members
#
#  id                           :integer          not null, primary key
#  pe_process_id                :integer
#  user_id                      :integer
#  is_evaluated                 :boolean          default(FALSE)
#  is_evaluator                 :boolean          default(FALSE)
#  step_assessment              :boolean          default(FALSE)
#  step_assessment_date         :datetime
#  step_calibration             :boolean          default(FALSE)
#  step_calibration_date        :datetime
#  step_feedback                :boolean          default(FALSE)
#  step_feedback_date           :datetime
#  step_feedback_accepted       :boolean          default(FALSE)
#  step_feedback_accepted_date  :datetime
#  step_feedback_text           :text
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  pe_box_id                    :integer
#  pe_box_before_cal_id         :integer
#  calibration_comment          :text
#  feedback_provider_id         :integer
#  pe_area_id                   :integer
#  step_validation              :boolean          default(FALSE)
#  step_validation_date         :datetime
#  require_validation           :boolean          default(FALSE)
#  can_select_evaluated         :boolean          default(FALSE)
#  can_be_selected_as_evaluated :boolean          default(FALSE)
#  removed                      :boolean          default(FALSE)
#  step_assign                  :boolean          default(FALSE)
#  step_assign_date             :datetime
#

require 'spec_helper'

describe PeMember do
  pending "add some examples to (or delete) #{__FILE__}"
end
