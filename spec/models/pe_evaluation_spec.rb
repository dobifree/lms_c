# == Schema Information
#
# Table name: pe_evaluations
#
#  id                                                            :integer          not null, primary key
#  position                                                      :integer
#  name                                                          :string(255)
#  description                                                   :text
#  weight                                                        :integer
#  entered_by_manager                                            :boolean
#  pe_process_id                                                 :integer
#  pe_dimension_id                                               :integer
#  created_at                                                    :datetime         not null
#  updated_at                                                    :datetime         not null
#  points_to_ohp                                                 :float            default(10.0)
#  max_per_cant_answer                                           :float            default(100.0)
#  max_percentage                                                :float            default(120.0)
#  min_percentage                                                :float            default(0.0)
#  pe_delegated_evaluation_id                                    :integer
#  assessment_layout                                             :integer          default(0)
#  allow_tracking                                                :boolean          default(FALSE)
#  final_evaluation_operation_evaluators                         :integer          default(0)
#  final_evaluation_operation_evaluations                        :integer          default(0)
#  step_assessment_show_auto_to_boss                             :boolean          default(FALSE)
#  allow_definition_by_users                                     :boolean          default(FALSE)
#  allow_definition_by_users_accepted                            :boolean          default(FALSE)
#  num_steps_definition_by_users_validated_before_accepted       :integer          default(0)
#  num_steps_definition_by_users_validated_after_accepted        :integer          default(0)
#  alias_steps_definition_by_users_validated_before_accepted     :string(255)
#  alias_steps_definition_by_users_validated_after_accepted      :string(255)
#  label_not_ready_definition_by_users                           :string(255)      default("not_ready|default")
#  label_ready_definition_by_users                               :string(255)      default("ready|success")
#  label_not_ready_definition_by_users_accepted                  :string(255)      default("not_ready|success")
#  label_ready_definition_by_users_accepted                      :string(255)      default("ready|success")
#  label_not_ready_definition_by_users_validated_before_accepted :string(255)      default("not_ready|primary")
#  label_ready_definition_by_users_validated_before_accepted     :string(255)      default("ready|success")
#  label_rejected_definition_by_users_validated_before_accepted  :string(255)      default("rejected|danger")
#  label_not_ready_definition_by_users_validated_after_accepted  :string(255)      default("not_ready|primary")
#  label_ready_definition_by_users_validated_after_accepted      :string(255)      default("ready|success")
#  label_rejected_definition_by_users_validated_after_accepted   :string(255)      default("rejected|danger")
#  allow_watching_own_tracking                                   :boolean          default(FALSE)
#  allow_watching_own_definition                                 :boolean          default(FALSE)
#  watch_my_definition_only_when_finished                        :boolean          default(TRUE)
#  send_email_to_accept_definition                               :boolean          default(FALSE)
#  email_to_accept_definition_subject                            :string(255)
#  email_to_accept_definition_content                            :text
#  entered_by_manager_questions                                  :boolean          default(FALSE)
#  lay_force_nobr_goal_length                                    :integer          default(100)
#  definition_by_users_rol                                       :string(255)
#  label_not_initiated_definition_by_users                       :string(255)      default("not_initiated|default")
#  allow_tracking_with_boss                                      :boolean          default(FALSE)
#  def_allow_individual_comments_by_boss                         :boolean          default(FALSE)
#  def_allow_watch_individual_comments_by_boss                   :boolean          default(FALSE)
#  def_allow_individual_comments_with_boss                       :boolean          default(FALSE)
#  definition_by_users_accept_message                            :string(255)      default("Confirmo que conozco y estoy de acuerdo con la definición")
#  track_allow_individual_comments_by_boss                       :boolean          default(FALSE)
#  track_allow_watch_individual_comments_by_boss                 :boolean          default(FALSE)
#  track_allow_individual_comments_with_boss                     :boolean          default(FALSE)
#  assess_allow_individual_comments_by_boss                      :boolean          default(FALSE)
#  assess_allow_watch_individual_comments_by_boss                :boolean          default(FALSE)
#  assess_allow_individual_comments_with_boss                    :boolean          default(FALSE)
#  qdefs_allow_individual_comments_by_boss                       :boolean          default(FALSE)
#  qdefs_allow_watch_individual_comments_by_boss                 :boolean          default(FALSE)
#  qdefs_allow_individual_comments_with_boss                     :boolean          default(FALSE)
#  def_allow_action_plan_by_boss                                 :boolean          default(FALSE)
#  def_allow_watch_action_plan_by_boss                           :boolean          default(FALSE)
#  def_allow_action_plan_with_boss                               :boolean          default(FALSE)
#  informative                                                   :boolean          default(FALSE)
#  display_in_qdefs                                              :boolean          default(FALSE)
#  display_weight_in_qdefs                                       :boolean          default(TRUE)
#  definition_by_user_finish_message                             :string(255)      default("Confirmo que he finalizado la definición")
#  definition_by_user_validate_before_message                    :string(255)      default("Confirmo que valido la definición")
#  definition_by_user_validate_after_message                     :string(255)      default("Confirmo que valido la definición")
#  definition_by_user_finish_validated_message                   :string(255)      default("Confirmo que he corregido y finalizado la definición")
#  allow_definition_by_managers                                  :boolean          default(FALSE)
#  definition_by_users_pdf                                       :boolean          default(FALSE)
#  definition_by_users_accept_pdf                                :boolean          default(FALSE)
#  allow_calibration                                             :boolean          default(FALSE)
#  final_temporal_assessment                                     :boolean
#  display_in_qres                                               :boolean          default(FALSE)
#  display_weight_in_qres                                        :boolean          default(FALSE)
#  qres_allow_individual_comments_by_boss                        :boolean          default(FALSE)
#  qres_allow_watch_individual_comments_by_boss                  :boolean          default(FALSE)
#  qres_allow_individual_comments_with_boss                      :boolean          default(FALSE)
#  step_assessment_show_auto_comment_to_boss                     :boolean          default(FALSE)
#  pe_evaluation_group_id                                        :integer
#  display_weight_in_assessment                                  :boolean          default(FALSE)
#  send_email_after_accept_definition                            :boolean          default(FALSE)
#  email_after_accept_definition_subject                         :string(255)
#  email_after_accept_definition_content                         :text
#  definition_by_users_clone                                     :boolean          default(FALSE)
#  def_by_users_validated_before_accepted_manager                :boolean          default(TRUE)
#  def_by_users_validated_after_accepted_manager                 :boolean          default(TRUE)
#  assess_boss_copy_auto                                         :boolean          default(FALSE)
#  assess_allow_files_to_boss                                    :boolean          default(FALSE)
#  cod                                                           :string(255)
#

require 'spec_helper'

describe PeEvaluation do
  pending "add some examples to (or delete) #{__FILE__}"
end
