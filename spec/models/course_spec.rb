# == Schema Information
#
# Table name: courses
#
#  id                            :integer          not null, primary key
#  codigo                        :string(255)
#  nombre                        :string(255)
#  descripcion                   :text
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  nombre_corto                  :string(255)
#  numero_oportunidades          :integer          default(1)
#  duracion                      :integer          default(0)
#  objetivos                     :text
#  dedicacion_estimada           :float            default(0.0)
#  nota_minima                   :integer
#  dncp_id                       :integer
#  clonable                      :boolean          default(FALSE)
#  imagen                        :string(255)
#  eval_presencial               :boolean          default(FALSE)
#  poll_presencial               :boolean          default(FALSE)
#  asistencia_presencial         :boolean          default(FALSE)
#  sence_code                    :string(255)
#  sence_hours                   :float
#  sence_student_value           :float
#  muestra_porcentajes           :boolean          default(FALSE)
#  percentage_minimum_grade      :boolean          default(FALSE)
#  sence_hour_value              :float
#  sence_students_number         :integer
#  sence_area                    :string(255)
#  sence_speciality              :string(255)
#  sence_group                   :string(255)
#  sence_training_type           :string(255)
#  sence_authorization_date      :date
#  sence_end_validity_date       :date
#  sence_name                    :string(255)
#  sence_otec_name               :string(255)
#  sence_otec_address            :string(255)
#  sence_otec_telephone          :string(255)
#  sence_region                  :string(255)
#  minimum_assistance            :integer          default(0)
#  sence_maximum_students_number :integer
#  sence_minimum_students_number :integer
#  maximum_reference_value       :float
#  calculate_percentage          :boolean          default(FALSE)
#  unit_alias                    :string(255)      default("unidad")
#  first_unit_number             :integer          default(1)
#  training_type_id              :integer
#  managed_by_manager            :boolean          default(FALSE)
#  active                        :boolean          default(TRUE)
#  finish_msg_success            :text
#  finish_msg_failure            :text
#  finish_msg_failure_oportunity :text
#  evaluado                      :boolean          default(TRUE)
#  control_total_asistencia      :boolean          default(FALSE)
#  asistencia_matricula          :boolean          default(FALSE)
#  training_mode_id              :integer
#  asistencia_minima             :integer          default(100)
#

require 'spec_helper'

describe Course do
  pending "add some examples to (or delete) #{__FILE__}"
end
