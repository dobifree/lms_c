# == Schema Information
#
# Table name: pe_questions
#
#  id                             :integer          not null, primary key
#  description                    :text
#  weight                         :float            default(1.0)
#  pe_evaluation_id               :integer
#  pe_group_id                    :integer
#  pe_element_id                  :integer
#  pe_question_id                 :integer
#  has_comment                    :boolean
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  pe_member_evaluator_id         :integer
#  pe_member_evaluated_id         :integer
#  indicator_type                 :integer          default(0)
#  indicator_unit                 :string(255)
#  indicator_goal                 :decimal(30, 6)
#  indicator_discrete_goal        :string(255)
#  kpi_indicator_id               :integer
#  confirmed                      :boolean          default(TRUE)
#  pe_rel_id                      :integer
#  informative                    :boolean          default(FALSE)
#  accepted                       :boolean          default(TRUE)
#  ready                          :boolean          default(TRUE)
#  validated                      :boolean          default(TRUE)
#  creator_id                     :integer
#  entered_by_manager             :boolean          default(FALSE)
#  points                         :float
#  pe_alternative_id              :integer
#  indicator_achievement          :float
#  indicator_discrete_achievement :string(255)
#  stored_image_id                :integer
#  pe_question_model_id           :integer
#  power                          :float            default(1.0)
#  leverage                       :float            default(0.0)
#  number_of_activities           :integer          default(0)
#  comment                        :text
#  note                           :text
#  has_comment_compulsory         :boolean          default(FALSE)
#  questions_grouped_id           :integer
#  has_comment_2                  :boolean          default(FALSE)
#  has_comment_2_compulsory       :boolean          default(FALSE)
#

require 'spec_helper'

describe PeQuestion do
  pending "add some examples to (or delete) #{__FILE__}"
end
