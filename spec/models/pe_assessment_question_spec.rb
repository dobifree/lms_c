# == Schema Information
#
# Table name: pe_assessment_questions
#
#  id                             :integer          not null, primary key
#  pe_assessment_evaluation_id    :integer
#  pe_assessment_question_id      :integer
#  points                         :decimal(30, 6)
#  percentage                     :float
#  comment                        :text
#  pe_alternative_id              :integer
#  indicator_achievement          :decimal(30, 6)
#  last_update                    :datetime
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  pe_question_id                 :integer
#  indicator_discrete_achievement :string(255)
#  comment_2                      :text
#

require 'spec_helper'

describe PeAssessmentQuestion do
  pending "add some examples to (or delete) #{__FILE__}"
end
