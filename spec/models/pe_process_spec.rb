# == Schema Information
#
# Table name: pe_processes
#
#  id                                                     :integer          not null, primary key
#  name                                                   :string(255)
#  period                                                 :string(255)
#  created_at                                             :datetime         not null
#  updated_at                                             :datetime         not null
#  two_dimensions                                         :boolean          default(TRUE)
#  from_date                                              :datetime
#  to_date                                                :datetime
#  active                                                 :boolean          default(FALSE)
#  has_step_calibration                                   :boolean          default(FALSE)
#  has_step_feedback                                      :boolean          default(FALSE)
#  has_step_feedback_accepted                             :boolean          default(FALSE)
#  step_feedback_show_names                               :boolean
#  step_feedback_accepted_send_mail                       :boolean
#  rep_detailed_final_results_elements                    :string(255)
#  of_persons                                             :boolean          default(TRUE)
#  has_step_definition_by_users                           :boolean          default(FALSE)
#  has_step_tracking                                      :boolean          default(FALSE)
#  has_step_validation                                    :boolean          default(FALSE)
#  has_step_assessment                                    :boolean          default(FALSE)
#  description                                            :string(255)
#  rep_detailed_final_results_show_boss                   :boolean
#  rep_detailed_final_results_show_feedback               :string(255)
#  rep_detailed_final_results_show_calibration            :string(255)
#  step_assessment_show_rep_dfr_to_boss                   :boolean          default(FALSE)
#  step_assessment_show_rep_dfr_to_boss_text              :text
#  step_feedback_show_rep_dfr                             :boolean
#  step_feedback_show_rep_dfr_text                        :text
#  step_feedback_show_rep_dfr_pdf                         :boolean
#  step_feedback_show_calibration                         :boolean          default(FALSE)
#  rep_consolidated_final_results_col_boss                :boolean          default(FALSE)
#  has_step_my_results                                    :boolean          default(FALSE)
#  display_results_in_user_profile                        :boolean          default(FALSE)
#  display_results_in_my_people                           :boolean          default(FALSE)
#  display_user_profile_show_rep_dfr                      :boolean          default(FALSE)
#  display_my_people_show_rep_dfr                         :boolean          default(FALSE)
#  display_user_profile_show_rep_dfr_pdf                  :boolean          default(FALSE)
#  display_my_people_show_rep_dfr_pdf                     :boolean          default(FALSE)
#  has_rep_detailed_final_results_only_boss               :boolean          default(FALSE)
#  rep_detailed_final_results_only_boss_show_boss         :boolean          default(FALSE)
#  rep_detailed_final_results_only_boss_elements          :string(255)
#  rep_detailed_final_results_only_boss_show_calibration  :string(255)
#  rep_detailed_final_results_only_boss_show_feedback     :string(255)
#  step_definition_by_users_alias                         :string(255)      default("Definir")
#  step_tracking_alias                                    :string(255)      default("Seguimiento de")
#  step_assessment_alias                                  :string(255)      default("Evaluar")
#  step_calibration_alias                                 :string(255)      default("Calibrar")
#  step_feedback_alias                                    :string(255)      default("Retroalimentar")
#  step_definition_by_users_img                           :string(255)
#  step_tracking_img                                      :string(255)
#  step_assessment_img                                    :string(255)
#  has_step_definition_by_users_accepted                  :boolean          default(FALSE)
#  has_step_definition_by_users_validated                 :boolean          default(FALSE)
#  mailer_name                                            :string(255)
#  has_step_my_subs_results                               :boolean          default(FALSE)
#  public_pe_members_box                                  :boolean          default(TRUE)
#  public_pe_members_dimension_name                       :boolean          default(TRUE)
#  step_assessment_show_final_results_to_boss             :boolean          default(FALSE)
#  step_assessment_show_final_results_to_boss_bf          :boolean          default(FALSE)
#  step_validation_send_email_when_accept                 :boolean          default(FALSE)
#  step_validation_send_email_when_reject                 :boolean          default(FALSE)
#  step_feedback_accepted_show_rep_dfr                    :boolean
#  step_feedback_accepted_show_rep_dfr_text               :text
#  public_pe_box                                          :boolean          default(TRUE)
#  public_pe_dimension_name                               :boolean          default(TRUE)
#  manage_dates                                           :boolean          default(FALSE)
#  manage_definition                                      :boolean          default(FALSE)
#  manage_enter_by_manager                                :boolean          default(FALSE)
#  manage_evaluations                                     :boolean          default(FALSE)
#  manage_calibration                                     :boolean          default(FALSE)
#  manage_feedback                                        :boolean          default(FALSE)
#  rep_detailed_final_results_show_feedback_provider      :boolean
#  has_step_query_definitions                             :boolean          default(FALSE)
#  step_query_definitions_alias                           :string(255)      default("Consultar definición")
#  rep_detailed_final_results_show_rel_weight             :boolean          default(FALSE)
#  has_step_selection                                     :boolean          default(FALSE)
#  step_selection_alias                                   :string(255)      default("Seleccionar")
#  step_assessment_send_email_to_validator                :boolean          default(FALSE)
#  step_assessment_finish_message                         :string(255)      default("Confirmo que he finalizado la evaluación")
#  step_validation_ok_message                             :string(255)      default("Confirmo que valido la evaluación")
#  step_validation_no_message                             :string(255)      default("No estoy de acuerdo con la evaluación realizada")
#  step_feedback_accepted_alias                           :string(255)      default("Confirmar retroalimentación")
#  step_feedback_accepted_mail_to_boss                    :boolean
#  step_feedback_finish_message                           :string(255)      default("Confirmo que he finalizado la retroalimentación.")
#  step_feedback_accepted_finish_message                  :string(255)      default("Confirmo que he recibido la retroalimentación y la información proporcionada es correcta.")
#  rep_detailed_final_results_show_feedback_date          :boolean          default(FALSE)
#  rep_detailed_final_results_show_feedback_accepted_date :boolean          default(FALSE)
#  step_assessment_show_rep_dfr_to_boss_pdf               :boolean          default(FALSE)
#  step_feed_show_rep_dfr_to_boss                         :boolean          default(FALSE)
#  step_feed_show_rep_dfr_to_boss_pdf                     :boolean          default(FALSE)
#  step_feed_show_rep_dfr_to_boss_text                    :text
#  step_feedback_accepted_show_rep_dfr_pdf                :boolean          default(FALSE)
#  step_assessment_final_result_alias                     :string(255)      default("Valoración final")
#  has_step_calibration_evaluation                        :boolean
#  step_calibration_evaluation_alias                      :string(255)      default("Calibrar")
#  has_step_query_results                                 :boolean          default(FALSE)
#  step_query_results_alias                               :string(255)      default("Consultar resultados")
#  step_assessment_can_finish                             :boolean          default(TRUE)
#  step_assessment_summary_group                          :boolean          default(FALSE)
#  step_feedback_summary                                  :boolean          default(TRUE)
#  step_query_show_summary                                :boolean          default(FALSE)
#  step_query_definitions_accept                          :boolean          default(FALSE)
#  step_query_definitions_accept_mail                     :boolean
#  step_query_definitions_accept_mail_to_boss             :boolean
#  step_assessment_show_calculus                          :boolean          default(TRUE)
#  step_assessment_uniq                                   :boolean
#  step_feedback_uniq                                     :boolean
#  step_assessment_menu                                   :boolean          default(TRUE)
#  step_feedback_menu                                     :boolean          default(TRUE)
#  step_assess_uniq_show_temp_res                         :boolean
#  has_step_assign                                        :boolean          default(FALSE)
#  step_assign_alias                                      :string(255)      default("Asignar evaluadores")
#  step_assign_min                                        :integer          default(1)
#  step_assign_max                                        :integer          default(1)
#  step_validation_uniq                                   :boolean
#  step_validation_menu                                   :boolean          default(TRUE)
#  step_assessment_cols_evals                             :boolean          default(TRUE)
#  step_assessment_back_list                              :boolean          default(FALSE)
#  manage_calibration_e                                   :boolean
#  step_assessment_show_results                           :boolean          default(TRUE)
#

require 'spec_helper'

describe PeProcess do
  pending "add some examples to (or delete) #{__FILE__}"
end
