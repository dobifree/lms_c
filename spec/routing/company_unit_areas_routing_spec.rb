require "spec_helper"

describe CompanyUnitAreasController do
  describe "routing" do

    it "routes to #index" do
      get("/company_unit_areas").should route_to("company_unit_areas#index")
    end

    it "routes to #new" do
      get("/company_unit_areas/new").should route_to("company_unit_areas#new")
    end

    it "routes to #show" do
      get("/company_unit_areas/1").should route_to("company_unit_areas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/company_unit_areas/1/edit").should route_to("company_unit_areas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/company_unit_areas").should route_to("company_unit_areas#create")
    end

    it "routes to #update" do
      put("/company_unit_areas/1").should route_to("company_unit_areas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/company_unit_areas/1").should route_to("company_unit_areas#destroy", :id => "1")
    end

  end
end
