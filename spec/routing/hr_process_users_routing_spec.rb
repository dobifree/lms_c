require "spec_helper"

describe HrProcessUsersController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_users").should route_to("hr_process_users#index")
    end

    it "routes to #new" do
      get("/hr_process_users/new").should route_to("hr_process_users#new")
    end

    it "routes to #show" do
      get("/hr_process_users/1").should route_to("hr_process_users#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_users/1/edit").should route_to("hr_process_users#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_users").should route_to("hr_process_users#create")
    end

    it "routes to #update" do
      put("/hr_process_users/1").should route_to("hr_process_users#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_users/1").should route_to("hr_process_users#destroy", :id => "1")
    end

  end
end
