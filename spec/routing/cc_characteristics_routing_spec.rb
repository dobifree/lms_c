require "spec_helper"

describe CcCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/cc_characteristics").should route_to("cc_characteristics#index")
    end

    it "routes to #new" do
      get("/cc_characteristics/new").should route_to("cc_characteristics#new")
    end

    it "routes to #show" do
      get("/cc_characteristics/1").should route_to("cc_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/cc_characteristics/1/edit").should route_to("cc_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/cc_characteristics").should route_to("cc_characteristics#create")
    end

    it "routes to #update" do
      put("/cc_characteristics/1").should route_to("cc_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/cc_characteristics/1").should route_to("cc_characteristics#destroy", :id => "1")
    end

  end
end
