require "spec_helper"

describe KpiColorsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_colors").should route_to("kpi_colors#index")
    end

    it "routes to #new" do
      get("/kpi_colors/new").should route_to("kpi_colors#new")
    end

    it "routes to #show" do
      get("/kpi_colors/1").should route_to("kpi_colors#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_colors/1/edit").should route_to("kpi_colors#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_colors").should route_to("kpi_colors#create")
    end

    it "routes to #update" do
      put("/kpi_colors/1").should route_to("kpi_colors#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_colors/1").should route_to("kpi_colors#destroy", :id => "1")
    end

  end
end
