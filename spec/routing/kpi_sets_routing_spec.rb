require "spec_helper"

describe KpiSetsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_sets").should route_to("kpi_sets#index")
    end

    it "routes to #new" do
      get("/kpi_sets/new").should route_to("kpi_sets#new")
    end

    it "routes to #show" do
      get("/kpi_sets/1").should route_to("kpi_sets#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_sets/1/edit").should route_to("kpi_sets#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_sets").should route_to("kpi_sets#create")
    end

    it "routes to #update" do
      put("/kpi_sets/1").should route_to("kpi_sets#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_sets/1").should route_to("kpi_sets#destroy", :id => "1")
    end

  end
end
