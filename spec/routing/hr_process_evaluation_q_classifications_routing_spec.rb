require "spec_helper"

describe HrProcessEvaluationQClassificationsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_evaluation_q_classifications").should route_to("hr_process_evaluation_q_classifications#index")
    end

    it "routes to #new" do
      get("/hr_process_evaluation_q_classifications/new").should route_to("hr_process_evaluation_q_classifications#new")
    end

    it "routes to #show" do
      get("/hr_process_evaluation_q_classifications/1").should route_to("hr_process_evaluation_q_classifications#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_evaluation_q_classifications/1/edit").should route_to("hr_process_evaluation_q_classifications#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_evaluation_q_classifications").should route_to("hr_process_evaluation_q_classifications#create")
    end

    it "routes to #update" do
      put("/hr_process_evaluation_q_classifications/1").should route_to("hr_process_evaluation_q_classifications#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_evaluation_q_classifications/1").should route_to("hr_process_evaluation_q_classifications#destroy", :id => "1")
    end

  end
end
