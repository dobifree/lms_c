require "spec_helper"

describe HrProcessCalibrationSessionMsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_calibration_session_ms").should route_to("hr_process_calibration_session_ms#index")
    end

    it "routes to #new" do
      get("/hr_process_calibration_session_ms/new").should route_to("hr_process_calibration_session_ms#new")
    end

    it "routes to #show" do
      get("/hr_process_calibration_session_ms/1").should route_to("hr_process_calibration_session_ms#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_calibration_session_ms/1/edit").should route_to("hr_process_calibration_session_ms#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_calibration_session_ms").should route_to("hr_process_calibration_session_ms#create")
    end

    it "routes to #update" do
      put("/hr_process_calibration_session_ms/1").should route_to("hr_process_calibration_session_ms#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_calibration_session_ms/1").should route_to("hr_process_calibration_session_ms#destroy", :id => "1")
    end

  end
end
