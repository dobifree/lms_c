require "spec_helper"

describe PeAlternativesController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_alternatives").should route_to("pe_alternatives#index")
    end

    it "routes to #new" do
      get("/pe_alternatives/new").should route_to("pe_alternatives#new")
    end

    it "routes to #show" do
      get("/pe_alternatives/1").should route_to("pe_alternatives#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_alternatives/1/edit").should route_to("pe_alternatives#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_alternatives").should route_to("pe_alternatives#create")
    end

    it "routes to #update" do
      put("/pe_alternatives/1").should route_to("pe_alternatives#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_alternatives/1").should route_to("pe_alternatives#destroy", :id => "1")
    end

  end
end
