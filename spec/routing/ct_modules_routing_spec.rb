require "spec_helper"

describe CtModulesController do
  describe "routing" do

    it "routes to #index" do
      get("/ct_modules").should route_to("ct_modules#index")
    end

    it "routes to #new" do
      get("/ct_modules/new").should route_to("ct_modules#new")
    end

    it "routes to #show" do
      get("/ct_modules/1").should route_to("ct_modules#show", :id => "1")
    end

    it "routes to #edit" do
      get("/ct_modules/1/edit").should route_to("ct_modules#edit", :id => "1")
    end

    it "routes to #create" do
      post("/ct_modules").should route_to("ct_modules#create")
    end

    it "routes to #update" do
      put("/ct_modules/1").should route_to("ct_modules#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/ct_modules/1").should route_to("ct_modules#destroy", :id => "1")
    end

  end
end
