require "spec_helper"

describe PeElementsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_elements").should route_to("pe_elements#index")
    end

    it "routes to #new" do
      get("/pe_elements/new").should route_to("pe_elements#new")
    end

    it "routes to #show" do
      get("/pe_elements/1").should route_to("pe_elements#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_elements/1/edit").should route_to("pe_elements#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_elements").should route_to("pe_elements#create")
    end

    it "routes to #update" do
      put("/pe_elements/1").should route_to("pe_elements#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_elements/1").should route_to("pe_elements#destroy", :id => "1")
    end

  end
end
