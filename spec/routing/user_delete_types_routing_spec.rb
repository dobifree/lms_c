require "spec_helper"

describe UserDeleteTypesController do
  describe "routing" do

    it "routes to #index" do
      get("/user_delete_types").should route_to("user_delete_types#index")
    end

    it "routes to #new" do
      get("/user_delete_types/new").should route_to("user_delete_types#new")
    end

    it "routes to #show" do
      get("/user_delete_types/1").should route_to("user_delete_types#show", :id => "1")
    end

    it "routes to #edit" do
      get("/user_delete_types/1/edit").should route_to("user_delete_types#edit", :id => "1")
    end

    it "routes to #create" do
      post("/user_delete_types").should route_to("user_delete_types#create")
    end

    it "routes to #update" do
      put("/user_delete_types/1").should route_to("user_delete_types#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/user_delete_types/1").should route_to("user_delete_types#destroy", :id => "1")
    end

  end
end
