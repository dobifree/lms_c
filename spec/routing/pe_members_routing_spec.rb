require "spec_helper"

describe PeMembersController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_members").should route_to("pe_members#index")
    end

    it "routes to #new" do
      get("/pe_members/new").should route_to("pe_members#new")
    end

    it "routes to #show" do
      get("/pe_members/1").should route_to("pe_members#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_members/1/edit").should route_to("pe_members#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_members").should route_to("pe_members#create")
    end

    it "routes to #update" do
      put("/pe_members/1").should route_to("pe_members#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_members/1").should route_to("pe_members#destroy", :id => "1")
    end

  end
end
