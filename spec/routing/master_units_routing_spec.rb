require "spec_helper"

describe MasterUnitsController do
  describe "routing" do

    it "routes to #index" do
      get("/master_units").should route_to("master_units#index")
    end

    it "routes to #new" do
      get("/master_units/new").should route_to("master_units#new")
    end

    it "routes to #show" do
      get("/master_units/1").should route_to("master_units#show", :id => "1")
    end

    it "routes to #edit" do
      get("/master_units/1/edit").should route_to("master_units#edit", :id => "1")
    end

    it "routes to #create" do
      post("/master_units").should route_to("master_units#create")
    end

    it "routes to #update" do
      put("/master_units/1").should route_to("master_units#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/master_units/1").should route_to("master_units#destroy", :id => "1")
    end

  end
end
