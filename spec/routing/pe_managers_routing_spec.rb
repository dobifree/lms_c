require "spec_helper"

describe PeManagersController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_managers").should route_to("pe_managers#index")
    end

    it "routes to #new" do
      get("/pe_managers/new").should route_to("pe_managers#new")
    end

    it "routes to #show" do
      get("/pe_managers/1").should route_to("pe_managers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_managers/1/edit").should route_to("pe_managers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_managers").should route_to("pe_managers#create")
    end

    it "routes to #update" do
      put("/pe_managers/1").should route_to("pe_managers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_managers/1").should route_to("pe_managers#destroy", :id => "1")
    end

  end
end
