require "spec_helper"

describe PeQuestionActivityStatusesController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_question_activity_statuses").should route_to("pe_question_activity_statuses#index")
    end

    it "routes to #new" do
      get("/pe_question_activity_statuses/new").should route_to("pe_question_activity_statuses#new")
    end

    it "routes to #show" do
      get("/pe_question_activity_statuses/1").should route_to("pe_question_activity_statuses#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_question_activity_statuses/1/edit").should route_to("pe_question_activity_statuses#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_question_activity_statuses").should route_to("pe_question_activity_statuses#create")
    end

    it "routes to #update" do
      put("/pe_question_activity_statuses/1").should route_to("pe_question_activity_statuses#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_question_activity_statuses/1").should route_to("pe_question_activity_statuses#destroy", :id => "1")
    end

  end
end
