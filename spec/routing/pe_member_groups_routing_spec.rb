require "spec_helper"

describe PeMemberGroupsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_member_groups").should route_to("pe_member_groups#index")
    end

    it "routes to #new" do
      get("/pe_member_groups/new").should route_to("pe_member_groups#new")
    end

    it "routes to #show" do
      get("/pe_member_groups/1").should route_to("pe_member_groups#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_member_groups/1/edit").should route_to("pe_member_groups#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_member_groups").should route_to("pe_member_groups#create")
    end

    it "routes to #update" do
      put("/pe_member_groups/1").should route_to("pe_member_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_member_groups/1").should route_to("pe_member_groups#destroy", :id => "1")
    end

  end
end
