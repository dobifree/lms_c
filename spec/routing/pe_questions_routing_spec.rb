require "spec_helper"

describe PeQuestionsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_questions").should route_to("pe_questions#index")
    end

    it "routes to #new" do
      get("/pe_questions/new").should route_to("pe_questions#new")
    end

    it "routes to #show" do
      get("/pe_questions/1").should route_to("pe_questions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_questions/1/edit").should route_to("pe_questions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_questions").should route_to("pe_questions#create")
    end

    it "routes to #update" do
      put("/pe_questions/1").should route_to("pe_questions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_questions/1").should route_to("pe_questions#destroy", :id => "1")
    end

  end
end
