require "spec_helper"

describe SecuCohadesController do
  describe "routing" do

    it "routes to #index" do
      get("/secu_cohades").should route_to("secu_cohades#index")
    end

    it "routes to #new" do
      get("/secu_cohades/new").should route_to("secu_cohades#new")
    end

    it "routes to #show" do
      get("/secu_cohades/1").should route_to("secu_cohades#show", :id => "1")
    end

    it "routes to #edit" do
      get("/secu_cohades/1/edit").should route_to("secu_cohades#edit", :id => "1")
    end

    it "routes to #create" do
      post("/secu_cohades").should route_to("secu_cohades#create")
    end

    it "routes to #update" do
      put("/secu_cohades/1").should route_to("secu_cohades#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/secu_cohades/1").should route_to("secu_cohades#destroy", :id => "1")
    end

  end
end
