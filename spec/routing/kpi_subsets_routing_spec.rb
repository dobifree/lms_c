require "spec_helper"

describe KpiSubsetsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_subsets").should route_to("kpi_subsets#index")
    end

    it "routes to #new" do
      get("/kpi_subsets/new").should route_to("kpi_subsets#new")
    end

    it "routes to #show" do
      get("/kpi_subsets/1").should route_to("kpi_subsets#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_subsets/1/edit").should route_to("kpi_subsets#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_subsets").should route_to("kpi_subsets#create")
    end

    it "routes to #update" do
      put("/kpi_subsets/1").should route_to("kpi_subsets#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_subsets/1").should route_to("kpi_subsets#destroy", :id => "1")
    end

  end
end
