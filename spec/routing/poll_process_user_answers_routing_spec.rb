require "spec_helper"

describe PollProcessUserAnswersController do
  describe "routing" do

    it "routes to #index" do
      get("/poll_process_user_answers").should route_to("poll_process_user_answers#index")
    end

    it "routes to #new" do
      get("/poll_process_user_answers/new").should route_to("poll_process_user_answers#new")
    end

    it "routes to #show" do
      get("/poll_process_user_answers/1").should route_to("poll_process_user_answers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/poll_process_user_answers/1/edit").should route_to("poll_process_user_answers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/poll_process_user_answers").should route_to("poll_process_user_answers#create")
    end

    it "routes to #update" do
      put("/poll_process_user_answers/1").should route_to("poll_process_user_answers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/poll_process_user_answers/1").should route_to("poll_process_user_answers#destroy", :id => "1")
    end

  end
end
