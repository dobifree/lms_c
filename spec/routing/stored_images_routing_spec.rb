require "spec_helper"

describe StoredImagesController do
  describe "routing" do

    it "routes to #index" do
      get("/stored_images").should route_to("stored_images#index")
    end

    it "routes to #new" do
      get("/stored_images/new").should route_to("stored_images#new")
    end

    it "routes to #show" do
      get("/stored_images/1").should route_to("stored_images#show", :id => "1")
    end

    it "routes to #edit" do
      get("/stored_images/1/edit").should route_to("stored_images#edit", :id => "1")
    end

    it "routes to #create" do
      post("/stored_images").should route_to("stored_images#create")
    end

    it "routes to #update" do
      put("/stored_images/1").should route_to("stored_images#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/stored_images/1").should route_to("stored_images#destroy", :id => "1")
    end

  end
end
