require "spec_helper"

describe JPayBandsController do
  describe "routing" do

    it "routes to #index" do
      get("/j_pay_bands").should route_to("j_pay_bands#index")
    end

    it "routes to #new" do
      get("/j_pay_bands/new").should route_to("j_pay_bands#new")
    end

    it "routes to #show" do
      get("/j_pay_bands/1").should route_to("j_pay_bands#show", :id => "1")
    end

    it "routes to #edit" do
      get("/j_pay_bands/1/edit").should route_to("j_pay_bands#edit", :id => "1")
    end

    it "routes to #create" do
      post("/j_pay_bands").should route_to("j_pay_bands#create")
    end

    it "routes to #update" do
      put("/j_pay_bands/1").should route_to("j_pay_bands#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/j_pay_bands/1").should route_to("j_pay_bands#destroy", :id => "1")
    end

  end
end
