require "spec_helper"

describe PeProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_processes").should route_to("pe_processes#index")
    end

    it "routes to #new" do
      get("/pe_processes/new").should route_to("pe_processes#new")
    end

    it "routes to #show" do
      get("/pe_processes/1").should route_to("pe_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_processes/1/edit").should route_to("pe_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_processes").should route_to("pe_processes#create")
    end

    it "routes to #update" do
      put("/pe_processes/1").should route_to("pe_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_processes/1").should route_to("pe_processes#destroy", :id => "1")
    end

  end
end
