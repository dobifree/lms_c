require "spec_helper"

describe HrProcessCalibrationSessionCsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_calibration_session_cs").should route_to("hr_process_calibration_session_cs#index")
    end

    it "routes to #new" do
      get("/hr_process_calibration_session_cs/new").should route_to("hr_process_calibration_session_cs#new")
    end

    it "routes to #show" do
      get("/hr_process_calibration_session_cs/1").should route_to("hr_process_calibration_session_cs#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_calibration_session_cs/1/edit").should route_to("hr_process_calibration_session_cs#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_calibration_session_cs").should route_to("hr_process_calibration_session_cs#create")
    end

    it "routes to #update" do
      put("/hr_process_calibration_session_cs/1").should route_to("hr_process_calibration_session_cs#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_calibration_session_cs/1").should route_to("hr_process_calibration_session_cs#destroy", :id => "1")
    end

  end
end
