require "spec_helper"

describe HrProcessCalibrationSessionsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_calibration_sessions").should route_to("hr_process_calibration_sessions#index")
    end

    it "routes to #new" do
      get("/hr_process_calibration_sessions/new").should route_to("hr_process_calibration_sessions#new")
    end

    it "routes to #show" do
      get("/hr_process_calibration_sessions/1").should route_to("hr_process_calibration_sessions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_calibration_sessions/1/edit").should route_to("hr_process_calibration_sessions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_calibration_sessions").should route_to("hr_process_calibration_sessions#create")
    end

    it "routes to #update" do
      put("/hr_process_calibration_sessions/1").should route_to("hr_process_calibration_sessions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_calibration_sessions/1").should route_to("hr_process_calibration_sessions#destroy", :id => "1")
    end

  end
end
