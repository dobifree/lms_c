require "spec_helper"

describe HrProcessDimensionEsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_dimension_es").should route_to("hr_process_dimension_es#index")
    end

    it "routes to #new" do
      get("/hr_process_dimension_es/new").should route_to("hr_process_dimension_es#new")
    end

    it "routes to #show" do
      get("/hr_process_dimension_es/1").should route_to("hr_process_dimension_es#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_dimension_es/1/edit").should route_to("hr_process_dimension_es#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_dimension_es").should route_to("hr_process_dimension_es#create")
    end

    it "routes to #update" do
      put("/hr_process_dimension_es/1").should route_to("hr_process_dimension_es#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_dimension_es/1").should route_to("hr_process_dimension_es#destroy", :id => "1")
    end

  end
end
