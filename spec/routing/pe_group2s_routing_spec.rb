require "spec_helper"

describe PeGroup2sController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_group2s").should route_to("pe_group2s#index")
    end

    it "routes to #new" do
      get("/pe_group2s/new").should route_to("pe_group2s#new")
    end

    it "routes to #show" do
      get("/pe_group2s/1").should route_to("pe_group2s#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_group2s/1/edit").should route_to("pe_group2s#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_group2s").should route_to("pe_group2s#create")
    end

    it "routes to #update" do
      put("/pe_group2s/1").should route_to("pe_group2s#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_group2s/1").should route_to("pe_group2s#destroy", :id => "1")
    end

  end
end
