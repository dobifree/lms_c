require "spec_helper"

describe SelApplyFormsController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_apply_forms").should route_to("sel_apply_forms#index")
    end

    it "routes to #new" do
      get("/sel_apply_forms/new").should route_to("sel_apply_forms#new")
    end

    it "routes to #show" do
      get("/sel_apply_forms/1").should route_to("sel_apply_forms#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_apply_forms/1/edit").should route_to("sel_apply_forms#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_apply_forms").should route_to("sel_apply_forms#create")
    end

    it "routes to #update" do
      put("/sel_apply_forms/1").should route_to("sel_apply_forms#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_apply_forms/1").should route_to("sel_apply_forms#destroy", :id => "1")
    end

  end
end
