require "spec_helper"

describe CourseCertificatesController do
  describe "routing" do

    it "routes to #index" do
      get("/course_certificates").should route_to("course_certificates#index")
    end

    it "routes to #new" do
      get("/course_certificates/new").should route_to("course_certificates#new")
    end

    it "routes to #show" do
      get("/course_certificates/1").should route_to("course_certificates#show", :id => "1")
    end

    it "routes to #edit" do
      get("/course_certificates/1/edit").should route_to("course_certificates#edit", :id => "1")
    end

    it "routes to #create" do
      post("/course_certificates").should route_to("course_certificates#create")
    end

    it "routes to #update" do
      put("/course_certificates/1").should route_to("course_certificates#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/course_certificates/1").should route_to("course_certificates#destroy", :id => "1")
    end

  end
end
