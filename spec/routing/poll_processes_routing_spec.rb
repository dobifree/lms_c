require "spec_helper"

describe PollProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/poll_processes").should route_to("poll_processes#index")
    end

    it "routes to #new" do
      get("/poll_processes/new").should route_to("poll_processes#new")
    end

    it "routes to #show" do
      get("/poll_processes/1").should route_to("poll_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/poll_processes/1/edit").should route_to("poll_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/poll_processes").should route_to("poll_processes#create")
    end

    it "routes to #update" do
      put("/poll_processes/1").should route_to("poll_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/poll_processes/1").should route_to("poll_processes#destroy", :id => "1")
    end

  end
end
