require "spec_helper"

describe CtMenusController do
  describe "routing" do

    it "routes to #index" do
      get("/ct_menus").should route_to("ct_menus#index")
    end

    it "routes to #new" do
      get("/ct_menus/new").should route_to("ct_menus#new")
    end

    it "routes to #show" do
      get("/ct_menus/1").should route_to("ct_menus#show", :id => "1")
    end

    it "routes to #edit" do
      get("/ct_menus/1/edit").should route_to("ct_menus#edit", :id => "1")
    end

    it "routes to #create" do
      post("/ct_menus").should route_to("ct_menus#create")
    end

    it "routes to #update" do
      put("/ct_menus/1").should route_to("ct_menus#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/ct_menus/1").should route_to("ct_menus#destroy", :id => "1")
    end

  end
end
