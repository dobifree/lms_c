require "spec_helper"

describe SelStepsController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_steps").should route_to("sel_steps#index")
    end

    it "routes to #new" do
      get("/sel_steps/new").should route_to("sel_steps#new")
    end

    it "routes to #show" do
      get("/sel_steps/1").should route_to("sel_steps#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_steps/1/edit").should route_to("sel_steps#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_steps").should route_to("sel_steps#create")
    end

    it "routes to #update" do
      put("/sel_steps/1").should route_to("sel_steps#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_steps/1").should route_to("sel_steps#destroy", :id => "1")
    end

  end
end
