require "spec_helper"

describe JccCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/jcc_characteristics").should route_to("jcc_characteristics#index")
    end

    it "routes to #new" do
      get("/jcc_characteristics/new").should route_to("jcc_characteristics#new")
    end

    it "routes to #show" do
      get("/jcc_characteristics/1").should route_to("jcc_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/jcc_characteristics/1/edit").should route_to("jcc_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/jcc_characteristics").should route_to("jcc_characteristics#create")
    end

    it "routes to #update" do
      put("/jcc_characteristics/1").should route_to("jcc_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/jcc_characteristics/1").should route_to("jcc_characteristics#destroy", :id => "1")
    end

  end
end
