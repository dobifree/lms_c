require "spec_helper"

describe ElecProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/elec_processes").should route_to("elec_processes#index")
    end

    it "routes to #new" do
      get("/elec_processes/new").should route_to("elec_processes#new")
    end

    it "routes to #show" do
      get("/elec_processes/1").should route_to("elec_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/elec_processes/1/edit").should route_to("elec_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/elec_processes").should route_to("elec_processes#create")
    end

    it "routes to #update" do
      put("/elec_processes/1").should route_to("elec_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/elec_processes/1").should route_to("elec_processes#destroy", :id => "1")
    end

  end
end
