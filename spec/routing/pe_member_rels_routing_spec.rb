require "spec_helper"

describe PeMemberRelsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_member_rels").should route_to("pe_member_rels#index")
    end

    it "routes to #new" do
      get("/pe_member_rels/new").should route_to("pe_member_rels#new")
    end

    it "routes to #show" do
      get("/pe_member_rels/1").should route_to("pe_member_rels#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_member_rels/1/edit").should route_to("pe_member_rels#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_member_rels").should route_to("pe_member_rels#create")
    end

    it "routes to #update" do
      put("/pe_member_rels/1").should route_to("pe_member_rels#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_member_rels/1").should route_to("pe_member_rels#destroy", :id => "1")
    end

  end
end
