require "spec_helper"

describe InformativeDocumentsController do
  describe "routing" do

    it "routes to #index" do
      get("/informative_documents").should route_to("informative_documents#index")
    end

    it "routes to #new" do
      get("/informative_documents/new").should route_to("informative_documents#new")
    end

    it "routes to #show" do
      get("/informative_documents/1").should route_to("informative_documents#show", :id => "1")
    end

    it "routes to #edit" do
      get("/informative_documents/1/edit").should route_to("informative_documents#edit", :id => "1")
    end

    it "routes to #create" do
      post("/informative_documents").should route_to("informative_documents#create")
    end

    it "routes to #update" do
      put("/informative_documents/1").should route_to("informative_documents#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/informative_documents/1").should route_to("informative_documents#destroy", :id => "1")
    end

  end
end
