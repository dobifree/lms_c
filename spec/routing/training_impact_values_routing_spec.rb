require "spec_helper"

describe TrainingImpactValuesController do
  describe "routing" do

    it "routes to #index" do
      get("/training_impact_values").should route_to("training_impact_values#index")
    end

    it "routes to #new" do
      get("/training_impact_values/new").should route_to("training_impact_values#new")
    end

    it "routes to #show" do
      get("/training_impact_values/1").should route_to("training_impact_values#show", :id => "1")
    end

    it "routes to #edit" do
      get("/training_impact_values/1/edit").should route_to("training_impact_values#edit", :id => "1")
    end

    it "routes to #create" do
      post("/training_impact_values").should route_to("training_impact_values#create")
    end

    it "routes to #update" do
      put("/training_impact_values/1").should route_to("training_impact_values#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/training_impact_values/1").should route_to("training_impact_values#destroy", :id => "1")
    end

  end
end
