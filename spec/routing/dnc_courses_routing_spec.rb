require "spec_helper"

describe DncCoursesController do
  describe "routing" do

    it "routes to #index" do
      get("/dnc_courses").should route_to("dnc_courses#index")
    end

    it "routes to #new" do
      get("/dnc_courses/new").should route_to("dnc_courses#new")
    end

    it "routes to #show" do
      get("/dnc_courses/1").should route_to("dnc_courses#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dnc_courses/1/edit").should route_to("dnc_courses#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dnc_courses").should route_to("dnc_courses#create")
    end

    it "routes to #update" do
      put("/dnc_courses/1").should route_to("dnc_courses#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dnc_courses/1").should route_to("dnc_courses#destroy", :id => "1")
    end

  end
end
