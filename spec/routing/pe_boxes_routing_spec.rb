require "spec_helper"

describe PeBoxesController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_boxes").should route_to("pe_boxes#index")
    end

    it "routes to #new" do
      get("/pe_boxes/new").should route_to("pe_boxes#new")
    end

    it "routes to #show" do
      get("/pe_boxes/1").should route_to("pe_boxes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_boxes/1/edit").should route_to("pe_boxes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_boxes").should route_to("pe_boxes#create")
    end

    it "routes to #update" do
      put("/pe_boxes/1").should route_to("pe_boxes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_boxes/1").should route_to("pe_boxes#destroy", :id => "1")
    end

  end
end
