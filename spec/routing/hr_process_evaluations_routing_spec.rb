require "spec_helper"

describe HrProcessEvaluationsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_evaluations").should route_to("hr_process_evaluations#index")
    end

    it "routes to #new" do
      get("/hr_process_evaluations/new").should route_to("hr_process_evaluations#new")
    end

    it "routes to #show" do
      get("/hr_process_evaluations/1").should route_to("hr_process_evaluations#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_evaluations/1/edit").should route_to("hr_process_evaluations#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_evaluations").should route_to("hr_process_evaluations#create")
    end

    it "routes to #update" do
      put("/hr_process_evaluations/1").should route_to("hr_process_evaluations#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_evaluations/1").should route_to("hr_process_evaluations#destroy", :id => "1")
    end

  end
end
