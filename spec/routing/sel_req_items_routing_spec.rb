require "spec_helper"

describe SelReqItemsController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_req_items").should route_to("sel_req_items#index")
    end

    it "routes to #new" do
      get("/sel_req_items/new").should route_to("sel_req_items#new")
    end

    it "routes to #show" do
      get("/sel_req_items/1").should route_to("sel_req_items#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_req_items/1/edit").should route_to("sel_req_items#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_req_items").should route_to("sel_req_items#create")
    end

    it "routes to #update" do
      put("/sel_req_items/1").should route_to("sel_req_items#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_req_items/1").should route_to("sel_req_items#destroy", :id => "1")
    end

  end
end
