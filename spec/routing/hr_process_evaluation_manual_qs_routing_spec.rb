require "spec_helper"

describe HrProcessEvaluationManualQsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_evaluation_manual_qs").should route_to("hr_process_evaluation_manual_qs#index")
    end

    it "routes to #new" do
      get("/hr_process_evaluation_manual_qs/new").should route_to("hr_process_evaluation_manual_qs#new")
    end

    it "routes to #show" do
      get("/hr_process_evaluation_manual_qs/1").should route_to("hr_process_evaluation_manual_qs#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_evaluation_manual_qs/1/edit").should route_to("hr_process_evaluation_manual_qs#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_evaluation_manual_qs").should route_to("hr_process_evaluation_manual_qs#create")
    end

    it "routes to #update" do
      put("/hr_process_evaluation_manual_qs/1").should route_to("hr_process_evaluation_manual_qs#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_evaluation_manual_qs/1").should route_to("hr_process_evaluation_manual_qs#destroy", :id => "1")
    end

  end
end
