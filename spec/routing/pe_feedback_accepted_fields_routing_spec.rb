require "spec_helper"

describe PeFeedbackAcceptedFieldsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_feedback_accepted_fields").should route_to("pe_feedback_accepted_fields#index")
    end

    it "routes to #new" do
      get("/pe_feedback_accepted_fields/new").should route_to("pe_feedback_accepted_fields#new")
    end

    it "routes to #show" do
      get("/pe_feedback_accepted_fields/1").should route_to("pe_feedback_accepted_fields#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_feedback_accepted_fields/1/edit").should route_to("pe_feedback_accepted_fields#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_feedback_accepted_fields").should route_to("pe_feedback_accepted_fields#create")
    end

    it "routes to #update" do
      put("/pe_feedback_accepted_fields/1").should route_to("pe_feedback_accepted_fields#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_feedback_accepted_fields/1").should route_to("pe_feedback_accepted_fields#destroy", :id => "1")
    end

  end
end
