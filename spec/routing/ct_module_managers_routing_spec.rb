require "spec_helper"

describe CtModuleManagersController do
  describe "routing" do

    it "routes to #index" do
      get("/ct_module_managers").should route_to("ct_module_managers#index")
    end

    it "routes to #new" do
      get("/ct_module_managers/new").should route_to("ct_module_managers#new")
    end

    it "routes to #show" do
      get("/ct_module_managers/1").should route_to("ct_module_managers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/ct_module_managers/1/edit").should route_to("ct_module_managers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/ct_module_managers").should route_to("ct_module_managers#create")
    end

    it "routes to #update" do
      put("/ct_module_managers/1").should route_to("ct_module_managers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/ct_module_managers/1").should route_to("ct_module_managers#destroy", :id => "1")
    end

  end
end
