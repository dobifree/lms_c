require "spec_helper"

describe HrProcessManagersController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_managers").should route_to("hr_process_managers#index")
    end

    it "routes to #new" do
      get("/hr_process_managers/new").should route_to("hr_process_managers#new")
    end

    it "routes to #show" do
      get("/hr_process_managers/1").should route_to("hr_process_managers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_managers/1/edit").should route_to("hr_process_managers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_managers").should route_to("hr_process_managers#create")
    end

    it "routes to #update" do
      put("/hr_process_managers/1").should route_to("hr_process_managers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_managers/1").should route_to("hr_process_managers#destroy", :id => "1")
    end

  end
end
