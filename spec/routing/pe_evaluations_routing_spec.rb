require "spec_helper"

describe PeEvaluationsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_evaluations").should route_to("pe_evaluations#index")
    end

    it "routes to #new" do
      get("/pe_evaluations/new").should route_to("pe_evaluations#new")
    end

    it "routes to #show" do
      get("/pe_evaluations/1").should route_to("pe_evaluations#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_evaluations/1/edit").should route_to("pe_evaluations#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_evaluations").should route_to("pe_evaluations#create")
    end

    it "routes to #update" do
      put("/pe_evaluations/1").should route_to("pe_evaluations#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_evaluations/1").should route_to("pe_evaluations#destroy", :id => "1")
    end

  end
end
