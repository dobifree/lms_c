require "spec_helper"

describe ElecEventsController do
  describe "routing" do

    it "routes to #index" do
      get("/elec_events").should route_to("elec_events#index")
    end

    it "routes to #new" do
      get("/elec_events/new").should route_to("elec_events#new")
    end

    it "routes to #show" do
      get("/elec_events/1").should route_to("elec_events#show", :id => "1")
    end

    it "routes to #edit" do
      get("/elec_events/1/edit").should route_to("elec_events#edit", :id => "1")
    end

    it "routes to #create" do
      post("/elec_events").should route_to("elec_events#create")
    end

    it "routes to #update" do
      put("/elec_events/1").should route_to("elec_events#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/elec_events/1").should route_to("elec_events#destroy", :id => "1")
    end

  end
end
