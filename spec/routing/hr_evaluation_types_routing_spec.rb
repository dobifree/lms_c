require "spec_helper"

describe HrEvaluationTypesController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_evaluation_types").should route_to("hr_evaluation_types#index")
    end

    it "routes to #new" do
      get("/hr_evaluation_types/new").should route_to("hr_evaluation_types#new")
    end

    it "routes to #show" do
      get("/hr_evaluation_types/1").should route_to("hr_evaluation_types#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_evaluation_types/1/edit").should route_to("hr_evaluation_types#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_evaluation_types").should route_to("hr_evaluation_types#create")
    end

    it "routes to #update" do
      put("/hr_evaluation_types/1").should route_to("hr_evaluation_types#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_evaluation_types/1").should route_to("hr_evaluation_types#destroy", :id => "1")
    end

  end
end
