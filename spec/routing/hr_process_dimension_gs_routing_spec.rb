require "spec_helper"

describe HrProcessDimensionGsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_dimension_gs").should route_to("hr_process_dimension_gs#index")
    end

    it "routes to #new" do
      get("/hr_process_dimension_gs/new").should route_to("hr_process_dimension_gs#new")
    end

    it "routes to #show" do
      get("/hr_process_dimension_gs/1").should route_to("hr_process_dimension_gs#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_dimension_gs/1/edit").should route_to("hr_process_dimension_gs#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_dimension_gs").should route_to("hr_process_dimension_gs#create")
    end

    it "routes to #update" do
      put("/hr_process_dimension_gs/1").should route_to("hr_process_dimension_gs#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_dimension_gs/1").should route_to("hr_process_dimension_gs#destroy", :id => "1")
    end

  end
end
