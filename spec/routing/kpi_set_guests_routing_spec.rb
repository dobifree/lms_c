require "spec_helper"

describe KpiSetGuestsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_set_guests").should route_to("kpi_set_guests#index")
    end

    it "routes to #new" do
      get("/kpi_set_guests/new").should route_to("kpi_set_guests#new")
    end

    it "routes to #show" do
      get("/kpi_set_guests/1").should route_to("kpi_set_guests#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_set_guests/1/edit").should route_to("kpi_set_guests#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_set_guests").should route_to("kpi_set_guests#create")
    end

    it "routes to #update" do
      put("/kpi_set_guests/1").should route_to("kpi_set_guests#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_set_guests/1").should route_to("kpi_set_guests#destroy", :id => "1")
    end

  end
end
