require "spec_helper"

describe DncProvidersController do
  describe "routing" do

    it "routes to #index" do
      get("/dnc_providers").should route_to("dnc_providers#index")
    end

    it "routes to #new" do
      get("/dnc_providers/new").should route_to("dnc_providers#new")
    end

    it "routes to #show" do
      get("/dnc_providers/1").should route_to("dnc_providers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dnc_providers/1/edit").should route_to("dnc_providers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dnc_providers").should route_to("dnc_providers#create")
    end

    it "routes to #update" do
      put("/dnc_providers/1").should route_to("dnc_providers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dnc_providers/1").should route_to("dnc_providers#destroy", :id => "1")
    end

  end
end
