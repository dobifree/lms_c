require "spec_helper"

describe ElecProcessRoundsController do
  describe "routing" do

    it "routes to #index" do
      get("/elec_process_rounds").should route_to("elec_process_rounds#index")
    end

    it "routes to #new" do
      get("/elec_process_rounds/new").should route_to("elec_process_rounds#new")
    end

    it "routes to #show" do
      get("/elec_process_rounds/1").should route_to("elec_process_rounds#show", :id => "1")
    end

    it "routes to #edit" do
      get("/elec_process_rounds/1/edit").should route_to("elec_process_rounds#edit", :id => "1")
    end

    it "routes to #create" do
      post("/elec_process_rounds").should route_to("elec_process_rounds#create")
    end

    it "routes to #update" do
      put("/elec_process_rounds/1").should route_to("elec_process_rounds#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/elec_process_rounds/1").should route_to("elec_process_rounds#destroy", :id => "1")
    end

  end
end
