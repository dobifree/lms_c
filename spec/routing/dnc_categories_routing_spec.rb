require "spec_helper"

describe DncCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/dnc_categories").should route_to("dnc_categories#index")
    end

    it "routes to #new" do
      get("/dnc_categories/new").should route_to("dnc_categories#new")
    end

    it "routes to #show" do
      get("/dnc_categories/1").should route_to("dnc_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dnc_categories/1/edit").should route_to("dnc_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dnc_categories").should route_to("dnc_categories#create")
    end

    it "routes to #update" do
      put("/dnc_categories/1").should route_to("dnc_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dnc_categories/1").should route_to("dnc_categories#destroy", :id => "1")
    end

  end
end
