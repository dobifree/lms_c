require "spec_helper"

describe LiqProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/liq_processes").should route_to("liq_processes#index")
    end

    it "routes to #new" do
      get("/liq_processes/new").should route_to("liq_processes#new")
    end

    it "routes to #show" do
      get("/liq_processes/1").should route_to("liq_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/liq_processes/1/edit").should route_to("liq_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/liq_processes").should route_to("liq_processes#create")
    end

    it "routes to #update" do
      put("/liq_processes/1").should route_to("liq_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/liq_processes/1").should route_to("liq_processes#destroy", :id => "1")
    end

  end
end
