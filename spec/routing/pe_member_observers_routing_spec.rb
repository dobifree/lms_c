require "spec_helper"

describe PeMemberObserversController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_member_observers").should route_to("pe_member_observers#index")
    end

    it "routes to #new" do
      get("/pe_member_observers/new").should route_to("pe_member_observers#new")
    end

    it "routes to #show" do
      get("/pe_member_observers/1").should route_to("pe_member_observers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_member_observers/1/edit").should route_to("pe_member_observers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_member_observers").should route_to("pe_member_observers#create")
    end

    it "routes to #update" do
      put("/pe_member_observers/1").should route_to("pe_member_observers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_member_observers/1").should route_to("pe_member_observers#destroy", :id => "1")
    end

  end
end
