require "spec_helper"

describe PeElementDescriptionsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_element_descriptions").should route_to("pe_element_descriptions#index")
    end

    it "routes to #new" do
      get("/pe_element_descriptions/new").should route_to("pe_element_descriptions#new")
    end

    it "routes to #show" do
      get("/pe_element_descriptions/1").should route_to("pe_element_descriptions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_element_descriptions/1/edit").should route_to("pe_element_descriptions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_element_descriptions").should route_to("pe_element_descriptions#create")
    end

    it "routes to #update" do
      put("/pe_element_descriptions/1").should route_to("pe_element_descriptions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_element_descriptions/1").should route_to("pe_element_descriptions#destroy", :id => "1")
    end

  end
end
