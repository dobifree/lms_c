require "spec_helper"

describe PeCalSessionsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_cal_sessions").should route_to("pe_cal_sessions#index")
    end

    it "routes to #new" do
      get("/pe_cal_sessions/new").should route_to("pe_cal_sessions#new")
    end

    it "routes to #show" do
      get("/pe_cal_sessions/1").should route_to("pe_cal_sessions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_cal_sessions/1/edit").should route_to("pe_cal_sessions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_cal_sessions").should route_to("pe_cal_sessions#create")
    end

    it "routes to #update" do
      put("/pe_cal_sessions/1").should route_to("pe_cal_sessions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_cal_sessions/1").should route_to("pe_cal_sessions#destroy", :id => "1")
    end

  end
end
