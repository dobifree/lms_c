require "spec_helper"

describe SelProcessTrackersController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_process_trackers").should route_to("sel_process_trackers#index")
    end

    it "routes to #new" do
      get("/sel_process_trackers/new").should route_to("sel_process_trackers#new")
    end

    it "routes to #show" do
      get("/sel_process_trackers/1").should route_to("sel_process_trackers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_process_trackers/1/edit").should route_to("sel_process_trackers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_process_trackers").should route_to("sel_process_trackers#create")
    end

    it "routes to #update" do
      put("/sel_process_trackers/1").should route_to("sel_process_trackers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_process_trackers/1").should route_to("sel_process_trackers#destroy", :id => "1")
    end

  end
end
