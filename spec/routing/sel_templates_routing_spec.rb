require "spec_helper"

describe SelTemplatesController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_templates").should route_to("sel_templates#index")
    end

    it "routes to #new" do
      get("/sel_templates/new").should route_to("sel_templates#new")
    end

    it "routes to #show" do
      get("/sel_templates/1").should route_to("sel_templates#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_templates/1/edit").should route_to("sel_templates#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_templates").should route_to("sel_templates#create")
    end

    it "routes to #update" do
      put("/sel_templates/1").should route_to("sel_templates#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_templates/1").should route_to("sel_templates#destroy", :id => "1")
    end

  end
end
