require "spec_helper"

describe UserCoursesController do
  describe "routing" do

    it "routes to #index" do
      get("/user_courses").should route_to("user_courses#index")
    end

    it "routes to #new" do
      get("/user_courses/new").should route_to("user_courses#new")
    end

    it "routes to #show" do
      get("/user_courses/1").should route_to("user_courses#show", :id => "1")
    end

    it "routes to #edit" do
      get("/user_courses/1/edit").should route_to("user_courses#edit", :id => "1")
    end

    it "routes to #create" do
      post("/user_courses").should route_to("user_courses#create")
    end

    it "routes to #update" do
      put("/user_courses/1").should route_to("user_courses#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/user_courses/1").should route_to("user_courses#destroy", :id => "1")
    end

  end
end
