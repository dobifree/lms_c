require "spec_helper"

describe PeQuestionActivityFieldsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_question_activity_fields").should route_to("pe_question_activity_fields#index")
    end

    it "routes to #new" do
      get("/pe_question_activity_fields/new").should route_to("pe_question_activity_fields#new")
    end

    it "routes to #show" do
      get("/pe_question_activity_fields/1").should route_to("pe_question_activity_fields#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_question_activity_fields/1/edit").should route_to("pe_question_activity_fields#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_question_activity_fields").should route_to("pe_question_activity_fields#create")
    end

    it "routes to #update" do
      put("/pe_question_activity_fields/1").should route_to("pe_question_activity_fields#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_question_activity_fields/1").should route_to("pe_question_activity_fields#destroy", :id => "1")
    end

  end
end
