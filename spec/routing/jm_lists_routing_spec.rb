require "spec_helper"

describe JmListsController do
  describe "routing" do

    it "routes to #index" do
      get("/jm_lists").should route_to("jm_lists#index")
    end

    it "routes to #new" do
      get("/jm_lists/new").should route_to("jm_lists#new")
    end

    it "routes to #show" do
      get("/jm_lists/1").should route_to("jm_lists#show", :id => "1")
    end

    it "routes to #edit" do
      get("/jm_lists/1/edit").should route_to("jm_lists#edit", :id => "1")
    end

    it "routes to #create" do
      post("/jm_lists").should route_to("jm_lists#create")
    end

    it "routes to #update" do
      put("/jm_lists/1").should route_to("jm_lists#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/jm_lists/1").should route_to("jm_lists#destroy", :id => "1")
    end

  end
end
