require "spec_helper"

describe TrainingModesController do
  describe "routing" do

    it "routes to #index" do
      get("/training_modes").should route_to("training_modes#index")
    end

    it "routes to #new" do
      get("/training_modes/new").should route_to("training_modes#new")
    end

    it "routes to #show" do
      get("/training_modes/1").should route_to("training_modes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/training_modes/1/edit").should route_to("training_modes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/training_modes").should route_to("training_modes#create")
    end

    it "routes to #update" do
      put("/training_modes/1").should route_to("training_modes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/training_modes/1").should route_to("training_modes#destroy", :id => "1")
    end

  end
end
