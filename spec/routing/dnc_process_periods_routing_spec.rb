require "spec_helper"

describe DncProcessPeriodsController do
  describe "routing" do

    it "routes to #index" do
      get("/dnc_process_periods").should route_to("dnc_process_periods#index")
    end

    it "routes to #new" do
      get("/dnc_process_periods/new").should route_to("dnc_process_periods#new")
    end

    it "routes to #show" do
      get("/dnc_process_periods/1").should route_to("dnc_process_periods#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dnc_process_periods/1/edit").should route_to("dnc_process_periods#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dnc_process_periods").should route_to("dnc_process_periods#create")
    end

    it "routes to #update" do
      put("/dnc_process_periods/1").should route_to("dnc_process_periods#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dnc_process_periods/1").should route_to("dnc_process_periods#destroy", :id => "1")
    end

  end
end
