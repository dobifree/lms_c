require "spec_helper"

describe KpiRolsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_rols").should route_to("kpi_rols#index")
    end

    it "routes to #new" do
      get("/kpi_rols/new").should route_to("kpi_rols#new")
    end

    it "routes to #show" do
      get("/kpi_rols/1").should route_to("kpi_rols#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_rols/1/edit").should route_to("kpi_rols#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_rols").should route_to("kpi_rols#create")
    end

    it "routes to #update" do
      put("/kpi_rols/1").should route_to("kpi_rols#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_rols/1").should route_to("kpi_rols#destroy", :id => "1")
    end

  end
end
