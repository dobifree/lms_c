require "spec_helper"

describe SelRequirementsController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_requirements").should route_to("sel_requirements#index")
    end

    it "routes to #new" do
      get("/sel_requirements/new").should route_to("sel_requirements#new")
    end

    it "routes to #show" do
      get("/sel_requirements/1").should route_to("sel_requirements#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_requirements/1/edit").should route_to("sel_requirements#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_requirements").should route_to("sel_requirements#create")
    end

    it "routes to #update" do
      put("/sel_requirements/1").should route_to("sel_requirements#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_requirements/1").should route_to("sel_requirements#destroy", :id => "1")
    end

  end
end
