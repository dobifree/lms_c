require "spec_helper"

describe TrainingImpactCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/training_impact_categories").should route_to("training_impact_categories#index")
    end

    it "routes to #new" do
      get("/training_impact_categories/new").should route_to("training_impact_categories#new")
    end

    it "routes to #show" do
      get("/training_impact_categories/1").should route_to("training_impact_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/training_impact_categories/1/edit").should route_to("training_impact_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/training_impact_categories").should route_to("training_impact_categories#create")
    end

    it "routes to #update" do
      put("/training_impact_categories/1").should route_to("training_impact_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/training_impact_categories/1").should route_to("training_impact_categories#destroy", :id => "1")
    end

  end
end
