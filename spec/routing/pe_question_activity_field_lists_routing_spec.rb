require "spec_helper"

describe PeQuestionActivityFieldListsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_question_activity_field_lists").should route_to("pe_question_activity_field_lists#index")
    end

    it "routes to #new" do
      get("/pe_question_activity_field_lists/new").should route_to("pe_question_activity_field_lists#new")
    end

    it "routes to #show" do
      get("/pe_question_activity_field_lists/1").should route_to("pe_question_activity_field_lists#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_question_activity_field_lists/1/edit").should route_to("pe_question_activity_field_lists#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_question_activity_field_lists").should route_to("pe_question_activity_field_lists#create")
    end

    it "routes to #update" do
      put("/pe_question_activity_field_lists/1").should route_to("pe_question_activity_field_lists#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_question_activity_field_lists/1").should route_to("pe_question_activity_field_lists#destroy", :id => "1")
    end

  end
end
