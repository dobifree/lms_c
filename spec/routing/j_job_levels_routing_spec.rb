require "spec_helper"

describe JJobLevelsController do
  describe "routing" do

    it "routes to #index" do
      get("/j_job_levels").should route_to("j_job_levels#index")
    end

    it "routes to #new" do
      get("/j_job_levels/new").should route_to("j_job_levels#new")
    end

    it "routes to #show" do
      get("/j_job_levels/1").should route_to("j_job_levels#show", :id => "1")
    end

    it "routes to #edit" do
      get("/j_job_levels/1/edit").should route_to("j_job_levels#edit", :id => "1")
    end

    it "routes to #create" do
      post("/j_job_levels").should route_to("j_job_levels#create")
    end

    it "routes to #update" do
      put("/j_job_levels/1").should route_to("j_job_levels#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/j_job_levels/1").should route_to("j_job_levels#destroy", :id => "1")
    end

  end
end
