require "spec_helper"

describe PeCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_characteristics").should route_to("pe_characteristics#index")
    end

    it "routes to #new" do
      get("/pe_characteristics/new").should route_to("pe_characteristics#new")
    end

    it "routes to #show" do
      get("/pe_characteristics/1").should route_to("pe_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_characteristics/1/edit").should route_to("pe_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_characteristics").should route_to("pe_characteristics#create")
    end

    it "routes to #update" do
      put("/pe_characteristics/1").should route_to("pe_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_characteristics/1").should route_to("pe_characteristics#destroy", :id => "1")
    end

  end
end
