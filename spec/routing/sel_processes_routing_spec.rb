require "spec_helper"

describe SelProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_processes").should route_to("sel_processes#index")
    end

    it "routes to #new" do
      get("/sel_processes/new").should route_to("sel_processes#new")
    end

    it "routes to #show" do
      get("/sel_processes/1").should route_to("sel_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_processes/1/edit").should route_to("sel_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_processes").should route_to("sel_processes#create")
    end

    it "routes to #update" do
      put("/sel_processes/1").should route_to("sel_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_processes/1").should route_to("sel_processes#destroy", :id => "1")
    end

  end
end
