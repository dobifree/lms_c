require "spec_helper"

describe BrcCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/brc_categories").should route_to("brc_categories#index")
    end

    it "routes to #new" do
      get("/brc_categories/new").should route_to("brc_categories#new")
    end

    it "routes to #show" do
      get("/brc_categories/1").should route_to("brc_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/brc_categories/1/edit").should route_to("brc_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/brc_categories").should route_to("brc_categories#create")
    end

    it "routes to #update" do
      put("/brc_categories/1").should route_to("brc_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/brc_categories/1").should route_to("brc_categories#destroy", :id => "1")
    end

  end
end
