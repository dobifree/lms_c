require "spec_helper"

describe PeProcessTutorialsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_process_tutorials").should route_to("pe_process_tutorials#index")
    end

    it "routes to #new" do
      get("/pe_process_tutorials/new").should route_to("pe_process_tutorials#new")
    end

    it "routes to #show" do
      get("/pe_process_tutorials/1").should route_to("pe_process_tutorials#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_process_tutorials/1/edit").should route_to("pe_process_tutorials#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_process_tutorials").should route_to("pe_process_tutorials#create")
    end

    it "routes to #update" do
      put("/pe_process_tutorials/1").should route_to("pe_process_tutorials#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_process_tutorials/1").should route_to("pe_process_tutorials#destroy", :id => "1")
    end

  end
end
