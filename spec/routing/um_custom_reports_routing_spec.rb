require "spec_helper"

describe UmCustomReportsController do
  describe "routing" do

    it "routes to #index" do
      get("/um_custom_reports").should route_to("um_custom_reports#index")
    end

    it "routes to #new" do
      get("/um_custom_reports/new").should route_to("um_custom_reports#new")
    end

    it "routes to #show" do
      get("/um_custom_reports/1").should route_to("um_custom_reports#show", :id => "1")
    end

    it "routes to #edit" do
      get("/um_custom_reports/1/edit").should route_to("um_custom_reports#edit", :id => "1")
    end

    it "routes to #create" do
      post("/um_custom_reports").should route_to("um_custom_reports#create")
    end

    it "routes to #update" do
      put("/um_custom_reports/1").should route_to("um_custom_reports#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/um_custom_reports/1").should route_to("um_custom_reports#destroy", :id => "1")
    end

  end
end
