require "spec_helper"

describe PeQuestionCommentsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_question_comments").should route_to("pe_question_comments#index")
    end

    it "routes to #new" do
      get("/pe_question_comments/new").should route_to("pe_question_comments#new")
    end

    it "routes to #show" do
      get("/pe_question_comments/1").should route_to("pe_question_comments#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_question_comments/1/edit").should route_to("pe_question_comments#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_question_comments").should route_to("pe_question_comments#create")
    end

    it "routes to #update" do
      put("/pe_question_comments/1").should route_to("pe_question_comments#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_question_comments/1").should route_to("pe_question_comments#destroy", :id => "1")
    end

  end
end
