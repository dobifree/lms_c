require "spec_helper"

describe KpiDashboardManagersController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_dashboard_managers").should route_to("kpi_dashboard_managers#index")
    end

    it "routes to #new" do
      get("/kpi_dashboard_managers/new").should route_to("kpi_dashboard_managers#new")
    end

    it "routes to #show" do
      get("/kpi_dashboard_managers/1").should route_to("kpi_dashboard_managers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_dashboard_managers/1/edit").should route_to("kpi_dashboard_managers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_dashboard_managers").should route_to("kpi_dashboard_managers#create")
    end

    it "routes to #update" do
      put("/kpi_dashboard_managers/1").should route_to("kpi_dashboard_managers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_dashboard_managers/1").should route_to("kpi_dashboard_managers#destroy", :id => "1")
    end

  end
end
