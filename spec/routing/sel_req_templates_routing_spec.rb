require "spec_helper"

describe SelReqTemplatesController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_req_templates").should route_to("sel_req_templates#index")
    end

    it "routes to #new" do
      get("/sel_req_templates/new").should route_to("sel_req_templates#new")
    end

    it "routes to #show" do
      get("/sel_req_templates/1").should route_to("sel_req_templates#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_req_templates/1/edit").should route_to("sel_req_templates#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_req_templates").should route_to("sel_req_templates#create")
    end

    it "routes to #update" do
      put("/sel_req_templates/1").should route_to("sel_req_templates#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_req_templates/1").should route_to("sel_req_templates#destroy", :id => "1")
    end

  end
end
