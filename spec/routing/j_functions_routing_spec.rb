require "spec_helper"

describe JFunctionsController do
  describe "routing" do

    it "routes to #index" do
      get("/j_functions").should route_to("j_functions#index")
    end

    it "routes to #new" do
      get("/j_functions/new").should route_to("j_functions#new")
    end

    it "routes to #show" do
      get("/j_functions/1").should route_to("j_functions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/j_functions/1/edit").should route_to("j_functions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/j_functions").should route_to("j_functions#create")
    end

    it "routes to #update" do
      put("/j_functions/1").should route_to("j_functions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/j_functions/1").should route_to("j_functions#destroy", :id => "1")
    end

  end
end
