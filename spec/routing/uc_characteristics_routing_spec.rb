require "spec_helper"

describe UcCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/uc_characteristics").should route_to("uc_characteristics#index")
    end

    it "routes to #new" do
      get("/uc_characteristics/new").should route_to("uc_characteristics#new")
    end

    it "routes to #show" do
      get("/uc_characteristics/1").should route_to("uc_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/uc_characteristics/1/edit").should route_to("uc_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/uc_characteristics").should route_to("uc_characteristics#create")
    end

    it "routes to #update" do
      put("/uc_characteristics/1").should route_to("uc_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/uc_characteristics/1").should route_to("uc_characteristics#destroy", :id => "1")
    end

  end
end
