require "spec_helper"

describe BrcProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/brc_processes").should route_to("brc_processes#index")
    end

    it "routes to #new" do
      get("/brc_processes/new").should route_to("brc_processes#new")
    end

    it "routes to #show" do
      get("/brc_processes/1").should route_to("brc_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/brc_processes/1/edit").should route_to("brc_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/brc_processes").should route_to("brc_processes#create")
    end

    it "routes to #update" do
      put("/brc_processes/1").should route_to("brc_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/brc_processes/1").should route_to("brc_processes#destroy", :id => "1")
    end

  end
end
