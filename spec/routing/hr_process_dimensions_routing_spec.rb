require "spec_helper"

describe HrProcessDimensionsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_dimensions").should route_to("hr_process_dimensions#index")
    end

    it "routes to #new" do
      get("/hr_process_dimensions/new").should route_to("hr_process_dimensions#new")
    end

    it "routes to #show" do
      get("/hr_process_dimensions/1").should route_to("hr_process_dimensions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_dimensions/1/edit").should route_to("hr_process_dimensions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_dimensions").should route_to("hr_process_dimensions#create")
    end

    it "routes to #update" do
      put("/hr_process_dimensions/1").should route_to("hr_process_dimensions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_dimensions/1").should route_to("hr_process_dimensions#destroy", :id => "1")
    end

  end
end
