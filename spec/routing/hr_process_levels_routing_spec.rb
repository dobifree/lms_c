require "spec_helper"

describe HrProcessLevelsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_levels").should route_to("hr_process_levels#index")
    end

    it "routes to #new" do
      get("/hr_process_levels/new").should route_to("hr_process_levels#new")
    end

    it "routes to #show" do
      get("/hr_process_levels/1").should route_to("hr_process_levels#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_levels/1/edit").should route_to("hr_process_levels#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_levels").should route_to("hr_process_levels#create")
    end

    it "routes to #update" do
      put("/hr_process_levels/1").should route_to("hr_process_levels#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_levels/1").should route_to("hr_process_levels#destroy", :id => "1")
    end

  end
end
