require "spec_helper"

describe KpiIndicatorsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_indicators").should route_to("kpi_indicators#index")
    end

    it "routes to #new" do
      get("/kpi_indicators/new").should route_to("kpi_indicators#new")
    end

    it "routes to #show" do
      get("/kpi_indicators/1").should route_to("kpi_indicators#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_indicators/1/edit").should route_to("kpi_indicators#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_indicators").should route_to("kpi_indicators#create")
    end

    it "routes to #update" do
      put("/kpi_indicators/1").should route_to("kpi_indicators#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_indicators/1").should route_to("kpi_indicators#destroy", :id => "1")
    end

  end
end
