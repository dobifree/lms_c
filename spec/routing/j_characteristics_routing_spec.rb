require "spec_helper"

describe JCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/j_characteristics").should route_to("j_characteristics#index")
    end

    it "routes to #new" do
      get("/j_characteristics/new").should route_to("j_characteristics#new")
    end

    it "routes to #show" do
      get("/j_characteristics/1").should route_to("j_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/j_characteristics/1/edit").should route_to("j_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/j_characteristics").should route_to("j_characteristics#create")
    end

    it "routes to #update" do
      put("/j_characteristics/1").should route_to("j_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/j_characteristics/1").should route_to("j_characteristics#destroy", :id => "1")
    end

  end
end
