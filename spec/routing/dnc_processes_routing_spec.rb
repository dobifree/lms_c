require "spec_helper"

describe DncProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/dnc_processes").should route_to("dnc_processes#index")
    end

    it "routes to #new" do
      get("/dnc_processes/new").should route_to("dnc_processes#new")
    end

    it "routes to #show" do
      get("/dnc_processes/1").should route_to("dnc_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dnc_processes/1/edit").should route_to("dnc_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dnc_processes").should route_to("dnc_processes#create")
    end

    it "routes to #update" do
      put("/dnc_processes/1").should route_to("dnc_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dnc_processes/1").should route_to("dnc_processes#destroy", :id => "1")
    end

  end
end
