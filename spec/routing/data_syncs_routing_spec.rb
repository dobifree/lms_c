require "spec_helper"

describe DataSyncsController do
  describe "routing" do

    it "routes to #index" do
      get("/data_syncs").should route_to("data_syncs#index")
    end

    it "routes to #new" do
      get("/data_syncs/new").should route_to("data_syncs#new")
    end

    it "routes to #show" do
      get("/data_syncs/1").should route_to("data_syncs#show", :id => "1")
    end

    it "routes to #edit" do
      get("/data_syncs/1/edit").should route_to("data_syncs#edit", :id => "1")
    end

    it "routes to #create" do
      post("/data_syncs").should route_to("data_syncs#create")
    end

    it "routes to #update" do
      put("/data_syncs/1").should route_to("data_syncs#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/data_syncs/1").should route_to("data_syncs#destroy", :id => "1")
    end

  end
end
