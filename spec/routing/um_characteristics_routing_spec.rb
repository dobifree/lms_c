require "spec_helper"

describe UmCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/um_characteristics").should route_to("um_characteristics#index")
    end

    it "routes to #new" do
      get("/um_characteristics/new").should route_to("um_characteristics#new")
    end

    it "routes to #show" do
      get("/um_characteristics/1").should route_to("um_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/um_characteristics/1/edit").should route_to("um_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/um_characteristics").should route_to("um_characteristics#create")
    end

    it "routes to #update" do
      put("/um_characteristics/1").should route_to("um_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/um_characteristics/1").should route_to("um_characteristics#destroy", :id => "1")
    end

  end
end
