require "spec_helper"

describe JmCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/jm_characteristics").should route_to("jm_characteristics#index")
    end

    it "routes to #new" do
      get("/jm_characteristics/new").should route_to("jm_characteristics#new")
    end

    it "routes to #show" do
      get("/jm_characteristics/1").should route_to("jm_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/jm_characteristics/1/edit").should route_to("jm_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/jm_characteristics").should route_to("jm_characteristics#create")
    end

    it "routes to #update" do
      put("/jm_characteristics/1").should route_to("jm_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/jm_characteristics/1").should route_to("jm_characteristics#destroy", :id => "1")
    end

  end
end
