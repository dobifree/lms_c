require "spec_helper"

describe KpiMeasurementsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_measurements").should route_to("kpi_measurements#index")
    end

    it "routes to #new" do
      get("/kpi_measurements/new").should route_to("kpi_measurements#new")
    end

    it "routes to #show" do
      get("/kpi_measurements/1").should route_to("kpi_measurements#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_measurements/1/edit").should route_to("kpi_measurements#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_measurements").should route_to("kpi_measurements#create")
    end

    it "routes to #update" do
      put("/kpi_measurements/1").should route_to("kpi_measurements#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_measurements/1").should route_to("kpi_measurements#destroy", :id => "1")
    end

  end
end
