require "spec_helper"

describe PlanningProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/planning_processes").should route_to("planning_processes#index")
    end

    it "routes to #new" do
      get("/planning_processes/new").should route_to("planning_processes#new")
    end

    it "routes to #show" do
      get("/planning_processes/1").should route_to("planning_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/planning_processes/1/edit").should route_to("planning_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/planning_processes").should route_to("planning_processes#create")
    end

    it "routes to #update" do
      put("/planning_processes/1").should route_to("planning_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/planning_processes/1").should route_to("planning_processes#destroy", :id => "1")
    end

  end
end
