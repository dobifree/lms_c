require "spec_helper"

describe PeQuestionModelsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_question_models").should route_to("pe_question_models#index")
    end

    it "routes to #new" do
      get("/pe_question_models/new").should route_to("pe_question_models#new")
    end

    it "routes to #show" do
      get("/pe_question_models/1").should route_to("pe_question_models#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_question_models/1/edit").should route_to("pe_question_models#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_question_models").should route_to("pe_question_models#create")
    end

    it "routes to #update" do
      put("/pe_question_models/1").should route_to("pe_question_models#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_question_models/1").should route_to("pe_question_models#destroy", :id => "1")
    end

  end
end
