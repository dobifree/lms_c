require "spec_helper"

describe DncProgramsController do
  describe "routing" do

    it "routes to #index" do
      get("/dnc_programs").should route_to("dnc_programs#index")
    end

    it "routes to #new" do
      get("/dnc_programs/new").should route_to("dnc_programs#new")
    end

    it "routes to #show" do
      get("/dnc_programs/1").should route_to("dnc_programs#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dnc_programs/1/edit").should route_to("dnc_programs#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dnc_programs").should route_to("dnc_programs#create")
    end

    it "routes to #update" do
      put("/dnc_programs/1").should route_to("dnc_programs#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dnc_programs/1").should route_to("dnc_programs#destroy", :id => "1")
    end

  end
end
