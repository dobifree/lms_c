require "spec_helper"

describe BrcMailsController do
  describe "routing" do

    it "routes to #index" do
      get("/brc_mails").should route_to("brc_mails#index")
    end

    it "routes to #new" do
      get("/brc_mails/new").should route_to("brc_mails#new")
    end

    it "routes to #show" do
      get("/brc_mails/1").should route_to("brc_mails#show", :id => "1")
    end

    it "routes to #edit" do
      get("/brc_mails/1/edit").should route_to("brc_mails#edit", :id => "1")
    end

    it "routes to #create" do
      post("/brc_mails").should route_to("brc_mails#create")
    end

    it "routes to #update" do
      put("/brc_mails/1").should route_to("brc_mails#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/brc_mails/1").should route_to("brc_mails#destroy", :id => "1")
    end

  end
end
