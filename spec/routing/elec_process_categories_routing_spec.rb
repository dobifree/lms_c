require "spec_helper"

describe ElecProcessCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/elec_process_categories").should route_to("elec_process_categories#index")
    end

    it "routes to #new" do
      get("/elec_process_categories/new").should route_to("elec_process_categories#new")
    end

    it "routes to #show" do
      get("/elec_process_categories/1").should route_to("elec_process_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/elec_process_categories/1/edit").should route_to("elec_process_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/elec_process_categories").should route_to("elec_process_categories#create")
    end

    it "routes to #update" do
      put("/elec_process_categories/1").should route_to("elec_process_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/elec_process_categories/1").should route_to("elec_process_categories#destroy", :id => "1")
    end

  end
end
