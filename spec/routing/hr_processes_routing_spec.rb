require "spec_helper"

describe HrProcessesController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_processes").should route_to("hr_processes#index")
    end

    it "routes to #new" do
      get("/hr_processes/new").should route_to("hr_processes#new")
    end

    it "routes to #show" do
      get("/hr_processes/1").should route_to("hr_processes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_processes/1/edit").should route_to("hr_processes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_processes").should route_to("hr_processes#create")
    end

    it "routes to #update" do
      put("/hr_processes/1").should route_to("hr_processes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_processes/1").should route_to("hr_processes#destroy", :id => "1")
    end

  end
end
