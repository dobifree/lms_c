require "spec_helper"

describe TrainingProgramsController do
  describe "routing" do

    it "routes to #index" do
      get("/training_programs").should route_to("training_programs#index")
    end

    it "routes to #new" do
      get("/training_programs/new").should route_to("training_programs#new")
    end

    it "routes to #show" do
      get("/training_programs/1").should route_to("training_programs#show", :id => "1")
    end

    it "routes to #edit" do
      get("/training_programs/1/edit").should route_to("training_programs#edit", :id => "1")
    end

    it "routes to #create" do
      post("/training_programs").should route_to("training_programs#create")
    end

    it "routes to #update" do
      put("/training_programs/1").should route_to("training_programs#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/training_programs/1").should route_to("training_programs#destroy", :id => "1")
    end

  end
end
