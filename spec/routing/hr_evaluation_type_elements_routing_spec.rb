require "spec_helper"

describe HrEvaluationTypeElementsController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_evaluation_type_elements").should route_to("hr_evaluation_type_elements#index")
    end

    it "routes to #new" do
      get("/hr_evaluation_type_elements/new").should route_to("hr_evaluation_type_elements#new")
    end

    it "routes to #show" do
      get("/hr_evaluation_type_elements/1").should route_to("hr_evaluation_type_elements#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_evaluation_type_elements/1/edit").should route_to("hr_evaluation_type_elements#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_evaluation_type_elements").should route_to("hr_evaluation_type_elements#create")
    end

    it "routes to #update" do
      put("/hr_evaluation_type_elements/1").should route_to("hr_evaluation_type_elements#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_evaluation_type_elements/1").should route_to("hr_evaluation_type_elements#destroy", :id => "1")
    end

  end
end
