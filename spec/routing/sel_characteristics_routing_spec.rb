require "spec_helper"

describe SelCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_characteristics").should route_to("sel_characteristics#index")
    end

    it "routes to #new" do
      get("/sel_characteristics/new").should route_to("sel_characteristics#new")
    end

    it "routes to #show" do
      get("/sel_characteristics/1").should route_to("sel_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_characteristics/1/edit").should route_to("sel_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_characteristics").should route_to("sel_characteristics#create")
    end

    it "routes to #update" do
      put("/sel_characteristics/1").should route_to("sel_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_characteristics/1").should route_to("sel_characteristics#destroy", :id => "1")
    end

  end
end
