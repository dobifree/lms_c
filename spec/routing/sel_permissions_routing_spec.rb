require "spec_helper"

describe SelPermissionsController do
  describe "routing" do

    it "routes to #index" do
      get("/sel_permissions").should route_to("sel_permissions#index")
    end

    it "routes to #new" do
      get("/sel_permissions/new").should route_to("sel_permissions#new")
    end

    it "routes to #show" do
      get("/sel_permissions/1").should route_to("sel_permissions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sel_permissions/1/edit").should route_to("sel_permissions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sel_permissions").should route_to("sel_permissions#create")
    end

    it "routes to #update" do
      put("/sel_permissions/1").should route_to("sel_permissions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sel_permissions/1").should route_to("sel_permissions#destroy", :id => "1")
    end

  end
end
