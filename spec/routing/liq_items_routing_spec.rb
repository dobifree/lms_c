require "spec_helper"

describe LiqItemsController do
  describe "routing" do

    it "routes to #index" do
      get("/liq_items").should route_to("liq_items#index")
    end

    it "routes to #new" do
      get("/liq_items/new").should route_to("liq_items#new")
    end

    it "routes to #show" do
      get("/liq_items/1").should route_to("liq_items#show", :id => "1")
    end

    it "routes to #edit" do
      get("/liq_items/1/edit").should route_to("liq_items#edit", :id => "1")
    end

    it "routes to #create" do
      post("/liq_items").should route_to("liq_items#create")
    end

    it "routes to #update" do
      put("/liq_items/1").should route_to("liq_items#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/liq_items/1").should route_to("liq_items#destroy", :id => "1")
    end

  end
end
