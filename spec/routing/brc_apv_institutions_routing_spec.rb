require "spec_helper"

describe BrcApvInstitutionsController do
  describe "routing" do

    it "routes to #index" do
      get("/brc_apv_institutions").should route_to("brc_apv_institutions#index")
    end

    it "routes to #new" do
      get("/brc_apv_institutions/new").should route_to("brc_apv_institutions#new")
    end

    it "routes to #show" do
      get("/brc_apv_institutions/1").should route_to("brc_apv_institutions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/brc_apv_institutions/1/edit").should route_to("brc_apv_institutions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/brc_apv_institutions").should route_to("brc_apv_institutions#create")
    end

    it "routes to #update" do
      put("/brc_apv_institutions/1").should route_to("brc_apv_institutions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/brc_apv_institutions/1").should route_to("brc_apv_institutions#destroy", :id => "1")
    end

  end
end
