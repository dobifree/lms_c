require "spec_helper"

describe HrProcessEvaluationQSchedulesController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_evaluation_q_schedules").should route_to("hr_process_evaluation_q_schedules#index")
    end

    it "routes to #new" do
      get("/hr_process_evaluation_q_schedules/new").should route_to("hr_process_evaluation_q_schedules#new")
    end

    it "routes to #show" do
      get("/hr_process_evaluation_q_schedules/1").should route_to("hr_process_evaluation_q_schedules#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_evaluation_q_schedules/1/edit").should route_to("hr_process_evaluation_q_schedules#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_evaluation_q_schedules").should route_to("hr_process_evaluation_q_schedules#create")
    end

    it "routes to #update" do
      put("/hr_process_evaluation_q_schedules/1").should route_to("hr_process_evaluation_q_schedules#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_evaluation_q_schedules/1").should route_to("hr_process_evaluation_q_schedules#destroy", :id => "1")
    end

  end
end
