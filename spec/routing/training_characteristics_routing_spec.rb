require "spec_helper"

describe TrainingCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/training_characteristics").should route_to("training_characteristics#index")
    end

    it "routes to #new" do
      get("/training_characteristics/new").should route_to("training_characteristics#new")
    end

    it "routes to #show" do
      get("/training_characteristics/1").should route_to("training_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/training_characteristics/1/edit").should route_to("training_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/training_characteristics").should route_to("training_characteristics#create")
    end

    it "routes to #update" do
      put("/training_characteristics/1").should route_to("training_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/training_characteristics/1").should route_to("training_characteristics#destroy", :id => "1")
    end

  end
end
