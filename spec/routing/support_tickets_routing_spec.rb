require "spec_helper"

describe SupportTicketsController do
  describe "routing" do

    it "routes to #index" do
      get("/support_tickets").should route_to("support_tickets#index")
    end

    it "routes to #new" do
      get("/support_tickets/new").should route_to("support_tickets#new")
    end

    it "routes to #show" do
      get("/support_tickets/1").should route_to("support_tickets#show", :id => "1")
    end

    it "routes to #edit" do
      get("/support_tickets/1/edit").should route_to("support_tickets#edit", :id => "1")
    end

    it "routes to #create" do
      post("/support_tickets").should route_to("support_tickets#create")
    end

    it "routes to #update" do
      put("/support_tickets/1").should route_to("support_tickets#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/support_tickets/1").should route_to("support_tickets#destroy", :id => "1")
    end

  end
end
