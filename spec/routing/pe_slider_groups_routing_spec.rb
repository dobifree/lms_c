require "spec_helper"

describe PeSliderGroupsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_slider_groups").should route_to("pe_slider_groups#index")
    end

    it "routes to #new" do
      get("/pe_slider_groups/new").should route_to("pe_slider_groups#new")
    end

    it "routes to #show" do
      get("/pe_slider_groups/1").should route_to("pe_slider_groups#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_slider_groups/1/edit").should route_to("pe_slider_groups#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_slider_groups").should route_to("pe_slider_groups#create")
    end

    it "routes to #update" do
      put("/pe_slider_groups/1").should route_to("pe_slider_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_slider_groups/1").should route_to("pe_slider_groups#destroy", :id => "1")
    end

  end
end
