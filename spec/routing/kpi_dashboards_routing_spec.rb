require "spec_helper"

describe KpiDashboardsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_dashboards").should route_to("kpi_dashboards#index")
    end

    it "routes to #new" do
      get("/kpi_dashboards/new").should route_to("kpi_dashboards#new")
    end

    it "routes to #show" do
      get("/kpi_dashboards/1").should route_to("kpi_dashboards#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_dashboards/1/edit").should route_to("kpi_dashboards#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_dashboards").should route_to("kpi_dashboards#create")
    end

    it "routes to #update" do
      put("/kpi_dashboards/1").should route_to("kpi_dashboards#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_dashboards/1").should route_to("kpi_dashboards#destroy", :id => "1")
    end

  end
end
