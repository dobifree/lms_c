require "spec_helper"

describe PeDimensionsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_dimensions").should route_to("pe_dimensions#index")
    end

    it "routes to #new" do
      get("/pe_dimensions/new").should route_to("pe_dimensions#new")
    end

    it "routes to #show" do
      get("/pe_dimensions/1").should route_to("pe_dimensions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_dimensions/1/edit").should route_to("pe_dimensions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_dimensions").should route_to("pe_dimensions#create")
    end

    it "routes to #update" do
      put("/pe_dimensions/1").should route_to("pe_dimensions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_dimensions/1").should route_to("pe_dimensions#destroy", :id => "1")
    end

  end
end
