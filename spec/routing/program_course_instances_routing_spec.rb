require "spec_helper"

describe ProgramCourseInstancesController do
  describe "routing" do

    it "routes to #index" do
      get("/program_course_instances").should route_to("program_course_instances#index")
    end

    it "routes to #new" do
      get("/program_course_instances/new").should route_to("program_course_instances#new")
    end

    it "routes to #show" do
      get("/program_course_instances/1").should route_to("program_course_instances#show", :id => "1")
    end

    it "routes to #edit" do
      get("/program_course_instances/1/edit").should route_to("program_course_instances#edit", :id => "1")
    end

    it "routes to #create" do
      post("/program_course_instances").should route_to("program_course_instances#create")
    end

    it "routes to #update" do
      put("/program_course_instances/1").should route_to("program_course_instances#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/program_course_instances/1").should route_to("program_course_instances#destroy", :id => "1")
    end

  end
end
