require "spec_helper"

describe UserDeleteReasonsController do
  describe "routing" do

    it "routes to #index" do
      get("/user_delete_reasons").should route_to("user_delete_reasons#index")
    end

    it "routes to #new" do
      get("/user_delete_reasons/new").should route_to("user_delete_reasons#new")
    end

    it "routes to #show" do
      get("/user_delete_reasons/1").should route_to("user_delete_reasons#show", :id => "1")
    end

    it "routes to #edit" do
      get("/user_delete_reasons/1/edit").should route_to("user_delete_reasons#edit", :id => "1")
    end

    it "routes to #create" do
      post("/user_delete_reasons").should route_to("user_delete_reasons#create")
    end

    it "routes to #update" do
      put("/user_delete_reasons/1").should route_to("user_delete_reasons#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/user_delete_reasons/1").should route_to("user_delete_reasons#destroy", :id => "1")
    end

  end
end
