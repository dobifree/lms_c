require "spec_helper"

describe PeMemberRelSharedCommentsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_member_rel_shared_comments").should route_to("pe_member_rel_shared_comments#index")
    end

    it "routes to #new" do
      get("/pe_member_rel_shared_comments/new").should route_to("pe_member_rel_shared_comments#new")
    end

    it "routes to #show" do
      get("/pe_member_rel_shared_comments/1").should route_to("pe_member_rel_shared_comments#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_member_rel_shared_comments/1/edit").should route_to("pe_member_rel_shared_comments#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_member_rel_shared_comments").should route_to("pe_member_rel_shared_comments#create")
    end

    it "routes to #update" do
      put("/pe_member_rel_shared_comments/1").should route_to("pe_member_rel_shared_comments#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_member_rel_shared_comments/1").should route_to("pe_member_rel_shared_comments#destroy", :id => "1")
    end

  end
end
