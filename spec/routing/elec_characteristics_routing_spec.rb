require "spec_helper"

describe ElecCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/elec_characteristics").should route_to("elec_characteristics#index")
    end

    it "routes to #new" do
      get("/elec_characteristics/new").should route_to("elec_characteristics#new")
    end

    it "routes to #show" do
      get("/elec_characteristics/1").should route_to("elec_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/elec_characteristics/1/edit").should route_to("elec_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/elec_characteristics").should route_to("elec_characteristics#create")
    end

    it "routes to #update" do
      put("/elec_characteristics/1").should route_to("elec_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/elec_characteristics/1").should route_to("elec_characteristics#destroy", :id => "1")
    end

  end
end
