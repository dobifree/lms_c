require "spec_helper"

describe HrProcessTemplatesController do
  describe "routing" do

    it "routes to #index" do
      get("/hr_process_templates").should route_to("hr_process_templates#index")
    end

    it "routes to #new" do
      get("/hr_process_templates/new").should route_to("hr_process_templates#new")
    end

    it "routes to #show" do
      get("/hr_process_templates/1").should route_to("hr_process_templates#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hr_process_templates/1/edit").should route_to("hr_process_templates#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hr_process_templates").should route_to("hr_process_templates#create")
    end

    it "routes to #update" do
      put("/hr_process_templates/1").should route_to("hr_process_templates#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hr_process_templates/1").should route_to("hr_process_templates#destroy", :id => "1")
    end

  end
end
