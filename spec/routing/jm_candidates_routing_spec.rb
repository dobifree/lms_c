require "spec_helper"

describe JmCandidatesController do
  describe "routing" do

    it "routes to #index" do
      get("/jm_candidates").should route_to("jm_candidates#index")
    end

    it "routes to #new" do
      get("/jm_candidates/new").should route_to("jm_candidates#new")
    end

    it "routes to #show" do
      get("/jm_candidates/1").should route_to("jm_candidates#show", :id => "1")
    end

    it "routes to #edit" do
      get("/jm_candidates/1/edit").should route_to("jm_candidates#edit", :id => "1")
    end

    it "routes to #create" do
      post("/jm_candidates").should route_to("jm_candidates#create")
    end

    it "routes to #update" do
      put("/jm_candidates/1").should route_to("jm_candidates#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/jm_candidates/1").should route_to("jm_candidates#destroy", :id => "1")
    end

  end
end
