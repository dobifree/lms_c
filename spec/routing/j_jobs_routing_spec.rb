require "spec_helper"

describe JJobsController do
  describe "routing" do

    it "routes to #index" do
      get("/j_jobs").should route_to("j_jobs#index")
    end

    it "routes to #new" do
      get("/j_jobs/new").should route_to("j_jobs#new")
    end

    it "routes to #show" do
      get("/j_jobs/1").should route_to("j_jobs#show", :id => "1")
    end

    it "routes to #edit" do
      get("/j_jobs/1/edit").should route_to("j_jobs#edit", :id => "1")
    end

    it "routes to #create" do
      post("/j_jobs").should route_to("j_jobs#create")
    end

    it "routes to #update" do
      put("/j_jobs/1").should route_to("j_jobs#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/j_jobs/1").should route_to("j_jobs#destroy", :id => "1")
    end

  end
end
