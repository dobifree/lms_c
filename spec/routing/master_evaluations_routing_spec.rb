require "spec_helper"

describe MasterEvaluationsController do
  describe "routing" do

    it "routes to #index" do
      get("/master_evaluations").should route_to("master_evaluations#index")
    end

    it "routes to #new" do
      get("/master_evaluations/new").should route_to("master_evaluations#new")
    end

    it "routes to #show" do
      get("/master_evaluations/1").should route_to("master_evaluations#show", :id => "1")
    end

    it "routes to #edit" do
      get("/master_evaluations/1/edit").should route_to("master_evaluations#edit", :id => "1")
    end

    it "routes to #create" do
      post("/master_evaluations").should route_to("master_evaluations#create")
    end

    it "routes to #update" do
      put("/master_evaluations/1").should route_to("master_evaluations#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/master_evaluations/1").should route_to("master_evaluations#destroy", :id => "1")
    end

  end
end
