require "spec_helper"

describe PeGroupsController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_groups").should route_to("pe_groups#index")
    end

    it "routes to #new" do
      get("/pe_groups/new").should route_to("pe_groups#new")
    end

    it "routes to #show" do
      get("/pe_groups/1").should route_to("pe_groups#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_groups/1/edit").should route_to("pe_groups#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_groups").should route_to("pe_groups#create")
    end

    it "routes to #update" do
      put("/pe_groups/1").should route_to("pe_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_groups/1").should route_to("pe_groups#destroy", :id => "1")
    end

  end
end
