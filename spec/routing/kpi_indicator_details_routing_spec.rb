require "spec_helper"

describe KpiIndicatorDetailsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_indicator_details").should route_to("kpi_indicator_details#index")
    end

    it "routes to #new" do
      get("/kpi_indicator_details/new").should route_to("kpi_indicator_details#new")
    end

    it "routes to #show" do
      get("/kpi_indicator_details/1").should route_to("kpi_indicator_details#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_indicator_details/1/edit").should route_to("kpi_indicator_details#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_indicator_details").should route_to("kpi_indicator_details#create")
    end

    it "routes to #update" do
      put("/kpi_indicator_details/1").should route_to("kpi_indicator_details#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_indicator_details/1").should route_to("kpi_indicator_details#destroy", :id => "1")
    end

  end
end
