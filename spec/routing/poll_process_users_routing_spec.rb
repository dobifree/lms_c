require "spec_helper"

describe PollProcessUsersController do
  describe "routing" do

    it "routes to #index" do
      get("/poll_process_users").should route_to("poll_process_users#index")
    end

    it "routes to #new" do
      get("/poll_process_users/new").should route_to("poll_process_users#new")
    end

    it "routes to #show" do
      get("/poll_process_users/1").should route_to("poll_process_users#show", :id => "1")
    end

    it "routes to #edit" do
      get("/poll_process_users/1/edit").should route_to("poll_process_users#edit", :id => "1")
    end

    it "routes to #create" do
      post("/poll_process_users").should route_to("poll_process_users#create")
    end

    it "routes to #update" do
      put("/poll_process_users/1").should route_to("poll_process_users#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/poll_process_users/1").should route_to("poll_process_users#destroy", :id => "1")
    end

  end
end
