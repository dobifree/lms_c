require "spec_helper"

describe KpiDashboardGuestsController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_dashboard_guests").should route_to("kpi_dashboard_guests#index")
    end

    it "routes to #new" do
      get("/kpi_dashboard_guests/new").should route_to("kpi_dashboard_guests#new")
    end

    it "routes to #show" do
      get("/kpi_dashboard_guests/1").should route_to("kpi_dashboard_guests#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_dashboard_guests/1/edit").should route_to("kpi_dashboard_guests#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_dashboard_guests").should route_to("kpi_dashboard_guests#create")
    end

    it "routes to #update" do
      put("/kpi_dashboard_guests/1").should route_to("kpi_dashboard_guests#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_dashboard_guests/1").should route_to("kpi_dashboard_guests#destroy", :id => "1")
    end

  end
end
