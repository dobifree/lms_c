require "spec_helper"

describe PeReportersController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_reporters").should route_to("pe_reporters#index")
    end

    it "routes to #new" do
      get("/pe_reporters/new").should route_to("pe_reporters#new")
    end

    it "routes to #show" do
      get("/pe_reporters/1").should route_to("pe_reporters#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_reporters/1/edit").should route_to("pe_reporters#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_reporters").should route_to("pe_reporters#create")
    end

    it "routes to #update" do
      put("/pe_reporters/1").should route_to("pe_reporters#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_reporters/1").should route_to("pe_reporters#destroy", :id => "1")
    end

  end
end
