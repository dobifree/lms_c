require "spec_helper"

describe SupportTicketTypesController do
  describe "routing" do

    it "routes to #index" do
      get("/support_ticket_types").should route_to("support_ticket_types#index")
    end

    it "routes to #new" do
      get("/support_ticket_types/new").should route_to("support_ticket_types#new")
    end

    it "routes to #show" do
      get("/support_ticket_types/1").should route_to("support_ticket_types#show", :id => "1")
    end

    it "routes to #edit" do
      get("/support_ticket_types/1/edit").should route_to("support_ticket_types#edit", :id => "1")
    end

    it "routes to #create" do
      post("/support_ticket_types").should route_to("support_ticket_types#create")
    end

    it "routes to #update" do
      put("/support_ticket_types/1").should route_to("support_ticket_types#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/support_ticket_types/1").should route_to("support_ticket_types#destroy", :id => "1")
    end

  end
end
