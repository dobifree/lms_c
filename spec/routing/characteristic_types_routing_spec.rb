require "spec_helper"

describe CharacteristicTypesController do
  describe "routing" do

    it "routes to #index" do
      get("/characteristic_types").should route_to("characteristic_types#index")
    end

    it "routes to #new" do
      get("/characteristic_types/new").should route_to("characteristic_types#new")
    end

    it "routes to #show" do
      get("/characteristic_types/1").should route_to("characteristic_types#show", :id => "1")
    end

    it "routes to #edit" do
      get("/characteristic_types/1/edit").should route_to("characteristic_types#edit", :id => "1")
    end

    it "routes to #create" do
      post("/characteristic_types").should route_to("characteristic_types#create")
    end

    it "routes to #update" do
      put("/characteristic_types/1").should route_to("characteristic_types#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/characteristic_types/1").should route_to("characteristic_types#destroy", :id => "1")
    end

  end
end
