require "spec_helper"

describe MasterPollsController do
  describe "routing" do

    it "routes to #index" do
      get("/master_polls").should route_to("master_polls#index")
    end

    it "routes to #new" do
      get("/master_polls/new").should route_to("master_polls#new")
    end

    it "routes to #show" do
      get("/master_polls/1").should route_to("master_polls#show", :id => "1")
    end

    it "routes to #edit" do
      get("/master_polls/1/edit").should route_to("master_polls#edit", :id => "1")
    end

    it "routes to #create" do
      post("/master_polls").should route_to("master_polls#create")
    end

    it "routes to #update" do
      put("/master_polls/1").should route_to("master_polls#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/master_polls/1").should route_to("master_polls#destroy", :id => "1")
    end

  end
end
