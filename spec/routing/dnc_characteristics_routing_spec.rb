require "spec_helper"

describe DncCharacteristicsController do
  describe "routing" do

    it "routes to #index" do
      get("/dnc_characteristics").should route_to("dnc_characteristics#index")
    end

    it "routes to #new" do
      get("/dnc_characteristics/new").should route_to("dnc_characteristics#new")
    end

    it "routes to #show" do
      get("/dnc_characteristics/1").should route_to("dnc_characteristics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dnc_characteristics/1/edit").should route_to("dnc_characteristics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dnc_characteristics").should route_to("dnc_characteristics#create")
    end

    it "routes to #update" do
      put("/dnc_characteristics/1").should route_to("dnc_characteristics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dnc_characteristics/1").should route_to("dnc_characteristics#destroy", :id => "1")
    end

  end
end
