require "spec_helper"

describe KpiDashboardTypesController do
  describe "routing" do

    it "routes to #index" do
      get("/kpi_dashboard_types").should route_to("kpi_dashboard_types#index")
    end

    it "routes to #new" do
      get("/kpi_dashboard_types/new").should route_to("kpi_dashboard_types#new")
    end

    it "routes to #show" do
      get("/kpi_dashboard_types/1").should route_to("kpi_dashboard_types#show", :id => "1")
    end

    it "routes to #edit" do
      get("/kpi_dashboard_types/1/edit").should route_to("kpi_dashboard_types#edit", :id => "1")
    end

    it "routes to #create" do
      post("/kpi_dashboard_types").should route_to("kpi_dashboard_types#create")
    end

    it "routes to #update" do
      put("/kpi_dashboard_types/1").should route_to("kpi_dashboard_types#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/kpi_dashboard_types/1").should route_to("kpi_dashboard_types#destroy", :id => "1")
    end

  end
end
