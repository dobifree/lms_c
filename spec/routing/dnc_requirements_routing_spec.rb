require "spec_helper"

describe DncRequirementsController do
  describe "routing" do

    it "routes to #index" do
      get("/dnc_requirements").should route_to("dnc_requirements#index")
    end

    it "routes to #new" do
      get("/dnc_requirements/new").should route_to("dnc_requirements#new")
    end

    it "routes to #show" do
      get("/dnc_requirements/1").should route_to("dnc_requirements#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dnc_requirements/1/edit").should route_to("dnc_requirements#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dnc_requirements").should route_to("dnc_requirements#create")
    end

    it "routes to #update" do
      put("/dnc_requirements/1").should route_to("dnc_requirements#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dnc_requirements/1").should route_to("dnc_requirements#destroy", :id => "1")
    end

  end
end
