require "spec_helper"

describe CompanyUnitsController do
  describe "routing" do

    it "routes to #index" do
      get("/company_units").should route_to("company_units#index")
    end

    it "routes to #new" do
      get("/company_units/new").should route_to("company_units#new")
    end

    it "routes to #show" do
      get("/company_units/1").should route_to("company_units#show", :id => "1")
    end

    it "routes to #edit" do
      get("/company_units/1/edit").should route_to("company_units#edit", :id => "1")
    end

    it "routes to #create" do
      post("/company_units").should route_to("company_units#create")
    end

    it "routes to #update" do
      put("/company_units/1").should route_to("company_units#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/company_units/1").should route_to("company_units#destroy", :id => "1")
    end

  end
end
