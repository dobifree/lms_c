require "spec_helper"

describe TrainingProvidersController do
  describe "routing" do

    it "routes to #index" do
      get("/training_providers").should route_to("training_providers#index")
    end

    it "routes to #new" do
      get("/training_providers/new").should route_to("training_providers#new")
    end

    it "routes to #show" do
      get("/training_providers/1").should route_to("training_providers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/training_providers/1/edit").should route_to("training_providers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/training_providers").should route_to("training_providers#create")
    end

    it "routes to #update" do
      put("/training_providers/1").should route_to("training_providers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/training_providers/1").should route_to("training_providers#destroy", :id => "1")
    end

  end
end
