require "spec_helper"

describe PeQuestionActivitiesController do
  describe "routing" do

    it "routes to #index" do
      get("/pe_question_activities").should route_to("pe_question_activities#index")
    end

    it "routes to #new" do
      get("/pe_question_activities/new").should route_to("pe_question_activities#new")
    end

    it "routes to #show" do
      get("/pe_question_activities/1").should route_to("pe_question_activities#list", :id => "1")
    end

    it "routes to #edit" do
      get("/pe_question_activities/1/edit").should route_to("pe_question_activities#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pe_question_activities").should route_to("pe_question_activities#create")
    end

    it "routes to #update" do
      put("/pe_question_activities/1").should route_to("pe_question_activities#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pe_question_activities/1").should route_to("pe_question_activities#destroy", :id => "1")
    end

  end
end
