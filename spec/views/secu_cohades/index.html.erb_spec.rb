require 'spec_helper'

describe "secu_cohades/index" do
  before(:each) do
    assign(:secu_cohades, [
      stub_model(SecuCohade,
        :name => "Name",
        :description => "Description"
      ),
      stub_model(SecuCohade,
        :name => "Name",
        :description => "Description"
      )
    ])
  end

  it "renders a list of secu_cohades" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
