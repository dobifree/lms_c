require 'spec_helper'

describe "secu_cohades/show" do
  before(:each) do
    @secu_cohade = assign(:secu_cohade, stub_model(SecuCohade,
      :name => "Name",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Description/)
  end
end
