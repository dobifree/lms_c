require 'spec_helper'

describe "secu_cohades/new" do
  before(:each) do
    assign(:secu_cohade, stub_model(SecuCohade,
      :name => "MyString",
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new secu_cohade form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => secu_cohades_path, :method => "post" do
      assert_select "input#secu_cohade_name", :name => "secu_cohade[name]"
      assert_select "input#secu_cohade_description", :name => "secu_cohade[description]"
    end
  end
end
