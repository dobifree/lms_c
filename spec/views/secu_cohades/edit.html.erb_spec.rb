require 'spec_helper'

describe "secu_cohades/edit" do
  before(:each) do
    @secu_cohade = assign(:secu_cohade, stub_model(SecuCohade,
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit secu_cohade form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => secu_cohades_path(@secu_cohade), :method => "post" do
      assert_select "input#secu_cohade_name", :name => "secu_cohade[name]"
      assert_select "input#secu_cohade_description", :name => "secu_cohade[description]"
    end
  end
end
