require 'spec_helper'

describe "pe_processes/edit" do
  before(:each) do
    @pe_process = assign(:pe_process, stub_model(PeProcess,
      :name => "MyString",
      :period => "MyString"
    ))
  end

  it "renders the edit pe_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_processes_path(@pe_process), :method => "post" do
      assert_select "input#pe_process_name", :name => "pe_process[name]"
      assert_select "input#pe_process_period", :name => "pe_process[period]"
    end
  end
end
