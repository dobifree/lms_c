require 'spec_helper'

describe "pe_processes/show" do
  before(:each) do
    @pe_process = assign(:pe_process, stub_model(PeProcess,
      :name => "Name",
      :period => "Period"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Period/)
  end
end
