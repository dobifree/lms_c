require 'spec_helper'

describe "pe_processes/new" do
  before(:each) do
    assign(:pe_process, stub_model(PeProcess,
      :name => "MyString",
      :period => "MyString"
    ).as_new_record)
  end

  it "renders new pe_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_processes_path, :method => "post" do
      assert_select "input#pe_process_name", :name => "pe_process[name]"
      assert_select "input#pe_process_period", :name => "pe_process[period]"
    end
  end
end
