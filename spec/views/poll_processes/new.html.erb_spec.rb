require 'spec_helper'

describe "poll_processes/new" do
  before(:each) do
    assign(:poll_process, stub_model(PollProcess,
      :name => "MyString",
      :description => "MyText",
      :active => false,
      :master_poll => nil,
      :finished => false,
      :finished_by_user_id => 1
    ).as_new_record)
  end

  it "renders new poll_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => poll_processes_path, :method => "post" do
      assert_select "input#poll_process_name", :name => "poll_process[name]"
      assert_select "textarea#poll_process_description", :name => "poll_process[description]"
      assert_select "input#poll_process_active", :name => "poll_process[active]"
      assert_select "input#poll_process_master_poll", :name => "poll_process[master_poll]"
      assert_select "input#poll_process_finished", :name => "poll_process[finished]"
      assert_select "input#poll_process_finished_by_user_id", :name => "poll_process[finished_by_user_id]"
    end
  end
end
