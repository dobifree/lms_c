require 'spec_helper'

describe "poll_processes/index" do
  before(:each) do
    assign(:poll_processes, [
      stub_model(PollProcess,
        :name => "Name",
        :description => "MyText",
        :active => false,
        :master_poll => nil,
        :finished => false,
        :finished_by_user_id => 1
      ),
      stub_model(PollProcess,
        :name => "Name",
        :description => "MyText",
        :active => false,
        :master_poll => nil,
        :finished => false,
        :finished_by_user_id => 1
      )
    ])
  end

  it "renders a list of poll_processes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
