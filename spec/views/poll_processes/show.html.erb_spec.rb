require 'spec_helper'

describe "poll_processes/show" do
  before(:each) do
    @poll_process = assign(:poll_process, stub_model(PollProcess,
      :name => "Name",
      :description => "MyText",
      :active => false,
      :master_poll => nil,
      :finished => false,
      :finished_by_user_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/false/)
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(/1/)
  end
end
