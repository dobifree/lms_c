require 'spec_helper'

describe "jm_lists/show" do
  before(:each) do
    @jm_list = assign(:jm_list, stub_model(JmList,
      :name => "Name",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
  end
end
