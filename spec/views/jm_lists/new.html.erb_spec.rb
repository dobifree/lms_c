require 'spec_helper'

describe "jm_lists/new" do
  before(:each) do
    assign(:jm_list, stub_model(JmList,
      :name => "MyString",
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new jm_list form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => jm_lists_path, :method => "post" do
      assert_select "input#jm_list_name", :name => "jm_list[name]"
      assert_select "textarea#jm_list_description", :name => "jm_list[description]"
    end
  end
end
