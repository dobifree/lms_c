require 'spec_helper'

describe "pe_characteristics/index" do
  before(:each) do
    assign(:pe_characteristics, [
      stub_model(PeCharacteristic,
        :pe_process => nil,
        :characteristic => nil
      ),
      stub_model(PeCharacteristic,
        :pe_process => nil,
        :characteristic => nil
      )
    ])
  end

  it "renders a list of pe_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
