require 'spec_helper'

describe "pe_characteristics/edit" do
  before(:each) do
    @pe_characteristic = assign(:pe_characteristic, stub_model(PeCharacteristic,
      :pe_process => nil,
      :characteristic => nil
    ))
  end

  it "renders the edit pe_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_characteristics_path(@pe_characteristic), :method => "post" do
      assert_select "input#pe_characteristic_pe_process", :name => "pe_characteristic[pe_process]"
      assert_select "input#pe_characteristic_characteristic", :name => "pe_characteristic[characteristic]"
    end
  end
end
