require 'spec_helper'

describe "pe_characteristics/new" do
  before(:each) do
    assign(:pe_characteristic, stub_model(PeCharacteristic,
      :pe_process => nil,
      :characteristic => nil
    ).as_new_record)
  end

  it "renders new pe_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_characteristics_path, :method => "post" do
      assert_select "input#pe_characteristic_pe_process", :name => "pe_characteristic[pe_process]"
      assert_select "input#pe_characteristic_characteristic", :name => "pe_characteristic[characteristic]"
    end
  end
end
