require 'spec_helper'

describe "pe_characteristics/show" do
  before(:each) do
    @pe_characteristic = assign(:pe_characteristic, stub_model(PeCharacteristic,
      :pe_process => nil,
      :characteristic => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
