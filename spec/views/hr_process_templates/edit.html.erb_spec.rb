require 'spec_helper'

describe "hr_process_templates/edit" do
  before(:each) do
    @hr_process_template = assign(:hr_process_template, stub_model(HrProcessTemplate,
      :nombre => "MyString",
      :descripcion => "MyString",
      :activo => false
    ))
  end

  it "renders the edit hr_process_template form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_templates_path(@hr_process_template), :method => "post" do
      assert_select "input#hr_process_template_nombre", :name => "hr_process_template[nombre]"
      assert_select "input#hr_process_template_descripcion", :name => "hr_process_template[descripcion]"
      assert_select "input#hr_process_template_activo", :name => "hr_process_template[activo]"
    end
  end
end
