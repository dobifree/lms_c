require 'spec_helper'

describe "hr_process_templates/new" do
  before(:each) do
    assign(:hr_process_template, stub_model(HrProcessTemplate,
      :nombre => "MyString",
      :descripcion => "MyString",
      :activo => false
    ).as_new_record)
  end

  it "renders new hr_process_template form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_templates_path, :method => "post" do
      assert_select "input#hr_process_template_nombre", :name => "hr_process_template[nombre]"
      assert_select "input#hr_process_template_descripcion", :name => "hr_process_template[descripcion]"
      assert_select "input#hr_process_template_activo", :name => "hr_process_template[activo]"
    end
  end
end
