require 'spec_helper'

describe "hr_process_templates/index" do
  before(:each) do
    assign(:hr_process_templates, [
      stub_model(HrProcessTemplate,
        :nombre => "Nombre",
        :descripcion => "Descripcion",
        :activo => false
      ),
      stub_model(HrProcessTemplate,
        :nombre => "Nombre",
        :descripcion => "Descripcion",
        :activo => false
      )
    ])
  end

  it "renders a list of hr_process_templates" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "Descripcion".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
