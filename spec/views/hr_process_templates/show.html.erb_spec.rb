require 'spec_helper'

describe "hr_process_templates/show" do
  before(:each) do
    @hr_process_template = assign(:hr_process_template, stub_model(HrProcessTemplate,
      :nombre => "Nombre",
      :descripcion => "Descripcion",
      :activo => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
    rendered.should match(/Descripcion/)
    rendered.should match(/false/)
  end
end
