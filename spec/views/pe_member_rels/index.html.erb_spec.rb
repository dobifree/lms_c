require 'spec_helper'

describe "pe_member_rels/index" do
  before(:each) do
    assign(:pe_member_rels, [
      stub_model(PeMemberRel,
        :pe_process => nil,
        :finished => false,
        :valid => false,
        :pe_member_evaluator_id => 1,
        :pe_rel => nil,
        :pe_member_evaluated_id => 2
      ),
      stub_model(PeMemberRel,
        :pe_process => nil,
        :finished => false,
        :valid => false,
        :pe_member_evaluator_id => 1,
        :pe_rel => nil,
        :pe_member_evaluated_id => 2
      )
    ])
  end

  it "renders a list of pe_member_rels" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
