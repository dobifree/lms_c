require 'spec_helper'

describe "pe_member_rels/show" do
  before(:each) do
    @pe_member_rel = assign(:pe_member_rel, stub_model(PeMemberRel,
      :pe_process => nil,
      :finished => false,
      :valid => false,
      :pe_member_evaluator_id => 1,
      :pe_rel => nil,
      :pe_member_evaluated_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/1/)
    rendered.should match(//)
    rendered.should match(/2/)
  end
end
