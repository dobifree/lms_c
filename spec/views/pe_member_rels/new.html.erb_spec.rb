require 'spec_helper'

describe "pe_member_rels/new" do
  before(:each) do
    assign(:pe_member_rel, stub_model(PeMemberRel,
      :pe_process => nil,
      :finished => false,
      :valid => false,
      :pe_member_evaluator_id => 1,
      :pe_rel => nil,
      :pe_member_evaluated_id => 1
    ).as_new_record)
  end

  it "renders new pe_member_rel form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_member_rels_path, :method => "post" do
      assert_select "input#pe_member_rel_pe_process", :name => "pe_member_rel[pe_process]"
      assert_select "input#pe_member_rel_finished", :name => "pe_member_rel[finished]"
      assert_select "input#pe_member_rel_valid", :name => "pe_member_rel[valid]"
      assert_select "input#pe_member_rel_pe_member_evaluator_id", :name => "pe_member_rel[pe_member_evaluator_id]"
      assert_select "input#pe_member_rel_pe_rel", :name => "pe_member_rel[pe_rel]"
      assert_select "input#pe_member_rel_pe_member_evaluated_id", :name => "pe_member_rel[pe_member_evaluated_id]"
    end
  end
end
