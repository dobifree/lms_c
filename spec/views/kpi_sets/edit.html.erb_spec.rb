require 'spec_helper'

describe "kpi_sets/edit" do
  before(:each) do
    @kpi_set = assign(:kpi_set, stub_model(KpiSet,
      :position => 1,
      :name => "MyString",
      :kpi_dashboard => nil
    ))
  end

  it "renders the edit kpi_set form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_sets_path(@kpi_set), :method => "post" do
      assert_select "input#kpi_set_position", :name => "kpi_set[position]"
      assert_select "input#kpi_set_name", :name => "kpi_set[name]"
      assert_select "input#kpi_set_kpi_dashboard", :name => "kpi_set[kpi_dashboard]"
    end
  end
end
