require 'spec_helper'

describe "kpi_sets/show" do
  before(:each) do
    @kpi_set = assign(:kpi_set, stub_model(KpiSet,
      :position => 1,
      :name => "Name",
      :kpi_dashboard => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
