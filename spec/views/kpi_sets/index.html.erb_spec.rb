require 'spec_helper'

describe "kpi_sets/index" do
  before(:each) do
    assign(:kpi_sets, [
      stub_model(KpiSet,
        :position => 1,
        :name => "Name",
        :kpi_dashboard => nil
      ),
      stub_model(KpiSet,
        :position => 1,
        :name => "Name",
        :kpi_dashboard => nil
      )
    ])
  end

  it "renders a list of kpi_sets" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
