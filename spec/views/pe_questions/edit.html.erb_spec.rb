require 'spec_helper'

describe "pe_questions/edit" do
  before(:each) do
    @pe_question = assign(:pe_question, stub_model(PeQuestion,
      :description => "MyText",
      :weight => 1,
      :pe_evaluation => nil,
      :pe_group => nil,
      :pe_element => nil,
      :pe_question_id => 1,
      :has_comment => false
    ))
  end

  it "renders the edit pe_question form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_questions_path(@pe_question), :method => "post" do
      assert_select "textarea#pe_question_description", :name => "pe_question[description]"
      assert_select "input#pe_question_weight", :name => "pe_question[weight]"
      assert_select "input#pe_question_pe_evaluation", :name => "pe_question[pe_evaluation]"
      assert_select "input#pe_question_pe_group", :name => "pe_question[pe_group]"
      assert_select "input#pe_question_pe_element", :name => "pe_question[pe_element]"
      assert_select "input#pe_question_pe_question_id", :name => "pe_question[pe_question_id]"
      assert_select "input#pe_question_has_comment", :name => "pe_question[has_comment]"
    end
  end
end
