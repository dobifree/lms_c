require 'spec_helper'

describe "pe_questions/show" do
  before(:each) do
    @pe_question = assign(:pe_question, stub_model(PeQuestion,
      :description => "MyText",
      :weight => 1,
      :pe_evaluation => nil,
      :pe_group => nil,
      :pe_element => nil,
      :pe_question_id => 2,
      :has_comment => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/2/)
    rendered.should match(/false/)
  end
end
