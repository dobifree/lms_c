require 'spec_helper'

describe "pe_questions/index" do
  before(:each) do
    assign(:pe_questions, [
      stub_model(PeQuestion,
        :description => "MyText",
        :weight => 1,
        :pe_evaluation => nil,
        :pe_group => nil,
        :pe_element => nil,
        :pe_question_id => 2,
        :has_comment => false
      ),
      stub_model(PeQuestion,
        :description => "MyText",
        :weight => 1,
        :pe_evaluation => nil,
        :pe_group => nil,
        :pe_element => nil,
        :pe_question_id => 2,
        :has_comment => false
      )
    ])
  end

  it "renders a list of pe_questions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
