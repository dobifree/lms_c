require 'spec_helper'

describe "hr_process_dimensions/index" do
  before(:each) do
    assign(:hr_process_dimensions, [
      stub_model(HrProcessDimension,
        :nombre => "Nombre",
        :hr_process_template => nil
      ),
      stub_model(HrProcessDimension,
        :nombre => "Nombre",
        :hr_process_template => nil
      )
    ])
  end

  it "renders a list of hr_process_dimensions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
