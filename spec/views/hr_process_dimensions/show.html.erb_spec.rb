require 'spec_helper'

describe "hr_process_dimensions/show" do
  before(:each) do
    @hr_process_dimension = assign(:hr_process_dimension, stub_model(HrProcessDimension,
      :nombre => "Nombre",
      :hr_process_template => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
    rendered.should match(//)
  end
end
