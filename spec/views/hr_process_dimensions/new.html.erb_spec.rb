require 'spec_helper'

describe "hr_process_dimensions/new" do
  before(:each) do
    assign(:hr_process_dimension, stub_model(HrProcessDimension,
      :nombre => "MyString",
      :hr_process_template => nil
    ).as_new_record)
  end

  it "renders new hr_process_dimension form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_dimensions_path, :method => "post" do
      assert_select "input#hr_process_dimension_nombre", :name => "hr_process_dimension[nombre]"
      assert_select "input#hr_process_dimension_hr_process_template", :name => "hr_process_dimension[hr_process_template]"
    end
  end
end
