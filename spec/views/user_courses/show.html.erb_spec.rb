require 'spec_helper'

describe "user_courses/show" do
  before(:each) do
    @user_course = assign(:user_course, stub_model(UserCourse,
      :enrollment => nil,
      :program_course => nil,
      :rango_fechas => false,
      :limite_tiempo => false,
      :fin => "",
      :numero_oportunidad => 1,
      :iniciado => false,
      :finalizado => false,
      :aprobado => false,
      :nota => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/1.5/)
  end
end
