require 'spec_helper'

describe "user_courses/index" do
  before(:each) do
    assign(:user_courses, [
      stub_model(UserCourse,
        :enrollment => nil,
        :program_course => nil,
        :rango_fechas => false,
        :limite_tiempo => false,
        :fin => "",
        :numero_oportunidad => 1,
        :iniciado => false,
        :finalizado => false,
        :aprobado => false,
        :nota => 1.5
      ),
      stub_model(UserCourse,
        :enrollment => nil,
        :program_course => nil,
        :rango_fechas => false,
        :limite_tiempo => false,
        :fin => "",
        :numero_oportunidad => 1,
        :iniciado => false,
        :finalizado => false,
        :aprobado => false,
        :nota => 1.5
      )
    ])
  end

  it "renders a list of user_courses" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
