require 'spec_helper'

describe "user_courses/edit" do
  before(:each) do
    @user_course = assign(:user_course, stub_model(UserCourse,
      :enrollment => nil,
      :program_course => nil,
      :rango_fechas => false,
      :limite_tiempo => false,
      :fin => "",
      :numero_oportunidad => 1,
      :iniciado => false,
      :finalizado => false,
      :aprobado => false,
      :nota => 1.5
    ))
  end

  it "renders the edit user_course form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => user_courses_path(@user_course), :method => "post" do
      assert_select "input#user_course_enrollment", :name => "user_course[enrollment]"
      assert_select "input#user_course_program_course", :name => "user_course[program_course]"
      assert_select "input#user_course_rango_fechas", :name => "user_course[rango_fechas]"
      assert_select "input#user_course_limite_tiempo", :name => "user_course[limite_tiempo]"
      assert_select "input#user_course_fin", :name => "user_course[fin]"
      assert_select "input#user_course_numero_oportunidad", :name => "user_course[numero_oportunidad]"
      assert_select "input#user_course_iniciado", :name => "user_course[iniciado]"
      assert_select "input#user_course_finalizado", :name => "user_course[finalizado]"
      assert_select "input#user_course_aprobado", :name => "user_course[aprobado]"
      assert_select "input#user_course_nota", :name => "user_course[nota]"
    end
  end
end
