require 'spec_helper'

describe "liq_processes/index" do
  before(:each) do
    assign(:liq_processes, [
      stub_model(LiqProcess,
        :year => 1,
        :month => 2
      ),
      stub_model(LiqProcess,
        :year => 1,
        :month => 2
      )
    ])
  end

  it "renders a list of liq_processes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
