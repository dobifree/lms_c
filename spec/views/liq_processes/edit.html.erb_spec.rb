require 'spec_helper'

describe "liq_processes/edit" do
  before(:each) do
    @liq_process = assign(:liq_process, stub_model(LiqProcess,
      :year => 1,
      :month => 1
    ))
  end

  it "renders the edit liq_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => liq_processes_path(@liq_process), :method => "post" do
      assert_select "input#liq_process_year", :name => "liq_process[year]"
      assert_select "input#liq_process_month", :name => "liq_process[month]"
    end
  end
end
