require 'spec_helper'

describe "pe_feedback_fields/index" do
  before(:each) do
    assign(:pe_feedback_fields, [
      stub_model(PeFeedbackField,
        :pe_process => nil,
        :position => 1,
        :name => "Name",
        :simple => false,
        :compound_names => "Compound Names"
      ),
      stub_model(PeFeedbackField,
        :pe_process => nil,
        :position => 1,
        :name => "Name",
        :simple => false,
        :compound_names => "Compound Names"
      )
    ])
  end

  it "renders a list of pe_feedback_fields" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Compound Names".to_s, :count => 2
  end
end
