require 'spec_helper'

describe "pe_feedback_fields/show" do
  before(:each) do
    @pe_feedback_field = assign(:pe_feedback_field, stub_model(PeFeedbackField,
      :pe_process => nil,
      :position => 1,
      :name => "Name",
      :simple => false,
      :compound_names => "Compound Names"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/false/)
    rendered.should match(/Compound Names/)
  end
end
