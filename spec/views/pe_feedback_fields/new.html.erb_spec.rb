require 'spec_helper'

describe "pe_feedback_fields/new" do
  before(:each) do
    assign(:pe_feedback_field, stub_model(PeFeedbackField,
      :pe_process => nil,
      :position => 1,
      :name => "MyString",
      :simple => false,
      :compound_names => "MyString"
    ).as_new_record)
  end

  it "renders new pe_feedback_field form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_feedback_fields_path, :method => "post" do
      assert_select "input#pe_feedback_field_pe_process", :name => "pe_feedback_field[pe_process]"
      assert_select "input#pe_feedback_field_position", :name => "pe_feedback_field[position]"
      assert_select "input#pe_feedback_field_name", :name => "pe_feedback_field[name]"
      assert_select "input#pe_feedback_field_simple", :name => "pe_feedback_field[simple]"
      assert_select "input#pe_feedback_field_compound_names", :name => "pe_feedback_field[compound_names]"
    end
  end
end
