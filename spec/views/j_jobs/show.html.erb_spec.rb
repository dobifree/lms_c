require 'spec_helper'

describe "j_jobs/show" do
  before(:each) do
    @j_job = assign(:j_job, stub_model(JJob,
      :name => "Name",
      :mission => "Mission",
      :j_job_level => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Mission/)
    rendered.should match(//)
  end
end
