require 'spec_helper'

describe "j_jobs/new" do
  before(:each) do
    assign(:j_job, stub_model(JJob,
      :name => "MyString",
      :mission => "MyString",
      :j_job_level => nil
    ).as_new_record)
  end

  it "renders new j_job form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_jobs_path, :method => "post" do
      assert_select "input#j_job_name", :name => "j_job[name]"
      assert_select "input#j_job_mission", :name => "j_job[mission]"
      assert_select "input#j_job_j_job_level", :name => "j_job[j_job_level]"
    end
  end
end
