require 'spec_helper'

describe "j_jobs/index" do
  before(:each) do
    assign(:j_jobs, [
      stub_model(JJob,
        :name => "Name",
        :mission => "Mission",
        :j_job_level => nil
      ),
      stub_model(JJob,
        :name => "Name",
        :mission => "Mission",
        :j_job_level => nil
      )
    ])
  end

  it "renders a list of j_jobs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Mission".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
