require 'spec_helper'

describe "brc_mails/index" do
  before(:each) do
    assign(:brc_mails, [
      stub_model(BrcMail,
        :def_begin_subject => "Def Begin Subject",
        :def_begin_body => "MyText",
        :def_rej_subject => "Def Rej Subject",
        :def_rej_body => "MyText",
        :val_begin_subject => "Val Begin Subject",
        :val_begin_body => "MyText",
        :def_remind_subject => "Def Remind Subject",
        :def_remind_body => "MyText",
        :val_remind_subject => "Val Remind Subject",
        :val_remind_body => "MyText",
        :member_begin_subject => "Member Begin Subject",
        :member_begin_body => "MyText",
        :member_begin_extra1_body => "MyText"
      ),
      stub_model(BrcMail,
        :def_begin_subject => "Def Begin Subject",
        :def_begin_body => "MyText",
        :def_rej_subject => "Def Rej Subject",
        :def_rej_body => "MyText",
        :val_begin_subject => "Val Begin Subject",
        :val_begin_body => "MyText",
        :def_remind_subject => "Def Remind Subject",
        :def_remind_body => "MyText",
        :val_remind_subject => "Val Remind Subject",
        :val_remind_body => "MyText",
        :member_begin_subject => "Member Begin Subject",
        :member_begin_body => "MyText",
        :member_begin_extra1_body => "MyText"
      )
    ])
  end

  it "renders a list of brc_mails" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Def Begin Subject".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Def Rej Subject".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Val Begin Subject".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Def Remind Subject".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Val Remind Subject".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Member Begin Subject".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
