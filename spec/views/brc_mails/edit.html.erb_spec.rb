require 'spec_helper'

describe "brc_mails/edit" do
  before(:each) do
    @brc_mail = assign(:brc_mail, stub_model(BrcMail,
      :def_begin_subject => "MyString",
      :def_begin_body => "MyText",
      :def_rej_subject => "MyString",
      :def_rej_body => "MyText",
      :val_begin_subject => "MyString",
      :val_begin_body => "MyText",
      :def_remind_subject => "MyString",
      :def_remind_body => "MyText",
      :val_remind_subject => "MyString",
      :val_remind_body => "MyText",
      :member_begin_subject => "MyString",
      :member_begin_body => "MyText",
      :member_begin_extra1_body => "MyText"
    ))
  end

  it "renders the edit brc_mail form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => brc_mails_path(@brc_mail), :method => "post" do
      assert_select "input#brc_mail_def_begin_subject", :name => "brc_mail[def_begin_subject]"
      assert_select "textarea#brc_mail_def_begin_body", :name => "brc_mail[def_begin_body]"
      assert_select "input#brc_mail_def_rej_subject", :name => "brc_mail[def_rej_subject]"
      assert_select "textarea#brc_mail_def_rej_body", :name => "brc_mail[def_rej_body]"
      assert_select "input#brc_mail_val_begin_subject", :name => "brc_mail[val_begin_subject]"
      assert_select "textarea#brc_mail_val_begin_body", :name => "brc_mail[val_begin_body]"
      assert_select "input#brc_mail_def_remind_subject", :name => "brc_mail[def_remind_subject]"
      assert_select "textarea#brc_mail_def_remind_body", :name => "brc_mail[def_remind_body]"
      assert_select "input#brc_mail_val_remind_subject", :name => "brc_mail[val_remind_subject]"
      assert_select "textarea#brc_mail_val_remind_body", :name => "brc_mail[val_remind_body]"
      assert_select "input#brc_mail_member_begin_subject", :name => "brc_mail[member_begin_subject]"
      assert_select "textarea#brc_mail_member_begin_body", :name => "brc_mail[member_begin_body]"
      assert_select "textarea#brc_mail_member_begin_extra1_body", :name => "brc_mail[member_begin_extra1_body]"
    end
  end
end
