require 'spec_helper'

describe "brc_mails/show" do
  before(:each) do
    @brc_mail = assign(:brc_mail, stub_model(BrcMail,
      :def_begin_subject => "Def Begin Subject",
      :def_begin_body => "MyText",
      :def_rej_subject => "Def Rej Subject",
      :def_rej_body => "MyText",
      :val_begin_subject => "Val Begin Subject",
      :val_begin_body => "MyText",
      :def_remind_subject => "Def Remind Subject",
      :def_remind_body => "MyText",
      :val_remind_subject => "Val Remind Subject",
      :val_remind_body => "MyText",
      :member_begin_subject => "Member Begin Subject",
      :member_begin_body => "MyText",
      :member_begin_extra1_body => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Def Begin Subject/)
    rendered.should match(/MyText/)
    rendered.should match(/Def Rej Subject/)
    rendered.should match(/MyText/)
    rendered.should match(/Val Begin Subject/)
    rendered.should match(/MyText/)
    rendered.should match(/Def Remind Subject/)
    rendered.should match(/MyText/)
    rendered.should match(/Val Remind Subject/)
    rendered.should match(/MyText/)
    rendered.should match(/Member Begin Subject/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
  end
end
