require 'spec_helper'

describe "dnc_categories/edit" do
  before(:each) do
    @dnc_category = assign(:dnc_category, stub_model(DncCategory,
      :name => "MyString"
    ))
  end

  it "renders the edit dnc_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_categories_path(@dnc_category), :method => "post" do
      assert_select "input#dnc_category_name", :name => "dnc_category[name]"
    end
  end
end
