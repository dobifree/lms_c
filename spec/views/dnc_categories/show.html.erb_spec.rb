require 'spec_helper'

describe "dnc_categories/show" do
  before(:each) do
    @dnc_category = assign(:dnc_category, stub_model(DncCategory,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
