require 'spec_helper'

describe "dnc_categories/index" do
  before(:each) do
    assign(:dnc_categories, [
      stub_model(DncCategory,
        :name => "Name"
      ),
      stub_model(DncCategory,
        :name => "Name"
      )
    ])
  end

  it "renders a list of dnc_categories" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
