require 'spec_helper'

describe "dnc_categories/new" do
  before(:each) do
    assign(:dnc_category, stub_model(DncCategory,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new dnc_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_categories_path, :method => "post" do
      assert_select "input#dnc_category_name", :name => "dnc_category[name]"
    end
  end
end
