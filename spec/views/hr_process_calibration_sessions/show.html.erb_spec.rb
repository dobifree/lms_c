require 'spec_helper'

describe "hr_process_calibration_sessions/show" do
  before(:each) do
    @hr_process_calibration_session = assign(:hr_process_calibration_session, stub_model(HrProcessCalibrationSession,
      :hr_process => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
  end
end
