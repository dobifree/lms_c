require 'spec_helper'

describe "hr_process_calibration_sessions/new" do
  before(:each) do
    assign(:hr_process_calibration_session, stub_model(HrProcessCalibrationSession,
      :hr_process => nil
    ).as_new_record)
  end

  it "renders new hr_process_calibration_session form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_calibration_sessions_path, :method => "post" do
      assert_select "input#hr_process_calibration_session_hr_process", :name => "hr_process_calibration_session[hr_process]"
    end
  end
end
