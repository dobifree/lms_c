require 'spec_helper'

describe "hr_process_calibration_sessions/edit" do
  before(:each) do
    @hr_process_calibration_session = assign(:hr_process_calibration_session, stub_model(HrProcessCalibrationSession,
      :hr_process => nil
    ))
  end

  it "renders the edit hr_process_calibration_session form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_calibration_sessions_path(@hr_process_calibration_session), :method => "post" do
      assert_select "input#hr_process_calibration_session_hr_process", :name => "hr_process_calibration_session[hr_process]"
    end
  end
end
