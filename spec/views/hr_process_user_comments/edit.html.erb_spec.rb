require 'spec_helper'

describe "hr_process_user_comments/edit" do
  before(:each) do
    @hr_process_user_comment = assign(:hr_process_user_comment, stub_model(HrProcessUserComment,
      :hr_process_user => nil,
      :comentario => "MyText"
    ))
  end

  it "renders the edit hr_process_user_comment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_user_comments_path(@hr_process_user_comment), :method => "post" do
      assert_select "input#hr_process_user_comment_hr_process_user", :name => "hr_process_user_comment[hr_process_user]"
      assert_select "textarea#hr_process_user_comment_comentario", :name => "hr_process_user_comment[comentario]"
    end
  end
end
