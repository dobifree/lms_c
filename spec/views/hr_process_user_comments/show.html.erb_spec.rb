require 'spec_helper'

describe "hr_process_user_comments/show" do
  before(:each) do
    @hr_process_user_comment = assign(:hr_process_user_comment, stub_model(HrProcessUserComment,
      :hr_process_user => nil,
      :comentario => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/MyText/)
  end
end
