require 'spec_helper'

describe "hr_process_user_comments/index" do
  before(:each) do
    assign(:hr_process_user_comments, [
      stub_model(HrProcessUserComment,
        :hr_process_user => nil,
        :comentario => "MyText"
      ),
      stub_model(HrProcessUserComment,
        :hr_process_user => nil,
        :comentario => "MyText"
      )
    ])
  end

  it "renders a list of hr_process_user_comments" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
