require 'spec_helper'

describe "hr_process_evaluation_manual_qs/edit" do
  before(:each) do
    @hr_process_evaluation_manual_q = assign(:hr_process_evaluation_manual_q, stub_model(HrProcessEvaluationManualQ,
      :texto => "MyString",
      :peso => 1,
      :hr_process_evaluation => nil,
      :hr_evaluation_type_element => nil,
      :hr_process_evaluation_manual_q_id => 1,
      :hr_process_user => nil
    ))
  end

  it "renders the edit hr_process_evaluation_manual_q form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_evaluation_manual_qs_path(@hr_process_evaluation_manual_q), :method => "post" do
      assert_select "input#hr_process_evaluation_manual_q_texto", :name => "hr_process_evaluation_manual_q[texto]"
      assert_select "input#hr_process_evaluation_manual_q_peso", :name => "hr_process_evaluation_manual_q[peso]"
      assert_select "input#hr_process_evaluation_manual_q_hr_process_evaluation", :name => "hr_process_evaluation_manual_q[hr_process_evaluation]"
      assert_select "input#hr_process_evaluation_manual_q_hr_evaluation_type_element", :name => "hr_process_evaluation_manual_q[hr_evaluation_type_element]"
      assert_select "input#hr_process_evaluation_manual_q_hr_process_evaluation_manual_q_id", :name => "hr_process_evaluation_manual_q[hr_process_evaluation_manual_q_id]"
      assert_select "input#hr_process_evaluation_manual_q_hr_process_user", :name => "hr_process_evaluation_manual_q[hr_process_user]"
    end
  end
end
