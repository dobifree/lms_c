require 'spec_helper'

describe "hr_process_evaluation_manual_qs/index" do
  before(:each) do
    assign(:hr_process_evaluation_manual_qs, [
      stub_model(HrProcessEvaluationManualQ,
        :texto => "Texto",
        :peso => 1,
        :hr_process_evaluation => nil,
        :hr_evaluation_type_element => nil,
        :hr_process_evaluation_manual_q_id => 2,
        :hr_process_user => nil
      ),
      stub_model(HrProcessEvaluationManualQ,
        :texto => "Texto",
        :peso => 1,
        :hr_process_evaluation => nil,
        :hr_evaluation_type_element => nil,
        :hr_process_evaluation_manual_q_id => 2,
        :hr_process_user => nil
      )
    ])
  end

  it "renders a list of hr_process_evaluation_manual_qs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Texto".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
