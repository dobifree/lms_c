require 'spec_helper'

describe "hr_process_evaluation_manual_qs/show" do
  before(:each) do
    @hr_process_evaluation_manual_q = assign(:hr_process_evaluation_manual_q, stub_model(HrProcessEvaluationManualQ,
      :texto => "Texto",
      :peso => 1,
      :hr_process_evaluation => nil,
      :hr_evaluation_type_element => nil,
      :hr_process_evaluation_manual_q_id => 2,
      :hr_process_user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Texto/)
    rendered.should match(/1/)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/2/)
    rendered.should match(//)
  end
end
