require 'spec_helper'

describe "hr_process_evaluation_q_classifications/index" do
  before(:each) do
    assign(:hr_process_evaluation_q_classifications, [
      stub_model(HrProcessEvaluationQClassification,
        :orden => 1,
        :nombre => "MyText",
        :hr_process_evaluation => nil,
        :hr_evaluation_type_element => nil
      ),
      stub_model(HrProcessEvaluationQClassification,
        :orden => 1,
        :nombre => "MyText",
        :hr_process_evaluation => nil,
        :hr_evaluation_type_element => nil
      )
    ])
  end

  it "renders a list of hr_process_evaluation_q_classifications" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
