require 'spec_helper'

describe "hr_process_evaluation_q_classifications/show" do
  before(:each) do
    @hr_process_evaluation_q_classification = assign(:hr_process_evaluation_q_classification, stub_model(HrProcessEvaluationQClassification,
      :orden => 1,
      :nombre => "MyText",
      :hr_process_evaluation => nil,
      :hr_evaluation_type_element => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/MyText/)
    rendered.should match(//)
    rendered.should match(//)
  end
end
