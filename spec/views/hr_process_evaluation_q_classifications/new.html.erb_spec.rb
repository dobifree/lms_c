require 'spec_helper'

describe "hr_process_evaluation_q_classifications/new" do
  before(:each) do
    assign(:hr_process_evaluation_q_classification, stub_model(HrProcessEvaluationQClassification,
      :orden => 1,
      :nombre => "MyText",
      :hr_process_evaluation => nil,
      :hr_evaluation_type_element => nil
    ).as_new_record)
  end

  it "renders new hr_process_evaluation_q_classification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_evaluation_q_classifications_path, :method => "post" do
      assert_select "input#hr_process_evaluation_q_classification_orden", :name => "hr_process_evaluation_q_classification[orden]"
      assert_select "textarea#hr_process_evaluation_q_classification_nombre", :name => "hr_process_evaluation_q_classification[nombre]"
      assert_select "input#hr_process_evaluation_q_classification_hr_process_evaluation", :name => "hr_process_evaluation_q_classification[hr_process_evaluation]"
      assert_select "input#hr_process_evaluation_q_classification_hr_evaluation_type_element", :name => "hr_process_evaluation_q_classification[hr_evaluation_type_element]"
    end
  end
end
