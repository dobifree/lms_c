require 'spec_helper'

describe "ct_modules/new" do
  before(:each) do
    assign(:ct_module, stub_model(CtModule,
      :name => "MyString",
      :active => false
    ).as_new_record)
  end

  it "renders new ct_module form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => ct_modules_path, :method => "post" do
      assert_select "input#ct_module_name", :name => "ct_module[name]"
      assert_select "input#ct_module_active", :name => "ct_module[active]"
    end
  end
end
