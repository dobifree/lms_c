require 'spec_helper'

describe "ct_modules/edit" do
  before(:each) do
    @ct_module = assign(:ct_module, stub_model(CtModule,
      :name => "MyString",
      :active => false
    ))
  end

  it "renders the edit ct_module form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => ct_modules_path(@ct_module), :method => "post" do
      assert_select "input#ct_module_name", :name => "ct_module[name]"
      assert_select "input#ct_module_active", :name => "ct_module[active]"
    end
  end
end
