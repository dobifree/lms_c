require 'spec_helper'

describe "ct_modules/show" do
  before(:each) do
    @ct_module = assign(:ct_module, stub_model(CtModule,
      :name => "Name",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/false/)
  end
end
