require 'spec_helper'

describe "ct_modules/index" do
  before(:each) do
    assign(:ct_modules, [
      stub_model(CtModule,
        :name => "Name",
        :active => false
      ),
      stub_model(CtModule,
        :name => "Name",
        :active => false
      )
    ])
  end

  it "renders a list of ct_modules" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
