require 'spec_helper'

describe "training_modes/show" do
  before(:each) do
    @training_mode = assign(:training_mode, stub_model(TrainingMode,
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
  end
end
