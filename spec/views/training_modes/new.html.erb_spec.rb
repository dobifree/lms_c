require 'spec_helper'

describe "training_modes/new" do
  before(:each) do
    assign(:training_mode, stub_model(TrainingMode,
      :nombre => "MyString"
    ).as_new_record)
  end

  it "renders new training_mode form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_modes_path, :method => "post" do
      assert_select "input#training_mode_nombre", :name => "training_mode[nombre]"
    end
  end
end
