require 'spec_helper'

describe "training_modes/edit" do
  before(:each) do
    @training_mode = assign(:training_mode, stub_model(TrainingMode,
      :nombre => "MyString"
    ))
  end

  it "renders the edit training_mode form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_modes_path(@training_mode), :method => "post" do
      assert_select "input#training_mode_nombre", :name => "training_mode[nombre]"
    end
  end
end
