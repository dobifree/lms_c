require 'spec_helper'

describe "pe_boxes/new" do
  before(:each) do
    assign(:pe_box, stub_model(PeBox,
      :position => 1,
      :short_name => "MyString",
      :name => "MyString",
      :description => "MyText",
      :text_color => "MyString",
      :bg_color => "MyString",
      :soft_text_color => "MyString",
      :soft_bg_color => "MyString",
      :pe_process => nil,
      :pe_dim_group_x_id => 1,
      :pe_dim_group_y_id => 1
    ).as_new_record)
  end

  it "renders new pe_box form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_boxes_path, :method => "post" do
      assert_select "input#pe_box_position", :name => "pe_box[position]"
      assert_select "input#pe_box_short_name", :name => "pe_box[short_name]"
      assert_select "input#pe_box_name", :name => "pe_box[name]"
      assert_select "textarea#pe_box_description", :name => "pe_box[description]"
      assert_select "input#pe_box_text_color", :name => "pe_box[text_color]"
      assert_select "input#pe_box_bg_color", :name => "pe_box[bg_color]"
      assert_select "input#pe_box_soft_text_color", :name => "pe_box[soft_text_color]"
      assert_select "input#pe_box_soft_bg_color", :name => "pe_box[soft_bg_color]"
      assert_select "input#pe_box_pe_process", :name => "pe_box[pe_process]"
      assert_select "input#pe_box_pe_dim_group_x_id", :name => "pe_box[pe_dim_group_x_id]"
      assert_select "input#pe_box_pe_dim_group_y_id", :name => "pe_box[pe_dim_group_y_id]"
    end
  end
end
