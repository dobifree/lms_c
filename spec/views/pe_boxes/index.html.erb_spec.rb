require 'spec_helper'

describe "pe_boxes/index" do
  before(:each) do
    assign(:pe_boxes, [
      stub_model(PeBox,
        :position => 1,
        :short_name => "Short Name",
        :name => "Name",
        :description => "MyText",
        :text_color => "Text Color",
        :bg_color => "Bg Color",
        :soft_text_color => "Soft Text Color",
        :soft_bg_color => "Soft Bg Color",
        :pe_process => nil,
        :pe_dim_group_x_id => 2,
        :pe_dim_group_y_id => 3
      ),
      stub_model(PeBox,
        :position => 1,
        :short_name => "Short Name",
        :name => "Name",
        :description => "MyText",
        :text_color => "Text Color",
        :bg_color => "Bg Color",
        :soft_text_color => "Soft Text Color",
        :soft_bg_color => "Soft Bg Color",
        :pe_process => nil,
        :pe_dim_group_x_id => 2,
        :pe_dim_group_y_id => 3
      )
    ])
  end

  it "renders a list of pe_boxes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Short Name".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Text Color".to_s, :count => 2
    assert_select "tr>td", :text => "Bg Color".to_s, :count => 2
    assert_select "tr>td", :text => "Soft Text Color".to_s, :count => 2
    assert_select "tr>td", :text => "Soft Bg Color".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
