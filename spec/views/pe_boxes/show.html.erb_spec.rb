require 'spec_helper'

describe "pe_boxes/show" do
  before(:each) do
    @pe_box = assign(:pe_box, stub_model(PeBox,
      :position => 1,
      :short_name => "Short Name",
      :name => "Name",
      :description => "MyText",
      :text_color => "Text Color",
      :bg_color => "Bg Color",
      :soft_text_color => "Soft Text Color",
      :soft_bg_color => "Soft Bg Color",
      :pe_process => nil,
      :pe_dim_group_x_id => 2,
      :pe_dim_group_y_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Short Name/)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/Text Color/)
    rendered.should match(/Bg Color/)
    rendered.should match(/Soft Text Color/)
    rendered.should match(/Soft Bg Color/)
    rendered.should match(//)
    rendered.should match(/2/)
    rendered.should match(/3/)
  end
end
