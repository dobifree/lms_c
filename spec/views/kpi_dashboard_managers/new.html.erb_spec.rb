require 'spec_helper'

describe "kpi_dashboard_managers/new" do
  before(:each) do
    assign(:kpi_dashboard_manager, stub_model(KpiDashboardManager,
      :kpi_dashboard => nil,
      :user => nil
    ).as_new_record)
  end

  it "renders new kpi_dashboard_manager form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_dashboard_managers_path, :method => "post" do
      assert_select "input#kpi_dashboard_manager_kpi_dashboard", :name => "kpi_dashboard_manager[kpi_dashboard]"
      assert_select "input#kpi_dashboard_manager_user", :name => "kpi_dashboard_manager[user]"
    end
  end
end
