require 'spec_helper'

describe "kpi_dashboard_managers/show" do
  before(:each) do
    @kpi_dashboard_manager = assign(:kpi_dashboard_manager, stub_model(KpiDashboardManager,
      :kpi_dashboard => nil,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
