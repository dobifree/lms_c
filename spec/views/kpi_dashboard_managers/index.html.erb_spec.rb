require 'spec_helper'

describe "kpi_dashboard_managers/index" do
  before(:each) do
    assign(:kpi_dashboard_managers, [
      stub_model(KpiDashboardManager,
        :kpi_dashboard => nil,
        :user => nil
      ),
      stub_model(KpiDashboardManager,
        :kpi_dashboard => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of kpi_dashboard_managers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
