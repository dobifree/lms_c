require 'spec_helper'

describe "kpi_dashboard_managers/edit" do
  before(:each) do
    @kpi_dashboard_manager = assign(:kpi_dashboard_manager, stub_model(KpiDashboardManager,
      :kpi_dashboard => nil,
      :user => nil
    ))
  end

  it "renders the edit kpi_dashboard_manager form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_dashboard_managers_path(@kpi_dashboard_manager), :method => "post" do
      assert_select "input#kpi_dashboard_manager_kpi_dashboard", :name => "kpi_dashboard_manager[kpi_dashboard]"
      assert_select "input#kpi_dashboard_manager_user", :name => "kpi_dashboard_manager[user]"
    end
  end
end
