require 'spec_helper'

describe "sel_permissions/new" do
  before(:each) do
    assign(:sel_permission, stub_model(SelPermission).as_new_record)
  end

  it "renders new sel_permission form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_permissions_path, :method => "post" do
    end
  end
end
