require 'spec_helper'

describe "sel_permissions/edit" do
  before(:each) do
    @sel_permission = assign(:sel_permission, stub_model(SelPermission))
  end

  it "renders the edit sel_permission form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_permissions_path(@sel_permission), :method => "post" do
    end
  end
end
