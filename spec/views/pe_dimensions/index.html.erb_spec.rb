require 'spec_helper'

describe "pe_dimensions/index" do
  before(:each) do
    assign(:pe_dimensions, [
      stub_model(PeDimension,
        :dimension => 1,
        :name => "Name",
        :pe_process => nil
      ),
      stub_model(PeDimension,
        :dimension => 1,
        :name => "Name",
        :pe_process => nil
      )
    ])
  end

  it "renders a list of pe_dimensions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
