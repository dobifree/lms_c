require 'spec_helper'

describe "pe_dimensions/new" do
  before(:each) do
    assign(:pe_dimension, stub_model(PeDimension,
      :dimension => 1,
      :name => "MyString",
      :pe_process => nil
    ).as_new_record)
  end

  it "renders new pe_dimension form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_dimensions_path, :method => "post" do
      assert_select "input#pe_dimension_dimension", :name => "pe_dimension[dimension]"
      assert_select "input#pe_dimension_name", :name => "pe_dimension[name]"
      assert_select "input#pe_dimension_pe_process", :name => "pe_dimension[pe_process]"
    end
  end
end
