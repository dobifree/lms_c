require 'spec_helper'

describe "kpi_set_guests/new" do
  before(:each) do
    assign(:kpi_set_guest, stub_model(KpiSetGuest,
      :kpi_set => nil,
      :user => nil
    ).as_new_record)
  end

  it "renders new kpi_set_guest form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_set_guests_path, :method => "post" do
      assert_select "input#kpi_set_guest_kpi_set", :name => "kpi_set_guest[kpi_set]"
      assert_select "input#kpi_set_guest_user", :name => "kpi_set_guest[user]"
    end
  end
end
