require 'spec_helper'

describe "kpi_set_guests/index" do
  before(:each) do
    assign(:kpi_set_guests, [
      stub_model(KpiSetGuest,
        :kpi_set => nil,
        :user => nil
      ),
      stub_model(KpiSetGuest,
        :kpi_set => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of kpi_set_guests" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
