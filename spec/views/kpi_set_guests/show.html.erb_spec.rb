require 'spec_helper'

describe "kpi_set_guests/show" do
  before(:each) do
    @kpi_set_guest = assign(:kpi_set_guest, stub_model(KpiSetGuest,
      :kpi_set => nil,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
