require 'spec_helper'

describe "um_custom_reports/edit" do
  before(:each) do
    @um_custom_report = assign(:um_custom_report, stub_model(UmCustomReport,
      :name => "MyString"
    ))
  end

  it "renders the edit um_custom_report form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => um_custom_reports_path(@um_custom_report), :method => "post" do
      assert_select "input#um_custom_report_name", :name => "um_custom_report[name]"
    end
  end
end
