require 'spec_helper'

describe "um_custom_reports/show" do
  before(:each) do
    @um_custom_report = assign(:um_custom_report, stub_model(UmCustomReport,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
