require 'spec_helper'

describe "um_custom_reports/new" do
  before(:each) do
    assign(:um_custom_report, stub_model(UmCustomReport,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new um_custom_report form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => um_custom_reports_path, :method => "post" do
      assert_select "input#um_custom_report_name", :name => "um_custom_report[name]"
    end
  end
end
