require 'spec_helper'

describe "um_custom_reports/index" do
  before(:each) do
    assign(:um_custom_reports, [
      stub_model(UmCustomReport,
        :name => "Name"
      ),
      stub_model(UmCustomReport,
        :name => "Name"
      )
    ])
  end

  it "renders a list of um_custom_reports" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
