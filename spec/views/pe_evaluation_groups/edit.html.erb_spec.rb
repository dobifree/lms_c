require 'spec_helper'

describe "pe_evaluation_groups/edit" do
  before(:each) do
    @pe_evaluation_group = assign(:pe_evaluation_group, stub_model(PeEvaluationGroup,
      :position => 1,
      :name => "MyString",
      :pe_process => nil
    ))
  end

  it "renders the edit pe_evaluation_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_evaluation_groups_path(@pe_evaluation_group), :method => "post" do
      assert_select "input#pe_evaluation_group_position", :name => "pe_evaluation_group[position]"
      assert_select "input#pe_evaluation_group_name", :name => "pe_evaluation_group[name]"
      assert_select "input#pe_evaluation_group_pe_process", :name => "pe_evaluation_group[pe_process]"
    end
  end
end
