require 'spec_helper'

describe "pe_evaluation_groups/index" do
  before(:each) do
    assign(:pe_evaluation_groups, [
      stub_model(PeEvaluationGroup,
        :position => 1,
        :name => "Name",
        :pe_process => nil
      ),
      stub_model(PeEvaluationGroup,
        :position => 1,
        :name => "Name",
        :pe_process => nil
      )
    ])
  end

  it "renders a list of pe_evaluation_groups" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
