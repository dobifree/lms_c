require 'spec_helper'

describe "pe_evaluation_groups/show" do
  before(:each) do
    @pe_evaluation_group = assign(:pe_evaluation_group, stub_model(PeEvaluationGroup,
      :position => 1,
      :name => "Name",
      :pe_process => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
