require 'spec_helper'

describe "pe_evaluation_groups/new" do
  before(:each) do
    assign(:pe_evaluation_group, stub_model(PeEvaluationGroup,
      :position => 1,
      :name => "MyString",
      :pe_process => nil
    ).as_new_record)
  end

  it "renders new pe_evaluation_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_evaluation_groups_path, :method => "post" do
      assert_select "input#pe_evaluation_group_position", :name => "pe_evaluation_group[position]"
      assert_select "input#pe_evaluation_group_name", :name => "pe_evaluation_group[name]"
      assert_select "input#pe_evaluation_group_pe_process", :name => "pe_evaluation_group[pe_process]"
    end
  end
end
