require 'spec_helper'

describe "pe_member_rel_shared_comments/index" do
  before(:each) do
    assign(:pe_member_rel_shared_comments, [
      stub_model(PeMemberRelSharedComment,
        :pe_member_rel => nil,
        :comment => "MyText"
      ),
      stub_model(PeMemberRelSharedComment,
        :pe_member_rel => nil,
        :comment => "MyText"
      )
    ])
  end

  it "renders a list of pe_member_rel_shared_comments" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
