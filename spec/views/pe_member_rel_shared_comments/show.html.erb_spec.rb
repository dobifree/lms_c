require 'spec_helper'

describe "pe_member_rel_shared_comments/show" do
  before(:each) do
    @pe_member_rel_shared_comment = assign(:pe_member_rel_shared_comment, stub_model(PeMemberRelSharedComment,
      :pe_member_rel => nil,
      :comment => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/MyText/)
  end
end
