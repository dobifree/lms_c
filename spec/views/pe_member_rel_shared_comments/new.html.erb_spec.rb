require 'spec_helper'

describe "pe_member_rel_shared_comments/new" do
  before(:each) do
    assign(:pe_member_rel_shared_comment, stub_model(PeMemberRelSharedComment,
      :pe_member_rel => nil,
      :comment => "MyText"
    ).as_new_record)
  end

  it "renders new pe_member_rel_shared_comment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_member_rel_shared_comments_path, :method => "post" do
      assert_select "input#pe_member_rel_shared_comment_pe_member_rel", :name => "pe_member_rel_shared_comment[pe_member_rel]"
      assert_select "textarea#pe_member_rel_shared_comment_comment", :name => "pe_member_rel_shared_comment[comment]"
    end
  end
end
