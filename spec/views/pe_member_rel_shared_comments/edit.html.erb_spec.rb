require 'spec_helper'

describe "pe_member_rel_shared_comments/edit" do
  before(:each) do
    @pe_member_rel_shared_comment = assign(:pe_member_rel_shared_comment, stub_model(PeMemberRelSharedComment,
      :pe_member_rel => nil,
      :comment => "MyText"
    ))
  end

  it "renders the edit pe_member_rel_shared_comment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_member_rel_shared_comments_path(@pe_member_rel_shared_comment), :method => "post" do
      assert_select "input#pe_member_rel_shared_comment_pe_member_rel", :name => "pe_member_rel_shared_comment[pe_member_rel]"
      assert_select "textarea#pe_member_rel_shared_comment_comment", :name => "pe_member_rel_shared_comment[comment]"
    end
  end
end
