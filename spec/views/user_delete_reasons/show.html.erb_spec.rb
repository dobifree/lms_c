require 'spec_helper'

describe "user_delete_reasons/show" do
  before(:each) do
    @user_delete_reason = assign(:user_delete_reason, stub_model(UserDeleteReason,
      :position => 1,
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Description/)
  end
end
