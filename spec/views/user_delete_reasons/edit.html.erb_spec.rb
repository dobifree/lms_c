require 'spec_helper'

describe "user_delete_reasons/edit" do
  before(:each) do
    @user_delete_reason = assign(:user_delete_reason, stub_model(UserDeleteReason,
      :position => 1,
      :description => "MyString"
    ))
  end

  it "renders the edit user_delete_reason form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => user_delete_reasons_path(@user_delete_reason), :method => "post" do
      assert_select "input#user_delete_reason_position", :name => "user_delete_reason[position]"
      assert_select "input#user_delete_reason_description", :name => "user_delete_reason[description]"
    end
  end
end
