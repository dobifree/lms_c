require 'spec_helper'

describe "pe_feedback_accepted_fields/index" do
  before(:each) do
    assign(:pe_feedback_accepted_fields, [
      stub_model(PeFeedbackAcceptedField,
        :pe_process => nil,
        :position => 1,
        :name => "Name",
        :simple => false,
        :field_type => 2,
        :required => false
      ),
      stub_model(PeFeedbackAcceptedField,
        :pe_process => nil,
        :position => 1,
        :name => "Name",
        :simple => false,
        :field_type => 2,
        :required => false
      )
    ])
  end

  it "renders a list of pe_feedback_accepted_fields" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
