require 'spec_helper'

describe "pe_feedback_accepted_fields/show" do
  before(:each) do
    @pe_feedback_accepted_field = assign(:pe_feedback_accepted_field, stub_model(PeFeedbackAcceptedField,
      :pe_process => nil,
      :position => 1,
      :name => "Name",
      :simple => false,
      :field_type => 2,
      :required => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/false/)
    rendered.should match(/2/)
    rendered.should match(/false/)
  end
end
