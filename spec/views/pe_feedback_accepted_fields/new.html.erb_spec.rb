require 'spec_helper'

describe "pe_feedback_accepted_fields/new" do
  before(:each) do
    assign(:pe_feedback_accepted_field, stub_model(PeFeedbackAcceptedField,
      :pe_process => nil,
      :position => 1,
      :name => "MyString",
      :simple => false,
      :field_type => 1,
      :required => false
    ).as_new_record)
  end

  it "renders new pe_feedback_accepted_field form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_feedback_accepted_fields_path, :method => "post" do
      assert_select "input#pe_feedback_accepted_field_pe_process", :name => "pe_feedback_accepted_field[pe_process]"
      assert_select "input#pe_feedback_accepted_field_position", :name => "pe_feedback_accepted_field[position]"
      assert_select "input#pe_feedback_accepted_field_name", :name => "pe_feedback_accepted_field[name]"
      assert_select "input#pe_feedback_accepted_field_simple", :name => "pe_feedback_accepted_field[simple]"
      assert_select "input#pe_feedback_accepted_field_field_type", :name => "pe_feedback_accepted_field[field_type]"
      assert_select "input#pe_feedback_accepted_field_required", :name => "pe_feedback_accepted_field[required]"
    end
  end
end
