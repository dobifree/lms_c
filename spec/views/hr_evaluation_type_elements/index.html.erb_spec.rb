require 'spec_helper'

describe "hr_evaluation_type_elements/index" do
  before(:each) do
    assign(:hr_evaluation_type_elements, [
      stub_model(HrEvaluationTypeElement,
        :nombre => "Nombre",
        :nivel => 1,
        :valor_minimo => 1.5,
        :valor_maximo => 1.5,
        :formula_promedio => false,
        :peso_minimo => 2,
        :peso_maximo => 3,
        :suma_total_pesos => 4,
        :plan_accion => false,
        :porcentaje_logro => false,
        :indicador_logro => false,
        :hr_evaluation_type => nil
      ),
      stub_model(HrEvaluationTypeElement,
        :nombre => "Nombre",
        :nivel => 1,
        :valor_minimo => 1.5,
        :valor_maximo => 1.5,
        :formula_promedio => false,
        :peso_minimo => 2,
        :peso_maximo => 3,
        :suma_total_pesos => 4,
        :plan_accion => false,
        :porcentaje_logro => false,
        :indicador_logro => false,
        :hr_evaluation_type => nil
      )
    ])
  end

  it "renders a list of hr_evaluation_type_elements" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
