require 'spec_helper'

describe "hr_evaluation_type_elements/new" do
  before(:each) do
    assign(:hr_evaluation_type_element, stub_model(HrEvaluationTypeElement,
      :nombre => "MyString",
      :nivel => 1,
      :valor_minimo => 1.5,
      :valor_maximo => 1.5,
      :formula_promedio => false,
      :peso_minimo => 1,
      :peso_maximo => 1,
      :suma_total_pesos => 1,
      :plan_accion => false,
      :porcentaje_logro => false,
      :indicador_logro => false,
      :hr_evaluation_type => nil
    ).as_new_record)
  end

  it "renders new hr_evaluation_type_element form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_evaluation_type_elements_path, :method => "post" do
      assert_select "input#hr_evaluation_type_element_nombre", :name => "hr_evaluation_type_element[nombre]"
      assert_select "input#hr_evaluation_type_element_nivel", :name => "hr_evaluation_type_element[nivel]"
      assert_select "input#hr_evaluation_type_element_valor_minimo", :name => "hr_evaluation_type_element[valor_minimo]"
      assert_select "input#hr_evaluation_type_element_valor_maximo", :name => "hr_evaluation_type_element[valor_maximo]"
      assert_select "input#hr_evaluation_type_element_formula_promedio", :name => "hr_evaluation_type_element[formula_promedio]"
      assert_select "input#hr_evaluation_type_element_peso_minimo", :name => "hr_evaluation_type_element[peso_minimo]"
      assert_select "input#hr_evaluation_type_element_peso_maximo", :name => "hr_evaluation_type_element[peso_maximo]"
      assert_select "input#hr_evaluation_type_element_suma_total_pesos", :name => "hr_evaluation_type_element[suma_total_pesos]"
      assert_select "input#hr_evaluation_type_element_plan_accion", :name => "hr_evaluation_type_element[plan_accion]"
      assert_select "input#hr_evaluation_type_element_porcentaje_logro", :name => "hr_evaluation_type_element[porcentaje_logro]"
      assert_select "input#hr_evaluation_type_element_indicador_logro", :name => "hr_evaluation_type_element[indicador_logro]"
      assert_select "input#hr_evaluation_type_element_hr_evaluation_type", :name => "hr_evaluation_type_element[hr_evaluation_type]"
    end
  end
end
