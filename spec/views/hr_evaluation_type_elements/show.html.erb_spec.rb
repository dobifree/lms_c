require 'spec_helper'

describe "hr_evaluation_type_elements/show" do
  before(:each) do
    @hr_evaluation_type_element = assign(:hr_evaluation_type_element, stub_model(HrEvaluationTypeElement,
      :nombre => "Nombre",
      :nivel => 1,
      :valor_minimo => 1.5,
      :valor_maximo => 1.5,
      :formula_promedio => false,
      :peso_minimo => 2,
      :peso_maximo => 3,
      :suma_total_pesos => 4,
      :plan_accion => false,
      :porcentaje_logro => false,
      :indicador_logro => false,
      :hr_evaluation_type => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
    rendered.should match(/1/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/false/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(//)
  end
end
