require 'spec_helper'

describe "pe_group2s/index" do
  before(:each) do
    assign(:pe_group2s, [
      stub_model(PeGroup2,
        :name => "Name",
        :pe_processes => nil
      ),
      stub_model(PeGroup2,
        :name => "Name",
        :pe_processes => nil
      )
    ])
  end

  it "renders a list of pe_group2s" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
