require 'spec_helper'

describe "pe_group2s/edit" do
  before(:each) do
    @pe_group2 = assign(:pe_group2, stub_model(PeGroup2,
      :name => "MyString",
      :pe_processes => nil
    ))
  end

  it "renders the edit pe_group2 form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_group2s_path(@pe_group2), :method => "post" do
      assert_select "input#pe_group2_name", :name => "pe_group2[name]"
      assert_select "input#pe_group2_pe_processes", :name => "pe_group2[pe_processes]"
    end
  end
end
