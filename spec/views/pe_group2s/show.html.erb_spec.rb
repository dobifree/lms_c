require 'spec_helper'

describe "pe_group2s/show" do
  before(:each) do
    @pe_group2 = assign(:pe_group2, stub_model(PeGroup2,
      :name => "Name",
      :pe_processes => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
