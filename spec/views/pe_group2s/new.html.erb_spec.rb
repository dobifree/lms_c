require 'spec_helper'

describe "pe_group2s/new" do
  before(:each) do
    assign(:pe_group2, stub_model(PeGroup2,
      :name => "MyString",
      :pe_processes => nil
    ).as_new_record)
  end

  it "renders new pe_group2 form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_group2s_path, :method => "post" do
      assert_select "input#pe_group2_name", :name => "pe_group2[name]"
      assert_select "input#pe_group2_pe_processes", :name => "pe_group2[pe_processes]"
    end
  end
end
