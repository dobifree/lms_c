require 'spec_helper'

describe "pe_reporters/show" do
  before(:each) do
    @pe_reporter = assign(:pe_reporter, stub_model(PeReporter,
      :pe_process => nil,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
