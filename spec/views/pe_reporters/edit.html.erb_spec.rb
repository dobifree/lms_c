require 'spec_helper'

describe "pe_reporters/edit" do
  before(:each) do
    @pe_reporter = assign(:pe_reporter, stub_model(PeReporter,
      :pe_process => nil,
      :user => nil
    ))
  end

  it "renders the edit pe_reporter form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_reporters_path(@pe_reporter), :method => "post" do
      assert_select "input#pe_reporter_pe_process", :name => "pe_reporter[pe_process]"
      assert_select "input#pe_reporter_user", :name => "pe_reporter[user]"
    end
  end
end
