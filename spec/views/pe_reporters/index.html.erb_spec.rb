require 'spec_helper'

describe "pe_reporters/index" do
  before(:each) do
    assign(:pe_reporters, [
      stub_model(PeReporter,
        :pe_process => nil,
        :user => nil
      ),
      stub_model(PeReporter,
        :pe_process => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of pe_reporters" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
