require 'spec_helper'

describe "dnc_process_periods/new" do
  before(:each) do
    assign(:dnc_process_period, stub_model(DncProcessPeriod,
      :name => "MyString",
      :dnc_process => nil
    ).as_new_record)
  end

  it "renders new dnc_process_period form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_process_periods_path, :method => "post" do
      assert_select "input#dnc_process_period_name", :name => "dnc_process_period[name]"
      assert_select "input#dnc_process_period_dnc_process", :name => "dnc_process_period[dnc_process]"
    end
  end
end
