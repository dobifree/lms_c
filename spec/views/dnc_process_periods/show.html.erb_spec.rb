require 'spec_helper'

describe "dnc_process_periods/show" do
  before(:each) do
    @dnc_process_period = assign(:dnc_process_period, stub_model(DncProcessPeriod,
      :name => "Name",
      :dnc_process => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
