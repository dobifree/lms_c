require 'spec_helper'

describe "dnc_process_periods/index" do
  before(:each) do
    assign(:dnc_process_periods, [
      stub_model(DncProcessPeriod,
        :name => "Name",
        :dnc_process => nil
      ),
      stub_model(DncProcessPeriod,
        :name => "Name",
        :dnc_process => nil
      )
    ])
  end

  it "renders a list of dnc_process_periods" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
