require 'spec_helper'

describe "pe_feedback_field_list_items/show" do
  before(:each) do
    @pe_feedback_field_list_item = assign(:pe_feedback_field_list_item, stub_model(PeFeedbackFieldListItem,
      :description => "Description",
      :pe_feedback_field_list => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Description/)
    rendered.should match(//)
  end
end
