require 'spec_helper'

describe "pe_feedback_field_list_items/index" do
  before(:each) do
    assign(:pe_feedback_field_list_items, [
      stub_model(PeFeedbackFieldListItem,
        :description => "Description",
        :pe_feedback_field_list => nil
      ),
      stub_model(PeFeedbackFieldListItem,
        :description => "Description",
        :pe_feedback_field_list => nil
      )
    ])
  end

  it "renders a list of pe_feedback_field_list_items" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
