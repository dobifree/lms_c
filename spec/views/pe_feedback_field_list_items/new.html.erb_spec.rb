require 'spec_helper'

describe "pe_feedback_field_list_items/new" do
  before(:each) do
    assign(:pe_feedback_field_list_item, stub_model(PeFeedbackFieldListItem,
      :description => "MyString",
      :pe_feedback_field_list => nil
    ).as_new_record)
  end

  it "renders new pe_feedback_field_list_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_feedback_field_list_items_path, :method => "post" do
      assert_select "input#pe_feedback_field_list_item_description", :name => "pe_feedback_field_list_item[description]"
      assert_select "input#pe_feedback_field_list_item_pe_feedback_field_list", :name => "pe_feedback_field_list_item[pe_feedback_field_list]"
    end
  end
end
