require 'spec_helper'

describe "training_impact_categories/edit" do
  before(:each) do
    @training_impact_category = assign(:training_impact_category, stub_model(TrainingImpactCategory,
      :nombre => "MyString"
    ))
  end

  it "renders the edit training_impact_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_impact_categories_path(@training_impact_category), :method => "post" do
      assert_select "input#training_impact_category_nombre", :name => "training_impact_category[nombre]"
    end
  end
end
