require 'spec_helper'

describe "training_impact_categories/new" do
  before(:each) do
    assign(:training_impact_category, stub_model(TrainingImpactCategory,
      :nombre => "MyString"
    ).as_new_record)
  end

  it "renders new training_impact_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_impact_categories_path, :method => "post" do
      assert_select "input#training_impact_category_nombre", :name => "training_impact_category[nombre]"
    end
  end
end
