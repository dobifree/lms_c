require 'spec_helper'

describe "training_impact_categories/show" do
  before(:each) do
    @training_impact_category = assign(:training_impact_category, stub_model(TrainingImpactCategory,
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
  end
end
