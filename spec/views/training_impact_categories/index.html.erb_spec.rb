require 'spec_helper'

describe "training_impact_categories/index" do
  before(:each) do
    assign(:training_impact_categories, [
      stub_model(TrainingImpactCategory,
        :nombre => "Nombre"
      ),
      stub_model(TrainingImpactCategory,
        :nombre => "Nombre"
      )
    ])
  end

  it "renders a list of training_impact_categories" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
  end
end
