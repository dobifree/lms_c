require 'spec_helper'

describe "pe_question_comments/show" do
  before(:each) do
    @pe_question_comment = assign(:pe_question_comment, stub_model(PeQuestionComment,
      :pe_question => nil,
      :pe_member_rel => nil,
      :comment => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/MyText/)
  end
end
