require 'spec_helper'

describe "pe_question_comments/new" do
  before(:each) do
    assign(:pe_question_comment, stub_model(PeQuestionComment,
      :pe_question => nil,
      :pe_member_rel => nil,
      :comment => "MyText"
    ).as_new_record)
  end

  it "renders new pe_question_comment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_question_comments_path, :method => "post" do
      assert_select "input#pe_question_comment_pe_question", :name => "pe_question_comment[pe_question]"
      assert_select "input#pe_question_comment_pe_member_rel", :name => "pe_question_comment[pe_member_rel]"
      assert_select "textarea#pe_question_comment_comment", :name => "pe_question_comment[comment]"
    end
  end
end
