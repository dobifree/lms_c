require 'spec_helper'

describe "courses/edit" do
  before(:each) do
    @course = assign(:course, stub_model(Course,
      :codigo => "MyString",
      :nombre => "MyString",
      :descripcion => "MyString"
    ))
  end

  it "renders the edit course form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => courses_path(@course), :method => "post" do
      assert_select "input#course_codigo", :name => "course[codigo]"
      assert_select "input#course_nombre", :name => "course[nombre]"
      assert_select "input#course_descripcion", :name => "course[descripcion]"
    end
  end
end
