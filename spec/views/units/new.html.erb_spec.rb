require 'spec_helper'

describe "units/new" do
  before(:each) do
    assign(:unit, stub_model(Unit,
      :numero => 1,
      :nombre => "MyString",
      :descripcion => "MyString",
      :course => nil
    ).as_new_record)
  end

  it "renders new unit form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => units_path, :method => "post" do
      assert_select "input#unit_numero", :name => "unit[numero]"
      assert_select "input#unit_nombre", :name => "unit[nombre]"
      assert_select "input#unit_descripcion", :name => "unit[descripcion]"
      assert_select "input#unit_course", :name => "unit[course]"
    end
  end
end
