require 'spec_helper'

describe "units/index" do
  before(:each) do
    assign(:units, [
      stub_model(Unit,
        :numero => 1,
        :nombre => "Nombre",
        :descripcion => "Descripcion",
        :course => nil
      ),
      stub_model(Unit,
        :numero => 1,
        :nombre => "Nombre",
        :descripcion => "Descripcion",
        :course => nil
      )
    ])
  end

  it "renders a list of units" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "Descripcion".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
