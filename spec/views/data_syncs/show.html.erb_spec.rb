require 'spec_helper'

describe "data_syncs/show" do
  before(:each) do
    @data_sync = assign(:data_sync, stub_model(DataSync,
      :description => "Description",
      :completed => false,
      :sync_errors => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Description/)
    rendered.should match(/false/)
    rendered.should match(/MyText/)
  end
end
