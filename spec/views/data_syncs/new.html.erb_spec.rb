require 'spec_helper'

describe "data_syncs/new" do
  before(:each) do
    assign(:data_sync, stub_model(DataSync,
      :description => "MyString",
      :completed => false,
      :sync_errors => "MyText"
    ).as_new_record)
  end

  it "renders new data_sync form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => data_syncs_path, :method => "post" do
      assert_select "input#data_sync_description", :name => "data_sync[description]"
      assert_select "input#data_sync_completed", :name => "data_sync[completed]"
      assert_select "textarea#data_sync_sync_errors", :name => "data_sync[sync_errors]"
    end
  end
end
