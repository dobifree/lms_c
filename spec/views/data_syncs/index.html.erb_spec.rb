require 'spec_helper'

describe "data_syncs/index" do
  before(:each) do
    assign(:data_syncs, [
      stub_model(DataSync,
        :description => "Description",
        :completed => false,
        :sync_errors => "MyText"
      ),
      stub_model(DataSync,
        :description => "Description",
        :completed => false,
        :sync_errors => "MyText"
      )
    ])
  end

  it "renders a list of data_syncs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
