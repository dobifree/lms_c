require 'spec_helper'

describe "data_syncs/edit" do
  before(:each) do
    @data_sync = assign(:data_sync, stub_model(DataSync,
      :description => "MyString",
      :completed => false,
      :sync_errors => "MyText"
    ))
  end

  it "renders the edit data_sync form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => data_syncs_path(@data_sync), :method => "post" do
      assert_select "input#data_sync_description", :name => "data_sync[description]"
      assert_select "input#data_sync_completed", :name => "data_sync[completed]"
      assert_select "textarea#data_sync_sync_errors", :name => "data_sync[sync_errors]"
    end
  end
end
