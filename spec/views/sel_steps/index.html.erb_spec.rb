require 'spec_helper'

describe "sel_steps/index" do
  before(:each) do
    assign(:sel_steps, [
      stub_model(SelStep,
        :sel_template => nil,
        :name => "Name",
        :description => "MyText",
        :position => 1,
        :step_type => 2
      ),
      stub_model(SelStep,
        :sel_template => nil,
        :name => "Name",
        :description => "MyText",
        :position => 1,
        :step_type => 2
      )
    ])
  end

  it "renders a list of sel_steps" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
