require 'spec_helper'

describe "sel_steps/show" do
  before(:each) do
    @sel_step = assign(:sel_step, stub_model(SelStep,
      :sel_template => nil,
      :name => "Name",
      :description => "MyText",
      :position => 1,
      :step_type => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(/2/)
  end
end
