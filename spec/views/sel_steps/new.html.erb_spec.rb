require 'spec_helper'

describe "sel_steps/new" do
  before(:each) do
    assign(:sel_step, stub_model(SelStep,
      :sel_template => nil,
      :name => "MyString",
      :description => "MyText",
      :position => 1,
      :step_type => 1
    ).as_new_record)
  end

  it "renders new sel_step form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_steps_path, :method => "post" do
      assert_select "input#sel_step_sel_template", :name => "sel_step[sel_template]"
      assert_select "input#sel_step_name", :name => "sel_step[name]"
      assert_select "textarea#sel_step_description", :name => "sel_step[description]"
      assert_select "input#sel_step_position", :name => "sel_step[position]"
      assert_select "input#sel_step_step_type", :name => "sel_step[step_type]"
    end
  end
end
