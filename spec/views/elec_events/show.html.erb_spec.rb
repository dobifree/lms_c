require 'spec_helper'

describe "elec_events/show" do
  before(:each) do
    @elec_event = assign(:elec_event, stub_model(ElecEvent,
      :elec_process => nil,
      :elec_recurrence => nil,
      :manual => false,
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(/false/)
  end
end
