require 'spec_helper'

describe "elec_events/edit" do
  before(:each) do
    @elec_event = assign(:elec_event, stub_model(ElecEvent,
      :elec_process => nil,
      :elec_recurrence => nil,
      :manual => false,
      :active => false
    ))
  end

  it "renders the edit elec_event form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => elec_events_path(@elec_event), :method => "post" do
      assert_select "input#elec_event_elec_process", :name => "elec_event[elec_process]"
      assert_select "input#elec_event_elec_recurrence", :name => "elec_event[elec_recurrence]"
      assert_select "input#elec_event_manual", :name => "elec_event[manual]"
      assert_select "input#elec_event_active", :name => "elec_event[active]"
    end
  end
end
