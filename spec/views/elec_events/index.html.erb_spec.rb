require 'spec_helper'

describe "elec_events/index" do
  before(:each) do
    assign(:elec_events, [
      stub_model(ElecEvent,
        :elec_process => nil,
        :elec_recurrence => nil,
        :manual => false,
        :active => false
      ),
      stub_model(ElecEvent,
        :elec_process => nil,
        :elec_recurrence => nil,
        :manual => false,
        :active => false
      )
    ])
  end

  it "renders a list of elec_events" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
