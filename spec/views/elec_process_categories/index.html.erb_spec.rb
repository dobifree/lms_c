require 'spec_helper'

describe "elec_process_categories/index" do
  before(:each) do
    assign(:elec_process_categories, [
      stub_model(ElecProcessCategory,
        :elec_process => nil,
        :name => "Name",
        :description => "MyText",
        :max_votes => 1,
        :self_vote => false
      ),
      stub_model(ElecProcessCategory,
        :elec_process => nil,
        :name => "Name",
        :description => "MyText",
        :max_votes => 1,
        :self_vote => false
      )
    ])
  end

  it "renders a list of elec_process_categories" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
