require 'spec_helper'

describe "elec_process_categories/edit" do
  before(:each) do
    @elec_process_category = assign(:elec_process_category, stub_model(ElecProcessCategory,
      :elec_process => nil,
      :name => "MyString",
      :description => "MyText",
      :max_votes => 1,
      :self_vote => false
    ))
  end

  it "renders the edit elec_process_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => elec_process_categories_path(@elec_process_category), :method => "post" do
      assert_select "input#elec_process_category_elec_process", :name => "elec_process_category[elec_process]"
      assert_select "input#elec_process_category_name", :name => "elec_process_category[name]"
      assert_select "textarea#elec_process_category_description", :name => "elec_process_category[description]"
      assert_select "input#elec_process_category_max_votes", :name => "elec_process_category[max_votes]"
      assert_select "input#elec_process_category_self_vote", :name => "elec_process_category[self_vote]"
    end
  end
end
