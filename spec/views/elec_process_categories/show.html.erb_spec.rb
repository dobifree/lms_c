require 'spec_helper'

describe "elec_process_categories/show" do
  before(:each) do
    @elec_process_category = assign(:elec_process_category, stub_model(ElecProcessCategory,
      :elec_process => nil,
      :name => "Name",
      :description => "MyText",
      :max_votes => 1,
      :self_vote => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(/false/)
  end
end
