require 'spec_helper'

describe "enrollments/edit" do
  before(:each) do
    @enrollment = assign(:enrollment, stub_model(Enrollment,
      :user => nil,
      :program => nil
    ))
  end

  it "renders the edit enrollment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => enrollments_path(@enrollment), :method => "post" do
      assert_select "input#enrollment_user", :name => "enrollment[user]"
      assert_select "input#enrollment_program", :name => "enrollment[program]"
    end
  end
end
