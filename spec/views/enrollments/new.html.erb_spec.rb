require 'spec_helper'

describe "enrollments/new" do
  before(:each) do
    assign(:enrollment, stub_model(Enrollment,
      :user => nil,
      :program => nil
    ).as_new_record)
  end

  it "renders new enrollment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => enrollments_path, :method => "post" do
      assert_select "input#enrollment_user", :name => "enrollment[user]"
      assert_select "input#enrollment_program", :name => "enrollment[program]"
    end
  end
end
