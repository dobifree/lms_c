require 'spec_helper'

describe "sel_templates/edit" do
  before(:each) do
    @sel_template = assign(:sel_template, stub_model(SelTemplate,
      :name => "MyString",
      :description => "MyText",
      :active => false
    ))
  end

  it "renders the edit sel_template form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_templates_path(@sel_template), :method => "post" do
      assert_select "input#sel_template_name", :name => "sel_template[name]"
      assert_select "textarea#sel_template_description", :name => "sel_template[description]"
      assert_select "input#sel_template_active", :name => "sel_template[active]"
    end
  end
end
