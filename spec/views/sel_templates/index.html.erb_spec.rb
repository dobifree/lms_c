require 'spec_helper'

describe "sel_templates/index" do
  before(:each) do
    assign(:sel_templates, [
      stub_model(SelTemplate,
        :name => "Name",
        :description => "MyText",
        :active => false
      ),
      stub_model(SelTemplate,
        :name => "Name",
        :description => "MyText",
        :active => false
      )
    ])
  end

  it "renders a list of sel_templates" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
