require 'spec_helper'

describe "sel_templates/show" do
  before(:each) do
    @sel_template = assign(:sel_template, stub_model(SelTemplate,
      :name => "Name",
      :description => "MyText",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/false/)
  end
end
