require 'spec_helper'

describe "brc_processes/index" do
  before(:each) do
    assign(:brc_processes, [
      stub_model(BrcProcess,
        :name => "Name",
        :period => "Period"
      ),
      stub_model(BrcProcess,
        :name => "Name",
        :period => "Period"
      )
    ])
  end

  it "renders a list of brc_processes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Period".to_s, :count => 2
  end
end
