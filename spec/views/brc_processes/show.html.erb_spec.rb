require 'spec_helper'

describe "brc_processes/show" do
  before(:each) do
    @brc_process = assign(:brc_process, stub_model(BrcProcess,
      :name => "Name",
      :period => "Period"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Period/)
  end
end
