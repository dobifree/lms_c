require 'spec_helper'

describe "brc_processes/new" do
  before(:each) do
    assign(:brc_process, stub_model(BrcProcess,
      :name => "MyString",
      :period => "MyString"
    ).as_new_record)
  end

  it "renders new brc_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => brc_processes_path, :method => "post" do
      assert_select "input#brc_process_name", :name => "brc_process[name]"
      assert_select "input#brc_process_period", :name => "brc_process[period]"
    end
  end
end
