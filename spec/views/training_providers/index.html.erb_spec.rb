require 'spec_helper'

describe "training_providers/index" do
  before(:each) do
    assign(:training_providers, [
      stub_model(TrainingProvider,
        :nombre => "Nombre"
      ),
      stub_model(TrainingProvider,
        :nombre => "Nombre"
      )
    ])
  end

  it "renders a list of training_providers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
  end
end
