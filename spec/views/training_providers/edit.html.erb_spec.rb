require 'spec_helper'

describe "training_providers/edit" do
  before(:each) do
    @training_provider = assign(:training_provider, stub_model(TrainingProvider,
      :nombre => "MyString"
    ))
  end

  it "renders the edit training_provider form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_providers_path(@training_provider), :method => "post" do
      assert_select "input#training_provider_nombre", :name => "training_provider[nombre]"
    end
  end
end
