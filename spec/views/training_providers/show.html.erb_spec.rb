require 'spec_helper'

describe "training_providers/show" do
  before(:each) do
    @training_provider = assign(:training_provider, stub_model(TrainingProvider,
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
  end
end
