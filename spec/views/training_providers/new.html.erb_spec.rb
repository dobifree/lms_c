require 'spec_helper'

describe "training_providers/new" do
  before(:each) do
    assign(:training_provider, stub_model(TrainingProvider,
      :nombre => "MyString"
    ).as_new_record)
  end

  it "renders new training_provider form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_providers_path, :method => "post" do
      assert_select "input#training_provider_nombre", :name => "training_provider[nombre]"
    end
  end
end
