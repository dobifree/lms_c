require 'spec_helper'

describe "pe_question_activity_field_lists/show" do
  before(:each) do
    @pe_question_activity_field_list = assign(:pe_question_activity_field_list, stub_model(PeQuestionActivityFieldList,
      :pe_evaluation => nil,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
  end
end
