require 'spec_helper'

describe "pe_question_activity_field_lists/edit" do
  before(:each) do
    @pe_question_activity_field_list = assign(:pe_question_activity_field_list, stub_model(PeQuestionActivityFieldList,
      :pe_evaluation => nil,
      :name => "MyString"
    ))
  end

  it "renders the edit pe_question_activity_field_list form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_question_activity_field_lists_path(@pe_question_activity_field_list), :method => "post" do
      assert_select "input#pe_question_activity_field_list_pe_evaluation", :name => "pe_question_activity_field_list[pe_evaluation]"
      assert_select "input#pe_question_activity_field_list_name", :name => "pe_question_activity_field_list[name]"
    end
  end
end
