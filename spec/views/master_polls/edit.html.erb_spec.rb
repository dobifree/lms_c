require 'spec_helper'

describe "master_polls/edit" do
  before(:each) do
    @master_poll = assign(:master_poll, stub_model(MasterPoll,
      :numero => 1,
      :nombre => "MyString",
      :descripcion => "MyText",
      :tags => "MyString"
    ))
  end

  it "renders the edit master_poll form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => master_polls_path(@master_poll), :method => "post" do
      assert_select "input#master_poll_numero", :name => "master_poll[numero]"
      assert_select "input#master_poll_nombre", :name => "master_poll[nombre]"
      assert_select "textarea#master_poll_descripcion", :name => "master_poll[descripcion]"
      assert_select "input#master_poll_tags", :name => "master_poll[tags]"
    end
  end
end
