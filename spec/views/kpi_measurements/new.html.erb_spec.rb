require 'spec_helper'

describe "kpi_measurements/new" do
  before(:each) do
    assign(:kpi_measurement, stub_model(KpiMeasurement,
      :value => 1.5,
      :percentage => 1.5,
      :kpi_indicator => nil
    ).as_new_record)
  end

  it "renders new kpi_measurement form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_measurements_path, :method => "post" do
      assert_select "input#kpi_measurement_value", :name => "kpi_measurement[value]"
      assert_select "input#kpi_measurement_percentage", :name => "kpi_measurement[percentage]"
      assert_select "input#kpi_measurement_kpi_indicator", :name => "kpi_measurement[kpi_indicator]"
    end
  end
end
