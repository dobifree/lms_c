require 'spec_helper'

describe "kpi_measurements/index" do
  before(:each) do
    assign(:kpi_measurements, [
      stub_model(KpiMeasurement,
        :value => 1.5,
        :percentage => 1.5,
        :kpi_indicator => nil
      ),
      stub_model(KpiMeasurement,
        :value => 1.5,
        :percentage => 1.5,
        :kpi_indicator => nil
      )
    ])
  end

  it "renders a list of kpi_measurements" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
