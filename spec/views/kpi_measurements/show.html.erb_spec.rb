require 'spec_helper'

describe "kpi_measurements/show" do
  before(:each) do
    @kpi_measurement = assign(:kpi_measurement, stub_model(KpiMeasurement,
      :value => 1.5,
      :percentage => 1.5,
      :kpi_indicator => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(//)
  end
end
