require 'spec_helper'

describe "kpi_measurements/edit" do
  before(:each) do
    @kpi_measurement = assign(:kpi_measurement, stub_model(KpiMeasurement,
      :value => 1.5,
      :percentage => 1.5,
      :kpi_indicator => nil
    ))
  end

  it "renders the edit kpi_measurement form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_measurements_path(@kpi_measurement), :method => "post" do
      assert_select "input#kpi_measurement_value", :name => "kpi_measurement[value]"
      assert_select "input#kpi_measurement_percentage", :name => "kpi_measurement[percentage]"
      assert_select "input#kpi_measurement_kpi_indicator", :name => "kpi_measurement[kpi_indicator]"
    end
  end
end
