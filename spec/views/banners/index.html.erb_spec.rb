require 'spec_helper'

describe "banners/index" do
  before(:each) do
    assign(:banners, [
      stub_model(Banner,
        :name => "Name",
        :crypted_name => "Crypted Name",
        :display_method => 1,
        :display_content => 2,
        :active => false,
        :complement_visible => false,
        :link => "MyText"
      ),
      stub_model(Banner,
        :name => "Name",
        :crypted_name => "Crypted Name",
        :display_method => 1,
        :display_content => 2,
        :active => false,
        :complement_visible => false,
        :link => "MyText"
      )
    ])
  end

  it "renders a list of banners" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Crypted Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
