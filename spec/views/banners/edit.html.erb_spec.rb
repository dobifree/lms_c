require 'spec_helper'

describe "banners/edit" do
  before(:each) do
    @banner = assign(:banner, stub_model(Banner,
      :name => "MyString",
      :crypted_name => "MyString",
      :display_method => 1,
      :display_content => 1,
      :active => false,
      :complement_visible => false,
      :link => "MyText"
    ))
  end

  it "renders the edit banner form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => banners_path(@banner), :method => "post" do
      assert_select "input#banner_name", :name => "banner[name]"
      assert_select "input#banner_crypted_name", :name => "banner[crypted_name]"
      assert_select "input#banner_display_method", :name => "banner[display_method]"
      assert_select "input#banner_display_content", :name => "banner[display_content]"
      assert_select "input#banner_active", :name => "banner[active]"
      assert_select "input#banner_complement_visible", :name => "banner[complement_visible]"
      assert_select "textarea#banner_link", :name => "banner[link]"
    end
  end
end
