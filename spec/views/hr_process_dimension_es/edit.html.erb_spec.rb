require 'spec_helper'

describe "hr_process_dimension_es/edit" do
  before(:each) do
    @hr_process_dimension_e = assign(:hr_process_dimension_e, stub_model(HrProcessDimensionE,
      :orden => 1,
      :porcentaje => 1,
      :hr_evaluation_type => nil,
      :hr_process_dimension => nil
    ))
  end

  it "renders the edit hr_process_dimension_e form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_dimension_es_path(@hr_process_dimension_e), :method => "post" do
      assert_select "input#hr_process_dimension_e_orden", :name => "hr_process_dimension_e[orden]"
      assert_select "input#hr_process_dimension_e_porcentaje", :name => "hr_process_dimension_e[porcentaje]"
      assert_select "input#hr_process_dimension_e_hr_evaluation_type", :name => "hr_process_dimension_e[hr_evaluation_type]"
      assert_select "input#hr_process_dimension_e_hr_process_dimension", :name => "hr_process_dimension_e[hr_process_dimension]"
    end
  end
end
