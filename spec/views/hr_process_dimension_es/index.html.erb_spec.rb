require 'spec_helper'

describe "hr_process_dimension_es/index" do
  before(:each) do
    assign(:hr_process_dimension_es, [
      stub_model(HrProcessDimensionE,
        :orden => 1,
        :porcentaje => 2,
        :hr_evaluation_type => nil,
        :hr_process_dimension => nil
      ),
      stub_model(HrProcessDimensionE,
        :orden => 1,
        :porcentaje => 2,
        :hr_evaluation_type => nil,
        :hr_process_dimension => nil
      )
    ])
  end

  it "renders a list of hr_process_dimension_es" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
