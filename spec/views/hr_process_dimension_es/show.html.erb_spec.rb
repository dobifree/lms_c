require 'spec_helper'

describe "hr_process_dimension_es/show" do
  before(:each) do
    @hr_process_dimension_e = assign(:hr_process_dimension_e, stub_model(HrProcessDimensionE,
      :orden => 1,
      :porcentaje => 2,
      :hr_evaluation_type => nil,
      :hr_process_dimension => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(//)
    rendered.should match(//)
  end
end
