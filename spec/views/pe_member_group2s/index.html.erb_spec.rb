require 'spec_helper'

describe "pe_member_group2s/index" do
  before(:each) do
    assign(:pe_member_group2s, [
      stub_model(PeMemberGroup2,
        :pe_process => nil,
        :pe_member => nil,
        :pe_group2 => nil
      ),
      stub_model(PeMemberGroup2,
        :pe_process => nil,
        :pe_member => nil,
        :pe_group2 => nil
      )
    ])
  end

  it "renders a list of pe_member_group2s" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
