require 'spec_helper'

describe "pe_member_group2s/show" do
  before(:each) do
    @pe_member_group2 = assign(:pe_member_group2, stub_model(PeMemberGroup2,
      :pe_process => nil,
      :pe_member => nil,
      :pe_group2 => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
  end
end
