require 'spec_helper'

describe "pe_member_group2s/edit" do
  before(:each) do
    @pe_member_group2 = assign(:pe_member_group2, stub_model(PeMemberGroup2,
      :pe_process => nil,
      :pe_member => nil,
      :pe_group2 => nil
    ))
  end

  it "renders the edit pe_member_group2 form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_member_group2s_path(@pe_member_group2), :method => "post" do
      assert_select "input#pe_member_group2_pe_process", :name => "pe_member_group2[pe_process]"
      assert_select "input#pe_member_group2_pe_member", :name => "pe_member_group2[pe_member]"
      assert_select "input#pe_member_group2_pe_group2", :name => "pe_member_group2[pe_group2]"
    end
  end
end
