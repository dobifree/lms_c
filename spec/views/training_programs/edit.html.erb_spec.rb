require 'spec_helper'

describe "training_programs/edit" do
  before(:each) do
    @training_program = assign(:training_program, stub_model(TrainingProgram,
      :nombre => "MyString"
    ))
  end

  it "renders the edit training_program form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_programs_path(@training_program), :method => "post" do
      assert_select "input#training_program_nombre", :name => "training_program[nombre]"
    end
  end
end
