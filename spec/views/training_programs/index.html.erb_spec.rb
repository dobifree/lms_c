require 'spec_helper'

describe "training_programs/index" do
  before(:each) do
    assign(:training_programs, [
      stub_model(TrainingProgram,
        :nombre => "Nombre"
      ),
      stub_model(TrainingProgram,
        :nombre => "Nombre"
      )
    ])
  end

  it "renders a list of training_programs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
  end
end
