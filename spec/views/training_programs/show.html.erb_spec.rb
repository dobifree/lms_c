require 'spec_helper'

describe "training_programs/show" do
  before(:each) do
    @training_program = assign(:training_program, stub_model(TrainingProgram,
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
  end
end
