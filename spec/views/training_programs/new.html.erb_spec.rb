require 'spec_helper'

describe "training_programs/new" do
  before(:each) do
    assign(:training_program, stub_model(TrainingProgram,
      :nombre => "MyString"
    ).as_new_record)
  end

  it "renders new training_program form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_programs_path, :method => "post" do
      assert_select "input#training_program_nombre", :name => "training_program[nombre]"
    end
  end
end
