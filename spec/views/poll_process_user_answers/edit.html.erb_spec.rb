require 'spec_helper'

describe "poll_process_user_answers/edit" do
  before(:each) do
    @poll_process_user_answer = assign(:poll_process_user_answer, stub_model(PollProcessUserAnswer,
      :poll_process_user => nil,
      :master_poll_question => nil,
      :master_poll_alternative => nil
    ))
  end

  it "renders the edit poll_process_user_answer form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => poll_process_user_answers_path(@poll_process_user_answer), :method => "post" do
      assert_select "input#poll_process_user_answer_poll_process_user", :name => "poll_process_user_answer[poll_process_user]"
      assert_select "input#poll_process_user_answer_master_poll_question", :name => "poll_process_user_answer[master_poll_question]"
      assert_select "input#poll_process_user_answer_master_poll_alternative", :name => "poll_process_user_answer[master_poll_alternative]"
    end
  end
end
