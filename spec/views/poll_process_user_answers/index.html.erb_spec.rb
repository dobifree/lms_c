require 'spec_helper'

describe "poll_process_user_answers/index" do
  before(:each) do
    assign(:poll_process_user_answers, [
      stub_model(PollProcessUserAnswer,
        :poll_process_user => nil,
        :master_poll_question => nil,
        :master_poll_alternative => nil
      ),
      stub_model(PollProcessUserAnswer,
        :poll_process_user => nil,
        :master_poll_question => nil,
        :master_poll_alternative => nil
      )
    ])
  end

  it "renders a list of poll_process_user_answers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
