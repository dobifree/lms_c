require 'spec_helper'

describe "poll_process_user_answers/show" do
  before(:each) do
    @poll_process_user_answer = assign(:poll_process_user_answer, stub_model(PollProcessUserAnswer,
      :poll_process_user => nil,
      :master_poll_question => nil,
      :master_poll_alternative => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
  end
end
