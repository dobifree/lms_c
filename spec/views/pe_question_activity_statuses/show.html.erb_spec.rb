require 'spec_helper'

describe "pe_question_activity_statuses/show" do
  before(:each) do
    @pe_question_activity_status = assign(:pe_question_activity_status, stub_model(PeQuestionActivityStatus,
      :pe_evaluation => nil,
      :position => 1,
      :name => "Name",
      :color => "Color",
      :completed => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/Color/)
    rendered.should match(/false/)
  end
end
