require 'spec_helper'

describe "pe_question_activity_statuses/new" do
  before(:each) do
    assign(:pe_question_activity_status, stub_model(PeQuestionActivityStatus,
      :pe_evaluation => nil,
      :position => 1,
      :name => "MyString",
      :color => "MyString",
      :completed => false
    ).as_new_record)
  end

  it "renders new pe_question_activity_status form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_question_activity_statuses_path, :method => "post" do
      assert_select "input#pe_question_activity_status_pe_evaluation", :name => "pe_question_activity_status[pe_evaluation]"
      assert_select "input#pe_question_activity_status_position", :name => "pe_question_activity_status[position]"
      assert_select "input#pe_question_activity_status_name", :name => "pe_question_activity_status[name]"
      assert_select "input#pe_question_activity_status_color", :name => "pe_question_activity_status[color]"
      assert_select "input#pe_question_activity_status_completed", :name => "pe_question_activity_status[completed]"
    end
  end
end
