require 'spec_helper'

describe "pe_question_activity_statuses/index" do
  before(:each) do
    assign(:pe_question_activity_statuses, [
      stub_model(PeQuestionActivityStatus,
        :pe_evaluation => nil,
        :position => 1,
        :name => "Name",
        :color => "Color",
        :completed => false
      ),
      stub_model(PeQuestionActivityStatus,
        :pe_evaluation => nil,
        :position => 1,
        :name => "Name",
        :color => "Color",
        :completed => false
      )
    ])
  end

  it "renders a list of pe_question_activity_statuses" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Color".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
