require 'spec_helper'

describe "brc_apv_institutions/edit" do
  before(:each) do
    @brc_apv_institution = assign(:brc_apv_institution, stub_model(BrcApvInstitution,
      :name => "MyString"
    ))
  end

  it "renders the edit brc_apv_institution form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => brc_apv_institutions_path(@brc_apv_institution), :method => "post" do
      assert_select "input#brc_apv_institution_name", :name => "brc_apv_institution[name]"
    end
  end
end
