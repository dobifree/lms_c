require 'spec_helper'

describe "brc_apv_institutions/new" do
  before(:each) do
    assign(:brc_apv_institution, stub_model(BrcApvInstitution,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new brc_apv_institution form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => brc_apv_institutions_path, :method => "post" do
      assert_select "input#brc_apv_institution_name", :name => "brc_apv_institution[name]"
    end
  end
end
