require 'spec_helper'

describe "brc_apv_institutions/show" do
  before(:each) do
    @brc_apv_institution = assign(:brc_apv_institution, stub_model(BrcApvInstitution,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
