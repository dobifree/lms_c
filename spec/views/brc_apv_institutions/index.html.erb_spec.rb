require 'spec_helper'

describe "brc_apv_institutions/index" do
  before(:each) do
    assign(:brc_apv_institutions, [
      stub_model(BrcApvInstitution,
        :name => "Name"
      ),
      stub_model(BrcApvInstitution,
        :name => "Name"
      )
    ])
  end

  it "renders a list of brc_apv_institutions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
