require 'spec_helper'

describe "kpi_indicator_details/index" do
  before(:each) do
    assign(:kpi_indicator_details, [
      stub_model(KpiIndicatorDetail,
        :kpi_indicator => nil,
        :goal => 1.5,
        :indicator_type => 1,
        :value => 1.5,
        :percentage => 1.5
      ),
      stub_model(KpiIndicatorDetail,
        :kpi_indicator => nil,
        :goal => 1.5,
        :indicator_type => 1,
        :value => 1.5,
        :percentage => 1.5
      )
    ])
  end

  it "renders a list of kpi_indicator_details" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
