require 'spec_helper'

describe "kpi_indicator_details/show" do
  before(:each) do
    @kpi_indicator_detail = assign(:kpi_indicator_detail, stub_model(KpiIndicatorDetail,
      :kpi_indicator => nil,
      :goal => 1.5,
      :indicator_type => 1,
      :value => 1.5,
      :percentage => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1.5/)
    rendered.should match(/1/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
  end
end
