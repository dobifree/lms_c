require 'spec_helper'

describe "kpi_indicator_details/new" do
  before(:each) do
    assign(:kpi_indicator_detail, stub_model(KpiIndicatorDetail,
      :kpi_indicator => nil,
      :goal => 1.5,
      :indicator_type => 1,
      :value => 1.5,
      :percentage => 1.5
    ).as_new_record)
  end

  it "renders new kpi_indicator_detail form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_indicator_details_path, :method => "post" do
      assert_select "input#kpi_indicator_detail_kpi_indicator", :name => "kpi_indicator_detail[kpi_indicator]"
      assert_select "input#kpi_indicator_detail_goal", :name => "kpi_indicator_detail[goal]"
      assert_select "input#kpi_indicator_detail_indicator_type", :name => "kpi_indicator_detail[indicator_type]"
      assert_select "input#kpi_indicator_detail_value", :name => "kpi_indicator_detail[value]"
      assert_select "input#kpi_indicator_detail_percentage", :name => "kpi_indicator_detail[percentage]"
    end
  end
end
