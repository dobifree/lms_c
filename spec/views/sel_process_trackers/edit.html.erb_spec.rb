require 'spec_helper'

describe "sel_process_trackers/edit" do
  before(:each) do
    @sel_process_tracker = assign(:sel_process_tracker, stub_model(SelProcessTracker))
  end

  it "renders the edit sel_process_tracker form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_process_trackers_path(@sel_process_tracker), :method => "post" do
    end
  end
end
