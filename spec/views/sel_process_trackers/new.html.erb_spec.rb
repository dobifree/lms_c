require 'spec_helper'

describe "sel_process_trackers/new" do
  before(:each) do
    assign(:sel_process_tracker, stub_model(SelProcessTracker).as_new_record)
  end

  it "renders new sel_process_tracker form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_process_trackers_path, :method => "post" do
    end
  end
end
