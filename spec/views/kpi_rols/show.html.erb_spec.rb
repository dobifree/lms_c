require 'spec_helper'

describe "kpi_rols/show" do
  before(:each) do
    @kpi_rol = assign(:kpi_rol, stub_model(KpiRol,
      :name => "Name",
      :kpi_dashboard => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
