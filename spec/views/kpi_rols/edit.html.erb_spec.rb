require 'spec_helper'

describe "kpi_rols/edit" do
  before(:each) do
    @kpi_rol = assign(:kpi_rol, stub_model(KpiRol,
      :name => "MyString",
      :kpi_dashboard => nil
    ))
  end

  it "renders the edit kpi_rol form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_rols_path(@kpi_rol), :method => "post" do
      assert_select "input#kpi_rol_name", :name => "kpi_rol[name]"
      assert_select "input#kpi_rol_kpi_dashboard", :name => "kpi_rol[kpi_dashboard]"
    end
  end
end
