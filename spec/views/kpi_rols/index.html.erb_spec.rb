require 'spec_helper'

describe "kpi_rols/index" do
  before(:each) do
    assign(:kpi_rols, [
      stub_model(KpiRol,
        :name => "Name",
        :kpi_dashboard => nil
      ),
      stub_model(KpiRol,
        :name => "Name",
        :kpi_dashboard => nil
      )
    ])
  end

  it "renders a list of kpi_rols" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
