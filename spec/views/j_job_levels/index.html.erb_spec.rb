require 'spec_helper'

describe "j_job_levels/index" do
  before(:each) do
    assign(:j_job_levels, [
      stub_model(JJobLevel,
        :name => "Name"
      ),
      stub_model(JJobLevel,
        :name => "Name"
      )
    ])
  end

  it "renders a list of j_job_levels" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
