require 'spec_helper'

describe "j_job_levels/edit" do
  before(:each) do
    @j_job_level = assign(:j_job_level, stub_model(JJobLevel,
      :name => "MyString"
    ))
  end

  it "renders the edit j_job_level form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_job_levels_path(@j_job_level), :method => "post" do
      assert_select "input#j_job_level_name", :name => "j_job_level[name]"
    end
  end
end
