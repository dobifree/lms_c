require 'spec_helper'

describe "j_job_levels/new" do
  before(:each) do
    assign(:j_job_level, stub_model(JJobLevel,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new j_job_level form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_job_levels_path, :method => "post" do
      assert_select "input#j_job_level_name", :name => "j_job_level[name]"
    end
  end
end
