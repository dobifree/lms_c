require 'spec_helper'

describe "j_job_levels/show" do
  before(:each) do
    @j_job_level = assign(:j_job_level, stub_model(JJobLevel,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
