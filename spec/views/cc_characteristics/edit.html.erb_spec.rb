require 'spec_helper'

describe "cc_characteristics/edit" do
  before(:each) do
    @cc_characteristic = assign(:cc_characteristic, stub_model(CcCharacteristic,
      :characteristic => nil,
      :position => 1
    ))
  end

  it "renders the edit cc_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => cc_characteristics_path(@cc_characteristic), :method => "post" do
      assert_select "input#cc_characteristic_characteristic", :name => "cc_characteristic[characteristic]"
      assert_select "input#cc_characteristic_position", :name => "cc_characteristic[position]"
    end
  end
end
