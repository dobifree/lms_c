require 'spec_helper'

describe "cc_characteristics/show" do
  before(:each) do
    @cc_characteristic = assign(:cc_characteristic, stub_model(CcCharacteristic,
      :characteristic => nil,
      :position => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
  end
end
