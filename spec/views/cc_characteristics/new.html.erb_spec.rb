require 'spec_helper'

describe "cc_characteristics/new" do
  before(:each) do
    assign(:cc_characteristic, stub_model(CcCharacteristic,
      :characteristic => nil,
      :position => 1
    ).as_new_record)
  end

  it "renders new cc_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => cc_characteristics_path, :method => "post" do
      assert_select "input#cc_characteristic_characteristic", :name => "cc_characteristic[characteristic]"
      assert_select "input#cc_characteristic_position", :name => "cc_characteristic[position]"
    end
  end
end
