require 'spec_helper'

describe "program_course_instances/show" do
  before(:each) do
    @program_course_instance = assign(:program_course_instance, stub_model(ProgramCourseInstance,
      :program_course => nil,
      :program_instance => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
