require 'spec_helper'

describe "program_course_instances/edit" do
  before(:each) do
    @program_course_instance = assign(:program_course_instance, stub_model(ProgramCourseInstance,
      :program_course => nil,
      :program_instance => nil
    ))
  end

  it "renders the edit program_course_instance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => program_course_instances_path(@program_course_instance), :method => "post" do
      assert_select "input#program_course_instance_program_course", :name => "program_course_instance[program_course]"
      assert_select "input#program_course_instance_program_instance", :name => "program_course_instance[program_instance]"
    end
  end
end
