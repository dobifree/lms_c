require 'spec_helper'

describe "program_course_instances/index" do
  before(:each) do
    assign(:program_course_instances, [
      stub_model(ProgramCourseInstance,
        :program_course => nil,
        :program_instance => nil
      ),
      stub_model(ProgramCourseInstance,
        :program_course => nil,
        :program_instance => nil
      )
    ])
  end

  it "renders a list of program_course_instances" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
