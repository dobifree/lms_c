require 'spec_helper'

describe "program_course_instances/new" do
  before(:each) do
    assign(:program_course_instance, stub_model(ProgramCourseInstance,
      :program_course => nil,
      :program_instance => nil
    ).as_new_record)
  end

  it "renders new program_course_instance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => program_course_instances_path, :method => "post" do
      assert_select "input#program_course_instance_program_course", :name => "program_course_instance[program_course]"
      assert_select "input#program_course_instance_program_instance", :name => "program_course_instance[program_instance]"
    end
  end
end
