require 'spec_helper'

describe "levels/new" do
  before(:each) do
    assign(:level, stub_model(Level,
      :nombre => "MyString",
      :orden => 1,
      :program => nil
    ).as_new_record)
  end

  it "renders new level form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => levels_path, :method => "post" do
      assert_select "input#level_nombre", :name => "level[nombre]"
      assert_select "input#level_orden", :name => "level[orden]"
      assert_select "input#level_program", :name => "level[program]"
    end
  end
end
