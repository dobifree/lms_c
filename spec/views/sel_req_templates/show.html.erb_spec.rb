require 'spec_helper'

describe "sel_req_templates/show" do
  before(:each) do
    @sel_req_template = assign(:sel_req_template, stub_model(SelReqTemplate,
      :name => "Name",
      :description => "MyText",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/false/)
  end
end
