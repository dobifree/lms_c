require 'spec_helper'

describe "sel_req_templates/new" do
  before(:each) do
    assign(:sel_req_template, stub_model(SelReqTemplate,
      :name => "MyString",
      :description => "MyText",
      :active => false
    ).as_new_record)
  end

  it "renders new sel_req_template form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_req_templates_path, :method => "post" do
      assert_select "input#sel_req_template_name", :name => "sel_req_template[name]"
      assert_select "textarea#sel_req_template_description", :name => "sel_req_template[description]"
      assert_select "input#sel_req_template_active", :name => "sel_req_template[active]"
    end
  end
end
