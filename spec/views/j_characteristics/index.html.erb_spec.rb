require 'spec_helper'

describe "j_characteristics/index" do
  before(:each) do
    assign(:j_characteristics, [
      stub_model(JCharacteristic,
        :characteristic => nil,
        :position => 1
      ),
      stub_model(JCharacteristic,
        :characteristic => nil,
        :position => 1
      )
    ])
  end

  it "renders a list of j_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
