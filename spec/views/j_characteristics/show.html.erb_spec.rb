require 'spec_helper'

describe "j_characteristics/show" do
  before(:each) do
    @j_characteristic = assign(:j_characteristic, stub_model(JCharacteristic,
      :characteristic => nil,
      :position => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
  end
end
