require 'spec_helper'

describe "j_characteristics/edit" do
  before(:each) do
    @j_characteristic = assign(:j_characteristic, stub_model(JCharacteristic,
      :characteristic => nil,
      :position => 1
    ))
  end

  it "renders the edit j_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_characteristics_path(@j_characteristic), :method => "post" do
      assert_select "input#j_characteristic_characteristic", :name => "j_characteristic[characteristic]"
      assert_select "input#j_characteristic_position", :name => "j_characteristic[position]"
    end
  end
end
