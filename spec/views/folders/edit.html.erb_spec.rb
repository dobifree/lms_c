require 'spec_helper'

describe "folders/edit" do
  before(:each) do
    @folder = assign(:folder, stub_model(Folder,
      :nombre => "MyString",
      :creador_id => 1,
      :father_id => 1
    ))
  end

  it "renders the edit folder form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => folders_path(@folder), :method => "post" do
      assert_select "input#folder_nombre", :name => "folder[nombre]"
      assert_select "input#folder_creador_id", :name => "folder[creador_id]"
      assert_select "input#folder_father_id", :name => "folder[father_id]"
    end
  end
end
