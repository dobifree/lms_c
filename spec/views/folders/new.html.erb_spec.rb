require 'spec_helper'

describe "folders/new" do
  before(:each) do
    assign(:folder, stub_model(Folder,
      :nombre => "MyString",
      :creador_id => 1,
      :father_id => 1
    ).as_new_record)
  end

  it "renders new folder form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => folders_path, :method => "post" do
      assert_select "input#folder_nombre", :name => "folder[nombre]"
      assert_select "input#folder_creador_id", :name => "folder[creador_id]"
      assert_select "input#folder_father_id", :name => "folder[father_id]"
    end
  end
end
