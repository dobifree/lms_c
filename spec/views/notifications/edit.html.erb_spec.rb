require 'spec_helper'

describe "notifications/edit" do
  before(:each) do
    @notification = assign(:notification, stub_model(Notification,
      :orden => 1,
      :asunto => "MyString",
      :texto => "MyText"
    ))
  end

  it "renders the edit notification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => notifications_path(@notification), :method => "post" do
      assert_select "input#notification_orden", :name => "notification[orden]"
      assert_select "input#notification_asunto", :name => "notification[asunto]"
      assert_select "textarea#notification_texto", :name => "notification[texto]"
    end
  end
end
