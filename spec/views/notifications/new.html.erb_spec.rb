require 'spec_helper'

describe "notifications/new" do
  before(:each) do
    assign(:notification, stub_model(Notification,
      :orden => 1,
      :asunto => "MyString",
      :texto => "MyText"
    ).as_new_record)
  end

  it "renders new notification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => notifications_path, :method => "post" do
      assert_select "input#notification_orden", :name => "notification[orden]"
      assert_select "input#notification_asunto", :name => "notification[asunto]"
      assert_select "textarea#notification_texto", :name => "notification[texto]"
    end
  end
end
