require 'spec_helper'

describe "dnc_courses/show" do
  before(:each) do
    @dnc_course = assign(:dnc_course, stub_model(DncCourse,
      :name => "Name",
      :dnc_program => nil,
      :dnc_category => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
    rendered.should match(//)
  end
end
