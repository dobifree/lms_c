require 'spec_helper'

describe "dnc_courses/new" do
  before(:each) do
    assign(:dnc_course, stub_model(DncCourse,
      :name => "MyString",
      :dnc_program => nil,
      :dnc_category => nil
    ).as_new_record)
  end

  it "renders new dnc_course form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_courses_path, :method => "post" do
      assert_select "input#dnc_course_name", :name => "dnc_course[name]"
      assert_select "input#dnc_course_dnc_program", :name => "dnc_course[dnc_program]"
      assert_select "input#dnc_course_dnc_category", :name => "dnc_course[dnc_category]"
    end
  end
end
