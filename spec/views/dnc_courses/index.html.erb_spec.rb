require 'spec_helper'

describe "dnc_courses/index" do
  before(:each) do
    assign(:dnc_courses, [
      stub_model(DncCourse,
        :name => "Name",
        :dnc_program => nil,
        :dnc_category => nil
      ),
      stub_model(DncCourse,
        :name => "Name",
        :dnc_program => nil,
        :dnc_category => nil
      )
    ])
  end

  it "renders a list of dnc_courses" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
