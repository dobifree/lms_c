require 'spec_helper'

describe "pe_question_models/index" do
  before(:each) do
    assign(:pe_question_models, [
      stub_model(PeQuestionModel,
        :description => "Description",
        :weight => 1.5,
        :pe_evaluation => nil,
        :pe_element => nil,
        :has_comment => false,
        :stored_image => nil
      ),
      stub_model(PeQuestionModel,
        :description => "Description",
        :weight => 1.5,
        :pe_evaluation => nil,
        :pe_element => nil,
        :has_comment => false,
        :stored_image => nil
      )
    ])
  end

  it "renders a list of pe_question_models" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
