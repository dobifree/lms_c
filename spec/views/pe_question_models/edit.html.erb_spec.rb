require 'spec_helper'

describe "pe_question_models/edit" do
  before(:each) do
    @pe_question_model = assign(:pe_question_model, stub_model(PeQuestionModel,
      :description => "MyString",
      :weight => 1.5,
      :pe_evaluation => nil,
      :pe_element => nil,
      :has_comment => false,
      :stored_image => nil
    ))
  end

  it "renders the edit pe_question_model form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_question_models_path(@pe_question_model), :method => "post" do
      assert_select "input#pe_question_model_description", :name => "pe_question_model[description]"
      assert_select "input#pe_question_model_weight", :name => "pe_question_model[weight]"
      assert_select "input#pe_question_model_pe_evaluation", :name => "pe_question_model[pe_evaluation]"
      assert_select "input#pe_question_model_pe_element", :name => "pe_question_model[pe_element]"
      assert_select "input#pe_question_model_has_comment", :name => "pe_question_model[has_comment]"
      assert_select "input#pe_question_model_stored_image", :name => "pe_question_model[stored_image]"
    end
  end
end
