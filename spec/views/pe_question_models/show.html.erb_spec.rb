require 'spec_helper'

describe "pe_question_models/show" do
  before(:each) do
    @pe_question_model = assign(:pe_question_model, stub_model(PeQuestionModel,
      :description => "Description",
      :weight => 1.5,
      :pe_evaluation => nil,
      :pe_element => nil,
      :has_comment => false,
      :stored_image => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Description/)
    rendered.should match(/1.5/)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(//)
  end
end
