require 'spec_helper'

describe "sel_apply_forms/edit" do
  before(:each) do
    @sel_apply_form = assign(:sel_apply_form, stub_model(SelApplyForm))
  end

  it "renders the edit sel_apply_form form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_apply_forms_path(@sel_apply_form), :method => "post" do
    end
  end
end
