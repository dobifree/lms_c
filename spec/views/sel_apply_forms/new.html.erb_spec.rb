require 'spec_helper'

describe "sel_apply_forms/new" do
  before(:each) do
    assign(:sel_apply_form, stub_model(SelApplyForm).as_new_record)
  end

  it "renders new sel_apply_form form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_apply_forms_path, :method => "post" do
    end
  end
end
