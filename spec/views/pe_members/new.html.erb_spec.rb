require 'spec_helper'

describe "pe_members/new" do
  before(:each) do
    assign(:pe_member, stub_model(PeMember,
      :pe_process => nil,
      :user => nil,
      :is_assessed => false,
      :step_assessment => false,
      :step_calibration => false,
      :step_feedback => false,
      :step_feedback_accepted => false,
      :step_feedback_text => "MyText"
    ).as_new_record)
  end

  it "renders new pe_member form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_members_path, :method => "post" do
      assert_select "input#pe_member_pe_process", :name => "pe_member[pe_process]"
      assert_select "input#pe_member_user", :name => "pe_member[user]"
      assert_select "input#pe_member_is_assessed", :name => "pe_member[is_assessed]"
      assert_select "input#pe_member_step_assessment", :name => "pe_member[step_assessment]"
      assert_select "input#pe_member_step_calibration", :name => "pe_member[step_calibration]"
      assert_select "input#pe_member_step_feedback", :name => "pe_member[step_feedback]"
      assert_select "input#pe_member_step_feedback_accepted", :name => "pe_member[step_feedback_accepted]"
      assert_select "textarea#pe_member_step_feedback_text", :name => "pe_member[step_feedback_text]"
    end
  end
end
