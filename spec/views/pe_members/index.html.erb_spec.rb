require 'spec_helper'

describe "pe_members/index" do
  before(:each) do
    assign(:pe_members, [
      stub_model(PeMember,
        :pe_process => nil,
        :user => nil,
        :is_assessed => false,
        :step_assessment => false,
        :step_calibration => false,
        :step_feedback => false,
        :step_feedback_accepted => false,
        :step_feedback_text => "MyText"
      ),
      stub_model(PeMember,
        :pe_process => nil,
        :user => nil,
        :is_assessed => false,
        :step_assessment => false,
        :step_calibration => false,
        :step_feedback => false,
        :step_feedback_accepted => false,
        :step_feedback_text => "MyText"
      )
    ])
  end

  it "renders a list of pe_members" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
