require 'spec_helper'

describe "pe_members/show" do
  before(:each) do
    @pe_member = assign(:pe_member, stub_model(PeMember,
      :pe_process => nil,
      :user => nil,
      :is_assessed => false,
      :step_assessment => false,
      :step_calibration => false,
      :step_feedback => false,
      :step_feedback_accepted => false,
      :step_feedback_text => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/MyText/)
  end
end
