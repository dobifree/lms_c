require 'spec_helper'

describe "training_characteristics/show" do
  before(:each) do
    @training_characteristic = assign(:training_characteristic, stub_model(TrainingCharacteristic,
      :name => "Name",
      :type => "Type",
      :data_type_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Type/)
    rendered.should match(/1/)
  end
end
