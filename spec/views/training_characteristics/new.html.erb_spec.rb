require 'spec_helper'

describe "training_characteristics/new" do
  before(:each) do
    assign(:training_characteristic, stub_model(TrainingCharacteristic,
      :name => "MyString",
      :type => "",
      :data_type_id => 1
    ).as_new_record)
  end

  it "renders new training_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_characteristics_path, :method => "post" do
      assert_select "input#training_characteristic_name", :name => "training_characteristic[name]"
      assert_select "input#training_characteristic_type", :name => "training_characteristic[type]"
      assert_select "input#training_characteristic_data_type_id", :name => "training_characteristic[data_type_id]"
    end
  end
end
