require 'spec_helper'

describe "training_characteristics/index" do
  before(:each) do
    assign(:training_characteristics, [
      stub_model(TrainingCharacteristic,
        :name => "Name",
        :type => "Type",
        :data_type_id => 1
      ),
      stub_model(TrainingCharacteristic,
        :name => "Name",
        :type => "Type",
        :data_type_id => 1
      )
    ])
  end

  it "renders a list of training_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Type".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
