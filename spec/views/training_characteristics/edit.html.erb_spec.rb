require 'spec_helper'

describe "training_characteristics/edit" do
  before(:each) do
    @training_characteristic = assign(:training_characteristic, stub_model(TrainingCharacteristic,
      :name => "MyString",
      :type => "",
      :data_type_id => 1
    ))
  end

  it "renders the edit training_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_characteristics_path(@training_characteristic), :method => "post" do
      assert_select "input#training_characteristic_name", :name => "training_characteristic[name]"
      assert_select "input#training_characteristic_type", :name => "training_characteristic[type]"
      assert_select "input#training_characteristic_data_type_id", :name => "training_characteristic[data_type_id]"
    end
  end
end
