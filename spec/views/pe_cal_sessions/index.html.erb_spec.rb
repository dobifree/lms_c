require 'spec_helper'

describe "pe_cal_sessions/index" do
  before(:each) do
    assign(:pe_cal_sessions, [
      stub_model(PeCalSession,
        :pe_process => nil,
        :description => "MyText",
        :active => false
      ),
      stub_model(PeCalSession,
        :pe_process => nil,
        :description => "MyText",
        :active => false
      )
    ])
  end

  it "renders a list of pe_cal_sessions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
