require 'spec_helper'

describe "pe_cal_sessions/edit" do
  before(:each) do
    @pe_cal_session = assign(:pe_cal_session, stub_model(PeCalSession,
      :pe_process => nil,
      :description => "MyText",
      :active => false
    ))
  end

  it "renders the edit pe_cal_session form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_cal_sessions_path(@pe_cal_session), :method => "post" do
      assert_select "input#pe_cal_session_pe_process", :name => "pe_cal_session[pe_process]"
      assert_select "textarea#pe_cal_session_description", :name => "pe_cal_session[description]"
      assert_select "input#pe_cal_session_active", :name => "pe_cal_session[active]"
    end
  end
end
