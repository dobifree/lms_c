require 'spec_helper'

describe "pe_cal_sessions/show" do
  before(:each) do
    @pe_cal_session = assign(:pe_cal_session, stub_model(PeCalSession,
      :pe_process => nil,
      :description => "MyText",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/MyText/)
    rendered.should match(/false/)
  end
end
