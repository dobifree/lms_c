require 'spec_helper'

describe "pe_cal_sessions/new" do
  before(:each) do
    assign(:pe_cal_session, stub_model(PeCalSession,
      :pe_process => nil,
      :description => "MyText",
      :active => false
    ).as_new_record)
  end

  it "renders new pe_cal_session form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_cal_sessions_path, :method => "post" do
      assert_select "input#pe_cal_session_pe_process", :name => "pe_cal_session[pe_process]"
      assert_select "textarea#pe_cal_session_description", :name => "pe_cal_session[description]"
      assert_select "input#pe_cal_session_active", :name => "pe_cal_session[active]"
    end
  end
end
