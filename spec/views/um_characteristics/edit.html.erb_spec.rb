require 'spec_helper'

describe "um_characteristics/edit" do
  before(:each) do
    @um_characteristic = assign(:um_characteristic, stub_model(UmCharacteristic,
      :characteristic => nil
    ))
  end

  it "renders the edit um_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => um_characteristics_path(@um_characteristic), :method => "post" do
      assert_select "input#um_characteristic_characteristic", :name => "um_characteristic[characteristic]"
    end
  end
end
