require 'spec_helper'

describe "um_characteristics/index" do
  before(:each) do
    assign(:um_characteristics, [
      stub_model(UmCharacteristic,
        :characteristic => nil
      ),
      stub_model(UmCharacteristic,
        :characteristic => nil
      )
    ])
  end

  it "renders a list of um_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
