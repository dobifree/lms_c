require 'spec_helper'

describe "um_characteristics/new" do
  before(:each) do
    assign(:um_characteristic, stub_model(UmCharacteristic,
      :characteristic => nil
    ).as_new_record)
  end

  it "renders new um_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => um_characteristics_path, :method => "post" do
      assert_select "input#um_characteristic_characteristic", :name => "um_characteristic[characteristic]"
    end
  end
end
