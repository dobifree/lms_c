require 'spec_helper'

describe "master_evaluations/index" do
  before(:each) do
    assign(:master_evaluations, [
      stub_model(MasterEvaluation,
        :numero => 1,
        :nombre => "Nombre",
        :descripcion => "MyText",
        :tags => "Tags"
      ),
      stub_model(MasterEvaluation,
        :numero => 1,
        :nombre => "Nombre",
        :descripcion => "MyText",
        :tags => "Tags"
      )
    ])
  end

  it "renders a list of master_evaluations" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Tags".to_s, :count => 2
  end
end
