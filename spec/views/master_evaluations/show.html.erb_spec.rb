require 'spec_helper'

describe "master_evaluations/show" do
  before(:each) do
    @master_evaluation = assign(:master_evaluation, stub_model(MasterEvaluation,
      :numero => 1,
      :nombre => "Nombre",
      :descripcion => "MyText",
      :tags => "Tags"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Nombre/)
    rendered.should match(/MyText/)
    rendered.should match(/Tags/)
  end
end
