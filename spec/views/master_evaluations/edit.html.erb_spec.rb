require 'spec_helper'

describe "master_evaluations/edit" do
  before(:each) do
    @master_evaluation = assign(:master_evaluation, stub_model(MasterEvaluation,
      :numero => 1,
      :nombre => "MyString",
      :descripcion => "MyText",
      :tags => "MyString"
    ))
  end

  it "renders the edit master_evaluation form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => master_evaluations_path(@master_evaluation), :method => "post" do
      assert_select "input#master_evaluation_numero", :name => "master_evaluation[numero]"
      assert_select "input#master_evaluation_nombre", :name => "master_evaluation[nombre]"
      assert_select "textarea#master_evaluation_descripcion", :name => "master_evaluation[descripcion]"
      assert_select "input#master_evaluation_tags", :name => "master_evaluation[tags]"
    end
  end
end
