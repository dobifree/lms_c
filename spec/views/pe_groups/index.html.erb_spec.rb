require 'spec_helper'

describe "pe_groups/index" do
  before(:each) do
    assign(:pe_groups, [
      stub_model(PeGroup,
        :name => "Name",
        :pe_evaluation => nil
      ),
      stub_model(PeGroup,
        :name => "Name",
        :pe_evaluation => nil
      )
    ])
  end

  it "renders a list of pe_groups" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
