require 'spec_helper'

describe "pe_groups/edit" do
  before(:each) do
    @pe_group = assign(:pe_group, stub_model(PeGroup,
      :name => "MyString",
      :pe_evaluation => nil
    ))
  end

  it "renders the edit pe_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_groups_path(@pe_group), :method => "post" do
      assert_select "input#pe_group_name", :name => "pe_group[name]"
      assert_select "input#pe_group_pe_evaluation", :name => "pe_group[pe_evaluation]"
    end
  end
end
