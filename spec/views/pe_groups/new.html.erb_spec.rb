require 'spec_helper'

describe "pe_groups/new" do
  before(:each) do
    assign(:pe_group, stub_model(PeGroup,
      :name => "MyString",
      :pe_evaluation => nil
    ).as_new_record)
  end

  it "renders new pe_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_groups_path, :method => "post" do
      assert_select "input#pe_group_name", :name => "pe_group[name]"
      assert_select "input#pe_group_pe_evaluation", :name => "pe_group[pe_evaluation]"
    end
  end
end
