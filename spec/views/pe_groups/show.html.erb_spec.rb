require 'spec_helper'

describe "pe_groups/show" do
  before(:each) do
    @pe_group = assign(:pe_group, stub_model(PeGroup,
      :name => "Name",
      :pe_evaluation => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
