require 'spec_helper'

describe "characteristics/index" do
  before(:each) do
    assign(:characteristics, [
      stub_model(Characteristic,
        :nombre => "Nombre"
      ),
      stub_model(Characteristic,
        :nombre => "Nombre"
      )
    ])
  end

  it "renders a list of characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
  end
end
