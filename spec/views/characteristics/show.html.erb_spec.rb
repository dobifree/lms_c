require 'spec_helper'

describe "characteristics/show" do
  before(:each) do
    @characteristic = assign(:characteristic, stub_model(Characteristic,
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
  end
end
