require 'spec_helper'

describe "kpi_dashboard_types/new" do
  before(:each) do
    assign(:kpi_dashboard_type, stub_model(KpiDashboardType,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new kpi_dashboard_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_dashboard_types_path, :method => "post" do
      assert_select "input#kpi_dashboard_type_name", :name => "kpi_dashboard_type[name]"
    end
  end
end
