require 'spec_helper'

describe "kpi_dashboard_types/show" do
  before(:each) do
    @kpi_dashboard_type = assign(:kpi_dashboard_type, stub_model(KpiDashboardType,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
