require 'spec_helper'

describe "kpi_dashboard_types/edit" do
  before(:each) do
    @kpi_dashboard_type = assign(:kpi_dashboard_type, stub_model(KpiDashboardType,
      :name => "MyString"
    ))
  end

  it "renders the edit kpi_dashboard_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_dashboard_types_path(@kpi_dashboard_type), :method => "post" do
      assert_select "input#kpi_dashboard_type_name", :name => "kpi_dashboard_type[name]"
    end
  end
end
