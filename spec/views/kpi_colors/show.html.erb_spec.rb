require 'spec_helper'

describe "kpi_colors/show" do
  before(:each) do
    @kpi_color = assign(:kpi_color, stub_model(KpiColor,
      :kpi_dashboard => nil,
      :percentage => 1.5,
      :color => "Color"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1.5/)
    rendered.should match(/Color/)
  end
end
