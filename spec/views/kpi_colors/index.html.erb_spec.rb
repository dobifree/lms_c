require 'spec_helper'

describe "kpi_colors/index" do
  before(:each) do
    assign(:kpi_colors, [
      stub_model(KpiColor,
        :kpi_dashboard => nil,
        :percentage => 1.5,
        :color => "Color"
      ),
      stub_model(KpiColor,
        :kpi_dashboard => nil,
        :percentage => 1.5,
        :color => "Color"
      )
    ])
  end

  it "renders a list of kpi_colors" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Color".to_s, :count => 2
  end
end
