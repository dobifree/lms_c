require 'spec_helper'

describe "kpi_colors/new" do
  before(:each) do
    assign(:kpi_color, stub_model(KpiColor,
      :kpi_dashboard => nil,
      :percentage => 1.5,
      :color => "MyString"
    ).as_new_record)
  end

  it "renders new kpi_color form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_colors_path, :method => "post" do
      assert_select "input#kpi_color_kpi_dashboard", :name => "kpi_color[kpi_dashboard]"
      assert_select "input#kpi_color_percentage", :name => "kpi_color[percentage]"
      assert_select "input#kpi_color_color", :name => "kpi_color[color]"
    end
  end
end
