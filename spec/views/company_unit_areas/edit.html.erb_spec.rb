require 'spec_helper'

describe "company_unit_areas/edit" do
  before(:each) do
    @company_unit_area = assign(:company_unit_area, stub_model(CompanyUnitArea,
      :name => "MyString",
      :company_unit => nil
    ))
  end

  it "renders the edit company_unit_area form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => company_unit_areas_path(@company_unit_area), :method => "post" do
      assert_select "input#company_unit_area_name", :name => "company_unit_area[name]"
      assert_select "input#company_unit_area_company_unit", :name => "company_unit_area[company_unit]"
    end
  end
end
