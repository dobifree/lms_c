require 'spec_helper'

describe "company_unit_areas/show" do
  before(:each) do
    @company_unit_area = assign(:company_unit_area, stub_model(CompanyUnitArea,
      :name => "Name",
      :company_unit => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
