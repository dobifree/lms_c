require 'spec_helper'

describe "company_unit_areas/index" do
  before(:each) do
    assign(:company_unit_areas, [
      stub_model(CompanyUnitArea,
        :name => "Name",
        :company_unit => nil
      ),
      stub_model(CompanyUnitArea,
        :name => "Name",
        :company_unit => nil
      )
    ])
  end

  it "renders a list of company_unit_areas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
