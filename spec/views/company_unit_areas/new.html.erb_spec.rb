require 'spec_helper'

describe "company_unit_areas/new" do
  before(:each) do
    assign(:company_unit_area, stub_model(CompanyUnitArea,
      :name => "MyString",
      :company_unit => nil
    ).as_new_record)
  end

  it "renders new company_unit_area form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => company_unit_areas_path, :method => "post" do
      assert_select "input#company_unit_area_name", :name => "company_unit_area[name]"
      assert_select "input#company_unit_area_company_unit", :name => "company_unit_area[company_unit]"
    end
  end
end
