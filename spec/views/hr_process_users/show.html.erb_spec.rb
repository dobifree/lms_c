require 'spec_helper'

describe "hr_process_users/show" do
  before(:each) do
    @hr_process_user = assign(:hr_process_user, stub_model(HrProcessUser,
      :hr_process => nil,
      :user => nil,
      :hr_process_level => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
  end
end
