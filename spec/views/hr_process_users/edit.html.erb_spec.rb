require 'spec_helper'

describe "hr_process_users/edit" do
  before(:each) do
    @hr_process_user = assign(:hr_process_user, stub_model(HrProcessUser,
      :hr_process => nil,
      :user => nil,
      :hr_process_level => nil
    ))
  end

  it "renders the edit hr_process_user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_users_path(@hr_process_user), :method => "post" do
      assert_select "input#hr_process_user_hr_process", :name => "hr_process_user[hr_process]"
      assert_select "input#hr_process_user_user", :name => "hr_process_user[user]"
      assert_select "input#hr_process_user_hr_process_level", :name => "hr_process_user[hr_process_level]"
    end
  end
end
