require 'spec_helper'

describe "hr_process_users/new" do
  before(:each) do
    assign(:hr_process_user, stub_model(HrProcessUser,
      :hr_process => nil,
      :user => nil,
      :hr_process_level => nil
    ).as_new_record)
  end

  it "renders new hr_process_user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_users_path, :method => "post" do
      assert_select "input#hr_process_user_hr_process", :name => "hr_process_user[hr_process]"
      assert_select "input#hr_process_user_user", :name => "hr_process_user[user]"
      assert_select "input#hr_process_user_hr_process_level", :name => "hr_process_user[hr_process_level]"
    end
  end
end
