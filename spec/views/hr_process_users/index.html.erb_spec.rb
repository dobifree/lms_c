require 'spec_helper'

describe "hr_process_users/index" do
  before(:each) do
    assign(:hr_process_users, [
      stub_model(HrProcessUser,
        :hr_process => nil,
        :user => nil,
        :hr_process_level => nil
      ),
      stub_model(HrProcessUser,
        :hr_process => nil,
        :user => nil,
        :hr_process_level => nil
      )
    ])
  end

  it "renders a list of hr_process_users" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
