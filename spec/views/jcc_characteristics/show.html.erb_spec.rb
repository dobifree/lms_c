require 'spec_helper'

describe "jcc_characteristics/show" do
  before(:each) do
    @jcc_characteristic = assign(:jcc_characteristic, stub_model(JccCharacteristic,
      :j_cost_center => nil,
      :characteristic => nil,
      :value_string => "Value String",
      :characteristic_value => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/Value String/)
    rendered.should match(//)
  end
end
