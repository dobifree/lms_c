require 'spec_helper'

describe "jcc_characteristics/index" do
  before(:each) do
    assign(:jcc_characteristics, [
      stub_model(JccCharacteristic,
        :j_cost_center => nil,
        :characteristic => nil,
        :value_string => "Value String",
        :characteristic_value => nil
      ),
      stub_model(JccCharacteristic,
        :j_cost_center => nil,
        :characteristic => nil,
        :value_string => "Value String",
        :characteristic_value => nil
      )
    ])
  end

  it "renders a list of jcc_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Value String".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
