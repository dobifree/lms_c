require 'spec_helper'

describe "jcc_characteristics/edit" do
  before(:each) do
    @jcc_characteristic = assign(:jcc_characteristic, stub_model(JccCharacteristic,
      :j_cost_center => nil,
      :characteristic => nil,
      :value_string => "MyString",
      :characteristic_value => nil
    ))
  end

  it "renders the edit jcc_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => jcc_characteristics_path(@jcc_characteristic), :method => "post" do
      assert_select "input#jcc_characteristic_j_cost_center", :name => "jcc_characteristic[j_cost_center]"
      assert_select "input#jcc_characteristic_characteristic", :name => "jcc_characteristic[characteristic]"
      assert_select "input#jcc_characteristic_value_string", :name => "jcc_characteristic[value_string]"
      assert_select "input#jcc_characteristic_characteristic_value", :name => "jcc_characteristic[characteristic_value]"
    end
  end
end
