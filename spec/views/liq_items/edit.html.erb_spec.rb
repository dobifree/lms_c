require 'spec_helper'

describe "liq_items/edit" do
  before(:each) do
    @liq_item = assign(:liq_item, stub_model(LiqItem,
      :liq_process => nil,
      :user => nil,
      :secu_cohade => nil,
      :description => "MyString",
      :monto => 1
    ))
  end

  it "renders the edit liq_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => liq_items_path(@liq_item), :method => "post" do
      assert_select "input#liq_item_liq_process", :name => "liq_item[liq_process]"
      assert_select "input#liq_item_user", :name => "liq_item[user]"
      assert_select "input#liq_item_secu_cohade", :name => "liq_item[secu_cohade]"
      assert_select "input#liq_item_description", :name => "liq_item[description]"
      assert_select "input#liq_item_monto", :name => "liq_item[monto]"
    end
  end
end
