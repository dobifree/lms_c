require 'spec_helper'

describe "liq_items/show" do
  before(:each) do
    @liq_item = assign(:liq_item, stub_model(LiqItem,
      :liq_process => nil,
      :user => nil,
      :secu_cohade => nil,
      :description => "Description",
      :monto => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/Description/)
    rendered.should match(/1/)
  end
end
