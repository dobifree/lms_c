require 'spec_helper'

describe "liq_items/new" do
  before(:each) do
    assign(:liq_item, stub_model(LiqItem,
      :liq_process => nil,
      :user => nil,
      :secu_cohade => nil,
      :description => "MyString",
      :monto => 1
    ).as_new_record)
  end

  it "renders new liq_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => liq_items_path, :method => "post" do
      assert_select "input#liq_item_liq_process", :name => "liq_item[liq_process]"
      assert_select "input#liq_item_user", :name => "liq_item[user]"
      assert_select "input#liq_item_secu_cohade", :name => "liq_item[secu_cohade]"
      assert_select "input#liq_item_description", :name => "liq_item[description]"
      assert_select "input#liq_item_monto", :name => "liq_item[monto]"
    end
  end
end
