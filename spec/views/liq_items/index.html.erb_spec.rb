require 'spec_helper'

describe "liq_items/index" do
  before(:each) do
    assign(:liq_items, [
      stub_model(LiqItem,
        :liq_process => nil,
        :user => nil,
        :secu_cohade => nil,
        :description => "Description",
        :monto => 1
      ),
      stub_model(LiqItem,
        :liq_process => nil,
        :user => nil,
        :secu_cohade => nil,
        :description => "Description",
        :monto => 1
      )
    ])
  end

  it "renders a list of liq_items" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
