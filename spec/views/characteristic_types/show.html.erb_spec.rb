require 'spec_helper'

describe "characteristic_types/show" do
  before(:each) do
    @characteristic_type = assign(:characteristic_type, stub_model(CharacteristicType,
      :position => 1,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
  end
end
