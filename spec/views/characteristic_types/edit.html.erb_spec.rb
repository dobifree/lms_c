require 'spec_helper'

describe "characteristic_types/edit" do
  before(:each) do
    @characteristic_type = assign(:characteristic_type, stub_model(CharacteristicType,
      :position => 1,
      :name => "MyString"
    ))
  end

  it "renders the edit characteristic_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => characteristic_types_path(@characteristic_type), :method => "post" do
      assert_select "input#characteristic_type_position", :name => "characteristic_type[position]"
      assert_select "input#characteristic_type_name", :name => "characteristic_type[name]"
    end
  end
end
