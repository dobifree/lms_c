require 'spec_helper'

describe "characteristic_types/index" do
  before(:each) do
    assign(:characteristic_types, [
      stub_model(CharacteristicType,
        :position => 1,
        :name => "Name"
      ),
      stub_model(CharacteristicType,
        :position => 1,
        :name => "Name"
      )
    ])
  end

  it "renders a list of characteristic_types" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
