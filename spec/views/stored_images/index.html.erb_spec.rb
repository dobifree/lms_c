require 'spec_helper'

describe "stored_images/index" do
  before(:each) do
    assign(:stored_images, [
      stub_model(StoredImage,
        :name => "Name",
        :crypted_name => "Crypted Name"
      ),
      stub_model(StoredImage,
        :name => "Name",
        :crypted_name => "Crypted Name"
      )
    ])
  end

  it "renders a list of stored_images" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Crypted Name".to_s, :count => 2
  end
end
