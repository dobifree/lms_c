require 'spec_helper'

describe "stored_images/show" do
  before(:each) do
    @stored_image = assign(:stored_image, stub_model(StoredImage,
      :name => "Name",
      :crypted_name => "Crypted Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Crypted Name/)
  end
end
