require 'spec_helper'

describe "stored_images/edit" do
  before(:each) do
    @stored_image = assign(:stored_image, stub_model(StoredImage,
      :name => "MyString",
      :crypted_name => "MyString"
    ))
  end

  it "renders the edit stored_image form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => stored_images_path(@stored_image), :method => "post" do
      assert_select "input#stored_image_name", :name => "stored_image[name]"
      assert_select "input#stored_image_crypted_name", :name => "stored_image[crypted_name]"
    end
  end
end
