require 'spec_helper'

describe "stored_images/new" do
  before(:each) do
    assign(:stored_image, stub_model(StoredImage,
      :name => "MyString",
      :crypted_name => "MyString"
    ).as_new_record)
  end

  it "renders new stored_image form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => stored_images_path, :method => "post" do
      assert_select "input#stored_image_name", :name => "stored_image[name]"
      assert_select "input#stored_image_crypted_name", :name => "stored_image[crypted_name]"
    end
  end
end
