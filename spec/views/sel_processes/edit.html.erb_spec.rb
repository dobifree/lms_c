require 'spec_helper'

describe "sel_processes/edit" do
  before(:each) do
    @sel_process = assign(:sel_process, stub_model(SelProcess,
      :sel_template => nil,
      :name => "MyString",
      :description => "MyText",
      :qty_required => 1,
      :source => 1,
      :apply_available => false,
      :owner_user_id => 1
    ))
  end

  it "renders the edit sel_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_processes_path(@sel_process), :method => "post" do
      assert_select "input#sel_process_sel_template", :name => "sel_process[sel_template]"
      assert_select "input#sel_process_name", :name => "sel_process[name]"
      assert_select "textarea#sel_process_description", :name => "sel_process[description]"
      assert_select "input#sel_process_qty_required", :name => "sel_process[qty_required]"
      assert_select "input#sel_process_source", :name => "sel_process[source]"
      assert_select "input#sel_process_apply_available", :name => "sel_process[apply_available]"
      assert_select "input#sel_process_owner_user_id", :name => "sel_process[owner_user_id]"
    end
  end
end
