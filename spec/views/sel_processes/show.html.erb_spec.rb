require 'spec_helper'

describe "sel_processes/show" do
  before(:each) do
    @sel_process = assign(:sel_process, stub_model(SelProcess,
      :sel_template => nil,
      :name => "Name",
      :description => "MyText",
      :qty_required => 1,
      :source => 2,
      :apply_available => false,
      :owner_user_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/false/)
    rendered.should match(/3/)
  end
end
