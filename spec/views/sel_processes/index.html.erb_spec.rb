require 'spec_helper'

describe "sel_processes/index" do
  before(:each) do
    assign(:sel_processes, [
      stub_model(SelProcess,
        :sel_template => nil,
        :name => "Name",
        :description => "MyText",
        :qty_required => 1,
        :source => 2,
        :apply_available => false,
        :owner_user_id => 3
      ),
      stub_model(SelProcess,
        :sel_template => nil,
        :name => "Name",
        :description => "MyText",
        :qty_required => 1,
        :source => 2,
        :apply_available => false,
        :owner_user_id => 3
      )
    ])
  end

  it "renders a list of sel_processes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
