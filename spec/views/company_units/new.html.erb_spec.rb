require 'spec_helper'

describe "company_units/new" do
  before(:each) do
    assign(:company_unit, stub_model(CompanyUnit,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new company_unit form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => company_units_path, :method => "post" do
      assert_select "input#company_unit_name", :name => "company_unit[name]"
    end
  end
end
