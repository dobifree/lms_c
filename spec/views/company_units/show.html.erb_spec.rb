require 'spec_helper'

describe "company_units/show" do
  before(:each) do
    @company_unit = assign(:company_unit, stub_model(CompanyUnit,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
