require 'spec_helper'

describe "company_units/edit" do
  before(:each) do
    @company_unit = assign(:company_unit, stub_model(CompanyUnit,
      :name => "MyString"
    ))
  end

  it "renders the edit company_unit form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => company_units_path(@company_unit), :method => "post" do
      assert_select "input#company_unit_name", :name => "company_unit[name]"
    end
  end
end
