require 'spec_helper'

describe "company_units/index" do
  before(:each) do
    assign(:company_units, [
      stub_model(CompanyUnit,
        :name => "Name"
      ),
      stub_model(CompanyUnit,
        :name => "Name"
      )
    ])
  end

  it "renders a list of company_units" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
