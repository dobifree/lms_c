require 'spec_helper'

describe "master_units/new" do
  before(:each) do
    assign(:master_unit, stub_model(MasterUnit,
      :numero => 1,
      :nombre => "MyString",
      :descripcion => "MyText",
      :tags => "MyString"
    ).as_new_record)
  end

  it "renders new master_unit form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => master_units_path, :method => "post" do
      assert_select "input#master_unit_numero", :name => "master_unit[numero]"
      assert_select "input#master_unit_nombre", :name => "master_unit[nombre]"
      assert_select "textarea#master_unit_descripcion", :name => "master_unit[descripcion]"
      assert_select "input#master_unit_tags", :name => "master_unit[tags]"
    end
  end
end
