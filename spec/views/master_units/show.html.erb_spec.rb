require 'spec_helper'

describe "master_units/show" do
  before(:each) do
    @master_unit = assign(:master_unit, stub_model(MasterUnit,
      :numero => 1,
      :nombre => "Nombre",
      :descripcion => "MyText",
      :tags => "Tags"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Nombre/)
    rendered.should match(/MyText/)
    rendered.should match(/Tags/)
  end
end
