require 'spec_helper'

describe "master_units/edit" do
  before(:each) do
    @master_unit = assign(:master_unit, stub_model(MasterUnit,
      :numero => 1,
      :nombre => "MyString",
      :descripcion => "MyText",
      :tags => "MyString"
    ))
  end

  it "renders the edit master_unit form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => master_units_path(@master_unit), :method => "post" do
      assert_select "input#master_unit_numero", :name => "master_unit[numero]"
      assert_select "input#master_unit_nombre", :name => "master_unit[nombre]"
      assert_select "textarea#master_unit_descripcion", :name => "master_unit[descripcion]"
      assert_select "input#master_unit_tags", :name => "master_unit[tags]"
    end
  end
end
