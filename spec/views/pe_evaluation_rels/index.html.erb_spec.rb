require 'spec_helper'

describe "pe_evaluation_rels/index" do
  before(:each) do
    assign(:pe_evaluation_rels, [
      stub_model(PeEvaluationRel,
        :pe_evaluation => nil,
        :pe_rel => nil,
        :weight => 1.5
      ),
      stub_model(PeEvaluationRel,
        :pe_evaluation => nil,
        :pe_rel => nil,
        :weight => 1.5
      )
    ])
  end

  it "renders a list of pe_evaluation_rels" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
