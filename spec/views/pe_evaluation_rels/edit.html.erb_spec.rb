require 'spec_helper'

describe "pe_evaluation_rels/edit" do
  before(:each) do
    @pe_evaluation_rel = assign(:pe_evaluation_rel, stub_model(PeEvaluationRel,
      :pe_evaluation => nil,
      :pe_rel => nil,
      :weight => 1.5
    ))
  end

  it "renders the edit pe_evaluation_rel form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_evaluation_rels_path(@pe_evaluation_rel), :method => "post" do
      assert_select "input#pe_evaluation_rel_pe_evaluation", :name => "pe_evaluation_rel[pe_evaluation]"
      assert_select "input#pe_evaluation_rel_pe_rel", :name => "pe_evaluation_rel[pe_rel]"
      assert_select "input#pe_evaluation_rel_weight", :name => "pe_evaluation_rel[weight]"
    end
  end
end
