require 'spec_helper'

describe "pe_evaluation_rels/show" do
  before(:each) do
    @pe_evaluation_rel = assign(:pe_evaluation_rel, stub_model(PeEvaluationRel,
      :pe_evaluation => nil,
      :pe_rel => nil,
      :weight => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/1.5/)
  end
end
