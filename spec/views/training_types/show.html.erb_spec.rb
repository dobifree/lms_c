require 'spec_helper'

describe "training_types/show" do
  before(:each) do
    @training_type = assign(:training_type, stub_model(TrainingType,
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
  end
end
