require 'spec_helper'

describe "training_types/index" do
  before(:each) do
    assign(:training_types, [
      stub_model(TrainingType,
        :nombre => "Nombre"
      ),
      stub_model(TrainingType,
        :nombre => "Nombre"
      )
    ])
  end

  it "renders a list of training_types" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
  end
end
