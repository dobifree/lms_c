require 'spec_helper'

describe "training_types/new" do
  before(:each) do
    assign(:training_type, stub_model(TrainingType,
      :nombre => "MyString"
    ).as_new_record)
  end

  it "renders new training_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_types_path, :method => "post" do
      assert_select "input#training_type_nombre", :name => "training_type[nombre]"
    end
  end
end
