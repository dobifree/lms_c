require 'spec_helper'

describe "training_types/edit" do
  before(:each) do
    @training_type = assign(:training_type, stub_model(TrainingType,
      :nombre => "MyString"
    ))
  end

  it "renders the edit training_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_types_path(@training_type), :method => "post" do
      assert_select "input#training_type_nombre", :name => "training_type[nombre]"
    end
  end
end
