require 'spec_helper'

describe "pe_member_groups/show" do
  before(:each) do
    @pe_member_group = assign(:pe_member_group, stub_model(PeMemberGroup,
      :pe_process => nil,
      :pe_member => nil,
      :pe_evaluation => nil,
      :pe_group => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
  end
end
