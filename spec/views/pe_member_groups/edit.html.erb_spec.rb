require 'spec_helper'

describe "pe_member_groups/edit" do
  before(:each) do
    @pe_member_group = assign(:pe_member_group, stub_model(PeMemberGroup,
      :pe_process => nil,
      :pe_member => nil,
      :pe_evaluation => nil,
      :pe_group => nil
    ))
  end

  it "renders the edit pe_member_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_member_groups_path(@pe_member_group), :method => "post" do
      assert_select "input#pe_member_group_pe_process", :name => "pe_member_group[pe_process]"
      assert_select "input#pe_member_group_pe_member", :name => "pe_member_group[pe_member]"
      assert_select "input#pe_member_group_pe_evaluation", :name => "pe_member_group[pe_evaluation]"
      assert_select "input#pe_member_group_pe_group", :name => "pe_member_group[pe_group]"
    end
  end
end
