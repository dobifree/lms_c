require 'spec_helper'

describe "pe_member_groups/index" do
  before(:each) do
    assign(:pe_member_groups, [
      stub_model(PeMemberGroup,
        :pe_process => nil,
        :pe_member => nil,
        :pe_evaluation => nil,
        :pe_group => nil
      ),
      stub_model(PeMemberGroup,
        :pe_process => nil,
        :pe_member => nil,
        :pe_evaluation => nil,
        :pe_group => nil
      )
    ])
  end

  it "renders a list of pe_member_groups" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
