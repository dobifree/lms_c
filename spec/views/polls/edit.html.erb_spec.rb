require 'spec_helper'

describe "polls/edit" do
  before(:each) do
    @poll = assign(:poll, stub_model(Poll,
      :nombre => "MyString",
      :descripcion => "MyString",
      :program => nil,
      :level => nil,
      :course => nil
    ))
  end

  it "renders the edit poll form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => polls_path(@poll), :method => "post" do
      assert_select "input#poll_nombre", :name => "poll[nombre]"
      assert_select "input#poll_descripcion", :name => "poll[descripcion]"
      assert_select "input#poll_program", :name => "poll[program]"
      assert_select "input#poll_level", :name => "poll[level]"
      assert_select "input#poll_course", :name => "poll[course]"
    end
  end
end
