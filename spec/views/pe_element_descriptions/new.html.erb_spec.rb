require 'spec_helper'

describe "pe_element_descriptions/new" do
  before(:each) do
    assign(:pe_element_description, stub_model(PeElementDescription,
      :position => 1,
      :name => "MyString",
      :pe_element => nil
    ).as_new_record)
  end

  it "renders new pe_element_description form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_element_descriptions_path, :method => "post" do
      assert_select "input#pe_element_description_position", :name => "pe_element_description[position]"
      assert_select "input#pe_element_description_name", :name => "pe_element_description[name]"
      assert_select "input#pe_element_description_pe_element", :name => "pe_element_description[pe_element]"
    end
  end
end
