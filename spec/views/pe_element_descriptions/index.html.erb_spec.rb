require 'spec_helper'

describe "pe_element_descriptions/index" do
  before(:each) do
    assign(:pe_element_descriptions, [
      stub_model(PeElementDescription,
        :position => 1,
        :name => "Name",
        :pe_element => nil
      ),
      stub_model(PeElementDescription,
        :position => 1,
        :name => "Name",
        :pe_element => nil
      )
    ])
  end

  it "renders a list of pe_element_descriptions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
