require 'spec_helper'

describe "pe_element_descriptions/show" do
  before(:each) do
    @pe_element_description = assign(:pe_element_description, stub_model(PeElementDescription,
      :position => 1,
      :name => "Name",
      :pe_element => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
