require 'spec_helper'

describe "pe_question_activity_fields/show" do
  before(:each) do
    @pe_question_activity_field = assign(:pe_question_activity_field, stub_model(PeQuestionActivityField,
      :pe_evaluation => nil,
      :position => 1,
      :name => "Name",
      :field_type => 2,
      :pe_question_activity_field_list => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/2/)
    rendered.should match(//)
  end
end
