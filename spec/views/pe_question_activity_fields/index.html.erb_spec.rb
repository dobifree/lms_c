require 'spec_helper'

describe "pe_question_activity_fields/index" do
  before(:each) do
    assign(:pe_question_activity_fields, [
      stub_model(PeQuestionActivityField,
        :pe_evaluation => nil,
        :position => 1,
        :name => "Name",
        :field_type => 2,
        :pe_question_activity_field_list => nil
      ),
      stub_model(PeQuestionActivityField,
        :pe_evaluation => nil,
        :position => 1,
        :name => "Name",
        :field_type => 2,
        :pe_question_activity_field_list => nil
      )
    ])
  end

  it "renders a list of pe_question_activity_fields" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
