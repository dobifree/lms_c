require 'spec_helper'

describe "pe_question_activity_fields/edit" do
  before(:each) do
    @pe_question_activity_field = assign(:pe_question_activity_field, stub_model(PeQuestionActivityField,
      :pe_evaluation => nil,
      :position => 1,
      :name => "MyString",
      :field_type => 1,
      :pe_question_activity_field_list => nil
    ))
  end

  it "renders the edit pe_question_activity_field form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_question_activity_fields_path(@pe_question_activity_field), :method => "post" do
      assert_select "input#pe_question_activity_field_pe_evaluation", :name => "pe_question_activity_field[pe_evaluation]"
      assert_select "input#pe_question_activity_field_position", :name => "pe_question_activity_field[position]"
      assert_select "input#pe_question_activity_field_name", :name => "pe_question_activity_field[name]"
      assert_select "input#pe_question_activity_field_field_type", :name => "pe_question_activity_field[field_type]"
      assert_select "input#pe_question_activity_field_pe_question_activity_field_list", :name => "pe_question_activity_field[pe_question_activity_field_list]"
    end
  end
end
