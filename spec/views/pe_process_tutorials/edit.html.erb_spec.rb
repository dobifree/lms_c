require 'spec_helper'

describe "pe_process_tutorials/edit" do
  before(:each) do
    @pe_process_tutorial = assign(:pe_process_tutorial, stub_model(PeProcessTutorial,
      :pe_process => nil,
      :tutorial => nil,
      :name => "MyString",
      :active => false
    ))
  end

  it "renders the edit pe_process_tutorial form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_process_tutorials_path(@pe_process_tutorial), :method => "post" do
      assert_select "input#pe_process_tutorial_pe_process", :name => "pe_process_tutorial[pe_process]"
      assert_select "input#pe_process_tutorial_tutorial", :name => "pe_process_tutorial[tutorial]"
      assert_select "input#pe_process_tutorial_name", :name => "pe_process_tutorial[name]"
      assert_select "input#pe_process_tutorial_active", :name => "pe_process_tutorial[active]"
    end
  end
end
