require 'spec_helper'

describe "pe_process_tutorials/show" do
  before(:each) do
    @pe_process_tutorial = assign(:pe_process_tutorial, stub_model(PeProcessTutorial,
      :pe_process => nil,
      :tutorial => nil,
      :name => "Name",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/false/)
  end
end
