require 'spec_helper'

describe "pe_process_tutorials/index" do
  before(:each) do
    assign(:pe_process_tutorials, [
      stub_model(PeProcessTutorial,
        :pe_process => nil,
        :tutorial => nil,
        :name => "Name",
        :active => false
      ),
      stub_model(PeProcessTutorial,
        :pe_process => nil,
        :tutorial => nil,
        :name => "Name",
        :active => false
      )
    ])
  end

  it "renders a list of pe_process_tutorials" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
