require 'spec_helper'

describe "log_systems/show" do
  before(:each) do
    @log_system = assign(:log_system, stub_model(LogSystem,
      :index => "Index"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Index/)
  end
end
