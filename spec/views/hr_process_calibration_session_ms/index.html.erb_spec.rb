require 'spec_helper'

describe "hr_process_calibration_session_ms/index" do
  before(:each) do
    assign(:hr_process_calibration_session_ms, [
      stub_model(HrProcessCalibrationSessionM,
        :hr_process_calibration_session => nil,
        :hr_process_user => nil
      ),
      stub_model(HrProcessCalibrationSessionM,
        :hr_process_calibration_session => nil,
        :hr_process_user => nil
      )
    ])
  end

  it "renders a list of hr_process_calibration_session_ms" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
