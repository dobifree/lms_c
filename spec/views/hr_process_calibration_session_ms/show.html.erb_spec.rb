require 'spec_helper'

describe "hr_process_calibration_session_ms/show" do
  before(:each) do
    @hr_process_calibration_session_m = assign(:hr_process_calibration_session_m, stub_model(HrProcessCalibrationSessionM,
      :hr_process_calibration_session => nil,
      :hr_process_user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
