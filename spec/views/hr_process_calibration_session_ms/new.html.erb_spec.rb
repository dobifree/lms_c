require 'spec_helper'

describe "hr_process_calibration_session_ms/new" do
  before(:each) do
    assign(:hr_process_calibration_session_m, stub_model(HrProcessCalibrationSessionM,
      :hr_process_calibration_session => nil,
      :hr_process_user => nil
    ).as_new_record)
  end

  it "renders new hr_process_calibration_session_m form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_calibration_session_ms_path, :method => "post" do
      assert_select "input#hr_process_calibration_session_m_hr_process_calibration_session", :name => "hr_process_calibration_session_m[hr_process_calibration_session]"
      assert_select "input#hr_process_calibration_session_m_hr_process_user", :name => "hr_process_calibration_session_m[hr_process_user]"
    end
  end
end
