require 'spec_helper'

describe "hr_evaluation_types/index" do
  before(:each) do
    assign(:hr_evaluation_types, [
      stub_model(HrEvaluationType,
        :nombre => "Nombre",
        :activa => false
      ),
      stub_model(HrEvaluationType,
        :nombre => "Nombre",
        :activa => false
      )
    ])
  end

  it "renders a list of hr_evaluation_types" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
