require 'spec_helper'

describe "hr_evaluation_types/edit" do
  before(:each) do
    @hr_evaluation_type = assign(:hr_evaluation_type, stub_model(HrEvaluationType,
      :nombre => "MyString",
      :activa => false
    ))
  end

  it "renders the edit hr_evaluation_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_evaluation_types_path(@hr_evaluation_type), :method => "post" do
      assert_select "input#hr_evaluation_type_nombre", :name => "hr_evaluation_type[nombre]"
      assert_select "input#hr_evaluation_type_activa", :name => "hr_evaluation_type[activa]"
    end
  end
end
