require 'spec_helper'

describe "hr_evaluation_types/new" do
  before(:each) do
    assign(:hr_evaluation_type, stub_model(HrEvaluationType,
      :nombre => "MyString",
      :activa => false
    ).as_new_record)
  end

  it "renders new hr_evaluation_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_evaluation_types_path, :method => "post" do
      assert_select "input#hr_evaluation_type_nombre", :name => "hr_evaluation_type[nombre]"
      assert_select "input#hr_evaluation_type_activa", :name => "hr_evaluation_type[activa]"
    end
  end
end
