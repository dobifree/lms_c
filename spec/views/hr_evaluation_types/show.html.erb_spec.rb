require 'spec_helper'

describe "hr_evaluation_types/show" do
  before(:each) do
    @hr_evaluation_type = assign(:hr_evaluation_type, stub_model(HrEvaluationType,
      :nombre => "Nombre",
      :activa => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
    rendered.should match(/false/)
  end
end
