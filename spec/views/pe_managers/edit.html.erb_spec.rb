require 'spec_helper'

describe "pe_managers/edit" do
  before(:each) do
    @pe_manager = assign(:pe_manager, stub_model(PeManager,
      :pe_process => nil,
      :user => nil
    ))
  end

  it "renders the edit pe_manager form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_managers_path(@pe_manager), :method => "post" do
      assert_select "input#pe_manager_pe_process", :name => "pe_manager[pe_process]"
      assert_select "input#pe_manager_user", :name => "pe_manager[user]"
    end
  end
end
