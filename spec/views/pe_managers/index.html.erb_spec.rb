require 'spec_helper'

describe "pe_managers/index" do
  before(:each) do
    assign(:pe_managers, [
      stub_model(PeManager,
        :pe_process => nil,
        :user => nil
      ),
      stub_model(PeManager,
        :pe_process => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of pe_managers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
