require 'spec_helper'

describe "pe_managers/new" do
  before(:each) do
    assign(:pe_manager, stub_model(PeManager,
      :pe_process => nil,
      :user => nil
    ).as_new_record)
  end

  it "renders new pe_manager form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_managers_path, :method => "post" do
      assert_select "input#pe_manager_pe_process", :name => "pe_manager[pe_process]"
      assert_select "input#pe_manager_user", :name => "pe_manager[user]"
    end
  end
end
