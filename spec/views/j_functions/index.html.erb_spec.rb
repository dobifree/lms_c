require 'spec_helper'

describe "j_functions/index" do
  before(:each) do
    assign(:j_functions, [
      stub_model(JFunction,
        :description => "MyText",
        :main_function => false,
        :j_job => nil
      ),
      stub_model(JFunction,
        :description => "MyText",
        :main_function => false,
        :j_job => nil
      )
    ])
  end

  it "renders a list of j_functions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
