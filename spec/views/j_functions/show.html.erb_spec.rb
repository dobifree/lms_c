require 'spec_helper'

describe "j_functions/show" do
  before(:each) do
    @j_function = assign(:j_function, stub_model(JFunction,
      :description => "MyText",
      :main_function => false,
      :j_job => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    rendered.should match(/false/)
    rendered.should match(//)
  end
end
