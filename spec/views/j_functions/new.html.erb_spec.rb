require 'spec_helper'

describe "j_functions/new" do
  before(:each) do
    assign(:j_function, stub_model(JFunction,
      :description => "MyText",
      :main_function => false,
      :j_job => nil
    ).as_new_record)
  end

  it "renders new j_function form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_functions_path, :method => "post" do
      assert_select "textarea#j_function_description", :name => "j_function[description]"
      assert_select "input#j_function_main_function", :name => "j_function[main_function]"
      assert_select "input#j_function_j_job", :name => "j_function[j_job]"
    end
  end
end
