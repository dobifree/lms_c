require 'spec_helper'

describe "ct_module_managers/edit" do
  before(:each) do
    @ct_module_manager = assign(:ct_module_manager, stub_model(CtModuleManager,
      :ct_module => nil,
      :user => nil
    ))
  end

  it "renders the edit ct_module_manager form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => ct_module_managers_path(@ct_module_manager), :method => "post" do
      assert_select "input#ct_module_manager_ct_module", :name => "ct_module_manager[ct_module]"
      assert_select "input#ct_module_manager_user", :name => "ct_module_manager[user]"
    end
  end
end
