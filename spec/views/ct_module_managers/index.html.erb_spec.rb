require 'spec_helper'

describe "ct_module_managers/index" do
  before(:each) do
    assign(:ct_module_managers, [
      stub_model(CtModuleManager,
        :ct_module => nil,
        :user => nil
      ),
      stub_model(CtModuleManager,
        :ct_module => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of ct_module_managers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
