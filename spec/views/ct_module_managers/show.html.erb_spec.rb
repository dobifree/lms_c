require 'spec_helper'

describe "ct_module_managers/show" do
  before(:each) do
    @ct_module_manager = assign(:ct_module_manager, stub_model(CtModuleManager,
      :ct_module => nil,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
