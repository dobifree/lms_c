require 'spec_helper'

describe "ct_module_managers/new" do
  before(:each) do
    assign(:ct_module_manager, stub_model(CtModuleManager,
      :ct_module => nil,
      :user => nil
    ).as_new_record)
  end

  it "renders new ct_module_manager form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => ct_module_managers_path, :method => "post" do
      assert_select "input#ct_module_manager_ct_module", :name => "ct_module_manager[ct_module]"
      assert_select "input#ct_module_manager_user", :name => "ct_module_manager[user]"
    end
  end
end
