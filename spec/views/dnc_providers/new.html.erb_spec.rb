require 'spec_helper'

describe "dnc_providers/new" do
  before(:each) do
    assign(:dnc_provider, stub_model(DncProvider,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new dnc_provider form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_providers_path, :method => "post" do
      assert_select "input#dnc_provider_name", :name => "dnc_provider[name]"
    end
  end
end
