require 'spec_helper'

describe "dnc_providers/show" do
  before(:each) do
    @dnc_provider = assign(:dnc_provider, stub_model(DncProvider,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
