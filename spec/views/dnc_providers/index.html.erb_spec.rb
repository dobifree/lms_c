require 'spec_helper'

describe "dnc_providers/index" do
  before(:each) do
    assign(:dnc_providers, [
      stub_model(DncProvider,
        :name => "Name"
      ),
      stub_model(DncProvider,
        :name => "Name"
      )
    ])
  end

  it "renders a list of dnc_providers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
