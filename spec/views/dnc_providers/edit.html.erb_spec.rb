require 'spec_helper'

describe "dnc_providers/edit" do
  before(:each) do
    @dnc_provider = assign(:dnc_provider, stub_model(DncProvider,
      :name => "MyString"
    ))
  end

  it "renders the edit dnc_provider form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_providers_path(@dnc_provider), :method => "post" do
      assert_select "input#dnc_provider_name", :name => "dnc_provider[name]"
    end
  end
end
