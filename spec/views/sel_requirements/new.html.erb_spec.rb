require 'spec_helper'

describe "sel_requirements/new" do
  before(:each) do
    assign(:sel_requirement, stub_model(SelRequirement,
      :sel_req_template => nil,
      :registered_by_user_id => 1,
      :done => false,
      :done_by_user_id => 1,
      :declined => false,
      :declined_comment => "MyText",
      :declined_by_user_id => 1
    ).as_new_record)
  end

  it "renders new sel_requirement form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_requirements_path, :method => "post" do
      assert_select "input#sel_requirement_sel_req_template", :name => "sel_requirement[sel_req_template]"
      assert_select "input#sel_requirement_registered_by_user_id", :name => "sel_requirement[registered_by_user_id]"
      assert_select "input#sel_requirement_done", :name => "sel_requirement[done]"
      assert_select "input#sel_requirement_done_by_user_id", :name => "sel_requirement[done_by_user_id]"
      assert_select "input#sel_requirement_declined", :name => "sel_requirement[declined]"
      assert_select "textarea#sel_requirement_declined_comment", :name => "sel_requirement[declined_comment]"
      assert_select "input#sel_requirement_declined_by_user_id", :name => "sel_requirement[declined_by_user_id]"
    end
  end
end
