require 'spec_helper'

describe "sel_requirements/index" do
  before(:each) do
    assign(:sel_requirements, [
      stub_model(SelRequirement,
        :sel_req_template => nil,
        :registered_by_user_id => 1,
        :done => false,
        :done_by_user_id => 2,
        :declined => false,
        :declined_comment => "MyText",
        :declined_by_user_id => 3
      ),
      stub_model(SelRequirement,
        :sel_req_template => nil,
        :registered_by_user_id => 1,
        :done => false,
        :done_by_user_id => 2,
        :declined => false,
        :declined_comment => "MyText",
        :declined_by_user_id => 3
      )
    ])
  end

  it "renders a list of sel_requirements" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
