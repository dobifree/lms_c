require 'spec_helper'

describe "sel_requirements/show" do
  before(:each) do
    @sel_requirement = assign(:sel_requirement, stub_model(SelRequirement,
      :sel_req_template => nil,
      :registered_by_user_id => 1,
      :done => false,
      :done_by_user_id => 2,
      :declined => false,
      :declined_comment => "MyText",
      :declined_by_user_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/false/)
    rendered.should match(/2/)
    rendered.should match(/false/)
    rendered.should match(/MyText/)
    rendered.should match(/3/)
  end
end
