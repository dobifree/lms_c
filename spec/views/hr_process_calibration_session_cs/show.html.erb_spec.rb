require 'spec_helper'

describe "hr_process_calibration_session_cs/show" do
  before(:each) do
    @hr_process_calibration_session_c = assign(:hr_process_calibration_session_c, stub_model(HrProcessCalibrationSessionC,
      :hr_process_calibration_session => nil,
      :user => nil,
      :manager => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
  end
end
