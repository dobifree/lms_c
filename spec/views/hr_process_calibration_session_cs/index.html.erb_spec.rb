require 'spec_helper'

describe "hr_process_calibration_session_cs/index" do
  before(:each) do
    assign(:hr_process_calibration_session_cs, [
      stub_model(HrProcessCalibrationSessionC,
        :hr_process_calibration_session => nil,
        :user => nil,
        :manager => false
      ),
      stub_model(HrProcessCalibrationSessionC,
        :hr_process_calibration_session => nil,
        :user => nil,
        :manager => false
      )
    ])
  end

  it "renders a list of hr_process_calibration_session_cs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
