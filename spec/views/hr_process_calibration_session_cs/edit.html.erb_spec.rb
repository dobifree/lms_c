require 'spec_helper'

describe "hr_process_calibration_session_cs/edit" do
  before(:each) do
    @hr_process_calibration_session_c = assign(:hr_process_calibration_session_c, stub_model(HrProcessCalibrationSessionC,
      :hr_process_calibration_session => nil,
      :user => nil,
      :manager => false
    ))
  end

  it "renders the edit hr_process_calibration_session_c form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_calibration_session_cs_path(@hr_process_calibration_session_c), :method => "post" do
      assert_select "input#hr_process_calibration_session_c_hr_process_calibration_session", :name => "hr_process_calibration_session_c[hr_process_calibration_session]"
      assert_select "input#hr_process_calibration_session_c_user", :name => "hr_process_calibration_session_c[user]"
      assert_select "input#hr_process_calibration_session_c_manager", :name => "hr_process_calibration_session_c[manager]"
    end
  end
end
