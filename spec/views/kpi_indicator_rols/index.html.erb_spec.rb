require 'spec_helper'

describe "kpi_indicator_rols/index" do
  before(:each) do
    assign(:kpi_indicator_rols, [
      stub_model(KpiIndicatorRol,
        :kpi_indicator => nil,
        :kpi_rol => nil
      ),
      stub_model(KpiIndicatorRol,
        :kpi_indicator => nil,
        :kpi_rol => nil
      )
    ])
  end

  it "renders a list of kpi_indicator_rols" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
