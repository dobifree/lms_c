require 'spec_helper'

describe "kpi_indicator_rols/edit" do
  before(:each) do
    @kpi_indicator_rol = assign(:kpi_indicator_rol, stub_model(KpiIndicatorRol,
      :kpi_indicator => nil,
      :kpi_rol => nil
    ))
  end

  it "renders the edit kpi_indicator_rol form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_indicator_rols_path(@kpi_indicator_rol), :method => "post" do
      assert_select "input#kpi_indicator_rol_kpi_indicator", :name => "kpi_indicator_rol[kpi_indicator]"
      assert_select "input#kpi_indicator_rol_kpi_rol", :name => "kpi_indicator_rol[kpi_rol]"
    end
  end
end
