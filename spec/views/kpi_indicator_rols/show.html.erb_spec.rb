require 'spec_helper'

describe "kpi_indicator_rols/show" do
  before(:each) do
    @kpi_indicator_rol = assign(:kpi_indicator_rol, stub_model(KpiIndicatorRol,
      :kpi_indicator => nil,
      :kpi_rol => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
