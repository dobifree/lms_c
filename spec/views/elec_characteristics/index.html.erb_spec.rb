require 'spec_helper'

describe "elec_characteristics/index" do
  before(:each) do
    assign(:elec_characteristics, [
      stub_model(ElecCharacteristic,
        :elec_process => nil,
        :characteristic => nil,
        :used_to_group_by => false,
        :can_be_used_in_reports => false,
        :can_be_used_in_reports_position => 1
      ),
      stub_model(ElecCharacteristic,
        :elec_process => nil,
        :characteristic => nil,
        :used_to_group_by => false,
        :can_be_used_in_reports => false,
        :can_be_used_in_reports_position => 1
      )
    ])
  end

  it "renders a list of elec_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
