require 'spec_helper'

describe "elec_characteristics/show" do
  before(:each) do
    @elec_characteristic = assign(:elec_characteristic, stub_model(ElecCharacteristic,
      :elec_process => nil,
      :characteristic => nil,
      :used_to_group_by => false,
      :can_be_used_in_reports => false,
      :can_be_used_in_reports_position => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/1/)
  end
end
