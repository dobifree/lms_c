require 'spec_helper'

describe "elec_characteristics/new" do
  before(:each) do
    assign(:elec_characteristic, stub_model(ElecCharacteristic,
      :elec_process => nil,
      :characteristic => nil,
      :used_to_group_by => false,
      :can_be_used_in_reports => false,
      :can_be_used_in_reports_position => 1
    ).as_new_record)
  end

  it "renders new elec_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => elec_characteristics_path, :method => "post" do
      assert_select "input#elec_characteristic_elec_process", :name => "elec_characteristic[elec_process]"
      assert_select "input#elec_characteristic_characteristic", :name => "elec_characteristic[characteristic]"
      assert_select "input#elec_characteristic_used_to_group_by", :name => "elec_characteristic[used_to_group_by]"
      assert_select "input#elec_characteristic_can_be_used_in_reports", :name => "elec_characteristic[can_be_used_in_reports]"
      assert_select "input#elec_characteristic_can_be_used_in_reports_position", :name => "elec_characteristic[can_be_used_in_reports_position]"
    end
  end
end
