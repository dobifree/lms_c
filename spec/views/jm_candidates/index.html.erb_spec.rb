require 'spec_helper'

describe "jm_candidates/index" do
  before(:each) do
    assign(:jm_candidates, [
      stub_model(JmCandidate,
        :name => "Name",
        :surname => "Surename",
        :email => "Email",
        :phone => "Phone",
        :password_digest => "Password Digest"
      ),
      stub_model(JmCandidate,
        :name => "Name",
        :surname => "Surename",
        :email => "Email",
        :phone => "Phone",
        :password_digest => "Password Digest"
      )
    ])
  end

  it "renders a list of jm_candidates" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Surename".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Password Digest".to_s, :count => 2
  end
end
