require 'spec_helper'

describe "jm_candidates/show" do
  before(:each) do
    @jm_candidate = assign(:jm_candidate, stub_model(JmCandidate,
      :name => "Name",
      :surname => "Surename",
      :email => "Email",
      :phone => "Phone",
      :password_digest => "Password Digest"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Surename/)
    rendered.should match(/Email/)
    rendered.should match(/Phone/)
    rendered.should match(/Password Digest/)
  end
end
