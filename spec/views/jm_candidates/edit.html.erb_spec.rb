require 'spec_helper'

describe "jm_candidates/edit" do
  before(:each) do
    @jm_candidate = assign(:jm_candidate, stub_model(JmCandidate,
      :name => "MyString",
      :surname => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :password_digest => "MyString"
    ))
  end

  it "renders the edit jm_candidate form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => jm_candidates_path(@jm_candidate), :method => "post" do
      assert_select "input#jm_candidate_name", :name => "jm_candidate[name]"
      assert_select "input#jm_candidate_surename", :name => "jm_candidate[surename]"
      assert_select "input#jm_candidate_email", :name => "jm_candidate[email]"
      assert_select "input#jm_candidate_phone", :name => "jm_candidate[phone]"
      assert_select "input#jm_candidate_password_digest", :name => "jm_candidate[password_digest]"
    end
  end
end
