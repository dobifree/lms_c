require 'spec_helper'

describe "dnc_programs/edit" do
  before(:each) do
    @dnc_program = assign(:dnc_program, stub_model(DncProgram,
      :name => "MyString"
    ))
  end

  it "renders the edit dnc_program form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_programs_path(@dnc_program), :method => "post" do
      assert_select "input#dnc_program_name", :name => "dnc_program[name]"
    end
  end
end
