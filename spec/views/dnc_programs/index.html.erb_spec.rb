require 'spec_helper'

describe "dnc_programs/index" do
  before(:each) do
    assign(:dnc_programs, [
      stub_model(DncProgram,
        :name => "Name"
      ),
      stub_model(DncProgram,
        :name => "Name"
      )
    ])
  end

  it "renders a list of dnc_programs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
