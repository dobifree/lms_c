require 'spec_helper'

describe "dnc_programs/new" do
  before(:each) do
    assign(:dnc_program, stub_model(DncProgram,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new dnc_program form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_programs_path, :method => "post" do
      assert_select "input#dnc_program_name", :name => "dnc_program[name]"
    end
  end
end
