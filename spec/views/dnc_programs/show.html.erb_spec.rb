require 'spec_helper'

describe "dnc_programs/show" do
  before(:each) do
    @dnc_program = assign(:dnc_program, stub_model(DncProgram,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
