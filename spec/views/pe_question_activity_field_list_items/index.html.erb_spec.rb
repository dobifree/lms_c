require 'spec_helper'

describe "pe_question_activity_field_list_items/index" do
  before(:each) do
    assign(:pe_question_activity_field_list_items, [
      stub_model(PeQuestionActivityFieldListItem,
        :pe_question_activity_field_list => nil,
        :description => "Description"
      ),
      stub_model(PeQuestionActivityFieldListItem,
        :pe_question_activity_field_list => nil,
        :description => "Description"
      )
    ])
  end

  it "renders a list of pe_question_activity_field_list_items" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
