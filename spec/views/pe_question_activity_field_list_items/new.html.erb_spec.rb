require 'spec_helper'

describe "pe_question_activity_field_list_items/new" do
  before(:each) do
    assign(:pe_question_activity_field_list_item, stub_model(PeQuestionActivityFieldListItem,
      :pe_question_activity_field_list => nil,
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new pe_question_activity_field_list_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_question_activity_field_list_items_path, :method => "post" do
      assert_select "input#pe_question_activity_field_list_item_pe_question_activity_field_list", :name => "pe_question_activity_field_list_item[pe_question_activity_field_list]"
      assert_select "input#pe_question_activity_field_list_item_description", :name => "pe_question_activity_field_list_item[description]"
    end
  end
end
