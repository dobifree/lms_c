require 'spec_helper'

describe "pe_question_activity_field_list_items/show" do
  before(:each) do
    @pe_question_activity_field_list_item = assign(:pe_question_activity_field_list_item, stub_model(PeQuestionActivityFieldListItem,
      :pe_question_activity_field_list => nil,
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Description/)
  end
end
