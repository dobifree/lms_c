require 'spec_helper'

describe "hr_process_evaluation_qs/new" do
  before(:each) do
    assign(:hr_process_evaluation_q, stub_model(HrProcessEvaluationQ,
      :texto => "MyText",
      :peso => 1,
      :hr_process_evaluation => nil,
      :hr_process_level => nil,
      :hr_evaluation_type_element => nil,
      :hr_process_evaluation_q_id => 1
    ).as_new_record)
  end

  it "renders new hr_process_evaluation_q form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_evaluation_qs_path, :method => "post" do
      assert_select "textarea#hr_process_evaluation_q_texto", :name => "hr_process_evaluation_q[texto]"
      assert_select "input#hr_process_evaluation_q_peso", :name => "hr_process_evaluation_q[peso]"
      assert_select "input#hr_process_evaluation_q_hr_process_evaluation", :name => "hr_process_evaluation_q[hr_process_evaluation]"
      assert_select "input#hr_process_evaluation_q_hr_process_level", :name => "hr_process_evaluation_q[hr_process_level]"
      assert_select "input#hr_process_evaluation_q_hr_evaluation_type_element", :name => "hr_process_evaluation_q[hr_evaluation_type_element]"
      assert_select "input#hr_process_evaluation_q_hr_process_evaluation_q_id", :name => "hr_process_evaluation_q[hr_process_evaluation_q_id]"
    end
  end
end
