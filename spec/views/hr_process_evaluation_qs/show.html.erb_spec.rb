require 'spec_helper'

describe "hr_process_evaluation_qs/show" do
  before(:each) do
    @hr_process_evaluation_q = assign(:hr_process_evaluation_q, stub_model(HrProcessEvaluationQ,
      :texto => "MyText",
      :peso => 1,
      :hr_process_evaluation => nil,
      :hr_process_level => nil,
      :hr_evaluation_type_element => nil,
      :hr_process_evaluation_q_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/2/)
  end
end
