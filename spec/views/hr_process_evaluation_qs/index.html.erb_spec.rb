require 'spec_helper'

describe "hr_process_evaluation_qs/index" do
  before(:each) do
    assign(:hr_process_evaluation_qs, [
      stub_model(HrProcessEvaluationQ,
        :texto => "MyText",
        :peso => 1,
        :hr_process_evaluation => nil,
        :hr_process_level => nil,
        :hr_evaluation_type_element => nil,
        :hr_process_evaluation_q_id => 2
      ),
      stub_model(HrProcessEvaluationQ,
        :texto => "MyText",
        :peso => 1,
        :hr_process_evaluation => nil,
        :hr_process_level => nil,
        :hr_evaluation_type_element => nil,
        :hr_process_evaluation_q_id => 2
      )
    ])
  end

  it "renders a list of hr_process_evaluation_qs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
