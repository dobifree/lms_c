require 'spec_helper'

describe "pe_feedback_field_lists/index" do
  before(:each) do
    assign(:pe_feedback_field_lists, [
      stub_model(PeFeedbackFieldList,
        :pe_process => nil,
        :name => "Name"
      ),
      stub_model(PeFeedbackFieldList,
        :pe_process => nil,
        :name => "Name"
      )
    ])
  end

  it "renders a list of pe_feedback_field_lists" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
