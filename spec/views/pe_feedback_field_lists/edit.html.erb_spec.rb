require 'spec_helper'

describe "pe_feedback_field_lists/edit" do
  before(:each) do
    @pe_feedback_field_list = assign(:pe_feedback_field_list, stub_model(PeFeedbackFieldList,
      :pe_process => nil,
      :name => "MyString"
    ))
  end

  it "renders the edit pe_feedback_field_list form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_feedback_field_lists_path(@pe_feedback_field_list), :method => "post" do
      assert_select "input#pe_feedback_field_list_pe_process", :name => "pe_feedback_field_list[pe_process]"
      assert_select "input#pe_feedback_field_list_name", :name => "pe_feedback_field_list[name]"
    end
  end
end
