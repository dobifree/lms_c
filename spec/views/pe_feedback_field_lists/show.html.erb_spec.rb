require 'spec_helper'

describe "pe_feedback_field_lists/show" do
  before(:each) do
    @pe_feedback_field_list = assign(:pe_feedback_field_list, stub_model(PeFeedbackFieldList,
      :pe_process => nil,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
  end
end
