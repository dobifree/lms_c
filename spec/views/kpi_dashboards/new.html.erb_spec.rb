require 'spec_helper'

describe "kpi_dashboards/new" do
  before(:each) do
    assign(:kpi_dashboard, stub_model(KpiDashboard,
      :name => "MyString",
      :period => "MyString",
      :frequency => "MyString"
    ).as_new_record)
  end

  it "renders new kpi_dashboard form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_dashboards_path, :method => "post" do
      assert_select "input#kpi_dashboard_name", :name => "kpi_dashboard[name]"
      assert_select "input#kpi_dashboard_period", :name => "kpi_dashboard[period]"
      assert_select "input#kpi_dashboard_frequency", :name => "kpi_dashboard[frequency]"
    end
  end
end
