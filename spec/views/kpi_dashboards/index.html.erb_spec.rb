require 'spec_helper'

describe "kpi_dashboards/index" do
  before(:each) do
    assign(:kpi_dashboards, [
      stub_model(KpiDashboard,
        :name => "Name",
        :period => "Period",
        :frequency => "Frequency"
      ),
      stub_model(KpiDashboard,
        :name => "Name",
        :period => "Period",
        :frequency => "Frequency"
      )
    ])
  end

  it "renders a list of kpi_dashboards" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Period".to_s, :count => 2
    assert_select "tr>td", :text => "Frequency".to_s, :count => 2
  end
end
