require 'spec_helper'

describe "kpi_dashboards/show" do
  before(:each) do
    @kpi_dashboard = assign(:kpi_dashboard, stub_model(KpiDashboard,
      :name => "Name",
      :period => "Period",
      :frequency => "Frequency"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Period/)
    rendered.should match(/Frequency/)
  end
end
