require 'spec_helper'

describe "kpi_dashboards/edit" do
  before(:each) do
    @kpi_dashboard = assign(:kpi_dashboard, stub_model(KpiDashboard,
      :name => "MyString",
      :period => "MyString",
      :frequency => "MyString"
    ))
  end

  it "renders the edit kpi_dashboard form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_dashboards_path(@kpi_dashboard), :method => "post" do
      assert_select "input#kpi_dashboard_name", :name => "kpi_dashboard[name]"
      assert_select "input#kpi_dashboard_period", :name => "kpi_dashboard[period]"
      assert_select "input#kpi_dashboard_frequency", :name => "kpi_dashboard[frequency]"
    end
  end
end
