require 'spec_helper'

describe "ct_menus/show" do
  before(:each) do
    @ct_menu = assign(:ct_menu, stub_model(CtMenu,
      :position => 1,
      :cod => "Cod",
      :name => "Name",
      :user_menu_alias => "User Menu Alias"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Cod/)
    rendered.should match(/Name/)
    rendered.should match(/User Menu Alias/)
  end
end
