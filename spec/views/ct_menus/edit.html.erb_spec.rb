require 'spec_helper'

describe "ct_menus/edit" do
  before(:each) do
    @ct_menu = assign(:ct_menu, stub_model(CtMenu,
      :position => 1,
      :cod => "MyString",
      :name => "MyString",
      :user_menu_alias => "MyString"
    ))
  end

  it "renders the edit ct_menu form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => ct_menus_path(@ct_menu), :method => "post" do
      assert_select "input#ct_menu_position", :name => "ct_menu[position]"
      assert_select "input#ct_menu_cod", :name => "ct_menu[cod]"
      assert_select "input#ct_menu_name", :name => "ct_menu[name]"
      assert_select "input#ct_menu_user_menu_alias", :name => "ct_menu[user_menu_alias]"
    end
  end
end
