require 'spec_helper'

describe "ct_menus/index" do
  before(:each) do
    assign(:ct_menus, [
      stub_model(CtMenu,
        :position => 1,
        :cod => "Cod",
        :name => "Name",
        :user_menu_alias => "User Menu Alias"
      ),
      stub_model(CtMenu,
        :position => 1,
        :cod => "Cod",
        :name => "Name",
        :user_menu_alias => "User Menu Alias"
      )
    ])
  end

  it "renders a list of ct_menus" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Cod".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "User Menu Alias".to_s, :count => 2
  end
end
