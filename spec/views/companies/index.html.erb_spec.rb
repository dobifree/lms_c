require 'spec_helper'

describe "companies/index" do
  before(:each) do
    assign(:companies, [
      stub_model(Company,
        :nombre => "Nombre",
        :codigo => "Codigo"
      ),
      stub_model(Company,
        :nombre => "Nombre",
        :codigo => "Codigo"
      )
    ])
  end

  it "renders a list of companies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "Codigo".to_s, :count => 2
  end
end
