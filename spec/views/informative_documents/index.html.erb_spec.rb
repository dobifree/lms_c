require 'spec_helper'

describe "informative_documents/index" do
  before(:each) do
    assign(:informative_documents, [
      stub_model(InformativeDocument,
        :position => 1,
        :name => "Name",
        :description => "MyText"
      ),
      stub_model(InformativeDocument,
        :position => 1,
        :name => "Name",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of informative_documents" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
