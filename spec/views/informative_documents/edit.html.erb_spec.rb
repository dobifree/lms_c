require 'spec_helper'

describe "informative_documents/edit" do
  before(:each) do
    @informative_document = assign(:informative_document, stub_model(InformativeDocument,
      :position => 1,
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit informative_document form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => informative_documents_path(@informative_document), :method => "post" do
      assert_select "input#informative_document_position", :name => "informative_document[position]"
      assert_select "input#informative_document_name", :name => "informative_document[name]"
      assert_select "textarea#informative_document_description", :name => "informative_document[description]"
    end
  end
end
