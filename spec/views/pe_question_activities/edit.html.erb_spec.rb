require 'spec_helper'

describe "pe_question_activities/edit" do
  before(:each) do
    @pe_question_activity = assign(:pe_question_activity, stub_model(PeQuestionActivity,
      :position => 1,
      :pe_question_activity_field => nil,
      :value_string => "MyString",
      :pe_question_activity_field_list_item => nil,
      :user_creator_id => 1
    ))
  end

  it "renders the edit pe_question_activity form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_question_activities_path(@pe_question_activity), :method => "post" do
      assert_select "input#pe_question_activity_position", :name => "pe_question_activity[position]"
      assert_select "input#pe_question_activity_pe_question_activity_field", :name => "pe_question_activity[pe_question_activity_field]"
      assert_select "input#pe_question_activity_value_string", :name => "pe_question_activity[value_string]"
      assert_select "input#pe_question_activity_pe_question_activity_field_list_item", :name => "pe_question_activity[pe_question_activity_field_list_item]"
      assert_select "input#pe_question_activity_user_creator_id", :name => "pe_question_activity[user_creator_id]"
    end
  end
end
