require 'spec_helper'

describe "pe_question_activities/index" do
  before(:each) do
    assign(:pe_question_activities, [
      stub_model(PeQuestionActivity,
        :position => 1,
        :pe_question_activity_field => nil,
        :value_string => "Value String",
        :pe_question_activity_field_list_item => nil,
        :user_creator_id => 2
      ),
      stub_model(PeQuestionActivity,
        :position => 1,
        :pe_question_activity_field => nil,
        :value_string => "Value String",
        :pe_question_activity_field_list_item => nil,
        :user_creator_id => 2
      )
    ])
  end

  it "renders a list of pe_question_activities" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Value String".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
