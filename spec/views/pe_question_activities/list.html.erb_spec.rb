require 'spec_helper'

describe "pe_question_activities/show" do
  before(:each) do
    @pe_question_activity = assign(:pe_question_activity, stub_model(PeQuestionActivity,
      :position => 1,
      :pe_question_activity_field => nil,
      :value_string => "Value String",
      :pe_question_activity_field_list_item => nil,
      :user_creator_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(//)
    rendered.should match(/Value String/)
    rendered.should match(//)
    rendered.should match(/2/)
  end
end
