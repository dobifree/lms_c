require 'spec_helper'

describe "pe_dimension_groups/show" do
  before(:each) do
    @pe_dimension_group = assign(:pe_dimension_group, stub_model(PeDimensionGroup,
      :position => 1,
      :name => "Name",
      :description => "MyText",
      :min_sign => "Min Sign",
      :min_percentage => 2,
      :max_sign => "Max Sign",
      :max_percentage => 3,
      :pe_dimension => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/Min Sign/)
    rendered.should match(/2/)
    rendered.should match(/Max Sign/)
    rendered.should match(/3/)
    rendered.should match(//)
  end
end
