require 'spec_helper'

describe "pe_dimension_groups/edit" do
  before(:each) do
    @pe_dimension_group = assign(:pe_dimension_group, stub_model(PeDimensionGroup,
      :position => 1,
      :name => "MyString",
      :description => "MyText",
      :min_sign => "MyString",
      :min_percentage => 1,
      :max_sign => "MyString",
      :max_percentage => 1,
      :pe_dimension => nil
    ))
  end

  it "renders the edit pe_dimension_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_dimension_groups_path(@pe_dimension_group), :method => "post" do
      assert_select "input#pe_dimension_group_position", :name => "pe_dimension_group[position]"
      assert_select "input#pe_dimension_group_name", :name => "pe_dimension_group[name]"
      assert_select "textarea#pe_dimension_group_description", :name => "pe_dimension_group[description]"
      assert_select "input#pe_dimension_group_min_sign", :name => "pe_dimension_group[min_sign]"
      assert_select "input#pe_dimension_group_min_percentage", :name => "pe_dimension_group[min_percentage]"
      assert_select "input#pe_dimension_group_max_sign", :name => "pe_dimension_group[max_sign]"
      assert_select "input#pe_dimension_group_max_percentage", :name => "pe_dimension_group[max_percentage]"
      assert_select "input#pe_dimension_group_pe_dimension", :name => "pe_dimension_group[pe_dimension]"
    end
  end
end
