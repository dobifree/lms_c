require 'spec_helper'

describe "pe_dimension_groups/index" do
  before(:each) do
    assign(:pe_dimension_groups, [
      stub_model(PeDimensionGroup,
        :position => 1,
        :name => "Name",
        :description => "MyText",
        :min_sign => "Min Sign",
        :min_percentage => 2,
        :max_sign => "Max Sign",
        :max_percentage => 3,
        :pe_dimension => nil
      ),
      stub_model(PeDimensionGroup,
        :position => 1,
        :name => "Name",
        :description => "MyText",
        :min_sign => "Min Sign",
        :min_percentage => 2,
        :max_sign => "Max Sign",
        :max_percentage => 3,
        :pe_dimension => nil
      )
    ])
  end

  it "renders a list of pe_dimension_groups" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Min Sign".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Max Sign".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
