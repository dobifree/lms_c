require 'spec_helper'

describe "evaluations/index" do
  before(:each) do
    assign(:evaluations, [
      stub_model(Evaluation,
        :nombre => "Nombre",
        :descripcion => "Descripcion",
        :program => nil,
        :level => nil,
        :course => nil
      ),
      stub_model(Evaluation,
        :nombre => "Nombre",
        :descripcion => "Descripcion",
        :program => nil,
        :level => nil,
        :course => nil
      )
    ])
  end

  it "renders a list of evaluations" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "Descripcion".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
