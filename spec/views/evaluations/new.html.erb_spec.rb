require 'spec_helper'

describe "evaluations/new" do
  before(:each) do
    assign(:evaluation, stub_model(Evaluation,
      :nombre => "MyString",
      :descripcion => "MyString",
      :program => nil,
      :level => nil,
      :course => nil
    ).as_new_record)
  end

  it "renders new evaluation form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => evaluations_path, :method => "post" do
      assert_select "input#evaluation_nombre", :name => "evaluation[nombre]"
      assert_select "input#evaluation_descripcion", :name => "evaluation[descripcion]"
      assert_select "input#evaluation_program", :name => "evaluation[program]"
      assert_select "input#evaluation_level", :name => "evaluation[level]"
      assert_select "input#evaluation_course", :name => "evaluation[course]"
    end
  end
end
