require 'spec_helper'

describe "evaluations/show" do
  before(:each) do
    @evaluation = assign(:evaluation, stub_model(Evaluation,
      :nombre => "Nombre",
      :descripcion => "Descripcion",
      :program => nil,
      :level => nil,
      :course => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
    rendered.should match(/Descripcion/)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
  end
end
