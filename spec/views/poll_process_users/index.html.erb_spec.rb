require 'spec_helper'

describe "poll_process_users/index" do
  before(:each) do
    assign(:poll_process_users, [
      stub_model(PollProcessUser,
        :poll_process => nil,
        :user => nil,
        :done => false
      ),
      stub_model(PollProcessUser,
        :poll_process => nil,
        :user => nil,
        :done => false
      )
    ])
  end

  it "renders a list of poll_process_users" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
