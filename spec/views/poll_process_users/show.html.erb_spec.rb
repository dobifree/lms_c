require 'spec_helper'

describe "poll_process_users/show" do
  before(:each) do
    @poll_process_user = assign(:poll_process_user, stub_model(PollProcessUser,
      :poll_process => nil,
      :user => nil,
      :done => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
  end
end
