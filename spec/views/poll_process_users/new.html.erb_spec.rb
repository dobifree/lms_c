require 'spec_helper'

describe "poll_process_users/new" do
  before(:each) do
    assign(:poll_process_user, stub_model(PollProcessUser,
      :poll_process => nil,
      :user => nil,
      :done => false
    ).as_new_record)
  end

  it "renders new poll_process_user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => poll_process_users_path, :method => "post" do
      assert_select "input#poll_process_user_poll_process", :name => "poll_process_user[poll_process]"
      assert_select "input#poll_process_user_user", :name => "poll_process_user[user]"
      assert_select "input#poll_process_user_done", :name => "poll_process_user[done]"
    end
  end
end
