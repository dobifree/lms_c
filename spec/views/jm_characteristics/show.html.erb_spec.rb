require 'spec_helper'

describe "jm_characteristics/show" do
  before(:each) do
    @jm_characteristic = assign(:jm_characteristic, stub_model(JmCharacteristic,
      :name => "Name",
      :description => "MyText",
      :option_type => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
  end
end
