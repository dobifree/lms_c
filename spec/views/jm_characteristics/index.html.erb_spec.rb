require 'spec_helper'

describe "jm_characteristics/index" do
  before(:each) do
    assign(:jm_characteristics, [
      stub_model(JmCharacteristic,
        :name => "Name",
        :description => "MyText",
        :option_type => 1
      ),
      stub_model(JmCharacteristic,
        :name => "Name",
        :description => "MyText",
        :option_type => 1
      )
    ])
  end

  it "renders a list of jm_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
