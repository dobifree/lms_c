require 'spec_helper'

describe "jm_characteristics/edit" do
  before(:each) do
    @jm_characteristic = assign(:jm_characteristic, stub_model(JmCharacteristic,
      :name => "MyString",
      :description => "MyText",
      :option_type => 1
    ))
  end

  it "renders the edit jm_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => jm_characteristics_path(@jm_characteristic), :method => "post" do
      assert_select "input#jm_characteristic_name", :name => "jm_characteristic[name]"
      assert_select "textarea#jm_characteristic_description", :name => "jm_characteristic[description]"
      assert_select "input#jm_characteristic_option_type", :name => "jm_characteristic[option_type]"
    end
  end
end
