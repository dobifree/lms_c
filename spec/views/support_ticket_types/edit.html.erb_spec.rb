require 'spec_helper'

describe "support_ticket_types/edit" do
  before(:each) do
    @support_ticket_type = assign(:support_ticket_type, stub_model(SupportTicketType,
      :nombre => "MyString"
    ))
  end

  it "renders the edit support_ticket_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => support_ticket_types_path(@support_ticket_type), :method => "post" do
      assert_select "input#support_ticket_type_nombre", :name => "support_ticket_type[nombre]"
    end
  end
end
