require 'spec_helper'

describe "support_ticket_types/index" do
  before(:each) do
    assign(:support_ticket_types, [
      stub_model(SupportTicketType,
        :nombre => "Nombre"
      ),
      stub_model(SupportTicketType,
        :nombre => "Nombre"
      )
    ])
  end

  it "renders a list of support_ticket_types" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
  end
end
