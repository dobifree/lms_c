require 'spec_helper'

describe "support_ticket_types/new" do
  before(:each) do
    assign(:support_ticket_type, stub_model(SupportTicketType,
      :nombre => "MyString"
    ).as_new_record)
  end

  it "renders new support_ticket_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => support_ticket_types_path, :method => "post" do
      assert_select "input#support_ticket_type_nombre", :name => "support_ticket_type[nombre]"
    end
  end
end
