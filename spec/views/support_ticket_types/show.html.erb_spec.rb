require 'spec_helper'

describe "support_ticket_types/show" do
  before(:each) do
    @support_ticket_type = assign(:support_ticket_type, stub_model(SupportTicketType,
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
  end
end
