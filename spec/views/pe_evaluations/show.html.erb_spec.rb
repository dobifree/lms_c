require 'spec_helper'

describe "pe_evaluations/show" do
  before(:each) do
    @pe_evaluation = assign(:pe_evaluation, stub_model(PeEvaluation,
      :position => 1,
      :name => "Name",
      :description => "MyText",
      :weight => 2,
      :entered_by_manager => false,
      :pe_process => nil,
      :pe_dimension => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/2/)
    rendered.should match(/false/)
    rendered.should match(//)
    rendered.should match(//)
  end
end
