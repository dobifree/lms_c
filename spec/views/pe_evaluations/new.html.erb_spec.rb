require 'spec_helper'

describe "pe_evaluations/new" do
  before(:each) do
    assign(:pe_evaluation, stub_model(PeEvaluation,
      :position => 1,
      :name => "MyString",
      :description => "MyText",
      :weight => 1,
      :entered_by_manager => false,
      :pe_process => nil,
      :pe_dimension => nil
    ).as_new_record)
  end

  it "renders new pe_evaluation form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_evaluations_path, :method => "post" do
      assert_select "input#pe_evaluation_position", :name => "pe_evaluation[position]"
      assert_select "input#pe_evaluation_name", :name => "pe_evaluation[name]"
      assert_select "textarea#pe_evaluation_description", :name => "pe_evaluation[description]"
      assert_select "input#pe_evaluation_weight", :name => "pe_evaluation[weight]"
      assert_select "input#pe_evaluation_entered_by_manager", :name => "pe_evaluation[entered_by_manager]"
      assert_select "input#pe_evaluation_pe_process", :name => "pe_evaluation[pe_process]"
      assert_select "input#pe_evaluation_pe_dimension", :name => "pe_evaluation[pe_dimension]"
    end
  end
end
