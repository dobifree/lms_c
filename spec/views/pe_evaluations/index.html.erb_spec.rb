require 'spec_helper'

describe "pe_evaluations/index" do
  before(:each) do
    assign(:pe_evaluations, [
      stub_model(PeEvaluation,
        :position => 1,
        :name => "Name",
        :description => "MyText",
        :weight => 2,
        :entered_by_manager => false,
        :pe_process => nil,
        :pe_dimension => nil
      ),
      stub_model(PeEvaluation,
        :position => 1,
        :name => "Name",
        :description => "MyText",
        :weight => 2,
        :entered_by_manager => false,
        :pe_process => nil,
        :pe_dimension => nil
      )
    ])
  end

  it "renders a list of pe_evaluations" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
