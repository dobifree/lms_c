require 'spec_helper'

describe "elec_processes/new" do
  before(:each) do
    assign(:elec_process, stub_model(ElecProcess,
      :name => "MyString",
      :description => "MyText",
      :active => false,
      :to_everyone => false,
      :max_votes => 1,
      :self_vote => false,
      :rounds => 1,
      :recurrent => false,
      :recurrence_pattern => "MyString",
      :recurrence_period_name => "MyString",
      :recurrence_period_correction_factor => 1
    ).as_new_record)
  end

  it "renders new elec_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => elec_processes_path, :method => "post" do
      assert_select "input#elec_process_name", :name => "elec_process[name]"
      assert_select "textarea#elec_process_description", :name => "elec_process[description]"
      assert_select "input#elec_process_active", :name => "elec_process[active]"
      assert_select "input#elec_process_to_everyone", :name => "elec_process[to_everyone]"
      assert_select "input#elec_process_max_votes", :name => "elec_process[max_votes]"
      assert_select "input#elec_process_self_vote", :name => "elec_process[self_vote]"
      assert_select "input#elec_process_rounds", :name => "elec_process[rounds]"
      assert_select "input#elec_process_recurrent", :name => "elec_process[recurrent]"
      assert_select "input#elec_process_recurrence_pattern", :name => "elec_process[recurrence_pattern]"
      assert_select "input#elec_process_recurrence_period_name", :name => "elec_process[recurrence_period_name]"
      assert_select "input#elec_process_recurrence_period_correction_factor", :name => "elec_process[recurrence_period_correction_factor]"
    end
  end
end
