require 'spec_helper'

describe "elec_processes/index" do
  before(:each) do
    assign(:elec_processes, [
      stub_model(ElecProcess,
        :name => "Name",
        :description => "MyText",
        :active => false,
        :to_everyone => false,
        :max_votes => 1,
        :self_vote => false,
        :rounds => 2,
        :recurrent => false,
        :recurrence_pattern => "Recurrence Pattern",
        :recurrence_period_name => "Recurrence Period Name",
        :recurrence_period_correction_factor => 3
      ),
      stub_model(ElecProcess,
        :name => "Name",
        :description => "MyText",
        :active => false,
        :to_everyone => false,
        :max_votes => 1,
        :self_vote => false,
        :rounds => 2,
        :recurrent => false,
        :recurrence_pattern => "Recurrence Pattern",
        :recurrence_period_name => "Recurrence Period Name",
        :recurrence_period_correction_factor => 3
      )
    ])
  end

  it "renders a list of elec_processes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Recurrence Pattern".to_s, :count => 2
    assert_select "tr>td", :text => "Recurrence Period Name".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
