require 'spec_helper'

describe "elec_processes/show" do
  before(:each) do
    @elec_process = assign(:elec_process, stub_model(ElecProcess,
      :name => "Name",
      :description => "MyText",
      :active => false,
      :to_everyone => false,
      :max_votes => 1,
      :self_vote => false,
      :rounds => 2,
      :recurrent => false,
      :recurrence_pattern => "Recurrence Pattern",
      :recurrence_period_name => "Recurrence Period Name",
      :recurrence_period_correction_factor => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/1/)
    rendered.should match(/false/)
    rendered.should match(/2/)
    rendered.should match(/false/)
    rendered.should match(/Recurrence Pattern/)
    rendered.should match(/Recurrence Period Name/)
    rendered.should match(/3/)
  end
end
