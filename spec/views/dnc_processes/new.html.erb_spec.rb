require 'spec_helper'

describe "dnc_processes/new" do
  before(:each) do
    assign(:dnc_process, stub_model(DncProcess,
      :name => "MyString",
      :period => "MyString"
    ).as_new_record)
  end

  it "renders new dnc_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_processes_path, :method => "post" do
      assert_select "input#dnc_process_name", :name => "dnc_process[name]"
      assert_select "input#dnc_process_period", :name => "dnc_process[period]"
    end
  end
end
