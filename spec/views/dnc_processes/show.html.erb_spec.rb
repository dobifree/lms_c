require 'spec_helper'

describe "dnc_processes/show" do
  before(:each) do
    @dnc_process = assign(:dnc_process, stub_model(DncProcess,
      :name => "Name",
      :period => "Period"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Period/)
  end
end
