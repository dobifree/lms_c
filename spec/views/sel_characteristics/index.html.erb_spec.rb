require 'spec_helper'

describe "sel_characteristics/index" do
  before(:each) do
    assign(:sel_characteristics, [
      stub_model(SelCharacteristic,
        :sel_template => nil,
        :name => "Name",
        :description => "MyText",
        :option_type => 1
      ),
      stub_model(SelCharacteristic,
        :sel_template => nil,
        :name => "Name",
        :description => "MyText",
        :option_type => 1
      )
    ])
  end

  it "renders a list of sel_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
