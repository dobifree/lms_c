require 'spec_helper'

describe "sel_characteristics/new" do
  before(:each) do
    assign(:sel_characteristic, stub_model(SelCharacteristic,
      :sel_template => nil,
      :name => "MyString",
      :description => "MyText",
      :option_type => 1
    ).as_new_record)
  end

  it "renders new sel_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_characteristics_path, :method => "post" do
      assert_select "input#sel_characteristic_sel_template", :name => "sel_characteristic[sel_template]"
      assert_select "input#sel_characteristic_name", :name => "sel_characteristic[name]"
      assert_select "textarea#sel_characteristic_description", :name => "sel_characteristic[description]"
      assert_select "input#sel_characteristic_option_type", :name => "sel_characteristic[option_type]"
    end
  end
end
