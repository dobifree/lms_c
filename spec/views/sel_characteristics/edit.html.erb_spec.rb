require 'spec_helper'

describe "sel_characteristics/edit" do
  before(:each) do
    @sel_characteristic = assign(:sel_characteristic, stub_model(SelCharacteristic,
      :sel_template => nil,
      :name => "MyString",
      :description => "MyText",
      :option_type => 1
    ))
  end

  it "renders the edit sel_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_characteristics_path(@sel_characteristic), :method => "post" do
      assert_select "input#sel_characteristic_sel_template", :name => "sel_characteristic[sel_template]"
      assert_select "input#sel_characteristic_name", :name => "sel_characteristic[name]"
      assert_select "textarea#sel_characteristic_description", :name => "sel_characteristic[description]"
      assert_select "input#sel_characteristic_option_type", :name => "sel_characteristic[option_type]"
    end
  end
end
