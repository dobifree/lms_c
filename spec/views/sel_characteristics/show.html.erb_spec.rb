require 'spec_helper'

describe "sel_characteristics/show" do
  before(:each) do
    @sel_characteristic = assign(:sel_characteristic, stub_model(SelCharacteristic,
      :sel_template => nil,
      :name => "Name",
      :description => "MyText",
      :option_type => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
  end
end
