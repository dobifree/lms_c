require 'spec_helper'

describe "kpi_subsets/new" do
  before(:each) do
    assign(:kpi_subset, stub_model(KpiSubset,
      :position => 1,
      :name => "MyString",
      :kpi_set => nil,
      :value => 1.5,
      :percentage => 1.5
    ).as_new_record)
  end

  it "renders new kpi_subset form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_subsets_path, :method => "post" do
      assert_select "input#kpi_subset_position", :name => "kpi_subset[position]"
      assert_select "input#kpi_subset_name", :name => "kpi_subset[name]"
      assert_select "input#kpi_subset_kpi_set", :name => "kpi_subset[kpi_set]"
      assert_select "input#kpi_subset_value", :name => "kpi_subset[value]"
      assert_select "input#kpi_subset_percentage", :name => "kpi_subset[percentage]"
    end
  end
end
