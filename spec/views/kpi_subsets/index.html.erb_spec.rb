require 'spec_helper'

describe "kpi_subsets/index" do
  before(:each) do
    assign(:kpi_subsets, [
      stub_model(KpiSubset,
        :position => 1,
        :name => "Name",
        :kpi_set => nil,
        :value => 1.5,
        :percentage => 1.5
      ),
      stub_model(KpiSubset,
        :position => 1,
        :name => "Name",
        :kpi_set => nil,
        :value => 1.5,
        :percentage => 1.5
      )
    ])
  end

  it "renders a list of kpi_subsets" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
