require 'spec_helper'

describe "kpi_subsets/show" do
  before(:each) do
    @kpi_subset = assign(:kpi_subset, stub_model(KpiSubset,
      :position => 1,
      :name => "Name",
      :kpi_set => nil,
      :value => 1.5,
      :percentage => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(//)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
  end
end
