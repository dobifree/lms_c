require 'spec_helper'

describe "course_certificates/edit" do
  before(:each) do
    @course_certificate = assign(:course_certificate, stub_model(CourseCertificate,
      :course => nil,
      :template => "MyString",
      :nombre => false,
      :nombre_x => 1,
      :nombre_y => 1,
      :desde => false,
      :desde_x => 1,
      :desde_y => 1,
      :hasta => false,
      :hasta_x => 1,
      :hasta_y => 1,
      :inicio => false,
      :inicio_x => 1,
      :incio_y => 1,
      :fin => false,
      :fin_x => 1,
      :fin_y => 1,
      :nota => false,
      :nota_x => 1,
      :nota_y => 1,
      :fecha_source => "MyString",
      :fecha_source_delay => 1,
      :fecha => false,
      :fecha_x => 1,
      :fecha_y => 1
    ))
  end

  it "renders the edit course_certificate form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => course_certificates_path(@course_certificate), :method => "post" do
      assert_select "input#course_certificate_course", :name => "course_certificate[course]"
      assert_select "input#course_certificate_template", :name => "course_certificate[template]"
      assert_select "input#course_certificate_nombre", :name => "course_certificate[nombre]"
      assert_select "input#course_certificate_nombre_x", :name => "course_certificate[nombre_x]"
      assert_select "input#course_certificate_nombre_y", :name => "course_certificate[nombre_y]"
      assert_select "input#course_certificate_desde", :name => "course_certificate[desde]"
      assert_select "input#course_certificate_desde_x", :name => "course_certificate[desde_x]"
      assert_select "input#course_certificate_desde_y", :name => "course_certificate[desde_y]"
      assert_select "input#course_certificate_hasta", :name => "course_certificate[hasta]"
      assert_select "input#course_certificate_hasta_x", :name => "course_certificate[hasta_x]"
      assert_select "input#course_certificate_hasta_y", :name => "course_certificate[hasta_y]"
      assert_select "input#course_certificate_inicio", :name => "course_certificate[inicio]"
      assert_select "input#course_certificate_inicio_x", :name => "course_certificate[inicio_x]"
      assert_select "input#course_certificate_incio_y", :name => "course_certificate[incio_y]"
      assert_select "input#course_certificate_fin", :name => "course_certificate[fin]"
      assert_select "input#course_certificate_fin_x", :name => "course_certificate[fin_x]"
      assert_select "input#course_certificate_fin_y", :name => "course_certificate[fin_y]"
      assert_select "input#course_certificate_nota", :name => "course_certificate[nota]"
      assert_select "input#course_certificate_nota_x", :name => "course_certificate[nota_x]"
      assert_select "input#course_certificate_nota_y", :name => "course_certificate[nota_y]"
      assert_select "input#course_certificate_fecha_source", :name => "course_certificate[fecha_source]"
      assert_select "input#course_certificate_fecha_source_delay", :name => "course_certificate[fecha_source_delay]"
      assert_select "input#course_certificate_fecha", :name => "course_certificate[fecha]"
      assert_select "input#course_certificate_fecha_x", :name => "course_certificate[fecha_x]"
      assert_select "input#course_certificate_fecha_y", :name => "course_certificate[fecha_y]"
    end
  end
end
