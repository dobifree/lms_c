require 'spec_helper'

describe "course_certificates/index" do
  before(:each) do
    assign(:course_certificates, [
      stub_model(CourseCertificate,
        :course => nil,
        :template => "Template",
        :nombre => false,
        :nombre_x => 1,
        :nombre_y => 2,
        :desde => false,
        :desde_x => 3,
        :desde_y => 4,
        :hasta => false,
        :hasta_x => 5,
        :hasta_y => 6,
        :inicio => false,
        :inicio_x => 7,
        :incio_y => 8,
        :fin => false,
        :fin_x => 9,
        :fin_y => 10,
        :nota => false,
        :nota_x => 11,
        :nota_y => 12,
        :fecha_source => "Fecha Source",
        :fecha_source_delay => 13,
        :fecha => false,
        :fecha_x => 14,
        :fecha_y => 15
      ),
      stub_model(CourseCertificate,
        :course => nil,
        :template => "Template",
        :nombre => false,
        :nombre_x => 1,
        :nombre_y => 2,
        :desde => false,
        :desde_x => 3,
        :desde_y => 4,
        :hasta => false,
        :hasta_x => 5,
        :hasta_y => 6,
        :inicio => false,
        :inicio_x => 7,
        :incio_y => 8,
        :fin => false,
        :fin_x => 9,
        :fin_y => 10,
        :nota => false,
        :nota_x => 11,
        :nota_y => 12,
        :fecha_source => "Fecha Source",
        :fecha_source_delay => 13,
        :fecha => false,
        :fecha_x => 14,
        :fecha_y => 15
      )
    ])
  end

  it "renders a list of course_certificates" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Template".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 9.to_s, :count => 2
    assert_select "tr>td", :text => 10.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 11.to_s, :count => 2
    assert_select "tr>td", :text => 12.to_s, :count => 2
    assert_select "tr>td", :text => "Fecha Source".to_s, :count => 2
    assert_select "tr>td", :text => 13.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 14.to_s, :count => 2
    assert_select "tr>td", :text => 15.to_s, :count => 2
  end
end
