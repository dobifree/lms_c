require 'spec_helper'

describe "course_certificates/show" do
  before(:each) do
    @course_certificate = assign(:course_certificate, stub_model(CourseCertificate,
      :course => nil,
      :template => "Template",
      :nombre => false,
      :nombre_x => 1,
      :nombre_y => 2,
      :desde => false,
      :desde_x => 3,
      :desde_y => 4,
      :hasta => false,
      :hasta_x => 5,
      :hasta_y => 6,
      :inicio => false,
      :inicio_x => 7,
      :incio_y => 8,
      :fin => false,
      :fin_x => 9,
      :fin_y => 10,
      :nota => false,
      :nota_x => 11,
      :nota_y => 12,
      :fecha_source => "Fecha Source",
      :fecha_source_delay => 13,
      :fecha => false,
      :fecha_x => 14,
      :fecha_y => 15
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Template/)
    rendered.should match(/false/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/false/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/false/)
    rendered.should match(/5/)
    rendered.should match(/6/)
    rendered.should match(/false/)
    rendered.should match(/7/)
    rendered.should match(/8/)
    rendered.should match(/false/)
    rendered.should match(/9/)
    rendered.should match(/10/)
    rendered.should match(/false/)
    rendered.should match(/11/)
    rendered.should match(/12/)
    rendered.should match(/Fecha Source/)
    rendered.should match(/13/)
    rendered.should match(/false/)
    rendered.should match(/14/)
    rendered.should match(/15/)
  end
end
