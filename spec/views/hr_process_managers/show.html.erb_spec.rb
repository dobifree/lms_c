require 'spec_helper'

describe "hr_process_managers/show" do
  before(:each) do
    @hr_process_manager = assign(:hr_process_manager, stub_model(HrProcessManager,
      :hr_process => nil,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
