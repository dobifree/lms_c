require 'spec_helper'

describe "hr_process_managers/index" do
  before(:each) do
    assign(:hr_process_managers, [
      stub_model(HrProcessManager,
        :hr_process => nil,
        :user => nil
      ),
      stub_model(HrProcessManager,
        :hr_process => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of hr_process_managers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
