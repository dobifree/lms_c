require 'spec_helper'

describe "hr_process_managers/edit" do
  before(:each) do
    @hr_process_manager = assign(:hr_process_manager, stub_model(HrProcessManager,
      :hr_process => nil,
      :user => nil
    ))
  end

  it "renders the edit hr_process_manager form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_managers_path(@hr_process_manager), :method => "post" do
      assert_select "input#hr_process_manager_hr_process", :name => "hr_process_manager[hr_process]"
      assert_select "input#hr_process_manager_user", :name => "hr_process_manager[user]"
    end
  end
end
