require 'spec_helper'

describe "pe_slider_groups/index" do
  before(:each) do
    assign(:pe_slider_groups, [
      stub_model(PeSliderGroup,
        :position => 1,
        :name => "Name",
        :description => "MyText",
        :text_color => "Text Color",
        :bg_color => "Bg Color",
        :min_sign => "Min Sign",
        :min_value => 2,
        :max_sign => "Max Sign",
        :max_value => 3,
        :pe_evaluation => nil
      ),
      stub_model(PeSliderGroup,
        :position => 1,
        :name => "Name",
        :description => "MyText",
        :text_color => "Text Color",
        :bg_color => "Bg Color",
        :min_sign => "Min Sign",
        :min_value => 2,
        :max_sign => "Max Sign",
        :max_value => 3,
        :pe_evaluation => nil
      )
    ])
  end

  it "renders a list of pe_slider_groups" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Text Color".to_s, :count => 2
    assert_select "tr>td", :text => "Bg Color".to_s, :count => 2
    assert_select "tr>td", :text => "Min Sign".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Max Sign".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
