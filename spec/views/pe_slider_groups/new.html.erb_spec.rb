require 'spec_helper'

describe "pe_slider_groups/new" do
  before(:each) do
    assign(:pe_slider_group, stub_model(PeSliderGroup,
      :position => 1,
      :name => "MyString",
      :description => "MyText",
      :text_color => "MyString",
      :bg_color => "MyString",
      :min_sign => "MyString",
      :min_value => 1,
      :max_sign => "MyString",
      :max_value => 1,
      :pe_evaluation => nil
    ).as_new_record)
  end

  it "renders new pe_slider_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_slider_groups_path, :method => "post" do
      assert_select "input#pe_slider_group_position", :name => "pe_slider_group[position]"
      assert_select "input#pe_slider_group_name", :name => "pe_slider_group[name]"
      assert_select "textarea#pe_slider_group_description", :name => "pe_slider_group[description]"
      assert_select "input#pe_slider_group_text_color", :name => "pe_slider_group[text_color]"
      assert_select "input#pe_slider_group_bg_color", :name => "pe_slider_group[bg_color]"
      assert_select "input#pe_slider_group_min_sign", :name => "pe_slider_group[min_sign]"
      assert_select "input#pe_slider_group_min_value", :name => "pe_slider_group[min_value]"
      assert_select "input#pe_slider_group_max_sign", :name => "pe_slider_group[max_sign]"
      assert_select "input#pe_slider_group_max_value", :name => "pe_slider_group[max_value]"
      assert_select "input#pe_slider_group_pe_evaluation", :name => "pe_slider_group[pe_evaluation]"
    end
  end
end
