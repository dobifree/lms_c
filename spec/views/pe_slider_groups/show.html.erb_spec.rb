require 'spec_helper'

describe "pe_slider_groups/show" do
  before(:each) do
    @pe_slider_group = assign(:pe_slider_group, stub_model(PeSliderGroup,
      :position => 1,
      :name => "Name",
      :description => "MyText",
      :text_color => "Text Color",
      :bg_color => "Bg Color",
      :min_sign => "Min Sign",
      :min_value => 2,
      :max_sign => "Max Sign",
      :max_value => 3,
      :pe_evaluation => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/Text Color/)
    rendered.should match(/Bg Color/)
    rendered.should match(/Min Sign/)
    rendered.should match(/2/)
    rendered.should match(/Max Sign/)
    rendered.should match(/3/)
    rendered.should match(//)
  end
end
