require 'spec_helper'

describe "uc_characteristics/index" do
  before(:each) do
    assign(:uc_characteristics, [
      stub_model(UcCharacteristic,
        :name => "Name"
      ),
      stub_model(UcCharacteristic,
        :name => "Name"
      )
    ])
  end

  it "renders a list of uc_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
