require 'spec_helper'

describe "uc_characteristics/new" do
  before(:each) do
    assign(:uc_characteristic, stub_model(UcCharacteristic,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new uc_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => uc_characteristics_path, :method => "post" do
      assert_select "input#uc_characteristic_name", :name => "uc_characteristic[name]"
    end
  end
end
