require 'spec_helper'

describe "uc_characteristics/edit" do
  before(:each) do
    @uc_characteristic = assign(:uc_characteristic, stub_model(UcCharacteristic,
      :name => "MyString"
    ))
  end

  it "renders the edit uc_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => uc_characteristics_path(@uc_characteristic), :method => "post" do
      assert_select "input#uc_characteristic_name", :name => "uc_characteristic[name]"
    end
  end
end
