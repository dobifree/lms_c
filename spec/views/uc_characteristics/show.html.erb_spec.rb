require 'spec_helper'

describe "uc_characteristics/show" do
  before(:each) do
    @uc_characteristic = assign(:uc_characteristic, stub_model(UcCharacteristic,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
