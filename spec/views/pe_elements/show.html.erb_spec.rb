require 'spec_helper'

describe "pe_elements/show" do
  before(:each) do
    @pe_element = assign(:pe_element, stub_model(PeElement,
      :name => "Name",
      :pe_evaluation => nil,
      :pe_element_id => 1,
      :assessed => false,
      :visible => false,
      :assessment_method => 2,
      :calculus_method => 3,
      :max_number => 4,
      :min_number => 5,
      :max_weight => 6,
      :min_weight => 7,
      :weight_sum => 8,
      :element_def_by => 9,
      :element_def_by_rol => "Element Def By Rol",
      :assessment_method_def_by => 10,
      :assessment_method_def_by_rol => "Assessment Method Def By Rol"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/5/)
    rendered.should match(/6/)
    rendered.should match(/7/)
    rendered.should match(/8/)
    rendered.should match(/9/)
    rendered.should match(/Element Def By Rol/)
    rendered.should match(/10/)
    rendered.should match(/Assessment Method Def By Rol/)
  end
end
