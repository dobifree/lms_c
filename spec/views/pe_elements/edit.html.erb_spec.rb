require 'spec_helper'

describe "pe_elements/edit" do
  before(:each) do
    @pe_element = assign(:pe_element, stub_model(PeElement,
      :name => "MyString",
      :pe_evaluation => nil,
      :pe_element_id => 1,
      :assessed => false,
      :visible => false,
      :assessment_method => 1,
      :calculus_method => 1,
      :max_number => 1,
      :min_number => 1,
      :max_weight => 1,
      :min_weight => 1,
      :weight_sum => 1,
      :element_def_by => 1,
      :element_def_by_rol => "MyString",
      :assessment_method_def_by => 1,
      :assessment_method_def_by_rol => "MyString"
    ))
  end

  it "renders the edit pe_element form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_elements_path(@pe_element), :method => "post" do
      assert_select "input#pe_element_name", :name => "pe_element[name]"
      assert_select "input#pe_element_pe_evaluation", :name => "pe_element[pe_evaluation]"
      assert_select "input#pe_element_pe_element_id", :name => "pe_element[pe_element_id]"
      assert_select "input#pe_element_assessed", :name => "pe_element[assessed]"
      assert_select "input#pe_element_visible", :name => "pe_element[visible]"
      assert_select "input#pe_element_assessment_method", :name => "pe_element[assessment_method]"
      assert_select "input#pe_element_calculus_method", :name => "pe_element[calculus_method]"
      assert_select "input#pe_element_max_number", :name => "pe_element[max_number]"
      assert_select "input#pe_element_min_number", :name => "pe_element[min_number]"
      assert_select "input#pe_element_max_weight", :name => "pe_element[max_weight]"
      assert_select "input#pe_element_min_weight", :name => "pe_element[min_weight]"
      assert_select "input#pe_element_weight_sum", :name => "pe_element[weight_sum]"
      assert_select "input#pe_element_element_def_by", :name => "pe_element[element_def_by]"
      assert_select "input#pe_element_element_def_by_rol", :name => "pe_element[element_def_by_rol]"
      assert_select "input#pe_element_assessment_method_def_by", :name => "pe_element[assessment_method_def_by]"
      assert_select "input#pe_element_assessment_method_def_by_rol", :name => "pe_element[assessment_method_def_by_rol]"
    end
  end
end
