require 'spec_helper'

describe "pe_elements/index" do
  before(:each) do
    assign(:pe_elements, [
      stub_model(PeElement,
        :name => "Name",
        :pe_evaluation => nil,
        :pe_element_id => 1,
        :assessed => false,
        :visible => false,
        :assessment_method => 2,
        :calculus_method => 3,
        :max_number => 4,
        :min_number => 5,
        :max_weight => 6,
        :min_weight => 7,
        :weight_sum => 8,
        :element_def_by => 9,
        :element_def_by_rol => "Element Def By Rol",
        :assessment_method_def_by => 10,
        :assessment_method_def_by_rol => "Assessment Method Def By Rol"
      ),
      stub_model(PeElement,
        :name => "Name",
        :pe_evaluation => nil,
        :pe_element_id => 1,
        :assessed => false,
        :visible => false,
        :assessment_method => 2,
        :calculus_method => 3,
        :max_number => 4,
        :min_number => 5,
        :max_weight => 6,
        :min_weight => 7,
        :weight_sum => 8,
        :element_def_by => 9,
        :element_def_by_rol => "Element Def By Rol",
        :assessment_method_def_by => 10,
        :assessment_method_def_by_rol => "Assessment Method Def By Rol"
      )
    ])
  end

  it "renders a list of pe_elements" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
    assert_select "tr>td", :text => 9.to_s, :count => 2
    assert_select "tr>td", :text => "Element Def By Rol".to_s, :count => 2
    assert_select "tr>td", :text => 10.to_s, :count => 2
    assert_select "tr>td", :text => "Assessment Method Def By Rol".to_s, :count => 2
  end
end
