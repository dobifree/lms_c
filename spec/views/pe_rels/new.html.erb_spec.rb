require 'spec_helper'

describe "pe_rels/new" do
  before(:each) do
    assign(:pe_rel, stub_model(PeRel,
      :pe_process => nil,
      :rel => 1,
      :alias => "MyString",
      :alias_reverse => "MyString",
      :alias_plural => "MyString",
      :alias_reverse_plural => "MyString"
    ).as_new_record)
  end

  it "renders new pe_rel form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_rels_path, :method => "post" do
      assert_select "input#pe_rel_pe_process", :name => "pe_rel[pe_process]"
      assert_select "input#pe_rel_rel", :name => "pe_rel[rel]"
      assert_select "input#pe_rel_alias", :name => "pe_rel[alias]"
      assert_select "input#pe_rel_alias_reverse", :name => "pe_rel[alias_reverse]"
      assert_select "input#pe_rel_alias_plural", :name => "pe_rel[alias_plural]"
      assert_select "input#pe_rel_alias_reverse_plural", :name => "pe_rel[alias_reverse_plural]"
    end
  end
end
