require 'spec_helper'

describe "pe_rels/index" do
  before(:each) do
    assign(:pe_rels, [
      stub_model(PeRel,
        :pe_process => nil,
        :rel => 1,
        :alias => "Alias",
        :alias_reverse => "Alias Reverse",
        :alias_plural => "Alias Plural",
        :alias_reverse_plural => "Alias Reverse Plural"
      ),
      stub_model(PeRel,
        :pe_process => nil,
        :rel => 1,
        :alias => "Alias",
        :alias_reverse => "Alias Reverse",
        :alias_plural => "Alias Plural",
        :alias_reverse_plural => "Alias Reverse Plural"
      )
    ])
  end

  it "renders a list of pe_rels" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Alias".to_s, :count => 2
    assert_select "tr>td", :text => "Alias Reverse".to_s, :count => 2
    assert_select "tr>td", :text => "Alias Plural".to_s, :count => 2
    assert_select "tr>td", :text => "Alias Reverse Plural".to_s, :count => 2
  end
end
