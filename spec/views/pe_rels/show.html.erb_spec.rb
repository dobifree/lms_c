require 'spec_helper'

describe "pe_rels/show" do
  before(:each) do
    @pe_rel = assign(:pe_rel, stub_model(PeRel,
      :pe_process => nil,
      :rel => 1,
      :alias => "Alias",
      :alias_reverse => "Alias Reverse",
      :alias_plural => "Alias Plural",
      :alias_reverse_plural => "Alias Reverse Plural"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/Alias/)
    rendered.should match(/Alias Reverse/)
    rendered.should match(/Alias Plural/)
    rendered.should match(/Alias Reverse Plural/)
  end
end
