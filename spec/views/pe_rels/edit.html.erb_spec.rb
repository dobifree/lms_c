require 'spec_helper'

describe "pe_rels/edit" do
  before(:each) do
    @pe_rel = assign(:pe_rel, stub_model(PeRel,
      :pe_process => nil,
      :rel => 1,
      :alias => "MyString",
      :alias_reverse => "MyString",
      :alias_plural => "MyString",
      :alias_reverse_plural => "MyString"
    ))
  end

  it "renders the edit pe_rel form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_rels_path(@pe_rel), :method => "post" do
      assert_select "input#pe_rel_pe_process", :name => "pe_rel[pe_process]"
      assert_select "input#pe_rel_rel", :name => "pe_rel[rel]"
      assert_select "input#pe_rel_alias", :name => "pe_rel[alias]"
      assert_select "input#pe_rel_alias_reverse", :name => "pe_rel[alias_reverse]"
      assert_select "input#pe_rel_alias_plural", :name => "pe_rel[alias_plural]"
      assert_select "input#pe_rel_alias_reverse_plural", :name => "pe_rel[alias_reverse_plural]"
    end
  end
end
