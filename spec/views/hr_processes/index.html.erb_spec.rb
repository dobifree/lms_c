require 'spec_helper'

describe "hr_processes/index" do
  before(:each) do
    assign(:hr_processes, [
      stub_model(HrProcess,
        :nombre => "Nombre",
        :abierto => false,
        :year => 1
      ),
      stub_model(HrProcess,
        :nombre => "Nombre",
        :abierto => false,
        :year => 1
      )
    ])
  end

  it "renders a list of hr_processes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
