require 'spec_helper'

describe "hr_processes/show" do
  before(:each) do
    @hr_process = assign(:hr_process, stub_model(HrProcess,
      :nombre => "Nombre",
      :abierto => false,
      :year => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
    rendered.should match(/false/)
    rendered.should match(/1/)
  end
end
