require 'spec_helper'

describe "hr_processes/new" do
  before(:each) do
    assign(:hr_process, stub_model(HrProcess,
      :nombre => "MyString",
      :abierto => false,
      :year => 1
    ).as_new_record)
  end

  it "renders new hr_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_processes_path, :method => "post" do
      assert_select "input#hr_process_nombre", :name => "hr_process[nombre]"
      assert_select "input#hr_process_abierto", :name => "hr_process[abierto]"
      assert_select "input#hr_process_year", :name => "hr_process[year]"
    end
  end
end
