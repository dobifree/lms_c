require 'spec_helper'

describe "pe_alternatives/index" do
  before(:each) do
    assign(:pe_alternatives, [
      stub_model(PeAlternative,
        :description => "MyText",
        :value => 1,
        :pe_question => nil
      ),
      stub_model(PeAlternative,
        :description => "MyText",
        :value => 1,
        :pe_question => nil
      )
    ])
  end

  it "renders a list of pe_alternatives" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
