require 'spec_helper'

describe "pe_alternatives/new" do
  before(:each) do
    assign(:pe_alternative, stub_model(PeAlternative,
      :description => "MyText",
      :value => 1,
      :pe_question => nil
    ).as_new_record)
  end

  it "renders new pe_alternative form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_alternatives_path, :method => "post" do
      assert_select "textarea#pe_alternative_description", :name => "pe_alternative[description]"
      assert_select "input#pe_alternative_value", :name => "pe_alternative[value]"
      assert_select "input#pe_alternative_pe_question", :name => "pe_alternative[pe_question]"
    end
  end
end
