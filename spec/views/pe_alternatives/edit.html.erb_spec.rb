require 'spec_helper'

describe "pe_alternatives/edit" do
  before(:each) do
    @pe_alternative = assign(:pe_alternative, stub_model(PeAlternative,
      :description => "MyText",
      :value => 1,
      :pe_question => nil
    ))
  end

  it "renders the edit pe_alternative form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_alternatives_path(@pe_alternative), :method => "post" do
      assert_select "textarea#pe_alternative_description", :name => "pe_alternative[description]"
      assert_select "input#pe_alternative_value", :name => "pe_alternative[value]"
      assert_select "input#pe_alternative_pe_question", :name => "pe_alternative[pe_question]"
    end
  end
end
