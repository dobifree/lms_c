require 'spec_helper'

describe "pe_alternatives/show" do
  before(:each) do
    @pe_alternative = assign(:pe_alternative, stub_model(PeAlternative,
      :description => "MyText",
      :value => 1,
      :pe_question => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(//)
  end
end
