require 'spec_helper'

describe "program_instances/new" do
  before(:each) do
    assign(:program_instance, stub_model(ProgramInstance,
      :name => "MyString",
      :program => nil
    ).as_new_record)
  end

  it "renders new program_instance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => program_instances_path, :method => "post" do
      assert_select "input#program_instance_name", :name => "program_instance[name]"
      assert_select "input#program_instance_program", :name => "program_instance[program]"
    end
  end
end
