require 'spec_helper'

describe "program_instances/index" do
  before(:each) do
    assign(:program_instances, [
      stub_model(ProgramInstance,
        :name => "Name",
        :program => nil
      ),
      stub_model(ProgramInstance,
        :name => "Name",
        :program => nil
      )
    ])
  end

  it "renders a list of program_instances" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
