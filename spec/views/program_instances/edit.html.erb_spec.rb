require 'spec_helper'

describe "program_instances/edit" do
  before(:each) do
    @program_instance = assign(:program_instance, stub_model(ProgramInstance,
      :name => "MyString",
      :program => nil
    ))
  end

  it "renders the edit program_instance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => program_instances_path(@program_instance), :method => "post" do
      assert_select "input#program_instance_name", :name => "program_instance[name]"
      assert_select "input#program_instance_program", :name => "program_instance[program]"
    end
  end
end
