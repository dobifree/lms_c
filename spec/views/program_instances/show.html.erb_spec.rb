require 'spec_helper'

describe "program_instances/show" do
  before(:each) do
    @program_instance = assign(:program_instance, stub_model(ProgramInstance,
      :name => "Name",
      :program => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
  end
end
