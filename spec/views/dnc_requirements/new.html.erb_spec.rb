require 'spec_helper'

describe "dnc_requirements/new" do
  before(:each) do
    assign(:dnc_requirement, stub_model(DncRequirement,
      :user => nil,
      :dnc_process => nil,
      :dnc_course => nil,
      :dnc_process_period => nil,
      :dnc_provider => nil
    ).as_new_record)
  end

  it "renders new dnc_requirement form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_requirements_path, :method => "post" do
      assert_select "input#dnc_requirement_user", :name => "dnc_requirement[user]"
      assert_select "input#dnc_requirement_dnc_process", :name => "dnc_requirement[dnc_process]"
      assert_select "input#dnc_requirement_dnc_course", :name => "dnc_requirement[dnc_course]"
      assert_select "input#dnc_requirement_dnc_process_period", :name => "dnc_requirement[dnc_process_period]"
      assert_select "input#dnc_requirement_dnc_provider", :name => "dnc_requirement[dnc_provider]"
    end
  end
end
