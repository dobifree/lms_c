require 'spec_helper'

describe "dnc_requirements/index" do
  before(:each) do
    assign(:dnc_requirements, [
      stub_model(DncRequirement,
        :user => nil,
        :dnc_process => nil,
        :dnc_course => nil,
        :dnc_process_period => nil,
        :dnc_provider => nil
      ),
      stub_model(DncRequirement,
        :user => nil,
        :dnc_process => nil,
        :dnc_course => nil,
        :dnc_process_period => nil,
        :dnc_provider => nil
      )
    ])
  end

  it "renders a list of dnc_requirements" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
