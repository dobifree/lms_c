require 'spec_helper'

describe "dnc_requirements/show" do
  before(:each) do
    @dnc_requirement = assign(:dnc_requirement, stub_model(DncRequirement,
      :user => nil,
      :dnc_process => nil,
      :dnc_course => nil,
      :dnc_process_period => nil,
      :dnc_provider => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
  end
end
