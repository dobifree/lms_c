require 'spec_helper'

describe "planning_processes/show" do
  before(:each) do
    @planning_process = assign(:planning_process, stub_model(PlanningProcess,
      :nombre => "Nombre",
      :abierto => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
    rendered.should match(/false/)
  end
end
