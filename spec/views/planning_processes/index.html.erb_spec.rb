require 'spec_helper'

describe "planning_processes/index" do
  before(:each) do
    assign(:planning_processes, [
      stub_model(PlanningProcess,
        :nombre => "Nombre",
        :abierto => false
      ),
      stub_model(PlanningProcess,
        :nombre => "Nombre",
        :abierto => false
      )
    ])
  end

  it "renders a list of planning_processes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
