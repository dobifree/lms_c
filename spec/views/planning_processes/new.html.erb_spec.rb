require 'spec_helper'

describe "planning_processes/new" do
  before(:each) do
    assign(:planning_process, stub_model(PlanningProcess,
      :nombre => "MyString",
      :abierto => false
    ).as_new_record)
  end

  it "renders new planning_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => planning_processes_path, :method => "post" do
      assert_select "input#planning_process_nombre", :name => "planning_process[nombre]"
      assert_select "input#planning_process_abierto", :name => "planning_process[abierto]"
    end
  end
end
