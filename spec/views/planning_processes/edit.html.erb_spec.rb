require 'spec_helper'

describe "planning_processes/edit" do
  before(:each) do
    @planning_process = assign(:planning_process, stub_model(PlanningProcess,
      :nombre => "MyString",
      :abierto => false
    ))
  end

  it "renders the edit planning_process form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => planning_processes_path(@planning_process), :method => "post" do
      assert_select "input#planning_process_nombre", :name => "planning_process[nombre]"
      assert_select "input#planning_process_abierto", :name => "planning_process[abierto]"
    end
  end
end
