require 'spec_helper'

describe "hr_process_dimension_gs/index" do
  before(:each) do
    assign(:hr_process_dimension_gs, [
      stub_model(HrProcessDimensionG,
        :orden => 1,
        :valor_minimo => 2,
        :valor_maximo => 3,
        :hr_process_dimension => nil
      ),
      stub_model(HrProcessDimensionG,
        :orden => 1,
        :valor_minimo => 2,
        :valor_maximo => 3,
        :hr_process_dimension => nil
      )
    ])
  end

  it "renders a list of hr_process_dimension_gs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
