require 'spec_helper'

describe "hr_process_dimension_gs/show" do
  before(:each) do
    @hr_process_dimension_g = assign(:hr_process_dimension_g, stub_model(HrProcessDimensionG,
      :orden => 1,
      :valor_minimo => 2,
      :valor_maximo => 3,
      :hr_process_dimension => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(//)
  end
end
