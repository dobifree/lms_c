require 'spec_helper'

describe "hr_process_dimension_gs/edit" do
  before(:each) do
    @hr_process_dimension_g = assign(:hr_process_dimension_g, stub_model(HrProcessDimensionG,
      :orden => 1,
      :valor_minimo => 1,
      :valor_maximo => 1,
      :hr_process_dimension => nil
    ))
  end

  it "renders the edit hr_process_dimension_g form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_dimension_gs_path(@hr_process_dimension_g), :method => "post" do
      assert_select "input#hr_process_dimension_g_orden", :name => "hr_process_dimension_g[orden]"
      assert_select "input#hr_process_dimension_g_valor_minimo", :name => "hr_process_dimension_g[valor_minimo]"
      assert_select "input#hr_process_dimension_g_valor_maximo", :name => "hr_process_dimension_g[valor_maximo]"
      assert_select "input#hr_process_dimension_g_hr_process_dimension", :name => "hr_process_dimension_g[hr_process_dimension]"
    end
  end
end
