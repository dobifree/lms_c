require 'spec_helper'

describe "j_cost_centers/new" do
  before(:each) do
    assign(:j_cost_center, stub_model(JCostCenter,
      :name => "MyString",
      :budget => "9.99",
      :active => false
    ).as_new_record)
  end

  it "renders new j_cost_center form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_cost_centers_path, :method => "post" do
      assert_select "input#j_cost_center_name", :name => "j_cost_center[name]"
      assert_select "input#j_cost_center_budget", :name => "j_cost_center[budget]"
      assert_select "input#j_cost_center_active", :name => "j_cost_center[active]"
    end
  end
end
