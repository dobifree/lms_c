require 'spec_helper'

describe "j_cost_centers/show" do
  before(:each) do
    @j_cost_center = assign(:j_cost_center, stub_model(JCostCenter,
      :name => "Name",
      :budget => "9.99",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/9.99/)
    rendered.should match(/false/)
  end
end
