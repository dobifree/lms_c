require 'spec_helper'

describe "j_cost_centers/index" do
  before(:each) do
    assign(:j_cost_centers, [
      stub_model(JCostCenter,
        :name => "Name",
        :budget => "9.99",
        :active => false
      ),
      stub_model(JCostCenter,
        :name => "Name",
        :budget => "9.99",
        :active => false
      )
    ])
  end

  it "renders a list of j_cost_centers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
