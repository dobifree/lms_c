require 'spec_helper'

describe "support_tickets/show" do
  before(:each) do
    @support_ticket = assign(:support_ticket, stub_model(SupportTicket,
      :user => nil,
      :support_ticket_type => nil,
      :descripcion => "MyText",
      :atendido => false,
      :respuesta => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/MyText/)
    rendered.should match(/false/)
    rendered.should match(/MyText/)
  end
end
