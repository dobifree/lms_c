require 'spec_helper'

describe "support_tickets/edit" do
  before(:each) do
    @support_ticket = assign(:support_ticket, stub_model(SupportTicket,
      :user => nil,
      :support_ticket_type => nil,
      :descripcion => "MyText",
      :atendido => false,
      :respuesta => "MyText"
    ))
  end

  it "renders the edit support_ticket form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => support_tickets_path(@support_ticket), :method => "post" do
      assert_select "input#support_ticket_user", :name => "support_ticket[user]"
      assert_select "input#support_ticket_support_ticket_type", :name => "support_ticket[support_ticket_type]"
      assert_select "textarea#support_ticket_descripcion", :name => "support_ticket[descripcion]"
      assert_select "input#support_ticket_atendido", :name => "support_ticket[atendido]"
      assert_select "textarea#support_ticket_respuesta", :name => "support_ticket[respuesta]"
    end
  end
end
