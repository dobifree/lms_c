require 'spec_helper'

describe "support_tickets/index" do
  before(:each) do
    assign(:support_tickets, [
      stub_model(SupportTicket,
        :user => nil,
        :support_ticket_type => nil,
        :descripcion => "MyText",
        :atendido => false,
        :respuesta => "MyText"
      ),
      stub_model(SupportTicket,
        :user => nil,
        :support_ticket_type => nil,
        :descripcion => "MyText",
        :atendido => false,
        :respuesta => "MyText"
      )
    ])
  end

  it "renders a list of support_tickets" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
