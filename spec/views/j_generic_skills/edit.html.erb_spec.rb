require 'spec_helper'

describe "j_generic_skills/edit" do
  before(:each) do
    @j_generic_skill = assign(:j_generic_skill, stub_model(JGenericSkill,
      :name => "MyString",
      :description => "MyText",
      :j_job_level => nil
    ))
  end

  it "renders the edit j_generic_skill form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_generic_skills_path(@j_generic_skill), :method => "post" do
      assert_select "input#j_generic_skill_name", :name => "j_generic_skill[name]"
      assert_select "textarea#j_generic_skill_description", :name => "j_generic_skill[description]"
      assert_select "input#j_generic_skill_j_job_level", :name => "j_generic_skill[j_job_level]"
    end
  end
end
