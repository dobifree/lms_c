require 'spec_helper'

describe "j_generic_skills/index" do
  before(:each) do
    assign(:j_generic_skills, [
      stub_model(JGenericSkill,
        :name => "Name",
        :description => "MyText",
        :j_job_level => nil
      ),
      stub_model(JGenericSkill,
        :name => "Name",
        :description => "MyText",
        :j_job_level => nil
      )
    ])
  end

  it "renders a list of j_generic_skills" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
