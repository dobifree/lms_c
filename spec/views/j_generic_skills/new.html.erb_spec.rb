require 'spec_helper'

describe "j_generic_skills/new" do
  before(:each) do
    assign(:j_generic_skill, stub_model(JGenericSkill,
      :name => "MyString",
      :description => "MyText",
      :j_job_level => nil
    ).as_new_record)
  end

  it "renders new j_generic_skill form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_generic_skills_path, :method => "post" do
      assert_select "input#j_generic_skill_name", :name => "j_generic_skill[name]"
      assert_select "textarea#j_generic_skill_description", :name => "j_generic_skill[description]"
      assert_select "input#j_generic_skill_j_job_level", :name => "j_generic_skill[j_job_level]"
    end
  end
end
