require 'spec_helper'

describe "j_generic_skills/show" do
  before(:each) do
    @j_generic_skill = assign(:j_generic_skill, stub_model(JGenericSkill,
      :name => "Name",
      :description => "MyText",
      :j_job_level => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(//)
  end
end
