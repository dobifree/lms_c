require 'spec_helper'

describe "hr_process_evaluations/show" do
  before(:each) do
    @hr_process_evaluation = assign(:hr_process_evaluation, stub_model(HrProcessEvaluation,
      :hr_process => nil,
      :hr_process_dimension_e => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
