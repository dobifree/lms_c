require 'spec_helper'

describe "hr_process_evaluations/index" do
  before(:each) do
    assign(:hr_process_evaluations, [
      stub_model(HrProcessEvaluation,
        :hr_process => nil,
        :hr_process_dimension_e => nil
      ),
      stub_model(HrProcessEvaluation,
        :hr_process => nil,
        :hr_process_dimension_e => nil
      )
    ])
  end

  it "renders a list of hr_process_evaluations" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
