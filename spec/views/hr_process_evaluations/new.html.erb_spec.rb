require 'spec_helper'

describe "hr_process_evaluations/new" do
  before(:each) do
    assign(:hr_process_evaluation, stub_model(HrProcessEvaluation,
      :hr_process => nil,
      :hr_process_dimension_e => nil
    ).as_new_record)
  end

  it "renders new hr_process_evaluation form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_evaluations_path, :method => "post" do
      assert_select "input#hr_process_evaluation_hr_process", :name => "hr_process_evaluation[hr_process]"
      assert_select "input#hr_process_evaluation_hr_process_dimension_e", :name => "hr_process_evaluation[hr_process_dimension_e]"
    end
  end
end
