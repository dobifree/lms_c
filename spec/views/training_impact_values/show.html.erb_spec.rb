require 'spec_helper'

describe "training_impact_values/show" do
  before(:each) do
    @training_impact_value = assign(:training_impact_value, stub_model(TrainingImpactValue,
      :valor => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
  end
end
