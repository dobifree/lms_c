require 'spec_helper'

describe "training_impact_values/index" do
  before(:each) do
    assign(:training_impact_values, [
      stub_model(TrainingImpactValue,
        :valor => 1
      ),
      stub_model(TrainingImpactValue,
        :valor => 1
      )
    ])
  end

  it "renders a list of training_impact_values" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
