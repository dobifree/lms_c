require 'spec_helper'

describe "training_impact_values/edit" do
  before(:each) do
    @training_impact_value = assign(:training_impact_value, stub_model(TrainingImpactValue,
      :valor => 1
    ))
  end

  it "renders the edit training_impact_value form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => training_impact_values_path(@training_impact_value), :method => "post" do
      assert_select "input#training_impact_value_valor", :name => "training_impact_value[valor]"
    end
  end
end
