require 'spec_helper'

describe "sel_req_items/edit" do
  before(:each) do
    @sel_req_item = assign(:sel_req_item, stub_model(SelReqItem))
  end

  it "renders the edit sel_req_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_req_items_path(@sel_req_item), :method => "post" do
    end
  end
end
