require 'spec_helper'

describe "sel_req_items/new" do
  before(:each) do
    assign(:sel_req_item, stub_model(SelReqItem).as_new_record)
  end

  it "renders new sel_req_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sel_req_items_path, :method => "post" do
    end
  end
end
