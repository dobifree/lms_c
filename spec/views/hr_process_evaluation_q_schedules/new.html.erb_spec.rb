require 'spec_helper'

describe "hr_process_evaluation_q_schedules/new" do
  before(:each) do
    assign(:hr_process_evaluation_q_schedule, stub_model(HrProcessEvaluationQSchedule,
      :hr_process_evaluation => nil,
      :hr_evaluation_type_element => nil
    ).as_new_record)
  end

  it "renders new hr_process_evaluation_q_schedule form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_evaluation_q_schedules_path, :method => "post" do
      assert_select "input#hr_process_evaluation_q_schedule_hr_process_evaluation", :name => "hr_process_evaluation_q_schedule[hr_process_evaluation]"
      assert_select "input#hr_process_evaluation_q_schedule_hr_evaluation_type_element", :name => "hr_process_evaluation_q_schedule[hr_evaluation_type_element]"
    end
  end
end
