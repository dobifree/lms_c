require 'spec_helper'

describe "hr_process_evaluation_q_schedules/show" do
  before(:each) do
    @hr_process_evaluation_q_schedule = assign(:hr_process_evaluation_q_schedule, stub_model(HrProcessEvaluationQSchedule,
      :hr_process_evaluation => nil,
      :hr_evaluation_type_element => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
