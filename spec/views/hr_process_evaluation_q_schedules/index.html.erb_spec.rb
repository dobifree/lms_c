require 'spec_helper'

describe "hr_process_evaluation_q_schedules/index" do
  before(:each) do
    assign(:hr_process_evaluation_q_schedules, [
      stub_model(HrProcessEvaluationQSchedule,
        :hr_process_evaluation => nil,
        :hr_evaluation_type_element => nil
      ),
      stub_model(HrProcessEvaluationQSchedule,
        :hr_process_evaluation => nil,
        :hr_evaluation_type_element => nil
      )
    ])
  end

  it "renders a list of hr_process_evaluation_q_schedules" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
