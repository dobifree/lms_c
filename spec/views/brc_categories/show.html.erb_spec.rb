require 'spec_helper'

describe "brc_categories/show" do
  before(:each) do
    @brc_category = assign(:brc_category, stub_model(BrcCategory,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
