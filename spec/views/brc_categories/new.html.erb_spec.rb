require 'spec_helper'

describe "brc_categories/new" do
  before(:each) do
    assign(:brc_category, stub_model(BrcCategory,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new brc_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => brc_categories_path, :method => "post" do
      assert_select "input#brc_category_name", :name => "brc_category[name]"
    end
  end
end
