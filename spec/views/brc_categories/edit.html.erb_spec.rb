require 'spec_helper'

describe "brc_categories/edit" do
  before(:each) do
    @brc_category = assign(:brc_category, stub_model(BrcCategory,
      :name => "MyString"
    ))
  end

  it "renders the edit brc_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => brc_categories_path(@brc_category), :method => "post" do
      assert_select "input#brc_category_name", :name => "brc_category[name]"
    end
  end
end
