require 'spec_helper'

describe "dnc_characteristics/new" do
  before(:each) do
    assign(:dnc_characteristic, stub_model(DncCharacteristic,
      :characteristic => nil
    ).as_new_record)
  end

  it "renders new dnc_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_characteristics_path, :method => "post" do
      assert_select "input#dnc_characteristic_characteristic", :name => "dnc_characteristic[characteristic]"
    end
  end
end
