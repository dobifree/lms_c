require 'spec_helper'

describe "dnc_characteristics/show" do
  before(:each) do
    @dnc_characteristic = assign(:dnc_characteristic, stub_model(DncCharacteristic,
      :characteristic => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
  end
end
