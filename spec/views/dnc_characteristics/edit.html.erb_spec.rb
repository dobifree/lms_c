require 'spec_helper'

describe "dnc_characteristics/edit" do
  before(:each) do
    @dnc_characteristic = assign(:dnc_characteristic, stub_model(DncCharacteristic,
      :characteristic => nil
    ))
  end

  it "renders the edit dnc_characteristic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => dnc_characteristics_path(@dnc_characteristic), :method => "post" do
      assert_select "input#dnc_characteristic_characteristic", :name => "dnc_characteristic[characteristic]"
    end
  end
end
