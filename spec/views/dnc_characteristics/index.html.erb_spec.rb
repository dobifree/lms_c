require 'spec_helper'

describe "dnc_characteristics/index" do
  before(:each) do
    assign(:dnc_characteristics, [
      stub_model(DncCharacteristic,
        :characteristic => nil
      ),
      stub_model(DncCharacteristic,
        :characteristic => nil
      )
    ])
  end

  it "renders a list of dnc_characteristics" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
