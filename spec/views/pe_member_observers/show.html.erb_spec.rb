require 'spec_helper'

describe "pe_member_observers/show" do
  before(:each) do
    @pe_member_observer = assign(:pe_member_observer, stub_model(PeMemberObserver,
      :pe_member_observed_id => 1,
      :observer_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
  end
end
