require 'spec_helper'

describe "pe_member_observers/edit" do
  before(:each) do
    @pe_member_observer = assign(:pe_member_observer, stub_model(PeMemberObserver,
      :pe_member_observed_id => 1,
      :observer_id => 1
    ))
  end

  it "renders the edit pe_member_observer form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pe_member_observers_path(@pe_member_observer), :method => "post" do
      assert_select "input#pe_member_observer_pe_member_observed_id", :name => "pe_member_observer[pe_member_observed_id]"
      assert_select "input#pe_member_observer_observer_id", :name => "pe_member_observer[observer_id]"
    end
  end
end
