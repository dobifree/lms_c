require 'spec_helper'

describe "pe_member_observers/index" do
  before(:each) do
    assign(:pe_member_observers, [
      stub_model(PeMemberObserver,
        :pe_member_observed_id => 1,
        :observer_id => 2
      ),
      stub_model(PeMemberObserver,
        :pe_member_observed_id => 1,
        :observer_id => 2
      )
    ])
  end

  it "renders a list of pe_member_observers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
