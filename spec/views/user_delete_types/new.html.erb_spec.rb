require 'spec_helper'

describe "user_delete_types/new" do
  before(:each) do
    assign(:user_delete_type, stub_model(UserDeleteType,
      :position => 1,
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new user_delete_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => user_delete_types_path, :method => "post" do
      assert_select "input#user_delete_type_position", :name => "user_delete_type[position]"
      assert_select "input#user_delete_type_description", :name => "user_delete_type[description]"
    end
  end
end
