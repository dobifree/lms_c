require 'spec_helper'

describe "user_delete_types/edit" do
  before(:each) do
    @user_delete_type = assign(:user_delete_type, stub_model(UserDeleteType,
      :position => 1,
      :description => "MyString"
    ))
  end

  it "renders the edit user_delete_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => user_delete_types_path(@user_delete_type), :method => "post" do
      assert_select "input#user_delete_type_position", :name => "user_delete_type[position]"
      assert_select "input#user_delete_type_description", :name => "user_delete_type[description]"
    end
  end
end
