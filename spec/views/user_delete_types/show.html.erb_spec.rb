require 'spec_helper'

describe "user_delete_types/show" do
  before(:each) do
    @user_delete_type = assign(:user_delete_type, stub_model(UserDeleteType,
      :position => 1,
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Description/)
  end
end
