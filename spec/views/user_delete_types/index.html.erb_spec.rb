require 'spec_helper'

describe "user_delete_types/index" do
  before(:each) do
    assign(:user_delete_types, [
      stub_model(UserDeleteType,
        :position => 1,
        :description => "Description"
      ),
      stub_model(UserDeleteType,
        :position => 1,
        :description => "Description"
      )
    ])
  end

  it "renders a list of user_delete_types" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
