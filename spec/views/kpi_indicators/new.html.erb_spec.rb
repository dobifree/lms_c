require 'spec_helper'

describe "kpi_indicators/new" do
  before(:each) do
    assign(:kpi_indicator, stub_model(KpiIndicator,
      :position => 1,
      :name => "MyString",
      :weight => 1.5,
      :kpi_set => nil,
      :goal => 1.5,
      :unit => "MyString",
      :type => 1
    ).as_new_record)
  end

  it "renders new kpi_indicator form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_indicators_path, :method => "post" do
      assert_select "input#kpi_indicator_position", :name => "kpi_indicator[position]"
      assert_select "input#kpi_indicator_name", :name => "kpi_indicator[name]"
      assert_select "input#kpi_indicator_weight", :name => "kpi_indicator[weight]"
      assert_select "input#kpi_indicator_kpi_set", :name => "kpi_indicator[kpi_set]"
      assert_select "input#kpi_indicator_goal", :name => "kpi_indicator[goal]"
      assert_select "input#kpi_indicator_unit", :name => "kpi_indicator[unit]"
      assert_select "input#kpi_indicator_type", :name => "kpi_indicator[type]"
    end
  end
end
