require 'spec_helper'

describe "kpi_indicators/show" do
  before(:each) do
    @kpi_indicator = assign(:kpi_indicator, stub_model(KpiIndicator,
      :position => 1,
      :name => "Name",
      :weight => 1.5,
      :kpi_set => nil,
      :goal => 1.5,
      :unit => "Unit",
      :type => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Name/)
    rendered.should match(/1.5/)
    rendered.should match(//)
    rendered.should match(/1.5/)
    rendered.should match(/Unit/)
    rendered.should match(/2/)
  end
end
