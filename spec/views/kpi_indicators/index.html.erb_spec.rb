require 'spec_helper'

describe "kpi_indicators/index" do
  before(:each) do
    assign(:kpi_indicators, [
      stub_model(KpiIndicator,
        :position => 1,
        :name => "Name",
        :weight => 1.5,
        :kpi_set => nil,
        :goal => 1.5,
        :unit => "Unit",
        :type => 2
      ),
      stub_model(KpiIndicator,
        :position => 1,
        :name => "Name",
        :weight => 1.5,
        :kpi_set => nil,
        :goal => 1.5,
        :unit => "Unit",
        :type => 2
      )
    ])
  end

  it "renders a list of kpi_indicators" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Unit".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
