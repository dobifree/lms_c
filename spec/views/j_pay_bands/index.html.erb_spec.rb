require 'spec_helper'

describe "j_pay_bands/index" do
  before(:each) do
    assign(:j_pay_bands, [
      stub_model(JPayBand,
        :name => "Name",
        :min_value => "",
        :min_value => "",
        :max_value => "",
        :max_value => "",
        :active => false
      ),
      stub_model(JPayBand,
        :name => "Name",
        :min_value => "",
        :min_value => "",
        :max_value => "",
        :max_value => "",
        :active => false
      )
    ])
  end

  it "renders a list of j_pay_bands" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
