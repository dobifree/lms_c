require 'spec_helper'

describe "j_pay_bands/new" do
  before(:each) do
    assign(:j_pay_band, stub_model(JPayBand,
      :name => "MyString",
      :min_value => "",
      :min_value => "",
      :max_value => "",
      :max_value => "",
      :active => false
    ).as_new_record)
  end

  it "renders new j_pay_band form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => j_pay_bands_path, :method => "post" do
      assert_select "input#j_pay_band_name", :name => "j_pay_band[name]"
      assert_select "input#j_pay_band_min_value", :name => "j_pay_band[min_value]"
      assert_select "input#j_pay_band_min_value", :name => "j_pay_band[min_value]"
      assert_select "input#j_pay_band_max_value", :name => "j_pay_band[max_value]"
      assert_select "input#j_pay_band_max_value", :name => "j_pay_band[max_value]"
      assert_select "input#j_pay_band_active", :name => "j_pay_band[active]"
    end
  end
end
