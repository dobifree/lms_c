require 'spec_helper'

describe "j_pay_bands/show" do
  before(:each) do
    @j_pay_band = assign(:j_pay_band, stub_model(JPayBand,
      :name => "Name",
      :min_value => "",
      :min_value => "",
      :max_value => "",
      :max_value => "",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
  end
end
