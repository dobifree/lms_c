require 'spec_helper'

describe "kpi_dashboard_guests/index" do
  before(:each) do
    assign(:kpi_dashboard_guests, [
      stub_model(KpiDashboardGuest,
        :kpi_dashboard => nil,
        :user => nil
      ),
      stub_model(KpiDashboardGuest,
        :kpi_dashboard => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of kpi_dashboard_guests" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
