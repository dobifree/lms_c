require 'spec_helper'

describe "kpi_dashboard_guests/edit" do
  before(:each) do
    @kpi_dashboard_guest = assign(:kpi_dashboard_guest, stub_model(KpiDashboardGuest,
      :kpi_dashboard => nil,
      :user => nil
    ))
  end

  it "renders the edit kpi_dashboard_guest form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_dashboard_guests_path(@kpi_dashboard_guest), :method => "post" do
      assert_select "input#kpi_dashboard_guest_kpi_dashboard", :name => "kpi_dashboard_guest[kpi_dashboard]"
      assert_select "input#kpi_dashboard_guest_user", :name => "kpi_dashboard_guest[user]"
    end
  end
end
