require 'spec_helper'

describe "kpi_dashboard_guests/new" do
  before(:each) do
    assign(:kpi_dashboard_guest, stub_model(KpiDashboardGuest,
      :kpi_dashboard => nil,
      :user => nil
    ).as_new_record)
  end

  it "renders new kpi_dashboard_guest form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kpi_dashboard_guests_path, :method => "post" do
      assert_select "input#kpi_dashboard_guest_kpi_dashboard", :name => "kpi_dashboard_guest[kpi_dashboard]"
      assert_select "input#kpi_dashboard_guest_user", :name => "kpi_dashboard_guest[user]"
    end
  end
end
