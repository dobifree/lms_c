require 'spec_helper'

describe "hr_process_levels/edit" do
  before(:each) do
    @hr_process_level = assign(:hr_process_level, stub_model(HrProcessLevel,
      :nombre => "MyString",
      :hr_process => nil
    ))
  end

  it "renders the edit hr_process_level form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_levels_path(@hr_process_level), :method => "post" do
      assert_select "input#hr_process_level_nombre", :name => "hr_process_level[nombre]"
      assert_select "input#hr_process_level_hr_process", :name => "hr_process_level[hr_process]"
    end
  end
end
