require 'spec_helper'

describe "hr_process_levels/new" do
  before(:each) do
    assign(:hr_process_level, stub_model(HrProcessLevel,
      :nombre => "MyString",
      :hr_process => nil
    ).as_new_record)
  end

  it "renders new hr_process_level form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => hr_process_levels_path, :method => "post" do
      assert_select "input#hr_process_level_nombre", :name => "hr_process_level[nombre]"
      assert_select "input#hr_process_level_hr_process", :name => "hr_process_level[hr_process]"
    end
  end
end
