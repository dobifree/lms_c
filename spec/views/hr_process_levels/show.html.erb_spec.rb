require 'spec_helper'

describe "hr_process_levels/show" do
  before(:each) do
    @hr_process_level = assign(:hr_process_level, stub_model(HrProcessLevel,
      :nombre => "Nombre",
      :hr_process => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nombre/)
    rendered.should match(//)
  end
end
