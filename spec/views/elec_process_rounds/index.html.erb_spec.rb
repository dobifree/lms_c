require 'spec_helper'

describe "elec_process_rounds/index" do
  before(:each) do
    assign(:elec_process_rounds, [
      stub_model(ElecProcessRound,
        :elec_process => nil,
        :name => "Name",
        :description => "MyText",
        :position => 1,
        :mandatory => false,
        :lifespan => 2,
        :qty_required => 3,
        :show_live_results => false,
        :show_final_results => false,
        :threshold_pctg_universe => 4,
        :threshold_pctg_voters => 5,
        :threshold_qty_voters => 6,
        :qty_min_podium => 7
      ),
      stub_model(ElecProcessRound,
        :elec_process => nil,
        :name => "Name",
        :description => "MyText",
        :position => 1,
        :mandatory => false,
        :lifespan => 2,
        :qty_required => 3,
        :show_live_results => false,
        :show_final_results => false,
        :threshold_pctg_universe => 4,
        :threshold_pctg_voters => 5,
        :threshold_qty_voters => 6,
        :qty_min_podium => 7
      )
    ])
  end

  it "renders a list of elec_process_rounds" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
  end
end
