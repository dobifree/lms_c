require 'spec_helper'

describe "elec_process_rounds/show" do
  before(:each) do
    @elec_process_round = assign(:elec_process_round, stub_model(ElecProcessRound,
      :elec_process => nil,
      :name => "Name",
      :description => "MyText",
      :position => 1,
      :mandatory => false,
      :lifespan => 2,
      :qty_required => 3,
      :show_live_results => false,
      :show_final_results => false,
      :threshold_pctg_universe => 4,
      :threshold_pctg_voters => 5,
      :threshold_qty_voters => 6,
      :qty_min_podium => 7
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(/false/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/4/)
    rendered.should match(/5/)
    rendered.should match(/6/)
    rendered.should match(/7/)
  end
end
