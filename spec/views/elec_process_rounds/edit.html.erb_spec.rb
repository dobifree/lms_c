require 'spec_helper'

describe "elec_process_rounds/edit" do
  before(:each) do
    @elec_process_round = assign(:elec_process_round, stub_model(ElecProcessRound,
      :elec_process => nil,
      :name => "MyString",
      :description => "MyText",
      :position => 1,
      :mandatory => false,
      :lifespan => 1,
      :qty_required => 1,
      :show_live_results => false,
      :show_final_results => false,
      :threshold_pctg_universe => 1,
      :threshold_pctg_voters => 1,
      :threshold_qty_voters => 1,
      :qty_min_podium => 1
    ))
  end

  it "renders the edit elec_process_round form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => elec_process_rounds_path(@elec_process_round), :method => "post" do
      assert_select "input#elec_process_round_elec_process", :name => "elec_process_round[elec_process]"
      assert_select "input#elec_process_round_name", :name => "elec_process_round[name]"
      assert_select "textarea#elec_process_round_description", :name => "elec_process_round[description]"
      assert_select "input#elec_process_round_position", :name => "elec_process_round[position]"
      assert_select "input#elec_process_round_mandatory", :name => "elec_process_round[mandatory]"
      assert_select "input#elec_process_round_lifespan", :name => "elec_process_round[lifespan]"
      assert_select "input#elec_process_round_qty_required", :name => "elec_process_round[qty_required]"
      assert_select "input#elec_process_round_show_live_results", :name => "elec_process_round[show_live_results]"
      assert_select "input#elec_process_round_show_final_results", :name => "elec_process_round[show_final_results]"
      assert_select "input#elec_process_round_threshold_pctg_universe", :name => "elec_process_round[threshold_pctg_universe]"
      assert_select "input#elec_process_round_threshold_pctg_voters", :name => "elec_process_round[threshold_pctg_voters]"
      assert_select "input#elec_process_round_threshold_qty_voters", :name => "elec_process_round[threshold_qty_voters]"
      assert_select "input#elec_process_round_qty_min_podium", :name => "elec_process_round[qty_min_podium]"
    end
  end
end
