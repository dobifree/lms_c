USER='ubuntu'
ANIO=$(date +%Y)

BACKUP=/home/$USER/backups

mkdir -p $BACKUP/$ANIO
CARPETA=$BACKUP/$ANIO/$(date +%Y-%m-%d_%Hh%Mm)
mkdir -p $CARPETA

mysqldump -uroot lms_dev > $CARPETA/lms_dev.sql --single-transaction -R
mysqldump -uroot lms > $CARPETA/lms.sql --single-transaction -R
mysqldump -uroot lms_hochschild > $CARPETA/lms_hochschild.sql --single-transaction -R
mysqldump -uroot lms_hochschild_training > $CARPETA/lms_hochschild_training.sql --single-transaction -R
mysqldump -uroot euroamerica > $CARPETA/euroamerica.sql --single-transaction -R
mysqldump -uroot lms_pacasmayo > $CARPETA/lms_pacasmayo.sql --single-transaction -R
mysqldump -uroot lms_pacasmayo_training > $CARPETA/lms_pacasmayo_training.sql --single-transaction -R
mysqldump -uroot lms_corpvida > $CARPETA/lms_corpvida.sql --single-transaction -R
mysqldump -uroot lms_pacifico > $CARPETA/lms_pacifico.sql --single-transaction -R
mysqldump -uroot lms_pacifico_training > $CARPETA/lms_pacifico_training.sql --single-transaction -R
mysqldump -uroot lms_fundacionsofofa > $CARPETA/lms_fundacionsofofa.sql --single-transaction -R
mysqldump -uroot lms_inkafarma_training > $CARPETA/lms_inkafarma_training.sql --single-transaction -R
mysqldump -uroot lms_inkafarma > $CARPETA/lms_inkafarma.sql --single-transaction -R
mysqldump -uroot lms_lan_training > $CARPETA/lms_lan_training.sql --single-transaction -R
mysqldump -uroot lms_proderco > $CARPETA/lms_proderco.sql --single-transaction -R
mysqldump -uroot lms_financiero_training > $CARPETA/lms_financiero_training.sql --single-transaction -R
mysqldump -uroot lms_financiero > $CARPETA/lms_financiero.sql --single-transaction -R
mysqldump -uroot lms_cmac_sullana > $CARPETA/lms_cmac_sullana.sql --single-transaction -R
mysqldump -uroot lms_prima_training > $CARPETA/lms_prima_training.sql --single-transaction -R
mysqldump -uroot lms_prima > $CARPETA/lms_prima.sql --single-transaction -R
mysqldump -uroot lms_gthpensum > $CARPETA/lms_gthpensum.sql --single-transaction -R
mysqldump -uroot lms_skcapacitacion_training > $CARPETA/lms_skcapacitacion_training.sql --single-transaction -R
mysqldump -uroot lms_skcapacitacion > $CARPETA/lms_skcapacitacion.sql --single-transaction -R
mysqldump -uroot lms_security > $CARPETA/lms_security.sql --single-transaction -R
mysqldump -uroot lms_security_training > $CARPETA/lms_security_training.sql --single-transaction -R
mysqldump -uroot lms_robocon > $CARPETA/lms_robocon.sql --single-transaction -R
mysqldump -uroot lms_visanet > $CARPETA/lms_visanet.sql --single-transaction -R
mysqldump -uroot lms_visanet_training > $CARPETA/lms_visanet_training.sql --single-transaction -R
mysqldump -uroot lms_danper > $CARPETA/lms_danper.sql --single-transaction -R
mysqldump -uroot lms_danper_training > $CARPETA/lms_danper_training.sql --single-transaction -R
mysqldump -uroot lms_demo_capitalteam > $CARPETA/lms_demo_capitalteam.sql --single-transaction -R
mysqldump -uroot lms_seduc > $CARPETA/lms_seduc.sql --single-transaction -R
mysqldump -uroot lms_seduc_training > $CARPETA/lms_seduc_training.sql --single-transaction -R
mysqldump -uroot lms_carsa > $CARPETA/lms_carsa.sql --single-transaction -R
mysqldump -uroot lms_carsa_training > $CARPETA/lms_carsa_training.sql --single-transaction -R
mysqldump -uroot lms_diners > $CARPETA/lms_diners.sql --single-transaction -R
mysqldump -uroot lms_diners_training > $CARPETA/lms_diners_training.sql --single-transaction -R
mysqldump -uroot lms_euroamerica_training > $CARPETA/lms_euroamerica_training.sql --single-transaction -R
mysqldump -uroot lms_eternit > $CARPETA/lms_eternit.sql --single-transaction -R
mysqldump -uroot lms_eternit_training > $CARPETA/lms_eternit_training.sql --single-transaction -R
mysqldump -uroot lms_tmluc > $CARPETA/lms_tmluc.sql --single-transaction -R
mysqldump -uroot lms_tmluc_training > $CARPETA/lms_tmluc_training.sql --single-transaction -R
mysqldump -uroot lms_primus > $CARPETA/lms_primus.sql --single-transaction -R
mysqldump -uroot lms_primus_training > $CARPETA/lms_primus_training.sql --single-transaction -R
mysqldump -uroot lms_bancofalabella > $CARPETA/lms_bancofalabella.sql --single-transaction -R
mysqldump -uroot lms_bancofalabella_training > $CARPETA/lms_bancofalabella_training.sql --single-transaction -R
mysqldump -uroot lms_loga_training > $CARPETA/lms_loga_training.sql --single-transaction -R
mysqldump -uroot lms_pcfactory_training > $CARPETA/lms_pcfactory_training.sql --single-transaction -R
mysqldump -uroot lms_pcfactory > $CARPETA/lms_pcfactory.sql --single-transaction -R
mysqldump -uroot lms_luzdelsur_training > $CARPETA/lms_luzdelsur_training.sql --single-transaction -R
mysqldump -uroot lms_luzdelsur > $CARPETA/lms_luzdelsur.sql --single-transaction -R
mysqldump -uroot lms_utpidat > $CARPETA/lms_utpidat.sql --single-transaction -R
mysqldump -uroot lms_utpidat_training > $CARPETA/lms_utpidat_training.sql --single-transaction -R
mysqldump -uroot lms_tcs > $CARPETA/lms_tcs.sql --single-transaction -R
mysqldump -uroot lms_tcs_training > $CARPETA/lms_tcs_training.sql --single-transaction -R

cd $CARPETA

tar czfv  lms_dev.tar.gz lms_dev.sql --remove-files
tar czfv  lms.tar.gz lms.sql --remove-files
tar czfv  lms_hochschild.tar.gz lms_hochschild.sql --remove-files
tar czfv  lms_hochschild_training.tar.gz lms_hochschild_training.sql --remove-files
tar czfv  euroamerica.tar.gz euroamerica.sql --remove-files
tar czfv  lms_pacasmayo.tar.gz lms_pacasmayo.sql --remove-files
tar czfv  lms_pacasmayo_training.tar.gz lms_pacasmayo_training.sql --remove-files
tar czfv  lms_corpvida.tar.gz lms_corpvida.sql --remove-files
tar czfv  lms_pacifico.tar.gz lms_pacifico.sql --remove-files
tar czfv  lms_pacifico_training.tar.gz lms_pacifico_training.sql --remove-files
tar czfv  lms_fundacionsofofa.tar.gz lms_fundacionsofofa.sql --remove-files
tar czfv  lms_inkafarma_training.tar.gz lms_inkafarma_training.sql --remove-files
tar czfv  lms_inkafarma.tar.gz lms_inkafarma.sql --remove-files
tar czfv  lms_lan_training.tar.gz lms_lan_training.sql --remove-files
tar czfv  lms_proderco.tar.gz lms_proderco.sql --remove-files
tar czfv  lms_financiero_training.tar.gz lms_financiero_training.sql --remove-files
tar czfv  lms_financiero.tar.gz lms_financiero.sql --remove-files
tar czfv  lms_cmac_sullana.tar.gz lms_cmac_sullana.sql --remove-files
tar czfv  lms_prima_training.tar.gz lms_prima_training.sql --remove-files
tar czfv  lms_prima.tar.gz lms_prima.sql --remove-files
tar czfv  lms_gthpensum.tar.gz lms_gthpensum.sql --remove-files
tar czfv  lms_skcapacitacion_training.tar.gz lms_skcapacitacion_training.sql --remove-files
tar czfv  lms_skcapacitacion.tar.gz lms_skcapacitacion.sql --remove-files
tar czfv  lms_security.tar.gz lms_security.sql --remove-files
tar czfv  lms_security_training.tar.gz lms_security_training.sql --remove-files
tar czfv  lms_robocon.tar.gz lms_robocon.sql --remove-files
tar czfv  lms_visanet.tar.gz lms_visanet.sql --remove-files
tar czfv  lms_visanet_training.tar.gz lms_visanet_training.sql --remove-files
tar czfv  lms_danper.tar.gz lms_danper.sql --remove-files
tar czfv  lms_danper_training.tar.gz lms_danper_training.sql --remove-files
tar czfv  lms_demo_capitalteam.tar.gz lms_demo_capitalteam.sql --remove-files
tar czfv  lms_seduc.tar.gz lms_seduc.sql --remove-files
tar czfv  lms_seduc_training.tar.gz lms_seduc_training.sql --remove-files
tar czfv  lms_carsa.tar.gz lms_carsa.sql --remove-files
tar czfv  lms_carsa_training.tar.gz lms_carsa_training.sql --remove-files
tar czfv  lms_diners.tar.gz lms_diners.sql --remove-files
tar czfv  lms_diners_training.tar.gz lms_diners_training.sql --remove-files
tar czfv  lms_euroamerica_training.tar.gz lms_euroamerica_training.sql --remove-files
tar czfv  lms_eternit.tar.gz lms_eternit.sql --remove-files
tar czfv  lms_eternit_training.tar.gz lms_eternit_training.sql --remove-files
tar czfv  lms_tmluc.tar.gz lms_tmluc.sql --remove-files
tar czfv  lms_tmluc_training.tar.gz lms_tmluc_training.sql --remove-files
tar czfv  lms_primus.tar.gz lms_primus.sql --remove-files
tar czfv  lms_primus_training.tar.gz lms_primus_training.sql --remove-files
tar czfv  lms_bancofalabella.tar.gz lms_bancofalabella.sql --remove-files
tar czfv  lms_bancofalabella_training.tar.gz lms_bancofalabella_training.sql --remove-files
tar czfv  lms_loga_training.tar.gz lms_loga_training.sql --remove-files
tar czfv  lms_pcfactory_training.tar.gz lms_pcfactory_training.sql --remove-files
tar czfv  lms_pcfactory.tar.gz lms_pcfactory.sql --remove-files
tar czfv  lms_luzdelsur_training.tar.gz lms_luzdelsur_training.sql --remove-files
tar czfv  lms_luzdelsur.tar.gz lms_luzdelsur.sql --remove-files
tar czfv  lms_utpidat.tar.gz lms_utpidat.sql --remove-files
tar czfv  lms_utpidat_training.tar.gz lms_utpidat_training.sql --remove-files
tar czfv  lms_tcs.tar.gz lms_tcs.sql --remove-files
tar czfv  lms_tcs_training.tar.gz lms_tcs_training.sql --remove-files