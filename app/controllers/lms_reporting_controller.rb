class LmsReportingController < ApplicationController

  before_filter :authenticate_user

  before_filter :verify_access_manage_module

  def courses

   #@company = session[:company]
    @programs = Program.active_programs_managed_by_manager

  end

  def program_course

   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course

  end

  def generate_r1

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    @elements, @elements_ids = @course.ordered_elements

    @lms_characteristics = LmsCharacteristic.reporting

    filename = 'reporte_' + @course.nombre.gsub(/\s/, '_') + '.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def generate_r2

   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course

    @lms_characteristics = LmsCharacteristic.reporting
    @training_characteristics = TrainingCharacteristic.rep2_characteristics

    filename = 'reporte_' + @course.nombre.gsub(/\s/, '_') + '.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end



  def generate_r3

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    @lms_characteristics = LmsCharacteristic.reporting_acta
    @training_characteristics = TrainingCharacteristic.rep3_characteristics

    filename = 'acta_' + @course.nombre.gsub(/\s/, '_') + '.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def generate_r2_0
   #@company = session[:company]

    @lms_characteristics = LmsCharacteristic.reporting
    @training_characteristics = TrainingCharacteristic.rep2_characteristics

    @training_characteristics_filter_course = Array.new

    xlsx_filename = 'reporte_general.xlsx'
    respond_to do |format|
      format.xlsx { render xlsx: 'excel_generate_r20.xlsx', filename: xlsx_filename }
    end
  end

  def generate_r2_1
   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course

    @lms_characteristics = LmsCharacteristic.reporting
    @training_characteristics = TrainingCharacteristic.rep2_characteristics

    xlsx_filename = 'reporte_' + @course.nombre.gsub(/\s/, '_') + '.xlsx'
    respond_to do |format|
      format.xlsx { render xlsx: 'excel_generate_r2.xlsx', filename: xlsx_filename }
    end
  end

  def generate_r3_1

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    @lms_characteristics = LmsCharacteristic.reporting_acta
    @training_characteristics = TrainingCharacteristic.rep3_characteristics

    filename = 'acta_' + @course.nombre.gsub(/\s/, '_') + '.xls'

    respond_to do |format|
      format.xlsx { render xlsx: 'excel_generate_r3.xlsx', filename: filename }
    end

  end

  def generate_pool_program_course
   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course


    reporte_excel = generate_report_pool_pc @program_course

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte_encuestas'+@program_course.course.nombre+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'lms', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1) || user_connected.lms_reports || is_lms_manage_enroll_manager?
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def generate_report_pool_pc(program_course)

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      program_course.course.polls.each do |poll|

        wb.add_worksheet(name: poll.nombre) do |reporte_excel|

          fila_actual = 1

          fila = Array.new
          filaHeader = Array.new

          fila << 'Curso'
          filaHeader << headerCenter
          fila << program_course.course.nombre
          filaHeader << headerCenter
          fila << ''
          filaHeader << headerCenter

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('B'+fila_actual.to_s+':C'+fila_actual.to_s)
          fila_actual += 1

          program_course.program_course_instances.each do |program_course_instance|

            if program_course_instance.from_date && program_course_instance.to_date

              fila << program_course_instance.description+ ', '+(localize program_course_instance.from_date, format: :full_date)+' - '+(localize program_course_instance.to_date, format: :full_date)
              filaHeader << headerCenter

            end

          end

          program_course.program_course_instances.each do |program_course_instance|

            if program_course_instance.from_date && program_course_instance.to_date

              if poll && poll.master_poll

                master_questions = poll.master_poll.master_poll_questions

                master_questions.each do |master_question|

                  fila = Array.new
                  fileStyle = Array.new

                  texto_q = master_question.texto.split('<br>')

                  texto_q_joined = ''
                  alto_fila = 0
                  texto_q.each_with_index do |t_q, numero_lineas|

                    if numero_lineas == 0

                      texto_q_joined = '= "'+ActionView::Base.full_sanitizer.sanitize(t_q)+'" '


                    else

                      texto_q_joined += '& CHAR(10) & "'+ActionView::Base.full_sanitizer.sanitize(t_q)+'" '

                    end
                    alto_fila += 20
                  end

                  fila << texto_q_joined
                  fileStyle << headerLeft
                  fila << ''
                  fileStyle << headerLeft
                  fila << ''
                  fileStyle << headerLeft

                  reporte_excel.add_row fila, :style => fileStyle
                  reporte_excel.merge_cells('A'+fila_actual.to_s+':C'+fila_actual.to_s)

                  fila_actual += 1

                  lista_alternativas = master_question.master_poll_alternatives

                  lista_alternativas.each_with_index do |alt|

                    fila = Array.new
                    fileStyle = Array.new

                    fila << alt.letra
                    fileStyle << fieldLeft

                    texto_a = alt.texto.split('<br>')

                    texto_a_joined = ''
                    alto_fila = 0
                    texto_a.each_with_index do |t_a, numero_lineas|

                      if numero_lineas == 0

                        texto_a_joined = '= "'+ActionView::Base.full_sanitizer.sanitize(t_a)+'" '

                      else

                        texto_a_joined += '& CHAR(10) & "'+ActionView::Base.full_sanitizer.sanitize(t_a)+'" '

                      end
                      alto_fila += 20
                    end

                    fila << texto_a_joined
                    fileStyle << fieldLeft

                    program_course_poll_summary = program_course_instance.get_program_course_poll_summary(poll, master_question, alt)

                    total_resp_alts = 0

                    total_resp_alts = program_course_poll_summary.numero_respuestas if program_course_poll_summary

                    fila << total_resp_alts
                    fileStyle << fieldLeft

                    reporte_excel.add_row fila, :style => fileStyle, height: alto_fila

                    fila_actual += 1

                  end

                end


              end

            end

          end

        end

      end



    end

    return reporte

  end

end
