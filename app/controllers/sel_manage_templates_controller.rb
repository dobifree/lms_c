class SelManageTemplatesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authorized_to_manage_processes

  # GET /sel_manage_templates
  # GET /sel_manage_templates.json
  def index
    @sel_templates = SelTemplate.all
  end

  def show_assign_attendants
    @sel_template = SelTemplate.find(params[:id])
    @users = User.find_all_by_activo(true).map { |user| [user.codigo + ' - ' +  user.full_name, user.id] }

    render layout: 'flat'
  end

  def assign_attendants
    @sel_template = SelTemplate.find(params[:id])

    if params[:sel_step_id]
      # sel_step2 = Object.new()
      params[:sel_step_id].each_key do |sel_step_id|
        sel_step = SelStep.find(sel_step_id)


        ################################################################
        #validate_user_process(sel_step_candidate.sel_process_step.sel_process_id)
        ################################################################

        SelTemplateAttendant.destroy_all(:sel_step_id => sel_step.id, :role => params[:role])


        if params[:user]

          # Necesario para poder tener un valor 'sin asignar'
          params[:user].delete_if { |value| value == '' }

          params[:user].each do |user_id|

            sel_template_attendant = SelTemplateAttendant.new()
            sel_template_attendant.sel_step_id = sel_step.id
            sel_template_attendant.user_id = user_id.to_s
            sel_template_attendant.role = params[:role]

            if !sel_template_attendant.save
              flash[:danger] = sel_template_attendant.user.full_name + ' ya ha sido asignado a otro rol. No se puede volver a asignar'
            else
              flash[:success] = 'Los responsables fueron asignados correctamente'
            end
          end
        end
      end
      redirect_to sel_manage_templates_show_assign_attendants_path(@sel_template)
    else
      flash[:danger] = 'No seleccionó ningún paso, no se pudo guardar nada'
      redirect_to :back
    end

  end




end
