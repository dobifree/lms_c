class CtModuleManagersController < ApplicationController

  include UsersHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def search

    @ct_module = CtModule.find(params[:ct_module_id])

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end


  end

  def create



    ct_module_manager = CtModuleManager.new
    ct_module_manager.ct_module_id = params[:ct_module_id]
    ct_module_manager.user_id = params[:user_id]

    ct_module_manager.save

    redirect_to ct_module_managers_search_path(params[:ct_module_id])


  end

  def destroy
    ct_module_manager = CtModuleManager.find(params[:id])
    ct_module_id = ct_module_manager.ct_module_id
    ct_module_manager.destroy

    redirect_to ct_modules_path(ct_module_id)

  end
end
