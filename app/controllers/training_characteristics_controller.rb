class TrainingCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /training_characteristics
  # GET /training_characteristics.json
  def index
    @training_characteristics = TrainingCharacteristic.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @training_characteristics }
    end
  end

  # GET /training_characteristics/1
  # GET /training_characteristics/1.json
  def show
    @training_characteristic = TrainingCharacteristic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @training_characteristic }
    end
  end

  # GET /training_characteristics/new
  # GET /training_characteristics/new.json
  def new
    @training_characteristic = TrainingCharacteristic.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @training_characteristic }
    end
  end

  # GET /training_characteristics/1/edit
  def edit
    @training_characteristic = TrainingCharacteristic.find(params[:id])
  end

  # POST /training_characteristics
  # POST /training_characteristics.json
  def create
    @training_characteristic = TrainingCharacteristic.new(params[:training_characteristic])

    if @training_characteristic.save

      if params[:characteristic_values]

        if File.extname(params[:characteristic_values].original_filename) == '.xlsx'

          directorio_temporal = '/tmp/'
          nombre_temporal = SecureRandom.hex(5)+'.xlsx'
          archivo_temporal = directorio_temporal+nombre_temporal

          path = File.join(directorio_temporal, nombre_temporal)
          File.open(path, 'wb') { |f| f.write(params[:characteristic_values].read) }

          update_characteristic_values_from_excel archivo_temporal

          flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

        else

          flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

        end

      elsif
      flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
      end

      flash[:success] = t('activerecord.success.model.characteristic.create_ok')
      redirect_to @training_characteristic
    else
      render action: 'new'
    end
  end

  # PUT /training_characteristics/1
  # PUT /training_characteristics/1.json
  def update
    @training_characteristic = TrainingCharacteristic.find(params[:id])

    if @training_characteristic.update_attributes(params[:training_characteristic])

      if params[:characteristic_values]

        if File.extname(params[:characteristic_values].original_filename) == '.xlsx'

          directorio_temporal = '/tmp/'
          nombre_temporal = SecureRandom.hex(5)+'.xlsx'
          archivo_temporal = directorio_temporal+nombre_temporal

          path = File.join(directorio_temporal, nombre_temporal)
          File.open(path, 'wb') { |f| f.write(params[:characteristic_values].read) }

          update_characteristic_values_from_excel archivo_temporal

          flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

        else

          flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

        end

      elsif
        flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
      end

      flash[:success] = t('activerecord.success.model.characteristic.create_ok')
      redirect_to @training_characteristic

    else
      render action: 'edit'
    end

  end

  # DELETE /training_characteristics/1
  # DELETE /training_characteristics/1.json
  def destroy
    @training_characteristic = TrainingCharacteristic.find(params[:id])
    @training_characteristic.destroy

    respond_to do |format|
      format.html { redirect_to training_characteristics_url }
      format.json { head :no_content }
    end
  end

  private

  def update_characteristic_values_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data


    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        unless fila[0].blank?

          characteristic_value = @training_characteristic.training_characteristic_values.where('value_string = ?', fila[1]).first

          characteristic_value = @training_characteristic.training_characteristic_values.build unless characteristic_value

          characteristic_value.position = fila[0]
          characteristic_value.value_string = fila[1]
          characteristic_value.save

        end

      end

    end

  end

end
