class PeMemberObserversController < ApplicationController
  # GET /pe_member_observers
  # GET /pe_member_observers.json
  def index
    @pe_member_observers = PeMemberObserver.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_member_observers }
    end
  end

  # GET /pe_member_observers/1
  # GET /pe_member_observers/1.json
  def show
    @pe_member_observer = PeMemberObserver.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_member_observer }
    end
  end

  # GET /pe_member_observers/new
  # GET /pe_member_observers/new.json
  def new
    @pe_member_observer = PeMemberObserver.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pe_member_observer }
    end
  end

  # GET /pe_member_observers/1/edit
  def edit
    @pe_member_observer = PeMemberObserver.find(params[:id])
  end

  # POST /pe_member_observers
  # POST /pe_member_observers.json
  def create
    @pe_member_observer = PeMemberObserver.new(params[:pe_member_observer])

    respond_to do |format|
      if @pe_member_observer.save
        format.html { redirect_to @pe_member_observer, notice: 'Pe member observer was successfully created.' }
        format.json { render json: @pe_member_observer, status: :created, location: @pe_member_observer }
      else
        format.html { render action: "new" }
        format.json { render json: @pe_member_observer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pe_member_observers/1
  # PUT /pe_member_observers/1.json
  def update
    @pe_member_observer = PeMemberObserver.find(params[:id])

    respond_to do |format|
      if @pe_member_observer.update_attributes(params[:pe_member_observer])
        format.html { redirect_to @pe_member_observer, notice: 'Pe member observer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pe_member_observer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pe_member_observers/1
  # DELETE /pe_member_observers/1.json
  def destroy
    @pe_member_observer = PeMemberObserver.find(params[:id])
    @pe_member_observer.destroy

    respond_to do |format|
      format.html { redirect_to pe_member_observers_url }
      format.json { head :no_content }
    end
  end
end
