class BrcCategoriesController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  # GET /brc_categories
  # GET /brc_categories.json
  def index
    @brc_categories = BrcCategory.all
  end

  # GET /brc_categories/1
  # GET /brc_categories/1.json
  def show
    @brc_category = BrcCategory.find(params[:id])
  end

  # GET /brc_categories/new
  # GET /brc_categories/new.json
  def new
    @brc_category = BrcCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @brc_category }
    end
  end

  # GET /brc_categories/1/edit
  def edit
    @brc_category = BrcCategory.find(params[:id])
  end

  # POST /brc_categories
  # POST /brc_categories.json
  def create

    @brc_category = BrcCategory.new(params[:brc_category])

    if @brc_category.save
      flash[:success] = t('activerecord.success.model.brc_category.create_ok')
      redirect_to brc_categories_path
    else
      render action: 'new'
    end

  end

  # PUT /brc_categories/1
  # PUT /brc_categories/1.json
  def update
    @brc_category = BrcCategory.find(params[:id])

    if @brc_category.update_attributes(params[:brc_category])
      flash[:success] = t('activerecord.success.model.brc_category.update_ok')
      redirect_to brc_categories_path
    else
      render action: 'edit'
    end
  end

  # DELETE /brc_categories/1
  # DELETE /brc_categories/1.json
  def destroy
    @brc_category = BrcCategory.find(params[:id])
    @brc_category.destroy

    respond_to do |format|
      format.html { redirect_to brc_categories_url }
      format.json { head :no_content }
    end
  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'brc', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

end
