class SelResultsController < ApplicationController

  include MenuHelper

  before_filter :authenticate_user, :except => [:download_file]
  before_filter :has_access_to_process, only: :index
  before_filter :has_access_set_results, only: [:show_set_results, :set_results]
  before_filter :can_download_file, :only => [:download_file]

  def index
    @sel_process = SelProcess.find(params[:sel_process_id])
   #@company = session[:company]
    #@pending_steps = pending_steps(@sel_process.id, user_connected.id)
    @assigned_steps = assigned_steps(@sel_process.id, user_connected.id)
  end

  def list_pending_processes
    @sel_processes = assigned_process(user_connected.id)
  end

  def show_set_results

    @from_process = 1 if params[:from_process]

    @prev_sel_step_candidate_id = params[:prev_sel_step_candidate_id] if params[:prev_sel_step_candidate_id]
    @prev_sel_step_candidate = SelStepCandidate.find(@prev_sel_step_candidate_id) if @prev_sel_step_candidate_id

   #@company = session[:company]
    @sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])
    @sel_process = @sel_step_candidate.sel_process_step.sel_process

    @validation_enabled = !has_pending_answers?(@sel_step_candidate.id)

    @sel_step = @sel_step_candidate.sel_process_step.sel_step
    #@candidate = @sel_step_candidate.jm_candidate

    if @prev_sel_step_candidate
      @answers_recorded = @prev_sel_step_candidate.sel_step_results
    else
      @answers_recorded = SelStepResult.where(:sel_step_candidate_id => params[:sel_step_candidate_id])
    end


    #@characteristics_to_show = JmCharacteristic.joins(:sel_templates).where(sel_templates: {:id => @sel_step.sel_template_id})
  end

  def set_results

    from_process = 1 if params[:from_process]

    sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])
    sel_step_candidate.copied_from_id = params[:prev_sel_step_candidate_id]
    sel_step_candidate.final_appointment_date = params[:sel_step_candidate][:final_appointment_date]
    sel_step_candidate.save
    sel_step_candidate.assign_attributes(params[:sel_step_candidate])

    sel_step_candidate.sel_evaluation_attachments.each do |attachment|
      if attachment.new_record? && attachment.original_filename

        form_file = attachment.original_filename
        attachment.sel_step_candidate_id = sel_step_candidate.id
        attachment.crypted_name = SecureRandom.hex(5).to_s
        attachment.registered_at = lms_time
        attachment.registered_by_user_id = user_connected.id
        attachment.original_filename = form_file.original_filename

        if attachment.save
          save_file_to_step_candidate(form_file, attachment)
        end
      end
    end

    if params[:prev_sel_evaluation_attachment]
      params[:prev_sel_evaluation_attachment].each_key do |attachment_id|
        prev_attach = SelEvaluationAttachment.find(attachment_id)
        new_attach = prev_attach.dup
        new_attach.copied_from = prev_attach
        new_attach.sel_step_candidate_id = sel_step_candidate.id
        new_attach.crypted_name = SecureRandom.hex(5).to_s
        new_attach.registered_at = lms_time
        new_attach.registered_by_user_id = user_connected.id

        if new_attach.save
          copy_file_to_step_candidate(new_attach, prev_attach)
        end
      end
    end

    if params[:char] && params[:char].count() > 0
      chars = params[:char]

      chars.each_key do |char_id|
        sel_step_result = SelStepResult.where(:sel_step_candidate_id => sel_step_candidate.id,
                                              :sel_step_characteristic_id => SelStepCharacteristic.where(:sel_step_id => sel_step_candidate.sel_process_step.sel_step_id, :sel_characteristic_id => char_id).first.id).
            where('sel_process_characteristic_id IS NULL').first_or_initialize
        sel_step_result.registered_by_user_id = user_connected.id
        sel_step_result.registered_at = lms_time

        if chars[char_id][:option]
          if !chars[char_id][:nonDiscreteValue].blank?
            sel_step_result.non_discrete_result = chars[char_id][:nonDiscreteValue]
          end

          if !chars[char_id][:comment].blank?
            sel_step_result.comment = chars[char_id][:comment]
          end
          sel_step_result.sel_option_id = chars[char_id][:option]
          sel_step_result.save
        end

      end
    end

    # características configurables
    if params[:process_char] && params[:process_char].count() > 0
      process_chars = params[:process_char]


      process_chars.each_key do |process_char_id|
        sel_step_characteristic = SelStepCharacteristic.where(:sel_step_id => sel_step_candidate.sel_process_step.sel_step_id,
                                                              :sel_characteristic_id => SelProcessCharacteristic.find(process_char_id).sel_characteristic_id).first

        sel_step_result = SelStepResult.where(:sel_step_candidate_id => sel_step_candidate.id,
                                              :sel_step_characteristic_id => sel_step_characteristic.id,
                                              :sel_process_characteristic_id => process_char_id).first_or_initialize
        sel_step_result.registered_by_user_id = user_connected.id
        sel_step_result.registered_at = lms_time

        if process_chars[process_char_id][:option]
          if !process_chars[process_char_id][:nonDiscreteValue].blank?
            sel_step_result.non_discrete_result = process_chars[process_char_id][:nonDiscreteValue]
          end

          #if !process_chars[process_char_id][:comment].blank?
          sel_step_result.comment = process_chars[process_char_id][:comment].blank? ? nil : process_chars[process_char_id][:comment]
          #end
          sel_step_result.sel_option_id = process_chars[process_char_id][:option]
          sel_step_result.save
        end

      end
    end


    if !has_pending_answers?(sel_step_candidate.id) && params[:done_validation]
      if sel_step_candidate.update_attributes(:done => true, :done_at => lms_time, :done_by_user_id => user_connected.id)
        flash[:success] = 'Los resultados fueron grabados correctamente'
        if assigned_steps(sel_step_candidate.sel_process_step.sel_process_id, user_connected.id).count == 0
          redirect_to root_path
        else
          if from_process
            redirect_to sel_process_show_open_step_path(sel_step_candidate.sel_process_step.sel_process, sel_step_candidate.sel_process_step)
          else
            redirect_to sel_list_pending_results_path(sel_step_candidate.sel_process_step.sel_process)
          end

        end
      else
        flash[:danger] = 'Los resultados no fueron grabados correctamente'
      end

    else
      flash[:danger] = 'Aún faltan preguntas por resolver y/o validar que se completó el proceso'
      if from_process
        redirect_to sel_input_results_from_process_path(sel_step_candidate, 1)
      else
        redirect_to sel_input_results_path sel_step_candidate
      end

    end

  end

  def download_file
    #sel_applicant_value = SelApplicantValue.find_by_crypted_name(params[:crypted_name])
    jm_answer_value = JmAnswerValue.where(:id => params[:jm_answer_value_id], :value_file_crypted_name => params[:crypted_name]).first
   company = @company
    #/storage/<%= Company.first.codigo %>/sel_processes/<%= sel_applicant.sel_process_id.to_s %>/<%= sel_applicant.jm_candidate_id %>/<%= register.id.to_s + '_' + register.crypted_name + File.extname(register.value) %>
    directory = company.directorio + '/' + company.codigo + '/jm_candidates/' + jm_answer_value.jm_candidate_answer.jm_candidate_id.to_s
    name = jm_answer_value.value_file_crypted_name + File.extname(jm_answer_value.value_file)
    full_path = File.join(directory, name)

    send_file full_path,
              filename: (jm_answer_value.jm_subcharacteristic.jm_characteristic.name + '_' + jm_answer_value.jm_candidate_answer.jm_candidate.surname)[0...255] + File.extname(jm_answer_value.value_file),
              type: 'application/octet-stream',
              disposition: 'attachment'
  end


  def show_attach_file_to_step_candidate

    @sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])
    @sel_evaluation_attachment = SelEvaluationAttachment.new()
    @sel_evaluation_attachment.sel_step_candidate = @sel_step_candidate

  end

  def attach_file_to_step_candidate
    @sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])

    @sel_evaluation_attachment = SelEvaluationAttachment.new(params[:sel_evaluation_attachment])
    @sel_evaluation_attachment.sel_step_candidate_id = @sel_step_candidate.id
    @sel_evaluation_attachment.crypted_name = SecureRandom.hex(5).to_s
    @sel_evaluation_attachment.registered_at = lms_time
    @sel_evaluation_attachment.registered_by_user_id = user_connected.id
    @sel_evaluation_attachment.original_filename = params[:file].original_filename

    if @sel_evaluation_attachment.save
      save_file_to_step_candidate(params[:file], @sel_evaluation_attachment)
      flash[:success] = 'Se adjunto el archivo correctamente'
      redirect_to sel_results_show_attach_file_to_step_candidate_path(@sel_step_candidate)
    else

      render 'show_attach_file_to_step_candidate'

    end


  end

  def download_attach_file_from_step_candidate

    @sel_evaluation_attachment = SelEvaluationAttachment.find_by_complex_crypted_name(params[:attachment_crypted_name])

   company = @company
    #/storage/<%= @company.codigo %>/sel_processes/<%= @sel_process.id.to_s %>/ficha/<%= @sel_process.file
    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + @sel_evaluation_attachment.sel_step_candidate.sel_process_step.sel_process_id.to_s + '/' + @sel_evaluation_attachment.sel_step_candidate.jm_candidate_id.to_s + '/attachments/' + @sel_evaluation_attachment.sel_step_candidate_id.to_s + '/'
    name = @sel_evaluation_attachment._crypted_name + File.extname(@sel_evaluation_attachment.original_filename)
    full_path = File.join(directory, name)

    send_file full_path,
              filename: (@sel_evaluation_attachment.description + '_' + @sel_evaluation_attachment.sel_step_candidate.jm_candidate.surname)[0...255] + File.extname(@sel_evaluation_attachment.original_filename),
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def delete_attach_file_from_step_candidate
    sel_evaluation_attachment = SelEvaluationAttachment.find_by_complex_crypted_name(params[:attachment_crypted_name])
    sel_evaluation_attachment.destroy
    render :json => true
  end


  private

  def can_download_file

  end

  def pending_steps(process_id, user_id)

    pending_steps_results(process_id, user_id)

=begin
    SelProcessStep.joins(:sel_process, [:sel_step_candidates => :sel_attendants])
        .where(sel_processes: {:finished_at => nil, :id => process_id},
               sel_step_candidates: {:done => false, :exonerated => false},
               sel_attendants: {:user_id => user_id}).
        select('DISTINCT sel_process_steps.*')
=end

  end

  def assigned_steps(process_id, user_id)
    #SelProcess.joins(:sel_process_steps => [:sel_step_candidates =>:sel_attendants])
    #   .where(sel_processes: {:finished_at => nil}, sel_step_candidates: {:done => false, :exonerated => false}, sel_attendants: {:user_id => user_id}).select('DISTINCT sel_processes.*')

    SelProcessStep.joins(:sel_process, [:sel_step_candidates => [:sel_attendants, :jm_candidate => :sel_applicants]])
        .where(sel_processes: {:finished => false, :id => process_id},
               #sel_step_candidates: {:done => false, :exonerated => false},
               sel_applicants: {:excluded => false, :sel_process_id => process_id},
               sel_attendants: {:user_id => user_id}).
        select('DISTINCT sel_process_steps.*')

    #SelStepCandidate.joins([:sel_attendants, {sel_process_step: :sel_process}]).where(:done => false, :exonerated => false, sel_processes: {:finished_at => nil}, sel_attendants: {:user_id => user_id}).count > 0 ? true : false
  end

  def has_access_to_process
    if assigned_steps(params[:sel_process_id], user_connected.id).count == 0

      flash[:danger] = 'Usted no tiene nada pendiente en este proceso'
      redirect_to root_path
    end
  end

  def has_access_set_results

    if SelProcessStep.joins(:sel_process, [:sel_step_candidates => :sel_attendants])
           .where(sel_processes: {:finished => false},
                  #sel_step_candidates: {:id => params[:sel_step_candidate_id], :done => false, :exonerated => false},
                  sel_step_candidates: {:id => params[:sel_step_candidate_id]},
                  sel_attendants: {:user_id => user_connected.id}).
        select('DISTINCT sel_step_candidates.*').count == 0

      flash[:danger] = 'Usted no tiene nada pendiente con este candidato'
      redirect_to root_path
    end

  end

  def has_pending_answers?(sel_step_candidate_id)
    sel_step_candidate = SelStepCandidate.find(sel_step_candidate_id)
    number_answered = SelStepResult.where(:sel_step_candidate_id => sel_step_candidate_id).count

    number_fixed_characteristics = sel_step_candidate.sel_process_step.sel_step.sel_characteristics.where(:configurable => false).count

    number_configurable_characteristics = sel_step_candidate.sel_process_step.sel_process_characteristics.count

    (number_fixed_characteristics + number_configurable_characteristics == number_answered) ? false : true

  end

  def save_file_to_step_candidate(file, sel_step_candidate_attachment)

   company = @company
    file_crypted_name = sel_step_candidate_attachment._crypted_name + File.extname(file.original_filename)
    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + sel_step_candidate_attachment.sel_step_candidate.sel_process_step.sel_process_id.to_s + '/' + sel_step_candidate_attachment.sel_step_candidate.jm_candidate_id.to_s + '/attachments/' + sel_step_candidate_attachment.sel_step_candidate_id.to_s + '/'

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') {|f| f.write(file.read)}


  end

  def copy_file_to_step_candidate(copy_attach_to, copy_attach_from)
   company = @company

    directory_from = company.directorio + '/' + company.codigo + '/sel_processes/' + copy_attach_from.sel_step_candidate.sel_process_step.sel_process_id.to_s + '/' + copy_attach_from.sel_step_candidate.jm_candidate_id.to_s + '/attachments/' + copy_attach_from.sel_step_candidate_id.to_s + '/'
    name_from = copy_attach_from._crypted_name + File.extname(copy_attach_from.original_filename)
    full_path_from = File.join(directory_from, name_from)

    directory_to = company.directorio + '/' + company.codigo + '/sel_processes/' + copy_attach_to.sel_step_candidate.sel_process_step.sel_process_id.to_s + '/' + copy_attach_to.sel_step_candidate.jm_candidate_id.to_s + '/attachments/' + copy_attach_to.sel_step_candidate_id.to_s + '/'
    name_to = copy_attach_to._crypted_name + File.extname(copy_attach_to.original_filename)
    full_path_to = File.join(directory_to, name_to)

    FileUtils.mkdir_p directory_to unless File.directory? directory_to
    FileUtils.cp full_path_from, full_path_to if File.file?(full_path_from)
  end

end
