class JmMyProfilesController < ApplicationController

  before_filter :module_active
  before_filter :candidate_authenticate
  before_filter :not_internal_user
  before_filter :set_layout

  # GET /jm_my_profiles/1
  # GET /jm_my_profiles/1.json
  def show
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
    @jm_profile_form = @jm_candidate.jm_profile_form
    @layout = @jm_profile_form.layout

    if params[:section_id]
      @selected_section = JmProfileSection.where(:id => params[:section_id]).joins(:jm_profile_chars => :jm_characteristic).where(:jm_characteristics => {:manager_only => false}).first
    end
  end

  def edit
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
    @jm_profile_form = @jm_candidate.jm_profile_form
    @layout = @jm_profile_form.layout
    @filling_profile = true

    if params[:section_id]
      @selected_section = JmProfileSection.where(:id => params[:section_id]).joins(:jm_profile_chars => :jm_characteristic).where(:jm_characteristics => {:manager_only => false}).first
    end
  end


  def save_profile
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
    if params[:section_id]
      selected_section = JmProfileSection.where(:id => params[:section_id]).joins(:jm_profile_chars => :jm_characteristic).where(:jm_characteristics => {:manager_only => false}).first
    end
    files_to_save = []

    @jm_candidate.assign_attributes(params[:jm_candidate])
    @jm_candidate.jm_candidate_answers.each_with_index  do |answer|
      if answer.new_record?
        answer.registered_at = lms_time
      end
      answer.jm_answer_values.each do |value|

        if value.new_record? && value.value_file
          value.value_file_crypted_name = SecureRandom.hex(5)
          files_to_save.append(value.dup)
          value.value_file = value.value_file.original_filename
        end
      end
    end

    if @jm_candidate.save

      files_to_save.each do |answer_value|
        file = answer_value.value_file
        save_file(file, @company, answer_value)

      end
      flash[:success] = t('activerecord.success.model.jm_candidate.update_ok')
      if selected_section
        redirect_to jm_my_profile_section_open_url(selected_section)
      else
        redirect_to jm_my_profile_path
      end

    else
      flash.now[:danger] = 'Los datos no se pudieron actualizar correctamente'
      render action: 'edit'
    end

  end



  private

  def candidate_authenticate
    if session[:candidate_connected_id]
      true
    else
      flash[:danger] = 'Debe estar autenticado para ver esta opción'
      redirect_to jm_login_path
    end
  end

  def set_layout
    self.class.layout session[:user_connected_id].nil? ? 'job_market' : 'application'
  end

  def module_active
    if CtModule.where(cod: 'jm', active: true).count == 0
      redirect_to root_path
    end
  end

  def not_internal_user
    candidate = JmCandidate.find(session[:candidate_connected_id])
    if candidate.internal_user?
      redirect_to list_jm_offers_path
    end
  end

  def save_file(file, company, jm_answer_value)
    # sel_processes tiene una copia de este método para poder enrollar desde el proceso
    file_crypted_name = jm_answer_value.value_file_crypted_name + File.extname(file.original_filename)
    candidate_id = session[:candidate_connected_id].to_s
    directory = company.directorio+'/'+company.codigo+'/jm_candidates/' + candidate_id + '/'

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') { |f| f.write(file.read) }


  end

end
