class JmCharacteristicsController < ApplicationController
  before_filter :authenticate_user
  before_filter :authenticate_user_admin
  # GET /jm_characteristics
  # GET /jm_characteristics.json
  def index
    @jm_characteristics = JmCharacteristic.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @jm_characteristics }
    end
  end

  # GET /jm_characteristics/1
  # GET /jm_characteristics/1.json
  def show
    @jm_characteristic = JmCharacteristic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @jm_characteristic }
    end
  end

  # GET /jm_characteristics/new
  # GET /jm_characteristics/new.json
  def new
    @jm_characteristic = JmCharacteristic.new

    2.times do
      @jm_characteristic.jm_subcharacteristics.build
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @jm_characteristic }
    end
  end

  # GET /jm_characteristics/1/edit
  def edit
    @jm_characteristic = JmCharacteristic.find(params[:id])

  end

  # POST /jm_characteristics
  # POST /jm_characteristics.json
  def create
    @jm_characteristic = JmCharacteristic.new(params[:jm_characteristic])

    respond_to do |format|
      if @jm_characteristic.save

=begin
        if @jm_characteristic.simple?
          @jm_characteristic.jm_subcharacteristics.destroy_all
          jm_subchar = JmSubcharacteristic.new()
          jm_subchar.jm_characteristic_id = @jm_characteristic.id
          jm_subchar.position = 1
          jm_subchar.name = @jm_characteristic.name
          jm_subchar.description = @jm_characteristic.description
          jm_subchar.option_type = params[:sub_char_simple][:option_type]
          if params[:sub_char_simple][:option_type] == '3'
            jm_subchar.jm_list_id = params[:sub_char_simple][:list]
          end
          jm_subchar.save
        end
=end

        flash[:success] = 'La característica fue creada correctamente'
        format.html { redirect_to jm_characteristics_path }
        format.json { render json: @jm_characteristic, status: :created, location: @jm_characteristic }
      else
        format.html { render action: "new" }
        format.json { render json: @jm_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /jm_characteristics/1
  # PUT /jm_characteristics/1.json
  def update
    @jm_characteristic = JmCharacteristic.find(params[:id])

    respond_to do |format|
      if @jm_characteristic.update_attributes(params[:jm_characteristic])


=begin
        if @jm_characteristic.simple?
          @jm_characteristic.jm_subcharacteristics.destroy_all
          jm_subchar = JmSubcharacteristic.new()
          jm_subchar.jm_characteristic_id = @jm_characteristic.id
          jm_subchar.position = 1
          jm_subchar.name = @jm_characteristic.name
          jm_subchar.description = @jm_characteristic.description
          jm_subchar.option_type = params[:sub_char_simple][:option_type]
          if params[:sub_char_simple][:option_type] == '3'
            jm_subchar.jm_list_id = params[:sub_char_simple][:list]
          else
            jm_subchar.jm_list_id = nil
          end

          jm_subchar.save
        end
=end

        flash[:success] = 'La característica fue actualizada correctamente'
        format.html { redirect_to jm_characteristics_path }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @jm_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jm_characteristics/1
  # DELETE /jm_characteristics/1.json
  def destroy
    @jm_characteristic = JmCharacteristic.find(params[:id])
    @jm_characteristic.destroy

    respond_to do |format|
      flash[:success] = 'La característica fue eliminada correctamente'
      format.html { redirect_to jm_characteristics_path }
      format.json { head :no_content }
    end
  end


  def toggle_manager_only
    jm_characteristic = JmCharacteristic.find(params[:id])

    jm_characteristic.manager_only = !jm_characteristic.manager_only?

    if jm_characteristic.save
      flash[:success] = 'La característica fue actualizada correctamente'
    else
      flash[:danger] = 'La característica no se actualizó correctamente'
    end

    redirect_to jm_characteristics_path

  end

end
