class TrackingFormStatesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @tracking_form = TrackingForm.find(params[:tracking_form_id])
    @tracking_form_states = @tracking_form.tracking_form_states
    render :layout => false
  end

  # GET /tracking_form_states/1
  # GET /tracking_form_states/1.json
  def show
    @tracking_form_state = TrackingFormState.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tracking_form_state }
    end
  end

  # GET /tracking_form_states/new
  # GET /tracking_form_states/new.json
  def new
    @tracking_form = TrackingForm.find(params[:tracking_form_id])
    @tracking_form_state = @tracking_form.tracking_form_states.build
  end

  # GET /tracking_form_states/1/edit
  def edit
    @tracking_form_state = TrackingFormState.find(params[:id])
  end

  # POST /tracking_form_states
  # POST /tracking_form_states.json
  def create
    @tracking_form_state = TrackingFormState.new(params[:tracking_form_state])

      if @tracking_form_state.save
        flash['success'] = 'Estado creado correctamente'
        redirect_to tracking_form_pill_open_path(@tracking_form_state.tracking_form, 'track_states')
      else
        flash.now['danger'] = 'Estado no pudo ser creado correctamente'
        render action: "new"
      end

  end

  # PUT /tracking_form_states/1
  # PUT /tracking_form_states/1.json
  def update
    @tracking_form_state = TrackingFormState.find(params[:id])
      if @tracking_form_state.update_attributes(params[:tracking_form_state])
        flash['success'] = 'Estado actualizado correctamente'
        redirect_to tracking_form_pill_open_path(@tracking_form_state.tracking_form, 'track_states')
      else
        flash.now['danger'] = 'Estado no pudo ser actualizado correctamente'
        if params[:tracking_form_state][:tracking_state_notifications_attributes]
          render action: "manage_notifications"
        else
          render action: "edit"
        end
      end
  end

  # DELETE /tracking_form_states/1
  # DELETE /tracking_form_states/1.json
  def destroy
    @tracking_form_state = TrackingFormState.find(params[:id])
    @tracking_form_state.destroy

    respond_to do |format|
      format.html { redirect_to tracking_form_states_url }
      format.json { head :no_content }
    end
  end

  def manage_notifications
    @tracking_form_state = TrackingFormState.find(params[:id])
  end
end
