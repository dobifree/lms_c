class UserManagingReportsController < ApplicationController

  include UsersHelper
  include UserManagingReportsHelper

  before_filter :authenticate_user

  before_filter :verify_access_manage_module, only: [ :rep_active_employees_characteristics,
                                                      :rep_active_employees_photos ]



  def rep_active_employees_characteristics
    reporte_excel = active_user_characteristics
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal
    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'characteristics_users_active.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end


  def rep_active_employees_photos
    reporte_excel = photos_user_active
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal
    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'usuarios_fotos.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'
  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end
end