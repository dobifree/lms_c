class DncProvidersController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /dnc_providers
  # GET /dnc_providers.json
  def index
    @dnc_providers = DncProvider.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dnc_providers }
    end
  end

  # GET /dnc_providers/1
  # GET /dnc_providers/1.json
  def show
    @dnc_provider = DncProvider.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dnc_provider }
    end
  end

  # GET /dnc_providers/new
  # GET /dnc_providers/new.json
  def new
    @dnc_provider = DncProvider.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dnc_provider }
    end
  end

  # GET /dnc_providers/1/edit
  def edit
    @dnc_provider = DncProvider.find(params[:id])
  end

  # POST /dnc_providers
  # POST /dnc_providers.json
  def create
    @dnc_provider = DncProvider.new(params[:dnc_provider])

    respond_to do |format|
      if @dnc_provider.save
        format.html { redirect_to @dnc_provider, notice: 'Dnc provider was successfully created.' }
        format.json { render json: @dnc_provider, status: :created, location: @dnc_provider }
      else
        format.html { render action: "new" }
        format.json { render json: @dnc_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dnc_providers/1
  # PUT /dnc_providers/1.json
  def update
    @dnc_provider = DncProvider.find(params[:id])

    respond_to do |format|
      if @dnc_provider.update_attributes(params[:dnc_provider])
        format.html { redirect_to @dnc_provider, notice: 'Dnc provider was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dnc_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dnc_providers/1
  # DELETE /dnc_providers/1.json
  def destroy
    @dnc_provider = DncProvider.find(params[:id])
    @dnc_provider.destroy

    respond_to do |format|
      format.html { redirect_to dnc_providers_url }
      format.json { head :no_content }
    end
  end
end
