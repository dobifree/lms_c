class PeValidationController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list, :massive_form, :massive_download_xls, :massive_validation]
  before_filter :get_data_2, only: [:show, :validate]

  before_filter :validate_open_process, only: [:people_list, :show, :validate, :massive_form, :massive_download_xls, :massive_validation]
  before_filter :validate_is_the_validator, only: [:show, :validate]


  def people_list



  end

  def show

  end

  def validate

    if params[:dont_validate] && params[:dont_validate] == '1'

      @pe_member_evaluated.step_assessment = false
      @pe_member_evaluated.step_validation = false
      @pe_member_evaluated.step_validation_date = lms_time
      @pe_member_evaluated.save

      @pe_member_rel.finished = false
      @pe_member_rel.validation_comment = params[:pe_member_rel][:validation_comment]
      @pe_member_rel.validated_at = lms_time
      @pe_member_rel.save

      if @pe_process.step_validation_send_email_when_reject?

        #begin
        #  LmsMailer.pe_step_validation_send_when_reject(@pe_member_rel.pe_member_evaluator.user, @pe_member_rel.pe_member_evaluated.user, @pe_process, user_connected, @company).deliver
        #rescue
        #end

        menu_name = 'Evaluación de desempeño'

        ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

          if ct_menu_cod == 'pe'
            menu_name = ct_menus_user_menu_alias[index_m]
            break
          end
        end

        begin
          PeMailer.step_validation_rejected(@pe_process, @company, menu_name, @pe_member_rel.pe_member_evaluated.user, @pe_member_rel.pe_member_evaluator.user, user_connected).deliver
        rescue Exception => e
          lm = LogMailer.new
          lm.module = 'pe'
          lm.step = 'def_by_user:validation:reject'
          lm.description = e.message+' '+e.backtrace.inspect
          lm.registered_at = lms_time
          lm.save
        end


      end

    else

      @pe_member_rel.validated = true
      @pe_member_rel.validated_at = lms_time
      @pe_member_rel.save

      step_validation = true

      @pe_member_evaluated.pe_member_rels_is_evaluated.where('validator_id IS NOT NULL').each do |pe_member_rel_is_evaluated|

        step_validation = false unless pe_member_rel_is_evaluated.validated

      end

      if step_validation

        @pe_member_evaluated.step_validation = true
        @pe_member_evaluated.step_validation_date = lms_time
        @pe_member_evaluated.save

      end

      if @pe_process.step_validation_send_email_when_accept?

        #begin
        #  LmsMailer.pe_step_validation_send_when_accept(@pe_member_rel.pe_member_evaluator.user, @pe_member_rel.pe_member_evaluated.user, @pe_process, user_connected, @company).deliver
        #rescue
        #end

        menu_name = 'Evaluación de desempeño'

        ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

          if ct_menu_cod == 'pe'
            menu_name = ct_menus_user_menu_alias[index_m]
            break
          end
        end

        begin
          PeMailer.step_validation_accepted(@pe_process, @company, menu_name, @pe_member_rel.pe_member_evaluated.user, @pe_member_rel.pe_member_evaluator.user, user_connected).deliver
        rescue Exception => e
          lm = LogMailer.new
          lm.module = 'pe'
          lm.step = 'def_by_user:validation:accept'
          lm.description = e.message+' '+e.backtrace.inspect
          lm.registered_at = lms_time
          lm.save
        end


      end

    end

    flash[:success] = 'La validación fue registrada correctamente'

    redirect_to pe_validation_show_path @pe_member_rel

  end

  def massive_form



  end

  def massive_download_xls

    filename = ('Validar ' + @pe_process.name).gsub(/\s/,'_') + '.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def massive_validation

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:xls_file_results]

      if File.extname(params[:xls_file_results].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:xls_file_results].read) }

        #check_xls_file_results archivo_temporal

        if @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.pe_managing.update_file_error')
          render 'massive_validation'
        else

          save_xls_validations archivo_temporal

          flash[:success] = 'La validación fue realizada correctamente'

          redirect_to pe_validation_people_list_path @pe_process

        end

      else

        flash.now[:danger] = t('activerecord.error.model.pe_managing.upload_file_no_xlsx')
        render 'massive_form'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.pe_managing.upload_file_no_file')
      render 'massive_form'

    end

  end

  private

  def get_data_1
    @pe_process = PeProcess.find(params[:pe_process_id])
  end

  def get_data_2

    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

   #@company = session[:company]

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_is_the_validator

    unless @pe_member_rel.validator_id == user_connected.id
      flash[:danger] = t('activerecord.error.model.pe_process_validation.is_not_the_validator')
      redirect_to root_path
    end

  end

  def save_xls_validations(archivo_temporal)

    pos_val = 2
    pos_val += @pe_process.pe_characteristics_gui_validation.size
    pos_evaluator = pos_val

    pos_val += 3

    @pe_process.pe_evaluations.each do |pe_evaluation|

      pos_val += 1 unless pe_evaluation.informative

    end

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]
        pe_member_evaluator = @pe_process.pe_member_by_user_code fila[pos_evaluator]

        if pe_member_evaluated && pe_member_evaluator

          pe_member_rel = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

          if pe_member_rel

            if pe_member_rel.validator_id == user_connected.id

              if pe_member_rel.finished && !pe_member_rel.validated

                if fila[pos_val] == 'NO'

                  pe_member_evaluated.step_assessment = false
                  pe_member_evaluated.step_validation = false
                  pe_member_evaluated.step_validation_date = lms_time
                  pe_member_evaluated.save

                  pe_member_rel.finished = false
                  pe_member_rel.validation_comment = fila[pos_val+1]
                  pe_member_rel.validated_at = lms_time
                  pe_member_rel.save

                  if @pe_process.step_validation_send_email_when_reject?

                    menu_name = 'Evaluación de desempeño'

                    ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

                      if ct_menu_cod == 'pe'
                        menu_name = ct_menus_user_menu_alias[index_m]
                        break
                      end
                    end

                    begin
                      PeMailer.step_validation_rejected(@pe_process, @company, menu_name, pe_member_rel.pe_member_evaluated.user, pe_member_rel.pe_member_evaluator.user, user_connected).deliver
                    rescue Exception => e
                      lm = LogMailer.new
                      lm.module = 'pe'
                      lm.step = 'def_by_user:validation:reject'
                      lm.description = e.message+' '+e.backtrace.inspect
                      lm.registered_at = lms_time
                      lm.save
                    end


                  end

                elsif fila[pos_val] == 'SI'

                  pe_member_rel.validated = true
                  pe_member_rel.validated_at = lms_time
                  pe_member_rel.save

                  step_validation = true

                  pe_member_evaluated.pe_member_rels_is_evaluated.where('validator_id IS NOT NULL').each do |pe_member_rel_is_evaluated|

                    step_validation = false unless pe_member_rel_is_evaluated.validated

                  end

                  if step_validation

                    pe_member_evaluated.step_validation = true
                    pe_member_evaluated.step_validation_date = lms_time
                    pe_member_evaluated.save

                  end

                  if @pe_process.step_validation_send_email_when_accept?

                    menu_name = 'Evaluación de desempeño'

                    ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

                      if ct_menu_cod == 'pe'
                        menu_name = ct_menus_user_menu_alias[index_m]
                        break
                      end
                    end

                    begin
                      PeMailer.step_validation_accepted(@pe_process, @company, menu_name, pe_member_rel.pe_member_evaluated.user, pe_member_rel.pe_member_evaluator.user, user_connected).deliver
                    rescue Exception => e
                      lm = LogMailer.new
                      lm.module = 'pe'
                      lm.step = 'def_by_user:validation:accept'
                      lm.description = e.message+' '+e.backtrace.inspect
                      lm.registered_at = lms_time
                      lm.save
                    end


                  end

                end

              end

            end

          end

        end

      end

    end

  end

end
