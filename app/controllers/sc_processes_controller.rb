class ScProcessesController < ApplicationController
  before_filter :get_gui_characteristics, only: [:show_records_list, :sc_process_he_report]

  before_filter :authenticate_user
  before_filter :verify_access_manage_module, only: [:new_annually,
                                                     :create_annually,
                                                     :new,
                                                     :edit,
                                                     :create,
                                                     :update,
                                                     :sc_process_he_report]

  before_filter :verify_access_recorder_validator_module_manager, only: [:show]
  before_filter :verify_only_recorder_validator, only: [:index]

  def index
    @sc_processes = ScProcess.order('year DESC, month DESC').all
    @recorder = false
    @validator = false
    user_privilage = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true).first
    @recorder = true if user_privilage && user_privilage.recorder
    @validator = true if (user_privilage && user_privilage.validator) || ct_module_sc_he_manager?
    respond_to do |format|
      format.html # index.html.erb
      format.json {render json: @sc_processes}
    end
  end

  def show
    @sc_process = ScProcess.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json {render json: @sc_process}
    end
  end

  def new_annually
    @sc_process = ScProcess.new

    respond_to do |format|
      format.html # new.html.erb
      format.json {render json: @sc_process}
    end
  end

  def create_annually
    year = params[:sc_process][:year].to_i
    deadline_day = params[:sc_process][:month].to_i
    since_day = params[:records_since_day].to_i
    until_day = params[:records_until_day].to_i

    case params[:records_since_month]
      when 'Anterior'
        month_since = -1
      when 'Actual'
        month_since = 0
      else
        month_since = +1
    end

    case params[:records_until_month]
      when 'Anterior'
        month_until = -1
      when 'Actual'
        month_until = 0
      else
        month_until = +1
    end

    12.times do |month|
      deadline = DateTime.new(year, month + 1, -1)
      deadline = DateTime.new(year, month + 1, deadline_day) if deadline_day < deadline.strftime("%d").to_i

      if month + 1 + month_since > 12
        month_record_since = 1
        year_record_since = year + 1
      else
        month_record_since = month + 1 + month_since
        year_record_since = year
      end

      if month + 1 + month_until > 12
        month_record_until = 1
        year_record_until = year + 1
      else
        month_record_until = month + 1 + month_until
        year_record_until = year
      end

      records_since = DateTime.new(year_record_since, month_record_since, -1)
      records_since = DateTime.new(year_record_since, month_record_since, since_day) if since_day < records_since.strftime("%d").to_i

      records_until = DateTime.new(year_record_until, month_record_until, -1)
      records_until = DateTime.new(year_record_until, month_record_until, until_day) if until_day < records_until.strftime("%d").to_i

      process = ScProcess.where(:year => year, :month => month + 1).first
      records_until = records_until.change({ hour: 23, min: 59, sec: 59 })
      ScProcess.create(year: year, month: month + 1, deadline: deadline, records_since: records_since, records_until: records_until) unless process
    end
    flash[:success] = t('views.sc_processes.flash_messages.changes_success')
    redirect_to he_ct_module_privilages_index_path(0)
  end

  def new
    @sc_process = ScProcess.new

    respond_to do |format|
      format.html # new.html.erb
      format.json {render json: @sc_process}
    end
  end

  def edit
    @sc_process = ScProcess.find(params[:sc_process_id])
  end

  def create
    @sc_process = ScProcess.new(params[:sc_process])
    @sc_process.records_until = @sc_process.records_until.change({ hour: 23, min: 59, sec: 59 })

    respond_to do |format|
      if @sc_process.save
        @sc_processes = ScProcess.all
        format.html {redirect_to he_ct_module_privilages_index_path(0)}
        format.json {render json: @sc_process, status: :created, location: @sc_process}
      else
        format.html {render action: "new"}
        format.json {render json: @sc_process.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    @sc_process = ScProcess.find(params[:sc_process_id])

    respond_to do |format|
      if @sc_process.update_attributes(params[:sc_process])
        @sc_process.records_until = @sc_process.records_until.change({ hour: 23, min: 59, sec: 59 })
        @sc_process.save
        flash[:success]=t('views.sc_processes.flash_messages.changes_success')
        format.html {redirect_to close_process_path(@sc_process.id, 0)}
        format.json {head :no_content}
      else
        format.html {render action: "edit"}
        format.json {render json: @sc_process.errors, status: :unprocessable_entity}
      end
    end
  end

  def sc_process_he_report
    process = ScProcess.find(params[:sc_process_id])
    report = ScReporting.where(sc_process_id: process.id, active: true).first
    if report
      report.active = false
      report.deactivated_by_user_id = user_connected.id
      report.deactivated_at = lms_time
      report.save
    end
    report = ScReporting.create(reported_at: lms_time, reported_by_user_id: user_connected.id, sc_process_id: process.id)
    @records = process.sc_records.where(status: 3, dont_charge_to_payroll: false)
    @users_id = []
    @records.each do |record|
      @users_id << record.user_id
      ScReportedRecord.create(sc_reporting_id: report.id, sc_record_id: record.id)
      record.sent_to_payroll = true
      record.save
    end
    @users_id = @users_id.uniq

    respond_to do |format|
      format.xlsx {render xlsx: 'sc_process_he_report_partial.xlsx', filename: 'Reporte completo de horas extras.xlsx'}
    end
  end

  def validation_report
    @sc_process = ScProcess.find(params[:sc_process_id])
    file_name = @sc_process.year.to_s+'-'+@sc_process.month.to_s+'-ValidacionDeProceso'
    respond_to do |format|
      format.xlsx {render xlsx: 'sc_process_validation_info.xlsx', filename: file_name}
    end
  end

  def sc_process_he_report_partial
    process = ScProcess.find(params[:sc_process_id])
    @users_id = []
    @records = process.sc_records.where(status: 3,
                                        dont_charge_to_payroll: false,
                                        sent_to_payroll: false).all

    report = ScReporting.where(active: true,
                               sc_process_id: process.id,
                               partial_report: true).first
    if report
      report.active = false
      report.deactivated_by_user_id = user_connected.id
      report.deactivated_at = lms_time
      report.save
    end

    if (!@records || (@records && !(@records.size > 0))) && !report.nil?
      @records = []
      reported_records = report.sc_reported_records
      reported_records.each { |rr| @records << rr.sc_record}
    end

    report = ScReporting.create(reported_at: lms_time,
                                reported_by_user_id: user_connected.id,
                                sc_process_id: process.id,
                                partial_report: true)
    @records.each do |record|
      @users_id << record.user_id
      ScReportedRecord.create(sc_reporting_id: report.id, sc_record_id: record.id)
      record.sent_to_payroll = true
      record.save
    end
    @users_id = @users_id.uniq
    respond_to do |format|
      format.xlsx {render xlsx: 'sc_process_he_report_partial.xlsx', filename: 'Reporte parcial de horas extras.xlsx'}
    end
  end

  private

  def get_gui_characteristics
    @sc_schedule_characteristics = ScScheduleCharacteristic.gui
  end

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_recorder_validator_module_manager
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first
    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    validator = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :validator => true).first

    if ct_module && ((ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1) || recorder || validator)
    else
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def verify_only_recorder_validator
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first
    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    validator = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :validator => true).first

    if ct_module && (recorder || validator || ct_module_sc_he_manager?)
    else
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end
end

# def destroy
#   @sc_process = ScProcess.find(params[:id])
#   @sc_process.destroy
#
#   respond_to do |format|
#     format.html { redirect_to sc_processes_url }
#     format.json { head :no_content }
#   end
# end

# def show_records_list
#   @sc_process = ScProcess.find(params[:sc_process_id])
#
#   render partial: 'sc_processes_registers/attendances_list', :locals => {:process => @sc_process,
#                                                                          :editable => params[:editable],
#                                                                          :characteristics => @sc_schedule_characteristics }
# end

# def new_record
#   render partial: 'record_pane',
#          :locals => {:process => ScProcess.find(params[:sc_process_id]),
#                      :record => ScRecord.new(),
#                      :users => User.where(:activo => true).map{|user| [user.codigo + ' - ' + user.comma_full_name, user.id]}}
# end
#
# def create_record
#   new_record = ScRecord.create(params[:sc_record])
#   new_record.user_id = params[:user_id]
#   new_record.registered_by_user = user_connected
#   new_record.registered_at = lms_time
#   new_record.sc_process_id = params[:sc_process_id]
#   new_record.save
#   new_record.search_match_user_process
#   new_record.calculate_he
#
#   render :nothing => true
# end
#
# def records_to_validation
#   # status: 0:Pendiente de enviar validación, 1:Pendiente de ser validado. 2:En discusión. 3:Validado. 4:Rechazado
#   records = ScRecord.where(:sc_process_id => params[:sc_process_id], :status => 0 )
#   users_id = []
#   records.each do |record|
#     users_id << record.user_id
#   end
#
#   users_id = users_id.uniq
#
#   users_id.each do |user_id|
#     sc_user_process = ScUserProcess.where(:user_id => user_id, :sc_process_id => params[:sc_process_id] ).first_or_create
#     records.each_with_index do |record, records_index|
#       if record.user_id == user_id
#         record.sc_user_process_id = sc_user_process.id
#         record.status = 1
#         record.save
#         #enlazar con bloques: Preguntar si hay más de uno.
#         #   Si no hay ninguno:mostrar Error en lista 'No se encontró horario o bloque para usuario' Actualizar status:pendiente de enviar a validación
#         #calcular horas extras según normas establecidas
#         #actualizar horas totales en:sc_user_process
#         records.delete_at(records_index)
#       end
#     end
#   end
#   render :nothing => true
# end