class PeFeedbackAcceptController < ApplicationController

  include PeReportingHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:show, :accept, :rep_detailed_final_results_pdf]

  before_filter :validate_open_process, only: [:show, :accept, :rep_detailed_final_results_pdf]
  before_filter :validate_has_to_accept_feedback, only: [:show, :accept, :rep_detailed_final_results_pdf]
  before_filter :validate_rep_detailed_final_results_pdf, only: [:rep_detailed_final_results_pdf]


  def show

  end

  def accept

    @feedback_error = false

    @pe_member.pe_member_accepted_feedbacks.destroy_all

    @pe_process.pe_feedback_accepted_fields.each do |pe_feedback_accepted_field|

      pe_member_accepted_feedback = @pe_member.pe_member_accepted_feedbacks.build
      pe_member_accepted_feedback.pe_process = @pe_process
      pe_member_accepted_feedback.pe_feedback_accepted_field = pe_feedback_accepted_field

      if pe_feedback_accepted_field.simple

        pe_member_accepted_feedback.comment = params['feedback_accepted_field_'+pe_feedback_accepted_field.id.to_s]

        pe_member_accepted_feedback.list_item_id = params['feedback_accepted_field_'+pe_feedback_accepted_field.id.to_s] if pe_feedback_accepted_field.field_type == 2

        @feedback_error = true unless pe_member_accepted_feedback.save

      else

      end

    end

    if @feedback_error
      flash.now[:success] = t('activerecord.success.model.pe_process_feedback_accept.partial_create_feedback_ok')
      flash.now[:danger] = t('activerecord.error.model.pe_process_feedback_accepted.incomplete_feedback')
      render 'show'
    else

      @pe_member.step_feedback_accepted = true
      @pe_member.step_feedback_accepted_date = lms_time

      if @pe_member.save

        if @pe_process.step_feedback_accepted_mail_to_boss

          menu_name = 'Evaluación de desempeño'

          ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

            if ct_menu_cod == 'pe'
              menu_name = ct_menus_user_menu_alias[index_m]
              break
            end
          end

          begin
            PeMailer.step_feedback_accept_to_feedback_provider(@pe_process, @company, menu_name, @pe_member.user, @pe_member.feedback_provider).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'pe'
            lm.step = 'def_by_user:feedback:to_feedback_provider'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

      end

      flash[:success] = t('activerecord.success.model.pe_process_feedback_accept.confirm_ok')

      redirect_to pe_feedback_accept_show_path @pe_process

    end

  end

  def rep_detailed_final_results_pdf

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, true, false, @pe_process.public_pe_box, @pe_process.public_pe_dimension_name, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private


  def get_data_1
    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_member = @pe_process.evaluated_members.where('user_id = ?', user_connected.id).first
   #@company = session[:company]
  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_has_to_accept_feedback

    unless @pe_process.has_step_feedback_accepted && @pe_member && @pe_member.step_feedback
      flash[:danger] = t('activerecord.error.model.pe_process_feedback_accepted.cant_accept_feedback')
      redirect_to root_path
    end

  end

  def validate_rep_detailed_final_results_pdf
    unless @pe_process.step_feedback_accepted_show_rep_dfr_pdf?
      flash[:danger] = t('activerecord.error.model.pe_process_feedback_accepted.cant_accept_feedback')
      redirect_to root_path
    end
  end

end
