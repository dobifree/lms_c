class PeAssignController < ApplicationController

  include UsersHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list]
  before_filter :get_data_2, only: [:show, :add, :finish]
  before_filter :get_data_3, only: [:remove]
  before_filter :validate_open_process
  before_filter :validate_access_to_assign
  before_filter :validate_rights_to_manage, only: [:add, :remove]

  def people_list


  end

  def show

    @user = User.new

    if params[:user]
      @users = search_active_employees(params[:user][:apellidos], params[:user][:nombre])
      @user.apellidos = params[:user][:apellidos]
      @user.nombre = params[:user][:nombre]
    end

  end

  def add

    user = User.find(params[:user_id])
    pe_member_evaluator = @pe_process.pe_member_by_user user
    unless pe_member_evaluator
      pe_member_evaluator = @pe_process.pe_members.build
      pe_member_evaluator.user = user
    end

    if pe_member_evaluator.save

      if @pe_process.pe_rels_available_to_assign.size == 1

        pe_rel = @pe_process.pe_rels_available_to_assign.first

      else

        pe_rel = PeRel.find(params[:pe_rel][:id]) unless params[:pe_rel][:id].blank?

      end

      if @pe_process.pe_rels_available_to_assign.include? pe_rel

        pe_member_rel = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

        unless pe_member_rel
          pe_member_rel = @pe_member_evaluated.pe_member_rels_is_evaluated.build
          pe_member_rel.pe_member_evaluator = pe_member_evaluator
          pe_member_rel.pe_process = @pe_process
        end

        pe_member_rel.pe_rel = pe_rel
        pe_member_rel.weight = 1
        pe_member_rel.assigned_by_boss = true
        pe_member_rel.save


        pe_member_evaluator.is_evaluator = true
        pe_member_evaluator.save

        flash[:success] = t('activerecord.success.model.pe_assign.add_ok')
        redirect_to pe_assign_show_path @pe_member_evaluated

      else
        @user = User.new

        render 'show'

      end
    else
      redirect_to pe_assign_show_path @pe_member_evaluated
    end

  end

  def remove
    @pe_member_rel.destroy
    flash[:success] = t('activerecord.success.model.pe_assign.remove_ok')
    redirect_to pe_assign_show_path @pe_member_evaluated
  end

  def finish
    @pe_member_evaluated.step_assign = true
    @pe_member_evaluated.step_assign_date = lms_time
    @pe_member_evaluated.save
    flash[:success] = t('activerecord.success.model.pe_assign.finish_ok')
    redirect_to pe_assign_people_list_path @pe_process
  end

  private

  def get_data_1

    @pe_process = PeProcess.find params[:pe_process_id]

    @pe_member_connected = @pe_process.pe_member_by_user user_connected

    @pe_rel_boss = @pe_process.pe_rel_boss

  end

  def get_data_2
    @pe_member_evaluated = PeMember.find params[:pe_member_id]
    @pe_process = @pe_member_evaluated.pe_process
    @pe_member_connected = @pe_process.pe_member_by_user user_connected
    @pe_rel_boss = @pe_process.pe_rel_boss
  end

  def get_data_3
    @pe_member_rel = PeMemberRel.find params[:pe_member_rel_id]
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_process = @pe_member_evaluated.pe_process
    @pe_member_connected = @pe_process.pe_member_by_user user_connected
    @pe_rel_boss = @pe_process.pe_rel_boss
  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_access_to_assign

    unless @pe_process.has_step_assign? && @pe_process.is_a_pe_member_evaluator_as_boss?(user_connected)
      flash[:danger] = t('activerecord.error.model.pe_process_assign.cant_assign')
      redirect_to root_path
    end

  end

  def validate_rights_to_manage

    pmr = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_connected

    unless pmr.pe_rel.rel_id == 1 && @pe_member_evaluated.step_assign == false
      flash[:danger] = t('activerecord.error.model.pe_process_assign.cant_assign')
      redirect_to root_path
    end

  end

end
