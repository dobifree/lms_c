class LicRegistersController < ApplicationController
  include LicRegistersHelper
  include VacationsModule
  include LicensesModule


  before_filter :authenticate_user
  before_filter :verify_access_module
  before_filter :verify_register_or_validator
  before_filter :available_register_to_edit, only: [:request_edit,
                                                    :request_update,
                                                    :request_deactivate]

  before_filter :available_register_to_edit_event, only: %i[ event_manage
                                                             event_edit
                                                             event_update]

  before_filter :active_event_by_request, only: %i[request_edit
                                                   request_update
                                                   request_deactivate]

  before_filter :active_event, only: %i[ request_new
                                         event_manage
                                         event_edit
                                         event_update ]

  def verify_licenses_conditions
    license = params[:bp_license_id] ? BpLicense.find(params[:bp_license_id]) : nil
    user_id = params[:user_id]
    request_id = params[:request_id]

    if (license && license.with_event) || !license
      event = params[:event_id] ? LicEvent.find(params[:event_id]) : nil
      event_date = event ? event.date_event : params[:date_event]
    else
      event = license.event_in(anniversary(user_id), user_id, params[:request_begin])
      event_date = event.date_event
    end

    amount = params[:request_days].size > 0 ? params[:request_days] : params[:request_hours]

    unless license.partially_taken
      amount = license.days if license.days && license.days > 0
      amount = license.hours if license.hours && license.hours > 0
    end

    request_begin = params[:request_begin].size > 0 ? params[:request_begin] : event_date
    request_end = nil
    request_end = vacations_end(license.only_laborable_days, request_begin, license.partially_taken ? amount : license.days, user_id) if license.days
    request_end = request_begin.to_datetime + (license.partially_taken ? amount : license.hours).to_i.hours if license.hours
    request_end = request_begin unless request_end

    messages = license.satisfy?(request_begin, request_end, amount, anniversary(user_id), user_id, event ? event.id : nil, request_id)
    other_messages = satisfy_license_begin(user_id, license.id, request_begin, event_date)
    messages += other_messages if other_messages.size > 0
    other_messages = satisfy_conflict_periods(request_begin, request_end.to_date, user_id, request_id)
    messages << other_messages if other_messages.size > 0
    messages << 'Debe tener por lo menos un aprobador activo asignado' if LicApprover.user_approvers(user_id).empty?

    if (license.days && license.days > 0) || (license.hours && license.hours > 0)
      requests = VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status, user_id: user_id)
      message = ''

      requests.each do |request|
        next unless (request.begin <= request_end.to_date && request.end >= request_begin.to_date)
        message += request.begin.strftime('%d/%m/%Y') + ' - ' + request.end.strftime('%d/%m/%Y') + ' (' + request.status_message + '). '
      end
      messages << 'Tiene conflicto con vacaciones: ' + message if message.size > 0
    end

    render partial: 'lic_registers/license_contidions_messages',
           locals: { messages: messages }
  end

  def get_available
    license = BpLicense.find(params[:bp_license_id])
    user_id = params[:user_id]

    event = license.event_in(anniversary(user_id), user_id, params[:request_begin])

    amount = event.sum_days if license.days
    amount = event.sum_hours unless amount
    available = license.days - amount if license.days
    available = license.hours - amount unless available

    render json: available.to_json
  end

  def get_end
    license = BpLicense.find(params[:bp_license_id])
    params[:request_days] = license.partially_taken ? params[:request_days] : license.days

    if params[:request_days] && params[:request_begin] && params[:request_days].size > 0 && params[:request_begin].size > 0
      request_end = vacations_end(license.only_laborable_days, params[:request_begin], params[:request_days], params[:user_id])
      render json: (localize request_end, :format => :day_snmonth_year).to_json
    else
      render json: 0.to_json
    end
  end

  def get_request_period
    license = BpLicense.find(params[:bp_license_id])
    amount = (params[:request_days] && params[:request_days].size > 0) ? params[:request_days] : ((params[:request_hours] && params[:request_hours].size > 0 ) ? params[:request_hours] : nil)
    user_id = params[:user_id]

    if (license && license.with_event) || !license
      event = params[:event_id] ? LicEvent.find(params[:event_id]) : nil
      event_date = event ? event.date_event : params[:date_event]
    else
      event = license.event_in(anniversary(user_id), user_id, params[:request_begin])
      event_date = event.date_event
    end

    request_begin =  params[:request_begin] && params[:request_begin].size > 0 ? params[:request_begin] : event_date
    request_end = nil
    request_end = vacations_end(license.only_laborable_days, request_begin, license.partially_taken ? amount : license.days, user_id) if license.days
    request_end = request_begin.to_datetime + (license.partially_taken ? amount : license.hours).to_i.hours if license.hours
    request_end = request_begin unless request_end

    ans = license.get_request_period(request_begin, request_end, user_id, anniversary(user_id))

    if ans && ans.size > 1
      json = ans[0].to_date.day.to_s + ' ' + t('date.month_names')[ans[0].to_date.month].to_s
      json += ' - '
      json += ans[1].to_date.day.to_s + ' ' + t('date.month_names')[ans[1].to_date.month].to_s
      render json: json.as_json
    else
      render json: ans[0].as_json
    end
  end

  def get_go_back
    license = BpLicense.find(params[:bp_license_id])
    params[:request_days] = params[:request_days].size > 0 ? params[:request_days] : license.days.to_s
    if params[:request_days].size > 0 && params[:request_begin].size > 0
      request_end = vacations_end(license.only_laborable_days, params[:request_begin], params[:request_days], params[:user_id])
      go_back_date = next_laborable_day?(request_end, params[:user_id])
      render json: (localize go_back_date, :format => :day_snmonth_year).to_json
    else
      render json: '-'.as_json
    end
  end

  def index
    @bp_group = BpGroup.user_active_group(lms_time, user_connected.id)
  end

  def verify_events_date
    events = LicEvent.where(user_id: user_connected.id,
                           bp_event_id: params[:bp_event_id],
                           date_event: params[:date_event].to_date)
    messages = []
    objs = []
    messages << 'Hay, por lo menos, un evento similar en la misma fecha. Para ir haga click: '

    events.each do |event|
      objs << event
    end

    messages << ' ' if objs.size >= 1
    messages = [] if messages.size == 1

    render partial: 'lic_registers/license_contidions_messages',
           locals: {messages: messages,
                    only_warning: true,
                    index_zero_title: true,
                    objs: objs }
  end

  def event_new
    @bp_license  = BpLicense.find(params[:bp_license_id])
    @lic_event = LicEvent.new(bp_form_id: @bp_license.active_form ? @bp_license.active_form.id : nil,
                              bp_event_id: @bp_license.bp_event_id)
  end

  def only_event_new
    @bp_event  = BpEvent.find(params[:bp_event_id])
    @lic_event = LicEvent.new(bp_form_id: @bp_event.active_form ? @bp_event.active_form.id : nil,
                              bp_event_id: @bp_event.id)
  end

  def only_event_create
    event = LicEvent.new(params[:lic_event])
    event.user_id = user_connected.id
    event.registered_at = lms_time
    event.registered_by_user_id = user_connected.id

    files_to_save = []
   company = @company

    event.lic_item_values.each do |value|
      value.registered_at = lms_time
      value.registered_by_user_id = user_connected.id
      next unless value.value_file
      if value.bp_item.item_file?
        value.value = value.value_file.original_filename
        file = value.value_file.dup
        value.value_file = SecureRandom.hex(5)
        files_to_save << [value.value_file.dup, file]
      end
    end

    if event.valid?
      files_to_save.each {|array| save_file(array[1], company, array[0])}
      event.save
      flash[:success] = 'Evento registrado correctamente'
      redirect_to lic_register_event_manage_path(event)
    else
      @lic_event = event
      @bp_event = event.bp_event
      render action: 'only_event_new'
    end
  end

  def event_create
    event = LicEvent.new(params[:lic_event])
    event.user_id = user_connected.id
    event.registered_at = lms_time
    event.registered_by_user_id = user_connected.id
    @bp_license  = BpLicense.find(params[:bp_license_id])
    event.bp_event_id = @bp_license.bp_event_id

   company = @company
    files_to_save = []

    event.lic_item_values.each do |value|
        value.registered_at = lms_time
        value.registered_by_user_id = user_connected.id
        next unless value.value_file
        if value.bp_item.item_file?
          value.value = value.value_file.original_filename
          file = value.value_file.dup
          value.value_file = SecureRandom.hex(5)
          files_to_save << [value.value_file.dup, file]
        end
      end

    saved_all = true
    event.lic_requests.each_with_index do |request, index|
      if index.zero? && !request.bp_license.with_event
        event_1 = request.bp_license.event_in1(anniversary(user_connected.id), user_connected.id, request.begin)
        event.date_event = event_1.date_event
        event.id = event_1.id if event_1.id
      end
      request.user_id = user_connected.id
      request.registered_at = lms_time
      request.registered_by_user_id = user_connected.id
      request = stablishing_attr(event, request)
      request.lic_event_id = event.id if event.id
      if request.lic_event_id && request.valid?
        request.save
      else
        saved_all = false
      end
    end

    request = event.lic_requests.first
    license = @bp_license
    messages = license.satisfy?(request.begin, request.end, request.days && request.days > 0 ? request.days : request.hours, anniversary(request.user_id), request.user_id, event ? event.id : nil, request.id)
    other_messages = satisfy_license_begin(request.user_id, license.id, request.begin, event.date_event)
    messages += other_messages if other_messages.size > 0
    # other_messages = satisfy_conflict_periods(request_begin, request_end.to_date, user_id, request_id)
    # messages << other_messages if other_messages.size > 0

    if (@bp_license.days && @bp_license.days > 0) || (@bp_license.hours && @bp_license.hours > 0)
      requests = VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status, user_id: request.user_id)
      message = ''

      request_end = request.end
      request_begin = request.begin

      requests.each do |request|
        next unless (request.begin <= request_end.to_date && request.end >= request_begin.to_date)
        message += request.begin.strftime('%d/%m/%Y') + ' - ' + request.end.strftime('%d/%m/%Y') + ' (' + request.status_message + '). '
      end
      messages += 'tiene conflicto con vacaciones: ' + message if message.size > 0
    end

    if event.valid? && !event.id && messages.size == 0
      files_to_save.each {|array| save_file(array[1], company, array[0])}
      event.save

      event.lic_requests.each do |request|
        LicApprover.request_approvers(request.id).each do |approver|
          VacsMailer.new_lic_pending_request(request, @company, approver.id).deliver if request.pending?
        end
        VacsMailer.new_lic_pending_requester(request,@company).deliver if request.pending?
      end
      redirect_to lic_register_event_manage_path(event)
    else
      if event.id && saved_all
        event.lic_requests.each do |request|
          LicApprover.request_approvers(request.id).each do |approver|
            VacsMailer.new_lic_pending_request(request, @company, approver.id).deliver if request.pending?
          end
          VacsMailer.new_lic_pending_requester(request,@company).deliver if request.pending?
        end
        redirect_to lic_register_event_manage_path(event)
      else
        @lic_event = event
        render action: 'event_new'
      end
    end
  end

  def event_manage
    @lic_event = LicEvent.find(params[:lic_event_id])
    @open_detail = params[:open_detail] ?  params[:open_detail].to_i : nil
  end

  def event_edit
    @lic_event = LicEvent.find(params[:lic_event_id])
  end

  def event_update
    prev_event = LicEvent.find(params[:lic_event_id])
    event = complete_event_update(params[:lic_event], prev_event.id)
   company = @company
    files_to_save = []

    event.lic_item_values.each do |value|
      value.registered_at = lms_time
      value.registered_by_user_id = user_connected.id
      next unless value.value_file
      if value.bp_item.item_file?
        next if value.value_file.class == String
        file = value.value_file.dup
        value.value = value.value_file.original_filename
        value.value_file = SecureRandom.hex(5)
        files_to_save << [value.value_file.dup, file]
      end
    end

    requests = []
    prev_event.lic_requests.where(active: true).each { |request| requests << request.dup }

    if event.valid?
      files_to_save.each {|array| save_file(array[1], company, array[0])}
      event.save
      requests.each { |request| request.update_attributes(lic_event_id: event.id) }
      flash[:success] = t('views.lic_registers.flash_messages.success_changed')
      redirect_to lic_register_event_manage_path(event)
    else
      @lic_event = event
      render action: 'event_edit'
    end
  end

  def request_new
    event = LicEvent.find(params[:lic_event_id])

    lic_request = LicRequest.new(lic_event_id: params[:lic_event_id],
                                 bp_license_id: params[:bp_license_id])

    lic_request =  set_attr(event, lic_request, nil, nil, nil, nil)
    # lic_request = stablishing_attr(event, lic_request)

    license = lic_request.bp_license
    user_id = lic_request.user_id ? lic_request.user_id : lic_request.lic_event.user_id
    request_id = nil

    if (license && license.with_event) || !license
      event_date = event.date_event
    else
      event = license.event_in(anniversary(user_id), user_id, lic_request.begin)
      event_date = event.date_event
    end

    amount = lic_request.days && lic_request.days > 0 ? lic_request.days : lic_request.hours

    unless license.partially_taken
      amount = license.days if license.days && license.days > 0
      amount = license.hours if license.hours && license.hours > 0
    end


    request_begin = lic_request.begin ? lic_request.begin : event_date
    request_end = nil
    request_end = license_end(license.only_laborable_days, request_begin,  license.partially_taken ? amount : license.days, user_id) if license.days
    # request_end = vacations_end(license.only_laborable_days, request_begin, license.partially_taken ? amount : license.days, user_id) if license.days
    request_end = request_begin.to_datetime + (license.partially_taken ? amount : license.hours).to_i.hours if license.hours
    request_end = request_begin unless request_end

    messages = license.satisfy?(request_begin, request_end, amount, anniversary(user_id), user_id, event ? event.id : nil, request_id)
    other_messages = satisfy_license_begin(user_id, license.id, request_begin, event_date)
    messages << other_messages if other_messages.size > 0
    other_messages = satisfy_conflict_periods(request_begin, request_end.to_date, user_id, request_id)
    messages << other_messages if other_messages.size > 0

    ans = license.get_request_period(request_begin, request_end, user_id, anniversary(user_id))

    if ans && ans.size > 1
      period = ans[0].to_date.day.to_s + ' ' + t('date.month_names')[ans[0].to_date.month].to_s
      period += ' - '
      period += ans[1].to_date.day.to_s + ' ' + t('date.month_names')[ans[1].to_date.month].to_s
    else
      period = ans[0]
    end

    if (license.days && license.days > 0) || (license.hours && license.hours > 0)
      requests = VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status, user_id: user_id)
      message = ''

      requests.each do |request|
        next unless (request.begin <= request_end.to_date && request.end >= request_begin.to_date)
        message += request.begin.strftime('%d/%m/%Y') + ' - ' + request.end.strftime('%d/%m/%Y') + ' (' + request.status_message + '). '
      end

      messages << ('tiene conflicto con vacaciones: ' + message) if message.size > 0
    end

    render partial: 'lic_registers/request_form',
           locals: {request: lic_request,
                    url_to_go: lic_register_request_create_path(lic_request.lic_event),
                    messages: messages,
                    period: period}
  end

  def request_create
    event = LicEvent.find(params[:lic_event_id])
    request = complete_request_create(event.id, params[:lic_request])
    license = request.bp_license

    messages = license.satisfy?(request.begin, request.end, request.days && request.days > 0 ? request.days : request.hours, anniversary(request.user_id), request.user_id, event ? event.id : nil, request.id)
    other_messages = satisfy_license_begin(request.user_id, license.id, request.begin, event.date_event)
    messages += other_messages if other_messages.size > 0
    # other_messages = satisfy_conflict_periods(request_begin, request_end.to_date, user_id, request_id)
    # messages << other_messages if other_messages.size > 0

    if (license.days && license.days > 0) || (license.hours && license.hours > 0)
      requests = VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status, user_id: request.user_id)
      message = ''

      request_end = request.end
      request_begin = request.begin

      requests.each do |request|
        next unless (request.begin <= request_end.to_date && request.end >= request_begin.to_date)
        message += request.begin.strftime('%d/%m/%Y') + ' - ' + request.end.strftime('%d/%m/%Y') + ' (' + request.status_message + '). '
      end
      messages += 'tiene conflicto con vacaciones: ' + message if message.size > 0
    end

    if request.valid? && messages.size == 0
      request.save
      flash[:success] = t('views.lic_registers.flash_messages.success_changed')
        LicApprover.request_approvers(request.id).each do |approver|
          VacsMailer.new_lic_pending_request(request, @company, approver.id).deliver if request.pending?
        end
      VacsMailer.new_lic_pending_requester(request,@company).deliver if request.pending?
      redirect_to lic_register_event_manage_path(event)
    else
      @lic_request = request
      redirect_to lic_register_event_manage_path(request.lic_event)
    end
  end

  def download_file
    value = LicItemValue.where(value_file: params[:value_file]).first
   company = @company
    if value && verify_value_access(value.id) && verify_request_access(value.lic_request_id)
      directory = company.directorio+'/'+company.codigo.to_s+'/lic_requests/' + value.lic_event.user_id.to_s + '/'
      full_path = directory + value.value_file
      send_file full_path,
                type: 'application/octet-stream',
                disposition: 'attachment',
                filename: value.value
    else
      flash[:danger] = t('views.lic_registers.flash_messages.danger_not_available_to_register')
      redirect_to lic_register_index_path
    end
  end

  def request_manage
    @lic_request = LicRequest.find(params[:lic_request_id])
  end

  def request_edit
    @lic_request = LicRequest.find(params[:lic_request_id])
    event = @lic_request.lic_event
    user_id = @lic_request.user_id ? @lic_request.user_id : event.user_id
    license = @lic_request.bp_license

    messages = license.satisfy?(@lic_request.begin,
                                @lic_request.end,
                                @lic_request.days ? @lic_request.days : @lic_request.hours,
                                anniversary(user_id),
                                user_id,
                                event ? event.id : nil,
                                @lic_request.id)

    other_messages = satisfy_license_begin(user_id, license.id, @lic_request.begin, event.date_event)
    messages += other_messages if other_messages.size > 0
    other_messages = satisfy_conflict_periods(@lic_request.begin, @lic_request.end.to_date, user_id, @lic_request.id) unless @lic_request.money
    messages << other_messages if other_messages.size > 0

    ans = license.get_request_period(@lic_request.begin, @lic_request.end, user_id, anniversary(user_id))

    if ans && ans.size > 1
      period = ans[0].to_date.day.to_s + ' ' + t('date.month_names')[ans[0].to_date.month].to_s+' '+event.date_event.year.to_s
      period += ' - '
      period += ans[1].to_date.day.to_s + ' ' + t('date.month_names')[ans[1].to_date.month].to_s+' '+event.date_event.year.to_s
    else
      period = ans[0]
    end

    if (license.days && license.days > 0) || (license.hours && license.hours > 0)
      requests = VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status, user_id: user_id)
      message = ''

      requests.each do |request|
        next unless (request.begin <= @lic_request.end.to_date && request.end >= @lic_request.begin.to_date)
        message += request.begin.strftime('%d/%m/%Y') + ' - ' + request.end.strftime('%d/%m/%Y') + ' (' + request.status_message + '). '
      end
      messages += 'tiene conflicto con vacaciones: ' + message if message.size > 0
    end

    render partial: 'lic_registers/request_form',
           locals: {request: @lic_request,
                    url_to_go: lic_register_request_update_path(@lic_request),
                    messages: messages,
                    period: period}
  end

  def request_update
    prev_request = LicRequest.find(params[:lic_request_id])
    request = complete_request_update(params[:lic_request], prev_request.id)
    license = prev_request.bp_license
    event_valid = true
    unless license.with_event
      event = license.event_in(anniversary(request.lic_event.user_id), request.lic_event.user_id, request.begin)
      unless request.lic_event_id == event.id
        unless event.id
          event.registered_at = lms_time
          event.user_id = prev_request.lic_event.user_id
          event.registered_by_user_id = user_connected.id
          event_valid = event.valid?
          event.save if event_valid
        end
        request.lic_event_id = event.id if event_valid
      end
    end

    event = request.lic_event
    messages = license.satisfy?(request.begin, request.end, request.days && request.days > 0 ? request.days : request.hours, anniversary(request.user_id), request.user_id, request.lic_event_id , request.id)
    other_messages = satisfy_license_begin(request.user_id, license.id, request.begin, event.date_event)
    messages += other_messages if other_messages.size > 0
    # other_messages = satisfy_conflict_periods(request_begin, request_end.to_date, user_id, request_id)
    # messages << other_messages if other_messages.size > 0

    if (license.days && license.days > 0) || (license.hours && license.hours > 0)
      requests = VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status, user_id: request.user_id)
      message = ''

      request_end = request.end
      request_begin = request.begin

      requests.each do |request|
        next unless (request.begin <= request_end.to_date && request.end >= request_begin.to_date)
        message += request.begin.strftime('%d/%m/%Y') + ' - ' + request.end.strftime('%d/%m/%Y') + ' (' + request.status_message + '). '
      end
      messages += 'tiene conflicto con vacaciones: ' + message if message.size > 0
    end

    if request.valid? && event_valid && messages.size == 0
      request.save
      prev_request = complete_deactivate_request(prev_request.id)
      prev_request.next_lic_request_id = request.id
      prev_request.save
      flash[:success] = t('views.lic_registers.flash_messages.success_changed')
      LicApprover.request_approvers(request.id).each do |approver|
        VacsMailer.new_lic_pending_request(request, @company, approver.id).deliver if request.pending?
      end
      VacsMailer.new_lic_pending_requester(request,@company).deliver if request.pending?
      redirect_to lic_register_event_manage_path(request.lic_event)
    else
      @lic_request = LicRequest.find(params[:lic_request_id]).assign_attributes(params[:lic_requests])
      redirect_to lic_register_event_manage_path(request.lic_event)
    end
  end

  def request_deactivate
    request = complete_deactivate_request(params[:lic_request_id])
    if request.valid?
      request.save
      flash[:success] = t('views.lic_registers.flash_messages.success_deleted')
      redirect_to lic_register_index_path
    else
      flash[:success] = t('views.lic_registers.flash_messages.danger_deleted')
      redirect_to lic_register_index_path
    end
  end

  def request_show_modal
    event = LicEvent.find(params[:lic_event_id])
    render partial: 'request_info_modal',
           locals: {event: event}
  end

  private

  def verify_request_access(request_id)
    true
  end

  def verify_value_access(value_id)
    true
  end

  def save_file(file, company, value_file)
    directory = company.directorio+'/'+company.codigo.to_s+'/lic_requests/' + user_connected.id.to_s + '/'
    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + value_file, 'wb') { |f| f.write(file.read) }
  end
end
