class DataSyncsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def sync_generate_active_users_register_characteristic

    Characteristic.where('data_type_id = ?', 11).each do |characteristic|

      DataSyncsController.sync_generate_active_users_register_characteristic characteristic, localize(lms_time, format: '%Y%m%d'), alias_username

    end

  end

  def sync_generate_active_users_training

    DataSyncsController.sync_generate_active_users_training @company, localize(lms_time, format: '%Y%m%d'), alias_username

  end

  def sync_read_users

    DataSyncsController.sync_read_users params[:diff_days].to_i

  end

  def sync_read_nodes

    DataSyncsController.sync_read_nodes params[:diff_days].to_i

  end

  def sync_read_users_photos

    DataSyncsController.sync_read_users_photos params[:diff_days].to_i

  end

  def self.sync_read_users(diff_days=0)

    ActiveRecord::Base.establish_connection 'production_desarrollosecurity'

    company = Company.where('codigo = ?', 'security').first

    lms_time = Time.now - 1.day - diff_days.days
    date_file = lms_time.strftime('%Y%m%d')

    has_sync_errors = false

    if File.exist?('/var/www/remote-storage/sync_security/upload/EMPLEADO_'+date_file+'.txt')

      company = Company.first

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      text = File.read('/var/www/remote-storage/sync_security/upload/EMPLEADO_'+date_file+'.txt')
      text = text.force_encoding('ISO-8859-1')
      text = text.encode('UTF-8', 'ISO-8859-1')
      text.gsub!(/\r\n?/, "\n")

      text.each_line do |line|

        if line.strip != ''

          datos_tmp = line.split('{')

          datos = datos_tmp[0].split(';')

          #0 usuario

          user = User.find_by_codigo datos[0]

          if user

            user.nombre = datos[1]
            user.apellidos = datos[2]
            user.email = datos[3] unless datos[3].downcase.include? 'sincorreo'
            user.activo = datos[4].downcase == 'activo' ? true : false

            unless user.valid?

              if user.errors.include? :email

                user.email = ''

                DataSyncsController.create_data_sync_log 'users', lms_time, true, 'línea: '+num_linea.to_s+', '+datos[0]+': El correo electrónico no es válido: '+datos[3]

                has_sync_errors = true

              end

            end

          else

            user = User.new

            user.codigo = datos[0]

            user.nombre = datos[1]
            user.apellidos = datos[2]
            user.email = datos[3] unless datos[3].downcase.include? 'sincorreo'
            user.activo = datos[4].downcase == 'activo' ? true : false
            user.requiere_cambio_pass = true
            user.password = datos[0].slice(0,4)
            user.password_confirmation = user.password

            unless user.valid?

              if user.errors.include? :email
                user.email = ''

                DataSyncsController.create_data_sync_log 'users', lms_time, true, 'línea: '+num_linea.to_s+', '+datos[0]+': El correo electrónico no es válido: '+datos[3]

                has_sync_errors = true

              end

            end

          end

          if user.save

              if datos_tmp[1]

                datos_tmp[1].gsub!("\n",'')

                atributos =datos_tmp[1].strip.slice(0..-2).split(';')

                atributos.each do |atributo|

                  atributo = atributo.split(':')

                  caracteristica = Characteristic.find_by_nombre(atributo[0])

                  if caracteristica && caracteristica.id != 14 && caracteristica.id != 27 && caracteristica.id != 28 && caracteristica.id != 29 && caracteristica.id != 30 && caracteristica.id != 31 && caracteristica.id != 32 && caracteristica.id != 2

                      if !user.activo && (caracteristica.data_type_id == 13 || caracteristica.data_type_id == 14)

                    else

                      if caracteristica && caracteristica.data_type_id != 6

                        new_value = nil

                        error = false

                        if atributo[1] && !atributo[1].blank?

                          if caracteristica.data_type_id == 5 || caracteristica.data_type_id == 14
                            characteristic_value = caracteristica.characteristic_values.where('value_string = ?', atributo[1].strip).first
                            if characteristic_value
                              new_value = characteristic_value.id.to_s
                            else
                              error = true
                            end
                          else
                            new_value = atributo[1].strip
                          end

                        end

                        if error

                          DataSyncsController.create_data_sync_log 'users', lms_time, false, 'línea: '+num_linea.to_s+', '+datos[0]+': '+atributo[0].force_encoding('UTF-8')+' no fue guardado: '+atributo[1]+' no existe'

                          has_sync_errors = true

                        else

                          error, user_characteristic = user.update_user_characteristic(caracteristica, new_value, lms_time, nil, nil, nil, nil, company)

                          if error

                            DataSyncsController.create_data_sync_log 'users', lms_time, false, 'línea: '+num_linea.to_s+', '+datos[0]+': '+atributo[0].force_encoding('UTF-8')+' no fue guardado: '+user_characteristic.errors.full_messages.to_s

                            has_sync_errors = true

                          end

                        end
                      else

                        DataSyncsController.create_data_sync_log 'users', lms_time, false, 'línea: '+num_linea.to_s+', '+datos[0]+': '+atributo[0].force_encoding('UTF-8')+' no existe'

                        has_sync_errors = true

                      end

                    end

                  end

                end

              end

          else

            DataSyncsController.create_data_sync_log 'users', lms_time, false, 'línea: '+num_linea.to_s+', '+datos[0]+': '+user.errors.full_messages.to_s

            has_sync_errors = true

          end

        end

        num_linea += 1

      end

    else

      DataSyncsController.create_data_sync_log 'users', lms_time, false, 'file not found'

      has_sync_errors = true

    end

    u = User.find_by_codigo '183019333'
    u.activo = false
    u.save

    u = User.find_by_codigo '63817651'
    u.activo = true
    u.save

    u = User.find_by_codigo '76397074'
    u.activo = true
    u.save

    if has_sync_errors

      data_sync_logs = DataSync.where('description = ? AND executed_at = ?', 'users', lms_time)

      begin
        LmsMailer.log_sync_security('users',data_sync_logs,lms_time,company).deliver
      rescue Exception => e

      end

    else

      begin
        LmsMailer.log_sync_security('users',Array.new,lms_time,company).deliver
      rescue Exception => e

      end

    end

  end

  def self.sync_read_nodes(diff_days=0)

    ActiveRecord::Base.establish_connection 'production_desarrollosecurity'

    company = Company.where('codigo = ?', 'security').first

    lms_time = Time.now - 1.day - diff_days.days
    date_file = lms_time.strftime('%Y%m%d')

    has_sync_errors = false

    if File.exist?('/var/www/remote-storage/sync_security/upload/ORGANIGRAMA_'+date_file+'.txt')

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      text = File.read('/var/www/remote-storage/sync_security/upload/ORGANIGRAMA_'+date_file+'.txt')
      text.gsub!(/\r\n?/, "\n")

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')
          datos[1].gsub!("\n", '')

          #0 usuario_jefe_directo
          #1 usuario_subordinado_directo

          user_jefe_directo = User.find_by_codigo datos[0].strip
          user_sub_directo = User.find_by_codigo datos[1].strip

          if user_jefe_directo && user_sub_directo

            unless user_jefe_directo.owned_nodes.first

              node = user_jefe_directo.owned_nodes.build
              node.nombre = ''
              unless node.save

                DataSyncsController.create_data_sync_log 'org', lms_time, false, 'línea: '+num_linea.to_s+', '+node.errors.full_messages.to_s

                has_sync_errors = true

              end

            end

            user_sub_directo.node_to_which_belongs = user_jefe_directo.owned_nodes.first

            unless user_sub_directo.save

              DataSyncsController.create_data_sync_log 'org', lms_time, false, 'línea: '+num_linea.to_s+', '+user_sub_directo.errors.full_messages.to_s
              has_sync_errors = true
            end


          else

            e = ''
            e = datos[0]+': usuario jefe no existe ' unless user_jefe_directo
            e += ' - '+datos[1]+': usuario sub no existe' unless user_sub_directo

            DataSyncsController.create_data_sync_log 'org', lms_time, false, 'línea: '+num_linea.to_s+', '+e
            has_sync_errors = true
          end

        end

        num_linea += 1

      end

      nodes = Node.all
      nodes.each do |node|
        node.node = node.owner.node_to_which_belongs
        node.save
      end

    else

      DataSyncsController.create_data_sync_log 'org', lms_time, false, 'file not found'
      has_sync_errors = true
    end

    if has_sync_errors

      data_sync_logs = DataSync.where('description = ? AND executed_at = ?', 'org', lms_time)

      begin
        LmsMailer.log_sync_security('org',data_sync_logs,lms_time,company).deliver
      rescue Exception => e

      end

    else

      begin
        LmsMailer.log_sync_security('org',Array.new,lms_time,company).deliver
      rescue Exception => e

      end

    end

  end

  def self.sync_read_users_photos(diff_days=0)

    ActiveRecord::Base.establish_connection 'production_desarrollosecurity'

    company = Company.where('codigo = ?', 'security').first

    lms_time = Time.now - 1.day - diff_days.days
    date_file = lms_time.strftime('%Y%m%d')

    has_sync_errors = false

    if File.exist?('/var/www/remote-storage/sync_security/upload/FOTOS_'+date_file+'.zip')

      @total_fotos_cargadas = 0
      @fotos_error = []
      @fotos_error_messages = []

      directory = company.directorio+'/'+company.codigo+'/fotos_usuarios/'

      FileUtils.mkdir_p directory unless File.directory? directory

      directory_tmp = directory+SecureRandom.hex(5)+'/'

      FileUtils.mkdir_p directory_tmp unless File.directory? directory_tmp

      name = 'FOTOS.zip'

      FileUtils.cp '/var/www/remote-storage/sync_security/upload/FOTOS_'+date_file+'.zip', directory_tmp+name

      system "unzip -o -q \"#{directory_tmp+name}\" -d \"#{directory_tmp}\" "
      #File.delete directory_tmp+name

      Dir.entries(directory_tmp).each do |foto|

        if foto != '.' && foto != '..' && foto != 'FOTOS.zip' && foto != '__MACOSX'

          ext = File.extname(foto)

          if ext.downcase == '.jpg' || ext.downcase == '.jpeg'

            file_tmp = directory_tmp+foto

            if File.size(file_tmp) <= 70.kilobytes

              codigo_usuario = File.basename(file_tmp, ext)

              user = User.find_by_codigo codigo_usuario

              if user

                if user.foto
                  file_actual = directory+user.foto
                  File.delete(file_actual) if File.exist? file_actual
                end

                user.set_foto

                user.save

                file = directory+user.foto

                FileUtils.mv(file_tmp, file)

                @total_fotos_cargadas += 1

              else

                DataSyncsController.create_data_sync_log 'photos', lms_time, false, foto+': no existe usuario'
                has_sync_errors = true

              end

            else

              DataSyncsController.create_data_sync_log 'photos', lms_time, false, foto+': tamaño > 70kb'
              has_sync_errors = true

            end

          else

            DataSyncsController.create_data_sync_log 'photos', lms_time, false, foto+': formato incorrecto'
            has_sync_errors = true

          end

        end

      end

      FileUtils.remove_dir directory_tmp

    else

      DataSyncsController.create_data_sync_log 'photos', lms_time, false, 'file not found'
      has_sync_errors = true

    end

    if has_sync_errors

      data_sync_logs = DataSync.where('description = ? AND executed_at = ?', 'photos', lms_time)

      begin
        LmsMailer.log_sync_security('photos',data_sync_logs,lms_time,company).deliver
      rescue Exception => e

      end

    else

      begin
        LmsMailer.log_sync_security('photos',Array.new,lms_time,company).deliver
      rescue Exception => e

      end

    end

  end

  def self.create_data_sync_log(description, executed_at, completed, sync_errors )
    data_sync = DataSync.new
    data_sync.description = description
    data_sync.executed_at = executed_at
    data_sync.completed = completed
    data_sync.sync_errors = sync_errors
    data_sync.save
  end

  def self.sync_generate_active_users_register_characteristic(characteristic_parent, generation_date, alias_username)

    characteristics = characteristic_parent.elements_characteristics.reorder('register_position')

    users = User.where('activo = ?', true)

    filename = characteristic_parent.nombre.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n,'').upcase+'_'+generation_date+'.csv'

    CSV.open('/var/www/remote-storage/sync_security/downloads/'+filename, 'wb', {:col_sep => ";", :encoding=> 'UTF-16LE'}) do |csv|
    #CSV.open('/Users/alejo/Downloads/CT/'+filename, 'wb', {:col_sep => ";", :encoding=> 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#{alias_username}"

      characteristics.each do |characteristic|

        columns.push characteristic.nombre

      end

      csv << columns

      users.each_with_index do |user, index_u|

        user_characteristics_registered = user.user_characteristics_registered(characteristic_parent)

        if user_characteristics_registered.size == 0

          columns = Array.new

          columns.push user.codigo

          csv << columns

        else

          user_characteristics_registered.each_with_index do |user_characteristic_registered|

            columns = Array.new

            columns.push user.codigo

            characteristics.each_with_index do |element_characteristic|

              element_user_characteristic = user_characteristic_registered.elements_user_characteristics.where('register_user_characteristic_id = ? AND characteristic_id = ?',user_characteristic_registered.id, element_characteristic.id).first

              unless element_characteristic.data_type_id == 6

                if element_user_characteristic

                  efcv = element_user_characteristic.excel_formatted_value

                  if efcv

                    columns.push efcv

                  else

                    columns.push ''

                  end

                else

                  columns.push ''

                end

              else

                if element_user_characteristic && element_user_characteristic.value_file

                  columns.push 'Sí'

                else

                  columns.push 'No'

                end

              end

            end

            csv << columns

          end

        end

      end

    end

  end

  def self.sync_generate_active_users_training(company, generation_date, alias_username)

    #company = session[:company]

    users = User.where('activo = ?', true)

    filename = 'CAPACITACION_'+generation_date+'.csv'

    CSV.open('/var/www/remote-storage/sync_security/downloads/'+filename, 'wb', {:col_sep => ";", :encoding=> 'UTF-16LE'}) do |csv|
    #CSV.open('/Users/alejo/Downloads/CT/'+filename, 'wb', {:col_sep => ";", :encoding=> 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#{alias_username}"
      columns.push 'Programa'
      if TrainingType.all.size > 0
        columns.push company.training_type_alias
      end
      columns.push 'Curso'
      columns.push 'Dedicación'
      columns.push 'Oportunidad'
      columns.push 'Fecha Inicio'
      columns.push 'Fecha Fin'
      columns.push 'Fecha Inicio Real'
      columns.push 'Fecha Fin Real'
      columns.push 'Estado'
      columns.push 'Asistencia'
      columns.push 'Nota Final'
      columns.push 'Resultado Final'

      csv << columns

      users.each_with_index do |user|

        user_courses = user.user_courses

        if user_courses.size == 0

          columns = Array.new

          columns.push user.codigo

          csv << columns

        else

          user_courses.each_with_index do |user_course|

            columns = Array.new

            columns.push user.codigo
            columns.push user_course.program_course.program.nombre
            if TrainingType.all.size > 0
              if user_course.course.training_type
                columns.push user_course.course.training_type.nombre
              else
                columns.push ''
              end
            end
            columns.push user_course.course.nombre
            columns.push user_course.course.dedicacion_estimada
            columns.push user_course.numero_oportunidad

            if user_course.desde
              columns.push user_course.desde.to_date
            else
              columns.push ''
            end

            if user_course.hasta
              columns.push user_course.hasta.to_date
            else
              columns.push ''
            end

            if user_course.inicio
              columns.push user_course.inicio.to_date
            else
              columns.push ''
            end

            if user_course.fin
              columns.push user_course.fin.to_date
            else
              columns.push ''
            end

            if !user_course.iniciado
              e = 'No iniciado'
            elsif user_course.finalizado
              e = 'Terminado'
            else
              e = 'Iniciado'
            end

            columns.push e

            if user_course.porcentaje_avance
              columns.push user_course.porcentaje_avance
            else
              columns.push ''
            end

            if user_course.nota
              columns.push user_course.nota
            else
              columns.push ''
            end

            if user_course.aprobado
              ef = 'Aprobado'
            elsif user_course.finalizado
              ef = 'Reprobado'
            else
              ef = ''
            end

            columns.push ef

            csv << columns

          end

        end

      end

    end

  end

end
