class BpAdminRuleVacationsController < ApplicationController
  include BpAdminRuleVacationsHelper
  include BpAdminConditionVacationsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin, except: :rule_vacations_show_modal

  def new
    @bp_rule_vacation = BpRuleVacation.new(bp_group_id: params[:bp_group_id])
    @bp_group = BpGroup.find(params[:bp_group_id])
  end

  def deactivate_rules
    rv = BpRuleVacation.find(params[:bp_rule_vacation_id])
    rv.bp_condition_vacations.only_available.each do |cond|
      cond.available = false
      cond.unavailable_at = lms_time
      cond.unavailable_by_admin_id = admin_connected.id
      cond.save
    end
    rv.available = false
    rv.unavailable_at = lms_time
    rv.unavailable_by_admin_id = admin_connected.id
    rv.save

    flash[:success] = 'Se desactivo correctamente'
    redirect_to bp_admin_group_manage_path(rv.bp_group_id)
  end

  def create
    rule_vacations = []
    params[:select2_groups].each do |group_id|
      rule_vacations << complete_create(params[:bp_rule_vacation], group_id)
    end

    validation = true
    rule_vacations.each do |rule_vacation|
      next if rule_vacation.valid?
      validation = false
    end

    if validation
      rule_vacations.each { |rule_vacation| rule_vacation.save }
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_created')
      redirect_to bp_admin_group_manage_path(params[:bp_group_id])
    else
      rule_vacation = complete_create(params[:bp_rule_vacation], params[:bp_group_id])
      rule_vacation.valid?
      @bp_rule_vacation = rule_vacation
      @bp_group = rule_vacation.bp_group
      render action: 'new'
    end
  end

  def edit
    @bp_rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
  end

  def update
    if params[:bp_rule_vacation][:until].blank?
      rule_vacations = []
      params[:select2_rule_vacations].each do |rv_id|
        rule_vacations << complete_update(params[:bp_rule_vacation], rv_id)
      end

      validation = true
      rule_vacations.each do |rv|
        next if rv.valid?
        validation = false
      end

      if validation
        rule_vacations.each {|rv| rv.save}
        flash[:success] = t('views.bp_licenses.flash_messages.success_changed')
        redirect_to bp_admin_rule_vacation_manage_path(params[:bp_group_id], params[:bp_rule_vacation_id])
      else
        rule_vacation = complete_update(params[:bp_rule_vacation], params[:bp_rule_vacation_id])

        rule_vacations.each do |cond|
          cond.errors.full_messages.each {|error| rule_vacation.errors[:base] << (error)}
        end

        group = BpGroup.find(params[:bp_group_id])
        @bp_rule_vacation = rule_vacation
        @bp_group = group
        render action: 'edit'
      end

    else

      rule_vacation = complete_update(params[:bp_rule_vacation], params[:bp_rule_vacation_id])
      group = BpGroup.find(params[:bp_group_id])
      if rule_vacation.save
        flash[:success] = t('views.bp_licenses.flash_messages.success_changed')
        redirect_to bp_admin_rule_vacation_manage_path(group, rule_vacation)
      else
        @bp_rule_vacation = rule_vacation
        @bp_group = group
        render action: 'edit'
      end

    end
  end

  def manage
    @bp_rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
    @bp_group = @bp_rule_vacation.bp_group
  end

  def delete
    rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
    answer = verify_to_delete_rules_vacation(rule_vacation.id)
    group_id = rule_vacation.bp_group_id
    rule_vacation_id = rule_vacation.id
    if answer && rule_vacation.destroy
      BpConditionVacation.where(bp_rule_vacation_id: rule_vacation_id).destroy_all
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_deleted')
      redirect_to bp_admin_group_manage_path(group_id)
    else
      flash[:danger] = t('views.bp_general_rules_vacations.flash_messages.danger_deleted')
      redirect_to bp_admin_rule_vacation_manage_path(rule_vacation.bp_group_id, rule_vacation.id)
    end
  end

  def rule_vacations_show_modal
    active_rules = BpRuleVacation.active_rules_vacations(params[:now].to_datetime, params[:now].to_datetime, params[:user_id])
    active_rules = active_rules ? active_rules : []
    render partial: 'bp_rule_vacations_info_modal',
           locals: { rule_vacations: active_rules }
  end

  private

  def complete_create(params, group_id)
    rule_vacation = BpRuleVacation.new(params)
    rule_vacation.bp_group_id = group_id
    rule_vacation.registered_by_admin_id = admin_connected.id
    rule_vacation.registered_at = lms_time
    return rule_vacation unless rule_vacation.until
    rule_vacation.deactivated_by_admin_id = admin_connected.id
    rule_vacation.deactivated_at = lms_time
    return rule_vacation
  end

  def complete_update(params, rule_vacation_id)
    rule_vacation = BpRuleVacation.find(rule_vacation_id)
    rule_vacation.assign_attributes(params)
    return rule_vacation if !params[:until] || (params[:until] && params[:until].blank?)
    rule_vacation.deactivated_by_admin_id = admin_connected.id
    rule_vacation.deactivated_at = lms_time
    return rule_vacation
  end

end
