class ElecProcessRoundsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /elec_process_rounds
  # GET /elec_process_rounds.json
  def index
    @elec_process_rounds = ElecProcessRound.find_all_by_elec_process_id(params[:elec_process_id])

    render :layout => false

  end

  # GET /elec_process_rounds/1
  # GET /elec_process_rounds/1.json
  def show
    @elec_process_round = ElecProcessRound.find(params[:id])

  end

  # GET /elec_process_rounds/new
  # GET /elec_process_rounds/new.json
  def new
    @elec_process = ElecProcess.find(params[:elec_process_id])
    @elec_process_round = ElecProcessRound.new
    @elec_process_round.elec_process = @elec_process

  end

  # GET /elec_process_rounds/1/edit
  def edit
    @elec_process_round = ElecProcessRound.find(params[:id])

  end

  # POST /elec_process_rounds
  # POST /elec_process_rounds.json
  def create
    @elec_process_round = ElecProcessRound.new(params[:elec_process_round])

    if @elec_process_round.save
      flash[:success] = 'Los datos de la ronda fueron guardados correctamente'
      redirect_to elec_processes_show_with_open_pill_path(@elec_process_round.elec_process_id, 'rounds')
    else
      render action: "new"
    end
  end

  # PUT /elec_process_rounds/1
  # PUT /elec_process_rounds/1.json
  def update
    @elec_process_round = ElecProcessRound.find(params[:id])

    if @elec_process_round.update_attributes(params[:elec_process_round])
      flash[:success] = 'Los datos de la ronda fueron guardados correctamente'
      redirect_to elec_processes_show_with_open_pill_path(@elec_process_round.elec_process_id, 'rounds')
    else
      render action: "edit"
    end
  end

  # DELETE /elec_process_rounds/1
  # DELETE /elec_process_rounds/1.json
  def destroy
    @elec_process_round = ElecProcessRound.find(params[:id])
    elec_process_id = @elec_process_round.elec_process_id
    @elec_process_round.destroy

    flash[:success] = 'La ronda fue eliminada correctamente'
    redirect_to elec_processes_show_with_open_pill_path(elec_process_id, 'rounds')

  end
end
