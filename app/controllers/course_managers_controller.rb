class CourseManagersController < ApplicationController

  include ActionView::Helpers::NumberHelper
  include ExcelReportsHelper

  before_filter :authenticate_user
  before_filter :verify_course_manager, only: [:groups_list, :register_asist_eval, :update_asist_eval,
                                               :register_poll, :update_poll,
                                               :empty_students_list_xlsx, :acta_students_xlsx]

  def groups_list


  end

  def register_asist_eval

    @grupo = params[:grupo]

    @course = @pcm.program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @pcm.program_course.id, @course.id, @grupo).joins(:user).order('apellidos, nombre')

    @user_courses = @pcm.program_course.matriculas_vigentes_por_grupo @grupo

  end

  def update_asist_eval

    @grupo = params[:grupo]

    @course = @pcm.program_course.course

    if @course.asistencia_presencial

      params[:user_id].each do |user_course_id,value|

        user_course = UserCourse.find(user_course_id)
        asist = params[:user_id][user_course.id.to_s.to_sym]
        if asist == user_course.id.to_s
          user_course.iniciado = true
          user_course.finalizado = true
          user_course.asistencia = true
          user_course.porcentaje_avance = 100
          user_course.inicio = user_course.desde if user_course.inicio.nil?
          user_course.fin = user_course.hasta if user_course.fin.nil?
        else
          user_course.asistencia = false
        end

        user_course.save

      end

    end

    if @course.eval_presencial

      params[:user_course_id].each do |user_course_id,value|

        user_course = UserCourse.find(user_course_id)

        user_course.nota = value

        user_course.inicio = user_course.desde if user_course.inicio.nil?

        if user_course.save

          if user_course.course.nota_minima == 0
            user_course.aprobado = true
          elsif !user_course.nota.nil?
            user_course.aprobado = true
            user_course.aprobado = false if (value.to_i < user_course.course.nota_minima) || user_course.nota == ''
          elsif
            user_course.aprobado = false
          end

          user_course.save

        end

      end

    end

    if @course.asistencia_presencial && @course.eval_presencial
      flash[:success] = t('activerecord.success.model.user_course.evaluate_assit_course_ok')
    elsif @course.asistencia_presencial
      flash[:success] = t('activerecord.success.model.user_course.assit_course_ok')
    elsif @course.eval_presencial
      flash[:success] = t('activerecord.success.model.user_course.evaluate_course_ok')
    end

    redirect_to course_managers_register_asist_eval_path(@pcm,@pcm.program_course, @grupo)


  end

  def register_poll

    @grupo = params[:grupo]

    @course = @pcm.program_course.course

    #@user_course = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @pcm.program_course.id, @course.id, @grupo).first

    @user_course = @pcm.program_course.matriculas_vigentes_por_grupo(@grupo).first

  end

  def update_poll

    @grupo = params[:grupo]

    @course = @pcm.program_course.course

    if @course.poll_presencial?

      poll = @course.polls.where('tipo_satisfaccion = ?', true).first

      master_questions = poll.master_poll.master_poll_questions

      master_questions.each do |master_question|

        master_question.master_poll_alternatives.each do |alt|

          poll_summary = ProgramCoursePollSummary.where('program_course_id = ? AND poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ? AND grupo = ? ',
                                                        @pcm.program_course.id, poll.id, master_question.id, alt.id, @grupo).first

          poll_summary.destroy if poll_summary

          poll_summary = ProgramCoursePollSummary.new
          poll_summary.program_course = @pcm.program_course
          poll_summary.poll = poll
          poll_summary.master_poll_question = master_question
          poll_summary.master_poll_alternative = alt
          poll_summary.grupo = @grupo
          poll_summary.numero_respuestas = 0

          if params[:poll_summary][alt.id.to_s.to_sym]

            poll_summary.numero_respuestas = params[:poll_summary][alt.id.to_s.to_sym]

          end

          poll_summary.save

        end

      end

      flash[:success] = t('activerecord.success.model.user_course.polling_course_ok')

    end

    redirect_to course_managers_register_poll_path(@pcm,@pcm.program_course, @grupo)


  end

  def empty_students_list_xlsx

    grupo = params[:grupo]

    course = @pcm.program_course.course
    program_course = @pcm.program_course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', program_course.id, course.id, grupo).joins(:user).order('apellidos, nombre')

    @user_courses = program_course.matriculas_vigentes_por_grupo grupo

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}



      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}





      wb.add_worksheet(:name => 'Lista') do |reporte_excel|

        characteristics = Characteristic.where('basic_report = ?', true).reorder('orden_basic_report')

        fila_actual = 1



        fila = Array.new
        fila << 'Curso'
        fila << ''
        fila << course.nombre
        fila << ''

        reporte_excel.add_row fila, :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':D'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Fecha'
        fila << ''
        fila << (localize  @user_courses.first.desde, format: :day_month_year)+' - '+(localize  @user_courses.first.hasta, format: :day_month_year)
        fila << ''

        reporte_excel.add_row fila, :style => headerTitleInfo, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':D'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 3

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        characteristics.each do |c|
          fila << c.nombre
          filaHeader <<  headerLeft
          cols_widths << 22
        end

        fila << ''
        filaHeader <<  headerLeft
        cols_widths << 20


        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          characteristics.each do |c|

            u_cha = uc.enrollment.user.characteristic_by_id(c.id)
            if u_cha
              fila << u_cha.valor if uc
            else
              fila << ''
            end

            fileStyle << fieldLeft

          end

          fila << ''
          fileStyle << fieldLeft

          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Lista - '+course.nombre+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def acta_students_xlsx

    grupo = params[:grupo]

    course = @pcm.program_course.course
    program_course = @pcm.program_course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', program_course.id, course.id, grupo).joins(:user).order('apellidos, nombre')

    @user_courses = program_course.matriculas_vigentes_por_grupo grupo

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}



      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 11,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Acta') do |reporte_excel|

        characteristics = Characteristic.where('basic_report = ?', true).reorder('orden_basic_report')

        fila_actual = 1

        fila = Array.new
        fila << 'Curso'
        fila << ''
        fila << course.nombre
        fila << ''
        fila << ''

        reporte_excel.add_row fila, :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':F'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Fecha'
        fila << ''
        fila << (localize  @user_courses.first.desde, format: :day_month_year)+' - '+(localize  @user_courses.first.hasta, format: :day_month_year)
        fila << 'Nota Min.'
        fila << course.nota_minima
        fila << ''

        reporte_excel.add_row fila, :style => headerTitleInfo, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('E'+fila_actual.to_s+':F'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 3

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        characteristics.each do |c|
          fila << c.nombre
          filaHeader <<  headerLeft
          cols_widths << 22
        end

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Nota'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10


        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          characteristics.each do |c|

            u_cha = uc.enrollment.user.characteristic_by_id(c.id)
            if u_cha
              fila << u_cha.valor if uc
            else
              fila << ''
            end

            fileStyle << fieldLeft

          end

          if uc.asistencia
            fila << 'Sí' if uc.asistencia
          else
            fila << 'NO'
          end

          fileStyle << fieldLeft

          fila << uc.nota

          if uc.aprobado
            fileStyle << fieldLeftAprob
            fila << 'Aprobado'
            fileStyle << fieldLeftAprob

          else
            fileStyle << fieldLeftReprob
            if uc.nota
              fila << 'Reprobado'
            else
              fila << ''
            end
            fileStyle << fieldLeftReprob


          end


          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Acta - '+course.nombre+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  private

    def verify_course_manager

      @pcm = ProgramCourseManager.find_by_id(params[:id])

      unless @pcm && @pcm.user_id == user_connected.id
        flash[:danger] = t('security.no_access_course_manager')
        redirect_to root_path
      end

    end

end
