class PollProcessUsersController < ApplicationController
  # GET /poll_process_users
  # GET /poll_process_users.json

  before_filter :authenticate_user
  before_filter :validate_access_answer

  def new
    @poll_process_users = PollProcessUser.new
  end

  def create

    @poll_process_users = PollProcessUser.where(:poll_process_id => params[:poll_process_user][:poll_process_id], :user_id => user_connected.id).first_or_initialize

    @questions_simple = params[:qs] ? params[:qs].count : 0
    @questions_multiple = params[:qm] ? params[:qm].count : 0

    qtext = {}
    if params[:qt]
      params[:qt].each do |question_id, text|
        if !text.blank?
          qtext[question_id] = text
        end
      end
    end

    @questions_text = qtext.count

    @questions_recorded = PollProcessUserAnswer.where(:poll_process_user_id => @poll_process_users.id).select('Distinct master_poll_question_id').count

    @number_questions_answered = @questions_simple + @questions_multiple + @questions_recorded + @questions_text
    @poll_number_questions = @poll_process_users.poll_process.master_poll.master_poll_questions.count


    # si se 'pausa' el sistema deja modificar (añadir, cambiar quitar) respuestas
    if params[:poll_process_user][:pause] == '1'
      @poll_process_users.done = false
      flash[:success] = 'Sus respuestas fueron guardadas correctamente, usted podrá continuar respondiendo su encuesta en otro momento'
    else
      @poll_process_users.done = true
      flash[:success] = 'Muchas gracias por responder la encuesta, ésta fue guardada correctamente'
    end



    PollProcessUserAnswer.destroy_all(:poll_process_user_id => @poll_process_users.id)
    @poll_process_users.save!

    #se graban las respuestas de tipo simple
    if @questions_simple > 0
      params[:qs].each do |question_id, alternative_id|
        save_answers(@poll_process_users.id, question_id, alternative_id, nil)
      end
    end


    #se graban las respuestas de tipo múltiple
    if @questions_multiple > 0
      params[:qm].each do |question_id, alternative_hash|
        alternative_hash.each do |alt_index, alternative_id|
          save_answers(@poll_process_users.id, question_id, alternative_id, nil)
        end
      end
    end

    #se graban las respuestas de tipo texto
    if @questions_text > 0
      qtext.each do |question_id, text|
        save_answers(@poll_process_users.id, question_id, nil, text)
      end
    end

    if @poll_process_users.done && @poll_process_users.poll_process.show_preview_results_user
      redirect_to poll_processes_view_report_path @poll_process_users.poll_process_id
    else
      redirect_to root_path
      #redirect_to poll_processes_execute_path @poll_process_users.poll_process_id
    end

  end


  private

  def validate_access_answer
    poll_process = PollProcess.find(params[:poll_process_user][:poll_process_id])

    if !PollProcess.actives_for_user_to_answer(session[:user_connected_id], lms_time).include? poll_process
      flash[:danger] = 'No tiene permitido responder esta encuesta'
      redirect_to root_path
    end

  end

  def save_answers(poll_process_user_id, master_poll_question_id, master_poll_alternative_id, long_answer)

    @poll_process_user_answer = PollProcessUserAnswer.new(:poll_process_user_id => poll_process_user_id,
                                                          :master_poll_question_id => master_poll_question_id,
                                                          :master_poll_alternative_id => master_poll_alternative_id,
                                                          :long_answer => long_answer)


    @poll_process_user_answer.save!
  end

end