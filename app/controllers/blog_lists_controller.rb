class BlogListsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /blog_lists
  # GET /blog_lists.json
  def index
    @blog_lists = BlogList.all

  end

  # GET /blog_lists/1
  # GET /blog_lists/1.json
  def show
    @blog_list = BlogList.find(params[:id])

  end

  # GET /blog_lists/new
  # GET /blog_lists/new.json
  def new
    @blog_list = BlogList.new

  end

  # GET /blog_lists/1/edit
  def edit
    @blog_list = BlogList.find(params[:id])
  end

  # POST /blog_lists
  # POST /blog_lists.json
  def create
    @blog_list = BlogList.new(params[:blog_list])

    if @blog_list.save
      flash[:success] = 'La lista fue creada correctamente'
      redirect_to @blog_list
    else
      flash.now[:danger] = 'La lista no fue creada correctamente'
      render 'new'
    end

  end

  # PUT /blog_lists/1
  # PUT /blog_lists/1.json
  def update
    @blog_list = BlogList.find(params[:id])

    if @blog_list.update_attributes(params[:blog_list])
      flash[:success] = 'La lista fue actualizada correctamente'
      redirect_to @blog_list
    else
      flash.now[:danger] = 'La lista no fue actualizada correctamente'
      render 'edit'
    end
  end

  # DELETE /blog_lists/1
  # DELETE /blog_lists/1.json
  def destroy
    @blog_list = BlogList.find(params[:id])
    @blog_list.destroy

    redirect_to blog_lists_path
  end
end
