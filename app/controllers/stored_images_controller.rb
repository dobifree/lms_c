class StoredImagesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @stored_images = StoredImage.all
  end

  def show
    @stored_image = StoredImage.find(params[:id])
  end

  def new
    @stored_image = StoredImage.new
  end

  def edit
    @stored_image = StoredImage.find(params[:id])
  end

  def create

   company = @company

    @stored_image = StoredImage.new(params[:stored_image])

    if @stored_image.save

      if params[:upload]

        upload = params[:upload]

        ext = File.extname(upload['datafile'].original_filename).downcase

        if ext == '.jpg' || ext == '.jpeg'

          directory = company.directorio+'/'+company.codigo+'/stored_images/'

          FileUtils.mkdir_p directory unless File.directory? directory

          file = directory+@stored_image.crypted_name
          File.delete(file) if File.exist? file

          file = directory+@stored_image.crypted_name

          File.open(file, 'wb') { |f| f.write(upload['datafile'].read) }

        end

      end

      flash[:success] = t('activerecord.success.model.stored_image.create_ok')

      redirect_to stored_images_path

    else

      render action: 'new'

    end

  end

  def update

   company = @company

    @stored_image = StoredImage.find(params[:id])

    if @stored_image.update_attributes(params[:stored_image])

      if params[:upload]

        upload = params[:upload]

        ext = File.extname(upload['datafile'].original_filename).downcase

        if ext == '.jpg' || ext == '.jpeg'

          directory = company.directorio+'/'+company.codigo+'/stored_images/'

          FileUtils.mkdir_p directory unless File.directory? directory

          file = directory+@stored_image.crypted_name
          File.delete(file) if File.exist? file

          file = directory+@stored_image.crypted_name

          File.open(file, 'wb') { |f| f.write(upload['datafile'].read) }

        end

      end

      flash[:success] = t('activerecord.success.model.stored_image.update_ok')

      redirect_to stored_images_path

    else
      render action: 'edit'
    end

  end

  def destroy

   company = @company
    directory = company.directorio+'/'+company.codigo+'/stored_images/'

    @stored_image = StoredImage.find(params[:id])

    file = directory+@stored_image.crypted_name
    File.delete(file) if File.exist? file

    @stored_image.destroy

    flash[:success] = t('activerecord.success.model.stored_image.delete_ok')

    redirect_to stored_images_path

  end

end
