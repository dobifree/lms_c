class BrgProcessesController < ApplicationController
  include BrgModule
  include BrgProcessesHelper

  before_filter :authenticate_user
  before_filter :active_module

  before_filter :brg_manager, except: %i[index_for_manager
                                         bonus_show_modal
                                         update_bonus
                                         update_bonus_by_ajax
                                         approve_bonus_ajax
                                         approve_bonus
                                         index_for_manager_0
                                         index_for_manager_1
                                         gimme_bonus_datatable_index_1
                                         gimme_sum_table]

  before_filter :verify_im_next_manager, only: %i[bonus_show_modal
                                                  update_bonus
                                                  update_bonus_by_ajax
                                                  approve_bonus_ajax
                                                  approve_bonus]

  before_filter :get_process, only: %i[manage
                                       edit
                                       update
                                       manage_users
                                       process_status_switch
                                       index_for_manager_1
                                       gimme_bonus_datatable_index_1
                                       gimme_sum_table]

  before_filter :get_process_for_bonus, only: %i[ reprocess_bonus ]

  before_filter :get_bonus, only: %i[change_paid_ajax
                                     bonus_show_modal
                                     update_bonus
                                     update_bonus_by_ajax
                                     approve_bonus_ajax
                                     approve_bonus]

  before_filter :verify_open_process_process, only: %i[edit
                                                       update
                                                       manage_users
                                                       index_for_manager_1
                                                       gimme_bonus_datatable_index_1
                                                       gimme_sum_table]

  before_filter :verify_open_process_for_bonus, only: %i[change_paid_ajax
                                                         bonus_show_modal
                                                         update_bonus
                                                         update_bonus_by_ajax
                                                         approve_bonus_ajax
                                                         approve_bonus
                                                         ]

  before_filter :get_manager, only: %i[index_for_manager
                                       reprocess_bonus
                                       index_for_manager_1
                                       gimme_bonus_datatable_index_1
                                       index_for_manager_0
                                       gimme_sum_table]

  before_filter :verify_active_bonus, only: %i[ reprocess_bonus]

  def gimme_bonus_datatable_index_1
    render partial: 'brg_processes/bonus_datatable',
                           locals: {editable: true,
                                    process: @process,
                                    bonus: BrgBonus.process(@process.id).all_pending.active_processes.open_processes.group_user_order,
                                    manager_view: true,
                                    simple_view: true,
                                    manager: @manager}
  end

  def reprocess_bonus
    bonus = reprocess_this_bonus(@bonus.id)
    if bonus && bonus.errors.size <= 0
      flash[:success] = t('views.brg_module.flash_messages.change_success')
    else
      flash[:danger] = t('views.brg_module.flash_messages.change_danger')
    end
    redirect_to brg_processes_manage_path(@process)
  end

  def index_for_manager_0
    @as_brg_rp_manager = params[:as_brg_rp_manager].blank? ? nil : true
  end

  def index_for_manager_1
    @tab = params[:tab].blank? ? 0 : params[:tab]
  end

  def gimme_sum_table
    render partial: 'brg_processes/group_bonus_approved_datatable',
           locals: {process: @process,
                    this_user_id: user_connected.id }
  end

  def index_for_manager
    @tab = params[:tab].blank? ? 0 : params[:tab]
  end

  def new
    begin_period = lms_time.change(year: (lms_time).year, month:01, day: 01, hour:00, min: 00, sec: 00)
    end_period = lms_time.change(year: (lms_time).year, month:12, day: 31, hour:23, min:59, sec: 59)
    reference_year = lms_time.year
    name = 'Proceso año '+(lms_time).year.to_s
    @process = BrgProcess.new(name: name, period_begin: begin_period, period_end: end_period, reference_year: reference_year)
  end

  def create
    process = BrgProcess.new(params[:brg_process])
    process = set_registered(process)
    if process.save
      # create_bonus_for_active_groups(process.id)
      flash[:success] = t('views.brg_module.flash_messages.create_success')
      redirect_to brg_processes_manage_path(process)
    else
      @process = process
      render 'new'
    end
  end

  def manage; @tab = params[:tab].blank? ? 0 : params[:tab] end

  def edit
    @super_edition = params[:super_edition].to_i == 1
  end

  def update
    if @process.update_attributes(params[:brg_process])
      reprocess_this(@process) if defined?(params[:super_update]) && params[:super_update].to_i == 1
      flash[:success] = t('views.brg_module.flash_messages.change_success')
      redirect_to brg_processes_manage_path(@process)
    else
      @super_edition = params[:super_update].to_i == 1
      render 'edit'
    end
  end

  def manage_users; @tab = params[:tab].blank? ? 0 : params[:tab] end

  def change_paid_ajax
    if @bonus.paid
      render partial: 'bonus_pay_row',
             locals: {bonus: @bonus,
                      message: t('views.brg_module.flash_messages.bonus_already_paid'),
                      process: @bonus.brg_process}
    else
      paid_description = @bonus.paid == false ?  '' : t('views.brg_module.messages.dont_pay_this_process_to_user')
      paid_value = @bonus.paid == false ? nil : false
      new_bonus = change_pay(@bonus.id,paid_description, paid_value)

      render partial: 'bonus_pay_row',
             locals: {bonus: new_bonus,
                      message: t('views.brg_module.flash_messages.change_success'),
                      process: @bonus.brg_process}
    end
  end

  def bonus_show_modal
    render partial: 'brg_processes/bonus_modal',
           locals: { bonus: @bonus, index: params[:index] }
  end

  # def update_bonus
  #   prev_bonus = @bonus
  #   bonus = prev_bonus.dup
  #   deactivate(prev_bonus).save
  #   bonus = set_registered(bonus)
  #   bonus.prev_bonus_id = prev_bonus.id
  #   bonus.assign_attributes(params[:brg_bonus])
  #   bonus = set_actual_value(bonus)
  #   bonus.save
  #   flash[:success] = t('views.brg_module.flash_messages.change_success')
  #   redirect_to brg_processes_index_for_manager_path
  # end

  def update_bonus_by_ajax
    if @bonus.registered_by_user_id == user_connected.id
      bonus = @bonus
      bonus = set_registered(bonus)
      bonus.assign_attributes(params[:brg_bonus])
      bonus = set_actual_value(bonus)
    else
      prev_bonus = @bonus
      bonus = prev_bonus.dup
      deactivate(prev_bonus).save
      bonus = set_registered(bonus)
      bonus.prev_bonus_id = prev_bonus.id
      bonus.assign_attributes(params[:brg_bonus])
      bonus = set_actual_value(bonus)
    end

    bonus.ready_to_go = false
    bonus.ready_to_go_at = nil

    if bonus.save
      generic_message = t('views.brg_module.flash_messages.change_success')
      type_generic_message = 'alert-success'
    else
      generic_message = ''
      bonus.errors.full_messages.each { |msg| generic_message += msg+'. ' }
      type_generic_message = 'alert-danger'
    end

    render partial: 'bonus_row_datatable',
           locals: { editable: true,
                     bonus: bonus,
                     manager_view: true,
                     simple_view: true,
                     my_turn: bonus.my_turn_manager?(user_connected.id),
                     manager: BrgManager.active.by_user(user_connected.id).first,
                     index: params[:index],
                     generic_message: generic_message,
                     type_generic_message: type_generic_message }
  end

  def approve_bonus_ajax
    bonus = @bonus
    approved_bonus = bonus.dup
    approved_bonus = set_registered(approved_bonus)
    approved_bonus = set_approve_bonus(approved_bonus)
    approved_bonus.prev_bonus_id = bonus.id

    if approved_bonus.save
      deactivate(bonus).save
      generic_message = t('views.brg_module.flash_messages.validated_success')
      type_generic_message = 'alert-success'
    else
      generic_message = ''
      approved_bonus.errors.full_messages.each { |msg| generic_message += msg+'. ' }
      type_generic_message = 'alert-danger'
    end

    render partial: 'bonus_row_datatable',
           locals: {editable: true,
                    bonus: approved_bonus,
                    manager_view: true,
                    simple_view: true,
                    manager: BrgManager.active.by_user(user_connected.id).first,
                    index: params[:index],
                    generic_message: generic_message,
                    type_generic_message: type_generic_message}

  end

  def approve_bonus
    bonus = @bonus
    approved_bonus = bonus.dup
    approved_bonus = set_registered(approved_bonus)
    approved_bonus.prev_bonus_id = bonus.id
    approved_bonus.save
    deactivate(bonus).save
    flash[:success] = t('views.brg_module.flash_messages.change_success')
    redirect_to brg_processes_index_for_manager_path
  end

  def set_evaluations
    process = PeProcess.find(params[:pe_process_id])
    evaluations = process.pe_evaluations
    evaluations_options = evaluations.all.map {|i| [i.name, i.id]}
    render json: evaluations_options
  end

  def process_status_switch
    if @process.update_attributes(status: @process.status == BrgProcess.in_process ?  BrgProcess.closed : BrgProcess.in_process )
      flash[:success] = t('views.brg_module.flash_messages.change_success')
    else
      flash[:danger] = t('views.brg_module.flash_messages.change_danger')
    end
    redirect_to brg_processes_manage_path(@process)
  end

  private

  def verify_open_process_for_bonus
    process =  BrgBonus.find(params[:brg_bonus_id]).brg_process
    return unless process.closed?
    flash[:danger] = t('views.brg_module.flash_messages.closed_process')
    redirect_to brg_processes_manage_path(process)
  end
end
