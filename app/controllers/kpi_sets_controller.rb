class KpiSetsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def new

    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
    @kpi_set = @kpi_dashboard.kpi_sets.build

  end

  def edit
    @kpi_set = KpiSet.find(params[:id])
    @kpi_dashboard = @kpi_set.kpi_dashboard
  end

  def create
    @kpi_set = KpiSet.new(params[:kpi_set])

    if @kpi_set.save
      flash[:success] =  t('activerecord.success.model.kpi_dashboard_set.create_ok')
      redirect_to @kpi_set.kpi_dashboard
    else
      @kpi_dashboard = @kpi_set.kpi_dashboard
      render action: 'new'
    end
  end


  def update
    @kpi_set = KpiSet.find(params[:id])

    if @kpi_set.update_attributes(params[:kpi_set])
      flash[:success] =  t('activerecord.success.model.kpi_dashboard_set.update_ok')
      redirect_to @kpi_set.kpi_dashboard
    else
      @kpi_dashboard = @kpi_set.kpi_dashboard
      render action: 'edit'
    end

  end

  def destroy

    kpi_set = KpiSet.find(params[:id])
    kpi_dashboard = kpi_set.kpi_dashboard
    kpi_set.destroy

    flash[:success] =  t('activerecord.success.model.kpi_dashboard_set.delete_ok')
    redirect_to kpi_dashboard

  end


  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'kpis', true).first

      unless admin_logged_in? || (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end

end
