class UcCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /uc_characteristics
  # GET /uc_characteristics.json
  def index
    @uc_characteristics = UcCharacteristic.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @uc_characteristics }
    end
  end

  # GET /uc_characteristics/1
  # GET /uc_characteristics/1.json
  def show
    @uc_characteristic = UcCharacteristic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @uc_characteristic }
    end
  end

  # GET /uc_characteristics/new
  # GET /uc_characteristics/new.json
  def new
    @uc_characteristic = UcCharacteristic.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @uc_characteristic }
    end
  end

  # GET /uc_characteristics/1/edit
  def edit
    @uc_characteristic = UcCharacteristic.find(params[:id])
  end

  # POST /uc_characteristics
  # POST /uc_characteristics.json
  def create
    @uc_characteristic = UcCharacteristic.new(params[:uc_characteristic])

    respond_to do |format|
      if @uc_characteristic.save
        format.html { redirect_to @uc_characteristic, notice: 'Uc characteristic was successfully created.' }
        format.json { render json: @uc_characteristic, status: :created, location: @uc_characteristic }
      else
        format.html { render action: "new" }
        format.json { render json: @uc_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /uc_characteristics/1
  # PUT /uc_characteristics/1.json
  def update
    @uc_characteristic = UcCharacteristic.find(params[:id])

    respond_to do |format|
      if @uc_characteristic.update_attributes(params[:uc_characteristic])
        format.html { redirect_to @uc_characteristic, notice: 'Uc characteristic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @uc_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /uc_characteristics/1
  # DELETE /uc_characteristics/1.json
  def destroy
    @uc_characteristic = UcCharacteristic.find(params[:id])
    @uc_characteristic.destroy

    respond_to do |format|
      format.html { redirect_to uc_characteristics_url }
      format.json { head :no_content }
    end
  end
end
