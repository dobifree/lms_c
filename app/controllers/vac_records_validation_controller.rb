class VacRecordsValidationController < ApplicationController
  include VacRecordsValidationHelper
  include VacRecordsRegisterHelper
  include VacManagerHelper
  include VacationsModule

  before_filter :authenticate_user
  before_filter :verify_access_validator_module
  before_filter :only_active_request, only: [:manage, :edit, :update, :change_status]
  before_filter :approver_access_request, only: [:edit, :update, :change_status]


  def index
    @as_manager = defined?(params[:as_manager]) && params[:as_manager].to_i == 1 && (ct_module_vacs_manager? ? true : false)
  end

  def manage
    @vac_request = VacRequest.find(params[:vac_request_id])
    @vac_request = VacRequest.find(@vac_request.latest_version)

    unless @vac_request.active
    flash[:danger] = t('views.vac_requests.flash_messages.danger_not_active_request')
    redirect_to root_path
    end
  end

  def edit
    @vac_request = VacRequest.find(params[:vac_request_id])
    @vac_request = VacRequest.find(@vac_request.latest_version)
  end

  def update
    actual_request = VacRequest.find(params[:vac_request_id])
    actual_request = VacRequest.find(actual_request.latest_version)
    params[:vac_request_id] = actual_request.id

    updated_request = complete_update(params[:vac_request_id], params[:vac_request])
    if updated_request.valid?
      if restrictive_warnings(updated_request.user_id, updated_request.begin, updated_request.end, updated_request.days, updated_request.days_progressive, params[:vac_request_id])
        @vac_request = actual_request
        @vac_request.errors.add(:base, 'Tiene por lo menos una alarma restrictiva')
        @vac_request.assign_attributes(params[:vac_request])

        render partial: 'vac_records_register/request_new_form',
                      locals: { request: @vac_request,
                                url_to_go: vac_records_validation_update_path(@vac_request),
                                validator: true  }
      else
        updated_request.save
        actual_request.next_request_id = updated_request.id
        actual_request.save
        request = deactivate_request(params[:vac_request_id])
        request.copy_active_rule_records(updated_request.id)
        request.save
        # complete_create_record(request.id) if updated_request.approved?
        flash[:success] = t('views.vac_requests.flash_messages.success_changed')
        redirect_to vac_records_validation_manage_path(updated_request)
      end
    else
      @vac_request = VacRequest.find(params[:vac_request_id])
      @vac_request = @vac_request.assign_attributes(params[:vac_request])
      render partial: 'vac_records_register/request_new_form',
                    locals: { request: @vac_request,
                              url_to_go: vac_records_validation_update_path(@vac_request),
                              validator: true  }
    end
  end

  def change_status
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)
    params[:vac_request_id] = request.id

    type_generic_message = 'danger'
    generic_message = t('views.vac_requests.flash_messages.danger_changed')

    request_status = params[:vac_request_status]
    status_description = params[:vac_request][:status_description]

    params = {status: request_status,
              status_description: status_description,
              status_changed_by_user_id: user_connected.id}

    updated_request = complete_update(request.id, params)
    if updated_request.valid?
      updated_request.save
      benefits = VacRuleRecord.where(active: true, vac_request_id: request.id)
      benefits.each do |benefit|
        new_benefit = benefit.dup
        new_benefit.vac_request_id = updated_request.id
        new_benefit.save
      end
      prev_request = VacRequest.find(request.id)
      prev_request.next_request_id = updated_request.id
      prev_request = deactivate_request(request.id)
      prev_request.save
      type_generic_message = 'success'
      generic_message = t('views.vac_requests.flash_messages.success_changed')
      update_accumulated_since(prev_request.begin, prev_request.user_id, 'Se rechazó solicitud cerrada') if prev_request.closed?
      request = updated_request
      VacsMailer.approved_request(request, @company).deliver if request.approved?
      VacsMailer.rejected_request(request, @company).deliver if request.rejected?
    end

    render partial: 'vac_records_register/request_info',
           locals: {request: request,
                    show_generic_message: true,
                    type_generic_message: type_generic_message,
                    generic_message: generic_message,
                    editable: true,
                    approver: true}
  end

  def status_description_modal
    request = VacRequest.find(params[:vac_request_id])
    request.status = params[:vac_request_status].to_i
    render partial: 'vac_records_register/status_description_modal',
           locals: { request: request,
                     required: request.status_description_required? }
  end

  def approved_requests
    render partial: 'vac_records_register/requests_list_mobile',
           locals: {
               show_new_request: true,
               show_users: true,
               validator: true,
               editable: true,
               dont_show_bonus_status: true,
               dont_show_status: true,
               requests: VacRequest.default_requests.only_this_status([VacRequest.approved]).approver_visible_1(user_connected.id)}
  end

  private

  def verify_access_validator_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'vacs', true).first
    validator = ct_module ? VacApprover.where(user_id: user_connected.id, active: true).first : nil
    validator = validator ? true : ct_module_vacs_manager?

    unless ct_module && validator
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def only_active_request
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)

    unless request.active
      flash[:danger] = t('views.vac_requests.flash_messages.danger_not_active_request')
      redirect_to root_path
    end
  end

  def approver_access_request
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)

    unless request.editable_by_approver?(user_connected.id, lms_time)
      flash[:danger] = 'No puede editar este registro'
      redirect_to vac_records_validation_index_path
    end
  end

  def only_active_record

  end

end
