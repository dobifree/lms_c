class TrackingProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin, except: [:index_manager, :subject_answers_report]
  before_filter :verify_access_manage_module, only: [:index_manager, :subject_answers_report]

  def index
    @tracking_processes = TrackingProcess.all
  end

  def index_manager
    @tracking_processes = TrackingProcess.all
  end

  # GET /tracking_processes/1
  # GET /tracking_processes/1.json
  def show
    @tracking_process = TrackingProcess.find(params[:id])

    if params[:pill_open]
      @show_pill_open = params[:pill_open]
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json {render json: @tracking_process}
    end
  end

  # GET /tracking_processes/new
  # GET /tracking_processes/new.json
  def new
    @tracking_process = TrackingProcess.new

    respond_to do |format|
      format.html # new.html.erb
      format.json {render json: @tracking_process}
    end
  end

  # GET /tracking_processes/1/edit
  def edit
    @tracking_process = TrackingProcess.find(params[:id])
  end

  # POST /tracking_processes
  # POST /tracking_processes.json
  def create
    @tracking_process = TrackingProcess.new(params[:tracking_process])

    respond_to do |format|
      if @tracking_process.save
        format.html {redirect_to @tracking_process, notice: 'Tracking process was successfully created.'}
        format.json {render json: @tracking_process, status: :created, location: @tracking_process}
      else
        format.html {render action: "new"}
        format.json {render json: @tracking_process.errors, status: :unprocessable_entity}
      end
    end
  end

  # PUT /tracking_processes/1
  # PUT /tracking_processes/1.json
  def update
    @tracking_process = TrackingProcess.find(params[:id])

    respond_to do |format|
      if @tracking_process.update_attributes(params[:tracking_process])
        format.html {redirect_to @tracking_process, notice: 'Tracking process was successfully updated.'}
        format.json {head :no_content}
      else
        format.html {render action: "edit"}
        format.json {render json: @tracking_process.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /tracking_processes/1
  # DELETE /tracking_processes/1.json
  def destroy
    @tracking_process = TrackingProcess.find(params[:id])
    @tracking_process.destroy

    respond_to do |format|
      format.html {redirect_to tracking_processes_url}
      format.json {head :no_content}
    end
  end

  def show_upload_participants_csv
    @tracking_process = TrackingProcess.find(params[:id])
    render :layout => false
  end

  def upload_participants_csv

    tracking_process = TrackingProcess.find(params[:id])
    text = params[:upload]['datafile'].read
    text.gsub!(/\r\n?/, "\n")

    #@lineas_error = []
    #@lineas_error_detalle = []
    #@lineas_error_messages = []
    num_linea = 1

    text.each_line do |line|

      if line.strip != ''
        datos = line.split(';')
        datos[1].delete!("\n")
        datos[1].delete!("\r")

        attendant_user = User.where(:codigo => datos[0].to_s, :activo => true).first

        subject_user = User.where(:codigo => datos[1].to_s, :activo => true).first

        if attendant_user && subject_user
          # se valida que el registro de participantes es incremental, sólo sube los pares nuevos. No elimina ninguno
          tracking_process.tracking_participants.where(:attendant_user_id => attendant_user.id, :subject_user_id => subject_user.id).first_or_create
        end

      end

      num_linea += 1

    end

    flash['success'] = 'Participantes registrados correctamente'
    redirect_to tracking_process_pill_open_path(tracking_process, 'participants')
  end

  def remove_couple_participants
    tracking_process = TrackingProcess.find(params[:id])
    if tracking_process.tracking_participants.find(params[:tracking_participant_id]).destroy
      flash['success'] = 'El registro fue eliminado correctamente.'
    else
      flash['danger'] = 'El registro no pudo ser eliminado correctamente'
    end
    redirect_to tracking_process_pill_open_path(tracking_process, 'participants')
  end

  def reset_couple_participants
    tracking_process = TrackingProcess.find(params[:id])
    if tracking_process.tracking_participants.find(params[:tracking_participant_id]).tracking_form_instances.destroy_all
      flash['success'] = 'El registro fue reseteado correctamente.'
    else
      flash['danger'] = 'El registro no pudo ser reseteado correctamente'
    end
    redirect_to tracking_process_pill_open_path(tracking_process, 'participants')
  end

  def list_config_report
    @tracking_process = TrackingProcess.find(params[:id])
    #@tracking_form_items_reportable = @tracking_process.tracking_forms.joins(:tracking_form_groups => {:tracking_form_questions => :tracking_form_items}).where(:tracking_form_items => {:reportable => true})
    @tracking_form_items_reportable = TrackingFormItem.joins(:tracking_form_question => {:tracking_form_group => :tracking_form}).where(:reportable => true, :tracking_forms => {:tracking_process_id => @tracking_process.id}).reorder('tracking_forms.position, tracking_form_groups.position, tracking_form_questions.position, tracking_form_items.position')
    render :layout => false
  end

  def show_config_report
    @tracking_process = TrackingProcess.find(params[:id])
    #@tracking_form_items_reportable = @tracking_process.tracking_forms.joins(:tracking_form_groups => {:tracking_form_questions => :tracking_form_items}).where(:tracking_form_items => {:reportable => true})
    @tracking_form_items_reportable = TrackingFormItem.joins(:tracking_form_question => {:tracking_form_group => :tracking_form}).where(:reportable => true, :tracking_forms => {:tracking_process_id => @tracking_process.id}).reorder('tracking_forms.position, tracking_form_groups.position, tracking_form_questions.position, tracking_form_items.position')
  end

  def toggle_config_report
    tracking_form_item = TrackingFormItem.find(params[:tracking_form_item_id])

    tracking_form_item.reportable = !tracking_form_item.reportable?

    if tracking_form_item.save
      flash[:success] = 'La visibilidad del item se actualizó correctamente'
    else
      flash[:danger] = 'La visibilidad del item no se actualizó correctamente'
    end

    redirect_to tracking_process_show_config_report_path(params[:id])

  end

  def show_config_chars
    @tracking_process = TrackingProcess.find(params[:id])

  end

  def config_chars
    @tracking_process = TrackingProcess.find(params[:id])
    if @tracking_process.update_attributes(params[:tracking_process])
      flash[:success] = 'Los datos se guardaron correctamente'
      redirect_to tracking_process_pill_open_path(@tracking_process, 'report_config')
    else
      flash.now[:danger] = 'Los datos no se guardaron correctamente'
      render 'show_config_chars'
    end
  end

  def subject_answers_report
    @tracking_process = TrackingProcess.find(params[:id])


    @tracking_form_item_ids = TrackingFormItem.joins(:tracking_form_question => {:tracking_form_group => :tracking_form}).where(:reportable => true, :tracking_forms => {:tracking_process_id => @tracking_process.id}).reorder('tracking_forms.position, tracking_form_groups.position, tracking_form_questions.position, tracking_form_items.position').pluck('tracking_form_items.id')

    sql = "select b.tracking_form_question_id, max(answers) as max_answers
                    from
                    (SELECT p.id, a.tracking_form_question_id, count(*) as answers
                    FROM tracking_participants as p inner join tracking_form_instances as i
                    ON (p.id = i.tracking_participant_id) inner join tracking_form_answers as a
                    ON (i.id = a.tracking_form_instance_id)
                    where p.tracking_process_id = #{@tracking_process.id}
                    and i.done = 1
                    group by p.id, a.tracking_form_question_id) as b
                    group by b.tracking_form_question_id"


    @max_by_question = Hash.new
    TrackingFormQuestion.connection.select(sql).each do |record|
      @max_by_question[record['tracking_form_question_id']] = record['max_answers']
    end

    #basic_char_ids = @tracking_process.tracking_process_chars.pluck('tracking_process_chars.characteristic_id')

    @include_due_dates = true
    #@basic_chars = Characteristic.where(:id => basic_char_ids)

    @basic_chars = @tracking_process.characteristics.joins(:tracking_process_chars).reorder('tracking_process_chars.position')

    request.format = "xls"
    filename = 'detalle_respuestas_' + @tracking_process.name.gsub(/\s/, '_') + '.xls'
    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end
  end

  def show_clone_form
    @tracking_process = TrackingProcess.find(params[:id])
    @show_pill_open = params[:pill_open] ? params[:pill_open] : 'structure'
  end

  def show_clone_structure
    tracking_process = TrackingProcess.find(params[:id])
    render partial: 'tracking_processes/clone/structure', locals: {tracking_process: tracking_process}
  end

  def save_clone_structure
    @tracking_process = TrackingProcess.find(params[:id])

    structure = {}
    structure[:all] = true

    definition = {}
    definition[:structure] = structure

    tracking_processes = session[:tracking_processes_clone] ? session[:tracking_processes_clone] : {}
    tracking_processes[@tracking_process.id] = definition

    session[:tracking_processes_clone] = tracking_processes

    redirect_to tracking_process_show_clone_form_pill_open_path(@tracking_process, 'data')
  end

  def show_clone_data
    tracking_process = TrackingProcess.find(params[:id])
    @tracking_form_actions_array = []
    if session[:tracking_processes_clone] && session[:tracking_processes_clone][tracking_process.id] && session[:tracking_processes_clone][tracking_process.id][:data]
      action = TrackingFormAction.find(session[:tracking_processes_clone][tracking_process.id][:data][:action_id])
      @action_id = action.id
      @form_id = action.tracking_form_id
      @just_filled = session[:tracking_processes_clone][tracking_process.id][:data][:just_filled]
      @include_tracking = session[:tracking_processes_clone][tracking_process.id][:data][:include_tracking]
      @exclude_finished_tracking = session[:tracking_processes_clone][tracking_process.id][:data][:exclude_finished_tracking]

      @tracking_form_actions_array = action.tracking_form.tracking_form_actions.map {|action| [action.position.to_s + ' - ' + action.name, action.id, {'data-fill-form' => action.fill_form}]}
    end
    render partial: 'tracking_processes/clone/data', locals: {tracking_process: tracking_process}
  end

  def get_options_for_form
    tracking_form = TrackingForm.find(params[:form_id])
    render json: tracking_form.tracking_form_actions.map {|action| [action.position.to_s + ' - ' + action.name, action.id, action.fill_form]}
  end

  def save_clone_data
    tracking_process = TrackingProcess.find(params[:id])
    if session[:tracking_processes_clone] && session[:tracking_processes_clone][tracking_process.id]
      if params[:form_id] && (tracking_form = tracking_process.tracking_forms.where(id: params[:form_id]).first)
        if params[:action_id] && (tracking_form_action = tracking_form.tracking_form_actions.where(id: params[:action_id]).first)
          data = {}
          data[:action_id] = tracking_form_action.id
          data[:just_filled] = (tracking_form_action.fill_form? && params[:just_filled]) ? true : false
          data[:include_tracking] = params[:include_tracking].blank? ? false : true
          data[:exclude_finished_tracking] = params[:exclude_finished_tracking].blank? ? false : true
          session[:tracking_processes_clone][tracking_process.id][:data] = data
          redirect_to tracking_process_show_clone_form_pill_open_path(tracking_process, 'confirm')
        end
      else
        if session[:tracking_processes_clone][tracking_process.id][:data]
          session[:tracking_processes_clone][tracking_process.id].delete(:data)
        else
          flash[:danger] = 'Debe configurar el punto de clonación de datos. Si no desea clonar datos (sólo la estructura) vaya directamente a la confirmación.'
        end
        redirect_to tracking_process_show_clone_form_pill_open_path(tracking_process, 'data')
      end
    else
      flash[:danger] = 'Primero debe seleccionar la estructura a clonar'
      redirect_to tracking_process_show_clone_form_path(tracking_process)
    end

  end

  def show_clone_confirmation
    tracking_process = TrackingProcess.find(params[:id])

    if session[:tracking_processes_clone] && session[:tracking_processes_clone][tracking_process.id]
      @clone_structure = true
      if session[:tracking_processes_clone][tracking_process.id][:data]
        @action = TrackingFormAction.find(session[:tracking_processes_clone][tracking_process.id][:data][:action_id])
        @just_filled = session[:tracking_processes_clone][tracking_process.id][:data][:just_filled]
        @include_tracking = session[:tracking_processes_clone][tracking_process.id][:data][:include_tracking]
        @exclude_finished_tracking = session[:tracking_processes_clone][tracking_process.id][:data][:exclude_finished_tracking]
      end
    end

    render partial: 'tracking_processes/clone/confirmation', locals: {tracking_process: tracking_process}
  end

  def clone_process
    tracking_process_to_clone = TrackingProcess.find(params[:id])

    if session[:tracking_processes_clone] && session[:tracking_processes_clone][tracking_process_to_clone.id]
      clone_config = session[:tracking_processes_clone][tracking_process_to_clone.id]
      begin
        # clonación de datos del proceso y estructura
        tracking_process_cloned = tracking_process_to_clone.dup
        tracking_process_cloned.active = false
        tracking_process_cloned.transaction do
          tracking_process_cloned.save!
          # características para los reportes
          tracking_process_to_clone.tracking_process_chars.each do |char|
            char_cloned = char.dup
            char_cloned.tracking_process = tracking_process_cloned
            char_cloned.save!
          end
          forms_conversion = {}
          actions_conversion = {}
          states_conversion = {}
          questions_conversion = {}
          question_items_conversion = {}
          # formularios
          tracking_process_to_clone.tracking_forms.each do |form|
            form_cloned = form.dup
            form_cloned.tracking_process_id = tracking_process_cloned.id
            form_cloned.save!
            forms_conversion[form.id] = form_cloned.id
            # acciones
            form.tracking_form_actions.each do |action|
              action_cloned = action.dup
              action_cloned.tracking_form_id = form_cloned.id
              action_cloned.save!
              actions_conversion[action.id] = action_cloned.id
              # notificaciones
              action.tracking_form_notifications.each do |notification|
                notification_cloned = notification.dup
                notification_cloned.tracking_form_action_id = actions_conversion[notification.tracking_form_action_id]
                notification_cloned.save!
              end
            end
            # estados de seguimiento
            form.tracking_form_states.each do |state|
              state_cloned = state.dup
              state_cloned.tracking_form_id = form_cloned.id
              state_cloned.save!
              states_conversion[state.id] = state_cloned.id
              # notificaciones
              state.tracking_state_notifications.each do |notification|
                notification_cloned = notification.dup
                notification_cloned.tracking_form_state_id = states_conversion[notification.tracking_form_state_id]
                notification_cloned.save!
              end
            end
            # preguntas
            form.tracking_form_groups.each do |group|
              group_cloned = group.dup
              group_cloned.tracking_form_id = form_cloned.id
              group_cloned.save!
              group.tracking_form_questions.each do |question|
                question_cloned = question.dup
                question_cloned.tracking_form_group_id = group_cloned.id
                question_cloned.save!
                questions_conversion[question.id] = question_cloned.id
                question.tracking_form_items.each do |item|
                  item_cloned = item.dup
                  item_cloned.tracking_form_question_id = question_cloned.id
                  item_cloned.save!
                  question_items_conversion[item.id] = item_cloned.id
                end
              end
            end
          end

          if clone_config[:data]
            include_tracking = clone_config[:data][:include_tracking]
            exclude_finished_tracking = clone_config[:data][:exclude_finished_tracking]
            just_filled = clone_config[:data][:just_filled]
            # clonación de datos
            action_to_clone = TrackingFormAction.find(clone_config[:data][:action_id])
            action_to_clone_log = if just_filled
                                    action_to_clone.tracking_form.tracking_form_actions.where('tracking_form_actions.position < ?', action_to_clone.position).last
                                    #tracking_process_to_clone.tracking_form_actions.where('tracking_form_actions.position < ?', action_to_clone.position).first
                                  else
                                    action_to_clone
                                  end
            is_last_form_action = action_to_clone.tracking_form.tracking_form_actions.last == action_to_clone
            is_last_form = tracking_process_to_clone.tracking_forms.last == action_to_clone.tracking_form
            form_to_clone_instances = if just_filled || !is_last_form_action || is_last_form
                                        action_to_clone.tracking_form
                                      else
                                        tracking_process_cloned.tracking_forms.where('position > ?', action_to_clone.tracking_form.position).first
                                      end
            # participantes
            participants_conversion = {}
            tracking_process_to_clone.tracking_participants.each do |participant|
              participant_cloned = participant.dup
              participant_cloned.tracking_process_id = tracking_process_cloned.id
              participant_cloned.save!
              participants_conversion[participant.id] = participant_cloned.id
            end
            # Instancias
            tracking_process_to_clone.tracking_forms.each do |form|
              form.tracking_form_instances.each do |instance|
                cloned_instance = instance.dup
                cloned_instance.tracking_form_id = forms_conversion[form.id]
                blank_instance = action_to_clone.tracking_form.position < form.position
                # se marca la instancia como 'no llenada (editable)' si así se configuró el punto de clonación
                if ((action_to_clone.tracking_form_id == form.id) && just_filled) || blank_instance
                  cloned_instance.filled = false
                  cloned_instance.done = false
                end
                cloned_instance.tracking_participant_id = participants_conversion[instance.tracking_participant_id]
                cloned_instance.save!
                # logs
                instance.tracking_form_logs.each do |log|
                  break if blank_instance || ((action_to_clone.tracking_form.id == form.id) && action_to_clone_log.nil?)
                  cloned_log = log.dup
                  cloned_log.tracking_form_instance_id = cloned_instance.id
                  cloned_log.tracking_form_action_id = actions_conversion[log.tracking_form_action_id]
                  cloned_log.save!
                  # rejection
                  reject = log.tracking_form_reject
                  next unless reject
                  cloned_reject = reject.dup
                  cloned_reject.tracking_form_log_id = cloned_log.id
                  cloned_reject.save!
                  break if (action_to_clone.tracking_form.id == form.id) && (log.tracking_form_action_id == action_to_clone_log.id)
                end
                # respuestas
                next if blank_instance
                instance.tracking_form_answers.each do |answer|
                  due_date = answer.tracking_form_due_date
                  traceable = answer.tracking_form_question.traceable? && due_date
                  next if traceable && exclude_finished_tracking && due_date.finished?
                  cloned_answer = answer.dup
                  cloned_answer.tracking_form_instance_id = cloned_instance.id
                  cloned_answer.tracking_form_question_id = questions_conversion[answer.tracking_form_question_id]
                  cloned_answer.save!
                  answer.tracking_form_values.each do |answer_value|
                    cloned_answer_value = answer_value.dup
                    cloned_answer_value.tracking_form_answer_id = cloned_answer.id
                    cloned_answer_value.tracking_form_item_id = question_items_conversion[answer_value.tracking_form_item_id]
                    cloned_answer_value.save!
                  end
                  next unless traceable
                  # compromiso
                  cloned_due_date = due_date.dup
                  cloned_due_date.tracking_form_answer_id = cloned_answer.id
                  cloned_due_date.save!
                  next unless include_tracking
                  # hitos de seguimiento
                  due_date.tracking_milestones.each do |milestone|
                    # solo los ya validados
                    break unless milestone.validated?
                    cloned_milestone = milestone.dup
                    cloned_milestone.tracking_form_due_date_id = cloned_due_date.id
                    cloned_milestone.tracking_form_state_id = states_conversion[milestone.tracking_form_state_id]
                    cloned_milestone.save!
                  end
                end
              end
              break if form_to_clone_instances.id == form.id
            end
          end
        end
        session[:tracking_processes_clone].delete(tracking_process_to_clone.id)
        flash[:success] = 'El proceso fue clonado correctamente.'
        redirect_to tracking_process_path tracking_process_cloned
      rescue Exception => exception
        #puts exception
        #puts exception.backtrace
        raise
        flash[:danger] = 'Upss... hubo un problema al ejecutar la clonación, intenta nuevamente y si vuelve a fallar llama a César!!'
        redirect_to tracking_process_show_clone_form_path(tracking_process_to_clone)
      end
    else
      flash[:danger] = 'Primero debe configurar la clonación'
      redirect_to tracking_process_show_clone_form_path(tracking_process_to_clone)
    end
  end

  private
  def verify_access_manage_module
    ct_module_trkn = CtModule.where(:cod => 'trkn', :active => true).first
    unless !admin_logged_in? && ct_module_trkn && ct_module_trkn.has_managers? && ct_module_trkn.ct_module_managers.where(:user_id => user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end
end
