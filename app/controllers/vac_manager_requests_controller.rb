class VacManagerRequestsController < ApplicationController
  include VacationsModule
  include VacsManagerModule
  include VacsRequestCrudModule
  include VacsRuleRecordCrudModule

  before_filter :authenticate_user
  before_filter :verify_i_am_manager

  def new
    @vac_request = VacRequest.new(user_id: params[:user_id])
    @back_to_search = (!params[:back_to_validation].blank? && params[:back_to_validation].to_i == 2)
  end

  def create
    request = create_request(params[:vac_request], params[:user_id], 'manager', params[:send_email])
    if request.id
      unless params[:benefits].blank?
        condition_vacations = []
        params[:benefits].each { |benefit| condition_vacations.push BpConditionVacation.find(benefit) }
        save_benefits_for(condition_vacations, request, 'manager')
      end

      render partial: 'vac_records_register/request_info',
             locals: {request: request,
                      editable: !request.pending?,
                      manager: true}
    else

      render partial: 'vac_records_register/request_new_form',
             locals: {request: request,
                      url_to_go: vac_manager_requests_create_path(request.user_id, params[:back_to_validation]),
                      manager: true}
    end
  end

  def edit
  end

  def update
  end

  def deactivate
  end
end