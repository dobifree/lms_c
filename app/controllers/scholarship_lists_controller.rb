class ScholarshipListsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /scholarship_lists
  # GET /scholarship_lists.json
  def index
    @scholarship_lists = ScholarshipList.all
  end

  # GET /scholarship_lists/1
  # GET /scholarship_lists/1.json
  def show
    @scholarship_list = ScholarshipList.find(params[:id])
  end

  # GET /scholarship_lists/new
  # GET /scholarship_lists/new.json
  def new
    @scholarship_list = ScholarshipList.new
  end

  # GET /scholarship_lists/1/edit
  def edit
    @scholarship_list = ScholarshipList.find(params[:id])
  end

  # POST /scholarship_lists
  # POST /scholarship_lists.json
  def create
    @scholarship_list = ScholarshipList.new(params[:scholarship_list])

    if @scholarship_list.save
      flash[:success] = 'Lista creada correctamente'
      redirect_to scholarship_lists_path
    else
      flash.now[:danger] = 'La Lista no pudo ser creada correctamente'
      render 'new'
    end
  end

  # PUT /scholarship_lists/1
  # PUT /scholarship_lists/1.json
  def update
    @scholarship_list = ScholarshipList.find(params[:id])
    if @scholarship_list.update_attributes(params[:scholarship_list])
      flash[:success] = 'Lista actualizada correctamente'
      redirect_to scholarship_lists_path
    else
      flash.now[:danger] = 'La Lista no pudo ser actualizada correctamente'
      render 'edit'
    end

  end

  # DELETE /scholarship_lists/1
  # DELETE /scholarship_lists/1.json
  def destroy
    @scholarship_list = ScholarshipList.find(params[:id])
    @scholarship_list.destroy
    flash[:success] = 'Lista eliminada correctamente'
    redirect_to scholarship_lists_path
  end
end
