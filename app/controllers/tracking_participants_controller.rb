class TrackingParticipantsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module


  # GET /tracking_participants
  # GET /tracking_participants.json
  def index
    @tracking_process = TrackingProcess.find(params[:tracking_process_id])
  end


  def report_status
    @tracking_process = TrackingProcess.find(params[:tracking_process_id])
    @chars_to_show = Characteristic.where(:mini_ficha => true).order(:orden_mini_ficha)
  end

  # GET /tracking_participants/1
  # GET /tracking_participants/1.json
  def show
    @tracking_participant = TrackingParticipant.find(params[:id])
  end

  # GET /tracking_participants/new
  # GET /tracking_participants/new.json
  def new
    tracking_process = TrackingProcess.find(params[:tracking_process_id])

    @tracking_participant = TrackingParticipant.new
    @tracking_participant.tracking_process = tracking_process


    @possible_attendants = User.where(:activo => true).each.map { |user| [user.codigo + ' - ' + user.comma_full_name, user.id] }
    @possible_subjects = User.where(:activo => true).each.map { |user| [user.codigo + ' - ' + user.comma_full_name, user.id] }

  end

  # GET /tracking_participants/1/edit
  def edit
    @tracking_participant = TrackingParticipant.find(params[:id])
    @possible_attendants = User.where(:activo => true).each.map { |user| [user.codigo + ' - ' + user.comma_full_name, user.id] }
  end

  # POST /tracking_participants
  # POST /tracking_participants.json
  def create
    @tracking_participant = TrackingParticipant.new(params[:tracking_participant])

    if @tracking_participant.save
      flash[:success] = 'El registro fue creado correctamente'
      redirect_to tracking_participants_path(@tracking_participant.tracking_process)
    else
      flash.now[:danger] = 'El registro no pudo ser creado correctamente'
      @possible_attendants = User.where(:activo => true).each.map { |user| [user.codigo + ' - ' + user.comma_full_name, user.id] }
      @possible_subjects = User.where(:activo => true).each.map { |user| [user.codigo + ' - ' + user.comma_full_name, user.id] }

      render 'new'
    end
  end

  # PUT /tracking_participants/1
  # PUT /tracking_participants/1.json
  def update
    @tracking_participant = TrackingParticipant.find(params[:id])


    if @tracking_participant.update_attributes(params[:tracking_participant])
      # reseteo
      # ya no se resetea porque ya se valida correctamente el cambio de evaluador.
      #reset_data(@tracking_participant)
      flash[:success] = 'El registro fue creado correctamente'
      redirect_to tracking_participants_path(@tracking_participant.tracking_process)
    else
      flash.now[:danger] = 'El registro no pudo ser actualizado correctamente'
      @possible_attendants = User.where(:activo => true).each.map { |user| [user.codigo + ' - ' + user.comma_full_name, user.id] }
      render 'edit'
    end
  end

  # DELETE /tracking_participants/1
  # DELETE /tracking_participants/1.json
  def destroy
    @tracking_participant = TrackingParticipant.find(params[:id])
    tracking_process = @tracking_participant.tracking_process
    @tracking_participant.destroy

    flash[:success] = 'El registro fue eliminado correctamente'
    redirect_to tracking_participants_path(tracking_process)
  end

  def reset
    @tracking_participant = TrackingParticipant.find(params[:id])
    reset_data(@tracking_participant)

    flash[:success] = 'El registro fue resetado correctamente'
    redirect_to tracking_participants_path(@tracking_participant.tracking_process)
  end


  private

  def verify_access_manage_module

    ct_module = CtModule.where(:cod => 'trkn', :active => true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where(:user_id => user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def reset_data(tracking_participant)
    # reseteo
    if tracking_participant.tracking_form_instances.any? && !tracking_participant.done?
      tracking_participant.tracking_form_instances.destroy_all
      first_tracking_form = tracking_participant.tracking_process.tracking_forms.first
      tracking_form_instance = tracking_participant.tracking_form_instances.where(:tracking_form_id => first_tracking_form.id).first
      unless tracking_form_instance
        tracking_form_instance = tracking_participant.tracking_form_instances.where(:tracking_form_id => first_tracking_form.id).first_or_initialize
        tracking_form_instance.filled = false
        tracking_form_instance.done = false
        tracking_form_instance.save
      end
    end
  end
end
