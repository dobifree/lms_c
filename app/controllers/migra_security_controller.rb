class MigraSecurityController < ApplicationController

  def paso_4
    #***primero cambiar tipo de characteristica de compañía, comuna a lista
    # 5 empresa
    # 13 comuna
    # 12 provincia

    list_chars = [5,13,12]

    list_chars.each do |lc|

      characteristic = Characteristic.find lc

      UserCharacteristic.where('characteristic_id = ?', lc).each do |uc|

        cv = characteristic.characteristic_values.where('value_string = ? ', uc.value_string).first

        if cv

          uc.characteristic_value_id = cv.id
          uc.save

        end


      end

      UserCharacteristicRecord.where('characteristic_id = ?', lc).each do |ucr|

        cv = characteristic.characteristic_values.where('value_string = ? ', ucr.value_string).first

        if cv

          ucr.characteristic_value_id = cv.id
          ucr.save

        end


      end

    end


  end

  def paso_5
    #hecho
    # asocia empresas a centros de costos, pero

    # primero se debe asociar characteristic empresa a centro de costos

    x = Array.new

    x.push ['3','BSCTY']
    x.push ['5','BSCTY']
    x.push ['8','BSCTY']
    x.push ['10','BSCTY']
    x.push ['50','BSCTY']
    x.push ['52','BSCTY']
    x.push ['54','BSCTY']
    x.push ['100','BSCTY']
    x.push ['104','BSCTY']
    x.push ['105','BSCTY']
    x.push ['107','BSCTY']
    x.push ['108','BSCTY']
    x.push ['109','BSCTY']
    x.push ['111','BSCTY']
    x.push ['115','BSCTY']
    x.push ['117','BSCTY']
    x.push ['118','BSCTY']
    x.push ['119','BSCTY']
    x.push ['120','BSCTY']
    x.push ['125','BSCTY']
    x.push ['126','BSCTY']
    x.push ['129','BSCTY']
    x.push ['131','BSCTY']
    x.push ['132','BSCTY']
    x.push ['133','BSCTY']
    x.push ['134','BSCTY']
    x.push ['136','BSCTY']
    x.push ['137','BSCTY']
    x.push ['140','BSCTY']
    x.push ['141','BSCTY']
    x.push ['144','BSCTY']
    x.push ['147','BSCTY']
    x.push ['153','BSCTY']
    x.push ['157','BSCTY']
    x.push ['158','BSCTY']
    x.push ['160','BSCTY']
    x.push ['161','BSCTY']
    x.push ['163','BSCTY']
    x.push ['164','BSCTY']
    x.push ['165','BSCTY']
    x.push ['170','BSCTY']
    x.push ['175','BSCTY']
    x.push ['200','BSCTY']
    x.push ['201','BSCTY']
    x.push ['202','BSCTY']
    x.push ['203','BSCTY']
    x.push ['204','BSCTY']
    x.push ['205','BSCTY']
    x.push ['206','BSCTY']
    x.push ['207','BSCTY']
    x.push ['208','BSCTY']
    x.push ['209','BSCTY']
    x.push ['234','BSCTY']
    x.push ['243','BSCTY']
    x.push ['250','BSCTY']
    x.push ['251','BSCTY']
    x.push ['252','BSCTY']
    x.push ['255','BSCTY']
    x.push ['258','BSCTY']
    x.push ['260','BSCTY']
    x.push ['261','BSCTY']
    x.push ['263','BSCTY']
    x.push ['264','BSCTY']
    x.push ['265','BSCTY']
    x.push ['270','BSCTY']
    x.push ['275','BSCTY']
    x.push ['279','BSCTY']
    x.push ['280','BSCTY']
    x.push ['282','BSCTY']
    x.push ['285','BSCTY']
    x.push ['286','BSCTY']
    x.push ['287','BSCTY']
    x.push ['288','BSCTY']
    x.push ['289','BSCTY']
    x.push ['295','BSCTY']
    x.push ['296','BSCTY']
    x.push ['300','BSCTY']
    x.push ['301','BSCTY']
    x.push ['304','BSCTY']
    x.push ['307','BSCTY']
    x.push ['400','BSCTY']
    x.push ['403','BSCTY']
    x.push ['404','BSCTY']
    x.push ['405','BSCTY']
    x.push ['406','BSCTY']
    x.push ['409','BSCTY']
    x.push ['410','BSCTY']
    x.push ['411','BSCTY']
    x.push ['412','BSCTY']
    x.push ['413','BSCTY']
    x.push ['415','BSCTY']
    x.push ['417','BSCTY']
    x.push ['418','BSCTY']
    x.push ['419','BSCTY']
    x.push ['420','BSCTY']
    x.push ['421','BSCTY']
    x.push ['424','BSCTY']
    x.push ['425','BSCTY']
    x.push ['426','BSCTY']
    x.push ['429','BSCTY']
    x.push ['430','BSCTY']
    x.push ['431','BSCTY']
    x.push ['432','BSCTY']
    x.push ['433','BSCTY']
    x.push ['434','BSCTY']
    x.push ['435','BSCTY']
    x.push ['436','BSCTY']
    x.push ['437','BSCTY']
    x.push ['440','BSCTY']
    x.push ['441','BSCTY']
    x.push ['444','BSCTY']
    x.push ['447','BSCTY']
    x.push ['448','BSCTY']
    x.push ['450','BSCTY']
    x.push ['453','BSCTY']
    x.push ['457','BSCTY']
    x.push ['460','BSCTY']
    x.push ['461','BSCTY']
    x.push ['463','BSCTY']
    x.push ['464','BSCTY']
    x.push ['465','BSCTY']
    x.push ['468','BSCTY']
    x.push ['469','BSCTY']
    x.push ['470','BSCTY']
    x.push ['475','BSCTY']
    x.push ['479','BSCTY']
    x.push ['480','BSCTY']
    x.push ['481','BSCTY']
    x.push ['483','BSCTY']
    x.push ['485','BSCTY']
    x.push ['486','BSCTY']
    x.push ['500','BSCTY']
    x.push ['503','BSCTY']
    x.push ['504','BSCTY']
    x.push ['505','BSCTY']
    x.push ['506','BSCTY']
    x.push ['507','BSCTY']
    x.push ['508','BSCTY']
    x.push ['510','BSCTY']
    x.push ['511','BSCTY']
    x.push ['512','BSCTY']
    x.push ['513','BSCTY']
    x.push ['515','BSCTY']
    x.push ['516','BSCTY']
    x.push ['517','BSCTY']
    x.push ['518','BSCTY']
    x.push ['519','BSCTY']
    x.push ['520','BSCTY']
    x.push ['521','BSCTY']
    x.push ['526','BSCTY']
    x.push ['528','BSCTY']
    x.push ['533','BSCTY']
    x.push ['534','BSCTY']
    x.push ['535','BSCTY']
    x.push ['536','BSCTY']
    x.push ['537','BSCTY']
    x.push ['538','BSCTY']
    x.push ['539','BSCTY']
    x.push ['540','BSCTY']
    x.push ['541','BSCTY']
    x.push ['543','BSCTY']
    x.push ['544','BSCTY']
    x.push ['546','BSCTY']
    x.push ['547','BSCTY']
    x.push ['548','BSCTY']
    x.push ['550','BSCTY']
    x.push ['558','BSCTY']
    x.push ['560','BSCTY']
    x.push ['561','BSCTY']
    x.push ['563','BSCTY']
    x.push ['564','BSCTY']
    x.push ['565','BSCTY']
    x.push ['570','BSCTY']
    x.push ['575','BSCTY']
    x.push ['580','BSCTY']
    x.push ['590','BSCTY']
    x.push ['591','BSCTY']
    x.push ['592','BSCTY']
    x.push ['593','BSCTY']
    x.push ['619','VDCTY']
    x.push ['AS0110','ASCTY']
    x.push ['AS0111','ASCTY']
    x.push ['CN001','SSC01']
    x.push ['CS0036','CSCTY']
    x.push ['CS0300','CSCTY']
    x.push ['CS0310','CSCTY']
    x.push ['CS0320','CSCTY']
    x.push ['CS0350','CSCTY']
    x.push ['CS0380','CSCTY']
    x.push ['CS0390','CSCTY']
    x.push ['CS1000','CSCTY']
    x.push ['CS1010','CSCTY']
    x.push ['CS1020','CSCTY']
    x.push ['CS1030','CSCTY']
    x.push ['CS1050','CSCTY']
    x.push ['CS400','CSCTY']
    x.push ['FM0110','FMCTY']
    x.push ['FM0112','FMCTY']
    x.push ['FM0202','FMCTY']
    x.push ['FM0506','FMCTY']
    x.push ['FM0606','FMCTY']
    x.push ['FM0609','FMCTY']
    x.push ['FM0610','FMCTY']
    x.push ['FM0611','FMCTY']
    x.push ['FM0612','FMCTY']
    x.push ['FM0613','FMCTY']
    x.push ['FS1010','FSCTY']
    x.push ['FS2010','FSCTY']
    x.push ['FS2220','FSCTY']
    x.push ['FS2230','FSCTY']
    x.push ['FS2240','FSCTY']
    x.push ['FS2250','FSCTY']
    x.push ['FS2260','FSCTY']
    x.push ['FS2270','FSCTY']
    x.push ['FS2280','FSCTY']
    x.push ['FS2290','FSCTY']
    x.push ['FS2294','FSCTY']
    x.push ['FS2295','FSCTY']
    x.push ['FS2296','FSCTY']
    x.push ['FS2516','FSCTY']
    x.push ['FS2517','FSCTY']
    x.push ['FS2518','FSCTY']
    x.push ['FS2519','FSCTY']
    x.push ['FS2520','FSCTY']
    x.push ['FS3010','FSCTY']
    x.push ['FS3310','FSCTY']
    x.push ['FS4001','FSCTY']
    x.push ['FS4010','FSCTY']
    x.push ['FS4110','FSCTY']
    x.push ['FS4210','FSCTY']
    x.push ['FS4310','FSCTY']
    x.push ['FS5000','FSCTY']
    x.push ['FS5008','FSCTY']
    x.push ['FS5009','FSCTY']
    x.push ['FS5110','FSCTY']
    x.push ['FS5210','FSCTY']
    x.push ['FS9099','FSCTY']
    x.push ['GL0101','GSCTY']
    x.push ['GL0102','GSCTY']
    x.push ['GL0105','GSCTY']
    x.push ['GL0106','GSCTY']
    x.push ['GL0107','GSCTY']
    x.push ['GL0108','GSCTY']
    x.push ['GL0110','GSCTY']
    x.push ['GL0111','GSCTY']
    x.push ['GL0201','GSCTY']
    x.push ['GL0401','GSCTY']
    x.push ['GL0501','GSCTY']
    x.push ['GL0701','GSCTY']
    x.push ['GL0702','GSCTY']
    x.push ['GL0703','GSCTY']
    x.push ['GL0704','GSCTY']
    x.push ['GR0090','GRCTY']
    x.push ['GR0100','GRCTY']
    x.push ['GR0105','GRCTY']
    x.push ['GR0110','GRCTY']
    x.push ['GR0120','GRCTY']
    x.push ['GR0125','GRCTY']
    x.push ['IN001','INVST']
    x.push ['IN002','INVST']
    x.push ['IN003','INVST']
    x.push ['IN005','INVST']
    x.push ['IN006','INVST']
    x.push ['IN008','INVST']
    x.push ['IS0100','ISCTY']
    x.push ['IS0300','ISCTY']
    x.push ['IS0500','ISCTY']
    x.push ['ISS005','ISC07']
    x.push ['ISS145','ISC07']
    x.push ['ISS150','ISC07']
    x.push ['ISS155','ISC07']
    x.push ['ISS160','ISC07']
    x.push ['ISS165','ISC07']
    x.push ['ISS170','ISC07']
    x.push ['IV001','INVST']
    x.push ['IV002','INVST']
    x.push ['IV003','INVST']
    x.push ['IV004','INVST']
    x.push ['IV005','INVST']
    x.push ['IV006','INVST']
    x.push ['IV007','INVST']
    x.push ['IV008','INVST']
    x.push ['IV613','INVST']
    x.push ['MS0200','MSCTY']
    x.push ['MS0205','MSCTY']
    x.push ['MS0213','MSCTY']
    x.push ['MS0214','MSCTY']
    x.push ['MS0215','MSCTY']
    x.push ['MS0225','MSCTY']
    x.push ['MS0230','MSCTY']
    x.push ['MS0245','MSCTY']
    x.push ['MS0255','MSCTY']
    x.push ['MS0275','MSCTY']
    x.push ['MS0292','MSCTY']
    x.push ['MS0300','MSCTY']
    x.push ['MS0340','MSCTY']
    x.push ['MS0400','MSCTY']
    x.push ['MS0421','MSCTY']
    x.push ['MS0422','MSCTY']
    x.push ['MS0500','MSCTY']
    x.push ['MS0510','MSCTY']
    x.push ['MS0520','MSCTY']
    x.push ['MS0550','MSCTY']
    x.push ['MS0600','MSCTY']
    x.push ['MS0620','MSCTY']
    x.push ['MS0700','MSCTY']
    x.push ['MS0710','MSCTY']
    x.push ['SS0100','SSC01']
    x.push ['SS0300','SSCTY']
    x.push ['SSC01','SSC01']
    x.push ['T1010001','TSCTY']
    x.push ['T2010001','TSCTY']
    x.push ['T2020001','TSCTY']
    x.push ['T2020002','TSCTY']
    x.push ['T2020003','TSCTY']
    x.push ['T2020004','TSCTY']
    x.push ['T2030001','TSCTY']
    x.push ['T2030002','TSCTY']
    x.push ['T2030003','TSCTY']
    x.push ['T2030004','TSCTY']
    x.push ['T2030005','TSCTY']
    x.push ['T2040001','TSCTY']
    x.push ['T3010001','TSCTY']
    x.push ['T3020001','TSCTY']
    x.push ['T3030001','TSCTY']
    x.push ['T3050001','TSCTY']
    x.push ['T3050002','TSCTY']
    x.push ['T3060001','TSCTY']
    x.push ['T3060002','TSCTY']
    x.push ['T3060006','TSCTY']
    x.push ['T3060008','TSCTY']
    x.push ['T4010001','TSCTY']
    x.push ['T4020001','TSCTY']
    x.push ['T4020010','TSCTY']
    x.push ['T4020214','TSCTY']
    x.push ['T4020876','TSCTY']
    x.push ['T4030001','TSCTY']
    x.push ['T4030155','TSCTY']
    x.push ['T4040001','TSCTY']
    x.push ['T4040396','TSCTY']
    x.push ['T4040614','TSCTY']
    x.push ['T4040801','TSCTY']
    x.push ['T4040923','TSCTY']
    x.push ['T4040928','TSCTY']
    x.push ['T4040948','TSCTY']
    x.push ['T4040993','TSCTY']
    x.push ['T5020001','TSCTY']
    x.push ['T5020061','TSCTY']
    x.push ['T5020106','TSCTY']
    x.push ['T5020403','TSCTY']
    x.push ['T5020601','TSCTY']
    x.push ['T5020602','TSCTY']
    x.push ['T5020606','TSCTY']
    x.push ['T5020625','TSCTY']
    x.push ['T5020652','TSCTY']
    x.push ['T5020805','TSCTY']
    x.push ['T5020808','TSCTY']
    x.push ['T5020810','TSCTY']
    x.push ['T5020815','TSCTY']
    x.push ['T5020831','TSCTY']
    x.push ['T5020855','TSCTY']
    x.push ['T5020889','TSCTY']
    x.push ['T5020891','TSCTY']
    x.push ['T5020918','TSCTY']
    x.push ['T5020932','TSCTY']
    x.push ['T5020933','TSCTY']
    x.push ['T5020938','TSCTY']
    x.push ['T5020941','TSCTY']
    x.push ['T5020942','TSCTY']
    x.push ['T5020944','TSCTY']
    x.push ['T5020949','TSCTY']
    x.push ['T5020954','TSCTY']
    x.push ['T5020995','TSCTY']
    x.push ['T5030001','TSCTY']
    x.push ['T5030011','TSCTY']
    x.push ['T5030013','TSCTY']
    x.push ['T5030016','TSCTY']
    x.push ['T5030018','TSCTY']
    x.push ['T5030037','TSCTY']
    x.push ['T5030039','TSCTY']
    x.push ['T5030069','TSCTY']
    x.push ['T5030079','TSCTY']
    x.push ['T5030153','TSCTY']
    x.push ['T5030335','TSCTY']
    x.push ['T5030360','TSCTY']
    x.push ['T5030702','TSCTY']
    x.push ['T5030709','TSCTY']
    x.push ['T5030712','TSCTY']
    x.push ['T5030716','TSCTY']
    x.push ['T5030719','TSCTY']
    x.push ['T5030732','TSCTY']
    x.push ['T5030737','TSCTY']
    x.push ['T5030739','TSCTY']
    x.push ['T5030743','TSCTY']
    x.push ['T5030971','TSCTY']
    x.push ['T5040001','TSCTY']
    x.push ['T5040002','TSCTY']
    x.push ['T5040005','TSCTY']
    x.push ['T5040009','TSCTY']
    x.push ['T5040041','TSCTY']
    x.push ['T5040042','TSCTY']
    x.push ['T5040190','TSCTY']
    x.push ['T5040388','TSCTY']
    x.push ['T5040939','TSCTY']
    x.push ['T5050001','TSCTY']
    x.push ['T5050067','TSCTY']
    x.push ['T5050192','TSCTY']
    x.push ['T5050830','TSCTY']
    x.push ['T5050836','TSCTY']
    x.push ['T5050844','TSCTY']
    x.push ['T5050881','TSCTY']
    x.push ['T5050940','TSCTY']
    x.push ['T6010001','TSCTY']
    x.push ['T6020001','TSCTY']
    x.push ['T6020002','TSCTY']
    x.push ['T6020361','TSCTY']
    x.push ['T6020363','TSCTY']
    x.push ['T6030001','TSCTY']
    x.push ['T6030002','TSCTY']
    x.push ['T6030004','TSCTY']
    x.push ['T6040001','TSCTY']
    x.push ['T7010001','RSCTY']
    x.push ['T7020001','RSCTY']
    x.push ['T7020002','RSCTY']
    x.push ['T7020004','RSCTY']
    x.push ['T7020005','RSCTY']
    x.push ['T7030001','RSCTY']
    x.push ['T9010001','TSCTY']
    x.push ['T9020001','TSCTY']
    x.push ['VD0500','VDCTY']
    x.push ['VD0502','VDCTY']
    x.push ['VD0505','VDCTY']
    x.push ['VD0506','VDCTY']
    x.push ['VD0507','VDCTY']
    x.push ['VD0508','VDCTY']
    x.push ['VD0510','VDCTY']
    x.push ['VD0511','VDCTY']
    x.push ['VD0512','VDCTY']
    x.push ['VD0514','VDCTY']
    x.push ['VD0515','VDCTY']
    x.push ['VD0516','VDCTY']
    x.push ['VD0519','VDCTY']
    x.push ['VD0521','VDCTY']
    x.push ['VD0534','VDCTY']
    x.push ['VD0537','VDCTY']
    x.push ['VD0539','VDCTY']
    x.push ['VD0540','VDCTY']
    x.push ['VD0541','VDCTY']
    x.push ['VD0542','VDCTY']
    x.push ['VD0543','VDCTY']
    x.push ['VD0544','VDCTY']
    x.push ['VD0546','VDCTY']
    x.push ['VD0550','VDCTY']
    x.push ['VD0551','VDCTY']
    x.push ['VD0554','VDCTY']
    x.push ['VD0570','VDCTY']
    x.push ['VD0571','VDCTY']
    x.push ['VD0573','SACTY']
    x.push ['VD0574','SACTY']
    x.push ['VD0575','SACTY']
    x.push ['VD0576','VDCTY']
    x.push ['VD0577','VDCTY']
    x.push ['VD0578','VDCTY']
    x.push ['VD0579','VDCTY']
    x.push ['VD0580','VDCTY']
    x.push ['VD0581','VDCTY']
    x.push ['VD0582','SACTY']
    x.push ['VD0583','SACTY']
    x.push ['VD0584','VDCTY']
    x.push ['VD0587','VDCTY']
    x.push ['VD0588','VDCTY']
    x.push ['VD0590','VDCTY']
    x.push ['VD0598','VDCTY']
    x.push ['VD0599','VDCTY']
    x.push ['VD0603','SACTY']
    x.push ['VD0610','VDCTY']
    x.push ['VD0611','VDCTY']
    x.push ['VD0614','VDCTY']
    x.push ['VD0615','VDCTY']
    x.push ['VD0617','VDCTY']
    x.push ['VD0618','VDCTY']
    x.push ['VD0619','VDCTY']
    x.push ['VD0622','VDCTY']
    x.push ['VD0623','VDCTY']
    x.push ['VD0624','VDCTY']
    x.push ['VD0625','VDCTY']
    x.push ['VD0626','VDCTY']
    x.push ['VD0630','VDCTY']
    x.push ['VD0631','VDCTY']
    x.push ['VD0633','VDCTY']
    x.push ['VD0634','VDCTY']
    x.push ['VD0635','VDCTY']
    x.push ['VD0637','VDCTY']
    x.push ['VD0639','VDCTY']
    x.push ['VD0640','VDCTY']
    x.push ['VD0641','VDCTY']
    x.push ['VD0642','VDCTY']
    x.push ['VD0650','VDCTY']
    x.push ['VD0651','VDCTY']
    x.push ['VI020','INVST']
    x.push ['VI031','INVST']
    x.push ['VI050','INVST']
    x.push ['VI070','INVST']
    x.push ['VS004','VSCTY']
    x.push ['VS0110','VSCTY']
    x.push ['VS0301','VSCTY']
    x.push ['VS0501','VSCTY']
    x.push ['VS0502','VSCTY']
    x.push ['VS0503','VSCTY']
    x.push ['VS0504','VSCTY']
    x.push ['VS0505','VSCTY']
    x.push ['VS0601','VSCTY']
    x.push ['VS0603','VSCTY']
    x.push ['VS0612','VSCTY']
    x.push ['VS0700','VSCTY']

    characteristic = Characteristic.find 5

    x.each do |d|

      jcc = JCostCenter.find_by_cc_id d[0]

      if jcc

        cv = CharacteristicValue.where('characteristic_id = 5 AND value_string_2 = ?',d[1]).first

        if cv

          jcc_characteristic = jcc.jcc_characteristics.build
          jcc_characteristic.characteristic = characteristic

          jcc_characteristic.characteristic_value_id = cv.id

          jcc_characteristic.save

        end
      end

    end


  end

  def paso_6

    # asociar characteristic 'Empresa' de user para j_jobs

    #*** primero crear chars para puesto
    # Nivel :List
    # Clasificación del Puesto :List
    # Función del Puesto :List

    #crea los puestos

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    file_name = '/var/www/remote-storage/batch_processing/6_puestos_security.xlsx'

    #file_name = '/Users/alejo/Documents/Current/CT/Requirements/Security/PeopleSoft/paso_a_prod/6_puestos_security.xlsx'

    if File.exist? file_name

      archivo = RubyXL::Parser.parse file_name

      characteristic_company = Characteristic.find 5

      jj_characteristic_cargo = JjCharacteristic.find_by_name 'Cargo'
      jj_characteristic_nivel = JjCharacteristic.find_by_name 'Nivel'
      jj_characteristic_cp = JjCharacteristic.find_by_name 'Clasificación del Puesto'
      jj_characteristic_fp = JjCharacteristic.find_by_name 'Función del Puesto'


      archivo.worksheets[0].extract_data.each_with_index do |fila, index|

        next if index <= 0

        j = JJob.new

        j.id = fila[0]
        j.j_code = fila[2]
        j.name = fila[3]
        j.version = 1
        j.from_date = lms_time

        jpb = JPayBand.find_by_name fila[5]

        if jpb
          j.j_pay_band = jpb
        else
          #j.j_pay_band_id = 1 # cuando las bandas estén correctas esto no debe estar
        end

        if j.save

          cv = characteristic_company.characteristic_values.where('value_string_2 = ?', fila[1]).first

          if cv

            jc = j.j_job_characteristics.build
            jc.characteristic = characteristic_company
            jc.characteristic_value_id = cv.id
            jc.save

          end

          cv = jj_characteristic_nivel.jj_characteristic_values.where('value_string = ?', fila[4]).first

          if cv

            jc = j.j_job_jj_characteristics.build
            jc.jj_characteristic = jj_characteristic_nivel
            jc.jj_characteristic_value_id = cv.id
            jc.save

          end

          cv = jj_characteristic_cp.jj_characteristic_values.where('value_string = ?', fila[6]).first

          if cv

            jc = j.j_job_jj_characteristics.build
            jc.jj_characteristic = jj_characteristic_cp
            jc.jj_characteristic_value_id = cv.id
            jc.save

          end

          cv = jj_characteristic_fp.jj_characteristic_values.where('value_string = ?', fila[7]).first

          if cv

            jc = j.j_job_jj_characteristics.build
            jc.jj_characteristic = jj_characteristic_fp
            jc.jj_characteristic_value_id = cv.id
            jc.save

          end



        end


      end

    end


  end

  def paso_7

    #asocia los puestos a los centros de costo

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    file_name = '/var/www/remote-storage/batch_processing/7_puestos_cc.xlsx'

    #file_name = '/Users/alejo/Documents/Current/CT/Requirements/Security/PeopleSoft/paso_a_prod/7_puestos_cc.xlsx'

    if File.exist? file_name

      archivo = RubyXL::Parser.parse file_name

      archivo.worksheets[0].extract_data.each_with_index do |fila, index|

        next if index <= 0

        j_job = JJob.find fila[1]
        j_cost_center = JCostCenter.find_by_cc_id fila[0]

        if j_job && j_cost_center

          jcc = j_job.j_job_ccs.build
          jcc.j_cost_center = j_cost_center
          jcc.save

        end

      end

    end

  end

  def paso_8

    #asocia usuarios a puestos

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    file_name = '/var/www/remote-storage/batch_processing/8_empleados_puestos.xlsx'

    #file_name = '/Users/alejo/Documents/Current/CT/Requirements/Security/PeopleSoft/paso_a_prod/8_empleados_puestos.xlsx'

    if File.exist? file_name

      characteristic_p = Characteristic.where('data_type_id = ?',13).first

      admin = Admin.first

      archivo = RubyXL::Parser.parse file_name

      archivo.worksheets[0].extract_data.each_with_index do |fila, index|

        next if index <= 0

        user = User.find_by_codigo fila[0]

        if user

          j_job = JJob.find fila[1]

          if j_job

            user.j_job = j_job

            if user.save

              user_j_job = user.user_j_jobs.build
              user_j_job.j_job = j_job
              user_j_job.from_date = lms_time.to_date
              user_j_job.save

              if characteristic_p

                user.update_user_characteristic(characteristic_p, user.j_job.name, user_j_job.from_date.to_datetime, nil, nil, admin, admin, @company)

              end

            end

          end

        end

      end

    end

  end

  def paso_9

    # primero se deben crear chars para usuario
    # Ubicación Geográfica :list
    # País de Origen :list
    # SUBASO :float
    # MONEDA SUBASO :list
    # ubicación
    # Tipo Contrato :list
    # Sexo :list
    # Estado Civil :list
    # Fecha Estado Civil :date
    # Nivel Educacional_ :list
    # Fecha Ingreso Empresa :date
    # Fecha Inicio Vacaciones Progresivas :date
    # Fecha Reconocimiento Vacaciones Progresivas :date
    # Rol :list

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    file_name = '/var/www/remote-storage/batch_processing/9_empleados.xlsx'

    #file_name = '/Users/alejo/Documents/Current/CT/Requirements/Security/PeopleSoft/paso_a_prod/9_empleados.xlsx'

    if File.exist? file_name

      characteristic_ubic_geof = Characteristic.find_by_nombre 'Ubicación Geográfica'
      characteristic_pais_origen = Characteristic.find_by_nombre 'País de Origen'
      characteristic_subaso = Characteristic.find_by_nombre 'SUBASO'
      characteristic_moneda_subaso = Characteristic.find_by_nombre 'MONEDA SUBASO'
      # ubicación
      characteristic_tipo_contrato = Characteristic.find_by_nombre 'Tipo Contrato'
      characteristic_sexo = Characteristic.find_by_nombre 'Sexo'
      characteristic_estado_civil = Characteristic.find_by_nombre 'Estado Civil'
      characteristic_fecha_estado_civil = Characteristic.find_by_nombre 'Fecha Estado Civil'
      characteristic_nivel_educacional_ = Characteristic.find_by_nombre 'Nivel Educacional_'
      characteristic_fecha_ingreso_empresa = Characteristic.find_by_nombre 'Fecha Ingreso Empresa'
      characteristic_fecha_inicio_vac_progresivas = Characteristic.find_by_nombre 'Fecha Inicio Vacaciones Progresivas'
      characteristic_fecha_recono_vac_progresivas = Characteristic.find_by_nombre 'Fecha Reconocimiento Vacaciones Progresivas'
      characteristic_rol = Characteristic.find_by_nombre 'Rol'

      admin = Admin.first

      archivo = RubyXL::Parser.parse file_name

      archivo.worksheets[0].extract_data.each_with_index do |fila, index|

        next if index <= 0

        user = User.find_by_codigo fila[0]

        if user

          # ubicación geográfica
          cv = characteristic_ubic_geof.characteristic_values.where('value_string = ?', fila[1]).first
          user.update_user_characteristic(characteristic_ubic_geof, cv.id, lms_time, nil, nil, admin, admin, @company) if cv

          # país de origen
          cv = characteristic_pais_origen.characteristic_values.where('value_string = ?', fila[2]).first
          user.update_user_characteristic(characteristic_pais_origen, cv.id, lms_time, nil, nil, admin, admin, @company) if cv

          # centro de costos
          jcc = JCostCenter.find_by_cc_id fila[3]

          if jcc
            user.j_cost_center = jcc
            user.save
          end

          # SUBASO
          user.update_user_characteristic(characteristic_subaso, fila[4], lms_time, nil, nil, admin, admin, @company)

          # moneda subaso
          cv = characteristic_moneda_subaso.characteristic_values.where('value_string = ?', fila[5]).first
          user.update_user_characteristic(characteristic_moneda_subaso, cv.id, lms_time, nil, nil, admin, admin, @company) if cv

          # ubicación


          # tipo contrato
          cv = characteristic_tipo_contrato.characteristic_values.where('value_string = ?', fila[7]).first
          user.update_user_characteristic(characteristic_tipo_contrato, cv.id, lms_time, nil, nil, admin, admin, @company) if cv

          # sexo
          cv = characteristic_sexo.characteristic_values.where('value_string = ?', fila[8]).first
          user.update_user_characteristic(characteristic_sexo, cv.id, lms_time, nil, nil, admin, admin, @company) if cv

          # estado civil
          cv = characteristic_estado_civil.characteristic_values.where('value_string = ?', fila[9]).first
          user.update_user_characteristic(characteristic_estado_civil, cv.id, lms_time, nil, nil, admin, admin, @company) if cv

          # fecha estado civil, en el excel hay que cambiar el formato a fecha yyyy-mm-dd
          user.update_user_characteristic(characteristic_fecha_estado_civil, fila[10], lms_time, nil, nil, admin, admin, @company)

          # nivel educacional_
          cv = characteristic_nivel_educacional_.characteristic_values.where('value_string = ?', fila[11]).first
          user.update_user_characteristic(characteristic_nivel_educacional_, cv.id, lms_time, nil, nil, admin, admin, @company) if cv

          # fecha ingreso empresa, en el excel hay que cambiar el formato a fecha yyyy-mm-dd
          user.update_user_characteristic(characteristic_fecha_ingreso_empresa, fila[12], lms_time, nil, nil, admin, admin, @company)

          user.from_date = fila[12]

          # fecha ingreso empresa, en el excel hay que cambiar el formato a fecha yyyy-mm-dd
          unless fila[13] == 'SIN INFORMACION'
            user.update_user_characteristic(characteristic_fecha_inicio_vac_progresivas, fila[13], lms_time, nil, nil, admin, admin, @company)
          end

          # fecha ingreso empresa, en el excel hay que cambiar el formato a fecha yyyy-mm-dd
          unless fila[14] == 'SIN INFORMACION'
            user.update_user_characteristic(characteristic_fecha_recono_vac_progresivas, fila[14], lms_time, nil, nil, admin, admin, @company)
          end

          # rol
          cv = characteristic_rol.characteristic_values.where('value_string = ?', fila[15]).first
          user.update_user_characteristic(characteristic_rol, cv.id, lms_time, nil, nil, admin, admin, @company) if cv

          user.save

        end

      end

    end

  end

end
