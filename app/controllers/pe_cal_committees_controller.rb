class PeCalCommitteesController < ApplicationController

  include UsersHelper

  before_filter :get_data_1, only: [:list, :create]
  before_filter :get_data_2, only: [:set_manager, :unset_manager, :destroy]
  before_filter :validate_manager, only: [:list, :create, :destroy]

  def list

    @pe_cal_committees = @pe_cal_session.pe_cal_committees_ordered

    @user_search = User.new(params[:user])

    @users_search = search_normal_users(@user_search.apellidos, @user_search.nombre)


  end

  def create

    pe_cal_committee = PeCalCommittee.new(params[:pe_cal_committee])
    pe_cal_committee.pe_process = @pe_process
    pe_cal_committee.pe_cal_session = @pe_cal_session

    pe_cal_committee.save

    flash[:success] =  t('activerecord.success.model.pe_cal_committee.create_ok')
    redirect_to pe_cal_committees_list_path(@pe_cal_session)


  end

  def set_manager
    @pe_cal_committee.manager = true
    @pe_cal_committee.save
    redirect_to pe_cal_committees_list_path(@pe_cal_session)
  end

  def unset_manager
    @pe_cal_committee.manager = false
    @pe_cal_committee.save
    redirect_to pe_cal_committees_list_path(@pe_cal_session)
  end

  def destroy

    @pe_cal_committee.destroy
    flash[:success] =  t('activerecord.success.model.pe_cal_committee.delete_ok')

    redirect_to pe_cal_committees_list_path(@pe_cal_session)


  end


  private

    def get_data_1
      @pe_cal_session = PeCalSession.find(params[:pe_cal_session_id])
      if @pe_cal_session.pe_evaluation
        @pe_evaluation = @pe_cal_session.pe_evaluation
        @pe_process = @pe_evaluation.pe_process
      else
        @pe_process = @pe_cal_session.pe_process
      end

    end

    def get_data_2
      @pe_cal_committee = PeCalCommittee.find(params[:id])
      @pe_cal_session = @pe_cal_committee.pe_cal_session
      if @pe_cal_session.pe_evaluation
        @pe_evaluation = @pe_cal_session.pe_evaluation
        @pe_process = @pe_evaluation.pe_process
      else
        @pe_process = @pe_cal_session.pe_process
      end
    end

    def validate_manager
      unless @pe_process.pe_managers.where('user_id = ?', user_connected.id).count > 0
        flash[:danger] = t('security.no_access_generic')
        redirect_to root_path
      end
    end


end
