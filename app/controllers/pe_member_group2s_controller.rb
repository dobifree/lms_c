class PeMemberGroup2sController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def massive_charge

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if params[:reemplazar_todo][:yes] == '1'

        @pe_process.pe_members.each do |pe_member_evaluated|

          pe_member_evaluated.pe_member_group2.destroy if pe_member_evaluated.pe_member_group2

        end

      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')

          user = User.where('codigo = ? AND activo = ?', datos[0], true).first

          if user

            pe_member_evaluated = @pe_process.pe_members.where('user_id = ?', user.id).first

            if pe_member_evaluated

              datos[1].delete!("\n") if datos[1]
              datos[1].delete!("\r") if datos[1]

              if datos[1].blank?

                if pe_member_evaluated.pe_member_group2
                  pe_member_evaluated.pe_member_group2.destroy
                end

              else

                pe_group2 = @pe_process.pe_group2s.where('name = ?', datos[1]).first

                unless pe_group2
                  pe_group2 = @pe_process.pe_group2s.build
                  pe_group2.name = datos[1]
                  pe_group2.save
                end

                if pe_group2

                  if pe_member_evaluated.pe_member_group2
                    pe_member_group2 = pe_member_evaluated.pe_member_group2
                  else
                    pe_member_group2 = PeMemberGroup2.new
                  end

                  pe_member_group2.pe_member = pe_member_evaluated
                  pe_member_group2.pe_group2 = pe_group2
                  pe_member_group2.pe_process = @pe_process
                  pe_member_group2.save


                else

                  @lineas_error.push num_linea
                  @lineas_error_detalle.push ('codigo: '+datos[1]).force_encoding('UTF-8')
                  @lineas_error_messages.push ['El grupo está vacío']

                end

              end




            else

              @lineas_error.push num_linea
              @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
              @lineas_error_messages.push ['No existe el participante']

            end

          else

            @lineas_error.push num_linea
            @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
            @lineas_error_messages.push ['El usuario no existe o no está activo']


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.pe_member_evaluated.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.pe_member_evaluated.carga_masiva_error') if @lineas_error.length > 0

    end


  end

  # GET /pe_member_group2s
  # GET /pe_member_group2s.json
  def index
    @pe_member_group2s = PeMemberGroup2.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_member_group2s }
    end
  end

  # GET /pe_member_group2s/1
  # GET /pe_member_group2s/1.json
  def show
    @pe_member_group2 = PeMemberGroup2.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_member_group2 }
    end
  end

  # GET /pe_member_group2s/new
  # GET /pe_member_group2s/new.json
  def new
    @pe_member_group2 = PeMemberGroup2.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pe_member_group2 }
    end
  end

  # GET /pe_member_group2s/1/edit
  def edit
    @pe_member_group2 = PeMemberGroup2.find(params[:id])
  end

  # POST /pe_member_group2s
  # POST /pe_member_group2s.json
  def create
    @pe_member_group2 = PeMemberGroup2.new(params[:pe_member_group2])

    respond_to do |format|
      if @pe_member_group2.save
        format.html { redirect_to @pe_member_group2, notice: 'Pe member group2 was successfully created.' }
        format.json { render json: @pe_member_group2, status: :created, location: @pe_member_group2 }
      else
        format.html { render action: "new" }
        format.json { render json: @pe_member_group2.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pe_member_group2s/1
  # PUT /pe_member_group2s/1.json
  def update
    @pe_member_group2 = PeMemberGroup2.find(params[:id])

    respond_to do |format|
      if @pe_member_group2.update_attributes(params[:pe_member_group2])
        format.html { redirect_to @pe_member_group2, notice: 'Pe member group2 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pe_member_group2.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pe_member_group2s/1
  # DELETE /pe_member_group2s/1.json
  def destroy
    @pe_member_group2 = PeMemberGroup2.find(params[:id])
    @pe_member_group2.destroy

    respond_to do |format|
      format.html { redirect_to pe_member_group2s_url }
      format.json { head :no_content }
    end
  end
end
