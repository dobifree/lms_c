class VacRecordsRegisterController < ApplicationController
  include VacRecordsRegisterHelper
  include VacManagerHelper
  include VacationsModule
  include ScheduleDateModule

  before_filter :authenticate_user
  before_filter :only_active_request, only: [:show, :edit, :update, :deactivate]
  before_filter :verify_schedule
  before_filter :register_access_request, only: [:edit, :update, :deactivate]
  before_filter :verify_rules

  # before_filter :verify_request_or_record_available_to_register

  def scheme_calculus
    vac_accum = VacAccumulated.user_latest(user_connected.id)
    legal_last_accu = VacAccumulated.user_latest_legal(user_connected.id)
    legal_last_accu = VacAccumulated.user_first(user_connected.id) unless legal_last_accu

    prev_auto_accumulated = VacAccumulated.user_latest_prog(user_connected.id)
    prev_auto_accumulated = VacAccumulated.user_first(user_connected.id) unless prev_auto_accumulated

    legal_future = future_days(legal_last_accu.up_to_date, params[:date_end], user_connected.id) + vac_accum.accumulated_real_days - VacRequest.sum_days_by_status(user_connected.id, VacRequest.approved)
    progressive = next_progressive_days(prev_auto_accumulated.up_to_date, params[:date_end], user_connected.id) - VacRequest.sum_progressive_days_by_status(user_connected.id, VacRequest.status_in_process).to_i

    legal_future = number_with_precision(legal_future, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
    progressive = number_with_precision(progressive, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

    render json: [legal_future, progressive]
  end

  def index
    @vac_requests = VacRequest.register(user_connected.id)
  end

  def register
    @vac_request = VacRequest.new(user_id: user_connected.id)
  end

  def show
    @vac_request = VacRequest.find(params[:vac_request_id])
    @vac_request = VacRequest.find(@vac_request.latest_version)
  end

  def create
    request = complete_create(params[:vac_request])
    if request.valid?
      exception = BpWarningException.first_usable_for_at(request.user_id, lms_time)
      restrictive_warnings = gimme_restrictive_warnings(request.user_id, request.begin, request.end, request.days, request.days_progressive, request.id)
      if exception.nil? && restrictive_warnings.size > 0 && !request.draft?
        @vac_request = request
        @vac_request.errors.add(:base, 'Tiene por lo menos una alarma restrictiva')
        render partial: 'request_new_form',
               locals: { request: @vac_request,
                         url_to_go: vac_records_register_register_create_path }
      else
        request.bp_warning_exception_id = exception.id if exception && restrictive_warnings.size > 0 && !request.draft?
        request.save
        if !params[:benefits].blank? && params[:benefits].size > 0
          satisfy_conditions = request_satify_this_benefits(request, true)
          params[:benefits].each { |benefit_id| set_benefit_for(request.id, benefit_id, satisfy_conditions) }
        end

        approvers = VacApprover.approvers_of(request.user_id)
        approvers.each do |approver|
          VacsMailer.new_pending_request(request, @company, approver.id).deliver if request.pending?
        end

        VacsMailer.new_pending_requester(request, @company).deliver if request.pending?

        render partial: 'request_info',
               locals: { request: request,
                         editable: request.draft?,
                         approver: false }
      end
    else
      @vac_request = request
      render partial: 'request_new_form',
             locals: { request: @vac_request,
                       url_to_go: vac_records_register_register_create_path }
    end
  end

  def edit
    @vac_request = VacRequest.find(params[:vac_request_id])
    @vac_request = VacRequest.find(@vac_request.latest_version)
  end

  def deactivate_rule_record
    benefit = VacRuleRecord.find(params[:vac_rule_record_id])
    benefit.active = false
    benefit.deactivated_at = lms_time
    benefit.deactivated_by_user_id = user_connected.id
    benefit.save
    render nothing: true
  end

  def update
    actual_request = VacRequest.find(params[:vac_request_id])
    actual_request = VacRequest.find(actual_request.latest_version)
    params[:vac_request_id] = actual_request.id

    actual_request = VacRequest.find(params[:vac_request_id])
    updated_request = complete_update(actual_request.id, params[:vac_request])

    if updated_request.valid?
      exception = actual_request.exception_available? ? actual_request.bp_warning_exception : BpWarningException.first_usable_for_at(updated_request.user_id, lms_time)
      restrictive_warnings = gimme_restrictive_warnings(updated_request.user_id, updated_request.begin, updated_request.end, updated_request.days, updated_request.days_progressive, params[:vac_request_id])
      if exception.nil? && restrictive_warnings.size > 0 && !updated_request.draft?
        @vac_request = actual_request
        @vac_request.errors.add(:base, 'Tiene por lo menos una alarma restrictiva')
        @vac_request.assign_attributes(params[:vac_request])

        render partial: 'request_new_form',
               locals: { request: @vac_request,
                         url_to_go: vac_records_register_register_update_path(@vac_request) }
      else
        updated_request.bp_warning_exception_id = exception.id if exception && restrictive_warnings.size > 0 && !updated_request.draft?
        updated_request.save
        actual_request.next_request_id = updated_request.id
        actual_request = deactivate_request(params[:vac_request_id])
        actual_request.save

        if !params[:benefits].blank? && params[:benefits].size > 0
          satisfy_conditions = request_satify_this_benefits(updated_request, true)
          params[:benefits].each { |benefit_id| set_benefit_for(updated_request.id, benefit_id, satisfy_conditions) }
        end

        approvers = VacApprover.approvers_of(updated_request.user_id)
        approvers.each do |approver|
          VacsMailer.new_pending_request(updated_request, @company, approver.id).deliver if updated_request.pending?
        end
        VacsMailer.new_pending_requester(updated_request, @company).deliver if updated_request.pending?

        render partial: 'request_info',
               locals: { request: updated_request,
                         editable: updated_request.draft?,
                         approver: false }
      end

    else
      @vac_request = actual_request
      @vac_request.assign_attributes(params[:vac_request])
      @vac_request.valid?

      render partial: 'request_new_form',
             locals: { request: @vac_request,
                       url_to_go: vac_records_register_register_update_path(@vac_request) }
    end
  end

  def deactivate
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)
    params[:vac_request_id] = request.id

    request = deactivate_request(params[:vac_request_id])
    request.save
    request.benefits_active.each do |benefit|
      deactivated_benefit = deactivate_benefit(benefit.id)
      deactivated_benefit.save
    end
    flash[:success] = t('views.vac_requests.flash_messages.success_deleted')
    redirect_to vac_records_register_index_path
  end

  def take_benefits
    # benefit = rule_record_complete_create(params[:benefit_id], params[:request_id])
    # benefit.save
    # update_end_vacations(params[:request_id])
    render nothing: true
  end

  def valid_create_request
    if params[:vac_request_id].blank?
      request = complete_create(params[:vac_request])
    else
      request = complete_update(params[:vac_request_id], params[:vac_request])
    end
    exception = request.exception_available? ? request.bp_warning_exception : BpWarningException.first_usable_for_at(request.user_id, lms_time)
    has_r_warnings = restrictive_warnings(request.user_id, request.begin, request.end, request.days, request.days_progressive, params[:vac_request_id])
    has_exception = !exception.nil?
    render json: (request.valid? && verified_closed_request(request, request.prev_request_id) && ( !has_r_warnings || request.draft? || (has_r_warnings && has_exception)))
  end

  def request_benefits
    request = VacRequest.find(params[:vac_request_id])
    benefits = request.vac_rule_records.where(active: true, refund_pending:false)

    render partial: 'vac_form/taken_benefits_list',
           locals: { benefits: benefits, not_as_form: true }
  end

  def scheme_days
    render partial: 'vac_records_register/scheme_days_modal',
           locals: { user: User.find(params[:user_id]) }
  end

  private

  def complete_create(params)
    request = VacRequest.new(params)
    request.user_id = user_connected.id
    request.registered_at = lms_time
    request.registered_by_user_id = user_connected.id
    return request
  end

  def only_active_request
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)
    unless request.active
      flash[:danger] = t('views.vac_requests.flash_messages.danger_not_active_request')
      redirect_to root_path
    end
  end

  def register_access_request
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)
    unless request.editable_by_register?
      flash[:danger] = t('views.vac_requests.flash_messages.danger_not_active_request')
      redirect_to root_path
    end
  end

  def verify_schedule
    schedule = schedule_of(user_connected.id, lms_time)
    return if schedule
    flash[:danger] = 'No tiene horario asignado'
    redirect_to root_path
  end

  def verify_rules
    rules = BpGeneralRulesVacation.user_active(lms_time, user_connected.id)
    return if rules
    ct_module = CtModule.where('cod = ? AND active = ?', 'vacs', true).first
    return if ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
    flash[:danger] = 'No tiene acceso a módulo'
    redirect_to root_path
  end

end
