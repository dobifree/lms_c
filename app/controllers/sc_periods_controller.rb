class ScPeriodsController < ApplicationController
  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def index
    @sc_periods = ScPeriod.all
  end

  def new
    @sc_period = ScPeriod.new
  end

  def edit
    @sc_period = ScPeriod.find(params[:id])
  end

  def create
    @sc_period = ScPeriod.new(params[:sc_period])
    if @sc_period.save
      flash[:success] = t('views.sc_periods.flash_messages.add_successfully')
      redirect_to sc_periods_path
    else
      render action: 'new'
    end
  end

  def update
    @sc_period = ScPeriod.find(params[:id])
     if @sc_period.update_attributes(params[:sc_period])
       unless @sc_period.active
         @sc_period.deactivated_by_user_id = user_connected.id
         @sc_period.deactivated_at = lms_time
         @sc_period.save
       end
       flash[:success] = t('views.sc_periods.flash_messages.changes_success')
       redirect_to sc_periods_path
     else
       render action: 'edit'
     end
  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end
end
