class UsersController < ApplicationController

  include ActionView::Helpers::NumberHelper
  include ExcelReportsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin, only: [:index, :search_users, :show, :new, :edit,
                                                 :create, :update, :edit_foto, :upload_foto,
                                                 :edit_requiere_cambio_password, :update_requiere_cambio_password,
                                                 :carga_masiva, :update_masivo, :carga_masiva_fotos,
                                                 :carga_masiva_fotos_upload, :carga_masiva_upload, :update_masivo_upload,
                                                 :lista_usuarios, :lista_usuarios_xlsx,
                                                 :edit_requiere_cambio_password_search, :update_requiere_cambio_password_search,
                                                 :edit_characteristics, :carga_masiva_excel, :download_xls_for_carga_masiva,
                                                 :update_characteristics,
                                                 :lista_usuarios, :lista_usuarios_csv, :lista_usuarios_xlsx,
                                                 :rep_logins, :rep_active_users, :rep_active_users_update_date, :rep_active_users_register,
                                                 :rep_users, :rep_active_users_register_sync, :rep_active_users_register_sync_csv,
                                                 :rep_active_users_training_sync, :rep_active_users_training_sync_csv]

  def index

    @user = User.new
    @users = {}

  end

  def search_users

    if !params[:user][:codigo].blank? || !params[:user][:apellidos].blank? || !params[:user][:nombre].blank? || !params[:user][:email].blank?

      filtro_jc = ''

      filtro_jc = 'AND jefe_capacitacion = 1' if params[:user][:jefe_capacitacion] == '1'

      filtro_jb = ''

      filtro_jb = 'AND jefe_biblioteca = 1' if params[:user][:jefe_biblioteca] == '1'

      filtro_pa = ''

      filtro_pa = 'AND perpetual_active = 1' if params[:user][:perpetual_active] == '1'


      if params[:user][:email].blank?

        @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND (email LIKE ? OR email IS NULL) ' + filtro_jc + ' ' + filtro_jb + ' ' + filtro_pa + ' ',
            '%' + params[:user][:codigo] + '%',
            '%' + params[:user][:apellidos] + '%',
            '%' + params[:user][:nombre] + '%',
            '%' + params[:user][:email] + '%').order('apellidos, nombre')

      else

        @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND email LIKE ? ' + filtro_jc + ' ' + filtro_jb + ' ' + filtro_pa + ' ',
            '%' + params[:user][:codigo] + '%',
            '%' + params[:user][:apellidos] + '%',
            '%' + params[:user][:nombre] + '%',
            '%' + params[:user][:email] + '%').order('apellidos, nombre')

      end

    else

      @users = {}


    end

    @user = User.new(params[:user])

    render 'index'

  end

  def show
    @user = User.find(params[:id])
   #@company = session[:company]
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def edit_characteristics
    @user = User.find(params[:id])
  end

  def edit_characteristics_by_user

  end

  def carga_masiva

  end

  def carga_masiva_excel

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5) + '.xlsx'
        archivo_temporal = directorio_temporal + nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') {|f| f.write(params[:upload]['datafile'].read)}

        from_date = lms_time
        from_date = DateTime.parse(params['from_date']) if params[:fijar_fecha] == '1'

        carga_usuario_from_excel archivo_temporal, from_date

        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
    end

  end

  def download_xls_for_carga_masiva


    reporte_excel = xls_for_carga_usuarios

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_Carga_Usuarios.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def update_masivo


  end

  def carga_masiva_fotos


  end

  def carga_masiva_fotos_upload

    if params[:upload]

      @total_fotos_cargadas = 0
      @fotos_error = []
      @fotos_error_messages = []

      upload = params[:upload]

     company = @company

      directory = company.directorio + '/' + company.codigo + '/fotos_usuarios/'

      FileUtils.mkdir_p directory unless File.directory? directory

      directory_tmp = directory + '/' + SecureRandom.hex(5) + '/'

      FileUtils.mkdir_p directory_tmp unless File.directory? directory_tmp

      if File.extname(upload['datafile'].original_filename) == '.zip'

        name = 'fotos.zip'
        path = File.join(directory_tmp, name)
        File.open(path, 'wb') {|f| f.write(upload['datafile'].read)}
        system "unzip -o -q \"#{directory_tmp + name}\" -d \"#{directory_tmp}\" "
        File.delete directory_tmp + name

        Dir.entries(directory_tmp).each do |foto|

          if foto != '.' && foto != '..'

            ext = File.extname(foto)

            if ext.downcase == '.jpg' || ext.downcase == '.jpeg'

              file_tmp = directory_tmp + foto

              if File.size(file_tmp) <= 70.kilobytes

                codigo_usuario = File.basename(file_tmp, ext)

                user = User.find_by_codigo codigo_usuario

                if user

                  if user.foto
                    file_actual = directory + user.foto
                    File.delete(file_actual) if File.exist? file_actual
                  end

                  user.set_foto

                  user.save

                  file = directory + user.foto

                  FileUtils.mv(file_tmp, file)

                  @total_fotos_cargadas += 1

                  #File.open(file, 'wb') { |f| f.write(File.open(file_tmp, 'read').read) }

                  #File.delete(file_tmp)
                else


                  @fotos_error.push foto
                  @fotos_error_messages.push 'no existe usuario'

                end


              else

                @fotos_error.push foto
                @fotos_error_messages.push 'tamaño > 70kb'

              end

            else

              @fotos_error.push foto
              @fotos_error_messages.push 'formato incorrecto'

            end

          end

        end

        FileUtils.remove_dir directory_tmp

        flash.now[:success] = t('activerecord.success.model.user.carga_masiva_ok')
        render 'carga_masiva_fotos'

      else

        flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_fotos_no_zip')

        render 'carga_masiva_fotos'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

      render 'carga_masiva_fotos'

    end


  end

  def create

    @user = User.new(params[:user])

    if @user.save
      flash[:success] = t('activerecord.success.model.user.create_ok')
      redirect_to @user
    else
      render 'new'
    end

  end

  def update

    @user = User.find(params[:id])

    if @user.update_attributes params[:user]
      flash[:success] = t('activerecord.success.model.user.update_ok')
      redirect_to @user
    else
      render 'edit'
    end

  end

  def update_characteristics

   company = @company

    user = User.find(params[:id])

    Characteristic.all.each do |characteristic|

      new_value = nil

      unless characteristic.data_type_id == 6
        new_value = params[('characteristic_' + characteristic.id.to_s).to_sym].strip unless params[('characteristic_' + characteristic.id.to_s).to_sym].blank?
      else
        new_value = params[('characteristic_' + characteristic.id.to_s).to_sym]
      end

      user.update_user_characteristic(characteristic, new_value, lms_time, nil, nil, admin_connected, admin_connected, company)

    end
=begin
    Characteristic.all.each do |characteristic|

      uc = user.user_characteristics.where('characteristic_id = ?', characteristic.id).first

      uc.destroy if uc

      unless params[('characteristic_'+characteristic.id.to_s).to_sym].blank?

        uc = UserCharacteristic.new
        uc.user = user
        uc.characteristic = characteristic
        uc.valor = params[('characteristic_'+characteristic.id.to_s).to_sym].strip unless characteristic.data_type_id == 6

        if characteristic.data_type_id == 0
          #string
          uc.value_string = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 1
          #text
          uc.value_text = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 2
          #date
          uc.value_date = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 3
          #int
          uc.value_int = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 4
          #float
          uc.value_float = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 5
          #lista
          uc.characteristic_value = characteristic.characteristic_values.where('id = ?',params[('characteristic_'+characteristic.id.to_s).to_sym]).first
        elsif characteristic.data_type_id == 6
          #file

          if params[('characteristic_'+characteristic.id.to_s).to_sym]

            directory = company.directorio+'/'+company.codigo+'/user_profiles/'+user.id.to_s+'/characteristics/'+characteristic.id.to_s+'/'

            FileUtils.remove_dir directory if File.directory? directory

            FileUtils.mkdir_p directory

            name = characteristic.id.to_s+'_'+SecureRandom.hex(5)+File.extname(params[('characteristic_'+characteristic.id.to_s).to_sym].original_filename)
            path = File.join(directory, name)
            File.open(path, 'wb') { |f| f.write(params[('characteristic_'+characteristic.id.to_s).to_sym].read) }

            uc.value_file = name

            uc.valor = name

          end

        end

        uc.save

      end

    end
=end

    flash[:success] = t('activerecord.success.model.user.update_characteristics_ok')
    redirect_to user


  end

  def edit_requiere_cambio_password


  end

  def update_requiere_cambio_password

    if verify_recaptcha

      User.update_all(requiere_cambio_pass: true)

      flash[:success] = t('activerecord.success.model.user.update_requiere_cambio_password')

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.user.update_requiere_cambio_password_captcha_error')

    end

    redirect_to user_edit_requiere_cambio_password_path


  end

  def edit_requiere_cambio_password_search


    @users = Array.new
    @characteristics = Characteristic.all
    @characteristics_prev = {}
    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        params[:characteristic][characteristic.id.to_s.to_sym].slice! 0

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]


          queries[n_q] = 'characteristic_id = ' + characteristic.id.to_s + ' AND ( '

          @characteristics_prev[characteristic.id].each_with_index do |valor, index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'valor = "' + valor + '" '

          end

          queries[n_q] += ' ) '

          n_q += 1

        end

      end

      if queries.length > 0

        queries.each_with_index do |query, index|

          if index == 0

            @users = User.joins(:user_characteristics).where(query).order('apellidos, nombre')

          else

            lista_users_ids = @users.map {|u| u.id}

            @users = User.joins(:user_characteristics).where(query + ' AND users.id IN (?) ', lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = Array.new

      end

    end


  end

  def update_requiere_cambio_password_search

    params[:user_id].each do |user_id, value|

      if value != '0'

        user = User.find(user_id)

        user.requiere_cambio_pass = true

        user.save

      end


    end


    flash[:success] = 'Solicitud de cambio password realizada'
    redirect_to user_edit_requiere_cambio_password_search_path


  end

  def edit_pass
    @user = user_connected
   #@company = session[:company]
  end

  def update_pass

   #@company = session[:company]
    @user = user_connected

    if user_connected.authenticate(params[:user][:current_password])
      unless params[:user][:password].blank?
        evaluation = @user.validate_password_policies(params[:user][:password])
        if evaluation[:valid]
          if user_connected.update_attributes(password: params[:user][:password], password_confirmation: params[:user][:password_confirmation])
            user_connected.requiere_cambio_pass = false
            user_connected.save
            flash[:success] = t('activerecord.success.model.user.update_pass_ok')
            redirect_to root_path
          else
            render 'edit_pass'
          end
        else
          evaluation[:errors].each do |rule|
            @user.errors.add :password, rule.error_match_message
          end
          render 'edit_pass'
        end
      else
        user_connected.errors.add :base, t('activerecord.errors.models.user.attributes.password.blank_update')
        render 'edit_pass'

      end

    else

      user_connected.errors.add :base, t('activerecord.errors.models.user.attributes.password.current')
      render 'edit_pass'

    end


  end

  def carga_masiva_upload

    if params[:upload]

     company = @company

      User.update_all({activo: false}) if params[:reemplazar_todo][:yes] == '1'

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos_tmp = line.split('{')

          datos = datos_tmp[0].split(';')

          if datos[4].nil?

            @lineas_error.push num_linea

            begin
              @lineas_error_detalle.push (line.force_encoding('UTF-8'))
            rescue Exception => e
              @lineas_error_detalle.push ('exception: ' + e.message)
            end

            @lineas_error_messages.push ['email null']

          else

            #0 usuario
            #1 nombres
            #2 apellidos
            #3 clave, si se pone asterisco no se actualizará
            #4 correo, si se pone asterisco no se actualizará
            #5 activo/inactivo
            #6 atributos

            user = User.find_by_codigo datos[0]

            unless user
              user = User.new
            end

            user.codigo = datos[0]
            user.nombre = datos[1]
            user.apellidos = datos[2]
            user.password = datos[3] if datos[3] != '*'
            user.email = datos[4].strip.downcase if datos[4] != '*'


            datos[5].delete!("\n")
            datos[5].delete!("\r")

            if datos[5] == 'activo'
              user.activo = true
            else
              user.activo = false
            end


            if user.save

              if datos_tmp[1]

                datos[6] = datos_tmp[1].strip.slice 0..-2

                atributos = datos[6].split(';')

                atributos.each do |atributo|

                  atributo = atributo.split(':')

                  caracteristica = Characteristic.find_by_nombre(atributo[0])

                  if caracteristica && caracteristica.data_type_id != 6

                    new_value = nil

                    error = false

                    if atributo[1] && !atributo[1].blank?

                      if caracteristica.data_type_id == 5
                        characteristic_value = caracteristica.characteristic_values.where('value_string = ?', atributo[1].strip).first
                        if characteristic_value
                          new_value = characteristic_value.id.to_s
                        else
                          error = true
                        end
                      else
                        new_value = atributo[1].strip
                      end

                    end

                    if error

                      @lineas_error.push num_linea

                      begin
                        @lineas_error_detalle.push ('codigo: ' + user.codigo +
                            '; nombre: ' + user.nombre +
                            '; apellidos: ' + user.apellidos +
                            '; password:' + datos[3] +
                            '; email: ' + (user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                      rescue Exception => e
                        @lineas_error_detalle.push ('exception: ' + e.message)
                      end

                      @lineas_error_messages.push [atributo[0].force_encoding('UTF-8') + ' no fue guardado: ' + atributo[1] + ' no existe']

                    else

                      error, user_characteristic = user.update_user_characteristic(caracteristica, new_value, lms_time, nil, nil, admin_connected, admin_connected, company)

                      if error

                        @lineas_error.push num_linea

                        begin
                          @lineas_error_detalle.push ('codigo: ' + user.codigo +
                              '; nombre: ' + user.nombre +
                              '; apellidos: ' + user.apellidos +
                              '; password:' + datos[3] +
                              '; email: ' + (user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                        rescue Exception => e
                          @lineas_error_detalle.push ('exception: ' + e.message)
                        end

                        @lineas_error_messages.push [atributo[0].force_encoding('UTF-8') + ' no fue guardado: ' + user_characteristic.errors.full_messages.to_s]

                      end

                    end


=begin
                    valor_incorrecto = false

                    user_caracteristica = UserCharacteristic.where('user_id = ? AND characteristic_id = ?', user.id, caracteristica.id).first
                    nuevo = false
                    unless user_caracteristica

                      user_caracteristica = UserCharacteristic.new
                      user_caracteristica.user = user
                      user_caracteristica.characteristic = caracteristica
                      nuevo = true
                    end

                    if atributo[1]
                      if atributo[1] == '*' && nuevo
                        valor_incorrecto = true
                      else
                        user_caracteristica.valor = atributo[1].strip

                        if caracteristica.data_type_id == 0
                          #string
                          user_caracteristica.value_string = atributo[1].strip
                        elsif caracteristica.data_type_id == 1
                          #text
                          user_caracteristica.value_text = atributo[1].strip
                        elsif caracteristica.data_type_id == 2
                          #date
                          user_caracteristica.value_date = atributo[1].strip
                        elsif caracteristica.data_type_id == 3
                          #int
                          user_caracteristica.value_int = atributo[1].strip
                        elsif caracteristica.data_type_id == 4
                          #float
                          user_caracteristica.value_float = atributo[1].strip
                        elsif caracteristica.data_type_id == 5
                          #lista
                          user_caracteristica.characteristic_value = caracteristica.characteristic_values.where('value_string = ?', atributo[1].strip).first
                        end

                      end

                    else
                      user_caracteristica.valor = ''
                    end

                    if valor_incorrecto

                      @lineas_error.push num_linea
                      begin
                        @lineas_error_detalle.push ('codigo: '+user.codigo+
                            '; nombre: '+user.nombre+
                            '; apellidos: '+user.apellidos+
                            '; password:'+datos[3]+
                            '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                      rescue Exception => e
                        @lineas_error_detalle.push ('exception: '+e.message)
                      end
                      @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no puede registrarse * en un atributo nuevo']

                    else

                      unless user_caracteristica.save

                        @lineas_error.push num_linea

                        begin
                          @lineas_error_detalle.push ('codigo: '+user.codigo+
                              '; nombre: '+user.nombre+
                              '; apellidos: '+user.apellidos+
                              '; password:'+datos[3]+
                              '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                        rescue Exception => e
                          @lineas_error_detalle.push ('exception: '+e.message)
                        end

                        @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no fue guardado: '+user_caracteristica.errors.full_messages.to_s]

                      end

                    end
=end
                  else

                    @lineas_error.push num_linea

                    begin
                      @lineas_error_detalle.push ('codigo: ' + user.codigo +
                          '; nombre: ' + user.nombre +
                          '; apellidos: ' + user.apellidos +
                          '; password:' + datos[3] +
                          '; email: ' + (user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                    rescue Exception => e
                      @lineas_error_detalle.push ('exception: ' + e.message)
                    end

                    @lineas_error_messages.push [atributo[0].force_encoding('UTF-8') + ' no existe']

                  end


                end

              end

            else

              @lineas_error.push num_linea

              begin
                @lineas_error_detalle.push ('codigo: ' + user.codigo +
                    '; nombre: ' + user.nombre +
                    '; apellidos: ' + user.apellidos +
                    '; password:' + datos[3] +
                    '; email: ' + (user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
              rescue Exception => e
                @lineas_error_detalle.push ('exception: ' + e.message)
              end


              @lineas_error_messages.push user.errors.full_messages

            end


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.user.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_error') if @lineas_error.length > 0

      render 'carga_masiva'

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

      render 'carga_masiva'

    end


  end

  def update_masivo_upload

    if params[:upload]

     company = @company

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos_tmp = line.split('{')

          datos = datos_tmp[0].split(';')

          #0 usuario

          user = User.find_by_codigo datos[0]

          if user

            datos.delete_at 0

            datos.each do |dato|

              dato.delete!("\n")
              dato.delete!("\r")

              dato = dato.split ':'
              case dato[0]
                when 'nombre'
                  user.nombre = dato[1]
                when 'apellidos'
                  user.apellidos = dato[1]
                when 'clave'
                  user.password = dato[1]
                when 'correo'
                  user.email = dato[1].strip.downcase
                when 'estado'
                  user.activo = dato[1] == 'activo' ? true : false
              end

            end


            if user.save

              if datos_tmp[1]

                datos[6] = datos_tmp[1].strip.slice 0..-2

                atributos = datos[6].split(';')

                atributos.each do |atributo|

                  atributo = atributo.split(':')

                  caracteristica = Characteristic.find_by_nombre(atributo[0])

                  if caracteristica && caracteristica.data_type_id != 6

                    new_value = nil

                    error = false

                    if atributo[1] && !atributo[1].blank?

                      if caracteristica.data_type_id == 5
                        characteristic_value = caracteristica.characteristic_values.where('value_string = ?', atributo[1].strip).first
                        if characteristic_value
                          new_value = characteristic_value.id.to_s
                        else
                          error = true
                        end
                      else
                        new_value = atributo[1].strip
                      end

                    end

                    if error

                      @lineas_error.push num_linea

                      begin
                        @lineas_error_detalle.push ('codigo: ' + user.codigo +
                            '; nombre: ' + user.nombre +
                            '; apellidos: ' + user.apellidos +
                            '; email: ' + (user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                      rescue Exception => e
                        @lineas_error_detalle.push ('exception: ' + e.message)
                      end

                      @lineas_error_messages.push [atributo[0].force_encoding('UTF-8') + ' no fue guardado: ' + atributo[1] + ' no existe']

                    else

                      error, user_characteristic = user.update_user_characteristic(caracteristica, new_value, lms_time, nil, nil, admin_connected, admin_connected, company)

                      if error

                        @lineas_error.push num_linea

                        begin
                          @lineas_error_detalle.push ('codigo: ' + user.codigo +
                              '; nombre: ' + user.nombre +
                              '; apellidos: ' + user.apellidos +
                              '; email: ' + (user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                        rescue Exception => e
                          @lineas_error_detalle.push ('exception: ' + e.message)
                        end

                        @lineas_error_messages.push [atributo[0].force_encoding('UTF-8') + ' no fue guardado: ' + user_characteristic.errors.full_messages.to_s]

                      end

                    end
=begin
                  if caracteristica

                    valor_incorrecto = false

                    user_caracteristica = UserCharacteristic.where('user_id = ? AND characteristic_id = ?', user.id, caracteristica.id).first
                    nuevo = false
                    unless user_caracteristica

                      user_caracteristica = UserCharacteristic.new
                      user_caracteristica.user = user
                      user_caracteristica.characteristic = caracteristica
                      nuevo = true
                    end

                    if atributo[1]
                      if atributo[1] == '*' && nuevo
                        valor_incorrecto = true
                      else
                        user_caracteristica.valor = atributo[1].strip

                        if caracteristica.data_type_id == 0
                          #string
                          user_caracteristica.value_string = atributo[1].strip
                        elsif caracteristica.data_type_id == 1
                          #text
                          user_caracteristica.value_text = atributo[1].strip
                        elsif caracteristica.data_type_id == 2
                          #date
                          user_caracteristica.value_date = atributo[1].strip
                        elsif caracteristica.data_type_id == 3
                          #int
                          user_caracteristica.value_int = atributo[1].strip
                        elsif caracteristica.data_type_id == 4
                          #float
                          user_caracteristica.value_float = atributo[1].strip
                        elsif caracteristica.data_type_id == 5
                          #lista
                          user_caracteristica.characteristic_value = caracteristica.characteristic_values.where('value_string = ?', atributo[1].strip).first
                        end

                      end

                    else
                      user_caracteristica.valor = ''
                    end

                    if valor_incorrecto

                      @lineas_error.push num_linea
                      begin
                        @lineas_error_detalle.push ('codigo: '+user.codigo+
                          '; nombre: '+user.nombre+
                          '; apellidos: '+user.apellidos+
                          '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                      rescue Exception => e
                        @lineas_error_detalle.push ('exception: '+e.message)
                      end
                      @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no puede registrarse * en un atributo nuevo']

                    else

                      unless user_caracteristica.save

                        @lineas_error.push num_linea

                        begin
                          @lineas_error_detalle.push ('codigo: '+user.codigo+
                            '; nombre: '+user.nombre+
                            '; apellidos: '+user.apellidos+
                            '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                        rescue Exception => e
                          @lineas_error_detalle.push ('exception: '+e.message)
                        end

                        @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no fue guardado: '+user_caracteristica.errors.full_messages.to_s]

                      end

                    end
=end
                  else

                    @lineas_error.push num_linea

                    begin
                      @lineas_error_detalle.push ('codigo: ' + user.codigo +
                          '; nombre: ' + user.nombre +
                          '; apellidos: ' + user.apellidos +
                          '; email: ' + (user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                    rescue Exception => e
                      @lineas_error_detalle.push ('exception: ' + e.message)
                    end

                    @lineas_error_messages.push [atributo[0].force_encoding('UTF-8') + ' no existe']

                  end


                end

              end

            else

              @lineas_error.push num_linea

              begin
                @lineas_error_detalle.push ('codigo: ' + user.codigo +
                    '; nombre: ' + user.nombre +
                    '; apellidos: ' + user.apellidos +
                    '; email: ' + (user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
              rescue Exception => e
                @lineas_error_detalle.push ('exception: ' + e.message)
              end

              @lineas_error_messages.push user.errors.full_messages

            end

          else
            #decir el usuario no existe

            @lineas_error.push num_linea

            begin
              @lineas_error_detalle.push ('codigo: ' + datos[0]).force_encoding('UTF-8')
            rescue Exception => e
              @lineas_error_detalle.push ('exception: ' + e.message)
            end

            @lineas_error_messages.push [('El usuario no existe').force_encoding('UTF-8')]


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.user.update_masivo_ok')
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_error') if @lineas_error.length > 0

      render 'update_masivo'

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

      render 'update_masivo'

    end


  end

  def edit_foto
    @user = User.find(params[:id])
   #@company = session[:company]
  end

  def upload_foto

    user = User.find(params[:id])

   company = @company

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext != '.jpg' && ext != '.jpeg'

        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_format')
        redirect_to users_edit_foto_path

      elsif File.size(params[:upload]['datafile'].tempfile) > 70.kilobytes

        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_wrong_size')
        redirect_to users_edit_foto_path

      else

        save_foto params[:upload], user, company

        flash[:success] = t('activerecord.success.model.user.update_foto')

        redirect_to user

      end


    else

      flash[:danger] = t('activerecord.error.model.user.update_foto_empty_file')

      redirect_to users_edit_foto_path

    end

  end

  def edit_foto_by_user
    @user = user_connected
   #@company = session[:company]

  end

  def upload_foto_by_user

   company = @company

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext != '.jpg' && ext != '.jpeg'

        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_format')
        redirect_to users_edit_foto_by_user_path

      elsif File.size(params[:upload]['datafile'].tempfile) > 70.kilobytes

        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_wrong_size')
        redirect_to users_edit_foto_by_user_path

      else

        save_foto params[:upload], user_connected, company

        flash[:success] = t('activerecord.success.model.user.update_foto')

        redirect_to user_profile_path

      end


    else

      flash[:danger] = t('activerecord.error.model.user.update_foto_empty_file')

      redirect_to users_edit_foto_by_user_path

    end

  end

  def lista_usuarios

    @characteristics = Characteristic.find_all_by_publica(true)

    @characteristics_priv = Characteristic.find_all_by_publica(false)

    @users = User.all

   #@company = session[:company]

  end

  def lista_usuarios_csv

    characteristics_pub = Characteristic.find_all_by_publica(true)
    characteristics_priv = Characteristic.find_all_by_publica(false)

    file_csv_name = SecureRandom.hex(5) + '.csv'

    CSV.open('/tmp/' + file_csv_name, 'wb', {:col_sep => "\t", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#"
      columns.push 'Código'
      columns.push 'Apellidos'
      columns.push 'Nombre'
      columns.push 'Correo Electrónico'
      columns.push 'Estado'

      characteristics_pub.each do |cha|
        columns.push cha.nombre
      end

      characteristics_priv.each do |cha|
        columns.push cha.nombre
      end

      csv << columns

      User.all.each_with_index do |user, index|

        columns = Array.new
        columns.push index + 1
        columns.push '="' + user.codigo + '"'
        columns.push user.apellidos
        columns.push user.nombre
        columns.push user.email

        if user.activo
          columns.push 'Activo'
        else
          columns.push 'Inactivo'
        end

        user_chars = Array.new

        user.user_characteristics.each do |user_char|
          user_chars[user_char.characteristic_id] = user_char.valor
        end

        characteristics_pub.each do |cha|

          user_cha = user_chars[cha.id]

          columns.push user_cha ? user_cha : '-'

        end

        characteristics_priv.each do |cha|

          user_cha = user_chars[cha.id]

          columns.push user_cha ? user_cha : '-'

        end

        csv << columns

      end

    end


    send_file '/tmp/' + file_csv_name,
              filename: 'lista_usuarios.csv',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def lista_usuarios_xlsx

   company = @company

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => {:style => :thin, color: '00'},
                                :alignment => {:horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => {:horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => {:horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => {:style => :thin, color: '00'},
                               :alignment => {:horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => {:style => :thin, color: '00'},
                                 :alignment => {:horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => {:style => :thin, color: '00'},
                              :alignment => {:horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => {:style => :thin, color: '00'},
                                   :alignment => {:horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => {:style => :thin, color: '00'},
                                    :alignment => {:horizontal => :left, :wrap_text => true}


      wb.add_worksheet(:name => 'Reporte Avance') do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Plataforma'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << $current_domain
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A' + fila_actual.to_s + ':B' + fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << (localize lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A' + fila_actual.to_s + ':B' + fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila


        fila_actual += 1


        fila = Array.new
        filaHeader = Array.new


        fila << 'DATOS PERSONALES'
        filaHeader << headerCenter
        fila << ''
        filaHeader << headerCenter
        fila << ''
        filaHeader << headerCenter
        fila << ''
        filaHeader << headerCenter
        fila << ''
        filaHeader << headerCenter
        fila << ''
        filaHeader << headerCenter


        characteristics = Characteristic.find_all_by_publica(true)

        #characteristics_porcentaje_sence = Characteristic.find_by_porcentaje_participante_sence(true)

        characteristics_priv = Characteristic.find_all_by_publica(false)

        if characteristics.length > 0

          fila << 'DATOS DEL ALUMNO'

          (1..characteristics.length).each do |n|

            filaHeader << headerCenter
            fila << ''

          end


        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A' + fila_actual.to_s + ':F' + fila_actual.to_s)

        if characteristics.length > 0

          reporte_excel.merge_cells('G' + fila_actual.to_s + ':' + letras[5 + characteristics.length] + fila_actual.to_s)

        end

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader << headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader << headerLeft
        cols_widths << 15

        fila << 'Apellidos'
        filaHeader << headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader << headerLeft
        cols_widths << 22

        fila << 'Correo Electrónico'
        filaHeader << headerLeft
        cols_widths << 22

        fila << 'Estado'
        filaHeader << headerLeft
        cols_widths << 22


        characteristics.each do |cha|

          fila << cha.nombre
          filaHeader << headerLeft
          cols_widths << 22

        end

        characteristics_priv.each do |cha|

          fila << cha.nombre
          filaHeader << headerLeft
          cols_widths << 22

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        User.all.each_with_index do |user, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index + 1
          fileStyle << fieldLeft

          fila << user.codigo
          fileStyle << fieldLeft

          fila << user.apellidos
          fileStyle << fieldLeft

          fila << user.nombre
          fileStyle << fieldLeft

          fila << user.email
          fileStyle << fieldLeft

          if user.activo
            fila << 'Activo'
          else
            fila << 'Inactivo'
          end

          fileStyle << fieldLeft

          characteristics.each do |cha|

            user_cha = user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end

          characteristics_priv.each do |cha|

            user_cha = user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end


          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end


      end

    end


    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Lista de Usuarios - ' + $current_domain + '.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def rep_logins

    @users = User.all

    filename = 'reporte_de_acceso_' + (localize(lms_time, format: :full_date_time_for_download)) + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_last_logins

    @users = User.all

    filename = 'reporte_de_último_acceso_' + (localize(lms_time, format: :full_date_time_for_download)) + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_active_users

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristics = Array.new
    @characteristic_types = Array.new
    @characteristic_types_sizes = Array.new

    CharacteristicType.all.each do |characteristic_type|

      aux = -1

      characteristic_type.characteristics.each_with_index do |characteristic, index_c|

        unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

          @characteristics.push characteristic
          aux += 1

        end

      end

      if aux > -1

        @characteristic_types.push characteristic_type
        @characteristic_types_sizes.push aux

      end

    end

    Characteristic.not_grouped_public_characteristics.each do |characteristic|

      @characteristics.push characteristic unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

    end

    @users = User.where('activo = ?', true)

    filename = 'reporte_usuarios_activos_' + (localize(lms_time, format: :full_date_time_for_download)) + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end


  def rep_active_users_update_date

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristics = Array.new
    @characteristic_types = Array.new
    @characteristic_types_sizes = Array.new

    CharacteristicType.all.each do |characteristic_type|

      aux = -1

      characteristic_type.characteristics.each_with_index do |characteristic, index_c|

        unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

          @characteristics.push characteristic
          aux += 1

        end

      end

      if aux > -1

        @characteristic_types.push characteristic_type
        @characteristic_types_sizes.push aux

      end

    end

    Characteristic.not_grouped_public_characteristics.each do |characteristic|

      @characteristics.push characteristic unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

    end

    @users = User.where('activo = ?', true)

    filename = 'reporte_usuarios_activos_' + (localize(lms_time, format: :full_date_time_for_download)) + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_active_users_register

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristic_parent = Characteristic.find(params[:characteristic_id])
    @characteristics = @characteristic_parent.elements_characteristics.reorder('register_position')

    @users = User.where('activo = ?', true)

    filename = 'reporte_usuarios_activos_' + @characteristic_parent.nombre + '_' + (localize(lms_time, format: :full_date_time_for_download)) + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_users

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristics = Array.new
    @characteristic_types = Array.new
    @characteristic_types_sizes = Array.new

    CharacteristicType.all.each do |characteristic_type|

      aux = -1

      characteristic_type.characteristics.each_with_index do |characteristic, index_c|

        unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

          @characteristics.push characteristic
          aux += 1

        end

      end

      if aux > -1

        @characteristic_types.push characteristic_type
        @characteristic_types_sizes.push aux

      end

    end

    Characteristic.not_grouped_public_characteristics.each do |characteristic|

      @characteristics.push characteristic unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

    end

    @users = User.all

    filename = 'reporte_usuarios_' + (localize(lms_time, format: :full_date_time_for_download)) + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_active_users_register_sync

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristic_parent = Characteristic.find(params[:characteristic_id])
    @characteristics = @characteristic_parent.elements_characteristics.reorder('register_position')

    @users = User.where('activo = ?', true)

    filename = 'sync_usuarios_activos_' + @characteristic_parent.nombre + '_' + (localize(lms_time, format: :year_month_day)) + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_active_users_register_sync_csv

   #@company = session[:company]

    @characteristic_parent = Characteristic.find(params[:characteristic_id])
    @characteristics = @characteristic_parent.elements_characteristics.reorder('register_position')

    @users = User.where('activo = ?', true)

    file_csv_name = SecureRandom.hex(5) + '.csv'

    CSV.open('/tmp/' + file_csv_name, 'wb', {:col_sep => ";", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#{alias_username}"

      @characteristics.each do |characteristic|

        columns.push characteristic.nombre

      end

      csv << columns

      @users.each_with_index do |user, index_u|

        user_characteristics_registered = user.user_characteristics_registered(@characteristic_parent)

        if user_characteristics_registered.size == 0

          columns = Array.new

          columns.push user.codigo

          csv << columns

        else

          user_characteristics_registered.each_with_index do |user_characteristic_registered|

            columns = Array.new

            columns.push user.codigo

            @characteristics.each_with_index do |element_characteristic|

              element_user_characteristic = user_characteristic_registered.elements_user_characteristics.where('register_user_characteristic_id = ? AND characteristic_id = ?', user_characteristic_registered.id, element_characteristic.id).first

              unless element_characteristic.data_type_id == 6

                if element_user_characteristic

                  efcv = element_user_characteristic.excel_formatted_value

                  if efcv

                    columns.push efcv

                  else

                    columns.push ''

                  end

                else

                  columns.push ''

                end

              else

                if element_user_characteristic && element_user_characteristic.value_file

                  columns.push 'Sí'

                else

                  columns.push 'No'

                end

              end

            end

            csv << columns

          end

        end

      end

    end

    filename = 'sync_usuarios_activos_' + @characteristic_parent.nombre + '_' + (localize(lms_time, format: :year_month_day)) + '.csv'

    send_file '/tmp/' + file_csv_name,
              filename: filename,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def rep_active_users_training_sync

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @users = User.where('activo = ?', true)

    filename = 'sync_usuarios_activos_Capacitación_' + (localize(lms_time, format: :year_month_day)) + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_active_users_training_sync_csv

   #@company = session[:company]

    @users = User.where('activo = ?', true)

    file_csv_name = SecureRandom.hex(5) + '.csv'

    CSV.open('/tmp/' + file_csv_name, 'wb', {:col_sep => ";", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#{alias_username}"
      columns.push 'Programa'
      if TrainingType.all.size > 0
        columns.push @company.training_type_alias
      end
      columns.push 'Curso'
      columns.push 'Dedicación'
      columns.push 'Oportunidad'
      columns.push 'Fecha Inicio'
      columns.push 'Fecha Fin'
      columns.push 'Fecha Inicio Real'
      columns.push 'Fecha Fin Real'
      columns.push 'Estado'
      columns.push 'Asistencia'
      columns.push 'Nota Final'
      columns.push 'Resultado Final'

      csv << columns

      @users.each_with_index do |user|

        user_courses = user.user_courses

        if user_courses.size == 0

          columns = Array.new

          columns.push user.codigo

          csv << columns

        else

          user_courses.each_with_index do |user_course|

            columns = Array.new

            columns.push user.codigo
            columns.push user_course.program_course.program.nombre
            if TrainingType.all.size > 0
              if user_course.course.training_type
                columns.push user_course.course.training_type.nombre
              else
                columns.push ''
              end
            end
            columns.push user_course.course.nombre
            columns.push user_course.course.dedicacion_estimada
            columns.push user_course.numero_oportunidad

            if user_course.desde
              columns.push user_course.desde.to_date
            else
              columns.push ''
            end

            if user_course.hasta
              columns.push user_course.hasta.to_date
            else
              columns.push ''
            end

            if user_course.inicio
              columns.push user_course.inicio.to_date
            else
              columns.push ''
            end

            if user_course.fin
              columns.push user_course.fin.to_date
            else
              columns.push ''
            end

            if !user_course.iniciado
              e = 'No iniciado'
            elsif user_course.finalizado
              e = 'Terminado'
            else
              e = 'Iniciado'
            end

            columns.push e

            if user_course.porcentaje_avance
              columns.push user_course.porcentaje_avance
            else
              columns.push ''
            end

            if user_course.nota
              columns.push user_course.nota
            else
              columns.push ''
            end

            if user_course.aprobado
              ef = 'Aprobado'
            elsif user_course.finalizado
              ef = 'Reprobado'
            else
              ef = ''
            end

            columns.push ef

            csv << columns

          end

        end

      end

    end

    filename = 'sync_usuarios_activos_Capacitación_' + (localize(lms_time, format: :year_month_day)) + '.csv'

    send_file '/tmp/' + file_csv_name,
              filename: filename,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

  def save_foto(upload, user, company)

    ext = File.extname(upload['datafile'].original_filename)

    if ext == '.jpg' || ext == '.jpeg'

      if File.size(upload['datafile'].tempfile) <= 70.kilobytes

        directory = company.directorio + '/' + company.codigo + '/fotos_usuarios/'

        FileUtils.mkdir_p directory unless File.directory? directory

        if user.foto
          file = directory + user.foto
          File.delete(file) if File.exist? file
        end

        user.set_foto

        user.save

        #puts user.errors.full_messages

        file = directory + user.foto

        File.open(file, 'wb') {|f| f.write(upload['datafile'].read)}

      end

    end

  end

end
