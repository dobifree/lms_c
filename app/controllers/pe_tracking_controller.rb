class PeTrackingController < ApplicationController

  include PeAssessmentHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list]
  before_filter :get_data_2, only: [:show, :track]


  before_filter :validate_open_process, only: [:people_list, :show, :track]
  before_filter :validate_track, only: [:people_list, :show, :track]
  before_filter :validate_track_member, only: [:show, :track]

  def people_list

    @pe_question_comment = PeQuestionComment.new

  end

  def show

    @pe_member_rel_shared_comment = PeMemberRelSharedComment.new
    @pe_question_comment = PeQuestionComment.new

    pe_evaluation = @pe_evaluation

    if !pe_evaluation.entered_by_manager? && pe_evaluation.has_pe_evaluation_rel(@pe_member_rel.pe_rel) && pe_evaluation.has_associations_to_kpis?

      pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation pe_evaluation

      unless pe_assessment_evaluation

        pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluations.build
        pe_assessment_evaluation.pe_process = @pe_process
        pe_assessment_evaluation.pe_evaluation = pe_evaluation
        pe_assessment_evaluation.last_update = lms_time

        pe_assessment_evaluation.points = -1

        pe_assessment_evaluation.percentage = -1

        pe_assessment_evaluation.save

      end

      sync_assessment_kpis(pe_assessment_evaluation, pe_evaluation, @pe_member_evaluated, @pe_member_evaluator, @pe_rel)

    end

  end

  def track

    pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation @pe_evaluation
    pe_assessment_evaluation.destroy if pe_assessment_evaluation

    pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluations.build
    pe_assessment_evaluation.pe_process = @pe_process
    pe_assessment_evaluation.pe_evaluation = @pe_evaluation
    pe_assessment_evaluation.last_update = lms_time

    pe_assessment_evaluation.points = -1

    pe_assessment_evaluation.percentage = -1

    pe_assessment_evaluation.save

    answer_evaluation pe_assessment_evaluation

    calculate_assessment_evaluation pe_assessment_evaluation

    flash[:success] = t('activerecord.success.model.pe_tracking.create_ok')

    redirect_to pe_tracking_show_path(@pe_evaluation, @pe_member_rel)

  end


  private

  def get_data_1

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process

    @pe_rel = @pe_process.pe_rel_by_id 0

    @pe_members_connected = @pe_process.pe_members_by_user user_connected

    @pe_member_evaluated = @pe_members_connected.first

    @pe_member_rel = @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(@pe_rel).first if @pe_member_evaluated && @pe_rel

    @pe_rel_previous = params[:pe_rel_id] ? PeRel.find(params[:pe_rel_id]) : nil

   #@company = session[:company]

  end

  def get_data_2

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_rel = @pe_member_rel.pe_rel
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_rels_comments = Array.new
    @pe_member_rels_comments.push @pe_member_rel

    if @pe_rel.rel == 1
      pe_rel_aux = @pe_process.pe_rel_by_id 0
      @pe_member_rels_comments += @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_aux) if pe_rel_aux
    elsif @pe_rel.rel == 0 && ((@pe_evaluation.def_allow_individual_comments_by_boss && @pe_evaluation.def_allow_watch_individual_comments_by_boss) || @pe_evaluation.def_allow_individual_comments_with_boss)
      pe_rel_aux = @pe_process.pe_rel_by_id 1
      @pe_member_rels_comments += @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_aux) if pe_rel_aux
    end

    @pe_member_connected = nil

    @pe_member_group = @pe_member_evaluated.pe_member_group(@pe_evaluation)

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
        break
      end

    end

   #@company = session[:company]

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_track

    unless @pe_evaluation.allow_tracking?
      flash[:danger] = t('activerecord.error.model.pe_tracking.cant_track')
      redirect_to root_path
    end

  end

  def validate_track_member

    unless @pe_member_connected && @pe_member_evaluator.id == @pe_member_connected.id && @pe_evaluation.pe_evaluation_rels.where('pe_rel_id = ?', @pe_member_rel.pe_rel_id).count == 1 && @pe_evaluation.pe_questions_by_pe_member_all_levels_not_confirmed(@pe_member_evaluated, @pe_member_evaluator, nil, nil).count == 0
      flash[:danger] = t('activerecord.error.model.pe_tracking.cant_track')
      redirect_to root_path
    end

  end


end
