class PeReportersController < ApplicationController

  include UsersHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def search
    @pe_process = PeProcess.find(params[:pe_process_id])

    @user = User.new
    @users = Array.new

    if params[:user]

      @user = User.new(params[:user])

      @users = search_normal_users(@user.apellidos, @user.nombre)

    end

  end

  def add_reporter
    @pe_process = PeProcess.find(params[:pe_process_id])
    @user = User.find(params[:user_id])

    pr = @pe_process.pe_reporters.where('user_id = ?', params[:user_id]).first

    pr.destroy if pr

    pe_reporter = @pe_process.pe_reporters.build
    pe_reporter.user = @user

    pe_reporter.general = params[:pe_reporter][:general]

    pe_reporter.save

    unless pe_reporter.general

      @pe_process.pe_characteristics.each do |pe_characteristic|

        if pe_characteristic.active_segmentation

          params[:characteristic][pe_characteristic.characteristic.id.to_s].slice! 0

          params[:characteristic][pe_characteristic.characteristic.id.to_s].each do |v|

            pcv = pe_reporter.pe_reporter_characteristic_values.build
            pcv.pe_characteristic = pe_characteristic
            pcv.value = v
            pcv.save

          end

        end

      end

    end



    redirect_to pe_reporters_search_path @pe_process

  end

  def destroy

    @pe_reporter = PeReporter.find(params[:id])

    pe_process = @pe_reporter.pe_process

    @pe_reporter.destroy

    redirect_to pe_process_tab_url(pe_process, 'managers')


  end
end
