class PeQuestionFilesController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:create]
  before_filter :get_data_2, only: [:download, :destroy]
  before_filter :validate_open_process, only: [:create, :destroy]
  before_filter :validate_create_file, only: [:create]
  before_filter :validate_destroy_file, only: [:destroy]
  before_filter :validate_download_file, only: [:download]

  def create

    if params[:upload]

      pe_question_file = @pe_question.pe_question_files.build
      pe_question_file.pe_member = @pe_member_rel.pe_member_evaluated
      pe_question_file.description = params[:pe_question_file][:description]
      pe_question_file.registered_at = lms_time
      pe_question_file.file_name = @pe_question.id.to_s+'_'+@pe_member_connected.id.to_s+'_'+SecureRandom.hex(5) + File.extname(params[:upload]['datafile'].original_filename)
      pe_question_file.user = user_connected

      if pe_question_file.save
        #flash[:success] = t('activerecord.success.model.pe_comment.create_ok')

        directory = @company.directorio + '/' + @company.codigo + '/pe/pe_question_files/'

        FileUtils.mkdir_p directory unless File.directory? directory

        file = directory + pe_question_file.file_name

        File.open(file, 'wb') {|f| f.write(params[:upload]['datafile'].read)}

        pe_question_file.save

        #render json: pe_question_file
        #
        #

        flash[:success] = t('activerecord.success.model.pe_question_file.create_ok')


      else
        pe_question_file.errors.full_messages.each do |msg|
          flash[:danger] = msg
          break
        end
      end

    else
      flash[:danger] = 'Debe adjuntar un archivo'
    end

    redirect_to pe_assessment_show_2_path @pe_member_rel

  end


  def destroy

    if File.exist? @company.directorio + '/' + @company.codigo + '/pe/pe_question_files/'+@pe_question_file.file_name

      File.delete @company.directorio + '/' + @company.codigo + '/pe/pe_question_files/'+@pe_question_file.file_name

    end

    @pe_question_file.destroy

    render json: 'ok'

  end

  def download

    send_file @company.directorio + '/' + @company.codigo + '/pe/pe_question_files/'+@pe_question_file.file_name,
              filename: ( @pe_question_file.description + File.extname(@pe_question_file.file_name)),
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

  def get_data_1

    @pe_question = PeQuestion.find(params[:pe_question_id])
    @pe_evaluation = @pe_question.pe_evaluation
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator
    @pe_process = @pe_member_rel.pe_process

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
        break
      end

    end

  end

  def get_data_2

    @pe_question_file = PeQuestionFile.find(params[:pe_question_file_id])
    @pe_question = @pe_question_file.pe_question
    @pe_evaluation = @pe_question.pe_evaluation
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator
    @pe_process = @pe_member_rel.pe_process

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
        break
      end

    end

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_create_file

    unless @pe_evaluation.assess_allow_files_to_boss && (@pe_member_rel.pe_rel.rel_id == 0 || @pe_member_rel.pe_rel.rel_id == 1)
      flash[:danger] = t('activerecord.error.model.pe_question_file.cant_create')
      redirect_to root_path
    end

  end

  def validate_destroy_file

    unless @pe_evaluation.assess_allow_files_to_boss && ( (@pe_member_rel.pe_rel.rel_id == 0 && @pe_member_connected && @pe_question_file.pe_member_id == @pe_member_connected.id) || (@pe_member_rel.pe_rel.rel_id == 1 && @pe_member_connected && @pe_member_rel.pe_member_evaluator_id == @pe_member_connected.id && @pe_question_file.pe_member_id == @pe_member_rel.pe_member_evaluated_id) )
      flash[:danger] = t('activerecord.error.model.pe_question_file.cant_destroy')
      redirect_to root_path
    end

  end

  def validate_download_file
    unless @pe_evaluation.assess_allow_files_to_boss && ( (@pe_member_rel.pe_rel.rel_id == 0 && @pe_member_connected && @pe_question_file.pe_member_id == @pe_member_connected.id) || (@pe_member_rel.pe_rel.rel_id == 1 && @pe_member_connected && @pe_member_rel.pe_member_evaluator_id == @pe_member_connected.id && @pe_question_file.pe_member_id == @pe_member_rel.pe_member_evaluated_id) )
      flash[:danger] = t('activerecord.error.model.pe_question_file.cant_download')
      redirect_to root_path
    end
  end


end
