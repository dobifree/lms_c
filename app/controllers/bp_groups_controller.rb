class BpGroupsController < ApplicationController

  def index
    @bp_groups = BpGroup.all
    @bp_seasons = BpSeason.all
    @bp_events = BpEvent.all
    @bp_general_rules_vacations = BpGeneralRulesVacation.all

    @tab = params[:tab] ? params[:tab] : 0
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bp_groups }
    end
  end

  def new
    @bp_group = BpGroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bp_group }
    end
  end

  def edit
    @bp_group = BpGroup.find(params[:id])
  end

  def create
    bp_group = BpGroup.new(params[:bp_group])
    bp_group.registered_by_admin_id = admin_connected.id
    bp_group.registered_at = lms_time

    if bp_group.save
      flash[:success] = t('views.bp_groups.flash_messages.success_created')
      redirect_to bp_groups_users_path(bp_group)
    else
      @bp_group = bp_group
      render action: "new"
    end
  end

  def update
    bp_group = BpGroup.find(params[:id])
    if bp_group.update_attributes(params[:bp_group])
      flash[:success] = t('views.bp_groups.flash_messages.success_changed')
      redirect_to bp_groups_users_path(bp_group)
    else
      @bp_group = bp_group
      render action: "edit"
    end
  end

  def bp_groups_deactivate
    bp_group = BpGroup.find(params[:bp_group_id])
    bp_group.active = false
    bp_group.deactivated_at = lms_time
    bp_group.deactivated_by_admin_id = admin_connected.id
    if bp_group.save
      flash[:success] = t('views.bp_groups.flash_messages.success_deactivated')
      redirect_to bp_groups_users_path(bp_group)
    else
      @bp_group = bp_group
      render action: "edit"
    end
  end

  def bp_groups_activate
    bp_group = BpGroup.find(params[:bp_group_id])
    bp_group.active = true
    bp_group.deactivated_at = nil
    bp_group.deactivated_by_admin_id = nil
    if bp_group.save
      flash[:success] = t('views.bp_groups.flash_messages.success_activated')
    else
      flash[:danger] = t('views.bp_groups.flash_messages.danger_activated')
    end
    redirect_to bp_groups_users_path(bp_group)
  end

  def groups_users
    @bp_group = BpGroup.find(params[:bp_group_id])
    @bp_group_users = @bp_group.bp_groups_users
  end

  def groups_users_new_search_people
    @bp_group = BpGroup.find(params[:bp_group_id])
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def groups_users_create
    bp_groups_user = BpGroupsUser.new(:user_id => params[:user_id], :bp_group_id => params[:bp_group_id])
    bp_groups_user.registered_at = lms_time
    bp_groups_user.registered_by_admin_id = admin_connected.id
    if bp_groups_user.save
      flash[:success] = t('views.bp_groups_users.flash_messages.success_assignment')
      redirect_to groups_users_new_search_people_path(params[:bp_group_id])
    else
      @bp_group = BpGroup.find(params[:bp_group_id])
      @bp_group_user = bp_groups_user
      @user = User.find(params[:user_id])
      @users = User.where(:id => params[:user_id])
      render action: 'groups_users_new_search_people'
    end
  end

  def groups_users_create_period_time
    desdehasta = params[:desdehasta].split('-')
    desde = DateTime.parse(desdehasta[0])
    hasta = DateTime.parse(desdehasta[1])

    bp_groups_user = BpGroupsUser.new(:user_id => params[:user_id], :bp_group_id => params[:bp_group_id], :since => desde, :until => hasta)
    bp_groups_user.registered_at = lms_time
    bp_groups_user.registered_by_admin_id = admin_connected.id

    if bp_groups_user.save
      flash[:success] = t('views.bp_groups_users.flash_messages.success_assignment')
      redirect_to groups_users_new_search_people_path(params[:bp_group_id])
    else
      @bp_group = BpGroup.find(params[:bp_group_id])
      @bp_group_user = bp_groups_user
      @user = User.find(params[:user_id])
      @users = User.where(:id => params[:user_id])
      render action: 'groups_users_new_search_people'
    end
  end

  def groups_users_deactivate
    bp_group_user = BpGroupsUser.find(params[:bp_group_user_id])
    bp_group_user.active = false
    bp_group_user.deactivated_at = lms_time
    bp_group_user.deactivated_by_admin_id = admin_connected.id
    if bp_group_user.save
      flash[:success] = t('views.bp_groups_users.flash_messages.success_deactivate_group_user')
      redirect_to bp_groups_users_path(bp_group_user)
    else
      flash[:danger] = t('views.bp_groups_users.flash_messages.danger_deactivate_group_user')
      redirect_to bp_groups_users_edit_path(bp_group_user)
    end
  end

  def groups_users_edit
    @bp_group_user = BpGroupsUser.find(params[:bp_group_user_id])
  end

  def groups_users_update
    bp_group_user = BpGroupsUser.find(params[:bp_group_user_id])
    if bp_group_user.update_attributes(params[:bp_groups_user])
      flash[:success] = t('views.bp_groups_users.flash_messages.success_messages')
      redirect_to bp_groups_users_path(bp_group_user.bp_group_id)
    else
      @bp_group_user = bp_group_user
      render action: "edit"
    end
  end

end