class CompanyUnitsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_jefe_capacitacion

  def index
    @planning_processes = PlanningProcess.where('abierto = ?', true)

  end

  def show
    @company_unit = CompanyUnit.find(params[:id])
  end

  def new
    @company_unit = CompanyUnit.new
  end

  def edit
    @company_unit = CompanyUnit.find(params[:id])
  end

  def delete
    @company_unit = CompanyUnit.find(params[:id])
  end

  def create
    @company_unit = CompanyUnit.new(params[:company_unit])

    if @company_unit.save
      flash[:success] = t('activerecord.success.model.company_unit.create_ok')

      planning_process_company_unit = PlanningProcessCompanyUnit.new
      planning_process_company_unit.planning_process_id = @company_unit.planning_process_id
      planning_process_company_unit.company_unit_id = @company_unit.id

      planning_process_company_unit.save


      redirect_to company_units_url
    else
      render action: 'new'
    end

  end

  def update
    @company_unit = CompanyUnit.find(params[:id])

    if @company_unit.update_attributes(params[:company_unit])
      flash[:success] = t('activerecord.success.model.company_unit.update_ok')
      redirect_to company_units_url
    else
      render action: 'edit'
    end

  end


  def destroy

    @company_unit = CompanyUnit.find(params[:id])

    if verify_recaptcha

      if @company_unit.destroy
        flash[:success] = t('activerecord.success.model.company_unit.delete_ok')
        redirect_to company_units_url
      else
        flash[:danger] = t('activerecord.success.model.company_unit.delete_error')
        redirect_to delete_company_unit_path(@company_unit)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.company_unit.captcha_error')
      redirect_to delete_company_unit_path(@company_unit)

    end

  end

  private

    def verify_jefe_capacitacion

      unless user_connected.jefe_capacitacion?
        flash[:danger] = t('security.no_access_planning_process_as_jefe_capacitacion')
        redirect_to root_path
      end

    end

end
