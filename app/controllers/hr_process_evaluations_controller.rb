class HrProcessEvaluationsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def edit

    @hr_process_evaluation = HrProcessEvaluation.find(params[:id])

    @hr_process = @hr_process_evaluation.hr_process

  end

  def update

    @hr_process_evaluation = HrProcessEvaluation.find(params[:id])
    @hr_process = @hr_process_evaluation.hr_process

    if @hr_process_evaluation.update_attributes(params[:hr_process_evaluation])

      @hr_process_evaluation.ajusta_fecha_fin
      @hr_process_evaluation.save

      flash[:success] = t('activerecord.success.model.hr_process_evaluation.update_ok')
      redirect_to @hr_process
    else
      render action: 'edit'
    end

  end


  def index
    @hr_process_evaluations = HrProcessEvaluation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @hr_process_evaluations }
    end
  end


  def show
    @hr_process_evaluation = HrProcessEvaluation.find(params[:id])
    @hr_process = @hr_process_evaluation.hr_process
  end

  # GET /hr_process_evaluations/new
  # GET /hr_process_evaluations/new.json
  def new
    @hr_process_evaluation = HrProcessEvaluation.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @hr_process_evaluation }
    end
  end



  # POST /hr_process_evaluations
  # POST /hr_process_evaluations.json
  def create
    @hr_process_evaluation = HrProcessEvaluation.new(params[:hr_process_evaluation])

    respond_to do |format|
      if @hr_process_evaluation.save
        format.html { redirect_to @hr_process_evaluation, notice: 'Hr process evaluation was successfully created.' }
        format.json { render json: @hr_process_evaluation, status: :created, location: @hr_process_evaluation }
      else
        format.html { render action: "new" }
        format.json { render json: @hr_process_evaluation.errors, status: :unprocessable_entity }
      end
    end
  end



  # DELETE /hr_process_evaluations/1
  # DELETE /hr_process_evaluations/1.json
  def destroy
    @hr_process_evaluation = HrProcessEvaluation.find(params[:id])
    @hr_process_evaluation.destroy

    respond_to do |format|
      format.html { redirect_to hr_process_evaluations_url }
      format.json { head :no_content }
    end
  end
end
