class BpAdminGeneralRulesVacationsController < ApplicationController
  include BpAdminGeneralRulesVacationsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin, except: :general_rule_description_modal


  def general_rule_description_modal
    general_rules = BpGeneralRulesVacation.find(params[:bp_general_rule_id])
    render partial: 'description_modal',
           locals: { general_rules: general_rules }

  end

  def index_by_user
    #LISTA LOS USUARIOS QUE SE ENCUENTRAN EN GRUPOS CON POR LO MENOS UN: BP_GENERAL_RULES_VACATION
    @users = []
    groups = BpGroup.all
    groups.each do |group|
      next if group.bp_general_rules_vacations.empty?
      group.bp_groups_users.each { |group_user| @users << group_user.user }
    end
    @users = @users.uniq_by(&:id)
  end

  def index_by_user_manage
    # USUARIO: SEGÚN LO QUE SE HAYA PASADO POR PARÁMETRO
    # REGLAS GENERALES A LISTAR: TODAS LAS DE LOS GRUPOS A LAS CUALES HA PERTENECIDO EL USUARIO. SE LE SUMAN TODAS LAS INDIVIDUALES
    @bp_general_rules_vacations = []
    @user = User.find(params[:user_id])
    groups_users = @user.bp_groups_users
    groups_users.each do |group_user|
      group_user.bp_group.bp_general_rules_vacations.each { |gen_rule| @bp_general_rules_vacations << gen_rule }
    end
    user_gen_rules = BpGeneralRulesVacation.where(user_id: @user.id)
    user_gen_rules.each { |gen_rule| @bp_general_rules_vacations << gen_rule }
  end

  def new_by_user
    @bp_general_rules_vacation = BpGeneralRulesVacation.new
    @user = User.find(params[:user_id])
    groups_user = BpGroupsUser.where(user_id: @user.id)
    groups_user.each do |group_user|
      next unless group_user.active?(lms_time)
      rules_vacations = group_user.bp_group.bp_general_rules_vacations
      rules_vacations.each do |rule_vacation|
        next unless rule_vacation.active?(lms_time)
        @bp_general_rules_vacation = rule_vacation.dup
      end
    end
    @bp_general_rules_vacation.bp_group_id = nil
    @bp_general_rules_vacation.user_id = @user.id
  end

  def create_by_user
    general_rules_vacation = complete_create(params[:bp_general_rules_vacation], nil, params[:user_id])
    if general_rules_vacation.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_created')
      redirect_to bp_admin_general_rules_vacation_index_by_user_manage_path(general_rules_vacation.user)
    else
      @bp_general_rules_vacation = general_rules_vacation
      @user = general_rules_vacation.user
      render action: 'new_by_user'
    end
  end

  def edit_by_user
    @bp_general_rules_vacation = BpGeneralRulesVacation.find(params[:bp_general_rules_vacation_id])
  end

  def update_by_user
    general_rules_vacation = complete_update(params[:bp_general_rules_vacation], params[:bp_general_rules_vacation_id])
    if general_rules_vacation.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_changed')
      redirect_to bp_admin_general_rules_vacation_index_by_user_manage_path(general_rules_vacation.user)
    else
      @bp_general_rules_vacation = general_rules_vacation
      render action: 'edit_by_user'
    end
  end

  def new
    @bp_general_rules_vacation = BpGeneralRulesVacation.new(bp_group_id: params[:bp_group_id])
    @bp_group = BpGroup.find(params[:bp_group_id])
  end

  def create
    general_rules_vacations = []
    params[:select2_groups].each do |group_id|
      general_rules_vacations << complete_create(params[:bp_general_rules_vacation], group_id, nil)
    end

    validation = true
    general_rules_vacations.each do |general_rule_vacation|
      next if general_rule_vacation.valid?
      validation = false
    end

    if validation
      general_rules_vacations.each { |general_rule_vacation| general_rule_vacation.save }
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_created')
      redirect_to bp_admin_group_manage_path(params[:bp_group_id])
    else
      general_rules_vacation = complete_create(params[:bp_general_rules_vacation], params[:bp_group_id], nil)
      general_rules_vacation.valid?
      @bp_general_rules_vacation = general_rules_vacation
      @bp_group = general_rules_vacation.bp_group
      render action: 'new'
    end
  end

  def edit
    @bp_general_rules_vacation = BpGeneralRulesVacation.find(params[:bp_general_rules_vacation_id])
    @bp_group = BpGroup.find(params[:bp_group_id])
  end

  def update
    general_rules_vacation = complete_update(params[:bp_general_rules_vacation], params[:bp_general_rules_vacation_id])
    group = BpGroup.find(params[:bp_group_id])
    if general_rules_vacation.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_changed')
      redirect_to bp_admin_group_manage_path(group)
    else
      @bp_general_rules_vacation = general_rules_vacation
      @bp_group = group
      render action: 'edit'
    end
  end

  def new_warning
    @bp_rules_warning = BpRulesWarning.new(bp_general_rules_vacation_id: params[:bp_general_rules_vacation_id])
  end

  def create_warning
    warning = warning_complete_create(params[:bp_rules_warning], params[:bp_general_rules_vacation_id])
    if warning.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_created')
      redirect_to bp_admin_general_rules_vacation_edit_path(warning.bp_general_rules_vacation.bp_group, warning.bp_general_rules_vacation)
    else
      @bp_rules_warning = warning
      render action: 'new_warning'
    end
  end

  def edit_warning
    @bp_rules_warning = BpRulesWarning.find(params[:bp_rules_warning_id])
  end

  def update_waning
    warning = warning_complete_update(params[:bp_rules_warning], params[:bp_rules_warning_id])
    if warning.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_changed')
      redirect_to bp_admin_general_rules_vacation_edit_path(warning.bp_general_rules_vacation.bp_group, warning.bp_general_rules_vacation)
    else
      @bp_rules_warning = warning
      render action: 'edit_warning'
    end
  end

  def deactivate_warning
    warning = warning_complete_update({active: false}, params[:bp_rules_warning_id])
    if warning.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_deleted')
      redirect_to bp_admin_general_rules_vacation_edit_path(warning.bp_general_rules_vacation.bp_group, warning.bp_general_rules_vacation)
    else
      @bp_rules_warning = warning
      render action: 'edit_warning'
    end
  end

  def new_progressive_day
    @bp_progressive_day = BpProgressiveDay.new(bp_general_rules_vacation_id: params[:bp_general_rules_vacation_id])
  end

  def create_progressive_day
    bp_progressive_day = progressive_day_complete_create(params[:bp_progressive_day], params[:bp_general_rules_vacation_id])
    if bp_progressive_day.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_created')
      redirect_to bp_admin_general_rules_vacation_edit_path(bp_progressive_day.bp_general_rules_vacation.bp_group, bp_progressive_day.bp_general_rules_vacation)
    else
      @bp_progressive_day = bp_progressive_day
      render action: 'edit_progressive_day'
    end
  end

  def edit_progressive_day
    @bp_progressive_day = BpProgressiveDay.find(params[:bp_progressive_day_id])
  end

  def update_progressive_day
    bp_progressive_day = progressive_day_complete_update(params[:bp_progressive_day], params[:bp_progressive_day_id])
    if bp_progressive_day.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_changed')
      redirect_to bp_admin_general_rules_vacation_edit_path(bp_progressive_day.bp_general_rules_vacation.bp_group, bp_progressive_day.bp_general_rules_vacation)
    else
      @bp_progressive_day = bp_progressive_day
      render action: 'edit_progressive_day'
    end
  end

  def deactivate_progressive_day
    bp_progressive_day = progressive_day_complete_update({active: false}, params[:bp_progressive_day_id])
    if bp_progressive_day.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_deleted')
      redirect_to bp_admin_general_rules_vacation_edit_path(bp_progressive_day.bp_general_rules_vacation.bp_group, bp_progressive_day.bp_general_rules_vacation)
    else
      @bp_progressive_day = bp_progressive_day
      render action: 'edit_progressive_day'
    end
  end

  def massive_upload_progressive_days
    @bp_general_rules_vacation = BpGeneralRulesVacation.find(params[:bp_general_rules_vacation_id])
  end

  def massive_create_upload_progressive_days
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      @bp_general_rules_vacation = BpGeneralRulesVacation.find(params[:bp_general_rules_vacation_id])
      if its_safe_progressive_days file
        flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'massive_upload_progressive_days'
  end

  private

  def complete_create(params, group_id, user_id)
    general_rules_vacation = BpGeneralRulesVacation.new(params)
    general_rules_vacation.bp_group_id = group_id if group_id
    general_rules_vacation.user_id = user_id if user_id
    general_rules_vacation.registered_by_admin_id = admin_connected.id
    general_rules_vacation.registered_at = lms_time
    return general_rules_vacation unless general_rules_vacation.until
    general_rules_vacation.deactivated_by_admin_id = admin_connected.id
    general_rules_vacation.deactivated_at = lms_time
    return general_rules_vacation
  end

  def complete_update(params, general_rules_vacation_id)
    general_rules_vacation = BpGeneralRulesVacation.find(general_rules_vacation_id)
    general_rules_vacation.assign_attributes(params)
    return general_rules_vacation if !params[:until] || (params[:until] && params[:until].blank?)
    general_rules_vacation.deactivated_by_admin_id = admin_connected.id
    general_rules_vacation.deactivated_at = lms_time
    return general_rules_vacation
  end

  def warning_complete_create(params, general_rules_vacation_id)
    warning = BpRulesWarning.new(params)
    warning.bp_general_rules_vacation_id = general_rules_vacation_id
    warning.registered_at = lms_time
    warning.registered_by_admin_id = admin_connected.id
    return warning
  end

  def warning_complete_update(params, warning_id)
    warning = BpRulesWarning.find(warning_id)
    active = warning.active
    warning.assign_attributes(params)
    return warning if warning.active == active
    warning.deactivated_at = warning.active ? nil : lms_time
    warning.deactivated_by_admin_id = warning.active ? nil : admin_connected.id
    return warning
  end

  def progressive_day_complete_create(params, general_rules_vacation_id)
    progressive_day = BpProgressiveDay.new(params)
    progressive_day.bp_general_rules_vacation_id = general_rules_vacation_id
    progressive_day.registered_at = lms_time
    progressive_day.registered_by_admin_id = admin_connected.id
    return progressive_day
  end

  def progressive_day_complete_update(params, progressive_day_id)
    progressive_day = BpProgressiveDay.find(progressive_day_id)
    active = progressive_day.active
    progressive_day.assign_attributes(params)
    return progressive_day if progressive_day.active == active
    progressive_day.deactivated_at = progressive_day.active ? nil : lms_time
    progressive_day.deactivated_by_admin_id = progressive_day.active ? nil : admin_connected.id
    return progressive_day
  end

  def verify_excel(params)
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
    return error_message unless params
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
    return error_message unless File.extname(params['datafile'].original_filename) == '.xlsx'
    error_message = ''
    return error_message
  end

  def get_temporal_file(params)
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal
    path = File.join(directorio_temporal, nombre_temporal)
    File.open(path, 'wb') { |f| f.write(params['datafile'].read) }
    return archivo_temporal
  end
end
