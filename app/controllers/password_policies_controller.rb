class PasswordPoliciesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /password_policies
  # GET /password_policies.json
  def index
    @password_policies = PasswordPolicy.all
  end

  # GET /password_policies/1
  # GET /password_policies/1.json
  def show
    @password_policy = PasswordPolicy.find(params[:id])
  end

  # GET /password_policies/new
  # GET /password_policies/new.json
  def new
    @password_policy = PasswordPolicy.new
    @password_policy.password_rules.build
  end

  # GET /password_policies/1/edit
  def edit
    @password_policy = PasswordPolicy.find(params[:id])
  end

  # POST /password_policies
  # POST /password_policies.json
  def create
    @password_policy = PasswordPolicy.new(params[:password_policy])
    # valores por defecto al no permitir su administración
    @password_policy.use_in_exa = true
    @password_policy.use_in_job_market = false
    ######################################################

    if @password_policy.save
      flash[:success] = 'La Polítita de seguridad de contraseña fue creada correctamente'
      redirect_to password_policies_path
    else
      flash[:danger] = 'La Polítita de seguridad de contraseña no pudo ser creada correctamente'
      render action: 'new'
    end
  end

  # PUT /password_policies/1
  # PUT /password_policies/1.json
  def update
    @password_policy = PasswordPolicy.find(params[:id])

    if @password_policy.update_attributes(params[:password_policy])
      flash[:success] = 'La Polítita de seguridad de contraseña fue actualizada correctamente'
      redirect_to password_policies_path
    else
      flash[:danger] = 'La Polítita de securidad de contraseña no pudo ser actualizada correctamente'
      render action: 'edit'
    end

  end

  # DELETE /password_policies/1
  # DELETE /password_policies/1.json
  def destroy
    @password_policy = PasswordPolicy.find(params[:id])
    @password_policy.destroy

    flash[:success] = 'La Polítita de seguridad de contraseña fue eliminada correctamente'
    redirect_to password_policies_path
  end
end
