class HrProcessEvaluationQsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_process = @hr_process_evaluation.hr_process
    #@hr_process_evaluation_qs = @hr_process_evaluation.questions_nivel_1


  end

  def carga_preguntas

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_data archivo_temporal, @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type, params[:hr_process_evaluation_q][:hr_process_level_id]

        if @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.master_evaluation.update_file_error')

          @hr_process = @hr_process_evaluation.hr_process
          @hr_process_evaluation_qs = @hr_process_evaluation.hr_process_evaluation_qs

          render 'index'
        else

          if @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.from_jobs



          else

            save_questions archivo_temporal, @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type, params[:hr_process_evaluation_q][:hr_process_level_id]

          end



          flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

          redirect_to hr_process_evaluation_qs_path @hr_process_evaluation
        end

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

        @hr_process = @hr_process_evaluation.hr_process
        @hr_process_evaluation_qs = @hr_process_evaluation.hr_process_evaluation_qs

        render 'index'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_evaluation_qs = @hr_process_evaluation.hr_process_evaluation_qs

      render 'index'

    end

  end

  def index_manual

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_process = @hr_process_evaluation.hr_process
    #@hr_process_evaluation_qs = @hr_process_evaluation.questions_nivel_1


  end

  def carga_preguntas_manual

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_process = @hr_process_evaluation.hr_process
    @hr_process_evaluation_qs = @hr_process_evaluation.hr_process_evaluation_qs


    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }


            save_questions_manual archivo_temporal, @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type


          flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

          redirect_to hr_process_evaluation_qs_manual_path @hr_process_evaluation

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')



        render 'index_manual'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')


      render 'index_manual'

    end

  end

  def carga_preguntas_fj

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])

    @hr_process = @hr_process_evaluation.hr_process

    JJob.where('current = ?', true).each do |j_job|

      hr_process_level = @hr_process.hr_process_levels.where('nombre = ?',j_job.id).first

      if hr_process_level

        hr_process_level.hr_process_evaluation_qs.where('hr_process_evaluation_id = ?', @hr_process_evaluation.id).destroy_all

        j_job.j_functions.where('main_function = ?', true).each do |j_function|

          hr_process_evaluation_q = HrProcessEvaluationQ.new

          hr_process_evaluation_q.texto = j_function.description
          hr_process_evaluation_q.peso = 1
          hr_process_evaluation_q.comentario = false
          hr_process_evaluation_q.discrete_display_in_reports = false
          hr_process_evaluation_q.hr_process_evaluation = @hr_process_evaluation
          hr_process_evaluation_q.hr_evaluation_type_element = @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.find_by_nivel(1)
          hr_process_evaluation_q.hr_process_level_id = hr_process_level.id

          if hr_process_evaluation_q.save


            alts = params[:structure_alts].split(';')

            alts.each do |alt_tmp|

              alt = alt_tmp.split(':')

              hr_process_evaluation_a = HrProcessEvaluationA.new
              hr_process_evaluation_a.valor = alt[0].to_i
              hr_process_evaluation_a.texto = alt[1]

              hr_process_evaluation_a.hr_process_evaluation_q = hr_process_evaluation_q

              hr_process_evaluation_a.save



            end



          end

        end

      end

    end

    redirect_to hr_process_evaluation_qs_path @hr_process_evaluation

  end

  def carga_preguntas_fj2

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])

    @hr_process = @hr_process_evaluation.hr_process

    JJob.where('current = ?', true).each do |j_job|

      hr_process_level = @hr_process.hr_process_levels.where('nombre = ?',j_job.id).first

      if hr_process_level

        hr_process_level.hr_process_evaluation_qs.where('hr_process_evaluation_id = ?', @hr_process_evaluation.id).destroy_all

        j_job.j_job_level.j_generic_skills.each do |j_generic_skill|

          hr_process_evaluation_q = HrProcessEvaluationQ.new

          hr_process_evaluation_q.texto = CGI.unescapeHTML('<strong>'+j_generic_skill.name+'</strong><br><br>'+j_generic_skill.description)
          hr_process_evaluation_q.peso = 1
          hr_process_evaluation_q.comentario = false
          hr_process_evaluation_q.discrete_display_in_reports = false
          hr_process_evaluation_q.hr_process_evaluation = @hr_process_evaluation
          hr_process_evaluation_q.hr_evaluation_type_element = @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.find_by_nivel(1)
          hr_process_evaluation_q.hr_process_level_id = hr_process_level.id

          if hr_process_evaluation_q.save


            alts = params[:structure_alts].split(';')

            alts.each do |alt_tmp|

              alt = alt_tmp.split(':')

              hr_process_evaluation_a = HrProcessEvaluationA.new
              hr_process_evaluation_a.valor = alt[0].to_i
              hr_process_evaluation_a.texto = alt[1]

              hr_process_evaluation_a.hr_process_evaluation_q = hr_process_evaluation_q

              hr_process_evaluation_a.save



            end



          end

        end

      end

    end

    redirect_to hr_process_evaluation_qs_path @hr_process_evaluation

  end

  # GET /hr_process_evaluation_qs/1
  # GET /hr_process_evaluation_qs/1.json
  def show
    @hr_process_evaluation_q = HrProcessEvaluationQ.find(params[:id])

    respond_to do |format|
      format.html # managege.html.erb
      format.json { render json: @hr_process_evaluation_q }
    end
  end

  # GET /hr_process_evaluation_qs/new
  # GET /hr_process_evaluation_qs/new.json
  def new
    @hr_process_evaluation_q = HrProcessEvaluationQ.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @hr_process_evaluation_q }
    end
  end

  # GET /hr_process_evaluation_qs/1/edit
  def edit
    @hr_process_evaluation_q = HrProcessEvaluationQ.find(params[:id])
  end

  # POST /hr_process_evaluation_qs
  # POST /hr_process_evaluation_qs.json
  def create
    @hr_process_evaluation_q = HrProcessEvaluationQ.new(params[:hr_process_evaluation_q])

    respond_to do |format|
      if @hr_process_evaluation_q.save
        format.html { redirect_to @hr_process_evaluation_q, notice: 'Hr process evaluation q was successfully created.' }
        format.json { render json: @hr_process_evaluation_q, status: :created, location: @hr_process_evaluation_q }
      else
        format.html { render action: "new" }
        format.json { render json: @hr_process_evaluation_q.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /hr_process_evaluation_qs/1
  # PUT /hr_process_evaluation_qs/1.json
  def update
    @hr_process_evaluation_q = HrProcessEvaluationQ.find(params[:id])

    respond_to do |format|
      if @hr_process_evaluation_q.update_attributes(params[:hr_process_evaluation_q])
        format.html { redirect_to @hr_process_evaluation_q, notice: 'Hr process evaluation q was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @hr_process_evaluation_q.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hr_process_evaluation_qs/1
  # DELETE /hr_process_evaluation_qs/1.json
  def destroy
    @hr_process_evaluation_q = HrProcessEvaluationQ.find(params[:id])
    @hr_process_evaluation_q.destroy

    respond_to do |format|
      format.html { redirect_to hr_process_evaluation_qs_url }
      format.json { head :no_content }
    end
  end

  private

    def check_data(archivo_temporal, hr_evaluation_type, hr_process_level_id)


      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      archivo = RubyXL::Parser.parse archivo_temporal

      if archivo.worksheets[0].sheet_name == 'preguntas'

        hr_process_level_id = nil if hr_process_level_id == ''

        data = archivo.worksheets[0].extract_data

        n_elementos = hr_evaluation_type.hr_evaluation_type_elements.length

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            #0 texto
            #1 peso
            #2 comentario
            #3 discreto_para_reportes

            #4 valor 1
            #5 texto 1
            #6 valor 2
            #7 texto 2
            #8 valor 3
            #9 texto 3

            #@lineas_error.push index+1

            hr_process_evaluation_q = nil

            hr_process_evaluation_q_padre = nil

            (0..n_elementos-1).each do |n_elemento|


              pos_texto = 0 + 4*n_elemento
              pos_peso = 1 + 4*n_elemento
              pos_comentario = 2 + 4*n_elemento
              pos_discreto = 3 + 4*n_elemento

              hr_process_evaluation_q = @hr_process_evaluation.question(hr_process_evaluation_q_padre.nil? ? nil : hr_process_evaluation_q_padre.id ,hr_process_level_id, CGI.unescapeHTML(fila[pos_texto].to_s))


              unless hr_process_evaluation_q

                hr_process_evaluation_q = HrProcessEvaluationQ.new

                hr_process_evaluation_q.texto = CGI.unescapeHTML fila[pos_texto].to_s
                hr_process_evaluation_q.peso = fila[pos_peso]
                hr_process_evaluation_q.comentario = fila[pos_comentario] =='no' ? false : true
                hr_process_evaluation_q.discrete_display_in_reports = fila[pos_discreto] =='discreto' ? true : false
                hr_process_evaluation_q.hr_process_evaluation = @hr_process_evaluation
                hr_process_evaluation_q.hr_evaluation_type_element = hr_evaluation_type.hr_evaluation_type_elements.find_by_nivel(n_elemento+1)
                hr_process_evaluation_q.hr_process_level_id = hr_process_level_id
                hr_process_evaluation_q.hr_process_evaluation_q = hr_process_evaluation_q_padre


                unless hr_process_evaluation_q.valid?

                  hr_process_evaluation_q.save

                  @lineas_error.push index+1
                  @lineas_error_messages.push hr_process_evaluation_q.errors.full_messages

                end


              end

              hr_process_evaluation_q_padre = hr_process_evaluation_q



            end

            pos_valores = 4*(n_elementos)

            check_valor = true

            hr_process_evaluation_a = nil

            while fila[pos_valores]
              #puts fila[pos_valores]

              if check_valor

                hr_process_evaluation_a = HrProcessEvaluationA.new
                hr_process_evaluation_a.valor = fila[pos_valores]

                check_valor = false

              else

                hr_process_evaluation_a.texto = CGI.unescapeHTML fila[pos_valores].to_s
                check_valor = true

                unless hr_process_evaluation_a.valid?

                  hr_process_evaluation_a.save

                  @lineas_error.push index+1
                  @lineas_error_messages.push hr_process_evaluation_a.errors.full_messages

                end



              end

              pos_valores += 1

            end

          end

        end

      else

        @lineas_error.push 'hoja'
        @lineas_error_messages.push ['Nombre de hoja incorrecto']


      end



    end

  def save_questions_manual(archivo_temporal, hr_evaluation_type)

    archivo = RubyXL::Parser.parse archivo_temporal



      @hr_process_evaluation.hr_process_evaluation_manual_qs.each do |hr_p_e_q|

        #hr_p_e_q.destroy

      end

      #@hr_process_evaluation.hr_process_evaluation_q_classifications.destroy_all

      data = archivo.worksheets[0].extract_data

      data.each_with_index do |fila, index|

        if index > 0 && fila[0]

          #0 codigo usuario
          #1 pilar
          #2 objetivo
          #3 indicador
          #4 unidad de medida
          #5 peso
          #6 tipo de indicador

          #7 valor 1
          #8 logro 1

          user = User.where('codigo = ?',fila[0]).first

          if user

            hr_process_user = @hr_process.hr_process_users.where('user_id = ?', user.id).first

            if hr_process_user

              #hr_process_user.hr_process_evaluation_manual_qs.destroy_all


              if hr_evaluation_type.hr_evaluation_type_elements.first.tiene_clasificacion

                hr_process_evaluation_q_clas = @hr_process_evaluation.hr_process_evaluation_q_classifications.where('nombre = ?', fila[1]).first

                unless hr_process_evaluation_q_clas

                  hr_process_evaluation_q_clas = HrProcessEvaluationQClassification.new
                  hr_process_evaluation_q_clas.nombre = fila[1]
                  hr_process_evaluation_q_clas.orden = 1
                  hr_process_evaluation_q_clas.hr_process_evaluation = @hr_process_evaluation
                  hr_process_evaluation_q_clas.hr_evaluation_type_element = hr_evaluation_type.hr_evaluation_type_elements.first
                  hr_process_evaluation_q_clas.save

                end

              end

              hr_process_evaluation_manual_q = nil

              hr_process_evaluation_manual_q_padre = nil

              hr_process_evaluation_manual_q = @hr_process_evaluation.manual_questions_nivel_1(hr_process_user).where('texto = ?', CGI.unescapeHTML(fila[2].to_s)).first

              unless hr_process_evaluation_manual_q

                hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.new

                hr_process_evaluation_manual_q.texto = CGI.unescapeHTML fila[2].to_s
                hr_process_evaluation_manual_q.confirmado = true
                hr_process_evaluation_manual_q.hr_process_evaluation = @hr_process_evaluation
                hr_process_evaluation_manual_q.hr_evaluation_type_element = hr_evaluation_type.hr_evaluation_type_elements.first
                hr_process_evaluation_manual_q.hr_process_user = hr_process_user

                if hr_evaluation_type.hr_evaluation_type_elements.first.tiene_clasificacion

                  hr_process_evaluation_manual_q.hr_process_evaluation_q_classification = hr_process_evaluation_q_clas

                end

                hr_process_evaluation_manual_q.save

              end

              hr_process_evaluation_manual_q_padre = hr_process_evaluation_manual_q

              hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.new

              hr_process_evaluation_manual_q.texto = CGI.unescapeHTML fila[3].to_s
              hr_process_evaluation_manual_q.peso = fila[5]
              hr_process_evaluation_manual_q.hr_process_evaluation_manual_q = hr_process_evaluation_manual_q_padre
              hr_process_evaluation_manual_q.confirmado = true

              hr_process_evaluation_manual_q.hr_process_evaluation = @hr_process_evaluation
              hr_process_evaluation_manual_q.hr_evaluation_type_element = hr_evaluation_type.hr_evaluation_type_elements.last
              hr_process_evaluation_manual_q.hr_process_user = hr_process_user
              hr_process_evaluation_manual_q.descripcion_indicador = CGI.unescapeHTML fila[3].to_s
              hr_process_evaluation_manual_q.unidad_medida_indicador = fila[4]
              hr_process_evaluation_manual_q.meta_indicador = 100

              tipo_indicador = case fila[6]
                                 when 'Rango'
                                   4
                                 when 'Discreto'
                                   5
                               end
              hr_process_evaluation_manual_q.tipo_indicador = tipo_indicador

              if hr_process_evaluation_manual_q.save



                pos_valores = 7

                check_valor = true

                pe_question_rank = nil

                while fila[pos_valores]


                    pe_question_rank = hr_process_evaluation_manual_q.pe_question_ranks.build

                    pe_question_rank.percentage = fila[pos_valores+1]
                    pe_question_rank.goal = 100
                    if hr_process_evaluation_manual_q.tipo_indicador == 4
                      pe_question_rank.goal = fila[pos_valores]
                    elsif hr_process_evaluation_manual_q.tipo_indicador == 5
                      pe_question_rank.discrete_goal = fila[pos_valores]
                    end

                    pe_question_rank.save

                  pos_valores += 2

                end

                hr_process_evaluation_manual_q.reload

                hr_process_evaluation_manual_q.set_goal_rank
                hr_process_evaluation_manual_q.save

              else

                #puts hr_process_evaluation_manual_q.errors.full_messages

              end


            end

          end

        end

      end


  end

    def save_questions(archivo_temporal, hr_evaluation_type, hr_process_level_id)

      archivo = RubyXL::Parser.parse archivo_temporal

      if archivo.worksheets[0].sheet_name == 'preguntas'

        hr_process_level_id = nil if hr_process_level_id == ''

        @hr_process_evaluation.hr_process_evaluation_qs.find_all_by_hr_process_level_id(hr_process_level_id).each do |hr_p_e_q|

          hr_p_e_q.destroy

        end

        n_elementos = hr_evaluation_type.hr_evaluation_type_elements.length

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            #0 texto
            #1 peso
            #2 comentario
            #3 discreto

            #3 valor 1
            #4 texto 1
            #5 valor 2
            #6 texto 2
            #7 valor 3
            #8 texto 3

            hr_process_evaluation_q = nil

            hr_process_evaluation_q_padre = nil

            (0..n_elementos-1).each do |n_elemento|

              pos_texto = 0 + 4*n_elemento
              pos_peso = 1 + 4*n_elemento
              pos_comentario = 2 + 4*n_elemento
              pos_discreto = 3 + 4*n_elemento

              hr_process_evaluation_q = @hr_process_evaluation.question(hr_process_evaluation_q_padre.nil? ? nil : hr_process_evaluation_q_padre.id ,hr_process_level_id, CGI.unescapeHTML(fila[pos_texto].to_s))

              unless hr_process_evaluation_q

                hr_process_evaluation_q = HrProcessEvaluationQ.new

                hr_process_evaluation_q.texto = CGI.unescapeHTML fila[pos_texto].to_s
                hr_process_evaluation_q.peso = fila[pos_peso]
                hr_process_evaluation_q.comentario = fila[pos_comentario] =='no' ? false : true
                hr_process_evaluation_q.discrete_display_in_reports = fila[pos_discreto] =='discreto' ? true : false
                hr_process_evaluation_q.hr_process_evaluation = @hr_process_evaluation
                hr_process_evaluation_q.hr_evaluation_type_element = hr_evaluation_type.hr_evaluation_type_elements.find_by_nivel(n_elemento+1)
                hr_process_evaluation_q.hr_process_level_id = hr_process_level_id
                hr_process_evaluation_q.hr_process_evaluation_q = hr_process_evaluation_q_padre
                hr_process_evaluation_q.save

              end

              hr_process_evaluation_q_padre = hr_process_evaluation_q

            end

            pos_valores = 4*(n_elementos)

            check_valor = true

            hr_process_evaluation_a = nil

            while fila[pos_valores]

              if check_valor

                hr_process_evaluation_a = HrProcessEvaluationA.new
                hr_process_evaluation_a.valor = fila[pos_valores]

                check_valor = false

              else

                hr_process_evaluation_a.texto = CGI.unescapeHTML fila[pos_valores].to_s
                check_valor = true

                hr_process_evaluation_a.hr_process_evaluation_q = hr_process_evaluation_q

                hr_process_evaluation_a.save

              end

              pos_valores += 1

            end

          end

        end

      end


    end

    def save_alts(archivo_temporal, hr_evaluation_type, hr_process_level_id)

      archivo = RubyXL::Parser.parse archivo_temporal

      if archivo.worksheets[0].sheet_name == 'preguntas'

        hr_process_level_id = nil if hr_process_level_id == ''

        @hr_process_evaluation.hr_process_evaluation_qs.find_all_by_hr_process_level_id(hr_process_level_id).each do |hr_p_e_q|

          hr_p_e_q.destroy

        end

        n_elementos = hr_evaluation_type.hr_evaluation_type_elements.length

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            #0 texto
            #1 peso
            #2 comentario
            #3 discreto

            #3 valor 1
            #4 texto 1
            #5 valor 2
            #6 texto 2
            #7 valor 3
            #8 texto 3

            hr_process_evaluation_q = nil

            hr_process_evaluation_q_padre = nil

            (0..n_elementos-1).each do |n_elemento|

              pos_texto = 0 + 4*n_elemento
              pos_peso = 1 + 4*n_elemento
              pos_comentario = 2 + 4*n_elemento
              pos_discreto = 3 + 4*n_elemento

              hr_process_evaluation_q = @hr_process_evaluation.question(hr_process_evaluation_q_padre.nil? ? nil : hr_process_evaluation_q_padre.id ,hr_process_level_id, CGI.unescapeHTML(fila[pos_texto].to_s))

              unless hr_process_evaluation_q

                hr_process_evaluation_q = HrProcessEvaluationQ.new

                hr_process_evaluation_q.texto = CGI.unescapeHTML fila[pos_texto].to_s
                hr_process_evaluation_q.peso = fila[pos_peso]
                hr_process_evaluation_q.comentario = fila[pos_comentario] =='no' ? false : true
                hr_process_evaluation_q.discrete_display_in_reports = fila[pos_discreto] =='discreto' ? true : false
                hr_process_evaluation_q.hr_process_evaluation = @hr_process_evaluation
                hr_process_evaluation_q.hr_evaluation_type_element = hr_evaluation_type.hr_evaluation_type_elements.find_by_nivel(n_elemento+1)
                hr_process_evaluation_q.hr_process_level_id = hr_process_level_id
                hr_process_evaluation_q.hr_process_evaluation_q = hr_process_evaluation_q_padre
                hr_process_evaluation_q.save

              end

              hr_process_evaluation_q_padre = hr_process_evaluation_q

            end

            pos_valores = 4*(n_elementos)

            check_valor = true

            hr_process_evaluation_a = nil

            while fila[pos_valores]

              if check_valor

                hr_process_evaluation_a = HrProcessEvaluationA.new
                hr_process_evaluation_a.valor = fila[pos_valores]

                check_valor = false

              else

                hr_process_evaluation_a.texto = CGI.unescapeHTML fila[pos_valores].to_s
                check_valor = true

                hr_process_evaluation_a.hr_process_evaluation_q = hr_process_evaluation_q

                hr_process_evaluation_a.save

              end

              pos_valores += 1

            end

          end

        end

      end


    end


end
