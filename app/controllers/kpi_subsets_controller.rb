class KpiSubsetsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def new

    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
    @kpi_subset = KpiSubset.new

  end

  def create
    @kpi_subset = KpiSubset.new(params[:kpi_subset])

    if @kpi_subset.save
      flash[:success] =  t('activerecord.success.model.kpi_dashboard_subset.create_ok')
      redirect_to @kpi_subset.kpi_dashboard
    else
      @kpi_dashboard = @kpi_subset.kpi_dashboard
      render action: 'new'
    end
  end

  def edit
    @kpi_subset = KpiSubset.find(params[:id])
    @kpi_dashboard = @kpi_subset.kpi_dashboard
  end

  def update
    @kpi_subset = KpiSubset.find(params[:id])

    if @kpi_subset.update_attributes(params[:kpi_subset])
      flash[:success] =  t('activerecord.success.model.kpi_dashboard_subset.update_ok')
      redirect_to @kpi_subset.kpi_dashboard
    else
      @kpi_dashboard = @kpi_subset.kpi_dashboard
      render action: 'edit'
    end

  end

  def destroy

    kpi_subset = KpiSubset.find(params[:id])
    kpi_dashboard = kpi_subset.kpi_dashboard
    kpi_subset.destroy

    flash[:success] =  t('activerecord.success.model.kpi_dashboard_subset.delete_ok')
    redirect_to kpi_dashboard

  end


  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'kpis', true).first

    unless admin_logged_in? || (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

end
