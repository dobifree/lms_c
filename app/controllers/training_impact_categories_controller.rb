class TrainingImpactCategoriesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @training_impact_categories = TrainingImpactCategory.all
  end


  def show
    @training_impact_category = TrainingImpactCategory.find(params[:id])
  end


  def new
    @training_impact_category = TrainingImpactCategory.new
  end


  def edit
    @training_impact_category = TrainingImpactCategory.find(params[:id])
  end

  def delete
    @training_impact_category = TrainingImpactCategory.find(params[:id])
  end


  def create
    @training_impact_category = TrainingImpactCategory.new(params[:training_impact_category])


      if @training_impact_category.save
        flash[:success] = t('activerecord.success.model.training_impact_category.create_ok')
        redirect_to training_impact_categories_path
      else
        render action: 'new'
      end

  end


  def update
    @training_impact_category = TrainingImpactCategory.find(params[:id])


      if @training_impact_category.update_attributes(params[:training_impact_category])
        flash[:success] = t('activerecord.success.model.training_impact_category.update_ok')
        redirect_to training_impact_categories_path
      else
        render action: 'edit'
      end

  end



  def destroy

    @training_impact_category = TrainingImpactCategory.find(params[:id])

    if verify_recaptcha

      if @training_impact_category.destroy
        flash[:success] = t('activerecord.success.model.training_impact_category.delete_ok')
        redirect_to training_impact_categories_path
      else
        flash[:danger] = t('activerecord.success.model.training_impact_category.delete_error')
        redirect_to delete_training_impact_category_path(@training_impact_category)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.training_impact_category.captcha_error')
      redirect_to delete_training_impact_category_path(@training_impact_category)

    end

  end
end
