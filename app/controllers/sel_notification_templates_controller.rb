class SelNotificationTemplatesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /sel_notification_templates
  # GET /sel_notification_templates.json
  def index
    @sel_notification_templates = SelNotificationTemplate.all
  end

  # GET /sel_notification_templates/1
  # GET /sel_notification_templates/1.json
  def show
    @sel_notification_template = SelNotificationTemplate.find(params[:id])
  end

  # GET /sel_notification_templates/new
  # GET /sel_notification_templates/new.json
  def new
    @sel_notification_template = SelNotificationTemplate.new
  end

  # GET /sel_notification_templates/1/edit
  def edit
    @sel_notification_template = SelNotificationTemplate.find(params[:id])
  end

  # POST /sel_notification_templates
  # POST /sel_notification_templates.json
  def create
    @sel_notification_template = SelNotificationTemplate.new(params[:sel_notification_template])
    if @sel_notification_template.save
      flash[:success] = 'La plantilla de notificación fue creada correctamente'
      redirect_to sel_notification_templates_path
    else
      flash.now[:danger] = 'La plantilla de notificación no fue creada correctamente'
      render action: "new"
    end
  end

  # PUT /sel_notification_templates/1
  # PUT /sel_notification_templates/1.json
  def update
    @sel_notification_template = SelNotificationTemplate.find(params[:id])

      if @sel_notification_template.update_attributes(params[:sel_notification_template])
        flash[:success] = 'La plantilla de notificación fue actualizada correctamente'
        redirect_to sel_notification_templates_path
      else
        flash.now[:danger] = 'La plantilla de notificación no fue actualizada correctamente'
        render action: "edit"
      end
  end

  # DELETE /sel_notification_templates/1
  # DELETE /sel_notification_templates/1.json
  def destroy
    @sel_notification_template = SelNotificationTemplate.find(params[:id])
    @sel_notification_template.destroy
    redirect_to sel_notification_templates_path
  end
end
