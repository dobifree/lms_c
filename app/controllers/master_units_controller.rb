class MasterUnitsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @master_units = MasterUnit.all

   #@company = session[:company]

  end

  def show
    @master_unit = MasterUnit.find(params[:id])
  end

  def preview
    @master_unit = MasterUnit.find(params[:id])
   #@company = session[:company]
  end

  def new
    @master_unit = MasterUnit.new
  end

  def edit
    @master_unit = MasterUnit.find(params[:id])
  end

  def upload_file_form
    @master_unit = MasterUnit.find(params[:id])
  end

  def delete
    @master_unit = MasterUnit.find(params[:id])
  end

  def create
  
    @master_unit = MasterUnit.new(params[:master_unit])

    if @master_unit.save
      flash[:success] = t('activerecord.success.model.master_unit.create_ok')
      redirect_to master_units_url
    else
      render action: 'new'
    end
  
  end

  def update
    @master_unit = MasterUnit.find(params[:id])

    if @master_unit.update_attributes(params[:master_unit])
      flash[:success] = t('activerecord.success.model.master_unit.update_ok')
      redirect_to master_units_url
    else
      render action: 'edit'
    end
  end

  def upload_file

    @master_unit = MasterUnit.find(params[:id])

   #@company = session[:company]

    if params[:upload]

      save_file params[:upload], @master_unit, @company

      flash[:success] = t('activerecord.success.model.master_unit.update_file_ok')

      redirect_to master_units_url

    else

      render 'upload_file_form'

    end

  end
  
  def destroy
    
    @master_unit = MasterUnit.find(params[:id])
   
    if verify_recaptcha
    
      if @master_unit.destroy
        flash[:success] = t('activerecord.success.model.master_unit.delete_ok')
        redirect_to master_units_url
      else
        flash[:danger] = t('activerecord.success.model.master_unit.delete_error')
        redirect_to delete_master_unit_path(@master_unit)
      end

    else

      flash.delete(:recaptcha_error)
      
      flash[:danger] = t('activerecord.error.model.master_unit.captcha_error')
      redirect_to delete_master_unit_path(@master_unit)

    end

  end

  private

    def save_file(upload, master_unit, company)

      directory = company.directorio+'/'+company.codigo+'/contenidos/'+master_unit.crypted_name+'/'

      FileUtils.remove_dir directory if File.directory? directory

      FileUtils.mkdir_p directory


      if File.extname(upload['datafile'].original_filename) == '.pdf'

        name = 'mu'+master_unit.numero.to_s+'.pdf'
        path = File.join(directory, name)
        File.open(path, 'wb') { |f| f.write(upload['datafile'].read) }

      elsif File.extname(upload['datafile'].original_filename) == '.zip'

        name = 'mu'+master_unit.id.to_s+'.zip'
        path = File.join(directory, name)
        File.open(path, 'wb') { |f| f.write(upload['datafile'].read) }
        system "unzip -o -q \"#{directory+name}\" -d \"#{directory}\" "
        File.delete directory+name
      end

    end

end
