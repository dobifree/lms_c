class BatchDataCorrectorsController < ApplicationController

  include PeAssessmentHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def cargador_form

  end

  def cargador

    @lineas_error = Array.new
    @lineas_error_detalle = Array.new
    @lineas_error_messages = Array.new

    require 'rubyXL'

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        if @lineas_error && @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.enrollment.update_file_error')
        else
          seduc_data_pe_4_fun archivo_temporal


          flash.now[:success] = 'proceso terminado'

        end

      else

        flash.now[:danger] = t('activerecord.error.model.enrollment.upload_file_no_xlsx')

      end

    else
      flash.now[:danger] = t('activerecord.error.model.enrollment.upload_file_no_file')

    end

    render 'cargador_form'

  end

  def seduc_data_pe_4_fun(archivo_temporal)

    pe_member_rels_array = Array.new
    pe_member_rels_f_array = Array.new

    pe_process = PeProcess.find 7
    pe_evaluation = PeEvaluation.find 23

    archivo = RubyXL::Parser.parse archivo_temporal

    archivo.worksheets[0].extract_data.each_with_index do |fila, index|

      if index > 0

        pe_member = pe_process.pe_member_by_user_code fila[0]
        pe_member_evaluator = pe_process.pe_member_by_user_code fila[2]

        pe_member_rel = pe_member.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

        pe_member_rels_array.push pe_member_rel
        pe_member_rels_f_array[pe_member_rel.id] = fila[8]

        pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_evaluation

        pe_member_evaluated_group = pe_member.pe_member_groups.where('pe_evaluation_id = ?', pe_evaluation.id).first

        pe_group = pe_evaluation.pe_groups.where('name = ?',fila[1]).first

        unless pe_member_evaluated_group

          pe_member_evaluated_group = pe_member.pe_member_groups.build
          pe_member_evaluated_group.pe_evaluation = pe_evaluation
          pe_member_evaluated_group.pe_process = pe_process
          pe_member_evaluated_group.pe_group = pe_group
          pe_member_evaluated_group.save
        end

        unless pe_assessment_evaluation

          pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluations.build
          pe_assessment_evaluation.pe_process = pe_process
          pe_assessment_evaluation.pe_evaluation = pe_evaluation
          pe_assessment_evaluation.last_update = fila[8]
          pe_assessment_evaluation.points = -1
          pe_assessment_evaluation.percentage = -1
          pe_assessment_evaluation.save
        end

        if pe_assessment_evaluation

          pe_question = pe_evaluation.pe_questions.where('pe_questions.description LIKE ? AND pe_group_id = ?', '<strong>'+fila[4]+'</strong>%', pe_group.id).first

          if pe_question

            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question

            unless pe_assessment_question

              pe_assessment_question = pe_assessment_evaluation.pe_assessment_questions.build
              pe_assessment_question.pe_question = pe_question

            end

            pe_alternative = pe_question.pe_alternatives.where('pe_alternatives.description = ?',fila[6]).first

            if pe_alternative

              pe_assessment_question.pe_alternative = pe_alternative
              pe_assessment_question.points = pe_alternative.value

              pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, pe_evaluation.points_to_ohp, pe_evaluation.min_percentage, pe_evaluation.max_percentage

              pe_assessment_question.last_update = fila[8]
              pe_assessment_question.save

            end


          end

        end

      end

    end

    pe_member_rels_array.uniq!

    pe_member_rels_array.each do |pe_member_rel|

      pe_member_rel.finished = true
      pe_member_rel.finished_at = pe_member_rels_f_array[pe_member_rel.id]
      pe_member_rel.valid_evaluator = true
      pe_member_rel.validated_at = nil
      pe_member_rel.save

    end

  end

  def seduc_data_pe_3_corp(archivo_temporal)

    pe_member_rels_array = Array.new
    pe_member_rels_f_array = Array.new

    pe_process = PeProcess.find 7
    pe_evaluation = PeEvaluation.find 22

    archivo = RubyXL::Parser.parse archivo_temporal

    archivo.worksheets[0].extract_data.each_with_index do |fila, index|

      if index > 0

        pe_member = pe_process.pe_member_by_user_code fila[0]
        pe_member_evaluator = pe_process.pe_member_by_user_code fila[1]

        pe_member_rel = pe_member.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

        pe_member_rels_array.push pe_member_rel
        pe_member_rels_f_array[pe_member_rel.id] = fila[7]

        pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_evaluation

        pe_member_evaluated_group = pe_member.pe_member_groups.where('pe_evaluation_id = ?', pe_evaluation.id).first

        unless pe_member_evaluated_group

          pe_member_evaluated_group = pe_member.pe_member_groups.build
          pe_member_evaluated_group.pe_evaluation = pe_evaluation
          pe_member_evaluated_group.pe_process = pe_process
          pe_member_evaluated_group.pe_group_id = 89
          pe_member_evaluated_group.save
        end

        unless pe_assessment_evaluation

          pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluations.build
          pe_assessment_evaluation.pe_process = pe_process
          pe_assessment_evaluation.pe_evaluation = pe_evaluation
          pe_assessment_evaluation.last_update = fila[7]
          pe_assessment_evaluation.points = -1
          pe_assessment_evaluation.percentage = -1
          pe_assessment_evaluation.save
        end

        if pe_assessment_evaluation

          pe_question = pe_evaluation.pe_questions.where('pe_questions.description LIKE ? ', '<strong>'+fila[3]+'</strong>%').first

          if pe_question

            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question

            unless pe_assessment_question

              pe_assessment_question = pe_assessment_evaluation.pe_assessment_questions.build
              pe_assessment_question.pe_question = pe_question

            end

            pe_alternative = pe_question.pe_alternatives.where('pe_alternatives.description = ?',fila[5]).first

            if pe_alternative

              pe_assessment_question.pe_alternative = pe_alternative
              pe_assessment_question.points = pe_alternative.value

              pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, pe_evaluation.points_to_ohp, pe_evaluation.min_percentage, pe_evaluation.max_percentage

              pe_assessment_question.last_update = fila[7]
              pe_assessment_question.save

            end


          end

        end

      end

    end

    pe_member_rels_array.uniq!

    pe_member_rels_array.each do |pe_member_rel|

      pe_member_rel.finished = true
      pe_member_rel.finished_at = pe_member_rels_f_array[pe_member_rel.id]
      pe_member_rel.valid_evaluator = true
      pe_member_rel.validated_at = nil
      pe_member_rel.save

    end

  end

  def seduc_data_pe(archivo_temporal)

    pe_member_rels_array = Array.new
    pe_member_rels_f_array = Array.new

    pe_process = PeProcess.find 8
    pe_evaluation = PeEvaluation.find 20

    archivo = RubyXL::Parser.parse archivo_temporal

    archivo.worksheets[0].extract_data.each_with_index do |fila, index|

      if index > 0

        pe_member = pe_process.pe_member_by_user_code fila[0]

        pe_member_rel = pe_member.pe_member_rel_is_evaluated_by_pe_member pe_member

        pe_member_rels_array.push pe_member_rel
        pe_member_rels_f_array[pe_member_rel.id] = fila[4]

        pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_evaluation

        pe_member_evaluated_group = pe_member.pe_member_groups.where('pe_evaluation_id = ?', pe_evaluation.id).first

        unless pe_member_evaluated_group

          pe_member_evaluated_group = pe_member.pe_member_groups.build
          pe_member_evaluated_group.pe_evaluation = pe_evaluation
          pe_member_evaluated_group.pe_process = pe_process
          pe_member_evaluated_group.pe_group_id = 70
          pe_member_evaluated_group.save
        end

        unless pe_assessment_evaluation

          pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluations.build
          pe_assessment_evaluation.pe_process = pe_process
          pe_assessment_evaluation.pe_evaluation = pe_evaluation
          pe_assessment_evaluation.last_update = fila[4]
          pe_assessment_evaluation.points = -1
          pe_assessment_evaluation.percentage = -1
          pe_assessment_evaluation.save
        end

        if pe_assessment_evaluation

          pe_question = pe_evaluation.pe_questions.where('pe_questions.description LIKE ? ', '<strong>'+fila[1]+'%').first

          if pe_question

            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question

            unless pe_assessment_question

              pe_assessment_question = pe_assessment_evaluation.pe_assessment_questions.build
              pe_assessment_question.pe_question = pe_question

            end

            pe_alternative = pe_question.pe_alternatives.where('pe_alternatives.description = ?',fila[2]).first

            if pe_alternative

              pe_assessment_question.pe_alternative = pe_alternative
              pe_assessment_question.points = pe_alternative.value

              pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, pe_evaluation.points_to_ohp, pe_evaluation.min_percentage, pe_evaluation.max_percentage

              pe_assessment_question.last_update = fila[4]
              pe_assessment_question.save

            end


          end

        end

      end

    end

    pe_member_rels_array.uniq!

    pe_member_rels_array.each do |pe_member_rel|

      pe_member_rel.finished = true
      pe_member_rel.finished_at = pe_member_rels_f_array[pe_member_rel.id]
      pe_member_rel.valid_evaluator = true
      pe_member_rel.validated_at = nil
      pe_member_rel.save

    end

  end

  def seduc_data_pe_2(archivo_temporal)

    pe_member_rels_array = Array.new
    pe_member_rels_f_array = Array.new

    pe_process = PeProcess.find 8
    pe_evaluation = PeEvaluation.find 21

    archivo = RubyXL::Parser.parse archivo_temporal

    archivo.worksheets[0].extract_data.each_with_index do |fila, index|

      if index > 0

        pe_member = pe_process.pe_member_by_user_code fila[0]

        pe_member_rel = pe_member.pe_member_rel_is_evaluated_by_pe_member pe_member

        pe_member_rels_array.push pe_member_rel
        pe_member_rels_f_array[pe_member_rel.id] = fila[6]

        pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_evaluation

        pe_member_evaluated_group = pe_member.pe_member_groups.where('pe_evaluation_id = ?', pe_evaluation.id).first

        pe_group = pe_evaluation.pe_groups.where('name = ?',fila[1]).first

        unless pe_member_evaluated_group

          pe_member_evaluated_group = pe_member.pe_member_groups.build
          pe_member_evaluated_group.pe_evaluation = pe_evaluation
          pe_member_evaluated_group.pe_process = pe_process
          pe_member_evaluated_group.pe_group = pe_group
          pe_member_evaluated_group.save
        end

        unless pe_assessment_evaluation

          pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluations.build
          pe_assessment_evaluation.pe_process = pe_process
          pe_assessment_evaluation.pe_evaluation = pe_evaluation
          pe_assessment_evaluation.last_update = fila[6]
          pe_assessment_evaluation.points = -1
          pe_assessment_evaluation.percentage = -1
          pe_assessment_evaluation.save
        end

        if pe_assessment_evaluation

          pe_question = pe_evaluation.pe_questions.where('pe_questions.description LIKE ? AND pe_group_id = ?', '<strong>'+fila[2]+'</strong>%', pe_group.id).first

          if pe_question

            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question

            unless pe_assessment_question

              pe_assessment_question = pe_assessment_evaluation.pe_assessment_questions.build
              pe_assessment_question.pe_question = pe_question

            end

            pe_alternative = pe_question.pe_alternatives.where('pe_alternatives.description = ?',fila[4]).first

            if pe_alternative

              pe_assessment_question.pe_alternative = pe_alternative
              pe_assessment_question.points = pe_alternative.value

              pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, pe_evaluation.points_to_ohp, pe_evaluation.min_percentage, pe_evaluation.max_percentage

              pe_assessment_question.last_update = fila[6]
              pe_assessment_question.save

            end


          end

        end

      end

    end

    pe_member_rels_array.uniq!

    pe_member_rels_array.each do |pe_member_rel|

      pe_member_rel.finished = true
      pe_member_rel.finished_at = pe_member_rels_f_array[pe_member_rel.id]
      pe_member_rel.valid_evaluator = true
      pe_member_rel.validated_at = nil
      pe_member_rel.save

    end

  end

end
