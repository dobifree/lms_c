class SecuCohadesController < ApplicationController


  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /secu_cohades
  # GET /secu_cohades.json
  def index
    @secu_cohades = SecuCohade.all
  end

  # GET /secu_cohades/1
  # GET /secu_cohades/1.json
  def show
    @secu_cohade = SecuCohade.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @secu_cohade }
    end
  end

  # GET /secu_cohades/new
  # GET /secu_cohades/new.json
  def new
    @secu_cohade = SecuCohade.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @secu_cohade }
    end
  end

  # GET /secu_cohades/1/edit
  def edit
    @secu_cohade = SecuCohade.find(params[:id])
  end

  # POST /secu_cohades
  # POST /secu_cohades.json
  def create
    @secu_cohade = SecuCohade.new(params[:secu_cohade])

    respond_to do |format|
      if @secu_cohade.save
        format.html { redirect_to secu_cohades_url, notice: 'Secu cohade was successfully created.' }
        format.json { render json: @secu_cohade, status: :created, location: @secu_cohade }
      else
        format.html { render action: "new" }
        format.json { render json: @secu_cohade.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /secu_cohades/1
  # PUT /secu_cohades/1.json
  def update
    @secu_cohade = SecuCohade.find(params[:id])

    respond_to do |format|
      if @secu_cohade.update_attributes(params[:secu_cohade])
        format.html { redirect_to secu_cohades_url, notice: 'Secu cohade was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @secu_cohade.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /secu_cohades/1
  # DELETE /secu_cohades/1.json
  def destroy
    @secu_cohade = SecuCohade.find(params[:id])
    @secu_cohade.destroy

    respond_to do |format|
      format.html { redirect_to secu_cohades_url }
      format.json { head :no_content }
    end
  end
end
