class PeCalSessionsController < ApplicationController


  before_filter :authenticate_user
  before_filter :get_data_1, only: [:index, :new]
  before_filter :get_data_1_e, only: [:index_e, :new_e]
  before_filter :get_data_2, only: [:create]
  before_filter :get_data_3, only: [:edit, :update, :destroy]
  before_filter :validate_manager, only: [:index, :new, :create, :edit, :update, :destroy]

  def index
    @pe_cal_sessions = @pe_process.pe_cal_sessions
  end

  def new
    @pe_cal_session = @pe_process.pe_cal_sessions.build
  end

  def index_e
    @pe_cal_sessions = @pe_evaluation.pe_cal_sessions
  end

  def new_e
    @pe_cal_session = @pe_evaluation.pe_cal_sessions.build
  end

  def create

    @pe_cal_session = PeCalSession.new(params[:pe_cal_session])

    if @pe_cal_session.save
      flash[:success] =  t('activerecord.success.model.pe_cal_session.create_ok')
      if @pe_cal_session.pe_evaluation
        redirect_to pe_cal_sessions_list_e_path(@pe_cal_session.pe_evaluation)
      else
        redirect_to pe_cal_sessions_list_path(@pe_process)
      end


    else

      if @pe_cal_session.pe_evaluation
        render action: 'new_e'
      else
        render action: 'new'
      end

    end

  end

  def edit

  end

  def update

    if @pe_cal_session.update_attributes(params[:pe_cal_session])
      flash[:success] =  t('activerecord.success.model.pe_cal_session.update_ok')
      if @pe_cal_session.pe_evaluation
        redirect_to pe_cal_sessions_list_e_path(@pe_cal_session.pe_evaluation)
      else
        redirect_to pe_cal_sessions_list_path(@pe_process)
      end
    else
      render action: 'edit'
    end

  end



  def destroy

    @pe_cal_session.destroy

    flash[:success] =  t('activerecord.success.model.pe_cal_session.delete_ok')

    if @pe_evaluation
      redirect_to pe_cal_sessions_list_e_path(@pe_evaluation)
    else
      redirect_to pe_cal_sessions_list_path(@pe_process)
    end



  end

  private

    def get_data_1
      @pe_process = PeProcess.find(params[:pe_process_id])
    end

    def get_data_1_e
      
      @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
      @pe_process = @pe_evaluation.pe_process

    end

    def get_data_2
      @pe_process = PeProcess.find(params[:pe_cal_session][:pe_process_id]) if params[:pe_cal_session][:pe_process_id]

      if params[:pe_cal_session][:pe_evaluation_id]
        @pe_evaluation = PeEvaluation.find(params[:pe_cal_session][:pe_evaluation_id])
        @pe_process = @pe_evaluation.pe_process
      end
    end

    def get_data_3
      @pe_cal_session = PeCalSession.find(params[:id])
      if @pe_cal_session.pe_evaluation
        @pe_evaluation = @pe_cal_session.pe_evaluation
        @pe_process = @pe_evaluation.pe_process
      else
        @pe_process = @pe_cal_session.pe_process
      end

    end

    def validate_manager
      unless @pe_process.pe_managers.where('user_id = ?', user_connected.id).count > 0
        flash[:danger] = t('security.no_access_generic')
        redirect_to root_path
      end
    end

end
