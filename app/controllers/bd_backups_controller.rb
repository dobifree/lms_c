class BdBackupsController < ApplicationController
  require 'open3'
  BACKUPS_ROOT_PATH = '/var/www/remote-storage/custom_bd_backups'

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /bd_backups
  # GET /bd_backups.json
  def index
    @bd_backups = BdBackup.all
  end

  # GET /bd_backups/1
  # GET /bd_backups/1.json
  def show
    @bd_backup = BdBackup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json {render json: @bd_backup}
    end
  end

  # GET /bd_backups/new
  # GET /bd_backups/new.json
  def new
    @bd_backup = BdBackup.new

    set_database_alternatives

    respond_to do |format|
      format.html # new.html.erb
      format.json {render json: @bd_backup}
    end
  end

  # GET /bd_backups/1/edit
  def edit
    @bd_backup = BdBackup.find(params[:id])
  end

  # POST /bd_backups
  # POST /bd_backups.json
  def create
    @bd_backup = BdBackup.new(params[:bd_backup])

    config, host, password, username = set_connection_config_values

    input_file_name = @bd_backup.file_name
    input_specific_tables = @bd_backup.specific_tables
    file_name_sql = @bd_backup.database + '_' + input_file_name.gsub(' ', '_') + '.sql'
    file_name_tar_gz = file_name_sql + '.tar.gz'
    @bd_backup.file_name = file_name_tar_gz
    @bd_backup.specific_tables = input_specific_tables.join(' ')
    @bd_backup.generated_by_admin = admin_connected
    @bd_backup.generated_at = lms_time

    begin
      @bd_backup.transaction do
        generate_dump_command = 'mysqldump -u' + username
        generate_dump_command += ' -p' + password if password
        generate_dump_command += ' -h' + host + ' ' + @bd_backup.database
        generate_dump_command += ' ' + @bd_backup.specific_tables if @bd_backup.specific_tables
        generate_dump_command += ' > ' + file_name_sql + ' --single-transaction -R'

        compress_file_command = 'tar czfv  ' + file_name_tar_gz + ' ' + file_name_sql + ' --remove-files'
        puts '<----------------------------'
        puts generate_dump_command.inspect
        puts compress_file_command.inspect
        puts '---------------------------->'
        @bd_backup.save!
        Dir.chdir(BACKUPS_ROOT_PATH) {
          if system(generate_dump_command)
            system(compress_file_command)
          else
            puts $?.inspect
            @bd_backup.errors.add(:base, 'Hubo un error al intentar generar el respaldo')
            File.delete(file_name_sql) if File.exist?(file_name_sql)
            raise
            redirect_to bd_backups_path
          end
        }

        flash[:success] = 'Respaldo generado correctamente'
        redirect_to bd_backups_path
      end
    rescue Exception => e
      flash.now[:danger] = 'El Respaldo no pudo ser generado correctamente'
      @bd_backup.file_name = input_file_name
      #@bd_backup.specific_tables = input_specific_tables
      set_database_alternatives
      render action: "new"
    end
  end

  # PUT /bd_backups/1
  # PUT /bd_backups/1.json
  def update
    @bd_backup = BdBackup.find(params[:id])

    respond_to do |format|
      if @bd_backup.update_attributes(params[:bd_backup])
        format.html {redirect_to @bd_backup, notice: 'Bd backup was successfully updated.'}
        format.json {head :no_content}
      else
        format.html {render action: "edit"}
        format.json {render json: @bd_backup.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /bd_backups/1
  # DELETE /bd_backups/1.json
  def destroy
    @bd_backup = BdBackup.find(params[:id])

    begin
      @bd_backup.transaction do
        file_name = @bd_backup.file_name
        @bd_backup.destroy
        Dir.chdir(BACKUPS_ROOT_PATH) {
          File.delete(file_name) if File.exist?(file_name)
        }
        flash[:success] = 'Respaldo eliminado correctamente'
      end
    rescue Exception
      flash[:danger] = 'El Respaldo no pudo ser eliminado correctamente'
    end
    redirect_to bd_backups_path
  end

  def get_tables_from_database
    database = params[:database]
    result_array = show_tables(database)
    render json: result_array
  end

  private

  def show_tables(database)
    sql = "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA =  '#{database}' ;"
    result = ActiveRecord::Base.connection.exec_query(sql)

    result_array = result.map{|row| row['TABLE_NAME']}

    result_array
  end

  def set_database_alternatives
    config, host, password, username = set_connection_config_values

    show_databases_command = 'mysqlshow -u' + username
    show_databases_command += ' -p' + password if password
    show_databases_command += ' -h' + host

    platforms = `#{show_databases_command}`.split().reject {|s| s == '|' || s.include?('+-') || s == 'Databases'}
    database_alternatives = []

    config.each_key do |connection_string|
      if config[connection_string]["adapter"] == 'mysql2' && platforms.include?(config[connection_string]["database"]) && !database_alternatives.include?(config[connection_string]["database"])
        database_alternatives << config[connection_string]["database"]
      end
    end

    @database_alternatives = database_alternatives.map{|database| [database, database, {'data-url' => bd_backups_get_tables_from_database_path(database)}]}
  end

  def set_connection_config_values
    config = Rails.configuration.database_configuration
    host = config[Rails.env]["host"]
    username = config[Rails.env]["username"]
    password = config[Rails.env]["password"]
    return config, host, password, username
  end
end
