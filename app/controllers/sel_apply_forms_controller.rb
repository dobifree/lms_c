class SelApplyFormsController < ApplicationController

  include SelTemplatesHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin
  # GET /sel_apply_forms
  # GET /sel_apply_forms.json
  def index
    @sel_apply_forms = SelApplyForm.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sel_apply_forms }
    end
  end

  # GET /sel_apply_forms/1
  # GET /sel_apply_forms/1.json
  def show
    @sel_apply_form = SelApplyForm.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sel_apply_form }
    end
  end

  # GET /sel_apply_forms/new
  # GET /sel_apply_forms/new.json
  def new
    @sel_template = SelTemplate.find(params[:template_id])
    @sel_apply_form = SelApplyForm.new
    @sel_apply_form.sel_template = @sel_template

  end

  # GET /sel_apply_forms/1/edit
  def edit
    @sel_apply_form = SelApplyForm.find(params[:id])
    @sel_template = @sel_apply_form.sel_template
  end

  # POST /sel_apply_forms
  # POST /sel_apply_forms.json
  def create
    @sel_apply_form = SelApplyForm.new(params[:sel_apply_form])

    respond_to do |format|
      if @sel_apply_form.save
        flash[:success] = 'El item fue añadido correctamente'
        format.html { redirect_to sel_templates_show_open_pill_path(@sel_apply_form.sel_template, 'applyForms') }


        format.json { render json: @sel_apply_form, status: :created, location: @sel_apply_form }
      else
        @sel_template = SelTemplate.find(@sel_apply_form.sel_template_id)
        format.html { render 'new' }
        format.json { render json: @sel_apply_form.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sel_apply_forms/1
  # PUT /sel_apply_forms/1.json
  def update
    @sel_apply_form = SelApplyForm.find(params[:id])

    respond_to do |format|
      if @sel_apply_form.update_attributes(params[:sel_apply_form])
        flash[:success] = 'El item fue modificado correctamente'
        format.html { redirect_to sel_templates_show_open_pill_path(@sel_apply_form.sel_template, 'applyForms') }
        format.json { head :no_content }
      else
        @sel_template = SelTemplate.find(@sel_apply_form.sel_template_id)
        format.html { render 'edit' }
        format.json { render json: @sel_apply_form.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sel_apply_forms/1
  # DELETE /sel_apply_forms/1.json
  def destroy
    @sel_apply_form = SelApplyForm.find(params[:id])

    if is_apply_form_deletable?(@sel_apply_form)
      @sel_apply_form.destroy
      respond_to do |format|
        flash[:success] = 'El item del formulario de postulación fue eliminado correctamente'
        format.html { redirect_to sel_templates_show_open_pill_path(@sel_apply_form.sel_template, 'applyForms') }
        format.json { head :no_content }
      end
    else
      flash[:danger] = 'El item del formulario de postulación no puede ser borrado.'
      redirect_to sel_templates_show_open_pill_path(@sel_apply_form.sel_template, 'applyForms')
    end
  end
end
