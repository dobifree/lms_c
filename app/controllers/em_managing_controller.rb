class EmManagingController < ApplicationController

  include EmManagingHelper

  before_filter :authenticate_user

  def search

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def edit_characteristics
    @user = User.find(params[:id])
  end

  def edit_characteristic_records
    @user = User.find(params[:id])
  end

  def edit_photo
    @user = User.find(params[:id])
  end

  def carga_masiva

  end

  def download_xls_for_massive_create_update

    name = params[:tipo] == 'new' ? 'carga_masiva' : 'actualización_masiva'

    reporte_excel = xls_for_massive_create_update name

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_'+name+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def update_masivo

  end

  def carga_masiva_fotos

  end

  def create

    @user = User.new(params[:user])

    @user.password = @user.password_confirmation = SecureRandom.hex(5)

    if @user.save
      flash[:success] = t('activerecord.success.model.employee.create_ok')
      redirect_to em_show_employee_path @user
    else
      render 'new'
    end

  end

  def update

    @user = User.find(params[:id])

    if @user.update_attributes params[:user]
      flash[:success] = t('activerecord.success.model.employee.update_ok')
      redirect_to em_show_employee_path @user
    else
      render 'edit'
    end

  end

  def update_characteristics

    user = User.find(params[:id])

    Characteristic.where('publica = ?', true).each do |characteristic|

      uc = user.user_characteristics.where('characteristic_id = ?', characteristic.id).first

      current_value = ''

      if uc
        current_value = uc.valor
        uc.destroy
      end

      if params[('characteristic_'+characteristic.id.to_s).to_sym].strip != ''

        ucr = user.user_characteristic_records.where('characteristic_id = ?', characteristic.id).last

        if ucr && ucr.value == current_value
          ucr.value = params[('characteristic_'+characteristic.id.to_s).to_sym].strip
          ucr.save
        end

        uc = UserCharacteristic.new
        uc.user = user
        uc.characteristic = characteristic
        uc.valor = params[('characteristic_'+characteristic.id.to_s).to_sym].strip
        uc.save

      end

    end

    flash[:success] = t('activerecord.success.model.employee.update_characteristics_ok')
    redirect_to em_show_employee_path user


  end

  def update_characteristic_records

    user = User.find(params[:id])

    Characteristic.where('publica = ?', true).each do |characteristic|

      if params[('change_characteristic_'+characteristic.id.to_s).to_sym] == '1'

        uc = user.user_characteristics.where('characteristic_id = ?', characteristic.id).first

        ucr = user.user_characteristic_records.where('characteristic_id = ?', characteristic.id).last

        vf = params[('valid_from_'+characteristic.id.to_s).to_sym].strip.split('/').map { |s| s.to_i }

        valid_from = Date.new(vf[2],vf[1],vf[0])

        current_value = ''

        current_value = uc.valor if uc

        if ucr && ucr.to_date.nil? && params[('characteristic_'+characteristic.id.to_s).to_sym].strip != current_value

          ucr.to_date = valid_from-1.day
          ucr.save

        else

          if uc && uc.valor != '' && params[('characteristic_'+characteristic.id.to_s).to_sym].strip != current_value

            ucr = UserCharacteristicRecord.new
            ucr.user = user
            ucr.characteristic = characteristic
            ucr.value = uc.valor

            ucr.from_date = user.created_at.to_date
            ucr.to_date = valid_from-1.day

            ucr.save

          end

        end

        if params[('characteristic_'+characteristic.id.to_s).to_sym].strip != current_value

          uc.destroy if uc

          if params[('characteristic_'+characteristic.id.to_s).to_sym].strip != ''

            uc = UserCharacteristic.new
            uc.user = user
            uc.characteristic = characteristic
            uc.valor = params[('characteristic_'+characteristic.id.to_s).to_sym].strip
            uc.save

            ucr = UserCharacteristicRecord.new
            ucr.user = user
            ucr.characteristic = characteristic
            ucr.value = params[('characteristic_'+characteristic.id.to_s).to_sym].strip

            ucr.from_date = valid_from

            ucr.save

          end

        end

      end

    end

    flash[:success] = t('activerecord.success.model.employee.update_characteristic_records_ok')
    redirect_to em_show_employee_path user


  end

  def carga_masiva_upload

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_employess_data archivo_temporal

        if @lineas_error.length > 0

          flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_error')
          render 'carga_masiva'

        else

          save_employees_data archivo_temporal

          flash[:success] = t('activerecord.success.model.employee.carga_masiva_ok')

          redirect_to em_employees_carga_masiva_path
        end

      else

        flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_xlsx')
        render 'carga_masiva'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_file')
      render 'carga_masiva'

    end

  end

  def carga_masiva_sk

  end

  def download_xls_for_massive_create_update_sk

    name = params[:tipo] == 'new' ? 'carga_masiva' : 'actualización_masiva'

    reporte_excel = xls_for_massive_create_update_sk name

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_'+name+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end


  def carga_masiva_upload_sk

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_employess_data_sk archivo_temporal

        if @lineas_error.length > 0

          flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_error')
          render 'carga_masiva_sk'

        else

          save_employees_data_sk archivo_temporal

          flash[:success] = t('activerecord.success.model.employee.carga_masiva_ok')

          redirect_to em_employees_carga_masiva_sk_path
        end

      else

        flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_xlsx')
        render 'carga_masiva_sk'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_file')
      render 'carga_masiva_sk'

    end

  end

  def update_masivo_sk

  end


  def update_masivo_upload_sk

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_employess_data_for_update_sk archivo_temporal

        if @lineas_error.length > 0

          flash.now[:danger] = t('activerecord.error.model.employee.update_masivo_error')
          render 'update_masivo_sk'

        else

          save_employees_data_for_update_sk archivo_temporal

          flash[:success] = t('activerecord.success.model.employee.update_masivo_ok')

          redirect_to em_employees_update_masivo_sk_path
        end

      else

        flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_xlsx')
        render 'update_masivo_sk'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_file')
      render 'update_masivo_sk'

    end

  end

  def carga_masiva_upload_txt

    if params[:upload]

      User.update_all({activo: false}) if params[:reemplazar_todo][:yes] == '1'

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos_tmp = line.split('{')

          datos = datos_tmp[0].split(';')

          if datos[4].nil?

            @lineas_error.push num_linea

            begin
              @lineas_error_detalle.push (line.force_encoding('UTF-8'))
            rescue Exception => e
              @lineas_error_detalle.push ('exception: '+e.message)
            end

            @lineas_error_messages.push ['email null']

          else

            #0 usuario
            #1 nombres
            #2 apellidos
            #3 clave, si se pone asterisco no se actualizará
            #4 correo, si se pone asterisco no se actualizará
            #5 activo/inactivo
            #6 atributos

            user = User.find_by_codigo datos[0]

            unless user
              user = User.new
            end

            user.codigo = datos[0]
            user.nombre = datos[1]
            user.apellidos = datos[2]
            user.password = datos[3] if datos[3] != '*'
            user.email = datos[4].strip.downcase if datos[4] != '*'
            if datos[5] == 'activo'
              user.activo = true
            else
              user.activo = false
            end


            if user.save

              if datos_tmp[1]

                datos[6] = datos_tmp[1].strip.slice 0..-2

                atributos = datos[6].split(';')

                atributos.each do |atributo|

                  atributo = atributo.split(':')

                  caracteristica = Characteristic.find_by_nombre(atributo[0])

                  if caracteristica

                    valor_incorrecto = false

                    user_caracteristica = UserCharacteristic.where('user_id = ? AND characteristic_id = ?', user.id, caracteristica.id).first
                    nuevo = false
                    unless user_caracteristica

                      user_caracteristica = UserCharacteristic.new
                      user_caracteristica.user = user
                      user_caracteristica.characteristic = caracteristica
                      nuevo = true
                    end

                    if atributo[1]
                      if atributo[1] == '*' && nuevo
                        valor_incorrecto = true
                      else
                        user_caracteristica.valor = atributo[1].strip
                      end

                    else
                      user_caracteristica.valor = ''
                    end

                    if valor_incorrecto

                      @lineas_error.push num_linea
                      begin
                        @lineas_error_detalle.push ('codigo: '+user.codigo+
                          '; nombre: '+user.nombre+
                          '; apellidos: '+user.apellidos+
                          '; password:'+datos[3]+
                          '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                      rescue Exception => e
                        @lineas_error_detalle.push ('exception: '+e.message)
                      end
                      @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no puede registrarse * en un atributo nuevo']

                    else

                      unless user_caracteristica.save

                        @lineas_error.push num_linea

                        begin
                          @lineas_error_detalle.push ('codigo: '+user.codigo+
                            '; nombre: '+user.nombre+
                            '; apellidos: '+user.apellidos+
                            '; password:'+datos[3]+
                            '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                        rescue Exception => e
                          @lineas_error_detalle.push ('exception: '+e.message)
                        end

                        @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no fue guardado: '+user_caracteristica.errors.full_messages.to_s]

                      end

                    end
                  else

                    @lineas_error.push num_linea

                    begin
                      @lineas_error_detalle.push ('codigo: '+user.codigo+
                        '; nombre: '+user.nombre+
                        '; apellidos: '+user.apellidos+
                        '; password:'+datos[3]+
                        '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                    rescue Exception => e
                      @lineas_error_detalle.push ('exception: '+e.message)
                    end

                    @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no existe']

                  end


                end

              end

            else

              @lineas_error.push num_linea

              begin
                @lineas_error_detalle.push ('codigo: '+user.codigo+
                  '; nombre: '+user.nombre+
                  '; apellidos: '+user.apellidos+
                  '; password:'+datos[3]+
                  '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
              rescue Exception => e
                @lineas_error_detalle.push ('exception: '+e.message)
              end



              @lineas_error_messages.push user.errors.full_messages

            end



          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.employee.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_error') if @lineas_error.length > 0

      render 'carga_masiva'

    else
      flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_file')

      render 'carga_masiva'

    end



  end

  def update_masivo_upload

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_employess_data_for_update archivo_temporal

        if @lineas_error.length > 0

          flash.now[:danger] = t('activerecord.error.model.employee.update_masivo_error')
          render 'update_masivo'

        else

          save_employees_data_for_update archivo_temporal

          flash[:success] = t('activerecord.success.model.employee.update_masivo_ok')

          redirect_to em_employees_update_masivo_path
        end

      else

        flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_xlsx')
        render 'update_masivo'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.employee.carga_masiva_no_file')
      render 'update_masivo'

    end

  end

  def update_masivo_upload_text

    if params[:upload]

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos_tmp = line.split('{')

          datos = datos_tmp[0].split(';')

          #0 usuario

          user = User.find_by_codigo datos[0]

          if user

            datos.delete_at 0

            datos.each do |dato|
              dato = dato.split ':'
              case dato[0]
                when 'nombre'
                  user.nombre = dato[1]
                when 'apellidos'
                  user.apellidos = dato[1]
                when 'clave'
                  user.password = dato[1]
                when 'correo'
                  user.email = dato[1].strip.downcase
                when 'estado'
                  user.activo = dato[1] == 'activo' ? true : false
              end

            end


            if user.save

              if datos_tmp[1]

                datos[6] = datos_tmp[1].strip.slice 0..-2

                atributos = datos[6].split(';')

                atributos.each do |atributo|

                  atributo = atributo.split(':')

                  caracteristica = Characteristic.find_by_nombre(atributo[0])

                  if caracteristica

                    valor_incorrecto = false

                    user_caracteristica = UserCharacteristic.where('user_id = ? AND characteristic_id = ?', user.id, caracteristica.id).first
                    nuevo = false
                    unless user_caracteristica

                      user_caracteristica = UserCharacteristic.new
                      user_caracteristica.user = user
                      user_caracteristica.characteristic = caracteristica
                      nuevo = true
                    end

                    if atributo[1]
                      if atributo[1] == '*' && nuevo
                        valor_incorrecto = true
                      else
                        user_caracteristica.valor = atributo[1].strip
                      end

                    else
                      user_caracteristica.valor = ''
                    end

                    if valor_incorrecto

                      @lineas_error.push num_linea
                      begin
                        @lineas_error_detalle.push ('codigo: '+user.codigo+
                          '; nombre: '+user.nombre+
                          '; apellidos: '+user.apellidos+
                          '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                      rescue Exception => e
                        @lineas_error_detalle.push ('exception: '+e.message)
                      end
                      @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no puede registrarse * en un atributo nuevo']

                    else

                      unless user_caracteristica.save

                        @lineas_error.push num_linea

                        begin
                          @lineas_error_detalle.push ('codigo: '+user.codigo+
                            '; nombre: '+user.nombre+
                            '; apellidos: '+user.apellidos+
                            '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                        rescue Exception => e
                          @lineas_error_detalle.push ('exception: '+e.message)
                        end

                        @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no fue guardado: '+user_caracteristica.errors.full_messages.to_s]

                      end

                    end
                  else

                    @lineas_error.push num_linea

                    begin
                      @lineas_error_detalle.push ('codigo: '+user.codigo+
                        '; nombre: '+user.nombre+
                        '; apellidos: '+user.apellidos+
                        '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
                    rescue Exception => e
                      @lineas_error_detalle.push ('exception: '+e.message)
                    end

                    @lineas_error_messages.push [atributo[0].force_encoding('UTF-8')+' no existe']

                  end


                end

              end

            else

              @lineas_error.push num_linea

              begin
                @lineas_error_detalle.push ('codigo: '+user.codigo+
                  '; nombre: '+user.nombre+
                  '; apellidos: '+user.apellidos+
                  '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
              rescue Exception => e
                @lineas_error_detalle.push ('exception: '+e.message)
              end

              @lineas_error_messages.push user.errors.full_messages

            end

          else
            #decir el usuario no existe

            @lineas_error.push num_linea

            begin
              @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
            rescue Exception => e
              @lineas_error_detalle.push ('exception: '+e.message)
            end

            @lineas_error_messages.push [('El usuario no existe').force_encoding('UTF-8')]


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.user.update_masivo_ok')
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_error') if @lineas_error.length > 0

      render 'update_masivo'

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

      render 'update_masivo'

    end



  end


  def update_photo

    user = User.find(params[:id])

   company = @company

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext != '.jpg' && ext != '.jpeg'

        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_format')
        redirect_to em_edit_employee_photo_path user

      elsif File.size(params[:upload]['datafile'].tempfile) > 70.kilobytes

        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_wrong_size')
        redirect_to em_edit_employee_photo_path user

      else

        save_foto params[:upload], user, company

        flash[:success] = t('activerecord.success.model.employee.update_foto')

        redirect_to em_show_employee_path user

      end



    else

      flash[:danger] = t('activerecord.error.model.user.update_foto_empty_file')

      redirect_to em_edit_employee_photo_path user

    end

  end


  def carga_masiva_fotos_upload

    if params[:upload]

      @total_fotos_cargadas = 0
      @fotos_error = []
      @fotos_error_messages = []

      upload = params[:upload]

     company = @company

      directory = company.directorio+'/'+company.codigo+'/fotos_usuarios/'

      FileUtils.mkdir_p directory unless File.directory? directory

      directory_tmp = directory+'/'+SecureRandom.hex(5)+'/'

      FileUtils.mkdir_p directory_tmp unless File.directory? directory_tmp

      if File.extname(upload['datafile'].original_filename) == '.zip'

        name = 'fotos.zip'
        path = File.join(directory_tmp, name)
        File.open(path, 'wb') { |f| f.write(upload['datafile'].read) }
        system "unzip -o -q \"#{directory_tmp+name}\" -d \"#{directory_tmp}\" "
        File.delete directory_tmp+name

        Dir.entries(directory_tmp).each do |foto|

          if foto != '.' && foto != '..'

            ext = File.extname(foto)

            if ext == '.jpg' || ext == '.jpeg'

              file_tmp = directory_tmp+foto

              if File.size(file_tmp) <= 70.kilobytes

                codigo_usuario = File.basename(file_tmp, ext)

                user = User.find_by_codigo codigo_usuario

                if user

                  if user.foto
                    file_actual = directory+user.foto
                    File.delete(file_actual) if File.exist? file_actual
                  end

                  user.set_foto

                  user.save

                  file = directory+user.foto

                  FileUtils.mv(file_tmp, file)

                  @total_fotos_cargadas += 1

                  #File.open(file, 'wb') { |f| f.write(File.open(file_tmp, 'read').read) }

                  #File.delete(file_tmp)
                else


                  @fotos_error.push foto
                  @fotos_error_messages.push 'no existe usuario'

                end



              else

                @fotos_error.push foto
                @fotos_error_messages.push 'tamaño > 70kb'

              end

            else

              @fotos_error.push foto
              @fotos_error_messages.push 'formato incorrecto'

            end

          end

        end

        FileUtils.remove_dir directory_tmp

        flash.now[:success] =  t('activerecord.success.model.user.carga_masiva_ok')
        render 'carga_masiva_fotos'

      else

        flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_fotos_no_zip')

        render 'carga_masiva_fotos'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

      render 'carga_masiva_fotos'

    end



  end


  def lista_usuarios

    @characteristics = Characteristic.find_all_by_publica(true)

    @characteristics_priv = Characteristic.find_all_by_publica(false)

    @users = User.all

  end

  def lista_usuarios_xlsx

   company = @company

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Reporte Avance') do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Plataforma'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << $current_domain
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila


        fila_actual += 1



        fila = Array.new
        filaHeader = Array.new


        fila << 'DATOS PERSONALES'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter


        characteristics = Characteristic.find_all_by_publica(true)

        #characteristics_porcentaje_sence = Characteristic.find_by_porcentaje_participante_sence(true)

        characteristics_priv = Characteristic.find_all_by_publica(false)

        if characteristics.length > 0

          fila << 'DATOS DEL ALUMNO'

          (1..characteristics.length).each do |n|

            filaHeader <<  headerCenter
            fila << ''

          end



        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':F'+fila_actual.to_s)

        if characteristics.length > 0

          reporte_excel.merge_cells('G'+fila_actual.to_s+':'+letras[5+characteristics.length]+fila_actual.to_s)

        end

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Correo Electrónico'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 22


        characteristics.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end

        characteristics_priv.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        User.all.each_with_index do |user, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << user.codigo
          fileStyle << fieldLeft

          fila << user.apellidos
          fileStyle << fieldLeft

          fila << user.nombre
          fileStyle << fieldLeft

          fila << user.email
          fileStyle << fieldLeft

          if user.activo
            fila << 'Activo'
          else
            fila << 'Inactivo'
          end

          fileStyle << fieldLeft

          characteristics.each do |cha|

            user_cha = user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end

          characteristics_priv.each do |cha|

            user_cha = user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end


          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Lista de Usuarios - '+$current_domain+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end


  private

  def save_foto(upload, user, company)

    ext = File.extname(upload['datafile'].original_filename)

    if ext == '.jpg' || ext == '.jpeg'

      if File.size(upload['datafile'].tempfile) <= 70.kilobytes

        directory = company.directorio+'/'+company.codigo+'/fotos_usuarios/'

        FileUtils.mkdir_p directory unless File.directory? directory

        if user.foto
          file = directory+user.foto
          File.delete(file) if File.exist? file
        end

        user.set_foto

        user.save

        #puts user.errors.full_messages

        file = directory+user.foto

        File.open(file, 'wb') { |f| f.write(upload['datafile'].read) }

      end

    end

  end

  private

  def check_employess_data(archivo_temporal)

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 código
      #1 nombre
      #2 apellidos
      #3 email
      #4 nombre_característica_x
      #5 nombre_característica_y
      #6 nombre_característica_z
      #7 nombre_característica_...

      pos_caracteristica = 4

      if index == 0

        while(fila[pos_caracteristica])

          unless Characteristic.where('nombre = ?', fila[pos_caracteristica]).count == 1
            @lineas_error.push index+1
            @lineas_error_messages.push ['Cabecera: La característica '+fila[pos_caracteristica]+' no existe']
            @lineas_error_detalle.push ''
          end

          pos_caracteristica += 1

        end

      else

        user = User.new
        user.codigo = fila[0].to_s
        user.nombre = fila[1].to_s
        user.apellidos = fila[2].to_s
        user.email = fila[3].to_s
        user.password = user.password_confirmation = SecureRandom.hex(5)

        unless user.valid?

          @lineas_error.push index+1

          begin
            @lineas_error_detalle.push (alias_username+' (código): '+user.codigo+
              '; nombre: '+user.nombre+
              '; apellidos: '+user.apellidos+
              '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
          rescue Exception => e
            @lineas_error_detalle.push ('exception: '+e.message)
          end

          @lineas_error_messages.push user.errors.full_messages

        end

      end

    end

  end

  def check_employess_data_sk(archivo_temporal)

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 RUT SIN DÍGITO
      #1 DÍGITO
      #2 NOMBRES
      #3 APELLIDO PATERNO
      #4 APELLIDO MATERNO
      #5 APELLIDOS
      #6 EMAIL
      #7 CÓDIGO ESCOLARIDAD
      #8 CÓDIGO NIVEL
      #9 COMUNA
      #10 DIRECCIÓN PARTICULAR
      #11 FECHA NACIMIENTO
      #12 NACIONALIDAD
      #13 RUT
      #14 SEXO
      #15 TIPO CONTRATO

      if index > 0

        user = User.new
        user.codigo = fila[0].to_s
        user.nombre = fila[2].to_s
        user.apellidos = fila[5].to_s
        user.email = fila[6].to_s
        user.password = user.password_confirmation = SecureRandom.hex(5)

        unless user.valid?

          @lineas_error.push index+1

          begin
            @lineas_error_detalle.push (alias_username+' (código): '+user.codigo+
              '; nombre: '+user.nombre+
              '; apellidos: '+user.apellidos+
              '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
          rescue Exception => e
            @lineas_error_detalle.push ('exception: '+e.message)
          end

          @lineas_error_messages.push user.errors.full_messages

        end

      end

    end

  end

  def check_employess_data_for_update(archivo_temporal)

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 código
      #1 nombre
      #2 apellidos
      #3 email
      #4 nombre_característica_x
      #5 nombre_característica_y
      #6 nombre_característica_z
      #7 nombre_característica_...

      pos_caracteristica = 4

      if index == 0

        while(fila[pos_caracteristica])

          unless Characteristic.where('nombre = ?', fila[pos_caracteristica]).count == 1
            @lineas_error.push index+1
            @lineas_error_messages.push ['Cabecera: La característica '+fila[pos_caracteristica]+' no existe']
            @lineas_error_detalle.push ''
          end

          pos_caracteristica += 1

        end

      else

        user = User.where('codigo = ?',fila[0]).first

        if user

          user.nombre = fila[1].to_s
          user.apellidos = fila[2].to_s
          user.email = fila[3].to_s
          user.password = user.password_confirmation = SecureRandom.hex(5)

          unless user.valid?

            @lineas_error.push index+1

            begin
              @lineas_error_detalle.push (alias_username+' (código): '+user.codigo+
                '; nombre: '+user.nombre+
                '; apellidos: '+user.apellidos+
                '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
            rescue Exception => e
              @lineas_error_detalle.push ('exception: '+e.message)
            end

            @lineas_error_messages.push user.errors.full_messages

          end

        else

          @lineas_error.push index+1

          begin
            @lineas_error_detalle.push (alias_username+' (código): '+fila[0].to_s+
              '; nombre: '+fila[1].to_s+
              '; apellidos: '+fila[2].to_s+
              '; email: '+fila[3].to_s).force_encoding('UTF-8')
          rescue Exception => e
            @lineas_error_detalle.push ('exception: '+e.message)
          end

          @lineas_error_messages.push ['El empleado no existe']

        end

      end

    end

  end

  def check_employess_data_for_update_sk(archivo_temporal)

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 RUT SIN DÍGITO
      #1 DÍGITO
      #2 NOMBRES
      #3 APELLIDO PATERNO
      #4 APELLIDO MATERNO
      #5 APELLIDOS
      #6 EMAIL
      #7 CÓDIGO ESCOLARIDAD
      #8 CÓDIGO NIVEL
      #9 COMUNA
      #10 DIRECCIÓN PARTICULAR
      #11 FECHA NACIMIENTO
      #12 NACIONALIDAD
      #13 RUT
      #14 SEXO
      #15 TIPO CONTRATO

      if index > 0

        user = User.where('codigo = ?',fila[0]).first

        if user

          user.nombre = fila[2].to_s
          user.apellidos = fila[5].to_s
          user.email = fila[6].to_s

          unless user.valid?

            @lineas_error.push index+1

            begin
              @lineas_error_detalle.push (alias_username+' (código): '+user.codigo+
                '; nombre: '+user.nombre+
                '; apellidos: '+user.apellidos+
                '; email: '+(user.email.nil? ? '' : user.email)).force_encoding('UTF-8')
            rescue Exception => e
              @lineas_error_detalle.push ('exception: '+e.message)
            end

            @lineas_error_messages.push user.errors.full_messages

          end

        else

          @lineas_error.push index+1

          begin
            @lineas_error_detalle.push (alias_username+': '+fila[0].to_s+
              '; nombre: '+fila[2].to_s+
              '; apellidos: '+fila[5].to_s+
              '; email: '+fila[6].to_s).force_encoding('UTF-8')
          rescue Exception => e
            @lineas_error_detalle.push ('exception: '+e.message)
          end

          @lineas_error_messages.push ['El empleado no existe']

        end

      end

    end

  end

  def save_employees_data_sk(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    pos_array = [1,3,4,7,8,9,10,11,12,13,14,15]
    caracteristicas_array = Array.new
    caracteristicas_array.push 'DÍGITO'
    caracteristicas_array.push 'APELLIDO PATERNO'
    caracteristicas_array.push 'APELLIDO MATERNO'
    caracteristicas_array.push 'CÓDIGO ESCOLARIDAD'
    caracteristicas_array.push 'CÓDIGO NIVEL'
    caracteristicas_array.push 'COMUNA'
    caracteristicas_array.push 'DIRECCIÓN PARTICULAR'
    caracteristicas_array.push 'FECHA NACIMIENTO'
    caracteristicas_array.push 'NACIONALIDAD'
    caracteristicas_array.push 'RUT'
    caracteristicas_array.push 'SEXO'
    caracteristicas_array.push 'TIPO CONTRATO'

    data.each_with_index do |fila, index|

      #0 RUT SIN DÍGITO
      #1 DÍGITO
      #2 NOMBRES
      #3 APELLIDO PATERNO
      #4 APELLIDO MATERNO
      #5 APELLIDOS
      #6 EMAIL
      #7 CÓDIGO ESCOLARIDAD
      #8 CÓDIGO NIVEL
      #9 COMUNA
      #10 DIRECCIÓN PARTICULAR
      #11 FECHA NACIMIENTO
      #12 NACIONALIDAD
      #13 RUT
      #14 SEXO
      #15 TIPO CONTRATO



      if index > 0

        user = User.new
        user.codigo = fila[0].to_s
        user.nombre = fila[2].to_s
        user.apellidos = fila[5].to_s
        user.email = fila[6].to_s
        user.password = user.password_confirmation = SecureRandom.hex(5)

        if user.save

          (0..11).each do |i|

            p = pos_array[i]

            if fila[p]

              caracteristica = Characteristic.where('nombre = ?', caracteristicas_array[i]).first

              if caracteristica

                user_caracteristica = UserCharacteristic.new
                user_caracteristica.user = user
                user_caracteristica.characteristic = caracteristica
                if p == 11
                  user_caracteristica.valor = (DateTime.new(1899,12,30) + fila[p].to_i.days).strftime('%d/%m/%Y')
                else
                  user_caracteristica.valor = fila[p].to_s
                end
                user_caracteristica.save

              end

            end


          end


        end

      end

    end

  end

  def save_employees_data_for_update(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    pos_caracteristica = 4

    caracteristicas_array = Array.new

    data.each_with_index do |fila, index|

      #0 código
      #1 nombre
      #2 apellidos
      #3 email
      #4 nombre_característica_x
      #5 nombre_característica_y
      #6 nombre_característica_z
      #7 nombre_característica_...

      if index == 0

        while(fila[pos_caracteristica])

          caracteristicas_array[pos_caracteristica] = fila[pos_caracteristica]

          pos_caracteristica += 1

        end

        pos_caracteristica -= 1

      else

        user = User.where('codigo = ?',fila[0]).first

        if user

          user.nombre = fila[1].to_s
          user.apellidos = fila[2].to_s
          user.email = fila[3].to_s
          user.password = user.password_confirmation = SecureRandom.hex(5)

          if user.save

            (4..pos_caracteristica).each do |p|

              characteristic = Characteristic.where('nombre = ?', caracteristicas_array[p]).first

              uc = user.user_characteristics.where('characteristic_id = ?', characteristic.id).first

              ucr = user.user_characteristic_records.where('characteristic_id = ?', characteristic.id).last

              valid_from = lms_date

              current_value = ''

              current_value = uc.valor if uc

              if ucr && ucr.to_date.nil?  && (!fila[p] || (fila[p] && fila[p].to_s.strip != current_value))

                ucr.to_date = valid_from-1.day
                ucr.save

              else

                if uc && uc.valor != '' && (!fila[p] || (fila[p] && fila[p].to_s.strip != current_value))

                  ucr = UserCharacteristicRecord.new
                  ucr.user = user
                  ucr.characteristic = characteristic
                  ucr.value = uc.valor

                  ucr.from_date = user.created_at.to_date
                  ucr.to_date = valid_from-1.day

                  ucr.save

                end

              end

              if !fila[p] || (fila[p] && fila[p].to_s.strip != current_value)

                uc.destroy if uc

                if fila[p] && fila[p].to_s != ''

                  uc = UserCharacteristic.new
                  uc.user = user
                  uc.characteristic = characteristic
                  uc.valor = fila[p].to_s.strip
                  uc.save

                  ucr = UserCharacteristicRecord.new
                  ucr.user = user
                  ucr.characteristic = characteristic
                  ucr.value = fila[p].to_s.strip

                  ucr.from_date = valid_from

                  ucr.save

                end

              end

            end

          end

        end

      end

    end

  end

  def save_employees_data_for_update_sk(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    pos_array = [1,3,4,7,8,9,10,11,12,13,14,15]
    caracteristicas_array = Array.new
    caracteristicas_array.push 'DÍGITO'
    caracteristicas_array.push 'APELLIDO PATERNO'
    caracteristicas_array.push 'APELLIDO MATERNO'
    caracteristicas_array.push 'CÓDIGO ESCOLARIDAD'
    caracteristicas_array.push 'CÓDIGO NIVEL'
    caracteristicas_array.push 'COMUNA'
    caracteristicas_array.push 'DIRECCIÓN PARTICULAR'
    caracteristicas_array.push 'FECHA NACIMIENTO'
    caracteristicas_array.push 'NACIONALIDAD'
    caracteristicas_array.push 'RUT'
    caracteristicas_array.push 'SEXO'
    caracteristicas_array.push 'TIPO CONTRATO'

    data.each_with_index do |fila, index|

      #0 RUT SIN DÍGITO
      #1 DÍGITO
      #2 NOMBRES
      #3 APELLIDO PATERNO
      #4 APELLIDO MATERNO
      #5 APELLIDOS
      #6 EMAIL
      #7 CÓDIGO ESCOLARIDAD
      #8 CÓDIGO NIVEL
      #9 COMUNA
      #10 DIRECCIÓN PARTICULAR
      #11 FECHA NACIMIENTO
      #12 NACIONALIDAD
      #13 RUT
      #14 SEXO
      #15 TIPO CONTRATO

      if index > 0

        user = User.where('codigo = ?',fila[0]).first

        if user

          user.nombre = fila[2].to_s
          user.apellidos = fila[5].to_s
          user.email = fila[6].to_s

          if user.save

            (0..11).each do |i|

              p = pos_array[i]

              characteristic = Characteristic.where('nombre = ?', caracteristicas_array[i]).first

              if characteristic

                uc = user.user_characteristics.where('characteristic_id = ?', characteristic.id).first

                ucr = user.user_characteristic_records.where('characteristic_id = ?', characteristic.id).last

                valid_from = lms_date

                current_value = ''

                current_value = uc.valor if uc

                if p == 11 && fila[p]
                  fila[p] = (DateTime.new(1899,12,30) + fila[p].to_i.days).strftime('%d/%m/%Y')
                end

                if ucr && ucr.to_date.nil?  && (!fila[p] || (fila[p] && fila[p].to_s.strip != current_value))

                  ucr.to_date = valid_from-1.day
                  ucr.save

                else

                  if uc && uc.valor != '' && (!fila[p] || (fila[p] && fila[p].to_s.strip != current_value))

                    ucr = UserCharacteristicRecord.new
                    ucr.user = user
                    ucr.characteristic = characteristic
                    ucr.value = uc.valor

                    ucr.from_date = user.created_at.to_date
                    ucr.to_date = valid_from-1.day

                    ucr.save

                  end

                end

                if !fila[p] || (fila[p] && fila[p].to_s.strip != current_value)

                  uc.destroy if uc

                  if fila[p] && fila[p].to_s != ''

                    uc = UserCharacteristic.new
                    uc.user = user
                    uc.characteristic = characteristic

                    uc.valor = fila[p].to_s
                    uc.save

                    ucr = UserCharacteristicRecord.new
                    ucr.user = user
                    ucr.characteristic = characteristic

                    ucr.value = fila[p].to_s

                    ucr.from_date = valid_from

                    ucr.save

                  end

                end

              end

            end

          end

        end

      end

    end

  end


end
