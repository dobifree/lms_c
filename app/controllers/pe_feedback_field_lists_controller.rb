class PeFeedbackFieldListsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show

    @pe_feedback_field_list = PeFeedbackFieldList.find(params[:id])
    @pe_process = @pe_feedback_field_list.pe_process

  end

  def new

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_feedback_field_list = @pe_process.pe_feedback_field_lists.build

  end

  def create

    @pe_feedback_field_list = PeFeedbackFieldList.new(params[:pe_feedback_field_list])

    if @pe_feedback_field_list.save

      redirect_to pe_process_tab_path @pe_feedback_field_list.pe_process, 'feedback'

    else

      @pe_process = @pe_feedback_field_list.pe_process
      render action: 'new'

    end

  end

  def edit
    @pe_feedback_field_list = PeFeedbackFieldList.find(params[:id])
    @pe_process = @pe_feedback_field_list.pe_process
  end


  def update

    @pe_feedback_field_list = PeFeedbackFieldList.find(params[:id])

    if @pe_feedback_field_list.update_attributes(params[:pe_feedback_field_list])

      redirect_to pe_process_tab_path @pe_feedback_field_list.pe_process, 'feedback'

    else

      @pe_process = @pe_feedback_field_list.pe_process
      render action: 'edit'

    end

  end

  def destroy

    @pe_feedback_field_list = PeFeedbackFieldList.find(params[:id])

    pe_process = @pe_feedback_field_list.pe_process

    @pe_feedback_field_list.destroy if @pe_feedback_field_list.pe_feedback_fields.size == 0 && @pe_feedback_field_list.pe_feedback_compound_fields.size == 0

    redirect_to pe_process_tab_path pe_process, 'feedback'


  end

  def massive_create_items

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_feedback_field_list = PeFeedbackFieldList.find(params[:pe_feedback_field_list_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          next if index == 0 || fila[1].blank?

          pe_feedback_field_list_item = @pe_feedback_field_list.pe_feedback_field_list_items.where('description = ?',fila[1]).first

          unless pe_feedback_field_list_item

            pe_feedback_field_list_item = @pe_feedback_field_list.pe_feedback_field_list_items.build
            pe_feedback_field_list_item.description = fila[1]

          end

          pe_feedback_field_list_item.position = fila[0]

          if pe_feedback_field_list_item.save

            if fila[2] && fila[3] && fila[4]

              pe_feedback_field_list_ = PeFeedbackFieldList.where('name = ? AND pe_process_id = ?', fila[2], @pe_process.id).first

              if pe_feedback_field_list_

                pe_feedback_field_list_item_ = pe_feedback_field_list_.pe_feedback_field_list_items.where('description = ?',fila[3]).first

                if pe_feedback_field_list_item_

                  pe_feedback_field_list_item.pe_feedback_field_list_item = pe_feedback_field_list_item_

                  if fila[4].downcase == 'si'
                    pe_feedback_field_list_item.list_item_selected = true
                  else
                    pe_feedback_field_list_item.list_item_selected = false
                  end

                  pe_feedback_field_list_item.save

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('item: '+fila[1].to_s).force_encoding('UTF-8')
                  @lineas_error_messages.push ['No existe el item '+fila[3]]

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('item: '+fila[1].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['No existe la lista '+fila[2]]

              end


            end

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('item: '+fila[1].to_s).force_encoding('UTF-8')
            @lineas_error_messages.push pe_feedback_field_list_item.errors.full_messages

          end

        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    end

    render 'show'

  end

end
