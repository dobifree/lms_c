class EvaluationsController < ApplicationController
  
  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new
    
    @course = Course.find(params[:course_id])
    @evaluation = @course.evaluations.build()

  end

  def edit
    @evaluation = Evaluation.find(params[:id])
    @course = @evaluation.course
  end

  def edit_master_evaluation
    @evaluation = Evaluation.find(params[:id])
    @course = @evaluation.course
  end

  def delete
    @evaluation = Evaluation.find(params[:id])
    @course = @evaluation.course
  end

  def create
    
    @course = Course.find(params[:course][:id])

    @evaluation = @course.evaluations.build(params[:evaluation])

    if @evaluation.save
      flash[:success] = t('activerecord.success.model.evaluation.create_ok')
      redirect_to @course
    else
      render action: 'new'
    end

  end

  
  def update
    
    @evaluation = Evaluation.find(params[:id])

    if @evaluation.update_attributes(params[:evaluation])

      @evaluation.evaluation_topics.destroy_all

      t_num_q = 0

      @evaluation.master_evaluation.master_evaluation_topics.each do |met|

        et = @evaluation.evaluation_topics.build

        et.master_evaluation_topic = met
        et.position = params['pos_q_topic_'+met.id.to_s]
        et.number_of_questions = params['num_q_topic_'+met.id.to_s]
        et.save

        t_num_q += et.number_of_questions

      end

      @evaluation.numero_preguntas=t_num_q
      @evaluation.save


      flash[:success] = t('activerecord.success.model.evaluation.update_ok')
      redirect_to @evaluation.course
    else
      @course = @evaluation.course
      render action: 'edit'
    end


  end

  def update_master_evaluation

    @evaluation = Evaluation.find(params[:evaluation][:id])

    if @evaluation.update_attributes(master_evaluation_id: params[:evaluation][:master_evaluation_id])
      flash[:success] = t('activerecord.success.model.evaluation.update_ok')
      @evaluation.after_clean_master_evaluation if @evaluation.master_evaluation.nil?
      redirect_to @evaluation.course
    else
      @course = @evaluation.course
      render action: 'edit_master_evaluation'
    end

  end

  def destroy
    
    @evaluation = Evaluation.find(params[:id])

    if verify_recaptcha

      units_sequence = @evaluation.despues_de_units + @evaluation.antes_de_units
      evaluations_sequence = @evaluation.despues_de_evaluations + @evaluation.antes_de_evaluations
      polls_sequence = @evaluation.despues_de_polls + @evaluation.antes_de_polls
    
      if @evaluation.destroy

        units_sequence.each { |unit| unit.reload.update_libre }
        evaluations_sequence.each { |evaluation| evaluation.reload.update_libre_course }
        polls_sequence.each { |poll| poll.reload.update_libre_course }

        flash[:success] = t('activerecord.success.model.evaluation.delete_ok')
        redirect_to course_path(@evaluation.course)
      else
        flash[:danger] = t('activerecord.success.model.evaluation.delete_error')
        redirect_to delete_evaluation_path(@evaluation)
      end

    else

      flash.delete(:recaptcha_error)
      
      flash[:danger] = t('activerecord.error.model.evaluation.captcha_error')
      redirect_to delete_evaluation_path(@evaluation)

    end

  end
end
