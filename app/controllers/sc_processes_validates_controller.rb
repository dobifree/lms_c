class ScProcessesValidatesController < ApplicationController
  include ScProcessesValidatesHelper

  before_filter :get_gui_characteristics, only: [:validate_attendance, :update_status, :refresh_validation_table]

  before_filter :authenticate_user, except: [:show_reject_attendance_modal,
                                             :refresh_validation_table]

  before_filter :verify_access_validator, except: [:show_reject_attendance_modal,
                                                   :refresh_validation_table]

  before_filter :authenticate_user_partial, only: [:show_reject_attendance_modal,
                                                   :refresh_validation_table]

  before_filter :verify_access_validator_partial, only: [:show_reject_attendance_modal,
                                                         :refresh_validation_table]

  def validate_attendance
    @sc_process = ScProcess.find(params[:sc_process_id])
    if ct_module_sc_he_manager?
      @records = ScRecord.where('sc_process_id = ? and status != 0', @sc_process.id)
    else
      @records = []
      validator = HeCtModulePrivilage.where(user_id: user_connected.id, active: true).first
      validator.sc_user_to_validates.each do |user_to_validate|
        @records += user_to_validate.user.sc_records.where('sc_process_id = ? and status != 0', @sc_process.id)
      end

      validator.sc_register_to_validates.each do |register_to_validate|
        @records += ScRecord.where('registered_by_user_id = ? and sc_process_id = ? and status != 0',register_to_validate.user_id, @sc_process.id)
      end
    end
    @records = @records.uniq_by(&:id)
    @recorders = []
    @users = []
    @records.each { |record| @recorders << record.registered_by_user unless @recorders.include?(record.registered_by_user) }
    @records.each { |record| @users << record.user unless @users.include?(record.user) }
    @records = @records.sort_by(&:begin).reverse
  end

  def approve_attendance
    record = ScRecord.find(params[:sc_record_id])
    unless record.status == 3
      log = ScLogRecord.new(:user_id => user_connected.id,
                            :sc_record_id => record.id,
                            :date => lms_time,
                            :record_before => record.as_json,
                            :action => 'Aprobar')
      log.save
      record.status = 3
      record.rejected_at = nil
      record.rejected_by_user_id = nil
      record.rejected_description = nil
      record.save
      update_he_user_process(record.user_id, record.sc_process_id)
    end
    render nothing: true
  end

  def back_to_edit
    record_selected = ScRecord.find(params[:sc_record_id])
    ScLogRecord.create(:user_id => user_connected.id, :sc_record_id => record_selected.id, :date => lms_time, :record_before => record_selected.as_json, :action => 'Volver a edición')
    record_selected.status = 0
    record_selected.save
    record_selected.sc_process.sc_records.each { |record| record.update_attributes(sent_to_payroll: false) }
    report = ScReporting.where(sc_process_id: record_selected.sc_process_id, active: true).first
    if report
      report.active = false
      report.deactivated_by_user_id = user_connected.id
      report.deactivated_at = lms_time
      report.save
    end
    update_he_user_process(record_selected.user_id, record_selected.sc_process_id)
    render nothing: true
  end

  def show_reject_attendance_modal
    @sc_process = ScProcess.find(params[:sc_process_id])
    render :partial => 'reject_attendance_modal', :locals => {:record => ScRecord.new()}
  end

  def reject_attendance
    record = ScRecord.find(params[:sc_record_id])
    unless record.status == 4
      log = ScLogRecord.create(:user_id => user_connected.id, :sc_record_id => record.id, :date => lms_time, :record_before => record.as_json, :action => 'Rechazar')
      record.status = 4
      record.rejected_at = lms_time
      record.rejected_by_user_id = user_connected.id
      record.rejected_description = params[:description]
      record.save
      update_he_user_process(record.user_id, record.sc_process_id)
    end
    render :nothing => true
  end

  def refresh_validation_table
    @sc_process = ScProcess.find(params[:sc_process_id])
    if ct_module_sc_he_manager?
      @records = ScRecord.where('sc_process_id = ? and status != 0', @sc_process.id)
    else
      @records = []
      validator = HeCtModulePrivilage.where(user_id: user_connected.id, active: true).first
      validator.sc_user_to_validates.each do |user_to_validate|
        @records += user_to_validate.user.sc_records.where('sc_process_id = ? and status != 0', @sc_process.id)
      end

      validator.sc_register_to_validates.each do |register_to_validate|
        register_to_validate.sc_user_to_registers.each do |user_to_register|
          @records += user_to_register.user.sc_records.where('sc_process_id = ? and status != 0', @sc_process.id)
        end
      end
    end
    @records = @records.uniq_by(&:id)
    @recorders = []
    @users = []
    @records.each { |record| @recorders << record.registered_by_user unless @recorders.include?(record.registered_by_user) }
    @records.each { |record| @users << record.user unless @users.include?(record.user) }
    @records = @records.sort_by(&:begin).reverse


    render :partial => 'validate_records_list',
           :locals => {:process => @sc_process,
                       :records => @records,
                       :recorders => @recorders,
                       :users => @users,
                       :editable => true,
                       :characteristics => @sc_schedule_characteristics,
                       :success_message => true }
  end


  private
  def get_gui_characteristics
    @sc_schedule_characteristics = ScScheduleCharacteristic.gui
  end

  def verify_access_validator_partial
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first
    validator = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :validator => true).first

    if ct_module && (validator || ct_module_sc_he_manager?)
    else
      render :partial => 'shared/div_message',
             :locals => { :type_generic_message => 'alert-danger',
                          :generic_message => t('security.no_access_to_manage_module')}
    end
  end

  def verify_access_validator
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first
    validator = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :validator => true).first

    if ct_module && (validator || ct_module_sc_he_manager?)
    else
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def authenticate_user_partial
    unless logged_in?
      render :partial => 'shared/div_message',
             :locals => { :type_generic_message => 'alert-danger',
                          :generic_message => 'No ha iniciado sesión'}
    end
  end
end