class CtMenusController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @ct_menus = CtMenu.all
  end


  def edit
    @ct_menu = CtMenu.find(params[:id])
  end

  def update

    @ct_menu = CtMenu.find(params[:id])

    if @ct_menu.update_attributes(params[:ct_menu])
      flash[:success] = t('activerecord.success.model.ct_menus.update_ok')
      redirect_to ct_menus_path
    else
      render action: 'edit'
    end

  end

end
