class UserManagingCreateController < ApplicationController

  include UserManagingCreateHelper

  before_filter :authenticate_user

  before_filter :verify_access_create_users, only: [:new, :create]

  def new
    @user = User.new
    @char_errors = Array.new
  end

  def create
    @user = User.new(params[:user])

    errors = false

    @char_errors = Array.new

    Characteristic.where('required_new = ? AND required_new_c = ?', true, true).each do |characteristic|

      #puesto con módulo de puestos

      params['characteristic_'+characteristic.id.to_s] = @user.j_job_id if characteristic.data_type_id == 13 && ct_module_jobs?

      #centro de costos

      params['characteristic_'+characteristic.id.to_s] = @user.j_cost_center_id if characteristic.data_type_id == 15

      if params[('characteristic_'+characteristic.id.to_s)].blank?

        if characteristic.characteristic_value

          if params[('characteristic_'+characteristic.characteristic_value.characteristic.id.to_s)] == characteristic.characteristic_value.id.to_s && characteristic.char_value_selected

            @user.valid? unless errors

            errors = true
            @char_errors.push characteristic.id

            @user.errors.add(characteristic.nombre, 'no puede ser vacío')

          elsif params[('characteristic_'+characteristic.characteristic_value.characteristic.id.to_s)] != characteristic.characteristic_value.id.to_s && !characteristic.char_value_selected

            @user.valid? unless errors

            errors = true
            @char_errors.push characteristic.id

            @user.errors.add(characteristic.nombre, 'no puede ser vacío')

          end

        else
          @user.valid? unless errors

          errors = true
          @char_errors.push characteristic.id

          @user.errors.add(characteristic.nombre, 'no puede ser vacío')
        end

      end

    end

    if errors
      render 'new'
    else
      if @user.save

        Characteristic.where('required_new = ?', true).each do |characteristic|

          new_value = nil

          if characteristic.data_type_id == 11

            #register

            current_ucs_ids = Array.new

            @user.user_characteristics_registered(characteristic).each {|c_uc| current_ucs_ids.push c_uc.id.to_s}

            if params['characteristic_'+characteristic.id.to_s+'_create']

              n_aux_files = Array.new

              characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|
                n_aux_files[index_element] = 0
              end

              params['characteristic_'+characteristic.id.to_s+'_create'].each_with_index do |c_id, n_aux|

                if c_id == '-1'

                  error, uc = @user.add_user_characteristic(characteristic, '-', lms_time, user_connected, user_connected, nil, nil, @company)

                  characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                    new_value = nil

                    unless element_characteristic.data_type_id == 6
                      new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s).to_sym][n_aux].blank?
                    else
                      #file

                      if params[('characteristic_file_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux] == '1'

                        new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)] && params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]]

                        n_aux_files[index_element] += 1

                      end

                    end

                    if new_value

                      error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                      if !error_in && uc_element

                        uc_element.register_user_characteristic = uc
                        uc_element.save

                      end

                    end

                  end

                else

                  current_ucs_ids.delete(c_id)

                  uc = @user.user_characteristics.where('id = ?', c_id).first

                  characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                    new_value = nil

                    uc_element = uc.elements_user_characteristics_by_characteristic(element_characteristic)

                    unless element_characteristic.data_type_id == 6

                      uc_element.destroy if uc_element

                      new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s).to_sym][n_aux].blank?

                      error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                      if !error_in && uc_element

                        uc_element.register_user_characteristic = uc
                        uc_element.save

                      end

                    else
                      #file

                      if params[('characteristic_file_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux] == '1'

                        new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)] && params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]]

                        if new_value

                          n_aux_files[index_element] += 1

                          if uc_element
                            uc_element.delete_file @company
                            uc_element.destroy
                          end

                          error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                          if !error_in && uc_element

                            uc_element.register_user_characteristic = uc
                            uc_element.save

                          end

                        end

                      end

                    end

                  end

                end

              end

            end

            current_ucs_ids.each { |current_uc_id| @user.user_characteristics.where('id = ?', current_uc_id).destroy_all }

          elsif characteristic.data_type_id == 13 && ct_module_jobs?

            if @user.j_job_id

              from_date = @user.from_date

              user_j_job_last = @user.user_j_jobs.last

              if user_j_job_last && user_j_job_last.to_date.nil?
                user_j_job_last.to_date = from_date-1.day
                user_j_job_last.save
              end

              user_j_job = @user.user_j_jobs.build
              user_j_job.j_job_id = @user.j_job_id
              user_j_job.from_date = from_date
              user_j_job.save

              @user.update_user_characteristic(characteristic, @user.j_job.name, from_date.to_datetime, user_connected, user_connected, nil, nil, @company)

            end

          else

            if (characteristic.data_type_id == 13 && !ct_module_jobs?) || characteristic.data_type_id != 13

              unless characteristic.data_type_id == 6
                new_value = params[('characteristic_'+characteristic.id.to_s).to_sym].to_s.strip unless params[('characteristic_'+characteristic.id.to_s).to_sym].blank?
              else
                #file
                new_value = params[('characteristic_'+characteristic.id.to_s).to_sym]
              end

              from_date = @user.from_date

              @user.update_user_characteristic(characteristic, new_value, from_date.to_datetime, user_connected, user_connected, nil, nil, @company)

            end

          end


        end

        cw = Characteristic.get_characteristic_from_date_work
        if cw
          @user.update_user_characteristic(cw, @user.from_date, lms_time, user_connected, user_connected, nil, nil, @company)
        end

        if @company.is_security

          begin
            UmMailer.create_user_security(@user, @company).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'um'
            lm.step = 'create_user'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

        flash[:success] = t('activerecord.success.model.user.create2_ok')
        if user_connected.um_create_users
          redirect_to user_managing_user_profile_for_users_manager_path(@user)
        else
          redirect_to user_managing_user_profile_path(@user)
        end
      else
        render 'new'
      end
    end

  end

  def new_from_excel
    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []
  end

  def create_from_excel

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5) + '.xlsx'
        archivo_temporal = directorio_temporal + nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') {|f| f.write(params[:upload]['datafile'].read)}

        #carga_usuario_from_excel archivo_temporal, @company

        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
    end

    render :new_from_excel
  end

  def new_from_excel_format
    reporte_excel = generate_xlsx_create @company

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_Alta_Usuarios.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'
  end

  private

  def verify_access_create_users

    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first

    if ct_module.create_delete_users? && (user_connected.um_create_users || user_connected.manage_ct_module_um)
    else

      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

end
