class PeQuestionsController < ApplicationController

  include PeAssessmentHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show_pe_member

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_member = PeMember.find(params[:pe_member_id])
    @pe_process = @pe_evaluation.pe_process
   #@company = session[:company]

  end

  def new

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    @pe_process = @pe_evaluation.pe_process

    @pe_question = @pe_evaluation.pe_questions.build

    pe_question_parent = nil

    if params[:pe_question_id].to_i > 0
      pe_question_parent = @pe_evaluation.pe_questions.where('id = ?', params[:pe_question_id]).first
      @pe_element = pe_question_parent.pe_element.pe_elements.first
    else
      @pe_element = @pe_evaluation.pe_elements.first
    end

    @pe_question.pe_question_id = pe_question_parent.id if pe_question_parent

  end

  def create

    @pe_evaluation = PeEvaluation.find(params[:pe_question][:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process

    pe_question_parent = nil

    if params[:pe_question][:pe_question_id].to_i > 0
      pe_question_parent = @pe_evaluation.pe_questions.where('id = ?', params[:pe_question][:pe_question_id]).first
      @pe_element = pe_question_parent.pe_element.pe_elements.first
    else
      @pe_element = @pe_evaluation.pe_elements.first
    end

    saved_at_least_one = false

    if pe_question_parent

      @pe_question = @pe_evaluation.pe_questions.build
      @pe_question.pe_question_id = pe_question_parent.id
      @pe_question.pe_element_id = @pe_element.id
      @pe_question.pe_group_id = pe_question_parent.pe_group_id
      @pe_question.description = params[:pe_question][:description]
      @pe_question.weight = @pe_element.assessed ? params[:pe_question][:weight] : 0
      @pe_question.has_comment = params[:pe_question][:has_comment]

      if @pe_question.save
        saved_at_least_one = true
      end

    else

      params[:pe_question][:pe_group].each do |pe_group_id|

        unless pe_group_id.blank?

          @pe_question = @pe_evaluation.pe_questions.build
          @pe_question.pe_element_id = @pe_element.id
          @pe_question.description = params[:pe_question][:description]
          @pe_question.weight = @pe_element.assessed ? params[:pe_question][:weight] : 0
          @pe_question.has_comment = params[:pe_question][:has_comment]
          @pe_question.pe_group_id = pe_group_id

          if @pe_question.save
            saved_at_least_one = true
          end

        end

      end

    end

    if saved_at_least_one
      redirect_to @pe_evaluation
    else

      unless @pe_question

        @pe_question = @pe_evaluation.pe_questions.build
        @pe_question.pe_question_id = pe_question_parent.id if pe_question_parent
        @pe_question.pe_element_id = @pe_element.id
        @pe_question.description = params[:pe_question][:description]
        @pe_question.weight = @pe_element.assessed ? params[:pe_question][:weight] : 0
        @pe_question.has_comment = params[:pe_question][:has_comment]



      end

      render 'new'

    end



  end

  def edit

    @pe_question = PeQuestion.find(params[:id])
    @pe_evaluation = @pe_question.pe_evaluation
    @pe_element = @pe_question.pe_element
    @pe_process = @pe_evaluation.pe_process

  end

  def update
    @pe_question = PeQuestion.find(params[:id])

    if @pe_question.update_attributes(params[:pe_question])

      unless @pe_question.pe_element.simple

        @pe_question.pe_question_descriptions.destroy_all

        desc = ''

        @pe_question.pe_element.pe_element_descriptions.each do |pe_element_description|

          pe_question_description = @pe_question.pe_question_descriptions.build
          pe_question_description.pe_element_description = pe_element_description
          pe_question_description.description = params['pe_element_description_'+pe_element_description.id.to_s]
          pe_question_description.save

          desc += params['pe_element_description_'+pe_element_description.id.to_s]

        end

        @pe_question.description = desc
        @pe_question.save

      end

      if @pe_question.pe_member_evaluated
        redirect_to pe_questions_show_pe_member_path @pe_question.pe_evaluation, @pe_question.pe_member_evaluated
      else
        redirect_to @pe_question.pe_evaluation
      end
    else
      @pe_evaluation = @pe_question.pe_evaluation
      @pe_element = @pe_question.pe_element
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end


  def destroy
    pe_question = PeQuestion.find(params[:id])

    @pe_evaluation = pe_question.pe_evaluation

    @pe_process = @pe_evaluation.pe_process

    @pe_member_evaluated = pe_question.pe_member_evaluated

    @pe_member_evaluator = pe_question.pe_member_evaluator

    #@pe_member_group = @pe_member_evaluated.pe_member_group(@pe_evaluation)

    pe_assessment_evaluations_array = Array.new

    pe_question.pe_assessment_questions.each do |pe_assessment_question|
      pe_assessment_evaluations_array.push pe_assessment_question.pe_assessment_evaluation
    end

    pe_question.destroy

    pe_assessment_evaluations_array.each do |pe_assessment_evaluation|

      pe_assessment_evaluation.reload

      calculate_assessment_evaluation pe_assessment_evaluation

      calculate_assessment_final_evaluation @pe_member_evaluated, @pe_evaluation

      calculate_assessment_dimension @pe_member_evaluated, @pe_evaluation.pe_dimension

      @pe_member_evaluated.set_pe_box
      @pe_member_evaluated.save

      unless @pe_process.of_persons?

        pe_area = @pe_member_evaluated.pe_area
        pe_area.set_pe_box
        pe_area.save

      end

      calculate_assessment_groups @pe_member_evaluated

    end

    if @pe_member_evaluated
      redirect_to pe_questions_show_pe_member_path @pe_evaluation, @pe_member_evaluated
    else
      redirect_to @pe_evaluation
    end

  end
=begin
  def destroy_pe_questions_massive

    pe_questions = PeQuestion.where("pe_questions.pe_evaluation_id = 18 and pe_member_evaluated_id = 6494 and pe_questions.description = 'TODASCalidadLa atención de requerimientos de información, consultas y resolución de problemas se realiza con proactividad, calidad y en un plazo razonable.'")

    pe_questions.each do |pe_question|

      @pe_evaluation = pe_question.pe_evaluation

      @pe_process = @pe_evaluation.pe_process

      @pe_member_evaluated = pe_question.pe_member_evaluated

      @pe_member_evaluator = pe_question.pe_member_evaluator

      #@pe_member_group = @pe_member_evaluated.pe_member_group(@pe_evaluation)

      pe_assessment_evaluations_array = Array.new

      pe_question.pe_assessment_questions.each do |pe_assessment_question|
        pe_assessment_evaluations_array.push pe_assessment_question.pe_assessment_evaluation
      end

      pe_question.destroy

      pe_assessment_evaluations_array.each do |pe_assessment_evaluation|

        pe_assessment_evaluation.reload

        calculate_assessment_evaluation pe_assessment_evaluation

        calculate_assessment_final_evaluation @pe_member_evaluated, @pe_evaluation

        calculate_assessment_dimension @pe_member_evaluated, @pe_evaluation.pe_dimension

        @pe_member_evaluated.set_pe_box
        @pe_member_evaluated.save

        unless @pe_process.of_persons?

          pe_area = @pe_member_evaluated.pe_area
          pe_area.set_pe_box
          pe_area.save

        end

      end

    end


  end
=end
end
