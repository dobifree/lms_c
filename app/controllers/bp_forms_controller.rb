class BpFormsController < ApplicationController
  include BpFormsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin
  before_filter :active_form, unless: [:new]

  def new
    @bp_form = BpForm.new(bp_event_id: params[:bp_event_id])
  end

  def create
    form = deactivate_form_form(params[:bp_event_id])
    new_form = complete_form_create(params[:bp_form], params[:bp_event_id])
    new_form.bp_items.each do |item|
      item.registered_at = lms_time
      item.registered_by_admin_id = admin_connected.id
      next unless item.item_list?
      item.bp_item_options.each do |option|
        option.registered_at = lms_time
        option.registered_by_admin_id = admin_connected.id
      end
    end
    if new_form.save
      if form
        form.save
        deactivate_items_form(form.id)
      end
      redirect_to bp_admin_event_manage_path(new_form.bp_event)
    else
      @bp_form = new_form
      render action: 'new'
    end
  end

  def edit
    @bp_form = BpForm.find(params[:id])
  end

  def update
    @bp_form = BpForm.find(params[:id])
    if @bp_form.update_attributes(params[:bp_form])
      redirect_to bp_admin_event_manage_path(@bp_form.bp_event)
    else
      render action: 'edit'
    end
  end

  private

  def active_form
    form = BpForm.find(params[:id]).first
    return unless form
    flash[:danger] = 'No tiene acceso a registro'
    redirect_to root_path
  end

  def deactivate_form_form(event_id)
    event = BpEvent.find(event_id)
    return false unless event.active_form
    form = event.active_form
    form.active = false
    form.deactivated_at = lms_time
    form.deactivated_by_admin_id = admin_connected.id
    return form
  end

  def deactivate_items_form(form_id)
    form = BpForm.find(form_id)
    form.active_items.each do |item|
      item.active = false
      item.deactivated_at = lms_time
      item.deactivated_by_admin_id = admin_connected.id
      if item.save
        deactivate_options_item(item.id)
      end
    end
  end

  def deactivate_options_item(item_id)
    item = BpItem.find(item_id)
    item.active_options.each do |option|
      option.active = false
      option.deactivated_at = lms_time
      option.deactivated_by_admin_id = admin_connected.id
      option.save
    end
  end

  def complete_form_create(params, event_id)
    form = BpForm.new(params)
    form.bp_event_id = event_id
    form.registered_at = lms_time
    form.registered_by_admin_id = admin_connected.id
    return form
  end

  def complete_item_create(item)
    item.registered_at = lms_time
    item.registered_by_admin_id = admin_connected.id
    return item
  end

  def complete_option_create(option)
    option.registered_at = lms_time
    option.registered_by_admin_id = admin_connected.id
    return option
  end

end
