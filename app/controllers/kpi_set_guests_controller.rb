class KpiSetGuestsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_admin



  # GET /kpi_set_guests/1
  # GET /kpi_set_guests/1.json
  def show
    @kpi_set_guest = KpiSetGuest.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @kpi_set_guest }
    end
  end

  # GET /kpi_set_guests/new
  # GET /kpi_set_guests/new.json
  def new
    @kpi_set_guest = KpiSetGuest.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @kpi_set_guest }
    end
  end

  # GET /kpi_set_guests/1/edit
  def edit
    @kpi_set_guest = KpiSetGuest.find(params[:id])
  end

  # POST /kpi_set_guests
  # POST /kpi_set_guests.json
  def create
    @kpi_set_guest = KpiSetGuest.new(params[:kpi_set_guest])

    respond_to do |format|
      if @kpi_set_guest.save
        format.html { redirect_to @kpi_set_guest, notice: 'Kpi set guest was successfully created.' }
        format.json { render json: @kpi_set_guest, status: :created, location: @kpi_set_guest }
      else
        format.html { render action: "new" }
        format.json { render json: @kpi_set_guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /kpi_set_guests/1
  # PUT /kpi_set_guests/1.json
  def update
    @kpi_set_guest = KpiSetGuest.find(params[:id])

    respond_to do |format|
      if @kpi_set_guest.update_attributes(params[:kpi_set_guest])
        format.html { redirect_to @kpi_set_guest, notice: 'Kpi set guest was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @kpi_set_guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kpi_set_guests/1
  # DELETE /kpi_set_guests/1.json
  def destroy
    @kpi_set_guest = KpiSetGuest.find(params[:id])
    @kpi_set_guest.destroy

    respond_to do |format|
      format.html { redirect_to kpi_set_guests_url }
      format.json { head :no_content }
    end
  end

  private

    def verify_access_manage_dashboard
      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',params[:kpi_dashboard_id]).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

end
