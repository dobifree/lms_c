class PeManagingController < ApplicationController

  include PeAssessmentHelper
  include UsersHelper

  before_filter :authenticate_user

  before_filter :access_to_manage_process, only: [:register_results_1, :edit_dates, :update_dates]
  before_filter :access_to_register_results, only: [:register_results_2, :register_results_3]
  before_filter :validate_open_process, only: [:register_results_3]

  before_filter :get_data_1, only: [:modify_evaluations_list_people, :manage_assessments, :manage_feedbacks, :edit_dates_2, :update_dates_2,
                                    :register_results_list_people,
                                    :register_results_list_evaluations, :download_excel_register_results_evaluations, :register_results_all_evaluations_questions_xls]
  before_filter :get_data_2, only: [:modify_evaluations_show, :manage_def_by_user, :destroy_validator_def_by_user, :change_boss_def_by_user, :change_val_def_by_user,
                                    :manage_assessment, :manage_feedback, :open_whole_assessment, :finish_whole_assessment, :create_evaluator,
                                    :open_feedback, :finish_feedback, :create_feedback_provider, :detroy_feedback_provider,
                                    :register_results_show]
  before_filter :get_data_3, only: [:open_assessment, :finish_assessment, :destroy_evaluator, :destroy_validator]
  before_filter :get_data_4, only: [:register_results_show_evaluation, :register_results, :register_results_xls, :register_results_questions_xls,
                                    :manage_def_by_users, :open_def_by_user, :close_def_by_user, :open_accept_def_by_user, :close_accept_def_by_user,
                                    :open_val_def_by_user, :close_val_def_by_user,
                                    :register_results_questions_show_evaluation, :register_results_questions_download_evaluation_to_activate,
                                    :register_results_questions_download_evaluation_to_assess]
  before_filter :get_data_5, only: [:open_def_by_user, :close_def_by_user, :open_accept_def_by_user, :close_accept_def_by_user, :open_val_def_by_user, :close_val_def_by_user]
  before_filter :get_data_6, only: [:create_validator]
  before_filter :get_data_7, only: [:modify_evaluations_new]
  before_filter :get_data_7a, only: [:modify_evaluations_create, :register_results_assess]
  before_filter :get_data_8, only: [:modify_evaluations_edit, :modify_evaluations_update, :modify_evaluations_destroy]

  before_filter :verify_is_a_manager, only: [:list_processes_2]
  before_filter :validate_manager, only: [:modify_evaluations_list_people, :modify_evaluations_show, :manage_assessments, :manage_assessment, :open_assessment, :finish_assessment,
                                          :open_whole_assessment, :finish_whole_assessment,
                                          :destroy_evaluator, :create_evaluator, :create_validator, :destroy_validator,
                                          :manage_feedbacks, :manage_feedback,
                                          :create_feedback_provider, :destroy_feedback_provider,
                                          :open_feedback, :finish_feedback,
                                          :edit_dates_2, :update_dates_2,
                                          :register_results_list_people, :register_results_show, :register_results_assess,
                                          :register_results_list_evaluations, :register_results_show_evaluation,
                                          :register_results, :register_results_xls, :register_results_questions_xls, :download_excel_register_results_evaluations,
                                          :manage_def_by_users, :manage_def_by_user, :destroy_validator_def_by_user, :change_boss_def_by_user, :change_val_def_by_user,
                                          :open_def_by_user, :close_def_by_user,
                                          :open_accept_def_by_user, :close_accept_def_by_user, :open_val_def_by_user, :close_val_def_by_user,
                                          :register_results_questions_show_evaluation, :register_results_questions_download_evaluation_to_activate,
                                          :register_results_questions_download_evaluation_to_assess, :register_results_all_evaluations_questions_xls]
  before_filter :validate_evaluation_to_entered_by_manager, only: [:register_results_show_evaluation, :register_results, :register_results_xls]

  before_filter :validate_evaluation_to_entered_by_manager_questions, only: [:register_results_questions_show_evaluation,
                                                                             :register_results_questions_download_evaluation_to_activate,
                                                                             :register_results_questions_download_evaluation_to_assess,
                                                                             :register_results_questions_xls]

  before_filter :validate_definition_by_managers, only: [:modify_evaluations_new, :modify_evaluations_create, :modify_evaluations_edit, :modify_evaluations_update, :modify_evaluations_destroy]

  def list_processes_2

  end

  def edit_dates_2

  end

  def update_dates_2

    @pe_process.from_date = params[:pe_process][:from_date]
    @pe_process.to_date = params[:pe_process][:to_date]

    @pe_process.pe_evaluations.each do |pe_evaluation|

      unless pe_evaluation.entered_by_manager

        @pe_process.pe_rels.each do |pe_rel|

          pe_evaluation_rel = pe_evaluation.pe_evaluation_rel(pe_rel)

          if pe_evaluation_rel

            pe_evaluation_rel.from_date = params['eval_by_'+pe_evaluation.id.to_s+'_'+pe_rel.rel.to_s+'_from_date']
            pe_evaluation_rel.to_date = params['eval_by_'+pe_evaluation.id.to_s+'_'+pe_rel.rel.to_s+'_to_date']

            pe_evaluation_rel.save

          end

        end

      end

    end

    @pe_process.save

    flash[:success] = t('activerecord.success.model.pe_managing.update_dates_ok')

    redirect_to pe_managing_edit_dates_2_path @pe_process

  end

  def manage_def_by_users

    @pe_rels_def_by = Array.new

    @pe_evaluation.pe_elements.each do |pe_element|

      if pe_element.element_def_by == 1

        pe_element.element_def_by_rol.split('-').each do |e|
          unless e.blank?

            pe_rel = @pe_process.pe_rel_by_id e.to_i

            @pe_rels_def_by.push pe_rel if pe_rel

          end
        end

      end

    end

    @pe_rels_def_by.uniq!


  end

  def manage_def_by_user
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    @user_search = User.new

    if params[:user] && (!params[:user][:apellidos].blank? || !params[:user][:nombre].blank?)

      @users_search = search_normal_users(params[:user][:apellidos], params[:user][:nombre])

      @user_search.apellidos = params[:user][:apellidos]
      @user_search.nombre = params[:user][:nombre]

    end

    @pe_rels_def_by = Array.new

    @pe_evaluation.pe_elements.each do |pe_element|

      if pe_element.element_def_by == 1

        pe_element.element_def_by_rol.split('-').each do |e|
          unless e.blank?

            pe_rel = @pe_process.pe_rel_by_id e.to_i

            @pe_rels_def_by.push pe_rel if pe_rel

          end
        end

      end

    end

    @pe_rels_def_by.uniq!

  end

  def open_def_by_user

    @pe_evaluation.pe_questions_by_pe_member_all_levels(@pe_member_rel.pe_member_evaluated, @pe_member_rel.pe_member_evaluator, nil, nil).each do |pe_question|

      pe_question.pe_question_validations.destroy_all

      pe_question.ready = false
      pe_question.confirmed = false

      pe_question.save

    end

    flash[:success] = t('activerecord.success.model.pe_managing.open_definition_ok')

    redirect_to pe_managing_def_by_user_path @pe_member_rel.pe_member_evaluated, @pe_evaluation
    #redirect_to pe_managing_def_by_users_path @pe_evaluation

  end

  def close_def_by_user

    @pe_evaluation.pe_questions_by_pe_member_all_levels(@pe_member_rel.pe_member_evaluated, @pe_member_rel.pe_member_evaluator, nil, nil).each do |pe_question|

      pe_question.confirmed = true
      pe_question.save

    end

    flash[:success] = t('activerecord.success.model.pe_managing.close_definition_ok')

    redirect_to pe_managing_def_by_user_path @pe_member_rel.pe_member_evaluated, @pe_evaluation
    #redirect_to pe_managing_def_by_users_path @pe_evaluation

  end

  def open_accept_def_by_user

    @pe_evaluation.pe_questions_by_pe_member_all_levels(@pe_member_rel.pe_member_evaluated, @pe_member_rel.pe_member_evaluator, nil, nil).each do |pe_question|

      pe_question.pe_question_validations.where('before_accept = ?', false).destroy_all

      pe_question.ready = false
      pe_question.accepted = false

      pe_question.save

    end

    flash[:success] = t('activerecord.success.model.pe_managing.open_accept_definition_ok')

    redirect_to pe_managing_def_by_user_path @pe_member_rel.pe_member_evaluated, @pe_evaluation
    #redirect_to pe_managing_def_by_users_path @pe_evaluation

  end

  def close_accept_def_by_user

    @pe_evaluation.pe_questions_by_pe_member_all_levels(@pe_member_rel.pe_member_evaluated, @pe_member_rel.pe_member_evaluator, nil, nil).each do |pe_question|

      pe_question.accepted = true
      pe_question.save

    end

    flash[:success] = t('activerecord.success.model.pe_managing.close_accept_definition_ok')

    redirect_to pe_managing_def_by_user_path @pe_member_rel.pe_member_evaluated, @pe_evaluation
    #redirect_to pe_managing_def_by_users_path @pe_evaluation

  end

  def open_val_def_by_user

    num_step = params[:num_step]
    before = params[:before] == '1' ? true : false

    @pe_evaluation.pe_questions_by_pe_member_all_levels(@pe_member_rel.pe_member_evaluated, @pe_member_rel.pe_member_evaluator, nil, nil).each do |pe_question|

      pe_question.pe_question_validations.where('step_number = ? AND before_accept = ?', num_step, before).destroy_all

      pe_question.ready = false
      pe_question.accepted = false if before

      pe_question.save

    end

    flash[:success] = t('activerecord.success.model.pe_managing.open_val_definition_ok')

    redirect_to pe_managing_def_by_user_path @pe_member_rel.pe_member_evaluated, @pe_evaluation
    #redirect_to pe_managing_def_by_users_path @pe_evaluation

  end

  def close_val_def_by_user

    num_step = params[:num_step]
    before = params[:before] == '1' ? true : false

    @pe_evaluation.pe_questions_by_pe_member_all_levels(@pe_member_rel.pe_member_evaluated, @pe_member_rel.pe_member_evaluator, nil, nil).each do |pe_question|

      pe_question.pe_question_validations.destroy_all

      pqv = pe_question.pe_question_validations.build
      pqv.step_number = num_step
      pqv.before_accept = before
      pqv.validated = true
      pqv.pe_evaluation = @pe_evaluation
      pqv.save

    end

    flash[:success] = t('activerecord.success.model.pe_managing.close_val_definition_ok')

    redirect_to pe_managing_def_by_user_path @pe_member_rel.pe_member_evaluated, @pe_evaluation
    #redirect_to pe_managing_def_by_users_path @pe_evaluation

  end

  def destroy_validator_def_by_user
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    num_step = params[:num_step]
    before = params[:before] == '1' ? true : false

    pdbuv = @pe_member.pe_definition_by_user_validator(@pe_evaluation, num_step, before)

    pdbuv.destroy if pdbuv
    redirect_to pe_managing_def_by_user_path @pe_member, @pe_evaluation

  end

  def change_boss_def_by_user
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    pe_rel_boss = @pe_process.pe_rel_boss

    user_evaluator = User.find(params[:user_id])

    if user_evaluator && pe_rel_boss

      pe_member_evaluator = @pe_process.pe_members.where('user_id = ?', user_evaluator.id).first

      if pe_member_evaluator

        pe_member_evaluator.is_evaluator = true

      else

        pe_member_evaluator = @pe_process.pe_members.build
        pe_member_evaluator.is_evaluator = true

        pe_member_evaluator.user = user_evaluator

      end

      if pe_member_evaluator && pe_member_evaluator.save

        pe_member_evaluated_rel_is_evaluated = @pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first

        unless pe_member_evaluated_rel_is_evaluated
          pe_member_evaluated_rel_is_evaluated = @pe_member.pe_member_rels_is_evaluated.build
          pe_member_evaluated_rel_is_evaluated.pe_process = @pe_process
          pe_member_evaluated_rel_is_evaluated.pe_rel = pe_rel_boss
        end

        pe_member_evaluated_rel_is_evaluated.pe_member_evaluator = pe_member_evaluator

        pe_member_evaluated_rel_is_evaluated.save

        @pe_member.feedback_provider = user_evaluator

        @pe_member.save

      end

    end

    redirect_to pe_managing_def_by_user_path @pe_member, @pe_evaluation

  end


  def change_val_def_by_user
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    num_step = params[:num_step]
    before = params[:before] == '1' ? true : false

    pdbuv = @pe_member.pe_definition_by_user_validator(@pe_evaluation, num_step, before)

    unless pdbuv
      pdbuv = @pe_member.pe_definition_by_user_validators.build
      pdbuv.pe_evaluation = @pe_evaluation
      pdbuv.pe_process = @pe_process
      pdbuv.step_number = num_step
      pdbuv.before_accept = before
    end

    pdbuv.validator_id = params[:user_id]
    pdbuv.save

    redirect_to pe_managing_def_by_user_path @pe_member, @pe_evaluation

  end


  def manage_assessments



  end

  def manage_assessment

    @user_search = User.new

    if params[:user] && (!params[:user][:apellidos].blank? || !params[:user][:nombre].blank?)

      @users_search = search_normal_users(params[:user][:apellidos], params[:user][:nombre])

      @user_search.apellidos = params[:user][:apellidos]
      @user_search.nombre = params[:user][:nombre]

    end

  end



  def open_assessment

    @pe_member_rel.finished = false
    @pe_member_rel.validated = false
    @pe_member_rel.validated_at = nil
    @pe_member_rel.save

    open_the_whole_assessment @pe_member_rel.pe_member_evaluated

    flash[:success] = t('activerecord.success.model.pe_managing.open_assessment_ok')

    redirect_to pe_managing_manage_assessment_path @pe_member_rel.pe_member_evaluated

  end

  def finish_assessment

    @pe_member_rel.finished = true
    @pe_member_rel.save

    finish_the_whole_assessment @pe_member_rel.pe_member_evaluated

    flash[:success] = t('activerecord.success.model.pe_managing.finish_assessment_ok')

    redirect_to pe_managing_manage_assessment_path @pe_member_rel.pe_member_evaluated

  end

  def open_whole_assessment

    open_the_whole_assessment @pe_member

    flash[:success] = t('activerecord.success.model.pe_managing.open_assessment_ok')

    redirect_to pe_managing_manage_assessment_path @pe_member

  end

  def finish_whole_assessment

    finish_the_whole_assessment @pe_member, true

    flash[:success] = t('activerecord.success.model.pe_managing.finish_assessment_ok')

    redirect_to pe_managing_manage_assessment_path @pe_member

  end

  def create_evaluator

    user_evaluator = User.find(params[:user_id])
    pe_rel = @pe_process.pe_rels.where('id = ?', params[:pe_rel][:id]).first

    if user_evaluator && pe_rel

      if @pe_process.of_persons?

        pe_member_evaluator = @pe_process.pe_members.where('user_id = ?', user_evaluator.id).first

        if pe_member_evaluator

          pe_member_evaluator.is_evaluator = true

        else

          pe_member_evaluator = @pe_process.pe_members.build
          pe_member_evaluator.is_evaluator = true

          pe_member_evaluator.user = user_evaluator

        end

      else

        pe_area_evaluator = @pe_process.pe_areas.where('name = ?', params[:area]).first

        unless pe_area_evaluator

          pe_area_evaluator = @pe_process.pe_areas.build
          pe_area_evaluator.name = params[:area]

        end

        pe_area_evaluator.is_evaluator = true

        if pe_area_evaluator.save

          pe_member_evaluator = @pe_process.pe_members.where('user_id = ? AND pe_area_id = ?', user_evaluator.id, pe_area_evaluator.id).first

          if pe_member_evaluator

            pe_member_evaluator.is_evaluator = true

          else

            pe_member_evaluator = @pe_process.pe_members.build
            pe_member_evaluator.is_evaluator = true

            pe_member_evaluator.user = user_evaluator
            pe_member_evaluator.pe_area = pe_area_evaluator

          end

        end

      end

      if pe_member_evaluator && pe_member_evaluator.save

        pe_member_evaluated_rel_is_evaluated = @pe_member.pe_member_rel_is_evaluated_by_pe_member(pe_member_evaluator)

        unless pe_member_evaluated_rel_is_evaluated
          pe_member_evaluated_rel_is_evaluated = @pe_member.pe_member_rels_is_evaluated.build
          pe_member_evaluated_rel_is_evaluated.pe_member_evaluator = pe_member_evaluator
          pe_member_evaluated_rel_is_evaluated.pe_process = @pe_process
        end

        pe_member_evaluated_rel_is_evaluated.pe_rel = pe_rel
        pe_member_evaluated_rel_is_evaluated.weight = params[:weight]

        if pe_member_evaluated_rel_is_evaluated.save

          @pe_member.is_evaluated = true

          #rel_index == 1 => jefe
          @pe_member.feedback_provider = user_evaluator if pe_rel.rel == 1

          @pe_member.save

          open_the_whole_assessment @pe_member

          flash[:success] = t('activerecord.success.model.pe_managing.add_evaluator_ok')

        end

      end

    end

    redirect_to pe_managing_manage_assessment_path @pe_member

  end

  def create_validator

    @pe_member_rel.validator = @user_validator
    @pe_member_rel.validated = false
    @pe_member_rel.validated_at = nil
    @pe_member_rel.save

    pe_member_evaluated = @pe_member_rel.pe_member_evaluated

    pe_member_evaluated.require_validation = true
    pe_member_evaluated.step_validation = false
    pe_member_evaluated.step_validation_date = nil
    pe_member_evaluated.save

    flash[:success] = t('activerecord.success.model.pe_managing.add_validator_ok')
    redirect_to pe_managing_manage_assessment_path @pe_member_rel.pe_member_evaluated

  end

  def destroy_validator
    @pe_member_rel.validator = nil
    @pe_member_rel.save

    pe_member_evaluated = @pe_member_rel.pe_member_evaluated

    if pe_member_evaluated.pe_member_rels_is_evaluated.where('validator_id is not null').size == 0
      pe_member_evaluated.require_validation = false
      pe_member_evaluated.save
    end

    flash[:success] = t('activerecord.success.model.pe_managing.delete_validator_ok')
    redirect_to pe_managing_manage_assessment_path @pe_member_rel.pe_member_evaluated
  end

  def destroy_evaluator

    pe_member_evaluated = @pe_member_rel.pe_member_evaluated

    pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

      pe_evaluation.pe_questions_by_pe_member_evaluated_evaluator_all_levels(pe_member_evaluated, @pe_member_rel.pe_member_evaluator).destroy_all

    end

    pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_rel.destroy

    pe_member_evaluated.reload

    pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

      calculate_assessment_final_evaluation pe_member_evaluated, pe_evaluation

      calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

    end

    pe_member_evaluated.set_pe_box
    pe_member_evaluated.save

    unless pe_member_evaluated.pe_process.of_persons?

      pe_area = pe_member_evaluated.pe_area
      pe_area.set_pe_box
      pe_area.save

    end

    calculate_assessment_groups pe_member_evaluated

    finish_the_whole_assessment pe_member_evaluated

    pe_member_evaluator.reload
    unless pe_member_evaluator.pe_member_rels_is_evaluator.size > 0
      pe_member_evaluator.is_evaluator = 0
      pe_member_evaluator.save
    end

    flash[:success] = t('activerecord.success.model.pe_managing.destroy_evaluator_ok')

    redirect_to pe_managing_manage_assessment_path pe_member_evaluated

  end

  def manage_feedbacks

  end

  def manage_feedback

    @user_search = User.new

    if params[:user] && (!params[:user][:apellidos].blank? || !params[:user][:nombre].blank?)

      @users_search = search_normal_users(params[:user][:apellidos], params[:user][:nombre])

      @user_search.apellidos = params[:user][:apellidos]
      @user_search.nombre = params[:user][:nombre]

    end

  end

  def open_feedback

    @pe_member.step_feedback = false
    @pe_member.step_feedback_accepted = false
    @pe_member.save

    flash[:success] = t('activerecord.success.model.pe_managing.open_feedback_ok')

    redirect_to pe_managing_manage_feedback_path @pe_member

  end

  def finish_feedback

    @pe_member.step_feedback = true
    @pe_member.step_feedback_date = lms_time
    @pe_member.save

    menu_name = 'Evaluación de desempeño'

    ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

      if ct_menu_cod == 'pe'
        menu_name = ct_menus_user_menu_alias[index_m]
        break
      end
    end

    #LmsMailer.ask_pe_member_to_accept_feedback(@pe_member, @pe_process, menu_name, @company).deliver if @pe_process.has_step_feedback_accepted? && @pe_process.step_feedback_accepted_send_mail?

    if @pe_process.has_step_feedback_accepted? && @pe_process.step_feedback_accepted_send_mail?

      menu_name = 'Evaluación de desempeño'

      ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

        if ct_menu_cod == 'pe'
          menu_name = ct_menus_user_menu_alias[index_m]
          break
        end
      end

      begin
        PeMailer.step_feedback_accept_ask_to_accept(@pe_process, @company, menu_name, @pe_member.user, @pe_member.feedback_provider).deliver
      rescue Exception => e
        lm = LogMailer.new
        lm.module = 'pe'
        lm.step = 'def_by_user:feedback:ask_to_accept'
        lm.description = e.message+' '+e.backtrace.inspect
        lm.registered_at = lms_time
        lm.save
      end

    end

    flash[:success] = t('activerecord.success.model.pe_managing.close_feedback_ok')

    redirect_to pe_managing_manage_feedback_path @pe_member

  end

  def create_feedback_provider

    feedback_provider = User.find(params[:user_id])

    @pe_member.feedback_provider = feedback_provider

    @pe_member.save

    flash[:success] = t('activerecord.success.model.pe_managing.create_feedback_provider_ok')

    redirect_to pe_managing_manage_feedback_path @pe_member

  end

  def detroy_feedback_provider

    @pe_member.feedback_provider = nil
    @pe_member.save

    flash[:success] = t('activerecord.success.model.pe_managing.destroy_feedback_provider_ok')

    redirect_to pe_managing_manage_feedback_path @pe_member

  end

  def modify_evaluations_list_people

  end

  def modify_evaluations_show
    @pe_member_evaluated = @pe_member

  end

  def modify_evaluations_new
    @goals = Array.new
    @percentages = Array.new

    @pe_question_ranks_non_discrete = Array.new
    @pe_question_ranks_discrete = Array.new

  end

  def modify_evaluations_create
    @goals = Array.new
    @percentages = Array.new

    @pe_question = PeQuestion.new(params[:pe_question])
    @pe_question.weight = fix_form_floats params[:pe_question][:weight] if params[:pe_question][:weight]
    @pe_question.power = fix_form_floats params[:pe_question][:power] if params[:pe_question][:power]
    @pe_question.leverage = fix_form_floats params[:pe_question][:leverage] if params[:pe_question][:leverage]
    @pe_question.pe_evaluation = @pe_evaluation
    @pe_question.pe_member_evaluated = @pe_member

    @pe_question.weight = 0 unless @pe_question.pe_element.assessed
    @pe_question.confirmed = true
    @pe_question.accepted = true
    @pe_question.validated = true
    @pe_question.ready = true
    @pe_question.creator = user_connected

    @pe_question_ranks_non_discrete = Array.new
    @pe_question_ranks_discrete = Array.new

    if @pe_question.kpi_indicator

      #@pe_question.description = @pe_question.kpi_indicator.name

    end

    if @pe_question.save

      rank_errors_non_discrete = false

      rank_errors_discrete = false

      unless @pe_question.kpi_indicator

        if @pe_question.indicator_type == 3
          #rangos
          if params[:pe_question_rank_non_discrete_create]

            @pe_question_ranks_non_discrete = Array.new

            cache_goals = Array.new
            cache_percentages = Array.new

            params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

              pe_question_rank = @pe_question.pe_question_ranks.build

              pe_question_rank.goal = fix_form_floats params[:pe_question_rank_non_discrete][index][:goal] if params[:pe_question_rank_non_discrete][index][:goal]
              pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_non_discrete][index][:percentage] if params[:pe_question_rank_non_discrete][index][:percentage]

              rank_errors_non_discrete = true unless pe_question_rank.valid?

              if cache_goals.include? pe_question_rank.goal
                rank_errors_non_discrete = true
                pe_question_rank.errors[:goal] << ' no puede estar repetida'
              end

              cache_goals.push pe_question_rank.goal

              if cache_percentages.include? pe_question_rank.percentage
                rank_errors_non_discrete = true
                pe_question_rank.errors[:percentage] << ' no puede estar repetido'
              end

              cache_percentages.push pe_question_rank.percentage

              pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_non_discrete][index][:model] if params[:pe_question_rank_non_discrete][index][:model]

              @pe_question_ranks_non_discrete.push pe_question_rank

            end

            unless rank_errors_non_discrete

              @pe_question.pe_question_ranks.destroy_all

              params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

                pe_question_rank = @pe_question.pe_question_ranks.build

                pe_question_rank.goal = fix_form_floats params[:pe_question_rank_non_discrete][index][:goal] if params[:pe_question_rank_non_discrete][index][:goal]
                pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_non_discrete][index][:percentage] if params[:pe_question_rank_non_discrete][index][:percentage]

                pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_non_discrete][index][:model] if params[:pe_question_rank_non_discrete][index][:model]

                pe_question_rank.save

              end

              @pe_question.reload
              @pe_question.set_goal_rank
              @pe_question.save

            end

          end

        end


        if @pe_question.indicator_type == 4
          #discreto
          if params[:pe_question_rank_discrete_create]

            @pe_question_ranks_discrete = Array.new

            cache_goals = Array.new
            cache_percentages = Array.new

            params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

              pe_question_rank = @pe_question.pe_question_ranks.build

              pe_question_rank.goal = 0
              pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
              pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_discrete][index][:percentage] if params[:pe_question_rank_discrete][index][:percentage]

              rank_errors_discrete = true unless pe_question_rank.valid?

              if cache_goals.include? pe_question_rank.discrete_goal
                rank_errors_non_discrete = true
                pe_question_rank.errors[:discrete_goal] << ' no puede estar repetida'
              end

              cache_goals.push pe_question_rank.discrete_goal

              if cache_percentages.include? pe_question_rank.percentage
                rank_errors_non_discrete = true
                pe_question_rank.errors[:percentage] << ' no puede estar repetido'
              end

              cache_percentages.push pe_question_rank.percentage

              pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_discrete][index][:model] if params[:pe_question_rank_discrete][index][:model]

              @pe_question_ranks_discrete.push pe_question_rank

            end

            unless rank_errors_discrete

              @pe_question.pe_question_ranks.destroy_all

              params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

                pe_question_rank = @pe_question.pe_question_ranks.build

                pe_question_rank.goal = 0
                pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
                pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_discrete][index][:percentage] if params[:pe_question_rank_discrete][index][:percentage]

                pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_discrete][index][:model] if params[:pe_question_rank_discrete][index][:model]

                pe_question_rank.save

              end

              @pe_question.reload
              @pe_question.set_goal_rank
              @pe_question.save

            end

          end

        end

      end

      if rank_errors_non_discrete || rank_errors_discrete
        @pe_element = @pe_question.pe_element
        render 'modify_evaluations_new'
      else

        update_results(@pe_question.pe_member_evaluated)

        flash[:success] = t('activerecord.success.model.pe_question.create_ok')
        redirect_to pe_managing_modify_evaluations_show_path @pe_member
      end

    else

      @pe_element = @pe_question.pe_element

      if @pe_question.indicator_type == 3

        if params[:pe_question_rank_non_discrete_create]

          params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

            pe_question_rank = @pe_question.pe_question_ranks.build

            pe_question_rank.goal = params[:pe_question_rank_non_discrete][index][:goal]
            pe_question_rank.percentage = params[:pe_question_rank_non_discrete][index][:percentage]

            @pe_question_ranks_non_discrete.push pe_question_rank

          end

        end

      end

      if @pe_question.indicator_type == 4

        if params[:pe_question_rank_discrete_create]

          params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

            pe_question_rank = @pe_question.pe_question_ranks.build

            pe_question_rank.goal = 0
            pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
            pe_question_rank.percentage = params[:pe_question_rank_discrete][index][:percentage]

            @pe_question_ranks_discrete.push pe_question_rank

          end

        end

      end

      render 'modify_evaluations_new'
    end

  end

  def modify_evaluations_edit
    @goals = Array.new
    @percentages = Array.new

    @pe_question_ranks_non_discrete = @pe_question.pe_question_ranks_non_discrete
    @pe_question_ranks_discrete = @pe_question.pe_question_ranks_discrete

  end

  def modify_evaluations_update

    @pe_question_ranks_non_discrete = @pe_question.pe_question_ranks_non_discrete
    @pe_question_ranks_discrete = @pe_question.pe_question_ranks_discrete

    params[:pe_question][:weight] = fix_form_floats(params[:pe_question][:weight]) if params[:pe_question][:weight]

    if @pe_question.update_attributes(params[:pe_question])

      rank_errors_non_discrete = false

      if @pe_question.indicator_type == 3
        #rangos
        if params[:pe_question_rank_non_discrete_create]

          @pe_question_ranks_non_discrete = Array.new

          cache_goals = Array.new
          cache_percentages = Array.new

          params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

            pe_question_rank = @pe_question.pe_question_ranks.build

            pe_question_rank.goal = fix_form_floats params[:pe_question_rank_non_discrete][index][:goal] if params[:pe_question_rank_non_discrete][index][:goal]
            pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_non_discrete][index][:percentage] if params[:pe_question_rank_non_discrete][index][:percentage]

            rank_errors_non_discrete = true unless pe_question_rank.valid?

            if cache_goals.include? pe_question_rank.goal
              rank_errors_non_discrete = true
              pe_question_rank.errors[:goal] << ' no puede estar repetida'
            end

            cache_goals.push pe_question_rank.goal

            if cache_percentages.include? pe_question_rank.percentage
              rank_errors_non_discrete = true
              pe_question_rank.errors[:percentage] << ' no puede estar repetido'
            end

            cache_percentages.push pe_question_rank.percentage

            pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_non_discrete][index][:model] if params[:pe_question_rank_non_discrete][index][:model]

            @pe_question_ranks_non_discrete.push pe_question_rank

          end

          unless rank_errors_non_discrete

            @pe_question.pe_question_ranks.destroy_all

            params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

              pe_question_rank = @pe_question.pe_question_ranks.build

              pe_question_rank.goal = fix_form_floats params[:pe_question_rank_non_discrete][index][:goal] if params[:pe_question_rank_non_discrete][index][:goal]
              pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_non_discrete][index][:percentage] if params[:pe_question_rank_non_discrete][index][:percentage]

              pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_non_discrete][index][:model] if params[:pe_question_rank_non_discrete][index][:model]

              pe_question_rank.save

            end

            @pe_question.reload
            @pe_question.set_goal_rank
            @pe_question.save

          end

        end

      end

      rank_errors_discrete = false

      if @pe_question.indicator_type == 4
        #discreto
        if params[:pe_question_rank_discrete_create]

          @pe_question_ranks_discrete = Array.new

          cache_goals = Array.new
          cache_percentages = Array.new

          params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

            pe_question_rank = @pe_question.pe_question_ranks.build

            pe_question_rank.goal = 0
            pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
            pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_discrete][index][:percentage] if params[:pe_question_rank_discrete][index][:percentage]

            rank_errors_discrete = true unless pe_question_rank.valid?

            if cache_goals.include? pe_question_rank.discrete_goal
              rank_errors_non_discrete = true
              pe_question_rank.errors[:discrete_goal] << ' no puede estar repetida'
            end

            cache_goals.push pe_question_rank.discrete_goal

            if cache_percentages.include? pe_question_rank.percentage
              rank_errors_non_discrete = true
              pe_question_rank.errors[:percentage] << ' no puede estar repetido'
            end

            cache_percentages.push pe_question_rank.percentage

            pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_discrete][index][:model] if params[:pe_question_rank_discrete][index][:model]

            @pe_question_ranks_discrete.push pe_question_rank

          end

          unless rank_errors_discrete

            @pe_question.pe_question_ranks.destroy_all

            params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

              pe_question_rank = @pe_question.pe_question_ranks.build

              pe_question_rank.goal = 0
              pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
              pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_discrete][index][:percentage] if params[:pe_question_rank_discrete][index][:percentage]

              pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_discrete][index][:model] if params[:pe_question_rank_discrete][index][:model]

              pe_question_rank.save

            end

            @pe_question.reload
            @pe_question.set_goal_rank
            @pe_question.save

          end

        end

      end

      if rank_errors_non_discrete || rank_errors_discrete
        render 'modify_evaluations_edit'
      else

        update_results(@pe_question.pe_member_evaluated)

        flash[:success] = t('activerecord.success.model.pe_question.create_ok')
        redirect_to pe_managing_modify_evaluations_show_path @pe_member
      end

    else
      render 'modify_evaluations_edit'
    end

  end

  def modify_evaluations_destroy

    @pe_question.destroy

    pe_member_evaluated = @pe_question.pe_member_evaluated

    update_results(pe_member_evaluated)



    flash[:success] = t('activerecord.success.model.pe_question.delete_ok')
    redirect_to pe_managing_modify_evaluations_show_path @pe_member

  end

  def register_results_list_people

  end

  def register_results_show

    @pe_rel_boss = @pe_process.pe_rel_by_id 1
    @pe_member_evaluated = @pe_member
    #@pe_member_rel = @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(@pe_rel_boss).first

    #@pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    #@pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_rels = @pe_process.pe_rels

  end

  def register_results_assess

    @pe_member_evaluated = @pe_member

    @pe_process.pe_rels.each do |pe_rel|

      pe_member_rel = @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).first

      if pe_member_rel && @pe_evaluation.entered_by_manager_questions? && @pe_evaluation.has_pe_evaluation_rel(pe_member_rel.pe_rel) && @pe_member_evaluated.has_to_be_assessed_in_evaluation(@pe_evaluation)
        @pe_rel = pe_rel
      end

    end

    @pe_rel = @pe_process.pe_rel_by_id 1

    @pe_member_rel = @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(@pe_rel).first

    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_connected = nil

    @pe_member_group = @pe_member_evaluated.pe_member_group(@pe_evaluation)

   #@company = session[:company]

    if @pe_evaluation.entered_by_manager_questions

      pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation @pe_evaluation
      pe_assessment_evaluation.destroy if pe_assessment_evaluation

      pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluations.build
      pe_assessment_evaluation.pe_process = @pe_process
      pe_assessment_evaluation.pe_evaluation = @pe_evaluation
      pe_assessment_evaluation.last_update = lms_time

      pe_assessment_evaluation.points = -1

      pe_assessment_evaluation.percentage = -1

      pe_assessment_evaluation.save

      @pe_evaluation.pe_questions_only_by_pe_member_1st_level_ready(@pe_member_evaluated).each do |pe_question_1st_level|

        if pe_question_1st_level.indicator_type == 0 || pe_question_1st_level.indicator_type == 1 || pe_question_1st_level.indicator_type == 2 || pe_question_1st_level.indicator_type == 5

          unless params['pe_question_indicator_goal_'+pe_question_1st_level.id.to_s].blank?

            pe_question_1st_level.indicator_goal = fix_form_floats params['pe_question_indicator_goal_'+pe_question_1st_level.id.to_s]
            pe_question_1st_level.save

          end

        end

        @pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          if pe_question_2nd_level.indicator_type == 0 || pe_question_2nd_level.indicator_type == 1 || pe_question_2nd_level.indicator_type == 2 || pe_question_2nd_level.indicator_type == 5

            unless params['pe_question_indicator_goal_'+pe_question_2nd_level.id.to_s].blank?

              pe_question_2nd_level.indicator_goal = fix_form_floats params['pe_question_indicator_goal_'+pe_question_2nd_level.id.to_s]
              pe_question_2nd_level.save

            end

          end

          @pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level.id).each do |pe_question_3rd_level|

            if pe_question_3rd_level.indicator_type == 0 || pe_question_3rd_level.indicator_type == 1 || pe_question_3rd_level.indicator_type == 2 || pe_question_3rd_level.indicator_type == 5

              unless params['pe_question_indicator_goal_'+pe_question_3rd_level.id.to_s].blank?

                pe_question_3rd_level.indicator_goal = fix_form_floats params['pe_question_indicator_goal_'+pe_question_3rd_level.id.to_s]
                pe_question_3rd_level.save

              end

            end

          end

        end

      end

      answer_evaluation pe_assessment_evaluation

      calculate_assessment_evaluation pe_assessment_evaluation

      calculate_assessment_final_evaluation(@pe_member_evaluated, @pe_evaluation, false)

      calculate_assessment_dimension @pe_member_evaluated, @pe_evaluation.pe_dimension

      @pe_member_evaluated.set_pe_box
      @pe_member_evaluated.save

      calculate_assessment_groups(@pe_member_evaluated)

    end

    @pe_rel_boss = @pe_process.pe_rel_by_id 1
    @pe_rels = @pe_process.pe_rels

    render 'register_results_show'

  end


  def register_results_list_evaluations

  end

  def register_results_show_evaluation

  end

  def register_results

    @pe_process.evaluated_members_ordered.readonly(false).each do |pe_member|

      pe_assessment_final_evaluation = pe_member.pe_assessment_final_evaluation @pe_evaluation
      pe_assessment_final_evaluation.destroy if pe_assessment_final_evaluation

      unless params['res_'+pe_member.id.to_s].blank?

        pe_assessment_final_evaluation = pe_member.pe_assessment_final_evaluations.build
        pe_assessment_final_evaluation.pe_process = @pe_process
        pe_assessment_final_evaluation.pe_evaluation = @pe_evaluation
        pe_assessment_final_evaluation.last_update = lms_time

        pe_assessment_final_evaluation.points = params['res_'+pe_member.id.to_s]
        pe_assessment_final_evaluation.percentage = params['res_'+pe_member.id.to_s]

        pe_assessment_final_evaluation.registered_by_manager = true

        if pe_assessment_final_evaluation.save

          calculate_assessment_dimension pe_member, @pe_evaluation.pe_dimension

          pe_member.set_pe_box
          pe_member.save

          unless @pe_process.of_persons?

            pe_area = pe_member.pe_area
            pe_area.set_pe_box
            pe_area.save

          end

          calculate_assessment_groups pe_member

          finish_the_whole_assessment pe_member

        end

      end

    end

    flash[:success] = t('activerecord.success.model.pe_managing.register_results_ok')

    redirect_to pe_managing_register_results_show_evaluation_path @pe_evaluation


  end

  def register_results_xls

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:xls_file_results]

      if File.extname(params[:xls_file_results].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:xls_file_results].read) }

        check_xls_file_results archivo_temporal

        if @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.pe_managing.update_file_error')
          render 'register_results_show_evaluation'
        else

          save_xls_file_results archivo_temporal

          flash[:success] = t('activerecord.success.model.pe_managing.register_results_ok')

          redirect_to pe_managing_register_results_show_evaluation_path @pe_evaluation

        end

      else

        flash.now[:danger] = t('activerecord.error.model.pe_managing.upload_file_no_xlsx')
        render 'register_results_show_evaluation'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.pe_managing.upload_file_no_file')
      render 'register_results_show_evaluation'

    end

  end

  def register_results_questions_xls

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:xls_file_results]

      if File.extname(params[:xls_file_results].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:xls_file_results].read) }

        #check_xls_file_results archivo_temporal

        if @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.pe_managing.update_file_error')
          render 'register_results_questions_show_evaluation'
        else

          save_xls_file_results_questions archivo_temporal

          flash[:success] = t('activerecord.success.model.pe_managing.register_results_ok')

          redirect_to pe_managing_register_questions_results_show_evaluation_path @pe_evaluation

        end

      else

        flash.now[:danger] = t('activerecord.error.model.pe_managing.upload_file_no_xlsx')
        render 'register_results_questions_show_evaluation'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.pe_managing.upload_file_no_file')
      render 'register_results_questions_show_evaluation'

    end

  end

  def register_results_all_evaluations_questions_xls


    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:xls_file_results]

      if File.extname(params[:xls_file_results].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:xls_file_results].read) }

        #check_xls_file_results archivo_temporal

        if @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.pe_managing.update_file_error')
          render 'register_results_list_people'
        else

          save_xls_file_results_all_evaluations_questions archivo_temporal

          flash[:success] = t('activerecord.success.model.pe_managing.register_results_ok')

          redirect_to pe_managing_register_results_list_people_path @pe_process

        end

      else

        flash.now[:danger] = t('activerecord.error.model.pe_managing.upload_file_no_xlsx')
        render 'register_results_list_people'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.pe_managing.upload_file_no_file')
      render 'register_results_list_people'

    end


  end

  ##########

  def register_results_questions_show_evaluation

  end

  def register_results_questions_download_evaluation_to_activate

    filename = ('Definir registro por gestor ' + @pe_evaluation.name).gsub(/\s/,'_') + '.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def register_results_questions_download_evaluation_to_assess

    filename = ('Registro por gestor ' + @pe_evaluation.name).gsub(/\s/,'_') + '.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def download_excel_register_results_evaluations

    filename = ('Evaluaciones ' + @pe_process.name).gsub(/\s/,'_') + '.xls'

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def processes_list

  end

  def edit_dates

  end

  def update_dates

    @hr_process.fecha_inicio = params[:hr_process][:fecha_inicio]
    @hr_process.fecha_fin = params[:hr_process][:fecha_fin]
    @hr_process.save

    @hr_process.current_hr_process_evaluations.each do |hr_process_evaluation|

      unless hr_process_evaluation.hr_process_dimension_e.registro_por_gestor?

        if hr_process_evaluation.hr_process_dimension_e.eval_jefe
          hr_process_evaluation.fecha_inicio_eval_jefe = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_inicio_eval_jefe]
          hr_process_evaluation.fecha_fin_eval_jefe = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_fin_eval_jefe]
          hr_process_evaluation.save
        end

        if hr_process_evaluation.hr_process_dimension_e.eval_auto
          hr_process_evaluation.fecha_inicio_eval_auto = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_inicio_eval_auto]
          hr_process_evaluation.fecha_fin_eval_auto = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_fin_eval_auto]
          hr_process_evaluation.save
        end

        if hr_process_evaluation.hr_process_dimension_e.eval_sub
          hr_process_evaluation.fecha_inicio_eval_sub = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_inicio_eval_sub]
          hr_process_evaluation.fecha_fin_eval_sub = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_fin_eval_sub]
          hr_process_evaluation.save
        end

        if hr_process_evaluation.hr_process_dimension_e.eval_par
          hr_process_evaluation.fecha_inicio_eval_par = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_inicio_eval_par]
          hr_process_evaluation.fecha_fin_eval_par = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_fin_eval_par]
          hr_process_evaluation.save
        end

        if hr_process_evaluation.hr_process_dimension_e.eval_cli
          hr_process_evaluation.fecha_inicio_eval_cli = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_inicio_eval_cli]
          hr_process_evaluation.fecha_fin_eval_cli = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_fin_eval_cli]
          hr_process_evaluation.save
        end

        if hr_process_evaluation.hr_process_dimension_e.eval_prov
          hr_process_evaluation.fecha_inicio_eval_prov = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_inicio_eval_prov]
          hr_process_evaluation.fecha_fin_eval_prov = params[:hr_process_evaluation][hr_process_evaluation.id.to_s.to_sym][:fecha_fin_eval_prov]
          hr_process_evaluation.save
        end

      end

    end

    flash[:success] = t('activerecord.success.model.hr_process_assessment.edit_dates_ok')

    redirect_to pe_managing_edit_dates_path(@hr_process)

  end

  def register_results_1

  end

  def register_results_2

  end

  def register_results_3

    hr_process_dimension = @hr_process_evaluation.hr_process_dimension_e.hr_process_dimension

    @hr_process.hr_process_users_evaluados.each do |hr_process_user|

      hr_process_assessment_ef = HrProcessAssessmentEf.where('hr_process_evaluation_id = ? AND hr_process_user_id = ?', @hr_process_evaluation.id, hr_process_user.id).first

      existe_prev = false

      if hr_process_assessment_ef
        resultado_porcentaje_prev = hr_process_assessment_ef.resultado_porcentaje
        registered_by_manager_prev = hr_process_assessment_ef.registered_by_manager
        existe_prev = true
      end

      HrProcessAssessmentEf.where('hr_process_evaluation_id = ? AND hr_process_user_id = ?', @hr_process_evaluation.id, hr_process_user.id).destroy_all

      params[:hr_process_assessment_ef][hr_process_user.id.to_s.to_sym].strip!

      if params[:hr_process_assessment_ef][hr_process_user.id.to_s.to_sym] != ''

        hr_process_assessment_ef = HrProcessAssessmentEf.new
        hr_process_assessment_ef.fecha = lms_time
        hr_process_assessment_ef.hr_process_evaluation = @hr_process_evaluation
        hr_process_assessment_ef.hr_process_user = hr_process_user
        hr_process_assessment_ef.resultado_puntos = params[:hr_process_assessment_ef][hr_process_user.id.to_s.to_sym]
        hr_process_assessment_ef.resultado_porcentaje = params[:hr_process_assessment_ef][hr_process_user.id.to_s.to_sym]

        if existe_prev

          if hr_process_assessment_ef.resultado_porcentaje != resultado_porcentaje_prev
            hr_process_assessment_ef.registered_by_manager = true
          else
            hr_process_assessment_ef.registered_by_manager = registered_by_manager_prev
          end

        else

          hr_process_assessment_ef.registered_by_manager = true

        end

        hr_process_assessment_ef.save

      end

      resultado_dimension = 0
      exiten_registros_ef = false

      @hr_process.hr_process_evaluations.each do |hr_p_e|

        if hr_p_e.hr_process_dimension.id == hr_process_dimension.id

          hr_p_a_ef = HrProcessAssessmentEf.where('hr_process_evaluation_id = ? AND hr_process_user_id = ?', hr_p_e.id, hr_process_user.id).first

          if hr_p_a_ef

            resultado_dimension += hr_p_a_ef.resultado_porcentaje*hr_p_e.hr_process_dimension_e.porcentaje
            exiten_registros_ef = true

          end

        end

      end

      if exiten_registros_ef

        resultado_dimension = resultado_dimension/100

        hr_process_assessment_d = hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

        unless hr_process_assessment_d

          hr_process_assessment_d = hr_process_user.hr_process_assessment_ds.new
          hr_process_assessment_d.hr_process_dimension = hr_process_dimension

        end

        hr_process_assessment_d.resultado_porcentaje = resultado_dimension

        #hr_process_dimension_g_match = hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').where('valor_minimo <= ? AND valor_maximo >= ?', hr_process_assessment_d.resultado_porcentaje, hr_process_assessment_d.resultado_porcentaje).first

        hr_process_dimension_g_match = nil

        hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

          if hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

            hr_process_dimension_g_match = hr_process_dimension_g
            break

          end

        end


        hr_process_assessment_d.hr_process_dimension_g = hr_process_dimension_g_match if hr_process_dimension_g_match

        hr_process_assessment_d.fecha = lms_time

        hr_process_assessment_d.save

      else

        hr_process_user.hr_process_assessment_ds.where('hr_process_dimension_id = ?',hr_process_dimension.id).destroy_all

      end

      hr_process_quadrant = hr_process_user.quadrant

      if hr_process_quadrant
        qua = hr_process_user.hr_process_assessment_qua
        unless qua
          qua = HrProcessAssessmentQua.new
          qua.hr_process_user = hr_process_user
        end
        qua.fecha = lms_time
        qua.hr_process_quadrant = hr_process_quadrant
        qua.save

      else
        qua = hr_process_user.hr_process_assessment_qua
        qua.destroy if qua
      end


    end

    flash[:success] = t('activerecord.success.model.hr_process_assessment.register_results_ok')

    redirect_to pe_managing_register_results_2_path(@hr_process_evaluation)

  end

  private

  def get_data_1
    @pe_process = PeProcess.find(params[:pe_process_id])
  end


  def get_data_2
    @pe_member = PeMember.find(params[:pe_member_id])
    @pe_process = @pe_member.pe_process
   #@company = session[:company]
  end

  def get_data_3

    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_process = @pe_member_rel.pe_process
  end

  def get_data_4
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
  end

  def get_data_5
    @pe_member_rel = @pe_process.pe_member_rels.where('id = ?', params[:pe_member_rel_id]).first
  end

  def get_data_6
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel][:id])
    @pe_process = @pe_member_rel.pe_process
    @user_validator = User.find(params[:user_id])
  end

  def get_data_7

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
    @pe_member = PeMember.find(params[:pe_member_id])

    @pe_question = @pe_evaluation.pe_questions.build

    if params[:pe_question_id] == '0'

      @pe_element = @pe_evaluation.pe_elements.first
      @pe_question_parent = nil

    else

      @pe_question_parent = @pe_evaluation.pe_questions.where('pe_questions.id = ?', params[:pe_question_id]).first
      @pe_element = @pe_question_parent.pe_element.pe_elements.first

    end

    @pe_question.pe_question_id = @pe_question_parent.id if @pe_question_parent
    @pe_question.pe_member_evaluated_id = @pe_member.id
    @pe_question.pe_element_id = @pe_element.id

  end

  def get_data_7a

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
    @pe_member = PeMember.find(params[:pe_member_id])

  end

  def get_data_8
    @pe_question = PeQuestion.find(params[:pe_question_id])
    @pe_element = @pe_question.pe_element
    @pe_evaluation = @pe_question.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
    @pe_member = @pe_question.pe_member_evaluated

  end


  def verify_is_a_manager

    unless user_connected.pe_managers.count > 0 || user_connected.hr_process_managers.count > 0
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end

  end

  def validate_manager

    unless @pe_process.pe_managers.where('user_id = ?', user_connected.id).count > 0
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

  def validate_evaluation_to_entered_by_manager
    unless @pe_evaluation.entered_by_manager
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

  def validate_evaluation_to_entered_by_manager_questions
    unless @pe_evaluation.entered_by_manager_questions
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

  ###############

  def validate_open_process

    unless @hr_process.abierto && @hr_process.fecha_inicio < lms_time && @hr_process.fecha_fin > lms_time
      flash[:danger] = t('activerecord.error.model.hr_process_assessment.out_of_time_to_manage')
      redirect_to root_path
    end

  end

  def access_to_manage_process

    @hr_process = HrProcess.find(params[:hr_process_id])

    unless user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).count > 0

      flash[:danger] = t('security.no_access_hr_process_manager')
      redirect_to root_path

    end

  end

  def access_to_register_results

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])

    @hr_process = @hr_process_evaluation.hr_process

    unless user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).count > 0

      flash[:danger] = t('security.no_access_hr_process_manager')
      redirect_to root_path

    end

  end

  def validate_definition_by_managers
    unless @pe_evaluation.allow_definition_by_managers?
      flash[:danger] = t('security.no_access_hr_process_manager')
      redirect_to root_path
    end
  end

  def check_xls_file_results(archivo_temporal)

    pe_evaluated_user_codes = @pe_process.pe_evaluated_user_codes

    @lineas_error = []
    @lineas_error_messages = []

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        #0 codigo
        #1 resultado

        unless pe_evaluated_user_codes.include? fila[0]

          @lineas_error.push index+1
          @lineas_error_messages.push ['Participante evaluado '+fila[0].to_s+' no existe']

        end

      end

    end

  end

  def save_xls_file_results(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        #0 codigo
        #1 resultado

        pe_member = @pe_process.pe_evaluated_member_by_user_code fila[0]

        if pe_member

          pe_assessment_final_evaluation = pe_member.pe_assessment_final_evaluation @pe_evaluation
          pe_assessment_final_evaluation.destroy if pe_assessment_final_evaluation

          if fila[1] && !fila[1].blank?

            pe_assessment_final_evaluation = pe_member.pe_assessment_final_evaluations.build
            pe_assessment_final_evaluation.pe_process = @pe_process
            pe_assessment_final_evaluation.pe_evaluation = @pe_evaluation
            pe_assessment_final_evaluation.last_update = lms_time

            pe_assessment_final_evaluation.points = fila[1]
            pe_assessment_final_evaluation.percentage = fila[1]

            pe_assessment_final_evaluation.registered_by_manager = true

            if pe_assessment_final_evaluation.save

              calculate_assessment_dimension pe_member, @pe_evaluation.pe_dimension

              pe_member.set_pe_box
              pe_member.save

              unless @pe_process.of_persons?

                pe_area = pe_member.pe_area
                pe_area.set_pe_box
                pe_area.save

              end

              calculate_assessment_groups pe_member

              finish_the_whole_assessment pe_member

            end

          end



        end

      end

    end

  end

  def save_xls_file_results_questions(archivo_temporal)

    pos_logro = 5
    pos_logro += @pe_evaluation.pe_elements.size
    pos_logro += 3 if @pe_evaluation.pe_elements.last.assessment_method == 2

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        logro = fila[pos_logro]
        pe_question_id = fila[pos_logro+2]

        pe_question = @pe_evaluation.pe_questions.where('id = ?', pe_question_id).first

        if pe_question

          pe_element = pe_question.pe_element

          if pe_element.assessment_method == 1
            #alternativas

            pe_alternative = pe_question.pe_alternatives.where('description = ?', logro).first unless logro.blank?

            if pe_alternative

              pe_question.pe_alternative = pe_alternative
              pe_question.save

            end

          elsif pe_element.assessment_method == 2

            #indicadores

            if pe_element.kpi_dashboard && pe_question.kpi_indicator

              if pe_question.indicator_type == 4
                pe_question.indicator_discrete_achievement = pe_question.kpi_indicator.value
              else
                pe_question.indicator_achievement = pe_question.kpi_indicator.value
              end

              pe_question.save

            else

              unless logro.blank?

                if pe_question.indicator_type == 4
                  pe_question.indicator_discrete_achievement = logro
                else
                  pe_question.indicator_achievement = logro
                end
                pe_question.save

              end

            end

          elsif pe_element.assessment_method == 3
            #valor numérico

            unless logro.blank?

              pe_question.points = logro
              pe_question.save

            end

          end

        end

      end

    end

  end

  def save_xls_file_results_all_evaluations_questions(archivo_temporal)

    pe_rel_boss = @pe_process.pe_rel_by_id 1

    members_included_in_file = Array.new

    pos_logro = 0

    if @pe_process.of_persons?

      pos_logro += 3

      pos_logro += @pe_process.pe_characteristics_rep_assessment_status.size

    else

      pos_logro += 4

    end

    pos_logro += 2

    max_num_elements = 1

    pe_evaluations = @pe_process.pe_evaluations

    general_indicator_power_leverage = false
    general_indicator_note = false

    pe_evaluations.each do |pe_evaluation|

      max_num_elements = pe_evaluation.pe_elements.size if pe_evaluation.pe_elements.size > max_num_elements

      available_indicator_types_selected_array = Array.new

      available_indicator_types_selected_array = pe_evaluation.pe_elements.last.available_indicator_types.split('-') if pe_evaluation.pe_elements && pe_evaluation.pe_elements.last && pe_evaluation.pe_elements.last.available_indicator_types && pe_evaluation.pe_elements.last.assessment_method == 2

      general_indicator_power_leverage = true if available_indicator_types_selected_array.size > 0 && available_indicator_types_selected_array.include?('5')

      general_indicator_note = true if pe_evaluation.pe_elements.last && pe_evaluation.pe_elements.last.allow_note

    end

    pos_logro += max_num_elements

    pos_logro += 1 if general_indicator_note

    pos_logro += 2

    pos_logro += 2 if general_indicator_power_leverage

    pos_logro += 1
    pos_logro += 1


    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    pe_member_prev = nil
    pe_member_current = nil

    pe_eval_prev = nil
    pe_eval_current = nil

    data.each_with_index do |fila, index|

      if index > 0 && !fila[0].blank? && !fila[1].blank?

        members_included_in_file.push fila[1]

        pe_member_current = fila[1]

        pe_question_id = fila[0]

        pe_question = PeQuestion.where('pe_questions.id = ?', pe_question_id).first

        if pe_question && pe_question.pe_evaluation.pe_process_id == @pe_process.id

          pe_eval_current = pe_question.pe_evaluation_id

          if pe_member_current != pe_member_prev || (pe_member_current == pe_member_prev && pe_eval_current != pe_eval_prev)

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[1]

            #pe_member_rel = pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first
            #pe_member_rel = nil
            #@pe_process.pe_rels.each do |pe_rel|
              pe_member_rel = pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first
              if pe_member_rel && pe_question.pe_evaluation.has_pe_evaluation_rel(pe_member_rel.pe_rel) && pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_question.pe_evaluation)
                if pe_question.pe_evaluation.entered_by_manager_questions && pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_question.pe_evaluation)

                end
              end
            #end

            if pe_member_rel

              pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_question.pe_evaluation

              unless pe_assessment_evaluation

                pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluations.build
                pe_assessment_evaluation.pe_process = @pe_process
                pe_assessment_evaluation.pe_evaluation = pe_question.pe_evaluation
                pe_assessment_evaluation.last_update = lms_time

                pe_assessment_evaluation.points = -1

                pe_assessment_evaluation.percentage = -1

                pe_assessment_evaluation.save

              end

              pe_question.pe_evaluation.pe_questions_only_by_pe_member_1st_level_ready(pe_member_rel.pe_member_evaluated).each do |pe_question_1st_level|

                if pe_question_1st_level.pe_member_evaluator_id.nil? || pe_question_1st_level.pe_member_evaluator_id == pe_member_rel.pe_member_evaluator_id

                  pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_question(pe_question_1st_level)

                  unless pe_assessment_question_1st_level

                    pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_questions.build
                    pe_assessment_question_1st_level.pe_question = pe_question_1st_level
                    pe_assessment_question_1st_level.save

                  end

                  pe_question.pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

                    if pe_question_2nd_level.pe_member_evaluator_id.nil? || pe_question_2nd_level.pe_member_evaluator_id == pe_member_rel.pe_member_evaluator_id

                      pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd_level)

                      unless pe_assessment_question_2nd_level

                        pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_questions.build
                        pe_assessment_question_2nd_level.pe_question = pe_question_2nd_level
                        pe_assessment_question_2nd_level.pe_assessment_question = pe_assessment_question_1st_level
                        pe_assessment_question_2nd_level.save

                      end

                      pe_question.pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level.id).each do |pe_question_3rd_level|

                        if pe_question_3rd_level.pe_member_evaluator_id.nil? || pe_question_3rd_level.pe_member_evaluator_id == pe_member_rel.pe_member_evaluator_id

                          pe_assessment_question_3rd_level = pe_assessment_evaluation.pe_assessment_question(pe_question_3rd_level)

                          unless pe_assessment_question_3rd_level

                            pe_assessment_question_3rd_level = pe_assessment_evaluation.pe_assessment_questions.build
                            pe_assessment_question_3rd_level.pe_question = pe_question_3rd_level
                            pe_assessment_question_3rd_level.pe_assessment_question = pe_assessment_question_2nd_level
                            pe_assessment_question_3rd_level.save

                          end

                        end

                      end

                    end

                  end

                end

              end

            end

          end

          pe_member_prev = pe_member_current

          pe_eval_prev = pe_question.pe_evaluation_id

        end

      end
    end

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        logro = fila[pos_logro]
        pe_question_id = fila[0]


        pe_question = PeQuestion.where('pe_questions.id = ?', pe_question_id).first

        if pe_question && pe_question.pe_evaluation.pe_process_id == @pe_process.id

          if fila[pos_logro-1] && pe_question.pe_element.assessment_method == 2

            if pe_question.indicator_type == 0 || pe_question.indicator_type == 1 || pe_question.indicator_type == 2 || pe_question.indicator_type == 5

              pe_question.indicator_goal = fila[pos_logro-1]
              pe_question.save

            end

          end

          pe_member_evaluated = @pe_process.pe_member_by_user_code fila[1]

          #pe_member_rel = pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first

          #pe_member_rel = nil
          #@pe_process.pe_rels.each do |pe_rel|
            pe_member_rel = pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first
            if pe_member_rel && pe_question.pe_evaluation.has_pe_evaluation_rel(pe_member_rel.pe_rel) && pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_question.pe_evaluation)
              if pe_question.pe_evaluation.entered_by_manager_questions && pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_question.pe_evaluation)
                #break
              end
            end
          #end

          if pe_member_rel

            pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_question.pe_evaluation

            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

            if pe_assessment_question

              pe_element = pe_question.pe_element

              if pe_element.assessment_method == 1
                #alternativas

                pe_alternative = pe_question.pe_alternatives.where('description = ?', logro).first unless logro.blank?

                if pe_alternative

                  pe_assessment_question.pe_alternative = pe_alternative
                  pe_assessment_question.points = pe_alternative.value

                  pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

                  pe_assessment_question.last_update = lms_time

                  pe_assessment_question.save

                else

                  pe_assessment_question.destroy


                end

              elsif pe_element.assessment_method == 2

                #indicadores

                if pe_element.kpi_dashboard && pe_question.kpi_indicator

                  if pe_question.indicator_type == 4
                    pe_assessment_question.indicator_discrete_achievement = pe_question.kpi_indicator.value
                  else
                    pe_assessment_question.indicator_achievement = pe_question.kpi_indicator.value
                  end

                  pe_assessment_question.percentage = pe_question.kpi_indicator.percentage

                  pe_assessment_question.points = pe_question.kpi_indicator.percentage

                  pe_assessment_question.last_update = lms_time
                  pe_assessment_question.save

                else

                  if logro.blank?

                    pe_assessment_question.destroy

                  else

                    if pe_question.indicator_type == 4
                      pe_assessment_question.indicator_discrete_achievement = logro
                      pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_discrete_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, pe_question.pe_evaluation.min_percentage, pe_question.pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
                    else
                      pe_assessment_question.indicator_achievement = logro
                      pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, pe_question.pe_evaluation.min_percentage, pe_question.pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
                    end



                    pe_assessment_question.points = pe_assessment_question.percentage

                    pe_assessment_question.last_update = lms_time
                    pe_assessment_question.save

                  end

                end

              elsif pe_element.assessment_method == 3
                #valor numérico

                if logro.blank?

                  pe_assessment_question.destroy

                else

                  pe_assessment_question.points = logro

                  pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, pe_question.pe_evaluation.points_to_ohp, pe_question.pe_evaluation.min_percentage, pe_question.pe_evaluation.max_percentage

                  pe_assessment_question.last_update = lms_time
                  pe_assessment_question.save

                end

              end

              @pe_evaluation = pe_question.pe_evaluation
              @pe_member_evaluated = pe_member_rel.pe_member_evaluated
              @pe_member_evaluator = pe_member_rel.pe_member_evaluator

              calculate_assessment_evaluation pe_assessment_evaluation

            end

          end

        end

      end

    end

    members_included_in_file.uniq!

    members_included_in_file.each do |member_code|

      pe_member_evaluated = @pe_process.pe_member_by_user_code member_code

      pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

        calculate_assessment_final_evaluation pe_member_evaluated, pe_evaluation, false

        calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

      end

      pe_member_evaluated.set_pe_box
      pe_member_evaluated.save

      unless pe_member_evaluated.pe_process.of_persons?

        pe_area = pe_member_evaluated.pe_area
        pe_area.set_pe_box
        pe_area.save

      end

      calculate_assessment_groups pe_member_evaluated

      finish_the_whole_assessment pe_member_evaluated

    end



  end

  def save_xls_file_results_questions_(archivo_temporal)

    pos_logro = 5
    pos_logro += @pe_evaluation.pe_elements.size
    pos_logro += 3 if @pe_evaluation.pe_elements.last.assessment_method == 2

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        @pe_member_evaluated = @pe_process.pe_evaluated_member_by_user_code fila[0]
        @pe_member_evaluator = @pe_process.pe_evaluator_member_by_user_code fila[2]

        if @pe_member_evaluated #&& @pe_member_evaluator

          #@pe_member_rel = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluator

          @pe_member_rel = @pe_member_evaluated.pe_member_rels_is_evaluated_as_rel_id_ordered(1).first

          if @pe_member_rel

            @pe_rel = @pe_member_rel.pe_rel

            pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation @pe_evaluation

            unless pe_assessment_evaluation

              pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluations.build
              pe_assessment_evaluation.pe_process = @pe_process
              pe_assessment_evaluation.pe_evaluation = @pe_evaluation
              pe_assessment_evaluation.last_update = lms_time

              pe_assessment_evaluation.points = -1

              pe_assessment_evaluation.percentage = -1

              pe_assessment_evaluation.save

            end

            if @pe_evaluation.pe_elements.size == 1
              manager_answer_evaluation_xls_1_element pe_assessment_evaluation, fila[pos_logro], fila[pos_logro+2]
            end

            if @pe_evaluation.pe_elements.size == 2
              manager_answer_evaluation_xls_2_elements pe_assessment_evaluation, fila[pos_logro], fila[pos_logro+2]
            end

            if @pe_evaluation.pe_elements.size == 3
              manager_answer_evaluation_xls_3_elements pe_assessment_evaluation, fila[pos_logro], fila[pos_logro+2]
            end

            #calculate_assessment_evaluation pe_assessment_evaluation

          end

        end

      end

    end

  end

  def update_results(pe_member_evaluated)

    pe_process = pe_member_evaluated.pe_process
    pe_rel_boss = pe_process.pe_rel_by_id 1

    pe_member_rel = pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first

    pe_process.pe_evaluations.each do |pe_evaluation|

      pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_evaluation

      if pe_assessment_evaluation

        @pe_evaluation = pe_evaluation
        @pe_member_evaluated = pe_member_evaluated
        @pe_member_evaluator = nil
        @pe_rel = nil

        re_calculate_answer_evaluation pe_assessment_evaluation

        calculate_assessment_evaluation pe_assessment_evaluation

        calculate_assessment_final_evaluation pe_member_evaluated, pe_evaluation

        calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

      end

    end

    pe_member_evaluated.set_pe_box
    pe_member_evaluated.save

    unless pe_member_evaluated.pe_process.of_persons?

      pe_area = pe_member_evaluated.pe_area
      pe_area.set_pe_box
      pe_area.save

    end

    calculate_assessment_groups pe_member_evaluated

    finish_the_whole_assessment pe_member_evaluated

  end

end
