class CandidatesController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module


  # GET /candidates
  # GET /candidates.json
  def index
    candidates_aux = JmCandidate.where(:internal_user => false)
    @candidates = Array.new

    if params[:jm_subchar]

      @subchar_filters = params[:jm_subchar]


      candidates_aux.each do |candidate|
        match_count = 0

        @subchar_filters.each_key do |subchar_id|
          subchar = JmSubcharacteristic.find(subchar_id)
          subchar_match = false
          JmAnswerValue.joins(:jm_candidate_answer).where(:jm_subcharacteristic_id => subchar_id, :jm_candidate_answers => {:active => true, :jm_candidate_id => candidate.id}).each do |answer_value|
            unless subchar_match
              if subchar.option_type == 5 # texto largo
                subchar_match = true if answer_value.value.include?(@subchar_filters[subchar_id])
              else
                subchar_match = true if @subchar_filters[subchar_id].include?(answer_value.value)
              end
            end
          end
          match_count += 1 if subchar_match
        end

        if match_count == @subchar_filters.count
          @candidates << candidate
        end

      end

    else
      @candidates = candidates_aux
    end


    #['Texto', 'Entero','Fecha', 'Selección', 'Archivo', 'Texto largo', 'Real']
    @jm_subcharacteristics_filter = JmSubcharacteristic.where(:option_type => [0, 1, 2, 3, 5]).joins(:jm_profile_char_to_reports).
        joins(:jm_characteristic => {:jm_profile_chars => :jm_profile_form}).
        where(:jm_profile_forms => {:active => true}, :jm_profile_char_to_reports => {:use_to_filter_candidates => true}).uniq.reorder('position_filter_candidates ASC')

    #['Texto', 'Entero','Fecha', 'Selección', 'Archivo', 'Texto largo', 'Real']
    @jm_subcharacteristics_table = JmSubcharacteristic.where(:option_type => [0, 1, 2, 3, 5]).joins(:jm_profile_char_to_reports).
        joins(:jm_characteristic => {:jm_profile_chars => :jm_profile_form}).
        where(:jm_profile_forms => {:active => true}, :jm_profile_char_to_reports => {:use_to_list_candidates => true}).uniq.reorder('position_list_candidates ASC')


  end

  # GET /candidates/1
  # GET /candidates/1.json
  def show
    @candidate = JmCandidate.find(params[:jm_candidate_id])
    @jm_profile_form = JmProfileForm.where(:active => true).first
    @layout = @jm_profile_form.layout

    if params[:section_id]
      @selected_section = JmProfileSection.where(:id => params[:section_id]).first
    end

    if params[:sel_process_id]
      @sel_process = SelProcess.find(params[:sel_process_id])
    end
  end

  # GET /candidates/new
  # GET /candidates/new.json
  def new
    @candidate = JmCandidate.new
  end

  # GET /candidates/1/edit
  def edit
    @candidate = JmCandidate.find(params[:jm_candidate_id])
    @jm_profile_form = JmProfileForm.where(:active => true).first
    @layout = @jm_profile_form.layout
    @filling_profile = true

    if params[:section_id]
      @selected_section = JmProfileSection.where(:id => params[:section_id]).first
    end

    if params[:sel_process_id]
      @sel_process = SelProcess.find(params[:sel_process_id])
    end
  end

  def edit_basic_data
    @candidate = JmCandidate.find(params[:jm_candidate_id])
  end

  # POST /candidates
  # POST /candidates.json
  def create
    @candidate = JmCandidate.new(params[:jm_candidate])

    @candidate.password = SecureRandom.hex(6).to_s

    if !@candidate.email.blank? && @candidate.save

      if (params[:photo]) && save_photo(params[:photo])
        photo = @candidate.build_jm_candidate_photo
        photo.assign_attributes(:crypted_name => params[:photo].original_filename, :file_extension => File.extname(params[:photo].original_filename))
        photo.save
      end

      flash[:success] = 'El Candidato fue creado corectamente'
      redirect_to edit_sel_candidate_path(@candidate)
    else
      @candidate.valid?
      @candidate.errors.add(:email, 'no puede ser vacío') if @candidate.email.blank?
      flash.now[:danger] = 'El Candidato no pudo ser creado corectamente'
      render action: "new"
    end
  end

  # PUT /candidates/1
  # PUT /candidates/1.json
  def update
    @candidate = JmCandidate.find(params[:jm_candidate_id])

    if params[:sel_process_id]
      @sel_process = SelProcess.find(params[:sel_process_id])
    end

    if params[:section_id]
      selected_section = JmProfileSection.where(:id => params[:section_id]).first
    end

    files_to_save = []

    @candidate.assign_attributes(params[:jm_candidate])
    @candidate.jm_candidate_answers.each_with_index do |answer|
      if answer.new_record?
        answer.registered_at = lms_time
        answer.registered_by_user_id = user_connected.id
      end
      answer.jm_answer_values.each do |value|

        if value.new_record? && value.value_file
          value.value_file_crypted_name = SecureRandom.hex(5)
          files_to_save.append(value.dup)
          value.value_file = value.value_file.original_filename
        end
      end
    end

    if @candidate.save

      files_to_save.each do |answer_value|
        file = answer_value.value_file
        save_file(file, @company, answer_value)

      end
      flash[:success] = t('activerecord.success.model.jm_candidate.update_ok')
      if @sel_process
        if selected_section
          redirect_to sel_candidate_from_process_section_open_path(@candidate, @sel_process, selected_section)
        else
          redirect_to sel_candidate_from_process_path(@candidate, @sel_process)
        end

      else
        if selected_section
          redirect_to sel_candidate_section_open_path(@candidate, selected_section)
        else
          redirect_to sel_candidate_path(@candidate)
        end
      end

    else
      flash.now[:danger] = 'Los datos no se pudieron actualizar correctamente'

      @jm_profile_form = JmProfileForm.where(:active => true).first
      @layout = @jm_profile_form.layout
      @filling_profile = true

      render action: 'edit'
    end
  end


  def update_basic_data
    @candidate = JmCandidate.find(params[:jm_candidate_id])

    if (params[:photo]) && save_photo(params[:photo])
      @candidate.build_jm_candidate_photo.assign_attributes(:crypted_name => params[:photo].original_filename, :file_extension => File.extname(params[:photo].original_filename))
    end

    if @candidate.update_attributes(params[:jm_candidate])
      flash[:success] = t('activerecord.success.model.jm_candidate.update_ok')
      redirect_to sel_candidate_path(@candidate)
    else
      flash.now[:danger] = 'Los datos no se pudieron actualizar correctamente'

      @jm_profile_form = JmProfileForm.where(:active => true).first
      @layout = @jm_profile_form.layout
      @filling_profile = true

      render action: 'edit_basic_data'
    end
  end

  def add_candidate_comment
    @candidate = JmCandidate.find(params[:jm_candidate_id])
    comment = @candidate.jm_candidate_comments.new(params[:jm_candidate_comment])
    comment.registered_at = lms_time
    comment.registered_by_user = user_connected


    if comment.save
      render :partial => 'candidates/jm_candidate_comments/show', :locals => {:log => comment}
    else
      render :json => false
    end
  end

  def show_processes_to_apply
    @candidate = JmCandidate.find(params[:jm_candidate_id])
    @procesess = SelProcess.active.external
  end


  # DELETE /candidates/1
  # DELETE /candidates/1.json
  def destroy
    @candidate = Candidate.find(params[:id])
    @candidate.destroy

    respond_to do |format|
      format.html { redirect_to candidates_url }
      format.json { head :no_content }
    end
  end

  def show_invitation_form
    @candidate = JmCandidate.find(params[:jm_candidate_id])
    @jm_message = @candidate.jm_messages.build
    @jm_message.sel_process_id = params[:sel_process_id]
    render :layout => false
  end

  def create_invitation
    candidate = JmCandidate.find(params[:jm_candidate_id])
    jm_message = JmMessage.new(params[:jm_message])
    jm_message.jm_candidate = candidate
    jm_message.sel_process_id = params[:sel_process_id]
    jm_message.sender_user = user_connected

    if jm_message.save
      flash[:success] = 'La invitación fue enviada correctamente'
      SelMailer.invitation_to_process(jm_message, @company).deliver
    else
      flash[:danger] = 'La invitación no pudo ser enviada correctamente'
    end
    redirect_to sel_candidate_show_process_to_apply_path(candidate)
  end

  private

  def verify_access_manage_module
    ct_module_jm = CtModule.where('cod = ? AND active = ?', 'jm', true).first
    ct_module_sel = CtModule.where('cod = ? AND active = ?', 'sel', true).first
    unless ct_module_jm && ct_module_sel && ct_module_sel.has_managers? && ct_module_sel.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def save_file(file, company, jm_answer_value)
    # sel_processes tiene una copia de este método para poder enrollar desde el proceso
    file_crypted_name = jm_answer_value.value_file_crypted_name + File.extname(file.original_filename)
    candidate_id = @candidate.id.to_s
    directory = company.directorio+'/'+company.codigo+'/jm_candidates/' + candidate_id + '/'

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') { |f| f.write(file.read) }


  end

  def save_photo(file)
    directory = @company.directorio+'/'+ @company.codigo+'/jm_candidates/' + @candidate.id.to_s + '/profile_photo/'
    file_crypted_name = file.original_filename

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') { |f| f.write(file.read) }
  end

end
