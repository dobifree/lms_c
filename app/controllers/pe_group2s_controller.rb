class PeGroup2sController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  # GET /pe_group2s
  # GET /pe_group2s.json
  def index
    @pe_group2s = PeGroup2.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_group2s }
    end
  end

  # GET /pe_group2s/1
  # GET /pe_group2s/1.json
  def show
    @pe_group2 = PeGroup2.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_group2 }
    end
  end

  # GET /pe_group2s/new
  # GET /pe_group2s/new.json
  def new
    @pe_group2 = PeGroup2.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pe_group2 }
    end
  end

  # GET /pe_group2s/1/edit
  def edit
    @pe_group2 = PeGroup2.find(params[:id])
  end

  # POST /pe_group2s
  # POST /pe_group2s.json
  def create
    @pe_group2 = PeGroup2.new(params[:pe_group2])

    respond_to do |format|
      if @pe_group2.save
        format.html { redirect_to @pe_group2, notice: 'Pe group2 was successfully created.' }
        format.json { render json: @pe_group2, status: :created, location: @pe_group2 }
      else
        format.html { render action: "new" }
        format.json { render json: @pe_group2.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pe_group2s/1
  # PUT /pe_group2s/1.json
  def update
    @pe_group2 = PeGroup2.find(params[:id])

    respond_to do |format|
      if @pe_group2.update_attributes(params[:pe_group2])
        format.html { redirect_to @pe_group2, notice: 'Pe group2 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pe_group2.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pe_group2s/1
  # DELETE /pe_group2s/1.json
  def destroy
    @pe_group2 = PeGroup2.find(params[:id])
    @pe_group2.destroy

    respond_to do |format|
      format.html { redirect_to pe_group2s_url }
      format.json { head :no_content }
    end
  end
end
