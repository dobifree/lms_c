class LmsCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @characteristics = Characteristic.not_grouped_characteristics
    @characteristic_types = CharacteristicType.all
  end

  def edit
    @lms_characteristic = LmsCharacteristic.find(params[:id])
    @characteristic = @lms_characteristic.characteristic

  end

  def update
    @lms_characteristic = LmsCharacteristic.find(params[:id])

    if @lms_characteristic.update_attributes(params[:lms_characteristic])

      redirect_to lms_characteristics_path

    end


  end

end
