class TrackingFormInstancesController < ApplicationController

  include TrackingFormInstancesHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin, :only => [:create_from_admin]

  def index
    @tracking_process = TrackingProcess.find(params[:tracking_process_id])
    @mini_ficha_chars = Characteristic.where(:mini_ficha => true).reorder(:orden_mini_ficha)

    @tracking_form_instances = []
    @my_tracking_form_instances = []

    @tracking_participants = []
    @my_tracking_participants = []

    tracking_form_participants = @tracking_process.tracking_participants.
        where('tracking_participants.attendant_user_id = ? OR tracking_participants.subject_user_id = ?', user_connected.id, user_connected.id)


    tracking_form_participants.each do |participant|
      #participant.tracking_form_instances.each do |instance|
      #@tracking_form_instances.append(instance)
      #end
      instance = participant.tracking_form_instances.joins(:tracking_form).order('tracking_forms.position ASC').last
      if participant.subject_user_id == user_connected.id
        #@my_tracking_form_instances.append(instance) if instance
        @my_tracking_participants.append(participant) if instance
      end

      if participant.attendant_user_id == user_connected.id
        #@tracking_form_instances.append(instance) if instance
        @tracking_participants.append(participant) if instance
      end
    end
  end

  def index_to_fill
    @tracking_process = TrackingProcess.find(params[:tracking_process_id])
    @mini_ficha_chars = Characteristic.where(:mini_ficha => true).order(:orden_mini_ficha)

    @tracking_form_instances = []
    @my_tracking_form_instances = []

    tracking_form_participants = @tracking_process.tracking_participants.
        where('tracking_participants.attendant_user_id = ? OR tracking_participants.subject_user_id = ?', user_connected.id, user_connected.id)


    tracking_form_participants.each do |participant|
      #participant.tracking_form_instances.each do |instance|
      #@tracking_form_instances.append(instance)
      #end
      instance = participant.tracking_form_instances.last
      if participant.subject_user_id == user_connected.id
        @my_tracking_form_instances.append(instance) if instance
      end

      if participant.attendant_user_id == user_connected.id
        @tracking_form_instances.append(instance) if instance
      end
    end
  end

  # GET /tracking_form_instances/1
  # GET /tracking_form_instances/1.json
  def show
    if params[:id]
      ##### Modo de vista mientras se está en definición (o no hay seguimiento)
      tracking_form_instance_open = TrackingFormInstance.find(params[:id])
      @tracking_participant = tracking_form_instance_open.tracking_participant
      @tracking_form_id = tracking_form_instance_open.tracking_form_id
      @pending_instance = @tracking_participant.tracking_form_instances.last
    elsif params[:tracking_participant_id]
      @tracking_participant = TrackingParticipant.find(params[:tracking_participant_id])
      if @tracking_participant.in_redefinition?
        tracking_form_instance_open = @tracking_participant.tracking_form_instances.where(:done => false).first
        @tracking_form_id = tracking_form_instance_open.tracking_form_id
        @pending_instance = @tracking_participant.tracking_form_instances.last
      else
        ##### Modo de vista mientras se está en seguimiento
        @show_tracking_pill = true
        @tracking_form_instances = @tracking_participant.tracking_form_instances.joins(:tracking_form_answers => :tracking_form_due_date).uniq
        @tracking_form_id = -99999999
      end


    end

    am_i_an_attendant = (@tracking_participant.attendant_user_id == user_connected.id)
    am_i_a_subject = (@tracking_participant.subject_user_id == user_connected.id)
    am_i_a_manager = ct_module_tracking_manager?
    am_i_a_boss = user_connected.es_jefe_de(@tracking_participant.subject_user)


    # si estoy involucrado
    unless (am_i_an_attendant || am_i_a_subject || am_i_a_manager || am_i_a_boss)
      flash['danger'] = 'Ud. no tiene ninguna permisos para acceder a este formulario'
      redirect_to tracking_form_instances_path(@tracking_participant.tracking_process)
    else
      if params[:modal]
        #carga del modal de solo lectura
        @hide_link_detail = true
        @hide_link_back = true
        @read_only = true
        render :layout => false
      end
    end


  end

  # GET /tracking_form_instances/new
  # GET /tracking_form_instances/new.json
  def new
    @tracking_form_instance = TrackingFormInstance.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tracking_form_instance }
    end
  end

  def make_form_editable
    ### AKA: reject_action
    @tracking_form_instance = TrackingFormInstance.find(params[:id])

    tracking_form_log_to_reject = @tracking_form_instance.tracking_form_logs.last

    tracking_form_reject = TrackingFormReject.new(
        :tracking_form_log_id => tracking_form_log_to_reject.id,
        :comment => params[:reject_comment],
        :registered_at => lms_time,
        :registered_by_user_id => user_connected.id
    )

    #fill_action = @tracking_form_instance.tracking_form.tracking_form_actions.where(:fill_form => true).first

    current_action = @tracking_form_instance.current_action(user_connected)

    # si no está finalizado, si ya fue llenado, y fue llenado en última instancia por el usuario conectado
    if @tracking_form_instance.can_make_editable?(user_connected) && current_action.can_be_rejected?
      @tracking_form_instance.update_attribute(:filled, false)
      tracking_form_reject.save

      rejected_action = tracking_form_reject.tracking_form_log.tracking_form_action

      # se verifican si existen notificaciones activas para el rechazo de esta acción (rechazo: action_tye => 1)
      current_action.tracking_form_notifications.where(:active => true, :action_type => 1).each do |notification|
        #### envio de notificación de 'acción rechazada'
        user_attendant = @tracking_form_instance.tracking_participant.attendant_user
        user_subject = @tracking_form_instance.tracking_participant.subject_user
        to, cc, subject, body = set_notification_final_values(notification, user_connected, user_attendant, user_subject)
        cc = cc - to
        #if to.any? || cc.any?
        begin
          TrackingMailer.action_rejected(@tracking_form_instance.tracking_participant.tracking_process, @company, to, cc, subject, body).deliver
        rescue Exception => e
          lm = LogMailer.new
          lm.module = 'trkn'
          lm.step = 'definition:action_rejected'
          lm.description = e.message+' '+e.backtrace.inspect
          lm.registered_at = lms_time
          lm.save
        end
        #end
      end


      flash['success'] = 'Se guardó correctamente'
    else
      flash['danger'] = 'No se guardó correctamente'
    end

    #redirect_to @tracking_form_instance.tracking_participant.tracking_form_instances.last
    redirect_to_pill(@tracking_form_instance.tracking_participant)
  end

  # POST /tracking_form_instances
  # POST /tracking_form_instances.json
  def create
    @tracking_form_instance = TrackingFormInstance.new(params[:tracking_form_instance])

    respond_to do |format|
      if @tracking_form_instance.save
        format.html { redirect_to @tracking_form_instance, notice: 'Tracking form instance was successfully created.' }
        format.json { render json: @tracking_form_instance, status: :created, location: @tracking_form_instance }
      else
        format.html { render action: "new" }
        format.json { render json: @tracking_form_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_from_admin
    tracking_form_instance = TrackingFormInstance.where(:tracking_form_id => params[:tracking_form_id], :tracking_participant_id => params[:tracking_participant_id]).first
    unless tracking_form_instance
      tracking_form_instance = TrackingFormInstance.new(:tracking_form_id => params[:tracking_form_id], :tracking_participant_id => params[:tracking_participant_id])
      tracking_form_instance.filled = false
      tracking_form_instance.done = false
      tracking_form_instance.save
    end
    render :json => tracking_form_instance
  end

  # PUT /tracking_form_instances/1
  # PUT /tracking_form_instances/1.json
  def update
    @tracking_form_instance = TrackingFormInstance.find(params[:id])

    update_de_respuestas_y_logs = params[:tracking_form_instance][:tracking_form_logs_attributes] ? true : false
    update_de_milestones = params[:tracking_form_instance][:tracking_form_logs_attributes] ? false : true

    # obtengo el indice del log que estoy por guadar (en la logica secuencial del rails ya que sólo pongo el último en el formulario anidado)
    if update_de_respuestas_y_logs
      if params[:finalizar]
        current_log_id = params[:tracking_form_instance][:tracking_form_logs_attributes].keys[0]

        last_action_to_do = @tracking_form_instance.tracking_form.tracking_form_actions.last

        current_action = TrackingFormAction.find(params[:tracking_form_instance][:tracking_form_logs_attributes][current_log_id.to_s][:tracking_form_action_id])
        @done_action = current_action

        if last_action_to_do.id == current_action.id
          params[:tracking_form_instance][:done] = true
        end
        if current_action.fill_form?
          params[:tracking_form_instance][:filled] = true
        end
        params[:tracking_form_instance][:tracking_form_logs_attributes][current_log_id.to_s][:registered_at] = lms_time
        params[:tracking_form_instance][:tracking_form_logs_attributes][current_log_id.to_s][:registered_by_user_id] = user_connected.id
      else
        params[:tracking_form_instance].delete(:tracking_form_logs_attributes)
      end
    end


    # complementos de información previo al guardado de los milestones
    if update_de_milestones

      states_finish_tracking = @tracking_form_instance.tracking_form.tracking_form_states.where(:finish_tracking => true).pluck(:id)

      params[:tracking_form_instance][:tracking_form_answers_attributes].each_key do |answer_key|
        param_answer = params[:tracking_form_instance][:tracking_form_answers_attributes][answer_key]

        if param_answer[:tracking_form_due_date_attributes][:tracking_milestones_attributes]
          param_answer[:tracking_form_due_date_attributes][:tracking_milestones_attributes].each_key do |milestone_key|
            param_milestone = param_answer[:tracking_form_due_date_attributes][:tracking_milestones_attributes][milestone_key]

            # el estado SIEMPRE debe venir, aunque sea como un hidden si el formulario de validación no lo incluye como select
            unless param_milestone[:tracking_form_state_id].blank?
              # si viene alguno de los campos que se ingresan al registro y no al validar...
              if param_milestone[:fulfilment_date]
                params[:tracking_form_instance][:tracking_form_answers_attributes][answer_key][:tracking_form_due_date_attributes][:tracking_milestones_attributes][milestone_key][:registered_at] = lms_time
                params[:tracking_form_instance][:tracking_form_answers_attributes][answer_key][:tracking_form_due_date_attributes][:tracking_milestones_attributes][milestone_key][:registered_by_user_id] = user_connected.id
              end

              if param_milestone[:validated] == '1'
                params[:tracking_form_instance][:tracking_form_answers_attributes][answer_key][:tracking_form_due_date_attributes][:tracking_milestones_attributes][milestone_key][:validated_at] = lms_time
                params[:tracking_form_instance][:tracking_form_answers_attributes][answer_key][:tracking_form_due_date_attributes][:tracking_milestones_attributes][milestone_key][:validated_by_user_id] = user_connected.id

                if states_finish_tracking.include?(param_milestone[:tracking_form_state_id].to_i)
                  params[:tracking_form_instance][:tracking_form_answers_attributes][answer_key][:tracking_form_due_date_attributes][:finished] = '1'
                end
              end
            end
          end
        end

      end

      #@tracking_form_instance.tracking_form_answers.tracking_form_due_date.tracking_milestones.where(:validated => true, :validated_at => nil).update_all(:validated_at => lms_time, :validated_by_user_id => user_connected.id)
    end


    if @tracking_form_instance.update_attributes(params[:tracking_form_instance])
      if @tracking_form_instance.done? &&
          (@tracking_form_instance.tracking_form.id != @tracking_form_instance.tracking_form.tracking_process.tracking_forms.last.id) &&
          update_de_respuestas_y_logs
        current_form = @tracking_form_instance.tracking_participant.current_form
        if current_form
          TrackingFormInstance.new(:tracking_participant_id => @tracking_form_instance.tracking_participant.id,
                                   :tracking_form_id => current_form.id,
                                   :filled => false,
                                   :done => false).save
        end
      end

      # se cierra la redefinición si es que hay alguna abierta siempre y cuando ya esté terminada la instancia
      if @tracking_form_instance.tracking_participant.in_redefinition? && @tracking_form_instance.filled? && @tracking_form_instance.done?
        @tracking_form_instance.tracking_redefinitions.where(:finished => false).update_all(:finished => true, :applied => true, :finished_at => lms_time)

        # si es el último formulario el que quedó listo en la redefinición, entonces se cierra la redefinición del participant
        if @tracking_form_instance.tracking_participant.tracking_form_instances.joins(:tracking_form).order('tracking_forms.position ASC').last.id == @tracking_form_instance.id
          @tracking_form_instance.tracking_participant.update_attributes(:in_redefinition => false)
        end
      end


      # si marcó 'finalizar' en el llenado del formulario o es otro tipo de acción
      if @done_action
        # se verifica si existen notificaciones activas para la ejecución de esta acción (ejecutar: action_tye => 0)
        user_attendant = @tracking_form_instance.tracking_participant.attendant_user
        user_subject = @tracking_form_instance.tracking_participant.subject_user
        @done_action.tracking_form_notifications.where(:active => true, :action_type => 0).each do |notification|
          #### envio de notificación de 'acción realizada'
          to, cc, subject, body = set_notification_final_values(notification, user_connected, user_attendant, user_subject)
          cc = cc - to
          #if to.any? || cc.any?
          begin
            TrackingMailer.action_done(@tracking_form_instance.tracking_participant.tracking_process, @company, to, cc, subject, body).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'trkn'
            lm.step = 'definition:action_done'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end
          #end
        end
      end


      flash['success'] = 'Los datos se guardaron correctamente'
    else
      flash['danger'] = 'Los datos no se guardaron correctamente'
    end


=begin
    if @tracking_form_instance.tracking_participant.in_redefinition?
      if @tracking_form_instance.done?
        redirect_to @tracking_form_instance.tracking_participant.tracking_form_instances.joins(:tracking_form).order('tracking_forms.position ASC').where('tracking_forms.position > ?', @tracking_form_instance.tracking_form.position).first
      else
        redirect_to @tracking_form_instance
      end
    else
      if @tracking_form_instance.tracking_participant.traceable?
        redirect_to tracking_participant_track_path(@tracking_form_instance.tracking_participant)
      else
        redirect_to @tracking_form_instance.tracking_participant.tracking_form_instances.joins(:tracking_form).order('tracking_forms.position ASC').last
      end
    end
=end

    redirect_to_pill(@tracking_form_instance.tracking_participant)


  end


  def add_milestone

    @tracking_pill = true

    tracking_form_due_date = TrackingFormDueDate.find(params[:tracking_form_due_date_id])
    @tracking_form_instance = tracking_form_due_date.tracking_form_answer.tracking_form_instance
    states_finish_tracking = @tracking_form_instance.tracking_form.tracking_form_states.where(:finish_tracking => true).pluck(:id)
    #@tracking_form_instance.tracking_form.tracking_form_states.where(:finish_tracking => true).pluck(:id)

    param_milestone = params[:tracking_form_due_date][:tracking_milestones_attributes]['0']

    if param_milestone[:fulfilment_date]
      params[:tracking_form_due_date][:tracking_milestones_attributes]['0'][:registered_at] = lms_time
      params[:tracking_form_due_date][:tracking_milestones_attributes]['0'][:registered_by_user_id] = user_connected.id
    end

    if param_milestone[:validated] == '1'
      params[:tracking_form_due_date][:tracking_milestones_attributes]['0'][:validated_at] = lms_time
      params[:tracking_form_due_date][:tracking_milestones_attributes]['0'][:validated_by_user_id] = user_connected.id

      if states_finish_tracking.include?(param_milestone[:tracking_form_state_id].to_i)
        params[:tracking_form_due_date][:finished] = '1'
      end
    end

    if param_milestone[:replied_comment]
      just_replying = true
      params[:tracking_form_due_date][:tracking_milestones_attributes]['0'][:replied_at] = lms_time
      params[:tracking_form_due_date][:tracking_milestones_attributes]['0'][:replied_by_user_id] = user_connected.id
    end

    tracking_form_due_date.update_attributes(params[:tracking_form_due_date])
    ###############

    unless just_replying
      # se verifica si existen notificaciones activas para la ejecución de este estado
      registered_state = TrackingFormState.find(param_milestone[:tracking_form_state_id])
      user_attendant = @tracking_form_instance.tracking_participant.attendant_user
      user_subject = @tracking_form_instance.tracking_participant.subject_user
      registered_state.tracking_state_notifications.where(:active => true).each do |notification|
        #### envio de notificación de 'estado registrado' en el seguimiento
        to, cc, subject, body = set_notification_final_values(notification, user_connected, user_attendant, user_subject)
        cc = cc - to
        #if to.any? || cc.any?
        begin
          TrackingMailer.state_selected(@tracking_form_instance.tracking_participant.tracking_process, @company, to, cc, subject, body).deliver
        rescue Exception => e
          lm = LogMailer.new
          lm.module = 'trkn'
          lm.step = 'tracking:seguimiento'
          lm.description = e.message+' '+e.backtrace.inspect
          lm.registered_at = lms_time
          lm.save
        end
        #end
      end

    end
    ################
    @answer = tracking_form_due_date.tracking_form_answer

    #TODO: estas 5 variables están calculándose de manera redundante en la función @tracking_form_instance.data_for_tracking_form(user_connected)
    am_i_an_attendant = (@tracking_form_instance.tracking_participant.attendant_user_id == user_connected.id)
    am_i_a_subject = (@tracking_form_instance.tracking_participant.subject_user_id == user_connected.id)

    traceable_by_attendant = @tracking_form_instance.tracking_form.traceable_by_attendant?
    traceable_to_subject = @tracking_form_instance.tracking_form.traceable_by_subject?

    @can_fill_milestones = (am_i_an_attendant && traceable_by_attendant) || (am_i_a_subject && traceable_to_subject)

    @builder, @filler, @counterpart = @tracking_form_instance.data_for_tracking_form(user_connected)

    render 'answer_detail_only', :layout => false
  end

  def destroy_milestone
    milestone = TrackingMilestone.find(params[:tracking_milestone_id])
    due_date = milestone.tracking_form_due_date
    if milestone.is_registered_by_user_or_same_role?(user_connected.id)

      if due_date.finished? && milestone.tracking_form_state.finish_tracking?
        due_date.finished = false
        due_date.save!
      end
      milestone.destroy
    end

    #render :json => milestone
    @tracking_pill = true
    @answer = due_date.tracking_form_answer
    @tracking_form_instance = @answer.tracking_form_instance


    #TODO: estas 5 variables están calculándose de manera redundante en la función @tracking_form_instance.data_for_tracking_form(user_connected)
    am_i_an_attendant = (@tracking_form_instance.tracking_participant.attendant_user_id == user_connected.id)
    am_i_a_subject = (@tracking_form_instance.tracking_participant.subject_user_id == user_connected.id)

    traceable_by_attendant = @tracking_form_instance.tracking_form.traceable_by_attendant?
    traceable_to_subject = @tracking_form_instance.tracking_form.traceable_by_subject?

    @can_fill_milestones = (am_i_an_attendant && traceable_by_attendant) || (am_i_a_subject && traceable_to_subject)

    @builder, @filler, @counterpart = @tracking_form_instance.data_for_tracking_form(user_connected)

    render 'answer_detail_only', :layout => false

  end


  # DELETE /tracking_form_instances/1
  # DELETE /tracking_form_instances/1.json
  def destroy
    @tracking_form_instance = TrackingFormInstance.find(params[:id])
    @tracking_form_instance.destroy

    respond_to do |format|
      format.html { redirect_to tracking_form_instances_url }
      format.json { head :no_content }
    end
  end

  def open_instance_for_redefinition
    t_f_instance = TrackingFormInstance.find(params[:id])

    if tracking_is_able_to_open_for_redefinition(t_f_instance, user_connected)

      redefinition = t_f_instance.tracking_redefinitions.create(:required_by_user_id => user_connected.id, :required_at => lms_time)
      original_data = t_f_instance.json_backup_with_children

      t_f_instance.update_attributes(:done => false, :filled => false)
      t_f_instance.tracking_participant.update_attributes(:in_redefinition => true)
      redefinition.update_attributes(:original_data => original_data)

      flash[:success] = 'En Redefinición'
    else
      flash[:danger] = 'No se pudo poner en Redefinición'
    end

    redirect_to_pill t_f_instance.tracking_participant
  end

  def rollback_redefinition
    t_f_instance = TrackingFormInstance.find(params[:id])

    participant = t_f_instance.tracking_participant

    if tracking_is_able_to_rollback_redefinition(t_f_instance, user_connected)
      redefinition = t_f_instance.tracking_redefinitions.where(:finished => false).last
      new_instance = TrackingFormInstance.new()

      t_f_instance.destroy
      new_instance.assign_attributes(ActiveSupport::JSON.decode redefinition.original_data)
      new_instance.save
      redefinition = new_instance.tracking_redefinitions.where(:finished => false).last
      redefinition.update_attributes(:finished => true, :finished_at => lms_time)
      new_instance.tracking_participant.update_attributes(:in_redefinition => false)
      flash[:success] = 'Se revirtieron los valores correctamente'
      redirect_to_pill new_instance.tracking_participant
    else
      flash[:danger] = 'No se revirtieron los valores correctamente'
      redirect_to_pill t_f_instance.tracking_participant
    end

  end

  def close_redefinition
    t_f_instance = TrackingFormInstance.find(params[:id])

    if tracking_is_able_to_close_redefinition(t_f_instance, user_connected)
      t_f_instance.tracking_participant.update_attributes(:in_redefinition => false)
      flash[:success] = 'Los datos se guardaron correctamente'
    else
      flash[:danger] = 'Los datos no se guardaron correctamente'
    end

    redirect_to_pill(t_f_instance.tracking_participant)

  end

  def redirect_to_pill(tracking_participant)
    t_f_instance = calculate_pill_to_open(tracking_participant)

    if t_f_instance
      redirect_to t_f_instance
    else
      redirect_to tracking_participant_track_path(tracking_participant)
    end
  end


  private

  def set_notification_final_values(notification, user_trigger, user_attendant, user_subject)

    to = fix_email_addresses(replace_variables(notification.to, user_trigger, user_attendant, user_subject))
    cc = fix_email_addresses(replace_variables(notification.cc, user_trigger, user_attendant, user_subject))
    subject = notification.subject #replace_variables(notification.subject, user_trigger, user_attendant, user_subject)
    body = replace_variables(notification.body, user_trigger, user_attendant, user_subject)

    return to, cc, subject, body
  end

  def replace_variables(text, user_trigger, user_attendant, user_subject)
=begin
      Ejecutor: %{trigger.nombre_completo}, %{trigger.email}, %{trigger.codigo}
      Contraparte: %{counterpart.nombre_completo}, %{counterpart.email}, %{counterpart.codigo}

      Evaluador: %{attendant.nombre_completo}, %{attendant.email}, %{attendant.codigo}
      Evaluado: %{subject.nombre_completo}, %{subject.email}, %{subject.codigo}
=end
    user_counterpart = user_trigger == user_attendant ? user_subject : user_attendant

    text.gsub! '%{trigger.nombre_completo}', user_trigger.comma_full_name
    text.gsub! '%{trigger.email}', user_trigger.email
    text.gsub! '%{trigger.codigo}', user_trigger.codigo
    text.gsub! '%{counterpart.nombre_completo}', user_counterpart.comma_full_name
    text.gsub! '%{counterpart.email}', user_counterpart.email
    text.gsub! '%{counterpart.codigo}', user_counterpart.codigo

    text.gsub! '%{attendant.nombre_completo}', user_attendant.comma_full_name
    text.gsub! '%{attendant.email}', user_attendant.email
    text.gsub! '%{attendant.codigo}', user_attendant.codigo
    text.gsub! '%{subject.nombre_completo}', user_subject.comma_full_name
    text.gsub! '%{subject.email}', user_subject.email
    text.gsub! '%{subject.codigo}', user_subject.codigo

    return text

  end

  def fix_email_addresses(addresses)
    addresses.gsub!(' ', '')
    array = []
    unless addresses.blank?
      array = addresses.split(',').uniq
      array.each do |email|
        unless ValidateEmail.valid?(email)
          array.delete(email)
        end
      end
    end

    return array
  end
end
