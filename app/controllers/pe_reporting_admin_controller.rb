class PeReportingAdminController < ApplicationController

  before_filter :authenticate_user_admin

  before_filter :get_data_2_4, only: [:rep_detailed_definition_evaluation_admin]

  def rep_detailed_definition_evaluation_admin

    filename = ('Definición de ' + @pe_evaluation.name).gsub(/\s/,'_') + '.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  private

  def get_data_2_4
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
  end

end
