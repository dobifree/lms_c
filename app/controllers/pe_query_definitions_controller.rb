class PeQueryDefinitionsController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list, :accept]
  before_filter :get_data_2, only: [:show]

  def people_list
    @pe_question_comment = PeQuestionComment.new
  end

  def show
    @pe_question_comment = PeQuestionComment.new
  end

  def accept

    @pe_process.pe_evaluations.each do |pe_evaluation|

      pe_evaluation.pe_questions_only_by_pe_member_all_levels(@pe_member_connected).each do |pe_question|

        pe_question.accepted = true
        pe_question.save

      end

    end

    if @pe_process.step_query_definitions_accept_mail?

      menu_name = 'Evaluación de desempeño'

      ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

        if ct_menu_cod == 'pe'
          menu_name = ct_menus_user_menu_alias[index_m]
          break
        end
      end

      begin
        PeMailer.step_query_definitions_accept(@pe_process, @company, menu_name, @pe_member_connected.user).deliver
      rescue Exception => e
        lm = LogMailer.new
        lm.module = 'pe'
        lm.step = 'query_definitions:accept'
        lm.description = e.message+' '+e.backtrace.inspect
        lm.registered_at = lms_time
        lm.save
      end

      unless  @pe_process.pe_process_notification.step_query_definitions_accept_cc.blank?

        ms = @pe_process.pe_process_notification.step_query_definitions_accept_cc.split(',')

        ms.each do |m|

          m.strip!

          begin
            PeMailer.step_query_definitions_accept_cc(@pe_process, @company, menu_name, @pe_member_connected.user, m).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'pe'
            lm.step = 'query_definitions:accept:cc'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

      end

      if @pe_process.step_query_definitions_accept_mail_to_boss?

        menu_name = 'Evaluación de desempeño'

        ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

          if ct_menu_cod == 'pe'
            menu_name = ct_menus_user_menu_alias[index_m]
            break
          end
        end

        pe_rel_boss = @pe_process.pe_rel_boss

        pe_member_rel_boss = @pe_member_connected.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first if pe_rel_boss

        if pe_member_rel_boss

          begin
            PeMailer.step_query_definitions_accept_to_boss(@pe_process, @company, menu_name, @pe_member_connected.user, pe_member_rel_boss.pe_member_evaluator.user).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'pe'
            lm.step = 'query_definitions:accept'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

      end

    end

    redirect_to pe_query_definitions_people_list_path(@pe_process)

  end

  private

  def get_data_1

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_member_connected = @pe_process.pe_members.where('user_id = ?', user_connected.id).first

    @pe_rel = @pe_process.pe_rel_by_id 0

    @pe_member_rel = @pe_member_connected.pe_member_rels_is_evaluated_as_pe_rel(@pe_rel).first if @pe_member_connected && @pe_rel

   #@company = session[:company]

  end

  def get_data_2
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_rel = @pe_member_rel.pe_rel
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member = @pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
      end

    end



   #@company = session[:company]

  end

end
