class BpAdminLicensesController < ApplicationController
  include BpAdminLicensesHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin, except: [:license_info_modal]
  # before_filter :verify_full_update, only: %i[full_update]

  def new
    @bp_license = BpLicense.new(bp_group_id: params[:bp_group_id])
    @bp_group = BpGroup.find(params[:bp_group_id])
  end

  def create
    licenses = []
    params[:select2_groups].each do |group_id|
      licenses << complete_create(params[:bp_license], group_id)
    end

    validation = true
    licenses.each do |license|
      next if license.valid?
      validation = false
    end

    if validation
      licenses.each { |license| license.save }
      flash[:success] = t('views.bp_licenses.flash_messages.success_created')
      redirect_to bp_admin_group_manage_path(params[:bp_group_id])
    else
      license = complete_create(params[:bp_license], params[:bp_group_id])

      licenses.each do |cond|
        cond.errors.full_messages.each { |error| license.errors[:base] << (error) }
      end

      @bp_license = license
      @bp_group = license.bp_group
      render action: 'new'
    end
  end

  def new_version
    @bp_license = BpLicense.find(params[:bp_license_id])
  end

  def create_version
    license = create_new_version(params[:bp_license], params[:bp_license_id])
    if license.save
      prev_license = BpLicense.find(params[:bp_license_id])
      prev_license.next_license_id = license.id
      prev_license.available = false
      prev_license.unavailable_by_admin_id = admin_connected.id
      prev_license.unavailable_at = lms_time
      prev_license.save

      flash[:success] = t('views.bp_licenses.flash_messages.success_changed')
      redirect_to bp_admin_group_manage_path(license.bp_group)
    else
      @bp_license = BpLicense.find(params[:bp_license_id])
      @bp_license.assign_attributes(params[:bp_license])
      @bp_license.valid?
      render action: 'new_version'
    end
  end

  def edit
    @bp_license = BpLicense.find(params[:bp_license_id])
    @bp_group = BpGroup.find(params[:bp_group_id])
  end

  def update
    if params[:bp_license][:until].blank?
      licenses = []
      params[:select2_licenses].each do |license_id|
        licenses << complete_update(params[:bp_license], license_id)
      end

      validation = true
      licenses.each do |license|
        next if license.valid?
        validation = false
      end

      if validation
        licenses.each { |license| license.save }
        flash[:success] = t('views.bp_licenses.flash_messages.success_created')
        redirect_to bp_admin_group_manage_path(params[:bp_group_id])
      else
        license = complete_update(params[:bp_license], params[:bp_license_id])

        licenses.each do |cond|
          cond.errors.full_messages.each { |error| license.errors[:base] << (error) }
        end

        @bp_license = license
        @bp_group = license.bp_group
        render action: 'edit'
      end

      else
      license = complete_update(params[:bp_license], params[:bp_license_id])
      group = BpGroup.find(params[:bp_group_id])
      if license.save
        flash[:success] = t('views.bp_licenses.flash_messages.success_changed')
        redirect_to bp_admin_group_manage_path(group)
      else
        @bp_license = license
        @bp_group = group
        render action: 'edit'
      end
    end
  end

  # def full_update
  #   license = create_new_version(params[:bp_license], params[:bp_license_id])
  #   group = BpGroup.find(params[:bp_group_id])
  #   if license.save
  #     flash[:success] = t('views.bp_licenses.flash_messages.success_changed')
  #     redirect_to bp_admin_group_manage_path(group)
  #   else
  #     @bp_license = license
  #     @bp_group = group
  #     render action: 'edit'
  #   end
  # end

  def license_info_modal
    license = BpLicense.find(params[:bp_license_id])
    render partial: 'bp_license_info_modal',
           locals: { license: license }
  end

  def deactivate
    license = BpLicense.find(params[:bp_license_id])
    license.available = false
    license.unavailable_at = lms_time
    license.unavailable_by_admin_id = admin_connected.id
    license.save
    flash[:success] = 'Se desactivo correctamente'
    redirect_to bp_admin_group_manage_path(license.bp_group)
  end

  private

  # def verify_full_update
  #   license = BpLicense.find(params[:bp_license_id])
  #   return unless license.in_process_instances.size > 0
  #   flash[:danger] = 'No puede realizar acción'
  #   redirect_to root_path
  # end

  def complete_create(params, group_id)
    license = BpLicense.new(params)
    license.bp_group_id = group_id
    license.registered_at = lms_time
    license.registered_by_admin_id = admin_connected.id
    license.deactivated_by_admin_id = admin_connected.id if license.until
    license.deactivated_at = lms_time if license.until
    return license
  end

  def complete_update(params, license_id)
    license = BpLicense.find(license_id)
    license.name = params[:name]
    license.description = params[:description]
    license.until = params[:until] if params[:until] && !params[:until].blank?
    return license if !params[:until] || (params[:until] && params[:until].blank?)
    license.deactivated_by_admin_id = admin_connected.id
    license.deactivated_at = lms_time
    license
  end

  def create_new_version(params, license_id)
    new_version = BpLicense.find(license_id).dup
    new_version.assign_attributes(params)
    new_version.prev_license_id = license_id
    new_version.registered_at = lms_time
    new_version.registered_by_admin_id = admin_connected.id
    return new_version if !params[:until] || (params[:until] && params[:until].blank?)
    new_version.deactivated_by_admin_id = admin_connected.id
    new_version.deactivated_at = lms_time
    new_version
  end



end
