class CharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @characteristics = Characteristic.not_grouped_characteristics
    @characteristic_types = CharacteristicType.all
  end

  def index2
    @characteristics = Characteristic.not_grouped_characteristics
    @characteristic_types = CharacteristicType.all
  end

  def show
    @characteristic = Characteristic.find(params[:id])
    @characteristic_value = CharacteristicValue.new
  end


  def new
    @characteristic = Characteristic.new
  end


  def edit
    @characteristic = Characteristic.find(params[:id])
  end

  def delete
    @characteristic = Characteristic.find(params[:id])
  end

  def create
    params[:characteristic][:nombre].strip!

    @characteristic = Characteristic.new(params[:characteristic])

    if @characteristic.save

      if @characteristic.data_type_id == 5

        if params[:characteristic_values]

          if File.extname(params[:characteristic_values].original_filename) == '.xlsx'

            directorio_temporal = '/tmp/'
            nombre_temporal = SecureRandom.hex(5)+'.xlsx'
            archivo_temporal = directorio_temporal+nombre_temporal

            path = File.join(directorio_temporal, nombre_temporal)
            File.open(path, 'wb') { |f| f.write(params[:characteristic_values].read) }

            update_characteristic_values_from_excel archivo_temporal

            flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

          else

            flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

          end

        elsif
          flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
        end

      end

      flash[:success] = t('activerecord.success.model.characteristic.create_ok')
      redirect_to @characteristic
    else
      render action: 'new'
    end

  end

  def update

=begin
    Characteristic.all .each do |c|

      lms_c = LmsCharacteristic.new
      lms_c.characteristic = c
      lms_c.save

    end
=end

    params[:characteristic][:nombre].strip!
    @characteristic = Characteristic.find(params[:id])

    if @characteristic.update_attributes(params[:characteristic])

      if @characteristic.data_type_id == 5

        if params[:characteristic_values]

          if File.extname(params[:characteristic_values].original_filename) == '.xlsx'

            directorio_temporal = '/tmp/'
            nombre_temporal = SecureRandom.hex(5)+'.xlsx'
            archivo_temporal = directorio_temporal+nombre_temporal

            path = File.join(directorio_temporal, nombre_temporal)
            File.open(path, 'wb') { |f| f.write(params[:characteristic_values].read) }

            update_characteristic_values_from_excel archivo_temporal

            flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

          else

            flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

          end

        elsif
          flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
        end

=begin
        if params[:characteristic_values]

          text = params[:characteristic_values].read
          text.gsub!(/\r\n?/, "\n")

          pos = 1

          text.each_line do |line|

            line.strip!

            unless line.blank?

              characteristic_value = @characteristic.characteristic_values.where('value_string = ?', line).first

              characteristic_value = @characteristic.characteristic_values.build unless characteristic_value

              characteristic_value.position = pos
              characteristic_value.value_string = line
              characteristic_value.save

              pos +=1

            end

          end

        end
=end

      elsif @characteristic.data_type_id == 11

        if @characteristic.characteristic_type_id

          elements_characteristic_to_be_used = Characteristic.where('publica = ? AND characteristic_type_id = ?', false, @characteristic.characteristic_type_id)

        else

          elements_characteristic_to_be_used = Characteristic.where('publica = ? AND characteristic_type_id IS NULL', false)

        end

        elements_characteristic_to_be_used.each do |characteristic|

          if params['children_characteristic_checked_'+characteristic.id.to_s] && params['children_characteristic_checked_'+characteristic.id.to_s] == '1'

            characteristic.register_characteristic = @characteristic
            characteristic.register_position = params['children_characteristic_position_'+characteristic.id.to_s]
            characteristic.save

          elsif characteristic.register_characteristic && characteristic.register_characteristic.id == @characteristic.id

            if characteristic.user_characteristics.size == 0

              characteristic.register_characteristic = nil
              characteristic.register_position = nil
              characteristic.save

            else

              characteristic.register_position = params['children_characteristic_position_'+characteristic.id.to_s]
              characteristic.save

            end

          end

        end

      end

      flash[:success] = t('activerecord.success.model.characteristic.update_ok')
      redirect_to characteristics_path
    else
      render action: 'edit'
    end

  end

  def edit_antiguo
    @characteristic = Characteristic.find(params[:id])
  end

  def update_antiguo
    @characteristic = Characteristic.find(params[:id])

    if @characteristic.update_attributes(params[:characteristic])
      flash[:success] = t('activerecord.success.model.characteristic.update_ok')
      redirect_to characteristics_uso_antiguo_path
    else
      render action: 'edit'
    end

  end

  def destroy

    @characteristic = Characteristic.find(params[:id])

    if verify_recaptcha

      if @characteristic.user_characteristics.count == 0 && @characteristic.pe_characteristics.size == 0 && @characteristic.pe_member_characteristics.size == 0 && @characteristic.folder_members.size == 0 && @characteristic.destroy
        flash[:success] = t('activerecord.success.model.characteristic.delete_ok')
        redirect_to characteristics_url
      else
        flash[:danger] = t('activerecord.error.model.characteristic.delete_error')
        redirect_to delete_characteristic_path(@characteristic)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.characteristic.captcha_error')
      redirect_to delete_characteristic_path(@characteristic)

    end

  end

  def create_value

    @characteristic = Characteristic.find(params[:characteristic_id])

    @characteristic_value = CharacteristicValue.new(params[:characteristic_value])
    @characteristic_value.characteristic = @characteristic

    if @characteristic_value.save
      flash[:success] = t('activerecord.success.model.characteristic_value.create_ok')
      redirect_to @characteristic
    else
      render action: 'show'
    end

  end

  def edit_value
    @characteristic_value = CharacteristicValue.find(params[:characteristic_value_id])
    @characteristic = @characteristic_value.characteristic
  end


  def update_value

    @characteristic_value = CharacteristicValue.find(params[:characteristic_value_id])
    @characteristic = @characteristic_value.characteristic

    if @characteristic_value.update_attributes(params[:characteristic_value])
      flash[:success] = t('activerecord.success.model.characteristic_value.update_ok')
      redirect_to @characteristic
    else
      render action: 'edit_value'
    end

  end

  def destroy_value
    @characteristic_value = CharacteristicValue.find(params[:characteristic_value_id])
    @characteristic = @characteristic_value.characteristic
    if UserCharacteristic.where('characteristic_value_id = ?', @characteristic_value.id).count+UserCharacteristicRecord.where('characteristic_value_id = ?', @characteristic_value.id).count == 0
      @characteristic_value.destroy
      flash[:success] = t('activerecord.success.model.characteristic_value.delete_ok')
      redirect_to @characteristic
    else
      flash.now[:danger] = t('activerecord.error.model.characteristic_value.used_error')
      render action: 'show'
    end
  end

  private

  def update_characteristic_values_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data


    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        unless fila[0].blank?

          characteristic_value = @characteristic.characteristic_values.where('value_string = ?', fila[1]).first

          characteristic_value = @characteristic.characteristic_values.build unless characteristic_value

          characteristic_value.position = fila[0]
          characteristic_value.value_string = fila[1]
          characteristic_value.value_string_2 = fila[2]
          characteristic_value.active = fila[5] == 0 ? false : true

          if characteristic_value.save

            unless fila[3].blank? || fila[4].blank?

              c = Characteristic.find_by_nombre fila[3]
              if c

                cv = CharacteristicValue.where('characteristic_id = ? AND value_string = ?', c.id, fila[4]).first

                if cv

                  cvp = characteristic_value.characteristic_value_parents.build

                  if cvp

                    cvp.characteristic_parent = c
                    cvp.characteristic_value_parent = cv
                    cvp.save

                  end

                end

              end

            end

          end



        end

      end

    end

  end

end
