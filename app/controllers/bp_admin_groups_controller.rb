class BpAdminGroupsController < ApplicationController
  include BpAdminGroupsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def bp_group_index
    @bp_groups = BpGroup.all
    @tab = params[:tab] ? params[:tab] : 0
  end

  def bp_group_new
    @bp_group = BpGroup.new
  end

  def bp_group_create
    group = group_complete_create(params[:bp_group])
    if group.save
      flash[:success] = t('views.bp_groups.flash_messages.success_created')
      redirect_to bp_admin_group_manage_path(group)
    else
      @bp_group = group
      render action: 'bp_group_new'
    end
  end

  def bp_group_edit
    @bp_group = BpGroup.find(params[:bp_group_id])
  end

  def bp_group_update
    group = group_complete_update(params[:bp_group], params[:bp_group_id])
    if group.save
      flash[:success] = t('views.bp_groups.flash_messages.success_changed')
      redirect_to bp_admin_group_manage_path(group)
    else
      @bp_group = group
      render action: 'bp_group_edit'
    end
  end

  def bp_group_manage
    @bp_group = BpGroup.find(params[:bp_group_id])
    @tab = params[:tab] ? params[:tab] : 0
    @tab_1 = params[:tab_1] ? params[:tab_1] : 1
  end







  def bp_group_user_new_search_people
    @bp_group = BpGroup.find(params[:bp_group_id])
    @user = User.new
    @users = {}
    @bp_group_user = BpGroupsUser.new()

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def bp_group_user_create
    group_user = group_user_complete_create(params[:bp_groups_user], params[:user_id], params[:bp_group_id])
    if group_user.save
      flash[:success] = t('views.bp_groups_users.flash_messages.success_assignment')
      redirect_to bp_admin_group_manage_path(group_user.bp_group)
    else
      @bp_group = group_user.bp_group
      @bp_group_user = group_user
      @user = group_user.user
      @users = []
      @users << @user
      render action: 'bp_group_user_new_search_people'
    end
  end

  def bp_group_user_edit
    @bp_group_user = BpGroupsUser.find(params[:bp_group_user_id])
  end

  def bp_group_user_update
    group_user = group_user_complete_update(params[:bp_groups_user], params[:bp_group_user_id])
    if group_user.save
      flash[:success] = t('views.bp_groups_users.flash_messages.success_messages')
      redirect_to bp_admin_group_manage_path(group_user.bp_group)
    else
      @bp_group_user = group_user
      render action: 'bp_group_user_edit'
    end
  end

  def bp_group_user_excel_create
    group = BpGroup.find(params[:bp_group_id])
    message = verify_excel(params[:upload])
    specified_close = params[:bp_license][:since] ? params[:bp_license][:since] : nil
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      close_open_assoc = params[:bp_license][:only_laborable_days] == '1' ? true : false
      close_with_excel = params[:bp_license][:whenever]  == '1' ? true : false
      specified_close = params[:bp_license][:since]
      if its_safe(file, group.id, close_open_assoc, close_with_excel, specified_close)
        @valid = true
        flash.now[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
        redirect_to bp_admin_group_manage_path(group.id)
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
        @bp_group = group
        @license = BpLicense.new(since: specified_close)
        render 'bp_group_user_excel_upload'
      end
    else
      flash.now[:danger] = message
      @bp_group = group
      @license = BpLicense.new(since: specified_close)
      render 'bp_group_user_excel_upload'
    end
  end

  def verify_excel(params)
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
    return error_message unless params
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
    return error_message unless File.extname(params['datafile'].original_filename) == '.xlsx'
    error_message = ''
    return error_message
  end

  def get_temporal_file(params)
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal
    path = File.join(directorio_temporal, nombre_temporal)
    File.open(path, 'wb') { |f| f.write(params['datafile'].read) }
    return archivo_temporal
  end

  def bp_group_user_excel_upload
    @bp_group = BpGroup.find(params[:bp_group_id])
    @license = BpLicense.new(only_laborable_days: false)
  end

  def bp_group_user_excel_delete
    @bp_group = BpGroup.find(params[:bp_group_id])
    @bp_group_user = BpGroupsUser.new
  end

  def bp_group_user_excel_destroy
    group = BpGroup.find(params[:bp_group_id])
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_to_destroy(file, group.id)
        @valid = true
        flash.now[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
        redirect_to bp_admin_group_manage_path(group.id)
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
        @bp_group = group
        @bp_group_user = BpGroupsUser.new
        render 'bp_group_user_excel_delete'
      end
    else
      flash.now[:danger] = message
      @bp_group = group
      @bp_group_user = BpGroupsUser.new
      render 'bp_group_user_excel_delete'
    end
  end

  private

  def group_complete_create(params)
    group = BpGroup.new(params)
    group.registered_by_admin_id = admin_connected.id
    group.registered_at = lms_time
    return group unless group.until
    group.deactivated_by_admin_id = admin_connected.id
    group.deactivated_at = lms_time
    return group
  end

  def group_complete_update(params, group_id)
    group = BpGroup.find(group_id)
    group.assign_attributes(params)
    return group if !params[:until] || (params[:until] && params[:until].blank?)
    group.deactivated_by_admin_id = admin_connected.id
    group.deactivated_at = lms_time
    return group
  end

  def group_user_complete_create(params, user_id, group_id)
    group_user = BpGroupsUser.new(params)
    group_user.user_id = user_id
    group_user.bp_group_id = group_id
    group_user.registered_by_admin_id = admin_connected.id
    group_user.registered_at = lms_time
    return group_user unless group_user.until
    group_user.deactivated_by_admin_id = admin_connected.id
    group_user.deactivated_at = lms_time
    return group_user
  end

  def group_user_complete_update(params, group_user_id)
    group_user = BpSeasonPeriod.find(group_user_id)
    group_user.assign_attributes(params)
    return group_user if !params[:until] || (params[:until] && params[:until].blank?)
    group_user.deactivated_by_admin_id = admin_connected.id
    group_user.deactivated_at = lms_time
    return group_user
  end

end
