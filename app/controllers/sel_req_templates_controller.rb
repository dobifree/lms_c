class SelReqTemplatesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /sel_req_templates
  # GET /sel_req_templates.json
  def index
    @sel_req_templates = SelReqTemplate.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sel_req_templates }
    end
  end

  # GET /sel_req_templates/1
  # GET /sel_req_templates/1.json
  def show
    @sel_req_template = SelReqTemplate.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sel_req_template }
    end
  end

  # GET /sel_req_templates/new
  # GET /sel_req_templates/new.json
  def new
    @sel_req_template = SelReqTemplate.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sel_req_template }
    end
  end

  # GET /sel_req_templates/1/edit
  def edit
    @sel_req_template = SelReqTemplate.find(params[:id])
  end

  # POST /sel_req_templates
  # POST /sel_req_templates.json
  def create
    @sel_req_template = SelReqTemplate.new(params[:sel_req_template])

    respond_to do |format|
      if @sel_req_template.save

        ###################################################################
        ## Solo una plantilla queda activa.
        active_massive_update(@sel_req_template.id) unless !@sel_req_template.active
        ###################################################################

        format.html {
          flash[:success]='Plantilla creada correctamente'
          redirect_to sel_req_templates_path
        }
        format.json { render json: @sel_req_template, status: :created, location: @sel_req_template }
      else
        format.html { render action: "new" }
        format.json { render json: @sel_req_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sel_req_templates/1
  # PUT /sel_req_templates/1.json
  def update
    @sel_req_template = SelReqTemplate.find(params[:id])

    if !@sel_req_template.is_in_use?

      respond_to do |format|
        if @sel_req_template.update_attributes(params[:sel_req_template])

          ###################################################################
          ## Solo una plantilla queda activa.
          active_massive_update(@sel_req_template.id) unless !@sel_req_template.active
          ###################################################################

          format.html {
            flash[:success]='Plantilla actualizada correctamente'
            redirect_to @sel_req_template
          }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @sel_req_template.errors, status: :unprocessable_entity }
        end
      end
    else
      flash[:success]='La plantilla no puede ser modificada por que ya está en uso.'
      redirect_to @sel_req_template
    end

  end

  def update_active_value
    @sel_req_template = SelReqTemplate.find(params[:id])

    @sel_req_template.active = !@sel_req_template.active

    if @sel_req_template.save

      ###################################################################
      ## Solo una plantilla queda activa.
      active_massive_update(@sel_req_template.id) unless !@sel_req_template.active
      ###################################################################

      flash[:success]='El estado de la plantilla fue actualizado correctamente.'

      if !SelReqTemplate.find_by_active(true)
        flash[:info]='No hay ninguna plantilla activa, esto implica que nadie podrá realizar solicitudes. Para dejar activa cualquiera simplemente cambie el estado a "activa" y las otras se desactivarán automáticamente'
      end



    else
      flash[:danger]='El estado de la plantilla no pudo ser actualizado.'
    end

    redirect_to sel_req_templates_path


  end

  # DELETE /sel_req_templates/1
  # DELETE /sel_req_templates/1.json
  def destroy
    @sel_req_template = SelReqTemplate.find(params[:id])

    if !@sel_req_template.is_in_use?

      @sel_req_template.destroy

      respond_to do |format|
        format.html {
          flash[:success]='Plantilla eliminada correctamente'
          redirect_to sel_req_templates_path
        }
        format.json { head :no_content }
      end
    else
      flash[:success]='La plantilla no puede ser modificada por que ya está en uso.'
      redirect_to @sel_req_template
    end
  end

  private

  def active_massive_update(sel_req_template_active_id)
    SelReqTemplate.where('id <> ?', sel_req_template_active_id).update_all(:active => false)
  end
end
