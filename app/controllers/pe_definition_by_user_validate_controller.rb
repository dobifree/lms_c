class PeDefinitionByUserValidateController < ApplicationController

  include PeDefinitionByUserValidateHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:show, :validate]
  before_filter :get_data_2, only: [:download_xls_for_validation_everybody, :download_xls_for_validation_pending, :validate_with_excel]

  before_filter :validate_open_process
  before_filter :can_validate, only: [:show, :validate]


  def show

    @pe_member_rel_shared_comment = PeMemberRelSharedComment.new
    @pe_question_comment = PeQuestionComment.new

  end

  def validate

    has_been_rejected = false

    @pe_evaluation.pe_questions_only_by_pe_member_all_levels(@pe_member_evaluated).each do |pe_question|

      pe_question.pe_question_validations.where('step_number = ? AND before_accept = ?', @step_number, @before_accept).destroy_all

      if params['validate_pe_question_'+pe_question.id.to_s]

        pqv = pe_question.pe_question_validations.build
        pqv.step_number = @step_number
        pqv.before_accept = @before_accept
        pqv.validated = params['validate_pe_question_'+pe_question.id.to_s] == '1' ? true : false
        pqv.comment = params['validate_pe_question_'+pe_question.id.to_s+'_comment'] unless pqv.validated
        pqv.pe_evaluation = @pe_evaluation
        if pqv.save

          unless pqv.validated

            has_been_rejected = true

            pe_question.confirmed = false
            pe_question.save

            pe_question.pe_question_validations.each do |pe_question_validation|

              pe_question_validation.validated = false
              pe_question_validation.attended = false
              pe_question_validation.save

            end

          end



        end

      else

        pqv = pe_question.pe_question_validations.build
        pqv.step_number = @step_number
        pqv.before_accept = @before_accept
        pqv.validated = true
        pqv.pe_evaluation = @pe_evaluation
        pqv.save

      end

    end

    @pe_evaluation.pe_questions_only_by_pe_member_all_levels(@pe_member_evaluated).each do |pe_question|
      pe_question.confirmed = false if has_been_rejected
      pe_question.save
    end

    pe_process_notification_def_val = @pe_process.pe_process_notification_def_val(@before_accept, @step_number)

    menu_name = 'Evaluación de desempeño'

    ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

      if ct_menu_cod == 'pe'
        menu_name = ct_menus_user_menu_alias[index_m]
        break
      end
    end

    if pe_process_notification_def_val

      if has_been_rejected

        if pe_process_notification_def_val.send_email_when_reject

          begin
            PeMailer.step_def_by_users_validate_definition_reject(@pe_process, @company, menu_name, @pe_evaluation, @pe_member_evaluated.user, @creators, pe_process_notification_def_val, user_connected).deliver
          rescue Exception => e

            lm = LogMailer.new
            lm.module = 'pe'
            lm.step = 'def_by_user:validate:reject'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save

          end

        end

      else

        if pe_process_notification_def_val.send_email_when_accept

          begin
            PeMailer.step_def_by_users_validate_definition_accept(@pe_process, @company, menu_name, @pe_evaluation, @pe_member_evaluated.user, @creators, pe_process_notification_def_val, user_connected).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'pe'
            lm.step = 'def_by_user:validate:accept'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

      end

    end

    if !has_been_rejected && @before_accept && @step_number == @pe_evaluation.num_steps_definition_by_users_validated_before_accepted && @pe_evaluation.send_email_to_accept_definition

      @creators.each do |creator|

        begin
          PeMailer.step_def_by_users_accept_definition(@pe_process, @company, menu_name, @pe_evaluation, @pe_member_evaluated.user, creator, user_connected).deliver
        rescue Exception => e
          lm = LogMailer.new
          lm.module = 'pe'
          lm.step = 'def_by_user:validate:accept:ask_to_confirm'
          lm.description = e.message+' '+e.backtrace.inspect
          lm.registered_at = lms_time
          lm.save
        end

      end

    end

    redirect_to pe_def_by_user_people_list_path @pe_evaluation

  end

  def download_xls_for_validation_everybody

    reporte_excel = xls_for_validation_everybody @pe_evaluation, @step_number, @before_accept

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Validación_'+@pe_evaluation.name+'_todos.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def download_xls_for_validation_pending

    reporte_excel = xls_for_validation_pending @pe_evaluation, @step_number, @before_accept

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Validación_'+@pe_evaluation.name+'_pendientes.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def validate_with_excel
    @pe_question_validation = PeQuestionValidation.new

    @pe_question_validation.before_accept = @before_accept
    @pe_question_validation.step_number = @step_number

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        save_validations archivo_temporal, @pe_evaluation, @step_number, @before_accept

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

        redirect_to pe_def_by_user_people_list_path @pe_evaluation

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

        render 'validate_with_excel'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

      render 'validate_with_excel'

    end


  end

  private

  def get_data_1

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
    @pe_member_evaluated = PeMember.find(params[:pe_member_evaluated_id])

    @creators = @pe_evaluation.pe_creators_of_pe_member_questions(@pe_member_evaluated)

    pe_member_evaluator_boss = @pe_member_evaluated.pe_member_evaluator_boss

    @creators.push(pe_member_evaluator_boss.user) if pe_member_evaluator_boss

    @creators.uniq!

    @step_number = params[:step_number].to_i
    @before_accept = params[:before_accept] == '1' ? true : false
   #@company = session[:company]

  end

  def get_data_2

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
    @step_number = params[:step_number].to_i
    @before_accept = params[:before_accept] == '1' ? true : false
   #@company = session[:company]

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def can_validate

    unless @pe_evaluation.is_pe_definition_by_user_validator?(user_connected, @step_number, @before_accept)

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_validate')
      redirect_to root_path

    end

  end

end
