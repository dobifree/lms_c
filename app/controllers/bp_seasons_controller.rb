class BpSeasonsController < ApplicationController

  def new
    @bp_season = BpSeason.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bp_season }
    end
  end

  def create
    bp_season = BpSeason.new(params[:bp_season])

    if bp_season.save
      flash[:success] = t('views.bp_seasons.flash_messages.success_created')
      redirect_to bp_groups_path
    else
      @bp_season = bp_season
      render action: "new"
    end
  end

  def edit
    @bp_season = BpSeason.find(params[:id])
  end

  def update
    bp_season = BpSeason.find(params[:id])
    if bp_season.update_attributes(params[:bp_season])
      flash[:success] = t('views.bp_seasons.flash_messages.success_changed')
      redirect_to bp_groups_path
    else
      @bp_season = bp_season
      render action: "edit"
    end
  end

  def bp_season_period_new
    @bp_season_period = BpSeasonPeriod.new
  end

  def bp_season_period_create
    bp_season_period = BpSeasonPeriod.new(params[:bp_season_period])

    if bp_season_period.save
      flash[:success] = t('views.bp_season_periods.flash_messages.success_created')
      redirect_to bp_groups_path
    else
      @bp_season_period = bp_season_period
      render action: "bp_season_period_new"
    end
  end

  def bp_season_period_deactivate
    bp_season_period = BpSeasonPeriod.find(params[:bp_season_period_id])
    bp_season_period.active = false
    bp_season_period.deactivated_by_admin_id = admin_connected.id
    bp_season_period.deactivated_at = lms_time
    if bp_season_period.save
      flash[:success] = t('views.bp_season_periods.flash_messages.success_deactivated')
    else
      flash[:danger] = t('views.bp_season_periods.flash_messages.danger_deactivated')
    end
    redirect_to bp_groups_path
  end

end
