class HolidayUsersController < ApplicationController
  include HolidayModule

  before_filter :authenticate_user
  before_filter :holiday_manager
  before_filter :active_module

  before_filter :user_by_id, only: %i[ assoc_new_user_step2 new_users_step2 create_users_type assoc_new_user_step2]
  before_filter :get_holiday_by_id, only: %i[ assoc_new_user assoc_new_user_step2 assoc_new_user_step2 ]
  before_filter :verify_active_holiday, only: %i[ assoc_new_user assoc_new_user_step2 ]
  before_filter :verify_active_assoc_chain, only: %i[ delete_assoc ]
  before_filter :get_assoc, only: %i[ delete_assoc ]


  def delete_assoc
    @assoc = deactivate @assoc
    @assoc.save ?
        flash[:success] = t('views.holiday_module.flash_messages.delete_success') :
        flash[:danger] = t('views.holiday_module.flash_messages.delete_danger')
    redirect_to holiday_manage_path(@assoc.holiday)
  end

  def assoc_new_user
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def assoc_new_user_step2
    assign_holiday(@holiday, @user, true).save
    flash[:success] = t('views.holiday_module.flash_messages.assigned_success')
    redirect_to holiday_manage_path(@holiday)
  end

end
# def new_users
#   @user = User.new
#   @users = {}
#
#   if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
#     @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
#     @user = User.new(params[:user])
#   end
# end
#
# def new_users_step2
#   @holiday = Holiday.new(hol_type: Holiday.user_type)
# end
#
# def create_users_type
#   holiday = Holiday.new(params[:holiday])
#   holiday.hol_type = Holiday.user_type
#   holiday = set_registered holiday
#   if holiday.save
#     assign_holiday(holiday, @user, true).save
#     flash[:success] = t('views.holiday_module.flash_messages.create_success')
#     redirect_to holiday_index_path(t('views.holiday_module.tabs.manager_index.user'))
#   else
#     @holiday = holiday
#     render action: "new_users_step2"
#   end
# end