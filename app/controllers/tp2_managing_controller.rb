class Tp2ManagingController < ApplicationController

  include Tp2ManagingHelper

  before_filter :authenticate_user

  def list_program_instances
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def new_program_instance

    @program_instance = ProgramInstance.new
    @programs = Program.where('activo = ?', true)

  end

  def create_program_instance

    @program_instance = ProgramInstance.new(params[:program_instance])

    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    @program_instance.from_date = desde
    @program_instance.to_date = hasta

    @program_instance.percentage_minimum_grade = true

    if @program_instance.save

      level = @program_instance.program.levels.first

      level.courses_actives_ordered_by_name.each do |course|

        pc = course.program_courses.where('level_id = ? AND program_id = ?', level.id, @program_instance.program.id).first

        pci = @program_instance.program_course_instances.where('program_course_id = ?', pc.id).first

        unless pci

          pci = @program_instance.program_course_instances.build
          pci.program_course_id = pc.id

        end

        pci.from_date = @program_instance.from_date
        pci.to_date = @program_instance.to_date
        pci.sence_hour_value = course.sence_hour_value
        pci.sence_student_value = course.sence_student_value
        pci.sence_hours = course.sence_hours
        pci.sence_training_type = course.sence_training_type
        pci.sence_region = course.sence_region
        pci.sence_otec_name = course.sence_otec_name
        pci.sence_otec_address = course.sence_otec_address
        pci.sence_otec_telephone = course.sence_otec_telephone
        pci.minimum_grade = course.nota_minima
        pci.minimum_assistance = course.minimum_assistance
        pci.sence_maximum_students_number = course.sence_maximum_students_number
        #pci.sence_minimum_students_number = course.sence_minimum_students_number

        if pci.save

          if @program_instance.master_poll_id

            poll = pci.polls.build
            poll.numero = 1
            poll.orden = 1
            poll.nombre = 'Encuesta de Satisfacción'
            poll.tipo_satisfaccion = true
            poll.master_poll_id = @program_instance.master_poll_id

            poll.save

          end


        end

      end


      flash[:success] = t('activerecord.success.model.training_configuration.create_program_ok')

      redirect_to tp2_list_program_instances_path
    else
      @programs = Program.where('activo = ?', true)
      render action: 'new_program_instance'
    end

  end

  def config_program_instance

    @program_instance = ProgramInstance.find(params[:program_instance_id])
   #@company = session[:company]

  end

  def edit_program_instance

    @program_instance = ProgramInstance.find(params[:program_instance_id])
   #@company = session[:company]

  end

  def update_program_instance

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    @program_instance.from_date = desde
    @program_instance.to_date = hasta

    master_poll_id_previous = @program_instance.master_poll_id

    if @program_instance.update_attributes(params[:program_instance])

      if master_poll_id_previous != @program_instance.master_poll_id

        level = @program_instance.program.levels.first

        level.courses_actives_ordered_by_name.each do |course|

          pc = course.program_courses.where('level_id = ? AND program_id = ?', level.id, @program_instance.program.id).first

          pci = @program_instance.program_course_instances.where('program_course_id = ?', pc.id).first

          if pci

            pci.polls.destroy_all
            pci.program_course_poll_summaries.destroy_all

            if @program_instance.master_poll_id

              poll = pci.polls.build
              poll.numero = 1
              poll.orden = 1
              poll.nombre = 'Encuesta de Satisfacción'
              poll.tipo_satisfaccion = true
              poll.master_poll_id = @program_instance.master_poll_id

              poll.save

            end

          end

        end

      end

      flash[:success] = t('activerecord.success.model.training_planification.update_program_instance_ok')
      redirect_to tp2_config_program_instance_path(@program_instance)
    else
     #@company = session[:company]
      render action: 'edit_program_instance'
    end

  end

  def delete_program_instance

    @program_instance = ProgramInstance.find(params[:program_instance_id])

  end

  def destroy_program_instance

    #verificar enrollments y user_courses

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    if verify_recaptcha

      if @program_instance.enrollments.count > 0

        flash[:danger] = t('activerecord.error.model.training_planification.program_existing_errollments')
        redirect_to tp2_delete_program_instance_path(@program_instance)

      else

        @program_instance.program_course_instances.destroy_all

        if @program_instance.destroy
          flash[:success] = t('activerecord.success.model.training_planification.destroy_program_instance_ok')
          redirect_to tp2_list_program_instances_path
        else
          flash[:danger] = t('activerecord.error.model.training_planification.destroy_program_instance_error')
          redirect_to tp2_delete_program_instance_path(@program_instance)
        end

      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.training_planification.program_instance_captcha_error')
      redirect_to tp2_delete_program_instance_path(@program_instance)

    end

  end

  def edit_program_course_instance

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
    @program_instance = @program_course_instance.program_instance
   #@company = session[:company]

  end

  def update_program_course_instance

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
    @program_instance = @program_course_instance.program_instance

    if @program_course_instance.update_attributes(params[:program_course_instance])
      flash[:success] = t('activerecord.success.model.training_planification.update_program_instance_ok')
      redirect_to tp2_config_program_instance_path(@program_instance)
    else
     #@company = session[:company]
      render action: 'edit_program_course_instance'
    end

  end

  def list_program_instances_to_enroll
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def form_enroll_from_xlsx
    @program_instance = ProgramInstance.find(params[:program_instance_id])
    @uc_charactetistics = UcCharacteristic.all
  end

  def enroll_from_xlsx

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_students_data_for_enroll archivo_temporal

        if @lineas_error.length > 0

          flash.now[:danger] = t('activerecord.error.model.training_planification.enroll_from_excel_error')
          @uc_charactetistics = UcCharacteristic.all
          render 'form_enroll_from_xlsx'

        else

          enroll_students_from_excel archivo_temporal, @program_instance, params

          flash[:success] = t('activerecord.success.model.training_planification.enroll_from_excel_ok')

          redirect_to tp2_list_program_instances_to_enroll_path
        end

      else

        flash.now[:danger] = t('activerecord.error.model.training_planification.enroll_from_excel_no_xlsx')
        @uc_charactetistics = UcCharacteristic.all
        render 'form_enroll_from_xlsx'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.training_planification.enroll_from_excel_no_file')
      @uc_charactetistics = UcCharacteristic.all
      render 'form_enroll_from_xlsx'

    end

  end

  def download_xls_for_enroll

    reporte_excel = xls_for_enroll

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_Inscripción.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def enroll_from_search_1

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    @characteristics = Characteristic.where('publica = ?', true)
    @characteristics_prev = {}

    @characteristics.each do |characteristic|

      @characteristics_prev[characteristic.id] = ''

    end

    @user = User.new
    @users = {}

  end

  def enroll_from_search_2

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    @user = User.new
    @users = {}

    @characteristics = Characteristic.where('publica = ?', true)
    @characteristics_prev = {}
    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|


        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND valor = "'+@characteristics_prev[characteristic.id]+'" '

          n_q += 1

        end

      end

      params[:user][:apellidos] ='%'  if params[:user] && params[:user][:apellidos].blank?
      params[:user][:nombre] ='%'  if params[:user] && params[:user][:nombre].blank?
      params[:user][:codigo] ='%'  if params[:user] && params[:user][:codigo].blank?

      if !params[:user][:codigo].blank? || !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        if queries.length > 0

          queries.each_with_index do |query,index|

            if index == 0

              @users = User.joins(:user_characteristics).where(
                'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query,
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

            else

              lista_users_ids = @users.map {|u| u.id }

              @users = User.joins(:user_characteristics).where(
                'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query+' AND users.id IN (?) ',
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                lista_users_ids).order('apellidos, nombre')

            end

          end

        else

          @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        end

      end

    end

    params[:user][:codigo] ='' if params[:user] && params[:user][:codigo] == '%'
    params[:user][:apellidos] ='' if params[:user] && params[:user][:apellidos] == '%'
    params[:user][:nombre] ='' if params[:user] && params[:user][:nombre] == '%'

    @user = User.new(params[:user])

    render 'enroll_from_search_1'

  end

  def enroll_from_search_3

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    level = @program_instance.program.levels.first

    params[:user_id].each do |user_id,value|

      if value != '0'

        user = User.find(user_id)

        if user

          enrollment = Enrollment.where('program_id = ? AND user_id = ? AND level_id = ? AND program_instance_id = ?', @program_instance.program.id, user.id, level.id, @program_instance.id).first

          unless enrollment

            enrollment = Enrollment.new
            enrollment.program = @program_instance.program
            enrollment.user = user
            enrollment.level = level
            enrollment.program_instance = @program_instance

          end

          if enrollment.save

           company = @company

            level.courses_ordered_by_estado.each do |course|

              pc = course.program_courses.where('level_id = ? AND program_id = ?', level.id, @program_instance.program.id).first

              pci = @program_instance.program_course_instances.where('program_course_id = ?', pc.id).first

              if pci

                if params[('pci_'+pci.id.to_s).to_sym] == '1'

                  user_course = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND course_id = ? AND program_course_instance_id = ?', enrollment.id, pc.id, course.id, pci.id).first

                  unless user_course

                    user_course = UserCourse.new

                    user_course.enrollment = enrollment
                    user_course.program_course = pc
                    user_course.course = course
                    user_course.program_course_instance = pci

                    user_course.desde = pci.from_date
                    user_course.hasta = pci.to_date

                    user_course.numero_oportunidad = 1

                    user_course.aplica_sence = true if company.modulo_sence

                  end

                  if user_course.save

                    UcCharacteristic.all.each do |characteristic|

                      unless params[('uc_char_'+characteristic.id.to_s).to_sym].blank?
                        uc_ucc = user_course.user_course_uc_characteristics.build
                        uc_ucc.uc_characteristic = characteristic
                        uc_ucc.value = params[('uc_char_'+characteristic.id.to_s).to_sym]
                        uc_ucc.save
                      end

                    end

                  end

                end

              end

            end

          end

        end

      end

    end

    flash[:success] = t('activerecord.success.model.training_planification.enroll_from_search_ok')

    redirect_to tp2_list_program_instances_to_enroll_path

  end

  def list_program_instances_to_modify
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def form_unenroll
    @program_instance = ProgramInstance.find(params[:program_instance_id])
  end

  def unenroll

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    level = @program_instance.program.levels.first

    @program_instance.enrollments.each do |enrollment|

      if params[('enrollment_'+enrollment.id.to_s).to_sym] == '1'

        if params['unroll_p_c'.to_sym] == 'p'

          level.courses_actives_ordered_by_name.each do |course|

            pc = course.program_courses.where('level_id = ? AND program_id = ?', level.id, @program_instance.program.id).first

            pci = @program_instance.program_course_instances.where('program_course_id = ?', pc.id).first

            if pci

              uc = pci.user_courses.where('enrollment_id = ?', enrollment.id).first

              uc.destroy if uc

            end

          end

          enrollment.destroy

        elsif params['unroll_p_c'.to_sym] == 'c'

          level.courses_actives_ordered_by_name.each do |course|

            pc = course.program_courses.where('level_id = ? AND program_id = ?', level.id, @program_instance.program.id).first

            pci = @program_instance.program_course_instances.where('program_course_id = ?', pc.id).first

            if pci && params[('pci_'+pci.id.to_s).to_sym] == '1'

              uc = pci.user_courses.where('enrollment_id = ?', enrollment.id).first

              uc.destroy if uc

            end

          end

        end


      end

    end

    flash[:success] = t('activerecord.success.model.training_planification.unenroll_ok')
    redirect_to tp2_unenroll_path(@program_instance)

  end

  def list_enrollments_to_modify
    @program_instance = ProgramInstance.find(params[:program_instance_id])
  end

  def edit_user_course
    @user_course = UserCourse.find(params[:user_course_id])
    @program_course_instance = @user_course.program_course_instance
    @program_instance = @program_course_instance.program_instance
  end

  def update_user_course
    @user_course = UserCourse.find(params[:user_course_id])
    @program_course_instance = @user_course.program_course_instance
    @program_instance = @program_course_instance.program_instance

    @user_course.user_course_uc_characteristics.destroy_all

    UcCharacteristic.all.each do |characteristic|

      unless params[('uc_char_'+characteristic.id.to_s).to_sym].blank?

        uc_uc_char = @user_course.user_course_uc_characteristics.build
        uc_uc_char.uc_characteristic = characteristic
        uc_uc_char.value = params[('uc_char_'+characteristic.id.to_s).to_sym]
        uc_uc_char.save

      end

    end

    flash[:success] = t('activerecord.success.model.training_planification.uc_modify_ok')
    redirect_to tp2_list_enrollments_to_modify_path(@program_instance)


  end

  def list_enrollments
    @program_instance = ProgramInstance.find(params[:program_instance_id])
  end

  def list_enrollments_full
    @program_instance = ProgramInstance.find(params[:program_instance_id])
  end

  def list_instances_to_reopen
    @program_instances = ProgramInstance.where('active = ?', false)
  end

  def reopen_instance

    @program_instance = ProgramInstance.find(params[:program_instance_id])
   #@company = session[:company]

  end

  def make_reopen_instance

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    @program_instance.active = true

    @program_instance.save

    flash[:success] = t('activerecord.success.model.training_planification.reopen_ok')

    redirect_to tp2_list_program_instances_path

  end

  private

  def check_students_data_for_enroll(archivo_temporal)

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 código

      if index > 0

        user = User.where('codigo = ?',fila[0]).first

        unless user

          @lineas_error.push index+1

          begin
            @lineas_error_detalle.push (alias_username+': '+fila[0].to_s).force_encoding('UTF-8')
          rescue Exception => e
            @lineas_error_detalle.push ('exception: '+e.message)
          end

          @lineas_error_messages.push ['El alumno no existe']

        end

      end


    end

  end

  def enroll_students_from_excel(archivo_temporal, program_instance, params)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 código
      #1 uccharacteristic 1
      #1 uccharacteristic 2
      #...

      if index > 0

        user = User.where('codigo = ?',fila[0]).first

        if user

          level = program_instance.program.levels.first

          enrollment = Enrollment.where('program_id = ? AND user_id = ? AND level_id = ? AND program_instance_id = ?', program_instance.program.id, user.id, level.id, program_instance.id).first

          unless enrollment

            enrollment = Enrollment.new
            enrollment.program = program_instance.program
            enrollment.user = user
            enrollment.level = level
            enrollment.program_instance = program_instance

          end

          if enrollment.save

           company = @company

            level.courses_ordered_by_estado.each do |course|

              pc = course.program_courses.where('level_id = ? AND program_id = ?', level.id, program_instance.program.id).first

              pci = program_instance.program_course_instances.where('program_course_id = ?', pc.id).first

              if pci

                if params[('pci_'+pci.id.to_s).to_sym] == '1'

                  user_course = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND course_id = ? AND program_course_instance_id = ?', enrollment.id, pc.id, course.id, pci.id).first

                  unless user_course

                    user_course = UserCourse.new

                    user_course.enrollment = enrollment
                    user_course.program_course = pc
                    user_course.course = course
                    user_course.program_course_instance = pci

                    user_course.desde = pci.from_date
                    user_course.hasta = pci.to_date

                    user_course.numero_oportunidad = 1

                    user_course.aplica_sence = true if company.modulo_sence

                  end

                  if user_course.save

                    UcCharacteristic.all.each_with_index do |characteristic, index_ucc|

                      if fila[index_ucc+1] && fila[index_ucc+1] != ''
                        uc_ucc = user_course.user_course_uc_characteristics.build
                        uc_ucc.uc_characteristic = characteristic
                        uc_ucc.value = fila[index_ucc+1]
                        uc_ucc.save
                      end

                    end

                  end


                end

              end

            end

          end

        end

      end


    end

  end

end
