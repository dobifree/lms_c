class JjCharacteristicsController < ApplicationController

  def index
    # @characteristics = JjCharacteristic.not_grouped_characteristics
    @characteristic_types = JCharacteristicType.all
  end

  def new
    @characteristic = JjCharacteristic.new
  end

  def create
    params[:jj_characteristic][:name].strip!

    @characteristic = JjCharacteristic.new(params[:jj_characteristic])

    if @characteristic.save

      if @characteristic.data_type_id == 5

        if params[:jj_characteristic_values]

          if File.extname(params[:jj_characteristic_values].original_filename) == '.xlsx'

            directorio_temporal = '/tmp/'
            nombre_temporal = SecureRandom.hex(5)+'.xlsx'
            archivo_temporal = directorio_temporal+nombre_temporal

            path = File.join(directorio_temporal, nombre_temporal)
            File.open(path, 'wb') { |f| f.write(params[:jj_characteristic_values].read) }

            update_characteristic_values_from_excel archivo_temporal

            flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

          else

            flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

          end

        elsif
        flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
        end

      end

      flash[:success] = t('activerecord.success.model.characteristic.create_ok')
      redirect_to jj_characteristics_index_path
    else
      render action: 'new'
    end

  end

  def update_characteristic_values_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data


    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        unless fila[0].blank?

          characteristic_value = @characteristic.jj_characteristic_values.where('value_string = ?', fila[1]).first

          characteristic_value = @characteristic.jj_characteristic_values.build unless characteristic_value

          characteristic_value.position = fila[0]
          characteristic_value.value_string = fila[1]
          characteristic_value.active = fila[2] == 0 ? false : true
          characteristic_value.save
        end

      end

    end

  end

  def edit
    @characteristic = JjCharacteristic.find(params[:jj_characteristic_id])
  end

  def update
    params[:jj_characteristic][:name].strip!
    @characteristic = JjCharacteristic.find(params[:jj_characteristic_id])

    if @characteristic.update_attributes(params[:jj_characteristic])

      if @characteristic.data_type_id == 5

        if params[:characteristic_values]

          if File.extname(params[:characteristic_values].original_filename) == '.xlsx'

            directorio_temporal = '/tmp/'
            nombre_temporal = SecureRandom.hex(5)+'.xlsx'
            archivo_temporal = directorio_temporal+nombre_temporal

            path = File.join(directorio_temporal, nombre_temporal)
            File.open(path, 'wb') { |f| f.write(params[:characteristic_values].read) }

            update_characteristic_values_from_excel archivo_temporal

            flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

          else

            flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

          end

        else
        flash[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
        end


      elsif @characteristic.data_type_id == 11

        if @characteristic.j_characteristic_type_id
          elements_characteristic_to_be_used = JjCharacteristic.where('public = ? AND j_characteristic_type_id = ?', false, @characteristic.j_characteristic_type_id)
        else
          elements_characteristic_to_be_used = JjCharacteristic.where('public = ? AND j_characteristic_type_id IS NULL', false)
        end

        elements_characteristic_to_be_used.each do |characteristic|

          if params['children_characteristic_checked_'+characteristic.id.to_s] && params['children_characteristic_checked_'+characteristic.id.to_s] == '1'

            characteristic.register_characteristic = @characteristic
            characteristic.register_position = params['children_characteristic_position_'+characteristic.id.to_s]
            characteristic.save

          elsif characteristic.register_characteristic && characteristic.register_characteristic.id == @characteristic.id

            if characteristic.j_job_jj_characteristics.size == 0

              characteristic.register_characteristic = nil
              characteristic.register_position = nil
              characteristic.save

            else

              characteristic.register_position = params['children_characteristic_position_'+characteristic.id.to_s]
              characteristic.save

            end

          end

        end

      end

      flash[:success] = t('activerecord.success.model.characteristic.update_ok')
      redirect_to jj_characteristic_config_path(@characteristic)
    else
      render action: 'edit'
    end
  end

  def show
    @characteristic = JjCharacteristic.find(params[:jj_characteristic_id])
    @characteristic_value = JjCharacteristicValue.new
  end

  def delete
    @characteristic = JjCharacteristic.find(params[:jj_characteristic_id])
  end

  def destroy

    @characteristic = JjCharacteristic.find(params[:jj_characteristic_id])

    if verify_recaptcha

      if  @characteristic.destroy # @characteristic.user_characteristics.count == 0 && @characteristic.pe_characteristics.size == 0 && @characteristic.pe_member_characteristics.size == 0 && @characteristic.folder_members.size == 0
        flash[:success] = t('activerecord.success.model.characteristic.delete_ok')
        redirect_to jj_characteristics_index_path
      else
        flash[:danger] = t('activerecord.error.model.characteristic.delete_error')
        redirect_to jj_characteristic_delete_path(@characteristic)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.characteristic.captcha_error')
      redirect_to jj_characteristic_delete_path(@characteristic)

    end

  end

  def create_value
    @characteristic = JjCharacteristic.find(params[:jj_characteristic_id])

    @characteristic_value = JjCharacteristicValue.new(params[:jj_characteristic_value])
    @characteristic_value.jj_characteristic_id = @characteristic.id

    if @characteristic_value.save
      flash[:success] = t('activerecord.success.model.characteristic_value.create_ok')
      redirect_to jj_characteristic_config_path(@characteristic)
    else
      render action: 'show'
    end
  end

  def edit_value
    @characteristic_value = JjCharacteristicValue.find(params[:jj_characteristic_value_id])
    @characteristic = @characteristic_value.jj_characteristic
  end

  def update_value
    @characteristic_value = JjCharacteristicValue.find(params[:jj_characteristic_value_id])
    @characteristic = @characteristic_value.jj_characteristic

    if @characteristic_value.update_attributes(params[:jj_characteristic_value])
      flash[:success] = t('activerecord.success.model.characteristic_value.update_ok')
      redirect_to jj_characteristic_config_path(@characteristic)
    else
      render action: 'edit_value'
    end
  end

  def destroy_value
    @characteristic_value = JjCharacteristicValue.find(params[:jj_characteristic_value_id])
    @characteristic = @characteristic_value.jj_characteristic
    # if UserCharacteristic.where('characteristic_value_id = ?', @characteristic_value.id).count+UserCharacteristicRecord.where('characteristic_value_id = ?', @characteristic_value.id).count == 0
    #   @characteristic_value.destroy
    #   flash[:success] = t('activerecord.success.model.characteristic_value.delete_ok')
    #   redirect_to jj_characteristic_config(@characteristic)
    # else
    #   flash.now[:danger] = t('activerecord.error.model.characteristic_value.used_error')
    #   render action: 'show'
    # end
  end



end
