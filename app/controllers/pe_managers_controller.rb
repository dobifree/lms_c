class PeManagersController < ApplicationController

  include UsersHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def search
    @pe_process = PeProcess.find(params[:pe_process_id])

    @user = User.new
    @users = Array.new

    if params[:user]

      @user = User.new(params[:user])

      @users = search_normal_users(@user.apellidos, @user.nombre)

    end

  end

  def add_manager
    @pe_process = PeProcess.find(params[:pe_process_id])
    @user = User.find(params[:user_id])

    pe_manager = @pe_process.pe_managers.build
    pe_manager.user = @user

    pe_manager.save

    redirect_to pe_managers_search_path @pe_process

  end

  def destroy

    @pe_manager = PeManager.find(params[:id])

    pe_process = @pe_manager.pe_process

    @pe_manager.destroy

    redirect_to pe_process_tab_url(pe_process, 'managers')


  end
end
