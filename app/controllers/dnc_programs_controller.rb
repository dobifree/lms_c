class DncProgramsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /dnc_programs
  # GET /dnc_programs.json
  def index
    @dnc_programs = DncProgram.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dnc_programs }
    end
  end

  # GET /dnc_programs/1
  # GET /dnc_programs/1.json
  def show
    @dnc_program = DncProgram.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dnc_program }
    end
  end

  # GET /dnc_programs/new
  # GET /dnc_programs/new.json
  def new
    @dnc_program = DncProgram.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dnc_program }
    end
  end

  # GET /dnc_programs/1/edit
  def edit
    @dnc_program = DncProgram.find(params[:id])
  end

  # POST /dnc_programs
  # POST /dnc_programs.json
  def create
    @dnc_program = DncProgram.new(params[:dnc_program])

    respond_to do |format|
      if @dnc_program.save
        format.html { redirect_to @dnc_program, notice: 'Dnc program was successfully created.' }
        format.json { render json: @dnc_program, status: :created, location: @dnc_program }
      else
        format.html { render action: "new" }
        format.json { render json: @dnc_program.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dnc_programs/1
  # PUT /dnc_programs/1.json
  def update
    @dnc_program = DncProgram.find(params[:id])

    respond_to do |format|
      if @dnc_program.update_attributes(params[:dnc_program])
        format.html { redirect_to @dnc_program, notice: 'Dnc program was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dnc_program.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dnc_programs/1
  # DELETE /dnc_programs/1.json
  def destroy
    @dnc_program = DncProgram.find(params[:id])
    @dnc_program.destroy

    respond_to do |format|
      format.html { redirect_to dnc_programs_url }
      format.json { head :no_content }
    end
  end
end
