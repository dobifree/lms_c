class BpAdminConditionVacationsController < ApplicationController
  include BpAdminConditionVacationsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new
    @bp_condition_vacation = BpConditionVacation.new(bp_rule_vacation_id: params[:bp_rule_vacation_id])
    @bp_rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
  end

  def deactivate_condition
    rv = BpConditionVacation.find(params[:bp_condition_vacation_id])
    rv.available = false
    rv.unavailable_at = lms_time
    rv.unavailable_by_admin_id = admin_connected.id
    rv.save
    flash[:success] = 'Se desactivo correctamente'
    redirect_to bp_admin_rule_vacation_manage_path(rv.bp_rule_vacation.bp_group, rv.bp_rule_vacation)
  end

  def create
    rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])

    condition_vacations = []
    params[:select2_groups].each do |group_id|
      condition_vacations << complete_create(params[:bp_condition_vacation], group_id)
    end

    validation = true
    condition_vacations.each do |condition_vacation|
      next if condition_vacation.valid?
      validation = false
    end

    if validation
      condition_vacations.each { |condition_vacation| condition_vacation.save }
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_created')
      redirect_to bp_admin_rule_vacation_manage_path(rule_vacation.bp_group, rule_vacation)
    else
      condition_vacation = complete_create(params[:bp_condition_vacation], rule_vacation.id)
      condition_vacation.valid?
      @bp_condition_vacation = condition_vacation
      @bp_rule_vacation = rule_vacation
      render action: 'new'
    end
  end

  def edit
    @bp_condition_vacation = BpConditionVacation.find(params[:bp_condition_vacation_id])
    @bp_rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
  end

  def update
    conditions = []
    rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
    condition_vacation = BpConditionVacation.find(params[:bp_condition_vacation_id])

    params[:select2_condition_vacations].each { |condition_vac_id| conditions << BpConditionVacation.find(condition_vac_id) }
    conditions.each { |cond| cond.assign_attributes(params[:bp_condition_vacation]) }
    all_valid = true
    conditions.each { |cond| all_valid = false unless cond.valid? }

    if all_valid
      conditions.each { |cond| cond.save }
      flash[:success] = t('views.bp_condition_vacations.flash_messages.success_changed')
      redirect_to bp_admin_rule_vacation_manage_path(rule_vacation.bp_group, rule_vacation)
    else
      condition_vacation.assign_attributes(params[:bp_condition_vacation])
      conditions.each { |cond| condition_vacation.errors += cond.errors }
      @bp_condition_vacation = condition_vacation
      @bp_rule_vacation = rule_vacation
      render action: 'edit'
    end
  end

  def new_version
    @bp_condition_vacation = BpConditionVacation.find(params[:bp_condition_vacation_id])
    @bp_rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
  end

  def create_version
    conditions = []
    rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
    condition_vacation = BpConditionVacation.find(params[:bp_condition_vacation_id])
    condition_ids = params[:select2_condition_vacations]

    condition_ids.each do |condition_vac_id|
      conditions << create_new_version(params[:bp_condition_vacation], condition_vac_id)
    end

    all_valid = true
    conditions.each { |cond| all_valid = false unless cond.valid? }

    if all_valid
      conditions.each do |cond|
        cond.save
        prev_cond = BpConditionVacation.find(cond.prev_cond_vac_id)
        prev_cond.next_cond_vac_id = cond.id
        prev_cond.unavailable_by_admin_id = admin_connected.id
        prev_cond.unavailable_at = lms_time
        prev_cond.available = false
        prev_cond.save
      end

      flash[:success] = t('views.bp_condition_vacations.flash_messages.success_changed')
      redirect_to bp_admin_rule_vacation_manage_path(rule_vacation.bp_group, rule_vacation)
    else
      condition_vacation.assign_attributes(params[:bp_condition_vacation])
      conditions.each do |cond|
        cond.errors.full_messages.each { |error| condition_vacation.errors[:base] << (error) }
      end
      @bp_condition_vacation = condition_vacation
      @bp_rule_vacation = rule_vacation
      render action: 'new_version'
    end
  end

  def delete
    condition_vacation = BpConditionVacation.find(params[:bp_condition_vacation_id])
    bp_rule_vacation_id = condition_vacation.bp_rule_vacation_id
    group_id = condition_vacation.bp_rule_vacation.bp_group_id
    answer = verify_to_delete_condition_vacation(condition_vacation.id)
    if answer && condition_vacation.destroy
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_deleted')
      redirect_to bp_admin_rule_vacation_manage_path(group_id, bp_rule_vacation_id)
    else
      flash[:danger] = t('views.bp_general_rules_vacations.flash_messages.danger_deleted')
      redirect_to bp_admin_condition_vacation_edit_path(bp_rule_vacation_id, condition_vacation.id)
    end
  end

  private

  def complete_create(params, rule_vacation_id)
    condition_vacation = BpConditionVacation.new(params)
    condition_vacation.bp_rule_vacation_id = rule_vacation_id

    generals_rules = condition_vacation.bp_rule_vacation.bp_group.bp_general_rules_vacations
    generals_rules.each { |gen| general_rules = gen if gen.active?(lms_time) }

    condition_vacation.bonus_only_laborable_day = defined?(general_rules) ? general_rules.only_laborable_day : true
    condition_vacation.registered_by_admin_id = admin_connected.id
    condition_vacation.registered_at = lms_time
    return condition_vacation unless condition_vacation.until
    condition_vacation.deactivated_by_admin_id = admin_connected.id
    condition_vacation.deactivated_at = lms_time
    return condition_vacation
  end

  def complete_update(params, condition_vacation_id)
    condition_vacation = BpConditionVacation.find(condition_vacation_id)
    condition_vacation.assign_attributes(params)
    return condition_vacation if !params[:until] || (params[:until] && params[:until].blank?)
    condition_vacation.deactivated_by_admin_id = admin_connected.id
    condition_vacation.deactivated_at = lms_time
    return condition_vacation
  end

  def create_new_version(params, cond_vac_id)
    new_version = BpConditionVacation.find(cond_vac_id).dup
    new_version.assign_attributes(params)
    new_version.prev_cond_vac_id = cond_vac_id
    new_version.registered_at = lms_time
    new_version.registered_by_admin_id = admin_connected.id

    # puts new_version.valid?
    # puts new_version.errors.inspect.to_yaml
    # puts '00000000'

    return new_version if !params[:until] || (params[:until] && params[:until].blank?)
    new_version.deactivated_by_admin_id = admin_connected.id
    new_version.deactivated_at = lms_time
    new_version
  end

end
