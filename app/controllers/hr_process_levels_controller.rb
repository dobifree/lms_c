class HrProcessLevelsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def show
    @hr_process_level = HrProcessLevel.find(params[:id])
  end


  def new

    @hr_process = HrProcess.find(params[:hr_process_id])

    @hr_process_level = @hr_process.hr_process_levels.new


  end


  def edit

    @hr_process_level = HrProcessLevel.find(params[:id])

    @hr_process = @hr_process_level.hr_process

  end

  def create

    @hr_process_level = HrProcessLevel.new(params[:hr_process_level])
    @hr_process = @hr_process_level.hr_process

    if @hr_process_level.save
      flash[:success] = t('activerecord.success.model.hr_process_level.create_ok')
      redirect_to @hr_process
    else

      render action: 'new'
    end

  end

  def new_from_j

    @hr_process = HrProcess.find(params[:hr_process_id])

    JJob.where('current = ?', true).each do |j_job|

      hr_process_level = @hr_process.hr_process_levels.build

      hr_process_level.nombre = j_job.id
      hr_process_level.save

    end

    redirect_to @hr_process

  end

  def update
    @hr_process_level = HrProcessLevel.find(params[:id])
    @hr_process = @hr_process_level.hr_process

    if @hr_process_level.update_attributes(params[:hr_process_level])
      flash[:success] = t('activerecord.success.model.hr_process_level.update_ok')
      redirect_to @hr_process
    else
      render action: 'edit'
    end

  end

  def delete
    @hr_process_level = HrProcessLevel.find(params[:id])

    @hr_process = @hr_process_level.hr_process

  end


  def destroy

    @hr_process_level = HrProcessLevel.find(params[:id])
    @hr_process = @hr_process_level.hr_process

    if verify_recaptcha

      if @hr_process_level.destroy
        flash[:success] = t('activerecord.success.model.hr_process_level.delete_ok')
        redirect_to @hr_process
      else
        flash[:danger] = t('activerecord.success.model.hr_process_level.delete_error')
        redirect_to delete_hr_process_level_path(@hr_process_level)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.hr_process_level.captcha_error')
      redirect_to delete_hr_process_level_path(@hr_process_level)

    end

  end

end
