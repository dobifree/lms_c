class PeQuestionActivityStatusesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show
    @pe_question_activity_status = PeQuestionActivityStatus.find(params[:id])
    @pe_evaluation = @pe_question_activity_status.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def new

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    @pe_process = @pe_evaluation.pe_process

    @pe_question_activity_status = @pe_evaluation.pe_question_activity_statuses.build

  end

  def create

    @pe_question_activity_status = PeQuestionActivityStatus.new(params[:pe_question_activity_status])

    if @pe_question_activity_status.save
      redirect_to @pe_question_activity_status
    else
      @pe_evaluation = @pe_question_activity_status.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'new'
    end

  end

  def edit
    @pe_question_activity_status = PeQuestionActivityStatus.find(params[:id])
    @pe_evaluation = @pe_question_activity_status.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def update

    @pe_question_activity_status = PeQuestionActivityStatus.find(params[:id])

    if @pe_question_activity_status.update_attributes(params[:pe_question_activity_status])
      redirect_to @pe_question_activity_status
    else
      @pe_evaluation = @pe_question_activity_status.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end

  def destroy
    @pe_question_activity_status = PeQuestionActivityStatus.find(params[:id])
    @pe_question_activity_status.destroy

    respond_to do |format|
      format.html { redirect_to pe_question_activity_statuses_url }
      format.json { head :no_content }
    end
  end
end
