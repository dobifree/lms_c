class CcCharacteristicsController < ApplicationController
  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @cc_characteristics = CcCharacteristic.all
  end

  def show
    @cc_characteristic = CcCharacteristic.find(params[:id])
  end

  def new
    @cc_characteristic = CcCharacteristic.new
  end

  def edit
    @cc_characteristic = CcCharacteristic.find(params[:id])
  end

  def create
    @cc_characteristic = CcCharacteristic.new(params[:cc_characteristic])

    if @cc_characteristic.save
      redirect_to cc_characteristics_path, notice: 'J characteristic was successfully created.'
    else
      render action: 'new'
    end

  end

  # PUT /cc_characteristics/1
  # PUT /cc_characteristics/1.json
  def update
    @cc_characteristic = CcCharacteristic.find(params[:id])

    respond_to do |format|
      if @cc_characteristic.update_attributes(params[:cc_characteristic])
        format.html { redirect_to @cc_characteristic, notice: 'J characteristic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cc_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cc_characteristics/1
  # DELETE /cc_characteristics/1.json
  def destroy
    @cc_characteristic = CcCharacteristic.find(params[:id])
    @cc_characteristic.destroy

    redirect_to cc_characteristics_url

  end
end
