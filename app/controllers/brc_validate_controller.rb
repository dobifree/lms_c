class BrcValidateController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:list_people, :massive_validate, :download_massive_validate_excel_format, :massive_validate_categ, :validate_item]
  before_filter :get_data_2, only: [:show, :new, :create, :finish]
  before_filter :get_data_3, only: [:massive_validate_categ, :validate_item]


  before_filter :validate_open_process, only: [:list_people, :show, :new, :create, :finish, :massive_validate, :download_massive_validate_excel_format, :massive_validate_categ, :validate_item]
  before_filter :validate_can_validate, only: [:show, :new, :create, :finish]

  def list_processes
    @brc_processes = user_connected.active_brc_processes_to_validate
  end

  def list_people
    @brc_members = @brc_process.brc_members_ordered_by_companies_ids @companies_ids
  end

  def show

  end

  def finish
    val = true

    @brc_member.brc_bonus_items.each do |brc_bonus_item|

      if params['validate_bonus_item_'+brc_bonus_item.id.to_s] == '0'

        val = false

        brc_bonus_item.rejected = true
        brc_bonus_item.validated = false
        brc_bonus_item.val_comment = params['validate_bonus_item_'+brc_bonus_item.id.to_s+'_comment']
        brc_bonus_item.save

      else

        brc_bonus_item.rejected = false
        brc_bonus_item.validated = true
        brc_bonus_item.val_comment = nil
        brc_bonus_item.save

      end

    end

    @brc_member.status_val = val
    @brc_member.status_rej = !val

    unless val
      @brc_member.status_def = false
    end

    @brc_member.save
    flash[:success] = t('activerecord.success.model.brc_member.validate_ok')
    redirect_to brc_validate_show_path(@brc_member)
  end

  def massive_validate

    @brc_process = BrcProcess.find(params[:brc_process_id])

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    brc_members_all = Array.new
    brc_members_rej = Array.new

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          next if index == 0

          if fila[0]

            brc_bonus_item = BrcBonusItem.find fila[0]

            if brc_bonus_item

              brc_member = brc_bonus_item.brc_member

              brc_members_all.push brc_member

              if fila[8] == 'SI'

                brc_bonus_item.validated = true
                brc_bonus_item.rejected = false


              else

                brc_bonus_item.validated = false
                brc_bonus_item.rejected = true
                brc_bonus_item.val_comment = fila[9]

                brc_member.status_val = false
                brc_member.status_rej = true
                brc_member.status_def = false
                brc_member.save

                brc_members_rej.push brc_member

              end

              brc_bonus_item.save

            end

          end

        end

        brc_members_all.uniq!
        brc_members_rej.uniq!

        brc_members_all = brc_members_all - brc_members_rej

        brc_members_all.each do |brc_m|
          brc_m.status_val = true
          brc_m.status_rej = false
          brc_m.save
        end

        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def download_massive_validate_excel_format

    reporte_excel = massive_massive_validate_excel_format @brc_process

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_validar.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def massive_validate_categ

    if @companies_ids.include? @characteristic_value.id

      @brc_category = BrcCategory.find(params[:brc_category_id])

      if @brc_category

        if params['validate_bonus_categ_'+@characteristic_value.id.to_s+'_'+@brc_category.id.to_s] == '0' && params['comment_categ_'+@characteristic_value.id.to_s+'_'+@brc_category.id.to_s].blank?

          flash[:danger] = t('activerecord.success.model.brc_member.validate_no_comment_error')

        else

          brcv = @brc_process.brc_process_categ_vals.where('characteristic_value_id = ? AND brc_category_id = ?', @characteristic_value.id, @brc_category.id).first

          unless brcv

            brcv = @brc_process.brc_process_categ_vals.build
            brcv.brc_category = @brc_category
            brcv.characteristic_value = @characteristic_value

          end

          brcv.comment_val = params['comment_categ_'+@characteristic_value.id.to_s+'_'+@brc_category.id.to_s]
          brcv.save

          @brc_members = @brc_process.brc_members_ordered_by_companies_ids_editable @companies_ids

          @brc_members.each do |brc_member|

            if brc_member.status_def && !brc_member.status_val

              uc_c = brc_member.user.user_characteristic @characteristic_company

              if uc_c.value == @characteristic_value.id

                brc_member.brc_bonus_items.each do |brc_bonus_item|

                  if brc_bonus_item.validated.nil?

                    if brc_bonus_item.brc_category_id == @brc_category.id

                      if params['validate_bonus_categ_'+@characteristic_value.id.to_s+'_'+@brc_category.id.to_s] == '1'
                        brc_bonus_item.validated = true
                        brc_bonus_item.rejected = false
                      else
                        brc_bonus_item.validated = false
                        brc_bonus_item.rejected = true
                        brc_bonus_item.val_comment = ''

                      end

                      brc_bonus_item.save

                    end

                  end

                end

              end

              num_rej = 0
              num_val = 0

              brc_member.brc_bonus_items.each do |brc_bonus_item|

                num_rej += 1 if brc_bonus_item.rejected

                num_val += 1 if brc_bonus_item.validated

              end

              if num_rej + num_val == brc_member.brc_bonus_items.size

                if num_rej == 0

                  brc_member.status_val = true
                  brc_member.status_rej = false
                  brc_member.save
                else
                  brc_member.status_val = false
                  brc_member.status_rej = true
                  brc_member.status_def = false
                  brc_member.save
                end

              end

            end

          end

          flash[:success] = t('activerecord.success.model.brc_member.validate_ok')

        end

      end

    end


    redirect_to brc_validate_list_people_path(@brc_process)

  end

  def validate_item
    @brc_bonus_item = BrcBonusItem.find(params[:brc_bonus_item_id])

    if @companies_ids.include? @characteristic_value.id

      @brc_members = @brc_process.brc_members_ordered_by_companies_ids_editable @companies_ids

      @brc_members.each do |brc_member|

        if @brc_bonus_item.brc_member == brc_member

          if brc_member.status_def && !brc_member.status_val

              uc_c = brc_member.user.user_characteristic @characteristic_company

              if uc_c.value == @characteristic_value.id

                brc_bonus_item = @brc_bonus_item

                if brc_bonus_item.validated.nil?

                  if brc_bonus_item.brc_category_id == @brc_category.id

                    if params['validate_bonus_item_'+@brc_bonus_item.id.to_s] == '0' && params['validate_bonus_item_'+brc_bonus_item.id.to_s+'_comment'].blank?

                      flash[:danger] = t('activerecord.success.model.brc_member.validate_no_comment_error')

                    else

                      if params['validate_bonus_item_'+@brc_bonus_item.id.to_s] == '1'
                        brc_bonus_item.validated = true
                        brc_bonus_item.rejected = false
                        brc_bonus_item.val_comment = ''
                      else
                        brc_bonus_item.validated = false
                        brc_bonus_item.rejected = true
                        brc_bonus_item.val_comment = params['validate_bonus_item_'+brc_bonus_item.id.to_s+'_comment']

                      end

                      brc_bonus_item.save

                      flash[:success] = t('activerecord.success.model.brc_member.validate_ok')

                    end

                  end

                end


              end

              num_rej = 0
              num_val = 0

              brc_member.brc_bonus_items.each do |brc_bonus_item|

                num_rej += 1 if brc_bonus_item.rejected

                num_val += 1 if brc_bonus_item.validated

              end

              if num_rej + num_val == brc_member.brc_bonus_items.size

                if num_rej == 0

                  brc_member.status_val = true
                  brc_member.status_rej = false
                  brc_member.save
                else
                  brc_member.status_val = false
                  brc_member.status_rej = true
                  brc_member.status_def = false
                  brc_member.save
                end

              end



          end

        end

      end

    end


    redirect_to brc_validate_list_people_path(@brc_process)

  end

  private

  def get_data_1
    @brc_process = BrcProcess.find(params[:brc_process_id])

    @companies_ids = Array.new

    @brc_process.brc_definers_by_validator(user_connected).each do |brc_definer|
      @companies_ids += brc_definer.brc_definer_companies.map { |brc_d_c| brc_d_c.characteristic_value_id }
    end

    @characteristic_company = Characteristic.get_characteristic_company

  end

  def get_data_2
    @brc_member = BrcMember.find(params[:brc_member_id])
    @brc_process = @brc_member.brc_process

    @companies_ids = Array.new

    @brc_process.brc_definers_by_validator(user_connected).each do |brc_definer|
      @companies_ids += brc_definer.brc_definer_companies.map { |brc_d_c| brc_d_c.characteristic_value_id }
    end

    @characteristic_company = Characteristic.get_characteristic_company

  end

  def get_data_3
    @characteristic_value = CharacteristicValue.find(params[:cv_id])
    @brc_category = BrcCategory.find(params[:brc_category_id])
  end

  def validate_open_process

    unless @brc_process.active
      flash[:danger] = t('activerecord.error.model.brc_process.not_active')
      redirect_to root_path
    end

  end

  def validate_can_validate

    uc_cp_id = @brc_member.user.characteristic_value @characteristic_company

    unless @companies_ids.include? uc_cp_id
      flash[:danger] = t('activerecord.error.model.brc_process.cant_define')
      redirect_to root_path
    end
  end

  def massive_massive_validate_excel_format(brc_process)

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Bonos')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'codigo_bono'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Rut'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Persona'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Empresa'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Cohade'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Categoría'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Tipo'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Bono'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Validar (SI/NO)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Comentario NO'
        filaHeader <<  headerCenter
        cols_widths << 20





        reporte_excel.add_row fila, :style => filaHeader, height: 20

        characteristic_company = Characteristic.get_characteristic_company

        brc_process.brc_members_ordered_by_companies_ids(@companies_ids).each do |brc_member|

          if brc_member && brc_member.status_def && !brc_member.status_val

            uc_c = brc_member.user.user_characteristic(characteristic_company)

            brc_member.brc_bonus_items.each do |brc_bonus_item|

              fila = Array.new
              fileStyle = Array.new

              fila << brc_bonus_item.id
              fileStyle << fieldLeft

              fila << brc_member.user.codigo
              fileStyle << fieldLeft

              fila << brc_member.user.apellidos+', '+brc_member.user.nombre
              fileStyle << fieldLeft

              fila << (uc_c ? uc_c.formatted_value : '')
              fileStyle << fieldLeft

              fila << brc_bonus_item.secu_cohade.name
              fileStyle << fieldLeft

              fila << brc_bonus_item.brc_category.name
              fileStyle << fieldLeft

              if brc_bonus_item.p_a
                fila << '% SUBASO'
                fileStyle << fieldLeft

                fila << brc_bonus_item.percentage
                fileStyle << fieldLeft

              else
                fila << 'pesos'
                fileStyle << fieldLeft

                fila << brc_bonus_item.amount
                fileStyle << fieldLeft
              end

              fila << (brc_bonus_item.rejected ? 'NO' : 'SI')
              fileStyle << fieldLeft

              fila << brc_bonus_item.val_comment
              fileStyle << fieldLeft

              reporte_excel.add_row fila, :style => fileStyle, height: 20

            end

          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

end
