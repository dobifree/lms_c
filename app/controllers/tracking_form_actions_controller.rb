class TrackingFormActionsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @tracking_form = TrackingForm.find(params[:tracking_form_id])
    @tracking_form_actions = @tracking_form.tracking_form_actions
    render :layout => false
  end

  # GET /tracking_form_actions/1
  # GET /tracking_form_actions/1.json
  def show
    @tracking_form_action = TrackingFormAction.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tracking_form_action }
    end
  end

  # GET /tracking_form_actions/new
  # GET /tracking_form_actions/new.json
  def new
    @tracking_form = TrackingForm.find(params[:tracking_form_id])
    @tracking_form_action = @tracking_form.tracking_form_actions.build

  end

  # GET /tracking_form_actions/1/edit
  def edit
    @tracking_form_action = TrackingFormAction.find(params[:id])
  end

  # POST /tracking_form_actions
  # POST /tracking_form_actions.json
  def create
    @tracking_form_action = TrackingFormAction.new(params[:tracking_form_action])

      if @tracking_form_action.save
        flash['success'] = 'Acción creada correctamente'
        redirect_to tracking_form_pill_open_path(@tracking_form_action.tracking_form, 'actions')
      else
        flash.now['danger'] = 'Acción no pudo ser creada correctamente'
        render action: "new"
      end

  end

  # PUT /tracking_form_actions/1
  # PUT /tracking_form_actions/1.json
  def update
    @tracking_form_action = TrackingFormAction.find(params[:id])
      if @tracking_form_action.update_attributes(params[:tracking_form_action])
        flash['success'] = 'Acción actualizada correctamente'
        redirect_to tracking_form_pill_open_path(@tracking_form_action.tracking_form, 'actions')
      else
        flash.now['danger'] = 'Acción no pudo ser actualizada correctamente'
        if params[:tracking_form_action][:tracking_form_notifications_attributes]
          @action_types_array = @tracking_form_action.action_type_options.each_with_index.map { |action_type, index| [action_type, index] }
          render action: "manage_notifications"
        else
          render action: "edit"
        end
      end
  end

  # DELETE /tracking_form_actions/1
  # DELETE /tracking_form_actions/1.json
  def destroy
    @tracking_form_action = TrackingFormAction.find(params[:id])
    @tracking_form_action.destroy

    respond_to do |format|
      format.html { redirect_to tracking_form_actions_url }
      format.json { head :no_content }
    end
  end

  def manage_notifications
    @tracking_form_action = TrackingFormAction.find(params[:id])
    @action_types_array = @tracking_form_action.action_type_options.each_with_index.map { |action_type, index| [action_type, index] }
  end
end
