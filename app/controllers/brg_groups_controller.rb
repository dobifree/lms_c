class BrgGroupsController < ApplicationController
  include BrgModule
  include BrgGroupsHelper

  before_filter :authenticate_user
  before_filter :active_module
  before_filter :brg_manager
  before_filter :editable_group, only: %i[edit
                                          update
                                          search_user_for_assign
                                          group_user_create]
  before_filter :get_group, only: %i[manage
                                     edit
                                     update
                                     deactivate_group
                                     deactivate_group_ajax
                                     search_user_for_assign]
  before_filter :get_group_user, only: %i[group_user_deactivate]
  before_filter :get_process, only: %i[new create
                                       search_user_for_assign
                                       group_user_create
                                       groups_massive_upload
                                       groups_massive_upload_2
                                       group_users_massive_upload
                                       group_users_massive_upload_2]

  def index
    @tab = params[:tab] ? params[:tab] : 0
  end

  def new
    @group = BrgGroup.new(brg_process_id: @process.id)
  end

  def create
    group = BrgGroup.new(params[:brg_group])
    group.brg_process_id = @process.id
    group.group_cod = (BrgGroup.last_id.to_i+1).to_s + group.name  if (!group.group_cod || group.group_cod .empty?) && group.name
    group = set_registered(group)
    if group.save
      flash[:success] = t('views.brg_module.flash_messages.create_success')
      redirect_to brg_processes_manage_path(@process)
    else
      @group = group
      render 'new'
    end
  end

  def manage; end
  def edit; end

  def update
    if @group.update_attributes(params[:brg_group])
      flash[:success] = t('views.brg_module.flash_messages.change_success')
      redirect_to brg_groups_manage_path(@group)
    else
      render action: 'edit'
    end
  end

  def deactivate_group
    @group = deactivate(@group)
    @group.save
    @group.brg_group_users.active.each {|assoc| deactivate(assoc).save}
    flash[:success] = t('views.brg_module.flash_messages.delete_success')
    redirect_to brg_groups_index_path(t('views.brg_module.tabs.manager_index.groups'))
  end

  def deactivate_group_ajax
    group = deactivate(@group)
    group.save
    group.brg_group_users.active.each {|assoc| deactivate(assoc).save}
    render nothing: true
  end

  def group_user_deactivate
    group_user = deactivate(@group_user)
    group_user.save
    render nothing: true
  end

  def search_user_for_assign
    @user = User.new
    @users = {}

    if params[:user] && !(params[:user][:codigo].blank? && params[:user][:apellidos].blank? && params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def group_user_create
    group_user = BrgGroupUser.new(user_id: params[:user_id], brg_group_id: params[:brg_group_id])
    group_user = set_registered(group_user)
    if group_user.save
      flash[:success] = t('views.brg_module.flash_messages.create_success')
      redirect_to brg_groups_manage_path(group_user.brg_group)
    else
      @group_user = group_user
      @user = group_user.user
      @group = group_user.brg_group
      @users = []
      render 'search_user_for_assign'
    end
  end

  def groups_massive_upload; end

  def groups_massive_upload_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_groups file, @process.id
        flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'groups_massive_upload'
  end

  def group_users_massive_upload

  end

  def group_users_massive_upload_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_group_users file, @process.id
        flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'group_users_massive_upload'
  end

  private

  def editable_group
    return if BrgGroup.find(params[:brg_group_id]).editable?
    flash[:danger] = t('views.brg_module.flash_messages.no_editable_group_danger')
    redirect_to(brg_groups_index_path(t('views.brg_module.tabs.manager_index.groups')))
  end
end
