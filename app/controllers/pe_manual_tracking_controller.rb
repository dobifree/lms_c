class PeManualTrackingController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list]
  before_filter :get_data_2, only: [:show, :track]
  before_filter :get_data_3, only: [:create_manual_q_comment]
  before_filter :get_data_4, only: [:destroy_manual_q_comment]

  before_filter :validate_open_process, only: [:people_list, :show, :track, :create_manual_q_comment, :destroy_manual_q_comment]
  before_filter :validate_access_to_evaluate, only:[:show, :track, :create_manual_q_comment, :destroy_manual_q_comment]

  def people_list

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])

  end

  def show

   #@company = session[:company]

    @hr_evaluation_type_element_nivel_1 = @hr_process_evaluation.evaluation_type_element_level_1

    @hr_evaluation_type_element_nivel_2 = @hr_process_evaluation.evaluation_type_element_level_2

  end


  def track

    @rel = @hr_process_user_connected.hr_process_evalua_rels.where('hr_process_user2_id = ?', @hr_process_user.id).first

    if @rel

      es_rel_jefe = true if @rel.tipo == 'jefe'
      es_otra_rel = true if @rel.tipo == 'par'
      es_otra_rel = true if @rel.tipo == 'sub'
      es_otra_rel = true if @rel.tipo == 'cli'
      es_otra_rel = true if @rel.tipo == 'prov'

      rel_tipo = @rel.tipo

    else

      rel_tipo = nil

    end

    responder_eval_aux(@hr_process_evaluation, es_rel_jefe, false, es_otra_rel, rel_tipo)

    evalua_colaborador = true

    if @hr_process_user_connected.id == @hr_process_user.id

      evalua_colaborador = false

    end

    @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.each do |hr_evaluation_type_element|

      if hr_evaluation_type_element.evaluacion_colaborativa
        if es_rel_jefe && @hr_process_evaluation.hr_process_dimension_e.eval_auto
          #guardar la auto, hacer q el evaluador es el evaluado
          @hr_process_user_connected = @hr_process_user
          responder_eval_aux(@hr_process_evaluation, false, false, false, nil)

        elsif @hr_process_evaluation.hr_process_dimension_e.eval_auto
          #guardar la jefe, hacer q el evaluador es el jefe
          @hr_process_user.hr_process_es_evaluado_rels.each do |rel|

            if rel.tipo == 'jefe'

              @hr_process_user_connected = rel.hr_process_user1
              responder_eval_aux(@hr_process_evaluation, true, false, false, 'jefe')

            end

          end

        end

        break

      end

    end

    flash[:success] = t('activerecord.success.model.hr_process_assessment.porcentaje_logro_ok')

    redirect_to pe_manual_tracking_show_path(@hr_process_evaluation, @hr_process_user)

  end


  def create_manual_q_comment

    @hr_process_evaluaion_manual_q = HrProcessEvaluationManualQ.find(params[:hr_process_evaluation_manual_q_id])

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    manual_comment = @hr_process_evaluaion_manual_q.hr_process_evaluation_manual_qcs.build
    manual_comment.comentario = params[:comentario]
    manual_comment.fecha = lms_time
    manual_comment.hr_process_user = @hr_process_user_connected
    manual_comment.save

    flash[:success] = t('activerecord.success.model.hr_process_user_comment.create_ok')

    redirect_to pe_manual_tracking_show_path(@hr_process_evaluation, @hr_process_user)

  end

  def destroy_manual_q_comment

    @hr_process_evaluaion_manual_qc.destroy

    flash[:success] = t('activerecord.success.model.hr_process_user_comment.delete_ok')

    redirect_to pe_manual_tracking_show_path(@hr_process_evaluation, @hr_process_user)

  end



  private

    def get_data_1
      @hr_process = HrProcess.find(params[:hr_process_id])
    end

    def get_data_2
      @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    end

    def get_data_3

      @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.find(params[:hr_process_evaluation_manual_q_id])

      @hr_process_evaluation = @hr_process_evaluation_manual_q.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
      @hr_process_user = @hr_process_evaluation_manual_q.hr_process_user
    end

    def get_data_4

      @hr_process_evaluaion_manual_qc = HrProcessEvaluationManualQc.find(params[:hr_process_evaluation_manual_qc_id])

      @hr_process_evaluation_manual_q = @hr_process_evaluaion_manual_qc.hr_process_evaluation_manual_q

      @hr_process_evaluation = @hr_process_evaluation_manual_q.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id
      @hr_process_user = @hr_process_evaluation_manual_q.hr_process_user

    end

    def validate_open_process

      unless @hr_process.abierto && @hr_process.fecha_inicio < lms_time && @hr_process.fecha_fin > lms_time
        flash[:danger] = t('activerecord.error.model.hr_process_assessment.out_of_time')
        redirect_to root_path
      end

    end

    def validate_access_to_evaluate

      unless @hr_process_user_connected.hr_process_evalua_rels.where('hr_process_user2_id = ? AND tipo = ?', @hr_process_user.id, 'jefe').count > 0 || @hr_process_user.id == @hr_process_user_connected.id

        flash[:danger] = t('security.no_access_hr_perform_evaluation')
        redirect_to root_path

      end

    end

    def responder_eval_aux(hr_process_evaluation, es_rel_jefe, es_porcentaje_logro, es_otra_rel = false, rel_tipo = 'jefe')

      if es_rel_jefe || es_otra_rel
        HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND hr_process_user_eval_id = ? AND tipo_rel = ?', hr_process_evaluation.id, @hr_process_user.id, @hr_process_user_connected.id, rel_tipo).destroy_all
      elsif hr_process_evaluation.hr_process_dimension_e.eval_auto && @hr_process_user_connected.id == @hr_process_user.id
        HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND hr_process_user_eval_id = ? AND tipo_rel IS NULL', hr_process_evaluation.id, @hr_process_user.id, @hr_process_user_connected.id).destroy_all
      end

      #hr_process_assessment_e.destroy if hr_process_assessment_e

      hr_process_assessment_e = HrProcessAssessmentE.new
      hr_process_assessment_e.fecha = lms_time
      hr_process_assessment_e.hr_process_evaluation = hr_process_evaluation
      hr_process_assessment_e.hr_process_user = @hr_process_user
      hr_process_assessment_e.hr_process_user_eval_id = @hr_process_user_connected.id

      if es_rel_jefe || es_otra_rel
        hr_process_assessment_e.tipo_rel = rel_tipo
      elsif hr_process_evaluation.hr_process_dimension_e.eval_auto && @hr_process_user_connected.id == @hr_process_user.id
        hr_process_assessment_e.tipo_rel = nil
      end

      if hr_process_assessment_e.save

        total_puntos_eval = 0
        total_pesos_eval = 0
        max_total_puntos_eval = 0
        formula_promedio_eval = false
        se_queda_en_no_aplica_eval = true

        if hr_process_evaluation.carga_manual?
          hr_process_evaluation_qs = hr_process_evaluation.manual_questions_nivel_1(@hr_process_user)
        else
          hr_process_evaluation_qs = hr_process_evaluation.questions_nivel_1(@hr_process_user.hr_process_level.id)
        end

        total_puntos_eval_nivel_1_no_aplica = 0
        total_pesos_eval_nivel_1_no_aplica = 0
        max_total_puntos_eval_nivel_1_no_aplica = 0

        hr_process_evaluation_qs.each do |hr_process_evaluation_q|

          total_pesos_eval += hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
          max_total_puntos_eval += hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)
          formula_promedio_eval = hr_process_evaluation_q.hr_evaluation_type_element.formula_promedio

          hr_process_assessment_q = HrProcessAssessmentQ.new
          hr_process_assessment_q.hr_process_assessment_e = hr_process_assessment_e

          if hr_process_evaluation.carga_manual?
            hr_process_assessment_q.hr_process_evaluation_manual_q = hr_process_evaluation_q
          else
            hr_process_assessment_q.hr_process_evaluation_q = hr_process_evaluation_q
          end

          hr_process_assessment_q.hr_process_user = @hr_process_user
          hr_process_assessment_q.fecha = lms_time

          if !hr_process_evaluation.carga_manual? && hr_process_evaluation_q.comentario
            hr_process_assessment_q.comentario = params["preg_#{hr_process_evaluation_q.id}_comment".to_sym]
          end

          if hr_process_assessment_q.save

            if hr_process_evaluation.carga_manual?
              num_pregs = hr_process_evaluation_q.hr_process_evaluation_manual_qs.length
              num_alts = 0
            else
              num_pregs = hr_process_evaluation_q.hr_process_evaluation_qs.length
              num_alts = hr_process_evaluation_q.hr_process_evaluation_as.length
            end

            if num_pregs == 0

              #se asume que se evalúa y no es sólo un agrupador

              if num_alts == 0

                if params["preg_#{hr_process_evaluation_q.id}".to_sym]

                  hr_process_assessment_q.resultado_puntos = params["preg_#{hr_process_evaluation_q.id}".to_sym].to_f

                  if hr_process_evaluation_q.hr_evaluation_type_element.indicador_logro


                    if hr_process_evaluation_q.tipo_indicador == 5

                      hr_process_assessment_q.logro_indicador_d = params["preg_#{hr_process_evaluation_q.id}".to_sym]

                    else

                      hr_process_assessment_q.logro_indicador = params["preg_#{hr_process_evaluation_q.id}".to_sym].to_f

                    end

                    if hr_process_evaluation_q.tipo_indicador == 5 || hr_process_assessment_q.logro_indicador

                      case hr_process_evaluation_q.tipo_indicador
                        when 1
                          hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador && hr_process_evaluation_q.meta_indicador > 0 ? hr_process_assessment_q.logro_indicador*100/hr_process_evaluation_q.meta_indicador : 0
                        when 2
                          hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador ? hr_process_evaluation_q.meta_indicador*100/hr_process_assessment_q.logro_indicador : 0
                        when 3
                          if hr_process_assessment_q.logro_indicador >= hr_process_evaluation_q.meta_indicador*-1 && hr_process_assessment_q.logro_indicador <= hr_process_evaluation_q.meta_indicador
                            hr_process_assessment_q.resultado_puntos = 100
                          else
                            hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador ? 100-(hr_process_assessment_q.logro_indicador-hr_process_evaluation_q.meta_indicador).abs*100/hr_process_evaluation_q.meta_indicador : 0
                          end
                        when 4
                          if hr_process_evaluation_q.pe_question_ranks.size > 0
                            metas = Array.new
                            porcentajes = Array.new

                            hr_process_evaluation_q.pe_question_ranks.each do |pe_question_rank|

                              metas.push pe_question_rank.goal
                              porcentajes.push pe_question_rank.percentage
                            end

                            if metas.first <= metas.last
                              #directo

                              if hr_process_assessment_q.logro_indicador < metas.first
                                hr_process_assessment_q.resultado_puntos = 0
                              elsif hr_process_assessment_q.logro_indicador >= metas.last
                                hr_process_assessment_q.resultado_puntos = porcentajes.last
                              else

                                meta_inf = meta_dif = 0
                                por_inf = por_dif = 0


                                (0..metas.length-1).each do |num_meta|
                                  if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= hr_process_assessment_q.logro_indicador && hr_process_assessment_q.logro_indicador <= metas[num_meta+1]
                                    meta_dif = metas[num_meta+1] - metas[num_meta]
                                    meta_inf = metas[num_meta]
                                    por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                    por_inf = porcentajes[num_meta]
                                    break
                                  end
                                end

                                if meta_dif == 0
                                  hr_process_assessment_q.resultado_puntos = por_inf
                                elsif meta_dif > 0
                                  hr_process_assessment_q.resultado_puntos = ((hr_process_assessment_q.logro_indicador-meta_inf)*por_dif/meta_dif)+por_inf
                                else
                                  hr_process_assessment_q.resultado_puntos = 0
                                end

                              end

                            else
                              #inverso
                              if hr_process_assessment_q.logro_indicador > metas.first
                                hr_process_assessment_q.resultado_puntos = 0
                              elsif hr_process_assessment_q.logro_indicador <= metas.last
                                hr_process_assessment_q.resultado_puntos = porcentajes.last
                              else

                                meta_sup = meta_dif = 0
                                por_inf = por_dif = 0


                                (0..metas.length-1).each do |num_meta|
                                  if metas[num_meta] && metas[num_meta+1] && metas[num_meta] >= hr_process_assessment_q.logro_indicador && hr_process_assessment_q.logro_indicador >= metas[num_meta+1]
                                    meta_dif = metas[num_meta] - metas[num_meta+1]
                                    meta_sup = metas[num_meta]
                                    por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                    por_inf = porcentajes[num_meta]
                                    break
                                  end
                                end

                                if meta_dif == 0
                                  hr_process_assessment_q.resultado_puntos = por_inf
                                elsif meta_dif > 0
                                  hr_process_assessment_q.resultado_puntos = ((meta_sup-hr_process_assessment_q.logro_indicador)*por_dif/meta_dif)+por_inf
                                else
                                  hr_process_assessment_q.resultado_puntos = 0
                                end

                              end

                            end

                          else
                            hr_process_assessment_q.resultado_puntos = 0
                          end

                        when 5

                          hr_process_assessment_q.resultado_puntos = 0

                          if hr_process_evaluation_q.pe_question_ranks.size > 0

                            hr_process_evaluation_q.pe_question_ranks.each do |pe_question_rank|

                              if hr_process_assessment_q.logro_indicador_d == pe_question_rank.discrete_goal

                                hr_process_assessment_q.resultado_puntos = pe_question_rank.percentage
                                break
                              end
                            end

                          end

                      end

                      hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.hr_evaluation_type_element.valor_minimo if hr_process_assessment_q.resultado_puntos < hr_process_evaluation_q.hr_evaluation_type_element.valor_minimo
                      hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo if hr_process_assessment_q.resultado_puntos > hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo

                    end

                  end

                  if hr_process_evaluation.cien_x_cien_puntos

                    hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                  else

                    hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo

                  end

                  if hr_process_assessment_q.save

                    total_puntos_eval += hr_process_assessment_q.resultado_puntos*hr_process_evaluation_q.peso

                    if hr_process_evaluation_q.hr_evaluation_type_element.indicador_logro && es_rel_jefe && es_porcentaje_logro

                      hr_process_assessment_l = HrProcessAssessmentL.new
                      hr_process_assessment_l.hr_process_evaluation_q_id = hr_process_assessment_q.hr_process_evaluation_q_id
                      hr_process_assessment_l.hr_process_evaluation_manual_q_id = hr_process_assessment_q.hr_process_evaluation_manual_q_id
                      hr_process_assessment_l.hr_process_user_id = hr_process_assessment_q.hr_process_user_id
                      hr_process_assessment_l.hr_process_user_eval_id = @hr_process_user_connected.id
                      hr_process_assessment_l.resultado_puntos = hr_process_assessment_q.resultado_puntos
                      hr_process_assessment_l.resultado_porcentaje = hr_process_assessment_q.resultado_porcentaje
                      hr_process_assessment_l.logro_indicador = hr_process_assessment_q.logro_indicador
                      hr_process_assessment_l.fecha = lms_time

                      hr_process_assessment_l.save

                    end

                  end

                end

              else

                if params["preg_#{hr_process_evaluation_q.id}".to_sym]

                  hr_process_evaluation_q.hr_process_evaluation_as.each do |hr_process_evaluation_a|

                    if params["preg_#{hr_process_evaluation_q.id}".to_sym] == hr_process_evaluation_a.id.to_s

                      hr_process_assessment_q.hr_process_evaluation_a = hr_process_evaluation_a
                      hr_process_assessment_q.resultado_puntos = hr_process_evaluation_a.valor
                      if hr_process_evaluation.cien_x_cien_puntos
                        hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos
                      else
                        hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo
                      end


                      if hr_process_assessment_q.save

                        if hr_process_evaluation_a.valor > -1

                          se_queda_en_no_aplica_eval=false

                          total_puntos_eval += hr_process_assessment_q.resultado_puntos*hr_process_evaluation_q.peso
                        else

                          total_pesos_eval -= hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
                          max_total_puntos_eval -= hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)
                        end

                      end

                    end


                  end

                end

              end

            else
              total_puntos_preg = 0
              total_pesos_preg = 0
              max_total_puntos_preg = 0
              formula_promedio_preg = false
              se_queda_en_no_aplica = true

              if hr_process_evaluation.carga_manual?
                hr_process_evaluation_qs = hr_process_evaluation_q.hr_process_evaluation_manual_qs
              else
                hr_process_evaluation_qs = hr_process_evaluation_q.hr_process_evaluation_qs
              end

              hr_process_evaluation_qs.each do |hr_process_evaluation_q1|

                total_pesos_preg += hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0
                max_total_puntos_preg += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
                formula_promedio_preg = hr_process_evaluation_q1.hr_evaluation_type_element.formula_promedio

                hr_process_assessment_q1 = HrProcessAssessmentQ.new
                hr_process_assessment_q1.hr_process_assessment_e = hr_process_assessment_e

                if hr_process_evaluation.carga_manual?
                  hr_process_assessment_q1.hr_process_evaluation_manual_q = hr_process_evaluation_q1
                else
                  hr_process_assessment_q1.hr_process_evaluation_q = hr_process_evaluation_q1
                end
                hr_process_assessment_q1.hr_process_user = @hr_process_user
                hr_process_assessment_q1.fecha = lms_time


                if !hr_process_evaluation.carga_manual? && hr_process_evaluation_q1.comentario
                  hr_process_assessment_q1.comentario = params["preg_#{hr_process_evaluation_q1.id}_comment".to_sym]
                end

                if hr_process_assessment_q1.save

                  if hr_process_evaluation.carga_manual? || hr_process_evaluation_q1.hr_process_evaluation_as.length == 0

                    if params["preg_#{hr_process_evaluation_q1.id}".to_sym]

                      hr_process_assessment_q1.resultado_puntos = params["preg_#{hr_process_evaluation_q1.id}".to_sym].to_f

                      if hr_process_evaluation_q1.hr_evaluation_type_element.indicador_logro

                        unless params['comentario_field_'+hr_process_evaluation_q1.id.to_s].blank?

                          if @hr_process_user_connected.user.id == user_connected.id

                            manual_comment = hr_process_evaluation_q1.hr_process_evaluation_manual_qcs.build
                            manual_comment.comentario = params['comentario_field_'+hr_process_evaluation_q1.id.to_s]
                            manual_comment.fecha = lms_time
                            manual_comment.hr_process_user = @hr_process_user_connected
                            manual_comment.save

                          end

                        end

                        if hr_process_evaluation_q1.tipo_indicador == 5

                          hr_process_assessment_q1.logro_indicador_d = params["preg_#{hr_process_evaluation_q1.id}".to_sym]

                        else

                          hr_process_assessment_q1.logro_indicador = params["preg_#{hr_process_evaluation_q1.id}".to_sym].to_f

                        end

                        if hr_process_evaluation_q1.tipo_indicador == 5 || hr_process_assessment_q1.logro_indicador



                          case hr_process_evaluation_q1.tipo_indicador
                            when 1

                              hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador && hr_process_evaluation_q1.meta_indicador > 0 ? hr_process_assessment_q1.logro_indicador*100/hr_process_evaluation_q1.meta_indicador : 0
                            when 2
                              hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador ? hr_process_evaluation_q1.meta_indicador*100/hr_process_assessment_q1.logro_indicador : 0
                            when 3
                              if hr_process_assessment_q1.logro_indicador >= hr_process_evaluation_q1.meta_indicador*-1 && hr_process_assessment_q1.logro_indicador <= hr_process_evaluation_q1.meta_indicador
                                hr_process_assessment_q1.resultado_puntos = 100
                              else
                                hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador ? 100-(hr_process_assessment_q1.logro_indicador-hr_process_evaluation_q1.meta_indicador).abs*100/hr_process_evaluation_q1.meta_indicador : 0
                              end

                            when 4
                              if hr_process_evaluation_q1.pe_question_ranks.size > 0
                                metas = Array.new
                                porcentajes = Array.new

                                hr_process_evaluation_q1.pe_question_ranks.each do |pe_question_rank|

                                  metas.push pe_question_rank.goal
                                  porcentajes.push pe_question_rank.percentage
                                end


                                if metas.first <= metas.last

                                  #directo
                                  if hr_process_assessment_q1.logro_indicador < metas.first
                                    hr_process_assessment_q1.resultado_puntos = 0
                                  elsif hr_process_assessment_q1.logro_indicador >= metas.last
                                    hr_process_assessment_q1.resultado_puntos = porcentajes.last
                                  else

                                    meta_inf = meta_dif = 0
                                    por_inf = por_dif = 0


                                    (0..metas.length-1).each do |num_meta|
                                      if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= hr_process_assessment_q1.logro_indicador && hr_process_assessment_q1.logro_indicador <= metas[num_meta+1]
                                        meta_dif = metas[num_meta+1] - metas[num_meta]
                                        meta_inf = metas[num_meta]
                                        por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                        por_inf = porcentajes[num_meta]
                                        break
                                      end
                                    end

                                    if meta_dif == 0
                                      hr_process_assessment_q1.resultado_puntos = por_inf
                                    elsif meta_dif > 0
                                      hr_process_assessment_q1.resultado_puntos = ((hr_process_assessment_q1.logro_indicador-meta_inf)*por_dif/meta_dif)+por_inf
                                    else
                                      hr_process_assessment_q1.resultado_puntos = 0
                                    end

                                  end

                                else
                                  #inverso

                                  if hr_process_assessment_q1.logro_indicador > metas.first
                                    hr_process_assessment_q1.resultado_puntos = 0
                                  elsif hr_process_assessment_q1.logro_indicador <= metas.last
                                    hr_process_assessment_q1.resultado_puntos = porcentajes.last
                                  else

                                    meta_sup = meta_dif = 0
                                    por_inf = por_dif = 0


                                    (0..metas.length-1).each do |num_meta|
                                      if metas[num_meta] && metas[num_meta+1] && metas[num_meta] >= hr_process_assessment_q1.logro_indicador && hr_process_assessment_q1.logro_indicador >= metas[num_meta+1]
                                        meta_dif = metas[num_meta] - metas[num_meta+1]
                                        meta_sup = metas[num_meta]
                                        por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                        por_inf = porcentajes[num_meta]
                                        break
                                      end
                                    end

                                    if meta_dif == 0
                                      hr_process_assessment_q1.resultado_puntos = por_inf
                                    elsif meta_dif > 0
                                      hr_process_assessment_q1.resultado_puntos = ((meta_sup-hr_process_assessment_q1.logro_indicador)*por_dif/meta_dif)+por_inf
                                    else
                                      hr_process_assessment_q1.resultado_puntos = 0
                                    end

                                  end


                                end

                              else
                                hr_process_assessment_q1.resultado_puntos = 0
                              end

                            when 5

                              hr_process_assessment_q1.resultado_puntos = 0

                              if hr_process_evaluation_q1.pe_question_ranks.size > 0

                                hr_process_evaluation_q1.pe_question_ranks.each do |pe_question_rank|

                                  if hr_process_assessment_q1.logro_indicador_d == pe_question_rank.discrete_goal
                                    hr_process_assessment_q1.resultado_puntos = pe_question_rank.percentage
                                    break
                                  end
                                end

                              end



                          end

                          hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.hr_evaluation_type_element.valor_minimo if hr_process_assessment_q1.resultado_puntos && hr_process_assessment_q1.resultado_puntos < hr_process_evaluation_q1.hr_evaluation_type_element.valor_minimo
                          hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo if hr_process_assessment_q1.resultado_puntos && hr_process_assessment_q1.resultado_puntos > hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo


                        end

                      end


                      if hr_process_evaluation.cien_x_cien_puntos

                        hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                      else

                        hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo

                      end



                      if hr_process_assessment_q1.save

                        total_puntos_preg += hr_process_assessment_q1.resultado_puntos*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                        total_puntos_eval_nivel_1_no_aplica += hr_process_assessment_q1.resultado_puntos*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
                        total_pesos_eval_nivel_1_no_aplica += (hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                        max_total_puntos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                      end

                    end

                  elsif hr_process_evaluation_q1.hr_process_evaluation_as.length > 0

                    # si tiene alternativas no puede haber sido carga manual

                    if params["preg_#{hr_process_evaluation_q1.id}".to_sym]

                      hr_process_evaluation_q1.hr_process_evaluation_as.each do |hr_process_evaluation_a|

                        if params["preg_#{hr_process_evaluation_q1.id}".to_sym] == hr_process_evaluation_a.id.to_s

                          hr_process_assessment_q1.hr_process_evaluation_a = hr_process_evaluation_a
                          hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_a.valor

                          if hr_process_evaluation.cien_x_cien_puntos

                            hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                          else

                            hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo

                          end

                          if hr_process_assessment_q1.save


                            if hr_process_evaluation_a.valor > -1

                              se_queda_en_no_aplica = false

                              total_puntos_preg += hr_process_assessment_q1.resultado_puntos*hr_process_evaluation_q1.peso

                              total_puntos_eval_nivel_1_no_aplica += hr_process_assessment_q1.resultado_puntos*hr_process_evaluation_q1.peso
                              total_pesos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.peso

                              max_total_puntos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*hr_process_evaluation_q1.peso
                            else
                              total_pesos_preg -= hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0
                              max_total_puntos_preg -= hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
                            end

                          end

                        end

                      end

                    end

                  end

                end

              end



              if se_queda_en_no_aplica

                hr_process_assessment_q.resultado_puntos = -1

              else

                se_queda_en_no_aplica_eval = false

                if formula_promedio_preg

                  if total_pesos_preg > 0

                    hr_process_assessment_q.resultado_puntos = total_puntos_preg/total_pesos_preg
                    if hr_process_evaluation.cien_x_cien_puntos
                      max_total_puntos_preg = hr_process_evaluation.cien_x_cien_puntos
                    else
                      max_total_puntos_preg = max_total_puntos_preg/total_pesos_preg
                    end

                  else

                    hr_process_assessment_q.resultado_puntos = 0
                    max_total_puntos_preg = 0

                  end

                else

                  hr_process_assessment_q.resultado_puntos = total_puntos_preg

                end

              end

              if max_total_puntos_preg > 0

                hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/max_total_puntos_preg

              else

                hr_process_assessment_q.resultado_porcentaje = 0

              end



              if hr_process_assessment_q.save

                if hr_process_assessment_q.resultado_puntos > -1

                  total_puntos_eval += hr_process_assessment_q.resultado_puntos*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)

                else

                  total_pesos_eval -= hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
                  max_total_puntos_eval -= hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)

                end

              end


            end

          end



        end



        if formula_promedio_eval


          if hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 1).first.aplica_evaluacion

            if total_pesos_eval > 0

              if se_queda_en_no_aplica_eval
                hr_process_assessment_e.resultado_puntos = -1
              else
                hr_process_assessment_e.resultado_puntos = total_puntos_eval/total_pesos_eval
              end

              max_total_puntos_eval = max_total_puntos_eval/total_pesos_eval

            else

              if se_queda_en_no_aplica_eval
                hr_process_assessment_e.resultado_puntos = -1
              else
                hr_process_assessment_e.resultado_puntos = 0
              end

              max_total_puntos_eval = 0

            end

          else

            if total_pesos_eval_nivel_1_no_aplica > 0



              hr_process_assessment_e.resultado_puntos = total_puntos_eval_nivel_1_no_aplica/total_pesos_eval_nivel_1_no_aplica
              max_total_puntos_eval = max_total_puntos_eval_nivel_1_no_aplica/total_pesos_eval_nivel_1_no_aplica

            else

              hr_process_assessment_e.resultado_puntos = 0
              max_total_puntos_eval = 0

            end

          end


        else

          if hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 1).first.aplica_evaluacion

            if se_queda_en_no_aplica_eval
              hr_process_assessment_e.resultado_puntos = -1
            else
              hr_process_assessment_e.resultado_puntos = total_puntos_eval
            end

          else

            if se_queda_en_no_aplica_eval

              hr_process_assessment_e.resultado_puntos = -1
            else
              hr_process_assessment_e.resultado_puntos = total_puntos_eval_nivel_1_no_aplica
            end

          end

        end

        max_total_puntos_eval = hr_process_evaluation.cien_x_cien_puntos if hr_process_evaluation.cien_x_cien_puntos

        if max_total_puntos_eval > 0

          hr_process_assessment_e.resultado_porcentaje = hr_process_assessment_e.resultado_puntos*100/max_total_puntos_eval

        else

          hr_process_assessment_e.resultado_porcentaje = 0

        end

        hr_process_assessment_e.save

        if hr_process_evaluation.tope_no_aplica

          type_element_second_orden = hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.last
          if type_element_second_orden
            total_preguntas_contestadas_no_aplica = hr_process_assessment_e.hr_process_assessment_qs.where('resultado_puntos = -1 AND hr_process_evaluation_q_id NOT IN(?)',hr_process_evaluation.questions_nivel_1(@hr_process_user.hr_process_level.id).map(&:id)).count if hr_process_assessment_e
            total_preguntas_por_contestar_no_aplica = hr_process_evaluation.total_number_of_questions_type_element(@hr_process_user.hr_process_level.id, type_element_second_orden.id)
          else
            total_preguntas_contestadas_no_aplica = hr_process_assessment_e.hr_process_assessment_qs.where('resultado_puntos = -1').count if hr_process_assessment_e
            total_preguntas_por_contestar_no_aplica = total_preguntas_por_contestar
          end

          if (total_preguntas_contestadas_no_aplica*100/total_preguntas_por_contestar_no_aplica)>hr_process_evaluation.tope_no_aplica
            hr_process_assessment_e.resultado_puntos = -1
            hr_process_assessment_e.save
          end

        end

        calculate_final_results hr_process_assessment_e.hr_process_evaluation.hr_process_dimension_e.hr_process_dimension

      end

    end

    def calculate_final_results(hr_process_dimension)

      hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

      dimension_es = hr_process_dimension.hr_process_dimension_es

      resultado_dimension = 0

      hr_process_evaluations.each do |hr_process_evaluation|

        if hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.id == hr_process_dimension.id


          HrProcessAssessmentEf.where('hr_process_evaluation_id = ? AND hr_process_user_id = ?', hr_process_evaluation.id, @hr_process_user.id).destroy_all


          resultado_evaluation = 0
          suma_pesos_evaluation = 0
          total_puntos_evaluation = 0

          HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND resultado_puntos <> - 1', hr_process_evaluation.id, @hr_process_user.id).each do |hr_process_assessment_e|

            peso = 0

            if hr_process_assessment_e.tipo_rel == 'jefe'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso
            elsif hr_process_assessment_e.tipo_rel.nil?
              peso = hr_process_evaluation.hr_process_dimension_e.eval_auto_peso
            elsif hr_process_assessment_e.tipo_rel == 'sub'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_sub_peso
            elsif hr_process_assessment_e.tipo_rel == 'par'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_par_peso
            elsif hr_process_assessment_e.tipo_rel == 'cli'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_cli_peso
            elsif hr_process_assessment_e.tipo_rel == 'prov'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_prov_peso
            end

            suma_pesos_evaluation += peso
            resultado_evaluation += hr_process_assessment_e.resultado_porcentaje * peso
            total_puntos_evaluation += 100 * peso
            #if peso > 0
            # puts '...'
            # puts hr_process_assessment_e.resultado_porcentaje
            # puts resultado_evaluation
            #end
          end

          if suma_pesos_evaluation > 0

            porcentaje_evaluation = resultado_evaluation*100/total_puntos_evaluation
            resultado_evaluation = resultado_evaluation/suma_pesos_evaluation

            hr_process_assessment_ef = HrProcessAssessmentEf.new
            hr_process_assessment_ef.fecha = lms_time
            hr_process_assessment_ef.hr_process_evaluation = hr_process_evaluation
            hr_process_assessment_ef.hr_process_user = @hr_process_user
            hr_process_assessment_ef.resultado_puntos = resultado_evaluation
            hr_process_assessment_ef.resultado_porcentaje = porcentaje_evaluation
            hr_process_assessment_ef.save


            resultado_dimension += porcentaje_evaluation*hr_process_evaluation.hr_process_dimension_e.porcentaje

          end

        end

      end

      resultado_dimension = resultado_dimension/100

      hr_process_assessment_d = @hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

      unless hr_process_assessment_d

        hr_process_assessment_d = @hr_process_user.hr_process_assessment_ds.new
        hr_process_assessment_d.hr_process_dimension = hr_process_dimension

      end

      hr_process_assessment_d.resultado_porcentaje = resultado_dimension



      #hr_process_dimension_g_match = hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').where('valor_minimo <= ? AND valor_maximo >= ?', hr_process_assessment_d.resultado_porcentaje, hr_process_assessment_d.resultado_porcentaje).first


      hr_process_dimension_g_match = nil

      hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

        if hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

          hr_process_dimension_g_match = hr_process_dimension_g
          break

        end

      end

      hr_process_assessment_d.hr_process_dimension_g = hr_process_dimension_g_match if hr_process_dimension_g_match

      hr_process_assessment_d.fecha = lms_time

      hr_process_assessment_d.save

      hr_process_quadrant = @hr_process_user.quadrant

      if hr_process_quadrant
        qua = @hr_process_user.hr_process_assessment_qua
        unless qua
          qua = HrProcessAssessmentQua.new
          qua.hr_process_user = @hr_process_user
        end
        qua.fecha = lms_time
        qua.hr_process_quadrant = hr_process_quadrant
        qua.save

      end
    end


end
