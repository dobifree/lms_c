class HrProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @hr_processes = HrProcess.all

  end


  def show
    @hr_process = HrProcess.find(params[:id])

    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions if @hr_process_template

    @hr_process_managers = @hr_process.hr_process_managers_order_by_apellidos_nombre
    @hr_process_levels = @hr_process.hr_process_levels

    update_evaluations_from_plantilla


    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)


  end

  def new
    @hr_process = HrProcess.new
  end

  def edit
    @hr_process = HrProcess.find(params[:id])
  end

  def delete
    @hr_process = HrProcess.find(params[:id])
  end



  def create
    @hr_process = HrProcess.new(params[:hr_process])

    if @hr_process.save
      flash[:success] = t('activerecord.success.model.hr_process.create_ok')
      redirect_to @hr_process
    else
      render action: 'new'
    end
  end

  def update

    @hr_process = HrProcess.find(params[:id])

    if @hr_process.update_attributes(params[:hr_process])
      flash[:success] = t('activerecord.success.model.hr_process.update_ok')
      redirect_to @hr_process
    else
      render action: 'edit'
    end


  end



  def destroy

    @hr_process = HrProcess.find(params[:id])

    if verify_recaptcha

      if @hr_process.destroy
        flash[:success] = t('activerecord.success.model.hr_process.delete_ok')
        redirect_to hr_processes_url
      else
        flash[:danger] = t('activerecord.success.model.hr_process.delete_error')
        redirect_to delete_hr_process_path(@hr_process)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.hr_process.captcha_error')
      redirect_to delete_hr_process_path(@hr_process)

    end

  end

  def edit_template
    @hr_process = HrProcess.find(params[:id])
  end

  def update_template
    @hr_process = HrProcess.find(params[:id])

    if @hr_process.update_attributes(params[:hr_process])

      update_evaluations_from_plantilla


      flash[:success] = t('activerecord.success.model.hr_process.update_template_ok')
      redirect_to @hr_process
    else
      render action: 'edit_template'
    end

  end

  private

    def update_evaluations_from_plantilla

      if @hr_process.hr_process_template

        @hr_process.hr_process_evaluations.each do |hr_process_evaluation|
          hr_process_evaluation.vigente = false
          hr_process_evaluation.save

        end

        @hr_process.hr_process_template.hr_process_dimensions.each do |hr_p_d|

          hr_p_d.hr_process_dimension_es.each do |hr_p_d_e|

            hr_process_evaluation = @hr_process.hr_process_evaluations.find_by_hr_process_dimension_e_id(hr_p_d_e.id)

            if hr_process_evaluation

              hr_process_evaluation.vigente = true
              hr_process_evaluation.save

            else

              hr_process_evaluation = @hr_process.hr_process_evaluations.new
              hr_process_evaluation.hr_process_dimension_e = hr_p_d_e

              hr_process_evaluation.save

            end

          end

        end

      end

    end

end
