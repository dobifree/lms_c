class VacWarningExceptionsController < ApplicationController
  include VacationsModule

  before_filter :authenticate_user
  before_filter :verify_access_manage_module
  before_filter :get_group_user, only: %i[ create delete ]
  before_filter :get_exception, only: %i[ delete ]

  def index
    if params[:user_id]
      @user = User.find(params[:user_id])
    end
  end

  def match_user
    code = params[:user][:codigo]
    lastname = params[:user][:apellidos]
    name = params[:user][:nombre]
    active = params[:include_no_active] ? [0,1] : 1
    users = code.blank? && lastname.blank? && name.blank? ? [] : User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo IN (?)', '%'+code+'%', '%'+lastname+'%', '%'+name+'%', active).order('apellidos, nombre').all
    render partial: 'shared/users_search_datatable',
           locals: { users: users,
                     action_to_do: 4,
                     as_ajax: true }
  end

  def find_user_information
    @user = User.find(params[:user_id])
    @dashboard = Dashboard.new(@user, lms_time)
    render partial: 'user_information',
           locals: { user: @user }
  end

  def create
    new_exception = BpWarningException.new(bp_groups_user_id: @group_user.id,
                           max_usages: 1,
                           registered_at: lms_time,
                           registered_by_user_id: user_connected.id,
                           active_since: lms_time)
    new_exception.save

    render partial: 'warning_exceptions_datatable',
           locals: { new_exception: new_exception,
                     user: @group_user.user,
                     exceptions_to_list: BpWarningException.available_val(true).by_group_user(@group_user.id).order_by_registered,
                     editable: true }
  end

  def delete
    @exception.available = false
    @exception.unavailable_by_user_id = user_connected.id
    @exception.unavailable_at = lms_time
    @exception.save

    render partial: 'warning_exceptions_datatable',
           locals: { new_exception: @exception,
                     user: @group_user.user,
                     exceptions_to_list: BpWarningException.available_val(true).by_group_user(@exception.bp_groups_user_id).order_by_registered,
                     editable: true }

  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'vacs', true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def get_group_user
    if params[:user_id].blank?
      exception = BpWarningException.find(params[:bp_warning_exception_id])
      @group_user = BpGroupsUser.active?(lms_time, exception.bp_groups_user.user_id)
    else
      @group_user = BpGroupsUser.active?(lms_time, params[:user_id])
    end
  end

  def get_exception
    @exception = BpWarningException.find(params[:bp_warning_exception_id])
  end

end