class PeDimensionsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def edit
    @pe_dimension = PeDimension.find(params[:id])
    @pe_process = @pe_dimension.pe_process
  end

  def update
    @pe_dimension = PeDimension.find(params[:id])

    if @pe_dimension.update_attributes(params[:pe_dimension])
      redirect_to pe_process_tab_path @pe_dimension.pe_process, 'structure'
    else
      redirect_to pe_process_tab_path @pe_dimension.pe_process, 'structure'
    end

  end

end
