class UserCoursesController < ApplicationController

  include UserCoursesModule

  before_filter :authenticate_user
  
  before_filter :verify_owner_create, only: [:create]
  before_filter :verify_new_oportunity, only: [:create]

  before_filter :verify_owner_update, only: [:update, :update_automatic]
  before_filter :verify_apto_iniciar_especifico, only: [:update]

  before_filter :verify_owner_user_course, only: [:show, :create_comment, :download_file, :finish_unit, :set_milestone, :get_milestone,
                                                  :start_evaluation, :set_clock, :answer_evaluation, :unpause_evaluation, :start_poll, :answer_poll, :certificado]
  before_filter :verify_valid_unit_evaluation_poll_show, only: [:show, :finish_unit, :set_milestone, :get_milestone, :start_evaluation, :set_clock, :answer_evaluation, :start_poll, :answer_poll]
  before_filter :verify_access_unit_evaluation_poll, only: [:show, :finish_unit, :start_evaluation, :set_clock, :answer_evaluation, :start_poll, :answer_poll]

  before_filter :verify_configured_evaluation, only: [:start_evaluation, :set_clock, :answer_evaluation]

  
  
  def index

   #@company = session[:company]
    @enrollments = user_connected.enrollments
    session[:dashboard_program_tab] = params[:program_id].to_i if params[:program_id]

  end


  def show

    session[:dashboard_program_tab] = @user_course.program_course.program.id
    session[:dashboard_program_training_type_tab] = @user_course.course.training_type ? @user_course.course.training_type.id : ''
    session[:dashboard_program_year_tab] = @user_course.desde ? @user_course.desde.year : Time.now.year

   #@company = session[:company]
    @course_comment = CourseComment.new

    if @unit

      @user_course_unit = @user_course.user_course_units.find_by_unit_id(@unit.id)

      if @user_course_unit

        if @user_course_unit.inicio
          if @unit.virtual
            @user_course_unit.update_attributes(ultimo_acceso: lms_time)
            create_log 'Unidad '+@unit.numero.to_s, :ingreso
          end

        else
          if @unit.virtual
            @user_course_unit.update_attributes(inicio: lms_time, ultimo_acceso: lms_time)
            create_log 'Unidad '+@unit.numero.to_s, :pi
          end
        end

      else

        if @unit.virtual
          @user_course_unit = UserCourseUnit.new(user_course: @user_course, unit: @unit, inicio: lms_time, ultimo_acceso: lms_time) if @unit.virtual
          create_log 'Unidad '+@unit.numero.to_s, :pi
          @user_course_unit.save
        end

      end

    elsif @evaluation

      @user_course_evaluation = @user_course.user_course_evaluations.find_by_evaluation_id(@evaluation.id)

      if @user_course_evaluation && @user_course_evaluation.inicio

        @user_course_evaluation.update_attributes(ultimo_acceso: lms_time)
        create_log 'Evaluacion '+@evaluation.numero.to_s, :ingreso

      end

    elsif @poll

      @user_course_poll = @user_course.user_course_polls.find_by_poll_id(@poll.id)

      if @user_course_poll && @user_course_poll.inicio

        @user_course_poll.update_attributes(ultimo_acceso: lms_time)
        create_log 'Poll '+@poll.numero.to_s, :ingreso

      end

    else

      @access_to_course_menu = true

      last_accessed_unit = @user_course.user_course_units.reorder('ultimo_acceso DESC').first
      last_accessed_unit = nil if last_accessed_unit && (last_accessed_unit.inicio.nil? || last_accessed_unit.ultimo_acceso.nil?)

      last_accessed_evaluation = @user_course.user_course_evaluations.reorder('ultimo_acceso DESC').first
      last_accessed_evaluation = nil if last_accessed_evaluation && (last_accessed_evaluation.inicio.nil? || last_accessed_evaluation.ultimo_acceso.nil?)

      last_accessed_poll = @user_course.user_course_polls.reorder('ultimo_acceso DESC').first
      last_accessed_poll = nil if last_accessed_poll && (last_accessed_poll.inicio.nil? || last_accessed_poll.ultimo_acceso.nil?)


      unless last_accessed_unit
        last_accessed_unit = UserCourseUnit.new
        last_accessed_unit.ultimo_acceso = Time.at(0)
      end
      unless last_accessed_evaluation
        last_accessed_evaluation = UserCourseEvaluation.new
        last_accessed_evaluation.ultimo_acceso = Time.at(0)
      end
      unless last_accessed_poll
        last_accessed_poll = UserCoursePoll.new
        last_accessed_poll.ultimo_acceso = Time.at(0)
      end

      if last_accessed_unit.ultimo_acceso > last_accessed_evaluation.ultimo_acceso && last_accessed_unit.ultimo_acceso > last_accessed_poll.ultimo_acceso

        @unit = last_accessed_unit.unit
        @user_course_unit = last_accessed_unit

        create_log 'Unidad '+@unit.numero.to_s, :ingreso

      elsif last_accessed_evaluation.ultimo_acceso > last_accessed_unit.ultimo_acceso && last_accessed_evaluation.ultimo_acceso > last_accessed_poll.ultimo_acceso

        @evaluation = last_accessed_evaluation.evaluation
        @user_course_evaluation = @user_course.user_course_evaluations.find_by_evaluation_id(@evaluation.id)
        create_log 'Evaluacion '+@evaluation.numero.to_s, :ingreso

      elsif last_accessed_poll.ultimo_acceso > last_accessed_unit.ultimo_acceso && last_accessed_poll.ultimo_acceso > last_accessed_evaluation.ultimo_acceso

        @poll = last_accessed_poll.poll
        @user_course_poll = @user_course.user_course_polls.find_by_poll_id(@poll.id)
        create_log 'Poll '+@poll.numero.to_s, :ingreso

      else

        order_course_elements(@user_course.course, true)

        if @elements[0]

          ei = @elements_ids[0].split('-')
          tipo = ei[0]
          id =  ei[1]

          if tipo=='u'

            @unit = @elements[0]

            @user_course_unit = @user_course.user_course_units.find_by_unit_id(@unit.id)

            if @user_course_unit

              if @user_course_unit.inicio
                @user_course_unit.update_attributes(ultimo_acceso: lms_time)
                create_log 'Unidad '+@unit.numero.to_s, :ingreso
              else
                @user_course_unit.update_attributes(inicio: lms_time, ultimo_acceso: lms_time)
                create_log 'Unidad '+@unit.numero.to_s, :pi
              end

            else

              @user_course_unit = UserCourseUnit.new(user_course: @user_course, unit: @unit, inicio: lms_time, ultimo_acceso: lms_time)
              create_log 'Unidad '+@unit.numero.to_s, :pi
              @user_course_unit.save

            end

          elsif tipo=='e'
            @evaluation = @elements[0]
          elsif tipo=='p'
            @poll = @elements[0]
          end


        end

      end

    end

    if @unit
      finish_unit_auto @user_course_unit if @unit.virtual
      finish_course @user_course
    end

    render layout: 'flat' if $mobile_client && params[:unit_id]

  end

  def set_milestone

    if @unit

      @user_course_unit = @user_course.user_course_units.find_by_unit_id(@unit.id)

      if @user_course_unit
        @user_course_unit.milestone = params[:milestone]
        @user_course_unit.save
      end

    end

    render :nothing => true

  end

  def get_milestone

    r = 0

    if @unit

      @user_course_unit = @user_course.user_course_units.find_by_unit_id(@unit.id)

      r = @user_course_unit.milestone if @user_course_unit

    end

    render :text => r

  end

  # GET /user_courses/new
  # GET /user_courses/new.json
  def new
    @user_course = UserCourse.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_course }
    end
  end


  # GET /user_courses/1/edit
  def edit
    @user_course = UserCourse.find(params[:id])
  end

  def download_file

    @user_course = UserCourse.find(params[:id])
    @course_file = @user_course.course.course_files.find(params[:course_file_id])
   company = @company
    directory = company.directorio+'/'+company.codigo+'/material_apoyo/'
    send_file directory+@course_file.crypted_name+'.'+@course_file.extension,
              filename: @course_file.nombre+'.'+@course_file.extension,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def create

    params[:user_course][:enrollment_id] = @enrollment.id
    params[:user_course][:course_id] = @program_course.course_id

    params[:user_course][:numero_oportunidad] = @numero_oportunidad
    params[:user_course][:inicio] = lms_time
    params[:user_course][:iniciado] = true

    params[:user_course][:limite_tiempo] = false  

    if @program_course.course.duracion > 0 

      params[:user_course][:limite_tiempo] = true  
    
      params[:user_course][:hasta] = params[:user_course][:fin] = lms_time + @program_course.course.duracion.days
       
    end
    
    @user_course = UserCourse.new(params[:user_course])

    if @user_course.save
      create_log 'Curso', :inicio
      flash[:from] = @user_course.id
      if $mobile_client
        redirect_to @user_course
      else
        redirect_to user_courses_path
      end



    else
      redirect_to user_courses_path
    end

  end

  def update

    iniciado = true
    inicio = lms_time
    limite_tiempo = false
    fin = NIL

    if @user_course.course.duracion > 0 

      limite_tiempo = true
      fin = lms_time + @user_course.course.duracion.days
      fin = @user_course.hasta if fin > @user_course.hasta
       
    end

    if @user_course.update_attributes(iniciado: iniciado, inicio: inicio, limite_tiempo: limite_tiempo, fin: fin)
      create_log 'Curso', :inicio
      flash[:from] = @user_course.id
      if $mobile_client
        redirect_to @user_course
      else
        redirect_to user_courses_path
      end
    else
      flash[:danger] = t('activerecord.success.model.user_course.iniciar_error')
      user_courses_path
    end

  end

  def update_automatic

    iniciado = true
    inicio = lms_time
    limite_tiempo = false
    fin = NIL

    if @user_course.course.duracion > 0

      limite_tiempo = true
      fin = lms_time + @user_course.course.duracion.days
      fin = @user_course.hasta if fin > @user_course.hasta

    end

    if @user_course.update_attributes(iniciado: iniciado, inicio: inicio, limite_tiempo: limite_tiempo, fin: fin)
      create_log 'Curso', :inicio
      redirect_to root_path
    else
      flash[:danger] = t('activerecord.success.model.user_course.iniciar_error')
      root_path
    end

  end

  def create_comment

    @course_comment = CourseComment.new

    @course_comment.course = @user_course.course
    @course_comment.user = user_connected
    @course_comment.comentario = params[:course_comment][:comentario]
    @course_comment.fecha = lms_time

    if @course_comment.save
      flash[:success] = t('activerecord.success.model.course_comment.create_ok')
      flash[:from] = 'comment'
      redirect_to user_course_path @user_course#, opt: 'comment'
    else
      render 'show'
    end

  end

  def confirmacion_iniciar_abierto_mobile
    @enrollment = Enrollment.find(params[:enrollment_id])
    @program_course = ProgramCourse.find(params[:program_course_id])

  end

  def confirmacion_iniciar_especifico_mobile
    @user_course = UserCourse.find(params[:id])
  end


  def finish_unit

    @user_course_unit = @user_course.user_course_units.find_by_unit_id(@unit.id)

    if @user_course_unit.unit.virtual? && !@user_course_unit.finalizada

      @user_course_unit.update_attributes(fin: lms_time, finalizada: true)

      create_log 'Unidad '+@unit.numero.to_s, :fin

      update_percentage_advance(@user_course_unit.user_course) if @user_course_unit.finalizada

      finish_course @user_course

    end

    estado, estado_boton, user_course, user_courses = user_course_status(@user_course.enrollment, @user_course.program_course)

    render partial: '/user_courses/tab_curso_menu',
    locals: {
       course: @user_course.course,
       estado: estado
    }


  end

  def set_clock

    user_course_evaluation = @user_course.user_course_evaluations.find_by_evaluation_id(@evaluation.id)

    render partial: '/user_courses/set_clock',
           locals: {
             evaluation: @evaluation,
             user_course_evaluation: user_course_evaluation
           }

  end

  def start_evaluation

    if @evaluation.master_evaluation

      user_course_evaluation = @user_course.user_course_evaluations.find_by_evaluation_id(@evaluation.id)

      if user_course_evaluation.nil? || (user_course_evaluation && user_course_evaluation.inicio.nil? )

        estado = @user_course.estado lms_time

        if estado == :en_progreso

          if user_course_evaluation
            user_course_evaluation.update_attributes(inicio: lms_time, ultimo_acceso: lms_time)
          else
            user_course_evaluation = UserCourseEvaluation.new(user_course: @user_course, evaluation: @evaluation, inicio: lms_time, ultimo_acceso: lms_time)
          end

          if user_course_evaluation.save

            create_log 'Evaluacion '+@evaluation.numero.to_s, :inicio

            preguntas_restantes = @evaluation.numero_preguntas

            if @evaluation.master_evaluation.has_topics?

              @evaluation.evaluation_topics.each do |et|

                num_q_t = et.number_of_questions

                master_questions = et.master_evaluation_topic.master_evaluation_questions.where('obligatoria = ? AND disponible = ?', true, true)

                master_questions.each do |master_question|

                  if num_q_t > 0

                    preguntas_restantes -= 1
                    num_q_t -= 1

                    user_course_evaluation_question = user_course_evaluation.user_course_evaluation_questions.new
                    user_course_evaluation_question.master_evaluation = @evaluation.master_evaluation
                    user_course_evaluation_question.master_evaluation_question = master_question
                    user_course_evaluation_question.respondida = false
                    user_course_evaluation_question.numero = @evaluation.numero_preguntas - preguntas_restantes
                    user_course_evaluation_question.save

                    break if preguntas_restantes == 0 || num_q_t == 0

                  end

                end

                master_questions = et.master_evaluation_topic.master_evaluation_questions.where('obligatoria = ? AND disponible = ?', false, true)

                created_pregs = []

                while num_q_t > 0

                  next_post_preg = rand(master_questions.length) - 1
                  while created_pregs.include? next_post_preg

                    next_post_preg = rand(master_questions.length) - 1

                  end

                  created_pregs.push next_post_preg

                  preguntas_restantes -= 1
                  num_q_t -= 1

                  user_course_evaluation_question = user_course_evaluation.user_course_evaluation_questions.new
                  user_course_evaluation_question.master_evaluation = @evaluation.master_evaluation
                  user_course_evaluation_question.master_evaluation_question = master_questions[next_post_preg]
                  user_course_evaluation_question.respondida = false
                  user_course_evaluation_question.numero = @evaluation.numero_preguntas - preguntas_restantes
                  user_course_evaluation_question.save

                  break if preguntas_restantes == 0 || num_q_t == 0


                end

              end



            else

              master_questions = @evaluation.master_evaluation.master_evaluation_questions.where('obligatoria = ? AND disponible = ?', true, true)

              master_questions.each do |master_question|

                preguntas_restantes -= 1

                user_course_evaluation_question = user_course_evaluation.user_course_evaluation_questions.new
                user_course_evaluation_question.master_evaluation = @evaluation.master_evaluation
                user_course_evaluation_question.master_evaluation_question = master_question
                user_course_evaluation_question.respondida = false
                user_course_evaluation_question.numero = @evaluation.numero_preguntas - preguntas_restantes
                user_course_evaluation_question.save

                break if preguntas_restantes == 0

              end

              master_questions = @evaluation.master_evaluation.master_evaluation_questions.where('obligatoria = ? AND disponible = ?', false, true)

              created_pregs = []


              while preguntas_restantes > 0

                next_post_preg = rand(master_questions.length) - 1
                while created_pregs.include? next_post_preg

                  next_post_preg = rand(master_questions.length) - 1

                end

                created_pregs.push next_post_preg

                preguntas_restantes -= 1


                user_course_evaluation_question = user_course_evaluation.user_course_evaluation_questions.new
                user_course_evaluation_question.master_evaluation = @evaluation.master_evaluation
                user_course_evaluation_question.master_evaluation_question = master_questions[next_post_preg]
                user_course_evaluation_question.respondida = false
                user_course_evaluation_question.numero = @evaluation.numero_preguntas - preguntas_restantes
                user_course_evaluation_question.save


                break if preguntas_restantes == 0


              end

            end

          end

        end

      end

    end

    redirect_to user_course_evaluation_path @user_course, @evaluation

  end

  def answer_evaluation

    if @evaluation.configurada?

      @user_course_evaluation = @user_course.user_course_evaluations.find_by_evaluation_id(@evaluation.id)

      estado = @user_course.estado lms_time

      if @user_course_evaluation && !@user_course_evaluation.finalizada && estado == :en_progreso

        @user_course_evaluation.user_course_evaluation_questions.each do |user_course_evaluation_question|

          unless user_course_evaluation_question.respondida

            have_time = true

            if @evaluation.aplicar_tiempo

              if @evaluation.visualizacion_lote
                if @evaluation.tiempo
                  have_time = false if ((@evaluation.tiempo*60) - (lms_time - @user_course_evaluation.inicio)) <= 0
                end
              else

                if @evaluation.tiempo
                  have_time = false if ((@evaluation.tiempo*60) - (lms_time - @user_course_evaluation.inicio)) <= 0
                else
                  tiempo_queda = nil

                  tiempo_queda = (user_course_evaluation_question.master_evaluation_question.tiempo - (lms_time - user_course_evaluation_question.fecha_inicio)) if user_course_evaluation_question.master_evaluation_question.tiempo && user_course_evaluation_question.fecha_inicio

                  have_time = false if tiempo_queda && tiempo_queda <= 0
                end

              end

            end


            if params[:pregunta] && params[:pregunta][user_course_evaluation_question.id.to_s]

              if user_course_evaluation_question.master_evaluation_question.respuesta_unica

                alt = user_course_evaluation_question.master_evaluation_question.master_evaluation_alternatives.find_by_id(params[:pregunta][user_course_evaluation_question.id.to_s])

                user_course_evaluation_question.respondida = true
                user_course_evaluation_question.fecha_respuesta = lms_time
                user_course_evaluation_question.correcta = alt.correcta
                user_course_evaluation_question.alternativas = params[:pregunta][user_course_evaluation_question.id.to_s]

                user_course_evaluation_question.save

              else

                alts = user_course_evaluation_question.master_evaluation_question.master_evaluation_alternatives

                correcta = true

                alts.each do |alt|

                  if alt.correcta

                    correcta = false unless params[:pregunta][user_course_evaluation_question.id.to_s].include? alt.id.to_s

                  else

                    correcta = false unless !params[:pregunta][user_course_evaluation_question.id.to_s].include? alt.id.to_s

                  end

                end

                user_course_evaluation_question.respondida = true
                user_course_evaluation_question.fecha_respuesta = lms_time
                user_course_evaluation_question.correcta = correcta

                user_course_evaluation_question.alternativas = params[:pregunta][user_course_evaluation_question.id.to_s].join('-')

                user_course_evaluation_question.save

              end

            else

              unless have_time

                user_course_evaluation_question.respondida = true
                user_course_evaluation_question.fecha_respuesta = lms_time
                user_course_evaluation_question.correcta = false
                user_course_evaluation_question.alternativas = nil

                user_course_evaluation_question.save

              end

            end

            if user_course_evaluation_question.respondida && params.has_key?(:rp_button)

              @user_course_evaluation.paused = true

            end


          end

        end

        @user_course_evaluation.ultimo_acceso=lms_time
        @user_course_evaluation.save

        finish_evaluation(@user_course_evaluation)
        update_percentage_advance(@user_course)
        recien_finalizado, fue_aprobado, tiene_oportunidades = finish_course(@user_course)

        if recien_finalizado

          if fue_aprobado
            if @user_course.course.finish_msg_success.blank?
              flash[:success] = t('activerecord.success.model.user_course.finish_course_aprob')
            else
              flash[:success] = @user_course.course.finish_msg_success
            end
          elsif tiene_oportunidades
            if @user_course.course.finish_msg_failure_oportunity.blank?
              flash[:danger] = t('activerecord.success.model.user_course.finish_course_reprob_si_op')
            else
              flash[:danger] = @user_course.course.finish_msg_failure_oportunity
            end

          else
            if @user_course.course.finish_msg_failure.blank?
              flash[:danger] = t('activerecord.success.model.user_course.finish_course_reprob_no_op')
            else
              flash[:danger] = @user_course.course.finish_msg_failure
            end

          end


        end

      end

    end

    redirect_to user_course_evaluation_path @user_course, @evaluation

  end

  def unpause_evaluation

    @evaluation = Evaluation.find(params[:evaluation_id])

    @user_course_evaluation = @user_course.user_course_evaluations.find_by_evaluation_id(@evaluation.id)

    @user_course_evaluation.paused = false
    @user_course_evaluation.save

    redirect_to user_course_evaluation_path @user_course, @evaluation

  end


  def start_poll

    if @poll.master_poll

      user_course_poll = @user_course.user_course_polls.find_by_poll_id(@poll.id)

      if user_course_poll.nil? || (user_course_poll && user_course_poll.inicio.nil? )

        estado = @user_course.estado lms_time

        if estado == :en_progreso || @poll.tipo_satisfaccion

          if user_course_poll
            user_course_poll.update_attributes(inicio: lms_time, ultimo_acceso: lms_time)
          else
            user_course_poll = UserCoursePoll.new(user_course: @user_course, poll: @poll, inicio: lms_time, ultimo_acceso: lms_time)
          end

          if user_course_poll.save

            create_log 'Encuesta '+@poll.numero.to_s, :inicio

            master_questions = @poll.master_poll.master_poll_questions

            master_questions.each_with_index do |master_question, index|

              user_course_poll_question = user_course_poll.user_course_poll_questions.new
              user_course_poll_question.master_poll = @poll.master_poll
              user_course_poll_question.master_poll_question = master_question
              user_course_poll_question.respondida = false
              user_course_poll_question.numero = index+1
              user_course_poll_question.save

            end

          end

        end

      end

    end

    redirect_to user_course_poll_path @user_course, @poll

  end

  def answer_poll

    if @poll.master_poll

      @user_course_poll = @user_course.user_course_polls.find_by_poll_id(@poll.id)

      estado = @user_course.estado lms_time

      if @user_course_poll && !@user_course_poll.finalizada && ( estado == :en_progreso || @poll.tipo_satisfaccion)

        @user_course_poll.user_course_poll_questions.each do |user_course_poll_question|

          if user_course_poll_question.master_poll_question.answer_type == 2

            unless user_course_poll_question.respondida

              user_course_poll_question.fecha_respuesta = lms_time

              user_course_poll_question.texto = params['pregunta'][user_course_poll_question.id.to_s]

              if (user_course_poll_question.master_poll_question.required && !user_course_poll_question.texto.blank?) || !user_course_poll_question.master_poll_question.required
                user_course_poll_question.respondida = true

                poll_summary = ProgramCoursePollSummary.where('program_course_id = ? AND poll_id = ? AND master_poll_question_id = ? AND grupo = ? ',
                                                              @user_course.program_course.id, @poll.id, user_course_poll_question.master_poll_question.id,
                                                              @user_course.grupo).first

                if poll_summary
                  poll_summary.numero_respuestas += 1
                  poll_summary.save
                else

                  poll_summary = ProgramCoursePollSummary.new
                  poll_summary.program_course = @user_course.program_course
                  poll_summary.poll = @poll
                  poll_summary.master_poll_question = user_course_poll_question.master_poll_question
                  poll_summary.grupo = @user_course.grupo
                  poll_summary.program_course_instance = @user_course.program_course_instance if @user_course.program_course_instance
                  poll_summary.numero_respuestas = 1
                  poll_summary.save

                end

              else
                user_course_poll_question.respondida = false
              end

              user_course_poll_question.save

            end

          else

            unless user_course_poll_question.respondida

              if params[:pregunta] && params[:pregunta][user_course_poll_question.id.to_s]

                user_course_poll_question.respondida = true
                user_course_poll_question.fecha_respuesta = lms_time

                if user_course_poll_question.master_poll_question.respuesta_unica || user_course_poll_question.master_poll_question.answer_type == 0

                  user_course_poll_question.alternativas = params[:pregunta][user_course_poll_question.id.to_s]

                  poll_summary = ProgramCoursePollSummary.where('program_course_id = ? AND poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ? AND grupo = ? ',
                                                                @user_course.program_course.id, @poll.id, user_course_poll_question.master_poll_question.id,
                                                                params[:pregunta][user_course_poll_question.id.to_s],
                                                                @user_course.grupo).first

                  if poll_summary
                    poll_summary.numero_respuestas += 1
                    poll_summary.save
                  else

                    poll_summary = ProgramCoursePollSummary.new
                    poll_summary.program_course = @user_course.program_course
                    poll_summary.poll = @poll
                    poll_summary.master_poll_question = user_course_poll_question.master_poll_question
                    poll_summary.master_poll_alternative_id = params[:pregunta][user_course_poll_question.id.to_s]
                    poll_summary.grupo = @user_course.grupo
                    poll_summary.program_course_instance = @user_course.program_course_instance if @user_course.program_course_instance
                    poll_summary.numero_respuestas = 1
                    poll_summary.save

                  end

                else

                  user_course_poll_question.alternativas = params[:pregunta][user_course_poll_question.id.to_s].join('-')

                  params[:pregunta][user_course_poll_question.id.to_s].each do |alt_id|

                    poll_summary = ProgramCoursePollSummary.where('program_course_id = ? AND poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ? AND grupo = ? ',
                                                                  @user_course.program_course.id, @poll.id, user_course_poll_question.master_poll_question.id,
                                                                  alt_id,
                                                                  @user_course.grupo).first

                    if poll_summary
                      poll_summary.numero_respuestas += 1
                      poll_summary.save
                    else

                      poll_summary = ProgramCoursePollSummary.new
                      poll_summary.program_course = @user_course.program_course
                      poll_summary.poll = @poll
                      poll_summary.master_poll_question = user_course_poll_question.master_poll_question
                      poll_summary.master_poll_alternative_id = alt_id
                      poll_summary.grupo = @user_course.grupo
                      poll_summary.program_course_instance = @user_course.program_course_instance if @user_course.program_course_instance
                      poll_summary.numero_respuestas = 1
                      poll_summary.save

                    end


                  end

                end


                user_course_poll_question.save

              end


            end

          end

        end


        finish_poll(@user_course_poll)
        update_percentage_advance(@user_course)

        recien_finalizado, fue_aprobado, tiene_oportunidades = finish_course(@user_course)

        if recien_finalizado

          if fue_aprobado
            if @user_course.course.finish_msg_success.blank?
              flash[:success] = t('activerecord.success.model.user_course.finish_course_aprob')
            else
              flash[:success] = @user_course.course.finish_msg_success
            end
          elsif tiene_oportunidades
            if @user_course.course.finish_msg_failure_oportunity.blank?
              flash[:danger] = t('activerecord.success.model.user_course.finish_course_reprob_si_op')
            else
              flash[:danger] = @user_course.course.finish_msg_failure_oportunity
            end

          else
            if @user_course.course.finish_msg_failure.blank?
              flash[:danger] = t('activerecord.success.model.user_course.finish_course_reprob_no_op')
            else
              flash[:danger] = @user_course.course.finish_msg_failure
            end

          end

        end


      end

    end

    redirect_to user_course_poll_path @user_course, @poll

  end

  # DELETE /user_courses/1
  # DELETE /user_courses/1.json
  def destroy
    @user_course = UserCourse.find(params[:id])
    @user_course.destroy

    respond_to do |format|
      format.html { redirect_to user_courses_url }
      format.json { head :no_content }
    end
  end

  def certificado

   company = @company
    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_cursos/'
    plantilla = directory+(@user_course.course.course_certificates.last.template)

    if @user_course.finalizado && @user_course.aprobado

      cc = @user_course.course.course_certificates.where('rango_desde <= ? AND rango_hasta >= ?',@user_course.fin, @user_course.fin).first

      if cc && cc.template

        require 'prawn/measurement_extensions'


        plantilla = directory+cc.template+'.jpg'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.pdf'
        archivo_temporal = directorio_temporal+nombre_temporal

        nombre =  user_connected.nombre+' '+user_connected.apellidos

        nombre_curso =  @user_course.course.nombre


        nota = ''
        nota = format_nota(@user_course.nota).to_s if @user_course.nota

        desde = ''
        desde = localize(@user_course.desde, format: cc.desde_formato.to_sym) if @user_course.desde
        hasta = ''
        hasta = localize(@user_course.hasta, format: cc.hasta_formato.to_sym) if @user_course.hasta

        inicio = ''
        inicio = localize(@user_course.inicio, format: cc.inicio_formato.to_sym) if @user_course.inicio
        fin = ''
        fin = localize(@user_course.fin, format: cc.fin_formato.to_sym) if @user_course.fin

        fecha = @user_course.fin
        fecha = @user_course.hasta if @user_course.hasta && cc.fecha_source == 'fin_convocatoria'

        fecha = fecha+cc.fecha_source_delay.days
        fecha = localize(fecha, format: cc.fecha_formato.to_sym)


        Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: [720,540]) do |pdf|

          pdf.font_families.update('Foco Light' => {:normal => '/var/www/remote-storage/fonts/Foco_Std_Lt_0.ttf'})
          pdf.font_families.update('HaasGrotDisp Light' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-45Light_1.ttf'})
          pdf.font_families.update('HaasGrotDisp Normal' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-65Medium_1.ttf'})
          pdf.font_families.update('HaasGrotDisp Bold' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-75Bold_1.ttf'})

          pdf.start_new_page
          pdf.image plantilla, at: [-36,504], width: 720


          if cc.nombre
            if cc.nombre_x == -1
              pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :style => :normal, :size => cc.nombre_size} ], :at => [0, cc.nombre_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :style => :normal, :size => cc.nombre_size} ], :at => [cc.nombre_x.mm, cc.nombre_y.mm]
            end
          end

          if cc.nombre_curso

            if cc.nombre_curso_x == -1
              pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :style => :normal, :size => cc.nombre_curso_size} ], :at => [0, cc.nombre_curso_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :style => :normal, :size => cc.nombre_curso_size} ], :at => [cc.nombre_curso_x.mm, cc.nombre_curso_y.mm]
            end
          end

          if cc.fecha
            if cc.fecha_x == -1
              pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :center
            elsif cc.fecha_x == -2
              pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :right
            else
              pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [cc.fecha_x.mm, cc.fecha_y.mm]
            end
          end

          if cc.desde
            if cc.desde_x == -1
              pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :style => :normal, :size => cc.desde_size} ], :at => [0, cc.desde_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :style => :normal, :size => cc.desde_size} ], :at => [cc.desde_x.mm, cc.desde_y.mm]
            end
          end

          if cc.hasta
            if cc.hasta_x == -1
              pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :style => :normal, :size => cc.hasta_size} ], :at => [0, cc.hasta_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :style => :normal, :size => cc.hasta_size} ], :at => [cc.hasta_x.mm, cc.hasta_y.mm]
            end
          end

          if cc.inicio
            if cc.inicio_x == -1
              pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :style => :normal, :size => cc.inicio_size} ], :at => [0, cc.inicio_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :style => :normal, :size => cc.inicio_size} ], :at => [cc.inicio_x.mm, cc.inicio_y.mm]
            end
          end

          if cc.fin
            if cc.fin_x == -1
              pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :style => :normal, :size => cc.fin_size} ], :at => [0, cc.fin_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :style => :normal, :size => cc.fin_size} ], :at => [cc.fin_x.mm, cc.fin_y.mm]
            end
          end

          if cc.nota
            if cc.nota_x == -1
              pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :style => :normal, :size => cc.nota_size} ], :at => [0, cc.nota_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :style => :normal, :size => cc.nota_size} ], :at => [cc.nota_x.mm, cc.nota_y.mm]
            end
          end


        end

        send_file archivo_temporal,
                  filename: 'Certificado - '+@user_course.course.nombre+'.pdf',
                  type: 'application/octet-stream',
                  disposition: 'attachment'

      end

    else

      send_file plantilla,
                filename: 'Certificado - '+@user_course.course.nombre+'.pdf',
                type: 'application/octet-stream',
                disposition: 'attachment'

    end

  end

  private

    def verify_owner_create

      @enrollment = Enrollment.find(params[:user_course][:enrollment_id])

      if @enrollment.user.id != user_connected.id
        flash[:danger] = t('security.no_access_user_course')
        redirect_to root_path
      end

    end

    def verify_owner_update

      @user_course = UserCourse.find(params[:id])

      if @user_course && @user_course.enrollment.user.id != user_connected.id
        flash[:danger] = t('security.no_access_user_course')
        redirect_to root_path
      end

    end

    def verify_new_oportunity

      @program_course = ProgramCourse.find(params[:user_course][:program_course_id])
      
      user_course = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND anulado = ? ', @enrollment.id, @program_course.id, false).order('numero_oportunidad DESC').first

      @numero_oportunidad = 1
      en_progreso = false
      ya_aprobo = false
      abandonado = false

      if user_course

        @numero_oportunidad = user_course.numero_oportunidad+1
        en_progreso = true if user_course.iniciado && !user_course.finalizado
        ya_aprobo = user_course.aprobado
        abandonado = true if en_progreso && user_course.limite_tiempo && user_course.fin < lms_time

      end

      unless @numero_oportunidad <= @program_course.course.numero_oportunidades && 
        (!en_progreso || (en_progreso && abandonado) )&& !ya_aprobo

        flash[:danger] = t('security.no_access_user_course')
        redirect_to root_path

      end

    end

    def verify_apto_iniciar_especifico

      if @user_course.finalizado || verify_plazo_vencido_especifico
        flash[:danger] = t('security.no_access_user_course')
        redirect_to root_path
      end

    end

    def verify_plazo_vencido_especifico

      fecha_limite = @user_course.hasta

      if @user_course.iniciado && @user_course.limite_tiempo

        fecha_limite = @user_course.fin

        if fecha_limite < lms_time
          return true
        else
          return false
        end

      else

        if fecha_limite < lms_time
          return true
        else
          return false
        end

      end



    end

    def verify_plazo_vencido_abierto

      if @user_course.limite_tiempo && @user_course.fin < lms_time
        return true
      else
        return false
      end

    end

    def verify_owner_user_course

      @user_course = UserCourse.find(params[:id])

      if @user_course && @user_course.enrollment.user.id != user_connected.id
        flash[:danger] = t('security.no_access_user_course')
        redirect_to root_path
      end

    end

    def verify_valid_unit_evaluation_poll_show

      if params[:unit_id]

        @unit = Unit.find(params[:unit_id])

        unless @user_course.course.units.include? @unit

          flash[:danger] = t('security.invalid_unit_course')
          redirect_to root_path

        end

      end

      if params[:evaluation_id]

        @evaluation = Evaluation.find(params[:evaluation_id])

        unless @user_course.course.evaluations.include? @evaluation

          flash[:danger] = t('security.invalid_evaluation_course')
          redirect_to root_path

        end

      end

      if params[:poll_id]

        @poll = Poll.find(params[:poll_id])

        unless @user_course.course.polls.include? @poll

          flash[:danger] = t('security.invalid_poll_course')
          redirect_to root_path

        end

      end

    end

    def verify_access_unit_evaluation_poll

      if @unit

        acceso, motivos = verify_access_to_unit @user_course, @unit

        unless acceso

          flash[:danger] = t('security.no_access_unit_course')
          redirect_to root_path

        end

      end

      if @evaluation

        acceso, motivos = verify_access_to_evaluation @user_course, @evaluation

        unless acceso

          flash[:danger] = t('security.no_access_evaluation_course')
          redirect_to root_path

        end

      end

      if @poll

        acceso, motivos = verify_access_to_poll @user_course, @poll

        unless acceso

          flash[:danger] = t('security.no_access_poll_course')
          redirect_to root_path

        end

      end

    end

    def verify_configured_evaluation

      unless @evaluation.configurada?

        flash[:danger] = t('security.no_access_evaluation_course')
        redirect_to @user_course

      end

    end

    def finish_unit_auto(user_course_unit)

      if user_course_unit.unit.finaliza_automaticamente && !user_course_unit.finalizada

        user_course_unit.update_attributes(fin: lms_time, finalizada: true)

        create_log 'Unidad '+@unit.numero.to_s, :fin

        update_percentage_advance(user_course_unit.user_course) if user_course_unit.finalizada


      end


    end

    def finish_evaluation(user_course_evaluation)

      total_pregs = user_course_evaluation.user_course_evaluation_questions.count

      total_pregs_resps = user_course_evaluation.user_course_evaluation_questions.find_all_by_respondida(true).count

      if total_pregs == total_pregs_resps

        user_course_evaluation.finalizada = true
        user_course_evaluation.fin = lms_time

        puntaje = 0

        user_course_evaluation.user_course_evaluation_questions.find_all_by_correcta(true).each do |q|

          puntaje += q.master_evaluation_question.puntaje

        end

        user_course_evaluation.nota = puntaje + user_course_evaluation.evaluation.nota_base

        if user_course_evaluation.user_course.course.calculate_percentage

          puntaje_total = 0

          user_course_evaluation.user_course_evaluation_questions.each do |q|

            puntaje_total += q.master_evaluation_question.puntaje

          end

          user_course_evaluation.nota = user_course_evaluation.nota*100/puntaje_total.to_f

        end

        if user_course_evaluation.nota >= user_course_evaluation.evaluation.course.nota_minima
          user_course_evaluation.aprobada = true
        else
          user_course_evaluation.aprobada = false
        end

        user_course_evaluation.save

        create_log 'Evaluacion '+user_course_evaluation.evaluation.numero.to_s, :fin

      end


    end

    def finish_poll(user_course_poll)

      total_pregs = user_course_poll.user_course_poll_questions.count

      total_pregs_resps = user_course_poll.user_course_poll_questions.find_all_by_respondida(true).count

      if total_pregs == total_pregs_resps

        user_course_poll.finalizada = true
        user_course_poll.fin = lms_time
        user_course_poll.save

        create_log 'Poll '+user_course_poll.poll.numero.to_s, :fin

      end


    end



    def create_log(elemento, accion)

      unless user_connected_bd?

        acciones = {inicio: 'Inicio', pi: 'Primer ingreso', ingreso: 'Ingreso', fin: 'Fin'}

        log = LogUserCourse.new

        log.user_course = @user_course
        log.fecha = lms_time
        log.elemento = elemento
        log.accion = acciones[accion]

        log.save

      end

    end


    

end
