class HolidaysController < ApplicationController
  include HolidaysHelper
  include HolidayModule

  before_filter :authenticate_user
  before_filter :holiday_manager
  before_filter :active_module
  before_filter :get_holiday_by_id, only: %i[ delete_global_date manage edit update]
  before_filter :verify_active_holiday, only: %i[ delete_global_date manage edit update]

  def index
    flash[:info] = t('views.holiday_module.flash_messages.impact_module')
    @tab = params[:tab] ? params[:tab] : 0
    @holidays = Holiday.active
  end

  def new_global_date
    @holiday = Holiday.new(hol_type: Holiday.global_type)
  end

  def create_global_date
    holiday = Holiday.new(params[:holiday])
    holiday.hol_type = Holiday.global_type
    holiday = set_registered holiday
    if holiday.save
      flash[:success] = t('views.holiday_module.flash_messages.create_success')
      redirect_to holiday_index_path(t('views.holiday_module.tabs.manager_index.global'))
    else
      @holiday = holiday
      render action: "new"
    end
  end

  def delete_global_date
    @holiday = deactivate @holiday
    if @holiday.save
      assocs = @holiday.active_users_assoc
      assocs.each { |assoc| deactivate(assoc).save }

      conditions = @holiday.active_chars
      conditions.each { |cond| deactivate(cond).save }
      flash[:success] = t('views.holiday_module.flash_messages.delete_success')
    else
      flash[:danger] = t('views.holiday_module.flash_messages.delete_danger')
    end
    redirect_to holiday_index_path(t('views.holiday_module.tabs.manager_index.global'))
  end

  def manage; end
  def edit; end
  def update
    @holiday.description = params[:holiday][:description] if params[:holiday][:description]
    @holiday.name = params[:holiday][:name] if params[:holiday][:name]

    if @holiday.save
      flash[:success] = t('views.holiday_module.flash_messages.change_success')
      if @holiday.global?
        redirect_to holiday_index_path(t('views.holiday_module.tabs.manager_index.global'))
      else
       redirect_to holiday_manage_path(holiday)
      end
    else
      @holiday = holiday
      render action: "edit"
    end
  end

end
