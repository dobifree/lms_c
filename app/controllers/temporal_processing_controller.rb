class TemporalProcessingController < ApplicationController

  include ExcelReportsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def puestos_visanet

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    file_name = '/var/www/remote-storage/batch_processing/puestos_visanet1.xlsx'

    if File.exist? file_name

      archivo = RubyXL::Parser.parse file_name

      archivo.worksheets[0].extract_data.each_with_index do |fila, index|

        next if index <= 1

        if fila[0] #nombre puesto

          j_job = JJob.new
          j_job.name = fila[0]
          j_job.version = 1
          j_job.from_date = lms_time



          if j_job.save

            #1 Área
            id_c_area = 1

            jj_characteristic_area = JjCharacteristic.find id_c_area
            jj_characteristic_area_v = jj_characteristic_area.find_jj_characteristic_value_string fila[1]

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_area
            j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_area_v
            j_job_jj_characteristic.save


            #2 Sub Área
            id_c_sub_area = 2

            jj_characteristic_sub_area = JjCharacteristic.find id_c_sub_area
            jj_characteristic_sub_area_v = jj_characteristic_sub_area.find_jj_characteristic_value_string fila[2]

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_sub_area
            j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_sub_area_v
            j_job_jj_characteristic.save


            #3 Supervisado por
            id_supervisado_por = 6

            jj_characteristic_supervisado_por = JjCharacteristic.find id_supervisado_por

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_supervisado_por
            j_job_jj_characteristic.value_string = fila[3]
            j_job_jj_characteristic.save

            #9 objetivo
            id_objetivo = 11

            jj_characteristic_objetivo = JjCharacteristic.find id_objetivo

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_objetivo
            j_job_jj_characteristic.value_text = fila[9]
            j_job_jj_characteristic.save

            #10 Consecuencia del error

            id_c_con_error = 96

            jj_characteristic_con_error = JjCharacteristic.find id_c_con_error
            jj_characteristic_con_error_v = jj_characteristic_con_error.find_jj_characteristic_value_string fila[10]

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_con_error
            j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_con_error_v
            j_job_jj_characteristic.save

            #11 Nivel de supervisión recibida

            id_c_nivel_sup_req = 103

            jj_characteristic_nivel_sup_req = JjCharacteristic.find id_c_nivel_sup_req
            jj_characteristic_nivel_sup_req_v = jj_characteristic_nivel_sup_req.find_jj_characteristic_value_string fila[11]

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_nivel_sup_req
            j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_nivel_sup_req_v
            j_job_jj_characteristic.save


            #12 Nivel educativo

            id_c_nivel_edu = 48

            jj_characteristic_nivel_edu = JjCharacteristic.find id_c_nivel_edu
            jj_characteristic_nivel_edu_v = jj_characteristic_nivel_edu.find_jj_characteristic_value_string fila[12]

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_nivel_edu
            j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_nivel_edu_v
            j_job_jj_characteristic.save


            #13 Carreras

            id_carreras = 50

            jj_characteristic_carreras = JjCharacteristic.find id_carreras

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_carreras
            j_job_jj_characteristic.value_string = fila[13]
            j_job_jj_characteristic.save

            #14 Especialización requerida para realizar funciones (detallar)rearea
            id_espec_req = 52

            jj_characteristic_espec_req = JjCharacteristic.find id_espec_req

            j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
            j_job_jj_characteristic.jj_characteristic =jj_characteristic_espec_req
            j_job_jj_characteristic.value_text = fila[14]
            j_job_jj_characteristic.save


          else
            @lineas_error.push index+1
            @lineas_error_detalle.push (fila[0]).force_encoding('UTF-8')
            @lineas_error_messages.push j_job.errors.full_messages
          end


        end

      end

      #funciones
      archivo.worksheets[1].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job


          id_c_function_p = 12

          jj_characteristic_function = JjCharacteristic.find id_c_function_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_function
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 Función
          id_c_function = 13

          jj_characteristic_function = JjCharacteristic.find id_c_function

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_function
          j_job_jj_characteristic.value_text = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

        end


      end

      #decisiones
      archivo.worksheets[2].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job


          id_c_decision_p = 104

          jj_characteristic_decision = JjCharacteristic.find id_c_decision_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_decision
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 Función
          id_c_decision = 15

          jj_characteristic_decision = JjCharacteristic.find id_c_decision

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_decision
          j_job_jj_characteristic.value_text = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

        end


      end

      #resp info
      archivo.worksheets[3].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job

          id_c_resp_p = 16

          jj_characteristic_resp = JjCharacteristic.find id_c_resp_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_resp
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 Informe
          id_c_informe = 17

          jj_characteristic_informe = JjCharacteristic.find id_c_informe

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_informe
          j_job_jj_characteristic.value_string = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #2 Area
          id_c_area = 18

          jj_characteristic_area = JjCharacteristic.find id_c_area

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_area
          j_job_jj_characteristic.value_string = fila[2]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #3 Accion
          id_c_accion = 19

          jj_characteristic_accion = JjCharacteristic.find id_c_accion
          jj_characteristic_accion_v = jj_characteristic_accion.find_jj_characteristic_value_string fila[3]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_accion
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_accion_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #4 Frecuencia
          id_c_freq = 20

          jj_characteristic_freq = JjCharacteristic.find id_c_freq
          jj_characteristic_freq_v = jj_characteristic_freq.find_jj_characteristic_value_string fila[4]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_freq
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_freq_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #5 Conf
          id_c_conf = 21

          jj_characteristic_conf = JjCharacteristic.find id_c_conf
          jj_characteristic_conf_v = jj_characteristic_conf.find_jj_characteristic_value_string fila[5]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_conf
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_conf_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

        end


      end

      #resp sup
      archivo.worksheets[4].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job

          id_c_resp_p = 29

          jj_characteristic_resp = JjCharacteristic.find id_c_resp_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_resp
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 puesto
          id_c_puesto = 30

          jj_characteristic_puesto = JjCharacteristic.find id_c_puesto

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_puesto
          j_job_jj_characteristic.value_string = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #2 tipo
          id_c_tipo = 31

          jj_characteristic_tipo = JjCharacteristic.find id_c_tipo
          jj_characteristic_tipo_v = jj_characteristic_tipo.find_jj_characteristic_value_string fila[2]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_tipo
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_tipo_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #3 num
          id_c_num = 33

          jj_characteristic_num = JjCharacteristic.find id_c_num
          jj_characteristic_num_v = jj_characteristic_num.find_jj_characteristic_value_string fila[3]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_num
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic_num_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save


        end


      end

      #resp ext
      archivo.worksheets[5].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job

          id_c_resp_p = 34

          jj_characteristic_resp = JjCharacteristic.find id_c_resp_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_resp
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 entidad
          id_c_entidad = 35

          jj_characteristic_entidad = JjCharacteristic.find id_c_entidad

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_entidad
          j_job_jj_characteristic.value_string = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #2 asunto
          id_c_asunto = 36

          jj_characteristic_asunto = JjCharacteristic.find id_c_asunto

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_asunto
          j_job_jj_characteristic.value_string = fila[2]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #3 jerarq
          id_c_jer = 37

          jj_characteristic_jer = JjCharacteristic.find id_c_jer

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_jer
          j_job_jj_characteristic.value_string = fila[3]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save


          #4 freq
          id_c__freq = 38

          jj_characteristic__freq = JjCharacteristic.find id_c__freq
          jj_characteristic__freq_v = jj_characteristic__freq.find_jj_characteristic_value_string fila[4]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic__freq
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic__freq_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save



        end


      end


      #resp int
      archivo.worksheets[6].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job

          id_c_resp_p = 39

          jj_characteristic_resp = JjCharacteristic.find id_c_resp_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_resp
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 entidad
          id_c_entidad = 40

          jj_characteristic_entidad = JjCharacteristic.find id_c_entidad

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_entidad
          j_job_jj_characteristic.value_string = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #2 asunto
          id_c_asunto = 41

          jj_characteristic_asunto = JjCharacteristic.find id_c_asunto

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_asunto
          j_job_jj_characteristic.value_string = fila[2]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #3 jerarq
          id_c_jer = 42

          jj_characteristic_jer = JjCharacteristic.find id_c_jer

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_jer
          j_job_jj_characteristic.value_string = fila[3]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save


          #4 freq
          id_c__freq = 43

          jj_characteristic__freq = JjCharacteristic.find id_c__freq
          jj_characteristic__freq_v = jj_characteristic__freq.find_jj_characteristic_value_string fila[4]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic__freq
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic__freq_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save



        end


      end

      #con_inf
      archivo.worksheets[7].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job

          id_c_resp_p = 53

          jj_characteristic_resp = JjCharacteristic.find id_c_resp_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_resp
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 prog
          id_c_prog = 54

          jj_characteristic_prog = JjCharacteristic.find id_c_prog

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_prog
          j_job_jj_characteristic.value_string = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #2 nivel
          id_c__nivel = 55

          jj_characteristic__nivel = JjCharacteristic.find id_c__nivel
          jj_characteristic__nivel_v = jj_characteristic__nivel.find_jj_characteristic_value_string fila[2]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic__nivel
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic__nivel_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save



        end


      end

      #con_idiomas
      archivo.worksheets[8].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job

          id_c_resp_p = 56

          jj_characteristic_resp = JjCharacteristic.find id_c_resp_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_resp
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 idio
          id_c_idio = 91

          jj_characteristic_idio = JjCharacteristic.find id_c_idio

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_idio
          j_job_jj_characteristic.value_string = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #2 nivel
          id_c__nivel = 92

          jj_characteristic__nivel = JjCharacteristic.find id_c__nivel
          jj_characteristic__nivel_v = jj_characteristic__nivel.find_jj_characteristic_value_string fila[2]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic__nivel
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic__nivel_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save



        end


      end

      #exp
      archivo.worksheets[9].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job

          id_c_resp_p = 105

          jj_characteristic_resp = JjCharacteristic.find id_c_resp_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_resp
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 exp
          id_c_exp = 106

          jj_characteristic_exp = JjCharacteristic.find id_c_exp

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_exp
          j_job_jj_characteristic.value_text = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          
        end


      end

      #comp
      archivo.worksheets[10].extract_data.each_with_index do |fila, index|

        next if index == 0

        j_job = JJob.find_by_name fila[0]

        if j_job

          id_c_resp_p = 59

          jj_characteristic_resp = JjCharacteristic.find id_c_resp_p

          j_job_jj_characteristic_p = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic_p.jj_characteristic = jj_characteristic_resp
          j_job_jj_characteristic_p.value_string = '-'
          j_job_jj_characteristic_p.save

          #1 idio
          id_c_comp = 93

          jj_characteristic_comp = JjCharacteristic.find id_c_comp

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic_comp
          j_job_jj_characteristic.value_string = fila[1]
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save

          #2 nivel
          id_c__nivel = 94

          jj_characteristic__nivel = JjCharacteristic.find id_c__nivel
          jj_characteristic__nivel_v = jj_characteristic__nivel.find_jj_characteristic_value_string fila[2]

          j_job_jj_characteristic = j_job.j_job_jj_characteristics.build
          j_job_jj_characteristic.jj_characteristic =jj_characteristic__nivel
          j_job_jj_characteristic.jj_characteristic_value = jj_characteristic__nivel_v
          j_job_jj_characteristic.register_job_jj_characteristic = j_job_jj_characteristic_p
          j_job_jj_characteristic.save



        end


      end

    end


  end


end
