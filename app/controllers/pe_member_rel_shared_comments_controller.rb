class PeMemberRelSharedCommentsController < ApplicationController

  before_filter :get_data_1, only: [:create]
  before_filter :get_data_2, only: [:destroy]
  before_filter :validate_open_process, only: [:create, :destroy]
  before_filter :validate_create_comment, only: [:create]
  before_filter :validate_destroy_comment, only: [:destroy]

  def create

    pe_member_rel_shared_comment = @pe_member_rel.pe_member_rel_shared_comments.build

    pe_member_rel_shared_comment.comment = params[:pe_member_rel_shared_comment][:comment]
    pe_member_rel_shared_comment.registered_at = lms_time

    if pe_member_rel_shared_comment.save
      flash[:success] = t('activerecord.success.model.pe_comment.create_ok')
    else
      pe_member_rel_shared_comment.errors.full_messages.each do |msg|
			  flash[:danger] = msg
        break
			end
    end

    if params[:f] == 'd'
      redirect_to pe_def_by_user_show_path @pe_evaluation, @pe_member_rel
    elsif params[:f] == 't'
      redirect_to pe_tracking_show_path @pe_evaluation, @pe_member_rel
    elsif params[:f] == 'a'
      redirect_to pe_assessment_show_2_path @pe_member_rel
    end

  end


  def destroy

    @pe_member_rel_shared_comment.destroy

    flash[:success] = t('activerecord.success.model.pe_comment.delete_ok')

    if params[:f] == 'd'
      redirect_to pe_def_by_user_show_path @pe_evaluation, @pe_member_rel
    elsif params[:f] == 't'
      redirect_to pe_tracking_show_path @pe_evaluation, @pe_member_rel
    elsif params[:f] == 'a'
      redirect_to pe_assessment_show_2_path @pe_member_rel
    end

  end

  private

  def get_data_1

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id]) if params[:pe_evaluation_id]
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator
    @pe_process = @pe_member_rel.pe_process

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
        break
      end

    end

  end

  def get_data_2

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id]) if params[:pe_evaluation_id]
    @pe_member_rel_shared_comment = PeMemberRelSharedComment.find(params[:pe_member_rel_shared_comment_id])
    @pe_member_rel = @pe_member_rel_shared_comment.pe_member_rel
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator
    @pe_process = @pe_member_rel.pe_process

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
        break
      end

    end

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_create_comment


    unless @pe_member_rel.pe_rel.allows_shared_comments? && @pe_member_connected && @pe_member_evaluator.id == @pe_member_connected.id
      flash[:danger] = t('activerecord.error.model.pe_comment.cant_comment')
      redirect_to root_path
    end

  end

  def validate_destroy_comment

    unless @pe_member_rel_shared_comment.pe_member_rel_id == @pe_member_rel.id && @pe_member_connected && @pe_member_evaluator.id == @pe_member_connected.id
      flash[:danger] = t('activerecord.error.model.pe_comment.cant_destroy_comment')
      redirect_to root_path
    end

  end


end
