class VacManagerController < ApplicationController
  include VacManagerHelper
  include VacRecordsRegisterHelper
  include VacationsModule

  before_filter :authenticate_user
  before_filter :verify_access_manage_module
  before_filter :only_active_request, only: [:request_manage,
                                             :request_edit,
                                             :request_update]

  #before_filter :verify_closable, only: [:request_edit, :request_update]

  def deactivated_description_modal
    request = VacRequest.find(params[:vac_request_id])
    render partial: 'deactivate_description_modal',
           locals: { request: request,
                     required: true }
  end

  def deactivate_request_manager
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)
    params[:vac_request_id] = request.id

    type_generic_message = 'danger'
    generic_message = t('views.vac_requests.flash_messages.danger_changed')

    request_status = 5
    status_description = params[:vac_request][:status_description]

    params = {status: request_status,
              status_description: status_description,
              status_changed_by_user_id: user_connected.id}

    updated_request = complete_update(request.id, params)
    updated_request.deactivated_at = lms_time
    updated_request.deactivated_by_user_id = user_connected.id
    updated_request.active = false
    if updated_request.valid? && request.active && !status_description.blank? && !request.closed?
      updated_request.save
      benefits = VacRuleRecord.where(active: true, vac_request_id: request.id)
      benefits.each do |benefit|
        new_benefit = benefit.dup
        new_benefit.vac_request_id = updated_request.id
        new_benefit.save
      end
      prev_request = VacRequest.find(request.id)
      prev_request.next_request_id = updated_request.id
      prev_request = deactivate_request(request.id)
      prev_request.save

      type_generic_message = 'success'
      generic_message = t('views.vac_requests.flash_messages.success_changed')
      request = updated_request
    end

    render partial: 'vac_warning_exceptions/requests_accordion', locals: { user: request.user,
                                                                           type_generic_message: type_generic_message,
                                                                           generic_message: generic_message,
                                                                           show_generic_message: true,
                                                                           show_this_request: request.id,
                                                                           show_historial: true }
  end

  def scheme_calculus
    user_id = params[:user_id]
    vac_accum = VacAccumulated.user_latest(user_id)

    legal_last_accu = VacAccumulated.user_latest_legal(user_id)
    legal_last_accu = VacAccumulated.user_first(user_id) unless legal_last_accu

    prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
    prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated

    legal_future = future_days(legal_last_accu.up_to_date, params[:date_end], user_id) + vac_accum.accumulated_real_days - VacRequest.sum_days_by_status(user_id, VacRequest.approved)
    progressive = next_progressive_days(prev_auto_accumulated.up_to_date, params[:date_end], user_id) - VacRequest.sum_progressive_days_by_status(user_id, VacRequest.status_in_process).to_i

    legal_future = number_with_precision(legal_future, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
    progressive = number_with_precision(progressive, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

    render json: [legal_future, progressive]
  end

  def scheme_days
    render partial: 'vac_records_register/scheme_days_modal',
           locals: { user: User.find(params[:user_id]),
                     as_manager: true }
  end

  def manual_requests_simple_list
     render partial: 'manual_requests_simple_list',
            locals: {requests: VacRequest.all_historical.by_user(params[:user_id]) }
  end

  def find_users_search
    code = params[:user][:codigo]
    lastname = params[:user][:apellidos]
    name = params[:user][:nombre]
    active = params[:include_no_active] ? [0,1] : 1
    users = code.blank? && lastname.blank? && name.blank? ? [] : User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo IN (?)', '%'+code+'%', '%'+lastname+'%', '%'+name+'%', active).order('apellidos, nombre').all
    render partial: 'shared/users_search_datatable',
           locals: { users: users,
                     action_to_do: 0,
                     as_ajax: true }
  end

  def users_search_paid_yn_benefits
    code = params[:user][:codigo]
    lastname = params[:user][:apellidos]
    name = params[:user][:nombre]
    active = params[:include_no_active] ? [0,1] : 1
    rules = VacRuleRecord.where(paid: [false, true], active: true).joins(:vac_request).where(vac_requests: {active: true, historical: false,registered_manually: false, status: [VacRequest.approved, VacRequest.closed]})
    users_id = []
    rules.each {|rule| users_id << rule.vac_request.user_id }
    users = code.blank? && lastname.blank? && name.blank? ? [] : User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo IN (?)', '%'+code+'%', '%'+lastname+'%', '%'+name+'%', active).where(id: users_id).order('apellidos, nombre').all

    render partial: 'shared/users_search_datatable',
           locals: { users: users,
                     action_to_do: 6,
                     as_ajax: true }
  end

  def paid_yn_benefits_for
    render partial: 'vac_records_register/vac_rule_records_datatable',
                           locals: { show_link: true,
                                     manager: true,
                                     show_users: true,
                                     show_bonus_status: true,
                                     show_vacations: true,
                                     show_vacations_status: true,
                                     id_datatable: 'paid_and_not_bonus_datatable',
                                     vac_rule_records: VacRuleRecord.where(paid: [false, true], active: true).joins(:vac_request).where(vac_requests: {user_id: params[:user_id], active: true, historical: false,registered_manually: false, status: [VacRequest.approved, VacRequest.closed]})}
  end


  def users_search_unpaid_benefits
    code = params[:user][:codigo]
    lastname = params[:user][:apellidos]
    name = params[:user][:nombre]
    active = params[:include_no_active] ? [0,1] : 1
    rules = VacRuleRecord.where(paid: nil, active: true).joins(:vac_request => :user).where(vac_requests: {active: true, historical: false,registered_manually: false, status: [VacRequest.approved, VacRequest.closed]})
    users_id = []
    rules.each {|rule| users_id << rule.vac_request.user_id }
    users = code.blank? && lastname.blank? && name.blank? ? [] : User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo IN (?)', '%'+code+'%', '%'+lastname+'%', '%'+name+'%', active).where(id: users_id).order('apellidos, nombre').all

    render partial: 'shared/users_search_datatable',
           locals: { users: users,
                     action_to_do: 5,
                     as_ajax: true }
  end

  def unpaid_benefits_for
    render partial: 'vac_records_register/vac_rule_records_datatable',
           locals: { show_link: true,
                     manager: true,
                     show_users: true,
                     show_bonus_status: true,
                     show_vacations: true,
                     show_vacations_status: true,
                     id_datatable: 'pending_bonus_datatable',
                     vac_rule_records: VacRuleRecord.where(paid: nil, active: true).joins(:vac_request => :user).where(vac_requests: {user_id: params[:user_id].to_i, active: true, historical: false,registered_manually: false, status: [VacRequest.approved, VacRequest.closed]})}
  end

  def historical_requests_from
    requests = VacRequest.joins(:user).where(user_id: params[:user_id], registered_manually: false, active: true, status: [VacRequest.closed], historical: false).order('apellidos asc, nombre asc, begin asc') + VacRequest.all_historical.by_user(params[:user_id])
    render partial: 'vac_records_register/requests_datatable',
           locals: { show_link: true,
                     close_massive: false,
                     manager: true,
                     historical: true,
                     editable: false,
                     show_users: true,
                     not_as_datatable: true,
                     requests: requests}
  end

  def find_users_search_generic
    code = params[:user][:codigo]
    lastname = params[:user][:apellidos]
    name = params[:user][:nombre]
    active = params[:include_no_active] ? [0,1] : 1
    users = code.blank? && lastname.blank? && name.blank? ? [] : User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo IN (?)', '%'+code+'%', '%'+lastname+'%', '%'+name+'%', active).order('apellidos, nombre').all
    render partial: 'shared/users_search_datatable',
           locals: { users: users,
                     action_to_do: params[:action_to_do],
                     as_ajax: true }
  end

  def historical_deletable_from
    render partial: 'historical_requests_deletable',
           locals: {  direct_delete: true,
                     requests: VacRequest.all_historical.by_user(params[:user_id]) }
  end


  def find_users_search_manual_req
    code = params[:user][:codigo]
    lastname = params[:user][:apellidos]
    name = params[:user][:nombre]
    active = params[:include_no_active] ? [0,1] : 1
    users = code.blank? && lastname.blank? && name.blank? ? [] : User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo IN (?)', '%'+code+'%', '%'+lastname+'%', '%'+name+'%', active).order('apellidos, nombre').all
    render partial: 'shared/users_search_datatable',
           locals: { users: users,
                     action_to_do: 1,
                     as_ajax: true }
  end

  def manual_requests_from
    render partial: 'records_manually',
           locals: { records: VacRequest.registered_manually_of(params[:user_id]),
                     not_as_datatable: true }
  end

  def scheme_report
    @date_end = params[:Fecha].to_date
    render xlsx: 'scheme_report_info.xlsx', filename: 'Reporte de proyección de días -'+@date_end.strftime('%Y-%m-%d')
  end

  def update_legal_and_prog
    this_date = params[:reference_date].to_date
    users = User.all_anniversary(this_date)
    users.each { |user| update_legal_accumulated(user.id, this_date); update_prog_accumulated(this_date, user.id)}
    flash[:success] = 'Proceso ejecutado correctamente'
    redirect_to vac_manager_index_path
  end

  def index
    @tab = params[:tab] ? params[:tab] : 0
  end

  def show_manual_record
    @vac_record = VacRequest.find(params[:vac_request_id])
  end

  def new_manual_record
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @users.delete_if { |user| !BpGeneralRulesVacation.user_active(lms_time, user.id) }
      @user = User.new(params[:user])
    end
  end

  def new_manual_record_step_2
    @vac_request = VacRequest.new(user_id: params[:user_id])
    @vac_request.days_progressive = 0
    @vac_request.days_manually = 0
  end

  def create_manual_record
    request = manual_request_create(params[:vac_request], params[:user_id])
    if request.save
      update_accumulated_since(request.begin.to_date, request.user_id, 'actualización solicitud manual')
      flash[:success] = t('views.vac_requests.flash_messages.success_created')
      redirect_to vac_manager_show_manual_record_path(request)
    else
      @vac_request = request
      render action: 'new_manual_record_step_2'
    end
  end

  def user_manage
    @user = User.find(params[:user_id])
  end

  def massive_upload_requests
  end

  def massive_create_upload_requests
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_requests file
        flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
        redirect_to vac_manager_index_path
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
        render 'massive_upload_requests'
      end
    else
      flash.now[:danger] = message
      render 'massive_upload_requests'
    end
  end

  def delete_historical_request
    request = VacRequest.find(params[:vac_request_id])
    request.active = false
    request.deactivated_at = lms_time
    request.deactivated_by_user_id = user_connected.id
    request.save
    flash[:success] = t('views.brg_module.flash_messages.delete_success')
    redirect_to vac_manager_massive_historical_upload_path
  end

  def delete_historical_request_ajax
    request = VacRequest.find(params[:vac_request_id])
    request.active = false
    request.deactivated_at = lms_time
    request.deactivated_by_user_id = user_connected.id
    request.save
    render nothing: true
  end

  def massive_historical_upload
  end

  def massive_historical_upload_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_historical file
        flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
        redirect_to vac_manager_index_path
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
        render 'massive_historical_upload'
      end
    else
      flash.now[:danger] = message
      render 'massive_historical_upload'
    end
  end

  def request_new
    @vac_requests = VacRequest.new(user_id: params[:user_id])
  end

  def request_manage
    @vac_request = VacRequest.find(params[:vac_request_id])
    @vac_request = VacRequest.find(@vac_request.latest_version)
    @back_to_validation = (!params[:back_to_validation].blank? && params[:back_to_validation].to_i == 1)
    @back_to_search = (!params[:back_to_validation].blank? && params[:back_to_validation].to_i == 2)
  end

  def request_edit
    @vac_request = VacRequest.find(params[:vac_request_id])
    @vac_request = VacRequest.find(@vac_request.latest_version)
  end

  def request_update
    actual_request = VacRequest.find(params[:vac_request_id])
    actual_request = VacRequest.find(actual_request.latest_version)
    params[:vac_request_id] = actual_request.id

    actual_request = VacRequest.find(params[:vac_request_id])
    updated_request = complete_update(actual_request.id, params[:vac_request])

    verify_closed_request = verified_closed_request(updated_request, updated_request.prev_request_id)
    if updated_request.valid? && verify_closed_request
      exception = actual_request.exception_available? ? actual_request.bp_warning_exception : BpWarningException.first_usable_for_at(updated_request.user_id, lms_time)
      restrictive_warnings = gimme_restrictive_warnings(updated_request.user_id, updated_request.begin, updated_request.end, updated_request.days, updated_request.days_progressive, params[:vac_request_id])
      updated_request.bp_warning_exception_id = exception.id if exception && restrictive_warnings.size > 0
      updated_request.save
      actual_request = deactivate_request(params[:vac_request_id])
      actual_request.next_request_id = updated_request.id
      actual_request.save

      copy_paid_benefits(updated_request.id, actual_request.id)
      update_accumulated_since(updated_request.begin.to_date, updated_request.user_id, 'Cierre de solicitud') if updated_request.closed?

      render partial: 'vac_records_register/request_info',
             locals: {request: updated_request,
                      editable: !updated_request.closed?,
                      manager: true}
    else
      @vac_request = actual_request
      @vac_request.assign_attributes(params[:vac_request])
      @vac_request.valid?
      @vac_request.errors.add(:base, 'Registro no puede ser cerrado') unless verify_closed_request

      render partial: 'vac_records_register/request_new_form',
             locals: {request: @vac_request,
                      url_to_go: vac_manager_request_update_path(@vac_request),
                      manager: true}
    end
  end

  def take_benefits
    benefit = BpConditionVacation.find(params[:benefit_id])
    request = VacRequest.find(params[:request_id])
    if benefit.bonus_money
      benefit = rule_record_complete_create(params[:benefit_id], params[:request_id])
    else
      if request.closed?
        benefit = rule_record_complete_create_paid(params[:benefit_id], params[:request_id], params[:paid], params[:paid_description])
      else
        benefit = rule_record_complete_create(params[:benefit_id], params[:request_id])
      end
    end
    benefit.save

    update_end_vacations(params[:request_id])
    render nothing: true
  end
  def benefit_change_paid
    rule_record = VacRuleRecord.find(params[:vac_rule_record_id])
    @vac_request = rule_record.vac_request
    if rule_record.paid.nil?
      rule_record.paid = params[:paid].to_i == 1
      rule_record.paid_at = lms_time
      rule_record.paid_by_user_id = user_connected.id
      rule_record.paid_description = params[:paid_description]
      rule_record.save

      render partial: 'vac_records_register/request_info',
             locals: {request: @vac_request,
                      editable: !@vac_request.closed?,
                      show_generic_message: true,
                      type_generic_message: 'success',
                      generic_message: t('views.vac_accumulateds.flash_messages.success_changed'),
                      manager: true}
    else

      render partial: 'vac_records_register/request_info',
             locals: {request: @vac_request,
                      editable: !@vac_request.closed?,
                      show_generic_message: true,
                      type_generic_message: 'danger',
                      generic_message: t('views.vac_accumulateds.flash_messages.danger_changed'),
                      manager: true}
    end
  end

  def massive_close_requests
    params_requests = defined?(params[:requests]) && !params[:requests].nil? ? params[:requests] : []

    params_requests.each do |request_id|
      actual_request = VacRequest.find(request_id)
      actual_request = VacRequest.find(actual_request.latest_version)

      updated_request = actual_request.dup
      updated_request.registered_at = lms_time
      updated_request.registered_by_user_id = user_connected.id
      updated_request.prev_request_id = actual_request.id
      updated_request.status = VacRequest.closed
      updated_request.status_changed_by_user_id = user_connected.id

      if updated_request.valid?
        updated_request.save
        actual_request.next_request_id = updated_request.id
        actual_request = deactivate_request(actual_request.id)
        actual_request.save

        actual_request.vac_rule_records.where(active: true).each do | rule_record |
          new_rule_record = rule_record.dup
          new_rule_record.vac_request_id = updated_request.id
          unless new_rule_record.bp_condition_vacation.bonus_money
            new_rule_record.paid = true
            new_rule_record.paid_at = lms_time
            new_rule_record.paid_by_user_id = user_connected.id
          end
          new_rule_record.save
        end
        update_accumulated_since(updated_request.begin.to_date, updated_request.user_id, 'Cierre de solicitud') if updated_request.closed?
      end
    end

    render nothing: true
  end

  def auto_close_seven_days
    requests_id = requests_id_before(lms_time.to_date)
    requests_id.each do |request_id|
      actual_request = VacRequest.find(request_id)
      actual_request = VacRequest.find(actual_request.latest_version)

      updated_request = actual_request.dup
      updated_request.registered_at = lms_time
      updated_request.registered_by_user_id = user_connected.id
      updated_request.prev_request_id = actual_request.id
      updated_request.status = VacRequest.closed
      updated_request.status_changed_by_user_id = user_connected.id

      if updated_request.valid?
        updated_request.save
        actual_request.next_request_id = updated_request.id
        actual_request = deactivate_request(actual_request.id)
        actual_request.save

        actual_request.vac_rule_records.where(active: true).each do | rule_record |
          new_rule_record = rule_record.dup
          new_rule_record.vac_request_id = updated_request.id
          unless new_rule_record.bp_condition_vacation.bonus_money
            new_rule_record.paid = true
            new_rule_record.paid_at = lms_time
            new_rule_record.paid_by_user_id = user_connected.id
          end
          new_rule_record.save
        end

        update_accumulated_since(updated_request.begin.to_date, updated_request.user_id, 'Cierre de solicitud automático') if updated_request.closed?
      end
    end

    flash[:success] = 'Se cerraron solicitudes correctamente'
    redirect_to vac_manager_index_path
  end

  def paid_description_modal
    vac_rule = VacRuleRecord.find(params[:vac_rule_record_id])
    vac_rule.paid = params[:paid].to_i
    render partial: 'paid_description_modal',
           locals: { vac_rule: vac_rule,
                     required: !vac_rule.paid }
  end

  def massive_upload_validators
  end

  def massive_upload_validators_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_approvers file
        flash.now[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'massive_upload_validators'
  end

  def approvee_delete
    approvee = VacUserToApprove.find(params[:vac_user_to_approve_id])
    approvee.active = false
    approvee.deactivated_by_user = user_connected
    approvee.deactivated_at = lms_time
    approvee.save
    render nothing: true
  end

  def show_request_historical_info
    request = VacRequest.find(params[:vac_request_id])
    render partial: 'request_historical_info_modal',
           locals: {request: request}
  end

  def benefits_report

  end

  def benefits_report_generate
    begin_range = params[:Inicio].to_date
    end_range = params[:Fin].to_date

    if params[:select2_status_requests] && params[:select2_bonus] && params[:select2_empresa]
      status = []
      params[:select2_status_requests].each {|st| status << st.to_i}

      @characteristic_values_array = []
      params[:select2_empresa].each { |val| @characteristic_values_array << val.to_i }

      paid_bonus = []
      params[:select2_bonus].each do |bon|
        paid_bonus << false if bon.to_i == -1
        paid_bonus << nil if bon.to_i == 0
        paid_bonus << true if bon.to_i == 1
      end

      begin_as_reference = params[:begin_as_reference] && params[:begin_as_reference] == 1.to_s ? true : false
      @include_money = params[:include_money] && params[:include_money] == 1.to_s ? true : false
      @include_not_money = params[:include_not_money] && params[:include_not_money] == 1.to_s ? true : false

      @bonus = []

      requests = VacRequest.where(active: true, status: status)
      requests.each do |request|
          end_date = request.end
          request.vac_rule_records.where(active: true, paid: [nil, true]).each do |vac_rule_record|
            benefit = vac_rule_record.bp_condition_vacation
            next unless benefit.bonus_days
            end_date = next_laborable_day?(end_date, request.user_id)
            end_date = vacations_end(benefit.bonus_only_laborable_day, end_date, benefit.bonus_days, request.user_id)
          end
        reference_date = begin_as_reference ? request.begin : end_date
        next unless begin_range <= reference_date && reference_date <= end_range
        request.vac_rule_records.where(active: true, paid: paid_bonus).each do |rule_record|
          @bonus << rule_record
        end
      end

      render xlsx: 'bonus_report_info.xlsx', filename: 'Reporte de bonos'
    else
      flash.now[:danger] = 'Debe seleccionar estados de solicitudes, bonos y empresas'
      render 'benefits_report'
    end
  end

  def benefits_report_generate_for_bonus
    @pay_pending_bonus = true
    begin_range = params[:Inicio_2].to_date
    end_range = params[:Fin_2].to_date

    if params[:select2_status_requests_2] && params[:select2_bonus_2] && params[:select2_empresa_2]
      status = []
      params[:select2_status_requests_2].each {|st| status << st.to_i}

      paid_bonus = []
      params[:select2_bonus_2].each do |bon|
        paid_bonus << false if bon.to_i == -1
        paid_bonus << nil if bon.to_i == 0
        paid_bonus << true if bon.to_i == 1
      end

      @characteristic_values_array = []
      params[:select2_empresa_2].each { |val| @characteristic_values_array << val.to_i }

      begin_as_reference = params[:begin_as_reference_2] && params[:begin_as_reference_2] == 1.to_s ? true : false
      @include_money = params[:include_money_2] && params[:include_money_2] == 1.to_s ? true : false
      @include_not_money = params[:include_not_money_2] && params[:include_not_money_2] == 1.to_s ? true : false

      @bonus = []

      requests = VacRequest.where(active: true, status: status)
      requests.each do |request|
        end_date = request.end
        request.vac_rule_records.where(active: true, paid: [nil, true]).each do |vac_rule_record|
          benefit = vac_rule_record.bp_condition_vacation
          next unless benefit.bonus_days
          end_date = next_laborable_day?(end_date, request.user_id)
          end_date = vacations_end(benefit.bonus_only_laborable_day, end_date, benefit.bonus_days, request.user_id)
        end
        reference_date = begin_as_reference ? request.begin : end_date
        next unless begin_range <= reference_date && reference_date <= end_range
        request.vac_rule_records.where(active: true, paid: paid_bonus).each do |rule_record|
          @bonus << rule_record
        end
      end

      render xlsx: 'bonus_report_info.xlsx', filename: 'Reporte de bonos'
    else
      flash.now[:danger] = 'Debe seleccionar estados de solicitudes, bonos y empresas'
      render 'benefits_report'
    end
  end

  def benefits_report_generate_requests
    begin_range = params[:Inicio_3].to_date
    end_range = params[:Fin_3].to_date
    @requests_r = []

    if params[:select2_status_requests_3] && params[:select2_empresa_3]
      status = []
      params[:select2_status_requests_3].each {|st| status << st.to_i}

      @characteristic_values_array = []
      params[:select2_empresa_3].each { |val| @characteristic_values_array << val.to_i }

      begin_as_reference = params[:begin_as_reference_3] && params[:begin_as_reference_3] == 1.to_s ? true : false

      requests = VacRequest.where(active: true, status: status, registered_manually: false, historical: false).all
      requests += VacRequest.where(active: true, registered_manually: false, historical: true).all if status.include?(5)

      requests.each do |request|
        end_date = request.end
        reference_date = begin_as_reference ? request.begin : end_date
        next unless begin_range <= reference_date && reference_date <= end_range
        @requests_r << request
      end
      render xlsx: 'requests_report_info.xlsx', filename: 'Reporte de solicitudes'
    else
      flash.now[:danger] = 'Debe seleccionar estados de solicitudes y empresas'
      render 'benefits_report'
    end
  end

  def status_description_modal
    request = VacRequest.find(params[:vac_request_id])
    request.status = params[:vac_request_status].to_i
    render partial: 'vac_records_register/status_description_modal',
           locals: { request: request,
                     manager: true,
                     required: request.status_description_required? }
  end

  def accumulated_info_for
    vac_request = VacRequest.find(params[:vac_request_id])
    render partial: 'vac_accumulateds/accumulated_info',
           locals: { vac_accumulated: VacAccumulated.user_latest(vac_request.user.id),
                     user: vac_request.user }
  end

  def change_status
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)
    params[:vac_request_id] = request.id

    type_generic_message = 'danger'
    generic_message = t('views.vac_requests.flash_messages.danger_changed')

    request_status = params[:vac_request_status]
    status_description = params[:vac_request][:status_description]

    params = {status: request_status,
              status_description: status_description,
              status_changed_by_user_id: user_connected.id}

    updated_request = complete_update(request.id, params)
    if updated_request.valid?
      updated_request.save
      benefits = VacRuleRecord.where(active: true, vac_request_id: request.id)
      benefits.each do |benefit|
        new_benefit = benefit.dup
        new_benefit.vac_request_id = updated_request.id
        new_benefit.save
      end
      prev_request = VacRequest.find(request.id)
      prev_request.next_request_id = updated_request.id
      prev_request = deactivate_request(request.id)
      prev_request.save

      if prev_request.closed? && !updated_request.closed?
        benefits = VacRuleRecord.where(active: true, vac_request_id: updated_request.id)
        benefits.each do |benefit|
          next unless benefit.bp_condition_vacation.bonus_days
          benefit.paid = nil
          benefit.paid_by_user_id = nil
          benefit.paid_at = nil
          benefit.save
        end
        prev_request.vac_accumulateds.active.each do |vac_accumulated|
          vac_accumulated.active = false
          vac_accumulated.deactivated_at = lms_time
          vac_accumulated.deactivated_by_user_id = user_connected.id
          vac_accumulated.deactivated_description = 'Solicitud reabierta.'
          vac_accumulated.save
        end
      end

      type_generic_message = 'success'
      generic_message = t('views.vac_requests.flash_messages.success_changed')
      update_accumulated_since(prev_request.begin, prev_request.user_id, 'Se rechazó solicitud cerrada') if prev_request.closed?
      request = updated_request
      VacsMailer.approved_request(request, @company).deliver if request.approved? && !(prev_request.closed? && !updated_request.closed?)
      VacsMailer.rejected_request(request, @company).deliver if request.rejected?
    end

    render partial: 'vac_records_register/request_info',
           locals: {request: request,
                    show_generic_message: true,
                    type_generic_message: type_generic_message,
                    generic_message: generic_message,
                    editable: true,
                    manager: true }
  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'vacs', true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_excel(params)
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
    return error_message unless params
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
    return error_message unless File.extname(params['datafile'].original_filename) == '.xlsx'
    error_message = ''
    return error_message
  end

  def get_temporal_file(params)
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal
    path = File.join(directorio_temporal, nombre_temporal)
    File.open(path, 'wb') { |f| f.write(params['datafile'].read) }
    return archivo_temporal
  end

  def only_active_request
    request = VacRequest.find(params[:vac_request_id])
    request = VacRequest.find(request.latest_version)

    unless request.active
      flash[:danger] = t('views.vac_requests.flash_messages.danger_not_active_request')
      redirect_to root_path
    end
  end

  def verify_closable
    request = VacRequest.find(params[:vac_request_id])
    return if request.closable?
    flash[:danger] = 'Registro no puede ser cerrado'
    redirect_to vac_manager_index_path
  end
end