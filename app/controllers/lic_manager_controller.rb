class LicManagerController < ApplicationController
  include LicManagerHelper
  include LicApproversHelper
  include LicRegistersHelper
  include VacationsModule
  include LicensesModule

  before_filter :authenticate_user
  before_filter :verify_access_manage_module
  before_filter :verify_active_approver, only: [:approver_manage, :approver_deactivate, :approvee_new, :approvee_create ]
  before_filter :active_approver_by_approvee, only: [:approvee_deactivate]

  before_filter :verify_active_request, only: %i[request_edit
                                                 request_update
                                                 change_paid
                                                 change_paid_modal
                                                 change_status_modal
                                                 change_status]

  before_filter :verify_active_event, only: %i[ event_manage
                                                event_edit
                                                event_update ]

  before_filter :available_to_manager_edit_request, only: %i[request_edit
                                                             request_update
                                                             change_paid
                                                             change_paid_modal
                                                             change_status_modal
                                                             change_status]

  def index
    @lic_approvers = LicApprover.where(active: true)
  end

  def approver_new
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def approver_create
    approver = approver_complete_create(params[:user_id])
    flash[:success] = t('views.lic_approvers.flash_messages.success_created')
    redirect_to lic_managers_approver_manage_path(approver)
  end

  def approver_manage
    @lic_approver = LicApprover.find(params[:lic_approver_id])
  end

  def approver_deactivate
    approver_complete_deactivate(params[:lic_approver_id])
    flash[:success] = t('views.lic_approvers.flash_messages.sucess_deleted')
    redirect_to lic_manager_index_path
  end

  def approvee_new
    @approver = LicApprover.find(params[:lic_approver_id])
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def approvee_create
    approver = approvee_complete_create(params[:lic_approver_id], params[:user_id])
    flash[:success] = t('views.lic_approvers.flash_messages.success_created')
    redirect_to lic_managers_approver_manage_path(approver)
  end

  def approvee_deactivate
    approver = approvee_complete_deactivate(params[:lic_user_to_approve_id])
    flash[:success] = t('views.lic_approvers.flash_messages.sucess_deleted')
    redirect_to lic_managers_approver_manage_path(approver)
  end

  def event_manage
    @lic_event = LicEvent.find(params[:lic_event_id])
  end

  def event_edit
    @lic_event = LicEvent.find(params[:lic_event_id])
  end

  def event_update
    prev_event = LicEvent.find(params[:lic_event_id])
    event = complete_event_update(params[:lic_event], prev_event.id)
   company = @company
    files_to_save = []

    event.lic_item_values.each do |value|
      value.registered_at = lms_time
      value.registered_by_user_id = user_connected.id
      next unless value.value_file
      if value.bp_item.item_file?
        next if value.value_file.class == String
        file = value.value_file.dup
        value.value = value.value_file.original_filename
        value.value_file = SecureRandom.hex(5)
        files_to_save << [value.value_file.dup, file]
      end
    end

    requests = []
    prev_event.lic_requests.where(active: true).each { |request| requests << request.dup }

    if event.valid?
      files_to_save.each {|array| save_file(array[1], company, array[0])}
      event.save
      requests.each { |request| request.update_attributes(lic_event_id: event.id) }
      flash[:success] = t('views.lic_registers.flash_messages.success_changed')
      redirect_to lic_managers_event_manage_path(event)
    else
      @lic_event = event
      render action: 'event_edit'
    end
  end

  def change_status
    request = LicRequest.find(params[:lic_request_id])
    updated_request = complete_change_status(request.id, params[:lic_request])
    updated_request.status = params[:status]

    if updated_request.valid?
      updated_request.save
      request = deactivate_request(request.id)
      request.save

      VacsMailer.approved_lic_request(updated_request,@company).deliver if updated_request.approved?
      VacsMailer.rejected_lic_request(updated_request,@company).deliver if updated_request.rejected?

      if updated_request.rejected?
        flash[:success] = t('views.lic_approvers.flash_messages.success_rejected')
        redirect_to lic_managers_event_manage_path(updated_request.lic_event)
      else
        flash[:success] = t('views.lic_approvers.flash_messages.success_approve') if updated_request.approved?
        redirect_to lic_managers_event_manage_path(updated_request.lic_event)
      end
    else
      flash[:danger] = t('views.lic_approvers.flash_messages.danger_not_valid')
      redirect_to lic_managers_event_manage_path(request.lic_event)
    end
  end

  def change_status_modal
    request = LicRequest.find(params[:lic_request_id])
    request.status = params[:status].to_i
    render partial: 'lic_approvers/status_description_modal',
           locals: { request: request,
                     required: request.status_require_description?,
                     url_to_go: lic_managers_change_status_path(request, request.status)}
  end

  def change_paid
    request = LicRequest.find(params[:lic_request_id])
    updated_request = complete_change_status(request.id, params[:lic_request])
    updated_request.paid = params[:paid].to_i == 1 ? true : false
    updated_request.paid_at = lms_time
    updated_request.paid_by_user_id = user_connected.id

    if updated_request.valid? && request.payable?
      updated_request.save
      request = deactivate_request(request.id)
      request.save

      if updated_request.paid
        flash[:success] = t('views.lic_approvers.flash_messages.success_paid')
        redirect_to lic_managers_event_manage_path(updated_request.lic_event)
      else
        flash[:success] = t('views.lic_approvers.flash_messages.success_not_paid') if updated_request.paid == false
        redirect_to lic_managers_event_manage_path(updated_request.lic_event)
      end
    else
      flash[:danger] = t('views.lic_approvers.flash_messages.danger_not_valid')
      redirect_to lic_managers_event_manage_path(request.lic_event)
    end
  end

  def change_paid_modal
    request = LicRequest.find(params[:lic_request_id])
    request.paid = params[:paid].to_i == 1 ? true : false
    render partial: 'lic_manager/paid_description_modal',
           locals: { request: request,
                     required: request.paid_require_description?,
                     url_to_go: lic_managers_change_paid_path(request, params[:paid])}
  end

  def request_edit
    @lic_request = LicRequest.find(params[:lic_request_id])

    request = @lic_request
    license = @lic_request.bp_license
    event = @lic_request.lic_event

    @messages = license.satisfy?(request.begin, request.end, request.days && request.days > 0 ? request.days : request.hours, anniversary(request.user_id), request.user_id, event ? event.id : nil, request.id)
    other_messages = satisfy_license_begin(request.user_id, license.id, request.begin, event.date_event)
    @messages += other_messages if other_messages.size > 0
    # other_messages = satisfy_conflict_periods(request_begin, request_end.to_date, user_id, request_id)
    # messages << other_messages if other_messages.size > 0

    requests = VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status, user_id: request.user_id)
    message = ''

    request_end = request.end
    request_begin = request.begin

    requests.each do |request|
      next unless (request.begin <= request_end.to_date && request.end >= request_begin.to_date)
      message += request.begin.strftime('%d/%m/%Y')+' - '+request.end.strftime('%d/%m/%Y')+' ('+request.status_message+'). '
    end
    @messages += 'tiene conflicto con vacaciones: ' + message if message.size > 0
  end

  def request_update
    prev_request = LicRequest.find(params[:lic_request_id])
    request = complete_request_update(params[:lic_request], prev_request.id)

    if request.valid?
      request.save
      prev_request = complete_deactivate_request(prev_request.id)
      prev_request.next_lic_request_id = request.id
      prev_request.save

      flash[:success] = t('views.lic_registers.flash_messages.success_changed')
      redirect_to lic_managers_event_manage_path(request.lic_event)
    else
      @lic_request = request
      render action: 'request_edit'
    end
  end

  def massive_upload_validators
  end

  def massive_upload_validators_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_approvers file
        flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'massive_upload_validators'
  end

  def approvee_delete
    approvee = LicUserToApprove.find(params[:lic_user_to_approve_id])
    approvee.active = false
    approvee.deactivated_by_user = user_connected
    approvee.deactivated_at = lms_time
    approvee.save
    render nothing: true
  end

  def show_request_historical_info
    request = LicRequest.find(params[:lic_request_id])
    render partial: 'request_historical_info_modal',
           locals: {request: request}
  end

  private

  def available_to_manager_edit_request
    request = LicRequest.find(params[:lic_request_id])
    return if request.available_manager_to_edit
    flash[:danger] = 'No dispone de privilegios para gestionar solicitud cerrada'
    redirect_to root_path
  end

  def verify_excel(params)
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
    return error_message unless params
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
    return error_message unless File.extname(params['datafile'].original_filename) == '.xlsx'
    error_message = ''
    return error_message
  end

  def get_temporal_file(params)
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal
    path = File.join(directorio_temporal, nombre_temporal)
    File.open(path, 'wb') { |f| f.write(params['datafile'].read) }
    return archivo_temporal
  end

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'licenses', true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_active_approver
    return unless defined?(params[:lic_approver_id])
    approver = LicApprover.find(params[:lic_approver_id])
    return if approver && approver.active
    flash[:danger] = 'No tiene acceso a registro'
    redirect_to root_path
  end

  def active_approver_by_approvee
    return unless defined?(params[:lic_user_to_approve_id])
    approvee = LicUserToApprove.find(params[:lic_user_to_approve_id])
    return if approvee && approvee.lic_approver.active
    flash[:danger] = 'No tiene acceso a registro'
    redirect_to root_path
  end

  def approver_complete_create(user_id)
    approver = LicApprover.where(user_id: user_id).first_or_create
    approver.active = true
    approver.deactivated_at = nil
    approver.deactivated_by_user_id = nil
    approver.registered_at = lms_time
    approver.registered_by_user_id = user_connected.id
    approver.save
    return approver
  end

  def approver_complete_deactivate(approver_id)
    approver = LicApprover.find(approver_id)
    approver.active = false
    approver.deactivated_at = lms_time
    approver.deactivated_by_user_id = user_connected.id
    approver.save
    return approver
  end

  def approvee_complete_create(approver_id, user_id)
    approvee = LicUserToApprove.where(user_id: user_id, lic_approver_id: approver_id).first_or_create
    approvee.active = true
    approvee.deactivated_at = nil
    approvee.deactivated_by_user_id = nil
    approvee.registered_at = lms_time
    approvee.registered_by_user_id = user_connected.id
    approvee.save
    return approvee.lic_approver
  end

  def approvee_complete_deactivate(approvee_id)
    approvee = LicUserToApprove.find(approvee_id)
    approvee.active = false
    approvee.deactivated_at = lms_time
    approvee.deactivated_by_user_id = user_connected.id
    approvee.save
    return approvee.lic_approver
  end
end
