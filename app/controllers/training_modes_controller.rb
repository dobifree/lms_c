class TrainingModesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @training_modes = TrainingMode.all
  end


  def show
    @training_mode = TrainingMode.find(params[:id])
  end


  def new
    @training_mode = TrainingMode.new
  end


  def edit
    @training_mode = TrainingMode.find(params[:id])
  end

  def delete
    @training_mode = TrainingMode.find(params[:id])
  end


  def create
    @training_mode = TrainingMode.new(params[:training_mode])


      if @training_mode.save
        flash[:success] = t('activerecord.success.model.training_mode.create_ok')
        redirect_to training_modes_path
      else
        render action: 'new'
      end

  end


  def update
    @training_mode = TrainingMode.find(params[:id])


      if @training_mode.update_attributes(params[:training_mode])
        flash[:success] = t('activerecord.success.model.training_mode.update_ok')
        redirect_to training_modes_path
      else
        render action: 'edit'
      end

  end




  def destroy

    @training_mode = TrainingMode.find(params[:id])

    if verify_recaptcha

      if @training_mode.destroy
        flash[:success] = t('activerecord.success.model.training_mode.delete_ok')
        redirect_to training_modes_path
      else
        flash[:danger] = t('activerecord.success.model.training_mode.delete_error')
        redirect_to delete_training_mode_path(@training_mode)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.training_mode.captcha_error')
      redirect_to delete_training_mode_path(@training_mode)

    end

  end
end
