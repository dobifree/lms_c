class HrProcessCalibrationSessionsController < ApplicationController

  before_filter :authenticate_user

  before_filter :acceso_gestionar_proceso, only: [:index, :new]
  before_filter :acceso_gestionar_proceso2, only: [:create]
  before_filter :acceso_gestionar_proceso3, only: [:show, :edit, :update, :destroy]

  before_filter :acceso_ver_proceso, only: [:finalizar_sesion]
  before_filter :acceso_calibrar, only: [:abrir_sesion, :finalizar_sesion]
  before_filter :acceso_abrir_sesion, only: [:abrir_sesion]

  def index

    @hr_process_calibration_sessions = @hr_process.hr_process_calibration_sessions

  end

  def show

  end

  def new

    @hr_process_calibration_session = @hr_process.hr_process_calibration_sessions.build

  end

  def create
    @hr_process_calibration_session = HrProcessCalibrationSession.new(params[:hr_process_calibration_session])

    if @hr_process_calibration_session.save
      flash[:success] = t('activerecord.success.model.hr_process_calibration_session.create_ok')
      redirect_to listar_hr_process_calibration_sessions_path(@hr_process_calibration_session.hr_process)
    else
      render action: 'new'
    end

  end

  def edit

  end

  def update

    if @hr_process_calibration_session.update_attributes(params[:hr_process_calibration_session])
      flash[:success] = t('activerecord.success.model.hr_process_calibration_session.update_ok')
      redirect_to listar_hr_process_calibration_sessions_path(@hr_process_calibration_session.hr_process)
    else
      render action: 'edit'
    end

  end

  def destroy
    @hr_process_calibration_session.destroy

    flash[:success] = t('activerecord.success.model.hr_process_calibration_session.delete_ok')
    redirect_to listar_hr_process_calibration_sessions_path(@hr_process)
  end

  def finalizar_sesion
    @hr_process_calibration_session.abierta = false
    @hr_process_calibration_session.save

    flash[:success] = t('activerecord.success.model.hr_process_calibration_session.finish_ok')

    redirect_to hr_process_assessment_ver_proceso_calibrar_path(@hr_process, @hr_process_calibration_session)

  end

  def abrir_sesion
    @hr_process_calibration_session.abierta = true
    @hr_process_calibration_session.save

    flash[:success] = t('activerecord.success.model.hr_process_calibration_session.open_ok')

    redirect_to hr_process_assessment_ver_proceso_calibrar_path(@hr_process, @hr_process_calibration_session)

  end

  private

    def acceso_gestionar_proceso

      @hr_process = HrProcess.find(params[:hr_process_id])

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

      unless hr_process_manager

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end

    def acceso_gestionar_proceso2

      @hr_process = HrProcess.find(params[:hr_process_calibration_session][:hr_process_id])

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

      unless hr_process_manager

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end

    def acceso_gestionar_proceso3

      @hr_process_calibration_session = HrProcessCalibrationSession.find(params[:id])
      @hr_process = @hr_process_calibration_session.hr_process

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

      unless hr_process_manager

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end

    def acceso_ver_proceso

      @hr_process = HrProcess.find(params[:hr_process_id])

      #unless @hr_process.fecha_inicio <= lms_time && lms_time <= @hr_process.fecha_fin
      unless @hr_process.abierto && @hr_process.fecha_inicio <= lms_time

        flash[:danger] = t('security.no_access_hr_perform_evaluation_time')
        redirect_to root_path

      end

    end

    def acceso_calibrar

      @hr_process_calibration_session = HrProcessCalibrationSession.find(params[:hr_process_calibration_session_id])

      @hr_process = @hr_process_calibration_session.hr_process

      hr_process_calibration_session_c = @hr_process_calibration_session.hr_process_calibration_session_cs.where('user_id = ? AND manager = ?', user_connected.id, true).first

      unless hr_process_calibration_session_c

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end

    def acceso_abrir_sesion

      unless lms_time >= @hr_process_calibration_session.inicio && lms_time <= @hr_process_calibration_session.fin
        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path
      end

    end

end
