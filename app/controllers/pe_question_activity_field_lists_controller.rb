class PeQuestionActivityFieldListsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show
    @pe_question_activity_field_list = PeQuestionActivityFieldList.find(params[:id])
    @pe_evaluation = @pe_question_activity_field_list.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def new

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    @pe_process = @pe_evaluation.pe_process

    @pe_question_activity_field_list = @pe_evaluation.pe_question_activity_field_lists.build

  end

  def create

    @pe_question_activity_field_list = PeQuestionActivityFieldList.new(params[:pe_question_activity_field_list])

    if @pe_question_activity_field_list.save
      redirect_to @pe_question_activity_field_list
    else
      @pe_evaluation = @pe_question_activity_field_list.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'new'
    end

  end

  def edit
    @pe_question_activity_field_list = PeQuestionActivityFieldList.find(params[:id])
    @pe_evaluation = @pe_question_activity_field_list.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def update

    @pe_question_activity_field_list = PeQuestionActivityFieldList.find(params[:id])

    if @pe_question_activity_field_list.update_attributes(params[:pe_question_activity_field_list])
      redirect_to @pe_question_activity_field_list
    else
      @pe_evaluation = @pe_question_activity_field_list.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end

  def destroy
    @pe_question_activity_field_list = PeQuestionActivityFieldList.find(params[:id])
    @pe_question_activity_field_list.destroy

    respond_to do |format|
      format.html { redirect_to pe_question_activity_field_lists_url }
      format.json { head :no_content }
    end
  end
end
