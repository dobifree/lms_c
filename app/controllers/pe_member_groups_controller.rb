class PeMemberGroupsController < ApplicationController
  # GET /pe_member_groups
  # GET /pe_member_groups.json
  def index
    @pe_member_groups = PeMemberGroup.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_member_groups }
    end
  end

  # GET /pe_member_groups/1
  # GET /pe_member_groups/1.json
  def show
    @pe_member_group = PeMemberGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_member_group }
    end
  end

  # GET /pe_member_groups/new
  # GET /pe_member_groups/new.json
  def new
    @pe_member_group = PeMemberGroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pe_member_group }
    end
  end

  # GET /pe_member_groups/1/edit
  def edit
    @pe_member_group = PeMemberGroup.find(params[:id])
  end

  # POST /pe_member_groups
  # POST /pe_member_groups.json
  def create
    @pe_member_group = PeMemberGroup.new(params[:pe_member_group])

    respond_to do |format|
      if @pe_member_group.save
        format.html { redirect_to @pe_member_group, notice: 'Pe member group was successfully created.' }
        format.json { render json: @pe_member_group, status: :created, location: @pe_member_group }
      else
        format.html { render action: "new" }
        format.json { render json: @pe_member_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pe_member_groups/1
  # PUT /pe_member_groups/1.json
  def update
    @pe_member_group = PeMemberGroup.find(params[:id])

    respond_to do |format|
      if @pe_member_group.update_attributes(params[:pe_member_group])
        format.html { redirect_to @pe_member_group, notice: 'Pe member group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pe_member_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pe_member_groups/1
  # DELETE /pe_member_groups/1.json
  def destroy
    @pe_member_group = PeMemberGroup.find(params[:id])
    @pe_member_group.destroy

    respond_to do |format|
      format.html { redirect_to pe_member_groups_url }
      format.json { head :no_content }
    end
  end
end
