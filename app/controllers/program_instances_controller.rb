class ProgramInstancesController < ApplicationController
  # GET /program_instances
  # GET /program_instances.json
  def index
    @program_instances = ProgramInstance.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @program_instances }
    end
  end

  # GET /program_instances/1
  # GET /program_instances/1.json
  def show
    @program_instance = ProgramInstance.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @program_instance }
    end
  end

  # GET /program_instances/new
  # GET /program_instances/new.json
  def new
    @program_instance = ProgramInstance.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @program_instance }
    end
  end

  # GET /program_instances/1/edit
  def edit
    @program_instance = ProgramInstance.find(params[:id])
  end

  # POST /program_instances
  # POST /program_instances.json
  def create
    @program_instance = ProgramInstance.new(params[:program_instance])

    respond_to do |format|
      if @program_instance.save
        format.html { redirect_to @program_instance, notice: 'Program instance was successfully created.' }
        format.json { render json: @program_instance, status: :created, location: @program_instance }
      else
        format.html { render action: "new" }
        format.json { render json: @program_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /program_instances/1
  # PUT /program_instances/1.json
  def update
    @program_instance = ProgramInstance.find(params[:id])

    respond_to do |format|
      if @program_instance.update_attributes(params[:program_instance])
        format.html { redirect_to @program_instance, notice: 'Program instance was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @program_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /program_instances/1
  # DELETE /program_instances/1.json
  def destroy
    @program_instance = ProgramInstance.find(params[:id])
    @program_instance.destroy

    respond_to do |format|
      format.html { redirect_to program_instances_url }
      format.json { head :no_content }
    end
  end
end
