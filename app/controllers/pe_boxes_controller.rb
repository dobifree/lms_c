class PeBoxesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def create_1d

    @pe_box = PeBox.new(params[:pe_box])

    if @pe_box.save
      redirect_to pe_process_tab_path @pe_box.pe_process, 'structure'
    else
      redirect_to pe_process_tab_path @pe_box.pe_process, 'structure'
    end

  end

  def update_1d

    @pe_box = PeBox.find(params[:id])

    if @pe_box.update_attributes(params[:pe_box])
      redirect_to pe_process_tab_path @pe_box.pe_process, 'structure'
    else
      redirect_to pe_process_tab_path @pe_box.pe_process, 'structure'
    end

  end

  def create_2d

    @pe_box = PeBox.new(params[:pe_box])

    if @pe_box.save
      redirect_to pe_process_tab_path @pe_box.pe_process, 'structure'
    else
      redirect_to pe_process_tab_path @pe_box.pe_process, 'structure'
    end

  end

  def update_2d

    @pe_box = PeBox.find(params[:id])

    if @pe_box.update_attributes(params[:pe_box])
      redirect_to pe_process_tab_path @pe_box.pe_process, 'structure'
    else
      redirect_to pe_process_tab_path @pe_box.pe_process, 'structure'
    end

  end


end
