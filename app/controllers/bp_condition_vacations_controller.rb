class BpConditionVacationsController < ApplicationController

  def new
    @bp_condition_vacation = BpConditionVacation.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bp_condition_vacation }
    end
  end

  def create
    condition_vacation = BpConditionVacation.new(params[:bp_condition_vacation])
    condition_vacation.registered_at = lms_time
    condition_vacation.registered_by_admin_id = admin_connected.id
    if condition_vacation.save
      flash[:success] = t('views.bp_season_periods.flash_messages.success_created')
      redirect_to bp_groups_path
    else
      @bp_condition_vacation = condition_vacation
      render action: "new"
    end
  end

  def deactivate
    condition_vacation = BpConditionVacation.find(params[:bp_condition_vacation_id])
    condition_vacation.active = false
    condition_vacation.deactivated_by_admin_id = admin_connected.id
    condition_vacation.deactivated_at = lms_time
    if condition_vacation.save
      flash[:success] = t('views.bp_condition_vacations.flash_messages.success_deactivate')
    else
      flash[:danger] = t('views.bp_condition_vacations.flash_messages.danger_deactivate')
    end
    redirect_to bp_groups_path
  end


end
