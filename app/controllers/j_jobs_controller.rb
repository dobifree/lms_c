class JJobsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def index
    @j_jobs = JJob.where('current = ?', true)
  end

  def show
    @j_job = JJob.find(params[:id])
    @j_functions = @j_job.j_functions
  end

  def show_history
    @j_job = JJob.find(params[:j_job_id])
    @j_functions = @j_job.j_functions
  end

  def new
    @j_job = JJob.new
  end

  def create
    @j_job = JJob.new(params[:j_job])
    @j_job.version = 1

    if @j_job.save

      JCharacteristic.all.each do |j_characteristic|

        j_job_characteristic = @j_job.j_job_characteristics.build
        j_job_characteristic.characteristic = j_characteristic.characteristic

        if j_characteristic.characteristic.data_type_id == 0
          j_job_characteristic.value_string = params['j_characteristic_'+j_characteristic.id.to_s]
        elsif
          j_job_characteristic.characteristic_value_id = params['j_characteristic_'+j_characteristic.id.to_s]
        end

        j_job_characteristic.save

      end

      if params[:cc]

        params[:cc].each do |j_cost_center_id|

          j_job_cc = @j_cost_center.j_job_ccs.build
          j_job_cc.j_cost_center_id = j_cost_center_id
          j_job_cc.save

        end

      end

      flash[:success] = t('activerecord.success.model.j_job.create_ok')
      redirect_to @j_job
    else
      render action: 'new'
    end

  end

  def edit
    @j_job = JJob.find(params[:id])
  end

  def update

    @j_job = JJob.find(params[:id])

    if @j_job.current

      if @j_job.update_attributes(params[:j_job])

        j_job_origin = @j_job.j_job

        if j_job_origin

          prev_current_job = j_job_origin.j_jobs.reorder('version DESC').second

          if prev_current_job

            prev_current_job.to_date = @j_job.from_date-1.day
            prev_current_job.save

          else

            j_job_origin.to_date = @j_job.from_date-1.day
            j_job_origin.save

          end

        end

        characteristic = Characteristic.where('data_type_id = ?',13).first

        if characteristic

          @j_job.users.each do |user|

            user.update_first_owned_node(@j_job.name)

            user_characteristic = user.user_characteristic characteristic

            if user_characteristic
              user_characteristic.update_value @j_job.name, @company
              user_characteristic.save
            end

            user_j_job_last = user.user_j_jobs.last
            if user_j_job_last && user_j_job_last.to_date.nil?

              user_characteristic_record = user.user_characteristic_records.where('characteristic_id = ? AND DATE(from_date) = ? AND to_date IS NULL', characteristic.id, user_j_job_last.from_date).first

              if user_characteristic_record

                user_characteristic_record.update_record_value @j_job.name
                user_characteristic_record.save

              end

            end

          end

        end

        @j_job.j_job_ccs.destroy_all

        if params[:cc]

          params[:cc].each do |j_cost_center_id|

            j_job_cc = @j_job.j_job_ccs.build
            j_job_cc.j_cost_center_id = j_cost_center_id
            j_job_cc.save

          end

        end

        flash[:success] = t('activerecord.success.model.j_job.update_ok')
        redirect_to @j_job

      else

        flash.now[:danger] = "error"
        render action: 'edit'

      end

    else

      flash.now[:danger] = "No se puede editar versión no-actual"
      render action: 'edit'
    end

  end

  def new_version

    @j_job = JJob.find(params[:j_job_id])
    @j_functions = @j_job.j_functions

  end

  def create_version

    j_job = JJob.find(params[:j_job][:id])

    new_version_job = j_job.dup

    new_version_job.version += 1

    new_version_job.j_job_id = j_job.id unless j_job.j_job_id

    new_version_job.from_date = params[:j_job][:from_date]

    if j_job.current && new_version_job.save
      flash[:success] = t('activerecord.success.model.j_job.create_version_ok')

      j_job.current = false
      j_job.to_date = new_version_job.from_date-1.day
      j_job.save

      j_job.j_functions.each do |j_function|

        new_version_j_function = j_function.dup
        new_version_j_function.j_job_id = new_version_job.id
        new_version_j_function.save

      end

      j_job.users.each do |user|
        user.j_job_id = new_version_job.id
        user.save

        user_j_job_last = user.user_j_jobs.last
        if user_j_job_last && user_j_job_last.j_job_id == j_job.id && user_j_job_last.to_date.nil?
          user_j_job_last.j_job_id = new_version_job.id
          user_j_job_last.save

        end

      end

      j_job.j_job_characteristics.each do |j_job_characteristic|

        new_j_job_characteristic = j_job_characteristic.dup
        new_j_job_characteristic.j_job_id = new_version_job.id
        new_j_job_characteristic.save

      end

      redirect_to new_version_job

    else

      flash[:danger] = t('activerecord.error.model.j_job.create_version_error')
      redirect_to j_jobs_new_version_path(j_job.id)

    end


  end

  def edit_jj_characteristics
    @j_job = JJob.find(params[:j_job_id])
    @j_characteristic_type = JCharacteristicType.find(params[:j_characteristic_type_id])
  end

  def update_jj_characteristics
    @j_job = JJob.find(params[:j_job_id])
    @j_characteristic_type = JCharacteristicType.find(params[:j_characteristic_type_id])

    characteristics = @j_characteristic_type.public_jj_characteristics

    characteristics.each do |jj_characteristic|

      new_value = nil

      if jj_characteristic.data_type_id == 11

        #register

        current_jjcs_ids = Array.new

        @j_job.j_job_jj_characteristics_registered(jj_characteristic).each {|c_uc| current_jjcs_ids.push c_uc.id.to_s}

        if params['characteristic_'+jj_characteristic.id.to_s+'_create']

          n_aux_files = Array.new

          jj_characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|
            n_aux_files[index_element] = 0
          end

          params['characteristic_'+jj_characteristic.id.to_s+'_create'].each_with_index do |c_id, n_aux|

            if c_id == '-1'

              error, jjc = @j_job.add_j_job_jj_characteristic(jj_characteristic, '-', @company)

              jj_characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                new_value = nil

                unless element_characteristic.data_type_id == 6
                  new_value = params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s).to_sym][n_aux].blank?
                else
                  #file

                  if params[('characteristic_file_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux] == '1'

                    new_value = params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)] && params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]]

                    n_aux_files[index_element] += 1

                  end

                end

                if new_value

                  saved_in, jjc_element = @j_job.add_j_job_jj_characteristic(element_characteristic, new_value, @company)
                  if saved_in && jjc_element

                    jjc_element.register_job_jj_characteristic = jjc
                    jjc_element.save

                  end

                end

              end

            else

              current_jjcs_ids.delete(c_id)

              jjc = @j_job.j_job_jj_characteristics.where('id = ?', c_id).first

              jj_characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                new_value = nil

                jjc_element = jjc.elements_j_job_jj_characteristics_by_characteristic(element_characteristic)

                unless element_characteristic.data_type_id == 6

                  jjc_element.destroy if jjc_element

                  new_value = params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s).to_sym][n_aux].blank?

                  saved_in, jjc_element = @j_job.add_j_job_jj_characteristic(element_characteristic, new_value, @company)

                  if saved_in && jjc_element

                    jjc_element.register_job_jj_characteristic = jjc
                    jjc_element.save

                  end

                else
                  #file

                  if params[('characteristic_file_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux] == '1'

                    new_value = params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)] && params[('characteristic_'+jj_characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]]

                    if new_value

                      n_aux_files[index_element] += 1

                      if jjc_element
                        jjc_element.delete_file @company
                        jjc_element.destroy
                      end

                      saved_in, jjc_element = @j_job.add_j_job_jj_characteristic(element_characteristic, new_value, @company)

                      if saved_in && jjc_element

                        jjc_element.register_job_jj_characteristic = jjc
                        jjc_element.save

                      end

                    end

                  end

                end

              end

            end

          end

        end

        current_jjcs_ids.each { |current_uc_id| @j_job.j_job_jj_characteristics.where('id = ?', current_uc_id).destroy_all }

      else

        if jj_characteristic.data_type_id != 13

          j_job_jj_characteristic = @j_job.j_job_jj_characteristic jj_characteristic

          error_val_sec_1 = false

          if jj_characteristic.val_sec_1

            j_char_company = JCharacteristic.first

            if j_char_company

              j_c_company = @j_job.j_job_characteristic j_char_company.characteristic

              if j_c_company

                JJob.where('j_code = ? AND id <> ?', @j_job.j_code, @j_job.id).each do |j_o|

                  j_c_o = j_o.j_job_characteristic j_char_company.characteristic
                  jj_c_o = j_o.j_job_jj_characteristic jj_characteristic

                  if j_c_o && jj_c_o && j_job_jj_characteristic

                    if j_c_company.characteristic_value_id == j_c_o.characteristic_value_id && jj_c_o.jj_characteristic_value_id.to_s == params[('characteristic_'+jj_characteristic.id.to_s).to_sym]

                      error_val_sec_1 = true
                      flash[:danger] = 'Nivel duplicado'

                    end

                  end


                end

              end

            end

          end

          unless error_val_sec_1

            j_job_jj_characteristic.destroy if j_job_jj_characteristic

            unless jj_characteristic.data_type_id == 6
              new_value = params[('characteristic_'+jj_characteristic.id.to_s).to_sym].strip unless params[('characteristic_'+jj_characteristic.id.to_s).to_sym].blank?
            else
              #file
              new_value = params[('characteristic_'+jj_characteristic.id.to_s).to_sym]
            end

            @j_job.add_j_job_jj_characteristic(jj_characteristic, new_value, @company)

          end

        end

      end

    end

    flash[:success] = t('activerecord.success.model.user_managinig.update_characteristics_ok')

    @j_job.save

    redirect_to @j_job

  end

  def destroy
    @j_job = JJob.find(params[:id])

    j_job_id = @j_job.id

    j_job_origin = @j_job.j_job

    if @j_job.current && @j_job.j_jobs.count == 0 && UserJJob.where('j_job_id = ? AND to_date IS NOT NULL', j_job_id).count == 0 && @j_job.destroy
      flash[:success] = t('activerecord.success.model.j_job.delete_ok')

      new_j_job_id = nil

      if j_job_origin

        new_current_job = j_job_origin.j_jobs.reorder('version DESC').first

        if new_current_job

          new_current_job.current = true
          new_current_job.to_date = nil
          new_current_job.save

          new_j_job_id = new_current_job.id

        else

          j_job_origin.current = true
          j_job_origin.to_date = nil
          j_job_origin.save

          new_j_job_id = j_job_origin.id

        end

        new_j_job = JJob.find new_j_job_id

        characteristic = Characteristic.where('data_type_id = ?',13).first

        User.where('j_job_id = ?', j_job_id).each do |user|
          user.j_job_id = new_j_job_id
          user.save

          if characteristic

            user_characteristic = user.user_characteristic characteristic

            if user_characteristic
              user_characteristic.update_value new_j_job.name, @company
              user_characteristic.save

            end

            user.update_first_owned_node(new_j_job.name)

            user_j_job_last = user.user_j_jobs.last
            if user_j_job_last && user_j_job_last.to_date.nil?

              user_characteristic_record = user.user_characteristic_records.where('characteristic_id = ? AND DATE(from_date) = ? AND to_date IS NULL', characteristic.id, user_j_job_last.from_date).first

              if user_characteristic_record

                user_characteristic_record.update_record_value new_j_job.name
                user_characteristic_record.save

              end

            end

          end

        end

      else

        if characteristic

          User.where('j_job_id = ?', j_job_id).each do |user|

            user_characteristic = user.user_characteristic characteristic

            user_characteristic.destroy if user_characteristic

            user.update_first_owned_node(nil)

            user_j_job_last = user.user_j_jobs.last
            if user_j_job_last && user_j_job_last.to_date.nil?

              user_characteristic_record = user.user_characteristic_records.where('characteristic_id = ? AND DATE(from_date) = ? AND to_date IS NULL', characteristic.id, user_j_job_last.from_date).first

              user_characteristic_record.destroy if user_characteristic_record

            end

          end

        end

      end



      UserJJob.where('j_job_id = ?', j_job_id).each do |user_j_job|
        user_j_job.j_job_id = new_j_job_id
        user_j_job.save
      end

      if new_j_job_id
        redirect_to j_job_path new_j_job_id
      else
        redirect_to j_jobs_url
      end
    else
      flash[:danger] = t('activerecord.error.model.j_job.delete_error')
      redirect_to @j_job
    end
  end

  def download_jj_characteristic_file

    @j_job = JJob.find(params[:j_job_id])
    @jj_characteristic = JjCharacteristic.find(params[:jj_characteristic_id])

    j_job_jj_characteristic = @j_job.j_job_jj_characteristic @jj_characteristic

    directory = @company.directorio+'/'+@company.codigo+'/j_job_profiles/'+@j_job.id.to_s+'/characteristics/'+@jj_characteristic.id.to_s+'/'


    send_file directory+j_job_jj_characteristic.value_file,
              filename: @jj_characteristic.name+'_'+j_job_jj_characteristic.value_file,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'jobs', true).first

      unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end

end
