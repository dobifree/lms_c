class HrProcessEvaluationQSchedulesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @hr_process_evaluation_q_schedules = HrProcessEvaluationQSchedule.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @hr_process_evaluation_q_schedules }
    end
  end

  # GET /hr_process_evaluation_q_schedules/1
  # GET /hr_process_evaluation_q_schedules/1.json
  def show
    @hr_process_evaluation_q_schedule = HrProcessEvaluationQSchedule.find(params[:id])

    respond_to do |format|
      format.html # managege.html.erb
      format.json { render json: @hr_process_evaluation_q_schedule }
    end
  end


  def new

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_process = @hr_process_evaluation.hr_process

    @hr_process_evaluation_q_schedule = @hr_process_evaluation.hr_process_evaluation_q_schedules.new


  end

  def edit
    @hr_process_evaluation_q_schedule = HrProcessEvaluationQSchedule.find(params[:id])
    @hr_process_evaluation = @hr_process_evaluation_q_schedule.hr_process_evaluation
    @hr_process = @hr_process_evaluation.hr_process
  end

  def create
    @hr_process_evaluation_q_schedule = HrProcessEvaluationQSchedule.new(params[:hr_process_evaluation_q_schedule])

    if @hr_process_evaluation_q_schedule.save
      flash[:success] = t('activerecord.success.model.hr_process_evaluation_q_schedule.create_ok')
      redirect_to  @hr_process_evaluation_q_schedule.hr_process_evaluation.hr_process
    else

      @hr_process_evaluation = HrProcessEvaluation.find(@hr_process_evaluation_q_schedule.hr_process_evaluation_id)
      @hr_process = @hr_process_evaluation.hr_process

      render action: 'new'
    end

  end

  def update

    @hr_process_evaluation_q_schedule = HrProcessEvaluationQSchedule.find(params[:id])

    if @hr_process_evaluation_q_schedule.update_attributes(params[:hr_process_evaluation_q_schedule])
      flash[:success] = t('activerecord.success.model.hr_process_evaluation_q_schedule.update_ok')
      redirect_to  @hr_process_evaluation_q_schedule.hr_process_evaluation.hr_process
    else
      @hr_process_evaluation =@hr_process_evaluation_q_schedule.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process

      render action: 'edit'
    end

  end

  def destroy
    @hr_process_evaluation_q_schedule = HrProcessEvaluationQSchedule.find(params[:id])
    @hr_process_evaluation =@hr_process_evaluation_q_schedule.hr_process_evaluation
    @hr_process = @hr_process_evaluation.hr_process

    @hr_process_evaluation_q_schedule.destroy

    flash[:success] = t('activerecord.success.model.hr_process_evaluation_q_schedule.delete_ok')
    redirect_to hr_process_evaluation_qs_path @hr_process_evaluation_q_schedule.hr_process_evaluation

  end
end
