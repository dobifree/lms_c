class PeFeedbackConfirmationController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:show, :confirm]
  before_filter :validate_open_process, only: [:show, :confirm]

  def show
    @hr_process_user = @hr_process_user_connected
   #@company = session[:company]
    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions
    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)
  end

  def confirm

    @hr_process_user_connected.feedback_recibido = params[:hr_process_user][:feedback_recibido]

    @hr_process_user_connected.save

    flash[:success] = t('activerecord.success.model.hr_process_assessment.feedback_ok')

    redirect_to pe_feedback_confirmation_show_path(@hr_process)


  end

  private

    def get_data_1
      @hr_process = HrProcess.find(params[:hr_process_id])
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)

    end

    def validate_open_process

      unless @hr_process.abierto && @hr_process.fecha_inicio < lms_time && @hr_process.fecha_fin > lms_time
        flash[:danger] = t('activerecord.error.model.hr_process_assessment.out_of_time')
        redirect_to root_path
      end

    end

end
