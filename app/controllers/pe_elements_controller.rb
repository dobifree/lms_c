class PeElementsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show
    @pe_element = PeElement.find(params[:id])
    @pe_evaluation = @pe_element.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end


  def new

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    @can_choose_not_assessed = true if @pe_evaluation.pe_elements.size == 0 || (@pe_evaluation.pe_elements.size > 0 && !@pe_evaluation.pe_elements.last.assessed)

    @pe_process = @pe_evaluation.pe_process
    @pe_element = @pe_evaluation.pe_elements.build

    @rank_goals = Array.new
    @rank_percentages = Array.new

    @discrete_goals = Array.new
    @discrete_percentages = Array.new

  end

  def create

    @rank_goals = Array.new
    @rank_percentages = Array.new

    @discrete_goals = Array.new
    @discrete_percentages = Array.new

    @pe_element = PeElement.new(params[:pe_element])

    @pe_evaluation = PeEvaluation.find(params[:pe_element][:pe_evaluation_id])

    if @pe_evaluation.pe_elements.size > 0
      @pe_element.pe_element = @pe_evaluation.pe_elements.last
      @pe_element.position = @pe_evaluation.pe_elements.size + 1
    end

    @pe_element.element_def_by_rol = ''
    params[:pe_element][:element_def_by_rol].each { |e| @pe_element.element_def_by_rol += e+'-' }

=begin
    @pe_element.assessment_method_def_by_rol = ''
    params[:pe_element][:assessment_method_def_by_rol].each { |e| @pe_element.assessment_method_def_by_rol += e+'-' }
=end

    @pe_element.available_indicator_types = ''
    if params[:pe_element][:available_indicator_types] && params[:pe_element][:available_indicator_types].size > 0
      params[:pe_element][:available_indicator_types].each { |t| @pe_element.available_indicator_types += t+'-' }
    end

    if @pe_element.save

      if params[:pe_question_rank_non_discrete_model_create]

        params[:pe_question_rank_non_discrete_model_create].each_with_index do |rank_model, index|

          pe_question_rank_model = @pe_element.pe_question_rank_models.build

          pe_question_rank_model.discrete = false

          pe_question_rank_model.goal = params[:pe_question_rank_non_discrete_model][index][:goal]

          pe_question_rank_model.percentage = params[:pe_question_rank_non_discrete_model][index][:percentage]

          pe_question_rank_model.save

        end

      end

      if params[:pe_question_rank_discrete_model_create]

        params[:pe_question_rank_discrete_model_create].each_with_index do |rank_model, index|

          pe_question_rank_model = @pe_element.pe_question_rank_models.build

          pe_question_rank_model.discrete = true

          pe_question_rank_model.discrete_goal = params[:pe_question_rank_discrete_model][index][:goal]

          pe_question_rank_model.percentage = params[:pe_question_rank_discrete_model][index][:percentage]

          pe_question_rank_model.save

        end

      end

      redirect_to @pe_element
    else

      @pe_process = @pe_evaluation.pe_process

      render action: 'new'

    end

  end

  def edit

    @pe_element = PeElement.find(params[:id])
    @pe_evaluation = @pe_element.pe_evaluation
    @pe_process = @pe_evaluation.pe_process

    @can_choose_not_assessed = true if (@pe_element.pe_element && !@pe_element.pe_element.assessed) || @pe_element.pe_element.nil?


  end

  def update

    @pe_element = PeElement.find(params[:id])


    if @pe_element.update_attributes(params[:pe_element])

      @pe_element.element_def_by_rol = ''
      params[:pe_element][:element_def_by_rol].each { |e| @pe_element.element_def_by_rol += e+'-' }
=begin
      @pe_element.assessment_method_def_by_rol = ''
      params[:pe_element][:assessment_method_def_by_rol].each { |e| @pe_element.assessment_method_def_by_rol += e+'-' }
=end

      @pe_element.available_indicator_types = ''
      if params[:pe_element][:available_indicator_types] && params[:pe_element][:available_indicator_types].size > 0
        params[:pe_element][:available_indicator_types].each { |t| @pe_element.available_indicator_types += t+'-' }
      end

      @pe_element.save

      #@pe_element.pe_question_rank_models.destroy_all
      puts params[:pe_question_rank_non_discrete_model]
      if params[:pe_question_rank_non_discrete_model_create]

        params[:pe_question_rank_non_discrete_model_create].each_with_index do |rank_model, index|

          if rank_model == '0'

            pe_question_rank_model = @pe_element.pe_question_rank_models.build

          else

            pe_question_rank_model = @pe_element.pe_question_rank_models.where('id = ?', rank_model).first

          end

          pe_question_rank_model.discrete = false

          pe_question_rank_model.goal = params[:pe_question_rank_non_discrete_model][index][:goal]

          pe_question_rank_model.percentage = params[:pe_question_rank_non_discrete_model][index][:percentage]

          pe_question_rank_model.save

        end

      end

      if params[:pe_question_rank_discrete_model_create]

        params[:pe_question_rank_discrete_model_create].each_with_index do |rank_model, index|

          if rank_model == '0'

            pe_question_rank_model = @pe_element.pe_question_rank_models.build

          else

            pe_question_rank_model = @pe_element.pe_question_rank_models.where('id = ?', rank_model).first

          end

          pe_question_rank_model.discrete = true

          pe_question_rank_model.discrete_goal = params[:pe_question_rank_discrete_model][index][:goal]

          pe_question_rank_model.percentage = params[:pe_question_rank_discrete_model][index][:percentage]

          pe_question_rank_model.save

        end

      end

      redirect_to @pe_element
    else

      @pe_evaluation = PeEvaluation.find(params[:pe_element][:pe_evaluation_id])
      @pe_process = @pe_evaluation.pe_process

      render action: 'edit'
    end

  end

  def destroy

    @pe_element = PeElement.find(params[:id])
    pe_evaluation = @pe_element.pe_evaluation
    @pe_element.destroy

    redirect_to pe_evaluation

  end
end
