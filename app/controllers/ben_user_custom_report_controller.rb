class BenUserCustomReportController < ApplicationController
  def index
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people
    @user = User.new
    @users = {}
    if params[:user]
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
    @users_q = BenUserCustomReport.all
    @users_q = @users_q.uniq_by(&:user_id)
  end

  def edit_user_privileges
    @user = User.find(params[:user_id])
    @report = BenCustomReport.find(params[:ben_custom_report_id])
    user_report = BenUserCustomReport.where(ben_custom_report_id: @report.id, user_id: @user.id).first
    if user_report
      user_report.destroy
    else
      BenUserCustomReport.create(ben_custom_report_id: @report.id, user_id: @user.id)
    end
    flash[:success] = 'Cambio realizado correctamente'
    redirect_to ben_user_custom_report_index_path
  end
end
