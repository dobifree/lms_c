class BrgReportsController < ApplicationController
  include BrgModule
  include BrgReportsHelper

  before_filter :authenticate_user
  before_filter :active_module
  before_filter :brg_manager

  def new_simple_report
    partial_report = defined?(params[:partial_report]) ? params[:partial_report] : 0
    partial_report = partial_report == 1
    report = BrgReport.new(partial_report: partial_report, brg_process_id: params[:brg_process_id])
    @report = set_registered(report)
    last_report = @report.partial_report ? BrgReport.active.from_process(@report.brg_process_id).only_partial.first : BrgReport.active.from_process(@report.brg_process_id).non_partial.first
    deactivate(last_report).save if last_report
    @report.save
    set_bonuses(@report)
    render xlsx: 'brg_simple_report.xlsx', filename: @report.brg_process.name
  end
end