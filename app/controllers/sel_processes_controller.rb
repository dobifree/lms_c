class SelProcessesController < ApplicationController
  before_filter :authenticate_user, except: [:download_file] # download_file es público
  before_filter :authorized_to_manage_processes, only: [:new, :create, :destroy]
  before_filter :authorized_to_processes, except: [:new, :create, :destroy, :download_file, :download_attach_file_from_applicant] # download_file es público

  # GET /sel_processes
  # GET /sel_processes.json
  def index
    if is_sel_process_manager?
      @sel_processes = SelProcess.scoped
    else
      @sel_processes = SelProcess.active().owner(session[:user_connected_id])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json {render json: @sel_processes}
    end
  end

  # GET /sel_processes/1
  # GET /sel_processes/1.json
  def show
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

   #@company = session[:company]

    count_steps_template = @sel_process.sel_template.sel_steps.count
    @new_applicants = @sel_process.sel_applicants.count - SelStepCandidate.joins(:sel_process_step).where(sel_process_steps: {:sel_process_id => @sel_process.id}).count('distinct jm_candidate_id')


    @new_selectables = SelApplicant.find_by_sql('select sel_applicants.*
                                                from sel_applicants INNER JOIN (
                                                select jm_candidate_id, sel_process_steps.sel_process_id
                                                from sel_step_candidates inner join sel_process_steps
                                                on (sel_step_candidates.sel_process_step_id = sel_process_steps.id)
                                                where (done = 1 OR exonerated = 1)
                                                and sel_process_steps.sel_process_id = ' + @sel_process.id.to_s + '
                                                group by jm_candidate_id, sel_process_steps.sel_process_id
                                                having count(*) = ' + count_steps_template.to_s + '
                                                ) as selectables
                                                on (sel_applicants.jm_candidate_id = selectables.jm_candidate_id and sel_applicants.sel_process_id = selectables.sel_process_id)
                                                where selected = 0 and excluded = 0').count

    @new_able_to_hire = SelApplicant.where(sel_process_id: @sel_process.id, selected: true, hired: false, excluded: false).count

    @show_step_open = false
    if params[:sel_step_id]
      @show_step_open = true
      @sel_step_id_open = params[:sel_step_id]
    end

  end

  # GET /sel_processes/new
  # GET /sel_processes/new.json
  def new
    @sel_process = SelProcess.new
    @sel_process.sel_owners.build
    @sel_process.sel_claimants.build

    # sugerencia del codigo autoincremental con ceros hasta la izquierda. longitud 8
    code_to_sugest = sprintf("%08d", ((SelProcess.last.nil? ? 0 : SelProcess.last.id) + 1))
    @sel_process.code = code_to_sugest


    validate_requirement_to_show()

  end

  # GET /sel_processes/1/edit
  def edit
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    if @sel_process.sel_step_candidates.count > 0
      @disable_template_id = true
    end

    if @sel_process.sel_owners.count == 0
      @sel_process.sel_owners.build
    end

    if @sel_process.sel_claimants.count == 0
      @sel_process.sel_claimants.build
    end
  end

  # POST /sel_processes
  # POST /sel_processes.json
  def create
    @sel_process = SelProcess.new(params[:sel_process])
    @sel_process.registered_by_user_id = user_connected.id
    @sel_process.registered_at = lms_time

    respond_to do |format|
      if @sel_process.save
        # generación del log de 'en curso' de la solicitud
        sel_req = @sel_process.sel_requirement
        if sel_req && sel_req.current_state_log.log_state_id != 7
          log = sel_req.sel_req_logs.new
          log.log_state_id = 7
          log.registered_at = lms_time
          log.registered_by_user_id = user_connected.id
          log.save
        end
        sel_req.sel_req_processes.where(active: true).update_all(active: false) if sel_req
        # generación del log de 'en curso' de la solicitud
        if params[:file]
          name = SecureRandom.hex(5).to_s + File.extname(params[:file].original_filename)
          if save_file(params[:file], name, @sel_process, @company)
            @sel_process.update_attribute(:file, name)
          end
        end

        generate_steps(@sel_process)

        flash[:success] = 'El proceso se ha creado satisfactoriamente'
        format.html {redirect_to sel_processes_path}
        format.json {render json: @sel_process, status: :created, location: @sel_process}
      else
        flash.now[:danger] = 'El proceso no se ha podido crear satisfactoriamente'
        format.html {
          validate_requirement_to_show()
          render action: "new"}
        format.json {render json: @sel_process.errors, status: :unprocessable_entity}
      end
    end
  end

  # PUT /sel_processes/1
  # PUT /sel_processes/1.json
  def update
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################


    if @sel_process.finished != params[:sel_process][:finished].to_s
      if @sel_process.finished?
        params[:sel_process][:finished_at] = ''
        params[:sel_process][:finished_by_user_id] = nil
      else
        params[:sel_process][:finished_at] = lms_time
        params[:sel_process][:finished_by_user_id] = user_connected.id
      end
    end


    if @sel_process.sel_template_id.to_s != params[:sel_process][:sel_template_id].to_s
      is_new_template = true
    end

    if (is_new_template && @sel_process.sel_step_candidates.count == 0) || (!is_new_template)
      if @sel_process.update_attributes(params[:sel_process])

        if params[:file]
          name = SecureRandom.hex(5).to_s + File.extname(params[:file].original_filename)
          if save_file(params[:file], name, @sel_process, @company)
            @sel_process.update_attribute(:file, name)
          end
        end

        if is_new_template
          generate_steps(@sel_process)
        end
        flash[:success] = 'El proceso se ha actualizado satisfactoriamente'
        redirect_to @sel_process
      else
        flash.now[:danger] = 'El proceso no se ha podido actualizar satisfactoriamente'
        render action: 'edit'
      end
    else
      flash[:danger] = 'El proceso ya está en curso y no se puede actualizar la Plantilla asociada'
      redirect_to edit_sel_process_path @sel_process
    end


  end

  # DELETE /sel_processes/1
  # DELETE /sel_processes/1.json
  def destroy
    @sel_process = SelProcess.find(params[:id])

    if @sel_process.sel_step_candidates.none?
      @sel_process.destroy
      flash[:success] = 'El proceso se ha eliminado correctamente'
      redirect_to sel_processes_path
    else
      flash[:danger] = 'El proceso no ha podido ser eliminado correctamente.'
      redirect_to @sel_process
    end
  end

  def show_config_particular_agenda
    sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])
    suggested_appointment_description = sel_step_candidate.sel_process_step.sel_step.name + ' - ' + sel_step_candidate.jm_candidate.full_name_2

    if sel_step_candidate.sel_appointment && sel_step_candidate.sel_appointment.sel_step_candidates.count == 1
      sel_appointment = sel_step_candidate.sel_appointment
    else
      sel_appointment = SelAppointment.new(:sel_process_step_id => sel_step_candidate.sel_process_step_id, :description => suggested_appointment_description)
      sel_step_candidate.sel_appointment = sel_appointment

    end
    render partial: 'config_particular_agenda', locals: {:sel_appointment => sel_appointment, :sel_step_candidate => sel_step_candidate}
  end

  def config_particular_agenda
    sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])
    if sel_step_candidate.sel_appointment.nil?
      sel_appointment = SelAppointment.new(params[:sel_appointment])
      sel_appointment.sel_process_step_id = sel_step_candidate.sel_process_step_id
      sel_appointment.registered_by_user_id = user_connected.id
      sel_appointment.registered_at = lms_time
      sel_appointment.save
      sel_step_candidate.sel_appointment = sel_appointment
      sel_step_candidate.save
    else
      sel_appointment = sel_step_candidate.sel_appointment.update_attributes(params[:sel_appointment])
      sel_appointment.registered_by_user_id = user_connected.id
      sel_appointment.registered_at = lms_time
      sel_appointment.save
    end
    redirect_to sel_process_show_open_step_path(sel_step_candidate.sel_process_step.sel_process_id, sel_step_candidate.sel_process_step_id)
  end

  def exclude_applicant
    sel_applicant = SelApplicant.find(params[:sel_applicant_id])
    if sel_applicant.hired?
      flash[:danger] = 'El candidato ya está contratado, no se puede excluir'
    else
      sel_applicant.excluded = true
      sel_applicant.excluded_at = lms_time
      sel_applicant.excluded_by_user_id = user_connected.id
      sel_applicant.save

      flash[:success] = 'El candidato fue excluído correctamente'
    end
    redirect_to sel_process_show_open_step_path(sel_applicant.sel_process_id, 'applicants')
  end

  def notify_exclude_applicant
    sel_applicant = SelApplicant.find(params[:sel_applicant_id])
    if sel_applicant.jm_candidate.internal_user?
      notification_template = SelNotificationTemplate.where(active: true, event_id: 16).first
    else
      notification_template = SelNotificationTemplate.where(active: true, event_id: 10).first
    end

    if notification_template
      notification = notification_template.generate_notification({jm_candidate: sel_applicant.jm_candidate, sel_process: sel_applicant.sel_process, trigger_user: user_connected}, user_connected, lms_time)
      SelMailer.sel_notification(notification, @company).deliver
    end
    flash[:success] = 'Notificación enviada correctamente'
    redirect_to sel_process_show_open_step_path(sel_applicant.sel_process_id, 'applicants')
  end

  def undo_exclude_applicant
    sel_applicant = SelApplicant.find(params[:sel_applicant_id])
    sel_applicant.excluded = false
    sel_applicant.excluded_at = nil
    sel_applicant.excluded_by_user_id = nil
    sel_applicant.save
    flash[:success] = 'El candidato fue incluído correctamente'

    redirect_to sel_process_show_open_step_path(sel_applicant.sel_process_id, 'applicants')
  end


  def download_file
    sel_process = SelProcess.find(params[:id])

   company = @company
    #/storage/<%= @company.codigo %>/sel_processes/<%= @sel_process.id.to_s %>/ficha/<%= @sel_process.file
    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + sel_process.id.to_s + '/ficha/'
    #name = SecureRandom.hex(5).to_s + File.extname(upload.original_filename)
    full_path = File.join(directory, sel_process.file)

    send_file full_path,
              filename: sel_process.code + File.extname(sel_process.file),
              type: 'application/octet-stream',
              disposition: 'attachment'
  end


  def show_detail_applicants_list
   #@company = session[:company]
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    #if @sel_process.sel_applicants.count > 0
    render 'detail_applicants_list', :layout => false
    #else
    # render 'show_detail_not_available', :layout => false
    #end
  end

  def enroll_applicants_to_first_steps
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    if params[:applicants]
      params[:applicants].each_key do |applicant_id|
        enroll_applicant_to_process(applicant_id)
      end
    end

    flash[:success] = 'Los candidatos fueron enrolados en los siguientes pasos correctamente'
    redirect_to sel_process_show_open_step_path(@sel_process.id, 'applicants')

  end


  def show_detail_process_step
    @sel_process_step = SelProcessStep.find(params[:sel_process_step_id])

    ################################################################
    validate_user_process(@sel_process_step.sel_process_id)
    ################################################################

    if @sel_process_step.sel_step.step_type == 1 && @sel_process_step.sel_step_candidates.count > 0
      prev_filter_step = SelProcessStep.joins(:sel_step).
          where(:sel_process_id => @sel_process_step.sel_process_id, sel_steps: {:step_type => 1}).
          where('sel_steps.position < ?', @sel_process_step.sel_step.position).order('sel_steps.position DESC').first

      initial_position = prev_filter_step ? prev_filter_step.sel_step.position : -1000000000

      @sel_process_steps_prev = SelProcessStep.joins(:sel_step).
          where(:sel_process_id => @sel_process_step.sel_process_id).
          where('sel_steps.position < ? AND sel_steps.position > ?', @sel_process_step.sel_step.position, initial_position)
    end


    if @sel_process_step.sel_step_candidates.joins(:jm_candidate => :sel_applicants).where(:sel_applicants => {sel_process_id: @sel_process_step.sel_process_id, excluded: false}).count > 0
      render 'show_detail_process_step', :layout => false
    else
      render 'show_detail_not_available', :layout => false
    end

  end

  def show_detail_selection_step
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    @sel_applicants_ordered_by_priority = @sel_process.sel_applicants_ordered_by_priority


    @sel_applicants = []
    count_steps_template = @sel_process.sel_template.sel_steps.count

    @sel_applicants_ordered_by_priority.each do |applicant|


      if is_candidate_selectable?(count_steps_template, applicant)
        @sel_applicants.push(applicant)
      end


      ####################
      prev_filter_step = SelProcessStep.joins(:sel_step).
          where(:sel_process_id => @sel_process.id, sel_steps: {:step_type => 1}).
          order('sel_steps.position DESC').first

      initial_position = prev_filter_step ? prev_filter_step.sel_step.position : -1000000000

      @sel_process_steps_prev = SelProcessStep.joins(:sel_step).
          where(:sel_process_id => @sel_process.id).
          where('sel_steps.position > ?', initial_position)
      ####################


    end

    if @sel_applicants.count > 0
      render 'detail_selection_step', :layout => false
    else
      render 'show_detail_not_available', :layout => false
    end
  end

  def show_detail_validation_step
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    @sel_applicants_ordered_by_name = SelApplicant.joins(:jm_candidate).where(sel_process_id: @sel_process.id, selected: true, excluded: false).reorder('surname, name')

    if @sel_applicants_ordered_by_name.count > 0
      render 'detail_validation_step', :layout => false
    else
      render 'show_detail_not_available', :layout => false
    end
  end


  def show_assign_attendants
    @sel_process_step = SelProcessStep.find(params[:id])

    ################################################################
    validate_user_process(@sel_process_step.sel_process_id)
    ################################################################

    @characteristics = Characteristic.all
    @characteristics_prev = {}
    render layout: 'flat'

  end

  def assign_attendants

    if params[:sel_step_candidate_id]
      sel_step_candidate2 = Object.new()
      params[:sel_step_candidate_id].each_key do |sel_step_candidate_id|
        sel_step_candidate = SelStepCandidate.find(sel_step_candidate_id)


        ################################################################
        validate_user_process(sel_step_candidate.sel_process_step.sel_process_id)
        ################################################################


        sel_step_candidate2 = sel_step_candidate
        SelAttendant.destroy_all(:sel_step_candidate_id => sel_step_candidate.id, :role => params[:role])


        if params[:user]

          # Necesario para poder tener un valor 'sin asignar'
          params[:user].delete_if {|value| value == ''}

          params[:user].each do |user_id|

            sel_attendant = SelAttendant.new()
            sel_attendant.sel_step_candidate_id = sel_step_candidate.id
            sel_attendant.user_id = user_id.to_s
            sel_attendant.role = params[:role]

            if !sel_attendant.save
              flash[:danger] = sel_attendant.user.full_name + ' ya ha sido asignado a otro rol. No se puede volver a asignar'
            else
              flash[:success] = 'Los responsables fueron asignados correctamente'
            end
          end
        end
      end
      redirect_to sel_process_show_assign_users_path(sel_step_candidate2.sel_process_step)
    else
      flash[:danger] = 'No seleccionó ningún candidato, no se pudo guardar nada'
      redirect_to :back
    end

  end


  def exonerate_step_candidate
    sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])

    ################################################################
    validate_user_process(sel_step_candidate.sel_process_step.sel_process_id)
    ################################################################
    if step_candidate_exonerate_editable?(sel_step_candidate)
      sel_step_candidate.update_attributes(:exonerated => true, :exonerated_comment => params[:comment], :exonerated_at => lms_time, :exonerated_by_user_id => user_connected.id)
    end

    flash[:success] = 'El candidato fue exonerado del paso correctamente'
    redirect_to sel_process_show_open_step_path(sel_step_candidate.sel_process_step.sel_process, sel_step_candidate.sel_process_step)
  end

  def undo_exonerate_step_candidate
    sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])

    ################################################################
    validate_user_process(sel_step_candidate.sel_process_step.sel_process_id)
    ################################################################
    if step_candidate_exonerate_editable?(sel_step_candidate)
      sel_step_candidate.update_attributes(:exonerated => false)
    end
    flash[:success] = 'Se revirtió la exoneración del paso correctamente'
    redirect_to sel_process_show_open_step_path(sel_step_candidate.sel_process_step.sel_process, sel_step_candidate.sel_process_step)
  end

  def execute_step_filter
    sel_process_step = SelProcessStep.find(params[:id])

    ################################################################
    validate_user_process(sel_process_step.sel_process_id)
    ################################################################


    steps_to_enroll = next_until_filter_steps(sel_process_step.sel_process, sel_process_step.sel_step.position)

    if params[:sc]
      params[:sc].each_key do |sel_step_candidate_id|
        sel_step_candidate_filter = SelStepCandidate.find(sel_step_candidate_id)
        if can_pass_filter_step?(sel_step_candidate_filter)
          step_candidate2 = SelStepCandidate.find(sel_step_candidate_id)
          steps_to_enroll.each do |sel_process_step|
            step_candidate = SelStepCandidate.new()
            step_candidate.sel_process_step_id = sel_process_step.id
            step_candidate.jm_candidate_id = step_candidate2.jm_candidate_id
            step_candidate.registered_at = lms_time
            step_candidate.registered_by_user_id = user_connected.id

            if step_candidate.save
              step_candidate.sel_process_step.sel_process_attendants.each do |sel_process_attendant|
                automatic_sel_attendant = SelAttendant.new()
                automatic_sel_attendant.sel_step_candidate_id = step_candidate.id
                automatic_sel_attendant.user_id = sel_process_attendant.user_id
                automatic_sel_attendant.role = sel_process_attendant.role
                automatic_sel_attendant.automatic_assign = true
                automatic_sel_attendant.save
              end
            end
          end

          sel_step_candidate_filter.done = true
          sel_step_candidate_filter.save
        end
      end
    end
    flash[:success] = 'Los candidatos fueron enrolados en los siguientes pasos correctamente'
    redirect_to sel_process_show_open_step_path(sel_process_step.sel_process_id, sel_process_step.id)
  end

  def select_candidates

    @sel_process = SelProcess.find(params[:id])
    count_steps_template = @sel_process.sel_template.sel_steps.count

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    if params[:applicant]
      params[:applicant].each_key do |applicant_id|
        sel_applicant = SelApplicant.where(:id => applicant_id, :sel_process_id => @sel_process.id).first
        if is_candidate_selectable?(count_steps_template, sel_applicant)
          sel_applicant.selected = true
          sel_applicant.selected_at = lms_time
          sel_applicant.selected_by = user_connected.id
          sel_applicant.save
          flash[:success] = 'Los candidatos fueron seleccionados correctamente'
        end
      end
    else
      flash[:danger] = 'No hay cambios para guardar'
    end

    redirect_to sel_process_show_open_step_path(@sel_process.id, 'selection')
  end

  def hire_candidates
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    if params[:applicant]
      params[:applicant].each_key do |applicant_id|
        if sel_applicant = SelApplicant.where(:id => applicant_id, :sel_process_id => @sel_process.id, :selected => true).first
          sel_applicant.hired = true
          sel_applicant.hired_at = lms_time
          sel_applicant.hired_by = user_connected.id
          sel_applicant.save
          flash[:success] = 'Los candidatos fueron seleccionados correctamente'
        end
      end
    else
      flash[:danger] = 'No hay cambios para guardar'
    end

    redirect_to sel_process_show_open_step_path(@sel_process.id, 'validation')
  end

  def show_assign_step_attendants
    @sel_process = SelProcess.find(params[:id])
    @users = User.find_all_by_activo(true).map {|user| [user.codigo + ' - ' + user.full_name, user.id]}

    render layout: 'flat'
  end

  def assign_step_attendants
    @sel_process = SelProcess.find(params[:id])

    if params[:sel_process_step_id]
      # sel_step2 = Object.new()
      params[:sel_process_step_id].each_key do |sel_process_step_id|
        sel_process_step = SelProcessStep.find(sel_process_step_id)


        ################################################################
        validate_user_process(sel_process_step.sel_process_id)
        ################################################################

        SelProcessAttendant.destroy_all(:sel_process_step_id => sel_process_step.id, :role => params[:role])


        if params[:user]

          # Necesario para poder tener un valor 'sin asignar'
          params[:user].delete_if {|value| value == ''}

          params[:user].each do |user_id|

            sel_process_attendant = SelProcessAttendant.new()
            sel_process_attendant.sel_process_step_id = sel_process_step.id
            sel_process_attendant.user_id = user_id.to_s
            sel_process_attendant.role = params[:role]

            if !sel_process_attendant.save
              flash[:danger] = sel_process_attendant.user.full_name + ' ya ha sido asignado a otro rol. No se puede volver a asignar'
            else
              flash[:success] = 'Los responsables fueron asignados correctamente'
            end
          end
        end
      end
      redirect_to sel_processes_show_assign_step_attendants_path(@sel_process)
    else
      flash[:danger] = 'No seleccionó ningún paso, no se pudo guardar nada'
      redirect_to :back
    end
  end

  ################################
  # undo steps

  def undo_validation

    sel_applicant = SelApplicant.find(params[:sel_applicant_id])

    ################################################################
    validate_user_process(sel_applicant.sel_process_id)
    ################################################################


    if sel_applicant.hired?
      SelApplicant.include_root_in_json = true
      sel_applicant.update_attribute(:hired, false)

      flash[:success] = 'Se reversó la validación correctamente'

    else
      flash[:danger] = 'El candidato no está validado, no se puede reversar.'
    end
    redirect_to sel_process_show_open_step_path(sel_applicant.sel_process_id, 'validation')
  end

  def undo_selection

    sel_applicant = SelApplicant.find(params[:sel_applicant_id])

    ################################################################
    validate_user_process(sel_applicant.sel_process_id)
    ################################################################

    if !sel_applicant.hired?
      if sel_applicant.selected?
        SelApplicant.include_root_in_json = true
        sel_applicant.update_attributes(:selected => false, :priority => nil)

        flash[:success] = 'Se reversó la selección correctamente'

      else
        flash[:danger] = 'El candidato no está seleccionado, no se puede reversar.'
      end
    else
      flash[:danger] = 'El candidato ya está validado, no se puede reversar.'
    end

    redirect_to sel_process_show_open_step_path(sel_applicant.sel_process_id, 'selection')
  end

  def undo_steps_until_next_filter

    sel_step_candidate_trigger = nil

    if params[:sel_step_candidate_id]

      sel_step_candidate_trigger = SelStepCandidate.find(params[:sel_step_candidate_id])
      sel_process_id = sel_step_candidate_trigger.sel_process_step.sel_process_id
      step_to_redirect_to = sel_step_candidate_trigger.sel_process_step_id

      if can_undo_filter_step(sel_step_candidate_trigger)
        ################################################################
        validate_user_process(sel_process_id)
        ################################################################
        step_candidates = next_step_candidate_until_filter(sel_step_candidate_trigger, nil)
      else
        flash[:danger] = 'Este paso no puede revertirse debido a que existe pasos de filtro, selección o validación posteriores ya realizados'
      end


    elsif params[:sel_applicant_id]
      sel_applicant = SelApplicant.find(params[:sel_applicant_id])
      sel_process_id = sel_applicant.sel_process_id
      step_to_redirect_to = 'applicants'
      if can_undo_filter_step_from_applicant(sel_applicant)
        ################################################################
        validate_user_process(sel_process_id)
        ################################################################
        step_candidates = next_step_candidate_until_filter(nil, sel_applicant)
      else
        flash[:danger] = 'Este paso no puede revertirse debido a que existe pasos de filtro, selección o validación posteriores ya realizados'
      end
    end

    if step_candidates
      # delete steps
      step_candidates.each do |step_candidate_to_destroy|
        step_candidate_to_destroy.destroy
      end
      if sel_step_candidate_trigger
        sel_step_candidate_trigger.update_attribute(:done, false)
      end
      flash[:success] = 'Se revirtieron los pasos correctamente'
    end

    redirect_to sel_process_show_open_step_path(sel_process_id, step_to_redirect_to)

  end


  ################################


  ## Add candidate to process
  #############################################
  def show_add_candidate_to_process
    @sel_process = SelProcess.find(params[:id])
    @external_candidates = JmCandidate.where(:internal_user => false).order(:surname)
    @user = User.new(params[:user])

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

    render layout: 'flat'
  end

  def validate_if_email_registered

    sel_process = SelProcess.find(params[:id])
    candidate = JmCandidate.where(:email => params[:email]).first
    if candidate.nil?
      redirect_to sel_processes_new_candidate_path(sel_process.id, params[:email])
    elsif sel_process.jm_candidates.include?(candidate)
      flash[:danger] = 'El usuario ya se encuentra registrado en el proceso'
      redirect_to sel_processes_show_add_candidate_to_process_path(sel_process.id)
    else
      redirect_to sel_processes_apply_form_candidate_path(sel_process.id, candidate.id)
    end

  end


  def apply_form_candidate
    @jm_candidate = JmCandidate.find(params[:jm_candidate_id])
    @sel_process = SelProcess.find(params[:id])

    if @sel_process.sel_applicants.joins(:jm_candidate).where(:jm_candidates => {:id => @jm_candidate.id}).any?
      flash[:danger] = 'El usuario ya se encuentra registrado en el proceso'
      redirect_to sel_processes_show_add_candidate_to_process_path(@sel_process.id)
    else
      ## se crea el applicant
      @sel_applicant = @jm_candidate.sel_applicants.build
      @sel_applicant.sel_process = @sel_process

      # se crean las preguntas específicas
      @sel_process.sel_apply_specific_questions.each do |question|
        @sel_applicant.sel_applicant_specific_answers.new(:sel_apply_specific_question_id => question.id)
      end
      #######

      render layout: 'flat'
    end
  end

  def new_candidate
    @jm_candidate = JmCandidate.new
    @jm_candidate.email = params[:email]
    render layout: 'flat'
  end

  def create_candidate
    sel_process = SelProcess.find(params[:id])
    @jm_candidate = JmCandidate.new(params[:jm_candidate])
    @jm_candidate.password = SecureRandom.hex(6).to_s

    if @jm_candidate.save
      #flash[:success] = 'El candidato fue añadido en el proceso correctamente'
      redirect_to sel_processes_apply_form_candidate_path(sel_process.id, @jm_candidate.id)
    else
      flash.now[:danger] = 'El candidato no pudo ser creado correctamente'
      render 'new_candidate', layout: 'flat'
    end

  end

  def create_internal_candidate
    sel_process = SelProcess.find(params[:id])
    @user = User.find(params[:user_id])

    unless jm_candidate = JmCandidate.where(:user_id => @user.id).first
      jm_candidate = JmCandidate.new()
      jm_candidate.name = @user.nombre
      jm_candidate.surname = @user.apellidos
      jm_candidate.internal_user = true
      jm_candidate.user_id = @user.id
      jm_candidate.email = ''
      jm_candidate.password = SecureRandom.hex(6).to_s

      unless jm_candidate.save
        flash[:danger] = 'El ' + alias_user + ' no pudo ser añadido correctamente'
        redirect_to sel_processes_show_add_candidate_to_process_path(sel_process)
      end
    end

    redirect_to sel_processes_apply_form_candidate_path(sel_process.id, jm_candidate.id)

  end

  def enroll_candidate
    @sel_process = SelProcess.find(params[:id])
    @jm_candidate = JmCandidate.find(params[:jm_candidate_id])
    #process_applicable_to_candidate()

   #@company = session[:company]

    @sel_applicant = SelApplicant.new(params[:sel_applicant])
    @sel_applicant.sel_process = @sel_process
    @sel_applicant.jm_candidate = @jm_candidate
    specific_questions_attachment_to_save = []
    @sel_applicant.sel_applicant_specific_answers.each do |specific_answer|
      if specific_answer.attachment_name
        specific_answer.attachment_code = SecureRandom.hex(5)
        specific_answer.sel_applicant = @sel_applicant
        specific_questions_attachment_to_save.append(specific_answer.dup)
        specific_answer.attachment_name = specific_answer.attachment_name.original_filename
      end
      specific_answer.registered_at = lms_time
      specific_answer.registered_by_user_id = user_connected.id
    end

    @jm_candidate.transaction do
      if @sel_applicant.save
        files_to_save = []

        @jm_candidate.assign_attributes(params[:jm_candidate])
        @jm_candidate.jm_candidate_answers.each_with_index do |answer|
          if answer.new_record?
            answer.registered_at = lms_time
            answer.registered_by_user_id = user_connected.id
          end
          answer.sel_applicant_records.each do |record|
            record.sel_applicant = @sel_applicant
          end
          answer.jm_answer_values.each do |value|

            if value.new_record? && value.value_file
              value.value_file_crypted_name = SecureRandom.hex(5)
              files_to_save.append(value.dup)
              value.value_file = value.value_file.original_filename
            end
          end
        end

        @jm_candidate.save

        files_to_save.each do |answer_value|
          file = answer_value.value_file
          save_file_application(file, @company, answer_value)
        end

        specific_questions_attachment_to_save.each do |specific_answer|
          file = specific_answer.attachment_name
          specific_answer.sel_applicant = @sel_applicant
          save_specific_attach_application(file, @company, specific_answer)
        end

        enroll_applicant_to_process(@sel_applicant.id)
        flash[:success] = 'El candidato fue añadido a este proceso correctamente'
        redirect_to sel_processes_show_add_candidate_to_process_path(@sel_process.id)
      else
        raise ActiveRecord::Rollback, 'Call tech support!'
        flash[:danger] = 'Ooops... esto es vergonzoso, puedes intentar postular nuevamente?'
        redirect_to :back
      end
    end
  end

  #############################################


  def show_attach_file_to_applicant

    @sel_applicant = SelApplicant.find(params[:sel_applicant_id])
    @sel_applicant_attachment = SelApplicantAttachment.new()
    @sel_applicant_attachment.sel_applicant = @sel_applicant

  end

  def attach_file_to_applicant
    @sel_applicant = SelApplicant.find(params[:sel_applicant_id])

    @sel_applicant_attachment = SelApplicantAttachment.new(params[:sel_applicant_attachment])
    @sel_applicant_attachment.sel_applicant_id = @sel_applicant.id
    @sel_applicant_attachment.crypted_name = SecureRandom.hex(5).to_s
    @sel_applicant_attachment.registered_at = lms_time
    @sel_applicant_attachment.registered_by_user_id = user_connected.id
    @sel_applicant_attachment.original_filename = params[:file].original_filename

    if @sel_applicant_attachment.save
      save_file_to_applicant(params[:file], @sel_applicant_attachment)
      flash[:success] = 'Se adjunto el archivo correctamente'
      redirect_to sel_processes_show_attach_file_to_applicant_path(@sel_applicant)
    else

      render 'show_attach_file_to_applicant'

    end


  end

  def download_attach_file_from_applicant

    @sel_applicant_attachment = SelApplicantAttachment.find_by_complex_crypted_name(params[:attachment_crypted_name])

   company = @company
    #/storage/<%= @company.codigo %>/sel_processes/<%= @sel_process.id.to_s %>/ficha/<%= @sel_process.file
    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + @sel_applicant_attachment.sel_applicant.sel_process_id.to_s + '/' + @sel_applicant_attachment.sel_applicant.jm_candidate_id.to_s + '/attachments/'
    name = @sel_applicant_attachment._crypted_name + File.extname(@sel_applicant_attachment.original_filename)
    full_path = File.join(directory, name)

    send_file full_path,
              filename: (@sel_applicant_attachment.description + '_' + @sel_applicant_attachment.sel_applicant.jm_candidate.surname)[0...255] + File.extname(@sel_applicant_attachment.original_filename),
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def delete_attach_file_from_applicant
    sel_applicant_attachment = SelApplicantAttachment.find_by_complex_crypted_name(params[:attachment_crypted_name])
    sel_applicant_attachment.destroy
    render :json => true
  end

  def download_specific_answer_attach_file_from_applicant

    sel_applicant = SelApplicant.find(params[:sel_applicant_id])
    sel_specific_answer  = sel_applicant.sel_applicant_specific_answers.where(attachment_code: params[:attachment_code]).first

    company = @company
    #/storage/<%= @company.codigo %>/sel_processes/<%= @sel_process.id.to_s %>/ficha/<%= @sel_process.file
    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + sel_applicant.sel_process_id.to_s + '/jm_candidates/' + sel_applicant.jm_candidate_id.to_s + '/specific_questions_attachments/'
    name = sel_specific_answer.attachment_code + File.extname(sel_specific_answer.attachment_name)
    full_path = File.join(directory, name)

    send_file full_path,
              filename: (sel_specific_answer.sel_apply_specific_question.question +  sel_applicant.jm_candidate.surname[0...255]) + '_' + File.extname(sel_specific_answer.attachment_name),
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def show_set_priority

    @sel_process = SelProcess.find(params[:id])
    @selected_applicants = @sel_process.sel_applicants_ordered_by_priority.where(selected: true, hired: false, excluded: false)
    render :layout => 'flat'

  end

  def set_priority
    @sel_process = SelProcess.find(params[:id])

    if params[:priority]

      #se ordenan por el campo priority para poder guardar el index+1 en lugar del valor ingresado
      Hash[params[:priority].sort_by {|k, v| Integer(v)}].each_with_index do |applicant_priority, index|
        SelApplicant.find(applicant_priority[0]).update_attribute(:priority, index + 1)
      end
      flash[:success] = 'La prioridad se guardó correcatemente.'
    end

    redirect_to sel_processes_show_set_priority_path(@sel_process)
  end


  def show_config_appointments

    @sel_process_step = SelProcessStep.find(params[:sel_process_step_id])

    if !@sel_process_step.sel_appointments.any?
      @sel_process_step.sel_appointments.build
    end

    render layout: 'flat'
  end

  def config_appointments

    @sel_process_step = SelProcessStep.find(params[:sel_process_step_id])

    if @sel_process_step.update_attributes(params[:sel_process_step])
      @sel_process_step.sel_appointments.update_all(:registered_at => lms_time, :registered_by_user_id => user_connected.id)
      flash[:success] = 'Los datos fueron guardados correctamente'
      redirect_to sel_processes_show_config_appointments_path(@sel_process_step)
    else
      render 'show_config_appointments', layout: 'flat'
    end


  end

  def show_assign_appointments
    @sel_process_step = SelProcessStep.find(params[:sel_process_step_id])

    render layout: 'flat'
  end

  def assign_appointments
    @sel_process_step = SelProcessStep.find(params[:sel_process_step_id])

    sel_appointment_id = Integer(params[:sel_appointment_id].keys[0].to_s)


    if SelStepCandidate.where(:sel_appointment_id => sel_appointment_id).update_all(:sel_appointment_id => nil) &&
        SelStepCandidate.where(:id => params[:sel_step_candidates]).update_all(:sel_appointment_id => sel_appointment_id)
      flash[:success] = 'Los datos fueron guardados correctamente'
    end

    redirect_to sel_processes_show_set_appointments_path @sel_process_step


  end


  def show_config_characteristics
    @sel_process_step = SelProcessStep.find(params[:sel_process_step_id])

    #@sel_process_step.sel_process_characteristics.where(:sel_characteristic_id => params[:sel_characteristic_id])

    @sel_characteristic = SelCharacteristic.find(params[:sel_characteristic_id])

    @is_editable = @sel_process_step.sel_step_results.joins(:sel_step_candidate).where(sel_step_candidates: {:done => true}).count > 0 ? false : true


    if !@sel_characteristic.configurable?
      flash[:danger] = 'Esta característica no es configurable'
      redirect_to :back
    end

  end

  def config_characteristics
    @sel_process_step = SelProcessStep.find(params[:sel_process_step_id])
    @sel_characteristic = SelCharacteristic.find(params[:sel_characteristic_id])

    if @sel_characteristic.configurable?

      # se añaden los campos no que no imputa el usuario
      params[:sel_process_step][:sel_process_characteristics_attributes].each_key do |proc_char_id|
        params[:sel_process_step][:sel_process_characteristics_attributes][proc_char_id][:sel_characteristic_id] = @sel_characteristic.id
        params[:sel_process_step][:sel_process_characteristics_attributes][proc_char_id][:registered_at] = lms_time
        params[:sel_process_step][:sel_process_characteristics_attributes][proc_char_id][:registered_by_user_id] = user_connected.id
      end

      if @sel_process_step.update_attributes(params[:sel_process_step])
        #@sel_process_step.sel_process_characteristics.update_all(:sel_characteristic_id => @sel_characteristic.id, :registered_at => lms_time, :registered_by_user_id => user_connected.id)
        flash[:success] = 'Los datos fueron guardados correctamente'
        redirect_to sel_processes_show_config_characteristics_path(@sel_process_step)
      else
        render 'show_config_characteristics'
      end
    else
      flash[:danger] = 'Esta característica no es configurable'
      redirect_to sel_process_show_open_step_path(@sel_process_step.sel_process, @sel_process_step.id)
    end


  end


  def undo_set_results_done
    sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])

    if is_step_candidate_reaperturable?(sel_step_candidate)
      sel_step_candidate.update_attribute(:done, false)

      flash[:success] = 'Se reabrió la evaluación correctamente'
    else
      flash[:danger] = 'No se pudo reabir la evaluación correctamente'
    end

    redirect_to sel_process_show_open_step_path(sel_step_candidate.sel_process_step.sel_process, sel_step_candidate.sel_process_step_id)
  end


  def show_set_comment_sel_applicant
    @sel_applicant = SelApplicant.find(params[:sel_applicant_id])

    render layout: false
  end

  def set_comment_sel_applicant
    sel_applicant = SelApplicant.find(params[:sel_applicant_id])

    if sel_applicant.update_attributes(:comment => params[:comment], :comment_by_user_id => user_connected.id, :comment_at => lms_time)
      flash[:success] = 'El comentario fue registrado correctamente'
    else
      flash[:danger] = 'El comentario no pudo ser registrado correctamente'
    end

    redirect_to sel_process_show_open_step_path(sel_applicant.sel_process_id, 'validation')
  end

  def benchmark_sel_applicants

    @sel_process = SelProcess.find(params[:id])

    render 'applicants_benchmark', layout: 'flat'
  end

  def list_configured_events

    @sel_process = SelProcess.find(params[:id])

    render layout: 'flat'

  end


  def list_evaluation_attachments
    @sel_step_candidate = SelStepCandidate.find(params[:sel_step_candidate_id])
    render :partial => 'sel_results/list_attach_from_step_candidate', :layout => false
  end

  def remove_applicant
    sel_applicant = SelApplicant.find(params[:sel_applicant_id])
    if is_candidate_enrollable?(sel_applicant)
      sel_applicant.destroy
      flash[:success] = 'El candidato fue eliminado correctamente del proceso'
    else
      flash[:danger] = 'El candidato no pudo eliminado correctamente del proceso'
    end
    redirect_to sel_process_show_open_step_path(sel_applicant.sel_process_id, 'applicants')
  end


  def sync_applicant_char_answers
    sel_applicant = SelApplicant.find(params[:sel_applicant_id])
    jm_characteristic = JmCharacteristic.find(params[:jm_characteristic_id])

    if jm_characteristic.simple
      prev_answer = sel_applicant.jm_candidate_answers.where(:jm_characteristic_id => jm_characteristic.id).first
      current_answer = sel_applicant.jm_candidate.jm_candidate_answers.where(:active => true, :jm_characteristic_id => jm_characteristic.id).first

      if prev_answer && !prev_answer.active?
        prev_answer.sel_applicant_records.where(:sel_applicant_id => sel_applicant.id).destroy_all
      end

      if current_answer
        current_answer.sel_applicant_records.where(:sel_applicant_id => sel_applicant.id).first_or_create
      end
      render :partial => 'jm_my_profiles/my_profile_answer_simple', :locals => {:answer => current_answer, :jm_characteristic => jm_characteristic}
    else
      prev_answers = sel_applicant.jm_candidate_answers.where(:jm_characteristic_id => jm_characteristic.id)
      current_answers = sel_applicant.jm_candidate.jm_candidate_answers.where(:active => true, :jm_characteristic_id => jm_characteristic.id)

      prev_answers.each do |prev_answer|
        unless prev_answer.active?
          prev_answer.sel_applicant_records.where(:sel_applicant_id => sel_applicant.id).destroy_all
        end
      end

      current_answers.each do |current_answer|
        current_answer.sel_applicant_records.where(:sel_applicant_id => sel_applicant.id).first_or_create
      end


      render :partial => 'jm_my_profiles/my_profile_answer_complex', :locals => {:answers => current_answers, :jm_characteristic => jm_characteristic}
    end
  end

  private

  def enroll_applicant_to_process(applicant_id)
    sel_applicant = SelApplicant.find(applicant_id)
    if is_candidate_enrollable?(sel_applicant)
      ##################################
      next_until_filter_steps(@sel_process).each do |sel_process_step|
        step_candidate = SelStepCandidate.new()
        step_candidate.sel_process_step_id = sel_process_step.id
        step_candidate.jm_candidate_id = sel_applicant.jm_candidate_id
        step_candidate.registered_at = lms_time
        step_candidate.registered_by_user_id = user_connected.id

        if step_candidate.save

          step_candidate.sel_process_step.sel_process_attendants.each do |sel_process_attendant|
            automatic_sel_attendant = SelAttendant.new()
            automatic_sel_attendant.sel_step_candidate_id = step_candidate.id
            automatic_sel_attendant.user_id = sel_process_attendant.user_id
            automatic_sel_attendant.role = sel_process_attendant.role
            automatic_sel_attendant.automatic_assign = true
            automatic_sel_attendant.save
          end
        end
      end
      ##################################
    end
  end

  def validate_user_process(sel_process_id)
    if is_my_own_process?(sel_process_id) || !(is_sel_process_manager? || SelOwner.where(:user_id => session[:user_connected_id], :sel_process_id => sel_process_id).count > 0)
      flash[:danger] = 'Usted no tiene privilegios para administrar este proceso.'
      redirect_to root_path
    end
  end

  def is_my_own_process?(sel_process_id)
    me_as_candidate = JmCandidate.where(internal_user: true, user_id: user_connected.id).first
    me_as_candidate.sel_applicants.where(sel_process_id: sel_process_id).any? if me_as_candidate
  end

  def is_candidate_selectable?(count_steps_template, applicant)
    count_steps_template == SelStepCandidate.joins(:sel_process_step).where(:jm_candidate_id => applicant.jm_candidate_id, sel_process_steps: {:sel_process_id => applicant.sel_process_id}).where('sel_step_candidates.done = 1 OR sel_step_candidates.exonerated = 1').count
  end

  def generate_steps(sel_process)

    #SelProcessStep.where('sel_process_id = ?', sel_process.id.to_s).delete_all
    sel_process.sel_process_steps.destroy_all
    sel_process.sel_template.sel_steps.each do |sel_step|
      sel_process_step = SelProcessStep.new()
      sel_process_step.sel_process_id = sel_process.id
      sel_process_step.sel_step_id = sel_step.id

      if sel_process_step.save
        sel_process_step.sel_step.sel_template_attendants.each do |sel_template_attendant|
          sel_process_attendant = SelProcessAttendant.new()
          sel_process_attendant.sel_process_step_id = sel_process_step.id
          sel_process_attendant.user_id = sel_template_attendant.user_id
          sel_process_attendant.role = sel_template_attendant.role
          sel_process_attendant.automatic_assign = true
          sel_process_attendant.save
        end
      end
    end

  end

  def save_file(upload, name, sel_process, company)

    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + sel_process.id.to_s + '/ficha/'
    #name = SecureRandom.hex(5).to_s + File.extname(upload.original_filename)
    full_path = File.join(directory, name)

    FileUtils.remove_dir directory if File.directory? directory
    FileUtils.mkdir_p directory

    File.open(full_path, 'wb') {|f| f.write(upload.read)}
  end


  def save_file_application(file, company, jm_answer_value)
    # sel_processes tiene una copia de este método para poder enrollar desde el proceso
    file_crypted_name = jm_answer_value.value_file_crypted_name + File.extname(file.original_filename)
    candidate_id = @jm_candidate.id.to_s
    directory = company.directorio + '/' + company.codigo + '/jm_candidates/' + candidate_id + '/'

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') {|f| f.write(file.read)}


  end

  def save_specific_attach_application(file, company, specific_answer)
    # sel_processes tiene una copia de este método para poder enrollar desde el proceso
    file_crypted_name = specific_answer.attachment_code + File.extname(file.original_filename)
    candidate_id = @jm_candidate.id.to_s
    process_id = specific_answer.sel_applicant.sel_process_id.to_s
    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + process_id + '/jm_candidates/' + candidate_id + '/specific_questions_attachments/'

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') {|f| f.write(file.read)}
  end

  def generate_crypted_name(file, sel_applicant_value)

    sel_applicant_value.id.to_s + '_' + sel_applicant_value.crypted_name + File.extname(file.original_filename)

  end

  def save_file_to_applicant(file, sel_applicant_attachment)

   company = @company
    file_crypted_name = sel_applicant_attachment._crypted_name + File.extname(file.original_filename)
    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + sel_applicant_attachment.sel_applicant.sel_process_id.to_s + '/' + sel_applicant_attachment.sel_applicant.jm_candidate_id.to_s + '/attachments/'

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') {|f| f.write(file.read)}


  end

  def validate_requirement_to_show
    @show_requirement_data = false
    ####################################################################################
    ### paso de parámetros desde el requerimiento al seleccionarse para 'atender'

    # llega desde la pantalla de requerimientos (solicitudes pendientes)
    if params[:sel_requirement_id]
      sel_requirement_id = params[:sel_requirement_id]
      # si existe un error en el llenado del formulario
    elsif params[:sel_process] && params[:sel_process][:sel_requirement_id]
      sel_requirement_id = @sel_process.sel_requirement_id
    end

    if sel_requirement_id
      @show_requirement_data = true
      @sel_requirement = SelRequirement.find(sel_requirement_id)
      @sel_process.sel_requirement_id = @sel_requirement.id
      @sel_process.sel_claimants.first.user_id = @sel_requirement.registered_by_user_id
    end
    ####################################################################################
  end

end
