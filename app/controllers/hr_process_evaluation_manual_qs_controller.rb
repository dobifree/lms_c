class HrProcessEvaluationManualQsController < ApplicationController

  before_filter :authenticate_user

  before_filter :acceso_ver_proceso, only: [:new, :ver_usuario, :confirmar, :habilitar_definicion]
  before_filter :acceso_new, only: [:new]
  before_filter :acceso_create, only: [:create]
  before_filter :acceso_update_edit_destroy, only: [:edit, :update, :destroy]
  before_filter :acceso_confirmar, only: [:confirmar]
  before_filter :acceso_gestionar_proceso , only: [:habilitar_definicion]

  def ver_usuario


   #@company = session[:company]

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    @hr_process_evaluation_type_element_nivel_2 = @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 2).first

  end

  def show
    @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.find(params[:id])

    respond_to do |format|
      format.html # show.html.erbrb
      format.json { render json: @hr_process_evaluation_manual_q }
    end
  end

  def new

   #@company = session[:company]

    @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.new

    @hr_process_evaluation_manual_q.hr_evaluation_type_element_id = @hr_evaluation_type_element.id
    @hr_process_evaluation_manual_q.hr_process_evaluation_id = @hr_process_evaluation.id

    if @hr_process_user
      @hr_process_evaluation_manual_q.hr_process_user_id = @hr_process_user.id
    else
      @hr_process_evaluation_manual_q.hr_process_user_id = @hr_process_user_connected.id
    end

    if params[:hr_process_evaluation_manual_q_id]

      @hr_process_evaluation_manual_q_padre = HrProcessEvaluationManualQ.find(params[:hr_process_evaluation_manual_q_id])
      @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q_id = @hr_process_evaluation_manual_q_padre.id

    end

    transform_meta_rango_into_form

  end

  def create

   #@company = session[:company]

    if params[:hr_process_evaluation_manual_q][:tipo_indicador] == '4'

      transform_meta_rango_into_form

      if @hr_evaluation_type_element.aplica_evaluacion && @porcentajes.first < @hr_evaluation_type_element.valor_minimo || @porcentajes.last > @hr_evaluation_type_element.valor_maximo

        flash.now[:danger] = 'El porcentaje de logro debe ser >= '+@hr_evaluation_type_element.valor_minimo.to_s+' y <= '+@hr_evaluation_type_element.valor_maximo.to_s

        if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
          @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
        end

        render action: 'new'

      else

        if @hr_process_evaluation_manual_q.save
          flash[:success] = t('activerecord.success.model.hr_process_evaluation_manual_q.create_ok')

          if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
            @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q.hr_evaluation_type_element
          end

          if @hr_process_evaluation_manual_q.hr_process_user.id == @hr_process_user_connected.id

            redirect_to hr_process_assessment_carga_manual_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element)

          else

            redirect_to hr_process_evaluation_manual_q_ver_usuario_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element, @hr_process_user)

          end

        else
          if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
            @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
          end

          transform_meta_rango_into_form

          render action: 'new'
        end


      end

    elsif @hr_evaluation_type_element.aplica_evaluacion && ( params[:hr_process_evaluation_manual_q][:peso].blank? || (@hr_evaluation_type_element.peso_minimo && params[:hr_process_evaluation_manual_q][:peso].to_f < @hr_evaluation_type_element.peso_minimo) || (@hr_evaluation_type_element.peso_maximo && params[:hr_process_evaluation_manual_q][:peso].to_f > @hr_evaluation_type_element.peso_maximo))

      #params[:hr_process_evaluation_manual_q][:meta_rango_indicador] = transform_form_into_meta_rango params

      #@hr_process_evaluation_manual_q.meta_rango_indicador = params[:hr_process_evaluation_manual_q][:meta_rango_indicador]

      transform_meta_rango_into_form

      flash.now[:danger] = 'El peso es obligatorio y debe ser >= '+@hr_evaluation_type_element.peso_minimo.to_s+' y <= '+@hr_evaluation_type_element.peso_maximo.to_s

      if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
        @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
      end

      render action: 'new'

    else

      if @hr_process_evaluation_manual_q.save
        flash[:success] = t('activerecord.success.model.hr_process_evaluation_manual_q.create_ok')

        if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
          @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q.hr_evaluation_type_element
        end

        if @hr_process_evaluation_manual_q.hr_process_user.id == @hr_process_user_connected.id

          redirect_to hr_process_assessment_carga_manual_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element)

        else

          redirect_to hr_process_evaluation_manual_q_ver_usuario_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element, @hr_process_user)

        end

      else
        if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
          @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
        end

        transform_meta_rango_into_form

        render action: 'new'
      end

    end

  end

  def edit

   #@company = session[:company]

    if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q

      @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q

    end

    transform_meta_rango_into_form


  end




  def update

   #@company = session[:company]

    if @hr_evaluation_type_element.aplica_evaluacion && params[:hr_process_evaluation_manual_q][:tipo_indicador] == '4'

      params[:hr_process_evaluation_manual_q][:meta_rango_indicador] = transform_form_into_meta_rango params

      @hr_process_evaluation_manual_q.meta_rango_indicador = params[:hr_process_evaluation_manual_q][:meta_rango_indicador]

      transform_meta_rango_into_form

      #puts @porcentajes.inspect

      if @porcentajes.first < @hr_evaluation_type_element.valor_minimo || @porcentajes.last > @hr_evaluation_type_element.valor_maximo

        flash.now[:danger] = 'El porcentaje de logro debe ser >= '+@hr_evaluation_type_element.valor_minimo.to_s+' y <= '+@hr_evaluation_type_element.valor_maximo.to_s

        if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
          @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
        end

        render action: 'edit'

      else

        if @hr_process_evaluation_manual_q.update_attributes(params[:hr_process_evaluation_manual_q])

          flash[:success] = t('activerecord.success.model.hr_process_evaluation_manual_q.update_ok')

          if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
            @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q.hr_evaluation_type_element
          end

          if @hr_process_evaluation_manual_q.hr_process_user.id == @hr_process_user_connected.id

            redirect_to hr_process_assessment_carga_manual_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element)

          else

            redirect_to hr_process_evaluation_manual_q_ver_usuario_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element, @hr_process_user)

          end

        else

          transform_meta_rango_into_form

          if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
            @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
          end

          render action: 'edit'

        end


      end

    elsif @hr_evaluation_type_element.aplica_evaluacion && ( params[:hr_process_evaluation_manual_q][:peso].blank? || (@hr_evaluation_type_element.peso_minimo && params[:hr_process_evaluation_manual_q][:peso].to_f < @hr_evaluation_type_element.peso_minimo) || (@hr_evaluation_type_element.peso_maximo && params[:hr_process_evaluation_manual_q][:peso].to_f > @hr_evaluation_type_element.peso_maximo))

      params[:hr_process_evaluation_manual_q][:meta_rango_indicador] = transform_form_into_meta_rango params

      @hr_process_evaluation_manual_q.meta_rango_indicador = params[:hr_process_evaluation_manual_q][:meta_rango_indicador]

      transform_meta_rango_into_form

      flash.now[:danger] = 'El peso es obligatorio y debe ser >= '+@hr_evaluation_type_element.peso_minimo.to_s+' y <= '+@hr_evaluation_type_element.peso_maximo.to_s

      if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
        @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
      end

      render action: 'edit'

    else

      params[:hr_process_evaluation_manual_q][:meta_rango_indicador] = transform_form_into_meta_rango params

      if @hr_process_evaluation_manual_q.update_attributes(params[:hr_process_evaluation_manual_q])

        flash[:success] = t('activerecord.success.model.hr_process_evaluation_manual_q.update_ok')

        if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
          @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q.hr_evaluation_type_element
        end

        if @hr_process_evaluation_manual_q.hr_process_user.id == @hr_process_user_connected.id

          redirect_to hr_process_assessment_carga_manual_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element)

        else

          redirect_to hr_process_evaluation_manual_q_ver_usuario_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element, @hr_process_user)

        end

      else

        transform_meta_rango_into_form

        if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
          @hr_process_evaluation_manual_q_padre = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
        end

        render action: 'edit'

      end

    end

  end

  def confirmar

   #@company = session[:company]

    @hr_process_evaluation.hr_process_evaluation_manual_qs.where('hr_process_user_id = ?',@hr_process_user.id).each do |hr_process_evaluation_manual_q|

      hr_process_evaluation_manual_q.confirmado = !hr_process_evaluation_manual_q.confirmado
      hr_process_evaluation_manual_q.save

    end

    redirect_to hr_process_evaluation_manual_q_ver_usuario_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element, @hr_process_user)

  end

  def habilitar_definicion

   #@company = session[:company]


    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])
    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id]) if params[:hr_process_user_id]

    @hr_process_evaluation.hr_process_evaluation_manual_qs.where('hr_process_user_id = ?',@hr_process_user.id).each do |hr_process_evaluation_manual_q|

      hr_process_evaluation_manual_q.confirmado = false
      hr_process_evaluation_manual_q.save

    end

    flash[:success] = t('activerecord.success.model.hr_process_assessment.habilitar_definicion_ok')

    redirect_to hr_process_assessment_autorizar_modificaciones_1_path @hr_process

  end

  def destroy


    if @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q
      @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q.hr_evaluation_type_element
    end

    if @hr_process_evaluation_manual_q.hr_process_user.id == @hr_process_user_connected.id
      @hr_process_evaluation_manual_q.destroy
      flash[:success] = t('activerecord.success.model.hr_process_evaluation_manual_q.delete_ok')
      redirect_to hr_process_assessment_carga_manual_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element)

    else
      @hr_process_evaluation_manual_q.destroy
      flash[:success] = t('activerecord.success.model.hr_process_evaluation_manual_q.delete_ok')
      redirect_to hr_process_evaluation_manual_q_ver_usuario_path(@hr_process, @hr_process_evaluation, @hr_evaluation_type_element, @hr_process_user)

    end


  end

  private

    def acceso_ver_proceso

      @hr_process = HrProcess.find(params[:hr_process_id])

      unless @hr_process.fecha_inicio <= lms_time && lms_time <= @hr_process.fecha_fin

        flash[:danger] = t('security.no_access_hr_perform_evaluation_time')
        redirect_to root_path

      end

    end

    def acceso_new

      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)

      @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
      @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])

      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id]) if params[:hr_process_user_id]

      definir_elemento_a_evaluar, definir_elemento_a_autoevaluar = verify_acceso_new_create_edit_update @hr_process_user_connected, @hr_process_user, @hr_process_evaluation, @hr_evaluation_type_element

      unless definir_elemento_a_evaluar || definir_elemento_a_autoevaluar

        flash[:danger] = t('security.no_access_create_hr_process_evaluation_manual_q')
        redirect_to root_path

      end

    end

    def acceso_create

      params[:hr_process_evaluation_manual_q][:meta_rango_indicador] = transform_form_into_meta_rango params

      @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.new(params[:hr_process_evaluation_manual_q])

      @hr_process_evaluation = @hr_process_evaluation_manual_q.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process

      unless @hr_process.fecha_inicio <= lms_time && lms_time <= @hr_process.fecha_fin

        flash[:danger] = t('security.no_access_hr_perform_evaluation_time')
        redirect_to root_path

      end

      @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_evaluation_type_element

      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
      #@hr_process_user = HrProcessUser.find(params[:hr_process_user_id]) if params[:hr_process_user_id]
      @hr_process_user = @hr_process_evaluation_manual_q.hr_process_user

      definir_elemento_a_evaluar, definir_elemento_a_autoevaluar = verify_acceso_new_create_edit_update @hr_process_user_connected, @hr_process_user, @hr_process_evaluation, @hr_evaluation_type_element

      unless definir_elemento_a_evaluar || definir_elemento_a_autoevaluar

        flash[:danger] = t('security.no_access_create_hr_process_evaluation_manual_q')
        redirect_to root_path

      end

    end

    def acceso_update_edit_destroy

      @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.find(params[:id])

      @hr_process_evaluation = @hr_process_evaluation_manual_q.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process

      unless @hr_process.fecha_inicio <= lms_time && lms_time <= @hr_process.fecha_fin

        flash[:danger] = t('security.no_access_hr_perform_evaluation_time')
        redirect_to root_path

      end

      @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_evaluation_type_element

      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
      @hr_process_user = @hr_process_evaluation_manual_q.hr_process_user if  @hr_process_evaluation_manual_q.hr_process_user.id != @hr_process_user_connected.id

      definir_elemento_a_evaluar, definir_elemento_a_autoevaluar = verify_acceso_new_create_edit_update @hr_process_user_connected, @hr_process_user, @hr_process_evaluation, @hr_evaluation_type_element

      unless definir_elemento_a_evaluar || definir_elemento_a_autoevaluar

        flash[:danger] = t('security.no_access_create_hr_process_evaluation_manual_q')
        redirect_to root_path

      end

    end

    def verify_acceso_new_create_edit_update(hr_process_user_connected, hr_process_user, hr_process_evaluation, hr_evaluation_type_element)

      definir_elemento_a_evaluar = false
      definir_elemento_a_autoevaluar = false

      if hr_evaluation_type_element.carga_manual

        hr_process_user_connected.hr_process_evalua_rels.each do |rel|

          if rel.tipo == 'jefe'

            if hr_process_user
              if rel.hr_process_user2_id == @hr_process_user.id
                definir_elemento_a_evaluar = true
                break
              end
            else
              definir_elemento_a_evaluar = true
              break
            end

          end

        end

        if hr_process_user.nil? && hr_evaluation_type_element.evaluacion_colaborativa && hr_process_evaluation.hr_process_dimension_e.eval_auto
          definir_elemento_a_autoevaluar = true
        elsif hr_process_user && hr_process_user.id == hr_process_user_connected.id && hr_process_evaluation.hr_process_dimension_e.eval_auto
          definir_elemento_a_autoevaluar = true
        end

      end

      return definir_elemento_a_evaluar, definir_elemento_a_autoevaluar

    end

    def acceso_confirmar

      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)

      @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
      @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])

      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id]) if params[:hr_process_user_id]

      definir_elemento_a_evaluar, definir_elemento_a_autoevaluar = verify_acceso_new_create_edit_update @hr_process_user_connected, @hr_process_user, @hr_process_evaluation, @hr_evaluation_type_element

      unless definir_elemento_a_evaluar

        flash[:danger] = t('security.no_access_create_hr_process_evaluation_manual_q')
        redirect_to root_path

      end

    end

    def acceso_gestionar_proceso

      @hr_process = HrProcess.find(params[:hr_process_id])

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

      unless hr_process_manager

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end

    def transform_meta_rango_into_form

      @metas = Array.new
      @porcentajes = Array.new

      if @hr_process_evaluation_manual_q.meta_rango_indicador

        @hr_process_evaluation_manual_q.meta_rango_indicador.split(',').each do |rango|
          valores = rango.split '-'
          @metas.push valores[0].to_f
          @porcentajes.push valores[1].to_f
        end
      end

    end

    def transform_form_into_meta_rango(params)

      meta_rango = ''

      metas = Array.new
      porcentajes = Array.new

      (1..10).each do |v|
        unless params["meta_#{v}".to_sym].blank?
          metas.push params["meta_#{v}".to_sym].to_f
          porcentajes.push params["porcentaje_#{v}".to_sym].to_f
        end
      end

      porcentajes = Hash[(0...porcentajes.size).zip porcentajes]
      porcentajes = porcentajes.sort_by { |pos, por| por}


      porcentajes.each do |k,v|
        meta_rango += ','+metas[k.to_i].to_f.to_s+'-'+v.to_s
      end

=begin
      metas = Hash[(0...metas.size).zip metas]
      metas = metas.sort_by { |pos, meta| meta}


      metas.each do |k,v|
        meta_rango += ','+v.to_s+'-'+porcentajes[k.to_i].to_f.to_s
      end
=end
      meta_rango.slice! 0
      meta_rango

    end

end
