class BrcProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  # GET /brc_processes
  # GET /brc_processes.json
  def index
    @brc_processes = BrcProcess.all
  end

  # GET /brc_processes/1
  # GET /brc_processes/1.json
  def show

    @characteristic_company = Characteristic.get_characteristic_company

    @brc_process = BrcProcess.find(params[:id])
    @brc_members_ordered = @brc_process.brc_members_ordered
    @brc_definers_ordered = @brc_process.brc_definers_ordered
  end

  # GET /brc_processes/new
  # GET /brc_processes/new.json
  def new
    @brc_process = BrcProcess.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @brc_process }
    end
  end

  # GET /brc_processes/1/edit
  def edit
    @brc_process = BrcProcess.find(params[:id])
  end

  # POST /brc_processes
  # POST /brc_processes.json
  def create

    @brc_process = BrcProcess.new(params[:brc_process])

    @brc_process.uf_value = fix_form_floats params[:brc_process][:uf_value] if params[:brc_process][:uf_value]

    if @brc_process.save
      flash[:success] = t('activerecord.success.model.brc_process.create_ok')
      redirect_to @brc_process
    else
      render action: 'new'
    end

  end

  # PUT /brc_processes/1
  # PUT /brc_processes/1.json
  def update
    @brc_process = BrcProcess.find(params[:id])

    params[:brc_process][:uf_value] = fix_form_floats params[:brc_process][:uf_value] if params[:brc_process][:uf_value]

    if @brc_process.update_attributes(params[:brc_process])
      flash[:success] = t('activerecord.success.model.brc_process.update_ok')
      redirect_to @brc_process
    else
      render action: 'edit'
    end
  end

  # DELETE /brc_processes/1
  # DELETE /brc_processes/1.json
  def destroy
    @brc_process = BrcProcess.find(params[:id])
    @brc_process.destroy

    respond_to do |format|
      format.html { redirect_to brc_processes_url }
      format.json { head :no_content }
    end
  end

  def download_file

    brc_process = BrcProcess.find(params[:brc_process_id])
    brc_member_file = BrcMemberFile.find(params[:brc_member_file_id])

    file = @company.directorio+'/'+@company.codigo+'/brc/'+brc_process.id.to_s+'/'+brc_member_file.brc_member.id.to_s+'/'+brc_member_file.file_name

    send_file file,
              filename: brc_member_file.brc_member.user.codigo+File.extname(brc_member_file.file_name),
              type: 'application/octet-stream',
              disposition: 'attachment'
  end

  def download_files

    files = ''

    brc_process = BrcProcess.find(params[:brc_process_id])

    brc_process.brc_members.each do |brc_member|

      brc_member.brc_member_files.each do |brc_member_file|

        file = @company.directorio+'/'+@company.codigo+'/brc/'+brc_process.id.to_s+'/'+brc_member_file.brc_member.id.to_s+'/'+brc_member_file.file_name

        if File.exist? file

          files += @company.directorio+'/'+@company.codigo+'/brc/'+brc_process.id.to_s+'/'+brc_member_file.brc_member.id.to_s+'/'+brc_member_file.file_name+' '

        end

      end

    end

    x = SecureRandom.hex(5)

    fn = "/tmp/proceso_brc_#{brc_process.year}_#{brc_process.period}_#{x}.zip"

    system "zip -j #{fn} #{files}"

    send_file fn,
              filename: "proceso_brc_#{brc_process.year}_#{brc_process.period}.zip",
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def reject_format
    brc_member = BrcMember.find(params[:brc_member_id])
    brc_member.update_attributes(params[:brc_member])
    brc_member.status_choose = false
    brc_member.save
    flash[:success] = t('activerecord.success.model.brc_process.reject_format_ok')
    redirect_to brc_member.brc_process
  end

  def remove_reject_format
    brc_member = BrcMember.find(params[:brc_member_id])
    brc_member.update_attributes(params[:brc_member])
    brc_member.save
    flash[:success] = t('activerecord.success.model.brc_process.reject_format_ok')
    redirect_to brc_member.brc_process
  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'brc', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

end
