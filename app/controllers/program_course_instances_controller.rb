class ProgramCourseInstancesController < ApplicationController
  # GET /program_course_instances
  # GET /program_course_instances.json
  def index
    @program_course_instances = ProgramCourseInstance.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @program_course_instances }
    end
  end

  # GET /program_course_instances/1
  # GET /program_course_instances/1.json
  def show
    @program_course_instance = ProgramCourseInstance.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @program_course_instance }
    end
  end

  # GET /program_course_instances/new
  # GET /program_course_instances/new.json
  def new
    @program_course_instance = ProgramCourseInstance.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @program_course_instance }
    end
  end

  # GET /program_course_instances/1/edit
  def edit
    @program_course_instance = ProgramCourseInstance.find(params[:id])
  end

  # POST /program_course_instances
  # POST /program_course_instances.json
  def create
    @program_course_instance = ProgramCourseInstance.new(params[:program_course_instance])

    respond_to do |format|
      if @program_course_instance.save
        format.html { redirect_to @program_course_instance, notice: 'Program course instance was successfully created.' }
        format.json { render json: @program_course_instance, status: :created, location: @program_course_instance }
      else
        format.html { render action: "new" }
        format.json { render json: @program_course_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /program_course_instances/1
  # PUT /program_course_instances/1.json
  def update
    @program_course_instance = ProgramCourseInstance.find(params[:id])

    respond_to do |format|
      if @program_course_instance.update_attributes(params[:program_course_instance])
        format.html { redirect_to @program_course_instance, notice: 'Program course instance was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @program_course_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /program_course_instances/1
  # DELETE /program_course_instances/1.json
  def destroy
    @program_course_instance = ProgramCourseInstance.find(params[:id])
    @program_course_instance.destroy

    respond_to do |format|
      format.html { redirect_to program_course_instances_url }
      format.json { head :no_content }
    end
  end
end
