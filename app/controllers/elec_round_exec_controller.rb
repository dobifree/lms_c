include ElecProcessesHelper
class ElecRoundExecController < ApplicationController

  before_filter :authenticate_user
  before_filter :load_variables, :except => [:load_user_mini_file]

  def show


    #render :layout => 'flat'
  end

  def show_detail_category

    @elec_votes_recorded = @elec_round.elec_votes.where(:user_id => user_connected.id, :elec_process_category_id => @elec_process_category.id)

    eligible_users_temp = User.where(:activo => true)

    # filtrando ya votados
    if @elec_votes_recorded.any?
      eligible_users_temp = eligible_users_temp.where('id NOT IN (?)', @elec_votes_recorded.pluck(:candidate_user_id))
    end
    ########################################################################################

    # filtrando self_vote
    if !@elec_process_category.self_vote?
      eligible_users_temp = eligible_users_temp.where('id <> ?', user_connected.id)
    end
    ########################################################################################

    # filtrando características de candidatos
    excluded_users = []

    users_id_temp = eligible_users_temp.pluck(:id)
    @elec_process_category.elec_category_characteristics.where(:filter_candidate => true).each do |filter_candidate|
      users_match = UserCharacteristic.where('UPPER(value_string) = ? AND characteristic_id = ?', filter_candidate.match_value, filter_candidate.characteristic_id)
      users_id_temp.each do |user_id|
        unless excluded_users.include?(user_id)
          value = users_match.where(:user_id => user_id).first
          excluded_users << user_id unless value
        end
      end
    end
=begin
    eligible_users_temp.each do |eligible_candidate|
      #counter = 0
      if @elec_process_category.elec_category_characteristics.where(:filter_candidate => true).each do |filter_candidate|
        if value = UserCharacteristic.where(:user_id => eligible_candidate.id, :characteristic_id => filter_candidate.characteristic_id).first
          #TODO: validar forma de comparación para las mayúsculas y las tildes
          if value.formatted_value.upcase != filter_candidate.match_value.upcase
            #counter = counter + 1
            # Si no cumple con alguno de los filtros, se excluye
            excluded_users << eligible_candidate.id unless excluded_users.include?(eligible_candidate.id)
          end
        else
          excluded_users << eligible_candidate.id unless excluded_users.include?(eligible_candidate.id)
        end
      end.empty?
        #counter = 1
      end

      #if counter == 0
      #excluded_users << eligible_candidate.id
      #end
    end
=end
    if excluded_users.any?
      eligible_users_temp = eligible_users_temp.where('id NOT IN (?)', excluded_users)
    end
    ########################################################################################
    # filtrando características de votante iguales al candidato (agrupador)
    group_by_chars = @elec_round.elec_process_round.elec_process.elec_characteristics.where(:used_to_group_by => true)

    if group_by_chars.any? # explícito para evitar un bucle de los seleccionables cuando no hay agrupador
      excluded_users2 = []
      eligible_users_temp.each do |eligible_candidate|
        counter = 0
        group_by_chars.each do |group_by_char|
          if (value_candidate = UserCharacteristic.where(:user_id => eligible_candidate.id, :characteristic_id => group_by_char.characteristic_id).first) &&
              (value_user = UserCharacteristic.where(:user_id => user_connected.id, :characteristic_id => group_by_char.characteristic_id).first)
            #TODO: validar forma de comparación para las mayúsculas y las tildes
            if value_candidate.valor == value_user.valor
              counter = counter + 1
            end
          end
        end
        # todas las caraterísticas tienen que ser iguales
        if counter < group_by_chars.count
          excluded_users2 << eligible_candidate.id
        end
      end
      if excluded_users2.any?
        eligible_users_temp = eligible_users_temp.where('id NOT IN (?)', excluded_users2)
      end
    end
    ########################################################################################

    @eligible_users_obj = eligible_users_temp
    @eligible_users = eligible_users_temp.map {|user| [user.codigo + ' - ' + user.comma_full_name, user.id]}
    render :layout => false
  end

  def vote

    if elec_can_user_vote_in_category?(@elec_round, @elec_process_category)
      elec_vote = ElecVote.new(:elec_round_id => @elec_round.id,
                               :elec_process_category_id => @elec_process_category.id,
                               :user_id => user_connected.id,
                               :candidate_user_id => params[:candidate_user_id],
                               :comment => params[:comment],
                               :registered_at => lms_time)

      if elec_vote.save
        # congelar valores de las características del votante y del candidato
        ElecHistoricCharacteristic.record_characteristic_values_user(user_connected, @elec_round.elec_event_id) #votante
        ElecHistoricCharacteristic.record_characteristic_values_user(elec_vote.candidate_user, @elec_round.elec_event_id) #candidato

        flash[:success] = 'Otorgado correctamente'
      else
        flash[:danger] = 'El voto no fue registrado correctamente'
      end


    else
      flash[:danger] = 'Ud. no puede otorgar en esta categoría'
    end

    redirect_to elec_round_exec_open_category_path(@elec_round.id, @elec_process_category.id)
  end

  def load_user_mini_file
    @user = User.find(params[:user_id])
    render :layout => false
  end

  def view_my_voters_detail

    if elec_can_user_view_my_voters_detail?(@elec_round, @elec_process_category)
      @my_votes = @elec_round.elec_votes.where(:elec_process_category_id => @elec_process_category.id, :candidate_user_id => user_connected.id)
      render layout: 'flat'
    else
      flash[:danger] = 'No es posible ver el detalle solicitado'
      redirect_to root_path
    end
  end

  private

  def load_variables
    @elec_round = ElecRound.find(params[:id])
    if params[:elec_process_category_id]
      @elec_process_category = ElecProcessCategory.find(params[:elec_process_category_id])
    else
      #TODO: añadir logica de seleccionar la primera categoría que tenga disponible una votación
      @elec_process_category = @elec_round.elec_process_round.elec_process.elec_process_categories.first

    end
  end

end
