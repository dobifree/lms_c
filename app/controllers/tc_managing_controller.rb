class TcManagingController < ApplicationController

  before_filter :authenticate_user
  before_filter :access_to_manage_module

  def list_programs
    @programs = Program.all
  end

  def new_program
    @program = Program.new
  end

  def create_program

    @program = Program.new(params[:program])

    @program.especifico = true
    @program.color = '000000'


    if @program.save

      level = @program.levels.build
      level.orden = 1
      level.nombre = 'Único'
      level.save

      flash[:success] = t('activerecord.success.model.training_configuration.create_program_ok')

      redirect_to tc_list_programs_path
    else
      render action: 'new_program'
    end

  end

  def edit_program
    @program = Program.find(params[:program_id])
  end

  def update_program

    @program = Program.find(params[:program_id])

    if @program.update_attributes(params[:program])
      flash[:success] = t('activerecord.success.model.training_configuration.update_program_ok')
      redirect_to tc_list_programs_path
    else
      render action: 'edit_program'
    end

  end

  def configure_certificate
    @program = Program.find(params[:program_id])
  end

  def configure_certificate_create
    @program = Program.find(params[:program_id])

    @course_certificate = CourseCertificate.new(params[:course_certificate])

    @course_certificate.program_id = @program.id
    @course_certificate.rango_desde = lms_time
    @course_certificate.rango_hasta = lms_time + 100.years

    if @course_certificate.save
      flash[:success] = t('activerecord.success.model.training_configuration.configure_certificate_ok')
      redirect_to tc_configure_certificate_path(@program)
    else

      render 'configure_certificate'
    end

  end

  def configure_certificate_update
    @course_certificate = CourseCertificate.find(params[:id])


    if @course_certificate.update_attributes(params[:course_certificate])
      flash[:success] = t('activerecord.success.model.training_configuration.configure_certificate_ok')
      redirect_to tc_configure_certificate_path(@course_certificate.program)
    else
      @course = @course_certificate.course
      render 'configure_certificate'
    end

  end

  def upload_certificate_template_form
    @course_certificate = CourseCertificate.find(params[:id])
    @program = @course_certificate.program

  end

  def upload_certificate_template

    @course_certificate = CourseCertificate.find(params[:id])
    @program = @course_certificate.program

   company = @company

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext != '.pdf'

        flash[:danger] = t('activerecord.error.model.course_certificate.update_template_wrong_format')
        redirect_to tc_configure_certificate_template_form_path @course_certificate

      elsif File.size(params[:upload]['datafile'].tempfile) > 2048.kilobytes

        flash[:danger] = t('activerecord.error.model.course_certificate.update_template_wrong_wrong_size')
        redirect_to tc_configure_certificate_template_form_path @course_certificate

      else

        save_template params[:upload], @course_certificate, company

        flash[:success] = t('activerecord.success.model.course_certificate.update_template_ok')

        redirect_to tc_configure_certificate_path @program

      end

    else

      flash[:danger] = t('activerecord.error.model.course_certificate.update_template_empty_file')

      redirect_to tc_configure_certificate_template_form_path @course_certificate

    end

  end


  def download_template
    @course_certificate = CourseCertificate.find(params[:id])

   company = @company
    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_programas/'

    if @course_certificate.template
      file = directory+@course_certificate.template+'.pdf'

      send_file file,
                filename: 'Plantilla Certificado - '+@course_certificate.program.nombre+'.pdf',
                type: 'application/octet-stream',
                disposition: 'attachment'

    end

  end

  def preview_certificado

    @course_certificate = CourseCertificate.find(params[:id])

   company = @company
    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_programas/'
    plantilla = directory+(@course_certificate.template)+'.jpg'

    require 'prawn/measurement_extensions'
    #require 'prawn/templates'

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    nombre =  'Nombres Apellidos'

    nombre_curso =  'Nombre del Programa'

    nota = '100'

    desde = localize(lms_time, format: @course_certificate.desde_formato.to_sym)
    hasta = localize(lms_time, format: @course_certificate.hasta_formato.to_sym)

    inicio = localize(lms_time, format: @course_certificate.inicio_formato.to_sym)
    fin = localize(lms_time, format: @course_certificate.fin_formato.to_sym)

    fecha = localize(lms_time, format: @course_certificate.fecha_formato.to_sym)

    cc = @course_certificate

    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: [720,540]) do |pdf|

      pdf.font_families.update('Foco Light' => {:normal => '/var/www/remote-storage/fonts/Foco_Std_Lt_0.ttf'})
      pdf.font_families.update('HaasGrotDisp Light' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-45Light_1.ttf'})
      pdf.font_families.update('HaasGrotDisp Normal' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-65Medium_1.ttf'})
      pdf.font_families.update('HaasGrotDisp Bold' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-75Bold_1.ttf'})
      #pdf.font_families.update('Foco Light' => {:normal => '/Users/alejo/Documents/Current/RocketLab/Seeds/LMS/diseños/pacífico/certificados/Foco_Std_Lt_0.ttf'})

      pdf.start_new_page
      pdf.image plantilla, at: [-36,504], width: 720

      if cc.nombre
        if cc.nombre_x == -1
          pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :style => :normal, :size => cc.nombre_size} ], :at => [0, cc.nombre_y.mm],  :align => :center
        else
          pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :style => :normal, :size => cc.nombre_size} ], :at => [cc.nombre_x.mm, cc.nombre_y.mm]
        end
      end

      if cc.nombre_curso

        if cc.nombre_curso_x == -1
          pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :style => :normal, :size => cc.nombre_curso_size} ], :at => [0, cc.nombre_curso_y.mm],  :align => :center
        else
          pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :style => :normal, :size => cc.nombre_curso_size} ], :at => [cc.nombre_curso_x.mm, cc.nombre_curso_y.mm]
        end
      end

      if cc.fecha
        if cc.fecha_x == -1
          pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :center
        elsif cc.fecha_x == -2
          pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :right
        else
          pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [cc.fecha_x.mm, cc.fecha_y.mm]
        end
      end

      if cc.desde
        if cc.desde_x == -1
          pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :style => :normal, :size => cc.desde_size} ], :at => [0, cc.desde_y.mm],  :align => :center
        else
          pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :style => :normal, :size => cc.desde_size} ], :at => [cc.desde_x.mm, cc.desde_y.mm]
        end
      end

      if cc.hasta
        if cc.hasta_x == -1
          pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :style => :normal, :size => cc.hasta_size} ], :at => [0, cc.hasta_y.mm],  :align => :center
        else
          pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :style => :normal, :size => cc.hasta_size} ], :at => [cc.hasta_x.mm, cc.hasta_y.mm]
        end
      end


    end

    send_file archivo_temporal,
              filename: 'Preview Certificado - '+@course_certificate.program.nombre+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'




  end

  def delete_program
    @program = Program.find(params[:program_id])
  end

  def destroy_program

    @program = Program.find(params[:program_id])

    if verify_recaptcha

      if @program.enrollments.count > 0

        flash[:danger] = t('activerecord.error.model.training_configuration.program_existing_errollments')
        redirect_to tc_delete_program_path(@program)

      elsif @program.program_instances.count > 0

        flash[:danger] = t('activerecord.error.model.training_configuration.program_existing_instances')
        redirect_to tc_delete_program_path(@program)

      else

        @program.levels.destroy_all
        @program.program_courses.destroy_all

        if @program.destroy
          flash[:success] = t('activerecord.success.model.training_configuration.destroy_program_ok')
          redirect_to tc_list_programs_path
        else
          flash[:danger] = t('activerecord.error.model.training_configuration.destroy_program_error')
          redirect_to tc_delete_program_path(@program)
        end

      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.training_configuration.program_captcha_error')
      redirect_to tc_delete_program_path(@program)

    end

  end

  def list_courses
    @courses = Course.all
   #@company = session[:company]
  end

  def new_course
    @course = Course.new
   #@company = session[:company]
  end

  def create_course

    @course = Course.new(params[:course])

    @course.percentage_minimum_grade = true
    @course.muestra_porcentajes = true

    if @course.save

      flash[:success] = t('activerecord.success.model.training_configuration.create_course_ok')
      redirect_to tc_list_courses_path

    else
     #@company = session[:company]
      render action: 'new_course'
    end

  end

  def edit_course
    @course = Course.find(params[:course_id])
   #@company = session[:company]
  end

  def update_course

    @course = Course.find(params[:course_id])

    if @course.update_attributes(params[:course])
      flash[:success] = t('activerecord.success.model.training_configuration.update_course_ok')
      redirect_to tc_list_courses_path
    else
     #@company = session[:company]
      render action: 'edit_course'
    end

  end

  def delete_course
    @course = Course.find(params[:course_id])
   #@company = session[:company]
  end

  def destroy_course

    @course = Course.find(params[:course_id])

    if verify_recaptcha

      if @course.program_courses.count > 0
        flash[:danger] = t('activerecord.error.model.training_configuration.course_existing_program_courses')
        redirect_to tc_delete_course_path(@course)
      else

        if @course.destroy
          flash[:success] = t('activerecord.success.model.training_configuration.destroy_course_ok')
          redirect_to tc_list_courses_path
        else
          flash[:danger] = t('activerecord.error.model.training_configuration.destroy_course_error')
          redirect_to tc_delete_course_path(@course)
        end

      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.training_configuration.course_captcha_error')
      redirect_to tc_delete_course_path(@course)

    end

  end

  def study_plan
    @program = Program.find(params[:program_id])

    @agregarcurso = @program.courses.count < Course.all.count && @program.levels.count > 0 && @program.program_instances.count == 0 ? true : false

   #@company = session[:company]

  end

  def new_program_course
    @program = Program.find(params[:program_id])
    @program_course = @program.program_courses.build

    if @program.courses.count == 0
      @courses = Course.all
    else
      @courses = Course.where('id not in (?)', @program.courses.map{ |course| course.id })
    end

  end

  def create_program_course

    @program_course = ProgramCourse.new(params[:program_course])
    @program = Program.find(@program_course.program_id)

    @program_course.level_id = @program.levels.first.id

    if @program_course.save
      flash[:success] = t('activerecord.success.model.training_configuration.create_program_course_ok')
      redirect_to tc_study_plan_path(@program_course.program_id)
    else

      @courses = Course.where('id not in (?)', @program.courses.map{ |course| course.id })

      render action: 'new'
    end

  end

  def delete_program_course
    @program_course = ProgramCourse.find(params[:program_course_id])
    @program = @program_course.program
  end

  def destroy_program_course

    @program_course = ProgramCourse.find(params[:program_course_id])
    @program = @program_course.program

    if verify_recaptcha

      if @program_course.user_courses.count > 0

        flash[:danger] = t('activerecord.error.model.training_configuration.program_courses_existing_errollments')
        redirect_to tc_delete_program_course_path(@program_course)

      elsif @program_course.program_course_instances.count > 0

        flash[:danger] = t('activerecord.error.model.training_configuration.program_courses_existing_instances')
        redirect_to tc_delete_program_course_path(@program_course)

      else

        if @program_course.destroy
          flash[:success] = t('activerecord.success.model.training_configuration.destroy_program_course_ok')
          redirect_to tc_study_plan_path(@program)
        else
          flash[:danger] = t('activerecord.error.model.training_configuration.destroy_program_course_error')
          redirect_to tc_delete_program_course_path(@program_course)
        end

      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.training_configuration.program_course_captcha_error')
      redirect_to tc_delete_program_course_path(@program_course)

    end

  end


  def access_to_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'tc', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1

      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path

    end

  end

  def save_template(upload, course_certificate, company)

    require 'RMagick'

    ext = File.extname(upload['datafile'].original_filename)

    if ext == '.pdf'

      if File.size(upload['datafile'].tempfile) <= 2048.kilobytes

        directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_programas/'

        FileUtils.mkdir_p directory unless File.directory? directory

        if course_certificate.template
          file = directory+course_certificate.template
          File.delete(file+'.jpg') if File.exist? file+'.jpg'
          File.delete(file+'.pdf') if File.exist? file+'.pdf'
        end

        course_certificate.set_template_name
        course_certificate.save

        file = directory+course_certificate.template+'.pdf'

        File.open(file, 'wb') { |f| f.write(upload['datafile'].read) }

        image = Magick::Image::read(file) do
          self.quality = 100
          self.density = '300x300'
        end

        image[0].write(directory+course_certificate.template+'.jpg')

      end

    end

  end

end
