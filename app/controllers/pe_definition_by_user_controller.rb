class PeDefinitionByUserController < ApplicationController

  include PeReportingHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list]
  before_filter :get_data_2, only: [:show, :new, :create, :finish, :show_to_clone, :clone_def]
  before_filter :get_data_3, only: [:new]
  before_filter :get_data_4, only: [:edit, :update, :destroy]
  before_filter :get_data_5, only: [:rep_detailed_final_results_pdf]
  before_filter :get_data_6, only: [:rep_definition_pdf]

  before_filter :validate_open_process
  before_filter :has_to_define_evaluation, only: [:people_list]
  before_filter :has_to_define_evaluation_to_user, only: [:show, :new, :create, :edit, :update, :destroy, :finish, :show_to_clone, :clone_def]
  before_filter :evaluation_can_clone, only: [:show_to_clone, :clone_def]
  before_filter :has_to_define_element_to_user, only: [:create, :update, :destroy]
  before_filter :definition_is_open, only: [:create, :update, :destroy]
  before_filter :validate_rep_detailed_final_results_pdf, only: [:rep_detailed_final_results_pdf]
  before_filter :validate_rep_definition_pdf, only: [:rep_definition_pdf]


  def people_list
    @pe_question_comment = PeQuestionComment.new

    @pe_question_activity = PeQuestionActivity.new
  end

  def show

    @pe_member_rel_shared_comment = PeMemberRelSharedComment.new
    @pe_question_comment = PeQuestionComment.new
    @pe_question_activity = PeQuestionActivity.new

    @pe_evaluation.pe_question_models.each do |pe_question_model|

      pe_question = pe_question_model.pe_questions.where('pe_member_evaluated_id = ?', @pe_member_evaluated.id).first

      unless pe_question
        pe_question = pe_question_model.pe_questions.build

        pe_question.pe_element = pe_question_model.pe_element
        pe_question.pe_evaluation = @pe_evaluation

        pe_question.description = pe_question_model.description

        pe_question.weight = pe_question_model.weight

        pe_question.pe_member_evaluated = @pe_member_evaluated

        pe_question.confirmed = false
        pe_question.accepted = false
        pe_question.validated = false
        pe_question.ready = false
        pe_question.creator = user_connected

        pe_question.save

      end

    end

  end

  def show_to_clone

    @pe_member_rel_shared_comment = PeMemberRelSharedComment.new
    @pe_question_comment = PeQuestionComment.new
    @pe_question_activity = PeQuestionActivity.new

    @pe_evaluation.pe_question_models.each do |pe_question_model|

      pe_question = pe_question_model.pe_questions.where('pe_member_evaluated_id = ?', @pe_member_evaluated.id).first

      unless pe_question
        pe_question = pe_question_model.pe_questions.build

        pe_question.pe_element = pe_question_model.pe_element
        pe_question.pe_evaluation = @pe_evaluation

        pe_question.description = pe_question_model.description

        pe_question.weight = pe_question_model.weight

        pe_question.pe_member_evaluated = @pe_member_evaluated

        pe_question.confirmed = false
        pe_question.accepted = false
        pe_question.validated = false
        pe_question.ready = false
        pe_question.creator = user_connected

        pe_question.save

      end

    end

  end

  def clone_def

    pe_members_to_clone = Array.new

    @pe_member_evaluator.pe_member_rels_is_evaluator_as_pe_rel_ordered(@pe_rel).each do |pe_member_rel_evaluated|

      if pe_member_rel_evaluated.id != @pe_member_rel.id && pe_member_rel_evaluated.pe_member_evaluated.has_to_be_assessed_in_evaluation(@pe_evaluation)

        #status_def = @pe_evaluation.definition_by_users_status_of_pe_member_evaluated pe_member_rel_evaluated.pe_member_evaluated

        status_def, step_label, step_label_smart_table = @pe_evaluation.pe_member_definition_by_users_status pe_member_rel_evaluated.pe_member_evaluated

        if (status_def == 'not_initiated' || status_def == 'not_confirmed' || status_def == 'in_definition') && params[:list_item_id][pe_member_rel_evaluated.id.to_s] && params[:list_item_id][pe_member_rel_evaluated.id.to_s] != '0'

          pe_members_to_clone.push pe_member_rel_evaluated.pe_member_evaluated

        end

      end

    end

    pe_members_to_clone.each do |pe_member_to_clone|

      @pe_evaluation.pe_questions.where('pe_member_evaluated_id = ?', pe_member_to_clone.id).destroy_all

      @pe_evaluation.pe_questions_only_by_pe_member_1st_level(@pe_member_evaluated).each do |pe_question_1st_level|

        pe_question_1st_level_dup = pe_question_1st_level.dup

        pe_question_1st_level_dup.pe_member_evaluated = pe_member_to_clone
        pe_question_1st_level_dup.confirmed = false
        pe_question_1st_level_dup.accepted = false
        pe_question_1st_level_dup.ready = false
        pe_question_1st_level_dup.validated = false

        pe_question_1st_level_dup.save

        pe_question_1st_level.pe_question_descriptions.each do |pe_question_description|
          pe_question_description_dup = pe_question_description.dup
          pe_question_description_dup.pe_question = pe_question_1st_level_dup
          pe_question_description_dup.save
        end

        pe_question_1st_level.pe_alternatives.each do |pe_alternative|
          pe_alternative_dup = pe_alternative.dup
          pe_alternative_dup.pe_question = pe_question_1st_level_dup
          pe_alternative_dup.save
        end

        pe_question_1st_level.pe_question_ranks.each do |pe_question_rank|
          pe_question_rank_dup = pe_question_rank.dup
          pe_question_rank_dup.pe_question = pe_question_1st_level_dup
          pe_question_rank_dup.save
        end

        @pe_evaluation.pe_questions_by_question(pe_question_1st_level).each do |pe_question_2nd_level|

          pe_question_2nd_level_dup = pe_question_2nd_level.dup

          pe_question_2nd_level_dup.pe_member_evaluated = pe_member_to_clone
          pe_question_2nd_level_dup.pe_question = pe_question_1st_level_dup
          pe_question_2nd_level_dup.confirmed = false
          pe_question_2nd_level_dup.accepted = false
          pe_question_2nd_level_dup.ready = false
          pe_question_2nd_level_dup.validated = false
          pe_question_2nd_level_dup.save

          pe_question_2nd_level.pe_question_descriptions.each do |pe_question_description|
            pe_question_description_dup = pe_question_description.dup
            pe_question_description_dup.pe_question = pe_question_2nd_level_dup
            pe_question_description_dup.save
          end

          pe_question_2nd_level.pe_alternatives.each do |pe_alternative|
            pe_alternative_dup = pe_alternative.dup
            pe_alternative_dup.pe_question = pe_question_2nd_level_dup
            pe_alternative_dup.save
          end

          pe_question_2nd_level.pe_question_ranks.each do |pe_question_rank|
            pe_question_rank_dup = pe_question_rank.dup
            pe_question_rank_dup.pe_question = pe_question_2nd_level_dup
            pe_question_rank_dup.save
          end

          @pe_evaluation.pe_questions_by_question(pe_question_2nd_level).each do |pe_question_3rd_level|

            pe_question_3rd_level_dup = pe_question_3rd_level.dup

            pe_question_3rd_level_dup.pe_member_evaluated = pe_member_to_clone
            pe_question_3rd_level_dup.pe_question = pe_question_2nd_level_dup
            pe_question_3rd_level_dup.confirmed = false
            pe_question_3rd_level_dup.accepted = false
            pe_question_3rd_level_dup.ready = false
            pe_question_3rd_level_dup.validated = false
            pe_question_3rd_level_dup.save

            pe_question_3rd_level.pe_question_descriptions.each do |pe_question_description|
              pe_question_description_dup = pe_question_description.dup
              pe_question_description_dup.pe_question = pe_question_3rd_level_dup
              pe_question_description_dup.save
            end

            pe_question_3rd_level.pe_alternatives.each do |pe_alternative|
              pe_alternative_dup = pe_alternative.dup
              pe_alternative_dup.pe_question = pe_question_3rd_level_dup
              pe_alternative_dup.save
            end

            pe_question_3rd_level.pe_question_ranks.each do |pe_question_rank|
              pe_question_rank_dup = pe_question_rank.dup
              pe_question_rank_dup.pe_question = pe_question_3rd_level_dup
              pe_question_rank_dup.save
            end

          end

        end

      end

    end

    flash[:success] = t('activerecord.success.model.pe_question.create_ok')

    redirect_to pe_def_by_user_show_to_clone_path @pe_evaluation, @pe_member_rel

  end

  def new

    @pe_question.indicator_goal = @pe_element.default_goal.to_f unless @pe_element.default_goal.blank?
    @pe_question.indicator_unit = @pe_element.default_unit unless @pe_element.default_unit.blank?

    @goals = Array.new
    @percentages = Array.new

    @pe_question_ranks_non_discrete = Array.new
    @pe_question_ranks_discrete = Array.new

  end

  def create

    @goals = Array.new
    @percentages = Array.new

    @pe_question = PeQuestion.new(params[:pe_question])
    @pe_question.weight = fix_form_floats params[:pe_question][:weight] if params[:pe_question][:weight]
    @pe_question.power = fix_form_floats params[:pe_question][:power] if params[:pe_question][:power]
    @pe_question.leverage = fix_form_floats params[:pe_question][:leverage] if params[:pe_question][:leverage]
    @pe_question.pe_evaluation = @pe_evaluation
    @pe_question.pe_member_evaluated = @pe_member_evaluated

    @pe_question.weight = 0 unless @pe_question.pe_element.assessed
    @pe_question.confirmed = false
    @pe_question.accepted = false
    @pe_question.validated = false
    @pe_question.ready = false
    @pe_question.creator = user_connected

    @pe_question_ranks_non_discrete = Array.new
    @pe_question_ranks_discrete = Array.new

    if @pe_question.kpi_indicator

      #@pe_question.description = @pe_question.kpi_indicator.name

    end

    if @pe_question.save

      rank_errors_non_discrete = false

      rank_errors_discrete = false

      unless @pe_question.kpi_indicator

        if @pe_question.indicator_type == 3
          #rangos
          if params[:pe_question_rank_non_discrete_create]

            @pe_question_ranks_non_discrete = Array.new

            cache_goals = Array.new
            cache_percentages = Array.new

            params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

              pe_question_rank = @pe_question.pe_question_ranks.build

              pe_question_rank.goal = fix_form_floats params[:pe_question_rank_non_discrete][index][:goal] if params[:pe_question_rank_non_discrete][index][:goal]
              pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_non_discrete][index][:percentage] if params[:pe_question_rank_non_discrete][index][:percentage]

              rank_errors_non_discrete = true unless pe_question_rank.valid?

              if cache_goals.include? pe_question_rank.goal
                rank_errors_non_discrete = true
                pe_question_rank.errors[:goal] << ' no puede estar repetida'
              end

              cache_goals.push pe_question_rank.goal

              if cache_percentages.include? pe_question_rank.percentage
                rank_errors_non_discrete = true
                pe_question_rank.errors[:percentage] << ' no puede estar repetido'
              end

              cache_percentages.push pe_question_rank.percentage

              pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_non_discrete][index][:model] if params[:pe_question_rank_non_discrete][index][:model]

              @pe_question_ranks_non_discrete.push pe_question_rank

            end

            unless rank_errors_non_discrete

              @pe_question.pe_question_ranks.destroy_all

              params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

                pe_question_rank = @pe_question.pe_question_ranks.build

                pe_question_rank.goal = fix_form_floats params[:pe_question_rank_non_discrete][index][:goal] if params[:pe_question_rank_non_discrete][index][:goal]
                pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_non_discrete][index][:percentage] if params[:pe_question_rank_non_discrete][index][:percentage]

                pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_non_discrete][index][:model] if params[:pe_question_rank_non_discrete][index][:model]

                pe_question_rank.save

              end

              @pe_question.reload
              @pe_question.set_goal_rank
              @pe_question.save

            end

          end

        end


        if @pe_question.indicator_type == 4
          #discreto
          if params[:pe_question_rank_discrete_create]

            @pe_question_ranks_discrete = Array.new

            cache_goals = Array.new
            cache_percentages = Array.new

            params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

              pe_question_rank = @pe_question.pe_question_ranks.build

              pe_question_rank.goal = 0
              pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
              pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_discrete][index][:percentage] if params[:pe_question_rank_discrete][index][:percentage]

              rank_errors_discrete = true unless pe_question_rank.valid?

              if cache_goals.include? pe_question_rank.discrete_goal
                rank_errors_non_discrete = true
                pe_question_rank.errors[:discrete_goal] << ' no puede estar repetida'
              end

              cache_goals.push pe_question_rank.discrete_goal

              if cache_percentages.include? pe_question_rank.percentage
                rank_errors_non_discrete = true
                pe_question_rank.errors[:percentage] << ' no puede estar repetido'
              end

              cache_percentages.push pe_question_rank.percentage

              pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_discrete][index][:model] if params[:pe_question_rank_discrete][index][:model]

              @pe_question_ranks_discrete.push pe_question_rank

            end

            unless rank_errors_discrete

              @pe_question.pe_question_ranks.destroy_all

              params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

                pe_question_rank = @pe_question.pe_question_ranks.build

                pe_question_rank.goal = 0
                pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
                pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_discrete][index][:percentage] if params[:pe_question_rank_discrete][index][:percentage]

                pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_discrete][index][:model] if params[:pe_question_rank_discrete][index][:model]

                pe_question_rank.save

              end

              @pe_question.reload
              @pe_question.set_goal_rank
              @pe_question.save

            end

          end

        end

      end

      if rank_errors_non_discrete || rank_errors_discrete
        @pe_element = @pe_question.pe_element
        render 'new'
      else
        flash[:success] = t('activerecord.success.model.pe_question.create_ok')
        redirect_to pe_def_by_user_show_path @pe_evaluation, @pe_member_rel
      end

    else

      @pe_element = @pe_question.pe_element

      if @pe_question.indicator_type == 3

        if params[:pe_question_rank_non_discrete_create]

          params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

            pe_question_rank = @pe_question.pe_question_ranks.build

            pe_question_rank.goal = params[:pe_question_rank_non_discrete][index][:goal]
            pe_question_rank.percentage = params[:pe_question_rank_non_discrete][index][:percentage]

            @pe_question_ranks_non_discrete.push pe_question_rank

          end

        end

      end

      if @pe_question.indicator_type == 4

        if params[:pe_question_rank_discrete_create]

          params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

            pe_question_rank = @pe_question.pe_question_ranks.build

            pe_question_rank.goal = 0
            pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
            pe_question_rank.percentage = params[:pe_question_rank_discrete][index][:percentage]

            @pe_question_ranks_discrete.push pe_question_rank

          end

        end

      end

      render 'new'
    end

  end

  def edit

    @goals = Array.new
    @percentages = Array.new

    @pe_question_ranks_non_discrete = @pe_question.pe_question_ranks_non_discrete
    @pe_question_ranks_discrete = @pe_question.pe_question_ranks_discrete

  end

  def update

    @pe_question.creator = user_connected

    @pe_question_ranks_non_discrete = @pe_question.pe_question_ranks_non_discrete
    @pe_question_ranks_discrete = @pe_question.pe_question_ranks_discrete

    if @pe_question.update_attributes(params[:pe_question])

      rank_errors_non_discrete = false

      if @pe_question.indicator_type == 3
        #rangos
        if params[:pe_question_rank_non_discrete_create]

          @pe_question_ranks_non_discrete = Array.new

          cache_goals = Array.new
          cache_percentages = Array.new

          params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

            pe_question_rank = @pe_question.pe_question_ranks.build

            pe_question_rank.goal = fix_form_floats params[:pe_question_rank_non_discrete][index][:goal] if params[:pe_question_rank_non_discrete][index][:goal]
            pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_non_discrete][index][:percentage] if params[:pe_question_rank_non_discrete][index][:percentage]

            rank_errors_non_discrete = true unless pe_question_rank.valid?

            if cache_goals.include? pe_question_rank.goal
              rank_errors_non_discrete = true
              pe_question_rank.errors[:goal] << ' no puede estar repetida'
            end

            cache_goals.push pe_question_rank.goal

            if cache_percentages.include? pe_question_rank.percentage
              rank_errors_non_discrete = true
              pe_question_rank.errors[:percentage] << ' no puede estar repetido'
            end

            cache_percentages.push pe_question_rank.percentage

            pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_non_discrete][index][:model] if params[:pe_question_rank_non_discrete][index][:model]

            @pe_question_ranks_non_discrete.push pe_question_rank

          end

          unless rank_errors_non_discrete

            @pe_question.pe_question_ranks.destroy_all

            params[:pe_question_rank_non_discrete_create].each_with_index do |pe_question_rank_form, index|

              pe_question_rank = @pe_question.pe_question_ranks.build

              pe_question_rank.goal = fix_form_floats params[:pe_question_rank_non_discrete][index][:goal] if params[:pe_question_rank_non_discrete][index][:goal]
              pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_non_discrete][index][:percentage] if params[:pe_question_rank_non_discrete][index][:percentage]

              pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_non_discrete][index][:model] if params[:pe_question_rank_non_discrete][index][:model]

              pe_question_rank.save

            end

            @pe_question.reload
            @pe_question.set_goal_rank
            @pe_question.save

          end

        end

      end

      rank_errors_discrete = false

      if @pe_question.indicator_type == 4
        #discreto
        if params[:pe_question_rank_discrete_create]

          @pe_question_ranks_discrete = Array.new

          cache_goals = Array.new
          cache_percentages = Array.new

          params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

            pe_question_rank = @pe_question.pe_question_ranks.build

            pe_question_rank.goal = 0
            pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
            pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_discrete][index][:percentage] if params[:pe_question_rank_discrete][index][:percentage]

            rank_errors_discrete = true unless pe_question_rank.valid?

            if cache_goals.include? pe_question_rank.discrete_goal
              rank_errors_non_discrete = true
              pe_question_rank.errors[:discrete_goal] << ' no puede estar repetida'
            end

            cache_goals.push pe_question_rank.discrete_goal

            if cache_percentages.include? pe_question_rank.percentage
              rank_errors_non_discrete = true
              pe_question_rank.errors[:percentage] << ' no puede estar repetido'
            end

            cache_percentages.push pe_question_rank.percentage

            pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_discrete][index][:model] if params[:pe_question_rank_discrete][index][:model]

            @pe_question_ranks_discrete.push pe_question_rank

          end

          unless rank_errors_discrete

            @pe_question.pe_question_ranks.destroy_all

            params[:pe_question_rank_discrete_create].each_with_index do |pe_question_rank_form, index|

              pe_question_rank = @pe_question.pe_question_ranks.build

              pe_question_rank.goal = 0
              pe_question_rank.discrete_goal = params[:pe_question_rank_discrete][index][:goal]
              pe_question_rank.percentage = fix_form_floats params[:pe_question_rank_discrete][index][:percentage] if params[:pe_question_rank_discrete][index][:percentage]

              pe_question_rank.pe_question_rank_model_id = params[:pe_question_rank_discrete][index][:model] if params[:pe_question_rank_discrete][index][:model]

              pe_question_rank.save

            end

            @pe_question.reload
            @pe_question.set_goal_rank
            @pe_question.save

          end

        end

      end

      if rank_errors_non_discrete || rank_errors_discrete
        render 'edit'
      else
        flash[:success] = t('activerecord.success.model.pe_question.create_ok')
        redirect_to pe_def_by_user_show_path @pe_evaluation, @pe_member_rel
      end

    else
      render 'edit'
    end

  end

  def destroy

    @pe_question.destroy
    flash[:success] = t('activerecord.success.model.pe_question.delete_ok')
    redirect_to pe_def_by_user_show_path @pe_evaluation, @pe_member_rel

  end

  def finish

    @pe_evaluation.pe_questions_only_by_pe_member_all_levels(@pe_member_evaluated).each do |pe_question|

      pe_question.creator = user_connected
      pe_question.confirmed = true
      pe_question.save

      pe_question.pending_validations.each do |pending_validation|

        pending_validation.attended = true
        pending_validation.save

      end

    end

    if @pe_evaluation.num_steps_definition_by_users_validated_before_accepted > 0 && @pe_member_evaluated.needs_definition_by_users_validation_before_accepted(@pe_evaluation) && @pe_evaluation.pe_process_notification_def.send_email_to_validator_when_defined

      menu_name = 'Evaluación de desempeño'

      ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

        if ct_menu_cod == 'pe'
          menu_name = ct_menus_user_menu_alias[index_m]
          break
        end
      end

      begin

        pe_definition_by_user_validator = @pe_member_evaluated.pe_definition_by_user_validators.where('pe_evaluation_id = ? AND before_accept = ? AND step_number = ?', @pe_evaluation.id, true, 1).first

        PeMailer.step_def_by_users_validate_before(@pe_process, @company, menu_name, @pe_evaluation, @pe_member_evaluated.user, @pe_member_evaluator.user, pe_definition_by_user_validator.validator).deliver if pe_definition_by_user_validator

      rescue Exception => e

        lm = LogMailer.new
        lm.module = 'pe'
        lm.step = 'def_by_user:finish'
        lm.description = e.message+' '+e.backtrace.inspect
        lm.registered_at = lms_time
        lm.save

      end

    elsif (@pe_evaluation.num_steps_definition_by_users_validated_before_accepted == 0 || !@pe_member_evaluated.needs_definition_by_users_validation_before_accepted(@pe_evaluation)) && @pe_evaluation.send_email_to_accept_definition

      menu_name = 'Evaluación de desempeño'

      ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

        if ct_menu_cod == 'pe'
          menu_name = ct_menus_user_menu_alias[index_m]
          break
        end
      end

      begin
        PeMailer.step_def_by_users_accept_definition(@pe_process, @company, menu_name, @pe_evaluation, @pe_member_evaluated.user, @pe_member_evaluator.user).deliver
      rescue Exception => e

        lm = LogMailer.new
        lm.module = 'pe'
        lm.step = 'def_by_user:finish'
        lm.description = e.message+' '+e.backtrace.inspect
        lm.registered_at = lms_time
        lm.save

      end

    end

    #flash[:success] = t('activerecord.success.model.pe_question.confirm_ok')
    redirect_to pe_def_by_user_show_path @pe_evaluation, @pe_member_rel

  end

  def rep_detailed_final_results_pdf

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    show_comments = @pe_member_rel.pe_rel.rel == 1 ? true : false

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, false, false, @pe_process.public_pe_box, @pe_process.public_pe_dimension_name, show_comments

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def rep_definition_pdf

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_definition_pdf archivo_temporal, @pe_member, @pe_process, @pe_evaluation

    send_file archivo_temporal,
              filename: 'Definición '+@pe_evaluation.name+' - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

  def get_data_1

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process

    @pe_rel = @pe_process.pe_rel_by_id 0

    @pe_members_connected = @pe_process.pe_members_by_user user_connected
    @pe_member_evaluated = @pe_members_connected.first

    @pe_member_rel = @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(@pe_rel).first if @pe_member_evaluated && @pe_rel

   #@company = session[:company]

  end

  def get_data_2

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_rel = @pe_member_rel.pe_rel
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_rels_comments = Array.new
    @pe_member_rels_comments.push @pe_member_rel

    if @pe_rel.rel == 1
      pe_rel_aux = @pe_process.pe_rel_by_id 0
      @pe_member_rels_comments += @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_aux) if pe_rel_aux
    elsif @pe_rel.rel == 0 && ((@pe_evaluation.def_allow_individual_comments_by_boss && @pe_evaluation.def_allow_watch_individual_comments_by_boss) || @pe_evaluation.def_allow_individual_comments_with_boss)
      pe_rel_aux = @pe_process.pe_rel_by_id 1
      @pe_member_rels_comments += @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_aux) if pe_rel_aux
    end

    if params[:pe_question]
      @pe_element = PeElement.find(params[:pe_question][:pe_element_id])
    end

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
        break
      end

    end

   #@company = session[:company]

  end

  def get_data_3

    @pe_question = @pe_evaluation.pe_questions.build

    if params[:pe_question_id] == '0'

      @pe_element = @pe_evaluation.pe_elements.first
      @pe_question_parent = nil

    else

      @pe_question_parent = @pe_evaluation.pe_questions.where('pe_questions.id = ?', params[:pe_question_id]).first
      @pe_element = @pe_question_parent.pe_element.pe_elements.first

    end

    @pe_question.pe_question_id = @pe_question_parent.id if @pe_question_parent
    @pe_question.pe_member_evaluated_id = @pe_member_evaluated.id
    @pe_question.pe_element_id = @pe_element.id

  end

  def get_data_4

    @pe_question = PeQuestion.find(params[:pe_question_id])

    @pe_element = @pe_question.pe_element

    @pe_evaluation = @pe_question.pe_evaluation
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])

    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_question.pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
        break
      end

    end

   #@company = session[:company]

  end

  def get_data_5
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_rel = @pe_member_rel.pe_rel
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member = @pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_rel_auto = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluated

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
      end

    end

   #@company = session[:company]

  end

  def get_data_6
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_rel = @pe_member_rel.pe_rel
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member = @pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_rel_auto = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluated

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
      end

    end

   #@company = session[:company]

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def has_to_define_evaluation

    has_to = false

    if @pe_process.has_step_definition_by_users? && @pe_process.pe_evaluations_where_has_to_define_questions(user_connected).size > 0
      has_to = true
    elsif @pe_process.has_step_definition_by_users? && @pe_process.has_step_definition_by_users_accepted? && @pe_process.pe_evaluations_where_has_to_accept_defined_questions(user_connected).size > 0
      has_to = true
    elsif @pe_process.has_step_definition_by_users? && @pe_process.has_step_definition_by_users_validated? && @pe_process.pe_evaluations_where_has_to_validate_defined_questions(user_connected).size > 0
      has_to = true
    elsif @pe_process.has_step_definition_by_users? && @pe_process.pe_evaluations_where_can_watch_own_defined_questions(user_connected).size > 0
      has_to = true
    elsif @pe_process.has_step_definition_by_users? && @pe_process.is_a_observer?(user_connected)
      has_to = true
    end

    unless has_to

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_def')
      redirect_to root_path

    end

  end

  def evaluation_can_clone

    unless @pe_evaluation.definition_by_users_clone?

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_def')
      redirect_to root_path

    end

  end

  def has_to_define_evaluation_to_user

    has_to = false

    if @pe_member_connected && @pe_process.has_step_definition_by_users? && @pe_evaluation.allow_definition_by_users

      @pe_evaluation.rel_ids_to_define_elements.each do |rel_id|

        if @pe_member_rel.pe_rel.rel == rel_id && @pe_member_evaluator.id == @pe_member_connected.id && @pe_member_evaluated.id == @pe_member_rel.pe_member_evaluated.id
          has_to = true
          break
        end

      end

    end

    unless has_to

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_def')
      redirect_to root_path

    end

  end

  def has_to_define_element_to_user

    has_to = false

    if @pe_member_connected && @pe_process.has_step_definition_by_users? && @pe_evaluation.allow_definition_by_users && @pe_element.element_def_by == 1

      @pe_element.element_def_by_rol.split('-').each do |rel|

        rel_id = rel.to_i

        if @pe_member_rel.pe_rel.rel == rel_id && @pe_member_evaluator.id == @pe_member_connected.id && @pe_member_evaluated.id == @pe_member_rel.pe_member_evaluated.id
          has_to = true
          break
        end

      end

    end

    unless has_to

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_def')
      redirect_to root_path

    end

  end

  def definition_is_open

    #status = @pe_evaluation.definition_by_users_status_of_pe_member_evaluated @pe_member_evaluated

    status_def, step_label, step_label_smart_table = @pe_evaluation.pe_member_definition_by_users_status @pe_member_evaluated

    unless (status_def == 'not_initiated' || status_def == 'not_confirmed' || status_def == 'in_definition') && !@pe_member_evaluated.step_assessment? && !@pe_member_rel.finished

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_def')
      redirect_to root_path

    end

  end

  def validate_rep_detailed_final_results_pdf

    unless @pe_member_rel.pe_rel.rel == 1 && @pe_process.step_feed_show_rep_dfr_to_boss_pdf?
      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_def')
      redirect_to root_path
    end

  end

  def validate_rep_definition_pdf

    unless @pe_member_rel.pe_rel.rel == 1 && @pe_evaluation.definition_by_users_pdf?
      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_def')
      redirect_to root_path
    end

  end

end
