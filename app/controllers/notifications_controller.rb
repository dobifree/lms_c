class NotificationsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @notifications = Notification.all
  end

  def show
    @notification = Notification.find(params[:id])
  end

  def new
    @notification = Notification.new
    @characteristics = Characteristic.all
    @characteristics_prev = {}
    @mostrar_todos = true
  end

  def edit
    @notification = Notification.find(params[:id])

    @characteristics = Characteristic.all

    @characteristics_prev = {}

    if NotificationCharacteristic.find_all_by_notification_id(params[:id]).count > 0

      @characteristics.each do |characteristic|
        @characteristics_buffer = []
        NotificationCharacteristic.where(:notification_id => params[:id], :characteristic_id => characteristic.id).each do |notification_characteristic|
          @characteristics_buffer.push(notification_characteristic.match_value)
        end
        if @characteristics_buffer.length > 0
          @characteristics_prev[characteristic.id] = @characteristics_buffer
        end
      end
      @mostrar_todos = false
    else
      @mostrar_todos = true
    end

  end

  def create

    @notification = Notification.new(params[:notification])
    @notification.fecha = lms_time

    if @notification.save
      save_characteristics @notification.id
      reorder_notifications
      flash[:success] = t('activerecord.success.model.notification.create_ok')
      redirect_to notifications_url
    else
      render action: 'new'
    end


  end

  def update

    @notification = Notification.find(params[:id])

    prev_orden = @notification.orden

    if @notification.update_attributes(params[:notification])

      save_characteristics @notification.id

      reorder_notifications if prev_orden != @notification.orden

      flash[:success] = t('activerecord.success.model.notification.update_ok')
      redirect_to notifications_url
    else
      render action: 'edit'
    end

  end

  def destroy

    @notification = Notification.find(params[:id])
    @notification.destroy
    reorder_notifications

    flash[:success] = t('activerecord.success.model.notification.delete_ok')
    redirect_to notifications_url

  end

  private

    def reorder_notifications

      Notification.all.each_with_index do |notification, index|

        notification.orden = index + 1
        notification.save

      end

    end

    def reorder_notifications_(notification, prev_orden)

      Notification.all.each do |n|

        if n.id != notification.id

          if prev_orden < notification.orden

            if n.orden > prev_orden && n.orden <= notification.orden

              n.orden = n.orden - 1
              n.orden.save

            end

          elsif prev_orden > notification.orden

            if n.orden >= notification.orden && n.orden < prev_orden

              n.orden = n.orden + 1
              n.orden.save

            end

          end

        end

      end

    end

  def save_characteristics(notification_id)

    # se actualizan las characteristics asociadas para filtrar
    @characteristics = Characteristic.all
    NotificationCharacteristic.delete_all(:notification_id => notification_id)

    if !params[:notification_todos]
      if params[:characteristic]
        @characteristics.each do |characteristic|
          params[:characteristic][characteristic.id.to_s.to_sym].slice! 0
          params[:characteristic][characteristic.id.to_s.to_sym].each do |match_value|
            NotificationCharacteristic.new(:notification_id => notification_id, :characteristic_id => characteristic.id, :match_value => match_value).save!
          end
        end
      end
    end

  end

end
