class DncProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /dnc_processes
  # GET /dnc_processes.json
  def index
    @dnc_processes = DncProcess.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dnc_processes }
    end
  end

  # GET /dnc_processes/1
  # GET /dnc_processes/1.json
  def show
    @dnc_process = DncProcess.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dnc_process }
    end
  end

  # GET /dnc_processes/new
  # GET /dnc_processes/new.json
  def new
    @dnc_process = DncProcess.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dnc_process }
    end
  end

  # GET /dnc_processes/1/edit
  def edit
    @dnc_process = DncProcess.find(params[:id])
  end

  # POST /dnc_processes
  # POST /dnc_processes.json
  def create
    @dnc_process = DncProcess.new(params[:dnc_process])

    respond_to do |format|
      if @dnc_process.save
        format.html { redirect_to @dnc_process, notice: 'Dnc process was successfully created.' }
        format.json { render json: @dnc_process, status: :created, location: @dnc_process }
      else
        format.html { render action: "new" }
        format.json { render json: @dnc_process.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dnc_processes/1
  # PUT /dnc_processes/1.json
  def update
    @dnc_process = DncProcess.find(params[:id])

    respond_to do |format|
      if @dnc_process.update_attributes(params[:dnc_process])
        format.html { redirect_to @dnc_process, notice: 'Dnc process was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dnc_process.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dnc_processes/1
  # DELETE /dnc_processes/1.json
  def destroy
    @dnc_process = DncProcess.find(params[:id])
    @dnc_process.destroy

    respond_to do |format|
      format.html { redirect_to dnc_processes_url }
      format.json { head :no_content }
    end
  end

  def massive_upload_form
    @dnc_process = DncProcess.find(params[:id])
  end

  def massive_upload

    @dnc_process = DncProcess.find(params[:id])

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        @dnc_process.dnc_requirements.destroy_all if params[:reemplazar_todo][:yes] == '1'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            fila[0] = fila[0].to_s

            user = User.find_by_codigo fila[0]

            if user

              dnc_program = DncProgram.where('name = ?', fila[1]).first
              unless dnc_program
                dnc_program = DncProgram.new
                dnc_program.name = fila[1]
              end

              if dnc_program.save

                dnc_category = DncCategory.where('name = ?', fila[2]).first
                unless dnc_category
                  dnc_category = DncCategory.new
                  dnc_category.name = fila[2]
                end

                if dnc_category.save

                  dnc_provider = DncProvider.where('name = ?', fila[4]).first
                  unless dnc_provider
                    dnc_provider = DncProvider.new
                    dnc_provider.name = fila[4]
                  end

                  if dnc_provider.save

                    dnc_process_period = @dnc_process.dnc_process_periods.where('name = ?', fila[5]).first
                    unless dnc_process_period
                      dnc_process_period = @dnc_process.dnc_process_periods.build
                      dnc_process_period.name = fila[5]
                    end

                    if dnc_process_period.save

                      dnc_course = dnc_program.dnc_courses.where('name = ?', fila[3]).first
                      unless dnc_course
                        dnc_course = dnc_program.dnc_courses.build
                        dnc_course.dnc_category = dnc_category
                        dnc_course.name = fila[3]
                      end

                      #dnc_course.duration = fila[6]

                      if dnc_course.save

                        dnc_requirement = @dnc_process.dnc_requirements.build
                        dnc_requirement.user = user
                        dnc_requirement.dnc_course = dnc_course
                        dnc_requirement.dnc_process_period = dnc_process_period
                        dnc_requirement.dnc_provider = dnc_provider
                        dnc_requirement.duration = fila[6]

                        unless dnc_requirement.save

                          @lineas_error.push index+1
                          @lineas_error_detalle.push ('dnc_requirement -> user: '+fila[0]).force_encoding('UTF-8')
                          @lineas_error_messages.push dnc_requirement.errors.full_messages

                        end

                      else

                        @lineas_error.push index+1
                        @lineas_error_detalle.push ('dnc_course: '+fila[3]).force_encoding('UTF-8')
                        @lineas_error_messages.push dnc_course.errors.full_messages

                      end

                    else

                      @lineas_error.push index+1
                      @lineas_error_detalle.push ('dnc_process_period: '+fila[5]).force_encoding('UTF-8')
                      @lineas_error_messages.push dnc_process_period.errors.full_messages

                    end

                  else

                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('dnc_provider: '+fila[4]).force_encoding('UTF-8')
                    @lineas_error_messages.push dnc_provider.errors.full_messages

                  end

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('dnc_category: '+fila[2]).force_encoding('UTF-8')
                  @lineas_error_messages.push dnc_category.errors.full_messages

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('dnc_program: '+fila[1]).force_encoding('UTF-8')
                @lineas_error_messages.push dnc_program.errors.full_messages

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('user: '+fila[0]).force_encoding('UTF-8')
              @lineas_error_messages.push ['No existe']

            end

          end

        end

        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    else
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
    end

    render 'massive_upload_form'

  end

end
