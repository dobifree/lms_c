class JCharacteristicTypesController < ApplicationController

  def index
    @characteristic_types = JCharacteristicType.all
  end


  def new
    @characteristic_type = JCharacteristicType.new
  end

  def edit
    @characteristic_type = JCharacteristicType.find(params[:j_characteristic_type_id])
  end

  def delete
    @characteristic_type = JCharacteristicType.find(params[:j_characteristic_type_id])
  end

  def create
    @characteristic_type = JCharacteristicType.new(params[:j_characteristic_type])

    if @characteristic_type.save
      flash[:success] = t('activerecord.success.model.characteristic_type.create_ok')
      redirect_to j_characteristic_type_index_path
    else
      render action: 'new'
    end

  end

  def update
    @characteristic_type = JCharacteristicType.find(params[:j_characteristic_type_id])

    if @characteristic_type.update_attributes(params[:j_characteristic_type])
      flash[:success] = t('activerecord.success.model.characteristic_type.update_ok')
      redirect_to j_characteristic_type_index_path
    else
      render action: 'edit'
    end

  end

  def destroy

    @characteristic_type = JCharacteristicType.find(params[:j_characteristic_type_id])

    if verify_recaptcha

      if @characteristic_type.characteristics.size == 0

        if @characteristic_type.destroy
          flash[:success] = t('activerecord.success.model.characteristic_type.delete_ok')
          redirect_to j_characteristic_type_index_path
        else
          flash[:danger] = t('activerecord.success.model.characteristic_type.delete_error')
          redirect_to j_characteristic_type_delete_path(@characteristic_type)
        end
      else
        flash[:danger] = t('activerecord.success.model.characteristic_type.delete_error')
        redirect_to j_characteristic_type_delete_path(@characteristic_type)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.characteristic_type.captcha_error')
      redirect_to j_characteristic_type_delete_path(@characteristic_type)

    end

  end

end


