class UmCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @um_characteristics = UmCharacteristic.all
  end

  def show
    @um_characteristic = UmCharacteristic.find(params[:id])
  end

  def new
    @um_characteristic = UmCharacteristic.new
  end

  def edit
    @um_characteristic = UmCharacteristic.find(params[:id])
  end

  def create
    @um_characteristic = UmCharacteristic.new(params[:um_characteristic])

    if @um_characteristic.save
      redirect_to um_characteristics_path, notice: 'Um characteristic was successfully created.'
    else
      render action: 'new'
    end

  end

  # PUT /um_characteristics/1
  # PUT /um_characteristics/1.json
  def update
    @um_characteristic = UmCharacteristic.find(params[:id])

    respond_to do |format|
      if @um_characteristic.update_attributes(params[:um_characteristic])
        format.html { redirect_to @um_characteristic, notice: 'Um characteristic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @um_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /um_characteristics/1
  # DELETE /um_characteristics/1.json
  def destroy
    @um_characteristic = UmCharacteristic.find(params[:id])
    @um_characteristic.destroy

    redirect_to um_characteristics_url

  end

end
