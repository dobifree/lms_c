class HrProcessManagersController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @hr_process = HrProcess.find(params[:hr_process_id])
    @hr_process_managers = @hr_process.hr_process_managers_order_by_apellidos_nombre

  end

  def search_manager

    @hr_process = HrProcess.find(params[:hr_process_id])

    @user = User.new
    @users = {}

    if params[:user]

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank? || !params[:user][:codigo].blank?

        @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ?',
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        @user.codigo = params[:user][:codigo]
        @user.apellidos = params[:user][:apellidos]
        @user.nombre = params[:user][:nombre]
      end

    end

  end

  def create

    hr_process_manager = HrProcessManager.new
    hr_process_manager.hr_process_id = params[:hr_process_id]
    hr_process_manager.user_id = params[:user_id]

    hr_process_manager.save

    flash[:success] = t('activerecord.success.model.hr_process_manager.create_ok')

    redirect_to hr_process_manager.hr_process


  end

  def destroy

    hr_process_manager = HrProcessManager.find(params[:id])
    hr_process = hr_process_manager.hr_process
    hr_process_manager.destroy

    flash[:success] = t('activerecord.success.model.hr_process_manager.delete_ok')
    redirect_to hr_process

  end

end
