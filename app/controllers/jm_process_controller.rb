class JmProcessController < ApplicationController
  before_filter :integrate_session_user_job_market
  before_filter :candidate_authenticate, except: :list_job_offers
  before_filter :applicable_job_offer, except: :list_job_offers
  before_filter :module_active
  before_filter :set_layout


  def list_job_offers
    @sel_processes = list_applicable_processes
    if session[:candidate_connected_id]
      @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
      @sel_processes_with_invitation = @jm_candidate.sel_invited_processes.where(:finished => false)
      @sel_processes = @sel_processes - @sel_processes_with_invitation
    end

    #render 'list_job_offers'
  end

  def show_job_offer
    @sel_process = SelProcess.find(params[:sel_process_id])
  end

  def show_apply_form

    @sel_process = SelProcess.find(params[:sel_process_id])
    process_applicable_to_candidate()
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])


    ## se crea el applicant
    @sel_applicant = @jm_candidate.sel_applicants.build
    @sel_applicant.sel_process = @sel_process

    # se crean las preguntas específicas
    @sel_process.sel_apply_specific_questions.each do |question|
      @sel_applicant.sel_applicant_specific_answers.new(:sel_apply_specific_question_id => question.id)
    end
    #######

  end

  def apply_job_offer
    # sel_processes tiene una copia de este método para poder enrollar desde el proceso
    @sel_process = SelProcess.find(params[:sel_process_id])
    process_applicable_to_candidate()

   #@company = session[:company]

    #@sel_applicant = SelApplicant.new(:sel_process_id => params[:sel_process_id], :jm_candidate_id => session[:candidate_connected_id])

    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])

    @sel_applicant = SelApplicant.new(params[:sel_applicant])
    @sel_applicant.sel_process = @sel_process
    @sel_applicant.jm_candidate = @jm_candidate
    specific_questions_attachment_to_save = []
    @sel_applicant.sel_applicant_specific_answers.each do |specific_answer|
      if specific_answer.attachment_name
        specific_answer.attachment_code = SecureRandom.hex(5)
        specific_answer.sel_applicant = @sel_applicant
        specific_questions_attachment_to_save.append(specific_answer.dup)
        specific_answer.attachment_name = specific_answer.attachment_name.original_filename
      end
      specific_answer.registered_at = lms_time
    end

    @sel_applicant.transaction do
      if @sel_applicant.save

        files_to_save = []

        @jm_candidate.assign_attributes(params[:jm_candidate])
        @jm_candidate.jm_candidate_answers.each_with_index do |answer, index|
          if answer.new_record?
            answer.registered_at = lms_time
          end
          answer.sel_applicant_records.each do |record|
            record.sel_applicant = @sel_applicant
          end
          answer.jm_answer_values.each do |value|

            if value.new_record? && value.value_file
              value.value_file_crypted_name = SecureRandom.hex(5)
              files_to_save.append(value.dup)
              value.value_file = value.value_file.original_filename
            end
          end
        end

        @jm_candidate.save

        files_to_save.each do |answer_value|
          file = answer_value.value_file
          save_file(file, @company, answer_value)
        end

        specific_questions_attachment_to_save.each do |specific_answer|
          file = specific_answer.attachment_name
          specific_answer.sel_applicant = @sel_applicant
          save_specific_attach_application(file, @company, specific_answer)
        end

        flash[:success] = 'Ya estás registrado como candidato para esta oferta. Gracias por participar'
        redirect_to list_jm_offers_path
      else
        raise ActiveRecord::Rollback, 'Call tech support!'
        flash[:danger] = 'Ooops... esto es vergonzoso, puedes intentar postular nuevamente?'
        redirect_to :back
      end
    end

  end

  private


  def set_layout
    self.class.layout session[:user_connected_id].nil? ? 'job_market' : 'application'
  end


  def save_file(file, company, jm_answer_value)
    # sel_processes tiene una copia de este método para poder enrollar desde el proceso
    file_crypted_name = jm_answer_value.value_file_crypted_name + File.extname(file.original_filename)
    candidate_id = session[:candidate_connected_id].to_s
    directory = company.directorio+'/'+company.codigo+'/jm_candidates/' + candidate_id + '/'

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') { |f| f.write(file.read) }


  end

  def save_specific_attach_application(file, company, specific_answer)
    # sel_processes tiene una copia de este método para poder enrollar desde el proceso
    file_crypted_name = specific_answer.attachment_code + File.extname(file.original_filename)
    candidate_id = @jm_candidate.id.to_s
    process_id = specific_answer.sel_applicant.sel_process_id.to_s
    directory = company.directorio + '/' + company.codigo + '/sel_processes/' + process_id + '/jm_candidates/' + candidate_id + '/specific_questions_attachments/'

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') {|f| f.write(file.read)}
  end

  def process_applicable_to_candidate
    if !@sel_process.apply_available
      flash[:danger] = 'La postulación para este proceso no está disponible'
      redirect_to list_jm_offers_path
    end

    if SelApplicant.where(:sel_process_id => @sel_process.id, :jm_candidate_id => session[:candidate_connected_id]).count > 0
      flash[:danger] = 'Usted ya postuló a este proceso. No es posible postular nuevamente.'
      redirect_to list_jm_offers_path
    end
  end

  def applicable_job_offer
    unless !SelProcess.find_by_id(params[:sel_process_id]).nil? && list_applicable_processes.include?(SelProcess.find(params[:sel_process_id]))
      flash[:danger] = 'El proceso no existe o ya no está disponible para postular. '
      redirect_to list_jm_offers_path
    end
  end

  def candidate_authenticate
    if session[:candidate_connected_id]
      true
    else
      flash[:danger] = 'no se puede postular sin iniciar sesión'
      redirect_to list_jm_offers_path
    end
  end

  def module_active
    if CtModule.where(cod: 'jm', active: true).count == 0
      redirect_to root_path
    end
  end

  def list_applicable_processes

    if session && session[:candidate_connected_id] && JmCandidate.find(session[:candidate_connected_id]).internal_user?
      SelProcess.active.in_time(lms_time).applicable.internal
    else
      SelProcess.active.in_time(lms_time).applicable.external
    end
  end
end
