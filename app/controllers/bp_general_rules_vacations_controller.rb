class BpGeneralRulesVacationsController < ApplicationController

  def new
    @bp_general_rules_vacation = BpGeneralRulesVacation.new
    respond_to do |format|
      format.html
      format.json { render json: @bp_general_rules_vacation }
    end
  end

  def create
    @bp_general_rules_vacation = BpGeneralRulesVacation.new(params[:bp_general_rules_vacation])
    @bp_general_rules_vacation.registered_at = lms_time
    @bp_general_rules_vacation.registered_by_admin_id = admin_connected.id

    if @bp_general_rules_vacation.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_created')
      redirect_to bp_groups_path
    else
      render action: "new"
    end
  end

  def deactivated
    bp_general_rules_vacation = BpGeneralRulesVacation.find(params[:bp_general_rules_vacation_id])
    bp_general_rules_vacation.active = false
    bp_general_rules_vacation.deactivated_at = lms_time
    bp_general_rules_vacation.deactivated_by_admin_id = admin_connected.id
    if bp_general_rules_vacation.save
      flash[:success] = t('views.bp_general_rules_vacations.flash_messages.success_deactivate_general_rules_vacations')
      redirect_to bp_groups_path
    else
      flash[:danger] = t('views.bp_general_rules_vacations.flash_messages.danger_deactivate_general_rules_vacations')
      redirect_to bp_groups_path
    end
  end

end