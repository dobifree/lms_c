class TrainingImpactValuesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @training_impact_values = TrainingImpactValue.all
  end


  def show
    @training_impact_value = TrainingImpactValue.find(params[:id])
  end


  def new
    @training_impact_value = TrainingImpactValue.new
  end


  def edit
    @training_impact_value = TrainingImpactValue.find(params[:id])
  end

  def delete
    @training_impact_value = TrainingImpactValue.find(params[:id])
  end


  def create
    @training_impact_value = TrainingImpactValue.new(params[:training_impact_value])


      if @training_impact_value.save
        flash[:success] = t('activerecord.success.model.training_impact_value.create_ok')
        redirect_to training_impact_values_path
      else
        render action: 'new'
      end

  end


  def update
    @training_impact_value = TrainingImpactValue.find(params[:id])


      if @training_impact_value.update_attributes(params[:training_impact_value])
        flash[:success] = t('activerecord.success.model.training_impact_value.update_ok')
        redirect_to training_impact_values_path
      else
        render action: 'edit'
      end

  end



  def destroy

    @training_impact_value = TrainingImpactValue.find(params[:id])

    if verify_recaptcha

      if @training_impact_value.destroy
        flash[:success] = t('activerecord.success.model.training_impact_value.delete_ok')
        redirect_to training_impact_values_path
      else
        flash[:danger] = t('activerecord.success.model.training_impact_value.delete_error')
        redirect_to delete_training_impact_value_path(@training_impact_value)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.training_impact_value.captcha_error')
      redirect_to delete_training_impact_value_path(@training_impact_value)

    end

  end
end
