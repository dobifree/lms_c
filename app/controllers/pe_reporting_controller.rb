class PeReportingController < ApplicationController

  include PeReportingHelper


  before_filter :authenticate_user

  before_filter :verify_open_process, only: [:list_subs, :state_of_progress_for_boss, :final_results_for_boss, :final_results_for_boss_pdf]
  before_filter :verify_boss, only: [:state_of_progress_for_boss, :final_results_for_boss, :final_results_for_boss_pdf]

  before_filter :verify_boss_node, only: [:final_results_for_boss_node, :final_results_for_boss_pdf_node]
  before_filter :verify_boss_node_f, only: [:final_results_for_boss_pdf_node_f]


  before_filter :verify_himself, only: [:final_results_for_himself_pdf_node]


  before_filter :verify_access_to_reports, only: [:final_results, :final_results_pdf]
  before_filter :get_data_1, only: [:list_reports, :state_of_evals,
                                    :final_results_consolidate_filter, :final_results_consolidate,
                                    :final_results_consolidate_filter_comp, :final_results_consolidate_comp,
                                    :final_results_consolidate_filter_comp_2, :final_results_consolidate_comp_2,
                                    :final_results_list_people, :state_of_feedback]
  before_filter :get_data_2, only: [:state_of_evals_det, :final_rep_only_boss_pdf]
  before_filter :verify_access_to_reports_as_manager, only: [:list_reports, :state_of_evals, :state_of_evals_det,
                                                             :final_results_consolidate_filter, :final_results_consolidate,
                                                             :final_results_consolidate_filter_comp, :final_results_consolidate_comp,
                                                             :final_results_consolidate_filter_comp_2, :final_results_consolidate_comp_2,
                                                             :final_results_list_people, :state_of_feedback, :final_rep_only_boss_pdf]


  before_filter :authenticate_user_admin, only:[:rep_detailed_final_results_people_pdf, :rep_detailed_final_results_people_for_employees_pdf,
                                                :download_xls_for_validation_everybody_admin, :rep_detailed_definition_evaluation_admin]

  before_filter :get_data_1_2, only: [:list_reports_2,
                                      :rep_evaluated_members_configuration, :rep_evaluations_configuration,
                                      :rep_general_status, :rep_assign_status, :rep_assessment_status, :rep_validation_status, :rep_calibration_status, :rep_feedback_status,
                                      :rep_consolidated_final_results_people_filter, :rep_consolidated_final_results_people,
                                      :rep_consolidated_final_results_areas,
                                      :rep_detailed_final_results_people_list, :rep_detailed_final_results_people_list_hab, :rep_detailed_final_results_people_list_i, :rep_detailed_final_results_area_list,
                                      :rep_detailed_final_results_evaluations, :rep_detailed_final_results_evaluations_cal, :rep_detailed_final_results_questions,
                                      :rep_consolidated_feedback, :rep_consolidated_feedback_cols, :rep_consolidated_feedback_accepted, :rep_consolidated_feedback_accepted_survey,
                                      :rep_detailed_final_results_people_pdf, :rep_detailed_final_results_people_for_employees_pdf,
                                      :download_xls_for_validation_everybody_admin, :rep_rol_privado]
  before_filter :get_data_2_2, only: [:rep_detailed_final_results_person, :rep_detailed_final_results_person_i, :rep_detailed_final_results_pdf, :rep_detailed_final_results_pdf_only_boss]
  before_filter :get_data_2_3, only: [:rep_detailed_final_results_area]
  before_filter :get_data_2_4, only: [:rep_definition_by_user_status, :rep_tracking_status, :rep_detailed_final_results_evaluation, :rep_detailed_definition_evaluation,
                                      :rep_detailed_definition_evaluation_admin, :rep_members_configuration_evaluation]

  before_filter :verify_is_a_manager_or_reporter, only: [:list_processes_2]

  before_filter :validate_manager_or_reporter, only: [:list_reports_2,
                                          :rep_evaluated_members_configuration, :rep_evaluations_configuration,
                                          :rep_members_configuration_evaluation,
                                          :rep_general_status, :rep_definition_by_user_status, :rep_tracking_status, :rep_assign_status, :rep_assessment_status, :rep_validation_status, :rep_calibration_status, :rep_feedback_status,
                                          :rep_consolidated_final_results_people_filter, :rep_consolidated_final_results_people,
                                          :rep_consolidated_final_results_areas,
                                          :rep_detailed_final_results_people_list, :rep_detailed_final_results_people_list_hab, :rep_detailed_final_results_people_list_i, :rep_detailed_final_results_person, :rep_detailed_final_results_person_i,
                                          :rep_detailed_final_results_pdf, :rep_detailed_final_results_pdf_only_boss,
                                          :rep_detailed_final_results_area_list, :rep_detailed_final_results_area,
                                          :rep_detailed_final_results_evaluation, :rep_detailed_final_results_evaluations, :rep_detailed_final_results_evaluations_cal,
                                                      :rep_detailed_final_results_questions,
                                          :rep_detailed_definition_evaluation, :rep_consolidated_feedback, :rep_consolidated_feedback_cols, :rep_consolidated_feedback_accepted,
                                          :rep_consolidated_feedback_accepted_survey, :rep_rol_privado]

  before_filter :validate_has_rep_detailed_final_results_for_boss, only: [:rep_detailed_final_results_pdf_only_boss]

  def list_processes_2

  end

  def list_reports_2

  end
  
  def rep_members_configuration_evaluation

  end

  def rep_evaluated_members_configuration

   #@company = session[:company]

    #@pe_members_evaluated = @pe_process.evaluated_members_ordered

    @pe_members_evaluated = @pe_process.evaluated_members_segmented_to_report user_connected


  end

  def rep_evaluations_configuration

    filename = ('Evaluaciones ' + @pe_process.name).gsub(/\s/,'_') + '.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_definition_by_user_status


  end

  def rep_detailed_definition_evaluation

    filename = ('Definición de ' + @pe_evaluation.name).gsub(/\s/,'_') + '.xls'

    @active = params[:active]

    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_detailed_definition_evaluation_admin

    filename = ('Definición de ' + @pe_evaluation.name).gsub(/\s/,'_') + '.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_general_status


  end

  def rep_tracking_status

  end

  def rep_assign_status

  end

  def rep_assessment_status

  end

  def rep_validation_status

  end

  def rep_calibration_status

  end

  def rep_feedback_status

  end

  def rep_detailed_final_results_people_list

  end

  def rep_detailed_final_results_people_list_i

  end

  def rep_detailed_final_results_people_list_hab

  end

  def rep_detailed_final_results_person

   #@company = session[:company]

  end

  def rep_detailed_final_results_person_i

   #@company = session[:company]

  end

  def rep_detailed_final_results_pdf

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_for_manager, @pe_process.rep_detailed_final_results_show_calibration_for_manager, true, true, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def rep_detailed_final_results_pdf_only_boss

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_detailed_final_results_pdf_only_boss archivo_temporal, @pe_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_only_boss_for_manager, @pe_process.rep_detailed_final_results_show_calibration_only_boss_for_manager, true, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def rep_detailed_final_results_area_list

  end

  def rep_detailed_final_results_area

   #@company = session[:company]

  end

  def rep_detailed_final_results_people_pdf

   #@company = session[:company]

    directorio_temporal = '/tmp/'+SecureRandom.hex(5)

    FileUtils.mkdir_p directorio_temporal unless File.directory? directorio_temporal

    @pe_process.evaluated_members.each do |pe_evaluated_member|

      nombre_temporal = pe_evaluated_member.user.apellidos+'_'+pe_evaluated_member.user.nombre+' - '+pe_evaluated_member.user.codigo+'.pdf'

      archivo_temporal = directorio_temporal+'/'+nombre_temporal

      generate_rep_detailed_final_results_pdf archivo_temporal, pe_evaluated_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_for_manager, @pe_process.rep_detailed_final_results_show_calibration_for_manager, true, true, true

    end

    system "tar -czf /tmp/detailed_report.tar.gz #{directorio_temporal}"


    send_file '/tmp/detailed_report.tar.gz',
              filename: @pe_process.name+'_detailed_report.tar.gz',
              type: 'application/octet-stream',
              disposition: 'attachment'

    FileUtils.remove_dir directorio_temporal
    #FileUtils.remove_entry '/tmp/detailed_report.tar.gz'

  end

  def rep_detailed_final_results_people_for_employees_pdf

   #@company = session[:company]

    directorio_temporal = '/tmp/'+SecureRandom.hex(5)

    FileUtils.mkdir_p directorio_temporal unless File.directory? directorio_temporal

    @pe_process.evaluated_members.each do |pe_evaluated_member|

      nombre_temporal = pe_evaluated_member.user.apellidos+'_'+pe_evaluated_member.user.nombre+' - '+pe_evaluated_member.user.codigo+'.pdf'

      archivo_temporal = directorio_temporal+'/'+nombre_temporal

      generate_rep_detailed_final_results_pdf archivo_temporal, pe_evaluated_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_for_user_profile, @pe_process.rep_detailed_final_results_show_calibration_for_user_profile, (@pe_process.public_pe_box && @pe_process.public_pe_members_box), (@pe_process.public_pe_dimension_name && @pe_process.public_pe_members_dimension_name), true

    end

    system "tar -czf /tmp/detailed_report.tar.gz #{directorio_temporal}"


    send_file '/tmp/detailed_report.tar.gz',
              filename: @pe_process.name+'_detailed_report.tar.gz',
              type: 'application/octet-stream',
              disposition: 'attachment'

    FileUtils.remove_dir directorio_temporal
    #FileUtils.remove_entry '/tmp/detailed_report.tar.gz'

  end

  def download_xls_for_validation_everybody_admin

    @pe_evaluation = PeEvaluation.find 9

    reporte_excel = xls_for_validation_everybody_admin @pe_process, @pe_evaluation

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Validación_'+@pe_evaluation.name+'_todos.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end



  def rep_consolidated_final_results_people_filter

  end

  def rep_consolidated_final_results_people

    @pe_members = Array.new

    pe_rel_jefe = @pe_process.pe_rel(:jefe)

    pe_members_ids = nil

    if pe_rel_jefe && params[:pe_member_evaluator_jefe] && !params[:pe_member_evaluator_jefe][:id].blank?

      pe_members_ids = @pe_process.pe_member_rels.where('pe_rel_id = ? AND pe_member_evaluator_id = ?', pe_rel_jefe.id, params[:pe_member_evaluator_jefe][:id]).pluck(:pe_member_evaluated_id)

    end

    @pe_process.pe_evaluations.each do |pe_evaluation|

      if pe_evaluation.pe_groups.size > 1

        params[:pe_group][pe_evaluation.id.to_s].slice! 0

        if params[:pe_group][pe_evaluation.id.to_s].size > 0

          if pe_members_ids
            pe_members_ids = pe_members_ids & pe_evaluation.pe_member_groups.where('pe_group_id IN (?)', params[:pe_group][pe_evaluation.id.to_s]).pluck(:pe_member_id)
          else
            pe_members_ids = pe_evaluation.pe_member_groups.where('pe_group_id IN (?)', params[:pe_group][pe_evaluation.id.to_s]).pluck(:pe_member_id)
          end

        end

      end

    end

    pe_characteristics = @pe_process.pe_characteristics_rep_consolidated_final_results

    @characteristics_selected = []

    queries = []

    n_q = 0

    if params[:characteristic]

      pe_characteristics.each do |pe_characteristic|

        params[:characteristic][pe_characteristic.characteristic.id.to_s].slice! 0

        if params[:characteristic][pe_characteristic.characteristic.id.to_s].length > 0

          @characteristics_selected.push params[:characteristic][pe_characteristic.characteristic.id.to_s].map { |c| c.titleize }

          queries[n_q] = 'characteristic_id = '+pe_characteristic.characteristic.id.to_s+' AND ( '

          params[:characteristic][pe_characteristic.characteristic.id.to_s].each_with_index do |valor, index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'value = "'+valor+'" '

          end

          queries[n_q] += ' ) '

          n_q += 1

        end

      end

      if pe_members_ids
        @pe_members = @pe_process.evaluated_members_segmented_to_report(user_connected, pe_members_ids)
      else
        @pe_members = @pe_process.evaluated_members_segmented_to_report(user_connected)
      end

      if queries.length > 0

        pe_members_ids = @pe_members.map { |pe_member| pe_member.id }

        queries.each_with_index do |query, index|

          if index == 0

            if pe_members_ids
              @pe_members = @pe_process.evaluated_members_ordered.joins(:pe_member_characteristics).where(query+' AND pe_members.id IN (?) ', pe_members_ids)
            else
              @pe_members = @pe_process.evaluated_members_ordered.joins(:pe_member_characteristics).where(query)
            end

          else

            pe_members_ids = @pe_members.map { |pe_member| pe_member.id }

            @pe_members = @pe_process.evaluated_members_ordered.joins(:pe_member_characteristics).where(query+' AND pe_members.id IN (?) ', pe_members_ids)

          end

        end

      end

    elsif @pe_process.pe_characteristics_rep_consolidated_final_results.size == 0
      @pe_members = @pe_process.evaluated_members_segmented_to_report(user_connected)
    end

  end

  def rep_consolidated_final_results_areas


  end

  def rep_detailed_final_results_evaluations

    filename = 'Reporte_detallado_evaluaciones_'+localize(lms_time, format: :full_date_time_for_download)+'.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_detailed_final_results_evaluations_cal



    filename = 'Reporte_detallado_evaluaciones_'+localize(lms_time, format: :full_date_time_for_download)+'.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_detailed_final_results_evaluations_1
    filename = 'Reporte_detallado_evaluaciones_'+localize(lms_time, format: :full_date_time_for_download)+'.xlsx'
    respond_to do |format|
      format.xlsx { render xlsx: 'rep_detailed_final_results_evaluations_1.xlsx', filename: filename}
    end
  end
  def rep_detailed_final_results_questions

    filename = 'Reporte_detallado_preguntas_'+localize(lms_time, format: :full_date_time_for_download)+'.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_detailed_final_results_evaluation

    filename = ('Reporte Detallado ' + @pe_evaluation.name).gsub(/\s/,'_') + '.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end


  def rep_detailed_final_results_evaluation_1
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process

    filename = ('Reporte Detallado ' + @pe_evaluation.name).gsub(/\s/,'_') + '.xlsx'
    respond_to do |format|
      format.xlsx { render xlsx: 'rep_detailed_final_results_evaluation_1.xlsx', filename: filename}
    end
  end

  def rep_consolidated_feedback

    filename = 'Reporte_consolidado_retroalimentación.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_consolidated_feedback_cols

    filename = 'Reporte_consolidado_retroalimentación.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_consolidated_feedback_accepted

    filename = 'Reporte_consolidado_confirmación_retroalimentación.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_consolidated_feedback_accepted_survey

    filename = 'Reporte_consolidado_encuestas.xls'


    respond_to do |format|
      format.xls {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end

  end

  def rep_rol_privado

    reporte_excel = xls_rol_privado @pe_process

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Rerporte Check - Rol Privado.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  ##############################


  def list_processes

  end

  def list_reports

  end

  def state_of_evals

  end

  def state_of_evals_det

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_template = @hr_process.hr_process_template

  end

  def state_of_feedback

  end

  def final_results_consolidate_filter

    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

  end

  def final_results_consolidate

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

    @characteristics_selected = []

    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        params[:characteristic][characteristic.id.to_s.to_sym].slice! 0

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_selected.push params[:characteristic][characteristic.id.to_s.to_sym].map { |c| c.titleize }

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND ( '

          params[:characteristic][characteristic.id.to_s.to_sym].each_with_index do |valor, index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'valor = "'+valor+'" '

          end

          queries[n_q] += ' ) '

          n_q += 1

        end

      end

      if queries.length > 0

        queries.each_with_index do |query, index|

          if index == 0

            @users = User.joins(:user_characteristics).where(query).order('apellidos, nombre')

          else

            lista_users_ids = @users.map { |u| u.id }

            @users = User.joins(:user_characteristics).where(query+' AND users.id IN (?) ', lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = User.all

      end

    end

  end

  def final_results_consolidate_filter_comp

    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

  end

  def final_results_consolidate_comp

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

    @characteristics_selected = []

    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        params[:characteristic][characteristic.id.to_s.to_sym].slice! 0

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_selected.push params[:characteristic][characteristic.id.to_s.to_sym].map { |c| c.titleize }

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND ( '

          params[:characteristic][characteristic.id.to_s.to_sym].each_with_index do |valor, index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'valor = "'+valor+'" '

          end

          queries[n_q] += ' ) '

          n_q += 1

        end

      end

      if queries.length > 0

        queries.each_with_index do |query, index|

          if index == 0

            @users = User.joins(:user_characteristics).where(query).order('apellidos, nombre')

          else

            lista_users_ids = @users.map { |u| u.id }

            @users = User.joins(:user_characteristics).where(query+' AND users.id IN (?) ', lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = User.all

      end

    end

  end

  def final_results_consolidate_filter_comp_2

    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

  end

  def final_results_consolidate_comp_2

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

    @characteristics_selected = []

    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        params[:characteristic][characteristic.id.to_s.to_sym].slice! 0

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_selected.push params[:characteristic][characteristic.id.to_s.to_sym].map { |c| c.titleize }

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND ( '

          params[:characteristic][characteristic.id.to_s.to_sym].each_with_index do |valor, index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'valor = "'+valor+'" '

          end

          queries[n_q] += ' ) '

          n_q += 1

        end

      end

      if queries.length > 0

        queries.each_with_index do |query, index|

          if index == 0

            @users = User.joins(:user_characteristics).where(query).order('apellidos, nombre')

          else

            lista_users_ids = @users.map { |u| u.id }

            @users = User.joins(:user_characteristics).where(query+' AND users.id IN (?) ', lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = User.all

      end

    end

  end

  def final_results_list_people

  end

  def list_subs

   #@company = session[:company]

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_template = @hr_process.hr_process_template

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

  end

  def state_of_progress_for_boss

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

   #@company = session[:company]

    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions

  end

  def final_results_for_boss

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

   #@company = session[:company]

  end

  def final_results_for_boss_node

   #@company = session[:company]

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

  end

  def final_results

   #@company = session[:company]

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

  end

  def final_results_pdf

    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    is_boss = false

    generate_final_results_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations, is_boss

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def final_results_for_boss_pdf

    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    is_boss = true

    generate_final_results_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations, is_boss

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def final_results_for_boss_pdf_node

    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    is_boss = true

    generate_final_results_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations, is_boss

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def final_results_for_boss_pdf_node_f

    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    is_boss = true

    generate_final_results_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations, is_boss

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def final_results_for_himself_pdf_node

    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    is_boss = false

    generate_final_results_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations, false, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def final_rep_only_boss_pdf

    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_final_results_only_boss_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

    def verify_is_a_manager_or_reporter

      unless user_connected.pe_managers.count > 0 || user_connected.hr_process_managers.count > 0 || user_connected.pe_reporters.count > 0
        flash[:danger] = t('security.no_access_generic')
        redirect_to root_path
      end

    end

    def get_data_1_2
      @pe_process = PeProcess.find(params[:pe_process_id])
    end

    def get_data_2_2

      @pe_member = PeMember.find(params[:pe_member_id])
      @pe_process = @pe_member.pe_process

    end

    def get_data_2_3

      @pe_area = PeArea.find(params[:pe_area_id])
      @pe_process = @pe_area.pe_process

      @pe_member = @pe_area.pe_members.where('is_evaluated = ?', true).first

    end

    def get_data_2_4
      @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
      @pe_process = @pe_evaluation.pe_process
    end

    def validate_manager_or_reporter
      unless @pe_process.pe_managers.where('user_id = ?', user_connected.id).count > 0 || @pe_process.pe_reporters.where('user_id = ?', user_connected.id).count > 0
        flash[:danger] = t('security.no_access_generic')
        redirect_to root_path
      end
    end

    def validate_has_rep_detailed_final_results_for_boss
      unless @pe_process.has_rep_detailed_final_results_only_boss
        flash[:danger] = t('security.no_access_generic')
        redirect_to root_path
      end
    end


  ########

    def get_data_1
      @hr_process = HrProcess.find(params[:hr_process_id])
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
    end

    def get_data_2
      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
      @hr_process = @hr_process_user.hr_process
    end

    def verify_open_process

      @hr_process = HrProcess.find(params[:hr_process_id])

      unless @hr_process.abierto && lms_time >= @hr_process.fecha_inicio

        flash[:danger] = t('security.no_access_pe_closed')
        redirect_to root_path

      end

    end

    def verify_boss

      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)

      unless @hr_process_user_connected.hr_process_evalua_rels.where('hr_process_user2_id = ? AND tipo = ?', @hr_process_user.id, 'jefe').count > 0 || @hr_process_user_connected.is_super_boss_of(@hr_process_user)

        flash[:danger] = t('security.no_access_pe_no_privileges_for_reports')
        redirect_to root_path

      end

    end

  def verify_boss_node

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    @hr_process = @hr_process_user.hr_process

    unless user_connected.es_jefe_de(@hr_process_user.user) || user_connected.mpi_view_users
      flash[:danger] = t('security.no_access_view_user_profile')
      redirect_to root_path
    end

  end

  def verify_boss_node_f

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    @hr_process = @hr_process_user.hr_process

  end

  def verify_himself

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    @hr_process = @hr_process_user.hr_process

    unless user_connected.id == @hr_process_user.user_id
      flash[:danger] = t('security.no_access_view_user_profile')
      redirect_to root_path
    end

  end

    def verify_access_to_reports

      @hr_process = HrProcess.find(params[:hr_process_id])

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

      ver_calibrar = true

      if params[:cs_id] && params[:cs_id].to_i > 0
        @hr_process_calibration_session = HrProcessCalibrationSession.find params[:cs_id]

        hr_process_calibration_session_c = @hr_process_calibration_session.hr_process_calibration_session_cs.where('user_id = ?', user_connected.id).first
        hr_process_calibration_session_m = @hr_process_calibration_session.hr_process_calibration_session_ms.where('hr_process_user_id = ? ', params[:hr_process_user_id]).first

        unless hr_process_calibration_session_c && hr_process_calibration_session_m
          ver_calibrar = false
        end

      else

        ver_calibrar = false

      end

      ver_resp = false

      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)

      if @hr_process_user_connected && @hr_process_user_connected.hr_process_evalua_rels.where('tipo = "resp" AND hr_process_user2_id = ?', params[:hr_process_user_id] ).size > 0

        ver_resp = true

      end

      unless hr_process_manager || ver_calibrar || ver_resp

        flash[:danger] = t('security.no_access_pe_no_privileges_for_reports')
        redirect_to root_path

      end

    end

    def verify_access_to_reports_as_manager

      unless user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).count > 0 || @hr_process_user_connected.hr_process_evalua_rels.where('tipo = "resp"').size > 0

        flash[:danger] = t('security.no_access_pe_no_privileges_for_reports')
        redirect_to root_path

      end

    end

end
