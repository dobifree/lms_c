class BrgManagersController < ApplicationController
  include BrgModule
  include BrgManagersHelper

  before_filter :authenticate_user
  before_filter :active_module
  before_filter :brg_manager
  before_filter :editable_manager, only: %i[edit update]
  before_filter :get_process, only: %i[new_step_0
                                       new
                                       create
                                       assoc_group_massive_upload
                                       assoc_group_massive_upload_2
                                       assoc_managee_massive_upload
                                       assoc_managee_massive_upload_2
                                       managers_massive_upload
                                       managers_massive_upload_2]
  def new_step_0
    @user = User.new
    @users = {}

    if params[:user] && !(params[:user][:codigo].blank? && params[:user][:apellidos].blank? && params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def new
    @manager = BrgManager.new(user_id: params[:user_id], brg_process_id: @process.id)
  end

  def create
    manager = BrgManager.new(params[:brg_manager])
    manager.brg_process_id = @process.id
    manager = set_registered(manager)
    if manager.save
      flash[:success] = t('views.brg_module.flash_messages.create_success')
      redirect_to brg_managers_manage_path(manager, t('views.brg_module.tabs.manage_manager.bonus'))
    else
      @manager = manager
      @user = manager.user
      # @users = search_active_users(manager.user.codigo, nil, nil)
      render 'new'
    end
  end

  def manage
    @tab = params[:tab] ? params[:tab] : 0
    @manager = BrgManager.find(params[:brg_manager_id])
  end

  def edit
    @manager = BrgManager.find(params[:brg_manager_id])
  end

  def update
    manager = BrgManager.find(params[:brg_manager_id])
    if manager.update_attributes(params[:brg_manager])
      flash[:success] = t('views.brg_module.flash_messages.change_success')
      redirect_to brg_managers_manage_path(manager, t('views.brg_module.tabs.manage_manager.bonus'))
    else
      @manager = manager
      render action: 'edit'
    end
  end

  def deactivate_manager
    manager = BrgManager.find(params[:brg_manager_id])
    manager = deactivate(manager)
    manager.save
    #desactivar managees
    #group.brg_group_users.active.each {|assoc| deactivate(assoc).save}
    flash[:success] = t('views.brg_module.flash_messages.delete_success')
    redirect_to brg_groups_index_path(t('views.brg_module.tabs.manager_index.managers'))
  end

  def deactivate_manager_ajax
    manager = BrgManager.find(params[:brg_manager_id])
    manager = deactivate(manager)
    manager.save
    #desactivar managees
    #group.brg_group_users.active.each {|assoc| deactivate(assoc).save}
    render nothing: true
  end

  def managers_massive_upload
  end

  def managers_massive_upload_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_managers file, @process.id
        flash.now[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'managers_massive_upload'
  end

  def assoc_manager
    @manager = BrgManager.find(params[:brg_manager_id])
    @user = User.new
    @users = {}

    if params[:user] && !(params[:user][:codigo].blank? && params[:user][:apellidos].blank? && params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def assoc_manager_2
    manager = BrgManager.find(params[:brg_manager_id])
    managee = BrgManager.where(user_id: params[:user_id], active: true).first

    relation = BrgManageRelation.new(manager_id: manager.id, managee_id: managee.id)

    relation = set_registered(relation)
    if relation.valid?
      relation.save
      redirect_to brg_managers_manage_path(manager, t('views.brg_module.tabs.manage_manager.managee'))
    else
      @relation = relation
      @manager = manager
      @user = managee.user
      @users = search_active_users(managee.user.codigo, '', '')
      render 'assoc_manager'
    end
  end

  def assoc_group
    @manager = BrgManager.find(params[:brg_manager_id])
  end

  def assoc_group_2
    manager = BrgManager.find(params[:brg_manager_id])
    group_to_manage = BrgGroup.find(params[:brg_group_id])

    relation = BrgManageRelation.new(manager_id: manager.id, group_to_manage_id: group_to_manage.id)
    relation = set_registered(relation)
    if relation.valid?
      relation.save
      redirect_to brg_managers_manage_path(manager, t('views.brg_module.tabs.manage_manager.group_manage'))
    else
      @relation = relation
      @manager = manager
      render 'assoc_group'
    end
  end

  def deactivate_relation_ajax
    relation = BrgManageRelation.find(params[:brg_manage_relation_id])
    relation = deactivate(relation)
    relation.save
    render nothing: true
  end

  def deactivate_relation
    relation = BrgManageRelation.find(params[:brg_manage_relation_id])
    relation = deactivate(relation)
    relation.save
    flash[:success] = t('views.brg_module.flash_messages.delete_success')
    redirect_to brg_managers_manage_path(relation.manager_id, params[:tab])
  end

  def assoc_group_massive_upload
  end

  def assoc_group_massive_upload_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_groups_assoc file, @process.id
        flash.now[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'assoc_group_massive_upload'
  end

  def assoc_managee_massive_upload
  end

  def assoc_managee_massive_upload_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_managees_assoc file, @process.id
        flash.now[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'assoc_managee_massive_upload'
  end

  private

  def editable_group
    return if BrgGroup.find(params[:brg_group_id]).editable?
    flash[:danger] = t('views.brg_module.flash_messages.no_editable_group_danger')
    redirect_to(brg_groups_index_path(t('views.brg_module.tabs.manager_index.groups')))
  end

  def editable_manager
    return if BrgManager.find(params[:brg_manager_id]).editable?
    flash[:danger] = t('views.brg_module.flash_messages.no_editable_manager_danger')
    redirect_to(brg_groups_index_path(t('views.brg_module.tabs.manager_index.groups')))
  end

end
