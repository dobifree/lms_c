class JPayBandsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def index
    @j_pay_bands = JPayBand.all
  end

  def show
    @j_pay_band = JPayBand.find(params[:id])
  end

  def new
    @j_pay_band = JPayBand.new
  end

  def edit
    @j_pay_band = JPayBand.find(params[:id])
  end

  def create

    params[:j_pay_band][:min_value] = fix_form_floats params[:j_pay_band][:min_value]
    params[:j_pay_band][:max_value] = fix_form_floats params[:j_pay_band][:max_value]
    params[:j_pay_band][:q1] = fix_form_floats params[:j_pay_band][:q1]
    params[:j_pay_band][:q2] = fix_form_floats params[:j_pay_band][:q2]
    params[:j_pay_band][:q3] = fix_form_floats params[:j_pay_band][:q3]

    @j_pay_band = JPayBand.new(params[:j_pay_band])

    if @j_pay_band.save
      flash[:success] = t('activerecord.success.model.j_pay_band.create_ok')
      redirect_to j_pay_bands_path
    else
      render action: 'new'
    end

  end

  # PUT /j_pay_bands/1
  # PUT /j_pay_bands/1.json
  def update
    @j_pay_band = JPayBand.find(params[:id])

    params[:j_pay_band][:min_value] = fix_form_floats params[:j_pay_band][:min_value]
    params[:j_pay_band][:max_value] = fix_form_floats params[:j_pay_band][:max_value]
    params[:j_pay_band][:q1] = fix_form_floats params[:j_pay_band][:q1]
    params[:j_pay_band][:q2] = fix_form_floats params[:j_pay_band][:q2]
    params[:j_pay_band][:q3] = fix_form_floats params[:j_pay_band][:q3]

    if @j_pay_band.update_attributes(params[:j_pay_band])
      flash[:success] = t('activerecord.success.model.j_pay_band.update_ok')
      redirect_to j_pay_bands_path
    else
      render action: 'edit'
    end

  end

  # DELETE /j_pay_bands/1
  # DELETE /j_pay_bands/1.json
  def destroy
    @j_pay_band = JPayBand.find(params[:id])
    @j_pay_band.destroy

    respond_to do |format|
      format.html { redirect_to j_pay_bands_url }
      format.json { head :no_content }
    end
  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'jobs', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

end
