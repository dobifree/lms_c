class BpLicensesController < ApplicationController

  def new
    @bp_license = BpLicense.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bp_license }
    end
  end

  def create
    license = BpLicense.new(params[:bp_license])
    license.registered_at = lms_time
    license.registered_by_admin_id = admin_connected.id
    if license.save
      flash[:success] = t('views.bp_licenses.flash_messages.success_created')
      redirect_to bp_groups_path
    else
      @bp_license = license
      render action: "new"
    end
  end

  def deactivate
    license = BpLicense.find(params[:bp_license_id])
    license.active = false
    license.deactivated_by_admin_id = admin_connected.id
    license.deactivated_at = lms_time
    if license.save
      flash[:success] = t('views.bp_licenses.flash_messages.success_deactivate')
    else
      flash[:danger] = t('views.bp_licenses.flash_messages.danger_deactivate')
    end
    redirect_to bp_groups_path
  end

end
