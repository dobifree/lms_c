class ScSchedulesController < ApplicationController

  before_filter :get_schedule_conditions, only: [:manage_schedule]

  before_filter :get_gui_characteristics, only: [:block_edit]

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def schedule_users_match_by_conditions
    schedule = ScSchedule.find(params[:sc_schedule_id])
    conditions = schedule.sc_characteristic_conditions.where(:sc_schedule_id => schedule.id, :active => true)
    users_id = []

    if conditions.size > 0
      user_characteristics = UserCharacteristic.where(:characteristic_id => conditions.first.characteristic_id)
    else
      user_characteristics = []
    end

    user_characteristics = user_characteristics.uniq_by { |obj| obj.user_id }
    user_characteristics.each {|obj| users_id << obj.user_id }


    conditions.each do |condition|
      user_characteristics = UserCharacteristic.where(:characteristic_id => condition.characteristic_id, :user_id => users_id)
      user_characteristics.delete_if {|user_characteristic| user_characteristic.value != condition.value }
      # user_characteristics = user_characteristics.uniq_by { |obj| obj.user_id }
      users_id = []
      user_characteristics.each { |obj| users_id << obj.user_id }
    end

    users = User.where(:id => users_id, :activo => true)
    schedule_users = ScScheduleUser.where(:sc_schedule_id => schedule.id, :active => true, :registered_manually => false)

    schedule_users.each_with_index do |schedule_user, index0|
      schedule_user_not_found = true
      users.each_with_index do |user, index1|
        if schedule_user.user_id == user.id
          unless schedule_user.he_able == user.he_able
            #Añadir todas las características relacionadas con el control de usuario que hayan cambiado desde la última actualización
            schedule_user.active = false
            schedule_user.deactivated_at = lms_time
            schedule_user.deactivated_by_user = user_connected
            schedule_user.save

            updated_schedule_user = ScScheduleUser.new(:user_id => user.id,
                                                       :sc_schedule_id => schedule.id,
                                                       :he_able => user.he_able)

            updated_schedule_user.registered_at = lms_time
            updated_schedule_user.registered_by_user = user_connected
            updated_schedule_user.active = true
            updated_schedule_user.save
          end
          users.delete_at(index1)
          schedule_user_not_found = false
        end
      end
      if schedule_user_not_found
        schedule_user.active = false
        schedule_user.deactivated_at = lms_time
        schedule_user.deactivated_by_user = user_connected
        schedule_user.save
      end
    end

    users.each do |user|
      new_schedule_user = ScScheduleUser.new(:user_id => user.id,
                                                 :sc_schedule_id => schedule.id,
                                                 :he_able => user.he_able)

      new_schedule_user.registered_at = lms_time
      new_schedule_user.registered_by_user = user_connected
      new_schedule_user.active = true
      new_schedule_user.save
    end

    render :nothing => true
  end

  def blocks_download_excel_headers
    respond_to do |format|
    format.xlsx { render xlsx: 'sc_schedules_blocks_massive_upload.xlsx', filename: t('views.sc_schedules.excel.files_names.blocks_massive_upload_headers'), locals: { only_headers: 1 } }
    end
    # respond_to do |format|
    #   format.xlsx { render xlsx: 'ben_cellphone_number_excel.xlsx', filename: t('views.ben_cellphone_process.shared.files_name.download_benefits'), locals: { only_headers: 1 } }
    #   #format.html { render index }
    # end
  end

  def block_deactivate
    block = ScBlock.find(params[:sc_block_id])
    block.active = false
    block.deactivated_at = lms_time
    block.sc_he_block_params.update_all(:active => false,
                                        :deactivated_at => lms_time)
    block.save
    flash[:success] = t('views.sc_processes.flash_messages.changes_success')
    redirect_to manage_schedule_path(block.sc_schedule, 0)
  end

  def refresh_blocks
    @sc_schedule = ScSchedule.find(params[:sc_schedule_id])
    @blocks = @sc_schedule.sc_blocks.where(:sc_schedule_id => params[:sc_schedule_id], :active => true).order(:day, :begin_time)

    render :partial => 'sc_block_table',
           :locals => { :sc_schedule => @sc_schedule,
                        :blocks => @blocks,
                        :editable => true }
  end

  # def delete_schedule_users
  #   schedule_user = ScScheduleUser.find(params[:sc_schedule_user_id])
  #   schedule_user.active = false
  #   schedule_user.deactivated_at = lms_time
  #   schedule_user.deactivated_by_user = user_connected
  #   schedule_user.deactivated_manually = true
  #   schedule_user.save
  #
  #   render :nothing => true
  # end

  # def schedule_users_create_manually
  #   user = User.find(params[:user_id])
  #   schedule = ScSchedule.find(params[:sc_schedule_id])
  #
  #   schedule_user = ScScheduleUser.where(:user_id => user.id,
  #                                        :sc_schedule_id => schedule.id,
  #                                        :active => true).first_or_initialize
  #   schedule_user.he_able = user.he_able
  #   schedule_user.registered_at = lms_time
  #   schedule_user.registered_by_user = user_connected
  #   schedule_user.active = true
  #   schedule_user.registered_manually = true
  #   schedule_user.save
  #
  #   flash[:success] = t('views.sc_schedules.flash_messages.success_messages')
  #   redirect_to manage_schedule_path(params[:sc_schedule_id], 2)
  # end

  # def refresh_schedule_users
  #   schedule = ScSchedule.find(params[:sc_schedule_id])
  #   schedule_users = schedule.sc_schedule_users.where(:active => true)
  #
  #   render partial: 'schedule_user_table',
  #          locals: {:schedule_users => schedule_users,
  #                   :editable => true,
  #                   :schedule => schedule }
  # end

  def create_schedule
    @sc_schedule = ScSchedule.new(params[:sc_schedule])
    @sc_schedule.registered_at = lms_time
    if @sc_schedule.save
      redirect_to manage_schedule_path(@sc_schedule,0)
    else
      render 'new'
    end
  end

  def load_characteristics_values_list
    values = CharacteristicValue.where(:characteristic_id =>  params[:characteristic_id]).reorder(:value_string).map{|char_value| [char_value.value_string, char_value.id]}
    render json: values
  end

  def search_people_create_schedule_user
    @user = User.new
    @users = {}
    @from_to = params[:search_for]
    @sc_schedule = ScSchedule.find(params[:sc_schedule_id])

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def manage_schedule
    bo = ScScheduleCharacteristic.condition.map{|char| [char.name, char.characteristic_id, {'data-data_type' => char.data_type_id}]}

    @sc_schedule = ScSchedule.find(params[:schedule_id])
    @sc_blocks = @sc_schedule.sc_blocks.where(:active => true).order(:day, :begin_time)
    @sc_schedule_users = @sc_schedule.sc_schedule_users.where(:active => true)
    @pill = defined?(params[:pill]) ? params[:pill] : 0
    @users = User.where(:activo => true).map{|user| [user.codigo + ' - ' + user.comma_full_name, user.id]}

    @conditions_for_select = [['- Seleccione característica -', 0]]
    @sc_schedule_conditions.each do |condition|
      @conditions_for_select << [condition.name, condition.characteristic_id]
    end

    @sc_conditions = @sc_schedule.sc_characteristic_conditions.where(:sc_schedule_id => @sc_schedule.id, :active => true).order(:characteristic_id)
  end


  def block_edit
    @sc_block = ScBlock.find(params[:sc_block_id])
    @sc_schedule = @sc_block.sc_schedule
    @sc_he_block_params = @sc_block.sc_he_block_params.where(:active => true).first
    @sc_he_block_params = ScHeBlockParam.new(:sc_block_id => @sc_block.id) unless @sc_he_block_params
    @sc_conditions = @sc_schedule.sc_characteristic_conditions.where(:sc_schedule_id => @sc_schedule.id, :active => true).order(:characteristic_id)
  end

  def block_update
    block = ScBlock.find(params[:sc_block_id])
    block.assign_attributes(params[:sc_block])
    if block.valid?
      original_block = ScBlock.find(params[:sc_block_id])
    edited_block = ScBlock.new(:name => params[:sc_block][:name],
                               :description => params[:sc_block][:description],
                               :day => params[:sc_block][:day],
                               :begin_time => params[:sc_block][:begin_time],
                               :end_time => params[:sc_block][:end_time],
                               :next_day => params[:sc_block][:next_day],
                               :workable => params[:sc_block][:workable],
                               :free_time => params[:sc_block][:free_time])


    if original_block.different?(edited_block)
      original_block.active = false
      original_block.deactivated_at = lms_time
      original_block.save

      edited_block.sc_schedule_id = original_block.sc_schedule_id
      edited_block.registered_at = lms_time
      edited_block.save
    end

      original_sc_he_block_params = params[:sc_block][:sc_he_block_params_attributes]['0'][:id] ? ScHeBlockParam.find(params[:sc_block][:sc_he_block_params_attributes]['0'][:id]) : nil
      edited_sc_he_block_params = ScHeBlockParam.new(:entry_min => params[:sc_block][:sc_he_block_params_attributes]['0'][:entry_min],
                                                     :entry_max => params[:sc_block][:sc_he_block_params_attributes]['0'][:entry_max],
                                                     :entry_include_min => params[:sc_block][:sc_he_block_params_attributes]['0'][:entry_include_min],
                                                     :entry_active => params[:sc_block][:sc_he_block_params_attributes]['0'][:entry_active],
                                                     :exit_min => params[:sc_block][:sc_he_block_params_attributes]['0'][:exit_min],
                                                     :exit_max => params[:sc_block][:sc_he_block_params_attributes]['0'][:exit_max],
                                                     :exit_include_min => params[:sc_block][:sc_he_block_params_attributes]['0'][:exit_include_min],
                                                     :exit_active => params[:sc_block][:sc_he_block_params_attributes]['0'][:exit_active])

      if original_sc_he_block_params
        if original_sc_he_block_params.different?(edited_sc_he_block_params)
          original_sc_he_block_params.active = false
          original_sc_he_block_params.deactivated_at = lms_time

          edited_sc_he_block_params.active = true
          edited_sc_he_block_params.sc_block_id = edited_block.id ? edited_block.id : params[:sc_block_id]
          edited_sc_he_block_params.registered_at = lms_time
        end
      else
        edited_sc_he_block_params.active = true
        edited_sc_he_block_params.sc_block_id = edited_block.id ? edited_block.id : params[:sc_block_id]
        edited_sc_he_block_params.registered_at = lms_time
      end
      flash[:success] = t('views.sc_processes.flash_messages.changes_success')
      redirect_to manage_schedule_path(original_block.sc_schedule, 0)
    else
      @sc_block = block
      @sc_schedule = @sc_block.sc_schedule
      @sc_he_block_params = @sc_block.sc_he_block_params.where(:active => true).first
      @sc_he_block_params = ScHeBlockParam.new(:sc_block_id => @sc_block.id) unless @sc_he_block_params
      @sc_conditions = @sc_schedule.sc_characteristic_conditions.where(:sc_schedule_id => @sc_schedule.id, :active => true).order(:characteristic_id)

      render action: 'block_edit'
    end

  end

  def block_new_modal
    block = ScBlock.new(:sc_schedule_id => params[:sc_schedule_id])
    block.sc_he_block_params.build
    render partial: 'block_new_modal',
           locals: {:block => block}
  end

  def block_create
    block = ScBlock.new(params[:sc_block])
    block.sc_schedule_id = params[:sc_schedule_id]
    if block.save
      success_message = true
      block = ScBlock.new(:sc_schedule_id => params[:sc_schedule_id])
      block.sc_he_block_params.build
    else
      success_message = false
    end

      render partial: 'block_new_modal',
           locals: {:block => block,
                    :success_message => success_message}
  end

  def block_delete

  end


  def delete_block
    block = ScBlock.find(params[:block_id])
    block.active = false
    block.deactivated_at = lms_time
    block.save
    render json: block
  end

  def update_block
    origin_block = ScBlock.find(params[:sc_block_id])
    edited_block = ScBlock.new(:name => params[:name],
                               :description => params[:description],
                               :day => params[:day],
                               :begin_time => params[:begin_time],
                               :end_time => params[:end_time],
                               :next_day => params[:next_day],
                               :workable => params[:workable] )

    if origin_block.different?(edited_block)
      origin_block.active = false
      origin_block.deactivated_at = lms_time
      origin_block.save

      edited_block.sc_schedule_id = origin_block.sc_schedule_id
      edited_block.registered_at = lms_time
      edited_block.save
    end


    @sc_schedule = ScSchedule.find(params[:sc_schedule_id])
    @blocks = @sc_schedule.sc_blocks.where(:sc_schedule_id => params[:sc_schedule_id], :active => true).order(:day, :begin_time)

    render partial: 'sc_block_row_form',
           locals: {:block => block,
                    :block_form => f_blocks,
                    :just_added => false,
                    :editable => true}
    render :nothing => true
  end

  def create_block
    block = ScBlock.create(:name => params[:name],
                           :description => params[:description],
                           :day => params[:day],
                           :begin_time => params[:begin_time],
                           :end_time => params[:end_time],
                           :next_day => params[:next_day],
                           :free_time => params[:free_time],
                           :workable => params[:workable],
                           :sc_schedule_id => params[:sc_schedule_id])

    block.registered_at = lms_time
    block.save

    render :nothing => true
  end

  def create_condition
    @sc_condition = ScCharacteristicCondition.new( :characteristic_id => params[:characteristic_id],
                                                      :condition => params[:condition],
                                                      :sc_schedule_id => params[:schedule_id],
                                                      :match_value_string => params[:string_val],
                                                      :match_value_text => params[:text_val],
                                                      :match_value_date => params[:date_val],
                                                      :match_value_int => params[:int_val],
                                                      :match_value_float => params[:float_val],
                                                      :match_value_char_id => params[:list_val])
    @sc_condition.active = true
    @sc_condition.registered_at = lms_time
    @sc_condition.save

    render :nothing => true
  end

  def update_condition
    origin_condition = ScCharacteristicCondition.find(params[:sc_characteristic_condition_id])
    edited_condition = ScCharacteristicCondition.new(:characteristic_id => params[:characteristic_id],
                                                     :sc_schedule_id => origin_condition.sc_schedule_id,
                                                     :condition => params[:condition],
                                                     :match_value_string => params[:string_val],
                                                     :match_value_text => params[:text_val],
                                                     :match_value_date => params[:date_val],
                                                     :match_value_int => params[:int_val],
                                                     :match_value_float => params[:float_val],
                                                     :match_value_char_id => params[:list_val])

    if origin_condition.different?(edited_condition)
      origin_condition.active = false
      origin_condition.deactivated_at = lms_time
      origin_condition.save

      edited_condition.active = true
      edited_condition.registered_at = lms_time
      edited_condition.save
    end

    render :nothing => true
  end

  def refresh_conditions
    @sc_schedule = ScSchedule.find(params[:sc_schedule_id])
    @sc_conditions = @sc_schedule.sc_characteristic_conditions.where(:sc_schedule_id => params[:sc_schedule_id], :active => true).order(:characteristic_id)

    render partial: 'sc_condition_table',
           locals: {:conditions => @sc_conditions,
                    :editable => true,
                    :schedule => @sc_schedule }
  end

  def delete_condition
    condition = ScCharacteristicCondition.find(params[:sc_characteristic_condition_id])
    condition.active = false
    condition.deactivated_at = lms_time
    condition.save
    render :nothing => true
  end

  def index
    @sc_schedules = ScSchedule.all
    @sc_conditions = []
    @sc_schedules.each do |schedule|
      @sc_conditions << schedule.sc_characteristic_conditions.where(:sc_schedule_id => schedule.id, :active => true).order(:characteristic_id)
    end


    respond_to do |format|
      format.html
      format.json { render json: @sc_schedules }
    end
  end

  def new
    @sc_schedule = ScSchedule.new

    conditions = Characteristic.where(:publica => true )
    @conditions_for_select = [['- Seleccione característica -', 0]]
    conditions.each do |condition|
      @conditions_for_select << [condition.nombre, condition.id]
    end

    respond_to do |format|
      format.html
      format.json { render json: @sc_schedule }
    end
  end

  def edit_schedule
    @sc_schedule = ScSchedule.find(params[:sc_schedule_id])
    render 'edit'
  end

  def update_schedule
    schedule = ScSchedule.find(params[:sc_schedule_id])

    respond_to do |format|
      if schedule.update_attributes(params[:sc_schedule])
        unless schedule.active
          schedule.deactivated_at = lms_time
        else
          schedule.deactivated_at = nil
        end
        schedule.save
        format.html { redirect_to manage_schedule_path(params[:sc_schedule_id], 0) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @sc_schedule = ScSchedule.new(params[:sc_schedule])

    @sc_schedule.sc_characteristic_conditions.each do |condition|

    end

    respond_to do |format|
      if @sc_schedule.save
        format.html { redirect_to @sc_schedule, notice: 'Sc schedule was successfully created.' }
        format.json { render json: @sc_schedule, status: :created, location: @sc_schedule }
      else
        format.html { render action: "new" }
        format.json { render json: @sc_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # def update
  #   @sc_schedule = ScSchedule.find(params[:id])
  #
  #   respond_to do |format|
  #     if @sc_schedule.update_attributes(params[:sc_schedule])
  #       format.html { redirect_to @sc_schedule, notice: 'Sc schedule was successfully updated.' }
  #       format.json { head :no_content }
  #     else
  #       format.html { render action: "edit" }
  #       format.json { render json: @sc_schedule.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # def destroy
  #   @sc_schedule = ScSchedule.find(params[:id])
  #   @sc_schedule.destroy
  #
  #   respond_to do |format|
  #     format.html { redirect_to sc_schedules_url }
  #     format.json { head :no_content }
  #   end
  # end

  private

  def get_gui_characteristics
    @sc_schedule_characteristics = ScScheduleCharacteristic.gui
  end

  def get_schedule_conditions
    @sc_schedule_conditions = ScScheduleCharacteristic.condition
  end

  def validate_active_schedule
    unless ScSchedule.find(params[:sc_schedule_id]).active
      flash[:danger] = t('views.sc_schedules.flash_messages.deactivated_schedule')
      redirect_to manage_schedule_path(params[:sc_schedule_id], 0)
    end
  end
end