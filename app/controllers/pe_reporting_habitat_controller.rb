class PeReportingHabitatController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_2, only: [:generate_rep_habitat_bpl]

  def generate_rep_habitat_bpl

    d = generate_rep_habitat_0
    r = SecureRandom.hex(5)

    if params[:r] == '0'

      cmd = "./jasperstarter pr afp_habitat_report.jasper -f pdf -t xml --xml-xpath databases/general --data-file #{d}.xml -o #{r}"

      name_file = 'bpl_'+@pe_process.period+'_'+@pe_member.user.codigo+'.pdf'

    elsif params[:r] == 'sin_asc'

      cmd = "./jasperstarter pr afp_habitat_report_sin_asc.jasper -f pdf -t xml --xml-xpath databases/general --data-file #{d}.xml -o #{r}"

      name_file = 'bpl_'+@pe_process.period+'_'+@pe_member.user.codigo+'_sin_asc.pdf'

    elsif params[:r] == 'sin_auto'

      cmd = "./jasperstarter pr afp_habitat_report_sin_auto.jasper -f pdf -t xml --xml-xpath databases/general --data-file #{d}.xml -o #{r}"

      name_file = 'bpl_'+@pe_process.period+'_'+@pe_member.user.codigo+'_sin_auto.pdf'

    elsif params[:r] == 'sin_auto_pares'

      cmd = "./jasperstarter pr afp_habitat_report_sin_auto_pares.jasper -f pdf -t xml --xml-xpath databases/general --data-file #{d}.xml -o #{r}"

      name_file = 'bpl_'+@pe_process.period+'_'+@pe_member.user.codigo+'_sin_auto_pares.pdf'

    elsif params[:r] == 'sin_pares'

      cmd = "./jasperstarter pr afp_habitat_report_sin_pares.jasper -f pdf -t xml --xml-xpath databases/general --data-file #{d}.xml -o #{r}"

      name_file = 'bpl_'+@pe_process.period+'_'+@pe_member.user.codigo+'_sin_pares.pdf'

    end

    Dir.chdir('/var/www/remote-storage/lms-storage/reps_jasper_habitat/jasperstarter2/bin/'){
      %x[#{cmd}]
    }

    send_file '/var/www/remote-storage/lms-storage/reps_jasper_habitat/jasperstarter2/bin/'+r+'.pdf',
              filename: name_file,
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def generate_rep_habitat_0

    @pe_process_prev = PeProcess.where('name = ? AND period = ?', @pe_process.name, @pe_process.period.to_i-1).first
    pe_evaluation_auto_prev = @pe_process_prev.pe_evaluations.where('name = ?', 'Autoevaluación').first if @pe_process_prev
    pe_evaluation_ascendente_prev = @pe_process_prev.pe_evaluations.where('name = ?', 'Evaluación Ascendente').first if @pe_process_prev
    pe_member_prev = @pe_process_prev.pe_member_by_user @pe_member.user if @pe_process_prev

    user = @pe_member.user

    pe_evaluation_auto = @pe_process.pe_evaluations.where('name = ?', 'Autoevaluación').first
    pe_evaluation_ascendente = @pe_process.pe_evaluations.where('name = ?', 'Evaluación Ascendente').first
    pe_evaluation_par = @pe_process.pe_evaluations.where('name = ?', 'Evaluación Pares').first

    pe_rel_par = @pe_process.pe_rel_by_id 2
    pe_rel_as = @pe_process.pe_rel_by_id 3
    pe_rel_sub = @pe_process.pe_rel_by_id 3
    pe_rel_auto = @pe_process.pe_rel_by_id 0

    pe_member_rels_par = @pe_member.pe_member_rels_is_evaluated_as_pe_rel pe_rel_par
    pe_member_rels_sub = @pe_member.pe_member_rels_is_evaluated_as_pe_rel pe_rel_sub
    pe_member_rels_as = @pe_member.pe_member_rels_is_evaluated_as_pe_rel pe_rel_as

    pond_asc = 80
    pond_au = 10

    pe_questions_asc_1 = Array.new
    pe_questions_asc_2 = Array.new

    ### asc

    pe_assessment_question_asc_1_value = Array.new
    pe_assessment_question_asc_1_color = Array.new

    pe_assessment_question_asc_1_int_value = Array.new
    pe_assessment_question_asc_1_decimal_value = Array.new
    pe_assessment_question_asc_1_extra_color = Array.new

    pe_assessment_question_asc_2_value = Array.new
    pe_assessment_question_asc_2_color = Array.new

    pe_assessment_evaluation_asc = Array.new

    res_global_asc = 0

    pe_member_rels_as.each do |pmr|

      if pmr.valid_evaluator && pmr.finished

        pae = pmr.pe_assessment_evaluation pe_evaluation_ascendente

        if pae.valid_evaluation
          pe_assessment_evaluation_asc.push(pae)
          res_global_asc += pae.percentage
        end

      end

    end

    global_asc_color = '#FFFFFF'
    global_asc_value = ' '

    if pe_assessment_evaluation_asc.size > 0
      v = (res_global_asc/pe_assessment_evaluation_asc.size)/20.0
      fin_asc = v*pond_asc/100.0
      global_asc_value = number_with_precision(v, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
      global_asc_color = '#FC0D1B' if v <= 2.75
      global_asc_color = '#FFFD38' if v >= 2.76 && v <= 3.99
      global_asc_color = '#94CE58' if v >= 4
    end
    pe_evaluation_ascendente.pe_questions.where('pe_question_id IS NULL AND NOT pe_questions.description LIKE ? AND informative <> ?', '%PREGUNTA GENERAL%', true).each do |pe_q|

      pe_questions_asc_1.push pe_q

      t1 = 0

      pe_assessment_evaluation_asc.each do |pae|
        pe_aq = pae.pe_assessment_question pe_q
        t1 += pe_aq.percentage if pe_aq && pe_aq.percentage
      end

      c1_asc = '#FFFFFF'
      v1_asc = ' '

      extra_c = '#FFFFFF'

      if pe_assessment_evaluation_asc.size > 0

        t1 = t1 / pe_assessment_evaluation_asc.size

        v1_asc = t1/20.0
        c1_asc = '#FC0D1B' if v1_asc <= 2.75
        c1_asc = '#FFFD38' if v1_asc >= 2.76 && v1_asc <= 3.99
        c1_asc = '#94CE58' if v1_asc >= 4

        sep = v1_asc.round(1).to_s.split('.')
        pe_assessment_question_asc_1_int_value.push sep[0]
        pe_assessment_question_asc_1_decimal_value.push sep[1]

        extra_c = '#DDD9C4' if v1_asc < 3.8
        extra_c = '#FFFFFF' if v1_asc >= 3.8 && v1_asc < 4.8
        extra_c = '#DBE5F0' if v1_asc >= 4.8

      else
        pe_assessment_question_asc_1_int_value.push 0
        pe_assessment_question_asc_1_decimal_value.push 0

      end

      pe_assessment_question_asc_1_extra_color.push extra_c

      pe_q.pe_questions.each do |pe_q2|
        pe_questions_asc_2.push pe_q2

        t2 = 0

        pe_assessment_evaluation_asc.each do |pae|
          pe_aq2 = pae.pe_assessment_question pe_q2
          t2 += pe_aq2.pe_alternative.value if pe_aq2 && pe_aq2.pe_alternative
        end

        c2_asc = '#FFFFFF'
        v2_asc = ' '

        if pe_assessment_evaluation_asc.size > 0

          t2 = t2 / pe_assessment_evaluation_asc.size

          v2_asc = t2/20.0

          c2_asc = '#FC0D1B' if v2_asc <= 2.75
          c2_asc = '#FFFD38' if v2_asc >= 2.76 && v2_asc <= 3.99
          c2_asc = '#94CE58' if v2_asc >= 4
        end

        pe_assessment_question_asc_2_value.push number_with_precision(v2_asc, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
        pe_assessment_question_asc_2_color.push c2_asc

        pe_assessment_question_asc_1_value.push number_with_precision(v1_asc, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
        pe_assessment_question_asc_1_color.push c1_asc

      end


    end

    sep = global_asc_value.split(',')
    pe_assessment_question_asc_1_int_value.push sep[0]
    pe_assessment_question_asc_1_decimal_value.push sep[1]

    comments_asc = ''

    pe_evaluation_ascendente.pe_questions.where('pe_question_id IS NOT NULL AND informative = ?', true).each do |pe_q2|

      pe_assessment_evaluation_asc.each do |pae|
        pe_aq2 = pae.pe_assessment_question pe_q2
        comments_asc += '• '+pe_aq2.comment+'<br><br>' unless pe_aq2.comment.blank?
      end

    end

    comments_asc = 'Sin comentarios' if comments_asc.blank?

    ##### asc proceso

    pe_assessment_question_asc_proc_1_int_value = Array.new
    pe_assessment_question_asc_proc_1_decimal_value = Array.new
    pe_assessment_question_asc_proc_1_extra_color = Array.new

    pe_assessment_evaluation_asc_proc = Array.new

    res_global_asc_proc = 0

    @pe_process.pe_member_rels_as_pe_rel(pe_rel_as).each do |pmr|

      if pmr.valid_evaluator && pmr.finished && !pmr.pe_member_evaluated.removed

        pae = pmr.pe_assessment_evaluation pe_evaluation_ascendente

        if pae.valid_evaluation
          pe_assessment_evaluation_asc_proc.push(pae)
          res_global_asc_proc += pae.percentage
        end

      end

    end

    t11 = 0

    pe_evaluation_ascendente.pe_questions.where('pe_question_id IS NULL AND NOT pe_questions.description LIKE ? AND informative <> ?', '%PREGUNTA GENERAL%', true).each do |pe_q|

      t1 = 0

      pe_assessment_evaluation_asc_proc.each do |pae|
        pe_aq = pae.pe_assessment_question pe_q
        t1 += pe_aq.percentage if pe_aq && pe_aq.percentage
      end

      if pe_assessment_evaluation_asc_proc.size > 0

        t1 = t1 / pe_assessment_evaluation_asc_proc.size

        v1_asc_proc = t1/20.0

        t11 += v1_asc_proc

        sep = v1_asc_proc.round(1).to_s.split('.')
        pe_assessment_question_asc_proc_1_int_value.push sep[0]
        pe_assessment_question_asc_proc_1_decimal_value.push sep[1]

      else
        pe_assessment_question_asc_proc_1_int_value.push 0
        pe_assessment_question_asc_proc_1_decimal_value.push 0

      end

    end

    t11 = t11/7

    sep = t11.round(1).to_s.split('.')

    pe_assessment_question_asc_proc_1_int_value.push sep[0]
    pe_assessment_question_asc_proc_1_decimal_value.push sep[1]

    
    ### asc_prev

    pe_assessment_question_asc_prev_1_int_value = Array.new
    pe_assessment_question_asc_prev_1_decimal_value = Array.new

    pe_assessment_evaluation_asc_prev = Array.new

    res_global_asc_prev = 0

    if @pe_process_prev

      if pe_member_prev && pe_evaluation_ascendente_prev

        pe_rel_as_prev = @pe_process.pe_rel_by_id 3

        pe_member_prev.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_as_prev).each do |pmr|

          if pmr.valid_evaluator && pmr.finished

            pae = pmr.pe_assessment_evaluation pe_evaluation_ascendente_prev

            if pae.valid_evaluation
              pe_assessment_evaluation_asc_prev.push(pae)
              res_global_asc_prev += pae.percentage
            end

          end

        end

        global_asc_prev_color = '#FFFFFF'
        global_asc_prev_value = ' '
        if pe_assessment_evaluation_asc_prev.size > 0
          res_global_asc_prev = (res_global_asc_prev/pe_assessment_evaluation_asc_prev.size)/20.0
        end

        pe_evaluation_ascendente_prev.pe_questions.where('pe_question_id IS NULL AND NOT pe_questions.description LIKE ? AND informative <> ?', '%PREGUNTA GENERAL%', true).each do |pe_q|

          t1 = 0

          pe_assessment_evaluation_asc_prev.each do |pae|
            pe_aq = pae.pe_assessment_question pe_q
            t1 += pe_aq.percentage if pe_aq && pe_aq.percentage
          end

          v1_asc_prev = ' '

          if pe_assessment_evaluation_asc_prev.size > 0

            t1 = t1 / pe_assessment_evaluation_asc_prev.size

            v1_asc_prev = t1/20.0

            sep = v1_asc_prev.round(1).to_s.split('.')
            pe_assessment_question_asc_prev_1_int_value.push sep[0]
            pe_assessment_question_asc_prev_1_decimal_value.push sep[1]

          else
            pe_assessment_question_asc_prev_1_int_value.push 0
            pe_assessment_question_asc_prev_1_decimal_value.push 0

          end

        end
      else

        8.times do
          pe_assessment_question_asc_prev_1_int_value.push 0
          pe_assessment_question_asc_prev_1_decimal_value.push 0
        end


      end

    else

      8.times do
        pe_assessment_question_asc_prev_1_int_value.push 0
        pe_assessment_question_asc_prev_1_decimal_value.push 0
      end

    end


    #### par

    pe_questions_par_1 = Array.new
    pe_questions_par_2 = Array.new

    pe_assessment_question_par_1_value = Array.new
    pe_assessment_question_par_1_color = Array.new

    pe_assessment_question_par_2_value = Array.new
    pe_assessment_question_par_2_color = Array.new

    pe_assessment_evaluation_par = Array.new

    res_global_par = 0

    pe_member_rels_par.each do |pmr|

      if pmr.valid_evaluator && pmr.finished

        pae = pmr.pe_assessment_evaluation pe_evaluation_par

        if pae.valid_evaluation
          pe_assessment_evaluation_par.push(pae)
          res_global_par += pae.percentage
        end

      end

    end

    global_par_color = '#FFFFFF'
    global_par_value = ' '
    if pe_assessment_evaluation_par.size > 0
      v = (res_global_par/pe_assessment_evaluation_par.size)/20.0

      global_par_value = number_with_precision(v, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
      global_par_color = '#FC0D1B' if v <= 2.75
      global_par_color = '#FFFD38' if v >= 2.76 && v <= 3.99
      global_par_color = '#94CE58' if v >= 4
    end

    pe_evaluation_par.pe_questions.where('pe_question_id IS NULL AND NOT pe_questions.description LIKE ? AND informative <> ?', '%PREGUNTA GENERAL%', true).each do |pe_q|

      pe_questions_par_1.push pe_q

      t1 = 0

      pe_assessment_evaluation_par.each do |pae|
        pe_aq = pae.pe_assessment_question pe_q
        t1 += pe_aq.percentage if pe_aq && pe_aq.percentage
      end

      c1_par = '#FFFFFF'
      v1_par = ' '

      if pe_assessment_evaluation_par.size > 0

        t1 = t1 / pe_assessment_evaluation_par.size

        v1_par = t1/20.0
        c1_par = '#FC0D1B' if v1_par <= 2.75
        c1_par = '#FFFD38' if v1_par >= 2.76 && v1_par <= 3.99
        c1_par = '#94CE58' if v1_par >= 4
      end

      pe_q.pe_questions.each do |pe_q2|
        pe_questions_par_2.push pe_q2

        t2 = 0
        n2 = 0

        pe_assessment_evaluation_par.each do |pae|
          pe_aq2 = pae.pe_assessment_question pe_q2
          if pe_aq2 && pe_aq2.pe_alternative && pe_aq2.pe_alternative.value > -1
            t2 += pe_aq2.pe_alternative.value
            n2 += 1
          end
        end

        c2_par = '#FFFFFF'
        v2_par = ' '

        if n2 > 0

          t2 = t2 / n2

          v2_par = t2/20.0

          c2_par = '#FC0D1B' if v2_par <= 2.75
          c2_par = '#FFFD38' if v2_par >= 2.76 && v2_par <= 3.99
          c2_par = '#94CE58' if v2_par >= 4
        end

        pe_assessment_question_par_2_value.push number_with_precision(v2_par, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
        pe_assessment_question_par_2_color.push c2_par

        pe_assessment_question_par_1_value.push number_with_precision(v1_par, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
        pe_assessment_question_par_1_color.push c1_par

      end

    end

    comments_par = ''

    pe_evaluation_par.pe_questions.where('pe_question_id IS NOT NULL AND informative = ?', true).each do |pe_q2|

      pe_assessment_evaluation_par.each do |pae|
        pe_aq2 = pae.pe_assessment_question pe_q2
        comments_par += '• '+pe_aq2.comment+'<br><br>' unless pe_aq2.comment.blank?
      end

    end

    comments_par = 'Sin comentarios' if comments_par.blank?


    ##### auto

    pe_questions_auto_1 = Array.new
    pe_questions_auto_2 = Array.new

    pe_assessment_question_auto_1_value = Array.new
    pe_assessment_question_auto_1_color = Array.new

    pe_assessment_question_auto_1_int_value = Array.new
    pe_assessment_question_auto_1_decimal_value = Array.new
    pe_assessment_question_auto_1_extra_color = Array.new

    pe_assessment_question_auto_2_value = Array.new
    pe_assessment_question_auto_2_color = Array.new


    pe_member_rel_auto = @pe_member.pe_member_rel_is_evaluated_by_pe_member @pe_member
    pe_assessment_evaluation_auto = pe_member_rel_auto.pe_assessment_evaluation pe_evaluation_auto

    res_global_auto = 0

    res_global_auto = pe_assessment_evaluation_auto.percentage if pe_assessment_evaluation_auto

    global_auto_color = '#FFFFFF'
    global_auto_value = ' '
    if pe_assessment_evaluation_auto
      v = (res_global_auto)/20.0
      fin_au = v*pond_au/100.0
      global_auto_value = number_with_precision(v, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
      global_auto_color = '#FC0D1B' if v <= 2.75
      global_auto_color = '#FFFD38' if v >= 2.76 && v <= 3.99
      global_auto_color = '#94CE58' if v >= 4
    end

    pe_evaluation_auto.pe_questions.where('pe_question_id IS NULL AND NOT pe_questions.description LIKE ? AND informative <> ?', '%PREGUNTA GENERAL%', true).each do |pe_q|

      pe_questions_auto_1.push pe_q

      pe_aq = pe_assessment_evaluation_auto.pe_assessment_question(pe_q) if pe_assessment_evaluation_auto

      c1_auto = '#FFFFFF'
      v1_auto = ' '

      extra_c = '#FFFFFF'

      if pe_aq && pe_aq.percentage
        v1_auto = pe_aq.percentage/20.0
        c1_auto = '#FC0D1B' if v1_auto <= 2.75
        c1_auto = '#FFFD38' if v1_auto >= 2.76 && v1_auto <= 3.99
        c1_auto = '#94CE58' if v1_auto >= 4

        sep = v1_auto.round(1).to_s.split('.')

        pe_assessment_question_auto_1_int_value.push sep[0]
        pe_assessment_question_auto_1_decimal_value.push sep[1]

        extra_c = '#DDD9C4' if v1_auto < 3.8
        extra_c = '#FFFFFF' if v1_auto >= 3.8 && v1_auto < 4.8
        extra_c = '#DBE5F0' if v1_auto >= 4.8

      else
        pe_assessment_question_auto_1_int_value.push 0
        pe_assessment_question_auto_1_decimal_value.push 0
      end

      pe_assessment_question_auto_1_extra_color.push extra_c

      pe_q.pe_questions.each do |pe_q2|
        pe_questions_auto_2.push pe_q2

        c2_auto = '#FFFFFF'
        v2_auto = ' '

        if pe_assessment_evaluation_auto

          pe_aq2 = pe_assessment_evaluation_auto.pe_assessment_question pe_q2


          if pe_aq2 && pe_aq2.pe_alternative
            v2_auto = pe_aq2.pe_alternative.value/20.0
            c2_auto = '#FC0D1B' if v2_auto <= 2.75
            c2_auto = '#FFFD38' if v2_auto >= 2.76 && v2_auto <= 3.99
            c2_auto = '#94CE58' if v2_auto >= 4
          end

        end

        pe_assessment_question_auto_2_value.push number_with_precision(v2_auto, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
        pe_assessment_question_auto_2_color.push c2_auto

        pe_assessment_question_auto_1_value.push number_with_precision(v1_auto, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
        pe_assessment_question_auto_1_color.push c1_auto



      end

    end


    sep = global_auto_value.to_s.split(',')

    pe_assessment_question_auto_1_int_value.push sep[0]
    pe_assessment_question_auto_1_decimal_value.push sep[1]

    ##### auto prev
    #

    pe_assessment_question_auto_prev_1_int_value = Array.new
    pe_assessment_question_auto_prev_1_decimal_value = Array.new

    if @pe_process_prev

      if pe_member_prev && pe_evaluation_auto_prev

        pe_member_rel_auto_prev = pe_member_prev.pe_member_rel_is_evaluated_by_pe_member pe_member_prev

        if pe_member_rel_auto_prev

          pe_assessment_evaluation_auto_prev = pe_member_rel_auto_prev.pe_assessment_evaluation(pe_evaluation_auto_prev)

          pe_evaluation_auto_prev.pe_questions.where('pe_question_id IS NULL AND NOT pe_questions.description LIKE ? AND informative <> ?', '%PREGUNTA GENERAL%', true).each do |pe_q|

            pe_aq = pe_assessment_evaluation_auto_prev.pe_assessment_question(pe_q) if pe_assessment_evaluation_auto_prev

            c1_auto_prev = '#FFFFFF'
            v1_auto_prev = ' '

            extra_c = '#FFFFFF'

            if pe_aq && pe_aq.percentage
              v1_auto_prev = pe_aq.percentage/20.0

              sep = v1_auto_prev.round(1).to_s.split('.')
              pe_assessment_question_auto_prev_1_int_value.push sep[0]
              pe_assessment_question_auto_prev_1_decimal_value.push sep[1]

            else
              pe_assessment_question_auto_prev_1_int_value.push 0
              pe_assessment_question_auto_prev_1_decimal_value.push 0
            end


          end

        else

          8.times do
            pe_assessment_question_auto_prev_1_int_value.push 0
            pe_assessment_question_auto_prev_1_decimal_value.push 0
          end

        end

      else
        8.times do
          pe_assessment_question_auto_prev_1_int_value.push 0
          pe_assessment_question_auto_prev_1_decimal_value.push 0
        end
      end

    else

      8.times do
        pe_assessment_question_auto_prev_1_int_value.push 0
        pe_assessment_question_auto_prev_1_decimal_value.push 0
      end

    end


    ### calculos final


    if fin_au
      fin_au_f = number_with_precision(fin_au, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
    else
      fin_au_f = ' '
    end

    if fin_asc
      fin_asc_f = number_with_precision(fin_asc, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
    else
      fin_asc_f = ' '
    end

    if fin_asc.nil?
      fin_asc = 0
    end

    if fin_au.nil?
      fin_au = 0
    end

    prom_fin = fin_asc+fin_au

    prom_fin_f = number_with_precision(prom_fin, precision: 1, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)


    ##### auto proceso

    pe_assessment_question_auto_proc_1_int_value = Array.new
    pe_assessment_question_auto_proc_1_decimal_value = Array.new

    pe_assessment_evaluation_auto_proc = Array.new

    res_global_auto_proc = 0

    @pe_process.pe_member_rels_as_pe_rel(pe_rel_auto).each do |pmr|

      if pmr.valid_evaluator && pmr.finished && !pmr.pe_member_evaluated.removed

        pae = pmr.pe_assessment_evaluation pe_evaluation_auto

        if pae.valid_evaluation
          pe_assessment_evaluation_auto_proc.push(pae)
          res_global_auto_proc += pae.percentage
        end

      end

    end

    t11 = 0

    pe_evaluation_auto.pe_questions.where('pe_question_id IS NULL AND NOT pe_questions.description LIKE ? AND informative <> ?', '%PREGUNTA GENERAL%', true).each do |pe_q|

      t1 = 0

      pe_assessment_evaluation_auto_proc.each do |pae|
        pe_aq = pae.pe_assessment_question pe_q
        t1 += pe_aq.percentage if pe_aq && pe_aq.percentage
      end

      if pe_assessment_evaluation_auto_proc.size > 0

        t1 = t1 / pe_assessment_evaluation_auto_proc.size

        v1_auto_proc = t1/20.0

        t11 += v1_auto_proc

        sep = v1_auto_proc.round(1).to_s.split('.')
        pe_assessment_question_auto_proc_1_int_value.push sep[0]
        pe_assessment_question_auto_proc_1_decimal_value.push sep[1]

      else
        pe_assessment_question_auto_proc_1_int_value.push 0
        pe_assessment_question_auto_proc_1_decimal_value.push 0

      end

    end

    t11 = t11/7

    sep = t11.round(1).to_s.split('.')

    pe_assessment_question_auto_proc_1_int_value.push sep[0]
    pe_assessment_question_auto_proc_1_decimal_value.push sep[1]
    
    #####

    d = SecureRandom.hex(5)

    filename = '/var/www/remote-storage/lms-storage/reps_jasper_habitat/jasperstarter2/bin/database_base.xml'
    xml = File.read(filename)
    doc = Nokogiri::XML(xml)

    doc.xpath('//databases//general//nombres').each do |nombres|
      nombres.content = user.nombre+' '+user.apellidos
    end

    doc.xpath('//databases//general//ano-evaluacion').each do |ae|
      ae.content = @pe_process.period
    end

    doc.xpath('//databases//general//ano-comparacion').each do |ac|
      ac.content = @pe_process.period.to_i - 1
    end

    doc.xpath('//databases//general//cargo').each do |cargo|
      cargo.content = user.first_owned_node.nombre
    end

    doc.xpath('//databases//general//cant-eval-pares').each do |num_e_p|
      num_e_p.content = pe_member_rels_par.size
    end

    doc.xpath('//databases//general//cant-eval-asc').each do |num_e_a|
      num_e_a.content = pe_member_rels_sub.size
    end

    doc.xpath('//databases//general//comentario-as-au').each do |c|
      c.content = comments_asc
    end

    doc.xpath('//databases//general//comentario-pares').each do |c|
      c.content = comments_par
    end

    doc.xpath('//databases//general//eval-glob-lider-as-au').each do |c|
      c.content = global_asc_value
    end

    doc.xpath('//databases//general//eval-glob-lider-as-au-color').each do |c|
      c.content = global_asc_color
    end

    doc.xpath('//databases//general//eval-glob-lider-pares').each do |c|
      c.content = global_par_value
    end

    doc.xpath('//databases//general//eval-glob-lider-pares-color').each do |c|
      c.content = global_par_color
    end

    doc.xpath('//databases//general//prom-dim-asc').each do |c|
      c.content = global_asc_value
    end

    doc.xpath('//databases//general//prom-dim-asc-color').each do |c|
      c.content = global_asc_color
    end

    doc.xpath('//databases//general//prom-dim-auto').each do |c|
      c.content = global_auto_value
    end

    doc.xpath('//databases//general//prom-dim-auto-color').each do |c|
      c.content = global_auto_color
    end

    doc.xpath('//databases//general//prom-dim-pares').each do |c|
      c.content = global_par_value
    end

    doc.xpath('//databases//general//prom-dim-pares-color').each do |c|
      c.content = global_par_color
    end

    doc.xpath('//databases//general//pond-as-final').each do |c|
      c.content = fin_asc_f
    end

    doc.xpath('//databases//general//pond-au-final').each do |c|
      c.content = fin_au_f
    end

    doc.xpath('//databases//general//pond-final').each do |c|
      c.content = prom_fin_f
    end

    doc.xpath('//databases//general//pond-as').each do |c|
      c.content = pond_asc.to_s+'%'
    end

    doc.xpath('//databases//general//pond-au').each do |c|
      c.content = pond_au.to_s+'%'
    end


    doc.xpath('//databases//resumen12//dimension').each_with_index do |question_1, index_q|

      if index_q <= 6
        question_1.content = pe_questions_auto_1[index_q].description.gsub('<strong>','').gsub('</strong>','') if pe_questions_auto_1[index_q]
      end

    end

    doc.xpath('//databases//resumen12//value-as-int').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_1_int_value[index_q] if pe_assessment_question_asc_1_int_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-as-dec').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_1_decimal_value[index_q] if pe_assessment_question_asc_1_decimal_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-as-color').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_1_extra_color[index_q] if pe_assessment_question_asc_1_extra_color[index_q]

    end

    doc.xpath('//databases//resumen12//value-as-prev-int').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_prev_1_int_value[index_q] if pe_assessment_question_asc_prev_1_int_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-as-prev-dec').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_prev_1_decimal_value[index_q] if pe_assessment_question_asc_prev_1_decimal_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-au-int').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_1_int_value[index_q] if pe_assessment_question_auto_1_int_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-au-dec').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_1_decimal_value[index_q] if pe_assessment_question_auto_1_decimal_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-au-color').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_1_extra_color[index_q] if pe_assessment_question_auto_1_extra_color[index_q]

    end

    doc.xpath('//databases//resumen12//value-au-prev-int').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_prev_1_int_value[index_q] if pe_assessment_question_auto_prev_1_int_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-au-prev-dec').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_prev_1_decimal_value[index_q] if pe_assessment_question_auto_prev_1_decimal_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-as-hab-int').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_proc_1_int_value[index_q] if pe_assessment_question_asc_proc_1_int_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-as-hab-dec').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_proc_1_decimal_value[index_q] if pe_assessment_question_asc_proc_1_decimal_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-au-hab-int').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_proc_1_int_value[index_q] if pe_assessment_question_auto_proc_1_int_value[index_q]

    end

    doc.xpath('//databases//resumen12//value-au-hab-dec').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_proc_1_decimal_value[index_q] if pe_assessment_question_auto_proc_1_decimal_value[index_q]

    end

    #asc au

    doc.xpath('//databases//instrumento12//dimension').each_with_index do |question_2, index_q|

      question_2.content = pe_questions_auto_2[index_q].pe_question.description.gsub('<strong>','').gsub('</strong>','') if pe_questions_auto_2[index_q]

    end

    doc.xpath('//databases//instrumento12//afirmacion').each_with_index do |question_2, index_q|

      question_2.content = pe_questions_auto_2[index_q].description if pe_questions_auto_2[index_q]

    end

    doc.xpath('//databases//instrumento12//value-as').each_with_index do |question_2, index_q|

      question_2.content = pe_assessment_question_asc_2_value[index_q]

    end

    doc.xpath('//databases//instrumento12//value-as-color').each_with_index do |question_2, index_q|

      question_2.content = pe_assessment_question_asc_2_color[index_q]

    end

    doc.xpath('//databases//instrumento12//promedio-as').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_1_value[index_q]

    end

    doc.xpath('//databases//instrumento12//promedio-as-color').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_asc_1_color[index_q]

    end

    doc.xpath('//databases//instrumento12//value-au').each_with_index do |question_2, index_q|

      question_2.content = pe_assessment_question_auto_2_value[index_q]

    end

    doc.xpath('//databases//instrumento12//value-au-color').each_with_index do |question_2, index_q|

      question_2.content = pe_assessment_question_auto_2_color[index_q]

    end

    doc.xpath('//databases//instrumento12//promedio-au').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_1_value[index_q]

    end

    doc.xpath('//databases//instrumento12//promedio-au-color').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_auto_1_color[index_q]

    end

    # par

    doc.xpath('//databases//instrumento3//dimension').each_with_index do |question_2, index_q|

      question_2.content = pe_questions_par_2[index_q].pe_question.description.gsub('<strong>','').gsub('</strong>','') if pe_questions_par_2[index_q]

    end

    doc.xpath('//databases//instrumento3//afirmacion').each_with_index do |question_2, index_q|

      question_2.content = pe_questions_par_2[index_q].description if pe_questions_par_2[index_q]

    end

    doc.xpath('//databases//instrumento3//value-pa').each_with_index do |question_2, index_q|

      question_2.content = pe_assessment_question_par_2_value[index_q]

    end

    doc.xpath('//databases//instrumento3//value-pa-color').each_with_index do |question_2, index_q|

      question_2.content = pe_assessment_question_par_2_color[index_q]

    end

    doc.xpath('//databases//instrumento3//promedio-pares').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_par_1_value[index_q]

    end

    doc.xpath('//databases//instrumento3//promedio-pares-color').each_with_index do |question_1, index_q|

      question_1.content = pe_assessment_question_par_1_color[index_q]

    end

    doc.xpath('//databases//instrumento3//observaciones').each_with_index do |question_1, index_q|

      question_1.content = 0

    end

    File.write('/var/www/remote-storage/lms-storage/reps_jasper_habitat/jasperstarter2/bin/'+d+'.xml', doc.to_xml)

    return d

    #File.delete('/Users/alejo/Downloads/CT/jasperstarter2/bin/'+r+'.pdf') if File.exist?('/Users/alejo/Downloads/CT/jasperstarter2/bin/'+r+'.pdf')

  end

  private

  def get_data_2

    @pe_member = PeMember.find(params[:pe_member_id])
    @pe_process = @pe_member.pe_process

  end

end
