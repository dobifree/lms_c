class PeAlternativesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def new

    @pe_question = PeQuestion.find(params[:pe_question_id])

    @pe_alternative = @pe_question.pe_alternatives.build

    @pe_evaluation = @pe_question.pe_evaluation
    @pe_element = @pe_question.pe_element
    @pe_process = @pe_evaluation.pe_process

  end

  def create
    @pe_alternative = PeAlternative.new(params[:pe_alternative])


    if @pe_alternative.save
      redirect_to @pe_alternative.pe_question.pe_evaluation
    else

      @pe_question = @pe_alternative.pe_question
      @pe_evaluation = @pe_question.pe_evaluation
      @pe_element = @pe_question.pe_element
      @pe_process = @pe_evaluation.pe_process

      render action: 'new'
    end

  end

  def edit
    @pe_alternative = PeAlternative.find(params[:id])

    @pe_question = @pe_alternative.pe_question
    @pe_evaluation = @pe_question.pe_evaluation
    @pe_element = @pe_question.pe_element
    @pe_process = @pe_evaluation.pe_process

  end


  def update

    @pe_alternative = PeAlternative.find(params[:id])

    if @pe_alternative.update_attributes(params[:pe_alternative])

      if @pe_alternative.pe_question.pe_member_evaluated
        redirect_to pe_questions_show_pe_member_path @pe_alternative.pe_question.pe_evaluation, @pe_alternative.pe_question.pe_member_evaluated
      else
        redirect_to @pe_alternative.pe_question.pe_evaluation
      end

    else
      @pe_question = @pe_alternative.pe_question
      @pe_evaluation = @pe_question.pe_evaluation
      @pe_element = @pe_question.pe_element
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end

  # DELETE /pe_alternatives/1
  # DELETE /pe_alternatives/1.json
  def destroy
    @pe_alternative = PeAlternative.find(params[:id])
    @pe_alternative.destroy

    respond_to do |format|
      format.html { redirect_to pe_alternatives_url }
      format.json { head :no_content }
    end
  end
end
