class BpEventsController < ApplicationController

  def new
    @bp_event = BpEvent.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bp_event }
    end
  end

  def create
    bp_event = BpEvent.new(params[:bp_event])

    if bp_event.save
      flash[:success] = t('views.bp_events.flash_messages.success_created')
      redirect_to bp_event_files_path(bp_event)
    else
      @bp_event = bp_event
      render action: "new"
    end
  end

  def edit
    @bp_event = BpEvent.find(params[:id])
  end

  def update
    bp_event = BpEvent.find(params[:id])
    if bp_event.update_attributes(params[:bp_event])
      flash[:success] = t('views.bp_events.flash_messages.success_changed')
      redirect_to bp_event_files_path(bp_event)
    else
      @bp_event = bp_event
      render action: "edit"
    end
  end

  def bp_event_files
    @bp_event = BpEvent.find(params[:bp_event_id])
    @bp_event_files = @bp_event.bp_event_files
  end

  def bp_event_file_new
    @bp_event_file = BpEventFile.new
    @bp_event_file.bp_event_id = params[:bp_event_id]
  end

  def bp_event_file_create
    bp_event_file = BpEventFile.new(params[:bp_event_file])
    bp_event_file.bp_event_id = params[:bp_event_id]
    bp_event_file.registered_by_admin_id = admin_connected.id
    bp_event_file.registered_at = lms_time

    if bp_event_file.save
      flash[:success] = t('views.bp_events_files.flash_messages.success_created')
      redirect_to bp_event_files_path(bp_event_file.bp_event)
    else
      @bp_event_file = bp_event_file
      render action: "bp_event_file_new"
    end
  end

  def bp_event_file_edit
    @bp_event_file = BpEventFile.find(params[:bp_event_file_id])
  end

  def bp_event_file_update
    bp_event_file = BpEventFile.find(params[:bp_event_file_id])
    if bp_event_file.update_attributes(params[:bp_event_file])
      flash[:success] = t('views.bp_events_files.flash_messages.success_changed')
      redirect_to bp_event_files_path(bp_event_file.bp_event)
    else
      @bp_event_file = bp_event_file
      render action: "bp_event_file_edit"
    end
  end

  def bp_event_file_deactivate
    bp_event_file = BpEventFile.find(params[:bp_event_file_id])
    bp_event_file.active = false
    bp_event_file.deactivated_by_admin_id = admin_connected.id
    bp_event_file.deactivated_at = lms_time
    if bp_event_file.save
      flash[:success] = t('views.bp_events_files.flash_messages.success_deactivate')
    else
      flash[:danger] = t('views.bp_events_files.flash_messages.danger_deactivate')
    end
    redirect_to bp_event_files_path(bp_event_file.bp_event)
  end

end
