class JJobLevelsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def index
    @j_job_levels = JJobLevel.all
  end

  def show
    @j_job_level = JJobLevel.find(params[:id])
  end

  def new
    @j_job_level = JJobLevel.new
  end

  def create

    @j_job_level = JJobLevel.new(params[:j_job_level])

    if @j_job_level.save
      flash[:success] = t('activerecord.success.model.j_job_level.create_ok')
      redirect_to j_job_levels_path
    else
      render action: 'new'
    end

  end

  def edit
    @j_job_level = JJobLevel.find(params[:id])
  end

  def update
    @j_job_level = JJobLevel.find(params[:id])

    if @j_job_level.update_attributes(params[:j_job_level])
      flash[:success] = t('activerecord.success.model.j_job_level.update_ok')
      redirect_to j_job_levels_path
    else
      render action: 'edit'
    end

  end

  def destroy
    @j_job_level = JJobLevel.find(params[:id])

    if @j_job_level.j_jobs.count > 0
      flash[:danger] = t('activerecord.error.model.j_job_level.delete_error')
    else
      @j_job_level.destroy
      flash[:success] = t('activerecord.success.model.j_job_level.delete_ok')
    end



    redirect_to j_job_levels_url

  end

  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'jobs', true).first

      unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end

end
