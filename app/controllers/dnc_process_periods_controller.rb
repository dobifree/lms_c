class DncProcessPeriodsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /dnc_process_periods
  # GET /dnc_process_periods.json
  def index
    @dnc_process_periods = DncProcessPeriod.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dnc_process_periods }
    end
  end

  # GET /dnc_process_periods/1
  # GET /dnc_process_periods/1.json
  def show
    @dnc_process_period = DncProcessPeriod.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dnc_process_period }
    end
  end

  # GET /dnc_process_periods/new
  # GET /dnc_process_periods/new.json
  def new
    @dnc_process_period = DncProcessPeriod.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dnc_process_period }
    end
  end

  # GET /dnc_process_periods/1/edit
  def edit
    @dnc_process_period = DncProcessPeriod.find(params[:id])
  end

  # POST /dnc_process_periods
  # POST /dnc_process_periods.json
  def create
    @dnc_process_period = DncProcessPeriod.new(params[:dnc_process_period])

    respond_to do |format|
      if @dnc_process_period.save
        format.html { redirect_to @dnc_process_period, notice: 'Dnc process period was successfully created.' }
        format.json { render json: @dnc_process_period, status: :created, location: @dnc_process_period }
      else
        format.html { render action: "new" }
        format.json { render json: @dnc_process_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dnc_process_periods/1
  # PUT /dnc_process_periods/1.json
  def update
    @dnc_process_period = DncProcessPeriod.find(params[:id])

    respond_to do |format|
      if @dnc_process_period.update_attributes(params[:dnc_process_period])
        format.html { redirect_to @dnc_process_period, notice: 'Dnc process period was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dnc_process_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dnc_process_periods/1
  # DELETE /dnc_process_periods/1.json
  def destroy
    @dnc_process_period = DncProcessPeriod.find(params[:id])
    @dnc_process_period.destroy

    respond_to do |format|
      format.html { redirect_to dnc_process_periods_url }
      format.json { head :no_content }
    end
  end
end
