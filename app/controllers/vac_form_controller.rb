class VacFormController < ApplicationController
  include VacFormHelper
  include VacationsModule
  include MedlicModule

  before_filter :authenticate_user
  before_filter :verify_access_vacs_module


  def set_vacs_attr_form

  end

  def set_vacs_attr(set_days, request, set_end_vacations)
    rules = BpGeneralRulesVacation.user_active(request.begin, request.user_id)
    if rules
      if set_days
        request.days = vacations_days(rules.only_laborable_day, request.begin, request.end, request.user_id) if request.begin && request.end
        request.end_vacations
      else
        #set end
      end
    else
      request
    end
  end

  def benefits_for(request, benefit_ids, only_ids, auto)
    benefits = []
    rules_vacations = BpRuleVacation.active_rules_vacations(request.begin.to_datetime, request.end.to_datetime, request.user_id)

    actual =  VacAccumulated.user_latest(request.user_id)
    actual_days = actual ? actual.accumulated_real_days : 0

    legal_last_accu = VacAccumulated.user_latest_legal(request.user_id)
    legal_last_accu = VacAccumulated.user_first(request.user_id) unless legal_last_accu

    update_days = future_days(legal_last_accu ? legal_last_accu.up_to_date : lms_time, request.begin, request.user_id)

    next_vac = (actual_days.to_f - request.to_i + update_days.to_f - VacRequest.sum_days_by_status(request.user_id, VacRequest.status_in_process).to_i)

    if rules_vacations && auto
      rules_vacations.each do |rule_vacation|
        benefit = rule_vacation.evaluate_benefits(lms_time,
                                                  request.begin,
                                                  request.end,
                                                  request.days, request.days_progressive,
                                                  anniversary(request.user_id),
                                                  request.user_id,
                                                  next_vac, request)
        next unless benefit
        benefits << benefit
      end
    end
    benefits
  end

  def set_days
    days = ''
    date_begin = params[:begin] ? params[:begin] : nil
    date_end = params[:end] ? params[:end] : nil
    user_id = params[:user_id] ? params[:user_id] : nil
    days = vacations_days(true, date_begin, date_end, user_id) if date_begin && date_end
    render json: days.as_json
  end

  def sum_or_rest_back_to_work
    benefit = BpConditionVacation.find(params[:benefit_id])
    days = benefit.bonus_days ? benefit.bonus_days : 0
    date_end = ''
    user_id = params[:user_id] ? params[:user_id] : nil
    date_begin = params[:begin] ? next_laborable_day?(params[:begin].to_date - 1.day, user_id) : nil
    date_end = vacations_end(true, date_begin, days, user_id) if date_begin && days
    date_end = next_laborable_day?(date_end, user_id)
    render json: date_end.strftime('%d/%m/%Y').to_json
  end

  def verify_restrictive_warnings
  end

  def set_end
    date_end = ''
    date_begin = params[:begin] ? params[:begin] : nil
    days = params[:days] ? params[:days] : nil
    user_id = params[:user_id] ? params[:user_id] : nil
    date_end = vacations_end(true, date_begin, days, user_id) if date_begin && days
    render json: date_end.strftime('%d/%m/%Y').to_json
  end

  def progressive_days_available?
    user = User.find(params[:user_id])
    prev_auto_accumulated = VacAccumulated.user_latest_prog(user.id)
    prev_auto_accumulated = VacAccumulated.user_first(user.id) unless prev_auto_accumulated
    accumulated = prev_auto_accumulated

    request = params[:request_id].size > 0 ? VacRequest.find(params[:request_id]) : nil
    value = next_progressive_days(accumulated.up_to_date, params[:begin_vacations], user.id) - VacRequest.sum_progressive_days_by_status(user.id, VacRequest.status_in_process).to_i
    value += request.days_progressive if request && request.days_progressive && (request.pending? || request.approved?)
    render json: accumulated ? value.as_json : 0.as_json
  end

  def set_days_only_vac
    total_days = params[:total_days].to_i
    progressive_days = params[:progressive_days].to_i
    render json: (total_days - progressive_days).as_json
  end

  def next_laborable_day
    date = next_laborable_day?(params[:last_vac_day], params[:user_id]).to_date
    render json: date.strftime('%d/%m/%Y').to_json
  end

  def next_vac_accumulated
    request = VacRequest.find(params[:request_id]) unless params[:request_id].blank?
    user_id = params[:user_id]
    rules = BpGeneralRulesVacation.user_active(params[:begin_vacations], user_id)

    if rules
      actual =  VacAccumulated.user_latest(user_id)
      actual_days = actual ? actual.accumulated_real_days : 0

      legal_last_accu = VacAccumulated.user_latest_legal(user_id)
      legal_last_accu = VacAccumulated.user_first(user_id) unless legal_last_accu

      update_days = future_days(legal_last_accu ? legal_last_accu.up_to_date : lms_time, params[:begin_vacations], user_id)

      next_vac = (actual_days.to_f - params[:days].to_i + update_days.to_f - VacRequest.sum_days_by_status(user_id, VacRequest.status_in_process).to_i)
      next_vac += request.days if request && request.days && (request.pending? || request.approved?)
      next_vac = number_with_precision( next_vac, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s
      render json: next_vac.to_json
    else
      render json: false
    end
  end

  def set_benefits
    user_id = user_connected.id
    vac_record_begin = params[:vacs_begin]
    vac_record_end = params[:vacs_end]
    vac_record_days = params[:days]
    vac_record_progressive_days = params[:progressive_days].to_i

    request = params[:vac_request_id].to_i > 0 ? VacRequest.find(params[:vac_request_id].to_i) : nil
    benefits = []
    taken_benefits = []
    taken_benefits_to_erase = []
    taken_days = 0

    rules_vacations = BpRuleVacation.active_rules_vacations(vac_record_begin.to_datetime, vac_record_end.to_datetime, user_id)

    if request
      taken_benefits_to_erase_1 = request.vac_rule_records.where(active: true, refund_pending: false).all
      taken_benefits_to_erase_1.each do | taken |
        taken_benefits_to_erase << taken.bp_condition_vacation
      end
    end

    if rules_vacations
      benefits = []
      taken_benefits = []

      # rules_vacations.each do |rule_vacation|
      #   benefit = rule_vacation.evaluate_benefits(lms_time, vac_record_begin, vac_record_end, vac_record_days, anniversary(user_id) , user_id)
      #   next unless benefit
      #   next unless !request || (request && !request.vac_rule_records.where(active: true, refund_pending: false, bp_condition_vacation_id:benefit.id).first)
      #   benefits << benefit
      # end

      actual =  VacAccumulated.user_latest(user_id)
      actual_days = actual ? actual.accumulated_real_days : 0
      legal_last_accu = VacAccumulated.user_latest_legal(user_id)
      legal_last_accu = VacAccumulated.user_first(user_id) unless legal_last_accu

      update_days = future_days(legal_last_accu ? legal_last_accu.up_to_date : lms_time, vac_record_begin, user_id)

      next_vac = (actual_days.to_f - vac_record_days.to_i + update_days.to_f - VacRequest.sum_days_by_status(user_id, VacRequest.status_in_process).to_i)
      next_vac += request.days if request && request.days && (request.pending? || request.approved?)

      prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
      prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated

      next_pro_days = next_progressive_days(prev_auto_accumulated.up_to_date, vac_record_begin, user_id) - VacRequest.sum_progressive_days_by_status(user_id, VacRequest.status_in_process).to_i
      next_pro_days += request.days_progressive if request && request.days_progressive && (request.pending? || request.approved?)
      next_pro_days -= vac_record_progressive_days

      rules_vacations.each do |rule_vacation|
        benefit = rule_vacation.evaluate_benefits(lms_time, vac_record_begin, vac_record_end, vac_record_days, vac_record_progressive_days, anniversary(user_id) , user_id, next_vac, next_pro_days, request)
        next unless benefit
        if request && request.vac_rule_records.where(active: true, refund_pending: false, bp_condition_vacation_id:benefit.id).first
          taken_benefits << benefit
          taken_days += benefit.bonus_days if benefit.bonus_days
          taken_benefits_to_erase.delete_if { |ben| ben.id == benefit.id }
        else
          benefits << benefit
        end
      end
    end

    render partial: 'benefits_list',
           locals: { benefits: benefits,
                     taken_days: taken_days,
                     editable: false,
                     hide_checkbox: true,
                     taken_benefits: taken_benefits,
                     taken_benefits_to_erase: taken_benefits_to_erase,
                     request_id: params[:vac_request_id]}
  end

  def set_benefits_as_manager
    vac_record_begin = params[:vacs_begin]
    vac_record_end = params[:vacs_end]
    vac_record_days = params[:days]
    vac_record_progressive_days = params[:progressive_days].to_i

    request = params[:vac_request_id].to_i > 0 ? VacRequest.find(params[:vac_request_id].to_i) : nil
    user_id = request ? request.user_id : params[:user_id]
    benefits = []
    taken_benefits = []
    taken_benefits_to_erase = []
    taken_days = 0

    rules_vacations = BpRuleVacation.active_rules_vacations(vac_record_begin.to_datetime, vac_record_end.to_datetime , user_id)

    if request
      taken_benefits_to_erase_1 = request.vac_rule_records.where(active: true).all
      taken_benefits_to_erase_1.each do | taken |
        taken_benefits_to_erase << taken.bp_condition_vacation
      end
    end

    if rules_vacations
      benefits = []
      taken_benefits = []

      actual =  VacAccumulated.user_latest(user_id)
      actual_days = actual ? actual.accumulated_real_days : 0

      legal_last_accu = VacAccumulated.user_latest_legal(user_id)
      legal_last_accu = VacAccumulated.user_first(user_id) unless legal_last_accu
      prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
      prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated

      update_days = future_days(legal_last_accu ? legal_last_accu.up_to_date : lms_time, vac_record_begin, user_id)

      next_vac = (actual_days.to_f - vac_record_days.to_i + update_days.to_f - VacRequest.sum_days_by_status(user_id, VacRequest.status_in_process).to_i)
      next_vac += request.days if request && request.days && (request.pending? || request.approved?)

      next_pro_days = next_progressive_days(prev_auto_accumulated.up_to_date, vac_record_begin, user_id) - VacRequest.sum_progressive_days_by_status(user_id, VacRequest.status_in_process).to_i
      next_pro_days += request.days_progressive if request && request.days_progressive && (request.pending? || request.approved?)
      next_pro_days -= vac_record_progressive_days

      rules_vacations.each do |rule_vacation|
        benefit = rule_vacation.evaluate_benefits(lms_time,
                                                  vac_record_begin,
                                                  vac_record_end,
                                                  vac_record_days, vac_record_progressive_days,
                                                  anniversary(user_id),
                                                  user_id,
                                                  next_vac,
                                                  next_pro_days, request)
        next unless benefit

        if request && request.vac_rule_records.where(active: true, bp_condition_vacation_id:benefit.id).first
          taken_benefits << benefit
          taken_days += benefit.bonus_days if benefit.bonus_days
          taken_benefits_to_erase.delete_if { |ben| ben.id == benefit.id }
        else
          benefits << benefit
        end
      end
    end

    render partial: 'benefits_list',
           locals: { benefits: benefits,
                     editable: true,
                     taken_days: taken_days,
                     taken_benefits: taken_benefits,
                     taken_benefits_to_erase: taken_benefits_to_erase,
                     request_id: params[:vac_request_id],
                     manager: true }
  end

  def set_next_days_record_form
    user_id = params[:user_id]
    days = params[:days].to_d
    actual =  VacAccumulated.user_latest(user_id)
    actual_days = actual ? actual.accumulated_days : 0
    update_days = actual_days - days
    render json: update_days.to_json
  end

  def vacs_detail
    if params[:vac_request_id] != nil
      request = VacRequest.find(params[:vac_request_id])
      back = back_to_work_request(request.id)
      dates = []
      dates << request.begin
      pivot = dates[0]
      loop do
        pivot += 1.day
        break if pivot > back
        dates << pivot
      end

      benefits = []
      params_benefits = []

      request.vac_rule_records.where(active: true).each do |vac_rule|
        params_benefits << vac_rule.bp_condition_vacation
      end

      params_benefits.each do |benefit|
        days = benefit.bonus_days
        next unless days
        days.times do
          benefits << benefit.bp_rule_vacation.name
        end
      end

      render partial: 'vacs_detail',
             locals: {dates: dates,
                      begin_vacs: request.begin.to_date,
                      end_vacs: request.end.to_date,
                      backs_to_work_date: back,
                      user_id: request.user_id,
                      benefits: benefits}

    else
      dates = []
      dates << params[:vacs_begin].to_date
      pivot = dates[0]
      loop do
        pivot += 1.day
        break if pivot > params[:back_to_work_day].to_date
        dates << pivot
      end

      benefits = []

      params_benefits = defined?(params[:benefits]) && !params[:benefits].nil? ? params[:benefits] : []


      params_benefits.each do |benefit_id|
        condition_vacation = BpConditionVacation.find(benefit_id)
        days = condition_vacation.bonus_days
        next unless days
        days.times do
          benefits << condition_vacation.bp_rule_vacation.name
        end
      end

      render partial: 'vacs_detail',
             locals: {dates: dates,
                      begin_vacs: params[:vacs_begin].to_date,
                      end_vacs: params[:vacs_end].to_date,
                      backs_to_work_date: params[:back_to_work_day].to_date,
                      user_id: params[:user_id],
                      benefits: benefits}
    end
  end

  def vacs_detail_as_modal
    if params[:vac_request_id] != nil
      request = VacRequest.find(params[:vac_request_id])
      back = back_to_work_request(request.id)
      dates = []
      dates << request.begin
      pivot = dates[0]
      loop do
        pivot += 1.day
        break if pivot > back
        dates << pivot
      end

      benefits = []
      params_benefits = []

      request.vac_rule_records.where(active: true).each do |vac_rule|
        params_benefits << vac_rule.bp_condition_vacation
      end

      params_benefits.each do |benefit|
        days = benefit.bonus_days
        next unless days
        days.times do
          benefits << benefit.bp_rule_vacation.name
        end
      end

      render partial: 'vacs_detail_as_model',
             locals: {dates: dates,
                      begin_vacs: request.begin.to_date,
                      end_vacs: request.end.to_date,
                      backs_to_work_date: back,
                      user_id: request.user_id,
                      benefits: benefits}

    else
      dates = []
      dates << params[:vacs_begin].to_date
      pivot = dates[0]
      loop do
        pivot += 1.day
        break if pivot > params[:back_to_work_day].to_date
        dates << pivot
      end

      benefits = []

      params_benefits = defined?(params[:benefits]) && !params[:benefits].nil? ? params[:benefits] : []


      params_benefits.each do |benefit_id|
        condition_vacation = BpConditionVacation.find(benefit_id)
        days = condition_vacation.bonus_days
        next unless days
        days.times do
          benefits << condition_vacation.bp_rule_vacation.name
        end
      end

      render partial: 'vacs_detail_as_model',
             locals: {dates: dates,
                      begin_vacs: params[:vacs_begin].to_date,
                      end_vacs: params[:vacs_end].to_date,
                      backs_to_work_date: params[:back_to_work_day].to_date,
                      user_id: params[:user_id],
                      benefits: benefits}
    end
  end

  def set_next_days_progressive_record_form
    user_id = params[:user_id]
    days = params[:days_progressive].to_i
    actual =  VacAccumulated.user_latest(user_id)
    actual_days = actual ? actual.accumulated_progressive_days : 0
    update_days = actual_days - days
    render json: update_days.to_json
  end

  def valid_register_request_form
    request = complete_create(params[:vac_request])
    render json: (request.valid? && !restrictive_warnings).to_json
  end

  def model_errors_request
    if params[:vac_request_id].blank?
      request = VacRequest.new(params[:vac_request])
      request.user_id = user_connected.id
    else
      request = VacRequest.find(params[:vac_request_id])
      request.assign_attributes(params[:vac_request])
    end
    request.registered_at = lms_time
    request.registered_by_user_id = user_connected.id
    request.status = VacRequest.approved
    request.valid?
    message = verify_conflict_benefits(request)
    request.errors.add(:base, 'Conflicto con beneficio: ' + message) if message.size > 0
    render partial: 'shared/error_messages',
           locals: {object: request}
  end

  def set_warnings
    user_id = params[:user_id]
    now_time = params[:vacs_begin]
    vacs_days = params[:vacs_days]
    request = params[:vac_request_id].to_i > 0 ? VacRequest.find(params[:vac_request_id].to_i) : nil
    vac_record_progressive_days = params[:progressive_days].to_i
    warnings = []

    rules = BpGeneralRulesVacation.user_active(now_time.to_datetime, user_id)

    if rules
      vacs_days = next_vac_accumulated_1(user_id, now_time, vacs_days)
      vacs_days += request.days if request && request.days && (request.pending? || request.approved?)
      actual = VacAccumulated.user_latest(user_id)
      prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
      prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated
      next_pro_days = next_progressive_days(prev_auto_accumulated.up_to_date, now_time, user_id) - VacRequest.sum_progressive_days_by_status(user_id, VacRequest.status_in_process).to_i
      next_pro_days += request.days_progressive if request && request.days_progressive && (request.pending? || request.approved?)
      next_pro_days -= vac_record_progressive_days

      warnings = rules ? rules.doesnt_satisfy_warnings(vacs_days, next_pro_days) : []
    else
      warnings = []
    end

    if params[:back_to_work_day]
      warnings += be_license(now_time.to_date,  params[:back_to_work_day].to_date, user_id)
    end

    render partial: 'warnings_list',
           locals: { warnings: warnings }
  end

  def restrictive_warnings
    user_id = params[:user_id]
    now_time = params[:vacs_begin]
    vacs_days = params[:vacs_days]
    request = params[:vac_request_id].to_i > 0 ? VacRequest.find(params[:vac_request_id].to_i) : nil
    vac_record_progressive_days = params[:progressive_days].to_i

    answer = false

    rules = BpGeneralRulesVacation.user_active(now_time.to_datetime, user_id)

    if rules
      if BpWarningException.first_usable_for_at(user_id, lms_time) || (request && request.exception_available?)
        render json: answer
      else
        vacs_days = next_vac_accumulated_1(user_id, now_time, vacs_days)
        vacs_days += request.days if request && request.days && (request.pending? || request.approved?)

        actual = VacAccumulated.user_latest(user_id)
        prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
        prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated
        next_pro_days = next_progressive_days(prev_auto_accumulated.up_to_date, now_time, user_id) - VacRequest.sum_progressive_days_by_status(user_id, VacRequest.status_in_process).to_i
        next_pro_days += request.days_progressive if request && request.days_progressive && (request.pending? || request.approved?)
        next_pro_days -= vac_record_progressive_days

        warnings = rules ? rules.doesnt_satisfy_warnings(vacs_days, next_pro_days) : []
        warnings.each {|warning| answer = warning.restrictive if warning.restrictive}
        render json: answer
      end
    else
      answer = false
      render json: answer
    end
  end

  def request_show_modal
    request = VacRequest.find(params[:vac_request_id])
    render partial: 'vac_records_register/request_info_modal',
           locals: { request: request }
  end

  def future_progressive_days
    render json: (next_progressive_days(params[:vacs_begin], params[:vacs_end], params[:user_id]) - VacRequest.sum_progressive_days_by_status(user_id, VacRequest.status_in_process).to_i)
  end

  private

  def verify_access_vacs_module
    rules = BpGeneralRulesVacation.user_active(lms_time, user_connected.id)
    return if rules
    ct_module = CtModule.where('cod = ? AND active = ?', 'vacs', true).first
    return if ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
    flash[:danger] = 'No tiene acceso a módulo'
    redirect_to root_path
  end
end