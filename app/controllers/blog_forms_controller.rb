class BlogFormsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /blog_forms
  # GET /blog_forms.json
  def index
    @blog_forms = BlogForm.all

  end

  # GET /blog_forms/1
  # GET /blog_forms/1.json
  def show
    @blog_form = BlogForm.find(params[:id])
  end

  # GET /blog_forms/new
  # GET /blog_forms/new.json
  def new
    @blog_form = BlogForm.new
    @blog_form.blog_form_items.build
    @blog_form.blog_form_items.build
    @blog_form.blog_form_items.build
  end

  # GET /blog_forms/1/edit
  def edit
    @blog_form = BlogForm.find(params[:id])
  end

  # POST /blog_forms
  # POST /blog_forms.json
  def create
    @blog_form = BlogForm.new(params[:blog_form])

    if @blog_form.save
      flash[:success] = 'El formulario fue creado correctamente'
      redirect_to @blog_form
    else
      flash.now[:danger] = 'El formulario no fue creado correctamente'
      render 'new'
    end
  end

  # PUT /blog_forms/1
  # PUT /blog_forms/1.json
  def update
    @blog_form = BlogForm.find(params[:id])

    if @blog_form.update_attributes(params[:blog_form])
      flash[:success] = 'El formulario fue actualizado correctamente'
      redirect_to @blog_form
    else
      flash[:danger] = 'El formulario no fue actualizado correctamente'
      render 'edit'
    end

  end

  # DELETE /blog_forms/1
  # DELETE /blog_forms/1.json
  def destroy
    @blog_form = BlogForm.find(params[:id])
    @blog_form.destroy

    redirect_to blog_forms_path
  end

  def show_config_permissions
    @blog_form = BlogForm.find(params[:id])
    @permissions = []
    BlogFormPermission.perm_type_options.each_with_index do |option, index|
      perm = @blog_form.blog_form_permissions.where(active: true, perm_type: index).first
      if perm
        @permissions << perm.dup
      else
        @permissions << @blog_form.blog_form_permissions.build(perm_type: index)
      end
    end
    puts @permissions.inspect
  end


  def config_permissions
    @blog_form = BlogForm.find(params[:id])

    @blog_form.transaction do
      begin
        @blog_form.blog_form_permissions.where(active: true).update_all(active: false)
        @blog_form.update_attributes(params[:blog_form])
      rescue Exception => exception
        #puts exception
        #puts exception.backtrace
        raise
        flash[:danger] = 'Upss... hubo un problema, intenta nuevamente y si vuelve a fallar llama a César!!'
        redirect_to blog_forms_show_config_permissions(@blog_form)
      end
      flash[:success] = 'Los permisos fueron configurados correctamente.'
      redirect_to @blog_form
    end
  end

end
