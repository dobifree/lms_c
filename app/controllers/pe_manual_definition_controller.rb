class PeManualDefinitionController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list]
  before_filter :get_data_2, only: [:show, :new, :confirm]
  before_filter :get_data_3, only: [:create]
  before_filter :get_data_4, only: [:edit, :update, :destroy]
  before_filter :validate_open_process, only: [:people_list, :show, :new, :create, :edit, :update, :destroy, :confirm]
  before_filter :validate_access_to_evaluate, only:[:show, :new, :create, :edit, :update, :destroy, :confirm]

  def people_list

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])

  end

  def show

   #@company = session[:company]

    @hr_evaluation_type_element_nivel_1 = @hr_process_evaluation.evaluation_type_element_level_1

    @hr_evaluation_type_element_nivel_2 = @hr_process_evaluation.evaluation_type_element_level_2

  end

  def new

    @goals = Array.new
    @percentages = Array.new

   #@company = session[:company]

    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])

    @hr_evaluation_type_element_nivel_1 = @hr_process_evaluation.evaluation_type_element_level_1

    @hr_evaluation_type_element_nivel_2 = @hr_process_evaluation.evaluation_type_element_level_2

    @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.new
    @hr_process_evaluation_manual_q.hr_evaluation_type_element_id = @hr_evaluation_type_element.id
    @hr_process_evaluation_manual_q.hr_process_evaluation_id = @hr_process_evaluation.id
    @hr_process_evaluation_manual_q.hr_process_user_id = @hr_process_user.id

    if params[:hr_process_evaluation_manual_q_id]

      @hr_process_evaluation_manual_q_padre = HrProcessEvaluationManualQ.find(params[:hr_process_evaluation_manual_q_id])
      @hr_process_evaluation_manual_q.hr_process_evaluation_manual_q_id = @hr_process_evaluation_manual_q_padre.id

    end

  end

  def create

    @goals = Array.new
    @percentages = Array.new

   #@company = session[:company]

    @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.new(params[:hr_process_evaluation_manual_q])

    @hr_process_evaluation_manual_q.meta_indicador =nil if(@hr_process_evaluation_manual_q.tipo_indicador == 4 || @hr_process_evaluation_manual_q.tipo_indicador == 5)

    @error_rangos = false

    if (@hr_process_evaluation_manual_q.tipo_indicador == 4 || @hr_process_evaluation_manual_q.tipo_indicador == 5) && @hr_process_evaluation_manual_q.hr_evaluation_type_element.aplica_evaluacion

      params[:pe_question_rank].each do |pe_question_rank|

        if !pe_question_rank[:percentage].blank?

          r = PeQuestionRank.new(pe_question_rank)
          r.hr_process_evaluation_manual_q = @hr_process_evaluation_manual_q


          if @hr_process_evaluation_manual_q.tipo_indicador == 5

            r.goal = 0
            r.discrete_goal = pe_question_rank[:goal]

          end

          @error_rangos = true unless r.valid?

          @goals.push r.goal
          @percentages.push r.percentage

        end
      end

    end

    if @error_rangos

      @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_evaluation_type_element

      @hr_evaluation_type_element_nivel_1 = @hr_process_evaluation.evaluation_type_element_level_1

      @hr_evaluation_type_element_nivel_2 = @hr_process_evaluation.evaluation_type_element_level_2

      @hr_process_evaluation_manual_q.valid?

      @hr_process_evaluation_manual_q.errors.add(:confirmado, 'El logro debe ser >= '+@hr_process_evaluation_manual_q.hr_evaluation_type_element.valor_minimo.to_s+' y <= '+@hr_process_evaluation_manual_q.hr_evaluation_type_element.valor_maximo.to_s)

      render action: 'new'

    else

      if @hr_process_evaluation_manual_q.save

        @hr_process_evaluation_manual_q.pe_question_ranks.destroy_all

        if (@hr_process_evaluation_manual_q.tipo_indicador == 4 || @hr_process_evaluation_manual_q.tipo_indicador == 5)

          params[:pe_question_rank].each do |pe_question_rank|
            r = PeQuestionRank.new(pe_question_rank)
            r.hr_process_evaluation_manual_q = @hr_process_evaluation_manual_q

            if @hr_process_evaluation_manual_q.tipo_indicador == 5

              r.goal = 0
              r.discrete_goal = pe_question_rank[:goal]

            end

            r.save
          end

          @hr_process_evaluation_manual_q.set_goal_rank
          @hr_process_evaluation_manual_q.save

        end

        flash[:success] =  t('activerecord.success.model.hr_process_evaluation_manual_q.create_ok')

        redirect_to pe_manual_definition_show_path @hr_process_evaluation_manual_q.hr_process_evaluation_id, @hr_process_evaluation_manual_q.hr_process_user_id

      else


        @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_evaluation_type_element

        @hr_evaluation_type_element_nivel_1 = @hr_process_evaluation.evaluation_type_element_level_1

        @hr_evaluation_type_element_nivel_2 = @hr_process_evaluation.evaluation_type_element_level_2


        render action: 'new'

      end

    end

  end

  def edit

    @goals = Array.new
    @percentages = Array.new

   #@company = session[:company]

    @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_evaluation_type_element

    @hr_evaluation_type_element_nivel_1 = @hr_process_evaluation.evaluation_type_element_level_1

    @hr_evaluation_type_element_nivel_2 = @hr_process_evaluation.evaluation_type_element_level_2

  end

  def update

    @goals = Array.new
    @percentages = Array.new

   #@company = session[:company]

    @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_evaluation_type_element

    @hr_evaluation_type_element_nivel_1 = @hr_process_evaluation.evaluation_type_element_level_1

    @hr_evaluation_type_element_nivel_2 = @hr_process_evaluation.evaluation_type_element_level_2

    if (@hr_process_evaluation_manual_q.tipo_indicador == 4 || @hr_process_evaluation_manual_q.tipo_indicador == 5) && @hr_process_evaluation_manual_q.hr_evaluation_type_element.aplica_evaluacion



      params[:pe_question_rank].each do |pe_question_rank|

        if !pe_question_rank[:percentage].blank?

          r = PeQuestionRank.new(pe_question_rank)
          r.hr_process_evaluation_manual_q = @hr_process_evaluation_manual_q

          if @hr_process_evaluation_manual_q.tipo_indicador == 5

            r.goal = 0

          end

          @error_rangos = true unless r.valid?

        end
      end

    end

    if @error_rangos

      @hr_process_evaluation_manual_q.valid?

      @hr_process_evaluation_manual_q.errors.add(:confirmado, 'El logro debe ser >= '+@hr_process_evaluation_manual_q.hr_evaluation_type_element.valor_minimo.to_s+' y <= '+@hr_process_evaluation_manual_q.hr_evaluation_type_element.valor_maximo.to_s)

      render action: 'edit'

    else

      if @hr_process_evaluation_manual_q.update_attributes(params[:hr_process_evaluation_manual_q])

        @hr_process_evaluation_manual_q.pe_question_ranks.destroy_all

        if (@hr_process_evaluation_manual_q.tipo_indicador == 4 || @hr_process_evaluation_manual_q.tipo_indicador == 5)

          params[:pe_question_rank].each do |pe_question_rank|
            r = PeQuestionRank.new(pe_question_rank)
            r.hr_process_evaluation_manual_q = @hr_process_evaluation_manual_q

            if @hr_process_evaluation_manual_q.tipo_indicador == 5

              r.goal = 0

            end

            r.save
          end

          @hr_process_evaluation_manual_q.reload
          @hr_process_evaluation_manual_q.set_goal_rank
          @hr_process_evaluation_manual_q.save

        end

        flash[:success] =  t('activerecord.success.model.hr_process_evaluation_manual_q.update_ok')

        redirect_to pe_manual_definition_show_path @hr_process_evaluation_manual_q.hr_process_evaluation_id, @hr_process_evaluation_manual_q.hr_process_user_id

      else
        render action: 'edit'
      end


    end

  end

  def destroy


    @hr_evaluation_type_element = @hr_process_evaluation_manual_q.hr_evaluation_type_element

    @hr_process_evaluation_manual_q.pe_question_ranks.destroy_all

    @hr_process_evaluation_manual_q.destroy

    flash[:success] = t('activerecord.success.model.hr_process_evaluation_manual_q.delete_ok')
    redirect_to pe_manual_definition_show_path @hr_process_evaluation, @hr_process_user

  end

  def confirm

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    @hr_process_evaluation.hr_process_evaluation_manual_qs.where('hr_process_user_id = ?',@hr_process_user.id).each do |hr_process_evaluation_manual_q|

      hr_process_evaluation_manual_q.confirmado = true
      hr_process_evaluation_manual_q.save

    end

    flash[:success] = t('activerecord.success.model.hr_process_evaluation_manual_q.confirme_ok')
    redirect_to pe_manual_definition_show_path @hr_process_evaluation, @hr_process_user

  end

  private

    def get_data_1
      @hr_process = HrProcess.find(params[:hr_process_id])
    end

    def get_data_2
      @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    end

    def get_data_3
      @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_manual_q][:hr_process_evaluation_id])
      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
      @hr_process_user = HrProcessUser.find(params[:hr_process_evaluation_manual_q][:hr_process_user_id])
    end

    def get_data_4

      @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.find(params[:hr_process_evaluation_manual_q_id])

      @hr_process_evaluation = @hr_process_evaluation_manual_q.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id
      @hr_process_user = @hr_process_evaluation_manual_q.hr_process_user

    end

    def validate_open_process

      unless @hr_process.abierto && @hr_process.fecha_inicio < lms_time && @hr_process.fecha_fin > lms_time
        flash[:danger] = t('activerecord.error.model.hr_process_assessment.out_of_time')
        redirect_to root_path
      end

    end

    def validate_access_to_evaluate

      unless @hr_process_user_connected.hr_process_evalua_rels.where('hr_process_user2_id = ? AND tipo = ?', @hr_process_user.id, 'jefe').count > 0 || @hr_process_user.id == @hr_process_user_connected.id

        flash[:danger] = t('security.no_access_hr_perform_evaluation')
        redirect_to root_path

      end

    end

end
