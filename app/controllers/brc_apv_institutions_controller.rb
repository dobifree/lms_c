class BrcApvInstitutionsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  # GET /brc_apv_institutions
  # GET /brc_apv_institutions.json
  def index
    @brc_apv_institutions = BrcApvInstitution.all
  end

  # GET /brc_apv_institutions/1
  # GET /brc_apv_institutions/1.json
  def show
    @brc_apv_institution = BrcApvInstitution.find(params[:id])
  end

  # GET /brc_apv_institutions/new
  # GET /brc_apv_institutions/new.json
  def new
    @brc_apv_institution = BrcApvInstitution.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @brc_apv_institution }
    end
  end

  # GET /brc_apv_institutions/1/edit
  def edit
    @brc_apv_institution = BrcApvInstitution.find(params[:id])
  end

  # POST /brc_apv_institutions
  # POST /brc_apv_institutions.json
  def create

    @brc_apv_institution = BrcApvInstitution.new(params[:brc_apv_institution])

    if @brc_apv_institution.save
      flash[:success] = t('activerecord.success.model.brc_apv_institution.create_ok')
      redirect_to brc_apv_institutions_path
    else
      render action: 'new'
    end

  end

  # PUT /brc_apv_institutions/1
  # PUT /brc_apv_institutions/1.json
  def update
    @brc_apv_institution = BrcApvInstitution.find(params[:id])

    if @brc_apv_institution.update_attributes(params[:brc_apv_institution])
      flash[:success] = t('activerecord.success.model.brc_apv_institution.update_ok')
      redirect_to brc_apv_institutions_path
    else
      render action: 'edit'
    end
  end

  # DELETE /brc_apv_institutions/1
  # DELETE /brc_apv_institutions/1.json
  def destroy
    @brc_apv_institution = BrcApvInstitution.find(params[:id])
    @brc_apv_institution.destroy

    respond_to do |format|
      format.html { redirect_to brc_apv_institutions_url }
      format.json { head :no_content }
    end
  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'brc', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

end
