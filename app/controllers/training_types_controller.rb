class TrainingTypesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @training_types = TrainingType.all
  end


  def show
    @training_type = TrainingType.find(params[:id])
  end


  def new
    @training_type = TrainingType.new
  end


  def edit
    @training_type = TrainingType.find(params[:id])
  end

  def delete
    @training_type = TrainingType.find(params[:id])
  end


  def create
    @training_type = TrainingType.new(params[:training_type])


      if @training_type.save
        flash[:success] = t('activerecord.success.model.training_type.create_ok')
        redirect_to training_types_path
      else
        render action: 'new'
      end

  end


  def update
    @training_type = TrainingType.find(params[:id])


      if @training_type.update_attributes(params[:training_type])
        flash[:success] = t('activerecord.success.model.training_type.update_ok')
        redirect_to training_types_path
      else
        render action: 'edit'
      end

  end



  def destroy

    @training_type = TrainingType.find(params[:id])

    if verify_recaptcha

      if @training_type.destroy
        flash[:success] = t('activerecord.success.model.training_type.delete_ok')
        redirect_to training_types_path
      else
        flash[:danger] = t('activerecord.success.model.training_type.delete_error')
        redirect_to delete_training_type_path(@training_type)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.training_type.captcha_error')
      redirect_to delete_training_type_path(@training_type)

    end

  end

end
