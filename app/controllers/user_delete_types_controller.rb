class UserDeleteTypesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /user_delete_types
  # GET /user_delete_types.json
  def index
    @user_delete_types = UserDeleteType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @user_delete_types }
    end
  end

  # GET /user_delete_types/1
  # GET /user_delete_types/1.json
  def show
    @user_delete_type = UserDeleteType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user_delete_type }
    end
  end

  # GET /user_delete_types/new
  # GET /user_delete_types/new.json
  def new
    @user_delete_type = UserDeleteType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_delete_type }
    end
  end

  # GET /user_delete_types/1/edit
  def edit
    @user_delete_type = UserDeleteType.find(params[:id])
  end

  # POST /user_delete_types
  # POST /user_delete_types.json
  def create
    @user_delete_type = UserDeleteType.new(params[:user_delete_type])

    respond_to do |format|
      if @user_delete_type.save
        format.html { redirect_to @user_delete_type, notice: 'User delete type was successfully created.' }
        format.json { render json: @user_delete_type, status: :created, location: @user_delete_type }
      else
        format.html { render action: "new" }
        format.json { render json: @user_delete_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /user_delete_types/1
  # PUT /user_delete_types/1.json
  def update
    @user_delete_type = UserDeleteType.find(params[:id])

    respond_to do |format|
      if @user_delete_type.update_attributes(params[:user_delete_type])
        format.html { redirect_to @user_delete_type, notice: 'User delete type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user_delete_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_delete_types/1
  # DELETE /user_delete_types/1.json
  def destroy
    @user_delete_type = UserDeleteType.find(params[:id])
    @user_delete_type.destroy

    respond_to do |format|
      format.html { redirect_to user_delete_types_url }
      format.json { head :no_content }
    end
  end
end
