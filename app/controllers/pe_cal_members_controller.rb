class PeCalMembersController < ApplicationController

  before_filter :get_data_1, only: [:list, :create]
  before_filter :get_data_2, only: [:destroy]
  before_filter :validate_manager, only: [:list, :create, :destroy]

  def list

    @pe_cal_members = @pe_cal_session.pe_cal_members_ordered

    @user_search = User.new(params[:user])

    @characteristics_prev = {}

    if params[:characteristic]

      @pe_process.pe_characteristics_gui_cal_member.each do |pe_characteristic|

        @characteristics_prev[pe_characteristic.characteristic.id] = params[:characteristic][pe_characteristic.characteristic.id.to_s] unless params[:characteristic][pe_characteristic.characteristic.id.to_s].blank?

      end

    end

    if params[:pe_member_evaluator_jefe] && !params[:pe_member_evaluator_jefe][:id].blank?
      @pe_members_search = @pe_process.search_evaluated_members_by_jefe_ordered(params[:pe_member_evaluator_jefe][:id],@user_search.apellidos, @user_search.nombre, true, @characteristics_prev)
    elsif @characteristics_prev.size > 0
      @pe_members_search = @pe_process.search_evaluated_members_ordered('', @user_search.apellidos, @user_search.nombre, true, @characteristics_prev)
    else
      @pe_members_search = @pe_process.search_evaluated_members_ordered('', @user_search.apellidos, @user_search.nombre)
    end

  end

  def create

    if params[:list_item]

      params[:list_item].each do |pe_member_id,value|

        if value != '0'

          pe_cal_member = @pe_cal_session.pe_cal_members.build
          pe_cal_member.pe_process = @pe_process
          pe_cal_member.pe_member_id = pe_member_id

          pe_cal_member.save

        end

      end

      flash[:success] =  t('activerecord.success.model.pe_cal_member.create_ok')

    end

    redirect_to pe_cal_members_list_path(@pe_cal_session)

  end

  def destroy

    @pe_cal_member.destroy
    flash[:success] =  t('activerecord.success.model.pe_cal_member.delete_ok')

    redirect_to pe_cal_members_list_path(@pe_cal_session)


  end


  private

  def get_data_1
    @pe_cal_session = PeCalSession.find(params[:pe_cal_session_id])
    if @pe_cal_session.pe_evaluation
      @pe_evaluation = @pe_cal_session.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
    else
      @pe_process = @pe_cal_session.pe_process
    end
  end

  def get_data_2
    @pe_cal_member = PeCalMember.find(params[:id])
    @pe_cal_session = @pe_cal_member.pe_cal_session
    if @pe_cal_session.pe_evaluation
      @pe_evaluation = @pe_cal_session.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
    else
      @pe_process = @pe_cal_session.pe_process
    end
  end

  def validate_manager
    unless @pe_process.pe_managers.where('user_id = ?', user_connected.id).count > 0
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end


end
