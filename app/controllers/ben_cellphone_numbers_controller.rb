class BenCellphoneNumbersController < ApplicationController
  include BenCellphoneNumbersHelper
  include BenCellphoneProcessesHelper

  before_filter :authenticate_user

  before_filter :verify_access_manage_module

  def complete_excel
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people
    @ben_cellphone_numbers = BenCellphoneNumber.includes(:user).order('users.apellidos asc, users.nombre asc, number asc')

    respond_to do |format|
      format.xlsx { render xlsx: 'ben_cellphone_number_complete_excel.xlsx', filename: t('views.ben_cellphone_process.shared.files_name.download_complete_excel_benefits')}
      format.html { render index }
    end
  end

  def search
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people

    @user = User.new
    @users = {}
    @from_to = params[:from_to]
    @ben_cellphone_number = nil

    @ben_cellphone_number = BenCellphoneNumber.find(params[:ben_cellphone_number_id]) if params[:ben_cellphone_number_id] and  params[:ben_cellphone_number_id]!= '-1'

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def index
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people
    @ben_cellphone_numbers = BenCellphoneNumber.includes(:user).order('users.apellidos asc, users.nombre asc, number asc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ben_cellphone_numbers }
    end
  end

  def show
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people

    if params[:ben_cellphone_number_id]
      @ben_cellphone_number = BenCellphoneNumber.find(params[:ben_cellphone_number_id])
      @ben_cellphone_number.user_id = User.find(params[:user_id]).id
      @ben_cellphone_number.save
      reprocess_number @ben_cellphone_number.id
    else
      @ben_cellphone_number = BenCellphoneNumber.find(params[:id])
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ben_cellphone_number }
    end
  end


  def new
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people
    @ben_cellphone_number = BenCellphoneNumber.new
    @ben_cellphone_number.user_id = params[:user_id]
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ben_cellphone_number }
    end
  end

  def edit
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people
    @ben_cellphone_number = BenCellphoneNumber.find(params[:id])
  end


  def create
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people
    params[:ben_cellphone_number][:number] = params[:ben_cellphone_number][:number].strip
    user = User.find(params[:user_id])
    @ben_cellphone_number = BenCellphoneNumber.new(params[:ben_cellphone_number])
    @ben_cellphone_number.user_id = user.id
    @ben_cellphone_number.subsidy = @ben_cellphone_number.subsidy.to_f.round(4) if @ben_cellphone_number.subsidy

    respond_to do |format|
      if @ben_cellphone_number.save
        reprocess_number @ben_cellphone_number.id
        format.html { redirect_to @ben_cellphone_number }
        format.json { render json: @ben_cellphone_number, status: :created, location: @ben_cellphone_number }
      else
        format.html { render action: "new" }
        format.json { render json: @ben_cellphone_number.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @ben_cellphone_number = BenCellphoneNumber.find(params[:ben_cellphone_number_id])
    params[:ben_cellphone_number][:subsidy] = fix_form_floats params[:ben_cellphone_number][:subsidy]
    respond_to do |format|
      if @ben_cellphone_number.update_attributes(params[:ben_cellphone_number])
        reprocess_number @ben_cellphone_number.id
        flash[:success] = t('views.ben_cellphone_number.flash_messages.updated_succed')
        format.html { redirect_to @ben_cellphone_number }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ben_cellphone_number.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @ben_cellphone_number = BenCellphoneNumber.find(params[:id])
    @ben_cellphone_number.destroy

    respond_to do |format|
      format.html { redirect_to ben_cellphone_numbers_url }
      format.json { head :no_content }
    end
  end


  def ben_massive_update

  end

  def ben_massive_update_aux
    @error_index = []
    @error_content = []
    @error_message = []

    if params[:upload]
      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'
        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5) + '.xlsx'
        archivo_temporal = directorio_temporal + nombre_temporal
        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        if its_safe archivo_temporal
          flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
          redirect_to action: 'index'
        else
          flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
          render 'ben_massive_update'
        end

      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
        render 'ben_massive_update'
      end
    else
      flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
      render 'ben_massive_update'
    end
  end

  def export_to_excel
    @ben_cellphone_numbers = BenCellphoneNumber.includes(:user).order('users.apellidos asc, users.nombre asc, number asc')
    respond_to do |format|
      format.xlsx { render xlsx: 'ben_cellphone_number_excel.xlsx', filename: t('views.ben_cellphone_process.shared.files_name.download_benefits'), locals: { only_headers: 0 } }
      format.html { render index }
    end
  end

  def download_excel_headers
    respond_to do |format|
      format.xlsx { render xlsx: 'ben_cellphone_number_excel.xlsx', filename: t('views.ben_cellphone_process.shared.files_name.download_benefits'), locals: { only_headers: 1 } }
      #format.html { render index }
    end
  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'ben_cel_sec', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end


end
