class SelRequirementsController < ApplicationController
  before_filter :authenticate_user
  before_filter :authorized_to_manage_processes, only: [:index, :unattended]
  # GET /sel_requirements
  # GET /sel_requirements.json
  def index
    @sel_requirements = SelRequirement.all
  end

  def mine
    @sel_requirements = []
    sel_requirements_aux = SelRequirement.where(:registered_by_user_id => user_connected.id)
    sel_requirements_aux.each do |sel_req|
      @sel_requirements << sel_req unless sel_req.deleted?
    end
    render 'sel_requirements/index'
  end

  def unattended
    @sel_requirements = SelRequirement.unattended
    render 'sel_requirements/index'
  end

  # GET /sel_requirements/1
  # GET /sel_requirements/1.json
  def show
    flash[:danger] = 'No tiene permisos para esta opción'
    redirect_to root_path
  end

  # GET /sel_requirements/new
  # GET /sel_requirements/new.json
  def new
    @sel_requirement = SelRequirement.new
    @sel_requirement.sel_req_template = SelReqTemplate.find_by_active(true)

    respond_to do |format|
      format.html # new.html.erb
      format.json {render json: @sel_requirement}
    end
  end

  # GET /sel_requirements/1/edit
  def edit
    @sel_requirement = SelRequirement.find(params[:id])
    @sel_requirement.sel_req_template = SelReqTemplate.find_by_active(true)
  end

  # POST /sel_requirements
  # POST /sel_requirements.json
  def create
    @sel_requirement = SelRequirement.new(params[:sel_requirement])
    @sel_requirement.registered_at = lms_time
    @sel_requirement.registered_by_user_id = user_connected.id

    @sel_requirement.transaction do
      if @sel_requirement.save
        log = @sel_requirement.sel_req_logs.new
        log.log_state_id = params[:validate] ? 1 : 0
        log.registered_at = lms_time
        log.registered_by_user_id = user_connected.id
        log.save
        params[:sel_requirement_value].each do |sel_req_item_id, value|
          if value[:char_value_string] || value[:char_value_text] || value[:char_value_date] || value[:char_value_int] || value[:char_value_float] || (value[:char_value_jjob_id] && !value[:char_value_jjob_id].blank?) || (value[:char_value_id] && !value[:char_value_id].blank?) || (value[:value] && !value[:value].blank?) # para soportar campos del tipo 'file' y 'select' que no sean obligatorios
            @sel_requirement_value = SelRequirementValue.new(:sel_requirement_id => @sel_requirement.id, :sel_req_item_id => sel_req_item_id)
            case value[:item_type]
            when '0' #string
              @sel_requirement_value.value = value[:value]
            when '1' #number
              @sel_requirement_value.value = value[:value]
            when '2' #date
              @sel_requirement_value.value = value[:value]
            when '3' #options (select)
              @sel_requirement_value.sel_req_option_id = value[:value]
              @sel_requirement_value.value = @sel_requirement_value.sel_req_option.name
            when '4' #file
              @sel_requirement_value.value = SecureRandom.hex(5).to_s + File.extname(value[:value].original_filename)
            when '5' #text
              @sel_requirement_value.value = value[:value]
            when '6' #usuario aprobador (select)
              @sel_requirement_value.value = value[:value]
            when '7' #characteristic
              case @sel_requirement_value.sel_req_item.characteristic.data_type_id
              when 0
                @sel_requirement_value.value = value[:char_value_string]
                @sel_requirement_value.char_value_string = value[:char_value_string]
              when 1
                @sel_requirement_value.value = value[:char_value_text]
                @sel_requirement_value.char_value_text = value[:char_value_text]
              when 2
                @sel_requirement_value.value = value[:char_value_date]
                @sel_requirement_value.char_value_date = value[:char_value_date]
              when 3
                @sel_requirement_value.value = value[:char_value_int]
                @sel_requirement_value.char_value_int = value[:char_value_int]
              when 4
                @sel_requirement_value.value = value[:char_value_float]
                @sel_requirement_value.char_value_float = value[:char_value_float]
              when 5
                char_value = @sel_requirement_value.sel_req_item.characteristic.characteristic_values.where(id: value[:char_value_id]).first
                @sel_requirement_value.value = char_value.value_string
                @sel_requirement_value.char_value = char_value
              when 13
                if ct_module_jobs?
                  j = JJob.find(value[:char_value_jjob_id])
                  j_c = ''
                  JCharacteristic.all.each do |j_characteristic|
                    j_c += ', ' + j.formatted_j_job_characteristic_value(j_characteristic.characteristic)
                  end
                  if @company.is_security
                    JjCharacteristic.where('val_sec_1 = ?', true).each do |jj_characteristic|
                      fc = j.formatted_jj_characteristic_value(jj_characteristic)
                      j_c += ', ' + fc unless fc.nil?
                    end
                  end
                  value_string = (j.j_code ? j.j_code + ' - ' : '') + '' + j.name + j_c, j.id
                  @sel_requirement_value.value = value_string
                  @sel_requirement_value.char_value_jjob = j
                else
                  @sel_requirement_value.value = value[:char_value_string]
                  @sel_requirement_value.value_string = value[:char_value_string]
                end

              end
            end
            if @sel_requirement_value.save
              if value[:item_type] == '4'
                #file = params['sel_applicant_value_' + sel_apply_form_id.to_s ]['datafile']
                file = value[:value]
                save_file(file, @company, @sel_requirement_value)
              end
            else
              raise ActiveRecord::Rollback, 'Call tech support!'
              flash[:danger] = 'Hubo un problema al intentar registrar la solicitud. Puede internarlo nuevamante.'
              redirect_to :back
            end
          end
        end
      end
    end
    flash[:success] = 'La solicitud fue registrada correctamente'
    redirect_to sel_requirements_mine_path

  end

  # PUT /sel_requirements/1
  # PUT /sel_requirements/1.json
  def update
    @sel_requirement = SelRequirement.find(params[:id])
    @sel_requirement.transaction do
      if @sel_requirement.save
        log = @sel_requirement.sel_req_logs.new
        log.log_state_id = params[:validate] ? 1 : 0
        log.registered_at = lms_time
        log.registered_by_user_id = user_connected.id
        log.save
        params[:sel_requirement_value].each do |sel_req_item_id, value|
          #if value[:value] && !value[:value].blank? # para soportar campos del tipo 'file' y 'select' que no sean obligatorios
          @sel_requirement_value = SelRequirementValue.where(:sel_requirement_id => @sel_requirement.id, :sel_req_item_id => sel_req_item_id).first_or_initialize
          case value[:item_type]
          when '0' #string
            @sel_requirement_value.value = value[:value]
          when '1' #number
            @sel_requirement_value.value = value[:value]
          when '2' #date
            @sel_requirement_value.value = value[:value]
          when '3' #options (select)
            @sel_requirement_value.sel_req_option_id = value[:value]
            @sel_requirement_value.value = value[:value] ? @sel_requirement_value.sel_req_option.name : ''
          when '4' #file
            if value[:value] && !value[:value].blank?
            @sel_requirement_value.value = SecureRandom.hex(5).to_s + File.extname(value[:value].original_filename)
            elsif value[:file]
              @sel_requirement_value.value = ''
            end
          when '5' #text
            @sel_requirement_value.value = value[:value]
          when '6' #usuario aprobador (select)
            @sel_requirement_value.value = value[:value]
          when '7' #characteristic
            case @sel_requirement_value.sel_req_item.characteristic.data_type_id
            when 0
              @sel_requirement_value.value = value[:char_value_string]
              @sel_requirement_value.char_value_string = value[:char_value_string]
            when 1
              @sel_requirement_value.value = value[:char_value_text]
              @sel_requirement_value.char_value_text = value[:char_value_text]
            when 2
              @sel_requirement_value.value = value[:char_value_date]
              @sel_requirement_value.char_value_date = value[:char_value_date]
            when 3
              @sel_requirement_value.value = value[:char_value_int]
              @sel_requirement_value.char_value_int = value[:char_value_int]
            when 4
              @sel_requirement_value.value = value[:char_value_float]
              @sel_requirement_value.char_value_float = value[:char_value_float]
            when 5
              char_value = @sel_requirement_value.sel_req_item.characteristic.characteristic_values.where(id: value[:char_value_id]).first
              @sel_requirement_value.value = char_value ? char_value.value_string : ''
              @sel_requirement_value.char_value = char_value
            when 13
              if ct_module_jobs?
                unless value[:char_value_jjob_id].blank?
                  j = JJob.find(value[:char_value_jjob_id])
                  j_c = ''
                  JCharacteristic.all.each do |j_characteristic|
                    j_c += ', ' + j.formatted_j_job_characteristic_value(j_characteristic.characteristic)
                  end
                  if @company.is_security
                    JjCharacteristic.where('val_sec_1 = ?', true).each do |jj_characteristic|
                      fc = j.formatted_jj_characteristic_value(jj_characteristic)
                      j_c += ', ' + fc unless fc.nil?
                    end
                  end
                  value_string = (j.j_code ? j.j_code + ' - ' : '') + '' + j.name + j_c, j.id
                  @sel_requirement_value.value = value_string
                  @sel_requirement_value.char_value_jjob = j
                else
                  @sel_requirement_value.value = ''
                  @sel_requirement_value.char_value_jjob = nil
                end
              else
                @sel_requirement_value.value = value[:char_value_string]
                @sel_requirement_value.value_string = value[:char_value_string]
              end

            end
          end
          if @sel_requirement_value.save
            if value[:value] && value[:item_type] == '4' && !value[:value].blank?
              #file = params['sel_applicant_value_' + sel_apply_form_id.to_s ]['datafile']
              file = value[:value]
              puts '----------------------------------------------------------------------'
              puts 'archivo a guardar -> ' + @sel_requirement_value.inspect
              save_file(file, @company, @sel_requirement_value)
            end
          else
            raise ActiveRecord::Rollback, 'Call tech support!'
            flash[:danger] = 'Hubo un problema al intentar registrar la solicitud. Puede internarlo nuevamante.'
            redirect_to :back
          end
          #end
        end
      end
    end

    redirect_to sel_requirements_mine_path
  end

  # DELETE /sel_requirements/1
  # DELETE /sel_requirements/1.json
  def destroy
    flash[:danger] = 'No tiene permisos para esta opción'
    redirect_to root_path
  end

  def delete
    sel_requirement = SelRequirement.find(params[:id])
    if sel_requirement.is_deletable?
      delete_log = sel_requirement.sel_req_logs.new
      delete_log.log_state_id = 4
      delete_log.registered_at = lms_time
      delete_log.registered_by_user_id = user_connected.id
      if delete_log.save
        flash[:success] = 'La solicitud fue eliminada correctamente'
      else
        flash[:danger] = 'La solicitud no pudo ser eliminada correctamente'
      end
    else
      flash[:danger] = 'La solicitud no puede ser eliminada'
    end
    redirect_to sel_requirements_mine_path
  end

  def reject
    sel_requirement = SelRequirement.find(params[:id])
    if sel_requirement.is_rejectable?
      sel_requirement.sel_req_processes.where(active: true).update_all(active: false)
      reject_log = sel_requirement.sel_req_logs.new
      reject_log.log_state_id = 3
      reject_log.registered_at = lms_time
      reject_log.registered_by_user_id = user_connected.id
      reject_log.comment = params[:reject_comment] if params[:reject_comment]
      if reject_log.save
        flash[:success] = 'La solicitud fue rechazada correctamente'
      else
        flash[:danger] = 'La solicitud no pudo ser rechazada correctamente'
      end
    else
      flash[:danger] = 'La solicitud no puede ser rechazada'
    end
    redirect_to sel_requirements_unattended_path
  end

  def download_file
    sel_requirement_value = SelRequirementValue.find(params[:sel_requirement_value_id])

    company = @company
    directory = company.directorio + '/' + company.codigo + '/sel_requirements/' + sel_requirement_value.sel_requirement_id.to_s
    #name = SecureRandom.hex(5).to_s + File.extname(upload.original_filename)
    full_path = File.join(directory, sel_requirement_value.value)

    send_file full_path,
              filename: sel_requirement_value.value,
              type: 'application/octet-stream',
              disposition: 'attachment'
  end

  def generate_approval_process
    sel_requirement = SelRequirement.find(params[:id])
    sel_requirement.sel_req_processes.where(active: true).update_all(active: false)
    sel_req_process = sel_requirement.sel_req_processes.build
    sel_req_process.registered_at = lms_time
    sel_req_process.registered_by_user_id = user_connected.id
    sel_req_process.active = true
    sel_req_process.save

    # generación del log de 'en atención' de la solicitud
    if sel_requirement && sel_requirement.current_state_log.log_state_id != 5
      log = sel_requirement.sel_req_logs.new
      log.log_state_id = 5
      log.registered_at = lms_time
      log.registered_by_user_id = user_connected.id
      log.save
    end
    # generación del log de 'en atención' de la solicitud

    flash[:success] = 'Proceso de aprobación de solicitud generado correctamente. Por favor configure los evaluadores.'
    redirect_to edit_sel_req_process_path sel_req_process
  end


  private

  def save_file(upload, company, sel_requirement_value)

    directory = company.directorio + '/' + company.codigo + '/sel_requirements/' + sel_requirement_value.sel_requirement_id.to_s
    #name = SecureRandom.hex(5).to_s + File.extname(upload.original_filename)
    full_path = File.join(directory, sel_requirement_value.value)

    #FileUtils.remove_dir directory if File.directory? directory
    FileUtils.mkdir_p directory unless File.directory? directory

    File.open(full_path, 'wb') {|f| f.write(upload.read)}
  end


end
