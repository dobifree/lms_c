class ScProcessesClosesController < ApplicationController
  include ScProcessesValidatesHelper
  before_filter :get_gui_characteristics, only: [:close_process,
                                                 :dont_charge_to_payroll_record_show_modal,
                                                 :show_to_close_records_list,
                                                 :refresh_user_process_row]

  before_filter :authenticate_user, except: [:attendances_detail,
                                             :dont_charge_to_payroll_record_show_modal,
                                             :show_to_close_records_list,
                                             :refresh_user_process_row]

  before_filter :verify_access_manage_module, except: [:attendances_detail,
                                                       :dont_charge_to_payroll_record_show_modal,
                                                       :show_to_close_records_list,
                                                       :refresh_user_process_row]

  before_filter :authenticate_user_partial, only: [:attendances_detail,
                                                   :dont_charge_to_payroll_record_show_modal,
                                                   :show_to_close_records_list,
                                                   :refresh_user_process_row]

  before_filter :verify_access_manage_module_partial, only: [:attendances_detail,
                                                             :dont_charge_to_payroll_record_show_modal,
                                                             :show_to_close_records_list,
                                                             :refresh_user_process_row]

  # STATUS GUIDE
  #     0:Pendiente de enviar validación,
  #     1:Pendiente de ser validado.
  #     2:En discusión.
  #     3:Validado.
  #     4:Rechazado


  def back_to_edit_process
    process = ScProcess.find(params[:sc_process_id])
    if process.closed
      render partial: 'shared/div_message',
             locals: { type_generic_message: 'alert-danger',
                       generic_message: 'Proceso cerrado, disponible solo para visualización' }
    else
      report = ScReporting.where(sc_process_id: process.id, active: true).first
      if report
        report.active = false
        report.deactivated_by_user_id = user_connected.id
        report.deactivated_at = lms_time
        report.save
      end
      process.sc_records.each { |record| record.update_attributes(sent_to_payroll: false) }
      render partial: 'shared/div_message',
             locals: { type_generic_message: 'alert-success',
                       generic_message: 'Cambio realizado correctamente' }
    end
  end

  def close_process
    @sc_process = ScProcess.find(params[:sc_process_id])
    @tab = params[:tab] ? params[:tab] : 0
    @to_validation = 0
    @waiting_validation = 0
  end

  def attendances_detail
    sc_user_process = ScUserProcess.find(params[:sc_user_process_id])
    render :partial => 'sc_user_process_payroll_detail',
           :locals => {:sc_user_process => sc_user_process, payroll_options: true}
  end

  def action_close_process
    process = ScProcess.find(params[:sc_process_id])
    process.closed = true
    process.closed_at = lms_time
    process.closed_by_user = user_connected
    process.save
    flash[:success] = t('views.sc_processes.flash_messages.success_close_process')
    redirect_to close_process_path(process, 1)
  end

  def action_open_process
    process = ScProcess.find(params[:sc_process_id])
    process.closed = false
    process.closed_at = nil
    process.closed_by_user = nil
    process.save
    flash[:success] = t('views.sc_processes.flash_messages.success_open_process')
    redirect_to close_process_path(process, 0)
  end

  def dont_charge_to_payroll_record_show_modal
    render :partial => 'dont_charge_to_payroll_record_modal',
           :locals => { :record => ScRecord.find(params[:sc_record_id]),
                        :characteristics => @sc_schedule_characteristics }

  end

  def dont_charge_to_payroll_update_record
    record = ScRecord.find(params[:sc_record_id])
    log = ScLogRecord.create(:user_id => user_connected.id, :sc_record_id => record.id, :date => lms_time, :record_before => record.as_json, :action => 'No cargar a Payroll')
    record.dont_charge_to_payroll = true
    record.dont_charge_to_payroll_at = lms_time
    record.dont_charge_to_payroll_by_user = user_connected
    record.dont_charge_to_payroll_description = params[:sc_record][:dont_charge_to_payroll_description]
    record.save
    update_he_user_process(record.user_id, record.sc_process_id)
    render :nothing => true
  end

  def charge_to_payroll_update_record
    record = ScRecord.find(params[:sc_record_id])
    log = ScLogRecord.create(:user_id => user_connected.id, :sc_record_id => record.id, :date => lms_time, :record_before => record.as_json, :action => 'Cargar a Payroll')
    record.dont_charge_to_payroll = false
    record.dont_charge_to_payroll_at = nil
    record.dont_charge_to_payroll_by_user = nil
    record.dont_charge_to_payroll_description = nil
    record.save
    update_he_user_process(record.user_id, record.sc_process_id)
    render :nothing => true
  end

  def show_to_close_records_list
    success_message = params[:success_message] ? params[:success_message] : false

    render :partial => 'charge_or_not_to_payroll_list',
           :locals => { :process => ScProcess.find(params[:sc_process_id]),
                        :editable => params[:editable],
                        :success_message => success_message,
                        :characteristics => @sc_schedule_characteristics }
  end

  def refresh_user_process_row
    sc_user_process = ScUserProcess.find(params[:sc_user_process_id])
    render :partial => 'sc_user_process_payroll',
           :locals => {:sc_user_process => sc_user_process,
                       :characteristics => @sc_schedule_characteristics,
                       :success_message => true,
                       :open_detail => true}
  end

  private
  def verify_open_process
    process = ScProcess.find(params[:sc_process_id])
    if process.closed
      flash[:danger] = t('views.sc_processes.flash_messages.process_closed')
      redirect_to close_process_path(process,0)
    end
  end

  def get_gui_characteristics
    @sc_schedule_characteristics = ScScheduleCharacteristic.gui
  end

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_manage_module_partial
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      render :partial => 'shared/div_message',
             :locals => { :type_generic_message => 'alert-danger',
                          :generic_message => t('security.no_access_to_manage_module')}
    end
  end

  def authenticate_user_partial
    unless logged_in?
      render :partial => 'shared/div_message',
             :locals => { :type_generic_message => 'alert-danger',
                          :generic_message => 'No ha iniciado sesión'}
    end

  end

end
