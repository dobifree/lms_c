class JmProfileSectionsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /jm_profile_sections
  # GET /jm_profile_sections.json
  def index
    @jm_profile_sections = JmProfileSection.all
  end

  # GET /jm_profile_sections/1
  # GET /jm_profile_sections/1.json
  def show
    @jm_profile_section = JmProfileSection.find(params[:id])
  end

  # GET /jm_profile_sections/new
  # GET /jm_profile_sections/new.json
  def new
    @jm_profile_section = JmProfileSection.new
  end

  # GET /jm_profile_sections/1/edit
  def edit
    @jm_profile_section = JmProfileSection.find(params[:id])
  end

  # POST /jm_profile_sections
  # POST /jm_profile_sections.json
  def create
    @jm_profile_section = JmProfileSection.new(params[:jm_profile_section])

      if @jm_profile_section.save
        flash[:success] = 'La sección ha sido creada correctamente'
        redirect_to jm_profile_sections_path
      else
        flash.now[:danger] = 'La sección no pudo ser creada correctamente'
        render 'new'
      end
  end

  # PUT /jm_profile_sections/1
  # PUT /jm_profile_sections/1.json
  def update
    @jm_profile_section = JmProfileSection.find(params[:id])

      if @jm_profile_section.update_attributes(params[:jm_profile_section])
        flash[:success]= 'La sección fue actualizada correctamente'
        redirect_to jm_profile_sections_path
      else
        flash.now[:danger]= 'La sección no pudo ser actualizada correctamente'
        render 'edit'
      end
  end

  # DELETE /jm_profile_sections/1
  # DELETE /jm_profile_sections/1.json
  def destroy
    @jm_profile_section = JmProfileSection.find(params[:id])
    @jm_profile_section.destroy

    flash[:success] = 'La sección ha sido eliminada correctamente'
    redirect_to jm_profile_sections_path
  end
end
