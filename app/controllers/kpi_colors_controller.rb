class KpiColorsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def new

    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
    @kpi_color = @kpi_dashboard.kpi_colors.build

  end

  def edit
    @kpi_color = KpiColor.find(params[:id])
    @kpi_dashboard = @kpi_color.kpi_dashboard
  end

  def create
    @kpi_color = KpiColor.new(params[:kpi_color])

    if @kpi_color.save
      flash[:success] =  t('activerecord.success.model.kpi_color.create_ok')
      redirect_to @kpi_color.kpi_dashboard
    else
      @kpi_dashboard = @kpi_color.kpi_dashboard
      render action: 'new'
    end
  end


  def update
    @kpi_color = KpiColor.find(params[:id])

    if @kpi_color.update_attributes(params[:kpi_color])
      flash[:success] =  t('activerecord.success.model.kpi_color.update_ok')
      redirect_to @kpi_color.kpi_dashboard
    else
      @kpi_dashboard = @kpi_color.kpi_dashboard
      render action: 'edit'
    end

  end

  def destroy

    kpi_color = KpiColor.find(params[:id])
    kpi_dashboard = kpi_color.kpi_dashboard
    kpi_color.destroy

    flash[:success] =  t('activerecord.success.model.kpi_dashboard_color.delete_ok')
    redirect_to kpi_dashboard

  end


  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'kpis', true).first

      unless admin_logged_in? || (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end

end
