class SelTemplatesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /sel_templates
  # GET /sel_templates.json
  def index
    @sel_templates = SelTemplate.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sel_templates }
    end
  end

  # GET /sel_templates/1
  # GET /sel_templates/1.json
  def show
    @sel_template = SelTemplate.find(params[:id])

    @show_pill_open = false
    if params[:pill_name]
      @show_pill_open = true
      @pill_name_open = params[:pill_name]
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sel_template }
    end
  end

  # GET /sel_templates/new
  # GET /sel_templates/new.json
  def new
    @sel_template = SelTemplate.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sel_template }
    end
  end

  # GET /sel_templates/1/edit
  def edit
    @sel_template = SelTemplate.find(params[:id])
  end

  # POST /sel_templates
  # POST /sel_templates.json
  def create
    @sel_template = SelTemplate.new(params[:sel_template])

    respond_to do |format|
      if @sel_template.save
        flash[:success] = 'La plantilla fue creada correctamente'
        format.html { redirect_to @sel_template }
        format.json { render json: @sel_template, status: :created, location: @sel_template }
      else
        format.html { render action: "new" }
        format.json { render json: @sel_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sel_templates/1
  # PUT /sel_templates/1.json
  def update
    @sel_template = SelTemplate.find(params[:id])

    respond_to do |format|
      if @sel_template.update_attributes(params[:sel_template])
        flash[:success] = 'La plantilla fue actualizada correctamente'
        format.html { redirect_to @sel_template}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sel_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sel_templates/1
  # DELETE /sel_templates/1.json
  def destroy
    @sel_template = SelTemplate.find(params[:id])
    @sel_template.destroy

    respond_to do |format|
      format.html { redirect_to sel_templates_url }
      format.json { head :no_content }
    end
  end

  def characteristics_list
    @sel_template = SelTemplate.find(params[:template_id])
    render  'characteristics_list', layout: false
  end

  def steps_list
    @sel_template = SelTemplate.find(params[:template_id])
    render  'steps_list', layout: false
  end

  def apply_forms_list
    @sel_template = SelTemplate.find(params[:template_id])
    render  'apply_forms_list', layout: false
  end

end
