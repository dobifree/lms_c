class ProgramsController < ApplicationController
  
  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @programs = Program.all
  end

  def show
    @program = Program.find(params[:id])
  end

  def new
    @program = Program.new
  end

  def edit
    @program = Program.find(params[:id])
  end

  def delete
    @program = Program.find(params[:id])
  end

  def create
    @program = Program.new(params[:program])    

    if @program.save
      flash[:success] = t('activerecord.success.model.program.create_ok')

      if @program.plan_anual
        Program.all.each do |p|
          if p.id != @program.id
            p.plan_anual = false
            p.save
          end

        end

      end

      redirect_to programs_path
    else
      render action: 'new'
    end
  end

  def update

    @program = Program.find(params[:id])

    if @program.update_attributes(params[:program])
      flash[:success] = t('activerecord.success.model.program.update_ok')
      if @program.plan_anual
        Program.all.each do |p|
          if p.id != @program.id
            p.plan_anual = false
            p.save
          end

        end

      end
      redirect_to programs_path
    else
      render action: 'edit'
    end

  end

  def destroy

    @program = Program.find(params[:id])
   
    if verify_recaptcha
    
      if @program.destroy
        flash[:success] = t('activerecord.success.model.program.delete_ok')  
        redirect_to programs_url
      else
        flash[:danger] = t('activerecord.success.model.program.delete_error')
        redirect_to delete_program_path(@program)
      end

    else

      flash.delete(:recaptcha_error)
      
      flash[:danger] = t('activerecord.error.model.program.captcha_error')
      redirect_to delete_program_path(@program)

    end

  end

  def search_program_inspector

    @program = Program.find(params[:id])

    @user = User.new
    @users = {}

    if params[:user]

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        @users = User.where(
            'apellidos LIKE ? AND nombre LIKE ?',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        @user.apellidos = params[:user][:apellidos]
        @user.nombre = params[:user][:nombre]
      end

    end

  end

  def create_program_inspector

    program = Program.find(params[:id])

    pi = program.program_inspectors.new(user_id: params[:user_id])
    pi.save

    flash[:success] = t('activerecord.success.model.program.add_program_inspector_ok')
    redirect_to programs_path


  end

  def destroy_program_inspector

    pi = ProgramInspector.find(params[:id])

    pi.destroy

    flash[:success] = t('activerecord.success.model.program.delete_program_inspector_ok')
    redirect_to programs_path

  end


end
