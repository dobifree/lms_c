class SupportTicketsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin, only: [:index, :index_atendidos, :search, :new, :show, :edit, :delete, :create, :update, :destroy, :xls_report]

  def index_user
    @support_tickets_sin_atender = user_connected.support_tickets.where('atendido = ?',false)
    @support_tickets_atendidos = user_connected.support_tickets.where('atendido = ?',true)

    @support_ticket = SupportTicket.new
  end

  def create_support_ticket_user

    @support_ticket = SupportTicket.new(params[:support_ticket])
    @support_ticket.user = user_connected
    @support_ticket.fecha_registro = Time.now.utc

    #@support_tickets = user_connected.support_tickets

    if @support_ticket.save

      LmsMailer.new_support_ticket(@support_ticket, @company).deliver

      flash[:success] = t('activerecord.success.model.support_ticket.create_ok')
      redirect_to support_tickets_index_user_path
    else
      @support_tickets_sin_atender = user_connected.support_tickets
      @support_tickets_atendidos = user_connected.support_tickets
      render action: 'index_user'
    end

  end

  def index
    @support_tickets = SupportTicket.find_all_by_atendido false
  end

  def index_atendidos
    @support_tickets = SupportTicket.find_all_by_atendido true
  end

  def search

    if params[:user]

      if params[:desdehasta]!= ''

        desdehasta = params[:desdehasta].split('-')

        desde_t = desdehasta[0].strip
        desde_d = desde_t.split('/').map { |s| s.to_i }

        desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

        hasta_t = desdehasta[1].strip
        hasta_d = hasta_t.split('/').map { |s| s.to_i }

        hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

        @desde = desdehasta[0].strip
        @hasta = desdehasta[1].strip

        @support_tickets = SupportTicket.joins(:user).where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND (email LIKE ? OR email IS NULL) AND
                                      support_tickets.id LIKE ? AND asunto LIKE ? AND fecha_registro >= ? AND fecha_registro <= ?',
                                                            '%'+params[:user][:codigo]+'%',
                                                            '%'+params[:user][:apellidos]+'%',
                                                            '%'+params[:user][:nombre]+'%',
                                                            '%'+params[:user][:email]+'%',
                                                            '%'+params[:support_ticket][:id]+'%',
                                                            '%'+params[:support_ticket][:asunto]+'%',
                                                            desde,
                                                            hasta)
      else

        @support_tickets = SupportTicket.joins(:user).where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND (email LIKE ? OR email IS NULL) AND
                                      support_tickets.id LIKE ? AND asunto LIKE ? ',
                                                            '%'+params[:user][:codigo]+'%',
                                                            '%'+params[:user][:apellidos]+'%',
                                                            '%'+params[:user][:nombre]+'%',
                                                            '%'+params[:user][:email]+'%',
                                                            '%'+params[:support_ticket][:id]+'%',
                                                            '%'+params[:support_ticket][:asunto]+'%')


      end




      @codigo = params[:user][:codigo]
      @apellidos = params[:user][:apellidos]
      @nombre = params[:user][:nombre]
      @email = params[:user][:email]

      @n_ticket = params[:support_ticket][:id]
      @asunto = params[:support_ticket][:asunto]

      @desdehasta = params[:desdehasta]

    else

      @support_tickets = Array.new

    end

  end

  def show
    @support_ticket = SupportTicket.find(params[:id])

    respond_to do |format|
      format.html # managege.html.erb
      format.json { render json: @support_ticket }
    end
  end


  def new
    @support_ticket = SupportTicket.new
  end

  def edit
   #@company = session[:company]
    @support_ticket = SupportTicket.find(params[:id])
  end

  def delete
    @support_ticket = SupportTicket.find(params[:id])
  end


  def create

    @support_ticket = SupportTicket.new(params[:support_ticket])

    Time.zone = user_connected_time_zone
    @support_ticket.fecha_registro = Time.zone.parse(params[:fecha_registro]).utc

    @support_ticket.fecha_respuesta = Time.zone.parse(params[:fecha_respuesta]).utc if @support_ticket.atendido

    if @support_ticket.save

      flash[:success] = t('activerecord.success.model.support_ticket.create_ok')
      redirect_to support_tickets_url
    else
      render action: 'new'
    end

  end


  def update
    @support_ticket = SupportTicket.find(params[:id])

    @support_ticket.fecha_respuesta = Time.now.utc if params[:support_ticket][:atendido]

    if @support_ticket.update_attributes(params[:support_ticket])

      if @support_ticket.atendido && @support_ticket.envio_email && @support_ticket.user.email

        LmsMailer.answer_support_ticket(@support_ticket, @company).deliver

      end

      flash[:success] = t('activerecord.success.model.support_ticket.update_ok')
      redirect_to support_tickets_url
    else
      render action: 'edit'
    end

  end


  def destroy
    @support_ticket = SupportTicket.find(params[:id])
    @support_ticket.destroy

    flash[:success] = t('activerecord.success.model.support_ticket.delete_ok')

    redirect_to support_tickets_url

  end

  def xls_report

    desde_hasta = params['desdehasta'].split(' - ')

    desde = desde_hasta[0].split(' ')
    desde_d = desde[0].split('/').map { |s| s }
    desde_h = desde[1].split(':').map { |s| s }

    from_date = DateTime.parse(desde_d[2]+'-'+desde_d[1]+'-'+desde_d[0]+' '+desde_h[0]+':'+desde_h[1]+':00 '+desde[2])

    hasta = desde_hasta[1].split(' ')
    hasta_d = hasta[0].split('/').map { |s| s }
    hasta_h = hasta[1].split(':').map { |s| s }

    to_date = DateTime.parse(hasta_d[2]+'-'+hasta_d[1]+'-'+hasta_d[0]+' '+hasta_h[0]+':'+hasta_h[1]+':59 '+hasta[2])

    @support_tickets = SupportTicket.where('atendido = ? and fecha_registro >= ? AND fecha_registro <= ?', true, from_date, to_date)

    filename = 'reporte_tickets_mesa_ayuda_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end


  end

end
