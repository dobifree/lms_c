class KpiManagingController < ApplicationController

  include UsersHelper
  include KpiManagingHelper

  before_filter :authenticate_user
  before_filter :verify_access_manage_dashboard, only:[:manage_indicators, :manage_guests, :create_dashboard_guest,
                                                       :download_xls_for_update, :upload_xls_for_update_form,
                                                       :upload_xls_for_update, :update_csv]
  before_filter :verify_access_manage_dashboard_2, only:[:destroy_dashboard_guest]
  before_filter :verify_access_manage_dashboard_3, only:[:destroy_kpi_set_guest]
  before_filter :verify_access_query_dashboard, only:[:query, :download_xls_query]

  def index

    @managed_kpi_dashboards = user_connected.managed_kpi_dashboards
    @guest_kpi_dashboards = user_connected.guest_kpi_dashboards + user_connected.guest_kpi_dashboards_from_set

    if $mobile_client
      render 'index_mobile'
    else
      render 'index'
    end

  end

  def manage_indicators

    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
    @kpi_sets = @kpi_dashboard.kpi_sets
   #@company = session[:company]

    if $mobile_client
      render 'manage_indicators_mobile'
    else
      render 'manage_indicators'
    end

  end

  def manage_guests

    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
    @kpi_dashboard_guest = @kpi_dashboard.kpi_dashboard_guests.build
    @user_search = User.new(params[:user])

    @users_search = search_normal_users(@user_search.apellidos, @user_search.nombre)

  end

  def create_dashboard_guest

    kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])

    if(params[:kpi_set_guest][:kpi_set_id]=='0')

      kpi_dashboard_guest = kpi_dashboard.kpi_dashboard_guests.build

      kpi_dashboard_guest.user_id = params[:user_id]

      kpi_dashboard_guest.save

    else

      kpi_set = KpiSet.find(params[:kpi_set_guest][:kpi_set_id])

      if kpi_set && kpi_set.kpi_dashboard_id == kpi_dashboard.id

        kpi_set_guest = kpi_set.kpi_set_guests.build
        kpi_set_guest.user_id = params[:user_id]
        kpi_set_guest.save
      end

    end

    flash[:success] =  t('activerecord.success.model.kpi_dashboard_guest.create_ok')
    redirect_to kpi_managing_dashboard_manage_guests_path kpi_dashboard
  end

  def destroy_dashboard_guest

    kpi_dashboard_guest = KpiDashboardGuest.find(params[:id])
    kpi_dashboard = kpi_dashboard_guest.kpi_dashboard
    kpi_dashboard_guest.destroy

    flash[:success] =  t('activerecord.success.model.kpi_dashboard_guest.delete_ok')
    redirect_to kpi_managing_dashboard_manage_guests_path kpi_dashboard

  end

  def destroy_kpi_set_guest

    kpi_set_guest = KpiSetGuest.find(params[:id])
    kpi_dashboard = kpi_set_guest.kpi_set.kpi_dashboard
    kpi_set_guest.destroy

    flash[:success] =  t('activerecord.success.model.kpi_dashboard_guest.delete_ok')
    redirect_to kpi_managing_dashboard_manage_guests_path kpi_dashboard

  end

  def query
   #@company = session[:company]
    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
    if user_connected.kpi_dashboard_guests.where('kpi_dashboard_id = ?',params[:kpi_dashboard_id]).count > 0
      @kpi_sets = @kpi_dashboard.kpi_sets
    else
      @kpi_sets = user_connected.kpi_sets.where('kpi_dashboard_id = ?',params[:kpi_dashboard_id])
    end

    if $mobile_client
      render 'query_mobile'
    else
      render 'query'
    end

  end

  def download_xls_for_update

    dashboard = KpiDashboard.find(params[:kpi_dashboard_id])

    kpi_sets = dashboard.kpi_sets

    reporte_excel = xls_for_update_monthly dashboard, kpi_sets

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: dashboard.name+' '+dashboard.period+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_xls_query

    dashboard = KpiDashboard.find(params[:kpi_dashboard_id])

    if user_connected.kpi_dashboard_guests.where('kpi_dashboard_id = ?',params[:kpi_dashboard_id]).count > 0
      kpi_sets = dashboard.kpi_sets
    else
      kpi_sets = user_connected.kpi_sets.where('kpi_dashboard_id = ?',params[:kpi_dashboard_id])
    end

    reporte_excel = xls_for_update_monthly dashboard, kpi_sets

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: dashboard.name+' '+dashboard.period+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def upload_xls_for_update_form
    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
  end

  def upload_xls_for_update

    kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_data archivo_temporal, kpi_dashboard

        if @lineas_error.length > 0
          flash[:danger] = t('activerecord.error.model.kpi_managing.update_file_error')
          redirect_to kpi_managing_dashboard_upload_xls_for_update_path kpi_dashboard
        else
          save_data archivo_temporal, kpi_dashboard
          flash[:success] = t('activerecord.success.model.kpi_managing.update_file_ok')
          redirect_to kpi_managing_dashboard_manage_indicators_path kpi_dashboard

        end

      else

        flash[:danger] = t('activerecord.error.model.kpi_managing.upload_file_no_xlsx')
        redirect_to kpi_managing_dashboard_upload_xls_for_update_path kpi_dashboard

      end

    else
      flash[:danger] = t('activerecord.error.model.kpi_managing.upload_file_no_file')
      redirect_to kpi_managing_dashboard_upload_xls_for_update_path kpi_dashboard

    end



  end

  def update_csv

   #@company = session[:company]

    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])

    if params[:upload]

      directory = @company.directorio+'/'+@company.codigo+'/kpi_dashboards/'

      FileUtils.mkdir_p directory unless File.directory?(directory)

      name = @kpi_dashboard.id.to_s+'.csv'
      path = File.join(directory, name)
      File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

      flash[:success] = t('activerecord.success.model.kpi_dashboard.csv_create_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_dashboard

    else

      flash.now[:danger] = 'Debe adjuntar un archivo CSV'
      render 'manage_indicators'
    end

  end

  private

    def verify_access_manage_dashboard
      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',params[:kpi_dashboard_id]).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

    def verify_access_manage_dashboard_2

      kpi_dashboard_guest = KpiDashboardGuest.find(params[:id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',kpi_dashboard_guest.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

    def verify_access_manage_dashboard_3

      kpi_set_guest = KpiSetGuest.find(params[:id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',kpi_set_guest.kpi_set.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

    def verify_access_query_dashboard

      unless user_connected.kpi_dashboard_guests.where('kpi_dashboard_id = ?',params[:kpi_dashboard_id]).count > 0 || user_connected.guest_kpi_dashboards_from_set.where('kpi_dashboard_id = ?',params[:kpi_dashboard_id]).count > 0
        flash[:danger] = t('security.no_access_query_kpi_dashboard')
        redirect_to root_path
      end
    end

    def check_data(archivo_temporal, kpi_dashboard)

      dt_i = Array.new
      dt_f = Array.new

      prev_month = nil

      (kpi_dashboard.from..kpi_dashboard.to).each do |date|

        unless prev_month == date.strftime('%m').to_i

          prev_month = date.strftime('%m').to_i

          dt_i.push date
          dt_f.push date + 1.month

        end

      end

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      archivo = RubyXL::Parser.parse archivo_temporal

      data = archivo.worksheets[0].extract_data

      index = 0

      (1..3).each { index += 1 }

      kpi_dashboard.kpi_sets.each do |kpi_set|
        index += 1

        kpi_indicators = kpi_dashboard.allow_subsets? ? kpi_set.kpi_indicators_ordered_by_subset : kpi_set.kpi_indicators

        kpi_indicators.each do |kpi_indicator|

          fila = data[index]

          ini_pos = 4

          dt_i.each do |fecha|

            ini_pos += 1

          end

          index += 1
        end

      end

    end

    def save_data(archivo_temporal, kpi_dashboard)

      dt_i = Array.new
      dt_f = Array.new

      prev_month = nil

      (kpi_dashboard.from..kpi_dashboard.to).each do |date|

        unless prev_month == date.strftime('%m').to_i

          prev_month = date.strftime('%m').to_i

          dt_i.push date
          dt_f.push date + 1.month

        end

      end

      archivo = RubyXL::Parser.parse archivo_temporal

      # update goals

      data = archivo.worksheets[1].extract_data

      index = 0

      (1..3).each { index += 1 }

      kpi_dashboard.kpi_sets.each do |kpi_set|
        index += 1

        kpi_indicators = kpi_dashboard.allow_subsets? ? kpi_set.kpi_indicators_ordered_by_subset : kpi_set.kpi_indicators

        kpi_indicators.each do |kpi_indicator|

          fila = data[index]

          ini_pos = 4

          dt_i.each do |fecha|

            kpi_indicator_detail = KpiIndicatorDetail.where('kpi_indicator_id = ? AND measured_at = ?',kpi_indicator.id, fecha).first

            unless kpi_indicator_detail
              kpi_indicator_detail = KpiIndicatorDetail.new
              kpi_indicator_detail.measured_at = fecha
              kpi_indicator_detail.kpi_indicator = kpi_indicator
              kpi_indicator_detail.save
            end

            if kpi_indicator.indicator_type == 4

              kpi_indicator_detail.kpi_indicator_ranks.destroy_all

              if fila && fila[ini_pos]

                ranks = fila[ini_pos].split('|')

                ranks.each do |rank|

                  gp = rank.split(':')

                  r = kpi_indicator_detail.kpi_indicator_ranks.build
                  r.goal = gp[0].to_f
                  r.percentage = gp[1].to_f
                  r.save

                end

              end

              kpi_indicator_detail.reload
              kpi_indicator_detail.set_goal_rank
              kpi_indicator_detail.save

            else

              if fila && fila[ini_pos].is_a?(Numeric)

                kpi_indicator_detail.goal = fila[ini_pos]
                kpi_indicator_detail.save

              end

            end

            kpi_indicator_detail.reload

            kpi_measurement = kpi_indicator_detail.kpi_indicator.kpi_measurements.where('measured_at = ?',kpi_indicator_detail.measured_at).first

            if kpi_measurement && kpi_measurement.save

              kpi_set = kpi_indicator_detail.kpi_indicator.kpi_set
              kpi_set.set_last_updates lms_time
              kpi_set.save

              if kpi_indicator_detail.kpi_indicator.kpi_subset

                kpi_subset = kpi_indicator_detail.kpi_indicator.kpi_subset
                kpi_subset.set_last_updates lms_time
                kpi_subset.save

              end

            end

            ini_pos += 1

          end

          if kpi_dashboard.show_indicator_final_result && kpi_indicator.final_result_method == 3

            if kpi_indicator.indicator_type == 4

              kpi_indicator.kpi_indicator_ranks.destroy_all

              if fila && fila[ini_pos]

                ranks = fila[ini_pos].split('|')

                ranks.each do |rank|

                  gp = rank.split(':')

                  r = kpi_indicator.kpi_indicator_ranks.build
                  r.goal = gp[0].to_f
                  r.percentage = gp[1].to_f
                  r.save

                end

              end

              kpi_indicator.reload
              kpi_indicator.set_goal_rank
              kpi_indicator.save

            else

              if fila && fila[ini_pos].is_a?(Numeric)

                kpi_indicator.goal = fila[ini_pos]
                kpi_indicator.save

              end

            end

          end

          index += 1
        end

      end

      #update measurements

      data = archivo.worksheets[0].extract_data

      index = 0

      (1..3).each { index += 1 }

      kpi_dashboard.reload

      kpi_dashboard.kpi_sets.each do |kpi_set|
        index += 1

        kpi_indicators = kpi_dashboard.allow_subsets? ? kpi_set.kpi_indicators_ordered_by_subset : kpi_set.kpi_indicators

        kpi_indicators.each do |kpi_indicator|

          fila = data[index]

          ini_pos = 4

          dt_i.each do |fecha|

            KpiMeasurement.where('kpi_indicator_id = ? AND measured_at = ?',kpi_indicator.id, fecha).destroy_all

            if fila && fila[ini_pos].is_a?(Numeric)

              kpi_measurement = KpiMeasurement.new
              kpi_measurement.measured_at = fecha
              kpi_measurement.value = fila[ini_pos]
              kpi_measurement.kpi_indicator = kpi_indicator
              kpi_measurement.save

            end

            ini_pos += 1

          end

          if kpi_dashboard.show_indicator_final_result && kpi_indicator.final_result_method == 3

            if fila && fila[ini_pos].is_a?(Numeric)

              kpi_indicator.value = fila[ini_pos]
              kpi_indicator.save

            end

          end

          kpi_indicator.set_final_result
          kpi_indicator.save

          index += 1
        end


      end

      kpi_dashboard.kpi_sets.each do |kpi_set|

        dt_i.each do |fecha|

          KpiManagingController.helpers.update_set_measurement(kpi_set, fecha)

          kpi_set.kpi_subsets.each do |kpi_subset|

            KpiManagingController.helpers.update_subset_measurement(kpi_subset, fecha)

          end

        end

        kpi_set.set_percentage

        kpi_set.save

        kpi_set.kpi_subsets.each do |kpi_subset|

          kpi_subset.set_percentage

          kpi_subset.save

        end

      end

    end


end
