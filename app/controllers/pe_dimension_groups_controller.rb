class PeDimensionGroupsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_dimension_group = PeDimensionGroup.new


  end

  def create
    @pe_dimension_group = PeDimensionGroup.new(params[:pe_dimension_group])

    if @pe_dimension_group.save
      redirect_to pe_process_tab_path @pe_dimension_group.pe_dimension.pe_process, 'structure'
    else
      @pe_process = @pe_dimension_group.pe_dimension.pe_process
      render action: 'new'
    end
  end

  def edit
    @pe_dimension_group = PeDimensionGroup.find(params[:id])
    @pe_process = @pe_dimension_group.pe_dimension.pe_process
  end


  def update
    @pe_dimension_group = PeDimensionGroup.find(params[:id])

    if @pe_dimension_group.update_attributes(params[:pe_dimension_group])
      redirect_to pe_process_tab_path @pe_dimension_group.pe_dimension.pe_process, 'structure'
    else
      @pe_process = @pe_dimension_group.pe_dimension.pe_process
      render action: 'edit'
    end

  end


  def destroy
    @pe_dimension_group = PeDimensionGroup.find(params[:id])
    @pe_dimension_group.destroy

    respond_to do |format|
      format.html { redirect_to pe_dimension_groups_url }
      format.json { head :no_content }
    end
  end
end
