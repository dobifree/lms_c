class PeNewReportingsController < ApplicationController
  include PeNewReportingsHelper

  before_filter :get_data_2_4, only: [:rep_detailed_final_results_evaluation]

  before_filter :validate_manager_or_reporter, only: [:rep_detailed_final_results_evaluation]


  def rep_detailed_final_results_evaluation

    reporte_excel = rep_excel_final_results_evaluation(@pe_evaluation, @pe_process)
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal
    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'rep_detailed_final_results_evaluation.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end



  private

  def get_data_2_4
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
  end

  def validate_manager_or_reporter
    unless @pe_process.pe_managers.where('user_id = ?', user_connected.id).count > 0 || @pe_process.pe_reporters.where('user_id = ?', user_connected.id).count > 0
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

end
