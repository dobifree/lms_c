class DncRequirementsController < ApplicationController
  # GET /dnc_requirements
  # GET /dnc_requirements.json
  def index
    @dnc_requirements = DncRequirement.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dnc_requirements }
    end
  end

  # GET /dnc_requirements/1
  # GET /dnc_requirements/1.json
  def show
    @dnc_requirement = DncRequirement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dnc_requirement }
    end
  end

  # GET /dnc_requirements/new
  # GET /dnc_requirements/new.json
  def new
    @dnc_requirement = DncRequirement.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dnc_requirement }
    end
  end

  # GET /dnc_requirements/1/edit
  def edit
    @dnc_requirement = DncRequirement.find(params[:id])
  end

  # POST /dnc_requirements
  # POST /dnc_requirements.json
  def create
    @dnc_requirement = DncRequirement.new(params[:dnc_requirement])

    respond_to do |format|
      if @dnc_requirement.save
        format.html { redirect_to @dnc_requirement, notice: 'Dnc requirement was successfully created.' }
        format.json { render json: @dnc_requirement, status: :created, location: @dnc_requirement }
      else
        format.html { render action: "new" }
        format.json { render json: @dnc_requirement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dnc_requirements/1
  # PUT /dnc_requirements/1.json
  def update
    @dnc_requirement = DncRequirement.find(params[:id])

    respond_to do |format|
      if @dnc_requirement.update_attributes(params[:dnc_requirement])
        format.html { redirect_to @dnc_requirement, notice: 'Dnc requirement was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dnc_requirement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dnc_requirements/1
  # DELETE /dnc_requirements/1.json
  def destroy
    @dnc_requirement = DncRequirement.find(params[:id])
    @dnc_requirement.destroy

    respond_to do |format|
      format.html { redirect_to dnc_requirements_url }
      format.json { head :no_content }
    end
  end
end
