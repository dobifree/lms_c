class LmsManagingExecuteController < ApplicationController

  include UsersModule
  include LmsManagingDefineModule
  include LmsManagingExecuteModule

  include LmsManagingExecuteHelper

  before_filter :authenticate_user

  before_filter :verify_access_manage_module_enroll, only: [:massive_charge_enrollments, :massive_charge_enrollments_download_format, :enroll, :enroll_1_people,
                                                            :remove_user_from_list, :enroll_2_courses, :remove_course_from_list, :enroll_3_dates, :save_dates_to_course,
                                                            :enroll_4_confirm, :enroll_5_enroll,
                                                            :pci_list_courses, :pci_program_course,
                                                            :program_course_instance_new, :program_course_instance_create, :program_course_instance_destroy,
                                                            :program_course_instance_edit_dates, :program_course_instance_update_dates,
                                                            :program_course_instance_search_add_manager, :program_course_instance_add_manager, :program_course_instance_remove_manager,
                                                            :enrollment_list_courses, :enrollment_program_course,
                                                            :enrollment_enroll,
                                                            :enrollment_delete_enrollments, :enrollment_destroy_enrollments, :enrollment_list,
                                                            :enroll_from_instances_1_people, :enroll_from_instances_2_confirm, :enroll_from_instances_3_enroll,
                                                            :massive_charge_enrollments_from_instances, :massive_charge_enrollments_from_instances_download_format,
                                                            :remove_user_from_instance_list]

  before_filter :verify_access_manage_module_manage_enroll, only: [:results_list_courses,
                                                                   :program_course_instance_register_results_download_format, :program_course_instance_register_results_massive_charge,
                                                                   :program_course_instance_register_results,
                                                                   :program_course_instance_register_results_unit, :program_course_instance_register_results_unit_register,
                                                                   :program_course_instance_register_results_evaluation, :program_course_instance_register_results_evaluation_register,
                                                                   :program_course_instance_register_results_poll, :program_course_instance_register_results_poll_register,
                                                                   :program_course_instance_register_attendance, :program_course_instance_register_attendance_register,
                                                                   :program_course_instance_register_attendance2, :program_course_instance_register_attendance_register2]

  def massive_charge_enrollments

   #@company = session[:company]

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        massive_charge_enrollments_from_excel archivo_temporal

        if @lineas_error.size == 0
          flash[:success] = t('activerecord.success.model.lms_managing.update_file_ok')
          redirect_to lms_managing_execute_massive_charge_enrollments_path
        else
          flash.now[:success] = t('activerecord.success.model.lms_managing.update_file_ok_with_errors')
        end

      else

        flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_file')
    end

  end

  def massive_charge_enrollments_download_format

   #@company = session[:company]

    reporte_excel = massive_charge_enrollments_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_carga_matriculas.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end


  def massive_charge_enrollments_from_instances

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])

    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        massive_charge_enrollments_from_instances_from_excel archivo_temporal, @program_course_instance

        if @lineas_error.size == 0
          flash[:success] = t('activerecord.success.model.lms_managing.update_file_ok')
          redirect_to lms_managing_execute_enrollment_enroll_pill_open_path(@program_course_instance.id, 'users')
        else
          flash.now[:success] = t('activerecord.success.model.lms_managing.update_file_ok_with_errors')
        end

      else

        flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_file')
    end

  end

  def massive_charge_enrollments_from_instances_download_format

   #@company = session[:company]

    reporte_excel = massive_charge_enrollments_from_instances_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_carga_matriculas.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end



  def enroll
    if params[:pill_open]
      @pill_open = params[:pill_open]
    end

  end

  def enroll_1_people

    @lms_characteristics = LmsCharacteristic.search_people

    @characteristics = @lms_characteristics.each.map { |lms_c| lms_c.characteristic }
    @characteristics_prev = {}

    @characteristics.each do |characteristic|

      @characteristics_prev[characteristic.id] = ''

    end

    @characteristics = @lms_characteristics.each.map { |lms_c| lms_c.characteristic }
    @characteristics_prev = {}
    queries = []

    n_q = 0

    if params[:characteristic]
      @characteristics.each do |characteristic|

        if params[:characteristic][characteristic.id.to_s.to_sym] != ''

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND '

          queries[n_q] += 'valor = "'+params[:characteristic][characteristic.id.to_s.to_sym] +'" '

          n_q += 1

        end

      end
    end


    @user = User.new
    @users = {}

    if params[:user]
      #@users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre]).where('id NOT IN (?)', lms_list_users_to_enroll.nil? ? 0 : lms_list_users_to_enroll)
      @user.codigo = params[:user][:codigo]
      @user.apellidos = params[:user][:apellidos]
      @user.nombre = params[:user][:nombre]

      if queries.length > 0

        queries.each_with_index do |query, index|

          if index == 0

            @users = User.joins(:user_characteristics).where(
                'activo = ? AND users.id NOT IN (?) AND codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query, true,
                lms_list_users_to_enroll.nil? ? 0 : lms_list_users_to_enroll,
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

          else

            lista_users_ids = @users.map { |u| u.id }

            @users = User.joins(:user_characteristics).where(
                'activo = ? AND users.id NOT IN (?) AND codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query+' AND users.id IN (?) ', true,
                lms_list_users_to_enroll.nil? ? 0 : lms_list_users_to_enroll,
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = User.where(
            'activo = ? AND users.id NOT IN (?) AND codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ', true,
            lms_list_users_to_enroll.nil? ? 0 : lms_list_users_to_enroll,
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

      end

    end

    @selected_users = Array.new

    if params[:add_user_id]
      lms_add_users_to_enroll_list(params[:add_user_id])
    end

    if lms_list_users_to_enroll
      @selected_users = User.where(:id => lms_list_users_to_enroll)
    end


    render :layout => false
  end

  def enroll_from_instances_1_people

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])

    @lms_characteristics = LmsCharacteristic.search_people

    @characteristics = @lms_characteristics.each.map { |lms_c| lms_c.characteristic }
    @characteristics_prev = {}

    @characteristics.each do |characteristic|

      @characteristics_prev[characteristic.id] = ''

    end

    @characteristics = @lms_characteristics.each.map { |lms_c| lms_c.characteristic }
    @characteristics_prev = {}
    queries = []

    n_q = 0

    if params[:characteristic]
      @characteristics.each do |characteristic|

        if params[:characteristic][characteristic.id.to_s.to_sym] != ''

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND '

          queries[n_q] += 'valor = "'+params[:characteristic][characteristic.id.to_s.to_sym] +'" '

          n_q += 1

        end

      end
    end


    @user = User.new
    @users = {}

    if params[:user]
      #@users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre]).where('id NOT IN (?)', lms_list_users_to_enroll.nil? ? 0 : lms_list_users_to_enroll)
      @user.codigo = params[:user][:codigo]
      @user.apellidos = params[:user][:apellidos]
      @user.nombre = params[:user][:nombre]

      if queries.length > 0

        queries.each_with_index do |query, index|

          if index == 0

            @users = User.joins(:user_characteristics).where(
                'activo = ? AND users.id NOT IN (?) AND codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query, true,
                lms_list_users_to_enroll.nil? ? 0 : lms_list_users_to_enroll,
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

          else

            lista_users_ids = @users.map { |u| u.id }

            @users = User.joins(:user_characteristics).where(
                'activo = ? AND users.id NOT IN (?) AND codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query+' AND users.id IN (?) ', true,
                lms_list_users_to_enroll.nil? ? 0 : lms_list_users_to_enroll,
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = User.where(
            'activo = ? AND users.id NOT IN (?) AND codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ', true,
            lms_list_users_to_enroll.nil? ? 0 : lms_list_users_to_enroll,
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

      end

    end

    @selected_users = Array.new

    if params[:add_user_id]
      lms_add_users_to_enroll_from_instances_list(params[:add_user_id], @program_course_instance.id)
    end

    if lms_list_users_to_enroll_from_instances(@program_course_instance.id)
      @selected_users = User.where(:id => lms_list_users_to_enroll_from_instances(@program_course_instance.id))
    end


    render :layout => false
  end

  def remove_user_from_instance_list
    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
    lms_remove_user_from_enroll_from_instances_list(params[:user_id], @program_course_instance.id)
    render :json => true
  end

  def remove_user_from_list
    lms_remove_user_from_enroll_list(params[:user_id])
    render :json => true
  end

  def enroll_2_courses

   #@company = session[:company]

    @selected_program_courses = Array.new

    if params[:program_courses_id]
      lms_add_courses_to_enroll_list(params[:program_courses_id])
    end

    if lms_list_courses_to_enroll
      @selected_program_courses = ProgramCourse.where(:id => lms_list_courses_to_enroll)
    end

    @programs = Program.active_programs_managed_by_manager

    render :layout => false
  end

  def remove_course_from_list
    lms_remove_course_from_enroll_list(params[:program_course_id])
    render :json => true
  end

  def enroll_3_dates

   #@company = session[:company]

    @selected_program_courses = Array.new
    @desdehasta = Array.new
    @dates_by_element = Array.new
    @desdehasta_elements = Array.new
    complete_dates = true

    if lms_list_courses_to_enroll
      @selected_program_courses = ProgramCourse.where(:id => lms_list_courses_to_enroll)
      @courses_detail = lms_detail_courses_to_enroll
    end

    if lms_detail_courses_to_enroll
      @selected_program_courses.each_with_index do |program_course, index_pc|
        if lms_detail_courses_to_enroll[program_course.id.to_s]
          @desdehasta[index_pc] = lms_detail_courses_to_enroll[program_course.id.to_s]

          if lms_detail_courses_to_enroll[program_course.id.to_s]['dates_by_element'] == 1
            @dates_by_element[index_pc] = true
            @desdehasta_elements[index_pc] = lms_detail_courses_to_enroll[program_course.id.to_s]['desdehasta_elements']
          else
            @dates_by_element[index_pc] = false
          end
        end
      end
    end

    render :layout => false
  end

  def save_dates_to_course
    if program_course_id = params['program_course_id']
      program_course = ProgramCourse.find(params['program_course_id'])
      desdehasta = params['desdehasta_' + program_course_id]
      dates_by_element = params['dates_by_element_' + program_course_id]
      if dates_by_element == '1'
        desdehasta_elements = {}
        elements, elements_ids = program_course.course.ordered_elements
        elements.each_index do |index_el|
          ei = elements_ids[index_el].gsub('-', '_')
          field_name = 'desdehasta_'+program_course.id.to_s+'_'+ei
          desdehasta_elements[field_name] = params[field_name]
        end
      end
      lms_add_dates_to_course(program_course_id, desdehasta, dates_by_element, desdehasta_elements)
    end
    render :json => true
  end

  def enroll_4_confirm

   #@company = session[:company]

    @selected_users = User.where(:id => lms_list_users_to_enroll)
    @selected_program_courses = ProgramCourse.where(:id => lms_list_courses_to_enroll)

    @courses_detail = lms_detail_courses_to_enroll
    render :layout => false

  end

  def enroll_from_instances_2_confirm

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
   #@company = session[:company]

    @selected_users = User.where(:id => lms_list_users_to_enroll_from_instances(@program_course_instance.id))

    render :layout => false

  end

  def enroll_5_enroll

    if enrollment_is_ok?


      @users_cant_enroll = Array.new
      @program_courses_cant_enroll = Array.new
      @reasons_cant_enroll = Array.new


      selected_users = Array.new
      selected_program_courses = Array.new

      if lms_list_users_to_enroll
        lms_list_users_to_enroll.each do |user_id|
          selected_users.push User.find(user_id)
        end

      end

      if lms_list_courses_to_enroll
        lms_list_courses_to_enroll.each do |program_course_id|
          selected_program_courses.push ProgramCourse.find(program_course_id)
        end
      end

      selected_program_courses.each_with_index do |program_course, index_pc|

        program = program_course.program
        course = program_course.course

        desde_hasta = lms_detail_courses_to_enroll[program_course.id.to_s]['desdehasta'].split(' - ')
        from_date = DateTime.strptime(desde_hasta[0], '%d/%m/%Y %I:%M %p')
        to_date = DateTime.strptime(desde_hasta[1], '%d/%m/%Y %I:%M %p')

        program_course_instance = program_course.program_course_instance_by_dates(from_date, to_date)

        unless program_course_instance
          program_course_instance = program_course.program_course_instances.build
          program_course_instance.from_date = from_date
          program_course_instance.to_date = to_date
          program_course_instance = nil unless program_course_instance.save
        end

        if program_course_instance
          selected_users.each do |user|
            enrollment = program.enrollment user

            unless enrollment
              enrollment = program.enrollments.build
              enrollment.user = user
              enrollment.level = program.levels.first

              enrollment = nil unless enrollment.save
            end
            if enrollment

              forzar_matricula = false
              forzar_matricula = true if params[:forzar_matricula] && params[:forzar_matricula][:aplica].to_s == '1'

              user_course_prev = enrollment.user_courses.where(:program_course_id => program_course.id, :anulado => false).order('id DESC').first
              estado_prev = nil
              estado_prev = user_course_prev.estado(lms_time) if user_course_prev

              matricular = false

              if forzar_matricula
                fd = params[:forzar_date].split('/').map { |s| s.to_i }
                fd = DateTime.new(fd[2], fd[1], fd[0], 0, 0, 0)
                if estado_prev
                  if estado_prev == :en_progreso || estado_prev == :no_iniciado
                    @reasons_cant_enroll.push 'Curso en progreso o no iniciado'
                  else
                    if estado_prev == :aprobado && user_course_prev.fin >= fd
                      @reasons_cant_enroll.push 'Curso no aprobado antes de '+fd.strftime('%d/%m/%Y')
                    else
                      matricular = true
                    end
                  end
                else
                  matricular = true
                end
                matricular = true unless estado_prev && (() || (estado_prev == :aprobado && user_course_prev.fin >= fd))
              else
                if estado_prev
                  if estado_prev == :aprobado || estado_prev == :en_progreso || estado_prev == :no_iniciado
                    @reasons_cant_enroll.push 'Curso aprobado, en progreso o no iniciado'
                  else
                    matricular = true
                  end
                else
                  matricular = true
                end
              end

              matricular = true

              if matricular
                user_course = program_course_instance.user_courses.where(:enrollment_id => enrollment.id, :desde => from_date, :hasta => to_date).first

                unless user_course

                  user_course = program_course_instance.user_courses.build

                  user_course.enrollment = enrollment
                  user_course.program_course = program_course
                  user_course.course = course
                  user_course.numero_oportunidad = 1
                  user_course.desde = from_date
                  user_course.hasta = to_date

                end
                if user_course.save
                  if lms_detail_courses_to_enroll[program_course.id.to_s]['dates_by_element'] == '1' #params['dates_by_element_'+program_course.id.to_s] == '1'
                    elements, elements_ids = program_course.course.ordered_elements

                    elements.each_with_index do |element, index_el|
                      ei = elements_ids[index_el].gsub('-', '_')
                      tipo = elements_ids[index_el].split('-')[0]
                      field_name = 'desdehasta_'+program_course.id.to_s+'_' + ei

                      desde_hasta = lms_detail_courses_to_enroll[program_course.id.to_s]['desdehasta_elements'][field_name].split(' - ') #params['desdehasta_'+program_course.id.to_s+'_'+tipo+'_'+element.id.to_s].split(' - ')
                      from_date_el = DateTime.strptime(desde_hasta[0], '%d/%m/%Y %I:%M %p')
                      to_date_el = DateTime.strptime(desde_hasta[1], '%d/%m/%Y %I:%M %p')

                      if tipo == 'u'
                        user_course_unit = user_course.user_course_units.build
                        user_course_unit.unit = element
                        user_course_unit.from_date = from_date_el
                        user_course_unit.to_date = to_date_el
                        user_course_unit.save
                      elsif tipo == 'e'
                        user_course_evaluation = user_course.user_course_evaluations.build
                        user_course_evaluation.evaluation = element
                        user_course_evaluation.from_date = from_date_el
                        user_course_evaluation.to_date = to_date_el
                        user_course_evaluation.save
                      elsif tipo == 'p'
                        user_course_poll = user_course.user_course_polls.build
                        user_course_poll.poll = element
                        user_course_poll.from_date = from_date_el
                        user_course_poll.to_date = to_date_el
                        user_course_poll.save
                      end
                    end
                  end
                end
              else
                @users_cant_enroll.push user
                @program_courses_cant_enroll.push program_course
              end
            end
          end
        end
      end

      lms_remove_enrollment_data

      flash[:success] = t('activerecord.success.model.enrollment.create_ok')

      render :json => true

    else
      flash[:danger] = 'Errores al procesar la matrícula'
      render :json => false
    end

  end


  def enroll_from_instances_3_enroll

    # usuarios preseleccionados para la matrícula

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
    users_to_enroll = lms_list_users_to_enroll_from_instances(@program_course_instance.id)

    if users_to_enroll && users_to_enroll.count > 0

      @users_cant_enroll = Array.new
      @reasons_cant_enroll = Array.new
      selected_users = Array.new

      users_to_enroll.each do |user_id|
        selected_users.push User.find(user_id)
      end

      program_course = @program_course_instance.program_course
      program = @program_course_instance.program_course.program
      course = @program_course_instance.program_course.course

      if @program_course_instance

        elements, elements_ids = program_course.course.ordered_elements

        selected_users.each do |user|
          enrollment = program.enrollment user

          unless enrollment
            enrollment = program.enrollments.build
            enrollment.user = user
            enrollment.level = program.levels.first

            enrollment = nil unless enrollment.save
          end
          if enrollment

            #forzar_matricula = false
            #forzar_matricula = true if params[:forzar_matricula] && params[:forzar_matricula][:aplica].to_s == '1'

            user_course_prev = enrollment.user_courses.where(:program_course_id => program_course.id, :anulado => false).order('id DESC').first
            estado_prev = nil
            estado_prev = user_course_prev.estado(lms_time) if user_course_prev

            matricular = false

            if false # forzar_matricula
              fd = params[:forzar_date].split('/').map { |s| s.to_i }
              fd = DateTime.new(fd[2], fd[1], fd[0], 0, 0, 0)
              if estado_prev
                if estado_prev == :en_progreso || estado_prev == :no_iniciado
                  @reasons_cant_enroll.push 'Curso en progreso o no iniciado'
                else
                  if estado_prev == :aprobado && user_course_prev.fin >= fd
                    @reasons_cant_enroll.push 'Curso no aprobado antes de '+fd.strftime('%d/%m/%Y')
                  else
                    matricular = true
                  end
                end
              else
                matricular = true
              end
              matricular = true unless estado_prev && (() || (estado_prev == :aprobado && user_course_prev.fin >= fd))
            else
              if estado_prev
                if estado_prev == :aprobado || estado_prev == :en_progreso || estado_prev == :no_iniciado
                  @reasons_cant_enroll.push 'Curso aprobado, en progreso o no iniciado'
                else
                  matricular = true
                end
              else
                matricular = true
              end
            end

            matricular = true

            if matricular
              user_course = @program_course_instance.user_courses.where(:enrollment_id => enrollment.id, :desde => @program_course_instance.from_date, :hasta => @program_course_instance.to_date).first
              unless user_course

                user_course = @program_course_instance.user_courses.build

                user_course.enrollment = enrollment
                user_course.program_course = @program_course_instance.program_course
                user_course.course = course
                user_course.numero_oportunidad = 1
                user_course.desde = @program_course_instance.from_date
                user_course.hasta = @program_course_instance.to_date

              end
              if user_course.save
                # en este IF empieza el problema, ya está solucionado

                if @program_course_instance.program_course_instance_units.size > 0 || @program_course_instance.program_course_instance_evaluations.size > 0 || @program_course_instance.program_course_instance_polls.size > 0
                  elements.each_with_index do |element, index_el|
                    #ei = elements_ids[index_el].gsub('-', '_')
                    ei = elements_ids[index_el].split('-')[1]
                    tipo = elements_ids[index_el].split('-')[0]

                    if tipo == 'u'

                      program_course_instance_unit = @program_course_instance.program_course_instance_units.where('unit_id = ?',ei).first

                      if program_course_instance_unit

                        user_course_unit = user_course.user_course_units.build
                        user_course_unit.unit = element
                        user_course_unit.from_date = program_course_instance_unit.from_date
                        user_course_unit.to_date = program_course_instance_unit.to_date
                        user_course_unit.save

                      end

                    elsif tipo == 'e'

                      program_course_instance_evaluation = @program_course_instance.program_course_instance_evaluations.where('evaluation_id = ?',ei).first

                      if program_course_instance_evaluation

                        user_course_evaluation = user_course.user_course_evaluations.build
                        user_course_evaluation.evaluation = element
                        user_course_evaluation.from_date = program_course_instance_evaluation.from_date
                        user_course_evaluation.to_date = program_course_instance_evaluation.to_date
                        user_course_evaluation.save

                      end

                    elsif tipo == 'p'

                      program_course_instance_poll = @program_course_instance.program_course_instance_polls.where('poll_id = ?',ei).first

                      if program_course_instance_poll

                        user_course_poll = user_course.user_course_polls.build
                        user_course_poll.poll = element
                        user_course_poll.from_date = program_course_instance_poll.from_date
                        user_course_poll.to_date = program_course_instance_poll.to_date
                        user_course_poll.save

                      end

                    end
                  end
                end
              end
            else
              @users_cant_enroll.push user
              @program_courses_cant_enroll.push program_course
            end
          end
        end
      end

      lms_remove_enrollment_from_instances_data(@program_course_instance.id)

      flash[:success] = t('activerecord.success.model.enrollment.create_ok')

      render :json => true

    else
      flash[:danger] = 'Errores al procesar la matrícula'
      render :json => false

    end

  end

  #programar dictado

  def pci_list_courses

   #@company = session[:company]
    @programs = Program.active_programs_managed_by_manager

  end

  def pci_program_course

   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course

    @current_user_courses = @program_course.matriculas_vigentes
    @elements, @elements_ids = @course.ordered_elements

  end

  def program_course_instance_new

   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course

    @program_course_instance = @program_course.program_course_instances.build

    @program_course_instance.from_date = lms_time
    @program_course_instance.to_date = lms_time

    @elements, @elements_ids = @course.ordered_elements

    @dates_by_element = true
    @desdehasta_elements = Array.new

    @elements.each_with_index do |element, index_el|
      @desdehasta_elements[index_el] = @program_course_instance.from_date.strftime('%d/%m/%Y 08:00 AM')+' - '+@program_course_instance.to_date.strftime('%d/%m/%Y 09:00 AM')
    end

  end

  def program_course_instance_create

   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course

    @program_course_instance = @program_course.program_course_instances.build
    @program_course_instance.description = params[:program_course_instance][:description]
    @program_course_instance.from_date = lms_time
    @program_course_instance.to_date = lms_time

    @dates_by_element = false
    @desdehasta_elements = Array.new

    complete_dates = true

    @elements, @elements_ids = @course.ordered_elements

    if params['dates_by_element'] == '1'

      @dates_by_element = true

      @elements.each_with_index do |element, index_el|

        ei = @elements_ids[index_el].split('-')
        tipo = ei[0]

        if params['desdehasta_'+tipo+'_'+element.id.to_s].blank?
          complete_dates = false
        else
          @desdehasta_elements[index_el] = params['desdehasta_'+tipo+'_'+element.id.to_s]
        end

      end

    end

    if complete_dates


      #program_course_instance = @program_course.program_course_instance_by_dates(from_date, to_date)

      #if program_course_instance

      #  flash.now[:danger] = t('activerecord.error.model.lms_managing.enroll_repeated_complete_dates')
      #  render 'program_course_instance_new'

      #else

      elements, elements_ids = @course.ordered_elements

      if params[:dates_by_element] == '1'

        min_from = nil
        max_to = nil

        elements.each_with_index do |element, index_el|
          ei = elements_ids[index_el].gsub('-', '_')
          tipo = elements_ids[index_el].split('-')[0]
          field_name = 'desdehasta_' + ei

          desde_hasta = params[field_name].split(' - ')
          from_date_el = DateTime.strptime(desde_hasta[0], '%d/%m/%Y %I:%M %p')
          to_date_el = DateTime.strptime(desde_hasta[1], '%d/%m/%Y %I:%M %p')

          if tipo == 'u' || tipo == 'e'

            if index_el == 0
              min_from = from_date_el
              max_to = to_date_el
            else
              min_from = from_date_el if from_date_el < min_from
              max_to = to_date_el if to_date_el > max_to
            end

          end

        end

        @program_course_instance.from_date = min_from
        @program_course_instance.to_date = max_to

      else

        desde_hasta = params['desdehasta'].split(' - ')

        desde = desde_hasta[0].split(' ')
        desde_d = desde[0].split('/').map { |s| s }
        desde_h = desde[1].split(':').map { |s| s }

        from_date = DateTime.parse(desde_d[2]+'-'+desde_d[1]+'-'+desde_d[0]+' '+desde_h[0]+':'+desde_h[1]+':00 '+desde[2])

        hasta = desde_hasta[1].split(' ')
        hasta_d = hasta[0].split('/').map { |s| s }
        hasta_h = hasta[1].split(':').map { |s| s }

        to_date = DateTime.parse(hasta_d[2]+'-'+hasta_d[1]+'-'+hasta_d[0]+' '+hasta_h[0]+':'+hasta_h[1]+':00 '+hasta[2])

        @program_course_instance.from_date = from_date
        @program_course_instance.to_date = to_date

      end

      @program_course_instance.creator = user_connected

      if @program_course_instance.save

        TrainingCharacteristic.for_instance.each do |training_characteristic|

          ctc = @program_course_instance.course_training_characteristic(training_characteristic)

          unless ctc

            ctc = @program_course_instance.course_training_characteristics.build
            ctc.training_characteristic = training_characteristic

          end

          if training_characteristic.data_type_id == 0
            ctc.value_string = params['training_characteristic_'+training_characteristic.id.to_s]
          elsif training_characteristic.data_type_id == 1
            ctc.value_int = params['training_characteristic_'+training_characteristic.id.to_s]
          elsif training_characteristic.data_type_id == 2
            ctc.training_characteristic_value_id = params['training_characteristic_'+training_characteristic.id.to_s]
          end

          ctc.save

        end

        if params[:dates_by_element] == '1'

          elements.each_with_index do |element, index_el|
            ei = elements_ids[index_el].gsub('-', '_')
            tipo = elements_ids[index_el].split('-')[0]
            field_name = 'desdehasta_' + ei

            desde_hasta = params[field_name].split(' - ')
            from_date_el = DateTime.strptime(desde_hasta[0], '%d/%m/%Y %I:%M %p')
            to_date_el = DateTime.strptime(desde_hasta[1], '%d/%m/%Y %I:%M %p')

            if tipo == 'u'
              program_course_instance_unit = @program_course_instance.program_course_instance_units.build
              program_course_instance_unit.unit = element
              program_course_instance_unit.from_date = from_date_el
              program_course_instance_unit.to_date = to_date_el
              program_course_instance_unit.save
            elsif tipo == 'e'
              program_course_instance_evaluation = @program_course_instance.program_course_instance_evaluations.build
              program_course_instance_evaluation.evaluation = element
              program_course_instance_evaluation.from_date = from_date_el
              program_course_instance_evaluation.to_date = to_date_el
              program_course_instance_evaluation.save
            elsif tipo == 'p'
              program_course_instance_poll = @program_course_instance.program_course_instance_polls.build
              program_course_instance_poll.poll = element
              program_course_instance_poll.from_date = from_date_el
              program_course_instance_poll.to_date = to_date_el
              program_course_instance_poll.save
            end
          end

        end

        flash[:success] = t('activerecord.success.model.lms_managing.create_pci_ok')
        redirect_to lms_managing_execute_pci_program_course_path @program_course

      else

        flash.now[:danger] = t('activerecord.error.model.lms_managing.update_pci_dates_error')
        render 'program_course_instance_new'

      end

      #end
    else
      flash.now[:danger] = t('activerecord.error.model.lms_managing.enroll_not_complete_dates')
      render 'program_course_instance_new'
    end


  end

  def program_course_instance_destroy

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course

    if @program_course_instance.user_courses.size == 0
      flash[:success] = t('activerecord.success.model.lms_managing.destroy_pci_ok')

      @program_course_instance.destroy

    else

    end

    redirect_to lms_managing_execute_pci_program_course_path @program_course

  end

  def program_course_instance_search_add_manager

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    @elements, @elements_ids = @course.ordered_elements

    @dates_by_element = false
    @desdehasta_elements = Array.new

    user_course = @program_course_instance.user_courses.first

    if @program_course_instance.program_course_instance_units.size > 0 || @program_course_instance.program_course_instance_evaluations.size > 0 || @program_course_instance.program_course_instance_polls.size > 0

      @elements.each_with_index do |element, index|

        ei = @elements_ids[index].split('-')
        tipo = ei[0]

        if tipo == 'u'

          program_course_instance_unit = @program_course_instance.program_course_instance_units.where('unit_id = ?', element.id).first

          if program_course_instance_unit && program_course_instance_unit.from_date && program_course_instance_unit.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = program_course_instance_unit.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+program_course_instance_unit.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        elsif tipo == 'e'

          program_course_instance_evaluation = @program_course_instance.program_course_instance_evaluations.where('evaluation_id = ?', element.id).first

          if program_course_instance_evaluation && program_course_instance_evaluation.from_date && program_course_instance_evaluation.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = program_course_instance_evaluation.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+program_course_instance_evaluation.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        elsif tipo == 'p'

          program_course_instance_poll = @program_course_instance.program_course_instance_polls.where('poll_id = ?', element.id).first

          if program_course_instance_poll && program_course_instance_poll.from_date && program_course_instance_poll.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = program_course_instance_poll.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+program_course_instance_poll.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        end

      end

    elsif user_course

      @elements.each_with_index do |element, index|

        ei = @elements_ids[index].split('-')
        tipo = ei[0]

        if tipo == 'u'

          user_course_unit = user_course.user_course_units.where('unit_id = ?', element.id).first

          if user_course_unit && user_course_unit.from_date && user_course_unit.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = user_course_unit.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+user_course_unit.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        elsif tipo == 'e'

          user_course_evaluation = user_course.user_course_evaluations.where('evaluation_id = ?', element.id).first

          if user_course_evaluation && user_course_evaluation.from_date && user_course_evaluation.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = user_course_evaluation.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+user_course_evaluation.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        elsif tipo == 'p'

          user_course_poll = user_course.user_course_polls.where('poll_id = ?', element.id).first

          if user_course_poll && user_course_poll.from_date && user_course_poll.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = user_course_poll.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+user_course_poll.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        end

      end

    end

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

    @users_q = User.users_with_lms_privileges

  end

  def program_course_instance_add_manager

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]

    pcim = @program_course_instance.program_course_instance_managers.build
    pcim.user_id = params[:user_id]

    @program_course_instance.save

    u = User.find params[:user_id]

    user_ct_module_privileges = u.user_ct_module_privileges
    unless user_ct_module_privileges
      user_ct_module_privileges = UserCtModulePrivileges.new
      user_ct_module_privileges.user = u
    end

    user_ct_module_privileges.lms_manage_enroll = true

    user_ct_module_privileges.save

    flash[:success] = t('activerecord.success.model.lms_managing.add_manager_pci_ok')
    redirect_to lms_managing_execute_pci_program_course_path(@program_course_instance.program_course)

  end

  def program_course_instance_remove_manager

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]

    pcim = @program_course_instance.program_course_instance_managers.where(:user_id => params[:user_id]).first

    pcim.destroy if pcim

    flash[:success] = t('activerecord.success.model.lms_managing.add_manager_pci_ok')
    redirect_to lms_managing_execute_pci_program_course_path(@program_course_instance.program_course)

  end

  def program_course_instance_edit_dates

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    @elements, @elements_ids = @course.ordered_elements

    @dates_by_element = false
    @desdehasta_elements = Array.new

    user_course = @program_course_instance.user_courses.first

    if @program_course_instance.program_course_instance_units.size > 0 || @program_course_instance.program_course_instance_evaluations.size > 0 || @program_course_instance.program_course_instance_polls.size > 0

      @elements.each_with_index do |element, index|

        ei = @elements_ids[index].split('-')
        tipo = ei[0]

        if tipo == 'u'

          program_course_instance_unit = @program_course_instance.program_course_instance_units.where('unit_id = ?', element.id).first

          if program_course_instance_unit && program_course_instance_unit.from_date && program_course_instance_unit.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = program_course_instance_unit.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+program_course_instance_unit.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        elsif tipo == 'e'

          program_course_instance_evaluation = @program_course_instance.program_course_instance_evaluations.where('evaluation_id = ?', element.id).first

          if program_course_instance_evaluation && program_course_instance_evaluation.from_date && program_course_instance_evaluation.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = program_course_instance_evaluation.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+program_course_instance_evaluation.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        elsif tipo == 'p'

          program_course_instance_poll = @program_course_instance.program_course_instance_polls.where('poll_id = ?', element.id).first

          if program_course_instance_poll && program_course_instance_poll.from_date && program_course_instance_poll.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = program_course_instance_poll.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+program_course_instance_poll.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        end

      end

    elsif user_course

      @elements.each_with_index do |element, index|

        ei = @elements_ids[index].split('-')
        tipo = ei[0]

        if tipo == 'u'

          user_course_unit = user_course.user_course_units.where('unit_id = ?', element.id).first

          if user_course_unit && user_course_unit.from_date && user_course_unit.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = user_course_unit.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+user_course_unit.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        elsif tipo == 'e'

          user_course_evaluation = user_course.user_course_evaluations.where('evaluation_id = ?', element.id).first

          if user_course_evaluation && user_course_evaluation.from_date && user_course_evaluation.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = user_course_evaluation.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+user_course_evaluation.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        elsif tipo == 'p'

          user_course_poll = user_course.user_course_polls.where('poll_id = ?', element.id).first

          if user_course_poll && user_course_poll.from_date && user_course_poll.to_date

            @dates_by_element = true

            @desdehasta_elements[index] = user_course_poll.from_date.strftime('%d/%m/%Y %I:%M %p')+' - '+user_course_poll.to_date.strftime('%d/%m/%Y %I:%M %p')

          else

            @desdehasta_elements[index] = nil

          end

        end

      end

    end

  end

  def program_course_instance_update_dates

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    @dates_by_element = false
    @desdehasta_elements = Array.new

    complete_dates = true

    if params['dates_by_element'] == '1'

      @dates_by_element = true

      @elements, @elements_ids = @course.ordered_elements

      @elements.each_with_index do |element, index_el|

        ei = @elements_ids[index_el].split('-')
        tipo = ei[0]

        if params['desdehasta_'+tipo+'_'+element.id.to_s].blank?
          complete_dates = false
        else
          @desdehasta_elements[index_el] = params['desdehasta_'+tipo+'_'+element.id.to_s]
        end

      end

    end

    if complete_dates

      elements, elements_ids = @course.ordered_elements

      if params[:dates_by_element] == '1'

        min_from = nil
        max_to = nil

        elements.each_with_index do |element, index_el|
          ei = elements_ids[index_el].gsub('-', '_')
          tipo = elements_ids[index_el].split('-')[0]
          field_name = 'desdehasta_' + ei

          desde_hasta = params[field_name].split(' - ')
          from_date_el = DateTime.strptime(desde_hasta[0], '%d/%m/%Y %I:%M %p')
          to_date_el = DateTime.strptime(desde_hasta[1], '%d/%m/%Y %I:%M %p')

          if tipo == 'u' || tipo == 'e'

            if index_el == 0
              min_from = from_date_el
              max_to = to_date_el
            else
              min_from = from_date_el if from_date_el < min_from
              max_to = to_date_el if to_date_el > max_to
            end

          end

        end

        @program_course_instance.from_date = min_from
        @program_course_instance.to_date = max_to

      else

        desde_hasta = params['desdehasta'].split(' - ')

        desde = desde_hasta[0].split(' ')
        desde_d = desde[0].split('/').map { |s| s }
        desde_h = desde[1].split(':').map { |s| s }

        from_date = DateTime.parse(desde_d[2]+'-'+desde_d[1]+'-'+desde_d[0]+' '+desde_h[0]+':'+desde_h[1]+':00 '+desde[2])

        hasta = desde_hasta[1].split(' ')
        hasta_d = hasta[0].split('/').map { |s| s }
        hasta_h = hasta[1].split(':').map { |s| s }

        to_date = DateTime.parse(hasta_d[2]+'-'+hasta_d[1]+'-'+hasta_d[0]+' '+hasta_h[0]+':'+hasta_h[1]+':00 '+hasta[2])

        @program_course_instance.from_date = from_date
        @program_course_instance.to_date = to_date

      end

      has_to_destroy_current_pci = false

      @program_course_instance.description = params[:program_course_instance][:description]
      user_courses = @program_course_instance.user_courses

      if @program_course_instance.save

        TrainingCharacteristic.for_instance.each do |training_characteristic|

          ctc = @program_course_instance.course_training_characteristic(training_characteristic)

          unless ctc

            ctc = @program_course_instance.course_training_characteristics.build
            ctc.training_characteristic = training_characteristic

          end

          if training_characteristic.data_type_id == 0
            ctc.value_string = params['training_characteristic_'+training_characteristic.id.to_s]
          elsif training_characteristic.data_type_id == 1
            ctc.value_int = params['training_characteristic_'+training_characteristic.id.to_s]
          elsif training_characteristic.data_type_id == 2
            ctc.training_characteristic_value_id = params['training_characteristic_'+training_characteristic.id.to_s]
          end

          ctc.save

        end

        elements, elements_ids = @program_course.course.ordered_elements

        @program_course_instance.program_course_instance_units.destroy_all
        @program_course_instance.program_course_instance_evaluations.destroy_all
        @program_course_instance.program_course_instance_polls.destroy_all

        if @dates_by_element

          elements.each_with_index do |element, index_el|
            ei = elements_ids[index_el].gsub('-', '_')
            tipo = elements_ids[index_el].split('-')[0]
            field_name = 'desdehasta_' + ei

            desde_hasta = params[field_name].split(' - ')
            from_date_el = DateTime.strptime(desde_hasta[0], '%d/%m/%Y %I:%M %p')
            to_date_el = DateTime.strptime(desde_hasta[1], '%d/%m/%Y %I:%M %p')

            if tipo == 'u'
              program_course_instance_unit = @program_course_instance.program_course_instance_units.build
              program_course_instance_unit.unit = element
              program_course_instance_unit.from_date = from_date_el
              program_course_instance_unit.to_date = to_date_el
              program_course_instance_unit.save
            elsif tipo == 'e'
              program_course_instance_evaluation = @program_course_instance.program_course_instance_evaluations.build
              program_course_instance_evaluation.evaluation = element
              program_course_instance_evaluation.from_date = from_date_el
              program_course_instance_evaluation.to_date = to_date_el
              program_course_instance_evaluation.save
            elsif tipo == 'p'
              program_course_instance_poll = @program_course_instance.program_course_instance_polls.build
              program_course_instance_poll.poll = element
              program_course_instance_poll.from_date = from_date_el
              program_course_instance_poll.to_date = to_date_el
              program_course_instance_poll.save
            end
          end

        end

        user_courses.each do |user_course|

          user_course.desde = @program_course_instance.from_date
          user_course.hasta = @program_course_instance.to_date

          if user_course.save

            if @dates_by_element

              elements.each_with_index do |element, index_el|

                ei = elements_ids[index_el].split('-')
                tipo = ei[0]

                desde_hasta = @desdehasta_elements[index_el].split(' - ')

                desde = desde_hasta[0].split(' ')
                desde_d = desde[0].split('/').map { |s| s }
                desde_h = desde[1].split(':').map { |s| s }
                from_date_el = DateTime.parse(desde_d[2]+'-'+desde_d[1]+'-'+desde_d[0]+' '+desde_h[0]+':'+desde_h[1]+':00 '+desde[2])

                hasta = desde_hasta[1].split(' ')
                hasta_d = hasta[0].split('/').map { |s| s }
                hasta_h = hasta[1].split(':').map { |s| s }
                to_date_el = DateTime.parse(hasta_d[2]+'-'+hasta_d[1]+'-'+hasta_d[0]+' '+hasta_h[0]+':'+hasta_h[1]+':59 '+hasta[2])

                if tipo == 'u'

                  user_course_unit = user_course.user_course_units.where('unit_id = ?', element.id).first

                  unless user_course_unit
                    user_course_unit = user_course.user_course_units.build
                    user_course_unit.unit = element
                  end

                  user_course_unit.from_date = from_date_el
                  user_course_unit.to_date = to_date_el
                  user_course_unit.save

                elsif tipo == 'e'

                  user_course_evaluation = user_course.user_course_evaluations.where('evaluation_id = ?', element.id).first

                  unless user_course_evaluation
                    user_course_evaluation = user_course.user_course_evaluations.build
                    user_course_evaluation.evaluation = element
                  end

                  user_course_evaluation.from_date = from_date_el
                  user_course_evaluation.to_date = to_date_el
                  user_course_evaluation.save

                elsif tipo == 'p'

                  user_course_poll = user_course.user_course_polls.where('poll_id = ?', element.id).first

                  unless user_course_poll
                    user_course_poll = user_course.user_course_polls.build
                    user_course_poll.poll = element
                  end

                  user_course_poll.from_date = from_date_el
                  user_course_poll.to_date = to_date_el
                  user_course_poll.save

                end


              end

            end

          end


        end

        flash[:success] = t('activerecord.success.model.lms_managing.update_pci_dates_ok')
        redirect_to lms_managing_execute_pci_edit_dates_path @program_course_instance

      else

        flash.now[:danger] = t('activerecord.error.model.lms_managing.update_pci_dates_error')
        render 'program_course_instance_edit_dates'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.lms_managing.enroll_not_complete_dates')
      render 'program_course_instance_edit_dates'
    end

  end

  #matrículas

  def enrollment_list_courses

   #@company = session[:company]
    @programs = Program.active_programs_managed_by_manager

  end

  def enrollment_program_course

   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course

  end

  def enrollment_enroll

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    if params[:pill_open]
    @pill_open = params[:pill_open]
    end


  end

  def enrollment_delete_enrollments

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

  end

  def enrollment_list

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    #@current_user_courses = @program_course.matriculas_vigentes
    @current_user_courses = @program_course_instance.user_courses.where(:anulado => false)

  end

  def enrollment_destroy_enrollments

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    if params[:user_courses]
      params[:user_courses].each do |uc_id|

        user_course = UserCourse.find uc_id

        if user_course

          user_course.destroy unless user_course.finalizado

        end

      end


    end

    flash[:success] = t('activerecord.success.model.lms_managing.destroy_pci_enrolls_ok')
    redirect_to lms_managing_execute_enrollment_delete_enrollments_path @program_course_instance

  end

  #asistencia y resultados

  def results_list_courses
   #@company = session[:company]
    @programs = Program.active_programs_managed_by_manager
  end

  def results_program_course
   #@company = session[:company]
    @program_course = ProgramCourse.find params[:program_course_id]
    @course = @program_course.course

    @pcis = user_connected.lms_manage_enroll ? @program_course.program_course_instances_by_creator_manager_ordered_by_date_desc(user_connected) : @program_course.program_course_instances_ordered_by_date_desc

    @current_user_courses = @program_course.matriculas_vigentes_instances @pcis

    #@current_user_courses = @program_course.matriculas_vigentes
    @elements, @elements_ids = @course.ordered_elements

  end

  def program_course_instance_register_results_download_format

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @elements, @elements_ids = @course.ordered_elements

    reporte_excel = massive_charge_results_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_registro_resultados.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def program_course_instance_register_results_massive_charge

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @elements, @elements_ids = @course.ordered_elements

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        massive_charge_results_from_excel archivo_temporal

        if @lineas_error.size == 0
          flash[:success] = t('activerecord.success.model.lms_managing.update_file_ok')
          redirect_to lms_managing_execute_pci_register_results_massive_charge_path @program_course_instance
        else
          flash.now[:success] = t('activerecord.success.model.lms_managing.update_file_ok_with_errors')
        end

      else

        flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_file')
    end

  end

  def program_course_instance_register_results

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @elements, @elements_ids = @course.ordered_elements

  end

  def program_course_instance_register_results_unit

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @unit = @course.units.where('units.id = ?', params[:unit_id]).first

  end

  def program_course_instance_register_results_unit_register

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @unit = @course.units.where('units.id = ?', params[:unit_id]).first

    @program_course_instance.matriculas_vigentes_read_only_false.each do |user_course|

      user_course_unit = user_course.user_course_units.where('unit_id = ?', @unit.id).first

      unless user_course_unit
        user_course_unit = user_course.user_course_units.build
        user_course_unit.unit = @unit
        user_course_unit.inicio = lms_time
      end

      if params['user_course_'+user_course.id.to_s] == '1'
        user_course_unit.inicio = lms_time
        user_course_unit.fin = lms_time
        user_course_unit.finalizada = true
      else
        user_course_unit.inicio = lms_time
        user_course_unit.fin = nil
        user_course_unit.finalizada = false
      end

      user_course_unit.save

      update_progress user_course
      update_user_course_status user_course

    end

    flash[:success] = t('activerecord.success.model.lms_managing.register_results_unit_ok')
    redirect_to lms_managing_execute_pci_register_results_unit_path(@program_course_instance, @unit)

  end

  def program_course_instance_register_attendance2
   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @elements, @elements_ids = @course.ordered_elements
  end

  def program_course_instance_register_attendance_register2

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    program = @program_course.program

    user = User.find_by_codigo params[:alumno]

    user_course_first = @program_course_instance.user_courses.first

    if user

      if user.activo

        user_course = @program_course_instance.user_course_by_user_read_only_false user

        unless user_course

          if @course.asistencia_matricula

            enrollment = program.enrollment user

            unless enrollment
              enrollment = program.enrollments.build
              enrollment.user = user
              enrollment.level = program.levels.first

              enrollment = nil unless enrollment.save
            end

            if enrollment

              forzar_matricula = false

              user_course_prev = enrollment.user_courses.where(:program_course_id => @program_course.id, :anulado => false).order('id DESC').first
              estado_prev = nil
              estado_prev = user_course_prev.estado(lms_time) if user_course_prev

              matricular = false

              if estado_prev
                if estado_prev == :aprobado || estado_prev == :en_progreso || estado_prev == :no_iniciado

                else
                  matricular = true
                end
              else
                matricular = true
              end

              matricular = true

              if matricular

                user_course = @program_course_instance.user_courses.build

                user_course.enrollment = enrollment
                user_course.program_course = @program_course
                user_course.course = @course
                user_course.numero_oportunidad = 1
                user_course.desde = @program_course_instance.from_date
                user_course.hasta = @program_course_instance.to_date

                user_course.enroll_np = true

                if user_course.save

                  elements, elements_ids = @program_course.course.ordered_elements

                  elements.each_with_index do |element, index_el|
                    ei = elements_ids[index_el].gsub('-', '_')
                    tipo = elements_ids[index_el].split('-')[0]

                    from_date_el = @program_course_instance.from_date
                    to_date_el = @program_course_instance.to_date

                    if tipo == 'u'

                      user_course_unit_first = user_course_first.user_course_units.where('unit_id = ?', element.id).first if user_course_first


                      if user_course_first && user_course_unit_first && user_course_unit_first.from_date && user_course_unit_first.to_date
                        user_course_unit = user_course.user_course_units.build
                        user_course_unit.unit = element
                        user_course_unit.from_date = user_course_unit_first.from_date
                        user_course_unit.to_date = user_course_unit_first.to_date
                        user_course_unit.save
                      end

                    elsif tipo == 'e'

                      user_course_evaluation_first = user_course_first.user_course_evaluations.where('evaluation_id = ?', element.id).first if user_course_first

                      if user_course_first && user_course_evaluation_first && user_course_evaluation_first.from_date && user_course_evaluation_first.to_date
                        user_course_evaluation = user_course.user_course_evaluations.build
                        user_course_evaluation.evaluation = element
                        user_course_evaluation.from_date = user_course_evaluation_first.from_date
                        user_course_evaluation.to_date = user_course_evaluation_first.to_date
                        user_course_evaluation.save
                      end

                    elsif tipo == 'p'

                      user_course_poll_first = user_course_first.user_course_polls.where('poll_id = ?', element.id).first if user_course_first

                      if user_course_first && user_course_poll_first && user_course_poll_first.from_date && user_course_poll_first.to_date
                        user_course_poll = user_course.user_course_polls.build
                        user_course_poll.poll = element
                        user_course_poll.from_date = user_course_poll_first.from_date
                        user_course_poll.to_date = user_course_poll_first.to_date
                        user_course_poll.save
                      end

                    end
                  end

                else


                end

              else
                flash[:danger] = t('activerecord.error.model.lms_managing.register_attendance_no_user')
              end
            end

          end

        end

        if user_course

          @elements, @elements_ids = @course.ordered_elements

          @elements.each_with_index do |element, index|

            unless element.virtual

              ei = @elements_ids[index].split('-')
              tipo = ei[0]
              id = ei[1]

              if tipo == 'u'

                user_course_unit = user_course.user_course_units.where('unit_id = ?', element.id).first

                unless user_course_unit
                  user_course_unit = user_course.user_course_units.build
                  user_course_unit.unit = element
                  user_course_unit.inicio = lms_time
                end


                user_course_unit.inicio = lms_time
                user_course_unit.fin = lms_time
                user_course_unit.finalizada = true


                user_course_unit.save

              end

            end

          end

          update_progress user_course
          update_user_course_status user_course

          flash[:success] = t('activerecord.success.model.lms_managing.register_attendance_ok')

        else

          flash[:danger] = t('activerecord.error.model.lms_managing.register_attendance_no_enrollment')

        end

      else

        flash[:danger] = t('activerecord.error.model.lms_managing.register_attendance_no_active_user')

      end

    else

      flash[:danger] = t('activerecord.error.model.lms_managing.register_attendance_no_user')

    end

    redirect_to lms_managing_execute_pci_register_attendance2_path(@program_course_instance)

  end


  def program_course_instance_unit_register_attendance2
   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @unit = Unit.find params[:unit_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
  end

  def program_course_instance_unit_register_attendance_register2

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @unit = Unit.find params[:unit_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    program = @program_course.program

    user = User.find_by_codigo params[:alumno]

    user_course_first = @program_course_instance.user_courses.first

    if user

      if user.activo

        user_course = @program_course_instance.user_course_by_user_read_only_false user

        unless user_course

          if @course.asistencia_matricula

            enrollment = program.enrollment user

            unless enrollment
              enrollment = program.enrollments.build
              enrollment.user = user
              enrollment.level = program.levels.first

              enrollment = nil unless enrollment.save
            end

            if enrollment

              forzar_matricula = false

              user_course_prev = enrollment.user_courses.where(:program_course_id => @program_course.id, :anulado => false).order('id DESC').first
              estado_prev = nil
              estado_prev = user_course_prev.estado(lms_time) if user_course_prev

              matricular = false

              if estado_prev
                if estado_prev == :aprobado || estado_prev == :en_progreso || estado_prev == :no_iniciado

                else
                  matricular = true
                end
              else
                matricular = true
              end

              matricular = true

              if matricular

                user_course = @program_course_instance.user_courses.build

                user_course.enrollment = enrollment
                user_course.program_course = @program_course
                user_course.course = @course
                user_course.numero_oportunidad = 1
                user_course.desde = @program_course_instance.from_date
                user_course.hasta = @program_course_instance.to_date

                user_course.enroll_np = true

                if user_course.save

                  elements, elements_ids = @program_course.course.ordered_elements

                  elements.each_with_index do |element, index_el|
                    ei = elements_ids[index_el].gsub('-', '_')
                    tipo = elements_ids[index_el].split('-')[0]

                    from_date_el = @program_course_instance.from_date
                    to_date_el = @program_course_instance.to_date

                    if tipo == 'u'

                      user_course_unit_first = user_course_first.user_course_units.where('unit_id = ?', element.id).first if user_course_first


                      if user_course_first && user_course_unit_first && user_course_unit_first.from_date && user_course_unit_first.to_date
                        user_course_unit = user_course.user_course_units.build
                        user_course_unit.unit = element
                        user_course_unit.from_date = user_course_unit_first.from_date
                        user_course_unit.to_date = user_course_unit_first.to_date
                        user_course_unit.save
                      end

                    elsif tipo == 'e'

                      user_course_evaluation_first = user_course_first.user_course_evaluations.where('evaluation_id = ?', element.id).first if user_course_first

                      if user_course_first && user_course_evaluation_first && user_course_evaluation_first.from_date && user_course_evaluation_first.to_date
                        user_course_evaluation = user_course.user_course_evaluations.build
                        user_course_evaluation.evaluation = element
                        user_course_evaluation.from_date = user_course_evaluation_first.from_date
                        user_course_evaluation.to_date = user_course_evaluation_first.to_date
                        user_course_evaluation.save
                      end

                    elsif tipo == 'p'

                      user_course_poll_first = user_course_first.user_course_polls.where('poll_id = ?', element.id).first if user_course_first

                      if user_course_first && user_course_poll_first && user_course_poll_first.from_date && user_course_poll_first.to_date
                        user_course_poll = user_course.user_course_polls.build
                        user_course_poll.poll = element
                        user_course_poll.from_date = user_course_poll_first.from_date
                        user_course_poll.to_date = user_course_poll_first.to_date
                        user_course_poll.save
                      end

                    end
                  end

                else


                end

              else
                flash[:danger] = t('activerecord.error.model.lms_managing.register_attendance_no_user')
              end
            end

          end

        end

        if user_course

          @elements, @elements_ids = @course.ordered_elements

          @elements.each_with_index do |element, index|

            unless element.virtual

              ei = @elements_ids[index].split('-')
              tipo = ei[0]
              id = ei[1]

              if tipo == 'u' && @unit.id == element.id

                user_course_unit = user_course.user_course_units.where('unit_id = ?', element.id).first

                unless user_course_unit
                  user_course_unit = user_course.user_course_units.build
                  user_course_unit.unit = element
                  user_course_unit.inicio = lms_time
                end


                user_course_unit.inicio = lms_time
                user_course_unit.fin = lms_time
                user_course_unit.finalizada = true


                user_course_unit.save

              end

            end

          end

          update_progress user_course
          update_user_course_status user_course

          flash[:success] = t('activerecord.success.model.lms_managing.register_attendance_ok')

        else

          flash[:danger] = t('activerecord.error.model.lms_managing.register_attendance_no_enrollment')

        end

      else
        flash[:danger] = t('activerecord.error.model.lms_managing.register_attendance_no_active_user')
      end


    else

      flash[:danger] = t('activerecord.error.model.lms_managing.register_attendance_no_user')

    end

    redirect_to lms_managing_execute_pci_unit_register_attendance2_path(@program_course_instance, @unit)

  end


  def program_course_instance_register_attendance
   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @elements, @elements_ids = @course.ordered_elements
  end

  def program_course_instance_register_attendance_register

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course

    @elements, @elements_ids = @course.ordered_elements

    @program_course_instance.matriculas_vigentes_read_only_false.each do |user_course|

      @elements.each_with_index do |element, index|

        unless element.virtual

          ei = @elements_ids[index].split('-')
          tipo = ei[0]
          id = ei[1]

          if tipo == 'u'

            user_course_unit = user_course.user_course_units.where('unit_id = ?', element.id).first

            unless user_course_unit
              user_course_unit = user_course.user_course_units.build
              user_course_unit.unit = element
              user_course_unit.inicio = lms_time
            end

            if params['user_course_'+user_course.id.to_s] == '1'
              user_course_unit.inicio = lms_time
              user_course_unit.fin = lms_time
              user_course_unit.finalizada = true
            elsif params['user_course_'+user_course.id.to_s] == '0'
              user_course_unit.inicio = lms_time
              user_course_unit.fin = nil
              user_course_unit.finalizada = false
            else
              user_course_unit.inicio = nil
              user_course_unit.fin = nil
              user_course_unit.finalizada = false
            end

            user_course_unit.save

          end


        end

      end

      update_progress user_course
      update_user_course_status user_course

    end

    flash[:success] = t('activerecord.success.model.lms_managing.register_results_unit_ok')
    redirect_to lms_managing_execute_pci_register_attendance_path(@program_course_instance)

  end

  def program_course_instance_register_results_evaluation

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @evaluation = @course.evaluations.where('evaluations.id = ?', params[:evaluation_id]).first

  end

  def program_course_instance_register_results_evaluation_register

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @evaluation = @course.evaluations.where('evaluations.id = ?', params[:evaluation_id]).first

    @program_course_instance.matriculas_vigentes_read_only_false.each do |user_course|

      user_course_evaluation = user_course.user_course_evaluations.where('evaluation_id = ?', @evaluation.id).first

      unless user_course_evaluation
        user_course_evaluation = user_course.user_course_evaluations.build
        user_course_evaluation.evaluation = @evaluation
      end

      if params['user_course_'+user_course.id.to_s].blank?

        user_course_evaluation.inicio = nil
        user_course_evaluation.fin = nil
        user_course_evaluation.ultimo_acceso = nil

        user_course_evaluation.finalizada = false
        user_course_evaluation.nota = nil
        user_course_evaluation.aprobada = nil

      else

        user_course_evaluation.inicio = lms_time
        user_course_evaluation.fin = lms_time
        user_course_evaluation.ultimo_acceso = lms_time

        user_course_evaluation.finalizada = true
        user_course_evaluation.nota = params['user_course_'+user_course.id.to_s]

        if user_course_evaluation.nota >= user_course_evaluation.evaluation.course.nota_minima
          user_course_evaluation.aprobada = true
        else
          user_course_evaluation.aprobada = false
        end

      end

      user_course_evaluation.save

      update_progress user_course

      update_user_course_status user_course

    end

    flash[:success] = t('activerecord.success.model.lms_managing.register_results_evaluation_ok')
    redirect_to lms_managing_execute_pci_register_results_evaluation_path(@program_course_instance, @evaluation)

  end


  def program_course_instance_register_results_poll

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @poll = @course.polls.where('polls.id = ?', params[:poll_id]).first

  end

  def program_course_instance_register_results_poll_register

   #@company = session[:company]
    @program_course_instance = ProgramCourseInstance.find params[:program_course_instance_id]
    @program_course = @program_course_instance.program_course
    @course = @program_course.course
    @poll = @course.polls.where('polls.id = ?', params[:poll_id]).first

    master_questions = @poll.master_poll.master_poll_questions

    master_questions.each do |master_question|

      master_question.master_poll_alternatives.each do |alt|

        poll_summary = ProgramCoursePollSummary.where('program_course_id = ? AND poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ? AND program_course_instance_id = ? ',
                                                      @program_course.id, @poll.id, master_question.id, alt.id, @program_course_instance.id).first

        poll_summary.destroy if poll_summary

        poll_summary = ProgramCoursePollSummary.new
        poll_summary.program_course = @program_course
        poll_summary.poll = @poll
        poll_summary.master_poll_question = master_question
        poll_summary.master_poll_alternative = alt
        poll_summary.program_course_instance = @program_course_instance
        poll_summary.numero_respuestas = 0

        if params[:poll_summary][alt.id.to_s.to_sym]

          poll_summary.numero_respuestas = params[:poll_summary][alt.id.to_s.to_sym]

        end

        poll_summary.save

      end

    end

    flash[:success] = t('activerecord.success.model.lms_managing.register_results_poll_ok')
    redirect_to lms_managing_execute_pci_register_results_poll_path(@program_course_instance, @poll)

  end

  private

  def verify_access_manage_module_enroll

    ct_module = CtModule.where('cod = ? AND active = ?', 'lms', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1) || user_connected.lms_enroll
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def verify_access_manage_module_manage_enroll

    ct_module = CtModule.where('cod = ? AND active = ?', 'lms', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1) || user_connected.lms_manage_enroll
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

end
