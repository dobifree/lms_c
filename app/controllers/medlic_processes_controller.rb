class MedlicProcessesController < ApplicationController
  include MedlicModule

  before_filter :active_module
  before_filter :medlic_manager
  before_filter :get_medlic_process, only: %i[ edit update manage upload_licenses delete ]

  def index; @processes = MedlicProcess.all end
  def new; @process = MedlicProcess.new end
  def manage; end

  def create
    process = MedlicProcess.new(params[:medlic_process])
    if process.save
      flash[:success] = t('views.medlic_module.flash_messages.create_success')
      redirect_to medlic_process_manage_path(process)
    else
      @process = process
      render 'new'
    end
  end

  def edit; end

  def update
    if @process.update_attributes(params[:medlic_process])
      flash[:success] = t('views.medlic_module.flash_messages.change_success')
      redirect_to medlic_process_manage_path(@process)
    else
      render 'edit'
    end
  end

  def delete
    if @process.deletable?
      @process.destroy
      flash[:success] = t('views.medlic_module.flash_messages.delete_success')
      redirect_to medlic_processes_index_path
    else
      flash[:danger] = t('views.medlic_module.flash_messages.delete_danger')
      redirect_to medlic_process_manage_path(@process)
    end
  end

  def upload_licenses
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_licenses(file, @process)
        flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = 'Se encontraron errores'
      end
    else
      flash.now[:danger] = message
    end
    render 'manage'
  end
end