class PeAssessmentController < ApplicationController

  include PeAssessmentHelper
  include PeReportingHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list]
  before_filter :get_data_2, only: [:show, :confirm]
  before_filter :get_data_3, only: [:assess]
  before_filter :get_data_3_temp, only: [:create_manual_q_comment]
  before_filter :get_data_4, only: [:destroy_manual_q_comment]
  before_filter :validate_open_process, only: [:people_list, :show, :confirm]
  before_filter :validate_access_to_evaluate, only:[:show, :assess, :confirm]

  before_filter :get_data_1_2, only: [:people_list_2]
  before_filter :get_data_2_2, only: [:show_2, :finish_assessment, :rep_detailed_final_results_pdf, :rep_detailed_final_results_pdf_auto]
  before_filter :get_data_3_2, only: [:assess_2]
  before_filter :validate_open_process_2, only: [:people_list_2, :show_2, :assess_2, :finish_assessment, :rep_detailed_final_results_pdf]
  before_filter :validate_show_member_2, only: [:show_2, :rep_detailed_final_results_pdf]
  before_filter :validate_assess_member_2, only: [:assess_2, :finish_assessment]
  before_filter :validate_assess_evaluation_2, only: [:assess_2]
  before_filter :validate_rep_detailed_final_results_pdf, only: [:rep_detailed_final_results_pdf]
  before_filter :validate_rep_detailed_final_results_pdf_auto, only: [:rep_detailed_final_results_pdf_auto]


  def people_list_2
    redirect_to pe_uniq_path if @pe_process.step_assessment_uniq? && session['from_uniq_assess_'+@pe_process.id.to_s] == '1'
  end

  def show_2

    if params['from_uniq'] == '1'
      session['from_uniq_assess_'+@pe_process.id.to_s] = '1'
    end

    @pe_member_rel_shared_comment = PeMemberRelSharedComment.new
    @pe_question_comment = PeQuestionComment.new
    @pe_question_file = PeQuestionFile.new

    @pe_process.pe_evaluations.each do |pe_evaluation|

      if !pe_evaluation.entered_by_manager? && pe_evaluation.has_pe_evaluation_rel(@pe_member_rel.pe_rel) && pe_evaluation.has_associations_to_kpis?

        pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation pe_evaluation

        unless pe_assessment_evaluation

          pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluations.build
          pe_assessment_evaluation.pe_process = @pe_process
          pe_assessment_evaluation.pe_evaluation = pe_evaluation
          pe_assessment_evaluation.last_update = lms_time

          pe_assessment_evaluation.points = -1

          pe_assessment_evaluation.percentage = -1

          pe_assessment_evaluation.save

        end

        sync_assessment_kpis(pe_assessment_evaluation, pe_evaluation, @pe_member_evaluated, @pe_member_evaluator, @pe_rel)

      end

      if pe_evaluation.assess_boss_copy_auto && @pe_member_rel.pe_rel.rel_id == 1

        if @pe_member_rel_auto.finished

          pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation pe_evaluation

          pe_assessment_evaluation_auto = @pe_member_rel_auto.pe_assessment_evaluation pe_evaluation

          if pe_assessment_evaluation_auto

            unless pe_assessment_evaluation

              pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluations.build
              pe_assessment_evaluation.pe_process = @pe_process
              pe_assessment_evaluation.pe_evaluation = pe_evaluation
              pe_assessment_evaluation.last_update = lms_time

              pe_assessment_evaluation.points = pe_assessment_evaluation_auto.points

              pe_assessment_evaluation.percentage = pe_assessment_evaluation_auto.percentage

            end

            if pe_assessment_evaluation.save

              pe_assessment_evaluation_auto.pe_assessment_questions.each do |pe_assessment_question_auto|

                unless pe_assessment_question_auto.pe_question.entered_by_manager

                  pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_assessment_question_auto.pe_question

                  unless pe_assessment_question

                    pe_assessment_question = pe_assessment_question_auto.dup

                    pe_assessment_question.pe_assessment_evaluation = pe_assessment_evaluation
                    pe_assessment_question.comment = ''
                    pe_assessment_question.comment_2 = ''
                    pe_assessment_question.save

                  end

                end

              end

            end

          end


        end

      end

      if @pe_member_rel_boss && pe_evaluation.assess_boss_copy_auto && @pe_member_rel.pe_rel.rel_id == 0

        pe_assessment_evaluation = @pe_member_rel_boss.pe_assessment_evaluation pe_evaluation

        pe_assessment_evaluation_auto = @pe_member_rel.pe_assessment_evaluation pe_evaluation

        if pe_assessment_evaluation

          unless pe_assessment_evaluation_auto

            pe_assessment_evaluation_auto = @pe_member_rel.pe_assessment_evaluations.build
            pe_assessment_evaluation_auto.pe_process = @pe_process
            pe_assessment_evaluation_auto.pe_evaluation = pe_evaluation
            pe_assessment_evaluation_auto.last_update = lms_time

            pe_assessment_evaluation_auto.points = pe_assessment_evaluation.points

            pe_assessment_evaluation_auto.percentage = pe_assessment_evaluation.percentage

          end

          if pe_assessment_evaluation_auto.save

            pe_assessment_evaluation.pe_assessment_questions.each do |pe_assessment_question|

              if pe_assessment_question.pe_question.entered_by_manager

                pe_assessment_question_auto = pe_assessment_evaluation_auto.pe_assessment_question pe_assessment_question.pe_question

                pe_assessment_question_auto.destroy if pe_assessment_question_auto

                pe_assessment_question_auto = pe_assessment_question.dup

                pe_assessment_question_auto.pe_assessment_evaluation = pe_assessment_evaluation_auto
                pe_assessment_question_auto.comment = ''
                pe_assessment_question_auto.comment_2 = ''
                pe_assessment_question_auto.save

              end

            end

          end

        end

      end

    end

  end

  def assess_2

    pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation @pe_evaluation
    pe_assessment_evaluation.destroy if pe_assessment_evaluation

    pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluations.build
    pe_assessment_evaluation.pe_process = @pe_process
    pe_assessment_evaluation.pe_evaluation = @pe_evaluation
    pe_assessment_evaluation.last_update = lms_time

    pe_assessment_evaluation.points = -1

    pe_assessment_evaluation.percentage = -1

    pe_assessment_evaluation.save

    answer_evaluation pe_assessment_evaluation

    calculate_assessment_evaluation pe_assessment_evaluation

    if @pe_evaluation.final_temporal_assessment

      calculate_assessment_final_evaluation(@pe_member_rel.pe_member_evaluated, @pe_evaluation, true)
      calculate_assessment_groups(@pe_member_rel.pe_member_evaluated)

    end

    pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel @pe_member_rel.pe_rel

    if pe_evaluation_rel.final_temporal_assessment
      calculate_assessment_final_evaluation(@pe_member_rel.pe_member_evaluated, @pe_evaluation, true)
      calculate_assessment_groups(@pe_member_rel.pe_member_evaluated)
    end


    @pe_member_rel_shared_comment = PeMemberRelSharedComment.new
    @pe_question_comment = PeQuestionComment.new
    @pe_question_file = PeQuestionFile.new

    render 'show_2'

  end

  def force_re_calculate_evaluations
=begin
    PeMemberRel.where('pe_member_evaluated_id = pe_member_evaluator_id').each do |pe_member_rel|

      @pe_member_rel = pe_member_rel
      @pe_rel = @pe_member_rel.pe_rel
      @pe_process = @pe_member_rel.pe_process
      @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
      @pe_member = @pe_member_evaluated
      @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

      @pe_member_connected = @pe_member_evaluated

      @pe_evaluation = PeEvaluation.find(1)

      @pe_member_group = @pe_member_evaluated.pe_member_group(@pe_evaluation)

      @company = Company.first

      pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation @pe_evaluation

      calculate_assessment_evaluation(pe_assessment_evaluation) if pe_assessment_evaluation

    end
=end


  end

  def finish_assessment

    ready_to_confirm = true
    valid_evaluator = true

    @pe_process.pe_evaluations.each do |pe_evaluation|

      if pe_evaluation.has_pe_evaluation_rel(@pe_member_rel.pe_rel) && @pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_evaluation)

        pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation(pe_evaluation)

        unless pe_assessment_evaluation

          pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluations.build
          pe_assessment_evaluation.pe_process = @pe_process
          pe_assessment_evaluation.pe_evaluation = pe_evaluation
          pe_assessment_evaluation.last_update = lms_time

          pe_assessment_evaluation.points = -1

          pe_assessment_evaluation.percentage = -1

          pe_assessment_evaluation.save

        end

        valid_evaluator = false if pe_assessment_evaluation && !pe_assessment_evaluation.valid_evaluation

        pe_member_group = @pe_member_evaluated.pe_member_group(pe_evaluation)

        if pe_evaluation.pe_groups.size > 0

          pe_group = pe_member_group ? pe_member_group.pe_group : nil

        else

          pe_group = nil

        end

        total_num_questions = pe_evaluation.total_number_of_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel)

        total_num_questions_need_to_answer = pe_evaluation.total_number_of_questions_by_pe_member_1st_level_ready_need_to_answer(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel)

        if @pe_member_rel.pe_rel.rel_id == 0
          total_num_questions_need_to_answer -= pe_evaluation.total_number_of_questions_by_pe_member_1st_level_ready_need_to_answer_manager(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel)
        end

        total_ansered_questions = pe_assessment_evaluation ? pe_assessment_evaluation.pe_assessment_questions.size : 0

        if total_ansered_questions < total_num_questions_need_to_answer || total_num_questions == 0
          ready_to_confirm = false
          break
        end

      end

    end

    if ready_to_confirm

      @pe_member_rel.finished = true
      @pe_member_rel.finished_at = lms_time
      @pe_member_rel.valid_evaluator = valid_evaluator
      @pe_member_rel.validated_at = nil
      @pe_member_rel.save

=begin
        calculate_assessment_final_evaluation @pe_member_evaluated, @pe_evaluation

        calculate_assessment_dimension @pe_member_evaluated, @pe_evaluation.pe_dimension

        @pe_member_evaluated.set_pe_box
        @pe_member_evaluated.save

        unless @pe_process.of_persons?

          pe_area = @pe_member_evaluated.pe_area
          pe_area.set_pe_box
          pe_area.save

        end
=end

      @pe_process.pe_evaluations.each do |pe_evaluation|

        calculate_assessment_final_evaluation @pe_member_evaluated, pe_evaluation

      end

      @pe_process.pe_dimensions.each do |pe_dimension|
        calculate_assessment_dimension @pe_member_evaluated, pe_dimension
        break unless @pe_process.two_dimensions?
      end

      @pe_member_evaluated.set_pe_box
      @pe_member_evaluated.save

      unless @pe_process.of_persons?

        pe_area = @pe_member_evaluated.pe_area
        pe_area.set_pe_box
        pe_area.save

      end

      calculate_assessment_groups @pe_member_evaluated

      finish_the_whole_assessment @pe_member_evaluated

      if @pe_process.has_step_validation? && @pe_process.step_assessment_send_email_to_validator

        begin

          menu_name = 'Evaluación de desempeño'

          ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

            if ct_menu_cod == 'pe'
              menu_name = ct_menus_user_menu_alias[index_m]
              break
            end
          end

          PeMailer.step_assessment_to_validator(@pe_process, @company, menu_name, @pe_member_rel.pe_member_evaluated.user, @pe_member_rel.pe_member_evaluator.user, @pe_member_rel.validator).deliver
        rescue Exception => e
          lm = LogMailer.new
          lm.module = 'pe'
          lm.step = 'assessment:finish'
          lm.description = e.message+' '+e.backtrace.inspect
          lm.registered_at = lms_time
          lm.save
        end

      end


=begin
      step_assessment = true

      @pe_member_evaluated.pe_member_rels_is_evaluated.each do |pe_member_rel|
        step_assessment = false unless pe_member_rel.finished
      end

      if step_assessment
        @pe_member_evaluated.step_assessment = true
        @pe_member_evaluated.step_assessment_date = lms_time
        @pe_member_evaluated.save
      end
=end

    end

    if @pe_process.step_assessment_back_list
      redirect_to pe_assessment_people_list_2_2_path @pe_process, @pe_rel.id
    else
      redirect_to pe_assessment_show_2_path @pe_member_rel
    end

  end


  def people_list

   #@company = session[:company]

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluations = @hr_process.current_hr_process_evaluations

  end

  def show

   #@company = session[:company]

    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions
    @rel = @hr_process_user_connected.rel_with @hr_process_user
    @hr_process_evaluations = @hr_process.current_hr_process_evaluations

  end

  def assess

    @rel = @hr_process_user_connected.hr_process_evalua_rels.where('hr_process_user2_id = ?', @hr_process_user.id).first

    if @rel

      es_rel_jefe = true if @rel.tipo == 'jefe'
      es_otra_rel = true if @rel.tipo == 'par'
      es_otra_rel = true if @rel.tipo == 'sub'
      es_otra_rel = true if @rel.tipo == 'cli'
      es_otra_rel = true if @rel.tipo == 'prov'

      rel_tipo = @rel.tipo

    else

      rel_tipo = nil

    end

    responder_eval_aux(@hr_process_evaluation, es_rel_jefe, false, es_otra_rel, rel_tipo)

    evalua_colaborador = true

    if @hr_process_user_connected.id == @hr_process_user.id

      evalua_colaborador = false

    end

    @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.each do |hr_evaluation_type_element|

      if hr_evaluation_type_element.evaluacion_colaborativa
        if es_rel_jefe && @hr_process_evaluation.hr_process_dimension_e.eval_auto
          #guardar la auto, hacer q el evaluador es el evaluado
          @hr_process_user_connected = @hr_process_user
          responder_eval_aux(@hr_process_evaluation, false, false, false, nil)

        elsif @hr_process_evaluation.hr_process_dimension_e.eval_auto
          #guardar la jefe, hacer q el evaluador es el jefe
          @hr_process_user.hr_process_es_evaluado_rels.each do |rel|

            if rel.tipo == 'jefe'

              @hr_process_user_connected = rel.hr_process_user1
              responder_eval_aux(@hr_process_evaluation, true, false, false, 'jefe')

            end

          end

        end

        break

      end

    end

    flash[:success] = t('activerecord.success.model.hr_process_assessment.porcentaje_logro_ok')

    redirect_to pe_assessment_show_path(@hr_process_user)


  end

  def create_manual_q_comment

    @hr_process_evaluaion_manual_q = HrProcessEvaluationManualQ.find(params[:hr_process_evaluation_manual_q_id])

    manual_comment = @hr_process_evaluaion_manual_q.hr_process_evaluation_manual_qcs.build
    manual_comment.comentario = params[:comentario]
    manual_comment.fecha = lms_time
    manual_comment.save

    flash[:success] = t('activerecord.success.model.hr_process_user_comment.create_ok')

    redirect_to pe_assessment_show_path(@hr_process_user)

  end

  def destroy_manual_q_comment

    @hr_process_evaluaion_manual_qc.destroy

    flash[:success] = t('activerecord.success.model.hr_process_user_comment.delete_ok')

    redirect_to pe_assessment_show_path(@hr_process_user)

  end

  def confirm

    @rel = @hr_process_user_connected.hr_process_evalua_rels.where('hr_process_user2_id = ?', @hr_process_user.id).first

    if @rel

      if @rel.tipo == 'jefe'
        es_rel_jefe = true
      end

    elsif @hr_process_user.id == @hr_process_user_connected.id
      es_auto_eval = true
    end

    if es_auto_eval

      @hr_process_user_connected.finalizada_auto = params[:hr_process_user][:finalizada_auto]
      @hr_process_user_connected.save

      if @hr_process_user_connected.finalizada_auto && @hr_process.ae_avisa_jefe_email
        @hr_process_user_connected.hr_process_es_evaluado_users.where('tipo = ?', 'jefe').each do |jefe|

          LmsMailer.informar_jefe_autoeval_terminada(jefe.user, @hr_process_user_connected.user, @hr_process, @company).deliver

        end
      end

      if @hr_process_user_connected.hr_process_es_evaluado_rels.where('finalizada = 0 and tipo <> ?', 'resp').count == 0 && @hr_process_user_connected.finalizada_auto
        @hr_process_user_connected.finalizado = true
        @hr_process_user_connected.save

      end

    else

      hr_process_user_rel = HrProcessUserRel.find(@rel.id)

      hr_process_user_rel.finalizada = params[:hr_process_user_rel][:finalizada]

      hr_process_user_rel.save

      if HrProcessAssessmentE.where('hr_process_user_id = ? AND hr_process_user_eval_id = ? AND resultado_puntos = -1', @hr_process_user.id, @hr_process_user_connected.id).count>0
        hr_process_user_rel.no_aplica = true
        hr_process_user_rel.save
      end

      if debe_realizar_autoevaluacion_proceso(@hr_process, @hr_process_user)

=begin
        if $current_domain == 'seduc' || $current_domain == 'seduc-training'

          if es_rel_jefe

            must_close_colab = false

            @hr_process.hr_process_evaluations.each do |hr_process_eval|

              hr_process_eval.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.each do |hr_evaluation_type_element|

                must_close_colab = true if hr_evaluation_type_element.evaluacion_colaborativa

              end

            end

            if must_close_colab && hr_process_user_rel.finalizada

              @hr_process_user.finalizada_auto = true
              @hr_process_user.save

            end

          end

        end
=end

        if @hr_process_user.hr_process_es_evaluado_rels.where('finalizada = 0 and tipo <> ?', 'resp').count == 0 && @hr_process_user.finalizada_auto
          @hr_process_user.finalizado = true
          @hr_process_user.save
        end

      else

        if @hr_process_user.hr_process_es_evaluado_rels.where('finalizada = 0 and tipo <> ?', 'resp').count == 0
          @hr_process_user.finalizado = true
          @hr_process_user.save
        end

      end

    end

    redirect_to pe_assessment_show_path(@hr_process_user)

  end

  def rep_detailed_final_results_pdf

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    show_comments = @pe_member_rel.pe_rel.rel == 1 ? true : false

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, false, false, @pe_process.public_pe_box, @pe_process.public_pe_dimension_name, show_comments

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def rep_detailed_final_results_pdf_auto

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    show_comments = false

    generate_rep_detailed_final_results_pdf_auto archivo_temporal, @pe_member, @pe_process, false, false, @pe_process.public_pe_box, @pe_process.public_pe_dimension_name, show_comments

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

    def get_data_1_2

      @pe_process = PeProcess.find(params[:pe_process_id])
      @pe_members_connected = @pe_process.pe_members.where('user_id = ?', user_connected.id)

     #@company = session[:company]

    end

    def get_data_2_2
      @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
      @pe_rel = @pe_member_rel.pe_rel
      @pe_process = @pe_member_rel.pe_process
      @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
      @pe_member = @pe_member_evaluated
      @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

      @pe_member_rel_auto = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluated

      @pe_rel_boss = @pe_process.pe_rel_by_id 1
      @pe_member_rel_boss = @pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(@pe_rel_boss).first if @pe_rel_boss

      @pe_member_connected = nil

      @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

        if pe_member_connected.id == @pe_member_evaluator.id
          @pe_member_connected = @pe_member_evaluator
        end

      end

     #@company = session[:company]

    end

    def get_data_3_2

      @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
      @pe_rel = @pe_member_rel.pe_rel
      @pe_process = @pe_member_rel.pe_process
      @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
      @pe_member = @pe_member_evaluated
      @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

      @pe_member_rel_auto = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluated

      @pe_member_connected = nil

      @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

        if pe_member_connected.id == @pe_member_evaluator.id
          @pe_member_connected = @pe_member_evaluator
          break
        end

      end

      @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

      @pe_member_group = @pe_member_evaluated.pe_member_group(@pe_evaluation)

     #@company = session[:company]

    end

    def validate_open_process_2

      unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
        flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
        redirect_to root_path
      end

    end

    def validate_show_member_2

      unless @pe_member_connected && @pe_member_evaluator.id == @pe_member_connected.id
        flash[:danger] = t('activerecord.error.model.pe_process_assessment.cant_assess')
        redirect_to root_path
      end

    end

    def validate_assess_member_2

      if @pe_member_rel.finished? || !@pe_member_connected || @pe_member_evaluator.id != @pe_member_connected.id

        flash[:danger] = t('activerecord.error.model.pe_process_assessment.cant_assess')
        redirect_to root_path
      end

    end

    def validate_assess_evaluation_2

      valid_group = false

      if @pe_evaluation.pe_groups.size > 0
        valid_group = true if @pe_member_group
      else
        valid_group = true
      end

      unless valid_group && @pe_evaluation.pe_evaluation_rels.where('pe_rel_id = ?', @pe_member_rel.pe_rel_id).count == 1

        flash[:danger] = t('activerecord.error.model.pe_process_assessment.cant_assess')
        redirect_to root_path

      end

    end

    def validate_rep_detailed_final_results_pdf

      unless @pe_member_connected && @pe_member_evaluator.id == @pe_member_connected.id && @pe_member_rel.pe_rel.rel == 1 && @pe_process.step_assessment_show_rep_dfr_to_boss_pdf?
        flash[:danger] = t('activerecord.error.model.pe_process_assessment.cant_assess')
        redirect_to root_path
      end

    end

    def validate_rep_detailed_final_results_pdf_auto

      unless @pe_member_connected && @pe_member_evaluator.id == @pe_member_connected.id && @pe_member_rel.pe_rel.rel == 0 && @pe_process.pe_process_reports.rep_detailed_auto?
        flash[:danger] = t('activerecord.error.model.pe_process_assessment.cant_assess')
        redirect_to root_path
      end

    end

    ####################################
    ####################################

    def get_data_1
      @hr_process = HrProcess.find(params[:hr_process_id])
    end

    def get_data_2
      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
      @hr_process = @hr_process_user.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
    end

    def get_data_3
      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
      @hr_process = @hr_process_user.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
      @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])

    end

    def get_data_3_temp

      @hr_process_evaluation_manual_q = HrProcessEvaluationManualQ.find(params[:hr_process_evaluation_manual_q_id])

      @hr_process_evaluation = @hr_process_evaluation_manual_q.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
      @hr_process_user = @hr_process_evaluation_manual_q.hr_process_user
    end

    def get_data_4

      @hr_process_evaluaion_manual_qc = HrProcessEvaluationManualQc.find(params[:hr_process_evaluation_manual_qc_id])

      @hr_process_evaluation_manual_q = @hr_process_evaluaion_manual_qc.hr_process_evaluation_manual_q

      @hr_process_evaluation = @hr_process_evaluation_manual_q.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id
      @hr_process_user = @hr_process_evaluation_manual_q.hr_process_user

    end

    def validate_open_process

      unless @hr_process.abierto && @hr_process.fecha_inicio < lms_time && @hr_process.fecha_fin > lms_time
        flash[:danger] = t('activerecord.error.model.hr_process_assessment.out_of_time')
        redirect_to root_path
      end

    end

    def validate_access_to_evaluate

      unless @hr_process_user_connected.has_to_assess_user?(@hr_process_user) || @hr_process_user.id == @hr_process_user_connected.id

        flash[:danger] = t('security.no_access_hr_perform_evaluation')
        redirect_to root_path

      end

    end



    def responder_eval_aux(hr_process_evaluation, es_rel_jefe, es_porcentaje_logro, es_otra_rel = false, rel_tipo = 'jefe')

      if es_rel_jefe || es_otra_rel
        HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND hr_process_user_eval_id = ? AND tipo_rel = ?', hr_process_evaluation.id, @hr_process_user.id, @hr_process_user_connected.id, rel_tipo).destroy_all
      elsif hr_process_evaluation.hr_process_dimension_e.eval_auto && @hr_process_user_connected.id == @hr_process_user.id
        HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND hr_process_user_eval_id = ? AND tipo_rel IS NULL', hr_process_evaluation.id, @hr_process_user.id, @hr_process_user_connected.id).destroy_all
      end

      #hr_process_assessment_e.destroy if hr_process_assessment_e

      hr_process_assessment_e = HrProcessAssessmentE.new
      hr_process_assessment_e.fecha = lms_time
      hr_process_assessment_e.hr_process_evaluation = hr_process_evaluation
      hr_process_assessment_e.hr_process_user = @hr_process_user
      hr_process_assessment_e.hr_process_user_eval_id = @hr_process_user_connected.id

      if es_rel_jefe || es_otra_rel
        hr_process_assessment_e.tipo_rel = rel_tipo
      elsif hr_process_evaluation.hr_process_dimension_e.eval_auto && @hr_process_user_connected.id == @hr_process_user.id
        hr_process_assessment_e.tipo_rel = nil
      end

      if hr_process_assessment_e.save


        total_puntos_eval = 0
        total_pesos_eval = 0
        max_total_puntos_eval = 0
        formula_promedio_eval = false
        se_queda_en_no_aplica_eval = true

        if hr_process_evaluation.carga_manual?
          hr_process_evaluation_qs = hr_process_evaluation.manual_questions_nivel_1(@hr_process_user)
        else
          hr_process_evaluation_qs = hr_process_evaluation.questions_nivel_1(@hr_process_user.hr_process_level.id)
        end

        total_puntos_eval_nivel_1_no_aplica = 0
        total_pesos_eval_nivel_1_no_aplica = 0
        max_total_puntos_eval_nivel_1_no_aplica = 0

        hr_process_evaluation_qs.each do |hr_process_evaluation_q|

          total_pesos_eval += hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
          max_total_puntos_eval += hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)
          formula_promedio_eval = hr_process_evaluation_q.hr_evaluation_type_element.formula_promedio

          hr_process_assessment_q = HrProcessAssessmentQ.new
          hr_process_assessment_q.hr_process_assessment_e = hr_process_assessment_e

          if hr_process_evaluation.carga_manual?
            hr_process_assessment_q.hr_process_evaluation_manual_q = hr_process_evaluation_q
          else
            hr_process_assessment_q.hr_process_evaluation_q = hr_process_evaluation_q
          end

          hr_process_assessment_q.hr_process_user = @hr_process_user
          hr_process_assessment_q.fecha = lms_time

          if !hr_process_evaluation.carga_manual? && hr_process_evaluation_q.comentario
            hr_process_assessment_q.comentario = params["preg_#{hr_process_evaluation_q.id}_comment".to_sym]
          end

          if hr_process_assessment_q.save

            if hr_process_evaluation.carga_manual?
              num_pregs = hr_process_evaluation_q.hr_process_evaluation_manual_qs.length
              num_alts = 0
            else
              num_pregs = hr_process_evaluation_q.hr_process_evaluation_qs.length
              num_alts = hr_process_evaluation_q.hr_process_evaluation_as.length
            end

            if num_pregs == 0

              #se asume que se evalúa y no es sólo un agrupador

              if num_alts == 0

                if params["preg_#{hr_process_evaluation_q.id}".to_sym]

                  hr_process_assessment_q.resultado_puntos = params["preg_#{hr_process_evaluation_q.id}".to_sym].to_f

                  if hr_process_evaluation_q.hr_evaluation_type_element.indicador_logro

                    if hr_process_evaluation_q.tipo_indicador == 5

                      hr_process_assessment_q.logro_indicador_d = params["preg_#{hr_process_evaluation_q.id}".to_sym]

                    else

                      hr_process_assessment_q.logro_indicador = params["preg_#{hr_process_evaluation_q.id}".to_sym].to_f

                    end

                    if hr_process_evaluation_q.tipo_indicador == 5 || hr_process_assessment_q.logro_indicador


                      case hr_process_evaluation_q.tipo_indicador
                        when 1
                          hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador && hr_process_evaluation_q.meta_indicador > 0 ? hr_process_assessment_q.logro_indicador*100/hr_process_evaluation_q.meta_indicador : 0
                        when 2
                          hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador ? hr_process_evaluation_q.meta_indicador*100/hr_process_assessment_q.logro_indicador : 0
                        when 3
                          if hr_process_assessment_q.logro_indicador >= hr_process_evaluation_q.meta_indicador*-1 && hr_process_assessment_q.logro_indicador <= hr_process_evaluation_q.meta_indicador
                            hr_process_assessment_q.resultado_puntos = 100
                          else
                            hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador ? 100-(hr_process_assessment_q.logro_indicador-hr_process_evaluation_q.meta_indicador).abs*100/hr_process_evaluation_q.meta_indicador : 0
                          end
                        when 4
                          if hr_process_evaluation_q.pe_question_ranks.size > 0
                            metas = Array.new
                            porcentajes = Array.new

                            hr_process_evaluation_q.pe_question_ranks.each do |pe_question_rank|

                              metas.push pe_question_rank.goal
                              porcentajes.push pe_question_rank.percentage
                            end

                            if metas.first <= metas.last
                              #directo

                              if hr_process_assessment_q.logro_indicador < metas.first
                                hr_process_assessment_q.resultado_puntos = 0
                              elsif hr_process_assessment_q.logro_indicador >= metas.last
                                hr_process_assessment_q.resultado_puntos = porcentajes.last
                              else

                                meta_inf = meta_dif = 0
                                por_inf = por_dif = 0


                                (0..metas.length-1).each do |num_meta|
                                  if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= hr_process_assessment_q.logro_indicador && hr_process_assessment_q.logro_indicador <= metas[num_meta+1]
                                    meta_dif = metas[num_meta+1] - metas[num_meta]
                                    meta_inf = metas[num_meta]
                                    por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                    por_inf = porcentajes[num_meta]
                                    break
                                  end
                                end

                                if meta_dif == 0
                                  hr_process_assessment_q.resultado_puntos = por_inf
                                elsif meta_dif > 0
                                  hr_process_assessment_q.resultado_puntos = ((hr_process_assessment_q.logro_indicador-meta_inf)*por_dif/meta_dif)+por_inf
                                else
                                  hr_process_assessment_q.resultado_puntos = 0
                                end

                              end

                            else
                              #inverso
                              if hr_process_assessment_q.logro_indicador > metas.first
                                hr_process_assessment_q.resultado_puntos = 0
                              elsif hr_process_assessment_q.logro_indicador <= metas.last
                                hr_process_assessment_q.resultado_puntos = porcentajes.last
                              else

                                meta_sup = meta_dif = 0
                                por_inf = por_dif = 0


                                (0..metas.length-1).each do |num_meta|
                                  if metas[num_meta] && metas[num_meta+1] && metas[num_meta] >= hr_process_assessment_q.logro_indicador && hr_process_assessment_q.logro_indicador >= metas[num_meta+1]
                                    meta_dif = metas[num_meta] - metas[num_meta+1]
                                    meta_sup = metas[num_meta]
                                    por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                    por_inf = porcentajes[num_meta]
                                    break
                                  end
                                end

                                if meta_dif == 0
                                  hr_process_assessment_q.resultado_puntos = por_inf
                                elsif meta_dif > 0
                                  hr_process_assessment_q.resultado_puntos = ((meta_sup-hr_process_assessment_q.logro_indicador)*por_dif/meta_dif)+por_inf
                                else
                                  hr_process_assessment_q.resultado_puntos = 0
                                end

                              end

                            end

                          else
                            hr_process_assessment_q.resultado_puntos = 0
                          end

                        when 5

                          hr_process_assessment_q.resultado_puntos = 0

                          if hr_process_evaluation_q.pe_question_ranks.size > 0

                            hr_process_evaluation_q.pe_question_ranks.each do |pe_question_rank|

                              if hr_process_assessment_q.logro_indicador_d == pe_question_rank.discrete_goal
                                hr_process_assessment_q.resultado_puntos = pe_question_rank.percentage
                                break
                              end
                            end

                          end


                      end

                      hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.hr_evaluation_type_element.valor_minimo if hr_process_assessment_q.resultado_puntos < hr_process_evaluation_q.hr_evaluation_type_element.valor_minimo
                      hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo if hr_process_assessment_q.resultado_puntos > hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo

                    end

                  end

                  if hr_process_evaluation.cien_x_cien_puntos

                    hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                  else

                    hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo

                  end

                  if hr_process_assessment_q.save

                    total_puntos_eval += hr_process_assessment_q.resultado_puntos*hr_process_evaluation_q.peso

                    if hr_process_evaluation_q.hr_evaluation_type_element.indicador_logro && es_rel_jefe && es_porcentaje_logro

                      hr_process_assessment_l = HrProcessAssessmentL.new
                      hr_process_assessment_l.hr_process_evaluation_q_id = hr_process_assessment_q.hr_process_evaluation_q_id
                      hr_process_assessment_l.hr_process_evaluation_manual_q_id = hr_process_assessment_q.hr_process_evaluation_manual_q_id
                      hr_process_assessment_l.hr_process_user_id = hr_process_assessment_q.hr_process_user_id
                      hr_process_assessment_l.hr_process_user_eval_id = @hr_process_user_connected.id
                      hr_process_assessment_l.resultado_puntos = hr_process_assessment_q.resultado_puntos
                      hr_process_assessment_l.resultado_porcentaje = hr_process_assessment_q.resultado_porcentaje
                      hr_process_assessment_l.logro_indicador = hr_process_assessment_q.logro_indicador
                      hr_process_assessment_l.fecha = lms_time

                      hr_process_assessment_l.save

                    end

                  end

                end

              else

                if params["preg_#{hr_process_evaluation_q.id}".to_sym]

                  hr_process_evaluation_q.hr_process_evaluation_as.each do |hr_process_evaluation_a|

                    if params["preg_#{hr_process_evaluation_q.id}".to_sym] == hr_process_evaluation_a.id.to_s

                      hr_process_assessment_q.hr_process_evaluation_a = hr_process_evaluation_a
                      hr_process_assessment_q.resultado_puntos = hr_process_evaluation_a.valor
                      if hr_process_evaluation.cien_x_cien_puntos
                        hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos
                      else
                        hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo
                      end


                      if hr_process_assessment_q.save

                        if hr_process_evaluation_a.valor > -1

                          se_queda_en_no_aplica_eval=false

                          total_puntos_eval += hr_process_assessment_q.resultado_puntos*hr_process_evaluation_q.peso
                        else

                          total_pesos_eval -= hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
                          max_total_puntos_eval -= hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)
                        end

                      end

                    end


                  end

                end

              end

            else
              total_puntos_preg = 0
              total_pesos_preg = 0
              max_total_puntos_preg = 0
              formula_promedio_preg = false
              se_queda_en_no_aplica = true

              if hr_process_evaluation.carga_manual?
                hr_process_evaluation_qs = hr_process_evaluation_q.hr_process_evaluation_manual_qs
              else
                hr_process_evaluation_qs = hr_process_evaluation_q.hr_process_evaluation_qs
              end

              hr_process_evaluation_qs.each do |hr_process_evaluation_q1|

                total_pesos_preg += hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0
                max_total_puntos_preg += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
                formula_promedio_preg = hr_process_evaluation_q1.hr_evaluation_type_element.formula_promedio

                hr_process_assessment_q1 = HrProcessAssessmentQ.new
                hr_process_assessment_q1.hr_process_assessment_e = hr_process_assessment_e

                if hr_process_evaluation.carga_manual?
                  hr_process_assessment_q1.hr_process_evaluation_manual_q = hr_process_evaluation_q1
                else
                  hr_process_assessment_q1.hr_process_evaluation_q = hr_process_evaluation_q1
                end
                hr_process_assessment_q1.hr_process_user = @hr_process_user
                hr_process_assessment_q1.fecha = lms_time


                if !hr_process_evaluation.carga_manual? && hr_process_evaluation_q1.comentario
                  hr_process_assessment_q1.comentario = params["preg_#{hr_process_evaluation_q1.id}_comment".to_sym]
                end

                if hr_process_assessment_q1.save

                  if hr_process_evaluation.carga_manual? || hr_process_evaluation_q1.hr_process_evaluation_as.length == 0

                    if params["preg_#{hr_process_evaluation_q1.id}".to_sym]

                      hr_process_assessment_q1.resultado_puntos = params["preg_#{hr_process_evaluation_q1.id}".to_sym].to_f

                      if hr_process_evaluation_q1.hr_evaluation_type_element.indicador_logro

                        unless params['comentario_field_'+hr_process_evaluation_q1.id.to_s].blank?

                          if @hr_process_user_connected.user.id == user_connected.id

                            manual_comment = hr_process_evaluation_q1.hr_process_evaluation_manual_qcs.build
                            manual_comment.comentario = params['comentario_field_'+hr_process_evaluation_q1.id.to_s]
                            manual_comment.fecha = lms_time
                            manual_comment.hr_process_user = @hr_process_user_connected
                            manual_comment.save

                          end

                        end

                        if hr_process_evaluation_q1.tipo_indicador == 5

                          hr_process_assessment_q1.logro_indicador_d = params["preg_#{hr_process_evaluation_q1.id}".to_sym]

                        else

                          hr_process_assessment_q1.logro_indicador = params["preg_#{hr_process_evaluation_q1.id}".to_sym].to_f

                        end

                        if hr_process_evaluation_q1.tipo_indicador == 5 || hr_process_assessment_q1.logro_indicador


                          case hr_process_evaluation_q1.tipo_indicador
                            when 1
                              hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador && hr_process_evaluation_q1.meta_indicador > 0 ? hr_process_assessment_q1.logro_indicador*100/hr_process_evaluation_q1.meta_indicador : 0
                            when 2
                              hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador ? hr_process_evaluation_q1.meta_indicador*100/hr_process_assessment_q1.logro_indicador : 0
                            when 3
                              if hr_process_assessment_q1.logro_indicador >= hr_process_evaluation_q1.meta_indicador*-1 && hr_process_assessment_q1.logro_indicador <= hr_process_evaluation_q1.meta_indicador
                                hr_process_assessment_q1.resultado_puntos = 100
                              else
                                hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador ? 100-(hr_process_assessment_q1.logro_indicador-hr_process_evaluation_q1.meta_indicador).abs*100/hr_process_evaluation_q1.meta_indicador : 0
                              end

                            when 4
                              if hr_process_evaluation_q1.pe_question_ranks.size > 0
                                metas = Array.new
                                porcentajes = Array.new

                                hr_process_evaluation_q1.pe_question_ranks.each do |pe_question_rank|

                                  metas.push pe_question_rank.goal
                                  porcentajes.push pe_question_rank.percentage
                                end


                                if metas.first <= metas.last

                                  #directo
                                  if hr_process_assessment_q1.logro_indicador < metas.first
                                    hr_process_assessment_q1.resultado_puntos = 0
                                  elsif hr_process_assessment_q1.logro_indicador >= metas.last
                                    hr_process_assessment_q1.resultado_puntos = porcentajes.last
                                  else

                                    meta_inf = meta_dif = 0
                                    por_inf = por_dif = 0


                                    (0..metas.length-1).each do |num_meta|
                                      if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= hr_process_assessment_q1.logro_indicador && hr_process_assessment_q1.logro_indicador <= metas[num_meta+1]
                                        meta_dif = metas[num_meta+1] - metas[num_meta]
                                        meta_inf = metas[num_meta]
                                        por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                        por_inf = porcentajes[num_meta]
                                        break
                                      end
                                    end

                                    if meta_dif == 0
                                      hr_process_assessment_q1.resultado_puntos = por_inf
                                    elsif meta_dif > 0
                                      hr_process_assessment_q1.resultado_puntos = ((hr_process_assessment_q1.logro_indicador-meta_inf)*por_dif/meta_dif)+por_inf
                                    else
                                      hr_process_assessment_q1.resultado_puntos = 0
                                    end

                                  end

                                else
                                  #inverso

                                  if hr_process_assessment_q1.logro_indicador > metas.first
                                    hr_process_assessment_q1.resultado_puntos = 0
                                  elsif hr_process_assessment_q1.logro_indicador <= metas.last
                                    hr_process_assessment_q1.resultado_puntos = porcentajes.last
                                  else

                                    meta_sup = meta_dif = 0
                                    por_inf = por_dif = 0


                                    (0..metas.length-1).each do |num_meta|
                                      if metas[num_meta] && metas[num_meta+1] && metas[num_meta] >= hr_process_assessment_q1.logro_indicador && hr_process_assessment_q1.logro_indicador >= metas[num_meta+1]
                                        meta_dif = metas[num_meta] - metas[num_meta+1]
                                        meta_sup = metas[num_meta]
                                        por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                        por_inf = porcentajes[num_meta]
                                        break
                                      end
                                    end

                                    if meta_dif == 0
                                      hr_process_assessment_q1.resultado_puntos = por_inf
                                    elsif meta_dif > 0
                                      hr_process_assessment_q1.resultado_puntos = ((meta_sup-hr_process_assessment_q1.logro_indicador)*por_dif/meta_dif)+por_inf
                                    else
                                      hr_process_assessment_q1.resultado_puntos = 0
                                    end

                                  end


                                end

                              else
                                hr_process_assessment_q1.resultado_puntos = 0
                              end

                            when 5

                              hr_process_assessment_q1.resultado_puntos = 0

                              if hr_process_evaluation_q1.pe_question_ranks.size > 0

                                hr_process_evaluation_q1.pe_question_ranks.each do |pe_question_rank|

                                  if hr_process_assessment_q1.logro_indicador_d == pe_question_rank.discrete_goal
                                    hr_process_assessment_q1.resultado_puntos = pe_question_rank.percentage
                                    break
                                  end
                                end

                              end


                          end

                          hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.hr_evaluation_type_element.valor_minimo if hr_process_assessment_q1.resultado_puntos && hr_process_assessment_q1.resultado_puntos < hr_process_evaluation_q1.hr_evaluation_type_element.valor_minimo
                          hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo if hr_process_assessment_q1.resultado_puntos && hr_process_assessment_q1.resultado_puntos > hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo


                        end

                      end


                      if hr_process_evaluation.cien_x_cien_puntos

                        hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                      else

                        hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo

                      end



                      if hr_process_assessment_q1.save

                        total_puntos_preg += hr_process_assessment_q1.resultado_puntos*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                        total_puntos_eval_nivel_1_no_aplica += hr_process_assessment_q1.resultado_puntos*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
                        total_pesos_eval_nivel_1_no_aplica += (hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                        max_total_puntos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                      end

                    end

                  elsif hr_process_evaluation_q1.hr_process_evaluation_as.length > 0

                    # si tiene alternativas no puede haber sido carga manual

                    if params["preg_#{hr_process_evaluation_q1.id}".to_sym]

                      hr_process_evaluation_q1.hr_process_evaluation_as.each do |hr_process_evaluation_a|

                        if params["preg_#{hr_process_evaluation_q1.id}".to_sym] == hr_process_evaluation_a.id.to_s

                          hr_process_assessment_q1.hr_process_evaluation_a = hr_process_evaluation_a
                          hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_a.valor

                          if hr_process_evaluation.cien_x_cien_puntos

                            hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                          else

                            hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo

                          end

                          if hr_process_assessment_q1.save


                            if hr_process_evaluation_a.valor > -1

                              se_queda_en_no_aplica = false

                              total_puntos_preg += hr_process_assessment_q1.resultado_puntos*hr_process_evaluation_q1.peso

                              total_puntos_eval_nivel_1_no_aplica += hr_process_assessment_q1.resultado_puntos*hr_process_evaluation_q1.peso
                              total_pesos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.peso

                              max_total_puntos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*hr_process_evaluation_q1.peso
                            else
                              total_pesos_preg -= hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0
                              max_total_puntos_preg -= hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
                            end

                          end

                        end

                      end

                    end

                  end

                end

              end



              if se_queda_en_no_aplica

                hr_process_assessment_q.resultado_puntos = -1

              else

                se_queda_en_no_aplica_eval = false

                if formula_promedio_preg

                  if total_pesos_preg > 0

                    hr_process_assessment_q.resultado_puntos = total_puntos_preg/total_pesos_preg
                    if hr_process_evaluation.cien_x_cien_puntos
                      max_total_puntos_preg = hr_process_evaluation.cien_x_cien_puntos
                    else
                      max_total_puntos_preg = max_total_puntos_preg/total_pesos_preg
                    end

                  else

                    hr_process_assessment_q.resultado_puntos = 0
                    max_total_puntos_preg = 0

                  end

                else

                  hr_process_assessment_q.resultado_puntos = total_puntos_preg

                end

              end

              if max_total_puntos_preg > 0

                hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/max_total_puntos_preg

              else

                hr_process_assessment_q.resultado_porcentaje = 0

              end



              if hr_process_assessment_q.save

                if hr_process_assessment_q.resultado_puntos > -1

                  total_puntos_eval += hr_process_assessment_q.resultado_puntos*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)

                else

                  total_pesos_eval -= hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
                  max_total_puntos_eval -= hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)

                end

              end


            end

          end



        end



        if formula_promedio_eval


          if hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 1).first.aplica_evaluacion

            if total_pesos_eval > 0

              if se_queda_en_no_aplica_eval
                hr_process_assessment_e.resultado_puntos = -1
              else
                hr_process_assessment_e.resultado_puntos = total_puntos_eval/total_pesos_eval
              end

              max_total_puntos_eval = max_total_puntos_eval/total_pesos_eval

            else

              if se_queda_en_no_aplica_eval
                hr_process_assessment_e.resultado_puntos = -1
              else
                hr_process_assessment_e.resultado_puntos = 0
              end

              max_total_puntos_eval = 0

            end

          else

            if total_pesos_eval_nivel_1_no_aplica > 0



              hr_process_assessment_e.resultado_puntos = total_puntos_eval_nivel_1_no_aplica/total_pesos_eval_nivel_1_no_aplica
              max_total_puntos_eval = max_total_puntos_eval_nivel_1_no_aplica/total_pesos_eval_nivel_1_no_aplica

            else

              hr_process_assessment_e.resultado_puntos = 0
              max_total_puntos_eval = 0

            end

          end


        else

          if hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 1).first.aplica_evaluacion

            if se_queda_en_no_aplica_eval
              hr_process_assessment_e.resultado_puntos = -1
            else
              hr_process_assessment_e.resultado_puntos = total_puntos_eval
            end

          else

            if se_queda_en_no_aplica_eval

              hr_process_assessment_e.resultado_puntos = -1
            else
              hr_process_assessment_e.resultado_puntos = total_puntos_eval_nivel_1_no_aplica
            end

          end

        end

        max_total_puntos_eval = hr_process_evaluation.cien_x_cien_puntos if hr_process_evaluation.cien_x_cien_puntos

        if max_total_puntos_eval > 0

          hr_process_assessment_e.resultado_porcentaje = hr_process_assessment_e.resultado_puntos*100/max_total_puntos_eval

        else

          hr_process_assessment_e.resultado_porcentaje = 0

        end

        hr_process_assessment_e.save

        if hr_process_evaluation.tope_no_aplica

          type_element_second_orden = hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.last
          if type_element_second_orden
            total_preguntas_contestadas_no_aplica = hr_process_assessment_e.hr_process_assessment_qs.where('resultado_puntos = -1 AND hr_process_evaluation_q_id NOT IN(?)',hr_process_evaluation.questions_nivel_1(@hr_process_user.hr_process_level.id).map(&:id)).count if hr_process_assessment_e
            total_preguntas_por_contestar_no_aplica = hr_process_evaluation.total_number_of_questions_type_element(@hr_process_user.hr_process_level.id, type_element_second_orden.id)
          else
            total_preguntas_contestadas_no_aplica = hr_process_assessment_e.hr_process_assessment_qs.where('resultado_puntos = -1').count if hr_process_assessment_e
            total_preguntas_por_contestar_no_aplica = total_preguntas_por_contestar
          end

          if (total_preguntas_contestadas_no_aplica*100/total_preguntas_por_contestar_no_aplica)>hr_process_evaluation.tope_no_aplica
            hr_process_assessment_e.resultado_puntos = -1
            hr_process_assessment_e.save
          end

        end

        calculate_final_results hr_process_assessment_e.hr_process_evaluation.hr_process_dimension_e.hr_process_dimension

      end

    end

    def calculate_final_results(hr_process_dimension)

      hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

      dimension_es = hr_process_dimension.hr_process_dimension_es

      resultado_dimension = 0

      hr_process_evaluations.each do |hr_process_evaluation|

        if hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.id == hr_process_dimension.id


          HrProcessAssessmentEf.where('hr_process_evaluation_id = ? AND hr_process_user_id = ?', hr_process_evaluation.id, @hr_process_user.id).destroy_all


          resultado_evaluation = 0
          suma_pesos_evaluation = 0
          total_puntos_evaluation = 0

          HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND resultado_puntos <> - 1', hr_process_evaluation.id, @hr_process_user.id).each do |hr_process_assessment_e|

            peso = 0

            if hr_process_assessment_e.tipo_rel == 'jefe'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso
            elsif hr_process_assessment_e.tipo_rel.nil?
              peso = hr_process_evaluation.hr_process_dimension_e.eval_auto_peso
            elsif hr_process_assessment_e.tipo_rel == 'sub'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_sub_peso
            elsif hr_process_assessment_e.tipo_rel == 'par'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_par_peso
            elsif hr_process_assessment_e.tipo_rel == 'cli'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_cli_peso
            elsif hr_process_assessment_e.tipo_rel == 'prov'
              peso = hr_process_evaluation.hr_process_dimension_e.eval_prov_peso
            end

            suma_pesos_evaluation += peso
            resultado_evaluation += hr_process_assessment_e.resultado_porcentaje * peso
            total_puntos_evaluation += 100 * peso

          end

          if suma_pesos_evaluation > 0

            porcentaje_evaluation = resultado_evaluation*100/total_puntos_evaluation
            resultado_evaluation = resultado_evaluation/suma_pesos_evaluation

            hr_process_assessment_ef = HrProcessAssessmentEf.new
            hr_process_assessment_ef.fecha = lms_time
            hr_process_assessment_ef.hr_process_evaluation = hr_process_evaluation
            hr_process_assessment_ef.hr_process_user = @hr_process_user
            hr_process_assessment_ef.resultado_puntos = resultado_evaluation
            hr_process_assessment_ef.resultado_porcentaje = porcentaje_evaluation
            hr_process_assessment_ef.save


            resultado_dimension += porcentaje_evaluation*hr_process_evaluation.hr_process_dimension_e.porcentaje

          end

        end

      end

      resultado_dimension = resultado_dimension/100

      hr_process_assessment_d = @hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

      unless hr_process_assessment_d

        hr_process_assessment_d = @hr_process_user.hr_process_assessment_ds.new
        hr_process_assessment_d.hr_process_dimension = hr_process_dimension

      end

      hr_process_assessment_d.resultado_porcentaje = resultado_dimension



      #hr_process_dimension_g_match = hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').where('valor_minimo <= ? AND valor_maximo >= ?', hr_process_assessment_d.resultado_porcentaje, hr_process_assessment_d.resultado_porcentaje).first


      hr_process_dimension_g_match = nil

      hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

        if hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

          hr_process_dimension_g_match = hr_process_dimension_g
          break

        end

      end

      hr_process_assessment_d.hr_process_dimension_g = hr_process_dimension_g_match if hr_process_dimension_g_match

      hr_process_assessment_d.fecha = lms_time

      hr_process_assessment_d.save

      hr_process_quadrant = @hr_process_user.quadrant

      if hr_process_quadrant
        qua = @hr_process_user.hr_process_assessment_qua
        unless qua
          qua = HrProcessAssessmentQua.new
          qua.hr_process_user = @hr_process_user
        end
        qua.fecha = lms_time
        qua.hr_process_quadrant = hr_process_quadrant
        qua.save

      end
    end

end
