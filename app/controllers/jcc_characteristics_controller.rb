class JccCharacteristicsController < ApplicationController
  before_filter :authenticate_user
  before_filter :authenticate_user_admin
  # GET /jcc_characteristics
  # GET /jcc_characteristics.json
  def index
    @jcc_characteristics = JccCharacteristic.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @jcc_characteristics }
    end
  end

  # GET /jcc_characteristics/1
  # GET /jcc_characteristics/1.json
  def show
    @jcc_characteristic = JccCharacteristic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @jcc_characteristic }
    end
  end

  # GET /jcc_characteristics/new
  # GET /jcc_characteristics/new.json
  def new
    @jcc_characteristic = JccCharacteristic.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @jcc_characteristic }
    end
  end

  # GET /jcc_characteristics/1/edit
  def edit
    @jcc_characteristic = JccCharacteristic.find(params[:id])
  end

  # POST /jcc_characteristics
  # POST /jcc_characteristics.json
  def create
    @jcc_characteristic = JccCharacteristic.new(params[:jcc_characteristic])

    respond_to do |format|
      if @jcc_characteristic.save
        format.html { redirect_to @jcc_characteristic, notice: 'Jcc characteristic was successfully created.' }
        format.json { render json: @jcc_characteristic, status: :created, location: @jcc_characteristic }
      else
        format.html { render action: "new" }
        format.json { render json: @jcc_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /jcc_characteristics/1
  # PUT /jcc_characteristics/1.json
  def update
    @jcc_characteristic = JccCharacteristic.find(params[:id])

    respond_to do |format|
      if @jcc_characteristic.update_attributes(params[:jcc_characteristic])
        format.html { redirect_to @jcc_characteristic, notice: 'Jcc characteristic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @jcc_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jcc_characteristics/1
  # DELETE /jcc_characteristics/1.json
  def destroy
    @jcc_characteristic = JccCharacteristic.find(params[:id])
    @jcc_characteristic.destroy

    respond_to do |format|
      format.html { redirect_to jcc_characteristics_url }
      format.json { head :no_content }
    end
  end
end
