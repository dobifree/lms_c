class HrProcessTemplatesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @hr_process_templates = HrProcessTemplate.all

  end


  def show
    @hr_process_template = HrProcessTemplate.find(params[:id])

    @hr_process_dimensions = @hr_process_template.hr_process_dimensions

=begin
    if @hr_process_template.hr_processes.first
      redirect_to @hr_process_template.hr_processes.first
    end
=end

  end

  def new
    @hr_process_template = HrProcessTemplate.new
  end

  def edit
    @hr_process_template = HrProcessTemplate.find(params[:id])
  end

  def delete
    @hr_process_template = HrProcessTemplate.find(params[:id])
  end

  def create
    @hr_process_template = HrProcessTemplate.new(params[:hr_process_template])

    if @hr_process_template.save
      flash[:success] = t('activerecord.success.model.hr_process_template.create_ok')
      redirect_to hr_process_templates_path
    else
      render action: 'new'
    end
  end

  def update

    @hr_process_template = HrProcessTemplate.find(params[:id])

    if @hr_process_template.update_attributes(params[:hr_process_template])
      flash[:success] = t('activerecord.success.model.hr_process_template.update_ok')
      redirect_to hr_process_templates_path
    else
      render action: 'edit'
    end


  end



  def destroy

    @hr_process_template = HrProcessTemplate.find(params[:id])

    if verify_recaptcha

      if @hr_process_template.destroy
        flash[:success] = t('activerecord.success.model.hr_process_template.delete_ok')
        redirect_to hr_process_templates_url
      else
        flash[:danger] = t('activerecord.success.model.hr_process_template.delete_error')
        redirect_to delete_hr_process_template_path(@hr_process_template)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.hr_process_template.captcha_error')
      redirect_to delete_hr_process_template_path(@hr_process_template)

    end

  end

  def create_quadrant

    @hr_process_template = HrProcessTemplate.find(params[:id])

    hr_process_quadrant = @hr_process_template.hr_process_quadrants.build(params[:hr_process_quadrant])
    hr_process_quadrant.hr_process_dimension_g_x_id = params[:hr_process_dimension_g_x_id] if params[:hr_process_dimension_g_x_id]
    hr_process_quadrant.hr_process_dimension_g_y_id = params[:hr_process_dimension_g_y_id] if params[:hr_process_dimension_g_y_id]

    if hr_process_quadrant.save

      flash[:success] = t('activerecord.success.model.hr_process_template.create_quadrant_ok')
=begin
      if params[:hr_process_dimension_g_id1]
        hr_process_quadrant_g = hr_process_quadrant.hr_process_quadrant_gs.new
        hr_process_quadrant_g.hr_process_dimension_g_id = params[:hr_process_dimension_g_id1]
        hr_process_quadrant_g.save

      end

      if params[:hr_process_dimension_g_id2]

        hr_process_quadrant_g = hr_process_quadrant.hr_process_quadrant_gs.new
        hr_process_quadrant_g.hr_process_dimension_g_id = params[:hr_process_dimension_g_id2]
        hr_process_quadrant_g.save

      end
=end
    else
      flash[:danger] = t('activerecord.error.model.hr_process_template.create_quadrant_error')
    end

    redirect_to @hr_process_template

  end

  def update_quadrant

    @hr_process_template = HrProcessTemplate.find(params[:id])

    hr_process_quadrant = HrProcessQuadrant.find(params[:hr_process_quadrant_id])

    if hr_process_quadrant.update_attributes(params[:hr_process_quadrant])

      flash[:success] = t('activerecord.success.model.hr_process_template.update_quadrant_ok')

    else
      flash[:danger] = t('activerecord.error.model.hr_process_template.update_quadrant_error')
    end

    redirect_to @hr_process_template

  end


end
