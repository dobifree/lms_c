class PeEvaluationGroupsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin



  # GET /pe_evaluation_groups/new
  # GET /pe_evaluation_groups/new.json
  def new

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_evaluation_group = @pe_process.pe_evaluation_groups.build

  end

  # GET /pe_evaluation_groups/1/edit
  def edit
    @pe_evaluation_group = PeEvaluationGroup.find(params[:id])
    @pe_process = @pe_evaluation_group.pe_process
  end

  # POST /pe_evaluation_groups
  # POST /pe_evaluation_groups.json
  def create
    @pe_evaluation_group = PeEvaluationGroup.new(params[:pe_evaluation_group])

    if @pe_evaluation_group.save

      if params['pe_evaluations_ids']

        params['pe_evaluations_ids'].each do |pe_evaluation_id|

          pe_evaluation = PeEvaluation.find(pe_evaluation_id)
          pe_evaluation.pe_evaluation_group = @pe_evaluation_group
          pe_evaluation.save

        end

      end


      redirect_to pe_process_tab_path(@pe_evaluation_group.pe_process,'evaluations')

    else
      @pe_process = @pe_evaluation_group.pe_process
      render action: 'new'

    end

  end

  # PUT /pe_evaluation_groups/1
  # PUT /pe_evaluation_groups/1.json
  def update

    @pe_evaluation_group = PeEvaluationGroup.find(params[:id])


    if @pe_evaluation_group.update_attributes(params[:pe_evaluation_group])

      @pe_evaluation_group.pe_process.pe_evaluations.each do |pe_evaluation|

        if pe_evaluation.pe_evaluation_group_id == @pe_evaluation_group.id

          pe_evaluation.pe_evaluation_group = nil
          pe_evaluation.save
        end

      end

      if params['pe_evaluations_ids']

        params['pe_evaluations_ids'].each do |pe_evaluation_id|

          pe_evaluation = PeEvaluation.find(pe_evaluation_id)
          pe_evaluation.pe_evaluation_group = @pe_evaluation_group
          pe_evaluation.save

        end

      end

      redirect_to pe_process_tab_path(@pe_evaluation_group.pe_process,'evaluations')

    else
      @pe_process = @pe_evaluation_group.pe_process
      render action: 'new'
    end

  end

  # DELETE /pe_evaluation_groups/1
  # DELETE /pe_evaluation_groups/1.json
  def destroy
    @pe_evaluation_group = PeEvaluationGroup.find(params[:id])
    @pe_evaluation_group.destroy

    respond_to do |format|
      format.html { redirect_to pe_evaluation_groups_url }
      format.json { head :no_content }
    end
  end
end
