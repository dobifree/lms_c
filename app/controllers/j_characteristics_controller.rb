class JCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @j_characteristics = JCharacteristic.all
  end

  def show
    @j_characteristic = JCharacteristic.find(params[:id])
  end

  def new
    @j_characteristic = JCharacteristic.new
  end

  def edit
    @j_characteristic = JCharacteristic.find(params[:id])
  end

  def create
    @j_characteristic = JCharacteristic.new(params[:j_characteristic])

    if @j_characteristic.save
      redirect_to j_characteristics_path, notice: 'J characteristic was successfully created.'
    else
      render action: 'new'
    end

  end

  # PUT /j_characteristics/1
  # PUT /j_characteristics/1.json
  def update
    @j_characteristic = JCharacteristic.find(params[:id])

    respond_to do |format|
      if @j_characteristic.update_attributes(params[:j_characteristic])
        format.html { redirect_to @j_characteristic, notice: 'J characteristic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @j_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /j_characteristics/1
  # DELETE /j_characteristics/1.json
  def destroy
    @j_characteristic = JCharacteristic.find(params[:id])
    @j_characteristic.destroy

    redirect_to j_characteristics_url

  end
  
end
