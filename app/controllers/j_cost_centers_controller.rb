class JCostCentersController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def index
    @j_cost_centers = JCostCenter.all
  end

  def show
    @j_cost_center = JCostCenter.find(params[:id])
  end

  def new
    @j_cost_center = JCostCenter.new
  end

  def edit
    @j_cost_center = JCostCenter.find(params[:id])
  end

  def create

    params[:j_cost_center][:budget] = fix_form_floats params[:j_cost_center][:budget]

    @j_cost_center = JCostCenter.new(params[:j_cost_center])

    if @j_cost_center.save

      CcCharacteristic.all.each do |cc_characteristic|

        jcc_characteristic = @j_cost_center.jcc_characteristics.build
        jcc_characteristic.characteristic = cc_characteristic.characteristic

        if cc_characteristic.characteristic.data_type_id == 0
          jcc_characteristic.value_string = params['cc_characteristic_'+cc_characteristic.id.to_s]
        elsif
          jcc_characteristic.characteristic_value_id = params['cc_characteristic_'+cc_characteristic.id.to_s]
        end

        jcc_characteristic.save

      end

      flash[:success] = t('activerecord.success.model.j_cost_center.create_ok')
      redirect_to j_cost_centers_path
    else
      render action: 'new'
    end

  end

  # PUT /j_cost_centers/1
  # PUT /j_cost_centers/1.json
  def update
    @j_cost_center = JCostCenter.find(params[:id])

    params[:j_cost_center][:budget] = fix_form_floats params[:j_cost_center][:budget]

    if @j_cost_center.update_attributes(params[:j_cost_center])

      @j_cost_center.jcc_characteristics.destroy_all

      CcCharacteristic.all.each do |cc_characteristic|

        jcc_characteristic = @j_cost_center.jcc_characteristics.build
        jcc_characteristic.characteristic = cc_characteristic.characteristic

        if cc_characteristic.characteristic.data_type_id == 0
          jcc_characteristic.value_string = params['cc_characteristic_'+cc_characteristic.id.to_s]
        elsif
        jcc_characteristic.characteristic_value_id = params['cc_characteristic_'+cc_characteristic.id.to_s]
        end

        jcc_characteristic.save

      end

      flash[:success] = t('activerecord.success.model.j_cost_center.update_ok')
      redirect_to j_cost_centers_path
    else
      render action: 'edit'
    end

  end

  # DELETE /j_cost_centers/1
  # DELETE /j_cost_centers/1.json
  def destroy
    @j_cost_center = JCostCenter.find(params[:id])
    @j_cost_center.destroy

    respond_to do |format|
      format.html { redirect_to j_cost_centers_url }
      format.json { head :no_content }
    end
  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'jobs', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

end
