class PeSelectionController < ApplicationController

  include PeAssessmentHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list, :search, :add]
  before_filter :get_data_3, only: [:add]
  before_filter :get_data_2, only: [:remove]
  before_filter :validate_open_process
  before_filter :validate_access_to_select
  before_filter :validate_can_be_added, only: [:add]
  before_filter :validate_can_be_removed, only: [:remove]

  def people_list


  end

  def search

    @user = User.new

    if params[:user]
      @pe_members = @pe_process.search_can_be_selected_as_evaluated_members_ordered('', params[:user][:apellidos], params[:user][:nombre])
      #@user.codigo = 'params[:user][:codigo]'
      @user.apellidos = params[:user][:apellidos]
      @user.nombre = params[:user][:nombre]
    end

  end

  def add

    pe_member_rel = @pe_member_connected.pe_member_rels_is_evaluator.build
    pe_member_rel.pe_process = @pe_process
    pe_member_rel.pe_member_evaluated = @pe_member_to_be_added

    if @pe_process.pe_rels_available_to_select.size == 1

      pe_rel = @pe_process.pe_rels_available_to_select.first

    else

      pe_rel = PeRel.find(params[:pe_rel][:id]) unless params[:pe_rel][:id].blank?

    end

    if @pe_process.pe_rels_available_to_select.include? pe_rel

      pe_member_rel.pe_rel = pe_rel

      if pe_member_rel.save

        @pe_member_to_be_added.is_evaluated = true
        @pe_member_to_be_added.save

        open_the_whole_assessment @pe_member_to_be_added

        @pe_member_connected.is_evaluator = true
        @pe_member_connected.save

        flash[:success] = t('activerecord.success.model.pe_selection.add_ok')
        redirect_to pe_selection_people_list_path @pe_process

      else
        @user = User.new

        render 'search'

      end

    else
      redirect_to pe_selection_people_list_path @pe_process
    end

  end

  def remove

    pe_member_rel = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_connected

    if pe_member_rel && !pe_member_rel.finished && @pe_process.pe_rels_available_to_select.include?(pe_member_rel.pe_rel)

      @pe_process.pe_evaluations.each do |pe_evaluation|

        pe_evaluation.pe_questions_by_pe_member_evaluated_evaluator_all_levels(@pe_member_evaluated, @pe_member_connected).destroy_all

      end

      pe_member_rel.destroy

      @pe_member_evaluated.reload

      @pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

        calculate_assessment_final_evaluation @pe_member_evaluated, pe_evaluation

        calculate_assessment_dimension @pe_member_evaluated, pe_evaluation.pe_dimension

      end

      @pe_member_evaluated.set_pe_box
      @pe_member_evaluated.save

      calculate_assessment_groups @pe_member_evaluated

      finish_the_whole_assessment @pe_member_evaluated

      if @pe_member_evaluated.pe_member_rels_is_evaluated.size == 0
        @pe_member_evaluated.is_evaluated = false
        @pe_member_evaluated.save
      end

      if @pe_member_connected.pe_member_rels_is_evaluator.size == 0
        @pe_member_connected.is_evaluator = false
        @pe_member_connected.save
      end

      flash[:success] = t('activerecord.success.model.pe_selection.remove_ok')

    else


    end


    redirect_to pe_selection_people_list_path @pe_process

  end

  private

  def get_data_1

    @pe_process = PeProcess.find params[:pe_process_id]

    @pe_member_connected = @pe_process.pe_member_by_user user_connected

  end

  def get_data_2

    @pe_member_evaluated = PeMember.find(params[:pe_member_id])

    @pe_process = @pe_member_evaluated.pe_process

    @pe_member_connected = @pe_process.pe_member_by_user user_connected

  end

  def get_data_3

    @pe_process = PeProcess.find params[:pe_process_id]
    @pe_member_connected = @pe_process.pe_member_by_user user_connected
    @pe_member_to_be_added = PeMember.find(params[:pe_member_id])

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_access_to_select

    unless @pe_process.has_step_selection? && @pe_member_connected.can_select_evaluated
      flash[:danger] = t('activerecord.error.model.pe_process_selection.cant_select')
      redirect_to root_path
    end

  end

  def validate_can_be_added

    unless @pe_member_to_be_added.can_be_selected_as_evaluated
      flash[:danger] = t('activerecord.error.model.pe_process_selection.cant_select')
      redirect_to root_path
    end

  end

  def validate_can_be_removed
    unless @pe_member_evaluated.can_be_selected_as_evaluated
      flash[:danger] = t('activerecord.error.model.pe_process_selection.cant_select')
      redirect_to root_path
    end
  end

end
