class UserProfileController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include ExcelReportsHelper


  include PeReportingHelper

  before_filter :authenticate_user
  before_filter :get_data_1
  before_filter :get_data_2, only: [:download_characteristic_file]
  before_filter :get_data_3, only: [:edit_characteristics, :update_characteristics]
  before_filter :get_data_4, only: [:pe_rep_detailed_final_results, :pe_rep_detailed_final_results_pdf]
  before_filter :get_data_5, only: [:download_user_characteristic_file]

  before_filter :validate_characteristic_file, only: [:download_characteristic_file, :download_user_characteristic_file]

  def edit_photo
    @user = user_connected
   #@company = session[:company]
  end

  def upload_photo
    @user = user_connected
   #@company = session[:company]
    if params[:upload]
      ext = File.extname(params[:upload]['datafile'].original_filename)
      if ext != '.jpg' && ext != '.jpeg'
        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_format')
        render action: 'edit_photo'

      elsif File.size(params[:upload]['datafile'].tempfile) > 70.kilobytes
        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_wrong_size')
        render action: 'edit_photo'
      else
        save_photo params[:upload], @user, @company
        flash[:success] = t('activerecord.success.model.user.update_foto')
        redirect_to show_user_profile_path
      end
    else
      flash[:danger] = t('activerecord.error.model.user.update_foto_empty_file')
      render action: 'edit_photo'
    end
  end


  def show_profile

  end

  def download_characteristic_file

    directory = @company.directorio + '/' + @company.codigo + '/user_profiles/' + user_connected.id.to_s + '/characteristics/' + @characteristic.id.to_s + '/'


    send_file directory + @user_characteristic.value_file,
              filename: @characteristic.nombre + '_' + @user_characteristic.value_file,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_user_characteristic_file

    directory = @company.directorio + '/' + @company.codigo + '/user_profiles/' + user_connected.id.to_s + '/characteristics/' + @characteristic.id.to_s + '/'


    send_file directory + @user_characteristic.value_file,
              filename: @characteristic.nombre + '_' + @user_characteristic.value_file,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def edit_characteristics

  end

  def update_characteristics

    if @characteristic_type
      characteristics = @characteristic_type.characteristics_updated_by_users
    else
      characteristics = Characteristic.not_grouped_characteristics_updated_by_users
    end

    characteristics.each do |characteristic|

      new_value = nil

      if characteristic.data_type_id == 11

        #register

        current_ucs_ids = Array.new

        user_connected.user_characteristics_registered(characteristic).each {|c_uc| current_ucs_ids.push c_uc.id.to_s}

        if params['characteristic_' + characteristic.id.to_s + '_create']

          n_aux_files = Array.new

          characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|
            n_aux_files[index_element] = 0
          end

          params['characteristic_' + characteristic.id.to_s + '_create'].each_with_index do |c_id, n_aux|

            if c_id == '-1'

              error, uc = user_connected.add_user_characteristic(characteristic, '-', lms_time, user_connected, user_connected, nil, nil, @company)

              characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                new_value = nil

                unless element_characteristic.data_type_id == 6
                  new_value = params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s).to_sym][n_aux].blank?
                else
                  #file

                  if params[('characteristic_file_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)][n_aux] == '1'

                    new_value = params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)] && params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)][n_aux_files[index_element]]

                    n_aux_files[index_element] += 1

                  end

                end

                if new_value

                  error_in, uc_element = user_connected.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                  if !error_in && uc_element

                    uc_element.register_user_characteristic = uc
                    uc_element.save

                  end

                end

              end

            else

              current_ucs_ids.delete(c_id)

              uc = user_connected.user_characteristics.where('id = ?', c_id).first

              characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                new_value = nil

                uc_element = uc.elements_user_characteristics_by_characteristic(element_characteristic)

                unless element_characteristic.data_type_id == 6

                  uc_element.destroy if uc_element

                  new_value = params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s).to_sym][n_aux].blank?

                  error_in, uc_element = user_connected.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                  if !error_in && uc_element

                    uc_element.register_user_characteristic = uc
                    uc_element.save

                  end

                else
                  #file

                  if params[('characteristic_file_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)][n_aux] == '1'

                    new_value = params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)] && params[('characteristic_' + characteristic.id.to_s + '_' + element_characteristic.id.to_s)][n_aux_files[index_element]]

                    if new_value

                      n_aux_files[index_element] += 1

                      uc_element.destroy if uc_element

                      error_in, uc_element = user_connected.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                      if !error_in && uc_element

                        uc_element.register_user_characteristic = uc
                        uc_element.save

                      end

                    end

                  end

                end

              end

            end

          end

        end

        current_ucs_ids.each {|current_uc_id| user_connected.user_characteristics.where('id = ?', current_uc_id).destroy_all}

      else

        unless characteristic.data_type_id == 6
          new_value = params[('characteristic_' + characteristic.id.to_s).to_sym].strip unless params[('characteristic_' + characteristic.id.to_s).to_sym].blank?
        else
          new_value = params[('characteristic_' + characteristic.id.to_s).to_sym]
        end

        #from_date = params[('characteristic_'+characteristic.id.to_s)+'_from'].to_datetime

        user_connected.update_user_characteristic(characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

      end

    end

    flash[:success] = t('activerecord.success.model.user_profile.update_characteristics_ok')
    if @characteristic_type
      redirect_to show_user_profile_after_update_path(@characteristic_type)
    else
      redirect_to show_user_profile_path
    end

  end

  def download_informative_document

    document = InformativeDocument.find(params[:document_id])
    profile = document.reader_profile(user_connected, user_connected)
    read_permission = document.profile_has_permission(profile)

    if read_permission
     company = @company

      file = company.directorio + '/' + company.codigo + '/informative_documents/' + document.id.to_s + '/' + user_connected.codigo + '.pdf'

      send_file file,
                filename: document.name + '.pdf',
                type: 'application/octet-stream',
                disposition: 'attachment'
    else
      flash[:danger] = 'Usted no tiene los privilegios necesarios para acceder a este documento.'
      redirect_to root_path
    end
  end

  def pe_rep_detailed_final_results


  end

  def pe_rep_detailed_final_results_pdf


    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.pdf'
    archivo_temporal = directorio_temporal + nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_for_user_profile, @pe_process.rep_detailed_final_results_show_calibration_for_user_profile, (@pe_process.public_pe_box && @pe_process.public_pe_members_box), (@pe_process.public_pe_dimension_name && @pe_process.public_pe_members_dimension_name), true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - ' + @pe_member.user.codigo + '.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

=begin
  def update_characteristics_old

    Characteristic.where('updated_by_user = ?', true).each do |characteristic|

      uc = user_connected.user_characteristic(characteristic)

      if uc

        if params[('characteristic_'+characteristic.id.to_s).to_sym].blank?
          uc.destroy if characteristic.data_type_id != 6
        else
          uc.valor = params[('characteristic_'+characteristic.id.to_s).to_sym].strip unless characteristic.data_type_id == 6

          if characteristic.data_type_id == 0
            #string
            uc.value_string = params[('characteristic_'+characteristic.id.to_s).to_sym]
          elsif characteristic.data_type_id == 1
            #text
            uc.value_text = params[('characteristic_'+characteristic.id.to_s).to_sym]
          elsif characteristic.data_type_id == 2
            #date
            uc.value_date = params[('characteristic_'+characteristic.id.to_s).to_sym]
          elsif characteristic.data_type_id == 3
            #int
            uc.value_int = params[('characteristic_'+characteristic.id.to_s).to_sym]
          elsif characteristic.data_type_id == 4
            #float
            uc.value_float = params[('characteristic_'+characteristic.id.to_s).to_sym]
          elsif characteristic.data_type_id == 5
            #lista
            uc.characteristic_value = characteristic.characteristic_values.where('id = ?',params[('characteristic_'+characteristic.id.to_s).to_sym]).first
          elsif characteristic.data_type_id == 6
            #file

            if params[('characteristic_'+characteristic.id.to_s).to_sym]

              directory = @company.directorio+'/'+@company.codigo+'/user_profiles/'+user_connected.id.to_s+'/characteristics/'+characteristic.id.to_s+'/'

              FileUtils.remove_dir directory if File.directory? directory

              FileUtils.mkdir_p directory

              name = characteristic.id.to_s+'_'+SecureRandom.hex(5)+File.extname(params[('characteristic_'+characteristic.id.to_s).to_sym].original_filename)
              path = File.join(directory, name)
              File.open(path, 'wb') { |f| f.write(params[('characteristic_'+characteristic.id.to_s).to_sym].read) }

              uc.value_file = name

              uc.valor = name

            end

          end

          uc.save
        end

      elsif !params[('characteristic_'+characteristic.id.to_s).to_sym].blank?

        uc = user_connected.user_characteristics.build
        uc.characteristic = characteristic

        uc.valor = params[('characteristic_'+characteristic.id.to_s).to_sym].strip unless characteristic.data_type_id == 6

        if characteristic.data_type_id == 0
          #string
          uc.value_string = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 1
          #text
          uc.value_text = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 2
          #date
          uc.value_date = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 3
          #int
          uc.value_int = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 4
          #float
          uc.value_float = params[('characteristic_'+characteristic.id.to_s).to_sym]
        elsif characteristic.data_type_id == 5
          #lista
          uc.characteristic_value = characteristic.characteristic_values.where('id = ?',params[('characteristic_'+characteristic.id.to_s).to_sym]).first
        elsif characteristic.data_type_id == 6
          #file

          if params[('characteristic_'+characteristic.id.to_s).to_sym]

            directory = @company.directorio+'/'+@company.codigo+'/user_profiles/'+user_connected.id.to_s+'/characteristics/'+characteristic.id.to_s+'/'

            FileUtils.remove_dir directory if File.directory? directory

            FileUtils.mkdir_p directory

            name = characteristic.id.to_s+'_'+SecureRandom.hex(5)+File.extname(params[('characteristic_'+characteristic.id.to_s).to_sym].original_filename)
            path = File.join(directory, name)
            File.open(path, 'wb') { |f| f.write(params[('characteristic_'+characteristic.id.to_s).to_sym].read) }

            uc.value_file = name

            uc.valor = name

          end

        end

        uc.save

      end

    end

    flash[:success] = t('activerecord.success.model.user_profile.update_characteristics_ok')
    redirect_to show_user_profile_path

  end
=end
  private

  def get_data_1

   #@company = session[:company]

  end

  def get_data_2

    @characteristic = Characteristic.find_by_id(params[:characteristic_id])
    @user_characteristic = user_connected.user_characteristics.find_by_characteristic_id(params[:characteristic_id])

  end

  def get_data_3
    @characteristic_type = CharacteristicType.find(params[:characteristic_type_id]) if params[:characteristic_type_id]
  end

  def get_data_4

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_member = @pe_process.pe_member_by_user user_connected

  end

  def get_data_5

    @user_characteristic = user_connected.user_characteristics.find_by_id(params[:user_characteristic_id])
    @characteristic = @user_characteristic ? @user_characteristic.characteristic : nil

  end

  def validate_characteristic_file

    unless @characteristic && @characteristic.data_type_id == 6 && @user_characteristic && @user_characteristic.value_file
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end

  end

  def save_photo(upload, user, company)

    ext = File.extname(upload['datafile'].original_filename)

    if ext == '.jpg' || ext == '.jpeg'

      if File.size(upload['datafile'].tempfile) <= 70.kilobytes

        directory = company.directorio + '/' + company.codigo + '/fotos_usuarios/'

        FileUtils.mkdir_p directory unless File.directory? directory

        if user.foto
          file = directory + user.foto
          File.delete(file) if File.exist? file
        end

        user.set_foto

        user.save


        file = directory + user.foto

        File.open(file, 'wb') {|f| f.write(upload['datafile'].read)}

      end

    end

  end
end
