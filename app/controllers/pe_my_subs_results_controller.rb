class PeMySubsResultsController < ApplicationController

  include PeReportingHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:my_subs_results]
  before_filter :get_data_2, only: [:rep_detailed_final_results_person, :rep_detailed_final_results_pdf]

  before_filter :validate_open_process
  before_filter :validate_has_step_my_subs_results

  before_filter :validate_is_boss_of, only: [:rep_detailed_final_results_person, :rep_detailed_final_results_pdf]


  def my_subs_results


  end

  def rep_detailed_final_results_person


  end

  def rep_detailed_final_results_pdf

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_for_my_people, @pe_process.rep_detailed_final_results_show_calibration_for_my_people, @pe_process.public_pe_box, @pe_process.public_pe_dimension_name, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private


  def get_data_1

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_members_connected = @pe_process.pe_members.where('user_id = ?', user_connected.id)

    @pe_rel = @pe_process.pe_rel_by_id 1

    @pe_member_rels = Array.new

    @pe_members_connected.each do |pe_member_connected|

      @pe_member_rels += pe_member_connected.pe_member_rels_is_evaluator_as_pe_rel_ordered(@pe_rel)

    end

   #@company = session[:company]

  end

  def get_data_2

    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_rel = @pe_member_rel.pe_rel
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member = @pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
      end

    end

   #@company = session[:company]

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_has_step_my_subs_results

    unless @pe_process.has_step_my_subs_results?
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_is_boss_of

    unless @pe_member_rel.pe_rel.rel == 1
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

end
