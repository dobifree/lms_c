class JmListsController < ApplicationController
  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /jm_lists
  # GET /jm_lists.json
  def index
    @jm_lists = JmList.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @jm_lists }
    end
  end

  # GET /jm_lists/1
  # GET /jm_lists/1.json
  def show
    @jm_list = JmList.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @jm_list }
    end
  end

  # GET /jm_lists/new
  # GET /jm_lists/new.json
  def new
    @jm_list = JmList.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @jm_list }
    end
  end

  # GET /jm_lists/1/edit
  def edit
    @jm_list = JmList.find(params[:id])
  end

  # POST /jm_lists
  # POST /jm_lists.json
  def create
    @jm_list = JmList.new(params[:jm_list])

    respond_to do |format|
      if @jm_list.save
        format.html { redirect_to jm_lists_path, notice: 'Jm list was successfully created.' }
        format.json { render json: @jm_list, status: :created, location: @jm_list }
      else
        format.html { render action: "new" }
        format.json { render json: @jm_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /jm_lists/1
  # PUT /jm_lists/1.json
  def update
    @jm_list = JmList.find(params[:id])

    respond_to do |format|
      if @jm_list.update_attributes(params[:jm_list])
        format.html { redirect_to jm_lists_path, notice: 'Jm list was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @jm_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jm_lists/1
  # DELETE /jm_lists/1.json
  def destroy
    @jm_list = JmList.find(params[:id])
    @jm_list.destroy

    respond_to do |format|
      format.html { redirect_to jm_lists_url }
      format.json { head :no_content }
    end
  end
end
