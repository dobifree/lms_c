class JGenericSkillsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def index
    @j_generic_skills = JGenericSkill.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @j_generic_skills }
    end
  end

  # GET /j_generic_skills/1
  # GET /j_generic_skills/1.json
  def show
    @j_generic_skill = JGenericSkill.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @j_generic_skill }
    end
  end

  def new
    @j_generic_skill = JGenericSkill.new

    @j_generic_skill.j_job_level_id = params[:j_job_level_id]
    @j_job_level = JJobLevel.find(@j_generic_skill.j_job_level_id)


  end


  def edit
    @j_generic_skill = JGenericSkill.find(params[:id])
    @j_job_level = @j_generic_skill.j_job_level
  end

  def create
    @j_generic_skill = JGenericSkill.new(params[:j_generic_skill])


    if @j_generic_skill.save
      flash[:success] = t('activerecord.success.model.j_generic_skill.create_ok')
      redirect_to @j_generic_skill.j_job_level
    else
      @j_job_level = JJobLevel.find(params[:j_generic_skill][:j_job_level_id])
      render action: 'new'
    end

  end


  def update
    @j_generic_skill = JGenericSkill.find(params[:id])

    if @j_generic_skill.update_attributes(params[:j_generic_skill])
      flash[:success] = t('activerecord.success.model.j_generic_skill.update_ok')
      redirect_to @j_generic_skill.j_job_level
    else
      @j_job_level = @j_generic_skill.j_job_level
      render action: 'edit'
    end

  end

  def destroy

    @j_generic_skill = JGenericSkill.find(params[:id])
    j_job_level = @j_generic_skill.j_job_level
    @j_generic_skill.destroy

    flash[:success] = t('activerecord.success.model.j_generic_skill.delete_ok')
    redirect_to j_job_level

  end


  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'jobs', true).first

      unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end

end
