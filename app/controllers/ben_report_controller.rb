class BenReportController < ApplicationController
  include BenReportHelper
  before_filter :verify_access_manage_module, only: [:index,
                                                     :new,
                                                     :create,
                                                     :load_characteristics_values_list,
                                                     :edit,
                                                     :update,
                                                     :delete ]

  before_filter :verify_is_reporter, only: [:list_reports, :gen_custom_report]
  before_filter :verify_close_process, only: [:gen_custom_report]

  def index
  end

  def new
    #SI SE AUMENTA UN NAME PARA BEN CUSTOM REPORT ATTRIBUTES. SE DEBE AUMENTAR EN
    # LA PLANTILLA: BEN_GEN_CUSTOM_REPORT.XLSX.AXLSX
    # EN HELPER: filter_bills_attributes(bills, report_id)
    # EN MODELO: BEN_CUSTOM_REPORT_ATTRIBUTE_FILTER
    @report = BenCustomReport.new
    @report.ben_custom_report_attributes.new(name: 'Numero de celular')
    @report.ben_custom_report_attributes.new(name: 'Folio')
    @report.ben_custom_report_attributes.new(name: 'RUT de compañia')
    @report.ben_custom_report_attributes.new(name: 'Descripción')
    @report.ben_custom_report_attributes.new(name: 'Valor neto')
    @report.ben_custom_report_attributes.new(name: 'Descuento Movistar')
    @report.ben_custom_report_attributes.new(name: 'Neto total')
    @report.ben_custom_report_attributes.new(name: 'Neto con IVA')
    @report.ben_custom_report_attributes.new(name: 'Tipo de factura')
    @report.ben_custom_report_attributes.new(name: 'Cargar a Payroll')
    @report.ben_custom_report_attributes.new(name: 'Registrado manualmente')
    @report.ben_custom_report_attributes.new(name: '¿Por qué el registro manual?')
    @report.ben_custom_report_attributes.new(name: 'Errores del registro')
    @report.ben_custom_report_attributes.new(name: 'Mes del proceso')
    @report.ben_custom_report_attributes.new(name: 'Año del proceso')
  end

  def create
    @report = BenCustomReport.new(params[:ben_custom_report])
    if @report.save
      if params[:select2_bill_types]
        BenCustomReportAttributeFilter.where(ben_custom_report_id: @report.id, name: 'Tipo de factura').destroy_all
        params[:select2_bill_types].each do |bill_type|
          BenCustomReportAttributeFilter.create(ben_custom_report_id: @report.id, name: 'Tipo de factura', match_value_int: bill_type.to_i)
        end
      end

      if params[:select2_company_rut]
        BenCustomReportAttributeFilter.where(ben_custom_report_id: @report.id, name: 'RUT de compañia').destroy_all
        params[:select2_company_rut].each do |company_rut|
          BenCustomReportAttributeFilter.create(ben_custom_report_id: @report.id, name: 'RUT de compañia', match_value_string: company_rut.to_s)
        end
      end

      if params[:select2_charge_to_payroll]
        BenCustomReportAttributeFilter.where(ben_custom_report_id: @report.id, name: 'Cargar a Payroll').destroy_all
        BenCustomReportAttributeFilter.create(ben_custom_report_id: @report.id, name: 'Cargar a Payroll', match_value_boolean: params[:select2_charge_to_payroll].to_s == '1' ? true : false)
      end

      flash[:success] = 'Cambio realizado correctamente'
      redirect_to ben_custom_reports_index_path
    else
      render action: 'new'
    end
  end

  def load_characteristics_values_list
    values = CharacteristicValue.where(:characteristic_id =>  params[:characteristic_id]).reorder(:value_string).map{|char_value| [char_value.value_string, char_value.id]}
    render json: values
  end

  def edit
    @report = BenCustomReport.find(params[:ben_custom_report_id])
  end

  def update

    @report = BenCustomReport.find(params[:ben_custom_report_id])
    if @report.update_attributes(params[:ben_custom_report])

      BenCustomReportAttributeFilter.where(ben_custom_report_id: @report.id, name: 'Tipo de factura').destroy_all
      if params[:select2_bill_types]
        params[:select2_bill_types].each do |bill_type|
          BenCustomReportAttributeFilter.create(ben_custom_report_id: @report.id, name: 'Tipo de factura', match_value_int: bill_type.to_i)
        end
      end

      BenCustomReportAttributeFilter.where(ben_custom_report_id: @report.id, name: 'RUT de compañia').destroy_all
      if params[:select2_company_rut]
        params[:select2_company_rut].each do |company_rut|
          BenCustomReportAttributeFilter.create(ben_custom_report_id: @report.id, name: 'RUT de compañia', match_value_string: company_rut.to_s)
        end
      end

      BenCustomReportAttributeFilter.where(ben_custom_report_id: @report.id, name: 'Cargar a Payroll').destroy_all
      if params[:select2_charge_to_payroll]
        BenCustomReportAttributeFilter.create(ben_custom_report_id: @report.id, name: 'Cargar a Payroll', match_value_boolean: params[:select2_charge_to_payroll].to_s == '1' ? true : false)
      end
      flash[:success] = 'Cambio realizado correctamente'
      redirect_to ben_custom_reports_index_path
    else
      render action: 'edit'
    end
  end


  def delete
    report = BenCustomReport.find(params[:ben_custom_report_id])
    report.destroy
    flash[:success] = 'Se eliminó reporte correctamete'
    redirect_to ben_custom_reports_index_path
  end

  def gen_custom_report
    if reporter_of_this?(params[:ben_custom_report_id]) || manager?
      process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])
      @only_active = params[:only_active] == 1
      @report = BenCustomReport.find(params[:ben_custom_report_id])

      bills_self_value = bills_by_self_value(process.id, @report.id)
      bills_user_connected = bills_by_user_connected(process.id, @report.id)

      if bills_self_value.size > 0 && bills_user_connected.size > 0
        bills_self_value.delete_if {|bill| !bills_user_connected.include?(bill)}
        @bills = bills_self_value
      else
        if bills_self_value.size > 0
          @bills = bills_self_value
        end
        if bills_user_connected.size > 0
          @bills = bills_user_connected
        end
        unless @bills
          @bills = BenCellphoneBillItem.where(ben_cellphone_process_id: process.id)
        end
      end

      @bills = filter_bills_attributes(@bills, @report.id)

      @characteristics_custom_report = @report.ben_custom_report_characteristics.where(relation_type: 0)
      @bills_attributes = @report.ben_custom_report_attributes.where(active: true).reorder('position ASC')

      respond_to do |format|
        format.xlsx {render xlsx: 'ben_gen_custom_report.xlsx', filename: process.year.to_s+'/'+process.month.to_s+'-'+@report.name.to_s+'.xlsx'}
      end
    else
      flash[:danger] = 'No dispone de privilegios necesarios para generar este reporte'
      redirect_to ben_custom_reports_list_reports_path
    end
  end

  def list_reports
    if manager?
      @reports = BenCustomReport.all
    else
      user_reports = BenUserCustomReport.where(user_id: user_connected.id)
      @reports = []
      user_reports.each {|user_report| @reports << user_report.ben_custom_report}
      @reports.uniq_by(&:id)
    end
  end

  private

  def verify_access_manage_module
    return if manager?
    flash[:danger] = t('security.no_access_to_manage_module')
    redirect_to root_path
  end

  def verify_is_reporter
    reporter = BenUserCustomReport.where(user_id: user_connected.id).first
    return if reporter || manager?
    flash[:danger] = 'No dispone de privilegios necesarios para generar reportes de este módulo'
    redirect_to root_path
  end

  def verify_close_process
    unless BenCellphoneProcess.find(params[:ben_cellphone_process_id]).charged_to_payroll?
      flash[:danger] = 'Proceso abierto, no se puede hacer reporte'
      redirect_to ben_custom_reports_list_reports_path
    end
  end

  def reporter_of_this?(report_id)
    reporter = BenUserCustomReport.where(user_id: user_connected.id, ben_custom_report_id: report_id).first
    return true if reporter
    return false
  end

  def manager?
    ct_module = CtModule.where('cod = ? AND active = ?', 'ben_cel_sec', true).first
    return false unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
    return true
  end

end
