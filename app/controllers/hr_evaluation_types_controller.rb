class HrEvaluationTypesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def index
    @hr_evaluation_types = HrEvaluationType.all
  end


  def show
    @hr_evaluation_type = HrEvaluationType.find(params[:id])

    @hr_evaluation_type_elements = @hr_evaluation_type.hr_evaluation_type_elements
  end


  def new
    @hr_evaluation_type = HrEvaluationType.new


  end


  def edit
    @hr_evaluation_type = HrEvaluationType.find(params[:id])
  end

  def delete
    @hr_evaluation_type = HrEvaluationType.find(params[:id])
  end


  def create
    @hr_evaluation_type = HrEvaluationType.new(params[:hr_evaluation_type])

    if @hr_evaluation_type.save
      flash[:success] = t('activerecord.success.model.hr_evaluation_type.create_ok')
      redirect_to hr_evaluation_types_path
    else
      render action: 'new'
    end

  end


  def update
    @hr_evaluation_type = HrEvaluationType.find(params[:id])


    if @hr_evaluation_type.update_attributes(params[:hr_evaluation_type])
      flash[:success] = t('activerecord.success.model.hr_evaluation_type.update_ok')
      redirect_to hr_evaluation_types_path
    else
      render action: 'edit'
    end

  end

  def destroy

    @hr_evaluation_type = HrEvaluationType.find(params[:id])

    if verify_recaptcha

      if @hr_evaluation_type.destroy
        flash[:success] = t('activerecord.success.model.hr_evaluation_type.delete_ok')
        redirect_to hr_evaluation_types_url
      else
        flash[:danger] = t('activerecord.success.model.hr_evaluation_type.delete_error')
        redirect_to delete_hr_evaluation_type_path(@hr_evaluation_type)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.hr_evaluation_type.captcha_error')
      redirect_to delete_hr_evaluation_type_path(@hr_evaluation_type)

    end

  end

end
