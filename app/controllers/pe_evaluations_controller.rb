class PeEvaluationsController < ApplicationController

  include PeEvaluationsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process
  end

  def new

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_evaluation = @pe_process.pe_evaluations.build

  end

  def create
    @pe_evaluation = PeEvaluation.new(params[:pe_evaluation])

    @pe_evaluation.definition_by_users_rol = ''
    params[:pe_evaluation][:definition_by_users_rol].each { |e| @pe_evaluation.definition_by_users_rol += e+'-' }

    if @pe_evaluation.save

      @pe_evaluation.pe_process.pe_rels.each do |pe_rel|

        pe_evaluation_def_rel_message = @pe_evaluation.pe_evaluation_def_rel_messages.build
        pe_evaluation_def_rel_message.pe_rel = pe_rel
        pe_evaluation_def_rel_message.message = params['pe_evaluation_def_rel_message_'+pe_rel.id.to_s]
        pe_evaluation_def_rel_message.save

        pe_evaluation_track_rel_message = @pe_evaluation.pe_evaluation_track_rel_messages.build
        pe_evaluation_track_rel_message.pe_rel = pe_rel
        pe_evaluation_track_rel_message.message = params['pe_evaluation_track_rel_message_'+pe_rel.id.to_s]
        pe_evaluation_track_rel_message.save

        pe_evaluation_assess_rel_message = @pe_evaluation.pe_evaluation_assess_rel_messages.build
        pe_evaluation_assess_rel_message.pe_rel = pe_rel
        pe_evaluation_assess_rel_message.message = params['pe_evaluation_assess_rel_message_'+pe_rel.id.to_s]
        pe_evaluation_assess_rel_message.save

      end

      save_pe_evaluation_rels
      save_pe_displayed_evaluations

      redirect_to @pe_evaluation
    else
      @pe_process = @pe_evaluation.pe_process
      render action: 'new'
    end



  end

  def edit
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process

  end

  def update
    @pe_evaluation = PeEvaluation.find(params[:id])

    if @pe_evaluation.update_attributes(params[:pe_evaluation])

      @pe_evaluation.definition_by_users_rol = ''
      params[:pe_evaluation][:definition_by_users_rol].each { |e| @pe_evaluation.definition_by_users_rol += e+'-' }

      @pe_evaluation.save

      @pe_evaluation.pe_evaluation_def_rel_messages.destroy_all
      @pe_evaluation.pe_evaluation_track_rel_messages.destroy_all
      @pe_evaluation.pe_evaluation_assess_rel_messages.destroy_all

      @pe_evaluation.pe_process.pe_rels.each do |pe_rel|

        pe_evaluation_def_rel_message = @pe_evaluation.pe_evaluation_def_rel_messages.build
        pe_evaluation_def_rel_message.pe_rel = pe_rel
        pe_evaluation_def_rel_message.message = params['pe_evaluation_def_rel_message_'+pe_rel.id.to_s]
        pe_evaluation_def_rel_message.save

        pe_evaluation_track_rel_message = @pe_evaluation.pe_evaluation_track_rel_messages.build
        pe_evaluation_track_rel_message.pe_rel = pe_rel
        pe_evaluation_track_rel_message.message = params['pe_evaluation_track_rel_message_'+pe_rel.id.to_s]
        pe_evaluation_track_rel_message.save

        pe_evaluation_assess_rel_message = @pe_evaluation.pe_evaluation_assess_rel_messages.build
        pe_evaluation_assess_rel_message.pe_rel = pe_rel
        pe_evaluation_assess_rel_message.message = params['pe_evaluation_assess_rel_message_'+pe_rel.id.to_s]
        pe_evaluation_assess_rel_message.save

      end

      save_pe_evaluation_rels
      save_pe_displayed_evaluations

      redirect_to @pe_evaluation
    else
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end

  def edit_notifications_definition_by_users_accepted
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process
  end

  def update_notifications_definition_by_users_accepted
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process

    if @pe_evaluation.update_attributes(params[:pe_evaluation])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'
    else
      render action: 'edit_notifications_definition_by_users_accepted'
    end

  end

  def edit_notifications_def
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process
    @pe_process_notification_def = @pe_evaluation.pe_process_notification_def
  end

  def update_notifications_def
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process
    @pe_process_notification_def = @pe_evaluation.pe_process_notification_def

    if @pe_process_notification_def.update_attributes(params[:pe_process_notification_def])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'
    else
      render action: 'edit_notifications_def'
    end


  end

  def download_xls_for_questions_update_pe_groups

    @pe_evaluation = PeEvaluation.find(params[:id])

    reporte_excel = xls_for_questions_update_groups @pe_evaluation

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_'+@pe_evaluation.name+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_xls_for_questions_update_pe_members

    @pe_evaluation = PeEvaluation.find(params[:id])

    reporte_excel = xls_for_questions_update_pe_members @pe_evaluation

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_'+@pe_evaluation.name+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_xls_for_questions_update_pe_member

    @pe_evaluation = PeEvaluation.find(params[:id])

    reporte_excel = xls_for_questions_update_pe_member @pe_evaluation

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_'+@pe_evaluation.name+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_xls_for_questions_update_pe_rels

    @pe_evaluation = PeEvaluation.find(params[:id])

    reporte_excel = xls_for_questions_update_pe_rels @pe_evaluation

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_'+@pe_evaluation.name+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def massive_update_form
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process
  end

  def massive_update

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        #check_data archivo_temporal, @pe_evaluation

        if @lineas_error && @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.master_evaluation.update_file_error')

          @pe_process = @pe_evaluation.pe_process

          render 'massive_update_form'

        else

          pe_group_ids = params[:pe_question] ? params[:pe_question][:pe_group_id] : nil
          pe_rel_ids = params[:pe_question] ? params[:pe_question][:pe_rel_id] : nil

          save_questions archivo_temporal,@pe_evaluation,params[:reemplazar_todo],params[:reemplazar_todo_excel], params[:reemplazar_solo_entered_by_manager], params[:marcar_ready], params[:marcar_confirmadas], params[:marcar_aceptadas], params[:groups_or_members], params[:pe_members_or_pe_member], pe_group_ids, pe_rel_ids

          @pe_evaluation.reload

          set_informative_questions @pe_evaluation

          #set_informative_evaluation @pe_evaluation


          flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

          render 'massive_update_form'

        end

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

        @pe_process = @pe_evaluation.pe_process

        render 'massive_update_form'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

      @pe_process = @pe_evaluation.pe_process

      render 'massive_update_form'

    end



  end

  def massive_update_finish_definition_form
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process
  end

  def massive_update_finish_definition

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_process = @pe_evaluation.pe_process

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        if @lineas_error && @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.master_evaluation.update_file_error')

          @pe_process = @pe_evaluation.pe_process

          render 'massive_update_finish_definition_form'

        else

          set_definition_ready archivo_temporal,@pe_evaluation

          flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

          render 'massive_update_finish_definition_form'

        end

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

        @pe_process = @pe_evaluation.pe_process

        render 'massive_update_finish_definition_form'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

      @pe_process = @pe_evaluation.pe_process

      render 'massive_update_finish_definition_form'

    end



  end

  # DELETE /pe_evaluations/1
  # DELETE /pe_evaluations/1.json
  def destroy
    @pe_evaluation = PeEvaluation.find(params[:id])
    @pe_evaluation.destroy

    respond_to do |format|
      format.html { redirect_to pe_evaluations_url }
      format.json { head :no_content }
    end
  end

  private

    def save_pe_evaluation_rels

      @pe_evaluation.pe_evaluation_rels.destroy_all

      @pe_evaluation.pe_process.pe_rels.each do |pe_rel|

        if params[('eval_by_'+pe_rel.rel.to_s).to_sym] == '1'

          pe_evaluation_rel = PeEvaluationRel.new
          pe_evaluation_rel.pe_evaluation_id = @pe_evaluation.id
          pe_evaluation_rel.pe_rel_id = pe_rel.id
          pe_evaluation_rel.weight = params[('eval_by_'+pe_rel.rel.to_s+'_weight').to_sym].to_f
          pe_evaluation_rel.final_temporal_assessment = params[('eval_by_'+pe_rel.rel.to_s+'_finish_temp').to_sym]

          tmp_d = params[('eval_by_'+pe_rel.rel.to_s+'_from_date').to_sym].split('/')
          pe_evaluation_rel.from_date = DateTime.new(tmp_d[2].to_i,tmp_d[1].to_i, tmp_d[0].to_i,0,0,0)

          tmp_d = params[('eval_by_'+pe_rel.rel.to_s+'_to_date').to_sym].split('/')
          pe_evaluation_rel.to_date = DateTime.new(tmp_d[2].to_i,tmp_d[1].to_i, tmp_d[0].to_i,0,0,0)


          if pe_evaluation_rel.save


            @pe_evaluation.pe_process.pe_group2s.each do |pe_group2|

              pe_evaluation_rel_group2 = pe_evaluation_rel.pe_evaluation_rel_group2s.where('pe_group2_id = ?', pe_group2.id).first

              if params['eval_by_g_'+pe_rel.rel.to_s+'_'+pe_group2.id.to_s+'_from_date'].blank? || params['eval_by_g_'+pe_rel.rel.to_s+'_'+pe_group2.id.to_s+'_to_date'].blank?

                pe_evaluation_rel_group2.destroy if pe_evaluation_rel_group2

              elsif !params['eval_by_g_'+pe_rel.rel.to_s+'_'+pe_group2.id.to_s+'_from_date'].blank? && !params['eval_by_g_'+pe_rel.rel.to_s+'_'+pe_group2.id.to_s+'_to_date'].blank?

                unless pe_evaluation_rel_group2

                  pe_evaluation_rel_group2 = pe_evaluation_rel.pe_evaluation_rel_group2s.build
                  pe_evaluation_rel_group2.pe_group2 = pe_group2

                end

                tmp_d = params['eval_by_g_'+pe_rel.rel.to_s+'_'+pe_group2.id.to_s+'_from_date'].split('/')
                pe_evaluation_rel_group2.from_date = DateTime.new(tmp_d[2].to_i,tmp_d[1].to_i, tmp_d[0].to_i,0,0,0)
                tmp_d = params['eval_by_g_'+pe_rel.rel.to_s+'_'+pe_group2.id.to_s+'_to_date'].split('/')
                pe_evaluation_rel_group2.to_date = DateTime.new(tmp_d[2].to_i,tmp_d[1].to_i, tmp_d[0].to_i,0,0,0)

                pe_evaluation_rel_group2.save

              end

            end

          end

        end


      end

    end

    def save_pe_displayed_evaluations

      @pe_evaluation.pe_evaluation_displayed_def_by_users.destroy_all

      if params['pe_displayed_evaluation_definition_by_users_ids']

        params['pe_displayed_evaluation_definition_by_users_ids'].each do |pe_displayed_evaluation_id|

          pe_displayed_evaluation = @pe_evaluation.pe_evaluation_displayed_def_by_users.build
          pe_displayed_evaluation.pe_displayed_evaluation_id = pe_displayed_evaluation_id
          pe_displayed_evaluation.save
          
        end

      end

      @pe_evaluation.pe_evaluation_displayed_trackings.destroy_all

      if params['pe_displayed_evaluation_tracking_ids']

        params['pe_displayed_evaluation_tracking_ids'].each do |pe_displayed_evaluation_id|

          pe_displayed_evaluation = @pe_evaluation.pe_evaluation_displayed_trackings.build
          pe_displayed_evaluation.pe_displayed_evaluation_id = pe_displayed_evaluation_id
          pe_displayed_evaluation.save

        end

      end

    end


end
