class HrProcessDimensionEsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show
    @hr_process_dimension_e = HrProcessDimensionE.find(params[:id])
  end


  def new

    @hr_process_dimension = HrProcessDimension.find(params[:hr_process_dimension_id])

    @hr_process_dimension_e = HrProcessDimensionE.new
    @hr_process_dimension_e.hr_process_dimension = @hr_process_dimension


  end


  def edit

    @hr_process_dimension_e = HrProcessDimensionE.find(params[:id])

    @hr_process_dimension = @hr_process_dimension_e.hr_process_dimension

  end

  def delete
    @hr_process_dimension_e = HrProcessDimensionE.find(params[:id])

    @hr_process_dimension = @hr_process_dimension_e.hr_process_dimension

  end

  def create

    @hr_process_dimension_e = HrProcessDimensionE.new(params[:hr_process_dimension_e])
    @hr_process_dimension = @hr_process_dimension_e.hr_process_dimension

    if @hr_process_dimension_e.save
      flash[:success] = t('activerecord.success.model.hr_process_dimension_e.create_ok')
      redirect_to @hr_process_dimension.hr_process_template
    else

      render action: 'new'
    end

  end

  def update
    @hr_process_dimension_e = HrProcessDimensionE.find(params[:id])
    @hr_process_dimension = @hr_process_dimension_e.hr_process_dimension

    if @hr_process_dimension_e.update_attributes(params[:hr_process_dimension_e])
      flash[:success] = t('activerecord.success.model.hr_process_dimension_e.update_ok')
      redirect_to @hr_process_dimension.hr_process_template
    else
      render action: 'edit'
    end

  end

  def destroy

    @hr_process_dimension_e = HrProcessDimensionE.find(params[:id])
    @hr_process_dimension = @hr_process_dimension_e.hr_process_dimension

    if verify_recaptcha

      if @hr_process_dimension_e.destroy
        flash[:success] = t('activerecord.success.model.hr_process_dimension_e.delete_ok')
        redirect_to @hr_process_dimension.hr_process_template
      else
        flash[:danger] = t('activerecord.success.model.hr_process_dimension_e.delete_error')
        redirect_to delete_hr_process_dimension_e_path(@hr_process_dimension_e)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.hr_process_dimension_e.captcha_error')
      redirect_to delete_hr_process_dimension_e_path(@hr_process_dimension_e)

    end

  end

end
