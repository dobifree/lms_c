class PeQuestionModelsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @pe_question_models = PeQuestionModel.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_question_models }
    end
  end

  # GET /pe_question_models/1
  # GET /pe_question_models/1.json
  def show
    @pe_question_model = PeQuestionModel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_question_model }
    end
  end

  def new

    @pe_element = PeElement.find(params[:pe_element_id])
    @pe_evaluation = @pe_element.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
    @pe_question_model = @pe_element.pe_question_models.build

  end

  def create
    @pe_question_model = PeQuestionModel.new(params[:pe_question_model])
    @pe_element = PeElement.find(params[:pe_question_model][:pe_element_id])
    @pe_evaluation = @pe_element.pe_evaluation

    if @pe_question_model.save
      redirect_to @pe_question_model.pe_element, notice: 'Pe question model was successfully created.'
    else
      @pe_evaluation = @pe_element.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'new'
    end

  end

  # GET /pe_question_models/1/edit
  def edit
    @pe_question_model = PeQuestionModel.find(params[:id])

    @pe_element = @pe_question_model.pe_element
    @pe_evaluation = @pe_element.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  # PUT /pe_question_models/1
  # PUT /pe_question_models/1.json
  def update
    @pe_question_model = PeQuestionModel.find(params[:id])


    if @pe_question_model.update_attributes(params[:pe_question_model])

      @pe_question_model.pe_questions.each do |pe_question|
        pe_question.description = @pe_question_model.description
        pe_question.save
      end

      redirect_to @pe_question_model.pe_element, notice: 'Pe question model was successfully updated.'
    else
      @pe_element = @pe_question_model.pe_element
      @pe_evaluation = @pe_element.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end

  # DELETE /pe_question_models/1
  # DELETE /pe_question_models/1.json
  def destroy
    @pe_question_model = PeQuestionModel.find(params[:id])

    pe_element = @pe_question_model.pe_element

    @pe_question_model.destroy

    redirect_to pe_element

  end
end
