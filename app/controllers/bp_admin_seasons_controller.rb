class BpAdminSeasonsController < ApplicationController
  include BpAdminSeasonsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin
  
  def bp_admin_seasons
    @bp_seasons = BpSeason.reorder('since DESC').all
  end

  def bp_admin_season_new
    @bp_season = BpSeason.new
  end

  def bp_admin_season_create
    season = season_complete_create(params[:bp_season])
    if season.save
      flash[:success] = t('views.bp_seasons.flash_messages.success_created')
      redirect_to bp_admin_season_manage_path(season)
    else
      @bp_season = season
      render action: 'bp_admin_season_new'
    end
  end

  def bp_admin_season_edit
    @bp_season = BpSeason.find(params[:bp_season_id])
  end

  def bp_admin_season_update
    season = season_complete_update(params[:bp_season], params[:bp_season_id])
    if season.save
      flash[:success] = t('views.bp_events.flash_messages.success_changed')
      redirect_to bp_admin_season_manage_path(season)
    else
      @bp_season = season
      render action: 'bp_admin_season_edit'
    end
  end

  def bp_admin_season_manage
    @bp_season = BpSeason.find(params[:bp_season_id])
  end

  def bp_season_period_new
    @bp_season_period = BpSeasonPeriod.new
    @bp_season = BpSeason.find(params[:bp_season_id])
    @bp_season_period.bp_season_id = @bp_season.id
  end

  def bp_season_period_create
    season_period = season_period_complete_create(params[:bp_season_period], params[:bp_season_id])
    if season_period.save
      flash[:success] = t('views.bp_season_periods.flash_messages.success_created')
      redirect_to bp_admin_season_manage_path(season_period.bp_season)
    else
      @bp_season_period = season_period
      @bp_season = @bp_season_period.bp_season
      render action: 'bp_season_period_new'
    end
  end

  def bp_season_period_edit
    @bp_season_period = BpSeasonPeriod.find(params[:bp_season_period_id])
    @bp_season = @bp_season_period.bp_season
  end

  def bp_season_period_update
    season_period = season_period_complete_update(params[:bp_season_period], params[:bp_season_period_id])
    if season_period.save
      flash[:success] = t('views.bp_season_periods.flash_messages.success_changed')
      redirect_to bp_admin_season_manage_path(season_period.bp_season)
    else
      @bp_season_period = season_period
      @bp_season = season_period.bp_season
      render action: 'bp_season_period_edit'
    end
  end

  private

  def season_complete_create(params)
    season = BpSeason.new(params)
    season.registered_by_admin_id = admin_connected.id
    season.registered_at = lms_time
    return season unless season.until
    season.deactivated_by_admin_id = admin_connected.id
    season.deactivated_at = lms_time
    return season
  end

  def season_complete_update(params, season_id)
    season = BpSeason.find(season_id)
    season.assign_attributes(params)
    return season if !params[:until] || (params[:until] && params[:until].blank?)
    season.deactivated_by_admin_id = admin_connected.id
    season.deactivated_at = lms_time
    return season
  end

  def season_period_complete_create(params, season_id)
    season_period = BpSeasonPeriod.new(params)
    season_period.bp_season_id = season_id
    season_period.registered_by_admin_id = admin_connected.id
    season_period.registered_at = lms_time
    return season_period unless season_period.until
    season_period.deactivated_by_admin_id = admin_connected.id
    season_period.deactivated_at = lms_time
    return season_period
  end

  def season_period_complete_update(params, season_period_id)
    season_period = BpSeasonPeriod.find(season_period_id)
    season_period.assign_attributes(params)
    return season_period if !params[:until] || (params[:until] && params[:until].blank?)
    season_period.deactivated_by_admin_id = admin_connected.id
    season_period.deactivated_at = lms_time
    return season_period
  end

end
