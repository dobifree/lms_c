class ProgramInspectorsController < ApplicationController

  include ActionView::Helpers::NumberHelper
  include ExcelReportsHelper

  before_filter :authenticate_user

  before_filter :verify_program_inspector

  before_filter :verify_access_to_program, only: [:empty_students_list_xlsx, :acta_students_xlsx, :reporte_avance_xlsx, :reporte_curso_xlsx,
                                                  :certificados_curso]

  def index


   #@company = session[:company]


  end

  def empty_students_list_xlsx

    grupo = params[:grupo]

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', program_course.id, course.id, grupo).joins(:user).order('apellidos, nombre')

    @user_courses = program_course.matriculas_vigentes_por_grupo grupo

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}



      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}





      wb.add_worksheet(:name => 'Lista') do |reporte_excel|

        characteristics = Characteristic.where('basic_report = ?', true).reorder('orden_basic_report')

        fila_actual = 1

        fila = Array.new
        fila << 'Curso'
        fila << ''
        fila << course.nombre
        fila << ''

        reporte_excel.add_row fila, :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':D'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Fecha'
        fila << ''
        fila << (localize  @user_courses.first.desde, format: :day_month_year)+' - '+(localize  @user_courses.first.hasta, format: :day_month_year)
        fila << ''

        reporte_excel.add_row fila, :style => headerTitleInfo, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':D'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        characteristics.each do |c|
          fila << c.nombre
          filaHeader <<  headerLeft
          cols_widths << 22
        end

        fila << ''
        filaHeader <<  headerLeft
        cols_widths << 20


        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          characteristics.each do |c|

            u_cha = uc.enrollment.user.characteristic_by_id(c.id)
            if u_cha
              fila << u_cha.valor if uc
            else
              fila << ''
            end

            fileStyle << fieldLeft

          end

          fila << ''
          fileStyle << fieldLeft

          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Lista - '+course.nombre+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def acta_students_xlsx

    grupo = params[:grupo]

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', program_course.id, course.id, grupo).joins(:user).order('apellidos, nombre')

    @user_courses = program_course.matriculas_vigentes_por_grupo grupo

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}



      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 11,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Acta') do |reporte_excel|

        characteristics = Characteristic.where('basic_report = ?', true).reorder('orden_basic_report')

        fila_actual = 1

        fila = Array.new
        fila << 'Curso'
        fila << ''
        fila << course.nombre
        fila << ''
        fila << ''

        reporte_excel.add_row fila, :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':F'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Fecha'
        fila << ''
        fila << (localize  @user_courses.first.desde, format: :day_month_year)+' - '+(localize  @user_courses.first.hasta, format: :day_month_year)
        fila << 'Nota Min.'
        fila << course.nota_minima
        fila << ''

        reporte_excel.add_row fila, :style => headerTitleInfo, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('E'+fila_actual.to_s+':F'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        characteristics.each do |c|
          fila << c.nombre
          filaHeader <<  headerLeft
          cols_widths << 22
        end

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Nota'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10


        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          characteristics.each do |c|

            u_cha = uc.enrollment.user.characteristic_by_id(c.id)
            if u_cha
              fila << u_cha.valor if uc
            else
              fila << ''
            end

            fileStyle << fieldLeft

          end

          if uc.asistencia
            fila << 'Sí' if uc.asistencia
          else
            fila << 'NO'
          end

          fileStyle << fieldLeft

          fila << uc.nota

          if uc.aprobado
            fileStyle << fieldLeftAprob
            fila << 'Aprobado'
            fileStyle << fieldLeftAprob

          else



            fileStyle << fieldLeftReprob
            if uc.nota
              fila << 'Reprobado'
            else
              fila << ''
            end


            fileStyle << fieldLeftReprob


          end


          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Acta - '+course.nombre+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def reporte_avance_xlsx

   company = @company

    grupo = params[:grupo]

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', program_course.id, course.id, grupo).joins(:user).order('apellidos, nombre')

    @user_courses = program_course.matriculas_vigentes_por_grupo grupo

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Reporte Avance') do |reporte_excel|

        si_hay_sence = false

        if company.modulo_sence

          if course.sence_code && course.sence_code != ''

            si_hay_sence = true

          end

        end

        fila_actual = 1



        fila = Array.new
        filaHeader = Array.new

        fila << 'Curso'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << course.nombre
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':E'+fila_actual.to_s)

        fila_actual += 1
=begin
        if company.modulo_sence

          if course.sence_code && course.sence_code != ''

            fila = Array.new
            fila << 'Código Sence'
            fila << ''
            fila << course.sence_code

            reporte_excel.add_row fila, :style => headerTitleInfo, height: 30
            reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

            fila_actual += 1

            fila = Array.new
            fila << 'Número de horas'
            fila << ''
            fila << course.sence_hours

            reporte_excel.add_row fila, :style => headerTitleInfo, height: 30
            reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

            fila_actual += 1

            fila = Array.new
            fila << 'Valor por participante'
            fila << ''
            fila << course.sence_student_value

            reporte_excel.add_row fila, :style => headerTitleInfo, height: 30
            reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

            fila_actual += 1

          end

        end
=end
        fila = Array.new
        filaHeader = Array.new

        fila << 'Nota Mínima para Aprobar'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo


        fila << course.nota_minima
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)


        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila


        fila_actual += 1



        fila = Array.new
        filaHeader = Array.new


        fila << 'DATOS PERSONALES'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        fila << 'DATOS CONVOCATORIA'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        fila << 'DATOS AVANCE EN CURSO'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        if si_hay_sence

          fila << ''
          filaHeader <<  headerCenter

          fila << ''
          filaHeader <<  headerCenter

        end

        characteristics = Characteristic.find_all_by_publica(true)

        characteristics_porcentaje_sence = Characteristic.find_by_porcentaje_participante_sence(true)



        if characteristics.length > 0

          fila << 'DATOS DEL ALUMNO'

          (1..characteristics.length).each do |n|

            filaHeader <<  headerCenter
            fila << ''

          end



        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':E'+fila_actual.to_s)
        reporte_excel.merge_cells('F'+fila_actual.to_s+':H'+fila_actual.to_s)

        if si_hay_sence

          reporte_excel.merge_cells('I'+fila_actual.to_s+':P'+fila_actual.to_s)

        else

          reporte_excel.merge_cells('I'+fila_actual.to_s+':N'+fila_actual.to_s)

        end

        if characteristics.length > 0

          if si_hay_sence

            reporte_excel.merge_cells('Q'+fila_actual.to_s+':'+letras[14+characteristics.length]+fila_actual.to_s)

          else

            reporte_excel.merge_cells('O'+fila_actual.to_s+':'+letras[12+characteristics.length]+fila_actual.to_s)

          end


        end

        fila_actual += 1



        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Correo Electrónico'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Fecha Inicio'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fecha Fin'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Nota Final'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Resultado Final'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Inicio Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fin Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        if si_hay_sence

          fila << 'Inscripción Sence'
          filaHeader <<  headerLeft
          cols_widths << 15

          fila << 'Código Sence'
          filaHeader <<  headerLeft
          cols_widths << 15

        end

        characteristics.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end







        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          fila << uc.enrollment.user.email
          fileStyle << fieldLeft

          fila << uc.numero_oportunidad
          fileStyle << fieldLeft

          if uc.desde
            fila << (localize  uc.desde, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.hasta
            fila << (localize  uc.hasta, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft


          if !uc.iniciado
            fila << 'No iniciado'
          else

            if uc.finalizado
              fila << 'Terminado'
            else
              fila << 'Iniciado'
            end

          end

          fileStyle << fieldLeft

          if uc.nota
            fila << uc.nota
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.aprobado
            fila << 'aprobado'
          else

            if uc.finalizado

              fila << 'reprobado'

            else

              fila << '-'

            end


          end
          fileStyle << fieldLeft

          fila << uc.porcentaje_avance.to_i.to_s+'%'
          fileStyle << fieldLeft

          if uc.inicio
            fila << (localize  uc.inicio, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.fin
            fila << (localize  uc.fin, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft



          if si_hay_sence

            if uc.aplica_sence
              fila << 'Sí'
            else
              fila << 'No'
            end
            fileStyle << fieldLeft

            if uc.aplica_sence
              fila << course.sence_code
            else
              fila << '-'
            end
            fileStyle << fieldLeft

          end

          characteristics.each do |cha|

            user_cha = uc.enrollment.user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end




          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte Avance - '+course.nombre+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def reporte_curso_xlsx

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    if rango_fechas

      #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND desde >= ? AND hasta <= ? ',
      #                                 program_course.id, course.id,desde, hasta).joins(:user).order('apellidos, nombre')

      @user_courses = program_course.matriculas_vigentes_desde_hasta(desde, hasta)

    else

      #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? ', program_course.id, course.id).joins(:user).order('apellidos, nombre')

      @user_courses = program_course.matriculas_vigentes

    end

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Reporte Avance') do |reporte_excel|

        si_hay_sence = false

        if company.modulo_sence

          if course.sence_code && course.sence_code != ''

            si_hay_sence = true

          end

        end

        fila_actual = 1



        fila = Array.new
        filaHeader = Array.new

        fila << 'Curso'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << course.nombre
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':E'+fila_actual.to_s)

        fila_actual += 1
=begin
        if company.modulo_sence

          if course.sence_code && course.sence_code != ''

            fila = Array.new
            fila << 'Código Sence'
            fila << ''
            fila << course.sence_code

            reporte_excel.add_row fila, :style => headerTitleInfo, height: 30
            reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

            fila_actual += 1

            fila = Array.new
            fila << 'Número de horas'
            fila << ''
            fila << course.sence_hours

            reporte_excel.add_row fila, :style => headerTitleInfo, height: 30
            reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

            fila_actual += 1

            fila = Array.new
            fila << 'Valor por participante'
            fila << ''
            fila << course.sence_student_value

            reporte_excel.add_row fila, :style => headerTitleInfo, height: 30
            reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

            fila_actual += 1

          end

        end
=end


        if rango_fechas

          fila = Array.new
          filaHeader = Array.new

          fila << 'Vigencia'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo


          fila << 'Del '+desde_t+' Al '+hasta_t
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        fila = Array.new
        filaHeader = Array.new

        fila << 'Nota Mínima para Aprobar'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo


        fila << course.nota_minima
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)


        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila


        fila_actual += 1



        fila = Array.new
        filaHeader = Array.new


        fila << 'DATOS PERSONALES'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        fila << 'DATOS CONVOCATORIA'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        fila << 'DATOS AVANCE EN CURSO'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        if si_hay_sence

          fila << ''
          filaHeader <<  headerCenter

          fila << ''
          filaHeader <<  headerCenter

        end

        characteristics = Characteristic.find_all_by_publica(true)

        characteristics_porcentaje_sence = Characteristic.find_by_porcentaje_participante_sence(true)



        if characteristics.length > 0

          fila << 'DATOS DEL ALUMNO'

          (1..characteristics.length).each do |n|

            filaHeader <<  headerCenter
            fila << ''

          end



        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':E'+fila_actual.to_s)
        reporte_excel.merge_cells('F'+fila_actual.to_s+':H'+fila_actual.to_s)

        if si_hay_sence

          reporte_excel.merge_cells('I'+fila_actual.to_s+':P'+fila_actual.to_s)

        else

          reporte_excel.merge_cells('I'+fila_actual.to_s+':N'+fila_actual.to_s)

        end

        if characteristics.length > 0

          if si_hay_sence

            reporte_excel.merge_cells('Q'+fila_actual.to_s+':'+letras[14+characteristics.length]+fila_actual.to_s)

          else

            reporte_excel.merge_cells('O'+fila_actual.to_s+':'+letras[12+characteristics.length]+fila_actual.to_s)

          end


        end

        fila_actual += 1



        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Correo Electrónico'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Fecha Inicio'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fecha Fin'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Nota Final'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Resultado Final'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Inicio Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fin Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        if si_hay_sence

          fila << 'Inscripción Sence'
          filaHeader <<  headerLeft
          cols_widths << 15

          fila << 'Código Sence'
          filaHeader <<  headerLeft
          cols_widths << 15

        end

        characteristics.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end







        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          fila << uc.enrollment.user.email
          fileStyle << fieldLeft

          fila << uc.numero_oportunidad
          fileStyle << fieldLeft

          if uc.desde
            fila << (localize  uc.desde, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.hasta
            fila << (localize  uc.hasta, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft


          if !uc.iniciado
            fila << 'No iniciado'
          else

            if uc.finalizado
              fila << 'Terminado'
            else
              fila << 'Iniciado'
            end

          end

          fileStyle << fieldLeft

          if uc.nota
            fila << uc.nota
          else
            fila << '-'
          end

          fileStyle << fieldLeft

          if uc.aprobado
            fila << 'aprobado'
          else
            if uc.finalizado

              fila << 'reprobado'

            else

              fila << '-'

            end
          end
          fileStyle << fieldLeft

          fila << uc.porcentaje_avance.to_i.to_s+'%'
          fileStyle << fieldLeft

          if uc.inicio
            fila << (localize  uc.inicio, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.fin
            fila << (localize  uc.fin, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft



          if si_hay_sence

            if uc.aplica_sence
              fila << 'Sí'
            else
              fila << 'No'
            end
            fileStyle << fieldLeft

            if uc.aplica_sence
              fila << course.sence_code
            else
              fila << '-'
            end
            fileStyle << fieldLeft

          end

          characteristics.each do |cha|

            user_cha = uc.enrollment.user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end




          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte Avance - '+course.nombre+'-'+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def reporte_general_cursos_xlsx

   company = @company

    programs = user_connected.programs.find_all_by_especifico(true)

    query = ''
    n_query = 0

    programs.each do |program|

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

    end

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    if rango_fechas

      #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND desde >= ? AND hasta <= ? ',
      #                                 program_course.id, course.id,desde, hasta).joins(:user).order('apellidos, nombre')

      user_courses = UserCourse.where('('+query+') AND anulado = ? AND desde >= ? AND hasta <= ?', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')


    else

      user_courses = UserCourse.where('('+query+') AND anulado = ? ', false).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')

    end


    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Reporte Avance') do |reporte_excel|

        fila_actual = 1

        if rango_fechas

          fila = Array.new
          filaHeader = Array.new

          fila << 'Vigencia'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo


          fila << 'Del '+desde_t+' Al '+hasta_t
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        fila = Array.new
        filaHeader = Array.new

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila


        fila_actual += 1



        fila = Array.new
        filaHeader = Array.new


        fila << 'DATOS PERSONALES'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        fila << 'DATOS CONVOCATORIA'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        fila << 'DATOS AVANCE EN CURSO'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        characteristics = Characteristic.find_all_by_publica(true)

        characteristics_porcentaje_sence = Characteristic.find_by_porcentaje_participante_sence(true)



        if characteristics.length > 0

          fila << 'DATOS DEL ALUMNO'

          (1..characteristics.length).each do |n|

            filaHeader <<  headerCenter
            fila << ''

          end



        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':E'+fila_actual.to_s)
        reporte_excel.merge_cells('F'+fila_actual.to_s+':J'+fila_actual.to_s)
        reporte_excel.merge_cells('K'+fila_actual.to_s+':P'+fila_actual.to_s)

        if characteristics.length > 0

          reporte_excel.merge_cells('Q'+fila_actual.to_s+':'+letras[12+characteristics.length]+fila_actual.to_s)

        end

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Correo Electrónico'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Curso'
        filaHeader <<  headerLeft
        cols_widths << 20

        fila << 'Programa'
        filaHeader <<  headerLeft
        cols_widths << 20

        fila << 'Fecha Inicio'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fecha Fin'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Nota Final'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Resultado Final'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Inicio Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fin Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        characteristics.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end



        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          fila << uc.enrollment.user.email
          fileStyle << fieldLeft

          fila << uc.numero_oportunidad
          fileStyle << fieldLeft

          fila << uc.course.nombre
          fileStyle << fieldLeft

          fila << uc.program_course.program.nombre
          fileStyle << fieldLeft

          if uc.desde
            fila << (localize  uc.desde, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.hasta
            fila << (localize  uc.hasta, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if !uc.iniciado
            fila << 'No iniciado'
          else

            if uc.finalizado
              fila << 'Terminado'
            else
              fila << 'Iniciado'
            end

          end

          fileStyle << fieldLeft

          if uc.nota
            fila << uc.nota
          else
            fila << '-'
          end

          fileStyle << fieldLeft

          if uc.aprobado
            fila << 'aprobado'
          else
            if uc.finalizado

              fila << 'reprobado'

            else

              fila << '-'

            end
          end
          fileStyle << fieldLeft

          fila << uc.porcentaje_avance.to_i.to_s+'%'
          fileStyle << fieldLeft

          if uc.inicio
            fila << (localize  uc.inicio, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.fin
            fila << (localize  uc.fin, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          characteristics.each do |cha|

            user_cha = uc.enrollment.user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end




          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte General -'+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def certificados_curso

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])

    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_cursos/'

    rango_fechas = false

    if params[:desdehasta_cert]

      rango_fechas = true

      desdehasta = params[:desdehasta_cert].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    if rango_fechas

      require 'prawn/measurement_extensions'

      directorio_temporal = '/tmp/'
      nombre_temporal = SecureRandom.hex(5)+'.pdf'
      archivo_temporal = directorio_temporal+nombre_temporal

      user_connected_time_zone_pdf = user_connected_time_zone

      Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: [720,540]) do |pdf|

        pdf.font_families.update('Foco Light' => {:normal => '/var/www/remote-storage/fonts/Foco_Std_Lt_0.ttf'})
        pdf.font_families.update('HaasGrotDisp Light' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-45Light_1.ttf'})
        pdf.font_families.update('HaasGrotDisp Normal' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-65Medium_1.ttf'})
        pdf.font_families.update('HaasGrotDisp Bold' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-75Bold_1.ttf'})

        program_course.matriculas_aprobadas_desde_hasta_convocatoria(desde, hasta).each do |user_course|

          cc = user_course.course.course_certificates.where('rango_desde <= ? AND rango_hasta >= ?',user_course.fin, user_course.fin).first

          if cc && cc.template

            plantilla = directory+(cc.template)+'.jpg'

            pdf.start_new_page
            pdf.image plantilla, at: [-36,504], width: 720

            nombre =  user_course.user.nombre+' '+user_course.user.apellidos

            nombre_curso =  user_course.course.nombre

            nota = ''
            nota = format_nota(user_course.nota).to_s if user_course.nota

            desde = ''
            desde = I18n.localize(user_course.desde, format: :day_snmonth_year) if user_course.desde
            hasta = ''
            hasta = I18n.localize(user_course.hasta, format: :day_snmonth_year) if user_course.hasta

            inicio = ''
            inicio = I18n.localize(user_course.inicio, format: :day_snmonth_year) if user_course.inicio
            fin = ''
            fin = I18n.localize(user_course.fin, format: :day_snmonth_year) if user_course.fin

            fecha = user_course.fin
            fecha = user_course.hasta if user_course.hasta && cc.fecha_source == 'fin_convocatoria'

            fecha = fecha+cc.fecha_source_delay.days
            fecha = I18n.localize(fecha, format: :full_date)



            if cc.nombre
              if cc.nombre_x == -1
                pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :size => cc.nombre_size} ], :at => [0, cc.nombre_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :size => cc.nombre_size} ], :at => [cc.nombre_x.mm, cc.nombre_y.mm]
              end
            end

            if cc.nombre_curso

              if cc.nombre_curso_x == -1
                pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :size => cc.nombre_curso_size} ], :at => [0, cc.nombre_curso_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :size => cc.nombre_curso_size} ], :at => [cc.nombre_curso_x.mm, cc.nombre_curso_y.mm]
              end
            end

            if cc.fecha
              if cc.fecha_x == -1
                pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :size => cc.fecha_size} ], :at => [cc.fecha_x.mm, cc.fecha_y.mm]
              end
            end

            if cc.desde
              if cc.desde_x == -1
                pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :size => cc.desde_size} ], :at => [0, cc.desde_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :size => cc.desde_size} ], :at => [cc.desde_x.mm, cc.desde_y.mm]
              end
            end

            if cc.hasta
              if cc.hasta_x == -1
                pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :size => cc.hasta_size} ], :at => [0, cc.hasta_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :size => cc.hasta_size} ], :at => [cc.hasta_x.mm, cc.hasta_y.mm]
              end
            end

            if cc.inicio
              if cc.inicio_x == -1
                pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :size => cc.inicio_size} ], :at => [0, cc.inicio_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :size => cc.inicio_size} ], :at => [cc.inicio_x.mm, cc.inicio_y.mm]
              end
            end

            if cc.fin
              if cc.fin_x == -1
                pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :size => cc.fin_size} ], :at => [0, cc.fin_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :size => cc.fin_size} ], :at => [cc.fin_x.mm, cc.fin_y.mm]
              end
            end

            if cc.nota
              if cc.nota_x == -1
                pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :size => cc.nota_size} ], :at => [0, cc.nota_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :size => cc.nota_size} ], :at => [cc.nota_x.mm, cc.nota_y.mm]
              end
            end

          end

        end

      end

      send_file archivo_temporal,
                filename: 'Certificados - '+program_course.course.nombre+'.pdf',
                type: 'application/octet-stream',
                disposition: 'attachment'


    end

  end

  private

    def verify_program_inspector

      if user_connected.program_inspectors.count == 0
        flash[:danger] = t('security.no_access_program_inspector')
        redirect_to root_path
      end

    end

    def verify_access_to_program

      pc = ProgramCourse.find_by_id(params[:program_course_id])

      pi = nil

      pi = pc.program.program_inspectors.find_by_user_id(user_connected.id) if pc


      unless pi && pi.user_id == user_connected.id
        flash[:danger] = t('security.no_access_program_inspector')
        redirect_to root_path
      end

    end


end
