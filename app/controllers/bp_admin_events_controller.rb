class BpAdminEventsController < ApplicationController
  include BpAdminEventsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def bp_admin_events
    @bp_events = BpEvent.reorder('since DESC').all
  end

  def bp_admin_event_new
    @bp_event = BpEvent.new
  end

  def bp_admin_event_create
    event = event_complete_create(params[:bp_event])
    if event.save
      flash[:success] = t('views.bp_events.flash_messages.success_created')
      redirect_to bp_admin_event_manage_path(event)
    else
      @bp_event = event
      render action: 'bp_admin_event_new'
    end
  end

  def bp_admin_event_edit
    @bp_event = BpEvent.find(params[:bp_event_id])
  end

  def bp_admin_event_update
    event = event_complete_update(params[:bp_event], params[:bp_event_id])
    if event.save
      flash[:success] = t('views.bp_events.flash_messages.success_changed')
      redirect_to bp_admin_event_manage_path(event)
    else
      @bp_event = event
      render action: 'bp_admin_event_edit'
    end
  end

  def bp_admin_event_manage
    @bp_event = BpEvent.find(params[:bp_event_id])
  end

  def bp_event_file_new
    @bp_event_file = BpEventFile.new
    @bp_event = BpEvent.find(params[:bp_event_id])
    @bp_event_file.bp_event_id = @bp_event.id
  end

  def bp_event_file_create
    event_file = event_file_complete_create(params[:bp_event_file], params[:bp_event_id])
    if event_file.save
      flash[:success] = t('views.bp_events_files.flash_messages.success_created')
      redirect_to bp_admin_event_manage_path(event_file.bp_event)
    else
      @bp_event_file = event_file
      @bp_event = event_file.bp_event
      render action: 'bp_event_file_new'
    end
  end

  def bp_event_file_edit
    @bp_event_file = BpEventFile.find(params[:bp_event_file_id])
    @bp_event = BpEvent.find(params[:bp_event_id])
  end

  def bp_event_file_update
    event_file = event_file_complete_update(params[:bp_event_file], params[:bp_event_file_id])
    if event_file.save
      flash[:success] = t('views.bp_events_files.flash_messages.success_changed')
      redirect_to bp_admin_event_manage_path(event_file.bp_event)
    else
      @bp_event_file = event_file
      @bp_event = event_file.bp_event
      render action: 'bp_event_file_edit'
    end
  end

  private

  def event_complete_create(params)
    event = BpEvent.new(params)
    event.registered_by_admin_id = admin_connected.id
    event.registered_at = lms_time
    return event unless event.until
    event.deactivated_by_admin_id = admin_connected.id
    event.deactivated_at = lms_time
    return event
  end

  def event_complete_update(params, event_id)
    event = BpEvent.find(event_id)
    event.assign_attributes(params)
    return event if !params[:until] || (params[:until] && params[:until].blank?)
    event.deactivated_by_admin_id = admin_connected.id
    event.deactivated_at = lms_time
    return event
  end

  def event_file_complete_create(params, event_id)
    event_file = BpEventFile.new(params)
    event_file.bp_event_id = event_id
    event_file.registered_by_admin_id = admin_connected.id
    event_file.registered_at = lms_time
    return event_file unless event_file.until
    event_file.deactivated_by_admin_id = admin_connected.id
    event_file.deactivated_at = lms_time
    return event_file
  end

  def event_file_complete_update(params, event_file_id)
    event_file = BpSeasonPeriod.find(event_file_id)
    event_file.assign_attributes(params)
    return event_file if !params[:until] || (params[:until] && params[:until].blank?)
    event_file.deactivated_by_admin_id = admin_connected.id
    event_file.deactivated_at = lms_time
    return event_file
  end
end
