class BenCellphoneCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @characteristics = Characteristic.not_grouped_characteristics
    @characteristic_types = CharacteristicType.all
  end

  def edit
    @ben_cellphone_characteristic = BenCellphoneCharacteristic.find(params[:id])
    @characteristic = @ben_cellphone_characteristic.characteristic
  end

  def update
    @ben_cellphone_characteristic = BenCellphoneCharacteristic.find(params[:id])
    redirect_to ben_cellphone_characteristics_path if @ben_cellphone_characteristic.update_attributes(params[:ben_cellphone_characteristic])
  end

end
