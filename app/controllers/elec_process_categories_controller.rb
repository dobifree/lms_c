class ElecProcessCategoriesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /elec_process_categories
  # GET /elec_process_categories.json
  def index
    @elec_process_categories = ElecProcessCategory.find_all_by_elec_process_id(params[:elec_process_id])

   render :layout => false
  end

  # GET /elec_process_categories/1
  # GET /elec_process_categories/1.json
  def show
    @elec_process_category = ElecProcessCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @elec_process_category }
    end
  end

  # GET /elec_process_categories/new
  # GET /elec_process_categories/new.json
  def new
    @elec_process = ElecProcess.find(params[:elec_process_id])
    @elec_process_category = ElecProcessCategory.new
    @elec_process_category.elec_process = @elec_process

    @characteristics = mapped_characteristics

  end

  # GET /elec_process_categories/1/edit
  def edit
    @elec_process_category = ElecProcessCategory.find(params[:id])
    @characteristics = mapped_characteristics

    1.times do
      #@elec_process_category.elec_category_characteristics.build

    end
  end

  # POST /elec_process_categories
  # POST /elec_process_categories.json
  def create
    @elec_process_category = ElecProcessCategory.new(params[:elec_process_category])

      if @elec_process_category.save
        flash[:success] = 'La categoría fue creada correctamente'
        redirect_to elec_processes_show_with_open_pill_path(@elec_process_category.elec_process_id, 'categories')
      else
        @characteristics = mapped_characteristics
        render action: "new"
      end

  end

  # PUT /elec_process_categories/1
  # PUT /elec_process_categories/1.json
  def update
    @elec_process_category = ElecProcessCategory.find(params[:id])

      if @elec_process_category.update_attributes(params[:elec_process_category])
        flash[:success] = 'La categoría fue actualizada correctamente'
        redirect_to elec_processes_show_with_open_pill_path(@elec_process_category.elec_process_id, 'categories')
      else
        @characteristics = mapped_characteristics
        render action: "edit"
      end
  end

  # DELETE /elec_process_categories/1
  # DELETE /elec_process_categories/1.json
  def destroy
    @elec_process_category = ElecProcessCategory.find(params[:id])
    elec_process_id = @elec_process_category.elec_process_id
    @elec_process_category.destroy

    flash[:success] = 'La categoría fue eliminada correctamente'
    redirect_to elec_processes_show_with_open_pill_path(elec_process_id, 'categories')

  end

  def load_characteristic_values

    values = UserCharacteristic.where(:characteristic_id =>  params[:characteristic_id]).reorder(:valor).map{|user_characteristic| [user_characteristic.formatted_value.upcase]}.uniq

    render json: values

  end

  private
  def mapped_characteristics
    Characteristic.available_all_to_other_modules.map{|characteristic| [characteristic.nombre, characteristic.id]}
  end
end
