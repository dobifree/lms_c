class PeQuestionActivitiesController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:list, :new]
  before_filter :get_data_3, only: [:create]
  before_filter :get_data_2, only: [:edit, :update, :destroy]
  before_filter :validate_open_process, only: [:list, :new, :create, :edit, :update, :destroy]
  before_filter :validate_can_modify_action_plan
  before_filter :validate_create_activity, only: [:new, :create, :edit, :update]
  before_filter :validate_destroy_activity, only: [:destroy]

  def list

  end

  def new

    @pe_question_activity = @pe_question.pe_question_activities.build

  end

  def create

    @pe_question_activity = PeQuestionActivity.new
    @pe_question_activity.user_creator = user_connected
    @pe_question_activity.pe_question = @pe_question

    custom_errors = Array.new

    if @pe_question_activity.save

      completed = true

      @pe_evaluation.pe_question_activity_fields.each do |pe_question_activity_field|

        pe_question_activity_f = @pe_question_activity.pe_question_activities.build
        pe_question_activity_f.pe_question_activity_field = pe_question_activity_field

        if pe_question_activity_field.field_type == 0

          pe_question_activity_f.value_string = params['new_pe_question_activity_field_'+pe_question_activity_field.id.to_s]

        elsif pe_question_activity_field.field_type == 1

          pe_question_activity_f.value_date = params['new_pe_question_activity_field_'+pe_question_activity_field.id.to_s]

        elsif pe_question_activity_field.field_type == 2

          pe_question_activity_f.pe_question_activity_field_list_item_id = params['new_pe_question_activity_field_'+pe_question_activity_field.id.to_s]

        end

        if pe_question_activity_f.valid?

          pe_question_activity_f.save

        else
          completed = false

          custom_errors.push pe_question_activity_field

        end

      end

      if completed

        flash[:success] = t('activerecord.success.model.pe_question_activity.create_ok')
        redirect_to pe_question_activities_list_path(@pe_question)

      else

        @pe_question_activity.destroy

        @pe_question_activity = PeQuestionActivity.new
        @pe_question_activity.user_creator = user_connected
        @pe_question_activity.pe_question = @pe_question

        custom_errors.each do |pe_question_activity_field|

          @pe_question_activity.errors.add(pe_question_activity_field.id.to_s, 'El campo '+pe_question_activity_field.name+' no puede ser vacío')

        end

        render 'new'

      end

    else

      render 'new'

    end


  end

  def edit

  end

  def update

      completed = true

      @pe_question_activity.user_creator = user_connected
      @pe_question_activity.save

      custom_errors = Array.new

      @pe_evaluation.pe_question_activity_fields.each do |pe_question_activity_field|

        pe_question_activity_f = @pe_question_activity.pe_question_activity_by_field pe_question_activity_field

        if pe_question_activity_field.field_type == 0

          pe_question_activity_f.value_string = params['new_pe_question_activity_field_'+pe_question_activity_field.id.to_s]

        elsif pe_question_activity_field.field_type == 1

          pe_question_activity_f.value_date = params['new_pe_question_activity_field_'+pe_question_activity_field.id.to_s]

        elsif pe_question_activity_field.field_type == 2

          pe_question_activity_f.pe_question_activity_field_list_item_id = params['new_pe_question_activity_field_'+pe_question_activity_field.id.to_s]

        end

        if pe_question_activity_f.valid?

          pe_question_activity_f.save

        else
          completed = false

          custom_errors.push pe_question_activity_field

        end

      end

      if completed

        flash[:success] = t('activerecord.success.model.pe_question_activity.update_ok')
        redirect_to pe_question_activities_list_path(@pe_question)

      else

        custom_errors.each do |pe_question_activity_field|

          @pe_question_activity.errors.add(pe_question_activity_field.id.to_s, 'El campo '+pe_question_activity_field.name+' no puede ser vacío')

        end

        render 'edit'

      end



  end

  def destroy

    @pe_question_activity.destroy

    flash[:success] = t('activerecord.success.model.pe_question_activity.delete_ok')
    redirect_to pe_question_activities_list_path(@pe_question)

  end


  private

  def get_data_1

    @pe_question = PeQuestion.find(params[:pe_question_id])
    @pe_element = @pe_question.pe_element
    @pe_evaluation = @pe_question.pe_evaluation
    @pe_process = @pe_evaluation.pe_process

    @pe_member_evaluated = @pe_question.pe_member_evaluated

    @pe_member_evaluator = @pe_process.pe_member_by_user user_connected

    @pe_member_rel = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluator
    @pe_member_rel ? @pe_rel = @pe_member_rel.pe_rel : nil

  end

  def get_data_3

    @pe_question = PeQuestion.find(params[:pe_question_activity][:pe_question_id])
    @pe_element = @pe_question.pe_element
    @pe_evaluation = @pe_question.pe_evaluation
    @pe_process = @pe_evaluation.pe_process

    @pe_member_evaluated = @pe_question.pe_member_evaluated

    @pe_member_evaluator = @pe_process.pe_member_by_user user_connected

    @pe_member_rel = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluator
    @pe_member_rel ? @pe_rel = @pe_member_rel.pe_rel : nil

  end

  def get_data_2

    @pe_question_activity = PeQuestionActivity.find(params[:id])
    @pe_question = @pe_question_activity.pe_question
    @pe_element = @pe_question.pe_element
    @pe_evaluation = @pe_question.pe_evaluation
    @pe_process = @pe_evaluation.pe_process

    @pe_member_evaluated = @pe_question.pe_member_evaluated

    @pe_member_evaluator = @pe_process.pe_member_by_user user_connected

    @pe_member_rel = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluator
    @pe_member_rel ? @pe_rel = @pe_member_rel.pe_rel : nil

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_can_modify_action_plan

    #status_def = @pe_evaluation.definition_by_users_status_of_pe_member_evaluated @pe_member_evaluated

    status_def, step_label, step_label_smart_table = @pe_evaluation.pe_member_definition_by_users_status @pe_member_evaluated

    unless (status_def == 'not_initiated' || status_def == 'not_confirmed' || status_def == 'in_definition') && @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member(@pe_member_evaluator)

      flash[:danger] = t('activerecord.error.model.pe_question_activity.cant_modify')
      redirect_to root_path

    end

  end

  def validate_create_activity

    unless @pe_question.pe_element.allow_action_plan && ( (@pe_evaluation.def_allow_action_plan_by_boss && @pe_member_evaluated.is_evaluated_as_rel_id_by_pe_member(1,@pe_member_evaluator)) || (@pe_evaluation.def_allow_action_plan_with_boss && (@pe_member_evaluated.is_evaluated_as_rel_id_by_pe_member(1,@pe_member_evaluator) || @pe_member_evaluated.user_id == user_connected.id) ))
      flash[:danger] = t('activerecord.error.model.pe_question_activity.cant_create')
      redirect_to root_path
    end

  end

  def validate_destroy_activity

    unless @pe_question.pe_element.allow_action_plan && ( (@pe_evaluation.def_allow_action_plan_by_boss && @pe_member_evaluated.is_evaluated_as_rel_id_by_pe_member(1,@pe_member_evaluator)) || (@pe_evaluation.def_allow_action_plan_with_boss && (@pe_member_evaluated.is_evaluated_as_rel_id_by_pe_member(1,@pe_member_evaluator) || @pe_member_evaluated.user_id == user_connected.id) ))
      flash[:danger] = t('activerecord.error.model.pe_question_activity.cant_destroy')
      redirect_to root_path
    end

  end



end
