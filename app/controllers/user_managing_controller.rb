class UserManagingController < ApplicationController

  include UsersHelper
  include BpModule

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:user_profile,
                                    :reports_privileges_edit, :reports_privileges_update,
                                    :mpi_privileges_edit, :mpi_privileges_update,
                                    :um_privileges_edit, :um_privileges_update,
                                    :user_profile_for_users_manager]
  before_filter :get_data_2, only: [:edit_characteristics, :update_characteristics]
  before_filter :get_data_3, only: [:edit, :edit_photo, :edit_password, :update_password, :edit_email, :update, :update_photo, :update_email, :delete_form, :delete]
  before_filter :get_data_4, only: [:download_characteristic_file]
  before_filter :get_data_5, only: [:download_user_characteristic_file]
  before_filter :get_data_6, only: [:edit_characteristic_history]
  before_filter :get_data_7, only: [:edit_user_characteristic_record, :update_user_characteristic_record, :delete_user_characteristic_record]
  before_filter :get_data_8, only: [:query_custom_report]

  before_filter :verify_access_manage_module, only: [:search, :user_profile,
                                                     :reports,
                                                     :rep_active_user_characteristics,
                                                     :rep_active_user_characteristic_register,
                                                     :rep_active_employees_characteristics, :rep_employees_characteristics,
                                                     :rep_active_employees_characteristic_register, :rep_employees_characteristic_register,
                                                     :rep_active_employees_characteristic_history, :rep_employees_characteristic_history,
                                                     :mpi_privileges_search, :mpi_privileges_edit, :mpi_privileges_update,
                                                     :um_privileges_search, :um_privileges_edit, :um_privileges_update,
                                                     :new_custom_report, :create_custom_report, :edit_custom_report, :update_custom_report, :delete_custom_report]

  before_filter :verify_access_query_reports, only: [:query_custom_reports, :query_custom_report]
  before_filter :verify_access_query_report, only: [:query_custom_report]
  before_filter :verify_access_manage_users, only: [:search_for_users_manager, :user_profile_for_users_manager]
  before_filter :verify_access_manage_users_data, only: [:edit_characteristics, :update_characteristics, :download_characteristic_file, :download_user_characteristic_file,
                                                         :edit_characteristic_history, :edit_user_characteristic_record, :update_user_characteristic_record, :delete_user_characteristic_record]
  before_filter :verify_access_manage_characteristic, only: [:edit_characteristic_history, :edit_user_characteristic_record, :update_user_characteristic_record, :delete_user_characteristic_record]
  before_filter :verify_is_not_current_record, only: [:edit_user_characteristic_record, :update_user_characteristic_record, :delete_user_characteristic_record]
  before_filter :verify_access_manage_users_password, only: [:edit_password, :update_password]
  before_filter :verify_access_manage_users_photo, only: [:edit_photo, :update_photo]
  before_filter :verify_access_manage_users_email, only: [:edit_email, :update_email]
  before_filter :verify_access_delete_users, only: [:delete_form, :delete]
  #before_filter :verify_access_create_users, only: [:new, :create]
  before_filter :verify_access_edit_users, only: [:edit, :update]

  def search

    if params[:user]

      @user = User.new(params[:user])

      if !@user.employee
        @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      else
        @users = search_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      end

    else
      @user = User.new
      @user.employee = false
      @users = {}
    end

  end

  def reports


  end

  def new_custom_report

    @um_custom_report = UmCustomReport.new

  end

  def create_custom_report

    @um_custom_report = UmCustomReport.new(params[:um_custom_report])

    if @um_custom_report.save

      UmCharacteristic.all.each do |um_characteristic|

        if params['um_characteristic_'+um_characteristic.id.to_s] == '1'

          um_custom_report_characteristic = @um_custom_report.um_custom_report_characteristics.build
          um_custom_report_characteristic.um_characteristic = um_characteristic
          um_custom_report_characteristic.position = params['um_characteristic_'+um_characteristic.id.to_s+'_pos']
          um_custom_report_characteristic.save

        end

      end


      flash[:success] = t('activerecord.success.model.user_managinig.create_custom_report_ok')
      redirect_to user_managing_reports_path
    else
      render action: 'new_custom_report'
    end

  end

  def edit_custom_report

    @um_custom_report = UmCustomReport.find params[:id]

  end

  def update_custom_report

    @um_custom_report = UmCustomReport.find params[:id]

    if @um_custom_report.update_attributes params[:um_custom_report]

      @um_custom_report.um_custom_report_characteristics.destroy_all

      UmCharacteristic.all.each do |um_characteristic|

        if params['um_characteristic_'+um_characteristic.id.to_s] == '1'

          um_custom_report_characteristic = @um_custom_report.um_custom_report_characteristics.build
          um_custom_report_characteristic.um_characteristic = um_characteristic
          um_custom_report_characteristic.position = params['um_characteristic_'+um_characteristic.id.to_s+'_pos']
          um_custom_report_characteristic.save

        end

      end


      flash[:success] = t('activerecord.success.model.user_managinig.update_custom_report_ok')
      redirect_to user_managing_reports_path
    else
      render action: 'edit_custom_report'
    end

  end

  def delete_custom_report

    @um_custom_report = UmCustomReport.find params[:id]

    @um_custom_report.destroy

    flash[:success] = t('activerecord.success.model.user_managinig.delete_custom_report_ok')
    redirect_to user_managing_reports_path

  end

  def rep_active_user_characteristics

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az')

    @characteristics = Array.new
    @characteristic_types = Array.new
    @characteristic_types_sizes = Array.new

    CharacteristicType.all.each do |characteristic_type|

      aux = -1

      characteristic_type.characteristics.each_with_index do |characteristic, index_c|

        unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

          @characteristics.push characteristic
          aux +=1

        end

      end

      if aux > -1

        @characteristic_types.push characteristic_type
        @characteristic_types_sizes.push aux

      end

    end

    Characteristic.not_grouped_public_characteristics.each do |characteristic|

      @characteristics.push characteristic unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

    end

    @users = User.where('activo = ?',true)

    filename = 'reporte_consolidado_usuarios_activos_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def rep_active_employees_characteristics

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az')

    @characteristics = Array.new
    @characteristic_types = Array.new
    @characteristic_types_sizes = Array.new

    CharacteristicType.all.each do |characteristic_type|

      aux = -1

      characteristic_type.characteristics.each_with_index do |characteristic, index_c|

        unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

          @characteristics.push characteristic
          aux +=1

        end

      end

      if aux > -1

        @characteristic_types.push characteristic_type
        @characteristic_types_sizes.push aux

      end

    end

    Characteristic.not_grouped_public_characteristics.each do |characteristic|

      @characteristics.push characteristic unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

    end

    @users = User.where('activo = ?', true)

    filename = 'reporte_consolidado_'+alias_users.downcase+'_activos_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def rep_employees_characteristics

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az')

    @characteristics = Array.new
    @characteristic_types = Array.new
    @characteristic_types_sizes = Array.new

    CharacteristicType.all.each do |characteristic_type|

      aux = -1

      characteristic_type.characteristics.each_with_index do |characteristic, index_c|

        unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

          @characteristics.push characteristic
          aux +=1

        end

      end

      if aux > -1

        @characteristic_types.push characteristic_type
        @characteristic_types_sizes.push aux

      end

    end

    Characteristic.not_grouped_public_characteristics.each do |characteristic|

      @characteristics.push characteristic unless characteristic.data_type_id == 6 || characteristic.data_type_id == 7 || characteristic.data_type_id == 11 || characteristic.register_characteristic

    end

    @users = User.where('employee = ?', true)

    filename = 'reporte_consolidado_'+alias_users.downcase+'_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def rep_active_user_characteristic_register

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristic_parent = Characteristic.find(params[:characteristic_id])
    @characteristics = @characteristic_parent.elements_characteristics.reorder('register_position')

    @users = User.where('activo = ?', true)

    filename = 'reporte_usuarios_activos_'+@characteristic_parent.nombre+'_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def rep_active_employees_characteristic_register

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristic_parent = Characteristic.find(params[:characteristic_id])
    @characteristics = @characteristic_parent.elements_characteristics.reorder('register_position')

    @um_characteristics_available = UmCharacteristic.all

    @users = User.where('activo = ?', true)

    filename = 'reporte_'+alias_users.downcase+'_activos_'+@characteristic_parent.nombre+'_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def rep_employees_characteristic_register

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristic_parent = Characteristic.find(params[:characteristic_id])
    @characteristics = @characteristic_parent.elements_characteristics.reorder('register_position')

    @um_characteristics_available = UmCharacteristic.all

    @users = User.all

    filename = 'reporte_'+alias_users.downcase+'_'+@characteristic_parent.nombre+'_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def rep_active_employees_characteristic_history

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristic = Characteristic.find(params[:characteristic_id])

    @users = User.where('activo = ? AND employee = ?', true, true)

    filename = 'reporte_'+alias_users.downcase+'_activos_'+@characteristic.nombre+'_historial_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def rep_employees_characteristic_history

   #@company = session[:company]

    @letras_excel = %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')

    @characteristic = Characteristic.find(params[:characteristic_id])

    @users = User.where('employee = ?', true)

    filename = 'reporte_'+alias_users.downcase+'_'+@characteristic.nombre+'_historial_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def user_profile

  end

  def reports_privileges_search

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

    @users_q = User.users_with_query_reports_privileges

    @ct_module = CtModule.find_by_cod('um')

  end

  def reports_privileges_edit

    @user_ct_module_privileges = @user.user_ct_module_privileges
    @user_ct_module_privileges = UserCtModulePrivileges.new unless @user_ct_module_privileges

  end

  def reports_privileges_update

    user_ct_module_privileges = @user.user_ct_module_privileges
    unless user_ct_module_privileges
      user_ct_module_privileges = UserCtModulePrivileges.new
      user_ct_module_privileges.user = @user
    end

    user_ct_module_privileges.update_attributes(params[:user_ct_module_privileges])

    if user_ct_module_privileges.save

      user_ct_module_privileges.user_ct_module_privileges_um_custom_reports.destroy_all

      if user_ct_module_privileges.um_query_reports

        UmCustomReport.all.each do |um_custom_report|

          if params['um_custom_report_'+um_custom_report.id.to_s] == '1'

            user_ct_module_privileges_um_custom_report = user_ct_module_privileges.user_ct_module_privileges_um_custom_reports.build
            user_ct_module_privileges_um_custom_report.um_custom_report = um_custom_report
            user_ct_module_privileges_um_custom_report.save

          end

        end

      end

    end

    flash[:success] = t('activerecord.success.model.user_managinig.update_reports_privileges_ok')
    redirect_to user_managing_reports_privileges_search_path

  end

  def mpi_privileges_search

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

    @users_q = User.users_with_mpi_view_users_privileges

  end

  def mpi_privileges_edit

    @user_ct_module_privileges = @user.user_ct_module_privileges
    @user_ct_module_privileges = UserCtModulePrivileges.new unless @user_ct_module_privileges

  end

  def mpi_privileges_update

    user_ct_module_privileges = @user.user_ct_module_privileges
    unless user_ct_module_privileges
      user_ct_module_privileges = UserCtModulePrivileges.new
      user_ct_module_privileges.user = @user
    end

    user_ct_module_privileges.update_attributes(params[:user_ct_module_privileges])

    user_ct_module_privileges.save

    flash[:success] = t('activerecord.success.model.user_managinig.update_mpi_privileges_ok')
    redirect_to user_managing_mpi_privileges_search_path

  end

  def um_privileges_search

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

    @users_q = User.users_with_um_manage_users_privileges

    @ct_module = CtModule.find_by_cod('um')

  end

  def um_privileges_edit

    @ct_module = CtModule.find_by_cod('um')

    @user_ct_module_privileges = @user.user_ct_module_privileges
    @user_ct_module_privileges = UserCtModulePrivileges.new unless @user_ct_module_privileges

  end

  def um_privileges_update

    user_ct_module_privileges = @user.user_ct_module_privileges
    unless user_ct_module_privileges
      user_ct_module_privileges = UserCtModulePrivileges.new
      user_ct_module_privileges.user = @user
    end

    user_ct_module_privileges.update_attributes(params[:user_ct_module_privileges])

    user_ct_module_privileges.save

    flash[:success] = t('activerecord.success.model.user_managinig.update_um_privileges_ok')
    redirect_to user_managing_um_privileges_search_path

  end

  def delete_form

  end

  def delete

    @user.activo = false

    if @user.update_attributes(params[:user])

      if @company.is_security

        begin
          UmMailer.delete_user_security(@user, @company).deliver
        rescue Exception => e
          lm = LogMailer.new
          lm.module = 'um'
          lm.step = 'delete_user'
          lm.description = e.message+' '+e.backtrace.inspect
          lm.registered_at = lms_time
          lm.save
        end

      end

      flash[:success] = t('activerecord.success.model.user.delete_ok')

      if user_connected.um_delete_users
        redirect_to user_managing_user_profile_for_users_manager_path(@user)
      else
        redirect_to user_managing_user_profile_path(@user)
      end



    else
      render 'delete_form'

    end

  end
=begin
  def new
    @user = User.new
    @char_errors = Array.new
  end

  def create
    @user = User.new(params[:user])

    errors = false

    @char_errors = Array.new

    Characteristic.where('required_new = ? AND required_new_c = ?', true, true).each do |characteristic|

      #puesto con módulo de puestos

      params['characteristic_'+characteristic.id.to_s] = @user.j_job_id if characteristic.data_type_id == 13 && ct_module_jobs?

      #centro de costos

      params['characteristic_'+characteristic.id.to_s] = @user.j_cost_center_id if characteristic.data_type_id == 15

      if params[('characteristic_'+characteristic.id.to_s)].blank?

        if characteristic.characteristic_value

          if params[('characteristic_'+characteristic.characteristic_value.characteristic.id.to_s)] == characteristic.characteristic_value.id.to_s && characteristic.char_value_selected

            @user.valid? unless errors

            errors = true
            @char_errors.push characteristic.id

            @user.errors.add(characteristic.nombre, 'no puede ser vacío')

          elsif params[('characteristic_'+characteristic.characteristic_value.characteristic.id.to_s)] != characteristic.characteristic_value.id.to_s && !characteristic.char_value_selected

            @user.valid? unless errors

            errors = true
            @char_errors.push characteristic.id

            @user.errors.add(characteristic.nombre, 'no puede ser vacío')

          end

        else
          @user.valid? unless errors

          errors = true
          @char_errors.push characteristic.id

          @user.errors.add(characteristic.nombre, 'no puede ser vacío')
        end

      end

    end

    if errors
      render 'new'
    else
      if @user.save

        Characteristic.where('required_new = ?', true).each do |characteristic|

          new_value = nil

          if characteristic.data_type_id == 11

            #register

            current_ucs_ids = Array.new

            @user.user_characteristics_registered(characteristic).each {|c_uc| current_ucs_ids.push c_uc.id.to_s}

            if params['characteristic_'+characteristic.id.to_s+'_create']

              n_aux_files = Array.new

              characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|
                n_aux_files[index_element] = 0
              end

              params['characteristic_'+characteristic.id.to_s+'_create'].each_with_index do |c_id, n_aux|

                if c_id == '-1'

                  error, uc = @user.add_user_characteristic(characteristic, '-', lms_time, user_connected, user_connected, nil, nil, @company)

                  characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                    new_value = nil

                    unless element_characteristic.data_type_id == 6
                      new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s).to_sym][n_aux].blank?
                    else
                      #file

                      if params[('characteristic_file_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux] == '1'

                        new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)] && params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]]

                        n_aux_files[index_element] += 1

                      end

                    end

                    if new_value

                      error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                      if !error_in && uc_element

                        uc_element.register_user_characteristic = uc
                        uc_element.save

                      end

                    end

                  end

                else

                  current_ucs_ids.delete(c_id)

                  uc = @user.user_characteristics.where('id = ?', c_id).first

                  characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                    new_value = nil

                    uc_element = uc.elements_user_characteristics_by_characteristic(element_characteristic)

                    unless element_characteristic.data_type_id == 6

                      uc_element.destroy if uc_element

                      new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s).to_sym][n_aux].blank?

                      error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                      if !error_in && uc_element

                        uc_element.register_user_characteristic = uc
                        uc_element.save

                      end

                    else
                      #file

                      if params[('characteristic_file_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux] == '1'

                        new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)] && params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]]

                        if new_value

                          n_aux_files[index_element] += 1

                          if uc_element
                            uc_element.delete_file @company
                            uc_element.destroy
                          end

                          error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                          if !error_in && uc_element

                            uc_element.register_user_characteristic = uc
                            uc_element.save

                          end

                        end

                      end

                    end

                  end

                end

              end

            end

            current_ucs_ids.each { |current_uc_id| @user.user_characteristics.where('id = ?', current_uc_id).destroy_all }

          elsif characteristic.data_type_id == 13 && ct_module_jobs?

            if @user.j_job_id

              from_date = @user.from_date

              user_j_job_last = @user.user_j_jobs.last

              if user_j_job_last && user_j_job_last.to_date.nil?
                user_j_job_last.to_date = from_date-1.day
                user_j_job_last.save
              end

              user_j_job = @user.user_j_jobs.build
              user_j_job.j_job_id = @user.j_job_id
              user_j_job.from_date = from_date
              user_j_job.save

              @user.update_user_characteristic(characteristic, @user.j_job.name, from_date.to_datetime, user_connected, user_connected, nil, nil, @company)

            end

          else

            if (characteristic.data_type_id == 13 && !ct_module_jobs?) || characteristic.data_type_id != 13

              unless characteristic.data_type_id == 6
                new_value = params[('characteristic_'+characteristic.id.to_s).to_sym].to_s.strip unless params[('characteristic_'+characteristic.id.to_s).to_sym].blank?
              else
                #file
                new_value = params[('characteristic_'+characteristic.id.to_s).to_sym]
              end

              from_date = @user.from_date

              @user.update_user_characteristic(characteristic, new_value, from_date.to_datetime, user_connected, user_connected, nil, nil, @company)

            end

          end


        end

        cw = Characteristic.get_characteristic_from_date_work
        if cw
          @user.update_user_characteristic(cw, @user.from_date, lms_time, user_connected, user_connected, nil, nil, @company)
        end

        if @company.is_security

          begin
            UmMailer.create_user_security(@user, @company).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'um'
            lm.step = 'create_user'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

        flash[:success] = t('activerecord.success.model.user.create2_ok')
        if user_connected.um_create_users
          redirect_to user_managing_user_profile_for_users_manager_path(@user)
        else
          redirect_to user_managing_user_profile_path(@user)
        end
      else
        render 'new'
      end
    end


  end
=end
  def query_custom_reports


  end

  def query_custom_report

    if params[:active] == '1'
      @users = User.where('activo = ?', true)
      filename = @um_custom_report.name+'_activos_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'
    else
      @users = User.all
      filename = @um_custom_report.name+'_todos_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'
    end

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def search_for_users_manager
    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

  end

  def user_profile_for_users_manager

  end


  def download_characteristic_file

    directory = @company.directorio+'/'+@company.codigo+'/user_profiles/'+@user.id.to_s+'/characteristics/'+@characteristic.id.to_s+'/'


    send_file directory+@user_characteristic.value_file,
              filename: @characteristic.nombre+'_'+@user_characteristic.value_file,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_user_characteristic_file

    directory = @company.directorio+'/'+@company.codigo+'/user_profiles/'+@user.id.to_s+'/characteristics/'+@characteristic.id.to_s+'/'


    send_file directory+@user_characteristic.value_file,
              filename: @characteristic.nombre+'_'+@user_characteristic.value_file,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def edit_characteristics

  end

  def update_characteristics

    if @characteristic_type
      characteristics = @characteristic_type.characteristics_updated_by_managers
    else
      characteristics = Characteristic.not_grouped_characteristics_updated_by_managers
    end

    characteristics.each do |characteristic|

      if characteristic.updated_by_manager

        new_value = nil

        if characteristic.data_type_id == 11

          #register

          current_ucs_ids = Array.new

          @user.user_characteristics_registered(characteristic).each {|c_uc| current_ucs_ids.push c_uc.id.to_s}

          if params['characteristic_'+characteristic.id.to_s+'_create']

            n_aux_files = Array.new

            characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|
              n_aux_files[index_element] = 0
            end

            params['characteristic_'+characteristic.id.to_s+'_create'].each_with_index do |c_id, n_aux|

              if c_id == '-1'

                error, uc = @user.add_user_characteristic(characteristic, '-', lms_time, user_connected, user_connected, nil, nil, @company)

                characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                  new_value = nil

                  unless element_characteristic.data_type_id == 6
                    new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s).to_sym][n_aux].blank?
                  else
                    #file

                    if params[('characteristic_file_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux] == '1'

                      new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)] && params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]]

                      n_aux_files[index_element] += 1

                    end

                  end

                  if new_value

                    error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                    if !error_in && uc_element

                      uc_element.register_user_characteristic = uc
                      uc_element.save

                    end

                  end

                end

              else

                current_ucs_ids.delete(c_id)

                uc = @user.user_characteristics.where('id = ?', c_id).first

                characteristic.elements_characteristics.each_with_index do |element_characteristic, index_element|

                  new_value = nil

                  uc_element = uc.elements_user_characteristics_by_characteristic(element_characteristic)

                  unless element_characteristic.data_type_id == 6

                    uc_element.destroy if uc_element

                    new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux].strip unless params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s).to_sym][n_aux].blank?

                    error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                    if !error_in && uc_element

                      uc_element.register_user_characteristic = uc
                      uc_element.save

                    end

                  else
                    #file

                    if params[('characteristic_file_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux] == '1'

                      new_value = params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]] if params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)] && params[('characteristic_'+characteristic.id.to_s+'_'+element_characteristic.id.to_s)][n_aux_files[index_element]]

                      if new_value

                        n_aux_files[index_element] += 1

                        if uc_element
                          uc_element.delete_file @company
                          uc_element.destroy
                        end

                        error_in, uc_element = @user.add_user_characteristic(element_characteristic, new_value, lms_time, user_connected, user_connected, nil, nil, @company)

                        if !error_in && uc_element

                          uc_element.register_user_characteristic = uc
                          uc_element.save

                        end

                      end

                    end

                  end

                end

              end

            end

          end

          current_ucs_ids.each { |current_uc_id| @user.user_characteristics.where('id = ?', current_uc_id).destroy_all }

        elsif characteristic.data_type_id == 13 && ct_module_jobs?

          if @user.j_job_id != params[:user][:j_job_id].to_i

            if @user.update_attributes(params[:user])

              from_date = lms_time

              from_date = params[('characteristic_'+characteristic.id.to_s)+'_from'].to_datetime if characteristic.has_history

              user_j_job_last = @user.user_j_jobs.last

              if user_j_job_last && user_j_job_last.to_date.nil?
                user_j_job_last.to_date = from_date-1.day
                user_j_job_last.save
              end

              user_j_job = @user.user_j_jobs.build
              user_j_job.j_job_id = @user.j_job_id
              user_j_job.from_date = from_date
              user_j_job.save

              j_job_name = ''
              j_job_name = @user.j_job.name if @user.j_job

              @user.update_user_characteristic(characteristic, j_job_name, from_date.to_datetime, user_connected, user_connected, nil, nil, @company)

            end

          end

        else

          if (characteristic.data_type_id == 13 && !ct_module_jobs?) || characteristic.data_type_id != 13

            unless characteristic.data_type_id == 6
              new_value = params[('characteristic_'+characteristic.id.to_s).to_sym].strip unless params[('characteristic_'+characteristic.id.to_s).to_sym].blank?
            else
              #file
              new_value = params[('characteristic_'+characteristic.id.to_s).to_sym]
            end

            from_date = lms_time

            from_date = params[('characteristic_'+characteristic.id.to_s)+'_from'].to_datetime if characteristic.has_history

            @user.update_user_characteristic(characteristic, new_value, from_date, user_connected, user_connected, nil, nil, @company)

          end

        end

      end

    end

    flash[:success] = t('activerecord.success.model.user_managinig.update_characteristics_ok')

    if @characteristic_type

      if user_connected.um_manage_users_data
        redirect_to user_managing_user_profile_for_users_manager_after_update_path(@user,@characteristic_type)
      else
        redirect_to user_managing_user_profile_after_update_path(@user,@characteristic_type)
      end

    else

      if user_connected.um_manage_users_data
        redirect_to user_managing_user_profile_for_users_manager_path(@user)
      else
        redirect_to user_managing_user_profile_path(@user)
      end

    end


  end

  def edit_characteristic_history

  end

  def edit_user_characteristic_record

  end

  def update_user_characteristic_record

    original_values = @user_characteristic_record.formatted_value+' | '+localize(@user_characteristic_record.from_date, format: :full_date_time_log)+' | '+localize(@user_characteristic_record.to_date, format: :full_date_time_log)

    @user_characteristic_record.update_record(params[:value],params[:from_date],params[:to_date])

    @user_characteristic_record.to_date += (24*3600-1).seconds

    if @user_characteristic_record.save

      log_ct_modeule_um = LogCtModuleUm.new
      log_ct_modeule_um.task = 'update record'
      log_ct_modeule_um.performed_at = lms_time
      log_ct_modeule_um.user = @user_characteristic_record.user
      log_ct_modeule_um.performed_by_manager = user_connected
      log_ct_modeule_um.description = 'from: '+original_values+' - to: '+@user_characteristic_record.formatted_value+' | '+localize(@user_characteristic_record.from_date, format: :full_date_time_log)+' | '+localize(@user_characteristic_record.to_date, format: :full_date_time_log)
      log_ct_modeule_um.save

      flash[:success] = t('activerecord.success.model.user_managinig.update_user_characteristic_record_ok')

    else

      flash[:danger] = t('activerecord.error.model.user_managinig.update_user_characteristic_record_error')

    end

    redirect_to user_managing_edit_characteristic_history_path @user, @characteristic

  end

  def delete_user_characteristic_record

    original_values = @user_characteristic_record.formatted_value+' | '+localize(@user_characteristic_record.from_date, format: :full_date_time_log)+' | '+localize(@user_characteristic_record.to_date, format: :full_date_time_log)

    if @user_characteristic_record.destroy

      log_ct_modeule_um = LogCtModuleUm.new
      log_ct_modeule_um.task = 'delete record'
      log_ct_modeule_um.performed_at = lms_time
      log_ct_modeule_um.user = @user_characteristic_record.user
      log_ct_modeule_um.performed_by_manager = user_connected
      log_ct_modeule_um.description = original_values
      log_ct_modeule_um.save

      flash[:success] = t('activerecord.success.model.user_managinig.delete_user_characteristic_record_ok')

    else

      flash[:danger] = t('activerecord.error.model.user_managinig.delete_user_characteristic_record_error')

    end

    redirect_to user_managing_edit_characteristic_history_path @user, @characteristic

  end

  def edit

  end

  def update

    if @user.update_attributes params[:user]
      flash[:success] = t('activerecord.success.model.user.update_ok')

      if user_connected.um_edit_users
        redirect_to user_managing_user_profile_for_users_manager_path(@user)
      else
        redirect_to user_managing_user_profile_path(@user)
      end

    else
      render 'edit'
    end

  end

  def edit_password

  end

  def update_password

    unless params[:user][:password].blank?

      if @user.update_attributes(params[:user])

        flash[:success] = t('activerecord.success.model.user.update_pass_ok')

        if user_connected.um_manage_users_password
          redirect_to user_managing_user_profile_for_users_manager_path(@user)
        else
          redirect_to user_managing_user_profile_path(@user)
        end

      else
        render 'edit_password'

      end

    else

      @user.errors.add :base, t('activerecord.errors.models.user.attributes.password.blank_update')
      render 'edit_password'

    end

  end

  def edit_email

  end

  def update_email

    if @user.update_attributes(params[:user])

      flash[:success] = t('activerecord.success.model.user.update_email_ok')

      if user_connected.um_manage_users_email
        redirect_to user_managing_user_profile_for_users_manager_path(@user)
      else
        redirect_to user_managing_user_profile_path(@user)
      end

    else
      render 'edit_email'

    end

  end

  def edit_photo

  end

  def update_photo

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext != '.jpg' && ext != '.jpeg'

        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_format')
        redirect_to user_managing_edit_photo_path(@user)

      elsif File.size(params[:upload]['datafile'].tempfile) > 70.kilobytes

        flash[:danger] = t('activerecord.error.model.user.update_foto_wrong_wrong_size')
        redirect_to user_managing_edit_photo_path(@user)

      else

        save_foto params[:upload], @user, @company

        flash[:success] = t('activerecord.success.model.user.update_foto')

        if user_connected.um_manage_users_photo
          redirect_to user_managing_user_profile_for_users_manager_path(@user.id)
        else
          redirect_to  user_managing_user_profile_path(@user.id)
        end

      end


    else

      flash[:danger] = t('activerecord.error.model.user.update_foto_empty_file')

      redirect_to user_managing_edit_photo_path(@user)

    end

  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_query_reports

    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1) || user_connected.um_query_reports?
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def verify_access_query_report

    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1) || user_connected.um_query_report?(@um_custom_report)
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_manage_users
    unless user_connected.um_manage_users
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_manage_users_data

    unless user_connected.um_manage_users_data || user_connected.manage_ct_module_um
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_manage_characteristic

    unless @characteristic.has_history
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def verify_is_not_current_record


    unless @user_characteristic_record.to_date

      flash[:danger] = t('activerecord.error.model.user_managinig.delete_update_current_user_characteristic_record_error')
      redirect_to root_path

    end


  end

  def verify_access_manage_users_password
    unless user_connected.um_manage_users_password || user_connected.manage_ct_module_um
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_manage_users_photo
    unless user_connected.um_manage_users_photo || user_connected.manage_ct_module_um
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end
  
  def verify_access_manage_users_email
    unless user_connected.um_manage_users_email || user_connected.manage_ct_module_um
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_delete_users

    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first

    if ct_module.create_delete_users? && (user_connected.um_delete_users || user_connected.manage_ct_module_um)
    else

      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_create_users

    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first

    if ct_module.create_delete_users? && (user_connected.um_create_users || user_connected.manage_ct_module_um)
    else

      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_edit_users

    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first

    if (user_connected.um_edit_users || user_connected.manage_ct_module_um)
    else

      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def get_data_1
   #@company = session[:company]
    @user = User.find(params[:user_id])
  end

  def get_data_2
   #@company = session[:company]
    @user = User.find(params[:user_id])
    @characteristic_type = CharacteristicType.find(params[:characteristic_type_id]) if params[:characteristic_type_id]
  end

  def get_data_3
   #@company = session[:company]
    @user = User.find(params[:user_id])
  end

  def get_data_4
   #@company = session[:company]
    @user = User.find(params[:user_id])

    @characteristic = Characteristic.find_by_id(params[:characteristic_id])
    @user_characteristic = @user.user_characteristics.find_by_characteristic_id(params[:characteristic_id])

  end

  def get_data_5
   #@company = session[:company]
    @user = User.find(params[:user_id])

    @user_characteristic = @user.user_characteristics.find_by_id(params[:user_characteristic_id])
    @characteristic = @user_characteristic ? @user_characteristic.characteristic : nil

  end

  def get_data_6
   #@company = session[:company]
    @user = User.find(params[:user_id])

    @characteristic = Characteristic.find_by_id(params[:characteristic_id])
    @characteristic_type = @characteristic.characteristic_type
  end

  def get_data_7

   #@company = session[:company]

    @user_characteristic_record = UserCharacteristicRecord.find(params[:user_characteristic_record_id])

    @user = @user_characteristic_record.user

    @characteristic = @user_characteristic_record.characteristic

  end

  def get_data_8
    @um_custom_report = UmCustomReport.find params[:id]
  end

  def save_foto(upload, user, company)

    ext = File.extname(upload['datafile'].original_filename)

    if ext == '.jpg' || ext == '.jpeg'

      if File.size(upload['datafile'].tempfile) <= 70.kilobytes

        directory = company.directorio + '/' + company.codigo + '/fotos_usuarios/'

        FileUtils.mkdir_p directory unless File.directory? directory

        if user.foto
          file = directory + user.foto
          File.delete(file) if File.exist? file
        end

        user.set_foto

        user.save

        #puts user.errors.full_messages

        file = directory + user.foto

        File.open(file, 'wb') {|f| f.write(upload['datafile'].read)}

      end

    end

  end

end
