class LmsAdminEnrollmentsController < ApplicationController

  include LmsAdminEnrollmentsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
user_connected
  end

  ## users
  def show_list_to_enroll_users
    @users_added = User.where(:id => lms_list_users_to_enroll)
    render :layout => false
  end

  def add_users_to_list
    added_users = lms_add_users_to_enroll_list(params['users'])
    #users = User.where(:id => added_users).map{|user| [user.id, user.codigo ,user.comma_full_name]}
    render :json => true
  end

  def remove_user_from_list
    lms_remove_user_from_enroll_list(params[:user_id])
    render :json => true
  end


  ## courses
  def show_list_to_enroll_courses
    @courses_added = ProgramCourse.where(:id => lms_list_courses_to_enroll)

    @courses_detail = lms_detail_courses_to_enroll

    @programs = Program.where(:especifico => true, :activo => true)

    render :layout => false
  end

  def add_courses_to_list
    added_courses = lms_add_courses_to_enroll_list(params['program_courses'])

    render :json => true
  end

  def remove_course_from_list
    lms_remove_course_from_enroll_list(params[:program_course_id])
    render :json => true
  end

  def save_dates_to_course
    if program_course_id = params['program_course_id']
      program_course = ProgramCourse.find(params['program_course_id'])
      desdehasta = params['desdehasta_' + program_course_id]
      dates_by_element = params['dates_by_element_' + program_course_id]
      if dates_by_element == '1'
        desdehasta_elements = {}
        elements, elements_ids = program_course.course.ordered_elements
        elements.each_index do |index_el|
          ei = elements_ids[index_el].gsub('-', '_')
          field_name = 'desdehasta_'+program_course.id.to_s+'_'+ei
          desdehasta_elements[field_name] = params[field_name]
        end
      end
      lms_add_dates_to_course(program_course_id, desdehasta, dates_by_element, desdehasta_elements)
    end
    render :json => true
  end

  #Confirmation

  def show_enroll_confirmation

    @user_list = User.where(:id => lms_list_users_to_enroll)
    @program_course_list = ProgramCourse.where(:id => lms_list_courses_to_enroll)

    @courses_detail = lms_detail_courses_to_enroll
    render :layout => false
  end

  def confirm_enroll

    if enrollment_is_ok?


      @users_cant_enroll = Array.new
      @program_courses_cant_enroll = Array.new
      @reasons_cant_enroll = Array.new


      selected_users = Array.new
      selected_program_courses = Array.new

      if lms_list_users_to_enroll
        lms_list_users_to_enroll.each do |user_id|
          selected_users.push User.find(user_id)
        end

      end

      if lms_list_courses_to_enroll
        lms_list_courses_to_enroll.each do |program_course_id|
          selected_program_courses.push ProgramCourse.find(program_course_id)
        end
      end

      selected_program_courses.each_with_index do |program_course, index_pc|

        program = program_course.program
        course = program_course.course

        desde_hasta = lms_detail_courses_to_enroll[program_course.id.to_s]['desdehasta'].split(' - ')
        from_date = DateTime.strptime(desde_hasta[0], '%d/%m/%Y %I:%M %p')
        to_date = DateTime.strptime(desde_hasta[1], '%d/%m/%Y %I:%M %p')

        program_course_instance = program_course.program_course_instance_by_dates(from_date, to_date)

        unless program_course_instance
          program_course_instance = program_course.program_course_instances.build
          program_course_instance.from_date = from_date
          program_course_instance.to_date = to_date
          program_course_instance = nil unless program_course_instance.save
        end

        if program_course_instance
          selected_users.each do |user|
            enrollment = program.enrollment user

            unless enrollment
              enrollment = program.enrollments.build
              enrollment.user = user
              enrollment.level = program.levels.first

              enrollment = nil unless enrollment.save
            end
            if enrollment

              forzar_matricula = false
              forzar_matricula = true if params[:forzar_matricula] && params[:forzar_matricula][:aplica].to_s == '1'

              user_course_prev = enrollment.user_courses.where(:program_course_id => program_course.id, :anulado => false).order('id DESC').first
              estado_prev = nil
              estado_prev = user_course_prev.estado(lms_time) if user_course_prev

              matricular = false

              if forzar_matricula
                fd = params[:forzar_date].split('/').map { |s| s.to_i }
                fd = DateTime.new(fd[2], fd[1], fd[0], 0, 0, 0)
                if estado_prev
                  if estado_prev == :en_progreso || estado_prev == :no_iniciado
                    @reasons_cant_enroll.push 'Curso en progreso o no iniciado'
                  else
                    if estado_prev == :aprobado && user_course_prev.fin >= fd
                      @reasons_cant_enroll.push 'Curso no aprobado antes de '+fd.strftime('%d/%m/%Y')
                    else
                      matricular = true
                    end
                  end
                else
                  matricular = true
                end
                matricular = true unless estado_prev && (() || (estado_prev == :aprobado && user_course_prev.fin >= fd))
              else
                if estado_prev
                  if estado_prev == :aprobado || estado_prev == :en_progreso || estado_prev == :no_iniciado
                    @reasons_cant_enroll.push 'Curso aprobado, en progreso o no iniciado'
                  else
                    matricular = true
                  end
                else
                  matricular = true
                end
              end

              if matricular
                user_course = program_course_instance.user_courses.where(:enrollment_id => enrollment.id, :desde => from_date, :hasta => to_date).first

                unless user_course

                  user_course = program_course_instance.user_courses.build

                  user_course.enrollment = enrollment
                  user_course.program_course = program_course
                  user_course.course = course
                  user_course.numero_oportunidad = 1
                  user_course.desde = from_date
                  user_course.hasta = to_date

                end
                if user_course.save
                  if lms_detail_courses_to_enroll[program_course.id.to_s]['dates_by_element'] == '1' #params['dates_by_element_'+program_course.id.to_s] == '1'
                    elements, elements_ids = program_course.course.ordered_elements

                    elements.each_with_index do |element, index_el|
                      ei = elements_ids[index_el].gsub('-', '_')
                      tipo = elements_ids[index_el].split('-')[0]
                      field_name = 'desdehasta_'+program_course.id.to_s+'_' + ei

                      desde_hasta = lms_detail_courses_to_enroll[program_course.id.to_s]['desdehasta_elements'][field_name].split(' - ') #params['desdehasta_'+program_course.id.to_s+'_'+tipo+'_'+element.id.to_s].split(' - ')
                      from_date_el = DateTime.strptime(desde_hasta[0], '%d/%m/%Y %I:%M %p')
                      to_date_el = DateTime.strptime(desde_hasta[1], '%d/%m/%Y %I:%M %p')

                      if tipo == 'u'
                        user_course_unit = user_course.user_course_units.build
                        user_course_unit.unit = element
                        user_course_unit.from_date = from_date_el
                        user_course_unit.to_date = to_date_el
                        user_course_unit.save
                      elsif tipo == 'e'
                        user_course_evaluation = user_course.user_course_evaluations.build
                        user_course_evaluation.evaluation = element
                        user_course_evaluation.from_date = from_date_el
                        user_course_evaluation.to_date = to_date_el
                        user_course_evaluation.save
                      elsif tipo == 'p'
                        user_course_poll = user_course.user_course_polls.build
                        user_course_poll.poll = element
                        user_course_poll.from_date = from_date_el
                        user_course_poll.to_date = to_date_el
                        user_course_poll.save
                      end
                    end
                  end
                end
              else
                @users_cant_enroll.push user
                @program_courses_cant_enroll.push program_course
              end
            end
          end
        end
      end

      lms_remove_enrollment_data

      flash[:success] = t('activerecord.success.model.enrollment.create_ok')

      render :json => true

    else
      flash[:danger] = 'Errores al procesar la matrícula'
      render :json => false
    end

  end

end
