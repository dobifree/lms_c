class Tr2ReportingController < ApplicationController

  include Tr2ReportingHelper

  before_filter :authenticate_user

  def list_program_instances
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def show_program_results

    @program_instance = ProgramInstance.find(params[:program_instance_id])
   #@company = session[:company]

  end

  def rep_fact_list_program_instances
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def rep_fact_list_program_course_instances
    @program_instance = ProgramInstance.find(params[:program_instance_id])
  end

  def rep_fact
    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])

    reporte_excel = xls_rep_fact @program_course_instance

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    sence_code = @program_course_instance.program_course.course.sence_code
    sence_code = '' if sence_code.nil?

    send_file archivo_temporal,
              filename: 'RepFact_'+sence_code+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def rep_carga_otic_list_program_instances
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def rep_carga_otic_list_program_course_instances
    @program_instance = ProgramInstance.find(params[:program_instance_id])
  end

  def rep_carga_otic_xls

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])

    reporte_excel = xls_rep_carga_otic @program_course_instance

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    sence_code = @program_course_instance.program_course.course.sence_code
    sence_code = '' if sence_code.nil?

    send_file archivo_temporal,
              filename: 'RepCargaOTIC_'+sence_code+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def rep_carga_otic_csv

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])

    archivo_temporal = csv_rep_carga_otic @program_course_instance

    sence_code = @program_course_instance.program_course.course.sence_code
    sence_code = '' if sence_code.nil?

    send_file archivo_temporal,
              filename: 'RepCargaOTIC_'+sence_code+'.csv',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def rep_consolidado_global_list_program_instances
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def rep_consolidado_global
    @program_instance = ProgramInstance.find(params[:program_instance_id])

    reporte_excel = xls_rep_consolidado_global @program_instance

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'RepConsolidadoGlobal_'+@program_instance.name+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def rep_resultados_finales_list_program_instances
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def rep_resultados_finales
    @program_instance = ProgramInstance.find(params[:program_instance_id])

    reporte_excel = xls_rep_resultados_finales @program_instance

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'RepResultadosFinales_'+@program_instance.name+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def rep_certificados

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    require 'prawn/measurement_extensions'

   company = @company
    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_programas/'

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    user_connected_time_zone_pdf = user_connected_time_zone

    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: [720,540]) do |pdf|

      pdf.font_families.update('Foco Light' => {:normal => '/var/www/remote-storage/fonts/Foco_Std_Lt_0.ttf'})
      pdf.font_families.update('HaasGrotDisp Light' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-45Light_1.ttf'})
      pdf.font_families.update('HaasGrotDisp Normal' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-65Medium_1.ttf'})
      pdf.font_families.update('HaasGrotDisp Bold' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-75Bold_1.ttf'})

      @program_instance.enrollments.where('aprobado = ?', true).each do |enrollment|

        cc = @program_instance.program.course_certificates.first

        if cc && cc.template

          plantilla = directory+(cc.template)+'.jpg'

          pdf.start_new_page
          pdf.image plantilla, at: [-36,504], width: 720

          nombre =  enrollment.user.nombre+' '+enrollment.user.apellidos

          nombre_curso =  @program_instance.name

          desde = ''
          desde = I18n.localize(@program_instance.from_date, format: cc.desde_formato.to_sym) if @program_instance.from_date
          hasta = ''
          hasta = I18n.localize(@program_instance.to_date, format: cc.hasta_formato.to_sym) if @program_instance.to_date

          fecha = @program_instance.to_date if @program_instance.to_date && cc.fecha_source == 'fin_convocatoria'

          fecha = fecha+cc.fecha_source_delay.days
          fecha = I18n.localize(fecha, format: :full_date)



          if cc.nombre
            if cc.nombre_x == -1
              pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :size => cc.nombre_size} ], :at => [0, cc.nombre_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :size => cc.nombre_size} ], :at => [cc.nombre_x.mm, cc.nombre_y.mm]
            end
          end

          if cc.nombre_curso

            if cc.nombre_curso_x == -1
              pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :size => cc.nombre_curso_size} ], :at => [0, cc.nombre_curso_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :size => cc.nombre_curso_size} ], :at => [cc.nombre_curso_x.mm, cc.nombre_curso_y.mm]
            end
          end

          if cc.fecha
            if cc.fecha_x == -1
              pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :size => cc.fecha_size} ], :at => [cc.fecha_x.mm, cc.fecha_y.mm]
            end
          end

          if cc.desde
            if cc.desde_x == -1
              pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :size => cc.desde_size} ], :at => [0, cc.desde_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :size => cc.desde_size} ], :at => [cc.desde_x.mm, cc.desde_y.mm]
            end
          end

          if cc.hasta
            if cc.hasta_x == -1
              pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :size => cc.hasta_size} ], :at => [0, cc.hasta_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :size => cc.hasta_size} ], :at => [cc.hasta_x.mm, cc.hasta_y.mm]
            end
          end


        end

      end

    end

    send_file archivo_temporal,
              filename: 'Certificados - '+@program_instance.name+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

end
