class JFunctionsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module


  # GET /j_functions
  # GET /j_functions.json
  def index
    @j_functions = JFunction.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @j_functions }
    end
  end

  # GET /j_functions/1
  # GET /j_functions/1.json
  def show
    @j_function = JFunction.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @j_function }
    end
  end

  def new
    @j_function = JFunction.new
    @j_function.j_job_id = params[:j_job_id]
    @j_job = JJob.find(@j_function.j_job_id)
  end

  def create

    @j_function = JFunction.new(params[:j_function])

    if @j_function.save
      flash[:success] = t('activerecord.success.model.j_function.create_ok')
      redirect_to @j_function.j_job
    else
      @j_job = JJob.find(params[:j_function][:j_job_id])
      render action: 'new'
    end

  end

  def edit
    @j_function = JFunction.find(params[:id])
    @j_job = @j_function.j_job
  end

  def update
    @j_function = JFunction.find(params[:id])

    if @j_function.update_attributes(params[:j_function])
      flash[:success] = t('activerecord.success.model.j_function.update_ok')
      redirect_to @j_function.j_job
    else
      @j_job = @j_function.j_job
      render action: 'edit'
    end

  end

  def destroy

    @j_function = JFunction.find(params[:id])
    j_job = @j_function.j_job
    @j_function.destroy

    flash[:success] = t('activerecord.success.model.j_function.delete_ok')
    redirect_to j_job
  end

  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'jobs', true).first

      unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end


end
