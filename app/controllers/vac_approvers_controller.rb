class VacApproversController < ApplicationController
  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def new
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def create
    approver = approver_complete_create(params[:user_id])
    flash[:success] = t('views.vac_approvers.flash_messages.success_created')
    redirect_to approver
  end

  def show
    @vac_approver = VacApprover.find(params[:id])
  end

  def deactivate
    approver_deactivate(params[:vac_approver_id])
    flash[:success] = t('views.vac_approvers.flash_messages.sucess_deleted')
    redirect_to vac_manager_index_path
  end

  def aprovee_new
    @approver = VacApprover.find(params[:vac_approver_id])
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def approvee_create
    approver = approvee_complete_create(params[:vac_approver_id], params[:user_id])
    flash[:success] = t('views.vac_approvers.flash_messages.success_created')
    redirect_to approver
  end

  def approvee_deactivate
    approver = approvee_complete_deactivate(params[:vac_user_to_approve_id])
    flash[:success] = t('views.vac_approvers.flash_messages.sucess_deleted')
    redirect_to approver
  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'vacs', true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def approver_complete_create(user_id)
    approver = VacApprover.where(user_id: user_id).first_or_create
    approver.active = true
    approver.deactivated_at = nil
    approver.deactivated_by_user_id = nil
    approver.registered_at = lms_time
    approver.registered_by_user_id = user_connected.id
    approver.save
    return approver
  end

  def approver_deactivate(approver_id)
    approver = VacApprover.find(approver_id)
    approver.active = false
    approver.deactivated_at = lms_time
    approver.deactivated_by_user_id = user_connected.id
    approver.save
    return approver
  end

  def approvee_complete_create(approver_id, user_id)
    approvee = VacUserToApprove.where(user_id: user_id, vac_approver_id: approver_id).first_or_create
    approvee.active = true
    approvee.deactivated_at = nil
    approvee.deactivated_by_user_id = nil
    approvee.registered_at = lms_time
    approvee.registered_by_user_id = user_connected.id
    approvee.save
    return approvee.vac_approver
  end

  def approvee_complete_deactivate(approvee_id)
    approvee = VacUserToApprove.find(approvee_id)
    approvee.active = false
    approvee.deactivated_at = lms_time
    approvee.deactivated_by_user_id = user_connected.id
    approvee.save
    return approvee.vac_approver
  end
end
