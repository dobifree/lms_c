class CharacteristicTypesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @characteristic_types = CharacteristicType.all
  end

  def show
    @characteristic_type = CharacteristicType.find(params[:id])
  end

  def new
    @characteristic_type = CharacteristicType.new
  end

  def edit
    @characteristic_type = CharacteristicType.find(params[:id])
  end

  def delete
    @characteristic_type = CharacteristicType.find(params[:id])
  end

  def create
    @characteristic_type = CharacteristicType.new(params[:characteristic_type])

    if @characteristic_type.save
      flash[:success] = t('activerecord.success.model.characteristic_type.create_ok')
      redirect_to characteristic_types_url
    else
      render action: 'new'
    end

  end

  def update
    @characteristic_type = CharacteristicType.find(params[:id])

    if @characteristic_type.update_attributes(params[:characteristic_type])
      flash[:success] = t('activerecord.success.model.characteristic_type.update_ok')
      redirect_to characteristic_types_url
    else
      render action: 'edit'
    end

  end

  def destroy

    @characteristic_type = CharacteristicType.find(params[:id])

    if verify_recaptcha

      if @characteristic_type.characteristics.size == 0

        if @characteristic_type.destroy
          flash[:success] = t('activerecord.success.model.characteristic_type.delete_ok')
          redirect_to characteristic_types_url
        else
          flash[:danger] = t('activerecord.success.model.characteristic_type.delete_error')
          redirect_to delete_characteristic_type_path(@characteristic_type)
        end
      else
        flash[:danger] = t('activerecord.success.model.characteristic_type.delete_error')
        redirect_to delete_characteristic_type_path(@characteristic_type)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.characteristic_type.captcha_error')
      redirect_to delete_characteristic_type_path(@characteristic_type)

    end

  end
end
