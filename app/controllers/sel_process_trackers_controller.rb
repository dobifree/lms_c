class SelProcessTrackersController < ApplicationController
  before_filter :authenticate_user

  def index
      @sel_processes = SelProcess.tracker(user_connected.id)
  end

  def show
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

   #@company = session[:company]

    count_steps_template = @sel_process.sel_template.sel_steps.count
    @new_applicants = @sel_process.sel_applicants.count - SelStepCandidate.joins(:sel_process_step).where(sel_process_steps: {:sel_process_id => @sel_process.id}).count('distinct jm_candidate_id')


    @new_selectables = SelApplicant.find_by_sql('select sel_applicants.*
                                                from sel_applicants INNER JOIN (
                                                select jm_candidate_id, sel_process_steps.sel_process_id
                                                from sel_step_candidates inner join sel_process_steps
                                                on (sel_step_candidates.sel_process_step_id = sel_process_steps.id)
                                                where (done = 1 OR exonerated = 1)
                                                and sel_process_steps.sel_process_id = ' + @sel_process.id.to_s + '
                                                group by jm_candidate_id, sel_process_steps.sel_process_id
                                                having count(*) = ' + count_steps_template.to_s + '
                                                ) as selectables
                                                on (sel_applicants.jm_candidate_id = selectables.jm_candidate_id and sel_applicants.sel_process_id = selectables.sel_process_id)
                                                where selected = 0 and excluded = 0').count

    @new_able_to_hire = SelApplicant.where(sel_process_id: @sel_process.id, selected: true, hired: false, excluded: false).count

    @show_step_open = false
    if params[:sel_step_id]
      @show_step_open = true
      @sel_step_id_open = params[:sel_step_id]
    end

  end

  def show_detail_applicants_list
   #@company = session[:company]
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    if @sel_process.sel_applicants.count > 0
      render 'detail_applicants_list', :layout => false
    else
      render 'show_detail_not_available', :layout => false
    end
  end

  def show_detail_process_step
    @sel_process_step = SelProcessStep.find(params[:sel_process_step_id])

    ################################################################
    validate_user_process(@sel_process_step.sel_process_id)
    ################################################################

    if @sel_process_step.sel_step.step_type == 1 && @sel_process_step.sel_step_candidates.count > 0
      prev_filter_step = SelProcessStep.joins(:sel_step).
          where(:sel_process_id => @sel_process_step.sel_process_id, sel_steps: {:step_type => 1}).
          where('sel_steps.position < ?', @sel_process_step.sel_step.position).order('sel_steps.position DESC').first

      initial_position = prev_filter_step ? prev_filter_step.sel_step.position : -1000000000

      @sel_process_steps_prev = SelProcessStep.joins(:sel_step).
          where(:sel_process_id => @sel_process_step.sel_process_id).
          where('sel_steps.position < ? AND sel_steps.position > ?', @sel_process_step.sel_step.position, initial_position)
    end


    if @sel_process_step.sel_step_candidates.joins(:jm_candidate => :sel_applicants).where(:sel_applicants => {sel_process_id: @sel_process_step.sel_process_id,excluded: false}).count > 0
      render 'show_detail_process_step', :layout => false
    else
      render 'show_detail_not_available', :layout => false
    end

  end

  def show_detail_selection_step
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    @sel_applicants_ordered_by_priority = @sel_process.sel_applicants_ordered_by_priority


    @sel_applicants = []
    count_steps_template = @sel_process.sel_template.sel_steps.count

    @sel_applicants_ordered_by_priority.each do |applicant|


      if is_candidate_selectable?(count_steps_template, applicant)
        @sel_applicants.push(applicant)
      end


      ####################
      prev_filter_step = SelProcessStep.joins(:sel_step).
          where(:sel_process_id => @sel_process.id, sel_steps: {:step_type => 1}).
          order('sel_steps.position DESC').first

      initial_position = prev_filter_step ? prev_filter_step.sel_step.position : -1000000000

      @sel_process_steps_prev = SelProcessStep.joins(:sel_step).
          where(:sel_process_id => @sel_process.id).
          where('sel_steps.position > ?', initial_position)
      ####################


    end

    if @sel_applicants.count > 0
      render 'detail_selection_step', :layout => false
    else
      render 'show_detail_not_available', :layout => false
    end
  end

  def show_detail_validation_step
    @sel_process = SelProcess.find(params[:id])

    ################################################################
    validate_user_process(@sel_process.id)
    ################################################################

    @sel_applicants_ordered_by_name = SelApplicant.joins(:jm_candidate).where(sel_process_id: @sel_process.id, selected: true, excluded: false).reorder('surname, name')

    if @sel_applicants_ordered_by_name.count > 0
      render 'detail_validation_step', :layout => false
    else
      render 'show_detail_not_available', :layout => false
    end
  end

  def show_attach_file_to_applicant

    @sel_applicant = SelApplicant.find(params[:sel_applicant_id])
    @sel_applicant_attachment = SelApplicantAttachment.new()
    @sel_applicant_attachment.sel_applicant = @sel_applicant

  end

  private

  def validate_user_process(sel_process_id)
    @sel_process = SelProcess.find(sel_process_id)
    if is_my_own_process?(sel_process_id) || !SelProcess.tracker(user_connected.id).include?(@sel_process)
      flash[:danger] = 'Usted no tiene privilegios para revisar este proceso.'
      redirect_to sel_process_trackers_path
    end
  end

  def is_my_own_process?(sel_process_id)
    me_as_candidate = JmCandidate.where(internal_user: true, user_id: user_connected.id).first
    me_as_candidate.sel_applicants.where(sel_process_id: sel_process_id).any? if me_as_candidate
  end

  def is_candidate_selectable?(count_steps_template, applicant)
    count_steps_template == SelStepCandidate.joins(:sel_process_step).where(:jm_candidate_id => applicant.jm_candidate_id, sel_process_steps: {:sel_process_id => applicant.sel_process_id}).where('sel_step_candidates.done = 1 OR sel_step_candidates.exonerated = 1').count
  end

end