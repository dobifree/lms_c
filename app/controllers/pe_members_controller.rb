class PeMembersController < ApplicationController

  include PeAssessmentHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  before_filter :get_data_1, only: [:show_evaluators, :show_evaluators, :open_whole_assessment, :finish_whole_assessment, :create_evaluator,
                                    :manage_feedback, :open_whole_assessment, :open_feedback, :finish_feedback, :create_feedback_provider, :detroy_feedback_provider]
  before_filter :get_data_2, only: [:open_assessment, :finish_assessment, :destroy_evaluator, :update_weight, :destroy_validator]
  before_filter :get_data_3, only: [:edit_evaluation_group, :update_evaluation_group, :edit_evaluation_weight, :update_evaluation_weight]
  before_filter :get_data_4, only: [:create_validator]

  def massive_charge

    pe_members_to_sync = Array.new

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if params[:reemplazar_todo][:yes] == '1'

        @pe_process.pe_members.each do |pe_member|

          if pe_member.started_participation?

            @lineas_error.push -1
            @lineas_error_detalle.push ('codigo: '+pe_member.user.codigo)
            @lineas_error_messages.push ['No se pudo eliminar el participante porque ya ha sido evaluado o ha evaluado a otro participante']

          else

            pe_member.destroy

          end

        end

        @pe_process.pe_areas.each do |pe_area|

          pe_area.destroy if pe_area.pe_members.size == 0

        end

      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      num_linea = 1

      text.each_line do |line|

        pe_member_evaluated = nil

        if line.strip != ''

          datos = line.split(';')

          datos[0].strip!

          if @pe_process.of_persons?

            user = User.where('codigo = ? AND activo = ?', datos[0], true).first

          else

            datos_u = datos[0].split(':')

            datos_u[0].strip!

            user = User.where('codigo = ? AND activo = ?', datos_u[0], true).first

          end

          if user

            if @pe_process.of_persons?

              pe_member_evaluated = @pe_process.pe_members.where('user_id = ?', user.id).first

              unless pe_member_evaluated

                pe_member_evaluated = @pe_process.pe_members.build
                pe_member_evaluated.user = user

              end

            else

              datos_u[1].strip!

              pe_area = @pe_process.pe_areas.where('name = ?', datos_u[1]).first

              unless pe_area

                pe_area = @pe_process.pe_areas.build
                pe_area.name = datos_u[1]

              end

              pe_area.is_evaluated = true

              if pe_area.save

                pe_member_evaluated = @pe_process.pe_members.where('user_id = ? AND pe_area_id = ?', user.id, pe_area.id).first

                unless pe_member_evaluated

                  pe_member_evaluated = @pe_process.pe_members.build
                  pe_member_evaluated.user = user
                  pe_member_evaluated.pe_area = pe_area

                end

              else

                @lineas_error.push num_linea
                @lineas_error_detalle.push ('area: '+datos_u[1]).force_encoding('UTF-8')
                @lineas_error_messages.push ['Error creando área']

              end

            end

            if pe_member_evaluated && pe_member_evaluated.save

              pe_members_to_sync.push pe_member_evaluated

              pos_col = 1

              @pe_process.pe_evaluations.each do |pe_evaluation|

                #if pe_evaluation.pe_groups.size > 0

                  datos_eval = datos[pos_col].split(':')

                  pe_member_evaluated_group = pe_member_evaluated.pe_member_groups.where('pe_evaluation_id = ?', pe_evaluation.id).first

                  unless pe_member_evaluated_group

                    pe_member_evaluated_group = pe_member_evaluated.pe_member_groups.build
                    pe_member_evaluated_group.pe_evaluation = pe_evaluation
                    pe_member_evaluated_group.pe_process = @pe_process

                  end

                  datos_eval[0].strip!

                  pe_group = pe_evaluation.pe_groups.where('name = ?', datos_eval[0]).first

                  if pe_group

                    pe_member_evaluated_group.pe_group = pe_group
                    pe_member_evaluated_group.save

                  end

                  pe_member_evaluated.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).destroy_all

                  pe_member_evaluated_evaluation = pe_member_evaluated.pe_member_evaluations.build
                  pe_member_evaluated_evaluation.pe_evaluation = pe_evaluation
                  pe_member_evaluated_evaluation.pe_process = @pe_process

                  if datos_eval[1]

                    datos_eval[1].strip!

                    pe_member_evaluated_evaluation.weight = datos_eval[1]
                    pe_member_evaluated_evaluation.save

                  end

                  pos_col += 1

                #end

              end

              #pe_member_evaluated.pe_member_evaluated_rels_is_evaluated.each { |pe_member_evaluated_rel| pe_member_evaluated_rel.destroy unless pe_member_evaluated_rel.finished }

              pe_member_evaluated_is_evaluated = false

              if datos[pos_col]

                datos[pos_col].delete!("\n")
                datos[pos_col].delete!("\r")

                datos[pos_col] = datos[pos_col].strip.slice 1..-2

                relaciones = datos[pos_col].split(',')

                relaciones.each do |relacion|

                  relacion = relacion.split(':')

                  relacion[0].strip!

                  if relacion[0] == 'puede_ser_seleccionado_como_evaluado'

                    pe_member_evaluated.reload
                    pe_member_evaluated.can_be_selected_as_evaluated = true
                    pe_member_evaluated.save

                  elsif relacion[0] == 'puede_seleccionar_evaluado'

                    pe_member_evaluated.reload
                    pe_member_evaluated.can_select_evaluated = true
                    pe_member_evaluated.save

                  else

                    rel_index = PeRel.rels.index relacion[0].to_sym

                    pe_member_evaluator = nil

                    #rel_index == 0 => auto , en el caso de auto no es necesario buscar persona, coge el evaluado
                    if rel_index == 0

                      pe_member_evaluated.reload
                      pe_member_evaluated.is_evaluated = true
                      pe_member_evaluated.is_evaluator = true
                      pe_member_evaluated.save

                      user_evaluator = user

                      pe_member_evaluator = pe_member_evaluated

                    else

                      relacion[1].strip!

                      user_evaluator = User.where('codigo = ? AND activo = ?', relacion[1], true).first

                      if user_evaluator

                        if @pe_process.of_persons?

                          pe_member_evaluator = @pe_process.pe_members.where('user_id = ?', user_evaluator.id).first

                          if pe_member_evaluator

                            pe_member_evaluator.is_evaluator = true

                          else

                            pe_member_evaluator = @pe_process.pe_members.build
                            pe_member_evaluator.is_evaluator = true

                            pe_member_evaluator.user = user_evaluator

                          end

                        else

                           relacion[3].strip!

                           pe_area_evaluator = @pe_process.pe_areas.where('name = ?', relacion[3]).first

                           unless pe_area_evaluator

                             pe_area_evaluator = @pe_process.pe_areas.build
                             pe_area_evaluator.name = relacion[3]

                           end

                           pe_area_evaluator.is_evaluator = true

                           if pe_area_evaluator.save

                             pe_member_evaluator = @pe_process.pe_members.where('user_id = ? AND pe_area_id = ?', user_evaluator.id, pe_area_evaluator.id).first

                             if pe_member_evaluator

                               pe_member_evaluator.is_evaluator = true

                             else

                               pe_member_evaluator = @pe_process.pe_members.build
                               pe_member_evaluator.is_evaluator = true

                               pe_member_evaluator.user = user_evaluator
                               pe_member_evaluator.pe_area = pe_area_evaluator

                             end

                           else

                             @lineas_error.push num_linea
                             @lineas_error_detalle.push ('area: '+datos_u[1]).force_encoding('UTF-8')
                             @lineas_error_messages.push ['Error creando área']

                           end

                        end

                      else

                        @lineas_error.push num_linea
                        @lineas_error_detalle.push ('rel: '+relacion[0]+', codigo: '+relacion[1]).force_encoding('UTF-8')
                        @lineas_error_messages.push ['El usuario no existe o no está activo']

                      end

                    end

                    if pe_member_evaluator && pe_member_evaluator.save

                      pe_members_to_sync.push pe_member_evaluator

                      if rel_index

                        pe_rel = @pe_process.pe_rels.where('rel = ?', rel_index).first

                        if pe_rel

                          pe_member_evaluated_rel_is_evaluated = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member(pe_member_evaluator)

                          unless pe_member_evaluated_rel_is_evaluated
                            pe_member_evaluated_rel_is_evaluated = pe_member_evaluated.pe_member_rels_is_evaluated.build
                            pe_member_evaluated_rel_is_evaluated.pe_member_evaluator = pe_member_evaluator
                            pe_member_evaluated_rel_is_evaluated.pe_process = @pe_process
                          end

                          pe_member_evaluated_rel_is_evaluated.pe_rel = pe_rel
                          if rel_index == 0
                            #auto
                            pe_member_evaluated_rel_is_evaluated.weight = relacion[1] ? relacion[1] : 1
                          else
                            #otras
                            pe_member_evaluated_rel_is_evaluated.weight = relacion[2] ? relacion[2] : 1
                          end


                          if pe_member_evaluated_rel_is_evaluated.save

                            #cargar responsables de feedback si es el caso

                            #rel_index == 1 => jefe
                            if rel_index == 1

                              pe_member_evaluated.reload
                              pe_member_evaluated.feedback_provider = user_evaluator
                              pe_member_evaluated.save

                            end

                            ###

                            pe_member_evaluated_is_evaluated = true

                          else

                            @lineas_error.push num_linea
                            @lineas_error_detalle.push ('rel: '+relacion[0]+', codigo: '+relacion[1]).force_encoding('UTF-8')
                            @lineas_error_messages.push pe_member_evaluated_rel_is_evaluated.errors.full_messages

                          end


                        else

                          @lineas_error.push num_linea
                          @lineas_error_detalle.push ('rel: '+relacion[0]+', codigo: '+relacion[1]).force_encoding('UTF-8')
                          @lineas_error_messages.push ['La relación no es válida o no está activa en el proceso']

                        end

                      end


                    else

                      @lineas_error.push num_linea
                      @lineas_error_detalle.push ('codigo: '+relacion[1]).force_encoding('UTF-8')
                      @lineas_error_messages.push ['El usuario '+relacion[1]+' no es participante del proceso']

                    end

                  end

                end

              end

              if pe_member_evaluated_is_evaluated
                pe_member_evaluated.reload
                pe_member_evaluated.is_evaluated = true
                pe_member_evaluated.removed = false
                pe_member_evaluated.save
              end


            else

              @lineas_error.push num_linea
              @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
              @lineas_error_messages.push ['Error creando participante']

            end

          else

            @lineas_error.push num_linea
            @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
            @lineas_error_messages.push ['El usuario no existe o no está activo']


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.pe_member.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.pe_member.carga_masiva_error') if @lineas_error.length > 0

      sync_cha(@pe_process, pe_members_to_sync.uniq)

    end


  end

  def destroy_evaluated

    pe_member = PeMember.find(params[:pe_member_id])
    pe_process = pe_member.pe_process

    pe_member.remove_as_assessed
    #pe_member.pe_member_rels_is_evaluated.destroy_all

    #pe_member.reload

    if pe_member.pe_member_rels_is_evaluator.size > 0
      pe_member.save
    else
      pe_member.destroy
    end

    flash[:success] = t('activerecord.success.model.pe_member.destroy_evaluated_ok')

    redirect_to pe_process_tab_path(pe_process, 'members')

  end

  def remove_evaluated

    pe_member = PeMember.find(params[:pe_member_id])
    pe_process = pe_member.pe_process

    pe_member.is_evaluated = false
    pe_member.removed = true
    pe_member.save

    flash[:success] = t('activerecord.success.model.pe_member.remove_evaluated_ok')

    redirect_to pe_process_tab_path(pe_process, 'members')

  end

  def reactivate_evaluated

    pe_member = PeMember.find(params[:pe_member_id])
    pe_process = pe_member.pe_process

    pe_member.is_evaluated = true
    pe_member.removed = false
    pe_member.save

    flash[:success] = t('activerecord.success.model.pe_member.reactivated_evaluated_ok')

    redirect_to pe_process_tab_path(pe_process, 'members')

  end

  def reset_evaluated

    pe_member = PeMember.find(params[:pe_member_id])
    pe_process = pe_member.pe_process

    pe_member.reset_assessed
    pe_member.save

    flash[:success] = t('activerecord.success.model.pe_member.reset_evaluated_ok')

    redirect_to pe_process_tab_path(pe_process, 'members')

  end

  def massive_destroy_evaluated

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]


            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]

            if pe_member_evaluated

              pe_member_evaluated.remove_as_assessed

              if pe_member_evaluated.pe_member_rels_is_evaluator.size > 0
                pe_member_evaluated.save
              else
                pe_member_evaluated.destroy
              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']

            end


          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def massive_finish_assessment

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]


            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]

            if pe_member_evaluated

              finish_the_whole_assessment(pe_member_evaluated, true)

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']

            end


          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      #flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def massive_finish_assessment_rel

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      filter_by_file = false

      pe_members_evaluated = Array.new

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        filter_by_file = true

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]

            if pe_member_evaluated

              pe_members_evaluated.push pe_member_evaluated.id

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']

            end


          end

        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    end

    if params[:pe_rel] && !params[:pe_rel][:id].blank?

      pe_rel = PeRel.find(params[:pe_rel][:id])

      if filter_by_file
        pe_member_rels = @pe_process.pe_member_rels.where('pe_rel_id = ? AND pe_member_evaluated_id IN (?)', pe_rel.id, pe_members_evaluated)
      else
        pe_member_rels = @pe_process.pe_member_rels.where('pe_rel_id = ?', pe_rel.id)
      end

      pe_member_rels.each do |pe_member_rel|

        unless pe_member_rel.finished?

          pe_member_rel.finished = true
          pe_member_rel.finished_at = lms_time
          pe_member_rel.save

          pe_member_evaluated = pe_member_rel.pe_member_evaluated

          finish_the_whole_assessment(pe_member_evaluated)

        end


      end

      flash[:success] = t('activerecord.success.model.pe_member.finish_rel_ok')

    end

  end

  def massive_destroy_evaluator

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]
            pe_member_evaluator = @pe_process.pe_member_by_user_code fila[1]

            if pe_member_evaluated && pe_member_evaluator

              pe_member_rel = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

              if pe_member_rel

                pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

                  pe_evaluation.pe_questions_by_pe_member_evaluated_evaluator_all_levels(pe_member_evaluated, pe_member_evaluator).destroy_all

                end

                pe_member_rel.destroy

                pe_member_evaluated.reload

                pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

                  calculate_assessment_final_evaluation pe_member_evaluated, pe_evaluation

                  calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

                end

                pe_member_evaluated.set_pe_box
                pe_member_evaluated.save

                unless pe_member_evaluated.pe_process.of_persons?

                  pe_area = pe_member_evaluated.pe_area
                  pe_area.set_pe_box
                  pe_area.save

                end

                calculate_assessment_groups pe_member_evaluated

                finish_the_whole_assessment pe_member_evaluated

                pe_member_evaluator.reload
                unless pe_member_evaluator.pe_member_rels_is_evaluator.size > 0
                  pe_member_evaluator.is_evaluator = 0
                  pe_member_evaluator.save
                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es evaluado por de '+fila[1].to_s]

              end


            else

              unless pe_member_evaluated

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']

              end

              unless pe_member_evaluator

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[1].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[1].to_s+' no es participante del proceso']

              end

            end

          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def massive_remove_evaluated

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]

            if pe_member_evaluated

              pe_member_evaluated.is_evaluated = false
              pe_member_evaluated.removed = true
              pe_member_evaluated.save

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']

            end


          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def massive_open_assessment
    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0] && fila[1]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]
            pe_member_evaluator = @pe_process.pe_member_by_user_code fila[1]

            if pe_member_evaluated && pe_member_evaluator

              pe_member_rel = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

              if pe_member_rel

                pe_member_rel.finished = false
                pe_member_rel.validated = false
                pe_member_rel.validated_at = nil
                pe_member_rel.save

                open_the_whole_assessment pe_member_rel.pe_member_evaluated

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es evaluado por de '+fila[1].to_s]

              end

            else

              unless pe_member_evaluated
                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']
              end

              unless pe_member_evaluator
                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[1].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[1].to_s+' no es participante del proceso']
              end


            end


          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end
  end

  def massive_replace_evaluators
    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0] && fila[1] && fila[2]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]
            pe_member_evaluator_old = @pe_process.pe_member_by_user_code fila[1]

            if pe_member_evaluated && pe_member_evaluator_old

              pe_member_rel = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator_old

              if pe_member_rel

                pe_member_evaluator_new = @pe_process.pe_member_by_user_code fila[2]

                unless pe_member_evaluator_new

                  user_new = User.find_by_codigo fila[2]

                  if user_new
                    pe_member_evaluator_new = @pe_process.pe_members.build
                    pe_member_evaluator_new.user = user_new
                    pe_member_evaluator_new.is_evaluator = true

                  else
                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('codigo: '+fila[2].to_s).force_encoding('UTF-8')
                    @lineas_error_messages.push ['El usuario '+fila[2].to_s+' no existe']
                  end

                end

                if pe_member_evaluator_new && pe_member_evaluator_new.save
                  pe_member_rel.pe_member_evaluator = pe_member_evaluator_new
                  if pe_member_rel.save
                    unless pe_member_evaluator_old.pe_member_rels_is_evaluator.size > 0
                      pe_member_evaluator_old.is_evaluator = false
                      pe_member_evaluator_old.save
                    end

                    if pe_member_rel.pe_rel.rel_id == 1

                      pe_member_evaluated.feedback_provider = pe_member_evaluator_new.user
                      pe_member_evaluated.save

                    end

                  end

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es evaluado por de '+fila[1].to_s]

              end

            else

              unless pe_member_evaluated
                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']
              end

              unless pe_member_evaluator_old
                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[1].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[1].to_s+' no es participante del proceso']
              end


            end


          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end
  end


  def massive_add_evaluator
    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0] && fila[1]

            user_evaluated = User.find_by_codigo fila[0]
            user_evaluator = User.find_by_codigo fila[1]
            rel_index = fila[2].blank? ? nil : PeRel.rels.index(fila[2].to_sym)

            if user_evaluated && user_evaluator && rel_index

              pe_rel = @pe_process.pe_rels.where('rel = ?', rel_index).first

              if pe_rel

                pe_member_evaluated = @pe_process.pe_members.where('user_id = ?', user_evaluated.id).first

                unless pe_member_evaluated

                  pe_member_evaluated = @pe_process.pe_members.build
                  pe_member_evaluated.user = user_evaluated

                end

                if pe_member_evaluated && pe_member_evaluated.save

                  if rel_index == 0
                    pe_member_evaluator = pe_member_evaluated
                  else
                    pe_member_evaluator = @pe_process.pe_members.where('user_id = ?', user_evaluator.id).first
                  end

                  unless pe_member_evaluator
                    pe_member_evaluator = @pe_process.pe_members.build
                    pe_member_evaluator.user = user_evaluator
                  end

                  if pe_member_evaluator && pe_member_evaluator.save

                    pe_member_rel = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

                    unless pe_member_rel
                      pe_member_rel = pe_member_evaluated.pe_member_rels_is_evaluated.build
                      pe_member_rel.pe_member_evaluator = pe_member_evaluator
                      pe_member_rel.pe_process = @pe_process
                    end

                    pe_member_rel.pe_rel = pe_rel

                    pe_member_rel.weight = fila[3].blank? ? 1 : fila[3]

                    pe_member_rel.finished = false
                    pe_member_rel.validated = false
                    pe_member_rel.validated_at = nil

                    if pe_member_rel.save
                      pe_member_evaluated.is_evaluated = true
                      pe_member_evaluator.is_evaluator = true
                      pe_member_evaluated.save
                      pe_member_evaluator.save

                      open_the_whole_assessment pe_member_rel.pe_member_evaluated

                    else
                      @lineas_error.push index+1
                      @lineas_error_detalle.push ('rel: '+fila[2]+', codigo: '+fila[1]).force_encoding('UTF-8')
                      @lineas_error_messages.push pe_member_rel.errors.full_messages
                    end

                  else
                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('codigo: '+fila[1].to_s).force_encoding('UTF-8')
                    @lineas_error_messages.push pe_member_evaluator.errors.full_messages
                  end

                else
                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                  @lineas_error_messages.push pe_member_evaluated.errors.full_messages

                end

              else
                @lineas_error.push index+1
                @lineas_error_detalle.push ('relación: '+fila[2].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['La relación '+fila[2].to_s+' no existe en el proceso']
              end

            else

              unless user_evaluated
                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no existe']
              end

              unless user_evaluator
                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[1].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[1].to_s+' no existe']
              end

              unless rel_index
                @lineas_error.push index+1
                @lineas_error_detalle.push ('relación: '+fila[2].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['La relación '+fila[2].to_s+' no existe']
              end

            end

          end

        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end
  end

  def massive_finish_def
    @pe_evaluation = PeEvaluation.find params[:pe_evaluation_id]
    @pe_process = @pe_evaluation.pe_process

    steps_array = Array.new

    steps_array.push 'Definir'

    @pe_evaluation.num_steps_definition_by_users_validated_before_accepted.times do |s|
      steps_array.push (@pe_evaluation.alias_step(s+1, true)+'_a_'+(s+1).to_s)
    end

    if @pe_evaluation.allow_definition_by_users_accepted
      steps_array.push 'Confirmar'
    end

    @pe_evaluation.num_steps_definition_by_users_validated_after_accepted.times do |s|
      steps_array.push (@pe_evaluation.alias_step(s+1, false)+'_d_'+(s+1).to_s)
    end

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0] && fila[1] && fila[2]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]

            if pe_member_evaluated


              if steps_array.include? fila[1]

                if fila[2] == 'abrir' || fila[2] == 'cerrar'

                  if fila[1] == 'Definir' && fila[2] == 'abrir'

                    @pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).each do |pe_question|

                      pe_question.pe_question_validations.destroy_all

                      pe_question.ready = false
                      pe_question.confirmed = false

                      pe_question.save

                    end

                  elsif fila[1] == 'Definir' && fila[2] == 'cerrar'

                    @pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).each do |pe_question|

                      pe_question.confirmed = true
                      pe_question.save

                    end

                  elsif fila[1] == 'Confirmar' && fila[2] == 'abrir'

                    @pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).each do |pe_question|

                      pe_question.pe_question_validations.where('before_accept = ?', false).destroy_all

                      pe_question.ready = false
                      pe_question.accepted = false

                      pe_question.save

                    end

                  elsif fila[1] == 'Confirmar' && fila[2] == 'cerrar'

                    @pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).each do |pe_question|

                      (1..@pe_evaluation.num_steps_definition_by_users_validated_before_accepted).each do |ns|

                        pe_question.pe_question_validations.where('step_number = ? AND before_accept = ?', ns, true).destroy_all

                        pqv = pe_question.pe_question_validations.build
                        pqv.step_number = ns
                        pqv.before_accept = true
                        pqv.validated = true
                        pqv.pe_evaluation = @pe_evaluation
                        pqv.save

                      end

                      pe_question.confirmed = true
                      pe_question.accepted = true
                      pe_question.save

                    end

                  else

                    x = fila[1].split('_')
                    num_step = x[2].to_i
                    before = x[1] == 'a' ? true : false

                    if x[0] == 'Validar'

                      if fila[2] == 'abrir'

                        @pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).each do |pe_question|

                          pe_question.pe_question_validations.where('step_number = ? AND before_accept = ?', num_step, before).destroy_all

                          pe_question.ready = false
                          pe_question.accepted = false if before

                          pe_question.save

                        end

                      elsif fila[2] == 'cerrar'

                        @pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).each do |pe_question|

                          pqv = pe_question.pe_question_validations.build
                          pqv.step_number = num_step
                          pqv.before_accept = before
                          pqv.validated = true
                          pqv.pe_evaluation = @pe_evaluation
                          pqv.save

                          unless before
                            pe_question.confirmed = true
                            pe_question.accepted = true
                            pe_question.save
                          end

                        end

                      end


                    end

                  end

                else
                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('estado: '+fila[2].to_s).force_encoding('UTF-8')
                  @lineas_error_messages.push ['El estado '+fila[2].to_s+' no existe']

                end

              else
                @lineas_error.push index+1
                @lineas_error_detalle.push ('paso: '+fila[1].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El paso '+fila[1].to_s+' no existe']
              end


            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es parte del proceso']

            end

          end

        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end
  end

  def show_evaluators

    @user_search = User.new

    if params[:user]

      @users_search = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

      @user_search.apellidos = params[:user][:apellidos]
      @user_search.nombre = params[:user][:nombre]

    end

  end

  def update_weight

    @pe_member_rel.weight = params[:new_weight]
    @pe_member_rel.save

    pe_member_evaluated = @pe_member_rel.pe_member_evaluated

    pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

      calculate_assessment_final_evaluation pe_member_evaluated, pe_evaluation

      calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

    end

    pe_member_evaluated.set_pe_box
    pe_member_evaluated.save

    unless pe_member_evaluated.pe_process.of_persons?

      pe_area = pe_member_evaluated.pe_area
      pe_area.set_pe_box
      pe_area.save

    end

    calculate_assessment_groups pe_member_evaluated

    finish_the_whole_assessment pe_member_evaluated

    flash[:success] = t('activerecord.success.model.pe_managing.update_weight_ok')

    redirect_to pe_members_show_evaluators_path pe_member_evaluated

  end


  def open_assessment

    @pe_member_rel.finished = false
    @pe_member_rel.validated = false
    @pe_member_rel.validated_at = nil
    @pe_member_rel.save

    open_the_whole_assessment @pe_member_rel.pe_member_evaluated

    flash[:success] = t('activerecord.success.model.pe_managing.open_assessment_ok')

    redirect_to pe_members_show_evaluators_path @pe_member_rel.pe_member_evaluated

  end

  def finish_assessment

    @pe_member_rel.finished = true
    @pe_member_rel.save

    finish_the_whole_assessment @pe_member_rel.pe_member_evaluated

    flash[:success] = t('activerecord.success.model.pe_managing.finish_assessment_ok')

    redirect_to pe_members_show_evaluators_path @pe_member_rel.pe_member_evaluated

  end

  def open_whole_assessment

    open_the_whole_assessment @pe_member

    flash[:success] = t('activerecord.success.model.pe_managing.open_assessment_ok')

    redirect_to pe_members_show_evaluators_path @pe_member

  end

  def finish_whole_assessment

    finish_the_whole_assessment @pe_member, true

    flash[:success] = t('activerecord.success.model.pe_managing.finish_assessment_ok')

    redirect_to pe_members_show_evaluators_path @pe_member

  end

  def create_evaluator

    user_evaluator = User.find(params[:user_id])
    pe_rel = @pe_process.pe_rels.where('id = ?', params[:pe_rel][:id]).first

    if user_evaluator && pe_rel

      if @pe_process.of_persons?

        pe_member_evaluator = @pe_process.pe_members.where('user_id = ?', user_evaluator.id).first

        if pe_member_evaluator

          pe_member_evaluator.is_evaluator = true

        else

          pe_member_evaluator = @pe_process.pe_members.build
          pe_member_evaluator.is_evaluator = true

          pe_member_evaluator.user = user_evaluator

        end

      else

        pe_area_evaluator = @pe_process.pe_areas.where('name = ?', params[:area]).first

        unless pe_area_evaluator

          pe_area_evaluator = @pe_process.pe_areas.build
          pe_area_evaluator.name = params[:area]

        end

        pe_area_evaluator.is_evaluator = true

        if pe_area_evaluator.save

          pe_member_evaluator = @pe_process.pe_members.where('user_id = ? AND pe_area_id = ?', user_evaluator.id, pe_area_evaluator.id).first

          if pe_member_evaluator

            pe_member_evaluator.is_evaluator = true

          else

            pe_member_evaluator = @pe_process.pe_members.build
            pe_member_evaluator.is_evaluator = true

            pe_member_evaluator.user = user_evaluator
            pe_member_evaluator.pe_area = pe_area_evaluator

          end

        end

      end

      if pe_member_evaluator && pe_member_evaluator.save

        pe_member_evaluated_rel_is_evaluated = @pe_member.pe_member_rel_is_evaluated_by_pe_member(pe_member_evaluator)

        unless pe_member_evaluated_rel_is_evaluated
          pe_member_evaluated_rel_is_evaluated = @pe_member.pe_member_rels_is_evaluated.build
          pe_member_evaluated_rel_is_evaluated.pe_member_evaluator = pe_member_evaluator
          pe_member_evaluated_rel_is_evaluated.pe_process = @pe_process
        end

        pe_member_evaluated_rel_is_evaluated.pe_rel = pe_rel
        pe_member_evaluated_rel_is_evaluated.weight = params[:weight]

        if pe_member_evaluated_rel_is_evaluated.save

          @pe_member.is_evaluated = true

          #rel_index == 1 => jefe
          @pe_member.feedback_provider = user_evaluator if pe_rel.rel == 1

          @pe_member.save

          open_the_whole_assessment @pe_member

          flash[:success] = t('activerecord.success.model.pe_managing.add_evaluator_ok')

        end

      end

    end

    redirect_to pe_members_show_evaluators_path @pe_member

  end

  def destroy_evaluator

    pe_member_evaluated = @pe_member_rel.pe_member_evaluated

    pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

      pe_evaluation.pe_questions_by_pe_member_evaluated_evaluator_all_levels(pe_member_evaluated, @pe_member_rel.pe_member_evaluator).destroy_all

    end

    pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_rel.destroy

    pe_member_evaluated.reload

    pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|

      calculate_assessment_final_evaluation pe_member_evaluated, pe_evaluation

      calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

    end

    pe_member_evaluated.set_pe_box
    pe_member_evaluated.save

    unless pe_member_evaluated.pe_process.of_persons?

      pe_area = pe_member_evaluated.pe_area
      pe_area.set_pe_box
      pe_area.save

    end

    calculate_assessment_groups pe_member_evaluated

    finish_the_whole_assessment pe_member_evaluated

    pe_member_evaluator.reload
    unless pe_member_evaluator.pe_member_rels_is_evaluator.size > 0
      pe_member_evaluator.is_evaluator = 0
      pe_member_evaluator.save
    end

    flash[:success] = t('activerecord.success.model.pe_managing.destroy_evaluator_ok')

    redirect_to pe_members_show_evaluators_path pe_member_evaluated

  end

  def create_validator

    @pe_member_rel.validator = @user_validator
    @pe_member_rel.validated = false
    @pe_member_rel.validated_at = nil
    @pe_member_rel.save

    pe_member_evaluated = @pe_member_rel.pe_member_evaluated

    pe_member_evaluated.require_validation = true

    pe_member_evaluated.step_validation = false
    pe_member_evaluated.step_validation_date = nil
    pe_member_evaluated.save

    flash[:success] = t('activerecord.success.model.pe_managing.add_validator_ok')
    redirect_to pe_members_show_evaluators_path @pe_member_rel.pe_member_evaluated

  end

  def destroy_validator
    @pe_member_rel.validator = nil
    @pe_member_rel.save

    pe_member_evaluated = @pe_member_rel.pe_member_evaluated

    if pe_member_evaluated.pe_member_rels_is_evaluated.where('validator_id is not null').size == 0
      pe_member_evaluated.require_validation = false
      pe_member_evaluated.save
    end

    flash[:success] = t('activerecord.success.model.pe_managing.delete_validator_ok')
    redirect_to pe_members_show_evaluators_path @pe_member_rel.pe_member_evaluated

  end

  def manage_feedback

    @user_search = User.new

    if params[:user]

      @users_search = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

      @user_search.apellidos = params[:user][:apellidos]
      @user_search.nombre = params[:user][:nombre]

    end

  end

  def open_feedback

    @pe_member.step_feedback = false
    @pe_member.step_feedback_accepted = false
    @pe_member.save

    flash[:success] = t('activerecord.success.model.pe_managing.open_feedback_ok')

    redirect_to pe_members_manage_feedback_path @pe_member

  end

  def finish_feedback

    @pe_member.step_feedback = true
    @pe_member.step_feedback_date = lms_time
    @pe_member.save

    menu_name = 'Evaluación de desempeño'

    ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

      if ct_menu_cod == 'pe'
        menu_name = ct_menus_user_menu_alias[index_m]
        break
      end
    end

    #LmsMailer.ask_pe_member_to_accept_feedback(@pe_member, @pe_process, menu_name, @company).deliver if @pe_process.has_step_feedback_accepted? && @pe_process.step_feedback_accepted_send_mail?

    if @pe_process.has_step_feedback_accepted? && @pe_process.step_feedback_accepted_send_mail?

      menu_name = 'Evaluación de desempeño'

      ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

        if ct_menu_cod == 'pe'
          menu_name = ct_menus_user_menu_alias[index_m]
          break
        end
      end

      begin
        PeMailer.step_feedback_accept_ask_to_accept(@pe_process, @company, menu_name, @pe_member.user, @pe_member.feedback_provider).deliver
      rescue Exception => e
        lm = LogMailer.new
        lm.module = 'pe'
        lm.step = 'def_by_user:feedback:ask_to_accept'
        lm.description = e.message+' '+e.backtrace.inspect
        lm.registered_at = lms_time
        lm.save
      end

    end

    flash[:success] = t('activerecord.success.model.pe_managing.close_feedback_ok')

    redirect_to pe_members_manage_feedback_path @pe_member

  end

  def create_feedback_provider

    feedback_provider = User.find(params[:user_id])

    @pe_member.feedback_provider = feedback_provider

    @pe_member.save

    flash[:success] = t('activerecord.success.model.pe_managing.create_feedback_provider_ok')

    redirect_to pe_members_manage_feedback_path @pe_member

  end

  def detroy_feedback_provider

    @pe_member.feedback_provider = nil
    @pe_member.save

    flash[:success] = t('activerecord.success.model.pe_managing.destroy_feedback_provider_ok')

    redirect_to pe_members_manage_feedback_path @pe_member

  end

  def sync_characteristics

    pe_process = PeProcess.find(params[:pe_process_id])
    sync_cha pe_process

    flash[:success] = t('activerecord.success.model.pe_member.sync_cha_ok')

    redirect_to pe_process_tab_path(pe_process, 'members')

  end

  def sync_characteristics_user

    pe_member = PeMember.find(params[:pe_member_id])

    if pe_member
      pe_process = pe_member.pe_process

      pe_members = Array.new
      pe_members.push pe_member

      sync_cha pe_process, pe_members

    end
    flash[:success] = t('activerecord.success.model.pe_member.sync_cha_ok')
    redirect_to pe_process_tab_path(pe_process, 'members')

  end

  def feedback_massive_charge

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if params[:reemplazar_todo][:yes] == '1'

        @pe_process.pe_members.each do |pe_member_evaluated|

          pe_member_evaluated.feedback_provider_id = nil
          pe_member_evaluated.save

        end

      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')

          user = User.where('codigo = ? AND activo = ?', datos[0], true).first

          if user

            pe_member_evaluated = @pe_process.pe_members.where('user_id = ?', user.id).first

            if pe_member_evaluated

              if datos[1].nil? || datos[1].blank?

                pe_member_evaluated.feedback_provider_id = nil
                pe_member_evaluated.save

              else

                datos[1].delete!("\n")
                datos[1].delete!("\r")

                user_feedback = User.where('codigo = ? AND activo = ?', datos[1], true).first

                if user_feedback

                  pe_member_evaluated.feedback_provider = user_feedback
                  pe_member_evaluated.save

                else

                  @lineas_error.push num_linea
                  @lineas_error_detalle.push ('codigo: '+datos[1]).force_encoding('UTF-8')
                  @lineas_error_messages.push ['El usuario no existe o no está activo']

                end

              end

            else

              @lineas_error.push num_linea
              @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
              @lineas_error_messages.push ['No existe el participante']

            end

          else

            @lineas_error.push num_linea
            @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
            @lineas_error_messages.push ['El usuario no existe o no está activo']


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.pe_member_evaluated.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.pe_member_evaluated.carga_masiva_error') if @lineas_error.length > 0

    end


  end

  def observers_massive_charge

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if params[:reemplazar_todo][:yes] == '1'

        @pe_process.pe_members.each do |pe_member_evaluated|

          pe_member_evaluated.pe_member_observers.destroy_all

        end

      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')

          user = User.where('codigo = ? AND activo = ?', datos[0], true).first

          if user

            pe_member_evaluated = @pe_process.pe_members.where('user_id = ?', user.id).first

            if pe_member_evaluated

              if datos[1].nil? || datos[1].blank?

                pe_member_evaluated.pe_member_observers.destroy_all

              else

                datos[1].delete!("\n")
                datos[1].delete!("\r")

                user_observer = User.where('codigo = ? AND activo = ?', datos[1], true).first

                if user_observer

                  pe_member_observer = pe_member_evaluated.pe_member_observers.build
                  pe_member_observer.observer = user_observer
                  pe_member_observer.pe_process = @pe_process
                  pe_member_observer.save


                else

                  @lineas_error.push num_linea
                  @lineas_error_detalle.push ('codigo: '+datos[1]).force_encoding('UTF-8')
                  @lineas_error_messages.push ['El usuario no existe o no está activo']

                end

              end

            else

              @lineas_error.push num_linea
              @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
              @lineas_error_messages.push ['No existe el participante']

            end

          else

            @lineas_error.push num_linea
            @lineas_error_detalle.push ('codigo: '+datos[0]).force_encoding('UTF-8')
            @lineas_error_messages.push ['El usuario no existe o no está activo']


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.pe_member_evaluated.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.pe_member_evaluated.carga_masiva_error') if @lineas_error.length > 0

    end


  end

  def validation_massive_charge

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if params[:reemplazar_todo][:yes] == '1'

        @pe_process.pe_member_rels.each do |pe_member_rel|

          pe_member_rel.validator_id = nil
          pe_member_rel.save

        end

        @pe_process.evaluated_members.each do |pe_member|
          pe_member.require_validation = false
          pe_member.save
        end

      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')

          user_evaluated = User.where('codigo = ? AND activo = ?', datos[0], true).first
          user_evaluator = User.where('codigo = ? AND activo = ?', datos[1], true).first

          if user_evaluated && user_evaluator

            pe_member_evaluated = @pe_process.evaluated_members.where('user_id = ?', user_evaluated.id).first

            pe_member_evaluator = @pe_process.evaluator_members.where('user_id = ?', user_evaluator.id).first

            if pe_member_evaluated && pe_member_evaluator

              pe_member_rel = @pe_process.pe_member_rels.where('pe_member_evaluated_id = ? AND pe_member_evaluator_id = ?', pe_member_evaluated.id, pe_member_evaluator.id).first

              if pe_member_rel

                if datos[2].nil? || datos[2].blank?

                  pe_member_rel.validator_id = nil
                  pe_member_rel.save

                  if pe_member_evaluated.pe_member_rels_is_evaluated.where('validator_id is not null').size == 0
                    pe_member_evaluated.require_validation = false
                    pe_member_evaluated.save
                  end

                else

                  datos[2].delete!("\n")
                  datos[2].delete!("\r")

                  user_validator = User.where('codigo = ? AND activo = ?', datos[2], true).first

                  if user_validator

                    pe_member_rel.validator = user_validator
                    pe_member_rel.save

                    pe_member_evaluated.require_validation = true
                    pe_member_evaluated.save

                  else

                    @lineas_error.push num_linea
                    @lineas_error_detalle.push ('codigo: '+datos[2]).force_encoding('UTF-8')
                    @lineas_error_messages.push ['El usuario no existe o no está activo']

                  end

                end

              else

                @lineas_error.push num_linea
                @lineas_error_detalle.push ('evaluado: '+datos[0]+', evaluador: '+datos[1]).force_encoding('UTF-8')
                @lineas_error_messages.push ['La relación/evaluación no existe']

              end

            else

              @lineas_error.push num_linea
              @lineas_error_detalle.push ('codigo: '+datos[0]+' codigo: '+datos[1]).force_encoding('UTF-8')
              @lineas_error_messages.push ['No existe el participante']

            end

          else

            @lineas_error.push num_linea
            @lineas_error_detalle.push ('codigo: '+datos[0]+', codigo: '+datos[1]).force_encoding('UTF-8')
            @lineas_error_messages.push ['El usuario no existe o no está activo']


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.pe_member_evaluated.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.pe_member_evaluated.carga_masiva_error') if @lineas_error.length > 0

    end


  end


  def validation_massive_finish

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0] && fila[1] && fila[2]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]
            pe_member_evaluator = @pe_process.pe_member_by_user_code fila[1]

            if pe_member_evaluated && pe_member_evaluator

              pe_member_rel = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

              if pe_member_rel

                user_validator = User.find_by_codigo fila[2]

                if user_validator && pe_member_rel.validator_id == user_validator.id

                  pe_member_rel.validated = true
                  pe_member_rel.validated_at = lms_time
                  pe_member_rel.save

                  step_validation = true

                  pe_member_evaluated.pe_member_rels_is_evaluated.where('validator_id IS NOT NULL').each do |pe_member_rel_is_evaluated|

                    step_validation = false unless pe_member_rel_is_evaluated.validated

                  end

                  if step_validation

                    pe_member_evaluated.step_validation = true
                    pe_member_evaluated.step_validation_date = lms_time
                    pe_member_evaluated.save

                  end

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                  @lineas_error_messages.push ['El usuario '+fila[2].to_s+' no es el validador ']

                end

              else
                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['El usuario '+fila[1].to_s+' no evalúa a '+fila[0].to_s+' ']

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario '+fila[0].to_s+' o '+fila[1].to_s+' no es participante del proceso']

            end


          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      #flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end


  end

  def validation_def_by_u_massive_charge

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process

    @pe_definition_by_user_validator = PeDefinitionByUserValidator.new
    @pe_definition_by_user_validator.pe_evaluation = @pe_evaluation
    @pe_definition_by_user_validator.step_number = params[:step_number]
    @pe_definition_by_user_validator.before_accept = params[:before_accept] == '1' ? true : false

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if params[:reemplazar_todo][:yes] == '1'
        @pe_evaluation.pe_definition_by_user_validators.where('step_number = ? AND before_accept = ?', @pe_definition_by_user_validator.step_number, @pe_definition_by_user_validator.before_accept).destroy_all
      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')

          datos[1] = datos[1].gsub("\n", '') if datos[1]

          user_evaluated = User.where('codigo = ? AND activo = ?', datos[0], true).first
          user_validator = User.where('codigo = ? AND activo = ?', datos[1], true).first if datos[1]


          if user_evaluated && user_validator

            pe_member_evaluated = @pe_process.evaluated_members.where('user_id = ?', user_evaluated.id).first

            if pe_member_evaluated

              pe_definition_by_user_validator = @pe_evaluation.pe_definition_by_user_validators.where('step_number = ? AND before_accept = ? AND pe_member_id = ?', @pe_definition_by_user_validator.step_number, @pe_definition_by_user_validator.before_accept, pe_member_evaluated).first

              unless pe_definition_by_user_validator

                pe_definition_by_user_validator = PeDefinitionByUserValidator.new
                pe_definition_by_user_validator.pe_evaluation = @pe_evaluation
                pe_definition_by_user_validator.pe_process = @pe_process
                pe_definition_by_user_validator.step_number = params[:step_number]
                pe_definition_by_user_validator.before_accept = params[:before_accept]
                pe_definition_by_user_validator.pe_member = pe_member_evaluated


              end

              pe_definition_by_user_validator.validator = user_validator
              pe_definition_by_user_validator.save

            else

              @lineas_error.push num_linea
              @lineas_error_detalle.push ('codigo: '+datos[0]+' codigo: '+datos[1]).force_encoding('UTF-8')
              @lineas_error_messages.push ['No existe el participante']

            end

          elsif user_evaluated

            pe_member_evaluated = @pe_process.evaluated_members.where('user_id = ?', user_evaluated.id).first

            if pe_member_evaluated

              pe_definition_by_user_validator = @pe_evaluation.pe_definition_by_user_validators.where('step_number = ? AND before_accept = ? AND pe_member_id = ?', @pe_definition_by_user_validator.step_number, @pe_definition_by_user_validator.before_accept, pe_member_evaluated).first
              pe_definition_by_user_validator.destroy if pe_definition_by_user_validator

            end

          else

            @lineas_error.push num_linea
            @lineas_error_detalle.push ('codigo: '+datos[0]+', codigo: '+datos[1]).force_encoding('UTF-8')
            @lineas_error_messages.push ['El usuario no existe o no está activo']


          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.pe_member.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.pe_member.carga_masiva_error') if @lineas_error.length > 0

    end



  end

  def edit_evaluation_group

    @pe_member_group = @pe_member.pe_member_group(@pe_evaluation)
    unless @pe_member_group
      @pe_member_group = @pe_member.pe_member_groups.build
    end

  end

  def update_evaluation_group

    @pe_member_group = @pe_member.pe_member_group(@pe_evaluation)

    if @pe_member_group

      if @pe_member_group.pe_group_id != params[:pe_member_group][:pe_group_id].to_i

        @pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel|

          if @pe_evaluation.has_pe_evaluation_rel pe_member_rel.pe_rel

            pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation @pe_evaluation

            pe_assessment_evaluation.destroy if pe_assessment_evaluation

            pe_member_rel.finished = false
            pe_member_rel.validated = false
            pe_member_rel.save

          end

        end

        @pe_member.step_assessment = false
        @pe_member.step_validation = false

        calculate_assessment_final_evaluation @pe_member, @pe_evaluation

        calculate_assessment_dimension @pe_member, @pe_evaluation.pe_dimension

        @pe_member.set_pe_box
        @pe_member.save

        unless @pe_member.pe_process.of_persons?

          pe_area = @pe_member.pe_area
          pe_area.set_pe_box
          pe_area.save

        end

        @pe_member_group.pe_group_id = params[:pe_member_group][:pe_group_id]
        @pe_member_group.save

        calculate_assessment_groups @pe_member

      end

    else
      @pe_member_group = @pe_member.pe_member_groups.build
      @pe_member_group.pe_evaluation = @pe_evaluation
      @pe_member_group.pe_process = @pe_process
      @pe_member_group.pe_group_id = params[:pe_member_group][:pe_group_id]
      @pe_member_group.save
    end


    flash[:success] = t('activerecord.success.model.pe_member.cambio_grupo_ok')
    redirect_to pe_members_edit_evaluation_group_path @pe_member, @pe_evaluation

  end

  def edit_evaluation_weight


  end

  def update_evaluation_weight

    pe_member_evaluation = @pe_member.pe_member_evaluation @pe_evaluation

    if params[:evaluated] == '1'

      if params[:default_weight] == '1'
        pe_member_evaluation.destroy if pe_member_evaluation
      else

        unless pe_member_evaluation
          pe_member_evaluation = @pe_member.pe_member_evaluations.build
          pe_member_evaluation.pe_evaluation = @pe_evaluation
        end

        pe_member_evaluation.weight = params[:custom_weight]

        pe_member_evaluation.save

      end

    else

      unless pe_member_evaluation
        pe_member_evaluation = @pe_member.pe_member_evaluations.build
        pe_member_evaluation.pe_evaluation = @pe_evaluation
      end

      pe_member_evaluation.weight = -1

      pe_member_evaluation.save

    end

    calculate_assessment_final_evaluation @pe_member, @pe_evaluation

    calculate_assessment_dimension @pe_member, @pe_evaluation.pe_dimension

    @pe_member.set_pe_box
    @pe_member.save

    unless @pe_member.pe_process.of_persons?

      pe_area = @pe_member.pe_area
      pe_area.set_pe_box
      pe_area.save

    end

    calculate_assessment_groups @pe_member

    flash[:success] = t('activerecord.success.model.pe_member.cambio_peso_ok')
    redirect_to pe_members_edit_evaluation_weight_path @pe_member, @pe_evaluation

  end

  def massive_change_evaluation_weight
    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]

            if pe_member_evaluated

              pe_evaluation = @pe_process.pe_evaluations.where(:name => fila[1]).first

              if pe_evaluation

                pe_member_evaluation = pe_member_evaluated.pe_member_evaluation pe_evaluation

                if fila[2] == -1

                  unless pe_member_evaluation
                    pe_member_evaluation = pe_member_evaluated.pe_member_evaluations.build
                    pe_member_evaluation.pe_evaluation = pe_evaluation
                  end

                  pe_member_evaluation.weight = -1

                  pe_member_evaluation.save

                elsif fila[2].blank?

                  pe_member_evaluation.destroy if pe_member_evaluation

                else

                  unless pe_member_evaluation
                    pe_member_evaluation = pe_member_evaluated.pe_member_evaluations.build
                    pe_member_evaluation.pe_evaluation = pe_evaluation
                  end

                  pe_member_evaluation.weight = fila[2]

                  pe_member_evaluation.save

                end

                calculate_assessment_final_evaluation pe_member_evaluated, pe_evaluation

                calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

                pe_member_evaluated.set_pe_box
                pe_member_evaluated.save

                unless pe_member_evaluated.pe_process.of_persons?

                  pe_area = pe_member_evaluated.pe_area
                  pe_area.set_pe_box
                  pe_area.save

                end

                calculate_assessment_groups pe_member_evaluated

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('evaluación: '+fila[1].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['La evaluación '+fila[1].to_s+' no existe']

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']

            end


          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      #flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end
  end

  def massive_change_evaluation_group

    @pe_process = PeProcess.find(params[:pe_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            pe_member_evaluated = @pe_process.pe_member_by_user_code fila[0]

            if pe_member_evaluated

              pe_evaluation = @pe_process.pe_evaluations.where(:name => fila[1]).first

              if pe_evaluation

                pe_group = pe_evaluation.pe_groups.where(:name => fila[2]).first

                if pe_group

                  pe_member_group = pe_member_evaluated.pe_member_group(pe_evaluation)

                  if pe_member_group.nil?

                    pe_member_group = pe_member_evaluated.pe_member_groups.build
                    pe_member_group.pe_evaluation = pe_evaluation
                    pe_member_group.pe_process = @pe_process
                    pe_member_group.pe_group = pe_group
                    pe_member_group.save

                  elsif pe_member_group.pe_group.id != pe_group.id

                    pe_member_evaluated.pe_member_rels_is_evaluated.each do |pe_member_rel|

                      if pe_evaluation.has_pe_evaluation_rel pe_member_rel.pe_rel

                        pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_evaluation

                        pe_assessment_evaluation.destroy if pe_assessment_evaluation

                        pe_member_rel.finished = false
                        pe_member_rel.validated = false
                        pe_member_rel.save

                      end

                    end

                    pe_member_evaluated.step_assessment = false
                    pe_member_evaluated.step_validation = false

                    calculate_assessment_final_evaluation pe_member_evaluated, pe_evaluation

                    calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

                    pe_member_evaluated.set_pe_box
                    pe_member_evaluated.save

                    unless @pe_process.of_persons?

                      pe_area = pe_member_evaluated.pe_area
                      pe_area.set_pe_box
                      pe_area.save

                    end

                    pe_member_group.pe_group = pe_group
                    pe_member_group.save

                    calculate_assessment_groups pe_member_evaluated

                  end

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('grupo: '+fila[2].to_s).force_encoding('UTF-8')
                  @lineas_error_messages.push ['El grupo '+fila[2].to_s+' no existe']

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('evaluación: '+fila[1].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push ['La evaluación '+fila[1].to_s+' no existe']

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0].to_s).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario '+fila[0].to_s+' no es participante del proceso']

            end


          end

        end

        if @lineas_error.size > 0



        end

        flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      end

    else

      #flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  # GET /pe_member_evaluateds
  # GET /pe_member_evaluateds.json
  def index
    @pe_member_evaluateds = PeMember.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_member_evaluateds }
    end
  end

  # GET /pe_member_evaluateds/1
  # GET /pe_member_evaluateds/1.json
  def show
    @pe_member_evaluated = PeMember.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_member_evaluated }
    end
  end

  # GET /pe_member_evaluateds/new
  # GET /pe_member_evaluateds/new.json
  def new
    @pe_member_evaluated = PeMember.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pe_member_evaluated }
    end
  end

  # GET /pe_member_evaluateds/1/edit
  def edit
    @pe_member_evaluated = PeMember.find(params[:id])
  end

  # POST /pe_member_evaluateds
  # POST /pe_member_evaluateds.json
  def create
    @pe_member_evaluated = PeMember.new(params[:pe_member_evaluated])

    respond_to do |format|
      if @pe_member_evaluated.save
        format.html { redirect_to @pe_member_evaluated, notice: 'Pe member was successfully created.' }
        format.json { render json: @pe_member_evaluated, status: :created, location: @pe_member_evaluated }
      else
        format.html { render action: "new" }
        format.json { render json: @pe_member_evaluated.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pe_member_evaluateds/1
  # PUT /pe_member_evaluateds/1.json
  def update
    @pe_member_evaluated = PeMember.find(params[:id])

    respond_to do |format|
      if @pe_member_evaluated.update_attributes(params[:pe_member_evaluated])
        format.html { redirect_to @pe_member_evaluated, notice: 'Pe member was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pe_member_evaluated.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pe_member_evaluateds/1
  # DELETE /pe_member_evaluateds/1.json
  def destroy
    @pe_member_evaluated = PeMember.find(params[:id])
    @pe_member_evaluated.destroy

    respond_to do |format|
      format.html { redirect_to pe_member_evaluateds_url }
      format.json { head :no_content }
    end
  end

  private


  def get_data_1
    @pe_member = PeMember.find(params[:pe_member_id])
    @pe_process = @pe_member.pe_process
   #@company = session[:company]
  end

  def get_data_2
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_process = @pe_member_rel.pe_process
  end

  def get_data_3
    @pe_member = PeMember.find(params[:pe_member_id])
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_member.pe_process
   #@company = session[:company]
  end

  def get_data_4
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel][:id])
    @pe_process = @pe_member_rel.pe_process
    @user_validator = User.find(params[:user_id])
  end

  def sync_cha(pe_process, pe_members = nil)

    Characteristic.all.each do |cha|

      pe_cha = pe_process.pe_characteristic(cha)

      unless pe_cha

        pe_cha = pe_process.pe_characteristics.build
        pe_cha.characteristic = cha
        pe_cha.save

      end

    end

    pe_members = pe_process.pe_members unless pe_members

    pe_members.each do |pe_member|

      pe_member.pe_member_characteristics.destroy_all

      pe_member.user.user_characteristics.each do |uc|

        pe_m_c = pe_member.pe_member_characteristics.build
        pe_m_c.pe_process = pe_process
        pe_m_c.characteristic = uc.characteristic
        pe_m_c.value = uc.formatted_value
        pe_m_c.save

      end

    end

  end

end
