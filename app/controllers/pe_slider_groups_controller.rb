class PeSliderGroupsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process

    @pe_slider_group = @pe_evaluation.pe_slider_groups.build

  end


  def create

    @pe_slider_group = PeSliderGroup.new(params[:pe_slider_group])
    @pe_evaluation = @pe_slider_group.pe_evaluation

    if @pe_slider_group.save
      redirect_to @pe_evaluation
    else
      @pe_process = @pe_evaluation.pe_process
      render 'new'
    end

  end


  def edit
    @pe_slider_group = PeSliderGroup.find(params[:id])
    @pe_evaluation = @pe_slider_group.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end


  def update

    @pe_slider_group = PeSliderGroup.find(params[:id])
    @pe_evaluation = @pe_slider_group.pe_evaluation

    if @pe_slider_group.update_attributes(params[:pe_slider_group])
      redirect_to @pe_evaluation
    else
      @pe_process = @pe_evaluation.pe_process
      render 'edit'
    end

  end

  # DELETE /pe_slider_groups/1
  # DELETE /pe_slider_groups/1.json
  def destroy
    @pe_slider_group = PeSliderGroup.find(params[:id])

    pe_evaluation = @pe_slider_group.pe_evaluation

    @pe_slider_group.destroy

    redirect_to pe_evaluation


  end
end
