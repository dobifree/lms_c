class LmsManagingPrivilegesController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:edit, :update]

  before_filter :verify_access_manage_module


  def search

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

    @users_q = User.users_with_lms_privileges

    @ct_module = CtModule.find_by_cod('lms')

  end

  def edit
    @user_ct_module_privileges = @user.user_ct_module_privileges
    @user_ct_module_privileges = UserCtModulePrivileges.new unless @user_ct_module_privileges
  end

  def update
    user_ct_module_privileges = @user.user_ct_module_privileges
    unless user_ct_module_privileges
      user_ct_module_privileges = UserCtModulePrivileges.new
      user_ct_module_privileges.user = @user
    end

    user_ct_module_privileges.update_attributes(params[:user_ct_module_privileges])

    user_ct_module_privileges.save

    flash[:success] = t('activerecord.success.model.user_managinig.update_lms_privilegios_ok')
    redirect_to lms_managing_privileges_search_path

  end


  private

  def get_data_1
   #@company = session[:company]
    @user = User.find(params[:user_id])
  end


  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'lms', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end


end
