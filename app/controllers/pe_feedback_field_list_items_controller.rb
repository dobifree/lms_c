class PeFeedbackFieldListItemsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new

    @pe_feedback_field_list = PeFeedbackFieldList.find(params[:pe_feedback_field_list_id])

    @pe_process = @pe_feedback_field_list.pe_process

    @pe_feedback_field_list_item = @pe_feedback_field_list.pe_feedback_field_list_items.build

  end

  def edit

    @pe_feedback_field_list_item = PeFeedbackFieldListItem.find(params[:id])
    @pe_feedback_field_list = @pe_feedback_field_list_item.pe_feedback_field_list
    @pe_process = @pe_feedback_field_list.pe_process

  end

  def create

    @pe_feedback_field_list_item = PeFeedbackFieldListItem.new(params[:pe_feedback_field_list_item])

    if @pe_feedback_field_list_item.save

      redirect_to @pe_feedback_field_list_item.pe_feedback_field_list

    else

      @pe_feedback_field_list = @pe_feedback_field_list_item.pe_feedback_field_list
      @pe_process = @pe_feedback_field_list.pe_process
      render action: 'new'

    end

  end

  def update

    @pe_feedback_field_list_item = PeFeedbackFieldListItem.find(params[:id])

    if @pe_feedback_field_list_item.update_attributes(params[:pe_feedback_field_list_item])

      redirect_to @pe_feedback_field_list_item.pe_feedback_field_list

    else

      @pe_feedback_field_list = @pe_feedback_field_list_item.pe_feedback_field_list
      @pe_process = @pe_feedback_field_list.pe_process
      render action: 'edit'

    end


  end

  def destroy

    @pe_feedback_field_list_item = PeFeedbackFieldListItem.find(params[:id])

    pe_feedback_field_list = @pe_feedback_field_list_item.pe_feedback_field_list

    @pe_feedback_field_list_item.destroy if @pe_feedback_field_list_item.pe_member_feedbacks.size == 0 && @pe_feedback_field_list_item.pe_member_compound_feedbacks.size == 0

    redirect_to pe_feedback_field_list


  end

end
