class PeMyResultsController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1
  before_filter :validate_open_process
  before_filter :validate_my_results


  def my_results

   #@company = session[:company]


  end

  private

  def get_data_1

    @pe_process = PeProcess.find(params[:pe_process_id])
    if @pe_process.of_persons?
      @pe_member = @pe_process.pe_member_by_user user_connected
    else
      @pe_members = @pe_process.pe_members_by_user user_connected
    end

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_my_results

    unless @pe_process.has_step_my_results? && @pe_process.is_a_pe_member_evaluated?(user_connected)
      flash[:danger] = t('activerecord.error.model.pe_my_results.cant_watch')
      redirect_to root_path
    end

  end

end
