class CtModulesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @ct_modules = CtModule.all
  end

  def change_status

    ct_module = CtModule.find(params[:ct_module_id])

    ct_module.active = !ct_module.active

    ct_module.save

    flash[:success] = t('activerecord.success.model.ct_modules.change_status_ok')

    redirect_to ct_modules_path

  end

  def edit

    @ct_module = CtModule.find(params[:id])


  end

  def update

    @ct_module = CtModule.find(params[:id])


    if @ct_module.update_attributes(params[:ct_module])

      flash[:success] = t('activerecord.success.model.ct_modules.update_ok')
      redirect_to ct_modules_path

    else

      render action: 'edit'

    end


  end

end
