class BrcMailsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module


  # GET /brc_mails
  # GET /brc_mails.json
  def index
    @brc_mail = BrcMail.first

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @brc_mails }
    end
  end

  # GET /brc_mails/1
  # GET /brc_mails/1.json
  def show
    @brc_mail = BrcMail.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @brc_mail }
    end
  end

  # GET /brc_mails/new
  # GET /brc_mails/new.json
  def new
    @brc_mail = BrcMail.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @brc_mail }
    end
  end

  # GET /brc_mails/1/edit
  def edit
    @brc_mail = BrcMail.find(params[:id])
  end

  # POST /brc_mails
  # POST /brc_mails.json
  def create
    @brc_mail = BrcMail.new(params[:brc_mail])

    respond_to do |format|
      if @brc_mail.save
        format.html { redirect_to brc_mails_path, notice: 'Brc mail was successfully created.' }
        format.json { render json: @brc_mail, status: :created, location: @brc_mail }
      else
        format.html { render action: "new" }
        format.json { render json: @brc_mail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /brc_mails/1
  # PUT /brc_mails/1.json
  def update
    @brc_mail = BrcMail.find(params[:id])

    respond_to do |format|
      if @brc_mail.update_attributes(params[:brc_mail])
        format.html { redirect_to brc_mails_path, notice: 'Brc mail was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @brc_mail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /brc_mails/1
  # DELETE /brc_mails/1.json
  def destroy
    @brc_mail = BrcMail.find(params[:id])
    @brc_mail.destroy

    respond_to do |format|
      format.html { redirect_to brc_mails_url }
      format.json { head :no_content }
    end
  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'brc', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

end
