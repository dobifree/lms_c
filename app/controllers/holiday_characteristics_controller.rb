class HolidayCharacteristicsController < ApplicationController
  include HolidayModule

  before_filter :authenticate_user
  before_filter :holiday_manager
  before_filter :active_module
  before_filter :get_holiday_by_id, only: %i[ new_hol_condition ]
  before_filter :verify_active_holiday, only: %i[ new_hol_condition ]

  def new
    @holiday = Holiday.new(hol_type: Holiday.characteristic_type)
  end

  def create
    holiday = Holiday.new(params[:holiday])
    holiday.hol_type = Holiday.characteristic_type
    holiday = set_registered holiday
    if holiday.save
      flash[:success] = t('views.holiday_module.flash_messages.create_success')
      redirect_to holiday_manage_path(holiday)
    else
      @holiday = holiday
      render action: "new"
    end
  end

  def row_show
    render partial: 'row_show',
           locals: { hol_char: HolidayCharacteristic.find(params[:hol_char_id]) }
  end

  def match_form
    render partial: 'row_form',
           locals: { hol_char: HolidayCharacteristic.new,
                     characteristic: Characteristic.find(params[:characteristic_id]),
                     url_to_go: params[:url_to_go] }
  end

  def new_hol_condition
    render partial: 'row_form',
           locals: { hol_char: HolidayCharacteristic.new,
                     url_to_go: holiday_characteristics_create_hol_condition_path(params[:holiday_id]) }
  end

  def create_hol_condition
    hol_char = HolidayCharacteristic.new(params[:holiday_characteristic])
    hol_char.holiday_id = params[:holiday_id]
    hol_char = set_registered(hol_char)
    if hol_char.save
      render partial: 'row_show',
             locals: { hol_char: hol_char }
    else
      render partial: 'row_form',
             locals: { hol_char: hol_char,
                       url_to_go: holiday_characteristics_create_characteristics_date_path }
    end
  end

  def edit_hol_condition
    hol_char = HolidayCharacteristic.find(params[:holiday_characteristic_id])
    render partial: 'row_form',
           locals: { hol_char: hol_char,
                     characteristic: hol_char.characteristic,
                     url_to_go: holiday_characteristics_update_hol_condition_path(hol_char) }
  end

  def update_hol_condition
    prev_hol_char = HolidayCharacteristic.find(params[:holiday_characteristic_id])
    hol_char = prev_hol_char.dup
    hol_char.assign_attributes(params[:holiday_characteristic])
    hol_char.prev_hol_char_id = prev_hol_char.id
    hol_char = set_registered(hol_char)
    if hol_char.save
      deactivate(prev_hol_char).save
      render partial: 'row_show',
             locals: { hol_char: hol_char }
    else
      hol_char = prev_hol_char
      hol_char.assign_attributes(params[:holiday_characteristic])
      hol_char.valid?
      render partial: 'row_form',
             locals: { hol_char: hol_char,
                       characteristic: hol_char.characteristic,
                       url_to_go: holiday_characteristics_update_hol_condition_path(hol_char.characteristic_id) }
    end
  end

  def delete_hol_condition
    hol_char = HolidayCharacteristic.find(params[:holiday_characteristic_id])
    deactivate(hol_char).save
    render partial: 'shared/div_message',
           locals: {type_generic_message: 'alert-success',
                    generic_message: t('views.holiday_module.flash_messages.delete_success')}

  end

end
