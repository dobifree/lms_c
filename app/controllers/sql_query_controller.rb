class SqlQueryController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin
  before_filter :authenticate_user_god


  def query_box

  end

  def query_results

    @results = []

    params[:textareaCodeSQL].split(';').each do | sql |

      sql.strip!
      result = {}
      result[:query] = sql
      begin
        if sql.upcase.start_with?('SELECT')
          result[:table] = true
          result[:rows] = ActiveRecord::Base.connection.exec_query(sql)
        else
          result[:table] = false
          result[:rows_affected] = ActiveRecord::Base.connection.delete(sql)
        end
      rescue Exception => e
        result[:error_message] = e.message
        result[:error_backtrace] = e.backtrace
      end

      @results.append(result)

    end

    render :layout => false
  end

end
