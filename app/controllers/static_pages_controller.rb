class StaticPagesController < ApplicationController

  SECRET_TOTP = "uhzpj7xk7lbifz2g"
  include PeReportingHelper
  include UserCoursesModule
  skip_before_filter :verify_authenticity_token, :only => :ask_for_validation_admin_logged_from_other_company
  before_filter :get_data
  before_filter :get_data2, only: [:pe_rep_detailed_final_results, :pe_rep_detailed_final_results_pdf]
  before_filter :validate_login, only: [:home, :sww, :user_profile, :download_informative_document,
                                        :pe_rep_detailed_final_results, :pe_rep_detailed_final_results_pdf]
  before_filter :validate_pe_rep_detailed_final_results, only: [:pe_rep_detailed_final_results]
  before_filter :validate_pe_rep_detailed_final_results_pdf, only: [:pe_rep_detailed_final_results_pdf]
  #before_filter :validate_characteristic_file, only: [:download_user_profile_characteristic_file]

  def home
    if normal_user_logged_in? && user_connected.requiere_cambio_pass
      redirect_to user_change_pass_path
    else
      if admin_logged_in?
        render 'dashboard_admin'
      else
        @dashboard = Dashboard.new(user_connected, lms_time)
        set_show_azure_cookie_value(user_connected.id)
        render 'dashboard_user'
      end
    end
  end

  def sww

  end

  def logout_ad

  end

  def error_external_authentication
  end

  def landing_azure_2
    redirect_to azure_ad_path
  end

  def landing_azure
    @close_parent = params[:close_parent].blank? ? false : (params[:close_parent].to_i == 1)
  end


  def get_lms_server_time
    #render text: lms_time
    #render text: Time.now.utc
    render text: Time.now.utc.in_time_zone(user_connected_time_zone)
  end

  def user_profile

    #@company = session[:company]

    if $mobile_client
      render 'static_pages/user_profile_mobile'
    else
      render 'static_pages/user_profile_new'
    end
  end

  def user_profile_new

    #@company = session[:company]

    if $mobile_client
      render 'static_pages/user_profile_mobile'
    else
      render 'static_pages/user_profile_new'
    end
  end


  def pe_rep_detailed_final_results


  end

  def pe_rep_detailed_final_results_pdf


    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.pdf'
    archivo_temporal = directorio_temporal + nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_for_user_profile, @pe_process.rep_detailed_final_results_show_calibration_for_user_profile, @pe_process.public_pe_members_box, @pe_process.public_pe_members_dimension_name, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - ' + @pe_member.user.codigo + '.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_informative_document

    document = InformativeDocument.find(params[:id])

    company = @company

    file = company.directorio + '/' + company.codigo + '/informative_documents/' + document.id.to_s + '/' + user_connected.codigo + '.pdf'


    send_file file,
              filename: document.name + '.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def recuperar_pass_paso1

    #@company = session[:company]

  end

  def recuperar_pass_paso2

    #@company = session[:company]

    if verify_recaptcha

      user = User.where('(codigo = ? or email = ?) AND activo = ? ', params[:user][:codigo], params[:user][:codigo], true).first

      if user && user.email
        clean_rec_pass_attempt
        user.codigo_rec_pass = user.codigo.to_s + '_' + SecureRandom.hex(40)
        user.fecha_rec_pass = lms_time
        user.save


        begin

          LmsMailer.recuperar_pass(user, @company).deliver
          flash[:success] = t('rec_pass.paso2_ok')
          redirect_to root_path
        rescue
          flash.now[:rec_pass] = t('rec_pass.mail_error')
          render 'recuperar_pass_paso1'
        end


      else
        count_rec_pass_attempt
        flash.now[:rec_pass] = t('rec_pass.invalid_user')
        render 'recuperar_pass_paso1'

      end

    else

      flash.delete(:recaptcha_error)

      flash.now[:rec_pass] = t('rec_pass.captcha_error')
      render 'recuperar_pass_paso1'

    end

  end

  def recuperar_pass_paso3
   #@company = session[:company]
    @user = User.where('codigo_rec_pass = ? AND fecha_rec_pass > ?', params[:codigo_rec_pass], lms_time - 2.days).first

    unless @user
      flash.now[:danger] = t('rec_pass.invalid_codigo')

      @user = User.new
      render 'recuperar_pass_error'
    end

  end

  def recuperar_pass_paso4
   #@company = session[:company]
    @user = User.where('codigo_rec_pass = ? AND fecha_rec_pass > ?', params[:user][:codigo_rec_pass], lms_time - 2.days).first

    if @user
      if !params[:user][:password].blank?
        evaluation = @user.validate_password_policies(params[:user][:password])
        if evaluation[:valid]
          if @user.update_attributes(password: params[:user][:password], password_confirmation: params[:user][:password_confirmation])
            @user.update_attributes(codigo_rec_pass: nil, fecha_rec_pass: nil)
            flash[:success] = t('activerecord.success.model.user.update_pass_ok')
            redirect_to root_path
          else
            params[:codigo_rec_pass] = params[:user][:codigo_rec_pass]
            render 'recuperar_pass_paso3'
          end
        else
          evaluation[:errors].each do |rule|
            @user.errors.add :password, rule.error_match_message
          end
          params[:codigo_rec_pass] = params[:user][:codigo_rec_pass]
          render 'recuperar_pass_paso3'
        end
      else
        @user.errors.add :base, t('activerecord.errors.models.user.attributes.password.blank_update')
        params[:codigo_rec_pass] = params[:user][:codigo_rec_pass]
        render 'recuperar_pass_paso3'
      end

    else

      flash.now[:danger] = t('rec_pass.invalid_codigo')
      @user = User.new
      render 'recuperar_pass_error'

    end


  end

  def jm_recuperar_pass_paso1
    @email_to_recover = params[:email]
    render :layout => 'job_market'
  end

  def jm_recuperar_pass_paso2
    @email_to_recover = params[:candidate][:email]

   #@company = session[:company]

    if verify_recaptcha

      candidate = JmCandidate.where(:email => params[:candidate][:email]).first

      if candidate && candidate.email
        candidate.codigo_rec_pass = SecureRandom.hex(3).to_s.upcase
        candidate.fecha_rec_pass = lms_time
        candidate.save
        flash[:success] = t('rec_pass.paso2_ok')

        notification_template = SelNotificationTemplate.where(event_id: 1, active: true).first
        if notification_template
          notification = notification_template.generate_notification({jm_candidate: candidate}, nil, lms_time)
          SelMailer.sel_notification(notification, @company).deliver
        else
          LmsMailer.jm_recuperar_pass(candidate, @company).deliver
        end
        redirect_to jm_recuperar_pass_paso3_path(candidate)
      else
        flash.now[:rec_pass] = t('rec_pass.invalid_user')
        render 'jm_recuperar_pass_paso1', :layout => 'job_market'

      end

    else

      flash.delete(:recaptcha_error)

      flash.now[:rec_pass] = t('rec_pass.captcha_error')
      render 'jm_recuperar_pass_paso1', :layout => 'job_market'

    end

  end

  def jm_recuperar_pass_paso3
   #@company = session[:company]
    @candidate = JmCandidate.where(id: params[:jm_candidate_id]).where('fecha_rec_pass > ?', lms_time - 30.minutes).first

    if @candidate
      render :layout => 'job_market'
    else
      flash.now[:danger] = t('rec_pass.invalid_codigo')
      @candidate = JmCandidate.new
      render 'recuperar_pass_error', :layout => 'job_market'
    end
  end

  def jm_recuperar_pass_paso4
   #@company = session[:company]
    @candidate = JmCandidate.where(email: params[:jm_candidate][:email], codigo_rec_pass: params[:jm_candidate][:codigo_rec_pass]).where('fecha_rec_pass > ?', lms_time - 30.minutes).first

    if @candidate
      if params[:jm_candidate][:password].blank?
        @candidate.errors.add :base, t('activerecord.errors.models.user.attributes.password.blank_update')
        params[:codigo_rec_pass] = params[:jm_candidate][:codigo_rec_pass]
        render 'jm_recuperar_pass_paso3', :layout => 'job_market'
      else
        if @candidate.update_attributes(password: params[:jm_candidate][:password], password_confirmation: params[:jm_candidate][:password_confirmation])
          @candidate.update_attributes(codigo_rec_pass: nil, fecha_rec_pass: nil)
          flash[:success] = t('activerecord.success.model.user.update_pass_ok')
          redirect_to jm_login_path
        else
          params[:codigo_rec_pass] = params[:jm_candidate][:codigo_rec_pass]
          render 'jm_recuperar_pass_paso3', :layout => 'job_market'
        end
      end
    else
      if candidate = JmCandidate.where(email: params[:jm_candidate][:email]).first
        flash[:danger] = t('rec_pass.jm_invalid_codigo')
        redirect_to jm_recuperar_pass_paso3_path(candidate)
      else
        flash[:danger] = 'Hubo un problema al intentar procesar su solicitud, por favor inténlo nuevamente.'
        redirect_to jm_login_path
      end
    end


  end

  def jm_regenerate_validation_code
    candidate = JmCandidate.find(params[:jm_candidate_id])
    if candidate.fecha_rec_pass > lms_time - 30.minutes
      candidate.codigo_rec_pass = SecureRandom.hex(3).to_s.upcase
      candidate.fecha_rec_pass = lms_time
      candidate.save
      flash[:success] = t('rec_pass.paso2_ok')

      notification_template = SelNotificationTemplate.where(event_id: 15, active: true).first
      if notification_template
        notification = notification_template.generate_notification({jm_candidate: candidate}, nil, lms_time)
        SelMailer.sel_notification(notification, @company).deliver
      else
        LmsMailer.jm_recuperar_pass(candidate, @company).deliver
      end

      redirect_to jm_recuperar_pass_paso3_path(candidate)
    else
      flash[:danger] = 'No existe un proceso pendiente de validación de correo para este usuario.'
    end
  end

  def redirect_from_ateneus
    redirect_to '/'
  end

  def test
    #User.integrate_hoc_satp
=begin
    tz = Time.now.utc.in_time_zone('America/Santiago').strftime('%::z').split(':')

    puts Time.now.utc.in_time_zone('America/Santiago')
    puts Time.now.utc.in_time_zone('America/Santiago').isdst
    puts Time.now.utc+((tz[0].to_i*60*60)+(tz[1].to_i*60)+tz[2].to_i).seconds

    tz = Time.now.utc.in_time_zone('Europe/Madrid').strftime('%::z').split(':')

    puts Time.now.utc.in_time_zone('Europe/Madrid')
    puts Time.now.utc.in_time_zone('Europe/Madrid').isdst
    puts Time.now.utc+((tz[0].to_i*60*60)+(tz[1].to_i*60)+tz[2].to_i).seconds
=end
  end

  def ejecuta_patch
=begin
    User.all.each do |user|
      licenses = MedlicLicense.by_user(user.id).reorder('date_begin ASC')

      licenses.each do |license|
        pivot = (license.date_begin.year.to_s+'-01-01').to_date

        loop do
          days = 0
          year_days = MedlicYearDaysLicense.where(user_id: user.id, year: pivot.year).first_or_create

          begin_lics = license.date_begin.to_date
          end_lics = license.date_end.to_date
          begin_date = (pivot.year.to_s+'-01-01').to_date
          end_date =  (pivot.year.to_s+'-12-31').to_date

          begin_period = begin_date.to_date
          end_period = end_date.to_date

          max_begin_date = begin_period > begin_lics ? begin_period : begin_lics
          min_end_date = end_period < end_lics ? end_period : end_lics
          days += (min_end_date - max_begin_date).to_i + 1

          year_days.days += days
          year_days.save

          pivot += 1.year
          break if pivot  > license.date_end
        end
      end
    end
=end
=begin
    cs = [5]

    UserCharacteristic.where('characteristic_id IN(?)', cs).each do |uc|

      cv = CharacteristicValue.where('characteristic_id = ? AND value_string = ?', uc.characteristic_id, uc.value_string).first

      if cv

        uc.characteristic_value_id = cv.id
        uc.value_string = nil
        uc.save

      end

    end

    UserCharacteristicRecord.where('characteristic_id IN(?)', cs).each do |ucr|

      cv = CharacteristicValue.where('characteristic_id = ? AND value_string = ?', ucr.characteristic_id, ucr.value_string).first

      if cv

        ucr.characteristic_value_id = cv.id
        ucr.value_string = nil
        ucr.save

      end

    end
=eng
=begin
    characteristic = Characteristic.where('data_type_id = ?',13).first

    j_job_level = JJobLevel.find 3

    Node.all.each do |node|

      j_job = JJob.new
      j_job.name = node.nombre
      j_job.j_job_level = j_job_level
      j_job.version = 1
      j_job.current = true
      j_job.from_date = lms_time
      unless j_job.save
        puts j_job.errors.full_messages
      end

    end

    User.all.each do |user|

      cv = user.characteristic_value(characteristic)

      j_job = JJob.where('name = ?', cv).first

      if j_job
        user.j_job = j_job
        user.save
      end

    end
=end


=begin
    ucs = UserCourse.where('program_course_id in(98,91,92) and iniciado = 1 and porcentaje_avance <> 100')

    ucs.each do |uc|
      update_percentage_advance(uc)
      finish_course(uc)
    end
=end


=begin
    UserCourse.all.each do |uc|
      uc.set_program_course_instance
      uc.save
    end
=end
=begin

      UserCourse.order('grupo_f DESC, hasta').each do |uc|


          uc.set_grupo_f
          uc.save


      end
=end
=begin

    UserCourse.all.each do |uc|

      uc.inicio = uc.inicio - 7.hours if uc.inicio
      uc.fin = uc.fin - 7.hours if uc.fin
      uc.save

    end


    LogLogin.all.each do |log|

      log.fecha = log.fecha - 7.hours
      log.save

    end

    LogFolderFile.all.each do |log|

      log.fecha = log.fecha - 7.hours
      log.save

    end

    LogUserCourse.all.each do |log|

      log.fecha = log.fecha - 7.hours
      log.save

    end
=end

  end


  def loading_sesion_data_from_other_company
    # usado por el sitio destino
    caller_company = Company.find(params[:caller_company_id])
    @remote_validation_url = caller_company.url + validate_smart_admin_login_path
    render :layout => 'flat'
  end

  def ask_for_validation_admin_logged_from_other_company
    # usado por el sitio base
    totp = ROTP::TOTP.new(SECRET_TOTP)
    token = admin_logged_in? ? Digest::SHA1.hexdigest(totp.now.to_s) : Digest::SHA1.hexdigest('E_G65s/#4df6s5d4f65oeirh')
    render :json => {:token => token, :id => session[:admin_connected_id]}, :callback => params[:callback]
  end

  def create_session_admin_from_other_company
    # usado por el sitio destino
    unless admin_logged_in?
      totp = ROTP::TOTP.new(SECRET_TOTP)
      if Digest::SHA1.hexdigest(totp.now.to_s) == params[:sha1_token]
        company = @company
        reset_session
        @company = company
        admin = Admin.find(params[:admin_id])
        create_session_admin(admin)
      end
    end

    redirect_to root_path
  end

  def single_sign_on
    data = params[:crypted_user_id]
    set_custom_http_auth_configuration
    begin
      cipher = OpenSSL::Cipher.new @custom_http_auth['user_code']['encriptation_method']
      cipher.decrypt
      # La llave se recibe como parámetro opcional, en caso de no venir debe estar en el secrets.yml
      cipher.key = @custom_http_auth['user_code']['cipher_key']
      decrypted_active_directory_id = decryp_user_from_custom_http_auth(data, cipher)
        # Se retorna el dato desencriptado
    rescue Exception => e
      error_decrypt = true
      puts 'error_decrypt: ' + error_decrypt.inspect
      puts 'traza: ' + e.inspect
    end

    #decrypted_active_directory_id = 'Nicolas.duhart@security.cl'
    #url_to_redirect = vac_records_register_register_new_path

    puts 'Se buscará el usuario con id: ' + decrypted_active_directory_id.inspect
    user = User.where(active_directory_id: decrypted_active_directory_id, activo: true).first unless decrypted_active_directory_id.blank?

    if user
        create_session(user, false)
        render :json => true
    else
      ActionMailer::Base.mail(from: 'Notificaciones EXA <notificaciones@exa.pe>', to: 'cesar@capitalteam.pe', cc: 'Innovacioncultura@security.cl', subject: 'Error en autenticación SSO Interna - Pruebas', body: 'Usuario no registrado. active_directory_id: ' + decrypted_active_directory_id.to_s).deliver
      render :json => false
    end

  end

  def insert_base_data
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='pe', name='Evaluación de Desempeño', alias='Evaluación de Desempeño', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'pe').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='sel', name='Selección', active=true, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'sel').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='jm', name='Job Market', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'jm').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='id', name='Documentos Informativos', active=false, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'id').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='mpi', name='Mis colaboradores', alias='Mis Colaboradores', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'mpi').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='um', name='Gestión de usuarios', active=true, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'um').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='lms', name='LMS', alias='Cursos', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'lms').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='kpis', name='KPIs', active=true, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'kpis').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='dnc', name='DNC', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'dnc').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='trkn', name='Seguimiento', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'trkn').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='blog', name='Bitácora', alias='Bitácora', active=false, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'blog').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='ben_cel_sec', name='Beneficios Celulares', alias='Beneficios Celulares', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'ben_cel_sec').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='sc_he', name='Control de horario', alias='Control de horario', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'sc_he').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='vacs', name='Vacaciones', alias='Vacaciones', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'vacs').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='ben_params', name='Parámetros de beneficios', alias='Parámetros de beneficios', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'ben_params').count == 0

    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='jobs', name='Puestos', alias='Puestos', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'jobs').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='polls', name='Encuestas', alias='Encuestas', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'polls').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='holidays', name='Feriados', alias='Feriados', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'holidays').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='brc', name='Bono Rol Comercial', alias='Bono Rol Comercial', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'brc').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='licenses', name='Beneficios', alias='Beneficios', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'licenses').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='brg', name='Bono Rol General', alias='Bono Rol General', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'brg').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='sch', name='Becas', alias='Becas', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'sch').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='medlic', name='Licencias', alias='Licencias', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'medlic').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='liq', name='Liquidaciones', alias='Liquidaciones', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'liq').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='lib', name='Biblioteca', alias='Biblioteca', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'lib').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_modules set cod='elec', name='Elecciones', alias='Elecciones', active=false, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'elec').count == 0

    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 1, cod='home', name='Inicio', user_menu_alias='Inicio', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'home').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 2, cod='tr', name='Capacitación', user_menu_alias='Cursos', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'tr').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 3, cod='lib', name='Biblioteca', user_menu_alias='Biblioteca', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'lib').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 4, cod='hd', name='Mesa de ayuda', user_menu_alias='Mesa de ayuda', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'hd').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 5, cod='sel', name='Selección', user_menu_alias='Selección', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'sel').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 6, cod='tm', name='Gestión de la capacitación', user_menu_alias='Gestión de la capacitación', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'tm').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 7, cod='pe', name='Evaluación de desempeño', user_menu_alias='Evaluación de desempeño', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'pe').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 8, cod='kpi', name='KPIs', user_menu_alias='KPI Dashboards', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'kpi').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 9, cod='up', name='Ficha de usuario', user_menu_alias='Ficha de usuario', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'up').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 10, cod='sec_', name='Ficha de usuario', user_menu_alias='Ficha de usuario', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'up').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 11, cod='brc', name='Bono comercial', user_menu_alias='Bono Comercial', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'brc').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 12, cod='vacs', name='Vacaciones', user_menu_alias='Vacaciones', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'vacs').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 13, cod='licenses', name='Beneficios', user_menu_alias='Beneficios', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'licenses').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 14, cod='brg', name='Bono general', user_menu_alias='Bono General', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'brg').count == 0
    ActiveRecord::Base.connection.execute "insert into ct_menus set position = 15, cod='brg_rp', name='Bono privado', user_menu_alias='Bono Privado', created_at = NOW(), updated_at = NOW()" if CtMenu.where('cod = ?', 'brg_rp').count == 0
  end

  def save_show_azure_ajax_call
    response = false
    value = @company.active_directory
    if value && user_connected.id == params[:user_id].to_i
      user = User.find(params[:user_id])
      new_value = (params[:show_value] == 'true' ? false : true)
      user.show_azure_landing_page = new_value
      response = user.valid?
      user.save
      set_show_azure_cookie_value(user.id)
    end
    render json: response
  end

  private

  def set_show_azure_cookie_value(user_id)
    value = @company.active_directory
    if value
      cookies[:show_azure] = {
          value: User.find(user_id).show_azure_landing_page,
          expires: 10.year.from_now
      }
    end
  end

  def get_data
   #@company = session[:company]
  end

  def get_data2

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_member = @pe_process.pe_member_by_user user_connected

  end

  def validate_login
    value = @company.active_directory
    unless logged_in?
      if value
        if defined?(params[:p]) && params[:p] == '1'
          render 'login'
        else
          session[:redirect_post_login] = root_path
          set_custom_http_auth_configuration
          render 'static_pages/loading_sesion_data_from_external_provider'
        end
      else
        render 'login'
      end
    end
  end

  def validate_pe_rep_detailed_final_results
    unless @pe_process.display_results_in_user_profile? && @pe_process.display_user_profile_show_rep_dfr?
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

  def validate_pe_rep_detailed_final_results_pdf
    unless @pe_process.display_results_in_user_profile? && @pe_process.display_user_profile_show_rep_dfr_pdf?
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

=begin
  def validate_characteristic_file

    @characteristic = Characteristic.find_by_id(params[:characteristic_id])

    @user_characteristic = user_connected.user_characteristics.find_by_characteristic_id(params[:characteristic_id])

    unless @characteristic && @characteristic.data_type_id == 6 && @user_characteristic && @user_characteristic.value_file
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end

  end
=end

end
