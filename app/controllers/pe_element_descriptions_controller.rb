class PeElementDescriptionsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new

    @pe_element = PeElement.find(params[:pe_element_id])
    @pe_element_description = @pe_element.pe_element_descriptions.build
    @pe_evaluation = @pe_element.pe_evaluation
    @pe_process = @pe_evaluation.pe_process

  end

  def edit
    @pe_element_description = PeElementDescription.find(params[:id])
    @pe_element = @pe_element_description.pe_element
    @pe_evaluation = @pe_element.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def create

    @pe_element_description = PeElementDescription.new(params[:pe_element_description])

    @pe_element = PeElement.find(params[:pe_element_description][:pe_element_id])

    if @pe_element_description.save
      redirect_to @pe_element
    else

      @pe_evaluation = @pe_element.pe_evaluation
      @pe_process = @pe_evaluation.pe_process

      render action: 'new'

    end


  end


  def update

    @pe_element_description = PeElementDescription.find(params[:id])


    if @pe_element_description.update_attributes(params[:pe_element_description])
      redirect_to @pe_element_description.pe_element
    else
      @pe_element = @pe_element_description.pe_element
      @pe_evaluation = @pe_element.pe_evaluation
      @pe_process = @pe_evaluation.pe_process

      render action: 'edit'
    end
  end


  def destroy
    @pe_element_description = PeElementDescription.find(params[:id])
    @pe_element = @pe_element_description.pe_element
    @pe_element_description.destroy

    redirect_to @pe_element

  end
end
