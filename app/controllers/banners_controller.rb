class BannersController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /banners
  # GET /banners.json
  def index
    @banners = Banner.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @banners }
    end
  end

  # GET /banners/1
  # GET /banners/1.json
  def show
    @banner = Banner.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @banner }
    end
  end

  # GET /banners/new
  # GET /banners/new.json
  def new
    @banner = Banner.new

    @characteristics = Characteristic.all
    @characteristics_prev = {}
    @mostrar_todos = true

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @banner }
    end
  end

  # GET /banners/1/edit
  def edit
    @banner = Banner.find(params[:id])
    @characteristics = Characteristic.all

    @characteristics_prev = {}

    if BannerCharacteristic.find_all_by_banner_id(params[:id]).count > 0

      @characteristics.each do |characteristic|
        @characteristics_buffer = []
        BannerCharacteristic.where(:banner_id => params[:id], :characteristic_id => characteristic.id).each do |banner_characteristic|
          @characteristics_buffer.push(banner_characteristic.match_value)
        end
        if @characteristics_buffer.length > 0
          @characteristics_prev[characteristic.id] = @characteristics_buffer
        end
      end
      @mostrar_todos = false
    else
      @mostrar_todos = true
    end
  end

  # POST /banners
  # POST /banners.json
  def create
    @banner = Banner.new(params[:banner])

    respond_to do |format|

      if @banner.save

        save_characteristics @banner.id
        #TODO: añadir el texto al locales
        flash[:success] = 'El banner fue creado correctamente'

        format.html { redirect_to banners_url }
        format.json { render json: @banner, status: :created, location: @banner }
      else

        @characteristics = Characteristic.all

        @characteristics_prev = {}

        format.html { render action: "new" }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /banners/1
  # PUT /banners/1.json
  def update
    @banner = Banner.find(params[:id])

    respond_to do |format|


      if @banner.update_attributes(params[:banner])

        save_characteristics @banner.id
        #TODO: añadir el texto al locales
        flash[:success] = 'El banner fue actualizado correctamente'
        format.html { redirect_to banners_url }
        format.json { head :no_content }
      else

        @characteristics = Characteristic.all

        @characteristics_prev = {}

        format.html { render action: "edit" }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end

  end

  # DELETE /banners/1
  # DELETE /banners/1.json
  def destroy
    @banner = Banner.find(params[:id])
    @banner.destroy

    respond_to do |format|
      format.html { redirect_to banners_url }
      format.json { head :no_content }
    end
  end

  def upload_image_form
    @banner = Banner.find(params[:id])
  end


  def upload_image
    @banner = Banner.find(params[:id])
   #@company = session[:company]

    if params[:upload]

      upload = params[:upload]

      if File.extname(upload['datafile'].original_filename) == '.jpg'
        #genera el crypted_name (la primera vez que se sube una imagen solamente)

        @banner.crypted_name = generate_crypted_name(@banner) unless @banner.crypted_name

        save_file params[:upload], @banner, @company

        @banner.save()

        flash[:success] = t('activerecord.success.model.banner.image_upload_ok')

        redirect_to banners_url

      else
        flash.now[:danger] = t('activerecord.error.model.banner.image_file_must_jpg')
        render 'upload_image_form'

      end
    else
      flash.now[:danger] = t('activerecord.error.model.banner.file_form_no_file_selected')
      render 'upload_image_form'

    end

  end


  def upload_complement_form
    @banner = Banner.find(params[:id])
  end

  def upload_complement
    @banner = Banner.find(params[:id])
   #@company = session[:company]

    if @banner.crypted_name

      if params[:upload]

        upload = params[:upload]

        if File.extname(upload['datafile'].original_filename) == '.zip'

          save_file params[:upload], @banner, @company

          flash[:success] = t('activerecord.success.model.banner.complement_upload_ok')

          redirect_to banners_url

        else
          flash.now[:danger] = t('activerecord.error.model.banner.complement_file_must_zip')
          render 'upload_complement_form'

        end
      else
        flash.now[:danger] = t('activerecord.error.model.banner.file_form_no_file_selected')
        render 'upload_complement_form'

      end
    else
      flash.now[:danger] = t('activerecord.error.model.banner.complement_must_have_image_before')
      render 'upload_image_form'
    end
  end

  def path_image_hard_drive(banner, company)

    directory = company.directorio+'/'+company.codigo+'/banners/'+banner.crypted_name+'/'
    name = 'ba'+banner.id.to_s+'.jpg'
    path = File.join(directory, name)

    path_hash = {:directory => directory, :name => name, :full_path => path}
  end

  def path_complement_hard_drive(banner, company)

    directory = company.directorio+'/'+company.codigo+'/banners/'+banner.crypted_name+'/f2/'
    name = 'ba'+banner.id.to_s+'.zip'
    path = File.join(directory, name)

    path_hash = {:directory => directory, :name => name, :full_path => path}
  end

  def generate_crypted_name(banner)

    crypted_name = banner.id.to_s + '_' + SecureRandom.hex(5)

  end

  private

  def save_file(upload, banner, company)

    image_path_hash = path_image_hard_drive(banner, company)

    FileUtils.mkdir_p image_path_hash[:directory] unless File.directory? image_path_hash[:directory]

    if File.extname(upload['datafile'].original_filename) == '.jpg'

      File.open(image_path_hash[:full_path], 'wb') { |f| f.write(upload['datafile'].read) }

    elsif File.extname(upload['datafile'].original_filename) == '.zip'
      complement_path_hash = path_complement_hard_drive(banner, company)

      FileUtils.remove_dir complement_path_hash[:directory] if File.directory? complement_path_hash[:directory]
      FileUtils.mkdir_p complement_path_hash[:directory]

      File.open(complement_path_hash[:full_path], 'wb') { |f| f.write(upload['datafile'].read) }
      system "unzip -o -q \"#{complement_path_hash[:full_path]}\" -d \"#{complement_path_hash[:directory]}\" "
      File.delete complement_path_hash[:full_path]
    end

  end

  def save_characteristics(banner_id)
    # se actualizan las characteristics asociadas para filtrar
    @characteristics = Characteristic.all
    BannerCharacteristic.delete_all(:banner_id => banner_id)

    if !params[:banner_todos]
      if params[:characteristic]
        @characteristics.each do |characteristic|
          params[:characteristic][characteristic.id.to_s.to_sym].slice! 0
          params[:characteristic][characteristic.id.to_s.to_sym].each do |match_value|
            BannerCharacteristic.new(:banner_id => banner_id, :characteristic_id => characteristic.id, :match_value => match_value).save!
          end
        end
      end
    end
  end

end
