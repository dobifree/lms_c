class CourseCertificatesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new

    @course = Course.find(params[:course_id])

    @course_certificate = @course.course_certificates.new


  end


  def edit
    @course_certificate = CourseCertificate.find(params[:id])
    @course = @course_certificate.course

  end


  def create

    @course_certificate = CourseCertificate.new(params[:course_certificate])

    if @course_certificate.save
      flash[:success] = t('activerecord.success.model.course_certificate.create_ok')
      redirect_to @course_certificate.course
    else
      @course = @course_certificate.course
      render 'new'
    end

  end

  def update
    @course_certificate = CourseCertificate.find(params[:id])


      if @course_certificate.update_attributes(params[:course_certificate])
        flash[:success] = t('activerecord.success.model.course_certificate.update_ok')
        redirect_to edit_course_certificate_path @course_certificate
      else
        @course = @course_certificate.course
        render action: 'edit'
      end

  end

  def destroy
    @course_certificate = CourseCertificate.find(params[:id])

   company = @company
    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_cursos/'

    if @course_certificate.template
      file = directory+@course_certificate.template
      File.delete(file) if File.exist? file
    end

    course = @course_certificate.course

    @course_certificate.destroy

    flash[:success] = t('activerecord.success.model.course_certificate.delete_ok')

    redirect_to course

  end

  def upload_template_form
    @course_certificate = CourseCertificate.find(params[:id])
    @course = @course_certificate.course

  end

  def upload_template
    @course_certificate = CourseCertificate.find(params[:id])
    @course = @course_certificate.course

   company = @company

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext != '.pdf'

        flash[:danger] = t('activerecord.error.model.course_certificate.update_template_wrong_format')
        redirect_to upload_course_certificate_template_path @course_certificate

      elsif File.size(params[:upload]['datafile'].tempfile) > 2048.kilobytes

        flash[:danger] = t('activerecord.error.model.course_certificate.update_template_wrong_wrong_size')
        redirect_to upload_course_certificate_template_path @course_certificate

      else

        save_template params[:upload], @course_certificate, company

        flash[:success] = t('activerecord.success.model.course_certificate.update_template_ok')

        redirect_to @course

      end



    else

      flash[:danger] = t('activerecord.error.model.course_certificate.update_template_empty_file')

      redirect_to @course

    end

  end

  def download_template
    @course_certificate = CourseCertificate.find(params[:id])

   company = @company
    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_cursos/'

    if @course_certificate.template
      file = directory+@course_certificate.template+'.pdf'

        send_file file,
              filename: 'Plantilla Certificado - '+@course_certificate.course.nombre+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

    end

  end

  def preview_certificado

    @course_certificate = CourseCertificate.find(params[:id])

   company = @company
    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_cursos/'
    plantilla = directory+(@course_certificate.template)+'.jpg'

        require 'prawn/measurement_extensions'
        #require 'prawn/templates'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.pdf'
        archivo_temporal = directorio_temporal+nombre_temporal

        nombre =  'Nombres Apellidos'

        nombre_curso =  'Nombre del Curso'


        nota = '20'

        desde = localize(lms_time, format: @course_certificate.desde_formato.to_sym)
        hasta = localize(lms_time, format: @course_certificate.hasta_formato.to_sym)

        inicio = localize(lms_time, format: @course_certificate.inicio_formato.to_sym)
        fin = localize(lms_time, format: @course_certificate.fin_formato.to_sym)

        fecha = localize(lms_time, format: @course_certificate.fecha_formato.to_sym)

        cc = @course_certificate

        Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: [720,540]) do |pdf|

          pdf.font_families.update('Foco Light' => {:normal => '/var/www/remote-storage/fonts/Foco_Std_Lt_0.ttf'})
          pdf.font_families.update('HaasGrotDisp Light' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-45Light_1.ttf'})
          pdf.font_families.update('HaasGrotDisp Normal' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-65Medium_1.ttf'})
          pdf.font_families.update('HaasGrotDisp Bold' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-75Bold_1.ttf'})
          #pdf.font_families.update('Foco Light' => {:normal => '/Users/alejo/Documents/Current/RocketLab/Seeds/LMS/diseños/pacífico/certificados/Foco_Std_Lt_0.ttf'})

          pdf.start_new_page
          pdf.image plantilla, at: [-36,504], width: 720

          if cc.nombre
            if cc.nombre_x == -1
              pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :style => :normal, :size => cc.nombre_size} ], :at => [0, cc.nombre_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :style => :normal, :size => cc.nombre_size} ], :at => [cc.nombre_x.mm, cc.nombre_y.mm]
            end
          end

          if cc.nombre_curso

            if cc.nombre_curso_x == -1
              pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :style => :normal, :size => cc.nombre_curso_size} ], :at => [0, cc.nombre_curso_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :style => :normal, :size => cc.nombre_curso_size} ], :at => [cc.nombre_curso_x.mm, cc.nombre_curso_y.mm]
            end
          end

          if cc.fecha
            if cc.fecha_x == -1
              pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :center
            elsif cc.fecha_x == -2
              pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :right
            else
              pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [cc.fecha_x.mm, cc.fecha_y.mm]
            end
          end

          if cc.desde
            if cc.desde_x == -1
              pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :style => :normal, :size => cc.desde_size} ], :at => [0, cc.desde_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :style => :normal, :size => cc.desde_size} ], :at => [cc.desde_x.mm, cc.desde_y.mm]
            end
          end

          if cc.hasta
            if cc.hasta_x == -1
              pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :style => :normal, :size => cc.hasta_size} ], :at => [0, cc.hasta_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :style => :normal, :size => cc.hasta_size} ], :at => [cc.hasta_x.mm, cc.hasta_y.mm]
            end
          end

          if cc.inicio
            if cc.inicio_x == -1
              pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :style => :normal, :size => cc.inicio_size} ], :at => [0, cc.inicio_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :style => :normal, :size => cc.inicio_size} ], :at => [cc.inicio_x.mm, cc.inicio_y.mm]
            end
          end

          if cc.fin
            if cc.fin_x == -1
              pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :style => :normal, :size => cc.fin_size} ], :at => [0, cc.fin_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :style => :normal, :size => cc.fin_size} ], :at => [cc.fin_x.mm, cc.fin_y.mm]
            end
          end

          if cc.nota
            if cc.nota_x == -1
              pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :style => :normal, :size => cc.nota_size} ], :at => [0, cc.nota_y.mm],  :align => :center
            else
              pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :style => :normal, :size => cc.nota_size} ], :at => [cc.nota_x.mm, cc.nota_y.mm]
            end
          end


        end

        send_file archivo_temporal,
                  filename: 'Preview Certificado - '+@course_certificate.course.nombre+'.pdf',
                  type: 'application/octet-stream',
                  disposition: 'attachment'




  end

  def save_template(upload, course_certificate, company)

    require 'RMagick'

    ext = File.extname(upload['datafile'].original_filename)

    if ext == '.pdf'

      if File.size(upload['datafile'].tempfile) <= 2048.kilobytes

        directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_cursos/'

        FileUtils.mkdir_p directory unless File.directory? directory

        if course_certificate.template
          file = directory+course_certificate.template
          File.delete(file+'.jpg') if File.exist? file+'.jpg'
          File.delete(file+'.pdf') if File.exist? file+'.pdf'
        end

        course_certificate.set_template_name
        course_certificate.save

        file = directory+course_certificate.template+'.pdf'

        File.open(file, 'wb') { |f| f.write(upload['datafile'].read) }

        image = Magick::Image::read(file) do
          self.quality = 100
          self.density = '300x300'
        end

        image[0].write(directory+course_certificate.template+'.jpg')

      end

    end

  end


end
