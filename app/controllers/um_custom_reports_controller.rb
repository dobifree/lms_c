class UmCustomReportsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /um_custom_reports
  # GET /um_custom_reports.json
  def index
    @um_custom_reports = UmCustomReport.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @um_custom_reports }
    end
  end

  # GET /um_custom_reports/1
  # GET /um_custom_reports/1.json
  def show
    @um_custom_report = UmCustomReport.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @um_custom_report }
    end
  end

  # GET /um_custom_reports/new
  # GET /um_custom_reports/new.json
  def new
    @um_custom_report = UmCustomReport.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @um_custom_report }
    end
  end

  # GET /um_custom_reports/1/edit
  def edit
    @um_custom_report = UmCustomReport.find(params[:id])
  end

  # POST /um_custom_reports
  # POST /um_custom_reports.json
  def create
    @um_custom_report = UmCustomReport.new(params[:um_custom_report])

    respond_to do |format|
      if @um_custom_report.save
        format.html { redirect_to @um_custom_report, notice: 'Um custom report was successfully created.' }
        format.json { render json: @um_custom_report, status: :created, location: @um_custom_report }
      else
        format.html { render action: "new" }
        format.json { render json: @um_custom_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /um_custom_reports/1
  # PUT /um_custom_reports/1.json
  def update
    @um_custom_report = UmCustomReport.find(params[:id])

    respond_to do |format|
      if @um_custom_report.update_attributes(params[:um_custom_report])
        format.html { redirect_to @um_custom_report, notice: 'Um custom report was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @um_custom_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /um_custom_reports/1
  # DELETE /um_custom_reports/1.json
  def destroy
    @um_custom_report = UmCustomReport.find(params[:id])
    @um_custom_report.destroy

    respond_to do |format|
      format.html { redirect_to um_custom_reports_url }
      format.json { head :no_content }
    end
  end
end
