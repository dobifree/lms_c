class PeCharacteristicsController < ApplicationController

  include PeAssessmentHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def edit_reps_configuration

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_reps_configuration

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_reps_configuration = true
        pe_characteristic.pos_reps_configuration = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_reps_configuration = false
        pe_characteristic.pos_reps_configuration = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'reports'
    
  end

  def edit_rep_assessment_status

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_rep_assessment_status

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_rep_assessment_status = true
        pe_characteristic.pos_rep_assessment_status = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_rep_assessment_status = false
        pe_characteristic.pos_rep_assessment_status = nil

      end

      pe_characteristic.save

    end
=begin
    @pe_process.evaluated_members.each do |pe_member_evaluated|

      if pe_member_evaluated.step_assessment? && pe_member_evaluated.pe_box.nil?

        @pe_process.pe_evaluations.each do |pe_evaluation|

          calculate_assessment_dimension pe_member_evaluated, pe_evaluation.pe_dimension

        end

        pe_member_evaluated.set_pe_box
        pe_member_evaluated.save
      end

    end
=end
    redirect_to pe_process_tab_url @pe_process, 'reports'

  end

  def edit_rep_calibration_status

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_rep_calibration_status

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_rep_calibration_status = true
        pe_characteristic.pos_rep_calibration_status = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_rep_calibration_status = false
        pe_characteristic.pos_rep_calibration_status = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'reports'

  end

  def edit_rep_feedback_status

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_rep_feedback_status

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_rep_feedback_status = true
        pe_characteristic.pos_rep_feedback_status = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_rep_feedback_status = false
        pe_characteristic.pos_rep_feedback_status = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'reports'

  end

  def edit_rep_consolidated_final_results

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_rep_consolidated_final_results

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_process.rep_consolidated_final_results_col_boss = params[:pe_process][:rep_consolidated_final_results_col_boss]
    @pe_process.save

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_rep_consolidated_final_results = true
        pe_characteristic.pos_rep_consolidated_final_results = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_rep_consolidated_final_results = false
        pe_characteristic.pos_rep_consolidated_final_results = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'reports'

  end

  def edit_rep_detailed_final_results

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_rep_detailed_final_results

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_rep_detailed_final_results = true
        pe_characteristic.pos_rep_detailed_final_results = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_rep_detailed_final_results = false
        pe_characteristic.pos_rep_detailed_final_results = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'reports'

  end

  def edit_gui_cal_committee

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_gui_cal_committee

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_gui_cal_committee = true
        pe_characteristic.pos_gui_cal_committee = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_gui_cal_committee = false
        pe_characteristic.pos_gui_cal_committee = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'calibration'

  end

  def edit_gui_cal_member

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_gui_cal_member

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_gui_cal_member = true
        pe_characteristic.pos_gui_cal_member = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_gui_cal_member = false
        pe_characteristic.pos_gui_cal_member = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'calibration'

  end

  def edit_gui_feedback

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_gui_feedback

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_gui_feedback = true
        pe_characteristic.pos_gui_feedback = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_gui_feedback = false
        pe_characteristic.pos_gui_feedback = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'feedback'

  end

  def edit_gui_validation

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_gui_validation

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_gui_validation = true
        pe_characteristic.pos_gui_validation = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_gui_validation = false
        pe_characteristic.pos_gui_validation = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'validation'

  end

  def edit_gui_selection

    @pe_process = PeProcess.find(params[:pe_process_id])

  end

  def update_gui_selection

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_process.pe_characteristics.each do |pe_characteristic|

      if params['cha_'+pe_characteristic.id.to_s+'_active']

        pe_characteristic.active_gui_selection = true
        pe_characteristic.pos_gui_selection = params['cha_'+pe_characteristic.id.to_s+'_position']

      else

        pe_characteristic.active_gui_selection = false
        pe_characteristic.pos_gui_selection = nil

      end

      pe_characteristic.save

    end

    redirect_to pe_process_tab_url @pe_process, 'sel'

  end

  ###

  def index
    @pe_characteristics = PeCharacteristic.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_characteristics }
    end
  end

  # GET /pe_characteristics/1
  # GET /pe_characteristics/1.json
  def show
    @pe_characteristic = PeCharacteristic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_characteristic }
    end
  end

  # GET /pe_characteristics/new
  # GET /pe_characteristics/new.json
  def new
    @pe_characteristic = PeCharacteristic.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pe_characteristic }
    end
  end

  # GET /pe_characteristics/1/edit
  def edit
    @pe_characteristic = PeCharacteristic.find(params[:id])
  end

  # POST /pe_characteristics
  # POST /pe_characteristics.json
  def create
    @pe_characteristic = PeCharacteristic.new(params[:pe_characteristic])

    respond_to do |format|
      if @pe_characteristic.save
        format.html { redirect_to @pe_characteristic, notice: 'Pe characteristic was successfully created.' }
        format.json { render json: @pe_characteristic, status: :created, location: @pe_characteristic }
      else
        format.html { render action: "new" }
        format.json { render json: @pe_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pe_characteristics/1
  # PUT /pe_characteristics/1.json
  def update
    @pe_characteristic = PeCharacteristic.find(params[:id])

    respond_to do |format|
      if @pe_characteristic.update_attributes(params[:pe_characteristic])
        format.html { redirect_to @pe_characteristic, notice: 'Pe characteristic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pe_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pe_characteristics/1
  # DELETE /pe_characteristics/1.json
  def destroy
    @pe_characteristic = PeCharacteristic.find(params[:id])
    @pe_characteristic.destroy

    respond_to do |format|
      format.html { redirect_to pe_characteristics_url }
      format.json { head :no_content }
    end
  end
end
