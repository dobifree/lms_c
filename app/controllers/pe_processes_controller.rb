class PeProcessesController < ApplicationController

  include PeAssessmentHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @pe_processes = PeProcess.all
  end

  def show
    @pe_process = PeProcess.find(params[:id])

  end

  def show_partial

    @pe_process = PeProcess.find(params[:id])
   #@company = session[:company]

    render partial: params[:tab]

  end

  def search_members

    @pe_process = PeProcess.find(params[:id])
   #@company = session[:company]

    @pe_members = @pe_process.search_members_ordered(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

    render partial: 'members_all_search'

  end

  def search_members_evaluated

    @pe_process = PeProcess.find(params[:id])
   #@company = session[:company]

    if params[:search_removed] == '1'

      @pe_members_evaluated = @pe_process.search_removed_members_ordered(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

    else

      @pe_members_evaluated = @pe_process.search_evaluated_members_ordered(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

    end

    render partial: 'members_evaluated_search'

  end

  def search_members_evaluator

    @pe_process = PeProcess.find(params[:id])
   #@company = session[:company]

    @pe_members_evaluator = @pe_process.search_evaluator_members_ordered(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

    render partial: 'members_evaluator_search'

  end

  def search_assessment_validators

    @pe_process = PeProcess.find(params[:id])
   #@company = session[:company]

    @pe_members_evaluated = @pe_process.search_evaluated_members_ordered(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

    render partial: 'validators'

  end

  def search_feedback_providers

    @pe_process = PeProcess.find(params[:id])
   #@company = session[:company]

    @pe_members_evaluated = @pe_process.search_evaluated_members_ordered(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

    render partial: 'feedback_providers'

  end

  def search_observers

    @pe_process = PeProcess.find(params[:id])
   #@company = session[:company]

    @pe_members_evaluated = @pe_process.search_evaluated_members_ordered(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])

    render partial: 'observers_search'

  end

  def new
    @pe_process = PeProcess.new
  end

  def create
    @pe_process = PeProcess.new(params[:pe_process])

    if @pe_process.save

      pe_dimension = @pe_process.pe_dimensions.build
      pe_dimension.dimension = 1
      pe_dimension.name = 'Eje X'
      pe_dimension.save

      pe_dimension = @pe_process.pe_dimensions.build
      pe_dimension.dimension = 2
      pe_dimension.name = 'Eje Y'
      pe_dimension.save

      flash[:success] = t('activerecord.success.model.pe_process.create_ok')
      redirect_to @pe_process
    else
      render action: 'new'
    end

  end

  def new_clone
    @pe_process = PeProcess.new
  end

  def create_clone

    pe_rels_array = Array.new
    pe_groups2_array = Array.new
    pe_boxes_array = Array.new

    pe_dimensions_array = Array.new
    pe_dimension_groups_array = Array.new
    pe_evaluations_array = Array.new
    pe_elements_array = Array.new
    pe_question_models_array = Array.new
    pe_question_rank_models_array = Array.new

    pe_questions_array = Array.new
    pe_alternatives_array = Array.new

    pe_groups_array = Array.new

    pe_areas_array = Array.new
    pe_members_array = Array.new
    pe_member_rels_array = Array.new

    pe_process_clone = PeProcess.find(params[:pe_process_clone_id])

    pe_feedback_field_list_array = Array.new
    pe_feedback_field_list_item_array = Array.new

    pe_feedback_field_array = Array.new
    pe_feedback_compound_field_array = Array.new
    pe_feedback_accepted_field_array = Array.new

    @pe_process = pe_process_clone.dup

    @pe_process.period = params[:pe_process][:period]
    @pe_process.name = params[:pe_process][:name]
    @pe_process.description = params[:pe_process][:description]
    @pe_process.from_date = params[:pe_process][:from_date]
    @pe_process.to_date = params[:pe_process][:to_date]

    if @pe_process.save

      pe_process_reports = @pe_process.pe_process_reports

      pe_process_reports.rep_evaluated_evaluators_configuration = pe_process_clone.pe_process_reports.rep_evaluated_evaluators_configuration
      pe_process_reports.rep_evaluations_configutation = pe_process_clone.pe_process_reports.rep_evaluations_configutation
      pe_process_reports.rep_general_status = pe_process_clone.pe_process_reports.rep_general_status
      pe_process_reports.rep_assessment_status = pe_process_clone.pe_process_reports.rep_assessment_status
      pe_process_reports.rep_validation_status = pe_process_clone.pe_process_reports.rep_validation_status
      pe_process_reports.rep_calibration_status = pe_process_clone.pe_process_reports.rep_calibration_status
      pe_process_reports.rep_feedback_status = pe_process_clone.pe_process_reports.rep_feedback_status
      pe_process_reports.rep_assessment_consolidated_results = pe_process_clone.pe_process_reports.rep_assessment_consolidated_results
      pe_process_reports.rep_assessment_detailed_results = pe_process_clone.pe_process_reports.rep_assessment_detailed_results
      pe_process_reports.rep_detailed_final_results_evaluations = pe_process_clone.pe_process_reports.rep_detailed_final_results_evaluations
      pe_process_reports.rep_consolidated_feedback = pe_process_clone.pe_process_reports.rep_consolidated_feedback
      pe_process_reports.rep_detailed_final_results_questions = pe_process_clone.pe_process_reports.rep_detailed_final_results_questions
      pe_process_reports.rep_consolidated_feedback_accepted = pe_process_clone.pe_process_reports.rep_consolidated_feedback_accepted
      pe_process_reports.rep_consolidated_feedback_accepted_survey = pe_process_clone.pe_process_reports.rep_consolidated_feedback_accepted_survey
      pe_process_reports.save


      pe_process_clone.pe_rels.each do |pe_rel_clone|
        pe_rel = pe_rel_clone.dup
        pe_rel.pe_process = @pe_process
        pe_rel.save

        pe_rels_array[pe_rel_clone.id] = pe_rel

      end

      pe_process_clone.pe_group2s.each do |pe_group2_clone|
        pe_group2 = pe_group2_clone.dup
        pe_group2.pe_process = @pe_process
        pe_group2.save

        pe_groups2_array[pe_group2_clone.id] = pe_group2

      end

      #structure
      if params[:clone_structure] == '1'

        pe_process_clone.pe_dimensions.each do |pe_dimension_clone|
          pe_dimension = pe_dimension_clone.dup
          pe_dimension.pe_process = @pe_process
          pe_dimension.save

          pe_dimensions_array[pe_dimension_clone.id] = pe_dimension

          pe_dimension_clone.pe_dimension_groups.each do |pe_dimension_group_clone|
            pe_dimension_group = pe_dimension_group_clone.dup
            pe_dimension_group.pe_dimension = pe_dimension
            pe_dimension_group.save

            pe_dimension_groups_array[pe_dimension_group_clone.id] = pe_dimension_group

          end

          pe_dimension_clone.pe_evaluations.each do |pe_evaluation_clone|
            pe_evaluation = pe_evaluation_clone.dup
            pe_evaluation.pe_dimension = pe_dimension
            pe_evaluation.pe_process = @pe_process
            pe_evaluation.save

            pe_evaluations_array[pe_evaluation_clone.id] = pe_evaluation

            pe_element_prev = nil

            pe_evaluation_clone.pe_elements.each do |pe_element_clone|
              pe_element = pe_element_clone.dup
              pe_element.pe_evaluation = pe_evaluation
              pe_element.pe_element = pe_element_prev
              pe_element.save
              pe_element_prev = pe_element

              pe_elements_array[pe_element_clone.id] = pe_element

              pe_element_clone.pe_element_descriptions.each do |pe_element_description_clone|
                pe_element_description = pe_element_description_clone.dup
                pe_element_description.pe_element = pe_element
                pe_element_description.save
              end

              pe_element_clone.pe_question_models.each do |pe_question_model_clone|
                pe_question_model = pe_question_model_clone.dup
                pe_question_model.pe_element = pe_element
                pe_question_model.save

                pe_question_models_array[pe_question_model_clone.id] = pe_question_model

              end

              pe_element_clone.pe_question_rank_models.each do |pe_question_rank_model_clone|
                pe_question_rank_model = pe_question_rank_model_clone.dup
                pe_question_rank_model.pe_element = pe_element
                pe_question_rank_model.save

                pe_question_rank_models_array[pe_question_rank_model_clone.id] = pe_question_rank_model

              end

            end

            pe_evaluation_clone.pe_groups.each do |pe_group_clone|
              pe_group = pe_group_clone.dup
              pe_group.pe_evaluation = pe_evaluation
              pe_group.save

              pe_groups_array[pe_group_clone.id] = pe_group

            end

            pe_evaluation_clone.pe_evaluation_rels.each do |pe_evaluation_rel_clone|
              pe_evaluation_rel = pe_evaluation_rel_clone.dup
              pe_evaluation_rel.pe_evaluation = pe_evaluation
              pe_evaluation_rel.pe_rel = pe_rels_array[pe_evaluation_rel_clone.pe_rel.id]
              pe_evaluation_rel.save

              pe_evaluation_rel_clone.pe_evaluation_rel_group2s.each do |pe_evaluation_rel_group2_clone|
                pe_evaluation_rel_group2 = pe_evaluation_rel_group2_clone.dup
                pe_evaluation_rel_group2.pe_evaluation_rel = pe_evaluation_rel
                pe_evaluation_rel_group2.pe_group2 = pe_groups2_array[pe_evaluation_rel_group2_clone.pe_group2.id] if pe_evaluation_rel_group2_clone.pe_group2
                pe_evaluation_rel_group2.save
              end

            end

            pe_evaluation_clone.pe_slider_groups.each do |pe_slider_group_clone|
              pe_slider_group = pe_slider_group_clone.dup
              pe_slider_group.pe_evaluation = pe_evaluation
              pe_slider_group.save
            end

            pe_evaluation_clone.pe_evaluation_def_rel_messages.each do |pe_evaluation_def_rel_message_clone|
              pe_evaluation_def_rel_message = pe_evaluation_def_rel_message_clone.dup
              pe_evaluation_def_rel_message.pe_evaluation = pe_evaluation
              pe_evaluation_def_rel_message.pe_rel = pe_rels_array[pe_evaluation_def_rel_message_clone.pe_rel.id]
              pe_evaluation_def_rel_message.save
            end

            pe_evaluation_clone.pe_delegated_evaluations.each do |pe_delegated_evaluation_clone|
              #not in use
            end

          end

        end

        pe_process_clone.pe_boxes.each do |pe_box_clone|
          pe_box = pe_box_clone.dup
          pe_box.pe_process = @pe_process
          pe_box.pe_dimension_group_x = pe_dimension_groups_array[pe_box_clone.pe_dimension_group_x.id] if pe_box_clone.pe_dimension_group_x
          pe_box.pe_dimension_group_y = pe_dimension_groups_array[pe_box_clone.pe_dimension_group_y.id] if pe_box_clone.pe_dimension_group_y
          pe_box.save

          pe_boxes_array[pe_box_clone.id] = pe_box

        end

        pe_process_clone.pe_evaluations.each do |pe_evaluation_clone|
          pe_evaluation_clone.pe_evaluation_displayed_def_by_users.each do |pe_evaluation_displayed_def_by_user_clone|
            pe_evaluation_displayed_def_by_user = pe_evaluation_displayed_def_by_user_clone.dup
            pe_evaluation_displayed_def_by_user.pe_evaluation = pe_evaluations_array[pe_evaluation_clone.id]
            pe_evaluation_displayed_def_by_user.pe_displayed_evaluation = pe_evaluations_array[pe_evaluation_displayed_def_by_user_clone.pe_displayed_evaluation.id]
            pe_evaluation_displayed_def_by_user.save
          end

          pe_evaluation_clone.pe_evaluation_displayed_trackings.each do |pe_evaluation_displayed_tracking_clone|
            pe_evaluation_displayed_tracking = pe_evaluation_displayed_tracking_clone.dup
            pe_evaluation_displayed_tracking.pe_evaluation = pe_evaluations_array[pe_evaluation_clone.id]
            pe_evaluation_displayed_tracking.pe_displayed_evaluation = pe_evaluations_array[pe_evaluation_displayed_tracking_clone.pe_displayed_evaluation.id]
            pe_evaluation_displayed_tracking.save
          end

        end

        pe_process_clone.pe_step_feedback_group2s.each do |pe_step_feedback_group2_clone|
          pe_step_feedback_group2 = pe_step_feedback_group2_clone.dup
          pe_step_feedback_group2.pe_process = @pe_process
          pe_step_feedback_group2.pe_group2 = pe_groups2_array[pe_step_feedback_group2_clone.pe_group2.id] if pe_step_feedback_group2_clone.pe_group2
          pe_step_feedback_group2.save
        end

        pe_process_clone.pe_process_reports_evaluations.each do |pe_process_reports_evaluation_clone|
          pe_process_reports_evaluation = pe_process_reports_evaluation_clone.dup
          pe_process_reports_evaluation.pe_process = @pe_process
          pe_process_reports_evaluation.pe_evaluation = pe_evaluations_array[pe_process_reports_evaluation_clone.pe_evaluation.id]
          pe_process_reports_evaluation.save
        end

      end

      #members
      if params[:clone_members] == '1'

        pe_process_clone.pe_areas.each do |pe_area_clone|
          pe_area = pe_area_clone.dup
          pe_area.pe_process = @pe_process

          pe_area.pe_box = pe_boxes_array[pe_area_clone.pe_box.id] if pe_area_clone.pe_box

          pe_area.save

          pe_areas_array[pe_area_clone.id] = pe_area

        end

        pe_process_clone.pe_members.each do |pe_member_clone|
          pe_member = pe_member_clone.dup
          pe_member.pe_process = @pe_process

          pe_member.pe_box = pe_boxes_array[pe_member_clone.pe_box.id] if pe_member_clone.pe_box

          pe_member.pe_box_before_calibration = pe_boxes_array[pe_member_clone.pe_box_before_calibration.id] if pe_member.pe_box_before_calibration


          pe_member.pe_area = pe_areas_array[pe_member_clone.pe_area.id] if pe_member_clone.pe_area

          pe_member.save

          pe_members_array[pe_member_clone.id] = pe_member

          pe_member_clone.pe_member_evaluations.each do |pe_member_evaluation_clone|

            pe_member_evaluation = pe_member_evaluation_clone.dup
            pe_member_evaluation.pe_process = @pe_process
            pe_member_evaluation.pe_member = pe_member
            pe_member_evaluation.pe_evaluation = pe_evaluations_array[pe_member_evaluation_clone.pe_evaluation.id]
            pe_member_evaluation.save

          end

          pe_member_clone.pe_member_groups.each do |pe_member_group_clone|
            pe_member_group = pe_member_group_clone.dup
            pe_member_group.pe_process = @pe_process
            pe_member_group.pe_member = pe_member
            pe_member_group.pe_evaluation = pe_evaluations_array[pe_member_group_clone.pe_evaluation.id]
            pe_member_group.pe_group = pe_groups_array[pe_member_group_clone.pe_group.id]
            pe_member_group.save
          end

          pe_member_clone.pe_member_observers.each do |pe_member_observer_clone|
            pe_member_observer = pe_member_observer_clone.dup
            pe_member_observer.pe_member_observed = pe_member
            pe_member_observer.pe_process = @pe_process
            pe_member_observer.save
          end

          pe_member_clone.pe_definition_by_user_validators.each do |pe_definition_by_user_validator_clone|
            pe_definition_by_user_validator = pe_definition_by_user_validator_clone.dup
            pe_definition_by_user_validator.pe_member = pe_member
            pe_definition_by_user_validator.pe_process = @pe_process
            pe_definition_by_user_validator.pe_evaluation = pe_evaluations_array[pe_definition_by_user_validator_clone.pe_evaluation.id]
            pe_definition_by_user_validator.save
          end

        end

        pe_process_clone.pe_member_rels.each do |pe_member_rel_clone|

          pe_member_rel = pe_member_rel_clone.dup
          pe_member_rel.pe_process = @pe_process
          pe_member_rel.pe_rel = pe_rels_array[pe_member_rel_clone.pe_rel.id]
          pe_member_rel.pe_member_evaluated = pe_members_array[pe_member_rel_clone.pe_member_evaluated.id]
          pe_member_rel.pe_member_evaluator = pe_members_array[pe_member_rel_clone.pe_member_evaluator.id]
          pe_member_rel.save

          pe_member_rels_array[pe_member_rel_clone.id] = pe_member_rel

        end

      end

      pe_process_clone.pe_member_observers.each do |pe_member_observer_clone|
        pe_member_observer = pe_member_observer_clone.dup
        pe_member_observer.pe_process = @pe_process
        pe_member_observer.pe_member_observed = pe_members_array[pe_member_observer_clone.pe_member_observed.id]
        pe_member_observer.save
      end


      #preguntas

      if params[:clone_questions] == '1'

        pe_process_clone.pe_evaluations.each do |pe_evaluation_clone|

          pe_evaluation_clone.pe_questions_by_question(nil).each do |pe_question_clone_1st_level_clone|

            pe_question1st_level = pe_question_clone_1st_level_clone.dup

            pe_question1st_level.pe_evaluation = pe_evaluations_array[pe_evaluation_clone.id]
            pe_question1st_level.pe_group = pe_groups_array[pe_question_clone_1st_level_clone.pe_group.id] if pe_question_clone_1st_level_clone.pe_group
            pe_question1st_level.pe_member_evaluator = pe_members_array[pe_question_clone_1st_level_clone.pe_member_evaluator.id] if pe_question_clone_1st_level_clone.pe_member_evaluator
            pe_question1st_level.pe_member_evaluated = pe_members_array[pe_question_clone_1st_level_clone.pe_member_evaluated.id] if pe_question_clone_1st_level_clone.pe_member_evaluated
            pe_question1st_level.pe_rel = pe_rels_array[pe_question_clone_1st_level_clone.pe_rel.id] if pe_question_clone_1st_level_clone.pe_rel
            pe_question1st_level.pe_element = pe_elements_array[pe_question_clone_1st_level_clone.pe_element.id] if pe_question_clone_1st_level_clone.pe_element
            pe_question1st_level.pe_question = nil
            pe_question1st_level.pe_alternative = nil
            pe_question1st_level.pe_question_model = pe_question_models_array[pe_question_clone_1st_level_clone.pe_question_model.id] if pe_question_clone_1st_level_clone.pe_question_model
            pe_question1st_level.save

            pe_questions_array[pe_question_clone_1st_level_clone.id] = pe_question1st_level

            pe_question_clone_1st_level_clone.pe_alternatives.each do |pe_alternative_clone|
              pe_alternative = pe_alternative_clone.dup
              pe_alternative.pe_question = pe_question1st_level
              pe_alternative.save

              pe_alternatives_array[pe_alternative_clone.id] = pe_alternative

            end

            pe_question1st_level.pe_alternative = pe_alternatives_array[pe_question_clone_1st_level_clone.pe_alternative.id] if pe_question_clone_1st_level_clone.pe_alternative
            pe_question1st_level.save

            pe_question_clone_1st_level_clone.pe_question_ranks.each do |pe_question_rank_clone|
              pe_question_rank = pe_question_rank_clone.dup
              pe_question_rank.pe_question = pe_question1st_level
              pe_question_rank.pe_question_rank_model = pe_question_rank_models_array[pe_question_rank_clone.pe_question_rank_model.id] if pe_question_rank_clone.pe_question_rank_model
              pe_question_rank.save
            end

            pe_question_clone_1st_level_clone.pe_question_comments.each do |pe_question_comment_clone|
              pe_question_comment = pe_question_comment_clone.dup
              pe_question_comment.pe_question = pe_question1st_level
              pe_question_comment.pe_member_rel = pe_member_rels_array[pe_question_comment_clone.pe_member_rel.id]
              pe_question_comment.save
            end

            pe_question_clone_1st_level_clone.pe_question_validations.each do |pe_question_validation_clone|
              pe_question_validation = pe_question_validation_clone.dup
              pe_question_validation.pe_question = pe_question1st_level
              pe_question_validation.pe_evaluation = pe_question1st_level.pe_evaluation
              pe_question_validation.save
            end

            pe_question_clone_1st_level_clone.pe_questions.each do |pe_question_clone_2nd_level_clone|

              pe_question2nd_level = pe_question_clone_2nd_level_clone.dup

              pe_question2nd_level.pe_evaluation = pe_evaluations_array[pe_evaluation_clone.id]
              pe_question2nd_level.pe_group = pe_groups_array[pe_question_clone_2nd_level_clone.pe_group.id] if pe_question_clone_2nd_level_clone.pe_group
              pe_question2nd_level.pe_member_evaluator = pe_members_array[pe_question_clone_2nd_level_clone.pe_member_evaluator.id] if pe_question_clone_2nd_level_clone.pe_member_evaluator
              pe_question2nd_level.pe_member_evaluated = pe_members_array[pe_question_clone_2nd_level_clone.pe_member_evaluated.id] if pe_question_clone_2nd_level_clone.pe_member_evaluated
              pe_question2nd_level.pe_rel = pe_rels_array[pe_question_clone_2nd_level_clone.pe_rel.id] if pe_question_clone_2nd_level_clone.pe_rel
              pe_question2nd_level.pe_element = pe_elements_array[pe_question_clone_2nd_level_clone.pe_element.id] if pe_question_clone_2nd_level_clone.pe_element
              pe_question2nd_level.pe_question = pe_question1st_level
              pe_question2nd_level.pe_alternative = nil
              pe_question2nd_level.pe_question_model = pe_question_models_array[pe_question_clone_2nd_level_clone.pe_question_model.id] if pe_question_clone_2nd_level_clone.pe_question_model
              pe_question2nd_level.save

              pe_questions_array[pe_question_clone_2nd_level_clone.id] = pe_question2nd_level

              pe_question_clone_2nd_level_clone.pe_alternatives.each do |pe_alternative_clone|
                pe_alternative = pe_alternative_clone.dup
                pe_alternative.pe_question = pe_question2nd_level
                pe_alternative.save

                pe_alternatives_array[pe_alternative_clone.id] = pe_alternative

              end

              pe_question2nd_level.pe_alternative = pe_alternatives_array[pe_question_clone_2nd_level_clone.pe_alternative.id] if pe_question_clone_2nd_level_clone.pe_alternative
              pe_question2nd_level.save

              pe_question_clone_2nd_level_clone.pe_question_ranks.each do |pe_question_rank_clone|
                pe_question_rank = pe_question_rank_clone.dup
                pe_question_rank.pe_question = pe_question2nd_level
                pe_question_rank.pe_question_rank_model = pe_question_rank_models_array[pe_question_rank_clone.pe_question_rank_model.id] if pe_question_rank_clone.pe_question_rank_model
                pe_question_rank.save
              end

              pe_question_clone_2nd_level_clone.pe_question_comments.each do |pe_question_comment_clone|
                pe_question_comment = pe_question_comment_clone.dup
                pe_question_comment.pe_question = pe_question2nd_level
                pe_question_comment.pe_member_rel = pe_member_rels_array[pe_question_comment_clone.pe_member_rel.id]
                pe_question_comment.save
              end

              pe_question_clone_2nd_level_clone.pe_question_validations.each do |pe_question_validation_clone|
                pe_question_validation = pe_question_validation_clone.dup
                pe_question_validation.pe_question = pe_question2nd_level
                pe_question_validation.pe_evaluation = pe_question2nd_level.pe_evaluation
                pe_question_validation.save
              end
              
              pe_question_clone_2nd_level_clone.pe_questions.each do |pe_question_clone_3rd_level_clone|

                pe_question3rd_level = pe_question_clone_3rd_level_clone.dup

                pe_question3rd_level.pe_evaluation = pe_evaluations_array[pe_evaluation_clone.id]
                pe_question3rd_level.pe_group = pe_groups_array[pe_question_clone_3rd_level_clone.pe_group.id] if pe_question_clone_3rd_level_clone.pe_group
                pe_question3rd_level.pe_member_evaluator = pe_members_array[pe_question_clone_3rd_level_clone.pe_member_evaluator.id] if pe_question_clone_3rd_level_clone.pe_member_evaluator
                pe_question3rd_level.pe_member_evaluated = pe_members_array[pe_question_clone_3rd_level_clone.pe_member_evaluated.id] if pe_question_clone_3rd_level_clone.pe_member_evaluated
                pe_question3rd_level.pe_rel = pe_rels_array[pe_question_clone_3rd_level_clone.pe_rel.id] if pe_question_clone_3rd_level_clone.pe_rel
                pe_question3rd_level.pe_element = pe_elements_array[pe_question_clone_3rd_level_clone.pe_element.id] if pe_question_clone_3rd_level_clone.pe_element
                pe_question3rd_level.pe_question = pe_question2nd_level
                pe_question3rd_level.pe_alternative = nil
                pe_question3rd_level.pe_question_model = pe_question_models_array[pe_question_clone_3rd_level_clone.pe_question_model.id] if pe_question_clone_3rd_level_clone.pe_question_model
                pe_question3rd_level.save

                pe_questions_array[pe_question_clone_3rd_level_clone.id] = pe_question3rd_level

                pe_question_clone_3rd_level_clone.pe_alternatives.each do |pe_alternative_clone|
                  pe_alternative = pe_alternative_clone.dup
                  pe_alternative.pe_question = pe_question3rd_level
                  pe_alternative.save

                  pe_alternatives_array[pe_alternative_clone.id] = pe_alternative

                end

                pe_question3rd_level.pe_alternative = pe_alternatives_array[pe_question_clone_3rd_level_clone.pe_alternative.id] if pe_question_clone_3rd_level_clone.pe_alternative
                pe_question3rd_level.save

                pe_question_clone_3rd_level_clone.pe_question_ranks.each do |pe_question_rank_clone|
                  pe_question_rank = pe_question_rank_clone.dup
                  pe_question_rank.pe_question = pe_question3rd_level
                  pe_question_rank.pe_question_rank_model = pe_question_rank_models_array[pe_question_rank_clone.pe_question_rank_model.id] if pe_question_rank_clone.pe_question_rank_model
                  pe_question_rank.save
                end

                pe_question_clone_3rd_level_clone.pe_question_comments.each do |pe_question_comment_clone|
                  pe_question_comment = pe_question_comment_clone.dup
                  pe_question_comment.pe_question = pe_question3rd_level
                  pe_question_comment.pe_member_rel = pe_member_rels_array[pe_question_comment_clone.pe_member_rel.id]
                  pe_question_comment.save
                end

                pe_question_clone_3rd_level_clone.pe_question_validations.each do |pe_question_validation_clone|
                  pe_question_validation = pe_question_validation_clone.dup
                  pe_question_validation.pe_question = pe_question3rd_level
                  pe_question_validation.pe_evaluation = pe_question3rd_level.pe_evaluation
                  pe_question_validation.save
                end

              end

            end

          end
        end

      end

      #resultados

      if params[:clone_assessments] == '1'

        pe_process_clone.pe_assessment_dimensions.each do |pe_assessment_dimension_clone|
          pe_assessment_dimension = pe_assessment_dimension_clone.dup
          pe_assessment_dimension.pe_process = @pe_process
          pe_assessment_dimension.pe_member = pe_members_array[pe_assessment_dimension_clone.pe_member.id]
          pe_assessment_dimension.pe_area = pe_areas_array[pe_assessment_dimension_clone.pe_area.id] if pe_assessment_dimension_clone.pe_area
          pe_assessment_dimension.pe_dimension = pe_dimensions_array[pe_assessment_dimension_clone.pe_dimension.id]
          pe_assessment_dimension.pe_dimension_group = pe_dimension_groups_array[pe_assessment_dimension_clone.pe_dimension_group.id] if pe_assessment_dimension_clone.pe_dimension_group
          pe_assessment_dimension.save

        end

        pe_process_clone.pe_assessment_final_evaluations.each do |pe_assessment_final_evaluation_clone|
          pe_assessment_final_evaluation = pe_assessment_final_evaluation_clone.dup
          pe_assessment_final_evaluation.pe_process = @pe_process
          pe_assessment_final_evaluation.pe_member = pe_members_array[pe_assessment_final_evaluation_clone.pe_member.id]
          pe_assessment_final_evaluation.pe_area = pe_areas_array[pe_assessment_final_evaluation_clone.pe_area.id] if pe_assessment_final_evaluation_clone.pe_area
          pe_assessment_final_evaluation.pe_evaluation = pe_evaluations_array[pe_assessment_final_evaluation_clone.pe_evaluation.id]
          pe_assessment_final_evaluation.pe_dimension_group = pe_dimension_groups_array[pe_assessment_final_evaluation_clone.pe_dimension_group.id] if pe_assessment_final_evaluation_clone.pe_dimension_group
          pe_assessment_final_evaluation.pe_original_dimension_group = pe_dimension_groups_array[pe_assessment_final_evaluation_clone.pe_original_dimension_group.id] if pe_assessment_final_evaluation_clone.pe_original_dimension_group
          pe_assessment_final_evaluation.save
        end

        pe_process_clone.pe_assessment_evaluations.each do |pe_assessment_evaluation_clone|
          pe_assessment_evaluation = pe_assessment_evaluation_clone.dup
          pe_assessment_evaluation.pe_process = @pe_process
          pe_assessment_evaluation.pe_evaluation = pe_evaluations_array[pe_assessment_evaluation_clone.pe_evaluation.id]
          pe_assessment_evaluation.pe_dimension_group = pe_dimension_groups_array[pe_assessment_evaluation_clone.pe_dimension_group.id] if pe_assessment_evaluation_clone.pe_dimension_group
          pe_assessment_evaluation.pe_member_rel = pe_member_rels_array[pe_assessment_evaluation_clone.pe_member_rel.id]
          pe_assessment_evaluation.save

          pe_assessment_evaluation_clone.pe_assessment_questions.where('pe_assessment_question_id IS NULL').each do |pe_assessment_question_1st_clone|
            pe_assessment_question_1st = pe_assessment_question_1st_clone.dup
            pe_assessment_question_1st.pe_assessment_evaluation = pe_assessment_evaluation
            pe_assessment_question_1st.pe_alternative = pe_alternatives_array[pe_assessment_question_1st_clone.pe_alternative.id] if pe_assessment_question_1st_clone.pe_alternative
            pe_assessment_question_1st.pe_question = pe_questions_array[pe_assessment_question_1st_clone.pe_question.id]
            pe_assessment_question_1st.pe_assessment_question = nil
            pe_assessment_question_1st.save

            pe_assessment_question_1st_clone.pe_assessment_questions.each do |pe_assessment_question_2nd_clone|

              pe_assessment_question_2nd = pe_assessment_question_2nd_clone.dup
              pe_assessment_question_2nd.pe_assessment_evaluation = pe_assessment_evaluation
              pe_assessment_question_2nd.pe_alternative = pe_alternatives_array[pe_assessment_question_2nd_clone.pe_alternative.id] if pe_assessment_question_2nd_clone.pe_alternative
              pe_assessment_question_2nd.pe_question = pe_questions_array[pe_assessment_question_2nd_clone.pe_question.id]
              pe_assessment_question_2nd.pe_assessment_question = pe_assessment_question_1st
              pe_assessment_question_2nd.save

              pe_assessment_question_2nd_clone.pe_assessment_questions.each do |pe_assessment_question_3rd_clone|

                pe_assessment_question_3rd = pe_assessment_question_3rd_clone.dup
                pe_assessment_question_3rd.pe_assessment_evaluation = pe_assessment_evaluation
                pe_assessment_question_3rd.pe_alternative = pe_alternatives_array[pe_assessment_question_3rd_clone.pe_alternative.id] if pe_assessment_question_3rd_clone.pe_alternative
                pe_assessment_question_3rd.pe_question = pe_questions_array[pe_assessment_question_3rd_clone.pe_question.id]
                pe_assessment_question_3rd.pe_assessment_question = pe_assessment_question_2nd
                pe_assessment_question_3rd.save
                
              end
              
            end

          end

        end

      else

        @pe_process.pe_members.each do |pe_member|
          pe_member.step_assessment = false
          pe_member.step_calibration = false
          pe_member.pe_box = nil
          pe_member.pe_box_before_cal = nil
          pe_member.save
        end

      end

      #comités de calibración

      if params[:clone_cal_comittee] == '1'

        pe_process_clone.pe_cal_sessions.each do |pe_cal_session_clone|
          pe_cal_session = pe_cal_session_clone.dup
          pe_cal_session.pe_process = @pe_process if pe_cal_session_clone.pe_process
          pe_cal_session.pe_evaluation = pe_evaluations_array[pe_cal_session_clone.pe_evaluation.id] if pe_cal_session_clone.pe_evaluation

          pe_cal_session.save

          pe_cal_session_clone.pe_cal_committees.each do |pe_cal_committee_clone|

            pe_cal_committee = pe_cal_committee_clone.dup
            pe_cal_committee.pe_process = @pe_process if pe_cal_committee_clone.pe_process
            pe_cal_committee.pe_cal_session = pe_cal_session
            pe_cal_committee.save

          end

          pe_cal_session_clone.pe_cal_members.each do |pe_cal_member_clone|
            pe_cal_member = pe_cal_member_clone.dup
            pe_cal_member.pe_cal_session = pe_cal_session
            pe_cal_member.pe_process = @pe_process if pe_cal_member_clone.pe_process
            pe_cal_member.pe_member = pe_members_array[pe_cal_member_clone.pe_member.id]
            pe_cal_member.save

          end

        end

        pe_process_clone.pe_evaluations.each do |pe_evaluation_clone|
          pe_evaluation_clone.pe_cal_sessions.each do |pe_cal_session_clone|

            pe_cal_session = pe_cal_session_clone.dup
            pe_cal_session.pe_process = @pe_process if pe_cal_session_clone.pe_process
            pe_cal_session.pe_evaluation = pe_evaluations_array[pe_cal_session_clone.pe_evaluation.id] if pe_cal_session_clone.pe_evaluation

            pe_cal_session.save

            pe_cal_session_clone.pe_cal_committees.each do |pe_cal_committee_clone|

              pe_cal_committee = pe_cal_committee_clone.dup
              pe_cal_committee.pe_process = @pe_process if pe_cal_committee_clone.pe_process
              pe_cal_committee.pe_cal_session = pe_cal_session
              pe_cal_committee.save

            end

            pe_cal_session_clone.pe_cal_members.each do |pe_cal_member_clone|
              pe_cal_member = pe_cal_member_clone.dup
              pe_cal_member.pe_cal_session = pe_cal_session
              pe_cal_member.pe_process = @pe_process if pe_cal_member_clone.pe_process
              pe_cal_member.pe_member = pe_members_array[pe_cal_member_clone.pe_member.id]
              pe_cal_member.save

            end

          end
        end

      end

      #retro structure

      if params[:clone_feedback_structure] == '1'

        pe_process_clone.pe_feedback_field_lists.each do |pe_feedback_field_list_clone|
          pe_feedback_field_list = pe_feedback_field_list_clone.dup
          pe_feedback_field_list.pe_process = @pe_process
          pe_feedback_field_list.save

          pe_feedback_field_list_array[pe_feedback_field_list_clone.id] = pe_feedback_field_list

          pe_feedback_field_list_clone.pe_feedback_field_list_items.each do |pe_feedback_field_list_item_clone|
            pe_feedback_field_list_item = pe_feedback_field_list_item_clone.dup
            pe_feedback_field_list_item.pe_feedback_field_list = pe_feedback_field_list
            pe_feedback_field_list_item.save

            pe_feedback_field_list_item_array[pe_feedback_field_list_item_clone.id] = pe_feedback_field_list_item

          end

        end

        pe_process_clone.pe_feedback_fields.each do |pe_feedback_field_clone|
          pe_feedback_field = pe_feedback_field_clone.dup
          pe_feedback_field.pe_process = @pe_process
          pe_feedback_field.pe_feedback_field_list = pe_feedback_field_list_array[pe_feedback_field_clone.pe_feedback_field_list.id] if pe_feedback_field_clone.pe_feedback_field_list
          pe_feedback_field.save

          pe_feedback_field_array[pe_feedback_field_clone.id] = pe_feedback_field

          pe_feedback_field_clone.pe_feedback_compound_fields.each do |pe_feedback_compound_field_clone|
            pe_feedback_compound_field = pe_feedback_compound_field.dup
            pe_feedback_compound_field.pe_feedback_field = pe_feedback_field
            pe_feedback_compound_field.pe_feedback_field_list = pe_feedback_field_list_array[pe_feedback_compound_field_clone.pe_feedback_field_list.id] if pe_feedback_compound_field_clone.pe_feedback_field_list
            pe_feedback_compound_field.save

            pe_feedback_compound_field_array[pe_feedback_compound_field_clone.id] = pe_feedback_compound_field

          end

        end

        pe_process_clone.pe_feedback_accepted_fields.each do |pe_feedback_accepted_field_clone|
          pe_feedback_accepted_field = pe_feedback_accepted_field_clone.dup
          pe_feedback_accepted_field.pe_process = @pe_process
          pe_feedback_accepted_field.pe_feedback_field_list = pe_feedback_field_list_array[pe_feedback_accepted_field_clone.pe_feedback_field_list.id] if pe_feedback_accepted_field_clone.pe_feedback_field_list
          pe_feedback_accepted_field.save

          pe_feedback_accepted_field_array[pe_feedback_accepted_field_clone.id] = pe_feedback_accepted_field

        end

      end

      #retro results

      if params[:clone_feedback_results] == '1'

        pe_process_clone.pe_members.each do |pe_member_clone|

          pe_member_clone.pe_member_feedbacks.each do |pe_member_feedback_clone|
            pe_member_feedback = pe_member_feedback_clone.dup
            pe_member_feedback.pe_member = pe_members_array[pe_member_feedback_clone.pe_member.id]
            pe_member_feedback.pe_process = @pe_process
            pe_member_feedback.pe_feedback_field = pe_feedback_field_array[pe_member_feedback_clone.pe_feedback_field.id]
            pe_member_feedback.pe_feedback_field_list_item = pe_feedback_field_list_item_array[pe_member_feedback_clone.pe_feedback_field_list_item.id] if pe_member_feedback_clone.pe_feedback_field_list_item
            pe_member_feedback.save

            pe_member_feedback_clone.pe_member_compound_feedbacks.each do |pe_member_compound_feedback_clone|
              pe_member_compound_feedback = pe_member_compound_feedback_clone.dup
              pe_member_compound_feedback.pe_member_feedback = pe_member_feedback
              pe_member_compound_feedback.pe_feedback_compound_field = pe_feedback_compound_field_array[pe_member_compound_feedback_clone.pe_feedback_compound_field.id] if pe_member_compound_feedback_clone.pe_feedback_compound_field
              pe_member_compound_feedback.pe_feedback_field_list_item = pe_feedback_field_list_item_array[pe_member_compound_feedback_clone.pe_feedback_field_list_item.id] if pe_member_compound_feedback_clone.pe_feedback_field_list_item
              pe_member_compound_feedback.save

            end

          end

          pe_member_clone.pe_member_accepted_feedbacks.each do |pe_member_accepted_feedback_clone|
            pe_member_accepted_feedback = pe_member_accepted_feedback_clone.dup
            pe_member_accepted_feedback.pe_member = pe_members_array[pe_member_accepted_feedback_clone.pe_member.id]
            pe_member_accepted_feedback.pe_process = @pe_process
            pe_member_accepted_feedback.pe_feedback_accepted_field = pe_feedback_accepted_field_array[pe_member_accepted_feedback_clone.pe_feedback_accepted_field.id]
            pe_member_accepted_feedback.pe_feedback_field_list_item = pe_feedback_field_list_item_array[pe_member_accepted_feedback_clone.pe_feedback_field_list_item.id] if pe_member_accepted_feedback_clone.pe_feedback_field_list_item
            pe_member_accepted_feedback.save
          end

        end

      else

        @pe_process.pe_members.each do |pe_member|
          pe_member.step_feedback = false
          pe_member.step_feedback_accepted = false
          pe_member.save
        end



      end

      #config reports

      if params[:clone_config_reports] == '1'



      end

      flash[:success] = t('activerecord.success.model.pe_process.create_ok')
      redirect_to @pe_process
    else
      render action: 'new_clone'
    end

  end

  def edit
    @pe_process = PeProcess.find(params[:id])
  end

  def update
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to @pe_process
    else
      render action: 'edit'
    end

  end

  def edit_steps
    @pe_process = PeProcess.find(params[:id])
  end

  def update_steps
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])


      @pe_process.pe_group2s.each do |pe_group2|

        pe_step_feedback_group2 = @pe_process.pe_step_feedback_group2s.where('pe_group2_id = ?', pe_group2.id).first

        if params['step_feedback_group2_'+pe_group2.id.to_s+'_from_date'].blank? || params['step_feedback_group2_'+pe_group2.id.to_s+'_to_date'].blank?

          pe_step_feedback_group2.destroy if pe_step_feedback_group2

        elsif !params['step_feedback_group2_'+pe_group2.id.to_s+'_from_date'].blank? && !params['step_feedback_group2_'+pe_group2.id.to_s+'_to_date'].blank?

          unless pe_step_feedback_group2

            pe_step_feedback_group2 = @pe_process.pe_step_feedback_group2s.build
            pe_step_feedback_group2.pe_group2 = pe_group2

          end

          tmp_d = params['step_feedback_group2_'+pe_group2.id.to_s+'_from_date'].split('/')
          pe_step_feedback_group2.from_date = DateTime.new(tmp_d[2].to_i,tmp_d[1].to_i, tmp_d[0].to_i,0,0,0)
          tmp_d = params['step_feedback_group2_'+pe_group2.id.to_s+'_to_date'].split('/')
          pe_step_feedback_group2.to_date = DateTime.new(tmp_d[2].to_i,tmp_d[1].to_i, tmp_d[0].to_i,0,0,0)

          pe_step_feedback_group2.save

        end

      end


      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'steps'
    else
      render action: 'edit_steps'
    end

  end

  def edit_notifications
    @pe_process = PeProcess.find(params[:id])
  end

  def update_notifications
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'
    else
      render action: 'edit_notifications'
    end

  end

  def edit_notifications_q_defs
    @pe_process = PeProcess.find(params[:id])


  end

  def update_notifications_q_defs
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'
    else
      render action: 'edit_notifications_q_defs'
    end

  end

  def edit_notifications_q_defs_boss
    @pe_process = PeProcess.find(params[:id])


  end

  def update_notifications_q_defs_boss
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'
    else
      render action: 'edit_notifications_q_defs_boss'
    end

  end

  def edit_notifications_def_val
    @pe_process = PeProcess.find(params[:id])

    @before_accepted = params[:before_accepted]
    @num_step = params[:num_step]

    @pe_process_notification_def_val = @pe_process.pe_process_notification_def_val(@before_accepted, @num_step)
    unless @pe_process_notification_def_val
      @pe_process_notification_def_val = @pe_process.pe_process_notification_def_vals.build
    end

  end

  def update_notifications_def_val
    @pe_process = PeProcess.find(params[:id])

    @before_accepted = params[:before_accepted]
    @num_step = params[:num_step]

    @pe_process_notification_def_val = @pe_process.pe_process_notification_def_val(@before_accepted, @num_step)
    unless @pe_process_notification_def_val
      @pe_process_notification_def_val = @pe_process.pe_process_notification_def_vals.build
      @pe_process_notification_def_val.before_accepted = @before_accepted
      @pe_process_notification_def_val.num_step = @num_step
    end

    @pe_process_notification_def_val.send_email_when_accept = params[:pe_process_notification_def_val][:send_email_when_accept]
    @pe_process_notification_def_val.accepted_subject = params[:pe_process_notification_def_val][:accepted_subject]
    @pe_process_notification_def_val.accepted_body = params[:pe_process_notification_def_val][:accepted_body]

    @pe_process_notification_def_val.send_email_when_reject = params[:pe_process_notification_def_val][:send_email_when_reject]
    @pe_process_notification_def_val.rejected_subject = params[:pe_process_notification_def_val][:rejected_subject]
    @pe_process_notification_def_val.rejected_body = params[:pe_process_notification_def_val][:rejected_body]

    if @pe_process_notification_def_val.save

      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'

    else

      render action: 'edit_notifications_def_val'

    end

  end

  def edit_notifications_feedback_accepted
    @pe_process = PeProcess.find(params[:id])


  end

  def update_notifications_feedback_accepted
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'
    else
      render action: 'edit_notifications_feedback_accepted'
    end

  end

  def edit_notifications_feedback_accepted_to_boss
    @pe_process = PeProcess.find(params[:id])


  end

  def update_notifications_feedback_accepted_to_boss
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'
    else
      render action: 'edit_notifications_feedback_accepted'
    end

  end

  def edit_notifications_validation
    @pe_process = PeProcess.find(params[:id])
  end

  def update_notifications_validation
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'notifications'
    else
      render action: 'edit_notifications_validation'
    end

  end
  
  def edit_display_results
    @pe_process = PeProcess.find(params[:id])
  end

  def update_display_results
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'display_results'
    else
      render action: 'edit_display_results'
    end

  end

  def edit_step_img
    @pe_process = PeProcess.find(params[:id])
    @step = params[:step]
  end

  def update_step_img
    @pe_process = PeProcess.find(params[:id])
    @step = params[:step]
   #@company = session[:company]

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext != '.jpg' && ext != '.jpeg'

        flash[:danger] = t('activerecord.error.model.pe_process.update_step_img_wrong_format')
        redirect_to pe_process_edit_step_img_path @pe_process, @step

      else

        file_name_old = nil
        file_name = ''

        directory = @company.directorio+'/'+@company.codigo+'/pe_processes/'+@pe_process.id.to_s+'/steps/'

        if @step == 'dbu'

          file_name_old = @pe_process.step_definition_by_users_img
          @pe_process.step_definition_by_users_img = 'dbu_'+SecureRandom.hex(5)+'.jpg'
          file_name = @pe_process.step_definition_by_users_img

        elsif @step == 'tracking'

          file_name_old = @pe_process.step_tracking_img
          @pe_process.step_tracking_img = 'tracking_'+SecureRandom.hex(5)+'.jpg'
          file_name = @pe_process.step_tracking_img

        elsif @step == 'assessment'

          file_name_old = @pe_process.step_assessment_img
          @pe_process.step_assessment_img = 'assessment_'+SecureRandom.hex(5)+'.jpg'
          file_name = @pe_process.step_assessment_img

        end

        @pe_process.save
        FileUtils.remove_file(directory+file_name_old) if file_name_old && File.file?(directory+file_name_old)

        save_step_img_file params[:upload], file_name, @pe_process, @company

        flash[:success] = t('activerecord.success.model.pe_process.update_step_img_ok')

        redirect_to pe_process_tab_path @pe_process, 'steps'

      end

    else
      flash[:danger] = t('activerecord.error.model.pe_process.update_step_img_no_file')
      redirect_to pe_process_edit_step_img_path @pe_process, @step

    end

  end

  def delete_step_img

    @pe_process = PeProcess.find(params[:id])
    @step = params[:step]
   company = @company

    directory = company.directorio+'/'+company.codigo+'/pe_processes/'+@pe_process.id.to_s+'/steps/'

    if @step == 'dbu'

      if @pe_process.step_definition_by_users_img

        file_name = @pe_process.step_definition_by_users_img
        @pe_process.step_definition_by_users_img = nil

      end

    elsif @step == 'tracking'

      if @pe_process.step_tracking_img

        file_name = @pe_process.step_tracking_img
        @pe_process.step_tracking_img = nil

      end


    elsif @step == 'assessment'

      if @pe_process.step_assessment_img

        file_name = @pe_process.step_assessment_img
        @pe_process.step_assessment_img = nil

      end

    end

    @pe_process.save

    FileUtils.remove_file directory+file_name if File.file? directory+file_name

    flash[:success] = t('activerecord.success.model.pe_process.delete_step_img_ok')

    redirect_to pe_process_tab_path @pe_process, 'steps'

  end

  def edit_feedback
    @pe_process = PeProcess.find(params[:id])
  end

  def update_feedback
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'feedback'
    else
      render action: 'edit_feedback'
    end

  end

  def edit_feedback_accepted
    @pe_process = PeProcess.find(params[:id])
  end

  def update_feedback_accepted
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'feedback_accepted'
    else
      render action: 'edit_feedback_accepted'
    end

  end

  def edit_active_reportes
    @pe_process = PeProcess.find(params[:id])
  end

  def update_active_reports
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'reports'
    else
      render action: 'edit_active_reportes'
    end

  end

  def edit_rep_detailed_final_results
    @pe_process = PeProcess.find(params[:id])
  end

  def update_rep_detailed_final_results
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'reports'
    else
      render action: 'edit_rep_detailed_final_results'
    end

  end

  def edit_rep_detailed_final_results_auto
    @pe_process = PeProcess.find(params[:id])
  end

  def update_rep_detailed_final_results_auto
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])
      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'reports'
    else
      render action: 'edit_rep_detailed_final_results_auto'
    end

  end

  def edit_manage_options
    @pe_process = PeProcess.find(params[:id])
  end

  def update_manage_options
    @pe_process = PeProcess.find(params[:id])

    if @pe_process.update_attributes(params[:pe_process])

      @pe_process.pe_characteristics.each do |pe_characteristic|

        if params['cha_'+pe_characteristic.id.to_s+'_active']

          pe_characteristic.active_segmentation = true

        else

          pe_characteristic.active_segmentation = false

        end

        pe_characteristic.save

      end


      flash[:success] = t('activerecord.success.model.pe_process.update_ok')
      redirect_to pe_process_tab_path @pe_process, 'managers'
    else
      render action: 'edit_manage_options'
    end
  end

  def re_calculate_assessments

    @pe_process = PeProcess.find(params[:id])

    @pe_process.pe_member_rels.each do |pe_member_rel|

      #if pe_member_rel.pe_rel.rel == 1 #&& pe_member_rel.pe_member_evaluated.id == 299

        @pe_member_rel = pe_member_rel

        @pe_rel = @pe_member_rel.pe_rel
        @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
        @pe_member = @pe_member_evaluated
        @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

       #@company = session[:company]

        @pe_process.pe_evaluations.each do |pe_evaluation|

          @pe_evaluation = pe_evaluation

          @pe_member_group = @pe_member_evaluated.pe_member_group(@pe_evaluation)

          pe_assessment_evaluation = @pe_member_rel.pe_assessment_evaluation @pe_evaluation

          if pe_assessment_evaluation

            #re_calculate_answer_evaluation_1_element pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 1

            #re_calculate_answer_evaluation_2_elements pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 2

            #re_calculate_answer_evaluation_3_elements pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 3

            #if pe_assessment_evaluation.pe_assessment_questions.size == pe_evaluation.pe_questions.where('pe_member_evaluated_id = ?', @pe_member_evaluated.id).size

            #if pe_assessment_evaluation.pe_assessment_questions.size == pe_evaluation.pe_questions.where('pe_group_id = ?', @pe_member_group.pe_group.id ).size

              calculate_assessment_evaluation pe_assessment_evaluation

              calculate_assessment_final_evaluation @pe_member_evaluated, pe_evaluation, false

              calculate_assessment_dimension @pe_member_evaluated, pe_evaluation.pe_dimension



            #end

          end

        end
        @pe_member_evaluated.reload
        @pe_member_evaluated.set_pe_box
        @pe_member_evaluated.save

        calculate_assessment_groups @pe_member_evaluated

      #end



    end

  end

  # DELETE /pe_processes/1
  # DELETE /pe_processes/1.json
  def destroy
    @pe_process = PeProcess.find(params[:id])
    @pe_process.destroy

    respond_to do |format|
      format.html { redirect_to pe_processes_url }
      format.json { head :no_content }
    end
  end

  private

  def save_step_img_file(upload, name, pe_process, company)

    directory = company.directorio+'/'+company.codigo+'/pe_processes/'+pe_process.id.to_s+'/steps/'

    FileUtils.mkdir_p directory

    path = File.join(directory, name)
    File.open(path, 'wb') { |f| f.write(upload['datafile'].read) }


  end

end
