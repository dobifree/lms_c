class NodesController < ApplicationController

  include UsersHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @root_nodes = Node.root_nodes
  end

  def download_hierarchy

    @users = User.all

    filename = 'jerarquía_nodos_'+$current_domain+'_'+(localize(lms_time, format: :full_date_time_for_download))+'.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  def carga_masiva_nodes

  end

  def carga_masiva_nodes_upload

    if params[:upload]

     company = @company

      if params[:reemplazar_todo][:yes] == '1'

        Node.delete_all
        User.update_all(node_id: nil)

      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')
          datos[1].gsub!("\n", '')

          #0 usuario_jefe_directo
          #1 nodo

          user = User.find_by_codigo datos[0]

          if user

            node = user.owned_nodes.first

            node = user.owned_nodes.build unless node

            node.nombre = datos[1]

            if node.save

              Characteristic.where('data_type_id = ?', 13).each do |c|

                user.update_user_characteristic(c, datos[1], lms_time, nil, nil, admin_connected, admin_connected, company)

              end

            else

              @lineas_error.push num_linea

              @lineas_error_detalle.push ('codigo_usuario: '+datos[0]+'; nodo_que_posee: '+datos[1]).force_encoding('UTF-8')
              @lineas_error_messages.push node.errors.full_messages

            end

          else

            @lineas_error.push num_linea
            @lineas_error_detalle.push ('codigo_usuario: '+datos[0]+'; nodo_que_posee: '+datos[1]).force_encoding('UTF-8')

            @lineas_error_messages.push [datos[0]+' no existe']

          end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.user.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_error') if @lineas_error.length > 0

      render 'carga_masiva_nodes'

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

      render 'carga_masiva_nodes'

    end

  end

  def carga_masiva

  end

  def carga_masiva_upload

    if params[:upload]

      if params[:reemplazar_todo][:yes] == '1'

        User.update_all(node_id: nil)
        Node.update_all(node_id: nil)

      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')
          datos[1].gsub!("\n", '')

          #0 usuario_jefe_directo
          #1 usuario_subordinado_directo

          user_jefe_directo = User.find_by_codigo datos[0]
          user_sub_directo = User.find_by_codigo datos[1]

          if user_jefe_directo && user_sub_directo

            unless user_jefe_directo.owned_nodes.first

              node = user_jefe_directo.owned_nodes.build
              node.nombre = ''
              unless node.save

                @lineas_error.push num_linea

                @lineas_error_detalle.push ('codigo_jefe_directo: '+datos[0]+'; codigo_sub_directo: '+datos[1]).force_encoding('UTF-8')
                @lineas_error_messages.push node.errors.full_messages

              end

            end

            user_sub_directo.node_to_which_belongs = user_jefe_directo.owned_nodes.first

            unless user_sub_directo.save

              @lineas_error.push num_linea

              @lineas_error_detalle.push ('codigo_jefe_directo: '+datos[0]+'; codigo_sub_directo: '+datos[1]).force_encoding('UTF-8')
              @lineas_error_messages.push user_sub_directo.errors.full_messages

            end


          else

            @lineas_error.push num_linea
            @lineas_error_detalle.push ('codigo_jefe_directo: '+datos[0]+'; codigo_sub_directo: '+datos[1]).force_encoding('UTF-8')

            @lineas_error_messages.push [datos[0]+' no existe'] unless user_jefe_directo
            @lineas_error_messages.push [datos[1]+' no existe'] unless user_sub_directo

          end




        end

        num_linea += 1

      end

      nodes = Node.all
      nodes.each do |node|
        node.node = node.owner.node_to_which_belongs
        node.save
      end

      error_ciclico = false


      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')
          datos[1].gsub!("\n", '')

          #0 usuario_jefe_directo
          #1 usuario_subordinado_directo

          user_jefe_directo = User.find_by_codigo datos[0]
          user_sub_directo = User.find_by_codigo datos[1]

          if user_jefe_directo && user_sub_directo

            ciclic = false
=begin
            n = user_jefe_directo.node_to_which_belongs

            while n

              if n.owner.id == user_sub_directo.id
                ciclic = true
                break
              else
                n = n.node
              end

            end

            ciclic = true
=end

            if user_jefe_directo.id == user_sub_directo.id || ciclic

              @lineas_error.push num_linea
              @lineas_error_detalle.push ('codigo_jefe_directo: '+datos[0]+'; codigo_sub_directo: '+datos[1]).force_encoding('UTF-8')

              @lineas_error_messages.push ['relación cíclica']

              error_ciclico = true

            end

          end

        end

        num_linea += 1

      end

      if error_ciclico

        User.update_all(node_id: nil)
        Node.update_all(node_id: nil)

      end

      flash.now[:success] = t('activerecord.success.model.user.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_error') if @lineas_error.length > 0

      render 'carga_masiva'

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

      render 'carga_masiva'

    end



  end


  def show

    @node = Node.find(params[:id])
    @user = @node.owner

    @user_search = User.new(params[:user])


    @users_search = search_normal_users(@user_search.apellidos, @user_search.nombre)

  end

  def new
    @node = Node.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @node }
    end
  end




  def create
    @node = Node.new(params[:node])

    respond_to do |format|
      if @node.save
        format.html { redirect_to @node, notice: 'Node was successfully created.' }
        format.json { render json: @node, status: :created, location: @node }
      else
        format.html { render action: "new" }
        format.json { render json: @node.errors, status: :unprocessable_entity }
      end
    end
  end



  def edit
    @node = Node.find(params[:id])
    @user = @node.owner
  end


  def update
    @node = Node.find(params[:id])


    if @node.update_attributes(params[:node])
      redirect_to @node
    else
      render action: 'edit'
    end

  end

  def unlink

    user = User.find(params[:user_id])

    node_parent = user.node_to_which_belongs

    node = user.owned_nodes.first
    node.node_id = nil
    node.save

    user.node_id = nil
    user.save

    redirect_to node_parent

  end

  def link

    node_parent = Node.find(params[:node_id])
    user = User.find(params[:user_id])

    unless user.owned_nodes.first

      owned_node = user.owned_nodes.build
      owned_node.nombre = ''
      owned_node.save

    end

    user.node_to_which_belongs = node_parent
    user.save

    owned_node.node = node_parent
    owned_node.save

    redirect_to node_parent

  end

  # DELETE /nodes/1
  # DELETE /nodes/1.json
  def destroy
    @node = Node.find(params[:id])
    @node.destroy

    respond_to do |format|
      format.html { redirect_to nodes_url }
      format.json { head :no_content }
    end
  end
end
