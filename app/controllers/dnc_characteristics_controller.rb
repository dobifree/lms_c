class DncCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @dnc_characteristics = DncCharacteristic.all
  end

  def show
    @dnc_characteristic = DncCharacteristic.find(params[:id])
  end

  def new
    @dnc_characteristic = DncCharacteristic.new
  end

  def edit
    @dnc_characteristic = DncCharacteristic.find(params[:id])
  end

  def create
    @dnc_characteristic = DncCharacteristic.new(params[:dnc_characteristic])

    if @dnc_characteristic.save
      redirect_to dnc_characteristics_path, notice: 'Dnc characteristic was successfully created.'
    else
      render action: 'new'
    end

  end

  # PUT /dnc_characteristics/1
  # PUT /dnc_characteristics/1.json
  def update
    @dnc_characteristic = DncCharacteristic.find(params[:id])

    respond_to do |format|
      if @dnc_characteristic.update_attributes(params[:dnc_characteristic])
        format.html { redirect_to @dnc_characteristic, notice: 'Dnc characteristic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dnc_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dnc_characteristics/1
  # DELETE /dnc_characteristics/1.json
  def destroy
    @dnc_characteristic = DncCharacteristic.find(params[:id])
    @dnc_characteristic.destroy

    redirect_to dnc_characteristics_url

  end

end
