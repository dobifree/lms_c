class FolderManagingController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_jefe_de_biblioteca
  before_filter :verify_access_download_folder_file, only: [:download_file]
  before_filter :verify_destroy_empty_folder, only: [:destroy]
  before_filter :verify_destroy_empty_folder_file, only: [:destroy_folder_file]

  def index

    if params[:id]

      #@folder = Folder.find(params[:id])
      @public = @folder.public?

      unless @public
        get_folder_ancestros(@folder).each do |ancentro|

          if ancentro.public?
            @public = true
            break
          end

          break if @public

        end
      end

      @folders = @folder.hijos
    else

      @folder = nil
      @folders = Folder.find_all_by_padre_id(nil)

    end

    @folder_search = Folder.new

  end

  def reporte_uso
    @folder_files = FolderFile.all
  end

  def search



    @folders = Folder.where('nombre LIKE ?', '%'+params[:search][:search]+'%')
    @folder_files = FolderFile.where(:is_last => true).where('nombre LIKE ? OR descripcion LIKE ?', '%'+params[:search][:search]+'%', '%'+params[:search][:search]+'%')

    @folder_search = Folder.new


  end


  def show


    #@folders = Folder.find_all_by_padre_id(@folder)


  end


  def new
    @folder = Folder.new

    if params[:id]
      @folder.padre = Folder.find(params[:id])
    end

  end

  def new_owner

    @folder = Folder.find(params[:id])
    @user = User.new
    @users = {}

    if params[:user]

      if !params[:user][:codigo].blank? || !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')


      end

    end

  end

  def new_reader

    @folder = Folder.find(params[:id])
    @user = User.new
    @users = {}

    if params[:user]

      if !params[:user][:codigo].blank? || !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')


      end

    end

  end

  def new_member

    @folder = Folder.find(params[:id])
    if admin_logged_in?
      @characteristics = Characteristic.joins(:lib_characteristic).all
    else
      @characteristics = Characteristic.joins(:lib_characteristic).find_all_by_publica true
    end


  end

  def show_file

   #@company = session[:company]
    @folder_file = FolderFile.find(params[:folder_file_id])

    @folder_file_comment = FolderFileComment.new if @company.accepts_comments?
    @folder_file_value = FolderFileValue.new if @company.accepts_values?

    #create_file_log(nil, :ingreso)

  end

  def new_file

    #@folder = Folder.find(params[:id])
    @folder_file = FolderFile.new
    @folder_file.folder = @folder

  end


=begin
  def replace_file

    @folder = Folder.find(params[:id])
    @folder_file_father = FolderFile.find(params[:folder_file_father_id])
    @folder_file = FolderFile.new
    @folder_file.folder = @folder
    @folder_file.folder_file_father = @folder_file_father
    @folder_file.nombre = @folder_file_father.nombre
    @folder_file.descripcion = @folder_file_father.descripcion

  end
=end



  def edit

    #@folder = Folder.find(params[:id])

  end

  def edit_members

    @folder = Folder.find(params[:id])
    @characteristics = Characteristic.joins(:lib_characteristic).all

    @characteristics_prev = {}

    @characteristics.each do |characteristic|

      @characteristics_prev[characteristic.id] = []

      @folder.folder_members.find_all_by_characteristic_id(characteristic).each do |member|

        @characteristics_prev[characteristic.id].push member.valor

      end


    end

  end

  def edit_file


  end


  def create


    @folder = Folder.new(params[:folder])
    @folder.creador = user_connected if normal_user_logged_in?


    if @folder.save
      flash[:success] = t('activerecord.success.model.folder.create_ok')
      if @folder.padre
        redirect_to folder_managing_folder_path(@folder.padre)
      else
        redirect_to folder_managing_folders_path
      end

    else
      render action: 'new'
    end

  end

  def create_owner

    @folder = Folder.find(params[:id])
    folder_owner = @folder.folder_owners.build
    folder_owner.user = User.find(params[:user][:id])
    folder_owner.save

    redirect_to folder_managing_folder_path(@folder)

  end

  def create_reader

    @folder = Folder.find(params[:id])
    folder_reader = @folder.folder_readers.build
    folder_reader.user = User.find(params[:user][:id])
    folder_reader.save

    redirect_to folder_managing_folder_path(@folder)

  end

  def create_member

    @folder = Folder.find(params[:id])

    @characteristics = Characteristic.joins(:lib_characteristic).all

    @characteristics.each do |characteristic|

      if params[:characteristic] && params[:characteristic][characteristic.id.to_s.to_sym] && params[:characteristic][characteristic.id.to_s.to_sym] != ''

        fm = @folder.folder_members.where('characteristic_id = ? AND valor = ?', characteristic.id, params[:characteristic][characteristic.id.to_s.to_sym]).first

        unless fm

          fm = FolderMember.new
          fm.folder = @folder
          fm.characteristic = characteristic
          fm.valor = params[:characteristic][characteristic.id.to_s.to_sym]

          fm.save

        end

      end

    end

    redirect_to folder_managing_folder_path(@folder)

  end


  def create_file

    #@folder = Folder.find(params[:id])
   company = @company

    @folder_file = FolderFile.new
    @folder_file.folder = @folder

    if params[:folder_file][:folder_file_father_id]
      @folder_file_father = FolderFile.find(params[:folder_file][:folder_file_father_id])
      @folder_file.folder_file_father = @folder_file_father
      @folder_file.valor = @folder_file_father.valor
    end

    @folder_file.nombre = params[:folder_file][:nombre]
    @folder_file.descripcion = params[:folder_file][:descripcion]

    @folder_file.user = user_connected if normal_user_logged_in?

    if params[:upload]

      if File.size(params[:upload]['datafile'].tempfile) > 200.megabytes

        @folder_file.errors.add(:archivo, t('activerecord.errors.models.folder_file.attributes.archivo.wrong_size'))
        render 'new_file'
      else

        @folder_file.extension = File.extname(params[:upload]['datafile'].original_filename)
        @folder_file.extension.slice! 0
        @folder_file.descargable = false if ['pdf', 'mp4'].include?(@folder_file.extension)

        @folder_file.size = File.size params[:upload]['datafile'].tempfile

        if @folder_file.save

          save_file params[:upload], @folder_file, company

          if @folder_file_father
            @folder_file_father.update_attributes(:is_last => false)
          end

          flash[:success] = t('activerecord.success.model.folder_file.create_ok')
          redirect_to folder_managing_folder_path(@folder)
        else
          if @folder_file_father
            render 'replace_file'
          else
            render 'new_file'
          end

        end
      end
    else
      @folder_file.errors.add(:archivo, t('activerecord.errors.models.folder_file.attributes.archivo.blank'))
      render 'new_file'
    end

  end

  def download_file

    #create_file_log(nil, :descarga)
   company = @company
    directory = company.directorio+'/'+company.codigo+'/mediateca/'

    send_file directory+@folder_file.crypted_name+'.'+@folder_file.extension,
              filename: @folder_file.nombre+'.'+@folder_file.extension,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end


  def update

    #@folder = Folder.find(params[:id])

    if @folder.update_attributes(params[:folder])
      flash[:success] = t('activerecord.success.model.folder.update_ok')
      redirect_to folder_managing_folder_path(@folder)

    else
      render action: 'edit'
    end

  end

  def update_members

    @folder = Folder.find(params[:id])

    @folder.folder_members.destroy_all

    @characteristics = Characteristic.joins(:lib_characteristic).all

    @characteristics.each do |characteristic|


      params[:characteristic][characteristic.id.to_s.to_sym].each do |valor|

        if valor != ''

          fm = FolderMember.new
          fm.folder = @folder
          fm.characteristic = characteristic
          fm.valor = valor

          fm.save

        end

      end


    end


    redirect_to folder_managing_folder_path(@folder)


  end

  def update_file

    #@folder = Folder.find(params[:id])

    if @folder_file.update_attributes(params[:folder_file])
      flash[:success] = t('activerecord.success.model.folder_file.update_ok')
      redirect_to folder_managing_show_folder_file_path @folder, @folder_file

    else
      render action: 'edit_file'
    end

  end

  def create_folder_file_comment

    #@folder = Folder.find(params[:id])
    @folder_file = FolderFile.find(params[:folder_file_id])

    if @company.accepts_comments?
      @folder_file_value = FolderFileValue.new

      @folder_file_comment = FolderFileComment.new

      @folder_file_comment.folder_file = @folder_file
      @folder_file_comment.user = user_connected if normal_user_logged_in?
      @folder_file_comment.comentario = params[:folder_file_comment][:comentario]
      @folder_file_comment.fecha = lms_time

      if @folder_file_comment.save
        flash[:success] = t('activerecord.success.model.folder_file_comment.create_ok')
        redirect_to folder_managing_show_folder_file_path(@folder, @folder_file)
      else
        render 'show_file'
      end
    end


  end

  def create_folder_file_value

    #@folder = Folder.find(params[:id])
    @folder_file = FolderFile.find(params[:folder_file_id])

    if @company.accepts_values?
      @folder_file_comment = FolderFileComment.new

      @folder_file_value = FolderFileValue.new

      @folder_file_value.folder_file = @folder_file
      @folder_file_value.user = user_connected if normal_user_logged_in?
      @folder_file_value.valor = params[:folder_file_value][:valor] if params[:folder_file_value]
      @folder_file_value.fecha = lms_time

      if @folder_file_value.save
        flash[:success] = t('activerecord.success.model.folder_file_value.create_ok')

        value = 0.0
        suma = 0
        total = 0
        #@folder_file.folder_file_values.each do |value|
        @folder_file.values_including_ancestors.each do |value|
          suma += value.valor
          total += 1
        end

        value = suma/total

        if total > 0

          @folder_file.valor = value
          @folder_file.save

        end

        redirect_to folder_managing_show_folder_file_path @folder, @folder_file
      else
        render 'show_file'
      end
    end
  end


  def destroy
    #@folder = Folder.find(params[:id])

    folder_padre = @folder.padre

    @folder.destroy

    flash[:success] = t('activerecord.success.model.folder.delete_ok')

    if folder_padre
      redirect_to folder_managing_folder_path folder_padre
    else
      redirect_to folder_managing_folders_path
    end


  end

  def destroy_owner
    @folder = Folder.find(params[:id])
    @folder.folder_owners.find_by_user_id(params[:user_id]).destroy

    redirect_to folder_managing_folder_path(@folder)

  end

  def destroy_reader
    @folder = Folder.find(params[:id])
    @folder.folder_readers.find_by_user_id(params[:user_id]).destroy

    redirect_to folder_managing_folder_path(@folder)

  end

  def destroy_member
    @folder = Folder.find(params[:id])

    @folder.folder_members.where('characteristic_id = ? AND valor = ? ', params[:characteristic_id], params[:valor]).destroy_all

    redirect_to folder_managing_folder_path(@folder)

  end

  def hidden_file

    @folder_file = FolderFile.find(params[:folder_file_id])
    @folder_file.oculto = true
    @folder_file.save

    flash[:success] = t('activerecord.success.model.folder_file.hidden_ok')

    redirect_to folder_managing_folder_path(@folder_file.folder)

  end

  def unhidden_file

    @folder_file = FolderFile.find(params[:folder_file_id])
    @folder_file.oculto = false
    @folder_file.save

    flash[:success] = t('activerecord.success.model.folder_file.unhidden_ok')

    redirect_to folder_managing_folder_path(@folder_file.folder)

  end

  def make_public
    @folder.public = true
    @folder.save

    flash[:success] = 'La carpeta se actualizó correctamente'

    redirect_to folder_managing_folder_path(@folder)
  end

  def make_not_public
    @folder.public = false
    @folder.save

    flash[:success] = 'La carpeta se actualizó correctamente'

    redirect_to folder_managing_folder_path(@folder)
  end

  def destroy_folder_file

    folder = @folder_file.folder

    if folder_file_father = @folder_file.folder_file_father
      folder_file_father.update_attributes(:is_last => true)
    end

    File.delete(@folder_file.full_path(@company)) if File.exist?(@folder_file.full_path(@company))

    @folder_file.destroy

    flash[:success] = t('activerecord.success.model.folder_file.delete_ok')

    redirect_to folder_managing_folder_path(folder)

  end

  private

  def save_file(upload, folder_file, company)

    directory = company.directorio+'/'+company.codigo+'/mediateca/'

    FileUtils.mkdir_p directory unless File.directory?(directory)

    name = folder_file.crypted_name+File.extname(upload['datafile'].original_filename)
    path = File.join(directory, name)
    File.open(path, 'wb') { |f| f.write(upload['datafile'].read) }

  end

  def verify_access_download_folder_file

    @folder_file = FolderFile.find(params[:folder_file_id])

    unless @folder_file.descargable
      flash[:danger] = t('security.no_allow_download_folder_file_mediateca')
      redirect_to folder_managing_folder_pathfolders_path @folder
    end

  end


  def verify_destroy_empty_folder

    if @folder.hijos.count > 0 || @folder.folder_files.count > 0

      flash[:danger] = t('activerecord.error.model.folder.no_empty')
      redirect_to folder_managing_folder_pathfolder_path @folder

    end

  end

  def verify_destroy_empty_folder_file

    @folder_file = FolderFile.find(params[:folder_file_id])

    if @folder_file.folder_file_comments.count > 0 || @folder_file.folder_file_values.count > 0

      flash[:danger] = t('activerecord.error.model.folder_file.no_empty')
      redirect_to folder_managing_folder_pathfolder_path @folder

    end

  end

  def verify_jefe_de_biblioteca

    @folder = Folder.find(params[:id]) if params[:id]

    unless user_connected.jefe_biblioteca?
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end

  end

  def create_file_log(elemento, accion)
    # el jefe de biblioteca, desde la pantalla de gestión, no deja log

    unless user_connected_bd? || admin_logged_in?

      acciones = {ingreso: 'Ingreso', descarga: 'Descarga'}

      log = LogFolderFile.new

      log.user = user_connected
      log.folder_file = @folder_file
      log.fecha = lms_time
      log.elemento = elemento
      log.accion = acciones[accion]

      #log.save

    end

  end

end
