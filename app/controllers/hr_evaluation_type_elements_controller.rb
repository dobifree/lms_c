class HrEvaluationTypeElementsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:id])
  end


  def new

    @hr_evaluation_type = HrEvaluationType.find(params[:hr_evaluation_type_id])

    @hr_evaluation_type_element = @hr_evaluation_type.hr_evaluation_type_elements.new


  end


  def edit
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:id])

    @hr_evaluation_type = @hr_evaluation_type_element.hr_evaluation_type

  end


  def create
    @hr_evaluation_type_element = HrEvaluationTypeElement.new(params[:hr_evaluation_type_element])
    @hr_evaluation_type = @hr_evaluation_type_element.hr_evaluation_type

    if @hr_evaluation_type_element.save
      flash[:success] = t('activerecord.success.model.hr_evaluation_type_element.create_ok')
      redirect_to @hr_evaluation_type
    else

      render action: 'new'
    end

  end


  def update
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:id])
    @hr_evaluation_type = @hr_evaluation_type_element.hr_evaluation_type

    if @hr_evaluation_type_element.update_attributes(params[:hr_evaluation_type_element])
      flash[:success] = t('activerecord.success.model.hr_evaluation_type_element.update_ok')
      redirect_to @hr_evaluation_type
    else
      render action: 'edit'
    end

  end

  def delete
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:id])

    @hr_evaluation_type = @hr_evaluation_type_element.hr_evaluation_type

  end


  def destroy

    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:id])
    @hr_evaluation_type = @hr_evaluation_type_element.hr_evaluation_type

    if verify_recaptcha

      if @hr_evaluation_type_element.destroy
        flash[:success] = t('activerecord.success.model.hr_evaluation_type_element.delete_ok')
        redirect_to @hr_evaluation_type
      else
        flash[:danger] = t('activerecord.success.model.hr_evaluation_type_element.delete_error')
        redirect_to delete_hr_evaluation_type_element_path(@hr_evaluation_type_element)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.hr_evaluation_type_element.captcha_error')
      redirect_to delete_hr_evaluation_type_element_path(@hr_evaluation_type_element)

    end



  end

end
