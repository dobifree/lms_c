class ScProcessesRegistersController < ApplicationController
  include ScSchedulesHelper
  include ScheduleDateModule

  before_filter :get_gui_characteristics, only: [:register_attendance,
                                                 :search_people_register_record,
                                                 :create_record,
                                                 :go_to_register_attendances_by_date,
                                                 :falta_por_validar_attendances,
                                                 :falta_enviar_a_validacion_attendances]

  before_filter :authenticate_user, except: [:refresh_falta_enviar_a_validar_datatable,
                                             :get_editable_form,
                                             :get_non_editable_form,
                                             :get_deleted_form,
                                             :create_record,
                                             :update_record,
                                             :new_record_form,
                                             :make_it_editable,
                                             :send_single_record_to_validation]

  before_filter :verify_access_recorder, except: [:refresh_falta_enviar_a_validar_datatable,
                                                  :get_editable_form,
                                                  :get_non_editable_form,
                                                  :get_deleted_form,
                                                  :create_record,
                                                  :update_record,
                                                  :new_record_form,
                                                  :make_it_editable,
                                                  :send_single_record_to_validation]


  before_filter :authenticate_user_partial, only: [:refresh_falta_enviar_a_validar_datatable,
                                                   :get_editable_form,
                                                   :get_non_editable_form,
                                                   :get_deleted_form,
                                                   :create_record,
                                                   :update_record,
                                                   :new_record_form,
                                                   :make_it_editable,
                                                   :send_single_record_to_validation]

  before_filter :verify_access_recorder_partial, only: [:refresh_falta_enviar_a_validar_datatable,
                                                        :get_editable_form,
                                                        :get_non_editable_form,
                                                        :get_deleted_form,
                                                        :create_record,
                                                        :update_record,
                                                        :new_record_form,
                                                        :make_it_editable,
                                                        :send_single_record_to_validation]



  def register_attendance
    @sc_process = ScProcess.find(params[:sc_process_id])
    register = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    @users_to_register = register.sc_user_to_registers.where(:active => true).joins(:user).order('apellidos, nombre')
    @tab = params[:tab] ? params[:tab] : 0
  end

  def refresh_falta_enviar_a_validar_datatable
    register = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    users_to_register = register.sc_user_to_registers.where(:active => true).joins(:user).order('apellidos, nombre')
    process = ScProcess.find(params[:sc_process_id])

    render :partial => 'datatable_falta_validar_attendances',
             :locals => {:characteristics => @sc_schedule_characteristics,
             :users_to_register => users_to_register,
             :process => process}
  end

  def add_user_to_session
    register = HeCtModulePrivilage.where(user_id: user_connected.id, active: true, recorder: true).first
    manual_user_to_register = ScManualUserToRegister.where(user_id: params[:user_id],
                                                           he_ct_module_privilage_id: register.id,
                                                           sc_process_id: params[:sc_process_id]).first_or_initialize

    unless manual_user_to_register.id
      manual_user_to_register.registered_by_user_id = user_connected.id
      manual_user_to_register.registered_at = lms_time
      manual_user_to_register.save
    end

    render :nothing => true
  end

  def search_people_register_record
    @user = User.new
    @users = {}
    @sc_process = ScProcess.find(params[:sc_process_id])

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def get_editable_form
    record = ScRecord.find(params[:sc_record_id])
    checkbox = params[:whoami] == 'falta_enviar_tab' ? true : false

    if record.status == 0 || record.status == 4
      unless record.sc_process.closed
        render :partial => 'update_record_form',
               :locals => {:user => record.user_id,
                           :editable => true,
                           :process => record.sc_process,
                           :record => record,
                           :whoami => params[:whoami],
                           :edit => true, :save => true, :delete => true, :cancel => true,
                           :checkbox => checkbox}
      else
        render :partial => 'update_record_form',
               :locals => {:user => record.user_id,
                           :editable => false,
                           :process => record.sc_process,
                           :record => record,
                           :whoami => params[:whoami],
                           :show_generic_message => true,
                           :type_generic_message => 'alert-danger',
                           :generic_message => t('views.sc_processes.flash_messages.process_closed'),
                           :edit => false, :save => false, :delete => false, :cancel => false,
                           :checkbox => checkbox }
      end
    else
      render :partial => 'update_record_form',
             :locals => {:user => record.user_id,
                         :editable => false,
                         :process => record.sc_process,
                         :record => record,
                         :whoami => params[:whoami],
                         :show_generic_message => true,
                         :type_generic_message => 'alert-danger',
                         :generic_message => t('views.sc_processes.flash_messages.record_not_editable'),
                         :edit => false, :save => false, :delete => false, :cancel => false,
                         :checkbox => checkbox }
    end

  end

  def get_non_editable_form
    record = ScRecord.find(params[:sc_record_id])
    checkbox = params[:whoami] == 'falta_enviar_tab' ? true : false

    edit = false
    delete = false
    save = false
    cancel = false

    if record.status == 0 || record.status == 4
      unless record.sc_process.closed
        edit = true
        delete = true
        save = true
        cancel = true
      end
    end

    render :partial => 'update_record_form',
           :locals => {:user => record.user_id,
                       :editable => false,
                       :process => record.sc_process,
                       :record => record,
                       :whoami => params[:whoami],
                       :edit => edit, :save => save, :delete => delete, :cancel => cancel,
                       :checkbox => checkbox }
  end

  def get_deleted_form
    record = ScRecord.find(params[:sc_record_id])
    checkbox = params[:whoami] == 'falta_enviar_tab' ? true : false
    if record.status == 0 || record.status == 4
      unless record.sc_process.closed
        record_id = record.id
        log = ScLogRecord.create(:user_id => user_connected.id, :sc_record_id => record.id, :date => lms_time, :record_before => record.as_json, :action => 'Eliminar')
        record.destroy

        render :partial => 'update_record_form',
               :locals => {:deleted => true,
                           :record_id => record_id}
      else
        render :partial => 'update_record_form',
               :locals => {:user => record.user_id,
                           :editable => false,
                           :process => record.sc_process,
                           :record => record,
                           :whoami => params[:whoami],
                           :show_generic_message => true,
                           :type_generic_message => 'alert-danger',
                           :generic_message => t('views.sc_processes.flash_messages.process_closed'),
                           :edit => false, :save => false, :delete => false, :cancel => false,
                           :checkbox => checkbox }
      end
    else
      render :partial => 'update_record_form',
             :locals => {:user => record.user_id,
                         :editable => false,
                         :process => record.sc_process,
                         :record => record,
                         :whoami => params[:whoami],
                         :show_generic_message => true,
                         :type_generic_message => 'alert-danger',
                         :generic_message => t('views.sc_processes.flash_messages.record_not_editable'),
                         :edit => false, :save => false, :delete => false, :cancel => false,
                         :checkbox => checkbox }
    end

  end

  def create_record
    new_record = complete_create_record(params[:sc_record], params[:user_id], params[:sc_process_id])
    if new_record.sc_process.closed
      new_record.errors.add(:sc_process_id, ' cerrado, disponible solamente para visualización')
      render partial: 'new_record_form',
             locals: {user: new_record.user,
                      process: new_record.sc_process,
                      new_record: new_record,
                      reference_date: new_record.begin,
                      cancel: false,
                      save: false}
    elsif new_record.save
      schedule_user = get_schedule_by_record(new_record)
      schedule_block = get_schedule_block_by_record(schedule_user, new_record)
      if schedule_block && !(new_record.begin_sc || new_record.end_sc)
        new_record.effective_time -= schedule_block.free_time
        block_effective_time = (schedule_block.end_time - schedule_block.begin_time) / 60 - schedule_block.free_time
        block_effective_time += 24 * 60 if schedule_block.next_day
        new_record.effective_time -= block_effective_time
        new_record.effective_time = 0 if new_record.effective_time < 0
        new_record.save
      end
      calculate_he(schedule_block, new_record) if new_record.user.he_able
      render :partial => 'update_record_form',
             :locals => {:user => new_record.user,
                         :process => new_record.sc_process,
                         :record => new_record,
                         :reference_date => new_record.begin,
                         :success_message_created => true,
                         :whoami => 'go_to_register_view',
                         :edit => true, :save => true, :delete => true, :cancel => true}
    else
      render :partial => 'new_record_form',
             :locals => {:user => new_record.user,
                         :process => new_record.sc_process,
                         :new_record => new_record,
                         :reference_date => new_record.begin,
                         :edit => true, :save => true, :delete => true, :cancel => true}
    end
  end

  def update_record
    checkbox = params[:whoami] == 'falta_enviar_tab' ? true : false
    record = ScRecord.find(params[:sc_record_id])

    if record.status == 0 || record.status == 4
      if record.sc_process.closed
        if params[:whoami] == 'falta_enviar_tab'
          render :partial => 'update_record_form',
                 :locals => {:user => record.user,
                             :process => record.sc_process,
                             :record => record,
                             :reference_date => record.begin,
                             :whoami => 'falta_enviar_tab',
                             :show_generic_message => true,
                             :type_generic_message => 'alert-danger',
                             :generic_message => t('views.sc_processes.flash_messages.process_closed'),
                             :edit => false, :save => false, :delete => false, :cancel => false,
                             :checkbox => checkbox }
        else
          render :partial => 'update_record_form',
                 :locals => {:user => record.user,
                             :process => record.sc_process,
                             :record => record,
                             :reference_date => record.begin,
                             :whoami => 'go_to_register_view',
                             :show_generic_message => true,
                             :type_generic_message => 'alert-danger',
                             :generic_message => t('views.sc_processes.flash_messages.process_closed'),
                             :edit => false, :save => false, :delete => false, :cancel => false,
                             :checkbox => checkbox }
          end
      else
        prev_record = record

        if record.update_attributes(params[:sc_record])
          log = ScLogRecord.new(:user_id => user_connected.id, :sc_record_id => record.id, :date => lms_time, :record_before => prev_record.as_json, :action => 'Editar')
          log.save
          record.status = 0
          record.effective_time = ((record.end - record.begin)/60) #minutes
          record.he = 0
          record.sc_record_blocks.destroy_all

          schedule_user = get_schedule_by_record(record)
          schedule_block = get_schedule_block_by_record(schedule_user, record)
          if schedule_block
            record.effective_time -= schedule_block.free_time
            block_effective_time = (schedule_block.end_time - schedule_block.begin_time)/60 - schedule_block.free_time
            block_effective_time += 24*60 if schedule_block.next_day
            record.effective_time -= block_effective_time
            record.effective_time = 0 if record.effective_time < 0
            record.save
          end
          calculate_he(schedule_block, record)
          record.save
        end
        message_success = false
        message_success = true if record.errors.size == 0
        if record.errors.size == 0
          render :partial => 'update_record_form',
                 :locals => {:user => record.user,
                             :process => record.sc_process,
                             :record => record,
                             :reference_date => record.begin,
                             :whoami => params[:whoami],
                             :success_message_changed => message_success,
                             :edit => true, :save => true, :delete => true, :cancel => true,
                             :checkbox => checkbox, :editable => false }
        else
          render :partial => 'update_record_form',
                 :locals => {:user => record.user,
                             :process => record.sc_process,
                             :record => record,
                             :reference_date => record.begin,
                             :success_message_changed => message_success,
                             :whoami => params[:whoami],
                             :edit => true, :save => true, :delete => true, :cancel => true,
                             :checkbox => checkbox, :editable => true }
        end
      end
    else
      if params[:whoami] == 'falta_enviar_tab'
        render :partial => 'update_record_form',
               :locals => {:user => record.user,
                           :process => record.sc_process,
                           :record => record,
                           :reference_date => record.begin,
                           :whoami => 'falta_enviar_tab',
                           :show_generic_message => true,
                           :type_generic_message => 'alert-danger',
                           :generic_message => t('views.sc_processes.flash_messages.record_not_editable'),
                           :edit => false, :save => false, :delete => false, :cancel => false,
                           :checkbox => checkbox }
      else
        render :partial => 'update_record_form',
               :locals => {:user => record.user,
                           :process => record.sc_process,
                           :record => record,
                           :reference_date => record.begin,
                           :whoami => 'go_to_register_view',
                           :show_generic_message => true,
                           :type_generic_message => 'alert-danger',
                           :generic_message => t('views.sc_processes.flash_messages.record_not_editable'),
                           :edit => false, :save => false, :delete => false, :cancel => false,
                           :checkbox => checkbox }
      end
    end
  end


  def new_record_form
    user = User.find(params[:user_id])
    process = ScProcess.find(params[:sc_process_id])
    new_record = ScRecord.new()
    reference_date = (params[:reference_date]).to_datetime.strftime("%d/%m/%Y")

    if process.closed
      render :partial => 'new_record_form',
             :locals => {:user => user,
                         :process => process,
                         :new_record => new_record,
                         :reference_date => reference_date, :cancel => false, :save => false }
    else
      render :partial => 'new_record_form',
             :locals => {:user => user,
                         :process => process,
                         :new_record => new_record,
                         :reference_date => reference_date, :cancel => true, :save => true }
    end

  end

  def go_to_register_attendances_by_date
    @sc_process = ScProcess.find(params[:sc_process_id])
    user_privilege = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    @users_to_register = user_privilege.sc_user_to_registers.where(:active => true).joins(:user).order('apellidos, nombre')
    @reference_date = params[:reference_date].to_date
  end

  def make_it_editable
    # status:
    # 0:Pendiente de enviar validación,
    # 1:Pendiente de ser validado.
    # 3:Validado.
    # 4:Rechazado

    record = ScRecord.find(params[:sc_record_id])

    if record.status == 1
      unless record.sc_process.closed
        log = ScLogRecord.create(:user_id => user_connected.id, :sc_record_id => record.id, :date => lms_time, :record_before => record.as_json, :action => 'Editable')
        record.status = 0
        record.save

        render :partial => 'shared/div_message',
               :locals => { type_generic_message: 'alert-success',
                            generic_message: 'Cambio realizado satisfactoriamente'}

        # render :partial => 'show_record_table',
        #        :locals => {:record => record,
        #                    :go_to_falta_validar => true,
        #                    :whoami => 'por_validar',
        #                    :show_generic_message => true,
        #                    :type_generic_message => 'alert-success',
        #                    :generic_message => 'Cambio realizado satisfactoriamente',
        #                    :only_generic_message => true}


      else
        render :partial => 'show_record_table',
               :locals => {:record => record,
                           :go_to_falta_validar => true,
                           :whoami => 'por_validar',
                           :show_generic_message => true,
                           :type_generic_message => 'alert-danger',
                           :generic_message => 'Proceso cerrado, disponible solamente para visualización',
                           :only_generic_message => false}
      end
    else
      render :partial => 'show_record_table',
             :locals => {:record => record,
                         :go_to_falta_validar => true,
                         :whoami => 'por_validar',
                         :show_generic_message => true,
                         :type_generic_message => 'alert-danger',
                         :generic_message => 'Registro no se encuentra pendiente de validación',
                         :only_generic_message => true}
    end

  end

def send_single_record_to_validation
  # status:
  # 0:Pendiente de enviar validación,
  # 1:Pendiente de ser validado.
  # 3:Validado.
  # 4:Rechazado

  record = ScRecord.find(params[:sc_record_id])
  if record.status == 0 || record.status == 4
    unless record.sc_process.closed
      log = ScLogRecord.create(:user_id => user_connected.id, :sc_record_id => record.id, :date => lms_time, :record_before => record.as_json, :action => 'Enviar a validación')

      sc_user_process = ScUserProcess.where(:user_id => record.user_id, :sc_process_id => record.sc_process_id).first
        if sc_user_process
        else
          sc_user_process = ScUserProcess.new(:user_id => record.user_id, :sc_process_id => record.sc_process_id, :he_total => 0)
          sc_user_process.save
        end

      record.status = 1
      record.sc_user_process_id = sc_user_process.id
      record.save

      render :partial => 'shared/div_message',
             :locals => { type_generic_message: 'alert-success',
                          generic_message:'Enviado a validación satisfactoriamente'}

      # render :partial => 'update_record_form',
      #        :locals => {:deleted => true,
      #                    :record_id => record.id,
      #                    :show_generic_message => true,
      #                    :type_generic_message => 'alert-success',
      #                    :generic_message => 'Enviado a validación satisfactoriamente'}

    else
      render :partial => 'update_record_form',
             :locals => {:user => record.user,
                         :process => record.sc_process,
                         :record => record,
                         :reference_date => record.begin,
                         :whoami => 'falta_enviar_tab',
                         :show_generic_message => true,
                         :type_generic_message => 'alert-danger',
                         :generic_message => t('views.sc_processes.flash_messages.process_closed'),
                         :edit => false, :save => false, :delete => false, :cancel => false,
                         :checkbox => false }
    end
  else
    render :partial => 'update_record_form',
           :locals => {:user => record.user,
                       :process => record.sc_process,
                       :record => record,
                       :reference_date => record.begin,
                       :whoami => 'falta_enviar_tab',
                       :show_generic_message => true,
                       :type_generic_message => 'alert-danger',
                       :generic_message => t('views.sc_processes.flash_messages.record_not_editable'),
                       :edit => false, :save => false, :delete => false, :cancel => false,
                       :checkbox => false }
  end
end

  def send_records_to_validation
    # status: 0:Pendiente de enviar validación, 1:Pendiente de ser validado. 2:En discusión. 3:Validado. 4:Rechazado
    records = ScRecord.where(:sc_process_id => params[:sc_process_id], :status => 0 )
    users_id = []
    records.each do |record|
      users_id << record.user_id
    end

    users_id = users_id.uniq

    users_id.each do |user_id|
      sc_user_process = ScUserProcess.where(:user_id => user_id, :sc_process_id => params[:sc_process_id]).first
      if sc_user_process
      else
        sc_user_process = ScUserProcess.new(:user_id => user_id, :sc_process_id => params[:sc_process_id], :he_total => 0)
        sc_user_process.save
      end


      records.each_with_index do |record, records_index|
        if record.user_id == user_id
          record.sc_user_process_id = sc_user_process.id
          record.status = 1
          record.save
        end
      end
    end
    flash[:success] = t('views.sc_processes.flash_messages.changes_success')
    redirect_to register_attendance_path(params[:sc_process_id], 1)
  end

  def falta_por_validar_attendances
    @sc_process = ScProcess.find(params[:sc_process_id])
    register = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    @users_to_register = register.sc_user_to_registers.where(:active => true).joins(:user).order('apellidos, nombre')

    render :partial => 'datatable_por_validar_attendances',
           :locals => {:characteristics => @sc_schedule_characteristics,
                       :users_to_register => @users_to_register,
                       :process => @sc_process }
  end

  def falta_enviar_a_validacion_attendances
    @sc_process = ScProcess.find(params[:sc_process_id])
    register = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    @users_to_register = register.sc_user_to_registers.where(:active => true).joins(:user).order('apellidos, nombre')

    render :partial => 'datatable_falta_validar_attendances',
           :locals => {:characteristics => @sc_schedule_characteristics,
                       :users_to_register => @users_to_register,
                       :process => @sc_process}
  end

  private
  def get_gui_characteristics
    @sc_schedule_characteristics = ScScheduleCharacteristic.gui
  end

  def verify_access_recorder
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first
    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true)
    if recorder && ct_module
    else
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_recorder_partial
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first
    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true)
    if recorder && ct_module
    else
      render :partial => 'shared/div_message',
             :locals => { :type_generic_message => 'alert-danger',
                          :generic_message => t('security.no_access_to_manage_module')}
    end
  end

  def authenticate_user_partial
    unless logged_in?
      render :partial => 'shared/div_message',
             :locals => { :type_generic_message => 'alert-danger',
                          :generic_message => 'No ha iniciado sesión'}
    end
  end

end

# STATUS GUIDE
# 0:Pendiente de enviar validación,
#                       1:Pendiente de ser validado.
#     2:En discusión.
#     3:Validado.
#     4:Rechazado
