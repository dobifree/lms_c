class ProgramCoursesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @programs = Program.all
  end

  def study_plan
    @program = Program.find(params[:program_id])

    @agregarcurso = false;

    if @program.courses.count < Course.all.count && @program.levels.count > 0
      @agregarcurso = true    
    end

  end

	def new
    @program = Program.find(params[:program_id])
    @program_course = @program.program_courses.build()
    
    if @program.courses.count == 0
      @courses = Course.all
    else
      @courses = Course.where('id not in (?)', @program.courses.map{ |course| course.id })
    end

  end


  def create

    @program = Program.find(params[:program][:id])

		course = Course.find(params[:program_course][:course_id])
		level = Level.find(params[:program_course][:level_id])
    
    @program_course = @program.program_courses.build(course: course, level: level)

    if @program_course.save
      flash[:success] = t('activerecord.success.model.program_course.create_ok')
      redirect_to study_plan_path(@program)
    else
      render action: 'new'
    end

  end

  def edit

    @program_course = ProgramCourse.find(params[:id])

  end

  def update

    @program_course = ProgramCourse.find(params[:id])

    if @program_course.update_attributes(params[:program_course])
      flash[:success] = t('activerecord.success.model.program_course.update_ok')

      redirect_to study_plan_path(@program_course.program)
    else
      render action: 'edit'
    end

  end

  def choose_migrate_program

    @program_course = ProgramCourse.find(params[:program_course_id])

  end

  def migrate_program

    @program_course = ProgramCourse.find(params[:program_course_id])

    previous_program = @program_course.program

    new_program = Program.find(params[:program_course][:program_id])

    new_level = new_program.levels.first

    if new_level

      @program_course.program = new_program

      @program_course.level = new_level

      if @program_course.save

        @program_course.user_courses.each do |user_course|

          new_enrollment = Enrollment.where('program_id = ? AND user_id = ? AND level_id = ?', new_program.id, user_course.enrollment.user.id, new_level.id).first

          unless new_enrollment

            new_enrollment = new_program.enrollments.build
            new_enrollment.user = user_course.enrollment.user
            new_enrollment.level = new_level

          end

          if new_enrollment.save

            user_course.enrollment = new_enrollment

            if !previous_program.especifico && new_program.especifico

              user_course.desde = user_course.inicio ? user_course.inicio : lms_date
              user_course.hasta = user_course.fin ? user_course.fin : lms_date+2.month-1.second

              user_course.set_grupo
              user_course.set_grupo_f

            elsif previous_program.especifico && !new_program.especifico

              unless user_course.inicio

                user_course.inicio = user_course.desde
                user_course.iniciado = true

              end

            end

            user_course.save

          end

        end

      end

      flash[:success] = t('activerecord.success.model.program_course.migrate_ok')

    end

    redirect_to study_plan_path new_program

  end

  def migrate_program_massive

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          next if index == 0

          fila[0] = fila[0].to_s
          fila[1] = fila[1].to_s

          course = Course.find_by_codigo fila[0]
          new_program = Program.find_by_nombre fila[1]

          if course && new_program

            program_courses = course.program_courses

            new_level = new_program.levels.first

            unless new_level
              new_level = new_program.levels.build
              new_level.nombre = '1'
              new_level.orden = 1
              new_level.save
            end

            new_program_course = new_program.program_courses.where('course_id = ?', course.id).first

            unless new_program_course
              new_program_course = new_program.program_courses.build
              new_program_course.course = course
              new_program_course.level = new_level
            end

            if new_program_course.save

              program_courses.each do |program_course|

                if program_course.id != new_program_course.id

                  program_course.user_courses.each do |user_course|

                    new_enrollment = Enrollment.where('program_id = ? AND user_id = ? AND level_id = ?', new_program.id, user_course.enrollment.user.id, new_level.id).first

                    unless new_enrollment

                      new_enrollment = new_program.enrollments.build
                      new_enrollment.user = user_course.enrollment.user
                      new_enrollment.level = new_level

                    end

                    if new_enrollment.save

                      user_course.enrollment = new_enrollment
                      user_course.program_course = new_program_course
                      user_course.save

                    end

                  end

                  program_course.destroy

                end

              end

            end





          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('').force_encoding('UTF-8')
            @lineas_error_messages.push [fila[0]+' no existe'] unless course
            @lineas_error_messages.push [fila[1]+' no existe'] unless new_program

          end

        end

        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def search_course_manager

    @program_course = ProgramCourse.find(params[:id])

    @user = User.new
    @users = {}

    if params[:user]

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        @users = User.where(
            'apellidos LIKE ? AND nombre LIKE ?',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        @user.apellidos = params[:user][:apellidos]
        @user.nombre = params[:user][:nombre]
      end

    end

  end

  def create_course_manager

    @program_course = ProgramCourse.find(params[:id])

    pcm = @program_course.program_course_managers.new(user_id: params[:user_id])
    pcm.save

    flash[:success] = t('activerecord.success.model.program_course.add_course_manager_ok')
    redirect_to study_plan_path @program_course.program_id


  end

  def destroy_course_manager

    pcm = ProgramCourseManager.find(params[:pcm_id])

    pc = pcm.program_course

    pcm.destroy

    flash[:success] = t('activerecord.success.model.program_course.delete_course_manager_ok')
    redirect_to study_plan_path pc.program_id

  end


end
