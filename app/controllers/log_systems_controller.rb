class LogSystemsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /log_systems
  # GET /log_systems.json
  def index

    @fecha_desde = params[:fecha_desde] ? params[:fecha_desde] : (lms_time - 1.day).strftime('%Y-%m-%d %H:%M:%S')
    @fecha_hasta = params[:fecha_hasta] ? params[:fecha_hasta] : (lms_time + 1.day).strftime('%Y-%m-%d %H:%M:%S')
    @patron_controller = params[:patron_controller] ? params[:patron_controller] : ''
    @patron_action = params[:patron_action] ? params[:patron_action] : ''

    @log_systems = LogSystem.where("registered_at >= '#{@fecha_desde}' AND registered_at <= '#{@fecha_hasta}' AND (IFNULL(controller, '') like '%#{@patron_controller}%' AND IFNULL(action, '') like '%#{@patron_action}%')").order('id DESC')

  end

  # GET /log_systems/1
  # GET /log_systems/1.json
  def show
    @log_system = LogSystem.find(params[:id])


  end


end
