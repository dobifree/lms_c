class MedlicReportsController < ApplicationController
  include MedlicModule

  before_filter :active_module
  before_filter :medlic_manager

  def new_simple_report
    partial_report = defined?(params[:partial_report]) ? params[:partial_report] : 0
    partial_report = partial_report.to_i == 1
    report = MedlicReport.new(partial: partial_report, medlic_process_id: params[:medlic_process_id])
    @report = set_registered(report)
    last_report = @report.partial ? MedlicReport.active.from_process(@report.medlic_process_id).only_partial.first : MedlicReport.active.from_process(@report.medlic_process_id).non_partial.first
    deactivate(last_report).save if last_report
    @report.save
    set_licenses(@report)
    render xlsx: 'medlic_simple_report.xlsx', filename: @report.medlic_process.name
  end
end