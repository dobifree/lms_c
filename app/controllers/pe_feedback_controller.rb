class PeFeedbackController < ApplicationController

  include PeReportingHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list]
  before_filter :get_data_2, only: [:new_feedback, :create_feedback, :final_rep_pdf, :final_rep_only_boss_pdf]

  before_filter :validate_open_process, only: [:people_list, :new_feedback, :create_feedback, :final_rep_pdf, :final_rep_only_boss_pdf]
  before_filter :validate_access_to_evaluate, only:[:new_feedback, :create_feedback, :final_rep_pdf, :final_rep_only_boss_pdf]

  before_filter :get_data_1_2, only: [:people_list_2]
  before_filter :get_data_2_2, only: [:new_feedback_2, :create_feedback_2, :rep_detailed_final_results_pdf]

  before_filter :validate_open_process_2, only: [:people_list_2, :new_feedback_2, :create_feedback_2, :rep_detailed_final_results_pdf]
  before_filter :validate_is_the_feedback_provider, only: [:new_feedback_2, :create_feedback_2, :rep_detailed_final_results_pdf]
  before_filter :validate_can_download_rep_detailed_final_results_pdf, only: [:rep_detailed_final_results_pdf]



  def people_list_2
    redirect_to pe_uniq_path if @pe_process.step_feedback_uniq? && session['from_uniq_feedback_'+@pe_process.id.to_s] == '1'

  end

  def new_feedback_2

    if params['from_uniq'] == '1'
      session['from_uniq_feedback_'+@pe_process.id.to_s] = '1'
    end

  end

  def create_feedback_2

    @feedback_error = false

    @pe_member.pe_member_feedbacks.destroy_all

    @pe_process.pe_feedback_fields.each do |pe_feedback_field|

      pe_member_feedback = @pe_member.pe_member_feedbacks.build
      pe_member_feedback.pe_process = @pe_process
      pe_member_feedback.pe_feedback_field = pe_feedback_field

      if pe_feedback_field.simple

        pe_member_feedback.comment = params['feedback_field_'+pe_feedback_field.id.to_s]

        pe_member_feedback.comment_date = params['feedback_field_'+pe_feedback_field.id.to_s] if pe_feedback_field.field_type == 1

        pe_member_feedback.list_item_id = params['feedback_field_'+pe_feedback_field.id.to_s] if pe_feedback_field.field_type == 2

        @feedback_error = true unless pe_member_feedback.save

      else

        if params['feedback_field_'+pe_feedback_field.id.to_s+'_create']

          number_of_compound_feedbacks = 0

          pe_member_feedback.comment = '-'

          pe_member_feedback.save

          at_least_one = false

          aux_pos = 1

          params['feedback_field_'+pe_feedback_field.id.to_s+'_create'].each_with_index do |feedback_field_create, pos|

            if feedback_field_create == '1'

              at_least_one = true

              n_errors = 0

              optional_row = true
              empty_row = true
              copy_of_saved_fields = Array.new


              pe_feedback_field.pe_feedback_compound_fields.each do |pe_feedback_compound_field|

                optional_row = false if pe_feedback_compound_field.required

                empty_row = false unless params['feedback_field_'+pe_feedback_field.id.to_s+'_'+pe_feedback_compound_field.id.to_s][pos].blank?

                pe_member_compound_feedback = pe_member_feedback.pe_member_compound_feedbacks.build

                pe_member_compound_feedback.position = aux_pos

                pe_member_compound_feedback.pe_feedback_compound_field = pe_feedback_compound_field

                pe_member_compound_feedback.comment = params['feedback_field_'+pe_feedback_field.id.to_s+'_'+pe_feedback_compound_field.id.to_s][pos]

                pe_member_compound_feedback.comment_date = params['feedback_field_'+pe_feedback_field.id.to_s+'_'+pe_feedback_compound_field.id.to_s][pos] if pe_feedback_compound_field.field_type == 1

                pe_member_compound_feedback.list_item_id = params['feedback_field_'+pe_feedback_field.id.to_s+'_'+pe_feedback_compound_field.id.to_s][pos] if pe_feedback_compound_field.field_type == 2

                if pe_member_compound_feedback.save
                  copy_of_saved_fields.push pe_member_compound_feedback
                else
                  n_errors += 1
                end

              end

              if optional_row && empty_row

                copy_of_saved_fields.each do |copy_of_saved_field|
                  copy_of_saved_field.destroy
                end

                aux_pos -= 1

              else

                if n_errors == 0

                  number_of_compound_feedbacks += 1

                elsif n_errors <= pe_feedback_field.pe_feedback_compound_fields.size

                  @feedback_error = true
                  number_of_compound_feedbacks += 1

                end

              end

            end

            aux_pos += 1

          end

          @feedback_error = true unless at_least_one

          if number_of_compound_feedbacks == 0

            pe_member_feedback.destroy

          else

            pe_member_feedback.reload
            pe_member_feedback.number_of_compound_feedbacks = number_of_compound_feedbacks
            pe_member_feedback.save

          end

        else

          @feedback_error = true

        end

      end

    end

    if @feedback_error
      flash.now[:success] = t('activerecord.success.model.pe_process_feedback.partial_create_feedback_ok')
      flash.now[:danger] = t('activerecord.error.model.pe_process_feedback.incomplete_feedback')
      render 'new_feedback_2'
    else

      @pe_member.step_feedback = params[:pe_member][:step_feedback]
      @pe_member.step_feedback_date = lms_time

      if @pe_member.save

        if @pe_member.step_feedback

          #LmsMailer.ask_pe_member_to_accept_feedback(@pe_member, @pe_process, menu_name, @company).deliver if @pe_process.has_step_feedback_accepted? && @pe_process.step_feedback_accepted_send_mail?

          if @pe_process.has_step_feedback_accepted? && @pe_process.step_feedback_accepted_send_mail?

            menu_name = 'Evaluación de desempeño'

            ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

              if ct_menu_cod == 'pe'
                menu_name = ct_menus_user_menu_alias[index_m]
                break
              end
            end

            begin
              PeMailer.step_feedback_accept_ask_to_accept(@pe_process, @company, menu_name, @pe_member.user, @pe_member.feedback_provider).deliver
            rescue Exception => e
              lm = LogMailer.new
              lm.module = 'pe'
              lm.step = 'def_by_user:feedback:ask_to_accept'
              lm.description = e.message+' '+e.backtrace.inspect
              lm.registered_at = lms_time
              lm.save
            end

          end

          flash[:success] = t('activerecord.success.model.pe_process_feedback.create_feedback_ok')

        else

          flash[:success] = t('activerecord.success.model.pe_process_feedback.create_feedback_ok')
          flash[:danger] = t('activerecord.error.model.pe_process_feedback.not_confirmed_feedback')

        end

        redirect_to pe_feedback_new_feedback_2_path(@pe_member)

      else

        flash[:danger] = t('activerecord.error.model.pe_process_feedback.create_feedback_error')
        redirect_to pe_feedback_new_feedback_2_path(@pe_member)
      end



    end



  end

=begin
  def create_feedback_2_old

    @feedback_error = false

    @pe_member.pe_member_feedbacks.destroy_all

    @pe_process.pe_feedback_fields.each do |pe_feedback_field|

      pe_member_feedback = @pe_member.pe_member_feedbacks.build
      pe_member_feedback.pe_process = @pe_process
      pe_member_feedback.pe_feedback_field = pe_feedback_field

      if pe_feedback_field.simple

        pe_member_feedback.comment = params['feedback_field_'+pe_feedback_field.id.to_s]

        if pe_feedback_field.field_type == 1
          pe_member_feedback.comment_date = params['feedback_field_'+pe_feedback_field.id.to_s]
        end

        if pe_feedback_field.field_type == 2
          pe_member_feedback.list_item_id = params['feedback_field_'+pe_feedback_field.id.to_s]
        end

        @feedback_error = true unless pe_member_feedback.save

      else

        pe_member_feedback.comment = '-'

        pe_member_feedback.save

        number_of_compound_feedbacks = 0

        (0..pe_feedback_field.max_number_of_compound_feedbacks-1).each do |pos|

          n_errors = 0

          pe_feedback_field.pe_feedback_compound_fields.each do |pe_feedback_compound_field|

            pe_member_compound_feedback = pe_member_feedback.pe_member_compound_feedbacks.build

            pe_member_compound_feedback.position = pos+1

            pe_member_compound_feedback.pe_feedback_compound_field = pe_feedback_compound_field

            pe_member_compound_feedback.comment = params['feedback_field_'+pe_feedback_field.id.to_s+'_'+pe_feedback_compound_field.id.to_s][pos]

            if pe_feedback_compound_field.field_type == 1
              pe_member_compound_feedback.comment_date = params['feedback_field_'+pe_feedback_field.id.to_s+'_'+pe_feedback_compound_field.id.to_s][pos]
            end

            if pe_feedback_compound_field.field_type == 2
              pe_member_compound_feedback.list_item_id = params['feedback_field_'+pe_feedback_field.id.to_s+'_'+pe_feedback_compound_field.id.to_s][pos]
            end

            n_errors += 1 unless pe_member_compound_feedback.save

          end

          if n_errors == 0

            number_of_compound_feedbacks += 1

          elsif n_errors < pe_feedback_field.pe_feedback_compound_fields.size

            @feedback_error = true
            number_of_compound_feedbacks += 1

          end

        end

        pe_member_feedback.reload
        pe_member_feedback.number_of_compound_feedbacks = number_of_compound_feedbacks
        pe_member_feedback.save

      end

    end

    if @feedback_error

      flash.now[:danger] = t('activerecord.error.model.pe_process_feedback.incomplete_feedback')
      render 'new_feedback_2'
    else

      @pe_member.step_feedback = true
      @pe_member.step_feedback_date = lms_time

      if @pe_member.save

        LmsMailer.ask_pe_member_to_accept_feedback(@pe_member, @pe_process, @company).deliver if @pe_process.has_step_feedback_accepted? && @pe_process.step_feedback_accepted_send_mail?

        flash[:success] = t('activerecord.success.model.pe_process_feedback.create_feedback_ok')
        redirect_to pe_feedback_new_feedback_2_path(@pe_member)

      else
        flash[:danger] = t('activerecord.error.model.pe_process_feedback.create_feedback_error')
        redirect_to pe_feedback_new_feedback_2_path(@pe_member)
      end



    end



  end
=end

  def rep_detailed_final_results_pdf

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, true, @pe_process.rep_detailed_final_results_show_calibration_for_feedback, @pe_process.public_pe_box, @pe_process.public_pe_dimension_name, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  #######

  def people_list
   #@company = session[:company]
    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions
  end

  def new_feedback
   #@company = session[:company]
    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions
    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)
  end

  def create_feedback

    @hr_process_user.feedback_confirmado = params[:hr_process_user][:feedback_confirmado]

    if @hr_process.tiene_paso_feedback_modo == 1

      @hr_process_user.texto_feedback = params[:hr_process_user][:texto_feedback]

    else

      @hr_process_user.tf_1 = params[:hr_process_user][:tf_1]
      @hr_process_user.tf_2 = params[:hr_process_user][:tf_2]

      @hr_process_user.tf_4 = params[:hr_process_user][:tf_4] if params[:hr_process_user][:tf_4]

      @hr_process_user.hr_process_user_tfs.destroy_all

      hay_error = false

      tot_num_tfs_3 = 5

      tot_num_tfs_3 = 1 if $current_domain == 'epacasmayo'

      (1..tot_num_tfs_3).each do |num_tf_t|

        if !params[('tf_'+num_tf_t.to_s+'_what').to_sym].blank? || !params[('tf_'+num_tf_t.to_s+'_how').to_sym].blank? || !params[('tf_'+num_tf_t.to_s+'_when').to_sym].blank?

          hr_process_user_tf = @hr_process_user.hr_process_user_tfs.build

          hr_process_user_tf.what = params[('tf_'+num_tf_t.to_s+'_what').to_sym]
          hr_process_user_tf.how = params[('tf_'+num_tf_t.to_s+'_how').to_sym]
          hr_process_user_tf.when = params[('tf_'+num_tf_t.to_s+'_when').to_sym]

          hr_process_user_tf.save

        end

        if params[('tf_'+num_tf_t.to_s+'_what').to_sym].blank? && params[('tf_'+num_tf_t.to_s+'_how').to_sym].blank? && params[('tf_'+num_tf_t.to_s+'_when').to_sym].blank?

        elsif params[('tf_'+num_tf_t.to_s+'_what').to_sym].blank? || params[('tf_'+num_tf_t.to_s+'_how').to_sym].blank? || params[('tf_'+num_tf_t.to_s+'_when').to_sym].blank?
          hay_error = true
        end

      end


    end

    if hay_error

      @hr_process_user.feedback_confirmado = false

    else

      @hr_process_user.feedback_date = lms_time

    end

    @hr_process_user.save

    if @hr_process.step_feedback_confirmation_mail?
      LmsMailer.informar_valuador_feedback_confirmado(@hr_process_user.user, @hr_process, @company).deliver
    end

    if hay_error
      flash[:danger] = t('activerecord.error.model.hr_process_assessment.feedback_mode_2_error')
    else
      flash[:success] = t('activerecord.success.model.hr_process_assessment.feedback_ok')
    end

    redirect_to pe_feedback_new_feedback_path(@hr_process_user)

  end

  def final_rep_pdf

    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    is_boss = true

    generate_final_results_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations, is_boss

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def final_rep_only_boss_pdf

    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_final_results_only_boss_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

  def get_data_1_2
    @pe_process = PeProcess.find(params[:pe_process_id])
  end

  def get_data_2_2
    @pe_member = PeMember.find(params[:pe_member_id])
    @pe_process = @pe_member.pe_process
   #@company = session[:company]
  end

  def validate_open_process_2

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_is_the_feedback_provider

    unless @pe_member.feedback_provider_id == user_connected.id
      flash[:danger] = t('activerecord.error.model.pe_process_feedback.is_not_the_feedback_provider')
      redirect_to root_path
    end

  end

  def validate_can_download_rep_detailed_final_results_pdf
    unless @pe_process.step_feedback_show_rep_dfr_pdf
      flash[:danger] = t('activerecord.error.model.pe_process_feedback.cant_download_rep_dfr')
      redirect_to root_path
    end
  end

    ####

    def get_data_1
      @hr_process = HrProcess.find(params[:hr_process_id])
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
    end

    def get_data_2
      @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
      @hr_process = @hr_process_user.hr_process
      @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)
    end

    def validate_open_process

      unless @hr_process.abierto && @hr_process.fecha_inicio < lms_time && @hr_process.fecha_fin > lms_time
        flash[:danger] = t('activerecord.error.model.hr_process_assessment.out_of_time')
        redirect_to root_path
      end

    end

    def validate_access_to_evaluate

      unless @hr_process_user_connected.has_to_assess_user?(@hr_process_user) || @hr_process_user.id == @hr_process_user_connected.id

        flash[:danger] = t('security.no_access_hr_perform_evaluation')
        redirect_to root_path

      end

    end


end
