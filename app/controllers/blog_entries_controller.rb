class BlogEntriesController < ApplicationController

  before_filter :authenticate_user
  before_filter :validate_write_access

  # GET /blog_entries/new
  # GET /blog_entries/new.json
  def new
    @blog_entry = BlogEntry.new(user_id: @user_owner.id, blog_form_id: @blog_form.id)

    @blog_entry.blog_form.blog_form_items.each do |item|
      temp_item = @blog_entry.blog_entry_items.build
      temp_item.blog_form_item = item
    end
  end

  # POST /blog_entries
  # POST /blog_entries.json
  def create
    @blog_entry = BlogEntry.new(params[:blog_entry])
    @blog_entry.user = @user_owner
    @blog_entry.blog_form = @blog_form
    @blog_entry.registered_by_user_id = user_connected.id
    @blog_entry.registered_at = lms_time
    if @blog_entry.save
      flash[:success] = 'El registro fue creado correctamente'
      if @user_owner == user_connected
        redirect_to show_user_profile_path
      else
        redirect_to my_people_mi_colaborador_path @blog_entry.user
      end
    else
      flash[:danger] = 'El registro no fue creado correctamente'
      if @user_owner == user_connected
        redirect_to new_my_blog_entry_path(@blog_entry.blog_form)
      else
        redirect_to new_blog_entry_path(@blog_entry.blog_form, @blog_entry.user)
      end

    end
  end

  # DELETE /blog_entries/1
  # DELETE /blog_entries/1.json
  def destroy
    blog_entry = BlogEntry.find(params[:id])
    user = blog_entry.user

    if blog_entry.registered_by_user == user_connected
      blog_entry.destroy
      flash[:success] = 'El registro fue eliminado correctamente'
    else
      flash[:danger] = 'El registro no fue eliminado correctamente'
    end

    if @user_owner == user_connected
      redirect_to show_user_profile_path
    else
      redirect_to my_people_mi_colaborador_path user
    end

  end

  private

  def validate_write_access
    entry = BlogEntry.find(params[:id]) if params[:id] # para 'destroy'
    @blog_form = entry ? entry.blog_form : BlogForm.find(params[:blog_form_id])
    @user_owner = entry ? entry.user : (params[:user_id] ? User.find(params[:user_id]) : user_connected)
    @profile = @blog_form.reader_writer_profile(@user_owner, user_connected)
    # puts @profile.to_s + ' -*-*-*-*--'
    # puts @user_owner.inspect
    # puts @blog_form.profile_has_permission(1, @profile).to_s + ' +-+-+-+-+-'
    unless @blog_form.profile_has_permission(1, @profile)
      flash[:danger] = 'No tiene los privilegios necesarios para acceder a esta bitácora'
      redirect_to root_path
    end
  end
end
