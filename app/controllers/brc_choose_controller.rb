class BrcChooseController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:show, :choose, :upload_file, :download_file, :delete_file]
  before_filter :get_data_2, only: [:download_file, :delete_file]

  before_filter :validate_open_process, only: [:show, :choose, :upload_file, :download_file, :delete_file]
  before_filter :validate_can_choose, only: [:choose]
  before_filter :validate_can_manipulate, only: [:download_file, :delete_file]


  def list_processes
    @brc_processes = user_connected.active_brc_processes_is_member
  end

  def show

    @t_50 = @brc_member.sum_cohades_diff_50_month

  end

  def choose

    params[:brc_member][:bonus_apv] = fix_form_floats params[:brc_member][:bonus_apv] if params[:brc_member][:bonus_apv]
    params[:brc_member][:bonus_convenio] = fix_form_floats params[:brc_member][:bonus_convenio] if params[:brc_member][:bonus_convenio]

    @brc_member.choose_by_own = true
    @brc_member.choose_date = lms_time

    if @brc_member.update_attributes(params[:brc_member])
      flash[:success] = t('activerecord.success.model.brc_member.choose_ok')
      redirect_to brc_choose_show_path @brc_process
    else
      @brc_member.status_choose = false
      @t_50 = @brc_member.sum_cohades_diff_50_month
      render action: 'show'
    end

  end

  def upload_file

    @brc_member_file = @brc_member.brc_member_files.build

    if params[:upload]

      @brc_member_file = BrcMemberFile.new(params[:brc_member_file])
      @brc_member_file.brc_member = @brc_member

      if @brc_member_file.save

        extension = File.extname(params[:upload]['datafile'].original_filename)
        extension.slice! 0

        @brc_member_file.set_file_name extension

        @brc_member_file.save

        directory = @company.directorio+'/'+@company.codigo+'/brc/'+@brc_process.id.to_s+'/'+@brc_member.id.to_s+'/'

        FileUtils.mkdir_p directory unless File.directory?(directory)

        name = @brc_member_file.file_name
        path = File.join(directory, name)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        flash[:success] = t('activerecord.success.model.brc_member_file.create_ok')
        redirect_to brc_choose_show_path(@brc_process)
      else
        render action: 'upload_file'
      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def download_file
    file = @company.directorio+'/'+@company.codigo+'/brc/'+@brc_process.id.to_s+'/'+@brc_member.id.to_s+'/'+@brc_member_file.file_name

    send_file file,
              filename: @brc_member_file.file_name,
              type: 'application/octet-stream',
              disposition: 'attachment'
  end

  def delete_file

    if @brc_member_file.file_name

      f = @company.directorio+'/'+@company.codigo+'/brc/'+@brc_process.id.to_s+'/'+@brc_member.id.to_s+'/'+@brc_member_file.file_name

      File.delete(f) if File.exist?(f)

    end

    @brc_member_file.destroy

    flash[:success] = t('activerecord.success.model.brc_member_file.delete_ok')
    redirect_to brc_choose_show_path(@brc_process)
  end

  private

  def get_data_1
    @brc_process = BrcProcess.find(params[:brc_process_id])
    @brc_member = user_connected.brc_member @brc_process

  end

  def get_data_2
    @brc_member_file = BrcMemberFile.find(params[:brc_member_file_id])

  end

  def validate_open_process

    unless @brc_process.active
      flash[:danger] = t('activerecord.error.model.brc_process.not_active')
      redirect_to root_path
    end

  end

  def validate_can_choose
    if !@brc_member.status_def || !@brc_member.status_val || !@brc_member.status_bon_cal || lms_time > @brc_process.choose_date || @brc_member.brc_member_files.size == 0
      flash[:danger] = t('activerecord.error.model.brc_process.cant_choose')
      redirect_to root_path
    end
  end

  def validate_can_manipulate
    if @brc_member_file.brc_member != @brc_member
      flash[:danger] = t('activerecord.error.model.brc_process.cant_choose')
      redirect_to root_path
    end
  end

end
