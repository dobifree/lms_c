class JmProfileFormsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /jm_profile_forms
  # GET /jm_profile_forms.json
  def index
    @jm_profile_forms = JmProfileForm.all
  end

  # GET /jm_profile_forms/1
  # GET /jm_profile_forms/1.json
  def show
    @jm_profile_form = JmProfileForm.find(params[:id])
  end

  # GET /jm_profile_forms/new
  # GET /jm_profile_forms/new.json
  def new
    @jm_profile_form = JmProfileForm.new
  end

  # GET /jm_profile_forms/1/edit
  def edit
    @jm_profile_form = JmProfileForm.find(params[:id])
  end

  # POST /jm_profile_forms
  # POST /jm_profile_forms.json
  def create
    @jm_profile_form = JmProfileForm.new(params[:jm_profile_form])

    if @jm_profile_form.save
      flash[:success] = 'El formulario fue creado satisfactoriamente'
      redirect_to @jm_profile_form
    else
      flash.now[:danger] = 'El formulario no puedo ser creado satisfactoriamente'
      render action: 'new'
    end
  end

  # PUT /jm_profile_forms/1
  # PUT /jm_profile_forms/1.json
  def update
    @jm_profile_form = JmProfileForm.find(params[:id])

    if @jm_profile_form.update_attributes(params[:jm_profile_form])
      flash[:success] = 'El formulario fue actualizado satisfactoriamente'
      redirect_to @jm_profile_form
    else
      flash.now[:danger] = 'El formulario no puedo ser actualizado satisfactoriamente'

      if params[:jm_profile_form][:jm_profile_chars_attributes]
        render action: 'manage_chars'
      else
        render action: 'edit'
      end



    end
  end

  # DELETE /jm_profile_forms/1
  # DELETE /jm_profile_forms/1.json
  def destroy
    @jm_profile_form = JmProfileForm.find(params[:id])
    @jm_profile_form.destroy

    flash[:success] = 'El formulario fue eliminado satisfactoriamente'
    redirect_to jm_profile_forms_path
  end



  def manage_chars
    @jm_profile_form = JmProfileForm.find(params[:id])
  end


  def preview_form
    @jm_profile_form = JmProfileForm.find(params[:id])
    @layout = params[:tmp_edited_layout] ? params[:tmp_edited_layout] : @jm_profile_form.layout
    @jm_candidate = JmCandidate.new
    @filling_profile = true

    render :layout => false
  end


  def edit_visibility_chars
    @jm_profile_form = JmProfileForm.find(params[:id])
  end

end
