class PlanningProgrammingIndicatorsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_jefe_capacitacion_training_analyst, only: [:listar_procesos, :ver_proceso, :ver_unidad, :ver_consolidado]

  def listar_procesos

    @planning_processes = PlanningProcess.all

  end

  def ver_proceso

    @planning_process = PlanningProcess.find(params[:planning_process_id])


  end

  def ver_unidad

    @planning_process = PlanningProcess.find(params[:planning_process_id])
    @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])


  end

  def ver_consolidado

    @planning_process = PlanningProcess.find(params[:planning_process_id])


  end

  private

    def verify_jefe_capacitacion_training_analyst

      permiso = true if user_connected.jefe_capacitacion? || user_connected.training_analysts.length > 0

      unless permiso
        flash[:danger] = t('security.no_access_planning_programming_indicators')
        redirect_to root_path
      end

    end

end
