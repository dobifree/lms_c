class LiqItemsController < ApplicationController
  # GET /liq_items
  # GET /liq_items.json
  def index
    @liq_items = LiqItem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @liq_items }
    end
  end

  # GET /liq_items/1
  # GET /liq_items/1.json
  def show
    @liq_item = LiqItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @liq_item }
    end
  end

  # GET /liq_items/new
  # GET /liq_items/new.json
  def new
    @liq_item = LiqItem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @liq_item }
    end
  end

  # GET /liq_items/1/edit
  def edit
    @liq_item = LiqItem.find(params[:id])
  end

  # POST /liq_items
  # POST /liq_items.json
  def create
    @liq_item = LiqItem.new(params[:liq_item])

    respond_to do |format|
      if @liq_item.save
        format.html { redirect_to @liq_item, notice: 'Liq item was successfully created.' }
        format.json { render json: @liq_item, status: :created, location: @liq_item }
      else
        format.html { render action: "new" }
        format.json { render json: @liq_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /liq_items/1
  # PUT /liq_items/1.json
  def update
    @liq_item = LiqItem.find(params[:id])

    respond_to do |format|
      if @liq_item.update_attributes(params[:liq_item])
        format.html { redirect_to @liq_item, notice: 'Liq item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @liq_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /liq_items/1
  # DELETE /liq_items/1.json
  def destroy
    @liq_item = LiqItem.find(params[:id])
    @liq_item.destroy

    respond_to do |format|
      format.html { redirect_to liq_items_url }
      format.json { head :no_content }
    end
  end
end
