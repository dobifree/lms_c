class UserInformationController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_training_analyst_jefe_capacitacion

  def search

    @user = User.new
    @users = {}

    @characteristics = Characteristic.find_all_by_publica(true)
    @characteristics_prev = {}
    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        #params[:characteristic][characteristic.id.to_s.to_sym].slice! 0 if params[:characteristic]

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]


          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND valor = "'+@characteristics_prev[characteristic.id]+'" '
=begin
          @characteristics_prev[characteristic.id].each_with_index do |valor,index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'valor = "'+valor+'" '

          end

          queries[n_q] += ' ) '
=end
          n_q += 1

        end

      end

      params[:user][:codigo] ='%'  if params[:user] && params[:user][:codigo].blank?
      params[:user][:apellidos] ='%'  if params[:user] && params[:user][:apellidos].blank?
      params[:user][:nombre] ='%'  if params[:user] && params[:user][:nombre].blank?

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank? || !params[:user][:codigo].blank?

        if queries.length > 0

          queries.each_with_index do |query,index|

            if index == 0

              @users = User.joins(:user_characteristics).where(
                  'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query,
                  '%'+params[:user][:codigo]+'%',
                  '%'+params[:user][:apellidos]+'%',
                  '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

            else

              lista_users_ids = @users.map {|u| u.id }

              @users = User.joins(:user_characteristics).where(
                  'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query+' AND users.id IN (?) ',
                  '%'+params[:user][:codigo]+'%',
                  '%'+params[:user][:apellidos]+'%',
                  '%'+params[:user][:nombre]+'%',
                  lista_users_ids).order('apellidos, nombre')

            end

          end

        else

          @users = User.where(
              'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
              '%'+params[:user][:codigo]+'%',
              '%'+params[:user][:apellidos]+'%',
              '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        end

      end

    end

    params[:user][:apellidos] ='' if params[:user] && params[:user][:apellidos] == '%'
    params[:user][:nombre] ='' if params[:user] && params[:user][:nombre] == '%'
    params[:user][:codigo] ='' if params[:user] && params[:user][:codigo] == '%'

    @user = User.new(params[:user])

  end

  def user_profile

    @user = User.find(params[:id])
   #@company = session[:company]

  end


  private

    def verify_training_analyst_jefe_capacitacion

      permiso = false

      permiso = true if user_connected.training_analysts.length > 0

      permiso = true if user_connected.jefe_capacitacion?

      unless permiso
        flash[:danger] = t('security.no_access_user_information')
        redirect_to root_path
      end

    end
=begin
    def verify_jefe_capacitacion

      unless user_connected.jefe_capacitacion?
        flash[:danger] = t('security.no_access_user_information')
        redirect_to root_path
      end

    end
=end
end
