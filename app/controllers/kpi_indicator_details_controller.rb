class KpiIndicatorDetailsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_dashboard_1, only:[:new]
  before_filter :verify_access_manage_dashboard_2, only:[:create]
  before_filter :verify_access_manage_dashboard_3, only:[:edit, :update]

  def index
    @kpi_indicator_details = KpiIndicatorDetail.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @kpi_indicator_details }
    end
  end

  def show
    @kpi_indicator_detail = KpiIndicatorDetail.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @kpi_indicator_detail }
    end
  end

  def new

    @kpi_indicator_detail = @kpi_indicator.kpi_indicator_details.build
    @kpi_indicator_detail.measured_at = params[:date]

    @kpi_dashboard = @kpi_indicator.kpi_set.kpi_dashboard

  end

  def create

    @kpi_indicator_detail = KpiIndicatorDetail.new(params[:kpi_indicator_detail])

    if @kpi_indicator_detail.save

      @kpi_indicator_detail.kpi_indicator_ranks.destroy_all

      if @kpi_indicator_detail.kpi_indicator.indicator_type == 4

        params[:kpi_indicator_rank].each do |kpi_indicator_rank|
          r = @kpi_indicator_detail.kpi_indicator_ranks.build
          r.goal = fix_form_floats kpi_indicator_rank[:goal]
          r.percentage = fix_form_floats kpi_indicator_rank[:percentage]
          r.save
        end

        @kpi_indicator_detail.reload
        @kpi_indicator_detail.set_goal_rank
        @kpi_indicator_detail.save

      end

      kpi_measurement = @kpi_indicator_detail.kpi_indicator.kpi_measurements.where('measured_at = ?',@kpi_indicator_detail.measured_at).first

      if kpi_measurement && kpi_measurement.save

        kpi_set = @kpi_indicator_detail.kpi_indicator.kpi_set
        kpi_set.set_last_updates lms_time
        kpi_set.save

      end

      kpi_indicator = @kpi_indicator_detail.kpi_indicator

      kpi_indicator.set_final_result
      kpi_indicator.save

      if kpi_indicator.kpi_subset

        kpi_subset = kpi_indicator.kpi_subset

        kpi_subset.reload

        kpi_subset.set_percentage

        kpi_subset.save

      end

      kpi_set = kpi_indicator.kpi_set

      kpi_set.set_percentage

      kpi_set.save

      flash[:success] =  t('activerecord.success.model.kpi_indicator.create_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator_detail.kpi_indicator.kpi_set.kpi_dashboard
    else
      @kpi_indicator = @kpi_indicator_detail.kpi_indicator
      @kpi_dashboard = @kpi_set.kpi_dashboard
      render action: 'new'
    end

  end


  def edit

    @kpi_indicator = @kpi_indicator_detail.kpi_indicator
    @kpi_dashboard = @kpi_indicator.kpi_set.kpi_dashboard

  end


  def update

    if @kpi_indicator_detail.update_attributes(params[:kpi_indicator_detail])

      @kpi_indicator_detail.kpi_indicator_ranks.destroy_all

      if @kpi_indicator_detail.kpi_indicator.indicator_type == 4

        params[:kpi_indicator_rank].each do |kpi_indicator_rank|
          r = @kpi_indicator_detail.kpi_indicator_ranks.build
          r.goal = fix_form_floats kpi_indicator_rank[:goal]
          r.percentage = fix_form_floats kpi_indicator_rank[:percentage]
          r.save
        end

        @kpi_indicator_detail.reload
        @kpi_indicator_detail.set_goal_rank
        @kpi_indicator_detail.save

      end

      kpi_measurement = @kpi_indicator_detail.kpi_indicator.kpi_measurements.where('measured_at = ?',@kpi_indicator_detail.measured_at).first

      if kpi_measurement && kpi_measurement.save

        kpi_set = @kpi_indicator_detail.kpi_indicator.kpi_set
        kpi_set.set_last_updates lms_time
        kpi_set.save

      end

      kpi_indicator = @kpi_indicator_detail.kpi_indicator

      kpi_indicator.set_final_result
      kpi_indicator.save

      if kpi_indicator.kpi_subset

        kpi_subset = kpi_indicator.kpi_subset

        kpi_subset.reload

        kpi_subset.set_percentage

        kpi_subset.save

      end

      kpi_set = kpi_indicator.kpi_set

      kpi_set.set_percentage

      kpi_set.save


      flash[:success] =  t('activerecord.success.model.kpi_indicator.update_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator_detail.kpi_indicator.kpi_set.kpi_dashboard

    else
      @kpi_indicator = @kpi_indicator_detail.kpi_indicator
      @kpi_dashboard = @kpi_set.kpi_dashboard
      render action: 'edit'
    end

  end

  # DELETE /kpi_indicator_details/1
  # DELETE /kpi_indicator_details/1.json
  def destroy
    @kpi_indicator_detail = KpiIndicatorDetail.find(params[:id])
    @kpi_indicator_detail.destroy

    respond_to do |format|
      format.html { redirect_to kpi_indicator_details_url }
      format.json { head :no_content }
    end
  end

  private

    def verify_access_manage_dashboard_1

      @kpi_indicator = KpiIndicator.find(params[:kpi_indicator_id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',@kpi_indicator.kpi_set.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

    def verify_access_manage_dashboard_2

      kpi_indicator = KpiIndicator.find(params[:kpi_indicator_detail][:kpi_indicator_id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?', kpi_indicator.kpi_set.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

    def verify_access_manage_dashboard_3

      @kpi_indicator_detail = KpiIndicatorDetail.find(params[:id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?', @kpi_indicator_detail.kpi_indicator.kpi_set.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

end
