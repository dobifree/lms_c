class KpiIndicatorsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_dashboard_1, only:[:new]
  before_filter :verify_access_manage_dashboard_2, only:[:create]
  before_filter :verify_access_manage_dashboard_3, only:[:edit, :edit_goal, :update, :update_goal, :update_value, :destroy]


  def new

    @kpi_dashboard = @kpi_set.kpi_dashboard
    @kpi_indicator = @kpi_set.kpi_indicators.build


  end

  def edit

    @kpi_set = @kpi_indicator.kpi_set
    @kpi_dashboard = @kpi_set.kpi_dashboard


  end

  def edit_goal

    @kpi_set = @kpi_indicator.kpi_set
    @kpi_dashboard = @kpi_set.kpi_dashboard

  end

  def create

    @kpi_indicator = KpiIndicator.new(params[:kpi_indicator])

    if @kpi_indicator.save

      flash[:success] =  t('activerecord.success.model.kpi_indicator.create_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard
    else
      @kpi_set = @kpi_indicator.kpi_set
      @kpi_dashboard = @kpi_set.kpi_dashboard
      render action: 'new'
    end

  end

  def update

    if @kpi_indicator.update_attributes(params[:kpi_indicator])


      @kpi_indicator.kpi_measurements.each do |kpi_measurement|
        kpi_measurement.save
      end

      @kpi_indicator.set_final_result
      @kpi_indicator.save

      kpi_set = @kpi_indicator.kpi_set

      kpi_set.set_percentage

      kpi_set.save

      kpi_set.kpi_subsets.each do |kpi_subset|

        kpi_subset.set_percentage

        kpi_subset.save

      end

      flash[:success] =  t('activerecord.success.model.kpi_indicator.update_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard
    else
      @kpi_set = @kpi_indicator.kpi_set
      @kpi_dashboard = @kpi_set.kpi_dashboard
      render action: 'edit'
    end

  end

  def update_goal

    if @kpi_indicator.update_attributes(params[:kpi_indicator])

      @kpi_indicator.kpi_indicator_ranks.destroy_all

      if @kpi_indicator.indicator_type == 4

        params[:kpi_indicator_rank].each do |kpi_indicator_rank|
          r = KpiIndicatorRank.new(kpi_indicator_rank)
          r.kpi_indicator = @kpi_indicator
          r.save
        end

        @kpi_indicator.reload
        @kpi_indicator.set_goal_rank
        @kpi_indicator.save

      end

      @kpi_indicator.set_final_result
      @kpi_indicator.save

      kpi_set = @kpi_indicator.kpi_set

      kpi_set.set_percentage

      kpi_set.save

      kpi_set.kpi_subsets.each do |kpi_subset|

        kpi_subset.set_percentage

        kpi_subset.save

      end

      flash[:success] =  t('activerecord.success.model.kpi_indicator.update_goal_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard

    else
      @kpi_dashboard = @kpi_indicator.kpi_set.kpi_dashboard
      render action: 'edit_goal'
    end


  end

  def update_value

    if @kpi_indicator.update_attributes(params[:kpi_indicator])

      @kpi_indicator.set_final_result
      @kpi_indicator.save

      kpi_set = @kpi_indicator.kpi_set

      kpi_set.set_percentage

      kpi_set.save

      kpi_set.kpi_subsets.each do |kpi_subset|

        kpi_subset.set_percentage

        kpi_subset.save

      end

      flash[:success] =  t('activerecord.success.model.kpi_indicator.update_value_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard

    else
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard
    end

  end

  def destroy

    kpi_dashboard = @kpi_indicator.kpi_set.kpi_dashboard

    @kpi_indicator.destroy

    dt_i = Array.new
    dt_f = Array.new

    prev_month = nil

    (kpi_dashboard.from..kpi_dashboard.to).each do |date|

      unless prev_month == date.strftime('%m').to_i

        prev_month = date.strftime('%m').to_i

        dt_i.push date
        dt_f.push date + 1.month

      end

    end

    kpi_dashboard.reload

    kpi_dashboard.kpi_sets.each do |kpi_set|

      dt_i.each do |fecha|

        KpiManagingController.helpers.update_set_measurement(kpi_set, fecha)

      end

      kpi_set.kpi_indicators.each do |kpi_indicator|

        p = 0
        s = 0

        kpi_indicator.kpi_measurements.each do |kpi_measurement|

          if kpi_measurement.percentage
            p += 1
            s += kpi_measurement.percentage
          end

        end

        if p > 0
          kpi_indicator.percentage = s/p
        else
          kpi_indicator.percentage = nil
        end

        kpi_indicator.save

      end

      kpi_set.reload

      p = 0
      s = 0

      kpi_set.kpi_indicators.each do |kpi_indicator|

        if kpi_indicator.percentage

          p += kpi_indicator.weight
          s += kpi_indicator.percentage*kpi_indicator.weight

        end

      end

      if p > 0
        kpi_set.percentage = s/p
      else
        kpi_set.percentage = nil
      end

      kpi_set.save


    end

    flash[:success] =  t('activerecord.success.model.kpi_indicator.delete_ok')
    redirect_to kpi_managing_dashboard_manage_indicators_path kpi_dashboard

  end

  private

    def verify_access_manage_dashboard_1

      @kpi_set = KpiSet.find(params[:kpi_set_id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',@kpi_set.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

    def verify_access_manage_dashboard_2

      kpi_set = KpiSet.find(params[:kpi_indicator][:kpi_set_id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',kpi_set.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

    def verify_access_manage_dashboard_3

      @kpi_indicator = KpiIndicator.find(params[:id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',@kpi_indicator.kpi_set.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end



end
