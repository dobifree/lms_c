class BrgUsersController < ApplicationController
  include BrgModule

  before_filter :authenticate_user
  before_filter :active_module
  before_filter :brg_manager
  before_filter :get_process
  before_filter :verify_open_process_process

  def massive_upload_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_process_users file, @process.id
        flash.now[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = 'Se ha encontrado algunos registros con errores'
      end
    else
      flash.now[:danger] = message
    end
    render 'massive_upload'
  end

  def massive_upload; end
end