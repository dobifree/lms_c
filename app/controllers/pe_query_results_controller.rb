class PeQueryResultsController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:people_list]
  before_filter :get_data_2, only: [:show]

  def people_list
    @pe_question_comment = PeQuestionComment.new



  end

  def show
    @pe_question_comment = PeQuestionComment.new
  end

  def get_data_1

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_member_connected = @pe_process.pe_members.where('user_id = ?', user_connected.id).first

    @pe_rel = @pe_process.pe_rel_by_id 0

    pe_rel_boss = @pe_process.pe_rel_by_id 1

    @pe_member_rel_boss = @pe_member_connected.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first

    @pe_member_rel = @pe_member_connected.pe_member_rels_is_evaluated_as_pe_rel(@pe_rel).first if @pe_member_connected && @pe_rel

   #@company = session[:company]

  end

  def get_data_2
    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_rel = @pe_member_rel.pe_rel
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member = @pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_connected = nil

    @pe_process.pe_members.where('user_id = ?', user_connected.id).each do |pe_member_connected|

      if pe_member_connected.id == @pe_member_evaluator.id
        @pe_member_connected = @pe_member_evaluator
      end

    end



   #@company = session[:company]

  end

end
