class BenCellphoneProcessesController < ApplicationController
  include BenCellphoneProcessesHelper

  before_filter :authenticate_user
  before_filter :verify_access_manage_module
  before_filter :get_characteristics, only: [:not_charge_to_payroll_modal,
                                             :not_charge_to_payroll_modal_step2,
                                             :upload_equipment_bill,
                                             :charge_to_payroll,
                                             :add_bill_item_modal_step2,
                                             :not_charge_to_payroll,
                                             :charge_or_not_to_payroll,
                                             :show,
                                             :upload_plan ]

  before_filter :verify_open_process, only: [:delete_data,
                                             :upload_equipment_bill,
                                             :upload_plan,
                                             :to_validation,
                                             :update, :edit]

  def update_messages_to_payroll
    @ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])


    @not_processed = BenCellphoneBillItem.where(:ben_cellphone_process_id => params[:ben_cellphone_process_id], :ben_user_cellphone_process_id => nil).joins(:ben_cellphone_number => :user).where(:ben_cellphone_numbers => {:active => true}, :users => {:activo => true})
    @not_processed = @not_processed.size
    @not_matched_and_charge0 = BenCellphoneBillItem.where(:ben_cellphone_process_id => params[:ben_cellphone_process_id]).joins(:ben_cellphone_number => :user).where('(ben_cellphone_numbers.active=0 or users.activo=0) and ben_cellphone_bill_items.not_charge_to_payroll=0')
    @not_matched_and_charge1 = BenCellphoneBillItem.where(:ben_cellphone_process_id => params[:ben_cellphone_process_id], :ben_cellphone_number_id => nil, :not_charge_to_payroll => 0)
    @not_matched_and_charge = @not_matched_and_charge0.size + @not_matched_and_charge1.size

    render :layout => false
  end

  def complete_excel
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people
    #@ben_cellphone_numbers = BenCellphoneNumber.includes(:user).order('users.apellidos asc, users.nombre asc, number asc')
    @ben_cellphone_bill_items = BenCellphoneBillItem.where(:ben_cellphone_process_id => params[:ben_cellphone_process_id])
    @ben_cellphone_bill_items = @ben_cellphone_bill_items.sort_by { |number| number.cellphone_number}
    ben_cellphone_process = @ben_cellphone_bill_items.first.ben_cellphone_process
    if ben_cellphone_process.charged_to_payroll
      filename = 'Proceso '+t('date.month_names')[ben_cellphone_process.month].to_s+' '+ben_cellphone_process.year.to_s+' - cerrado.xlsx'
    else
      filename = 'Proceso '+t('date.month_names')[ben_cellphone_process.month].to_s+' '+ben_cellphone_process.year.to_s+' - abierto.xlsx'
    end

    respond_to do |format|
      format.xlsx { render xlsx: 'ben_cellphone_process_complete_excel.xlsx', filename: filename}
      format.html { render index }
    end
  end


  def bills_detail
    @user_cellphone_process = BenUserCellphoneProcess.find(params[:ben_user_cellphone_process_id])
    @ben_cellphone_process = @user_cellphone_process.ben_cellphone_process
    render :layout => false
  end

  def not_charge_to_payroll_modal
    @ben_cellphone_bill_item = BenCellphoneBillItem.find(params[:ben_cellphone_bill_item_id])
    render :layout => false
  end

  def not_charge_to_payroll_modal_step2
    @ben_cellphone_bill_item = BenCellphoneBillItem.find(params[:ben_cellphone_bill_item_id])

    if defined?(params[:text_area])
      @ben_cellphone_bill_item.update_attributes(:not_charge_to_payroll_description => params[:text_area],
                                                 :not_charge_to_payroll => true,
                                                 :not_charge_to_payroll_by_user_id => user_connected,
                                                 :not_charge_to_payroll_at => lms_time)
    else
      @ben_cellphone_bill_item.update_attributes(:not_charge_to_payroll_description => params[:ben_cellphone_bill_item][:not_charge_to_payroll_description],
                                                 :not_charge_to_payroll => true,
                                                 :not_charge_to_payroll_by_user_id => user_connected,
                                                 :not_charge_to_payroll_at => lms_time)
    end

    @ben_cellphone_bill_item.not_charge_to_payroll_description = params[:ben_cellphone_bill_item][:not_charge_to_payroll_description] if defined?(params[:ben_cellphone_bill_item][:not_charge_to_payroll_description])
    @ben_cellphone_bill_item.save

    user_process = @ben_cellphone_bill_item.ben_user_cellphone_process
      if user_process
        if @ben_cellphone_bill_item.registered_manually
          user_process.registered_manually_total -= @ben_cellphone_bill_item.net_with_taxes
        else
          user_process.total_bill -= @ben_cellphone_bill_item.net_with_taxes
          user_process.equipment_bill -= @ben_cellphone_bill_item.net_with_taxes if @ben_cellphone_bill_item.bill_type == 1
          user_process.plan_bill -= @ben_cellphone_bill_item.net_with_taxes if @ben_cellphone_bill_item.bill_type == 0
          user_process.applied_subsidy = user_process.configured_subsidy > user_process.total_bill ? user_process.total_bill : user_process.configured_subsidy
          user_process.applied_subsidy = 0 if user_process.total_bill < 0
          user_process.total_charged = user_process.total_bill - user_process.applied_subsidy
        end
        user_process.save
      else
        #show_process @ben_cellphone_bill_item.ben_cellphone_process.id
      end

    render :layout => false
  end

  def charge_to_payroll
    @ben_cellphone_bill_item = BenCellphoneBillItem.find(params[:ben_cellphone_bill_item_id])
    @ben_cellphone_bill_item.update_attributes(:not_charge_to_payroll_description => nil,
                                               :not_charge_to_payroll => false,
                                               :not_charge_to_payroll_by_user_id => user_connected,
                                               :not_charge_to_payroll_at => lms_time )

    user_process = @ben_cellphone_bill_item.ben_user_cellphone_process
    if user_process
      if @ben_cellphone_bill_item.registered_manually
        user_process.registered_manually_total += @ben_cellphone_bill_item.net_with_taxes
      else
        user_process.total_bill+=@ben_cellphone_bill_item.net_with_taxes
        user_process.equipment_bill += @ben_cellphone_bill_item.net_with_taxes if @ben_cellphone_bill_item.bill_type == 1
        user_process.plan_bill += @ben_cellphone_bill_item.net_with_taxes if @ben_cellphone_bill_item.bill_type == 0
        user_process.applied_subsidy = user_process.configured_subsidy > user_process.total_bill ? user_process.total_bill : user_process.configured_subsidy
        user_process.applied_subsidy = 0 if user_process.total_bill < 0
        user_process.total_charged = user_process.total_bill - user_process.applied_subsidy
      end
      user_process.save
    end
    #flash[:success] = t('views.ben_cellphone_process.shared.flash_messages.change_sucess')
    render :layout =>false
  end

  def add_bill_item_modal
    user_process = BenUserCellphoneProcess.find(params[:ben_user_cellphone_process_id])
    item = user_process.ben_cellphone_bill_items.first
    company_rut = item ? item.company_rut : ''
    @ben_cellphone_bill_item = BenCellphoneBillItem.new(:company_rut => company_rut,
                                                        :ben_user_cellphone_process_id => user_process.id )
    render :layout => false
  end

  def add_bill_item_modal_step2
    params[:ben_cellphone_bill_item][:net_with_taxes] = params[:ben_cellphone_bill_item][:net_with_taxes].blank? ? nil  : (fix_form_floats params[:ben_cellphone_bill_item][:net_with_taxes]).to_f.round(4)
    user_process = BenUserCellphoneProcess.find(params[:ben_user_cellphone_process_id])
    @ben_cellphone_bill_item = BenCellphoneBillItem.new(:ben_cellphone_process_id => user_process.ben_cellphone_process.id,
                                                        :ben_cellphone_number_id => user_process.ben_cellphone_number_id,
                                                        :ben_user_cellphone_process_id => user_process.id,
                                                        :bill_type => 2, #Siempre para registro manual
                                                        :registered_manually => true,
                                                        :registered_manually_at => lms_time,
                                                        :registered_manually_by_user_id => user_connected,
                                                        :cellphone_number => user_process.ben_cellphone_number.number,
                                                        :net_value => 0,
                                                        :net_total => 0,
                                                        :discount => 0)

    @ben_cellphone_bill_item.update_attributes(params[:ben_cellphone_bill_item])

      if @ben_cellphone_bill_item.save
        #Actualizar user_cellphone_process
        user_process.registered_manually_total += @ben_cellphone_bill_item.net_with_taxes
        user_process.save
      else

      end

    render :layout => false
  end

  def index
    @ben_cellphone_processes = BenCellphoneProcess.order('year desc, month desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ben_cellphone_processes }
    end
  end


  def no_matched_bills_errors_excel
    #ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])
    show_process (params[:ben_cellphone_process_id])
    respond_to do |format|
      format.xlsx { render xlsx: 'ben_cellphone_process_no_matched_bills_errors_excel.xlsx' , filename: t('views.ben_cellphone_process.shared.files_name.no_matched_bills_errors_excel') }
    end

  end


  def charge_to_payroll_excel
    ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])

    should_continue = true
    show_process params[:ben_cellphone_process_id]

    if !ben_cellphone_process.charged_to_payroll? and @no_match_bills.size > 0
      @no_match_bills.each do |bill_item|
        should_continue = false unless bill_item.not_charge_to_payroll?
      end
    end

    if should_continue
      unless ben_cellphone_process.charged_to_payroll
        ben_cellphone_process.charged_to_payroll = !ben_cellphone_process.charged_to_payroll?
        ben_cellphone_process.charged_to_payroll_by_user_id = user_connected.id
        ben_cellphone_process.charged_to_payroll_at = lms_time
        ben_cellphone_process.save
      end
      @ben_user_cellphone_processes = ben_cellphone_process.ben_user_cellphone_processes
      ben_cellphone_process.generated_registered_not_manually_excel_by_user_id = user_connected.id
      ben_cellphone_process.generated_registered_not_manually_excel_at = lms_time
      ben_cellphone_process.save
      respond_to do |format|
        format.xlsx { render xlsx: 'ben_cellphone_process_charge_to_payroll_excel.xlsx' , filename: t('views.ben_cellphone_process.shared.files_name.to_payroll') }
      end
    else
      flash[:danger] = t('views.ben_cellphone_process.shared.flash_messages.no_match_records_to_payroll')
      redirect_to ben_cellphone_process_just_added_bill_item2_path(ben_cellphone_process.ben_cellphone_bill_items.first)
    end

  end

  def registered_manually_excel
    ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])

    should_continue = true
    show_process params[:ben_cellphone_process_id]

    if !ben_cellphone_process.charged_to_payroll? and @no_match_bills.size > 0
      @no_match_bills.each do |bill_item|
        should_continue = false unless bill_item.not_charge_to_payroll?
      end
    end

    if should_continue
      unless ben_cellphone_process.charged_to_payroll
        ben_cellphone_process.charged_to_payroll = !ben_cellphone_process.charged_to_payroll?
        ben_cellphone_process.charged_to_payroll_by_user_id = user_connected.id
        ben_cellphone_process.charged_to_payroll_at = lms_time
        ben_cellphone_process.save
      end
      @ben_cellphone_bill_items = ben_cellphone_process.ben_cellphone_bill_items
      ben_cellphone_process.generated_registered_manually_excel_by_user_id = user_connected.id
      ben_cellphone_process.generated_registered_manually_excel_at = lms_time
      ben_cellphone_process.save

      respond_to do |format|
        filename = 'Notas de crédito_'+ben_cellphone_process.month.to_s+'/'+ben_cellphone_process.year.to_s+'.xlsx'
        format.xlsx { render xlsx: 'ben_cellphone_process_registered_manually_excel.xlsx' , filename: filename }
      end
    else
      flash[:danger] = t('views.ben_cellphone_process.shared.flash_messages.no_match_records_to_payroll')
      redirect_to ben_cellphone_process_just_added_bill_item2_path(ben_cellphone_process.ben_cellphone_bill_items.first)
    end

  end

  def delete_data
    ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])
    ben_cellphone_process.ben_cellphone_bill_items.destroy_all
    ben_cellphone_process.ben_user_cellphone_processes.destroy_all
    flash[:success] = t('views.ben_cellphone_process.shared.flash_messages.deleted_data')
    redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id], 0)
  end

  def close_open_process
    ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])
    show_process params[:ben_cellphone_process_id]
    if ben_cellphone_process.charged_to_payroll
      ben_cellphone_process.charged_to_payroll = false
      ben_cellphone_process.generated_registered_manually_excel_by_user = nil
      ben_cellphone_process.generated_registered_not_manually_excel_by_user = nil
      ben_cellphone_process.generated_registered_manually_excel_at = nil
      ben_cellphone_process.generated_registered_not_manually_excel_at = nil
      @no_match_bills.each do |bill|
        bill = BenCellphoneBillItem.find(bill.id)
        bill.bill_item_errors = nil
      end
      ben_cellphone_process.save
      reprocess params[:ben_cellphone_process_id]
      flash[:success] = t('views.ben_cellphone_process.shared.flash_messages.open_process')
    else
    unless @not_matched_and_charge > 0
      unless @not_processed > 0
        ben_cellphone_process.charged_to_payroll = true
        ben_cellphone_process.charged_to_payroll_by_user_id = user_connected.id
        ben_cellphone_process.charged_to_payroll_at = lms_time
        ben_cellphone_process.save

        @no_match_bills.each do |bill|
          bill = BenCellphoneBillItem.find(bill.id)
          errors = String.new
          bill.no_match_messages.each_with_index { |message, index|
            errors = errors.to_s+'-'+message[0].to_s if index > 0
            errors = message[0].to_s if index == 0
          }
          bill.bill_item_errors = errors.to_s
          bill.save
        end
        flash[:success] = t('views.ben_cellphone_process.shared.flash_messages.close_process')
      else
        flash[:danger] = t('views.ben_cellphone_process.shared.flash_messages.not_processed_records')
      end
    else
      flash[:danger] = t('views.ben_cellphone_process.shared.flash_messages.no_match_records_to_payroll')
    end
      end
    redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id],2)
  end

  def to_validation
    reprocess params[:ben_cellphone_process_id]
    flash[:success] = t('views.ben_cellphone_process.shared.flash_messages.reprocess_success')
    redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id],0)
  end

  def show
    show_process params[:ben_cellphone_process_id]
    @no_match_bills = @no_match_bills.sort_by { |number| number.cellphone_number}
    @tab = params[:tab] ? params[:tab] : 0

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ben_cellphone_process }
    end
  end

  def new
    @ben_cellphone_process = BenCellphoneProcess.new
    @ben_cellphone_process.year = Time.current.year

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ben_cellphone_process }
    end
  end

  def edit
    @ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])
  end

  def create
    @ben_cellphone_process = BenCellphoneProcess.new(params[:ben_cellphone_process])
    @ben_cellphone_process.charged_to_payroll_by_user = user_connected

    respond_to do |format|
      if @ben_cellphone_process.save
        format.html { redirect_to show_ben_cellphone_process_path(@ben_cellphone_process.id, 0) }
        format.json { render json: @ben_cellphone_process, status: :created, location: @ben_cellphone_process }
      else
        format.html { render action: "new" }
        format.json { render json: @ben_cellphone_process.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])

    respond_to do |format|
      if @ben_cellphone_process.update_attributes(params[:ben_cellphone_process])
        format.html { redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id],0) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ben_cellphone_process.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @ben_cellphone_process = BenCellphoneProcess.find(params[:id])
    #@ben_cellphone_process.destroy

    respond_to do |format|
      format.html { redirect_to ben_cellphone_processes_url }
      format.json { head :no_content }
    end
  end

  def upload_plan
    @ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])
    @error_index_plan = []
    @error_content_plan = []
    @error_message_plan = []

    if params[:upload]
      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'
        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5) + '.xlsx'
        archivo_temporal = directorio_temporal + nombre_temporal
        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        if its_safe_plan archivo_temporal, 'BASE'
          flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
          redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id], 0)
        else
          flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
          show_process @ben_cellphone_process.id
          @tab = 0
          render 'show'
        end
      else
        flash[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
        redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id], 0)
      end
    else
      flash[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
      redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id], 0)
    end
  end

  def upload_equipment_bill
    @ben_cellphone_process = BenCellphoneProcess.find(params[:ben_cellphone_process_id])
    @error_index_equipment_bill = []
    @error_content_equipment_bill = []
    @error_message_equipment_bill = []

    if params[:upload]
      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'
        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5) + '.xlsx'
        archivo_temporal = directorio_temporal + nombre_temporal
        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        if its_safe_equipment_bill archivo_temporal
          flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
          redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id], 0)
        else
          flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
          show_process @ben_cellphone_process.id
          @tab = 0
          render 'show'
        end

      else
        flash[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
        redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id], 0)
      end

    else
      flash[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
      redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id], 0)
    end
  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'ben_cel_sec', true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def get_characteristics
    @ben_cellphone_characteristics = BenCellphoneCharacteristic.search_people
  end

  def verify_open_process
    if BenCellphoneProcess.find(params[:ben_cellphone_process_id]).charged_to_payroll?
      flash[:danger] = t('views.ben_cellphone_process.shared.flash_messages.closed_process')
      redirect_to show_ben_cellphone_process_path(params[:ben_cellphone_process_id], 0)
    end
  end

end
