class ScholarshipApplicationsController < ApplicationController

  before_filter :authenticate_user

  # GET /scholarship_applications
  # GET /scholarship_applications.json
  def index
    @scholarship_applications = ScholarshipApplication.all
  end

  def go_to
    @scholarship_application = ScholarshipApplication.where(scholarship_process_id: params[:scholarship_process_id], subject_user_id: params[:subject_user_id]).first_or_initialize

    if validate_privileges(@scholarship_application.subject_user)
      if @scholarship_application.new_record?
        if @scholarship_application.scholarship_process.applicable?(lms_date)
          render 'new'
        else
          flash[:danger] = 'Este proceso ya no acepta postulaciones.'
          redirect_to root_path
        end
      else
        render 'show'
      end
    else
      flash[:danger] = 'No tiene los privilegios necesarios para ejecutar esta acción'
      redirect_to root_path
    end

  end

  # GET /scholarship_applications/1
  # GET /scholarship_applications/1.json
  def show
    @scholarship_application = ScholarshipApplication.find(params[:id])
    unless validate_privileges(@scholarship_application.subject_user)
      flash[:danger] = 'No tiene los privilegios necesarios para ejecutar esta acción'
      redirect_to root_path
    end
  end

  # GET /scholarship_applications/new
  # GET /scholarship_applications/new.json
  def new
    @scholarship_application = ScholarshipApplication.new
  end

  # GET /scholarship_applications/1/edit
  def edit
    @scholarship_application = ScholarshipApplication.find(params[:id])
    if !validate_privileges(@scholarship_application.subject_user, @scholarship_application.registered_by_user) || @scholarship_application.done?
      flash[:danger] = 'No tiene los privilegios necesarios para ejecutar esta acción'
      redirect_to root_path
    end
  end

  # POST /scholarship_applications
  # POST /scholarship_applications.json
  def create
    @scholarship_application = ScholarshipApplication.new(params[:scholarship_application])
    if validate_privileges(@scholarship_application.subject_user)
      @scholarship_application.registered_at = lms_time
      @scholarship_application.registered_by_user = user_connected
      if @scholarship_application.done?
        @scholarship_application.done_at = lms_time
      end
      if @scholarship_application.save
        if @scholarship_application.done?
          flash[:success] = 'La postulación ha sido guardada correctamente y enviada a evaluación.'
        else
          flash[:success] = 'Los datos de la postulación han sido guardados correctamente.'
        end
        redirect_to @scholarship_application
      else
        render 'new'
      end
    else
      flash[:danger] = 'No tiene los privilegios necesarios para ejecutar esta acción'
      redirect_to root_path
    end

  end

  # PUT /scholarship_applications/1
  # PUT /scholarship_applications/1.json
  def update
    @scholarship_application = ScholarshipApplication.find(params[:id])
    if validate_privileges(@scholarship_application.subject_user, @scholarship_application.registered_by_user) && !@scholarship_application.done?
      @scholarship_application.assign_attributes(params[:scholarship_application])
      if @scholarship_application.done?
        @scholarship_application.done_at = lms_time
      end

      if @scholarship_application.save
        if @scholarship_application.done?
          flash[:success] = 'La postulación ha sido guardada correctamente y enviada a evaluación.'
        else
          flash[:success] = 'Los datos de la postulación han sido guardados correctamente.'
        end
        redirect_to @scholarship_application
      else
        flash.now[:danger] = 'La postulación no pudo ser guardada correctamente.'
        render 'edit'
      end
    else
      flash[:danger] = 'No tiene los privilegios necesarios para ejecutar esta acción'
      redirect_to root_path
    end
  end

  # DELETE /scholarship_applications/1
  # DELETE /scholarship_applications/1.json
  def destroy
    @scholarship_application = ScholarshipApplication.find(params[:id])
    subject_user = @scholarship_application.subject_user
    if validate_privileges(@scholarship_application.subject_user) && !@scholarship_application.done?
      @scholarship_application.destroy
      flash[:success] = 'La postulación fue eliminada correctamente'
      redirect_to my_people_mi_colaborador_path(subject_user)
    else
      flash[:danger] = 'No tiene los privilegios necesarios para ejecutar esta acción'
      redirect_to root_path
    end

  end

  def validate_privileges(subject_user, creator_user = user_connected)
    user_connected.es_jefe_de(subject_user) && creator_user == user_connected
  end
end
