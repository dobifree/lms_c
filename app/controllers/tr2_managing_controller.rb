class Tr2ManagingController < ApplicationController

  before_filter :authenticate_user

  def list_program_instances
    @program_instances = ProgramInstance.where('active = ?', true)
  end

  def show_results

    @program_instance = ProgramInstance.find(params[:program_instance_id])
   #@company = session[:company]

  end

  def edit_results

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
    @program_instance = @program_course_instance.program_instance
   #@company = session[:company]

  end



  def update_results

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
    @program_instance = @program_course_instance.program_instance
   #@company = session[:company]

    @program_course_instance.user_courses.each do |uc|

      uc.nota = params[('nota_'+uc.id.to_s).to_sym]

      uc.porcentaje_avance = params[('asist_'+uc.id.to_s).to_sym]

      uc.iniciado = true

      uc.inicio = @program_course_instance.from_date
      uc.fin = @program_course_instance.to_date

      uc.finalizado = true
      uc.aprobado = false
      uc.aprobado = true if ((!@program_course_instance.minimum_assistance || (@program_course_instance.minimum_assistance && uc.porcentaje_avance >= @program_course_instance.minimum_assistance)) && (!@program_course_instance.minimum_grade || (@program_course_instance.minimum_grade && uc.nota && uc.nota >= @program_course_instance.minimum_grade)))

      uc.save

      define_results_program_instance(@program_instance, uc.enrollment)

    end

    flash[:success] = t('activerecord.success.model.training_results.update_results_ok')

    redirect_to tr2_show_results_path @program_instance

  end

  def edit_results_from_excel

    @program_instance = ProgramInstance.find(params[:program_instance_id])
   #@company = session[:company]

  end

  def update_results_from_excel

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        #check_students_data_for_enroll archivo_temporal
        @lineas_error = Array.new
        if @lineas_error.length > 0

          flash.now[:danger] = t('activerecord.error.model.training_results.update_results_from_excel_error')
          render 'edit_results_from_excel'

        else

          update_results_from_excel_ archivo_temporal, @program_course_instance

          flash[:success] = t('activerecord.success.model.training_results.update_results_ok')

          redirect_to tr2_edit_results_from_excel_path @program_course_instance.program_instance

        end

      else

        flash.now[:danger] = t('activerecord.error.model.training_results.update_results_from_excel_no_xlsx')
        render 'edit_results_from_excel'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.training_results.update_results_from_excel_no_file')
      render 'edit_results_from_excel'

    end



  end

  def download_excel_to_edit_results
    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])

    reporte_excel = xls_for_edit_results @program_course_instance

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_registro_resultados'+@program_course_instance.program_course.course.nombre+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def show_results_es

    @program_instance = ProgramInstance.find(params[:program_instance_id])
   #@company = session[:company]

  end

  def edit_results_es

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
    @program_instance = @program_course_instance.program_instance
   #@company = session[:company]

  end

  def update_results_es

    @program_course_instance = ProgramCourseInstance.find(params[:program_course_instance_id])
    @program_instance = @program_course_instance.program_instance
   #@company = session[:company]

    poll = @program_course_instance.polls.where('tipo_satisfaccion = ?', true).first

    if poll

      master_questions = poll.master_poll.master_poll_questions

      master_questions.each do |master_question|

        master_question.master_poll_alternatives.each do |alt|

          poll_summary = ProgramCoursePollSummary.where('program_course_instance_id = ? AND poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ? ',
                                                        @program_course_instance.id, poll.id, master_question.id, alt.id).first

          poll_summary.destroy if poll_summary

          poll_summary = ProgramCoursePollSummary.new
          poll_summary.program_course_instance = @program_course_instance
          poll_summary.poll = poll
          poll_summary.master_poll_question = master_question
          poll_summary.master_poll_alternative = alt
          poll_summary.numero_respuestas = 0

          if params[:poll_summary][alt.id.to_s.to_sym]

            poll_summary.numero_respuestas = params[:poll_summary][alt.id.to_s.to_sym]

          end

          poll_summary.save

        end

      end

    end

    flash[:success] = t('activerecord.success.model.training_results.update_results_es_ok')

    redirect_to tr2_show_results_es_path(@program_instance)

  end

  def finish_instance

    @program_instance = ProgramInstance.find(params[:program_instance_id])
   #@company = session[:company]

  end

  def make_finish_instance

    @program_instance = ProgramInstance.find(params[:program_instance_id])

    @program_instance.active = false

    @program_instance.save

    flash[:success] = t('activerecord.success.model.training_results.finish_ok')

    redirect_to tr2_list_program_instances_path

  end

  private

    def letras_excel

      letras = %w(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI AJ AK AL AM AN AO AP AQ AR AS AT AU AV AW AX AY AZ BA BB BC BD BE BF BG BH BI BJ BK BL BM BN BO BP BQ BR BS BT BU BV BW BX BY BZ CA CB CC CD CE CF CG CH CI CJ CK CL CM CN CO CP CQ CR CS CT CU CV CW CX CY CZ)

    end

    def xls_for_edit_results program_course_instance

      letras = letras_excel

      cols_widths = Array.new

      reporte = Axlsx::Package.new
      wb = reporte.workbook

      wb.use_shared_strings = true

      wb.styles do |s|

        headerTitle = s.add_style :b => true,
                                  :sz => 9,
                                  :border => { :style => :thin, color: '00' },
                                  :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

        headerTitleInfo = s.add_style :b => true,
                                      :sz => 9,
                                      :alignment => { :horizontal => :left, :wrap_text => true}

        headerTitleInfoDet = s.add_style :b => false,
                                         :sz => 9,
                                         :alignment => { :horizontal => :left, :wrap_text => true}

        headerLeft = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :left, :wrap_text => true}

        headerCenter = s.add_style :b => true,
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :center, :wrap_text => true}

        fieldLeft = s.add_style :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, :wrap_text => true}


        fieldLeftAprob = s.add_style fg_color: '0000ff',
                                     :sz => 9,
                                     :border => { :style => :thin, color: '00' },
                                     :alignment => { :horizontal => :left, :wrap_text => true}

        fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                      :sz => 9,
                                      :border => { :style => :thin, color: '00' },
                                      :alignment => { :horizontal => :left, :wrap_text => true}

        wb.add_worksheet(:name => 'Formato_Resultados') do |reporte_excel|

          fila = Array.new
          filaHeader = Array.new
          cols_widths = Array.new

          fila << program_course_instance.program_instance.name
          filaHeader <<  headerCenter
          cols_widths << 10

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A1:B1')

          fila = Array.new
          filaHeader = Array.new
          cols_widths = Array.new

          fila << '#'
          filaHeader <<  headerTitle
          cols_widths << 10

          fila << alias_username
          filaHeader <<  headerTitle
          cols_widths << 20

          fila << 'Apellidos'
          filaHeader <<  headerTitle
          cols_widths << 20

          fila << 'Nombre'
          filaHeader <<  headerTitle
          cols_widths << 20

          fila << program_course_instance.program_course.course.nombre
          filaHeader <<  headerCenter
          cols_widths << 20

          fila << ''
          filaHeader <<  headerCenter
          cols_widths << 20

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('E2:F2')

          program_course_instance.user_courses.each_with_index do |uc, index|

            fila = Array.new
            filaHeader = Array.new

            fila << index+1
            filaHeader <<  fieldLeft

            fila << uc.enrollment.user.codigo
            filaHeader <<  fieldLeft

            fila << uc.enrollment.user.apellidos
            filaHeader <<  fieldLeft

            fila << uc.enrollment.user.nombre
            filaHeader <<  fieldLeft

            fila << uc.nota
            filaHeader <<  fieldLeft

            fila << uc.porcentaje_avance
            filaHeader <<  fieldLeft


            reporte_excel.add_row fila, :style => filaHeader, height: 20


          end

          reporte_excel.column_info.each_with_index do |col, index|
            col.width = cols_widths[index]
          end

        end

      end

      return reporte

    end

    def update_results_from_excel_(archivo_temporal, program_course_instance)

      archivo = RubyXL::Parser.parse archivo_temporal

      program_instance = program_course_instance.program_instance

      data = archivo.worksheets[0].extract_data

      data.each_with_index do |fila, index|

        #0 código

        if index > 1

          user = User.where('codigo = ?',fila[1]).first

          if user

            level = program_instance.program.levels.first

            enrollment = Enrollment.where('program_id = ? AND user_id = ? AND level_id = ? AND program_instance_id = ?', program_instance.program.id, user.id, level.id, program_instance.id).first

            if enrollment

              uc = program_course_instance.user_courses.where('enrollment_id = ?', enrollment.id).first

              uc.nota = fila[4]
              uc.porcentaje_avance = fila[5]

              uc.iniciado = true

              uc.inicio = program_course_instance.from_date
              uc.fin = program_course_instance.to_date

              uc.finalizado = true

              uc.aprobado = false
              uc.aprobado = true if ((!program_course_instance.minimum_assistance || (program_course_instance.minimum_assistance && uc.porcentaje_avance && uc.porcentaje_avance >= program_course_instance.minimum_assistance)) && (!program_course_instance.minimum_grade || (program_course_instance.minimum_grade && uc.nota && uc.nota >= program_course_instance.minimum_grade)))

              uc.save

              define_results_program_instance(program_instance, enrollment)

            end

          end

        end


      end

    end

    def define_results_program_instance(program_instance, enrollment)

      tmp_num = 0

      program_instance.program_course_instances.each do |program_course_instance|

        uc = program_course_instance.user_courses.where('enrollment_id = ?', enrollment.id).first

        tmp_num +=1 if uc && uc.aprobado

      end

      if tmp_num >= program_instance.num_passed_courses
        enrollment.aprobado = true
        enrollment.save
      end

    end

end
