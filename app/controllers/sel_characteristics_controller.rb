class SelCharacteristicsController < ApplicationController

  include SelTemplatesHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /sel_characteristics
  # GET /sel_characteristics.json
  def index
    @sel_characteristics = SelCharacteristic.find_all_by_sel_template_id(params[:sel_template])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sel_characteristics }
    end
  end

  def new
    @sel_template = SelTemplate.find(params[:template_id])
    @sel_characteristic = SelCharacteristic.new
    @sel_characteristic.sel_template = @sel_template

    3.times do
      @sel_characteristic.sel_options.build
    end

  end

  def edit
    @sel_characteristic = SelCharacteristic.find(params[:id])
    @sel_template = @sel_characteristic.sel_template

  end

  # POST /sel_characteristics
  # POST /sel_characteristics.json
  def create
    @sel_characteristic = SelCharacteristic.new(params[:sel_characteristic])

    respond_to do |format|
      if @sel_characteristic.save
        flash[:success] = 'La característica fue añadida correctamente'
        format.html { redirect_to sel_templates_show_open_pill_path(@sel_characteristic.sel_template, 'characteristics') }
        format.json { render json: @sel_characteristic, status: :created, location: @sel_characteristic }
      else
        #format.html { render action: "new" }
        @sel_template = SelTemplate.find(@sel_characteristic.sel_template_id)
        format.html { render 'new' }
        format.json { render json: @sel_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sel_characteristics/1
  # PUT /sel_characteristics/1.json
  def update
    @sel_characteristic = SelCharacteristic.find(params[:id])
    @sel_template = SelTemplate.find(@sel_characteristic.sel_template_id)

    respond_to do |format|
      if @sel_characteristic.update_attributes(params[:sel_characteristic])
        flash[:success] = 'La característica fue actualizada correctamente'
        format.html { redirect_to sel_templates_show_open_pill_path(@sel_template, 'characteristics') }
        format.json { head :no_content }
      else
        @sel_template = SelTemplate.find(@sel_characteristic.sel_template_id)
        format.html { render 'edit' }
        format.json { render json: @sel_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sel_characteristics/1
  # DELETE /sel_characteristics/1.json
  def destroy
    @sel_characteristic = SelCharacteristic.find(params[:id])

    if is_sel_characteristic_deletable?(@sel_characteristic)
      @sel_characteristic.destroy
      respond_to do |format|
        flash[:success] = 'La característica fue eliminada correctamente'
        format.html { redirect_to sel_templates_show_open_pill_path(@sel_characteristic.sel_template, 'characteristics') }
        format.json { head :no_content }
      end
    else
      flash[:danger] = 'La característica no puede ser borrada.'
      redirect_to sel_templates_show_open_pill_path(@sel_characteristic.sel_template, 'characteristics')
    end
  end
end
