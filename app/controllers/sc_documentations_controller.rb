class ScDocumentationsController < ApplicationController
  include ScDocumentationsHelper

  before_filter :get_gui_characteristics
  before_filter :authenticate_user
  before_filter :verify_recorder

  def delete_modal
    document = ScDocumentation.find(params[:sc_documentation_id])
    render partial: 'delete_modal', locals: { document: document }
  end

  def index_manage
    @manager = manager?
    @users = []
    if @manager
      sc_documentations = ScDocumentation.all
      sc_documentations.each { |doc| @users << doc.user if doc.user.he_able }
      @users = @users.uniq_by(&:id)
    else
      flash[:danger] = 'No tiene los beneficios necesarios'
      redirect_to root_path
    end
  end

  def users_to_show
    all_users = params[:all_users] == '1' ? true : false
    manager = params[:manager_list] == '1' ? true : false
    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    if manager
      @users = User.where(activo: true) if all_users
      @users = User.where(activo: true, he_able: true) unless all_users
      @users = []
      sc_documentations = ScDocumentation.all
      sc_documentations.each do |doc|
        if all_users
        @users << doc.user
        else
          @users << doc.user if doc.user.he_able
        end
      end
      @users = @users.uniq_by(&:id)
      render partial: 'list_documents', locals: {users: @users, manager: manager, all_users: all_users}
    else
      if recorder
        @users = []
        people_to_record = recorder.sc_user_to_registers.where(active: true)
        people_to_record.each do |person_to_record|
          if all_users
            @users << person_to_record.user
          else
            @users << person_to_record.user if person_to_record.user.he_able
          end
        end
        render partial: 'list_documents', locals: {users: @users, manager: manager, all_users: all_users}
      else
        flash[:danger] = 'No tiene los beneficios necesarios'
        redirect_to root_path
      end
    end
  end


  def index
    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    if recorder
      @users = []
      people_to_record = recorder.sc_user_to_registers.where(active: true)
      people_to_record.each do |person_to_record|
        @users << person_to_record.user if person_to_record.user.he_able
      end
    else
      flash[:danger] = 'No tiene los beneficios necesarios'
      redirect_to root_path
    end
  end

  def historical_documentation
    if manager?
      @user = User.find(params[:user_id])
    else
      flash[:danger] = 'No tiene los beneficios necesarios'
      redirect_to sc_documentations_path
    end
  end

  def new
    @manager = manager?
    @sc_documentation = ScDocumentation.new(user_id: params[:user_id])
  end

  def edit
    @sc_documentation = ScDocumentation.find(params[:sc_documentation_id])
    @manager = manager?
  end

  def create
    manager = manager?
    @sc_documentation = ScDocumentation.new(params[:sc_documentation])

    if !verify_verify_active_period(@sc_documentation.sc_period_id) && !manager
      flash[:danger] = 'Período cerrado, los documentos no son editables'
      render action: 'new'
    else
      if @sc_documentation.valid?
        if params[:upload] && @sc_documentation.save
          name = @sc_documentation.user.id.to_s + @sc_documentation.name + SecureRandom.hex(5).to_s + File.extname(params[:upload]['datafile'].original_filename)
          directory = @company.directorio.to_s + '/' + @company.codigo.to_s + '/sc_periods/' + @sc_documentation.sc_period_id.to_s + '/'
          @sc_documentation.doc_path = name
          @sc_documentation.registered_at = lms_time
          @sc_documentation.registered_by_user_id = user_connected.id
          @sc_documentation.save
          update_active_doc(@sc_documentation.user_id)
          save_file(params[:upload]['datafile'], name, directory)
          flash[:success] = t('views.sc_periods.flash_messages.add_successfully')
          redirect_to sc_documentations_path
        else
          flash.now[:danger] = 'No se ha seleccionado archivo'
          render action: 'new'
        end
      else
        render action: 'new'
      end
    end
  end

  def update
    manager = manager?
    @sc_documentation = ScDocumentation.find(params[:id])

    if !verify_verify_active_period(@sc_documentation.sc_period_id) && !manager
      flash[:danger] = 'Período cerrado, los documentos no son editables'
      redirect_to sc_documentations_path
    else
      if @sc_documentation.active
          if @sc_documentation.update_attributes(params[:sc_documentation])
            update_active_doc(@sc_documentation.user_id)
            flash[:success] = t('views.sc_periods.flash_messages.changes_success')
            redirect_to sc_documentations_path
          else
            render action: 'edit'
          end
      else
        flash[:danger] = 'Documento no activo, no es editable'
        redirect_to sc_documentations_path
      end
    end
  end

  def delete
    @sc_documentation = ScDocumentation.find(params[:sc_documentation_id])
    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first

    if @sc_documentation.active && (recorder || @manager)
      @sc_documentation.deleted = true
      @sc_documentation.deleted_description = params[:sc_documentation][:deleted_description]
      @sc_documentation.active = false
      @sc_documentation.deactivated_by_user_id = user_connected.id
      @sc_documentation.deactivated_at = lms_time
      @sc_documentation.save
      update_active_doc(@sc_documentation.user_id)
    end

    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    if recorder
      @users = []
      people_to_record = recorder.sc_user_to_registers.where(active: true)
      people_to_record.each {|person_to_record| @users << person_to_record.user if person_to_record.user.he_able}
    end
    render partial: 'list_documents', locals: {users: @users, manager: false, success_message: true, all_users: '0'}
  end

  def download_file
    @sc_documentation = ScDocumentation.find(params[:sc_documentation_id])
    if @sc_documentation.doc_path.split('.')[0] == params[:doc_path]
      complete_doc_path = @company.directorio.to_s + '/' + @company.codigo.to_s + '/sc_periods/' + @sc_documentation.sc_period_id.to_s + '/' + @sc_documentation.doc_path.to_s
      send_file complete_doc_path,
                type: 'application/octet-stream',
                disposition: 'attachment'
    else
      render nothing: true
    end
  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def get_gui_characteristics
    @sc_schedule_characteristics = ScScheduleCharacteristic.gui
  end

  def save_file(upload, name, directory)
    full_path = File.join(directory, name)
    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(full_path, 'wb') { |f| f.write(upload.read) }
  end

  def verify_recorder
    recorder = HeCtModulePrivilage.where(:user_id => user_connected.id, :active => true, :recorder => true).first
    return if recorder
    return if manager?
    flash[:danger] = 'No dispone de privilegios necesarios para realizar esta acción'
    redirect_to root_path
  end

  def verify_verify_active_period(period_id)
    period = ScPeriod.find(period_id)
    return period.active
  end

  def manager?
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first
    if ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      return true
    else
      return false
    end
  end
end
