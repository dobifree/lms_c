class BrcDefineController < ApplicationController

  include BrcManagingHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:list_people, :massive_assign_members, :massive_finish, :download_defined_bonus, :remove_members]
  before_filter :get_data_2, only: [:show, :new, :create, :finish]
  before_filter :get_data_3, only: [:edit, :update, :delete]

  before_filter :validate_open_process, only: [:list_people, :show, :new, :create, :finish, :edit, :update, :delete, :massive_assign_members, :massive_finish, :download_defined_bonus]
  before_filter :validate_can_define, only: [:show, :new, :create, :finish, :edit, :update, :delete, :massive_assign_members, :massive_finish, :download_defined_bonus, :remove_members]
  before_filter :validate_can_define_company, only: [:show, :new, :create, :finish, :edit, :update, :delete]

  def list_processes
    @brc_processes = user_connected.active_brc_processes_to_define
  end

  def list_people
    @brc_members = @brc_process.brc_members_ordered_by_companies_ids @companies_ids
  end

  def show

  end

  def new

    @brc_bonus_item = @brc_member.brc_bonus_items.build
    @brc_bonus_item.brc_process = @brc_process

  end

  def create

    @brc_bonus_item = @brc_member.brc_bonus_items.where('secu_cohade_id = ? AND brc_category_id = ?', @brc_process.secu_cohade.id, params[:brc_bonus_item][:brc_category_id]).first

    unless @brc_bonus_item

      @brc_bonus_item = BrcBonusItem.new(params[:brc_bonus_item])
      @brc_bonus_item.secu_cohade = @brc_process.secu_cohade
      @brc_bonus_item.brc_member = @brc_member
      @brc_bonus_item.brc_process = @brc_process

    end

    @brc_bonus_item.p_a = params[:brc_bonus_item][:percentage]
    @brc_bonus_item.percentage = fix_form_floats params[:brc_bonus_item][:percentage] if params[:brc_bonus_item][:percentage]
    @brc_bonus_item.amount = fix_form_floats params[:brc_bonus_item][:amount] if params[:brc_bonus_item][:amount]
    @brc_bonus_item.validated = nil

    if @brc_bonus_item.p_a

      subaso = subaso_from_rl @brc_process, @brc_member.user

      if subaso
        @brc_bonus_item.amount_c = @brc_bonus_item.percentage*subaso/100
      else
        @brc_bonus_item.amount_c = @brc_bonus_item.percentage*0/100
      end

    end

    if @brc_bonus_item.save
      flash[:success] = t('activerecord.success.model.brc_bonus_item.create_ok')
      redirect_to brc_define_show_path(@brc_member)
    else
      render action: 'new'
    end

  end

  def edit

  end

  def update

    params[:brc_bonus_item][:percentage] = fix_form_floats params[:brc_bonus_item][:percentage] if params[:brc_bonus_item][:percentage]
    params[:brc_bonus_item][:amount] = fix_form_floats params[:brc_bonus_item][:amount] if params[:brc_bonus_item][:amount]

    if @brc_bonus_item.update_attributes(params[:brc_bonus_item])
      @brc_bonus_item.validated = nil
      if @brc_bonus_item.p_a

        subaso = subaso_from_rl @brc_process, @brc_member.user

        if subaso
          @brc_bonus_item.amount_c = @brc_bonus_item.percentage*subaso/100
        else
          @brc_bonus_item.amount_c = @brc_bonus_item.percentage*0/100
        end

        @brc_bonus_item.save

      end

      flash[:success] = t('activerecord.success.model.brc_bonus_item.update_ok')
      redirect_to brc_define_show_path(@brc_member)
    else
      render action: 'edit'
    end

  end

  def delete
    @brc_bonus_item.destroy
    flash[:success] = t('activerecord.success.model.brc_bonus_item.delete_ok')
    redirect_to brc_define_show_path(@brc_member)
  end

  def finish
    @brc_member.status_def = true
    @brc_member.save

    @brc_member.brc_bonus_items.each do |brc_b_i|
      brc_b_i.validated = nil
      brc_b_i.save
    end
    flash[:success] = t('activerecord.success.model.brc_member.define_ok')
    redirect_to brc_define_show_path(@brc_member)
  end

  def massive_finish

    if params[:list_item]

      params[:list_item].each do |brc_member_id,value|

        if value != '0'

          brc_member = @brc_process.brc_members.where('brc_members.id = ?', brc_member_id).first
          brc_member.status_def = true
          brc_member.save
        end

      end
    end
    flash[:success] = t('activerecord.success.model.brc_member.define_ok')
    redirect_to brc_define_list_people_path(@brc_process)

  end

  def massive_assign_members

    @brc_process = BrcProcess.find(params[:brc_process_id])

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    c_company = Characteristic.get_characteristic_company
    c_rol = Characteristic.get_characteristic_rol_security

    cohade = @brc_process.secu_cohade

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          next if index == 0

          fila[0] = fila[0].to_s

          user_member = User.find_by_codigo fila[0]
          category = BrcCategory.find_by_name fila[3]

          if user_member && cohade && category

            cc_value = user_member.characteristic_value c_company

            if cc_value && @companies_ids.include?(cc_value)

              cc_rol = user_member.user_characteristic c_rol

              if 1==1#cc_rol && cc_rol.characteristic_value && cc_rol.characteristic_value.value_string_2 == 'Comercial'

                brc_member = @brc_process.brc_member user_member

                unless brc_member
                  brc_member = @brc_process.brc_members.build
                  brc_member.user = user_member
                end

                if brc_member.save

                  if brc_member.status_def

                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('RUT: '+fila[0]).force_encoding('UTF-8')
                    @lineas_error_messages.push ['bono enviado a validación']

                  else

                    brc_member.status_def = false
                    brc_member.save

                    brc_bonus_item = brc_member.brc_bonus_items.where('secu_cohade_id = ? AND brc_category_id = ?', cohade.id, category.id).first

                    unless brc_bonus_item
                      brc_bonus_item = brc_member.brc_bonus_items.build
                      brc_bonus_item.brc_process = @brc_process
                    end

                    if fila[1]
                      brc_bonus_item.p_a = true
                      brc_bonus_item.percentage = fila[1]
                      brc_bonus_item.amount = 0

                      subaso = subaso_from_rl @brc_process, user_member

                      if subaso
                        brc_bonus_item.amount_c = brc_bonus_item.percentage*subaso/100
                      else
                        brc_bonus_item.amount_c = brc_bonus_item.percentage*0/100
                      end

                    elsif fila[2]

                      brc_bonus_item.p_a = false
                      brc_bonus_item.amount = fila[2]
                      brc_bonus_item.percentage = nil
                      brc_bonus_item.amount_c = 0

                    end

                    brc_bonus_item.secu_cohade = cohade
                    brc_bonus_item.brc_category = category
                    brc_bonus_item.validated = nil

                    unless brc_bonus_item.save

                      @lineas_error.push index+1
                      @lineas_error_detalle.push ('RUT: '+fila[0]).force_encoding('UTF-8')
                      @lineas_error_messages.push brc_bonus_item.errors.full_messages

                    end

                  end

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('RUT: '+fila[0]).force_encoding('UTF-8')
                  @lineas_error_messages.push brc_member.errors.full_messages
                end

              else
                @lineas_error.push index+1
                @lineas_error_detalle.push ('RUT: '+fila[0]).force_encoding('UTF-8')
                @lineas_error_messages.push ['No pertence al rol comercial']
              end

            else
              @lineas_error.push index+1
              @lineas_error_detalle.push ('RUT: '+fila[0]).force_encoding('UTF-8')
              @lineas_error_messages.push ['No pertence a la empresa asignada']
            end

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('RUT: '+fila[0]).force_encoding('UTF-8')

            @lineas_error_messages.push ['RUT '+fila[0]+' no existe'] unless user_member
            @lineas_error_messages.push ['Categoría '+(fila[3] ? fila[3] : '')+' no existe'] unless category

          end

        end

        if @lineas_error.size == 0

          flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')
          
        end


      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def download_massive_assign_members_excel_format

    reporte_excel = massive_assign_members_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_carga_bonos.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_categories_excel

    reporte_excel = categories_excel

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Categorias.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_defined_bonus

    reporte_excel = defined_bonus_excel @brc_process

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Bonos_definidos.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def remove_members

    @brc_members = @brc_process.brc_members_ordered_by_companies_ids @companies_ids

    @brc_members.each do |brc_member|
      brc_member.destroy unless brc_member.status_def
    end

    flash[:success] = t('activerecord.success.model.brc_member.remove_all_ok')

    redirect_to brc_define_massive_assign_members_path @brc_process

  end

  private

  def get_data_1
    @brc_process = BrcProcess.find(params[:brc_process_id])
    @brc_definer = @brc_process.brc_definer user_connected
    @companies_ids = @brc_definer.brc_definer_companies.map { |brc_d_c| brc_d_c.characteristic_value_id }
    @characteristic_company = Characteristic.get_characteristic_company

  end

  def get_data_2
    @brc_member = BrcMember.find(params[:brc_member_id])
    @brc_process = @brc_member.brc_process
    @brc_definer = @brc_process.brc_definer user_connected
    @companies_ids = @brc_definer.brc_definer_companies.map { |brc_d_c| brc_d_c.characteristic_value_id }
    @characteristic_company = Characteristic.get_characteristic_company

  end

  def get_data_3
    @brc_bonus_item = BrcBonusItem.find(params[:brc_bonus_item_id])
    @brc_member = @brc_bonus_item.brc_member
    @brc_process = @brc_member.brc_process
    @brc_definer = @brc_process.brc_definer user_connected
    @companies_ids = @brc_definer.brc_definer_companies.map { |brc_d_c| brc_d_c.characteristic_value_id }
    @characteristic_company = Characteristic.get_characteristic_company

  end

  def validate_open_process

    unless @brc_process.active
      flash[:danger] = t('activerecord.error.model.brc_process.not_active')
      redirect_to root_path
    end

  end

  def validate_can_define

    unless @brc_definer
      flash[:danger] = t('activerecord.error.model.brc_process.cant_define')
      redirect_to root_path
    end

  end

  def validate_can_define_company

    uc_cp_id = @brc_member.user.characteristic_value @characteristic_company

    unless @brc_definer.define_company_id uc_cp_id
      flash[:danger] = t('activerecord.error.model.brc_process.cant_define')
      redirect_to root_path
    end

  end

  def massive_assign_members_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_bonos')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << '% SUBASO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Monto pesos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Categoría'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def defined_bonus_excel(brc_process)

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Bonos')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'Rut'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Persona'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Empresa'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Estado'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Cohade'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Categoría'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Tipo'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Bono'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Definido (SI/NO)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Validado (SI/NO)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Comentario NO'
        filaHeader <<  headerCenter
        cols_widths << 20





        reporte_excel.add_row fila, :style => filaHeader, height: 20

        characteristic_company = Characteristic.get_characteristic_company

        brc_process.brc_members_ordered_by_companies_ids(@companies_ids).each do |brc_member|

          if brc_member

            uc_c = brc_member.user.user_characteristic(characteristic_company)

            brc_member.brc_bonus_items.each do |brc_bonus_item|

              fila = Array.new
              fileStyle = Array.new

              fila << brc_member.user.codigo
              fileStyle << fieldLeft

              fila << brc_member.user.apellidos+', '+brc_member.user.nombre
              fileStyle << fieldLeft

              fila << (uc_c ? uc_c.formatted_value : '')
              fileStyle << fieldLeft

              fila << (brc_member.user.activo ? 'Activo' : 'Inactivo')
              fileStyle << fieldLeft

              fila << brc_bonus_item.secu_cohade.name
              fileStyle << fieldLeft

              fila << brc_bonus_item.brc_category.name
              fileStyle << fieldLeft

              if brc_bonus_item.p_a
                fila << '% SUBASO'
                fileStyle << fieldLeft

                fila << brc_bonus_item.percentage
                fileStyle << fieldLeft

              else
                fila << 'pesos'
                fileStyle << fieldLeft

                fila << brc_bonus_item.amount
                fileStyle << fieldLeft
              end

              fila << (brc_member.status_def ? 'SI' : 'NO')
              fileStyle << fieldLeft

              if brc_member.status_val
                fila << (brc_member.status_val ? 'SI' : 'NO')
              else
                if brc_member.status_rej
                  fila << 'RECHAZADO'
                else
                  fila << (brc_member.status_val ? 'SI' : 'NO')
                end
              end

              fileStyle << fieldLeft

              fila << brc_bonus_item.val_comment
              fileStyle << fieldLeft

              reporte_excel.add_row fila, :style => fileStyle, height: 20

            end

          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def subaso_from_rl(brc_process, user)

    liq_process = brc_process.liq_process

    subaso = liq_process.get_subaso_brc_user user

    while liq_process && subaso.nil?

      y = liq_process.year
      m = liq_process.month

      m = m-1

      if m == 0
        m == 12
        y = y-1
      end

      liq_process = LiqProcess.liq_process_by_year_month y,m
      subaso =liq_process.get_subaso_brc_user(user) if liq_process

    end

    subaso

  end

end
