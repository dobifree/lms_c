class PeRelsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_rel = @pe_process.pe_rels.build

  end

  def create

    @pe_rel = PeRel.new(params[:pe_rel])

    if @pe_rel.save
      redirect_to pe_process_tab_path @pe_rel.pe_process, 'rels'
    else
      @pe_process = @pe_rel.pe_process
      render action: 'new'
    end

  end

  def edit
    @pe_rel = PeRel.find(params[:id])
    @pe_process = @pe_rel.pe_process
  end

  def update
    @pe_rel = PeRel.find(params[:id])

    if @pe_rel.update_attributes(params[:pe_rel])
      redirect_to pe_process_tab_path @pe_rel.pe_process, 'rels'
    else
      @pe_process = @pe_rel.pe_process
      render action: 'edit'
    end

  end


  def destroy
    @pe_rel = PeRel.find(params[:id])
    @pe_rel.destroy

    respond_to do |format|
      format.html { redirect_to pe_rels_url }
      format.json { head :no_content }
    end
  end

  def edit_available_in_selection
    @pe_process = PeProcess.find(params[:pe_process_id])
  end

end
