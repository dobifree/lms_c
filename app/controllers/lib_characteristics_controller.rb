class LibCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /lib_characteristics
  # GET /lib_characteristics.json
  def index
    @lib_characteristics = LibCharacteristic.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lib_characteristics }
    end
  end

  # GET /lib_characteristics/1
  # GET /lib_characteristics/1.json
  def show
    @lib_characteristic = LibCharacteristic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lib_characteristic }
    end
  end

  # GET /lib_characteristics/new
  # GET /lib_characteristics/new.json
  def new
    @lib_characteristic = LibCharacteristic.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lib_characteristic }
    end
  end

  # GET /lib_characteristics/1/edit
  def edit
    @lib_characteristic = LibCharacteristic.find(params[:id])
  end

  # POST /lib_characteristics
  # POST /lib_characteristics.json
  def create
    @lib_characteristic = LibCharacteristic.new(params[:lib_characteristic])

      if @lib_characteristic.save
        flash['success'] = 'Característica añadida correctamente'
        redirect_to lib_characteristics_path
      else
        render action: "new"
      end
  end

  # PUT /lib_characteristics/1
  # PUT /lib_characteristics/1.json
  def update
    @lib_characteristic = LibCharacteristic.find(params[:id])

    respond_to do |format|
      if @lib_characteristic.update_attributes(params[:lib_characteristic])
        format.html { redirect_to @lib_characteristic, notice: 'Lib characteristic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lib_characteristic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lib_characteristics/1
  # DELETE /lib_characteristics/1.json
  def destroy
    @lib_characteristic = LibCharacteristic.find(params[:id])
    @lib_characteristic.destroy

    flash['success'] = 'Característica eliminada correctamente'
    redirect_to lib_characteristics_path
  end
end
