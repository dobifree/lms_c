class HeCtModulePrivilagesController < ApplicationController
  include HeCtModulePrivilagesHelper

  before_filter :get_gui_characteristics, only: [:index,
                                                 :privilages_management,
                                                 :schedule_users,
                                                 :privilages_search_people_new,
                                                 :people_to_record_search_new,
                                                 :people_to_validate_search_new,
                                                 #:people_to_validate_recorders_search_new,
                                                 :schedule_search_people_create_schedule_user,
                                                 :recorders_to_validate_search_new,
                                                 :schedule_refresh_users,
                                                 :edit,
                                                 :new,
                                                 :user_to_register_managment,
                                                 :update,
                                                 :user_to_register_search_recorders,
                                                 :schedule_users_create_manually,
                                                 :schedule_users_assign_user_with_period,
                                                 :schedule_refresh_users_time_period, :schedule_users_match_by_conditions]

  before_filter :authenticate_user, :except => [:schedule_refresh_users_time_period,
                                                :schedule_refresh_users,
                                                :schedule_users_new_manually,
                                                :schedule_users_define_period_show_modal, :schedule_users_match_by_conditions]

  before_filter :verify_access_manage_module, :except => [:schedule_refresh_users_time_period,
                                                          :schedule_refresh_users,
                                                          :schedule_users_new_manually,
                                                          :schedule_users_define_period_show_modal, :schedule_users_match_by_conditions]

  before_filter :autenticate_user_partial, :only => [:schedule_refresh_users_time_period,
                                                     :schedule_refresh_users,
                                                     :schedule_users_new_manually,
                                                     :schedule_users_define_period_show_modal, :schedule_users_match_by_conditions]

  before_filter :verify_access_manage_module_partial, :only => [:schedule_refresh_users_time_period,
                                                                :schedule_refresh_users,
                                                                :schedule_users_new_manually,
                                                                :schedule_users_define_period_show_modal, :schedule_users_match_by_conditions]

  def recorders_excel_upload_step_2
    @error_index = []
    @error_content = []
    @error_message = []

    if params[:upload]
      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'
        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5) + '.xlsx'
        archivo_temporal = directorio_temporal + nombre_temporal
        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') {|f| f.write(params[:upload]['datafile'].read)}

        if its_safe archivo_temporal
          flash[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
        else
          flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
        end
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
      end
    else
      flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
    end
    render 'recorders_excel_upload'
  end

  def recorders_excel_upload

  end

  def index
    @he_ct_module_privilages = HeCtModulePrivilage.all
    @sc_schedules = ScSchedule.where(:active => true)
    @sc_processes = ScProcess.order('year DESC, month DESC').all
    @tab = params[:tab] ? params[:tab] : 0

    @sc_conditions = []
    @sc_schedules.each do |schedule|
      @sc_conditions << schedule.sc_characteristic_conditions.where(:sc_schedule_id => schedule.id, :active => true).order(:characteristic_id)
    end

    @users_to_register_attendances =[]
    users_to_register = ScUserToRegister.all
    users_to_register.each do |user_to_register|
      @users_to_register_attendances << user_to_register.user
    end
    @users_to_register_attendances = @users_to_register_attendances.uniq { |user| user.id }

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @he_ct_module_privilages }
    end
  end

  def privilages_search_people_new
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def privilages_management
    @he_ct_module_privilage = HeCtModulePrivilage.find(params[:he_ct_module_privilages_id])
    @tab = params[:tab] ? params[:tab] : 0
    @sc_user_to_register = @he_ct_module_privilage.sc_user_to_registers.where(:active => true)
    @sc_user_to_validate = @he_ct_module_privilage.sc_user_to_validates.where(:active => true)
    @sc_register_to_validate = @he_ct_module_privilage.sc_register_to_validates.where(:active => true)
  end

  def new
    @he_ct_module_privilage = HeCtModulePrivilage.new
    @he_ct_module_privilage.user = User.find(params[:user_id])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @he_ct_module_privilage }
    end
  end

  def create
    @he_ct_module_privilage = HeCtModulePrivilage.new(params[:he_ct_module_privilage])
    @he_ct_module_privilage.user = User.find(params[:user_id])
    @he_ct_module_privilage.active = true
    @he_ct_module_privilage.registered_by_user = user_connected
    @he_ct_module_privilage.registered_at = lms_time
    respond_to do |format|
      if @he_ct_module_privilage.save
        format.html { redirect_to he_ct_module_privilages_index_path(2) }
        format.json { render json: @he_ct_module_privilage, status: :created, location: @he_ct_module_privilage }
      else
        format.html { render action: "new" }
        format.json { render json: @he_ct_module_privilage.errors, status: :unprocessable_entity }
      end
    end
  end

  def people_to_record_search_new
    @he_ct_module_privilage = HeCtModulePrivilage.find(params[:he_ct_module_privilage_id])
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def people_to_record_new
    sc_user_to_register = ScUserToRegister.where(:user_id => params[:user_id],
                                                 :he_ct_module_privilage_id =>  params[:he_ct_module_privilage_id]).first
    if sc_user_to_register
      sc_user_to_register.active = true
      sc_user_to_register.deactivated_by_user = nil
      sc_user_to_register.deactivated_at = nil
    else
      sc_user_to_register = ScUserToRegister.new(:user_id => params[:user_id],
                                                 :registered_at => lms_time,
                                                 :registered_by_user_id => user_connected.id,
                                                 :he_ct_module_privilage_id => params[:he_ct_module_privilage_id])
    end


    sc_user_to_register.save
    flash[:success] = t('views.he_ct_module_privilage.flash_messages.add_user_successfully')
    redirect_to privilages_management_path(params[:he_ct_module_privilage_id],0)
  end

  def people_to_record_delete
    sc_user_to_register = ScUserToRegister.find(params[:sc_user_to_register_id])
    sc_user_to_register.active = false
    sc_user_to_register.deactivated_by_user = user_connected
    sc_user_to_register.deactivated_at = lms_time
    sc_user_to_register.save

    flash[:success] = t('views.he_ct_module_privilage.flash_messages.delete_user_successfully') if params[:go_to] == '0' || params[:go_to] == '1'
    redirect_to privilages_management_path(sc_user_to_register.he_ct_module_privilage_id,0) if params[:go_to] == '0'
    redirect_to user_to_register_managment_path(sc_user_to_register.user.id,0) if params[:go_to] == '1'
    render nothing: true if params[:go_to] == '2'
  end

  def people_to_validate_search_new
    @he_ct_module_privilage = HeCtModulePrivilage.find(params[:he_ct_module_privilage_id])
    @user = User.new
    @users = {}

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def people_to_validate_new
    sc_user_to_validate = ScUserToValidate.where(:user_id => params[:user_id],
                                                 :he_ct_module_privilage_id => params[:he_ct_module_privilage_id]).first
    if sc_user_to_validate
      sc_user_to_validate.active = true
      sc_user_to_validate.deactivated_at = nil
      sc_user_to_validate.deactivated_by_user = nil
    else
      sc_user_to_validate = ScUserToValidate.new(:user_id => params[:user_id],
                                                 :registered_at => lms_time,
                                                 :registered_by_user_id => user_connected.id,
                                                 :he_ct_module_privilage_id => params[:he_ct_module_privilage_id])
    end

    sc_user_to_validate.save
    flash[:success] = t('views.he_ct_module_privilage.flash_messages.add_user_successfully')
    redirect_to privilages_management_path(params[:he_ct_module_privilage_id],1)
  end

  def people_to_validate_delete
    sc_user_to_validate = ScUserToValidate.find(params[:sc_user_to_validate_id])
    sc_user_to_validate.active = false
    sc_user_to_validate.deactivated_by_user = user_connected
    sc_user_to_validate.deactivated_at = lms_time
    sc_user_to_validate.save

    flash[:success] = t('views.he_ct_module_privilage.flash_messages.delete_user_successfully')
    redirect_to privilages_management_path(sc_user_to_validate.he_ct_module_privilage_id,1)
  end

  def recorders_to_validate_search_new
    @he_ct_module_privilage = HeCtModulePrivilage.find(params[:he_ct_module_privilage_id])
    @user = User.new
    @users = {}
    register_not_found_message = false
    same_person_message = false
    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      if @users
        users_privilage = HeCtModulePrivilage.where(:active => true, :recorder => true)
        @users.each_with_index do |user, index|
          should_delete = true
          users_privilage.each do |user_privilage|
            if user.id != @he_ct_module_privilage.user.id
              should_delete = false if user.id == user_privilage.user.id
              # register_not_found_message = true
            else
              same_person_message = true
            end
          end
          @users.delete_at(index) if should_delete
        end
      end
      @user = User.new(params[:user])
    end
    flash.now[:danger] = 'No se ha encontrado registrador con los parámetros ingresados'  if register_not_found_message
    flash.now[:danger] = 'No puede seleccionar la misma persona a la cual asigna'  if same_person_message
  end

  def recorders_to_validate_new
    sc_register_to_validate = ScRegisterToValidate.where(:user_id => params[:user_id],
                                                 :he_ct_module_privilage_id => params[:he_ct_module_privilage_id]).first
    if sc_register_to_validate
      sc_register_to_validate.active = true
      sc_register_to_validate.deactivated_at = nil
      sc_register_to_validate.deactivated_by_user = nil
    else
      sc_register_to_validate = ScRegisterToValidate.new(:user_id => params[:user_id],
                                                         :registered_at => lms_time,
                                                         :registered_by_user_id => user_connected.id,
                                                         :he_ct_module_privilage_id => params[:he_ct_module_privilage_id])
    end

    sc_register_to_validate.save
    flash[:success] = t('views.he_ct_module_privilage.flash_messages.add_user_successfully')
    redirect_to privilages_management_path(params[:he_ct_module_privilage_id],1)
  end

  def recorders_to_validate_delete
    sc_register_to_validate = ScRegisterToValidate.find(params[:sc_user_to_validate_id])
    sc_register_to_validate.active = false
    sc_register_to_validate.deactivated_by_user = user_connected
    sc_register_to_validate.deactivated_at = lms_time
    sc_register_to_validate.save

    flash[:success] = t('views.he_ct_module_privilage.flash_messages.delete_user_successfully')
    redirect_to privilages_management_path(sc_register_to_validate.he_ct_module_privilage_id,1)
  end

  def schedule_users_match_by_conditions
    schedule = ScSchedule.find(params[:sc_schedule_id])
    conditions = schedule.sc_characteristic_conditions.where(:sc_schedule_id => schedule.id, :active => true)
    users_id = []

    if conditions.size > 0
      user_characteristics = UserCharacteristic.where(:characteristic_id => conditions.first.characteristic_id)
    else
      user_characteristics = []
    end

    user_characteristics = user_characteristics.uniq_by { |obj| obj.user_id }
    user_characteristics.each {|obj| users_id << obj.user_id }


    conditions.each do |condition|
      user_characteristics = UserCharacteristic.where(:characteristic_id => condition.characteristic_id, :user_id => users_id)
      user_characteristics.delete_if {|user_characteristic| user_characteristic.value != condition.value }
      # user_characteristics = user_characteristics.uniq_by { |obj| obj.user_id }
      users_id = []
      user_characteristics.each { |obj| users_id << obj.user_id }
    end

    users = User.where(:id => users_id, :activo => true)
    schedule_users = ScScheduleUser.where(:sc_schedule_id => schedule.id, :active => true, :registered_manually => false)

    schedule_users.each_with_index do |schedule_user, index0|
      schedule_user_not_found = true
      users.each_with_index do |user, index1|
        if schedule_user.user_id == user.id
          unless schedule_user.he_able == user.he_able
            #Añadir todas las características relacionadas con el control de usuario que hayan cambiado desde la última actualización
            schedule_user.active = false
            schedule_user.deactivated_at = lms_time
            schedule_user.deactivated_by_user = user_connected
            schedule_user.save

            updated_schedule_user = ScScheduleUser.new(:user_id => user.id,
                                                       :sc_schedule_id => schedule.id,
                                                       :he_able => user.he_able)

            updated_schedule_user.registered_at = lms_time
            updated_schedule_user.registered_by_user = user_connected
            updated_schedule_user.active = true
            updated_schedule_user.save
          end
          users.delete_at(index1)
          schedule_user_not_found = false
        end
      end
      if schedule_user_not_found
        schedule_user.active = false
        schedule_user.deactivated_at = lms_time
        schedule_user.deactivated_by_user = user_connected
        schedule_user.save
      end
    end

    users.each do |user|
      new_schedule_user = ScScheduleUser.new(:user_id => user.id,
                                             :sc_schedule_id => schedule.id,
                                             :he_able => user.he_able)

      new_schedule_user.registered_at = lms_time
      new_schedule_user.registered_by_user = user_connected
      new_schedule_user.active = true
      new_schedule_user.save
    end

    render :nothing => true
  end


  def schedule_users_define_period_show_modal
    render :partial => 'dont_charge_to_payroll_record_modal'
  end

  def schedule_users
    @sc_schedule = ScSchedule.find(params[:sc_schedule_id])
    @sc_schedule_users = @sc_schedule.sc_schedule_users.where('active = true and active_since is null')
    @sc_conditions = []
    @sc_conditions = @sc_schedule.sc_characteristic_conditions.where(:sc_schedule_id => @sc_schedule.id, :active => true).order(:characteristic_id)
    @sc_schedule_users_by_time_periods = @sc_schedule.sc_schedule_users.where('active = true and active_since is not null')
  end

  def blocks_detail
    @schedule = ScSchedule.find(params[:sc_schedule_id])
    if @schedule.active
      render partial: 'blocks_detail_modal'
    else
      flash[:danger] = 'Registro no activo'
      redirect_to root_path
    end
  end

  def schedule_search_people_create_schedule_user
    @user = User.new
    @users = {}
    @from_to = params[:search_for]
    @sc_schedule = ScSchedule.find(params[:sc_schedule_id])
    @sc_conditions = @sc_schedule.sc_characteristic_conditions.where(:sc_schedule_id => @sc_schedule.id, :active => true).order(:characteristic_id)

    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])
    end
  end

  def schedule_users_new_manually
    user = User.find(params[:user_id])
    schedule = ScSchedule.find(params[:sc_schedule_id])

    schedule_user = ScScheduleUser.new(:user_id => user.id,
                                       :sc_schedule_id => schedule.id)

    render :partial => 'schedule_users_define_period_modal',
           :locals => { :schedule_user => schedule_user,
                        :characteristics =>  @sc_schedule_characteristics }

  end

  def schedule_users_assign_user_with_period
    desdehasta = params[:desdehasta].split('-')
    desde = DateTime.parse(desdehasta[0])
    hasta = DateTime.parse(desdehasta[1])

    user = User.find(params[:user_id])
    schedule_user = ScScheduleUser.new(:user_id => user.id,
                                       :sc_schedule_id => params[:sc_schedule_id],
                                       :active_since => desde,
                                       :active_end => hasta,
                                       he_able: user.he_able)
    schedule_user.active = true
    schedule_user.registered_by_user = user_connected
    schedule_user.registered_at = lms_time

    if schedule_user.save
      flash[:success] = 'Usuario asociado correctamente'
      redirect_to schedule_users_path(params[:sc_schedule_id])
    else
      @schedule_user = schedule_user
      @user = User.new
      @users = {}
      @sc_schedule = ScSchedule.find(params[:sc_schedule_id])
      @sc_conditions = @sc_schedule.sc_characteristic_conditions.where(:sc_schedule_id => @sc_schedule.id, :active => true).order(:characteristic_id)
      @users = search_active_users(User.find(params[:user_id]).codigo, '', '')

      render :action => 'schedule_search_people_create_schedule_user'

    end



  end

  def schedule_users_create_manually
    user = User.find(params[:user_id])
    schedule = ScSchedule.find(params[:sc_schedule_id])

    schedule_user = ScScheduleUser.where('user_id = ? and sc_schedule_id = ? and active_since is null', user.id, schedule.id).first
    schedule_user = ScScheduleUser.new(user_id: user.id, sc_schedule_id: schedule.id, active: true) unless schedule_user

    schedule_user.he_able = user.he_able
    schedule_user.registered_at = lms_time
    schedule_user.registered_by_user = user_connected
    schedule_user.active = true
    schedule_user.registered_manually = true

    if schedule_user.save
      flash[:success] = t('views.sc_schedules.flash_messages.success_association')
      redirect_to schedule_users_path(params[:sc_schedule_id])
    else
      @schedule_user = schedule_user
      @user = User.new
      @users = {}
      @sc_schedule = schedule
      @sc_conditions = @sc_schedule.sc_characteristic_conditions.where(:sc_schedule_id => @sc_schedule.id, :active => true).order(:characteristic_id)
      @users = search_active_users(user.codigo, '', '')

      render :action => 'schedule_search_people_create_schedule_user'
    end
  end

  def schedule_refresh_users
    schedule = ScSchedule.find(params[:sc_schedule_id])
    schedule_users = schedule.sc_schedule_users.where('active is true and active_since is null')

    render partial: 'schedule_user_table',
           locals: {:schedule_users => schedule_users,
                    :editable => true,
                    :schedule => schedule,
                    :characteristics => @sc_schedule_characteristics }
  end

  def schedule_refresh_users_time_period
    schedule = ScSchedule.find(params[:sc_schedule_id])
    schedule_users = schedule.sc_schedule_users.where('active = true and active_since is not null')

    render partial: 'schedule_user_by_time_periods_table',
           locals: {:schedule_users => schedule_users,
                    :editable => true,
                    :schedule => schedule,
                    :characteristics => @sc_schedule_characteristics }
  end

  def schedule_delete_users
    schedule_user = ScScheduleUser.find(params[:sc_schedule_user_id])
    schedule_user.active = false
    schedule_user.deactivated_at = lms_time
    schedule_user.deactivated_by_user = user_connected
    schedule_user.deactivated_manually = true
    schedule_user.save

    render :nothing => true
  end

  def deactivate_time_period
    schedule_user = ScScheduleUser.find(params[:sc_schedule_user_id])
    schedule_user.active = false
    schedule_user.deactivated_at = lms_time
    schedule_user.deactivated_by_user = user_connected
    schedule_user.deactivated_manually = true
    schedule_user.save

    render :nothing => true
  end

  def show
    @he_ct_module_privilage = HeCtModulePrivilage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @he_ct_module_privilage }
    end
  end

  def edit
    @he_ct_module_privilage = HeCtModulePrivilage.find(params[:he_ct_module_privilages_id])
  end


  def update
    @he_ct_module_privilage = HeCtModulePrivilage.find(params[:he_ct_module_privilages_id])
    @he_ct_module_privilage.active = params[:he_ct_module_privilage][:active]
    @he_ct_module_privilage.recorder = params[:he_ct_module_privilage][:recorder]
    @he_ct_module_privilage.validator = params[:he_ct_module_privilage][:validator]

    if @he_ct_module_privilage.active
      @he_ct_module_privilage.deactivated_at = nil
      @he_ct_module_privilage.deactivated_by_user = nil
    else
      @he_ct_module_privilage.deactivated_at = lms_time
      @he_ct_module_privilage.deactivated_by_user = user_connected
    end

    respond_to do |format|
      if @he_ct_module_privilage.save
        format.html { redirect_to privilages_management_path(@he_ct_module_privilage.id,0) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @he_ct_module_privilage.errors, status: :unprocessable_entity }
      end
    end
  end

  def user_to_register_managment
    @tab = params[:tab] ? params[:tab] : 0
    @user = User.find(params[:user_id])

    @users_to_register = ScUserToRegister.where(:user_id => @user.id, :active => true)
    @users_to_register = @users_to_register.uniq_by { |user_to_register| user_to_register.he_ct_module_privilage.user_id }


    @users_to_validate = ScUserToValidate.where(:user_id => @user.id, :active => true)
    @users_to_validate = @users_to_validate.uniq_by { |user_to_validate| user_to_validate.he_ct_module_privilage.user_id }

    users_to_register_ids = []
    @users_to_register.each do |user_to_register|
      users_to_register_ids << user_to_register.he_ct_module_privilage.user_id
    end

    @registers_to_validate = ScRegisterToValidate.where(:active => true, :user_id => users_to_register_ids)
    @registers_to_validate.each_with_index do |register_to_validate,index|
      @registers_to_validate.delete_at(index) unless register_to_validate.he_ct_module_privilage.active
    end
    @registers_to_validate = @registers_to_validate.uniq_by { |register_to_validate| register_to_validate.he_ct_module_privilage_id }

  end

  def user_to_register_toggle_he_able
    user = User.find(params[:user_id])
    if user.activo
      user.he_able = !user.he_able
      user.save
    end
    flash[:success] = 'Cambio realizado exitosamente'
    redirect_to user_to_register_managment_path(params[:user_id],0)
  end

  def user_to_register_toggle_flexible_sc
    user = User.find(params[:user_id])
    if user.activo
      user.flexible_sc = !user.flexible_sc
      user.save
    end
    flash[:success] = 'Cambio realizado exitosamente'
    redirect_to user_to_register_managment_path(params[:user_id],0)
  end

  def user_to_register_search_recorders
    @user_to_register = User.find(params[:user_id])
    @user = User.new
    @users = {}
    show_message = false
    if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
      @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      if @users
        @users.each_with_index do |user, index|
          he_ct_module_privilage = nil
          he_ct_module_privilage = HeCtModulePrivilage.where(:active => true, :recorder => true, :user_id => user.id).first
          @users.delete_at(index) unless he_ct_module_privilage
          @users.delete_at(index) if he_ct_module_privilage && @user_to_register.id == user.id
          show_message = true
        end
        @user = User.new(params[:user])
      end
    end
    if @users.size == 0 && show_message
    flash.now[:danger] = 'No se ha encontrado registradores activos con los parámetros ingresados'
    end
  end

  def user_to_register_vinculate_recorder
    he_ct_module_privilage = HeCtModulePrivilage.where(:user_id => params[:he_ct_module_privilage_id], :active => true).first
    user_to_register = ScUserToRegister.where(:user_id => params[:user_id], :he_ct_module_privilage_id => he_ct_module_privilage.id).first_or_initialize
    user_to_register.active = true
    user_to_register.deactivated_at =  nil
    user_to_register.deactivated_by_user = nil
    user_to_register.registered_at = lms_time unless user_to_register.registered_at
    user_to_register.registered_by_user = user_connected unless user_to_register.registered_by_user
    user_to_register.save
    redirect_to user_to_register_managment_path(params[:user_id],0)
  end

  private

  def get_gui_characteristics
    @sc_schedule_characteristics = ScScheduleCharacteristic.gui
  end

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

  def verify_access_manage_module_partial
    ct_module = CtModule.where('cod = ? AND active = ?', 'sc_he', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      render :partial => 'shared/div_message',
             :locals => { :id_generic_message => 'verify_access_manage_module',
                          :type_generic_message => 'alert-danger',
                          :generic_message => t('security.no_access_to_manage_module')}
    end
  end

  def autenticate_user_partial
    unless logged_in?
      render :partial => 'shared/div_message',
           :locals => { :id_generic_message => 'verify_access_manage_module',
                        :type_generic_message => 'alert-danger',
                        :generic_message => 'No ha iniciado sesión'}
    end
  end
end


# def destroy
#   @he_ct_module_privilage = HeCtModulePrivilage.find(params[:id])
#   @he_ct_module_privilage.destroy
#
#   respond_to do |format|
#     format.html { redirect_to he_ct_module_privilages_url }
#     format.json { head :no_content }
#   end
# end

# def people_to_validate_recorders_search_new
#   @he_ct_module_privilage = HeCtModulePrivilage.find(params[:he_ct_module_privilage_id])
#   @user = User.new
#   @users = {}
#
#   if params[:user] and !(params[:user][:codigo].blank? and params[:user][:apellidos].blank? and params[:user][:nombre].blank?)
#     @users = search_active_users(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
#     @user = User.new(params[:user])
#   end
# end