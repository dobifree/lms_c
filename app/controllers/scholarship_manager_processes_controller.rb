class ScholarshipManagerProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_manager_privileges

  # GET /scholarship_processes
  # GET /scholarship_processes.json
  def index
    @scholarship_processes = ScholarshipProcess.all
  end

  # GET /scholarship_processes/1
  # GET /scholarship_processes/1.json
  def show
    @scholarship_process = ScholarshipProcess.find(params[:id])
  end

  # GET /scholarship_processes/new
  # GET /scholarship_processes/new.json
  def new
    @scholarship_process = ScholarshipProcess.new
  end

  def show_application
    @scholarship_application = ScholarshipApplication.find(params[:scholarship_application_id])
  end

  def evaluate_application
    @scholarship_application = ScholarshipApplication.find(params[:scholarship_application_id])
    if params[:evaluation] && params[:evaluation_comment]
      @scholarship_application.evaluated = true
      @scholarship_application.evaluated_at = lms_time
      @scholarship_application.evaluated_by_user = user_connected
      @scholarship_application.evaluation = params[:evaluation]
      @scholarship_application.evaluation_comment = params[:evaluation_comment]
      if @scholarship_application.save
        flash[:success] = 'La aplicación fue evaluada correctamente.'
      else
        flash[:danger] = 'La aplicación no pudo ser evaluada correctamente, por favor intente nuevamente.'
      end
    else
      flash[:danger] = 'La evaluación requiere un comentario, por favor intente nuevamente.'
    end
    redirect_to scholarship_manager_processes_show_application_path(@scholarship_application)
  end


  def applications_list_report
    @scholarship_process = ScholarshipProcess.find(params[:id])
    request.format = "xlsx"
    filename = 'detalle_postulaciones_' + @scholarship_process.name.gsub(/\s/, '_') + '.xlsx'
    respond_to do |format|
      format.xlsx {headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""}
    end
  end

  def revert_evaluation
    @scholarship_application = ScholarshipApplication.find(params[:scholarship_application_id])
    @scholarship_application.evaluated = false
    @scholarship_application.evaluated_at = nil
    @scholarship_application.evaluated_by_user_id = nil
    @scholarship_application.evaluation = nil
    @scholarship_application.evaluation_comment = nil

    if @scholarship_application.save
      flash[:success] = 'La evaluación se reversó correctamente'
    else
      flash[:danger] = 'La evaluación no se pudo reversar correctamente'
    end

    redirect_to scholarship_manager_processes_show_application_path(@scholarship_application)

  end

  def delete_application
    @scholarship_application = ScholarshipApplication.find(params[:scholarship_application_id])
    scholarship_process = @scholarship_application.scholarship_process
    if @scholarship_application.evaluated?
      flash[:danger] = 'No se puede eliminar una postulación ya evaluada.'
      redirect_to scholarship_manager_processes_show_application_path(@scholarship_application)
    elsif @scholarship_application.destroy
      flash[:success] = 'Postulación eliminada correctamente'
      redirect_to scholarship_manager_processes_show_path(scholarship_process)
    else
      flash[:danger] = 'No se pudo eliminar la postulación, por favor intente nuevamente'
      redirect_to scholarship_manager_processes_show_application_path(@scholarship_application)
    end
  end

  def massive_evaluation
    @scholarship_process = ScholarshipProcess.find(params[:id])
    if File.extname(params['applications_file'].original_filename) == '.xlsx'

      directorio_temporal = '/tmp/'
      nombre_temporal = SecureRandom.hex(5) + '.xlsx'
      archivo_temporal = directorio_temporal + nombre_temporal
      path = File.join(directorio_temporal, nombre_temporal)
      File.open(path, 'wb') {|f| f.write(params['applications_file'].read)}

      should_save = true
      states = ['Aprobado', 'Rechazado', 'En revisión']
      @excel_errors = []

      begin
        file = RubyXL::Parser.parse archivo_temporal
        data = file.worksheets[0].extract_data
      rescue Exception => e
        data = []
        should_save = false
        @excel_errors << ['-', 'El archivo no tiene el formato correcto.', '-', '-']
      end

      # se toman los 2 últimos campos como evaluacion y comentario
      evaluation_index = data[0].size - 2
      comment_index = data[0].size - 1

      data.each_with_index do |row, index|
        next if index.zero?

        break if !row[0] && !row[evaluation_index]

        user = User.where(codigo: row[0].to_s).first if row[0] || !row[0].to_s.empty?

        application = ScholarshipApplication.where(subject_user_id: user.id, done: true).first if user

        if !user
          should_save = false
          @excel_errors << [(index + 1).to_s, 'El ' + alias_username + ' no existe', row[0], row[evaluation_index]]
        end

        if user && !application
          should_save = false
          @excel_errors << [(index + 1).to_s, 'El ' + alias_user + ' no tiene registro de postulación en este proceso', row[0], row[evaluation_index]]
        end

        if !row[evaluation_index] || !states.include?(row[evaluation_index].to_s) || row[evaluation_index].to_s.empty?
          should_save = false
          @excel_errors << [(index + 1).to_s, 'Evaluación sólo puede tener los valores ' + states.to_s, row[0], row[evaluation_index]]
        end
      end
      if should_save
        data.each_with_index do |row, index|
          next if index.zero?
          codigo = row[0]
          evaluation = row[evaluation_index]
          comment = row[comment_index]

          user = User.where(codigo: codigo.to_s).first
          application = ScholarshipApplication.where(subject_user_id: user.id, done: true).first
          case evaluation
            when 'Aprobado'
              unless application.evaluated && application.evaluation
                application.evaluated = true
                application.evaluation = true
                application.evaluation_comment = comment
                application.evaluated_at = lms_time
                application.evaluated_by_user = user_connected
                application.save
              end
            when 'Rechazado'
              unless application.evaluated && !application.evaluation
                application.evaluated = true
                application.evaluation = false
                application.evaluation_comment = comment
                application.evaluated_at = lms_time
                application.evaluated_by_user = user_connected
                application.save
              end
            when 'En revisión'
              unless !application.evaluated
                application.evaluated = nil
                application.evaluation = nil
                application.evaluation_comment = nil
                application.evaluated_at = nil
                application.evaluated_by_user = nil
                application.save
              end
          end
        end
        flash[:success] = 'El archivo se procesó correctamente.'
        redirect_to scholarship_manager_processes_show_path(@scholarship_process)
      else
        flash.now[:danger] = 'Hay errores en el archivo, favor revise el detalle para que los puede corregir y vuelva a itentarlo'
        render 'show'
      end
    else
      flash[:danger] = 'El archivo no tiene el formato correcto (.xlsx)'
      redirect_to scholarship_manager_processes_show_application_path(@scholarship_application)
    end

  end

  private

  def verify_manager_privileges
    scholarship_module = get_ct_module('sch')
    scholarship_module && scholarship_module.active? && scholarship_module.ct_module_managers.where(user_id: user_connected.id).any?
  end
end
