class PeGroupsController < ApplicationController


  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
    @pe_group = @pe_evaluation.pe_groups.build

  end

  def create

    @pe_group = PeGroup.new(params[:pe_group])


    if @pe_group.save
      redirect_to @pe_group.pe_evaluation
    else
      @pe_evaluation = PeEvaluation.find(params[:pe_group][:pe_evaluation_id])
      @pe_process = @pe_evaluation.pe_process
      render action: 'new'
    end

  end


  def edit
    @pe_group = PeGroup.find(params[:id])
    @pe_evaluation = @pe_group.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def update
    @pe_group = PeGroup.find(params[:id])


    if @pe_group.update_attributes(params[:pe_group])
      redirect_to @pe_group.pe_evaluation
    else
      @pe_evaluation = @pe_group.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end


  def destroy
    @pe_group = PeGroup.find(params[:id])
    pe_evaluation = @pe_group.pe_evaluation

    @pe_group.destroy if @pe_group.pe_member_groups.size == 0

    redirect_to pe_evaluation

  end

end
