class JManagingController < ApplicationController

  include JManagingHelper

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def list_jobs
    @j_jobs = JJob.where('current = ?', true)
  end

  def assign_job_search
    @j_job = JJob.find(params[:j_job_id])
    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end
  end

  def manage_user_job_2

    @user = User.find(params[:user_id])
    @j_job = JJob.find(params[:j_job_id])

  end

  def search_users

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

  end

  def manage_user_job

    @user = User.find(params[:user_id])

  end

  def assign_job

    @user = User.find(params[:user_id])

    if params[:to_date_continua] == '1'

      if @user.j_job_id != params[:user][:j_job_id].to_i

        if @user.update_attributes(params[:user])

          tmp_date = params[:from_date].split('/')
          from_date = Date.new(tmp_date[2].to_i,tmp_date[1].to_i,tmp_date[0].to_i)

          user_j_job_last = @user.user_j_jobs.last

          if user_j_job_last && user_j_job_last.to_date.nil?
            user_j_job_last.to_date = from_date-1.day
            user_j_job_last.save
          end

          user_j_job = @user.user_j_jobs.build
          user_j_job.j_job_id = @user.j_job_id
          user_j_job.from_date = from_date
          user_j_job.save

          characteristic = Characteristic.where('data_type_id = ?',13).first

          if characteristic

            @user.update_user_characteristic(characteristic, @user.j_job.name, from_date.to_datetime, user_connected, user_connected, nil, nil, @company)

          end

          flash[:success] = t('activerecord.success.model.j_managing.assign_job_ok')
        else
          flash[:danger] = t('activerecord.error.model.j_managing.assign_job_error')
        end

      else
        flash[:danger] = t('activerecord.error.model.j_managing.assign_same_job_error')
      end

    else

      user_j_job = @user.user_j_jobs.build
      user_j_job.j_job_id = params[:user][:j_job_id]

      tmp_date = params[:from_date].split('/')
      from_date = Date.new(tmp_date[2].to_i,tmp_date[1].to_i,tmp_date[0].to_i)
      user_j_job.from_date = from_date

      tmp_date = params[:to_date].split('/')
      to_date = Date.new(tmp_date[2].to_i,tmp_date[1].to_i,tmp_date[0].to_i)
      user_j_job.to_date = to_date

      user_j_job.save

      characteristic = Characteristic.where('data_type_id = ?',13).first

      if characteristic

        @user.add_user_chatacteristic_record(characteristic, user_j_job.j_job.name, from_date.to_datetime, to_date.to_datetime+(24*3600-1).seconds, user_connected, user_connected, nil, nil, @company)

      end

      flash[:success] = t('activerecord.success.model.j_managing.assign_job_ok')

    end

    redirect_to j_managing_manager_user_job_path(@user)


  end

  def remove_job

    user_j_job = UserJJob.find(params[:id])
    user = user_j_job.user
    remove_current = false
    remove_current = true if user_j_job.to_date.nil?

    characteristic = Characteristic.where('data_type_id = ?',13).first

    if characteristic

      if remove_current

        user_characteristic = user_j_job.user.user_characteristic characteristic
        user_characteristic_record = user_j_job.user.user_characteristic_records.where('characteristic_id = ? AND DATE(from_date) = ? AND to_date IS NULL', characteristic.id, user_j_job.from_date).first

      else

        user_characteristic_record = user_j_job.user.user_characteristic_records.where('characteristic_id = ? AND DATE(from_date) = ? AND DATE(to_date) = ?', characteristic.id, user_j_job.from_date, user_j_job.to_date).first

      end

    end

    if user_j_job.destroy

      if characteristic

        if user_characteristic_record

          original_values = user_characteristic_record.formatted_value+' | '+localize(user_characteristic_record.from_date, format: :full_date_time_log)+' | '+(user_characteristic_record.to_date ? localize(user_characteristic_record.to_date, format: :full_date_time_log) : '')

          if user_characteristic_record.destroy

            log_ct_modeule_um = LogCtModuleUm.new
            log_ct_modeule_um.task = 'delete record'
            log_ct_modeule_um.performed_at = lms_time
            log_ct_modeule_um.user = user_characteristic_record.user
            log_ct_modeule_um.performed_by_manager = user_connected
            log_ct_modeule_um.description = original_values
            log_ct_modeule_um.save

          end

        end

        if user_characteristic

          user_characteristic.user.update_first_owned_node(nil)

          original_values = user_characteristic.formatted_value

          if user_characteristic.destroy

            log_ct_modeule_um = LogCtModuleUm.new
            log_ct_modeule_um.task = 'delete characteristic'
            log_ct_modeule_um.performed_at = lms_time
            log_ct_modeule_um.user = user_characteristic.user
            log_ct_modeule_um.performed_by_manager = user_connected
            log_ct_modeule_um.description = original_values
            log_ct_modeule_um.save

          end

        end

      end

      if remove_current
        user.j_job_id = nil
        user.save
      end

      flash[:success] = t('activerecord.success.model.j_managing.remove_job_ok')
    else
      flash[:danger] = t('activerecord.error.model.j_managing.remove_job_error')
    end

    redirect_to j_managing_manager_user_job_path(user)

  end

  def hierarchy

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

  end

  def assign_as_boss_search

    @user_boss = User.find(params[:user_id])

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end
  end

  def assign_as_boss

    @user_boss = User.find(params[:user_boss_id])
    @user_sub = User.find(params[:user_id])

    unless @user_boss.owned_nodes.first

      node = @user_boss.owned_nodes.build
      node.nombre = ''
      node.save

    end

    @user_sub.node_to_which_belongs = @user_boss.owned_nodes.first

    @user_sub.save

    flash[:success] = t('activerecord.success.model.j_managing.assign_as_boss_ok')

    redirect_to j_managing_assign_as_boss_search_path(@user_boss)

  end

  def assign_as_sub_search

    @user_sub = User.find(params[:user_id])

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end
  end

  def assign_as_sub

    @user_sub = User.find(params[:user_sub_id])
    @user_boss = User.find(params[:user_id])

    unless @user_boss.owned_nodes.first

      node = @user_boss.owned_nodes.build
      node.nombre = ''
      node.save

    end

    @user_sub.node_to_which_belongs = @user_boss.owned_nodes.first

    @user_sub.save

    flash[:success] = t('activerecord.success.model.j_managing.assign_as_sub_ok')

    redirect_to j_managing_assign_as_boss_search_path(@user_boss)

  end
  
  def replace_boss_search

    @user_boss = User.find(params[:user_id])

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end
  end

  def replace_boss

    @user_boss = User.find(params[:user_boss_id])
    @user_new_boss = User.find(params[:user_id])

    unless @user_new_boss.owned_nodes.first

      node = @user_new_boss.owned_nodes.build
      node.nombre = ''
      node.save

    end

    @user_boss.members.each do |member|

      member.node_to_which_belongs = @user_new_boss.owned_nodes.first

      member.save

    end

    flash[:success] = t('activerecord.success.model.j_managing.replace_boss_ok')

    redirect_to j_managing_replace_boss_search_path(@user_new_boss)

  end

  def massive_replace_hierarchy

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0

            if fila[0] && fila[1]

              fila[0] = fila[0].to_s
              fila[1] = fila[1].to_s

              user_jefe_directo = User.find_by_codigo fila[0]
              user_sub_directo = User.find_by_codigo fila[1]

              if user_jefe_directo && user_sub_directo

                unless user_jefe_directo.owned_nodes.first

                  node = user_jefe_directo.owned_nodes.build
                  node.nombre = ''
                  unless node.save

                    @lineas_error.push index+1

                    @lineas_error_detalle.push ('codigo_jefe_directo: '+fila[0]+'; codigo_sub_directo: '+fila[1]).force_encoding('UTF-8')
                    @lineas_error_messages.push node.errors.full_messages

                  end

                end

                user_sub_directo.node_to_which_belongs = user_jefe_directo.owned_nodes.first

                unless user_sub_directo.save

                  @lineas_error.push index+1

                  @lineas_error_detalle.push ('codigo_jefe_directo: '+fila[0]+'; codigo_sub_directo: '+fila[1]).force_encoding('UTF-8')
                  @lineas_error_messages.push user_sub_directo.errors.full_messages

                end


              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo_jefe_directo: '+fila[0]+'; codigo_sub_directo: '+fila[1]).force_encoding('UTF-8')

                @lineas_error_messages.push [fila[0]+' no existe'] unless user_jefe_directo
                @lineas_error_messages.push [fila[1]+' no existe'] unless user_sub_directo

              end

            end

          end

        end


        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
    end



  end

  def massive_change_cc

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0

            if fila[0] && fila[1]

              fila[0] = fila[0].to_s
              fila[1] = fila[1].to_s

              user = User.find_by_codigo fila[0]

              if user

                jcc = JCostCenter.find_by_cc_id fila[1]

                if jcc
                  user.j_cost_center = jcc
                  unless user.save
                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('usuario: '+fila[0]).force_encoding('UTF-8')
                    @lineas_error_messages.push user.errors.full_messages
                  end
                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('cc: '+fila[1]).force_encoding('UTF-8')
                  @lineas_error_messages.push [fila[1]+' no existe']

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('usuario: '+fila[0]).force_encoding('UTF-8')
                @lineas_error_messages.push [fila[0]+' no existe']

              end

            end

          end

        end


        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
    end



  end

  def massive_change_job

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0

            if fila[0] && fila[1] && fila[2]

              fila[0] = fila[0].to_s
              fila[1] = fila[1].to_s

              from_date = fila[2]

              error_date = false
              begin
                x = from_date.to_datetime
              rescue
                error_date = true
              end

              unless error_date

                user = User.find_by_codigo fila[0]

                if user

                  j_job = JJob.find_by_j_code fila[1]

                  if j_job

                    user_j_job_last = user.user_j_jobs.last

                    if user_j_job_last && user_j_job_last.to_date.nil?
                      user_j_job_last.to_date = from_date-1.day
                      user_j_job_last.save
                    end

                    user_j_job = user.user_j_jobs.build
                    user_j_job.j_job = j_job
                    user_j_job.from_date = from_date
                    if user_j_job.save

                      user.j_job = j_job
                      unless user.save
                        @lineas_error.push index+1
                        @lineas_error_detalle.push ('usuario: '+fila[0]).force_encoding('UTF-8')
                        @lineas_error_messages.push user.errors.full_messages
                      end

                    end

                    characteristic = Characteristic.where('data_type_id = ?',13).first

                    if characteristic

                      user.update_user_characteristic(characteristic, user.j_job.name, from_date.to_datetime, user_connected, user_connected, nil, nil, @company)

                    end

                  else

                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('puesto: '+fila[1]).force_encoding('UTF-8')
                    @lineas_error_messages.push [fila[1]+' no existe']

                  end
                else
                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('usuario: '+fila[0]).force_encoding('UTF-8')
                  @lineas_error_messages.push [fila[0]+' no existe']
                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('fecha: '+fila[2].to_s).force_encoding('UTF-8')
                @lineas_error_messages.push [fila[2].to_s+' inválida']

              end

            end

          end

        end


        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
    end



  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'jobs', true).first

    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end


end