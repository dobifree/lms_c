class ScScheduleCharacteristicsController < ApplicationController
  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @characteristics = Characteristic.not_grouped_characteristics
    @characteristic_types = CharacteristicType.all
  end

  def edit
    @sc_schedule_characteristic = ScScheduleCharacteristic.find(params[:id])
    @characteristic = @sc_schedule_characteristic.characteristic
  end

  def update
    @sc_schedule_characteristic = ScScheduleCharacteristic.find(params[:id])
    redirect_to sc_schedule_characteristics_index_path if @sc_schedule_characteristic.update_attributes(params[:sc_schedule_characteristic])
  end
end
