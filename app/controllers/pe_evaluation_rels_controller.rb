class PeEvaluationRelsController < ApplicationController
  # GET /pe_evaluation_rels
  # GET /pe_evaluation_rels.json
  def index
    @pe_evaluation_rels = PeEvaluationRel.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_evaluation_rels }
    end
  end

  # GET /pe_evaluation_rels/1
  # GET /pe_evaluation_rels/1.json
  def show
    @pe_evaluation_rel = PeEvaluationRel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_evaluation_rel }
    end
  end

  # GET /pe_evaluation_rels/new
  # GET /pe_evaluation_rels/new.json
  def new
    @pe_evaluation_rel = PeEvaluationRel.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pe_evaluation_rel }
    end
  end

  # GET /pe_evaluation_rels/1/edit
  def edit
    @pe_evaluation_rel = PeEvaluationRel.find(params[:id])
  end

  # POST /pe_evaluation_rels
  # POST /pe_evaluation_rels.json
  def create
    @pe_evaluation_rel = PeEvaluationRel.new(params[:pe_evaluation_rel])

    respond_to do |format|
      if @pe_evaluation_rel.save
        format.html { redirect_to @pe_evaluation_rel, notice: 'Pe evaluation rel was successfully created.' }
        format.json { render json: @pe_evaluation_rel, status: :created, location: @pe_evaluation_rel }
      else
        format.html { render action: "new" }
        format.json { render json: @pe_evaluation_rel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pe_evaluation_rels/1
  # PUT /pe_evaluation_rels/1.json
  def update
    @pe_evaluation_rel = PeEvaluationRel.find(params[:id])

    respond_to do |format|
      if @pe_evaluation_rel.update_attributes(params[:pe_evaluation_rel])
        format.html { redirect_to @pe_evaluation_rel, notice: 'Pe evaluation rel was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pe_evaluation_rel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pe_evaluation_rels/1
  # DELETE /pe_evaluation_rels/1.json
  def destroy
    @pe_evaluation_rel = PeEvaluationRel.find(params[:id])
    @pe_evaluation_rel.destroy

    respond_to do |format|
      format.html { redirect_to pe_evaluation_rels_url }
      format.json { head :no_content }
    end
  end
end
