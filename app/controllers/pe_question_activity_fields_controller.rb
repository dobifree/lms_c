class PeQuestionActivityFieldsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def show
    @pe_question_activity_field = PeQuestionActivityField.find(params[:id])
    @pe_evaluation = @pe_question_activity_field.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def new

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])

    @pe_process = @pe_evaluation.pe_process

    @pe_question_activity_field = @pe_evaluation.pe_question_activity_fields.build

  end

  def create

    @pe_question_activity_field = PeQuestionActivityField.new(params[:pe_question_activity_field])

    if @pe_question_activity_field.save
      redirect_to @pe_question_activity_field
    else
      @pe_evaluation = @pe_question_activity_field.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'new'
    end

  end

  def edit
    @pe_question_activity_field = PeQuestionActivityField.find(params[:id])
    @pe_evaluation = @pe_question_activity_field.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def update

    @pe_question_activity_field = PeQuestionActivityField.find(params[:id])

    if @pe_question_activity_field.update_attributes(params[:pe_question_activity_field])
      redirect_to @pe_question_activity_field
    else
      @pe_evaluation = @pe_question_activity_field.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end

  def destroy
    @pe_question_activity_field = PeQuestionActivityField.find(params[:id])
    @pe_question_activity_field.destroy

    respond_to do |format|
      format.html { redirect_to pe_question_activity_fields_url }
      format.json { head :no_content }
    end
  end
end
