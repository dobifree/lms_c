class InformativeDocumentsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /informative_documents
  # GET /informative_documents.json
  def index
    if params[:inf_doc_folder_id]
      @inf_doc_folder = InfDocFolder.find(params[:inf_doc_folder_id])
      @informative_documents = @inf_doc_folder.informative_documents
    else
      @informative_documents = InformativeDocument.where(inf_doc_folder_id: nil)
    end
  end

  # GET /informative_documents/1
  # GET /informative_documents/1.json
  def show
    @informative_document = InformativeDocument.find(params[:id])
  end

  # GET /informative_documents/new
  # GET /informative_documents/new.json
  def new
    @informative_document = InformativeDocument.new
    @informative_document.inf_doc_folder_id = params[:inf_doc_folder_id] if params[:inf_doc_folder_id]
  end

  # GET /informative_documents/1/edit
  def edit
    @informative_document = InformativeDocument.find(params[:id])
  end

  # POST /informative_documents
  # POST /informative_documents.json
  def create

    @informative_document = InformativeDocument.new(params[:informative_document])

    if @informative_document.save
      flash[:success] = 'El Documento informativo fue creado correctamente'
      if @informative_document.inf_doc_folder
        redirect_to informative_documents_by_folder_path(@informative_document.inf_doc_folder)
      else
        redirect_to informative_documents_path
      end
    else
      flash.now[:danger] = 'El Documento informativo no puedo ser creado correctamente'
      render action: 'new'
    end

  end

  # PUT /informative_documents/1
  # PUT /informative_documents/1.json
  def update
    @informative_document = InformativeDocument.find(params[:id])

    if @informative_document.update_attributes(params[:informative_document])
      flash[:success] = 'El Documento informativo fue creado correctamente'
      if @informative_document.inf_doc_folder
        redirect_to informative_documents_by_folder_path(@informative_document.inf_doc_folder)
      else
        redirect_to informative_documents_path
      end
    else
      flash.now[:danger] = 'El Documento informativo no puedo ser creado correctamente'
      render action: 'edit'
    end

  end

  def edit_files
    @informative_document = InformativeDocument.find(params[:id])
  end

  def update_files
    @informative_document = InformativeDocument.find(params[:id])
    if params[:upload]

      @total_docs_cargadas = 0
      @docs_error = []
      @docs_error_messages = []

      upload = params[:upload]

     company = @company

      directory = company.directorio+'/'+company.codigo+'/informative_documents/'

      FileUtils.mkdir_p directory unless File.directory? directory

      directory_tmp = directory+'/'+SecureRandom.hex(5)+'/'

      FileUtils.mkdir_p directory_tmp unless File.directory? directory_tmp

      if File.extname(upload['datafile'].original_filename) == '.zip'

        name = 'infdocs.zip'
        path = File.join(directory_tmp, name)
        File.open(path, 'wb') { |f| f.write(upload['datafile'].read) }
        system "unzip -o -q \"#{directory_tmp+name}\" -d \"#{directory_tmp}\" "
        File.delete directory_tmp+name

        Dir.entries(directory_tmp).each do |document|

          if document != '.' && document != '..'

            ext = File.extname(document)

            if ext == '.pdf' || ext == '.PDF'

              file_tmp = directory_tmp+document

              codigo_usuario = File.basename(file_tmp, ext)

              user = User.find_by_codigo codigo_usuario

              if user

                file = directory+@informative_document.id.to_s+'/'+codigo_usuario+'.pdf'

                FileUtils.mkdir_p directory+@informative_document.id.to_s unless File.directory? directory+@informative_document.id.to_s

                FileUtils.mv(file_tmp, file)

                @total_docs_cargadas += 1

              else


                @docs_error.push document
                @docs_error_messages.push 'no existe usuario'

              end

            else

              @docs_error.push document
              @docs_error_messages.push 'formato incorrecto'

            end

          end

        end

        FileUtils.remove_dir directory_tmp

        flash[:success] =  t('activerecord.success.model.user.carga_masiva_ok')
        if @informative_document.inf_doc_folder
          redirect_to informative_documents_by_folder_path(@informative_document.inf_doc_folder)
        else
          redirect_to informative_documents_path
        end

      else

        flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_fotos_no_zip')

        render 'edit_files'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

      render 'edit_files'

    end

  end

  # DELETE /informative_documents/1
  # DELETE /informative_documents/1.json
  def destroy
    @informative_document = InformativeDocument.find(params[:id])
    folder = @informative_document.inf_doc_folder
    @informative_document.destroy

    if folder
      flash[:success] = 'El Documento informativo fue eliminado correctamente'
      redirect_to informative_documents_by_folder_path(@informative_document.inf_doc_folder)
    else
      flash[:danger] = 'El Documento informativo fue eliminado correctamente'
      redirect_to informative_documents_path
    end
  end
end
