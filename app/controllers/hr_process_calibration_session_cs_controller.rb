class HrProcessCalibrationSessionCsController < ApplicationController

  before_filter :authenticate_user

  before_filter :acceso_gestionar_proceso, only: [:index, :search, :create]
  before_filter :acceso_gestionar_proceso2, only: [:destroy]
  before_filter :acceso_gestionar_proceso3, only: [:update_manager]

  def index
    @hr_process_calibration_session_cs = @hr_process_calibration_session.hr_process_calibration_session_cs_order_by_apellidos_nombre
    @user = User.new
    @users = Array.new
  end

  def search

    @user = User.new
    @users = {}

    if params[:user]

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        @users = User.where(
            'apellidos LIKE ? AND nombre LIKE ?',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        @user.apellidos = params[:user][:apellidos]
        @user.nombre = params[:user][:nombre]
      end

    end

    @hr_process_calibration_session_cs = @hr_process_calibration_session.hr_process_calibration_session_cs_order_by_apellidos_nombre

    render 'index'

  end

  def create

    hr_process_calibration_session_c = @hr_process_calibration_session.hr_process_calibration_session_cs.build
    hr_process_calibration_session_c.user_id = params[:user_id]

    if hr_process_calibration_session_c.save
      flash[:success] = t('activerecord.success.model.hr_process_calibration_session_c.create_ok')
    end

    redirect_to listar_hr_process_calibration_session_cs_path(@hr_process_calibration_session)

  end

  def update_manager

    @hr_process_calibration_session_c.manager = !@hr_process_calibration_session_c.manager
    @hr_process_calibration_session_c.save
    flash[:success] = t('activerecord.success.model.hr_process_calibration_session_c.update_manager_ok')

    redirect_to listar_hr_process_calibration_session_cs_path(@hr_process_calibration_session)

  end

  def destroy

    @hr_process_calibration_session_c.destroy

    flash[:success] = t('activerecord.success.model.hr_process_calibration_session_c.delete_ok')

    redirect_to listar_hr_process_calibration_session_cs_path(@hr_process_calibration_session)

  end

  # GET /hr_process_calibration_session_cs/1
  # GET /hr_process_calibration_session_cs/1.json
  def show
    @hr_process_calibration_session_c = HrProcessCalibrationSessionC.find(params[:id])

    respond_to do |format|
      format.html # show.html.erbrb
      format.json { render json: @hr_process_calibration_session_c }
    end
  end

  # GET /hr_process_calibration_session_cs/new
  # GET /hr_process_calibration_session_cs/new.json
  def new
    @hr_process_calibration_session_c = HrProcessCalibrationSessionC.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @hr_process_calibration_session_c }
    end
  end

  # GET /hr_process_calibration_session_cs/1/edit
  def edit
    @hr_process_calibration_session_c = HrProcessCalibrationSessionC.find(params[:id])
  end

  # POST /hr_process_calibration_session_cs
  # POST /hr_process_calibration_session_cs.json


  # PUT /hr_process_calibration_session_cs/1
  # PUT /hr_process_calibration_session_cs/1.json
  def update
    @hr_process_calibration_session_c = HrProcessCalibrationSessionC.find(params[:id])

    respond_to do |format|
      if @hr_process_calibration_session_c.update_attributes(params[:hr_process_calibration_session_c])
        format.html { redirect_to @hr_process_calibration_session_c, notice: 'Hr process calibration session c was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @hr_process_calibration_session_c.errors, status: :unprocessable_entity }
      end
    end
  end



  private

    def acceso_gestionar_proceso

      @hr_process_calibration_session = HrProcessCalibrationSession.find(params[:hr_process_calibration_session_id])

      @hr_process = @hr_process_calibration_session.hr_process

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

      unless hr_process_manager

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end

    def acceso_gestionar_proceso2

      @hr_process_calibration_session_c = HrProcessCalibrationSessionC.find(params[:id])

      @hr_process_calibration_session = @hr_process_calibration_session_c.hr_process_calibration_session

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process_calibration_session.hr_process.id).first

      unless hr_process_manager

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end

  def acceso_gestionar_proceso3

    @hr_process_calibration_session_c = HrProcessCalibrationSessionC.find(params[:hr_process_calibration_session_c_id])

    @hr_process_calibration_session = @hr_process_calibration_session_c.hr_process_calibration_session

    hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process_calibration_session.hr_process.id).first

    unless hr_process_manager

      flash[:danger] = t('security.no_access_hr_process_manager')
      redirect_to root_path

    end

  end



end
