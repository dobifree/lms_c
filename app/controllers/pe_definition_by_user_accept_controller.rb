class PeDefinitionByUserAcceptController < ApplicationController

  include PeReportingHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:accept_definition, :rep_definition_pdf]

  before_filter :validate_open_process, only: [:accept_definition, :rep_definition_pdf]
  before_filter :has_to_accept_definition, only: [:accept_definition]
  before_filter :validate_rep_definition_pdf, only: [:rep_definition_pdf]

  def accept_definition

    @pe_members_connected.each do |pe_member_connected|

      @pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_connected).each do |pe_question|

        pe_question.accepted = true
        pe_question.save

      end

    end

    if @pe_evaluation.send_email_after_accept_definition

      menu_name = 'Evaluación de desempeño'

      ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

        if ct_menu_cod == 'pe'
          menu_name = ct_menus_user_menu_alias[index_m]
          break
        end
      end

      begin
        PeMailer.step_def_by_users_after_accept_definition(@pe_process, @company, menu_name, @pe_evaluation, @pe_member_connected.user, @creators).deliver
      rescue Exception => e
        lm = LogMailer.new
        lm.module = 'pe'
        lm.step = 'def_by_user:accept'
        lm.description = e.message+' '+e.backtrace.inspect
        lm.registered_at = lms_time
        lm.save
      end

    end

    redirect_to pe_def_by_user_people_list_path @pe_evaluation

  end

  def rep_definition_pdf
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_definition_pdf archivo_temporal, @pe_member_connected, @pe_process, @pe_evaluation

    send_file archivo_temporal,
              filename: 'Definición '+@pe_evaluation.name+' - '+@pe_member_connected.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'
  end

  private

  def get_data_1

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process

    @pe_members_connected = @pe_process.pe_members_by_user user_connected

    @pe_member_connected = @pe_process.pe_member_by_user user_connected

    @creators = @pe_evaluation.pe_creators_of_pe_member_questions(@pe_member_connected)

    @creators.uniq!

    #pe_member_evaluator_boss = @pe_member_evaluated.pe_member_evaluator_boss

    #@creators.push(pe_member_evaluator_boss.user) if pe_member_evaluator_boss

    #@creators.uniq!

  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def has_to_accept_definition

    has_to = false

    if @pe_process.has_step_definition_by_users? && @pe_process.has_step_definition_by_users_accepted?

      rel_ids = @pe_evaluation.rel_ids_to_define_elements

      @pe_members_connected.each do |pe_member|

        rel_ids.each do |rel_id|

          if pe_member.has_to_be_assesed_by_rel_id(rel_id) && @pe_evaluation.has_rel_id(rel_id)
            has_to = true
            break
          end

        end

      end

    end

    unless has_to

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_accept')
      redirect_to root_path

    end

  end

  def validate_rep_definition_pdf
    unless @pe_evaluation.definition_by_users_accept_pdf?
      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_def')
      redirect_to root_path
    end
  end

end
