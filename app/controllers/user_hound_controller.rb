class UserHoundController < ApplicationController

  before_filter :authenticate_user

  def main
    render :layout => 'flat'
  end

  def show_select2_finder
    @users = User.where(:activo => true).map{|user| [user.codigo + ' - ' + user.comma_full_name, user.id]}
    render :layout => false
  end

  def show_basic_finder
    @user = User.new()
    @users = {}
    render :layout => false
  end

  def find_by_basic_data

    if !params[:user][:codigo].blank? || !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?
        @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')
    else
      @users = {}
    end

    render 'list_found_users',  :layout => false

  end

  def show_chars_finder
    @characteristics = Characteristic.all
    @users = {}
    render :layout => false
  end

  def find_by_chars

    @characteristics = Characteristic.all
    @users = {}

    queries = []
    n_q = 0
    @characteristics.each do |characteristic|
      params[:characteristic][characteristic.id.to_s.to_sym].slice! 0
      if params[:characteristic][characteristic.id.to_s.to_sym].length > 0
        queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND ( '
        params[:characteristic][characteristic.id.to_s.to_sym].each_with_index do |valor,index|
          if index > 0
            queries[n_q] += ' OR '
          end
          queries[n_q] += 'valor = "'+valor+'" '
        end
        queries[n_q] += ' ) '
        n_q += 1
      end
    end

    if queries.length > 0
      queries.each_with_index do |query,index|
        if index == 0
          @users = User.joins(:user_characteristics).where(query).order('apellidos, nombre')
        else
          lista_users_ids = @users.map {|u| u.id }
          @users = User.joins(:user_characteristics).where(query+' AND users.id IN (?) ', lista_users_ids).order('apellidos, nombre')
        end
      end
    else
      @users = User.all
    end

    render 'list_found_users',  :layout => false

  end

end
