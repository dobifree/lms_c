class UnitsController < ApplicationController
  
  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new
    @course = Course.find(params[:course_id])
    @unit = @course.units.build()
  end
  
  def edit
    @unit = Unit.find(params[:id])
    @course = @unit.course
  end

  def edit_master_unit
    @unit = Unit.find(params[:id])
    @course = @unit.course
  end

  def delete
    @unit = Unit.find(params[:id])
    @course = @unit.course
  end  

  def create

    @course = Course.find(params[:course][:id])

    @unit = @course.units.build(params[:unit])

    if @unit.save
      flash[:success] = t('activerecord.success.model.unit.create_ok')
      redirect_to @course
    else
      render action: 'new'
    end

  end

  
  def update
    
    @unit = Unit.find(params[:id])

    if @unit.update_attributes(params[:unit])
      flash[:success] = t('activerecord.success.model.unit.update_ok')
      redirect_to @unit.course
    else
      @course = @unit.course
      render action: 'edit'
    end

  end

  def update_finaliza_automaticamente


    unit = Unit.find(params[:id])

    unit.update_attribute(:finaliza_automaticamente, !unit.finaliza_automaticamente)

    flash[:success] = t('activerecord.success.model.unit.update_finaliza_automaticamente_ok')

    redirect_to unit.course


  end

  def update_master_unit
    
    @unit = Unit.find(params[:unit][:id])

    if @unit.update_attributes(master_unit_id: params[:unit][:master_unit_id])
      flash[:success] = t('activerecord.success.model.unit.update_ok')
      redirect_to @unit.course
    else
      @course = @unit.course
      render action: 'edit_master_unit'
    end

  end

  def destroy
  
    @unit = Unit.find(params[:id])

    if verify_recaptcha
    
      units_sequence = @unit.despues_de_units + @unit.antes_de_units
      evaluations_sequence = @unit.despues_de_evaluations + @unit.antes_de_evaluations
      polls_sequence = @unit.despues_de_polls + @unit.antes_de_polls
      

      if @unit.destroy
        
        units_sequence.each { |unit| unit.reload.update_libre }
        evaluations_sequence.each { |evaluation| evaluation.reload.update_libre_course }
        polls_sequence.each { |poll| poll.reload.update_libre_course }

        flash[:success] = t('activerecord.success.model.unit.delete_ok')
        redirect_to course_path(@unit.course)
      else
        flash[:danger] = t('activerecord.success.model.unit.delete_error')
        redirect_to delete_unit_path(@unit)
      end

    else

      flash.delete(:recaptcha_error)
      
      flash[:danger] = t('activerecord.error.model.unit.captcha_error')
      redirect_to delete_unit_path(@unit)

    end


  end

  
end
