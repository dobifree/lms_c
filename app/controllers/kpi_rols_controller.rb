class KpiRolsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def new

    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
    @kpi_rol = @kpi_dashboard.kpi_rols.build

  end

  def edit
    @kpi_rol = KpiRol.find(params[:id])
    @kpi_dashboard = @kpi_rol.kpi_dashboard
  end

  def create
    @kpi_rol = KpiRol.new(params[:kpi_rol])

    if @kpi_rol.save
      flash[:success] =  t('activerecord.success.model.kpi_rol.create_ok')
      redirect_to @kpi_rol.kpi_dashboard
    else
      @kpi_dashboard = @kpi_rol.kpi_dashboard
      render action: 'new'
    end
  end


  def update
    @kpi_rol = KpiRol.find(params[:id])

    if @kpi_rol.update_attributes(params[:kpi_rol])
      flash[:success] =  t('activerecord.success.model.kpi_rol.update_ok')
      redirect_to @kpi_rol.kpi_dashboard
    else
      @kpi_dashboard = @kpi_rol.kpi_dashboard
      render action: 'edit'
    end

  end

  def destroy

    kpi_rol = KpiRol.find(params[:id])
    kpi_dashboard = kpi_rol.kpi_dashboard
    kpi_rol.destroy

    flash[:success] =  t('activerecord.success.model.kpi_rol.delete_ok')
    redirect_to kpi_dashboard

  end

  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'kpis', true).first

      unless admin_logged_in? || (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end

end
