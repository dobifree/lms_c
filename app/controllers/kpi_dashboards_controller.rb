class KpiDashboardsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def index
    @kpi_dashboards = KpiDashboard.all
  end

  def show
    @kpi_dashboard = KpiDashboard.find(params[:id])
  end

  def new
    @kpi_dashboard = KpiDashboard.new
    @kpi_dashboard.from = Date.today
    @kpi_dashboard.to = @kpi_dashboard.from
  end

  def create

    @kpi_dashboard = KpiDashboard.new(params[:kpi_dashboard])

    @kpi_dashboard.from, @kpi_dashboard.to = get_from_to params[:from_to]

    if @kpi_dashboard.save
      flash[:success] = t('activerecord.success.model.kpi_dashboard.create_ok')
      redirect_to kpi_dashboards_path
    else
      render action: 'new'
    end
  end

  def edit
    @kpi_dashboard = KpiDashboard.find(params[:id])
  end

  def update
    @kpi_dashboard = KpiDashboard.find(params[:id])

    @kpi_dashboard.from, @kpi_dashboard.to = get_from_to params[:from_to]

    if @kpi_dashboard.update_attributes(params[:kpi_dashboard])
      flash[:success] = t('activerecord.success.model.kpi_dashboard.update_ok')
      redirect_to @kpi_dashboard
    else
      render action: 'edit'
    end

  end

  def edit_csv
    @kpi_dashboard = KpiDashboard.find(params[:id])
  end

  def update_csv

    #@folder = Folder.find(params[:id])
   company = @company

    @kpi_dashboard = KpiDashboard.find(params[:id])

    if params[:upload]

      directory = company.directorio+'/'+company.codigo+'/kpi_dashboards/'

      FileUtils.mkdir_p directory unless File.directory?(directory)

      name = @kpi_dashboard.id.to_s+'.csv'
      path = File.join(directory, name)
      File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

      flash[:success] = t('activerecord.success.model.kpi_dashboard.csv_create_ok')
      redirect_to @kpi_dashboard


    else
      #@folder_file.errors.add(:archivo, t('activerecord.errors.models.folder_file.attributes.archivo.blank'))
      render 'edit_csv'
    end

  end

  def select_dashboard_to_clone
    @kpi_dashboard = KpiDashboard.find(params[:id])
  end

  def clone_dashboard
    @kpi_dashboard = KpiDashboard.find(params[:id])



    if params[:dahsboard_to_clone_id].blank?

      flash[:danger] = t('activerecord.error.model.kpi_dashboard.select_to_clone_error')
      redirect_to kpi_dashboard_clone_dashboard_path @kpi_dashboard

    else



      kpi_dashoard_to_clone = KpiDashboard.find(params[:dahsboard_to_clone_id])

      if kpi_dashoard_to_clone

        @kpi_dashboard.kpi_sets.destroy_all
        @kpi_dashboard.kpi_colors.destroy_all

        kpi_dashoard_to_clone.kpi_colors.each do |kpi_color|

          kpi_color_clone = @kpi_dashboard.kpi_colors.build
          kpi_color_clone.color = kpi_color.color
          kpi_color_clone.percentage = kpi_color.percentage
          kpi_color_clone.save

        end

        kpi_dashoard_to_clone.kpi_sets.each do |kpi_set|

          kpi_set_clone = @kpi_dashboard.kpi_sets.build
          kpi_set_clone.name = kpi_set.name
          kpi_set_clone.position = kpi_set.position
          kpi_set_clone.save

          if @kpi_dashboard.allow_subsets?

            kpi_set.kpi_subsets.each do |kpi_subset|

              kpi_subset_clone = kpi_set_clone.kpi_subsets.build
              kpi_subset_clone.name = kpi_subset.name
              kpi_subset_clone.position = kpi_subset.position
              kpi_subset_clone.save

              kpi_subset.kpi_indicators.each do |kpi_indicator|

                kpi_indicator_clone = kpi_subset_clone.kpi_indicators.build
                kpi_indicator_clone.name = kpi_indicator.name
                kpi_indicator_clone.position = kpi_indicator.position
                kpi_indicator_clone.indicator_type = kpi_indicator.indicator_type
                kpi_indicator_clone.unit = kpi_indicator.unit
                kpi_indicator_clone.weight = kpi_indicator.weight
                kpi_indicator_clone.kpi_set = kpi_set_clone
                kpi_indicator_clone.save

              end

            end

          else

            kpi_set.kpi_indicators.each do |kpi_indicator|

              kpi_indicator_clone = kpi_set_clone.kpi_indicators.build
              kpi_indicator_clone.name = kpi_indicator.name
              kpi_indicator_clone.position = kpi_indicator.position
              kpi_indicator_clone.indicator_type = kpi_indicator.indicator_type
              kpi_indicator_clone.unit = kpi_indicator.unit
              kpi_indicator_clone.weight = kpi_indicator.weight
              kpi_indicator_clone.save

            end

          end

        end

        flash[:success] = t('activerecord.success.model.kpi_dashboard.csv_create_ok')

      else

        flash[:danger] = t('activerecord.error.model.kpi_dashboard.clone_error')

      end


      redirect_to @kpi_dashboard

    end



  end

  def delete
    @kpi_dashboard = KpiDashboard.find(params[:id])
  end

  def destroy

    @kpi_dashboard = KpiDashboard.find(params[:id])

    if verify_recaptcha

      if @kpi_dashboard.destroy
        flash[:success] = t('activerecord.success.model.kpi_dashboard.delete_ok')
        redirect_to kpi_dashboards_url
      else
        flash[:danger] = t('activerecord.error.model.kpi_dashboard.delete_error')
        redirect_to kpi_dashboard_delete_path(@kpi_dashboard)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.kpi_dashboard.captcha_error')
      redirect_to kpi_dashboard_delete_path(@kpi_dashboard)

    end

  end

  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'kpis', true).first

      unless admin_logged_in? || (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end

end
