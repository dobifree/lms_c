class BrcManagingController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def massive_assign_members

    @brc_process = BrcProcess.find(params[:brc_process_id])

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          next if index == 0

          fila[0] = fila[0].to_s
          fila[1] = fila[1].to_s
          fila[2] = fila[2].to_s

          user_member = User.find_by_codigo fila[0]
          user_define = User.find_by_codigo fila[1]
          user_validate = User.find_by_codigo fila[2]

          if user_member && user_define && user_validate

            brc_member = @brc_process.brc_member user_member

            unless brc_member
              brc_member = @brc_process.brc_members.build
            end

            brc_member.user = user_member
            brc_member.user_define = user_define
            brc_member.user_validate = user_validate

            unless brc_member.save
              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo_usuario_participante: '+fila[0]+'; codigo_usuario_responsable_definir: '+fila[1]+'; codigo_usuario_responsable_validar: '+fila[2]).force_encoding('UTF-8')
              @lineas_error_messages.push brc_member.errors.full_messages
            end

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('codigo_usuario_participante: '+fila[0]+'; codigo_usuario_responsable_definir: '+fila[1]+'; codigo_usuario_responsable_validar: '+fila[2]).force_encoding('UTF-8')

            @lineas_error_messages.push [fila[0]+' no existe'] unless user_member
            @lineas_error_messages.push [fila[1]+' no existe'] unless user_define
            @lineas_error_messages.push [fila[2]+' no existe'] unless user_validate

          end

        end

        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def massive_assign_definers

    @brc_process = BrcProcess.find(params[:brc_process_id])

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          next if index == 0

          fila[0] = fila[0].to_s
          fila[1] = fila[1].to_s

          user_define = User.find_by_codigo fila[0]
          user_validate = User.find_by_codigo fila[1]

          if user_define && user_validate

            brc_definer = @brc_process.brc_definer user_define

            unless brc_definer
              brc_definer = @brc_process.brc_definers.build
            end

            brc_definer.user = user_define
            brc_definer.user_validate = user_validate

            if brc_definer.save

              brc_definer.brc_definer_companies.destroy_all

              (2..40).each do |i|

                if fila[i]

                  cv = CharacteristicValue.where('value_string = ?', fila[i]).first

                  if cv

                    brc_definer_company = brc_definer.brc_definer_companies.build

                    brc_definer_company.characteristic_value = cv

                    brc_definer_company.save

                  else

                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('Empresa: '+fila[i]).force_encoding('UTF-8')
                    @lineas_error_messages.push [fila[i]+' no existe']

                  end

                end

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo_usuario_responsable_definir: '+fila[0]+'; codigo_usuario_responsable_validar: '+fila[1]).force_encoding('UTF-8')
              @lineas_error_messages.push brc_definer.errors.full_messages
            end

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('codigo_usuario_responsable_definir: '+fila[0]+'; codigo_usuario_responsable_validar: '+fila[1]).force_encoding('UTF-8')

            @lineas_error_messages.push [fila[0]+' no existe'] unless user_define
            @lineas_error_messages.push [fila[1]+' no existe'] unless user_validate

          end

        end

        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def massive_charge_bonus_calculated

    @brc_process = BrcProcess.find(params[:brc_process_id])

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        archivo = RubyXL::Parser.parse archivo_temporal

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          next if index == 0

          #Rut	Nombre	Bono_Bruto	Bono_Liquido	Retención	%Retención

          fila[0] = fila[0].to_s #RUT
          fila[1] = fila[1].to_s #Nombre

          user_member = User.find_by_codigo fila[0]

          if user_member

            brc_member = @brc_process.brc_member user_member

            if brc_member

              if brc_member.status_def && brc_member.status_val

                if brc_member.status_choose

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('codigo_usuario_participante: '+fila[0]).force_encoding('UTF-8')
                  @lineas_error_messages.push [fila[0]+' bono ya ha sido distribuído']

                else

                  brc_member.status_bon_cal = true
                  brc_member.bonus_total = fila[2].round
                  brc_member.bonus_liquido = fila[3].round
                  brc_member.bonus_retencion = fila[4].round
                  brc_member.bonus_p_retencion = fila[5]

                  unless brc_member.save
                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('codigo_usuario_participante: '+fila[0]).force_encoding('UTF-8')
                    @lineas_error_messages.push brc_member.errors.full_messages
                  end

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo_usuario_participante: '+fila[0]).force_encoding('UTF-8')
                @lineas_error_messages.push [fila[0]+' definición no finalizada y validada']

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo_usuario_participante: '+fila[0]).force_encoding('UTF-8')
              @lineas_error_messages.push [fila[0]+' no existe']

            end

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('codigo_usuario_participante: '+fila[0]).force_encoding('UTF-8')
            @lineas_error_messages.push [fila[0]+' no existe']

          end

        end

        flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')

    end

  end

  def download_massive_assign_members_excel_format

    reporte_excel = massive_assign_members_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_carga.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_massive_assign_definers_excel_format

    reporte_excel = massive_assign_definers_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_carga.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_bonus_defined_excel_format

    brc_process = BrcProcess.find(params[:brc_process_id])

    reporte_excel = bonus_defined_excel_format brc_process

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Bonos_definidos.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_bonus_chosen_excel_format

    brc_process = BrcProcess.find(params[:brc_process_id])

    reporte_excel = bonus_chosen_excel_format brc_process

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Bonos_dist_payroll.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_massive_charge_bonus_calculated_excel_format

    reporte_excel = massive_charge_bonus_calculated_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_carga_bonos_payroll.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def massive_choose_list

    @characteristic_company = Characteristic.get_characteristic_company

    @brc_process = BrcProcess.find(params[:brc_process_id])
    @brc_members_ordered = @brc_process.brc_members_ordered

  end

  def massive_choose
    @brc_process = BrcProcess.find(params[:brc_process_id])

    @brc_process.brc_members.each do |brc_member|

      if brc_member.status_def && brc_member.status_val && brc_member.status_bon_cal && !brc_member.status_choose

        brc_member.bonus_cc = true
        brc_member.choose_by_own = false
        brc_member.choose_date = lms_time
        brc_member.status_choose = true

        brc_member.save
      end

    end

    flash[:success] = t('activerecord.success.model.brc_member.choose_ok')

    redirect_to brc_managing_massive_choose_list_path(@brc_process)

  end

  def download_bonus_chosen_excel_format_2

    brc_process = BrcProcess.find(params[:brc_process_id])

    reporte_excel = bonus_chosen_excel_format_2 brc_process

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Bonos_dist.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'brc', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def massive_assign_members_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_programas')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'codigo_usuario_participante'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'codigo_usuario_responsable_definir'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'codigo_usuario_responsable_validar'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def massive_assign_definers_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_programas')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'codigo_usuario_responsable_definir'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'codigo_usuario_responsable_validar'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Empresa'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Empresa'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Empresa'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def bonus_defined_excel_format brc_process

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Bonos')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'Rut'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Monto Bruto'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Cohade'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Usuario'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Empresa'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        characteristic_company = Characteristic.get_characteristic_company

        brc_process.brc_members_ordered.each do |brc_member|

          if brc_member && brc_member.status_def && brc_member.status_val && !brc_member.status_choose

            uc_c = brc_member.user.user_characteristic(characteristic_company)

            brc_member.brc_bonus_items.each do |brc_bonus_item|

              fila = Array.new
              fileStyle = Array.new

              fila << brc_member.user.codigo
              fileStyle << fieldLeft

              if brc_bonus_item.p_a
                fila << brc_bonus_item.amount_c
              else
                fila << brc_bonus_item.amount
              end

              fileStyle << fieldLeft

              fila << brc_bonus_item.secu_cohade.name
              fileStyle << fieldLeft

              fila << brc_member.user.apellidos+', '+brc_member.user.nombre
              fileStyle << fieldLeft

              fila << (uc_c ? uc_c.formatted_value : '')
              fileStyle << fieldLeft


              reporte_excel.add_row fila, :style => fileStyle, height: 20

            end

          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def bonus_chosen_excel_format brc_process

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Bonos')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new



        fila << 'Rut'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Bono Bruto'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Bono Liquido'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Retención'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << '% Retención'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Cta Cte'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Deposito APV'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Deposito Convenido'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Institucion de Pago APV'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Institucion de Pago Convenido'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        brc_process.brc_members_ordered.each do |brc_member|

          if brc_member && brc_member.status_choose

            fila = Array.new
            fileStyle = Array.new

            fila << brc_member.user.codigo
            fileStyle << fieldLeft

            fila << brc_member.user.apellidos+', '+brc_member.user.nombre
            fileStyle << fieldLeft

            fila << brc_member.bonus_total
            fileStyle << fieldLeft

            fila << brc_member.bonus_liquido
            fileStyle << fieldLeft

            fila << brc_member.bonus_retencion
            fileStyle << fieldLeft

            fila << brc_member.bonus_p_retencion
            fileStyle << fieldLeft

            if brc_member.bonus_cc
              fila << 'SI'
            else

              if brc_member.bonus_apv + brc_member.bonus_convenio == brc_member.bonus_total
                fila << 'NO'
              else
                fila << 'SI'
              end

            end
            fileStyle << fieldLeft

            fila << brc_member.bonus_apv
            fileStyle << fieldLeft

            fila << brc_member.bonus_convenio
            fileStyle << fieldLeft

            fila << (brc_member.brc_apv_institution ? brc_member.brc_apv_institution.name : '')
            fileStyle << fieldLeft

            fila << (brc_member.brc_con_institution ? brc_member.brc_con_institution.name : '')
            fileStyle << fieldLeft

            reporte_excel.add_row fila, :style => fileStyle, height: 20


          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def bonus_chosen_excel_format_2 brc_process

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Bonos distribuidos')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new



        fila << 'Rut'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Bono Bruto'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Bono Liquido'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Retención'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << '% Retención'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Cta Cte'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Deposito APV'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Deposito Convenido'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Institucion de Pago APV'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Institucion de Pago Convenido'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Distribuido por usuario'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Fecha distribución'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Hora distribución'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        brc_process.brc_members_ordered.each do |brc_member|

          if brc_member && brc_member.status_choose

            fila = Array.new
            fileStyle = Array.new

            fila << brc_member.user.codigo
            fileStyle << fieldLeft

            fila << brc_member.user.apellidos+', '+brc_member.user.nombre
            fileStyle << fieldLeft

            fila << brc_member.bonus_total
            fileStyle << fieldLeft

            fila << brc_member.bonus_liquido
            fileStyle << fieldLeft

            fila << brc_member.bonus_retencion
            fileStyle << fieldLeft

            fila << brc_member.bonus_p_retencion
            fileStyle << fieldLeft

            if brc_member.bonus_cc
              fila << 'SI'
            else

              if brc_member.bonus_apv + brc_member.bonus_convenio == brc_member.bonus_total
                fila << 'NO'
              else
                fila << 'SI'
              end

            end
            fileStyle << fieldLeft

            fila << brc_member.bonus_apv
            fileStyle << fieldLeft

            fila << brc_member.bonus_convenio
            fileStyle << fieldLeft

            fila << (brc_member.brc_apv_institution ? brc_member.brc_apv_institution.name : '')
            fileStyle << fieldLeft

            fila << (brc_member.brc_con_institution ? brc_member.brc_con_institution.name : '')
            fileStyle << fieldLeft

            fila << (brc_member.choose_by_own ? 'SI' : 'NO')
            fileStyle << fieldLeft

            fila << brc_member.choose_date.strftime('%d-%m-%Y')
            fileStyle << fieldLeft

            fila << brc_member.choose_date.strftime('%H:%M')
            fileStyle << fieldLeft

            reporte_excel.add_row fila, :style => fileStyle, height: 20


          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def massive_charge_bonus_calculated_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_bonos_payroll')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'RUT'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombres (opcional)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Bono Bruto'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Bono Líquido'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Retención'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << '% Retención'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

end
