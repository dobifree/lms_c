class ElecProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /elec_processes
  # GET /elec_processes.json
  def index
    @elec_processes = ElecProcess.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @elec_processes }
    end
  end

  # GET /elec_processes/1
  # GET /elec_processes/1.json
  def show
    @elec_process = ElecProcess.find(params[:id])

    if params[:pill_name]
      @show_pill_open = true
      @pill_name_open = params[:pill_name]
    end

  end

  # GET /elec_processes/new
  # GET /elec_processes/new.json
  def new
    @elec_process = ElecProcess.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @elec_process }
    end
  end

  # GET /elec_processes/1/edit
  def edit
    @elec_process = ElecProcess.find(params[:id])
  end

  # POST /elec_processes
  # POST /elec_processes.json
  def create
    @elec_process = ElecProcess.new(params[:elec_process])

    respond_to do |format|
      if @elec_process.save
        format.html { redirect_to @elec_process, notice: 'Elec process was successfully created.' }
        format.json { render json: @elec_process, status: :created, location: @elec_process }
      else
        format.html { render action: "new" }
        format.json { render json: @elec_process.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /elec_processes/1
  # PUT /elec_processes/1.json
  def update
    @elec_process = ElecProcess.find(params[:id])

    respond_to do |format|
      if @elec_process.update_attributes(params[:elec_process])
        format.html { redirect_to @elec_process, notice: 'Elec process was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @elec_process.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /elec_processes/1
  # DELETE /elec_processes/1.json
  def destroy
    @elec_process = ElecProcess.find(params[:id])
    @elec_process.destroy

    respond_to do |format|
      format.html { redirect_to elec_processes_url }
      format.json { head :no_content }
    end
  end

  def show_schedule_detail
    @elec_process = ElecProcess.find(params[:id])

    render 'elec_processes/schedule/main', :layout => false

  end

  def show_rounds_detail
    @elec_process = ElecProcess.find(params[:id])

    render 'elec_processes/rounds/main', :layout => false

  end
end
