class PeObserveController < ApplicationController

  include PeReportingHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:definition_by_user, :tracking]
  before_filter :get_data_2, only: [:assessment]
  before_filter :get_data_3, only: [:pe_rep_detailed_final_results, :pe_rep_detailed_final_results_pdf]
  before_filter :get_data_4, only: [:final_results_pdf_node]

  before_filter :can_observe, only: [:definition_by_user, :tracking, :assessment, :pe_rep_detailed_final_results]
  before_filter :can_observe_or_feedback, only: [:pe_rep_detailed_final_results_pdf]


  def definition_by_user

  end


  def tracking


  end

  def assessment


  end

  def pe_rep_detailed_final_results


  end

  def pe_rep_detailed_final_results_pdf

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member_evaluated_report, @pe_process_report, @pe_process_report.rep_detailed_final_results_show_feedback_for_my_people, @pe_process_report.rep_detailed_final_results_show_calibration_for_my_people, @pe_process_report.public_pe_box, @pe_process_report.public_pe_dimension_name, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member_evaluated_report.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def final_results_pdf_node



    hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    is_boss = true

    generate_final_results_pdf archivo_temporal, hr_process_user.user, @hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations, is_boss

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

  def get_data_1

    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
    @pe_member_evaluated = PeMember.find(params[:pe_member_evaluated_id])
   #@company = session[:company]

  end

  def get_data_2

    @pe_member_rel = PeMemberRel.find(params[:pe_member_rel_id])
    @pe_rel = @pe_member_rel.pe_rel
    @pe_process = @pe_member_rel.pe_process
    @pe_member_evaluated = @pe_member_rel.pe_member_evaluated
    @pe_member_evaluator = @pe_member_rel.pe_member_evaluator

    @pe_member_rel_auto = @pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member @pe_member_evaluated

   #@company = session[:company]

  end

  def get_data_3

    @user = User.find params[:user_id]

    @pe_process_report = PeProcess.find(params[:pe_process_report_id])
    @pe_member_evaluated_report = @pe_process_report.pe_member_by_user @user

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_member_evaluated = @pe_process.pe_member_by_user @user


   #@company = session[:company]

  end

  def get_data_4

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    @hr_process = @hr_process_user.hr_process

    @user = User.find params[:user_id]
    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_member_evaluated = @pe_process.pe_member_by_user @user

  end

  def can_observe

    unless @pe_process.is_observer_of? user_connected, @pe_member_evaluated

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_observe')
      redirect_to root_path

    end

  end

  def can_observe_or_feedback

    unless @pe_process.is_observer_of?(user_connected, @pe_member_evaluated) || @pe_member_evaluated.feedback_provider_id == user_connected.id

      flash[:danger] = t('activerecord.error.model.pe_process_def_by_user.cant_observe')
      redirect_to root_path

    end

  end

end