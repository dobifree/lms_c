class KpiDashboardTypesController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def index
    @kpi_dashboard_types = KpiDashboardType.all
  end


  def show
    @kpi_dashboard_type = KpiDashboardType.find(params[:id])
  end

  def new
    @kpi_dashboard_type = KpiDashboardType.new
  end

  def edit
    @kpi_dashboard_type = KpiDashboardType.find(params[:id])
  end

  def create
    @kpi_dashboard_type = KpiDashboardType.new(params[:kpi_dashboard_type])

    if @kpi_dashboard_type.save
      flash[:success] = t('activerecord.success.model.kpi_dashboard_type.create_ok')
      redirect_to kpi_dashboard_types_path
    else
      render action: 'new'
    end

  end

  def update
    @kpi_dashboard_type = KpiDashboardType.find(params[:id])

    if @kpi_dashboard_type.update_attributes(params[:kpi_dashboard_type])
      flash[:success] = t('activerecord.success.model.kpi_dashboard_type.update_ok')
      redirect_to kpi_dashboard_types_path
    else
      render action: 'edit'
    end

  end

  def destroy

    @kpi_dashboard_type = KpiDashboardType.find(params[:id])

    @kpi_dashboard_type.destroy if @kpi_dashboard_type.kpi_dashboards.size == 0

    flash[:success] = t('activerecord.success.model.kpi_dashboard_type.delete_ok')

    redirect_to kpi_dashboard_types_path

  end

  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'kpis', true).first

      unless admin_logged_in? || (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end

end
