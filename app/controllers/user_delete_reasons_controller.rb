class UserDeleteReasonsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /user_delete_reasons
  # GET /user_delete_reasons.json
  def index
    @user_delete_reasons = UserDeleteReason.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @user_delete_reasons }
    end
  end

  # GET /user_delete_reasons/1
  # GET /user_delete_reasons/1.json
  def show
    @user_delete_reason = UserDeleteReason.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user_delete_reason }
    end
  end

  # GET /user_delete_reasons/new
  # GET /user_delete_reasons/new.json
  def new
    @user_delete_reason = UserDeleteReason.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_delete_reason }
    end
  end

  # GET /user_delete_reasons/1/edit
  def edit
    @user_delete_reason = UserDeleteReason.find(params[:id])
  end

  # POST /user_delete_reasons
  # POST /user_delete_reasons.json
  def create
    @user_delete_reason = UserDeleteReason.new(params[:user_delete_reason])

    respond_to do |format|
      if @user_delete_reason.save
        format.html { redirect_to @user_delete_reason, notice: 'User delete reason was successfully created.' }
        format.json { render json: @user_delete_reason, status: :created, location: @user_delete_reason }
      else
        format.html { render action: "new" }
        format.json { render json: @user_delete_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /user_delete_reasons/1
  # PUT /user_delete_reasons/1.json
  def update
    @user_delete_reason = UserDeleteReason.find(params[:id])

    respond_to do |format|
      if @user_delete_reason.update_attributes(params[:user_delete_reason])
        format.html { redirect_to @user_delete_reason, notice: 'User delete reason was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user_delete_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_delete_reasons/1
  # DELETE /user_delete_reasons/1.json
  def destroy
    @user_delete_reason = UserDeleteReason.find(params[:id])
    @user_delete_reason.destroy

    respond_to do |format|
      format.html { redirect_to user_delete_reasons_url }
      format.json { head :no_content }
    end
  end
end
