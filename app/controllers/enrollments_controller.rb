class EnrollmentsController < ApplicationController

  include ActionView::Helpers::NumberHelper
  include ExcelReportsHelper
  
  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def index

   #@company = session[:company]

  end

  def ver_programa

   #@company = session[:company]
    @program = Program.find(params[:program_id])


  end

  def reportes

   #@company = session[:company]

  end




  def matricular1

   #@company = session[:company]

    @characteristics = Characteristic.all
    @characteristics_prev = {}

    @characteristics.each do |characteristic|

      @characteristics_prev[characteristic.id] = ''

    end

    @user = User.new
    @users = {}

  end

  def matricular2

   #@company = session[:company]

    @characteristics = Characteristic.all
    @characteristics_prev = {}
    queries = []

    n_q = 0

    @characteristics.each do |characteristic|

      params[:characteristic][characteristic.id.to_s.to_sym].slice! 0

      if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

        @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]


        queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND ( '

        @characteristics_prev[characteristic.id].each_with_index do |valor,index|

          if index > 0

            queries[n_q] += ' OR '

          end

          queries[n_q] += 'valor = "'+valor+'" '

        end

        queries[n_q] += ' ) '

        n_q += 1

      end

    end

    if !params[:user][:codigo].blank? || !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

      if queries.length > 0

        queries.each_with_index do |query,index|

          if index == 0

            @users = User.joins(:user_characteristics).where(
                'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query,
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

          else

            lista_users_ids = @users.map {|u| u.id }

            @users = User.joins(:user_characteristics).where(
                'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query+' AND users.id IN (?) ',
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

      end

    else

      @users = {}

    end

    @user = User.new(params[:user])

    render 'matricular1'

  end

  def matricular3

    @users_cant_enroll = Array.new
    @program_courses_cant_enroll = Array.new
    @reasons_cant_enroll = Array.new

   #@company = session[:company]

    @characteristics = Characteristic.all
    @characteristics_prev = {}

    @characteristics.each do |characteristic|

      if params[:characteristic][characteristic.id.to_s.to_sym]

        @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]

      else

        @characteristics_prev[characteristic.id] = ''

      end

    end

    @users = {}
    @user = User.new(params[:user])

    params[:program][:id].slice! 0
    params[:program_course][:id].slice! 0

    params[:user_id].each do |user_id,value|

      if value != '0'

        user = User.find(user_id)

        params[:program][:id].each do |program_id|

            program = Program.find(program_id)
            level = program.levels.first

            enrollment = Enrollment.where('program_id = ? AND user_id = ?',program.id, user.id).first

            if enrollment

              enrollment.active = true

            else

              enrollment = Enrollment.new(program: program, user: user, level: level)

            end

            enrollment.save

        end

        forzar_matricula = false

        forzar_matricula = true if params[:forzar_matricula] && params[:forzar_matricula][:aplica].to_s == '1'

        params[:program_course][:id].each do |program_and_course_id|


          if program_and_course_id && program_and_course_id != ''

            datos = program_and_course_id.split('_')

            program = Program.find(datos[0])
            level = program.levels.first

            enrollment = Enrollment.where('program_id = ? AND user_id = ?',program.id, user.id).first

            unless enrollment

              enrollment = Enrollment.new(program: program, user: user, level: level)
              enrollment.save

            end

            program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program.id, datos[1]).first

            user_course_prev = enrollment.user_courses.where('program_course_id = ? AND anulado = ? ', program_course.id, false).order('id DESC').first

            estado_prev = nil
            estado_prev = user_course_prev.estado(lms_time) if user_course_prev

            matricular = false

            if forzar_matricula

              fd = params[:forzar_date].split('/').map { |s| s.to_i }

              fd = DateTime.new(fd[2],fd[1],fd[0],0,0,0)

              if estado_prev

                if estado_prev == :en_progreso || estado_prev == :no_iniciado
                  @reasons_cant_enroll.push 'Curso en progreso o no iniciado'
                else
                  if estado_prev == :aprobado && user_course_prev.fin >= fd
                    @reasons_cant_enroll.push 'Curso no aprobado antes de '+fd.strftime('%d/%m/%Y')
                  else
                    matricular = true
                  end

                end


              else

                matricular = true

              end

              matricular = true unless estado_prev && ( () || (estado_prev == :aprobado && user_course_prev.fin >= fd ) )

            else

              if estado_prev

                if estado_prev == :aprobado || estado_prev == :en_progreso || estado_prev == :no_iniciado
                  @reasons_cant_enroll.push 'Curso aprobado, en progreso o no iniciado'
                else
                  matricular = true
                end

              else
                matricular = true
              end

            end

            if matricular

              user_course = UserCourse.new

              user_course.enrollment = enrollment
              user_course.program_course = program_course
              user_course.course = program_course.course


              desde = params[:desde].split(' ')
              desde_d = desde[0].split('/').map { |s| s.to_i }
              desde_h = desde[1].split(':').map { |s| s.to_i }

              user_course.desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],desde_h[0],desde_h[1],desde_h[2])

              hasta = params[:hasta].split(' ')
              hasta_d = hasta[0].split('/').map { |s| s.to_i }
              hasta_h = hasta[1].split(':').map { |s| s.to_i }

              user_course.hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],hasta_h[0],hasta_h[1],hasta_h[2])


              user_course_last = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND anulado = ? AND hasta = ? ', enrollment.id, program_course.id, false, user_course.hasta).order('numero_oportunidad DESC').first

              numero_oportunidad = 1

              numero_oportunidad = user_course_last.numero_oportunidad+1 if user_course_last

              user_course.numero_oportunidad = numero_oportunidad

              if params[:aplica_sence] && params[:aplica_sence][:aplica] == '1'

                if @company.modulo_sence && program_course.course.sence_code && program_course.course.sence_code != ''

                  user_course.aplica_sence = true

                end

              end

              user_course.save

            else

              @users_cant_enroll.push user
              @program_courses_cant_enroll.push program_course


            end

          end

        end

      end

    end

    flash.now[:success] = t('activerecord.success.model.enrollment.create_ok')

    matricular2

  end

  def anular_matricula_masiva1

   #@company = session[:company]

    @characteristics = Characteristic.all
    @characteristics_prev = {}

    @characteristics.each do |characteristic|

      @characteristics_prev[characteristic.id] = ''

    end

    @user = User.new
    @users = {}

  end

  def anular_matricula_masiva2

   #@company = session[:company]

    @characteristics = Characteristic.all
    @characteristics_prev = {}
    queries = []

    n_q = 0

    @characteristics.each do |characteristic|

      params[:characteristic][characteristic.id.to_s.to_sym].slice! 0

      if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

        @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]


        queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND ( '

        @characteristics_prev[characteristic.id].each_with_index do |valor,index|

          if index > 0

            queries[n_q] += ' OR '

          end

          queries[n_q] += 'valor = "'+valor+'" '

        end

        queries[n_q] += ' ) '

        n_q += 1

      end

    end

    if !params[:user][:codigo].blank? || !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

      if queries.length > 0

        queries.each_with_index do |query,index|

          if index == 0

            @users = User.joins(:user_characteristics).where(
                'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query,
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

          else

            lista_users_ids = @users.map {|u| u.id }

            @users = User.joins(:user_characteristics).where(
                'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND '+query+' AND users.id IN (?) ',
                '%'+params[:user][:codigo]+'%',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = User.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+params[:user][:codigo]+'%',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

      end

    else

      @users = {}

    end

    @user = User.new(params[:user])

    render 'anular_matricula_masiva1'

  end

  def anular_matricula_masiva3

   #@company = session[:company]

    @characteristics = Characteristic.all
    @characteristics_prev = {}

    @characteristics.each do |characteristic|

      if params[:characteristic][characteristic.id.to_s.to_sym]

        @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]

      else

        @characteristics_prev[characteristic.id] = ''

      end

    end


    @users = {}
    @user = User.new(params[:user])

    params[:program][:id].slice! 0
    params[:program_course][:id].slice! 0

    params[:user_id].each do |user_id,value|

      if value != '0'

        user = User.find(user_id)

        params[:program][:id].each do |program_id|

          enrollment = Enrollment.where('program_id = ? AND user_id = ? ', program_id, user_id).first

          if enrollment

            if enrollment.user_courses.size == 0

              enrollment.destroy

            else

              enrollment.active = false
              enrollment.save

            end

          end

        end

        params[:program_course][:id].each do |program_and_course_id|

          if program_and_course_id && program_and_course_id != ''

            datos = program_and_course_id.split('_')

            enrollment = Enrollment.where('program_id = ? AND user_id = ?',datos[0], user_id).first

            if enrollment

              program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', datos[0], datos[1]).first

              user_course = enrollment.user_courses.where('program_course_id = ? AND anulado = ? ', program_course.id, false).order('id DESC').first

              if user_course

                user_course.anulado = true
                user_course.save

              end

            end

          end

        end

      end

    end

    flash.now[:success] = t('activerecord.success.model.enrollment.cancel_ok')

    anular_matricula_masiva2

  end

  def migracion_masiva_ateneus

  end

  def migracion_masiva_ateneus_upload

    @lineas_error = Array.new
    @lineas_error_detalle = Array.new
    @lineas_error_messages = Array.new

    require 'rubyXL'

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        #check_migra_ateneus archivo_temporal

        if @lineas_error && @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.enrollment.update_file_error')
        else
          ejecuta_migra_ateneus archivo_temporal

          #carga_puestos_security archivo_temporal

          flash.now[:success] = t('activerecord.success.model.enrollment.migra_ateneus_ok')

        end

      else

        flash.now[:danger] = t('activerecord.error.model.enrollment.upload_file_no_xlsx')

      end

    else
      flash.now[:danger] = t('activerecord.error.model.enrollment.upload_file_no_file')

    end

    render 'migracion_masiva_ateneus'

  end

  def migration_training_history_format

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_usuarios')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'cod usuario'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'programa'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'curso'
        filaHeader <<  headerCenter
        cols_widths << 20

        if TrainingType.all.size > 0
          fila << 'Tipo de capacitación'
          filaHeader <<  headerCenter
          cols_widths << 20
        end

        fila << 'dedicación estimada (horas)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'fecha inicio convoctoria'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'fecha fin convocatoria'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'fecha inicio real'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'fecha fin real'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'número oportunidad'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'asistencia (0-100)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'aprobado (0/1)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'nota'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    reporte_excel = reporte

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_migración_masiva_historial.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end


  def consultar_resultados_grupo_f

    @grupo_f = params[:grupo_f]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @program_course.id, @course.id, @grupo).joins(:user).order('apellidos, nombre')

    @user_courses = @program_course.matriculas_por_grupo_f @grupo_f

  end

  def register_asist_eval

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @program_course.id, @course.id, @grupo).joins(:user).order('apellidos, nombre')

    @user_courses = @program_course.matriculas_vigentes_por_grupo @grupo

  end

  def update_asist_eval

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    if @course.asistencia_presencial

      params[:user_id].each do |user_course_id,value|

        user_course = UserCourse.find(user_course_id)
        asist = params[:user_id][user_course.id.to_s.to_sym]
        if asist == user_course.id.to_s
          user_course.iniciado = true
          user_course.finalizado = true
          user_course.asistencia = true
          user_course.porcentaje_avance = 100
          user_course.inicio = user_course.desde if user_course.inicio.nil?
          user_course.fin = user_course.hasta if user_course.fin.nil?
        else
          user_course.asistencia = false
        end

        user_course.save

      end

    end

    if @course.eval_presencial

      params[:user_course_id].each do |user_course_id,value|

        user_course = UserCourse.find(user_course_id)

        user_course.nota = value

        user_course.inicio = user_course.desde if user_course.inicio.nil?

        if user_course.save

          if user_course.course.nota_minima == 0
            user_course.aprobado = true
          elsif !user_course.nota.nil?
            user_course.aprobado = true
            user_course.aprobado = false if value.to_i < user_course.course.nota_minima
          end

          user_course.save

        end

      end

    end

    if @course.asistencia_presencial && @course.eval_presencial
      flash[:success] = t('activerecord.success.model.user_course.evaluate_assit_course_ok')
    elsif @course.asistencia_presencial
      flash[:success] = t('activerecord.success.model.user_course.assit_course_ok')
    elsif @course.eval_presencial
      flash[:success] = t('activerecord.success.model.user_course.evaluate_course_ok')
    end

    redirect_to enrollments_register_asist_eval_path(@program_course, @grupo)


  end

  def register_poll

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    #@user_course = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @program_course.id, @course.id, @grupo).first

    @user_course = @program_course.matriculas_vigentes_por_grupo(@grupo).first

  end

  def update_poll

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    if @course.poll_presencial?

      poll = @course.polls.where('tipo_satisfaccion = ?', true).first

      master_questions = poll.master_poll.master_poll_questions

      master_questions.each do |master_question|

        master_question.master_poll_alternatives.each do |alt|

          poll_summary = ProgramCoursePollSummary.where('program_course_id = ? AND poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ? AND grupo = ? ',
                                                        @program_course.id, poll.id, master_question.id, alt.id, @grupo).first

          poll_summary.destroy if poll_summary

          poll_summary = ProgramCoursePollSummary.new
          poll_summary.program_course = @program_course
          poll_summary.poll = poll
          poll_summary.master_poll_question = master_question
          poll_summary.master_poll_alternative = alt
          poll_summary.grupo = @grupo
          poll_summary.numero_respuestas = 0

          if params[:poll_summary][alt.id.to_s.to_sym]

            poll_summary.numero_respuestas = params[:poll_summary][alt.id.to_s.to_sym]

          end

          poll_summary.save

        end

      end

      flash[:success] = t('activerecord.success.model.user_course.polling_course_ok')

    end

    redirect_to enrollments_register_poll_path(@program_course, @grupo)


  end

  def define_aplica_sence

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @program_course.id, @course.id, @grupo).joins(:user).order('apellidos, nombre')

    @user_courses = @program_course.matriculas_vigentes_por_grupo @grupo

  end

  def update_aplica_sence

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    params[:user_id].each do |user_course_id,value|

      user_course = UserCourse.find(user_course_id)
      aplica = params[:user_id][user_course.id.to_s.to_sym]
      if aplica == user_course.id.to_s
        user_course.aplica_sence = true
      else
        user_course.aplica_sence = false
      end

      user_course.save

    end

    flash[:success] = t('activerecord.success.model.user_course.aplica_sence_ok')

    redirect_to enrollments_define_aplica_sence_path(@program_course, @grupo)


  end

  def define_anula_matricula

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @program_course.id, @course.id, @grupo).joins(:user).order('apellidos, nombre')

    @user_courses = @program_course.matriculas_por_grupo @grupo


  end

  def update_anula_matricula

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    params[:user_id].each do |user_course_id,value|

      user_course = UserCourse.find(user_course_id)
      anula_matricula = params[:user_id][user_course.id.to_s.to_sym]
      if anula_matricula == user_course.id.to_s
        user_course.anulado = true
      else
        user_course.anulado = false
      end

      user_course.save

    end

    flash[:success] = t('activerecord.success.model.user_course.anula_matricula_ok')

    redirect_to enrollments_define_anula_matricula_path(@program_course, @grupo)


  end

  def define_anula_matricula_grupo_f

    @grupo_f = params[:grupo_f]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @program_course.id, @course.id, @grupo).joins(:user).order('apellidos, nombre')

    @user_courses = @program_course.matriculas_por_grupo_f @grupo_f


  end

  def update_anula_matricula_grupo_f

    @grupo_f = params[:grupo_f]

    @program_course = ProgramCourse.find(params[:program_course_id])

    params[:user_id].each do |user_course_id,value|

      user_course = UserCourse.find(user_course_id)
      anula_matricula = params[:user_id][user_course.id.to_s.to_sym]
      if anula_matricula == user_course.id.to_s
        user_course.anulado = true
      else
        user_course.anulado = false
      end

      user_course.save

    end

    flash[:success] = t('activerecord.success.model.user_course.anula_matricula_ok')

    redirect_to enrollments_define_anula_matricula_grupo_f_path(@program_course, @grupo_f)


  end

  def edit_desde_hasta

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @program_course.id, @course.id, @grupo).joins(:user).order('apellidos, nombre')

    @user_course = @program_course.matriculas_por_grupo(@grupo).first


  end

  def update_desde_hasta

    @grupo = params[:grupo]

    @program_course = ProgramCourse.find(params[:program_course_id])

    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = Time.utc(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = Time.utc(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)


    user_courses = @program_course.matriculas_por_grupo_para_update @grupo

    user_courses.each do |uc|

      uc.desde = desde
      uc.hasta = hasta

      uc.set_grupo
      uc.set_grupo_f

      uc.save

      @grupo = uc.grupo

    end


    flash[:success] = t('activerecord.success.model.user_course.update_desde_hasta_ok')

    redirect_to enrollments_ver_programa_path(@program_course.program)


  end

  def edit_hasta

    @grupo_f = params[:grupo_f]

    @program_course = ProgramCourse.find(params[:program_course_id])

    @course = @program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', @program_course.id, @course.id, @grupo).joins(:user).order('apellidos, nombre')

    @user_courses = @program_course.matriculas_por_grupo_f(@grupo_f)

  end

  def update_hasta

    @grupo_f = params[:grupo_f]

    @program_course = ProgramCourse.find(params[:program_course_id])

    hasta = params[:hasta].split(' ')
    hasta_d = hasta[0].split('/').map { |s| s.to_i }
    hasta_h = hasta[1].split(':').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],hasta_h[0],hasta_h[1],hasta_h[2])

    user_courses = @program_course.matriculas_por_grupo_f_para_update @grupo_f

    user_courses.each do |uc|

      if params["uc_#{uc.id}".to_sym] && params["uc_#{uc.id}".to_sym] == uc.id.to_s

        hasta = params[:hasta].split(' ')
        hasta_d = hasta[0].split('/').map { |s| s.to_i }
        hasta_h = hasta[1].split(':').map { |s| s.to_i }

        uc.hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],hasta_h[0],hasta_h[1],hasta_h[2])

        uc.set_grupo
        uc.set_grupo_f

        uc.save

      end

    end


    flash[:success] = t('activerecord.success.model.user_course.update_hasta_ok')

    redirect_to enrollments_ver_programa_path(@program_course.program)


  end

  def empty_students_list_xlsx

    grupo = params[:grupo]

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', program_course.id, course.id, grupo).joins(:user).order('apellidos, nombre')

    @user_courses = program_course.matriculas_vigentes_por_grupo grupo

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}



      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}





      wb.add_worksheet(:name => 'Lista') do |reporte_excel|

        characteristics = Characteristic.where('basic_report = ?', true).reorder('orden_basic_report')

        fila_actual = 1

        fila = Array.new
        fila << 'Curso'
        fila << ''
        fila << course.nombre
        fila << ''

        reporte_excel.add_row fila, :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':D'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Fecha'
        fila << ''
        fila << (localize  @user_courses.first.desde, format: :day_month_year)+' - '+(localize  @user_courses.first.hasta, format: :day_month_year)
        fila << ''

        reporte_excel.add_row fila, :style => headerTitleInfo, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':D'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        characteristics.each do |c|
          fila << c.nombre
          filaHeader <<  headerLeft
          cols_widths << 22
        end

        fila << ''
        filaHeader <<  headerLeft
        cols_widths << 20


        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          characteristics.each do |c|

            u_cha = uc.enrollment.user.characteristic_by_id(c.id)
            if u_cha
              fila << u_cha.valor if uc
            else
              fila << ''
            end

            fileStyle << fieldLeft

          end

          fila << ''
          fileStyle << fieldLeft

          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Lista - '+course.nombre+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def acta_students_xlsx

    grupo = params[:grupo]

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    #@user_courses = UserCourse.where('program_course_id = ? AND course_id = ? AND grupo = ? ', program_course.id, course.id, grupo).joins(:user).order('apellidos, nombre')

    @user_courses = program_course.matriculas_vigentes_por_grupo grupo

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}



      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 11,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Acta') do |reporte_excel|

        characteristics = Characteristic.where('basic_report = ?', true).reorder('orden_basic_report')

        fila_actual = 1

        fila = Array.new
        fila << 'Curso'
        fila << ''
        fila << course.nombre
        fila << ''
        fila << ''

        reporte_excel.add_row fila, :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':F'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Fecha'
        fila << ''
        fila << (localize  @user_courses.first.desde, format: :day_month_year)+' - '+(localize  @user_courses.first.hasta, format: :day_month_year)
        fila << 'Nota Min.'
        fila << course.nota_minima
        fila << ''

        reporte_excel.add_row fila, :style => headerTitleInfo, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('E'+fila_actual.to_s+':F'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        characteristics.each do |c|
          fila << c.nombre
          filaHeader <<  headerLeft
          cols_widths << 22
        end

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Nota'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10


        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          characteristics.each do |c|

            u_cha = uc.enrollment.user.characteristic_by_id(c.id)
            if u_cha
              fila << u_cha.valor if uc
            else
              fila << ''
            end

            fileStyle << fieldLeft

          end

          if uc.asistencia
            fila << 'Sí' if uc.asistencia
          else
            fila << 'NO'
          end

          fileStyle << fieldLeft

          fila << uc.nota

          if uc.aprobado
            fileStyle << fieldLeftAprob
            fila << 'Aprobado'
            fileStyle << fieldLeftAprob

          else



            fileStyle << fieldLeftReprob
            if uc.nota
              fila << 'Reprobado'
            else
              fila << ''
            end


            fileStyle << fieldLeftReprob


          end


          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Acta - '+course.nombre+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def reporte_curso_excel_grupo

   company = @company

    grupo = params[:grupo]

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    user_courses = program_course.matriculas_vigentes_por_grupo grupo

    reporte_excel = reporte_curso_excel('Reporte', course, user_courses, company, nil, nil, nil, true)

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte - '+course.nombre+'-'+(localize  user_courses.first.desde, format: :day_snmonth)+'-'+(localize  user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def reporte_curso_excel_grupo_f

   company = @company

    grupo_f = params[:grupo_f]

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    user_courses = program_course.matriculas_vigentes_por_grupo_f grupo_f

    reporte_excel = reporte_curso_excel('Reporte', course, user_courses, company, nil, nil, nil, true)

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte - '+course.nombre+'-'+(localize  user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def reporte_curso_excel_rango

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])

    course = program_course.course

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    if rango_fechas
      user_courses = program_course.matriculas_vigentes_desde_hasta(desde, hasta)
    else
      user_courses = program_course.matriculas_vigentes
    end

    if rango_fechas
      reporte_excel = reporte_curso_excel('Reporte', course, user_courses, company, true, desde_t, hasta_t, true)
    else
      reporte_excel = reporte_curso_excel('Reporte', course, user_courses, company)
    end

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte - '+course.nombre+'-'+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def reporte_cursos_excel_rango

   company = @company

    programs = Program.all

    query = ''
    n_query = 0

    programs.each do |program|

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

    end

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    if query != ''
      if rango_fechas
        user_courses = case params[:tipo_fecha]
                         when '1'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       end

      else
        user_courses = UserCourse.where('('+query+') AND anulado = ?', false).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')
      end
    else
      user_courses = Array.new
    end

    if rango_fechas
      reporte_excel = reporte_curso_excel('Reporte', nil, user_courses, company, rango_fechas, desde_t, hasta_t, true)
    else
      reporte_excel = reporte_curso_excel('Reporte', nil, user_courses, company, nil, nil, nil, true)
    end

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte General -'+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def reporte_cursos_excel_rango_solo_finalizados

   company = @company

    programs = Program.all

    query = ''
    n_query = 0

    programs.each do |program|

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

    end

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    if query != ''
      if rango_fechas
        #user_courses_ids = UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')

        user_courses_ids = case params[:tipo_fecha]
                         when '1'
                           UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       end

      else
        user_courses_ids = UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ?').joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
      end

      user_courses = Array.new

      user_courses_ids.each do |uc_id|
        uc_tmp = UserCourse.find(uc_id.id)

        estado_tmp = uc_tmp.estado lms_time

        if estado_tmp == :aprobado || estado_tmp == :reprobado
          user_courses << uc_tmp
        else
          user_courses_tmp = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', uc_tmp.enrollment_id, uc_tmp.program_course_id, false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).reorder('id DESC')


          estado_reprobado = false
          user_courses_tmp.each do |uc_tmp_r|

            if uc_tmp_r.iniciado && uc_tmp_r.finalizado && !uc_tmp_r.aprobado

              estado_reprobado = true
              user_courses << uc_tmp_r
              break

            end

          end

          unless estado_reprobado
            user_courses << uc_tmp
          end


        end


      end

    else
      user_courses = Array.new
    end

    if rango_fechas
      reporte_excel = reporte_curso_excel('Reporte', nil, user_courses, company, rango_fechas, desde_t, hasta_t, true)
    else
      reporte_excel = reporte_curso_excel('Reporte', nil, user_courses, company, nil, nil, nil, true)
    end

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte de Último Estado -'+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def reporte_detallado_por_curso_excel

   company = @company

    programs = Program.all

    if params[:program_course_id]

      course = ProgramCourse.find(params[:program_course_id]).course

      query = 'program_course_id = '+params[:program_course_id]

      rango_fechas = false

      if params[:desdehasta]

        rango_fechas = true

        desdehasta = params[:desdehasta].split('-')

        desde_t = desdehasta[0].strip
        desde_d = desde_t.split('/').map { |s| s.to_i }

        desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

        hasta_t = desdehasta[1].strip
        hasta_d = hasta_t.split('/').map { |s| s.to_i }

        hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

      end

      if query != ''
        if rango_fechas
          user_courses = case params[:tipo_fecha]
                           when '1'
                             UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           when '2'
                             UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           when '3'
                             UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           when '4'
                             UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           when '5'
                             UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           when '6'
                             UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           when '7'
                             UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         end

        else
          user_courses = UserCourse.where('('+query+') AND anulado = ?', false).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')
        end
      else
        user_courses = Array.new
      end

      if rango_fechas
        reporte_excel = reporte_detallado_curso_excel('Reporte', course, user_courses, company, rango_fechas, desde_t, hasta_t, true)
      else
        reporte_excel = reporte_detallado_curso_excel('Reporte', course, user_courses, company, nil, nil, nil, true)
      end

      directorio_temporal = '/tmp/'
      nombre_temporal = SecureRandom.hex(5)+'.xlsx'
      archivo_temporal = directorio_temporal+nombre_temporal

      reporte_excel.serialize archivo_temporal

      send_file archivo_temporal,
                filename: 'Reporte Detallado por Curso - '+course.nombre+' - '+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
                type: 'application/octet-stream',
                disposition: 'attachment'

    end

  end

  def reporte_por_programa_xlsx

   company = @company

    program = Program.find(params['program_id'])

    query = ''
    n_query = 0

    program_courses = Array.new

    program.levels.each do |level|

      level.courses.each_with_index do |course|

        program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

        if params["pc_#{program_course.id}".to_sym] == '1'

          program_courses.push program_course

          if n_query == 0
            query += ' program_course_id = '+program_course.id.to_s+' '
            n_query += 1
          else
            query += ' OR program_course_id = '+program_course.id.to_s+' '

          end

        end

      end

    end

    rango_fechas = false

    if params[:desdehasta_por_programa]

      rango_fechas = true

      desdehasta = params[:desdehasta_por_programa].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    enrollments = Enrollment.where('program_id = ?', program.id).joins(:user).reorder('apellidos, nombre')

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Reporte Avance') do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Programa'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program.nombre
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        if rango_fechas

          fila = Array.new
          filaHeader = Array.new

          fila << 'Vigencia'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo


          fila << 'Del '+desde_t+' Al '+hasta_t
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila


        fila_actual += 1



        fila = Array.new
        filaHeader = Array.new


        fila << 'DATOS PERSONALES'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        program_courses.each do |program_course|

          fila << program_course.course.nombre
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter

          fila << 'DATOS AVANCE EN CURSO: '+program_course.course.nombre
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter

        end

        characteristics = Characteristic.find_all_by_publica(true)

        characteristics_porcentaje_sence = Characteristic.find_by_porcentaje_participante_sence(true)

        characteristics_priv = Characteristic.find_all_by_publica(false)

        if characteristics.length > 0

          fila << 'DATOS DEL ALUMNO'

          (1..characteristics.length).each do |n|

            filaHeader <<  headerCenter
            fila << ''

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':E'+fila_actual.to_s)

        pos_letras = 5

        program_courses.each do |program_course|

          reporte_excel.merge_cells(letras[(pos_letras)]+fila_actual.to_s+':'+letras[(pos_letras)+8]+fila_actual.to_s)
          #reporte_excel.merge_cells(letras[(pos_letras)+4]+fila_actual.to_s+':'+letras[(pos_letras)+10]+fila_actual.to_s)

          pos_letras += 9

        end

        if characteristics.length > 0

          reporte_excel.merge_cells(letras[(pos_letras)]+fila_actual.to_s+':'+letras[pos_letras+characteristics.length-1]+fila_actual.to_s)

        end

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Correo Electrónico'
        filaHeader <<  headerLeft
        cols_widths << 22

        program_courses.each do |program_course|

          fila << '#'
          filaHeader <<  headerLeft
          cols_widths << 6

          fila << 'Fecha Inicio'
          filaHeader <<  headerLeft
          cols_widths << 10

          fila << 'Fecha Fin'
          filaHeader <<  headerLeft
          cols_widths << 10

          fila << 'Estado'
          filaHeader <<  headerLeft
          cols_widths << 10

          fila << 'Nota Final'
          filaHeader <<  headerLeft
          cols_widths << 10

          fila << 'Resultado Final'
          filaHeader <<  headerLeft
          cols_widths << 15

          fila << 'Asistencia'
          filaHeader <<  headerLeft
          cols_widths << 10

          fila << 'Inicio Real'
          filaHeader <<  headerLeft
          cols_widths << 10

          fila << 'Fin Real'
          filaHeader <<  headerLeft
          cols_widths << 10

        end

        characteristics.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end

        characteristics_priv.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila_actual += 1

        enrollments.each_with_index do |enrollment, index|


          if enrollment.user_courses.size == 0
            fila = Array.new
            fileStyle = Array.new

            fila << index+1
            fileStyle << fieldLeft

            fila << enrollment.user.codigo
            fileStyle << fieldLeft

            fila << enrollment.user.apellidos
            fileStyle << fieldLeft

            fila << enrollment.user.nombre
            fileStyle << fieldLeft

            fila << enrollment.user.email
            fileStyle << fieldLeft

            program_courses.each do |program_course|

              9.times do

                fila << ''
                fileStyle << fieldLeft

              end

            end

            characteristics.each do |cha|

              user_cha = enrollment.user.characteristic_by_id(cha.id)

              val_cha = '-'
              if user_cha
                val_cha = user_cha.valor
              end

              fila << val_cha
              fileStyle << fieldLeft

            end

            characteristics_priv.each do |cha|

              user_cha = enrollment.user.characteristic_by_id(cha.id)

              val_cha = '-'
              if user_cha
                val_cha = user_cha.valor
              end

              fila << val_cha
              fileStyle << fieldLeft

            end

            reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

            fila_actual += 1

          else

            existe_oportunidad = true
            numero_oportunidad = 0

            while existe_oportunidad

              numero_oportunidad += 1

              if rango_fechas
                existen = UserCourse.where('('+query+') AND enrollment_id = ? AND numero_oportunidad = ? AND anulado = ? AND ((desde >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', enrollment.id, numero_oportunidad, false, desde, hasta, desde, hasta, desde, hasta).size
              else
                existen = UserCourse.where('('+query+') AND enrollment_id = ? AND numero_oportunidad = ? AND anulado = ? ', enrollment.id, numero_oportunidad, false).size
              end

              if existen > 0

                fila = Array.new
                fileStyle = Array.new

                fila << index+1
                fileStyle << fieldLeft

                fila << enrollment.user.codigo
                fileStyle << fieldLeft

                fila << enrollment.user.apellidos
                fileStyle << fieldLeft

                fila << enrollment.user.nombre
                fileStyle << fieldLeft

                fila << enrollment.user.email
                fileStyle << fieldLeft

                program_courses.each do |program_course|

                  if rango_fechas
                    uc = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND numero_oportunidad = ? AND anulado = ? AND ((desde >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', enrollment.id, program_course.id, numero_oportunidad, false, desde, hasta, desde, hasta, desde, hasta).first

                  else
                    uc = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND numero_oportunidad = ? AND anulado = ? ', enrollment.id, program_course.id, numero_oportunidad, false).first
                  end

                  if uc

                    fila << uc.numero_oportunidad
                    fileStyle << fieldLeft

                    if uc.desde
                      fila << (localize  uc.desde, format: :day_month_year)
                    else
                      fila << '-'
                    end
                    fileStyle << fieldLeft

                    if uc.hasta
                      fila << (localize  uc.hasta, format: :day_month_year)
                    else
                      fila << '-'
                    end
                    fileStyle << fieldLeft

                    if !uc.iniciado
                      fila << 'No iniciado'
                    else

                      if uc.finalizado
                        fila << 'Terminado'
                      else
                        fila << 'Iniciado'
                      end

                    end

                    fileStyle << fieldLeft

                    if uc.nota
                      fila << uc.nota
                    else
                      fila << '-'
                    end

                    fileStyle << fieldLeft

                    if uc.aprobado
                      fila << 'aprobado'
                    else
                      if uc.finalizado

                        fila << 'reprobado'

                      else

                        fila << '-'

                      end
                    end
                    fileStyle << fieldLeft

                    fila << uc.porcentaje_avance.to_i.to_s+'%'
                    fileStyle << fieldLeft

                    if uc.inicio
                      fila << (localize  uc.inicio, format: :day_month_year)
                    else
                      fila << '-'
                    end
                    fileStyle << fieldLeft

                    if uc.fin
                      fila << (localize  uc.fin, format: :day_month_year)
                    else
                      fila << '-'
                    end
                    fileStyle << fieldLeft

                  else

                    9.times do

                      fila << ''
                      fileStyle << fieldLeft

                    end

                  end

                end

                characteristics.each do |cha|

                  user_cha = enrollment.user.characteristic_by_id(cha.id)

                  val_cha = '-'
                  if user_cha
                    val_cha = user_cha.valor
                  end

                  fila << val_cha
                  fileStyle << fieldLeft

                end

                characteristics_priv.each do |cha|

                  user_cha = enrollment.user.characteristic_by_id(cha.id)

                  val_cha = '-'
                  if user_cha
                    val_cha = user_cha.valor
                  end

                  fila << val_cha
                  fileStyle << fieldLeft

                end

                reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

                fila_actual += 1

              else

                existe_oportunidad = false

              end



            end

          end



        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end


      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte Por Programa -'+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def reporte_encuesta_satisfaccion_xlsx

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    query = ' program_course_id = '+program_course.id.to_s+' '

    if query != ''
      if rango_fechas


        user_courses = case params[:tipo_fecha]
                         when '1'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       end



      else
        user_courses = UserCourse.where('('+query+') AND anulado = ?', false).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')
      end
    else
      user_courses = Array.new
    end



    if rango_fechas
      reporte_excel = reporte_encuesta_satisfaccion_excel('Reporte', program_course, user_courses, company, rango_fechas, desde, hasta, desde_t, hasta_t)
    else
      reporte_excel = reporte_encuesta_satisfaccion_excel('Reporte', program_course, user_courses, company, nil, nil, nil, nil, nil)
    end

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Encuesta de Satisfacción - '+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def reporte_encuesta_satisfaccion_con_nombres_xlsx

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    query = ' program_course_id = '+program_course.id.to_s+' '

    if query != ''
      if rango_fechas
        user_courses = case params[:tipo_fecha]
                         when '1'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       end
      else
        user_courses = UserCourse.where('('+query+') AND anulado = ?', false).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')
      end
    else
      user_courses = Array.new
    end

    if rango_fechas
      reporte_excel = reporte_encuesta_satisfaccion_con_nombres_excel('Reporte', program_course, user_courses, company, rango_fechas, desde_t, hasta_t)
    else
      reporte_excel = reporte_encuesta_satisfaccion_con_nombres_excel('Reporte', program_course, user_courses, company, nil, nil, nil)
    end

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Encuesta de Satisfacción - '+(localize  desde, format: :day_snmonth)+'-'+(localize  hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def certificados_curso

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])

    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_cursos/'

    rango_fechas = false

    if params[:desdehasta_cert]

      rango_fechas = true

      desdehasta = params[:desdehasta_cert].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    end

    if rango_fechas

      require 'prawn/measurement_extensions'

      directorio_temporal = '/tmp/'
      nombre_temporal = SecureRandom.hex(5)+'.pdf'
      archivo_temporal = directorio_temporal+nombre_temporal

      user_connected_time_zone_pdf = user_connected_time_zone

      Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: [720,540]) do |pdf|

        pdf.font_families.update('Foco Light' => {:normal => '/var/www/remote-storage/fonts/Foco_Std_Lt_0.ttf'})
        pdf.font_families.update('HaasGrotDisp Light' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-45Light_1.ttf'})
        pdf.font_families.update('HaasGrotDisp Normal' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-65Medium_1.ttf'})
        pdf.font_families.update('HaasGrotDisp Bold' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-75Bold_1.ttf'})
        #pdf.font_families.update('Foco Light' => {:normal => '/Users/alejo/Documents/Current/RocketLab/Seeds/LMS/diseños/pacífico/certificados/Foco_Std_Lt_0.ttf'})


        program_course.matriculas_aprobadas_rango_fechas(desde, hasta).each do |user_course|

          cc = user_course.course.course_certificates.where('rango_desde <= ? AND rango_hasta >= ?',user_course.fin, user_course.fin).first

          if cc && cc.template

            plantilla = directory+(cc.template)+'.jpg'

            pdf.start_new_page
            pdf.image plantilla, at: [-36,504], width: 720

            nombre =  user_course.user.nombre+' '+user_course.user.apellidos

            nombre_curso =  user_course.course.nombre

            nota = ''
            nota = format_nota(user_course.nota).to_s if user_course.nota
            if user_course.nota
              if user_course.nota.to_i == user_course.nota
                nota = user_course.nota.to_i
              else
                nota = user_course.nota
              end

            end

            nota = nota.to_s


            desde = ''
            desde = I18n.localize(user_course.desde, format: cc.desde_formato.to_sym) if user_course.desde
            hasta = ''
            hasta = I18n.localize(user_course.hasta, format: cc.hasta_formato.to_sym) if user_course.hasta

            inicio = ''
            inicio = I18n.localize(user_course.inicio, format: cc.inicio_formato.to_sym) if user_course.inicio
            fin = ''
            fin = I18n.localize(user_course.fin, format: cc.fin_formato.to_sym) if user_course.fin

            fecha = user_course.fin
            fecha = user_course.hasta if user_course.hasta && cc.fecha_source == 'fin_convocatoria'

            fecha = fecha+cc.fecha_source_delay.days
            fecha = I18n.localize(fecha, format: :full_date)



            if cc.nombre
              if cc.nombre_x == -1
                pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :style => :normal, :size => cc.nombre_size} ], :at => [0, cc.nombre_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :style => :normal, :size => cc.nombre_size} ], :at => [cc.nombre_x.mm, cc.nombre_y.mm]
              end
            end

            if cc.nombre_curso

              if cc.nombre_curso_x == -1
                pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :style => :normal, :size => cc.nombre_curso_size} ], :at => [0, cc.nombre_curso_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :style => :normal, :size => cc.nombre_curso_size} ], :at => [cc.nombre_curso_x.mm, cc.nombre_curso_y.mm]
              end
            end

            if cc.fecha
              if cc.fecha_x == -1
                pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [0, cc.fecha_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :style => :normal, :size => cc.fecha_size} ], :at => [cc.fecha_x.mm, cc.fecha_y.mm]
              end
            end

            if cc.desde
              if cc.desde_x == -1
                pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :style => :normal, :size => cc.desde_size} ], :at => [0, cc.desde_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :style => :normal, :size => cc.desde_size} ], :at => [cc.desde_x.mm, cc.desde_y.mm]
              end
            end

            if cc.hasta
              if cc.hasta_x == -1
                pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :style => :normal, :size => cc.hasta_size} ], :at => [0, cc.hasta_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :style => :normal, :size => cc.hasta_size} ], :at => [cc.hasta_x.mm, cc.hasta_y.mm]
              end
            end

            if cc.inicio
              if cc.inicio_x == -1
                pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :style => :normal, :size => cc.inicio_size} ], :at => [0, cc.inicio_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :style => :normal, :size => cc.inicio_size} ], :at => [cc.inicio_x.mm, cc.inicio_y.mm]
              end
            end

            if cc.fin
              if cc.fin_x == -1
                pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :style => :normal, :size => cc.fin_size} ], :at => [0, cc.fin_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :style => :normal, :size => cc.fin_size} ], :at => [cc.fin_x.mm, cc.fin_y.mm]
              end
            end

            if cc.nota
              if cc.nota_x == -1
                pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :style => :normal, :size => cc.nota_size} ], :at => [0, cc.nota_y.mm],  :align => :center
              else
                pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :style => :normal, :size => cc.nota_size} ], :at => [cc.nota_x.mm, cc.nota_y.mm]
              end
            end

          end

        end

      end

      send_file archivo_temporal,
                filename: 'Certificados - '+program_course.course.nombre+'.pdf',
                type: 'application/octet-stream',
                disposition: 'attachment'


    end

  end

  private

  def ejecuta_migra_ateneus_standar(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    archivo.worksheets[0].extract_data.each_with_index do |fila, index|

      if index > 0

        #0 codigo_usuario
        #1 programa_curso_id
        #2 fecha_inicio_convoctoria
        #3 fecha_fin_convocatoria
        #4 fecha_inicio_real
        #5 fecha_fin_real
        #6 estado
        #7 asistencia
        #8 aprobado
        #9 nota

        user = User.where('codigo = ?',fila[0]).first
        program_course = ProgramCourse.where('id = ?',fila[1]).first

        program = program_course.program
        level = program.levels.first

        enrollment = Enrollment.where('program_id = ? && user_id = ? && level_id = ?', program.id, user.id, level.id).first

        unless enrollment

          enrollment = Enrollment.new(program: program, user: user, level: level)
          enrollment.save

        end

        fechas = Array.new

        (2..5).each do |col|

          if fila[col] == '-'
            fechas.push nil
          else
            fechas.push col.even? ? fila[col] : fila[col]+ 23.hours + 59.minutes + 59.seconds
          end

        end

        iniciado = false
        finalizado = false
        aprobado = false
        anulado = false

        numero_oportunidad = fila[6].to_i

        case fila[7]
          when 'No Iniciado'
          when 'Iniciado'
            iniciado = true
          when 'Terminado'
            iniciado = true
            finalizado = true
            aprobado = true if fila[9] == 'Aprobado'
          when 'Anulado'
            anulado = true
        end

        porcentaje_avance = fila[8]
        asistencia = porcentaje_avance == 100 ? true : false

        if fila[10] == '-'
          nota = nil
        else
          nota = fila[10].to_f
        end

        user_course = UserCourse.new(enrollment: enrollment,program_course: program_course,
                                     desde: fechas[0], hasta: fechas[1], inicio: fechas[2], fin: fechas[3],
                                     limite_tiempo: false, iniciado: iniciado, finalizado: finalizado, aprobado: aprobado,
                                     anulado: anulado, course: program_course.course, asistencia: asistencia,
                                     porcentaje_avance: porcentaje_avance, nota: nota, numero_oportunidad: numero_oportunidad, dncp_id: nil,
                                     aplica_sence: false)

        if user_course.save

          unidad_numero = 0

          unidad_numero = fila[11].to_i if fila[11]

          if unidad_numero > 0

            program_course.course.units.each do |unit|
              if unit.numero <= unidad_numero
                UserCourseUnit.new(user_course: user_course, unit: unit, inicio: lms_time, fin: lms_time, finalizada: true, ultimo_acceso: lms_time).save
              end
            end

            if user_course.finalizado

              program_course.course.evaluations.each do |evaluation|
                if evaluation.numero <= unidad_numero
                  UserCourseEvaluation.new(user_course: user_course, evaluation: evaluation, inicio: lms_time, fin: lms_time, finalizada: true, ultimo_acceso: lms_time).save
                end
              end

              program_course.course.polls.each do |poll|
                if poll.numero <= unidad_numero
                  UserCoursePoll.new(user_course: user_course, poll: poll, inicio: lms_time, fin: lms_time, finalizada: true, ultimo_acceso: lms_time).save
                end
              end

            end

          end

        end

      end

    end


  end

  def carga_puestos_security(archivo_temporal)

    characteristic = Characteristic.where('data_type_id = ?',13).first

    characteristic_2 = Characteristic.find 5

    j_job_level = JJobLevel.find 1

    archivo = RubyXL::Parser.parse archivo_temporal

    archivo.worksheets[0].extract_data.each_with_index do |fila, index|

      if index > 0

        j_job = JJob.find_by_name fila[1]

        j_cost_center = JCostCenter.find_by_cc_id fila[2]

        j_pay_band = JPayBand.find_by_name fila[3]

        if j_cost_center && j_pay_band

          unless j_job

            j_job = JJob.new
            j_job.name = fila[1]
            j_job.j_job_level = j_job_level
            j_job.j_pay_band = j_pay_band
            j_job.j_cost_center = j_cost_center
            j_job.total_positions = 5
            j_job.version = 1
            j_job.current = true
            j_job.from_date = lms_time
            j_job.save

            j_job_characteristic = j_job.j_job_characteristics.build

            j_job_characteristic.characteristic = characteristic_2
            j_job_characteristic.characteristic_value_id = 143
            j_job_characteristic.save

          end

          user = User.find_by_codigo fila[0]

          if user

            user.j_job = j_job
            user.save

            user.update_user_characteristic(characteristic, j_job.name, lms_time, nil, nil, admin_connected, admin_connected, @company)

          end



        end


      end

    end

  end

  def ejecuta_migra_ateneus(archivo_temporal)

    #modificado para Danper

    archivo = RubyXL::Parser.parse archivo_temporal

    archivo.worksheets[0].extract_data.each_with_index do |fila, index|

      if index > 0

        #0 codigo_usuario
        #1 apellidos
        #2 nombres
        #3 programa
        #4 curso
        #5 tipo de capacitación
        #6 dedicación estimada (horas)
        #7 fecha_inicio_convoctoria
        #8 fecha_fin_convocatoria
        #9 fecha_inicio_real
        #10 fecha_fin_real
        #11 número de oportunidad
        #12 estado
        #13 asistencia
        #14 aprobado
        #15 nota

        user = User.where('codigo = ?',fila[0]).first

        #unless user
        #  user = User.new
        #  user.codigo = fila[0]
        #  user.apellidos = fila[1]
        #  user.nombre = fila[2]
        #  user.password = fila[0]
        #  user.password_confirmation = fila[0]
        #end

        if user#.save

          program = Program.where('nombre = ?', fila[3]).first
          unless program

            program = Program.new
            program.nombre = fila[3]
            program.color = 'FA0006';
            program.codigo = SecureRandom.hex(5)

          end

          if program.save

            training_type = nil

            unless fila[5].blank?

              training_type = TrainingType.where('nombre = ?', fila[5]).first

              unless training_type

                training_type = TrainingType.new
                training_type.nombre = fila[5]
                training_type.color = 'FA0006';
                training_type.save

              end

            end

            course = Course.where('nombre = ?', fila[4]).first

            unless course

              course = Course.new
              course.nombre = fila[4]
              course.descripcion = fila[4]
              course.objetivos = ''
              course.codigo = SecureRandom.hex(5)
              course.numero_oportunidades = 1
              course.duracion = 0
              course.dedicacion_estimada = fila[6]
              course.nota_minima = 0
              course.minimum_assistance = 0
              course.training_type = training_type

            end

            if course.save

              #para_gasco_end

              #ctc = course.course_training_characteristics.where('training_characteristic_id = ?', 1).first

              #unless ctc
              #  ctc = course.course_training_characteristics.build
              #  ctc.training_characteristic_id = 1
              #  ctc.training_characteristic_value = TrainingCharacteristicValue.where('value_string = ?', fila[11]).first
              #  ctc.save unless ctc.training_characteristic_value.nil?
              #end

              #para_gasco_end

              level = program.levels.first

              unless level
                level = program.levels.build
                level.nombre = 'unico'
                level.orden = 1
              end

              if level.save

                program_course = ProgramCourse.where('program_id = ? AND course_id = ? AND level_id = ?',program.id, course.id, level.id).first

                unless program_course

                  program_course = program.program_courses.build
                  program_course.level = level
                  program_course.course = course

                end

                if program_course.save

                  enrollment = Enrollment.where('program_id = ? && user_id = ? && level_id = ?', program.id, user.id, level.id).first

                  unless enrollment

                    enrollment = Enrollment.new(program: program, user: user, level: level)
                    enrollment.save

                  end

                  fechas = Array.new

                  fechas[0] = fila[7].to_date + 0.hours + 0.minutes + 0.seconds
                  fechas[1] = fila[8].to_date + 23.hours + 59.minutes + 59.seconds
                  fechas[2] = fila[9].to_date + 0.hours + 0.minutes + 0.seconds
                  fechas[3] = fila[10].to_date + 23.hours + 59.minutes + 59.seconds
=begin
                  if fila[12] == 'No iniciado'

                    iniciado = false
                    finalizado = false

                  elsif fila[12] == 'Iniciado'

                    iniciado = true
                    finalizado = false

                  elsif fila[12] == 'Terminado'

                    iniciado = true
                    finalizado = true

                  end
=end
                  iniciado = true
                  finalizado = true
                  #aprobado = fila[14] == 1 ? true : false
                  aprobado = true
                  anulado = false

                  #numero_oportunidad = fila[11]
                  numero_oportunidad = 1

                  #porcentaje_avance = fila[13]
                  porcentaje_avance = 100#fila[12]
                  #asistencia = fila[13] == 100 ? true : false
                  asistencia = true

                  nota = nil#fila[13].blank? ? nil : fila[13]
                  #nota = nil

                  user_course = UserCourse.new(enrollment: enrollment, program_course: program_course,
                                               desde: fechas[0], hasta: fechas[1], inicio: fechas[2], fin: fechas[3],
                                               limite_tiempo: false, iniciado: iniciado, finalizado: finalizado, aprobado: aprobado,
                                               anulado: anulado, course: program_course.course, asistencia: asistencia,
                                               porcentaje_avance: porcentaje_avance, nota: nota, numero_oportunidad: numero_oportunidad, dncp_id: nil,
                                               aplica_sence: false)

                  if user_course.save



                  else


                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('codigo: '+fila[0]+', programa: '+fila[1]+', curso: '+fila[2])
                    @lineas_error_messages.push ['No se pudo guardar el historial: '+user_course.errors.full_messages.to_s]

                  end

                end

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('curso: '+fila[0].to_s+'-'+fila[4].to_s)
              @lineas_error_messages.push ['No se pudo guardar el curso: '+course.errors.full_messages.to_s]

            end

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('programa: '+fila[3].to_s)
            @lineas_error_messages.push ['No se pudo guardar el programa: '+program.errors.full_messages.to_s]

          end

        else
          @lineas_error.push index+1
          @lineas_error_detalle.push ('codigo: '+fila[0].to_s)
          @lineas_error_messages.push ['No se pudo guardar el usuario: '+user.errors.full_messages.to_s]
        end

      end

    end


  end

  def check_migra_ateneus(archivo_temporal)

    @lineas_error = []
    @lineas_error_messages = []
    num_linea = 1

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        #0 codigo_usuario
        #1 programa_curso_id
        #2 fecha_inicio_convoctoria
        #3 fecha_fin_convocatoria
        #4 fecha_inicio_real
        #5 fecha_fin_real
        #6 estado: No Iniciado | Iniciado | Terminado | Anulado
        #7 asistencia
        #8 aprobado: - | Aprobado | Reprobado
        #9 nota

        if User.where('codigo = ?',fila[0]).count == 0
          @lineas_error.push index+1
          @lineas_error_messages.push ['El usuario no existe']
        else
          if ProgramCourse.where('id = ?',fila[1]).count == 0
            @lineas_error.push index+1
            @lineas_error_messages.push ['El programa_curso no existe']
          else

            fechas = Array.new

            (2..5).each do |col|
              if fila[col] == '-'
                fechas.push nil
              else

=begin                  if fila[col] == 40632
                  @lineas_error.push index+1
                  @lineas_error_messages.push ['fecha inválida']
                end
=end
=begin
                begin

                  t = fila[col].split
                  Date.new(t[2],t[1], t[0])
                rescue ArgumentError
                  @lineas_error.push index+1
                  @lineas_error_messages.push ['fecha inválida']
                end
=end

                fechas.push col.even? ? fila[col] : fila[col]+ 23.hours + 59.minutes + 59.seconds
              end

            end

            iniciado = false
            finalizado = false
            aprobado = false
            anulado = false

            if fila[6] <= 0
              @lineas_error.push index+1
              @lineas_error_messages.push ['Número de oportunidad inválido - '+fila[6]]
            end

            if fila[7] != 'No Iniciado' && fila[7] != 'Iniciado' && fila[7] != 'Terminado' && fila[7] != 'Anulado'
              @lineas_error.push index+1
              @lineas_error_messages.push ['Estado inválido - '+fila[7]]
            end

            if fila[8] < 0 || fila[8] > 100
              @lineas_error.push index+1
              @lineas_error_messages.push ['% de asistencia inválido']
            end

            if fila[9] != '-' && fila[9] != 'Aprobado' && fila[9] != 'Reprobado'
              @lineas_error.push index+1
              @lineas_error_messages.push ['Estado aprobado/reprobado inválido: '+fila[9]]
            end

            if fila[10] != '-' && fila[10].to_f < 0
              @lineas_error.push index+1
              @lineas_error_messages.push ['Nota negativa']
            end

          end

        end

      end

    end

  end


end
