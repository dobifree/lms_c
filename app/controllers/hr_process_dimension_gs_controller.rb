class HrProcessDimensionGsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def show
    @hr_process_dimension_g = HrProcessDimensionG.find(params[:id])
  end


  def new

    @hr_process_dimension = HrProcessDimension.find(params[:hr_process_dimension_id])

    @hr_process_dimension_g = @hr_process_dimension.hr_process_dimension_gs.new


  end


  def edit

    @hr_process_dimension_g = HrProcessDimensionG.find(params[:id])

    @hr_process_dimension = @hr_process_dimension_g.hr_process_dimension

  end

  def delete
    @hr_process_dimension_g = HrProcessDimensionG.find(params[:id])

    @hr_process_dimension = @hr_process_dimension_g.hr_process_dimension

  end

  def create

    @hr_process_dimension_g = HrProcessDimensionG.new(params[:hr_process_dimension_g])
    @hr_process_dimension = @hr_process_dimension_g.hr_process_dimension

    if @hr_process_dimension_g.save
      flash[:success] = t('activerecord.success.model.hr_process_dimension_g.create_ok')
      redirect_to @hr_process_dimension.hr_process_template
    else

      render action: 'new'
    end

  end

  def update
    @hr_process_dimension_g = HrProcessDimensionG.find(params[:id])
    @hr_process_dimension = @hr_process_dimension_g.hr_process_dimension

    if @hr_process_dimension_g.update_attributes(params[:hr_process_dimension_g])
      ajustar_evals(@hr_process_dimension)
      flash[:success] = t('activerecord.success.model.hr_process_dimension_g.update_ok')
      redirect_to @hr_process_dimension.hr_process_template
    else
      render action: 'edit'
    end

  end

  def destroy

    @hr_process_dimension_g = HrProcessDimensionG.find(params[:id])
    @hr_process_dimension = @hr_process_dimension_g.hr_process_dimension

    if verify_recaptcha

      #hr_process_quadrant_gs = @hr_process_dimension_g.hr_process_quadrant_gs

      if @hr_process_dimension_g.destroy

        #hr_process_quadrant_gs.each do |hr_process_quadrant_g|

        #  if hr_process_quadrant_g && hr_process_quadrant_g.hr_process_quadrant
        #    hr_process_quadrant_g.hr_process_quadrant.destroy
        #  end

        #end

        flash[:success] = t('activerecord.success.model.hr_process_dimension_g.delete_ok')
        redirect_to @hr_process_dimension.hr_process_template
      else
        flash[:danger] = t('activerecord.success.model.hr_process_dimension_g.delete_error')
        redirect_to delete_hr_process_dimension_g_path(@hr_process_dimension_g)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.hr_process_dimension_g.captcha_error')
      redirect_to delete_hr_process_dimension_g_path(@hr_process_dimension_g)

    end

  end

  private

    def ajustar_evals(hr_process_dimension)


      hr_process_dimension.hr_process_template.hr_processes.each do |hr_process|

        hr_process.hr_process_users.each do |hr_process_user|

          hr_process_user.hr_process_assessment_ds.each do |hr_process_assessment_d|

            if hr_process_assessment_d.hr_process_dimension.id == hr_process_dimension.id
=begin
              hr_process_dimension_g_match = nil

              hr_process_dimension.hr_process_dimension_gs.each do |hr_process_dimension_g|

                hr_process_dimension_g_match = hr_process_dimension_g if hr_process_dimension_g.valor_minimo <= hr_process_assessment_d.resultado_porcentaje && hr_process_assessment_d.resultado_porcentaje <= hr_process_dimension_g.valor_maximo

              end

              if hr_process_dimension_g_match

                hr_process_assessment_d.hr_process_dimension_g = hr_process_dimension_g_match
                hr_process_assessment_d.save
              end
=end
              hr_process_assessment_d_cal = hr_process_assessment_d.hr_process_assessment_d_cal

              if hr_process_assessment_d_cal



                hr_process_dimension_g_match = nil

                hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

                  if hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

                    hr_process_dimension_g_match = hr_process_dimension_g
                    break

                  end

                end

                if hr_process_dimension_g_match
                  hr_process_assessment_d_cal.hr_process_dimension_g = hr_process_dimension_g_match
                  hr_process_assessment_d_cal.save
                end
              else



                hr_process_dimension_g_match = nil

                hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

                  if hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

                    hr_process_dimension_g_match = hr_process_dimension_g
                    break

                  end

                end

                if hr_process_dimension_g_match

                  hr_process_assessment_d.hr_process_dimension_g = hr_process_dimension_g_match
                  hr_process_assessment_d.save
                end
              end

            end

          end

          hr_process_quadrant = hr_process_user.quadrant

          if hr_process_quadrant

            hr_process_assessment_qua = hr_process_user.hr_process_assessment_qua

            if hr_process_assessment_qua
=begin
              hr_process_calibration_session_m = hr_process_user.hr_process_calibration_session_ms.first
              hr_process_assessment_qua_cal = nil
              if hr_process_calibration_session_m

                hr_process_assessment_qua_cal = HrProcessAssessmentQuaCal.where('hr_process_calibration_session_id = ? AND  hr_process_calibration_session_m_id = ?', hr_process_calibration_session_m.hr_process_calibration_session.id, hr_process_calibration_session_m.id).first

              end

              if hr_process_assessment_qua_cal
                hr_process_assessment_qua_cal.hr_process_quadrant = hr_process_quadrant
                hr_process_assessment_qua_cal.save
              else
=end
                hr_process_assessment_qua.hr_process_quadrant = hr_process_quadrant
                hr_process_assessment_qua.save
#              end
            else



=begin
              hr_process_calibration_session_m = hr_process_user.hr_process_calibration_session_ms.first
              hr_process_assessment_qua_cal = nil

              if hr_process_calibration_session_m

                hr_process_assessment_qua_cal = HrProcessAssessmentQuaCal.where('hr_process_calibration_session_id = ? AND  hr_process_calibration_session_m_id = ?', hr_process_calibration_session_m.hr_process_calibration_session.id, hr_process_calibration_session_m.id).first

              end

              if hr_process_assessment_qua_cal


                qua = HrProcessAssessmentQua.new
                qua.hr_process_user = hr_process_user
                qua.fecha = lms_time
                qua.hr_process_quadrant_id = hr_process_assessment_qua_cal.hr_process_quadrant.id
                qua.save

                hr_process_assessment_qua_cal.hr_process_quadrant_id = hr_process_quadrant.id
                hr_process_assessment_qua_cal.save

              else
=end
                qua = HrProcessAssessmentQua.new
                qua.hr_process_user = hr_process_user
                qua.fecha = lms_time
                qua.hr_process_quadrant = hr_process_quadrant
                qua.save




#              end



            end

          end

        end


      end

    end



end
