class ApplicationController < ActionController::Base

  include ApplicationHelper
	include SessionsHelper
  include CoursesHelper
  include UserCoursesHelper
  include FoldersHelper
  include HrProcessAssessmentsHelper
  include MenuHelper
  include SelProcessesHelper
  include UsersHelper

  protect_from_forgery

  before_filter :get_company
  before_filter :set_db_connection
  before_filter :identify_ie
  before_filter :identify_ie8
  before_filter :identify_ie9
  before_filter :identify_chrome18
  before_filter :identify_mobile

  #before_filter :prepare_data

  rescue_from Exception, :with => :render_500 if Rails.env.production?

  def render_500(exception)
    commit = `git show --pretty=%H -q`

    ls = LogSystem.new
    ls.user = user_connected if normal_user_logged_in?
    ls.description = exception.to_s+exception.backtrace.join("\n")
    ls.params = params.to_s if params
    ls.controller = controller_name
    ls.action = action_name
    ls.registered_at = normal_user_logged_in? ? lms_time : Time.now.utc
    ls.request_raw = request.inspect.to_yaml
    ls.session_raw = session.inspect.to_yaml
    ls.server_name = Socket.gethostname
    ls.remote_ip = request.remote_ip
    ls.remote_host = request.remote_host
    ls.path_info = request.path_info
    ls.request_method = request.request_method
    ls.original_url = request.original_url
    ls.url_referer = URI(request.referer).path if request.referer
    ls.request_xhr = !request.xhr?.nil?
    ls.commit_id = commit
    ls.commit_deployed_at = File.mtime('.')
    ls.save

    if normal_user_logged_in?
      redirect_to sww_path
    elsif admin_logged_in?
      redirect_to sww_path
    else
      render 'static_pages/sww', layout: false
    end

  end

  def set_db_connection

    ActiveRecord::Base.establish_connection @company.connection_string

    #$current_domain = 'gasco-training' if Rails.env.development? # design dev
    #puts ActiveRecord::Base.connection_config
  end

  def identify_ie

    $browser_ie = false

    user_agent = UserAgent.parse(request.env['HTTP_USER_AGENT'])

    #$browser_ie = true if user_agent.browser == 'Internet Explorer' && user_agent.platform == 'Windows'
    $browser_ie = true if user_agent.to_s.include? 'Trident'

  end

  def identify_ie8

    $browser_ie8 = false

    user_agent = UserAgent.parse(request.env['HTTP_USER_AGENT'])

    $browser_ie8 = true if user_agent.browser == 'Internet Explorer' && user_agent.version.to_s.to_i < 9 && user_agent.platform == 'Windows'

  end

  def identify_ie9

    $browser_ie9 = false

    user_agent = UserAgent.parse(request.env['HTTP_USER_AGENT'])

    $browser_ie9 = true if user_agent.browser == 'Internet Explorer' && user_agent.version.to_s.to_i == 9 && user_agent.platform == 'Windows'

  end

  def identify_chrome18

    $browser_chrome18 = false

    user_agent = UserAgent.parse(request.env['HTTP_USER_AGENT'])
    #puts user_agent.browser
    #puts user_agent.version.to_s.to_i
    $browser_chrome18 = true if user_agent.browser == 'Chrome' && user_agent.version.to_s.to_i < 19

  end

  def identify_mobile
    $mobile_client = false
    user_agent = request.user_agent
    $mobile_client = true if user_agent.present? &&
        /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.match(user_agent) ||
        /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.match(user_agent[0..3])
        #user_agent =~ /\b(Android|iPhone|iPad|Windows Phone|Opera Mobi|Kindle|BackBerry|PlayBook|NokiaBrowser)\b/i
  end

  def get_company
    $full_current_domain = request.host
    $full_current_protocol_domain = request.protocol+request.host_with_port
    $current_domain = $full_current_domain.split('.')[0]

    another_domain = nil

    if $full_current_protocol_domain.slice(-3,3) == '.pe'
      another_domain = $full_current_protocol_domain.sub '.pe', '.cl'
    elsif $full_current_protocol_domain.slice(-3,3) == '.cl'
      another_domain = $full_current_protocol_domain.sub '.cl', '.pe'
    end

    #session[:company] = Company.where(:url => $full_current_protocol_domain).first #unless session[:company]

    if another_domain
      @company = Company.where('url = ? or url = ?',$full_current_protocol_domain,another_domain).first
    else
      @company = Company.where(:url => $full_current_protocol_domain).first #unless session[:company]
    end

   #@company = session[:company]
  end


end
