class PlanningProcessesController < ApplicationController

  include ActionView::Helpers::NumberHelper
  include ExcelReportsHelper

  before_filter :authenticate_user

  before_filter :verify_jefe_capacitacion, only: [:index, :new, :show, :edit, :delete, :new_unit,
                                                  :search_analyst_manager, :create, :update, :destroy, :create_unit,
                                                  :create_training_analyst, :destroy_training_analyst, :destroy_company_unit_manager]

  before_filter :verify_training_analyst, only: [:show_unit, :new_goal, :create_goal, :edit_goal, :update_goal, :destroy_goal,
                                                 :edit_fecha_fin_unit,
                                                 :update_fecha_fin_unit, :search_manager_superintendent,
                                                 :create_area_manager, :destroy_area_manager,
                                                 :create_area_superintendent, :destroy_area_superintendent, :programming_unit,
                                                 :programming_unit_pace, :programming_unit_pace_indicators, :create_dncp, :update_dncp,
                                                 :programming_manage_students, :programming_create_student, :programming_create_student_xls,
                                                 :programming_destroy_student,
                                                 :programming_prepare_course, :programming_program_course, :programming_course_evaluation, :edit_programming_course_group,
                                                 :update_programming_course_group,
                                                 :programming_course_evaluation_xls, :programming_course_evaluation_from_xls,
                                                 :programming_course_evaluate, :programming_course_poll, :programming_course_polling,
                                                 :empty_students_list_xlsx, :acta_students_xlsx]

  before_filter :verify_area_manager_superintendent_training_analyst, only: [:show_dnc, :create_dnc, :update_dnc, :destroy_dnc,
                                                                             :dnc_manage_students, :create_dnc_student, :create_dnc_student_xls,
                                                                             :destroy_dnc_student, :send_to_approve_area,
                                                                             :define_area_goals, :update_area_goals]

  before_filter :verify_area_superintendent, only: [:request_dnc_area_correction, :destroy_dnc_area_correction, :approve_area]

  before_filter :verify_unit_manager_jefe_capacitacion, only: [:unit_manager, :show_pace]

  before_filter :verify_unit_manager, only: [:approve_unit, :request_area_correction, :destroy_area_correction]

  before_filter :verify_any_profile, only: [:dnc_xlsx, :pace_xlsx]

  before_filter :verify_planning_process_abierto, only: [:show, :edit, :delete, :new_unit, :new_goal, :edit_goal, :destroy_goal,
                                                         :search_analyst_manager,
                                                         :show_unit, :show_dnc, :create_dnc, :update_dnc, :destroy_dnc, :dnc_manage_students,
                                                         :create_dnc_student, :create_dnc_student_xls, :destroy_dnc_student, :edit_fecha_fin_unit, :update_fecha_fin_unit,
                                                         :search_manager_superintendent, :update, :destroy, :create_unit, :create_goal,
                                                         :update_goal, :create_training_analyst, :destroy_training_analyst, :create_company_unit_manager,
                                                         :destroy_company_unit_manager,
                                                         :create_area_manager, :destroy_area_manager,
                                                         :create_area_superintendent, :destroy_area_superintendent,
                                                         :send_to_approve_area, :request_dnc_area_correction,
                                                         :destroy_dnc_area_correction, :approve_area, :unit_manager, :show_pace, :approve_unit,
                                                         :request_area_correction, :destroy_area_correction, :programming_unit,
                                                         :programming_unit_pace, :programming_unit_pace_indicators, :create_dncp, :update_dncp,
                                                         :programming_manage_students, :programming_create_student, :programming_create_student_xls,
                                                         :programming_destroy_student, :programming_prepare_course, :programming_program_course,
                                                         :programming_course_evaluation, :edit_programming_course_group,
                                                         :update_programming_course_group, :programming_course_evaluation_xls, :programming_course_evaluation_from_xls, :programming_course_evaluate, :programming_course_poll,
                                                         :programming_course_polling]


  def index
    @planning_processes_abiertos = PlanningProcess.find_all_by_abierto(true)
    @planning_processes_finalizados = PlanningProcess.find_all_by_abierto(false)
  end


  def show
    #@planning_process = PlanningProcess.find(params[:id])
  end

  def new
    @planning_process = PlanningProcess.new
  end

  def edit
    #@planning_process = PlanningProcess.find(params[:id])
  end

  def delete
    #@planning_process = PlanningProcess.find(params[:id])
  end

  def new_unit
    #@planning_process = PlanningProcess.find(params[:id])
  end

  def new_goal
    #@planning_process = PlanningProcess.find(params[:id])
    @planning_process_goal = PlanningProcessGoal.new
  end

  def edit_goal
    #@planning_process = PlanningProcess.find(params[:id])
    @planning_process_goal = PlanningProcessGoal.find(params[:planning_process_goal_id])
  end

  def search_analyst_manager

    #@planning_process = PlanningProcess.find(params[:id])
    @ppcu = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])

    @user = User.new
    @users = {}

    if params[:user]

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        @users = User.where(
            'apellidos LIKE ? AND nombre LIKE ?',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        @user.apellidos = params[:user][:apellidos]
        @user.nombre = params[:user][:nombre]
      end

    end

  end

  def search_manager_superintendent

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @user = User.new
    @users = {}

    if params[:user]

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        @users = User.where(
            'apellidos LIKE ? AND nombre LIKE ?',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        @user.apellidos = params[:user][:apellidos]
        @user.nombre = params[:user][:nombre]
      end

    end

  end



  def show_unit



  end

  def edit_fecha_fin_unit



  end

  def show_dnc

    @new_dnc = Dnc.new



  end



  def create
    @planning_process = PlanningProcess.new(params[:planning_process])

    if @planning_process.save
      flash[:success] = t('activerecord.success.model.planning_process.create_ok')
      redirect_to planning_processes_url
    else
      render action: 'new'
    end

  end


  def update
    #@planning_process = PlanningProcess.find(params[:id])

    if @planning_process.update_attributes(params[:planning_process])
      flash[:success] = t('activerecord.success.model.planning_process.update_ok')
      redirect_to @planning_process
    else
      render action: 'edit'
    end

  end

  def destroy

    #@planning_process = PlanningProcess.find(params[:id])

    if verify_recaptcha

      if @planning_process.destroy
        flash[:success] = t('activerecord.success.model.planning_process.delete_ok')
        redirect_to planning_processes_url
      else
        flash[:danger] = t('activerecord.success.model.planning_process.delete_error')
        redirect_to delete_planning_process_path(@planning_process)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.planning_process.captcha_error')
      redirect_to delete_planning_process_path(@planning_process)

    end

  end

  def create_unit

    #@planning_process = PlanningProcess.find(params[:id])

    add_ok = false

    params[:company_unit][:id].each do |company_unit_id|

      planning_process_company_unit = PlanningProcessCompanyUnit.new
      planning_process_company_unit.planning_process = @planning_process
      planning_process_company_unit.company_unit_id = company_unit_id

      add_ok = true if planning_process_company_unit.save

    end

    if add_ok

      flash[:success] = t('activerecord.success.model.planning_process.add_unit_ok')

    else

      flash[:danger] = t('activerecord.error.model.planning_process.add_unit_error')

    end

    redirect_to @planning_process

  end

  def create_goal

    #@planning_process = PlanningProcess.find(params[:id])

    @planning_process_goal = @planning_process_company_unit.planning_process_goals.new(params[:planning_process_goal])

    if @planning_process_goal.save

      flash[:success] = t('activerecord.success.model.planning_process_goal.create_ok')
      redirect_to show_planning_process_unit_path @planning_process, @planning_process_company_unit
    else

      render 'new_goal'

    end

  end

  def update_goal

    #@planning_process = PlanningProcess.find(params[:id])

    @planning_process_goal = PlanningProcessGoal.find(params[:planning_process_goal_id])

    if @planning_process_goal.update_attributes(params[:planning_process_goal])

      flash[:success] = t('activerecord.success.model.planning_process_goal.update_ok')
      redirect_to show_planning_process_unit_path @planning_process, @planning_process_company_unit
    else

      render 'edit_goal'

    end

  end

  def destroy_goal

    planning_process_goal = PlanningProcessGoal.find(params[:planning_process_goal_id])

    if planning_process_goal.dnc_planning_process_goals.count == 0 && planning_process_goal.dnc_planning_process_goal_ps.count == 0

      planning_process_goal.destroy
      flash[:success] = t('activerecord.success.model.planning_process_goal.delete_ok')

    end

    redirect_to show_planning_process_unit_path @planning_process, @planning_process_company_unit

  end

  def update_fecha_fin_unit

    if @planning_process_company_unit.update_attributes(fecha_fin: params[:planning_process_company_unit][:fecha_fin])
      flash[:success] = t('activerecord.success.model.planning_process.update_fecha_fin_unit_ok')
      redirect_to show_planning_process_unit_path(@planning_process, @planning_process_company_unit)

    else
      render 'edit_fecha_fin_unit'
    end



  end

  def create_training_analyst

    @ppcu = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])
    #@ppcu.training_analysts.destroy_all

    PlanningProcessCompanyUnit.find_all_by_company_unit_id(@ppcu.company_unit_id).each do |ppcu|


      training_analyst = ppcu.training_analysts.new(user_id: params[:user_id])
      training_analyst.save


    end



    flash[:success] = t('activerecord.success.model.planning_process.add_training_analyst_ok')
    redirect_to @planning_process


  end

  def destroy_training_analyst

    @ppcu = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])
    #@user = User.find(params[:user_id])

    PlanningProcessCompanyUnit.find_all_by_company_unit_id(@ppcu.company_unit_id).each do |ppcu|

      training_analyst = ppcu.training_analysts.find_by_user_id(params[:user_id])
      training_analyst.destroy

    end

    flash['success'] = t('activerecord.success.model.planning_process.delete_training_analyst_ok')

    redirect_to @planning_process

  end

  def create_company_unit_manager

    @ppcu = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])
    @ppcu.company_unit_managers.destroy_all

    company_unit_manager = @ppcu.company_unit_managers.new(user_id: params[:user_id])
    company_unit_manager.save

    flash[:success] = t('activerecord.success.model.planning_process.add_company_unit_manager_ok')
    redirect_to @planning_process

  end

  def destroy_company_unit_manager

    @ppcu = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])
    @ppcu.company_unit_managers.destroy_all

    flash['success'] = t('activerecord.success.model.planning_process.delete_company_unit_manager_ok')

    redirect_to @planning_process

  end

  def create_area_manager

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])
    @planning_process_company_unit.company_unit_area_managers.where('company_unit_area_id = ?', @company_unit_area.id).destroy_all

    company_unit_area_manager = @planning_process_company_unit.company_unit_area_managers.new(company_unit_area_id: @company_unit_area.id, user_id: params[:user_id])
    company_unit_area_manager.save

    flash[:success] = t('activerecord.success.model.planning_process.add_area_manager_ok')
    redirect_to show_planning_process_unit_path(@planning_process, @planning_process_company_unit)


  end

  def create_area_superintendent

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])
    @planning_process_company_unit.area_superintendents.where('company_unit_area_id = ?', @company_unit_area.id).destroy_all

    company_unit_area_superintendent = @planning_process_company_unit.area_superintendents.new(company_unit_area_id: @company_unit_area.id, user_id: params[:user_id])
    company_unit_area_superintendent.save

    flash[:success] = t('activerecord.success.model.planning_process.add_area_superintendent_ok')
    redirect_to show_planning_process_unit_path(@planning_process, @planning_process_company_unit)

  end

  def destroy_area_manager

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])
    @planning_process_company_unit.company_unit_area_managers.where('company_unit_area_id = ?', @company_unit_area.id).destroy_all


    flash[:success] = t('activerecord.success.model.planning_process.delete_area_manager_ok')
    redirect_to show_planning_process_unit_path(@planning_process, @planning_process_company_unit)


  end

  def destroy_area_superintendent

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])
    @planning_process_company_unit.area_superintendents.where('company_unit_area_id = ?', @company_unit_area.id).destroy_all

    flash[:success] = t('activerecord.success.model.planning_process.delete_area_superintendent_ok')
    redirect_to show_planning_process_unit_path(@planning_process, @planning_process_company_unit)

  end

  def define_area_goals

    @planning_process_company_unit_area = @planning_process_company_unit.planning_process_company_unit_areas_area(@company_unit_area)

    @planning_process_company_unit_area = PlanningProcessCompanyUnitArea.new unless @planning_process_company_unit_area

  end

  def update_area_goals

    ppcua = @planning_process_company_unit.planning_process_company_unit_areas_area(@company_unit_area)

    unless ppcua
      ppcua = @planning_process_company_unit.planning_process_company_unit_areas.new
      ppcua.planning_process = @planning_process_company_unit.planning_process
      ppcua.company_unit_area = @company_unit_area

    end

    ppcua.objetivos = params[:planning_process_company_unit_area][:objetivos]
    ppcua.save

    flash[:success] = t('activerecord.success.model.planning_process.define_area_goals')
    redirect_to planning_process_show_dnc_path @planning_process, @planning_process_company_unit, @company_unit_area

  end



  def create_dnc

    dnc = Dnc.new(params[:dnc])

    h = 0;
    m = 0;

    h = params[:dnc_tiempo_capacitacion_horas].strip.to_i if params[:dnc_tiempo_capacitacion_horas]
    m = params[:dnc_tiempo_capacitacion_minutos].strip.to_i if params[:dnc_tiempo_capacitacion_minutos]

    dnc.tiempo_capacitacion = (h*60)+m

    dnc.planning_process_id = @planning_process.id
    dnc.planning_process_company_unit_id = @planning_process_company_unit.id
    dnc.company_unit_area_id = @company_unit_area.id

    checked_goals = false

    @planning_process_company_unit.planning_process_goals.each do |goal|

      if params[:planning_process_goal][goal.id.to_s] == '1'
        checked_goals = true
      end

    end

    if checked_goals && dnc.save

      @planning_process_company_unit.planning_process_goals.each do |goal|

        if params[:planning_process_goal][goal.id.to_s] == '1'
          dnc_goal = dnc.dnc_planning_process_goals.new
          dnc_goal.planning_process_goal_id = goal.id
          dnc_goal.save
        end

      end

      it = 0.0

      TrainingImpactCategory.all.each do |cat|

        dnc_training_impact = dnc.dnc_training_impacts.new
        dnc_training_impact.training_impact_category_id = cat.id
        dnc_training_impact.training_impact_value_id = params[:training_impact_category][cat.id.to_s]
        dnc_training_impact.save

        it += dnc_training_impact.training_impact_value.valor*dnc_training_impact.training_impact_category.peso

      end

      dnc.impacto_total = it
      dnc.save

      flash[:success] = t('activerecord.success.model.dnc.create_ok')
      redirect_to planning_process_show_dnc_path(@planning_process,@planning_process_company_unit,@company_unit_area)

    else
      @new_dnc = dnc
      @dnc_tic = {}
      @dnc_goal = {}

      @new_dnc.errors.add('Debe', t('activerecord.error.model.dnc.objetivos_empty')) unless checked_goals

      TrainingImpactCategory.all.each do |cat|
        @dnc_tic[cat.id.to_s] = params[:training_impact_category][cat.id.to_s]
      end

      @planning_process_company_unit.planning_process_goals.each do |goal|

        if params[:planning_process_goal][goal.id.to_s] == '1'
          @dnc_goal[goal.id.to_s] = '1'
        end

      end

      render 'show_dnc'

    end



  end

  def update_dnc

    dnc = Dnc.find(params[:dnc_id])

    h = 0;
    m = 0;

    h = params[:dnc_tiempo_capacitacion_horas].strip.to_i if params[:dnc_tiempo_capacitacion_horas]
    m = params[:dnc_tiempo_capacitacion_minutos].strip.to_i if params[:dnc_tiempo_capacitacion_minutos]

    params[:dnc][:tiempo_capacitacion] = (h*60)+m

    checked_goals = false

    @planning_process_company_unit.planning_process_goals.each do |goal|

      if params[:planning_process_goal][goal.id.to_s] == '1'
        checked_goals = true
      end

    end

    if checked_goals && dnc.update_attributes(params[:dnc])

      dnc.dnc_planning_process_goals.destroy_all
      dnc.dnc_training_impacts.destroy_all

      @planning_process_company_unit.planning_process_goals.each do |goal|

        if params[:planning_process_goal][goal.id.to_s] == '1'
          dnc_goal = dnc.dnc_planning_process_goals.new
          dnc_goal.planning_process_goal_id = goal.id
          dnc_goal.save
        end

      end

      it = 0.0


      TrainingImpactCategory.all.each do |cat|

        dnc_training_impact = dnc.dnc_training_impacts.new
        dnc_training_impact.training_impact_category_id = cat.id
        dnc_training_impact.training_impact_value_id = params[:training_impact_category][cat.id.to_s]
        dnc_training_impact.save

        it += dnc_training_impact.training_impact_value.valor*dnc_training_impact.training_impact_category.peso

      end


      dnc.impacto_total = it
      dnc.save

      flash[:success] = t('activerecord.success.model.dnc.update_ok')
      redirect_to planning_process_show_dnc_path(@planning_process,@planning_process_company_unit,@company_unit_area)

    else
      @new_dnc = Dnc.new
      @try_update_dnc = dnc

      @try_update_dnc.errors.add('Debe', t('activerecord.error.model.dnc.objetivos_empty')) unless checked_goals

      render 'show_dnc'

    end



  end



  def destroy_dnc

    dnc = Dnc.find(params[:dnc_id])

    dnc.dnc_planning_process_goals.destroy_all
    dnc.dnc_training_impacts.destroy_all
    dnc.dnc_students.destroy_all
    dnc.dnc_area_corrections.destroy_all
    dnc.destroy

    flash[:success] = t('activerecord.success.model.dnc.delete_ok')
    redirect_to planning_process_show_dnc_path(@planning_process,@planning_process_company_unit,@company_unit_area)

  end

  def dnc_manage_students

    @dnc = Dnc.find(params[:dnc_id])

    @user = User.new
    @users = {}

    @characteristics = Characteristic.find_all_by_publica(true)
    @characteristics_prev = {}
    queries = []

    n_q = 0

    if params[:characteristic]

        @characteristics.each do |characteristic|

        #params[:characteristic][characteristic.id.to_s.to_sym].slice! 0 if params[:characteristic]

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]


          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND valor = "'+@characteristics_prev[characteristic.id]+'"'
=begin
          @characteristics_prev[characteristic.id].each_with_index do |valor,index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'valor = "'+valor+'" '

          end

          queries[n_q] += ' ) '
=end
          n_q += 1

        end

      end

        params[:user][:apellidos] ='%'  if params[:user] && params[:user][:apellidos].blank?
        params[:user][:nombre] ='%'  if params[:user] && params[:user][:nombre].blank?

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        if queries.length > 0

          queries.each_with_index do |query,index|

            if index == 0

              @users = User.joins(:user_characteristics).where(
                  'apellidos LIKE ? AND nombre LIKE ? AND '+query,
                  '%'+params[:user][:apellidos]+'%',
                  '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

            else

              lista_users_ids = @users.map {|u| u.id }

              @users = User.joins(:user_characteristics).where(
                  'apellidos LIKE ? AND nombre LIKE ? AND '+query+' AND users.id IN (?) ',
                  '%'+params[:user][:apellidos]+'%',
                  '%'+params[:user][:nombre]+'%',
                  lista_users_ids).order('apellidos, nombre')

            end

          end

        else

          @users = User.where(
              'apellidos LIKE ? AND nombre LIKE ? ',
              '%'+params[:user][:apellidos]+'%',
              '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        end

      end

    end

    params[:user][:apellidos] ='' if params[:user] && params[:user][:apellidos] == '%'
    params[:user][:nombre] ='' if params[:user] && params[:user][:nombre] == '%'

    @user = User.new(params[:user])

    @lineas_error = {}

  end

  def create_dnc_student
    @dnc = Dnc.find(params[:dnc_id])
    @users = {}
    @user = User.new(params[:user])

    @characteristics = Characteristic.find_all_by_publica(true)
    @characteristics_prev = {}


    @characteristics.each do |characteristic|

      params[:characteristic][characteristic.id.to_s.to_sym].slice! 0 if params[:characteristic]

      if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

        @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]

      end

    end

    params[:user_id].each do |user_id,value|

      if value != '0'

        dnc_student = @dnc.dnc_students.new
        dnc_student.user_id = user_id
        dnc_student.save

        @dnc.reload

        @dnc.number_students = @dnc.dnc_students.size
        @dnc.save


      end

    end

    flash['success'] = t('activerecord.success.model.dnc_student.create_ok')

    redirect_to planning_process_dnc_manage_students_path(@planning_process,@planning_process_company_unit, @company_unit_area, @dnc)

  end

  def create_dnc_student_xls

    @dnc = Dnc.find(params[:dnc_id])
    @users = {}
    @user = User.new(params[:user])

    @characteristics = Characteristic.find_all_by_publica(true)
    @characteristics_prev = {}

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_students_data_for_enroll archivo_temporal

        if @lineas_error.length > 0

          flash.now[:danger] = t('activerecord.error.model.dnc_student.enroll_from_excel_error')
          render 'dnc_manage_students'

        else

          enroll_students_dnc_from_excel archivo_temporal, @dnc

          flash['success'] = t('activerecord.success.model.dnc_student.create_xls_ok')

          redirect_to planning_process_dnc_manage_students_path(@planning_process,@planning_process_company_unit, @company_unit_area, @dnc)

        end

      else

        flash.now[:danger] = t('activerecord.error.model.dnc_student.enroll_from_excel_no_xlsx')
        render 'dnc_manage_students'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.dnc_student.enroll_from_excel_no_file')
      render 'dnc_manage_students'

    end



  end

  def destroy_dnc_student

    @dnc = Dnc.find(params[:dnc_id])
    @user = User.find(params[:user_id])

    dns_student = @dnc.dnc_students.find_by_user_id(@user.id)

    dns_student.destroy

    @dnc.reload

    @dnc.number_students = @dnc.dnc_students.size
    @dnc.save

    flash['success'] = t('activerecord.success.model.dnc_student.delete_ok')


    redirect_to planning_process_dnc_manage_students_path(@planning_process,@planning_process_company_unit, @company_unit_area, @dnc)

  end

  def send_to_approve_area

    dncs = @planning_process_company_unit.dncs_area @company_unit_area

    dncs.each do |dnc|
      dnc.enviado_aprobar_area = true
      dnc.requiere_modificaciones_area = false
      dnc.save

      dnc.dnc_area_corrections.each do |dnc_ac|

        dnc_ac.revisado = true
        dnc_ac.save

      end

    end

    LmsMailer.planning_process_send_to_approve_area(@planning_process_company_unit, @company_unit_area, @company).deliver

    flash['success'] = t('activerecord.success.model.dnc.send_to_area_approve')
    redirect_to planning_process_show_dnc_path(@planning_process,@planning_process_company_unit,@company_unit_area)

  end

  def approve_area

    dncs = @planning_process_company_unit.dncs_area @company_unit_area

    dncs.each do |dnc|
      dnc.enviado_aprobar_area = true
      dnc.requiere_modificaciones_area = false
      dnc.aprobado_area = true
      dnc.save

      dnc.dnc_area_corrections.each do |dnc_ac|

        dnc_ac.revisado = true
        dnc_ac.save

      end

    end

    @planning_process_company_unit.area_corrections(@company_unit_area).each do |ac|

      ac.revisado = true
      ac.save

    end

    LmsMailer.planning_process_approve_area(@planning_process_company_unit, @company_unit_area, @company).deliver

    flash['success'] = t('activerecord.success.model.dnc.area_approve_ok')
    redirect_to planning_process_show_dnc_path(@planning_process,@planning_process_company_unit,@company_unit_area)

  end

  def request_dnc_area_correction

    @dnc = Dnc.find(params[:dnc_id])

    @dnc.requiere_modificaciones_area = true
    @dnc.save

    dnc_area_correction = @dnc.dnc_area_corrections.new
    dnc_area_correction.correccion = params[:dnc_area_correction][:correccion]

    dnc_area_correction.save

    LmsMailer.planning_process_request_dnc_area_correction(@planning_process_company_unit, @company_unit_area, @company).deliver

    flash['success'] = t('activerecord.success.model.dnc_area_correction.create_ok')

    redirect_to planning_process_show_dnc_path(@planning_process,@planning_process_company_unit,@company_unit_area)

  end

  def destroy_dnc_area_correction

    @dnc = Dnc.find(params[:dnc_id])
    dnc_ac = DncAreaCorrection.find(params[:dnc_area_correction_id])

    @dnc.requiere_modificaciones_area = false
    @dnc.save

    dnc_ac.destroy

    flash['success'] = t('activerecord.success.model.dnc_area_correction.delete_ok')

    redirect_to planning_process_show_dnc_path(@planning_process,@planning_process_company_unit,@company_unit_area)

  end

  def unit_manager



  end

  def show_pace

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])
    @ppcu_area_correction = PlanningProcessCompanyUnitAreaCorrection.new

  end

  def request_area_correction

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @ppcu_area_correction = @planning_process_company_unit.planning_process_company_unit_area_corrections.new

    @ppcu_area_correction.company_unit_area = @company_unit_area
    @ppcu_area_correction.correccion = params[:planning_process_company_unit_area_correction][:correccion]

    @ppcu_area_correction.save

    dncs = @planning_process_company_unit.dncs_area @company_unit_area

    dncs.each do |dnc|
      dnc.aprobado_area = false
      dnc.save
    end

    LmsMailer.planning_process_request_area_correction(@planning_process_company_unit, @company_unit_area, @company).deliver

    flash['success'] = t('activerecord.success.model.planning_process_company_unit_area_correction.create_ok')

    redirect_to planning_process_show_pace_path(@planning_process,@planning_process_company_unit,@company_unit_area)

  end

  def destroy_area_correction

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    ppcu_area_correction = PlanningProcessCompanyUnitAreaCorrection.find(params[:ppcu_area_correction_id])

    ppcu_area_correction.destroy

    area_corrections_no_revisado = @planning_process_company_unit.area_corrections_revisado @company_unit_area, false

    if area_corrections_no_revisado.length == 0

      dncs = @planning_process_company_unit.dncs_area @company_unit_area

      dncs.each do |dnc|
        dnc.aprobado_area = true
        dnc.save
      end

    end



    flash['success'] = t('activerecord.success.model.planning_process_company_unit_area_correction.delete_ok')

    redirect_to planning_process_show_pace_path(@planning_process,@planning_process_company_unit,@company_unit_area)

  end

  def approve_unit

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    program_anual = Program.find_by_plan_anual(true)

    if program_anual && program_anual.levels.length > 0

      dncs = @planning_process_company_unit.dncs_area @company_unit_area

      dncs.each do |dnc|
        dnc.enviado_aprobar_area = true
        dnc.requiere_modificaciones_area = false
        dnc.aprobado_area = true
        dnc.aprobado_unidad = true
        dnc.save

        dnc.dnc_area_corrections.each do |dnc_ac|

          dnc_ac.revisado = true
          dnc_ac.save

        end

      end

      @planning_process_company_unit.area_corrections(@company_unit_area).each do |ac|

        ac.revisado = true
        ac.save

      end

      program_unit program_anual

    end

    LmsMailer.planning_process_approve_unit(@planning_process_company_unit, @company_unit_area, @company).deliver

    flash['success'] = t('activerecord.success.model.dnc.unit_approve_ok')
    redirect_to planning_process_show_pace_path(@planning_process,@planning_process_company_unit,@company_unit_area)

  end

  def programming_unit



  end


  def migracion_masiva_excel

  end

  def migracion_masiva_excel_upload

    require 'rubyXL'

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        #check_migra_ateneus archivo_temporal

        if @lineas_error && @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.enrollment.update_file_error')
        else
          ejecuta_migra_excel archivo_temporal

          flash.now[:success] = t('activerecord.success.model.enrollment.migra_ateneus_ok')

        end

      else

        flash.now[:danger] = t('activerecord.error.model.enrollment.upload_file_no_xlsx')

      end

    else
      flash.now[:danger] = t('activerecord.error.model.enrollment.upload_file_no_file')

    end

    render 'migracion_masiva_excel'

  end





  def programming_unit_pace_indicators



    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    data_table = GoogleVisualr::DataTable.new
    data_table.new_column('string', 'mes'       )
    data_table.new_column('number', 'Gestión'     )
    data_table.new_column('number', 'Medio Ambiente'     )
    data_table.new_column('number', 'Otros'  )
    data_table.new_column('number', 'Plan Desarrollo Individual')

    data_table.add_rows( [
                             ['Ene', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Feb', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Mar', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Abr', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['May', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Jun', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Jul', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Ago', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Sep', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Oct', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Nov', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Dic', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ]
                         ] )


    @data_table1 = data_table

    opts   = {
        :width => '1000',
        :height => '400',
        :title => 'Cumplimiento en Base a Registro de Asistencia',
        :vAxis => {:title => 'Porcentaje'},
        :hAxis => {:title => 'Meses'},
        :seriesType => 'bars',
        :series => {'4' => {:type => 'line'},},
        :vAxis => {:maxValue => 100},
        :vAxis => {:minValue => 0}
    }

    @rep1 = GoogleVisualr::Interactive::ComboChart.new(data_table, opts)

    data_table = GoogleVisualr::DataTable.new
    data_table.new_column('string', 'mes'       )
    data_table.new_column('number', 'Gestión'     )
    data_table.new_column('number', 'Medio Ambiente'     )
    data_table.new_column('number', 'Otros'  )
    data_table.new_column('number', 'Plan Desarrollo Individual')

    data_table.add_rows( [
                             ['Ene', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Feb', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Mar', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Abr', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['May', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Jun', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Jul', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Ago', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Sep', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Oct', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Nov', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Dic', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ]
                         ] )

    @data_table2 = data_table

    opts   = {
        :width => '1000',
        :height => '400',
        :title => 'Ejecutados vs Programados',
        :vAxis => {:title => 'Porcentaje'},
        :hAxis => {:title => 'Meses'},
        :seriesType => 'bars',
        :series => {'4' => {:type => 'line'},},
        :vAxis => {:maxValue => 100},
        :vAxis => {:minValue => 0}
    }

    @rep2 = GoogleVisualr::Interactive::ComboChart.new(data_table, opts)

    data_table = GoogleVisualr::DataTable.new
    data_table.new_column('string', 'mes'       )
    data_table.new_column('number', 'Gestión'     )
    data_table.new_column('number', 'Medio Ambiente'     )
    data_table.new_column('number', 'Otros'  )
    data_table.new_column('number', 'Plan Desarrollo Individual')

    data_table.add_rows( [
                             ['Ene', 70 + Random.rand(50), 70 + Random.rand(30) , 70 + Random.rand(20), 70 + Random.rand(20)  ],
                             ['Feb', 70 + Random.rand(30), 70 + Random.rand(40) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Mar', 70 + Random.rand(70), 70 + Random.rand(30) , 70 + Random.rand(40), 70 + Random.rand(30)  ],
                             ['Abr', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(40)  ],
                             ['May', 70 + Random.rand(50), 70 + Random.rand(70) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Jun', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(60), 70 + Random.rand(30)  ],
                             ['Jul', 70 + Random.rand(70), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(50)  ],
                             ['Ago', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Sep', 70 + Random.rand(60), 70 + Random.rand(20) , 70 + Random.rand(70), 70 + Random.rand(70)  ],
                             ['Oct', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Nov', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(60), 70 + Random.rand(90)  ],
                             ['Dic', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ]
                         ] )

    @data_table3 = data_table

    opts   = {
        :width => '1000',
        :height => '400',
        :title => 'Costo Real vs Costo Presupuestado (en el plan) ',
        :vAxis => {:title => 'Porcentaje'},
        :hAxis => {:title => 'Meses'},
        :seriesType => 'bars',
        :series => {'4' => {:type => 'line'},},
        :vAxis => {:minValue => 0}
    }

    @rep3 = GoogleVisualr::Interactive::ComboChart.new(data_table, opts)

    data_table = GoogleVisualr::DataTable.new
    data_table.new_column('string', 'mes'       )
    data_table.new_column('number', 'Gestión'     )
    data_table.new_column('number', 'Medio Ambiente'     )
    data_table.new_column('number', 'Otros'  )
    data_table.new_column('number', 'Plan Desarrollo Individual')

    data_table.add_rows( [
                             ['Ene', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Feb', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Mar', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Abr', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['May', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Jun', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Jul', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Ago', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Sep', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Oct', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Nov', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Dic', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ]
                         ] )

    @data_table4 = data_table

    opts   = {
        :width => '1000',
        :height => '400',
        :title => 'Cursos no Programados ',
        :vAxis => {:title => 'Porcentaje'},
        :hAxis => {:title => 'Meses'},
        :seriesType => 'bars',
        :series => {'4' => {:type => 'line'},},
        :vAxis => {:maxValue => 100},
        :vAxis => {:minValue => 0}
    }

    @rep4 = GoogleVisualr::Interactive::ComboChart.new(data_table, opts)

    data_table = GoogleVisualr::DataTable.new
    data_table.new_column('string', 'mes'       )
    data_table.new_column('number', 'Gestión'     )
    data_table.new_column('number', 'Medio Ambiente'     )
    data_table.new_column('number', 'Otros'  )
    data_table.new_column('number', 'Plan Desarrollo Individual')

    data_table.add_rows( [
                             ['Ene', 70 + Random.rand(50), 70 + Random.rand(30) , 70 + Random.rand(20), 70 + Random.rand(20)  ],
                             ['Feb', 70 + Random.rand(30), 70 + Random.rand(40) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Mar', 70 + Random.rand(70), 70 + Random.rand(30) , 70 + Random.rand(40), 70 + Random.rand(30)  ],
                             ['Abr', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(40)  ],
                             ['May', 70 + Random.rand(50), 70 + Random.rand(70) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Jun', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(60), 70 + Random.rand(30)  ],
                             ['Jul', 70 + Random.rand(70), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(50)  ],
                             ['Ago', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Sep', 70 + Random.rand(60), 70 + Random.rand(20) , 70 + Random.rand(70), 70 + Random.rand(70)  ],
                             ['Oct', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ],
                             ['Nov', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(60), 70 + Random.rand(90)  ],
                             ['Dic', 70 + Random.rand(30), 70 + Random.rand(30) , 70 + Random.rand(30), 70 + Random.rand(30)  ]
                         ] )

    @data_table5 = data_table

    opts   = {
        :width => '1000',
        :height => '400',
        :title => 'Costo Real vs Costo Presupuestado (no programados) ',
        :vAxis => {:title => 'Porcentaje'},
        :hAxis => {:title => 'Meses'},
        :seriesType => 'bars',
        :series => {'4' => {:type => 'line'},},
        :vAxis => {:minValue => 0}
    }

    @rep5 = GoogleVisualr::Interactive::ComboChart.new(data_table, opts)


  end

  def programming_unit_pace



    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])
    @new_dncp = Dncp.new

    @mes_actual = lms_time.strftime '%-m'

    @mes_actual = params[:mes_actual] if params[:mes_actual]


  end


  def create_dncp


    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    dncp = Dncp.new(params[:dncp])

    h = 0;
    m = 0;

    h = params[:dncp_tiempo_capacitacion_horas].strip.to_i if params[:dncp_tiempo_capacitacion_horas]
    m = params[:dncp_tiempo_capacitacion_minutos].strip.to_i if params[:dncp_tiempo_capacitacion_minutos]

    dncp.tiempo_capacitacion = (h*60)+m

    dncp.planning_process_id = @planning_process.id
    dncp.planning_process_company_unit_id = @planning_process_company_unit.id
    dncp.company_unit_area_id = @company_unit_area.id

    checked_goals = false

    @planning_process_company_unit.planning_process_goals.each do |goal|

      if params[:planning_process_goal][goal.id.to_s] == '1'
        checked_goals = true
      end

    end

    if checked_goals && dncp.save

      @planning_process_company_unit.planning_process_goals.each do |goal|

        if params[:planning_process_goal][goal.id.to_s] == '1'
          dncp_goal = dncp.dnc_planning_process_goal_ps.new

          dncp_goal.planning_process_goal_id = goal.id
          dncp_goal.save
        end

      end

      it = 0.0

      TrainingImpactCategory.all.each do |cat|

        dncp_training_impact = dncp.dnc_training_impact_ps.new
        dncp_training_impact.training_impact_category_id = cat.id
        dncp_training_impact.training_impact_value_id = params[:training_impact_category][cat.id.to_s]
        dncp_training_impact.save

        it += dncp_training_impact.training_impact_value.valor*dncp_training_impact.training_impact_category.peso

      end

      dncp.impacto_total = it

      #course_shape_id = nil
      #course_shape_id = params[:course][:course_shape_id] if params[:course] && params[:course][:course_shape_id] && params[:course][:course_shape_id] != ''

      #dncp.course_id = course_shape_id

      dncp.save

      program_anual = Program.find_by_plan_anual(true)

      master_poll_id = nil
      master_poll_id = params[:course][:master_poll_id] if params[:course] && params[:course][:master_poll_id]


      create_course_from_dncp program_anual, dncp, master_poll_id


      flash[:success] = t('activerecord.success.model.dncp.create_ok')
      redirect_to programming_process_unit_pace_path(@planning_process, @planning_process_company_unit, @company_unit_area, dncp.meses)


    else
      @new_dncp = dncp
      @dncp_tic = {}
      @dncp_goal = {}

      @new_dncp.errors.add('Debe', t('activerecord.error.model.dncp.objetivos_empty')) unless checked_goals

      TrainingImpactCategory.all.each do |cat|
        @dncp_tic[cat.id.to_s] = params[:training_impact_category][cat.id.to_s]
      end

      @planning_process_company_unit.planning_process_goals.each do |goal|

        if params[:planning_process_goal][goal.id.to_s] == '1'
          @dncp_goal[goal.id.to_s] = '1'
        end

      end

      @mes_actual = lms_time.strftime '%-m'

      @mes_actual = params[:mes_actual] if params[:mes_actual]


      render 'programming_unit_pace'

    end



  end

  def update_dncp


    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    dncp = Dncp.find(params[:dncp_id])

    h = 0;
    m = 0;

    h = params[:dncp_tiempo_capacitacion_horas].strip.to_i if params[:dncp_tiempo_capacitacion_horas]
    m = params[:dncp_tiempo_capacitacion_minutos].strip.to_i if params[:dncp_tiempo_capacitacion_minutos]

    params[:dncp][:tiempo_capacitacion] = (h*60)+m

    checked_goals = false

    @planning_process_company_unit.planning_process_goals.each do |goal|

      if params[:planning_process_goal][goal.id.to_s] == '1'
        checked_goals = true
      end

    end

    if checked_goals && dncp.update_attributes(params[:dncp])

      dncp.dnc_planning_process_goal_ps.destroy_all
      dncp.dnc_training_impact_ps.destroy_all

      @planning_process_company_unit.planning_process_goals.each do |goal|

        if params[:planning_process_goal][goal.id.to_s] == '1'
          dncp_goal = dncp.dnc_planning_process_goal_ps.new
          dncp_goal.planning_process_goal_id = goal.id
          dncp_goal.save
        end

      end

      it = 0.0


      TrainingImpactCategory.all.each do |cat|

        dncp_training_impact = dncp.dnc_training_impact_ps.new
        dncp_training_impact.training_impact_category_id = cat.id
        dncp_training_impact.training_impact_value_id = params[:training_impact_category][cat.id.to_s]
        dncp_training_impact.save

        it += dncp_training_impact.training_impact_value.valor*dncp_training_impact.training_impact_category.peso

      end


      dncp.impacto_total = it

      #course_shape_id = nil
      #course_shape_id = params[:course][:course_shape_id] if params[:course] && params[:course][:course_shape_id] && params[:course][:course_shape_id] != ''

      #dncp.course_id = course_shape_id


      dncp.save

      master_poll_id = nil
      master_poll_id = params[:course][:master_poll_id] if params[:course] && params[:course][:master_poll_id]

      update_course_from_dncp dncp, master_poll_id

      flash[:success] = t('activerecord.success.model.dncp.update_ok')
      redirect_to programming_process_unit_pace_path(@planning_process, @planning_process_company_unit, @company_unit_area, dncp.meses)

    else
      @new_dncp = Dncp.new
      @try_update_dncp = dncp

      @mes_actual = dncp.meses

      @try_update_dncp.errors.add('Debe', t('activerecord.error.model.dncp.objetivos_empty')) unless checked_goals

      render 'programming_unit_pace'

    end



  end

  def programming_manage_students

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @user = User.new
    @users = {}

    @characteristics = Characteristic.find_all_by_publica(true)
    @characteristics_prev = {}
    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        #params[:characteristic][characteristic.id.to_s.to_sym].slice! 0 if params[:characteristic]

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]


          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND valor = "'+@characteristics_prev[characteristic.id]+'" '
=begin
          @characteristics_prev[characteristic.id].each_with_index do |valor,index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'valor = "'+valor+'" '

          end

          queries[n_q] += ' ) '
=end
          n_q += 1

        end

      end

      params[:user][:apellidos] ='%'  if params[:user] && params[:user][:apellidos].blank?
      params[:user][:nombre] ='%'  if params[:user] && params[:user][:nombre].blank?

      if !params[:user][:apellidos].blank? || !params[:user][:nombre].blank?

        if queries.length > 0

          queries.each_with_index do |query,index|

            if index == 0

              @users = User.joins(:user_characteristics).where(
                  'apellidos LIKE ? AND nombre LIKE ? AND '+query,
                  '%'+params[:user][:apellidos]+'%',
                  '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

            else

              lista_users_ids = @users.map {|u| u.id }

              @users = User.joins(:user_characteristics).where(
                  'apellidos LIKE ? AND nombre LIKE ? AND '+query+' AND users.id IN (?) ',
                  '%'+params[:user][:apellidos]+'%',
                  '%'+params[:user][:nombre]+'%',
                  lista_users_ids).order('apellidos, nombre')

            end

          end

        else

          @users = User.where(
              'apellidos LIKE ? AND nombre LIKE ? ',
              '%'+params[:user][:apellidos]+'%',
              '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

        end

      end

    end

    params[:user][:apellidos] ='' if params[:user] && params[:user][:apellidos] == '%'
    params[:user][:nombre] ='' if params[:user] && params[:user][:nombre] == '%'

    @user = User.new(params[:user])

  end

  def programming_create_student

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])
    @users = {}
    @user = User.new(params[:user])

    @characteristics = Characteristic.find_all_by_publica(true)
    @characteristics_prev = {}


    @characteristics.each do |characteristic|

      params[:characteristic][characteristic.id.to_s.to_sym].slice! 0 if params[:characteristic]

      if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

        @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]

      end

    end

    params[:user_id].each do |user_id,value|

      if value != '0'

        dncp_student = @dncp.dnc_student_ps.new
        dncp_student.user_id = user_id
        dncp_student.save

        @dncp.reload

        @dncp.number_students = @dncp.dnc_student_ps.size
        @dncp.save


      end

    end

    flash['success'] = t('activerecord.success.model.dncp_student.create_ok')

    redirect_to programming_manage_students_path(@planning_process,@planning_process_company_unit, @company_unit_area, @dncp)

  end

  def programming_create_student_xls

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])
    @users = {}
    @user = User.new(params[:user])

    @characteristics = Characteristic.find_all_by_publica(true)
    @characteristics_prev = {}

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_students_data_for_enroll archivo_temporal

        if @lineas_error.length > 0

          flash.now[:danger] = t('activerecord.error.model.dnc_student.enroll_from_excel_error')
          render 'programming_manage_students'

        else

          enroll_students_dncp_from_excel archivo_temporal, @dncp

          flash['success'] = t('activerecord.success.model.dnc_student.create_xls_ok')

          redirect_to programming_manage_students_path(@planning_process,@planning_process_company_unit, @company_unit_area, @dncp)

        end

      else

        flash.now[:danger] = t('activerecord.error.model.dnc_student.enroll_from_excel_no_xlsx')
        render 'programming_manage_students'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.dnc_student.enroll_from_excel_no_file')
      render 'programming_manage_students'

    end



  end

  def programming_destroy_student

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])
    @user = User.find(params[:user_id])

    dncp_student = @dncp.dnc_student_ps.find_by_user_id(@user.id)

    dncp_student.destroy

    @dncp.reload

    @dncp.number_students = @dncp.dnc_student_ps.size
    @dncp.save

    flash['success'] = t('activerecord.success.model.dncp_student.delete_ok')


    redirect_to programming_manage_students_path(@planning_process,@planning_process_company_unit, @company_unit_area, @dncp)

  end

  def programming_prepare_course

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])


  end

  def programming_program_course

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    program_anual = Program.find_by_plan_anual(true)
    level = program_anual.levels.first

    course = @dncp.course

    program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    desdehasta = params[:desdehasta].split('-')

    desde = desdehasta[0].strip
    desde_d = desde.split('/').map { |s| s.to_i }
    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta = desdehasta[1].strip
    hasta_d = hasta.split('/').map { |s| s.to_i }
    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    uc_temp = program_course.user_courses.where('desde = ? AND hasta = ?', desde, hasta).first

    program_course_instance = uc_temp.program_course_instance if uc_temp

    unless program_course_instance

      program_course_instance = program_course.program_course_instances.build
      program_course_instance.from_date = desde
      program_course_instance.to_date = hasta
      program_course_instance.obra = params[:acta]

    end


    if params[:user_id]

      params[:user_id].each do |user_id,value|

        if value != '0'

          user = User.find(user_id)

          enrollment = Enrollment.where('program_id = ? AND user_id = ?',program_anual.id, user.id).first

          unless enrollment

            enrollment = Enrollment.new(program: program_anual, user: user, level: level)
            enrollment.save

          end

          user_course = UserCourse.new

          user_course.enrollment = enrollment
          user_course.program_course = program_course
          user_course.course = course



          user_course.desde = desde


          user_course.hasta = hasta

          user_course_last = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND anulado = ? ', enrollment.id, program_course.id, false).order('numero_oportunidad DESC').first

          numero_oportunidad = 1

          numero_oportunidad = user_course_last.numero_oportunidad+1 if user_course_last

          user_course.numero_oportunidad = numero_oportunidad

          user_course.dncp_id = @dncp.id

          user_course.save

          #######

          if program_course_instance.save
            user_course.program_course_instance = program_course_instance
            user_course.save
          end

          #######

        end

      end

    end


    flash[:success] = t('activerecord.success.model.planning_process.program_course_ok')
    redirect_to programming_process_unit_pace_path(@planning_process, @planning_process_company_unit, @company_unit_area, @dncp.meses)

  end

  def programming_course_evaluation

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @grupo = params[:grupo]

    program_anual = Program.find_by_plan_anual(true)
    course = @dncp.course
    program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    @user_courses = UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ? AND anulado = ? ', program_course.id, @dncp.id, @grupo, false).joins(:user).order('apellidos, nombre')

  end

  def edit_programming_course_group

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @grupo = params[:grupo]

    program_anual = Program.find_by_plan_anual(true)
    course = @dncp.course
    program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    @user_courses = UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ? AND anulado = ? ', program_course.id, @dncp.id, @grupo, false).joins(:user).order('apellidos, nombre')

  end

  def update_programming_course_group

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @grupo = params[:grupo]

    program_anual = Program.find_by_plan_anual(true)

    course = @dncp.course

    program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    desdehasta = params[:desdehasta].split('-')

    desde = desdehasta[0].strip
    desde_d = desde.split('/').map { |s| s.to_i }
    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta = desdehasta[1].strip
    hasta_d = hasta.split('/').map { |s| s.to_i }
    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    u_c = UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ?', program_course.id, @dncp.id, @grupo).first

    program_course_instance = u_c.program_course_instance if u_c

    program_course_instance = program_course.program_course_instances.build unless program_course_instance

    program_course_instance.from_date = desde
    program_course_instance.to_date = hasta
    program_course_instance.obra = params[:acta]

    if program_course_instance.save

      UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ?', program_course.id, @dncp.id, @grupo).each do |uc|

        uc.desde = desde
        uc.hasta = hasta
        uc.program_course_instance = program_course_instance
        uc.save

      end



    end


    flash[:success] = t('activerecord.success.model.planning_process.program_course_ok')
    redirect_to programming_process_unit_pace_path(@planning_process, @planning_process_company_unit, @company_unit_area, @dncp.meses)

  end

  def programming_course_evaluation_xls

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @grupo = params[:grupo]

    program_anual = Program.find_by_plan_anual(true)
    course = @dncp.course
    program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    @user_courses = UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ? AND anulado = ? ', program_course.id, @dncp.id, @grupo, false).joins(:user).order('apellidos, nombre')

    reporte_excel = xls_programming_course_evaluation_xls course, @user_courses, @dncp

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Registro de Resultados '+course.nombre+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def programming_course_evaluation_from_xls

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @grupo = params[:grupo]

    program_anual = Program.find_by_plan_anual(true)
    course = @dncp.course
    program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    @user_courses = UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ? AND anulado = ? ', program_course.id, @dncp.id, @grupo, false).joins(:user).order('apellidos, nombre')


    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        update_program_course_evaluation_results_from_excel archivo_temporal, program_course, @dncp

        flash[:success] = t('activerecord.success.model.training_results.update_results_ok')

        redirect_to programming_course_evaluation_path @planning_process, @planning_process_company_unit, @company_unit_area, @dncp, params[:grupo]

      else

        flash.now[:danger] = t('activerecord.error.model.training_results.update_results_from_excel_no_xlsx')
        render 'programming_course_evaluation'

      end

    else

      flash.now[:danger] = t('activerecord.error.model.training_results.update_results_from_excel_no_file')
      render 'programming_course_evaluation'

    end


  end

  def programming_course_evaluate

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    if @dncp.asistencia_presencial

      params[:user_id].each do |user_course_id,value|

        user_course = UserCourse.find(user_course_id)
        asist = params[:user_id][user_course.id.to_s.to_sym]
        if asist == user_course.id.to_s
          user_course.iniciado = true
          user_course.finalizado = true
          user_course.asistencia = true
          user_course.porcentaje_avance = 100
          user_course.inicio = user_course.desde if user_course.inicio.nil?
          user_course.fin = user_course.hasta if user_course.fin.nil?
        else
          user_course.asistencia = false
        end

        user_course.save

      end

    end

    if @dncp.eval_presencial

      params[:user_course_id].each do |user_course_id,value|

        user_course = UserCourse.find(user_course_id)

        user_course.nota = value

        user_course.inicio = user_course.desde if user_course.inicio.nil?

        if user_course.save

          if user_course.course.nota_minima == 0
            user_course.aprobado = true
          elsif !user_course.nota.nil?
            user_course.aprobado = true
            user_course.aprobado = false if (value.to_i < user_course.course.nota_minima) || user_course.nota == ''
          elsif
            user_course.aprobado = false
          end

          user_course.save

        end

      end

    end

    if @dncp.asistencia_presencial && @dncp.eval_presencial
      flash[:success] = t('activerecord.success.model.planning_process.evaluate_assit_course_ok')
    elsif @dncp.asistencia_presencial
      flash[:success] = t('activerecord.success.model.planning_process.assit_course_ok')
    elsif @dncp.eval_presencial
      flash[:success] = t('activerecord.success.model.planning_process.evaluate_course_ok')
    end

    redirect_to programming_course_evaluation_path(@planning_process,@planning_process_company_unit, @company_unit_area, @dncp.id, params[:grupo])

  end

  def programming_course_poll

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @grupo = params[:grupo]

    program_anual = Program.find_by_plan_anual(true)
    course = @dncp.course
    @program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    @user_course = UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ? AND anulado = ? ', @program_course.id, @dncp.id, @grupo, false).first

  end

  def programming_course_polling

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    if @dncp.poll_presencial?

      program_anual = Program.find_by_plan_anual(true)
      course = @dncp.course
      @program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

      poll = @dncp.course.polls.where('tipo_satisfaccion = ?', true).first

      master_questions = poll.master_poll.master_poll_questions

      master_questions.each do |master_question|

        master_question.master_poll_alternatives.each do |alt|

          poll_summary = ProgramCoursePollSummary.where('program_course_id = ? AND poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ? AND grupo = ? ',
                                                        @program_course.id, poll.id, master_question.id, alt.id, params[:grupo]).first

          poll_summary.destroy if poll_summary

          poll_summary = ProgramCoursePollSummary.new
          poll_summary.program_course = @program_course
          poll_summary.poll = poll
          poll_summary.master_poll_question = master_question
          poll_summary.master_poll_alternative = alt
          poll_summary.grupo = params[:grupo]
          poll_summary.numero_respuestas = 0

          if params[:poll_summary][alt.id.to_s.to_sym]

            poll_summary.numero_respuestas = params[:poll_summary][alt.id.to_s.to_sym]

          end

          poll_summary.save

        end

      end

      flash[:success] = t('activerecord.success.model.planning_process.polling_course_ok')

    end

    redirect_to programming_course_poll_path(@planning_process,@planning_process_company_unit, @company_unit_area, @dncp.id, params[:grupo])

  end

  #noinspection RubyArgCount
  def dnc_xlsx

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel


    cats = TrainingImpactCategory.all
    meses = t('date.month_names')

    total_p1 = 6
    total_p2 = cats.length+1
    total_p3 = 6
    total_cols = total_p1+ total_p2 + total_p3

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, vertical: :top, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                              :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :center, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                              :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      headerRight = s.add_style bg_color: secundary_background_color[$current_domain],
                                :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :right, :wrap_text => true}

      headerRotated = s.add_style bg_color: secundary_background_color[$current_domain],
                                  :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { horizontal: :center, :textRotation => 90}

      fieldLeft = s.add_style :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      fieldRight = s.add_style :sz => 11,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :right, :wrap_text => true}

      fieldCenter = s.add_style :sz => 11,
                                  :border => { :style => :thin, color: '00' },
                                  :alignment => {:horizontal => :center, :wrap_text => true}

      fieldLeftTT = Hash.new

      TrainingType.all.each do |tt|

        fieldLeftTT[tt.id.to_s] = s.add_style :bg_color => tt.color,
                                              :sz => 11,
                                              :border => { :style => :thin, color: '00' },
                                              :alignment => { :horizontal => :left, :wrap_text => true}

      end




      wb.add_worksheet(:name => 'DNC') do |reporte_excel|

        fila_actual = 1

        reporte_excel.add_row ['Detección de Necesidades de Capacitación'], :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Proceso de Planificación'
        fila << ''
        fila << @planning_process.year.to_s+' - '+@planning_process.nombre
        (total_cols-3).times{ fila << ''}

        reporte_excel.add_row fila, :style => headerTitleInfo
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Unidad'
        fila << ''
        fila << @planning_process_company_unit.company_unit.nombre
        (total_cols-3).times{ fila << ''}

        reporte_excel.add_row fila, :style => headerTitleInfo
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Área'
        fila << ''
        fila << @company_unit_area.nombre
        (total_cols-3).times{ fila << ''}

        reporte_excel.add_row fila, :style => headerTitleInfo
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        area_goals = ''

        pp_cua = @planning_process_company_unit.planning_process_company_unit_areas_area(@company_unit_area)

        area_goals = format_html_to_excel(pp_cua.objetivos) if pp_cua

        @planning_process_company_unit

        fila = Array.new
        fila << 'Objetivos'
        fila << ''
        fila << area_goals
        (total_cols-3).times{ fila << ''}



        reporte_excel.add_row fila, :style => headerTitleInfo, height: area_goals.count("\n")*14
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1


        fila = Array.new
        fila << 'Datos del Curso'
        fila << ''
        fila << ''
        fila << ''
        fila << ''
        fila << ''
        fila << 'Impacto'
        (cats.length).times { fila << ''}
        fila << 'Dictado del Curso'
        fila << ''
        fila << ''
        fila << ''
        fila << ''
        fila << ''

        #reporte_excel.add_row fila, :style => headerCenter
        #reporte_excel.merge_cells('A'+fila_actual.to_s+':F'+fila_actual.to_s)
        #reporte_excel.merge_cells('G'+fila_actual.to_s+':'+letras[5+cats.length+1]+fila_actual.to_s)
        #reporte_excel.merge_cells(letras[5+cats.length+2]+fila_actual.to_s+':'+letras[5+cats.length+7]+fila_actual.to_s)

        reporte_excel.add_row fila, :style => headerCenter
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[total_p1-1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[total_p1]+fila_actual.to_s+':'+letras[total_p1+total_p2-1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[total_p1+total_p2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila2 = Array.new
        fila2Header = Array.new
        cols_widths = Array.new

        fila2 << '#'
        fila2Header <<  headerRight
        cols_widths << 3

        fila2 << 'Nombre del Curso o Tema del Capacitación'
        fila2Header <<  headerLeft
        cols_widths << 25

        fila2 << 'Tipo de Capacitación'
        fila2Header <<  headerLeft
        cols_widths << 15

        fila2 << 'Oportunidades de Mejora'
        fila2Header <<  headerLeft
        cols_widths << 15

        fila2 << 'Objetivos Estratégicos'
        fila2Header <<  headerLeft
        cols_widths << 20

        fila2 << 'Objetivos del Curso'
        fila2Header <<  headerLeft
        cols_widths << 20



        cats.each do |cat|
          fila2 << cat.nombre
          fila2Header << headerRotated
          cols_widths << 3
        end

        fila2 << 'TOTAL'
        fila2Header << headerRotated
        cols_widths << 6

        fila2 << 'Número de Participantes'
        fila2Header << headerCenter
        cols_widths << 15

        fila2 << 'Modalidad de Capacitación'
        fila2Header << headerCenter
        cols_widths << 15

        fila2 << 'Horas de Capacitación'
        fila2Header << headerCenter
        cols_widths << 15

        fila2 << 'Mes Tentativo'
        fila2Header << headerLeft
        cols_widths << 12

        fila2 << 'Proveedor'
        fila2Header << headerLeft
        cols_widths << 12

        fila2 << 'Costo (USD)'
        fila2Header << headerRight
        cols_widths << 10

        reporte_excel.add_row fila2, style: fila2Header, :height => 120

        fila_actual += 1

        fila_inicio_contenidos = fila_actual

        @planning_process_company_unit.dncs_area(@company_unit_area).each_with_index do |dnc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldRight

          fila << dnc.tema_capacitacion
          fileStyle << fieldLeft

          fila << dnc.training_type.nombre
          fileStyle << fieldLeftTT[dnc.training_type.id.to_s]

          fila << format_html_to_excel(dnc.oportunidad_mejora)
          fileStyle << fieldLeft

          obj_est = ''
          dnc.dnc_planning_process_goals.each_with_index do |dnc_goal, index1|

            obj_est += "- #{dnc_goal.planning_process_goal.descripcion}\n"

          end

          fila << obj_est
          fileStyle << fieldLeft

          fila << format_html_to_excel(dnc.objetivos_curso)

          fileStyle << fieldLeft

          cats.each_with_index do |cat, index1|
            i = dnc.dnc_training_impacts.find_by_training_impact_category_id(cat.id)

            if i
              fila << i.training_impact_value.valor
              fileStyle << fieldCenter
            else
              fila << ''
            end
          end

          fila << number_with_precision(dnc.impacto_total, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter)
          fileStyle << fieldCenter

          fila << dnc.dnc_students.count
          fileStyle << fieldCenter

          fila << dnc.training_mode.nombre
          fileStyle << fieldCenter

          hh = dnc.tiempo_capacitacion/60
          hh = '0'+hh.to_s if hh < 10
          mm = dnc.tiempo_capacitacion%60
          mm = '0'+mm.to_s if mm < 10

          fila << hh.to_s+':'+mm.to_s
          fileStyle << fieldCenter

          fila << meses[dnc.meses.to_i]
          fileStyle << fieldLeft

          if dnc.training_provider
            fila << dnc.training_provider.nombre
          else
            fila << '--Por definir--'
          end

          fileStyle << fieldLeft

          fila << number_with_precision(dnc.costo_directo + dnc.costo_indirecto, precision: 0)
          fileStyle << fieldRight

          reporte_excel.add_row fila, style: fileStyle

          fila_actual += 1

        end

        fila = Array.new


        fila << ''
        fila << ''
        fila << ''
        fila << ''
        fila << ''
        fila << ''
        (cats.length).times { fila << ''}
        fila << ''
        fila << ''
        fila << ''
        fila << ''
        fila << ''

        fila << 'Total'
        #fila << number_to_currency(costo_total, precision: 0)
        #fila << number_with_precision(costo_total, precision: 0)
        fila << '=SUM('+letras[total_cols-1]+fila_inicio_contenidos.to_s+':'+letras[total_cols-1]+(fila_actual-1).to_s+')'

        reporte_excel.add_row fila, style: headerRight

        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[total_cols-3]+fila_actual.to_s)

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

        #reporte_excel.column_widths cols_widths


      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'DNC-'+@planning_process.year.to_s+'-'+@planning_process_company_unit.company_unit.nombre+'-'+@company_unit_area.nombre+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def pace_xlsx

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    cats = TrainingImpactCategory.all
    meses = t('date.month_names')


    total_p1 = 4
    total_p2 = cats.length+1
    total_p3 = 7
    total_cols = total_p1+ total_p2 + total_p3

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 11,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerRight = s.add_style bg_color: secundary_background_color[$current_domain],
                                :b => true,
                                :sz => 11,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :right, :wrap_text => true}

      headerRotated = s.add_style bg_color: secundary_background_color[$current_domain],
                                  :b => true,
                                  :sz => 11,
                                  :border => { :style => :thin, color: '00' },
                                  :alignment => { horizontal: :center, :textRotation => 90}

      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      fieldRight = s.add_style :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :right, :wrap_text => true}

      fieldCenter = s.add_style :sz => 11,
                                :border => { :style => :thin, color: '00' },
                                :alignment => {:horizontal => :center, :wrap_text => true}

      fieldLeftTT = Hash.new

      TrainingType.all.each do |tt|

        fieldLeftTT[tt.id.to_s] = s.add_style :bg_color => tt.color,
                                              :sz => 11,
                                              :border => { :style => :thin, color: '00' },
                                              :alignment => { :horizontal => :left, :wrap_text => true}

      end




      wb.add_worksheet(:name => 'DNC') do |reporte_excel|

        fila_actual = 1

        reporte_excel.add_row ['Plan de Capacitación y Desarrollo'], :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Proceso de Planificación'
        fila << ''
        fila << @planning_process.year.to_s+' - '+@planning_process.nombre
        (total_cols-3).times{ fila << ''}

        reporte_excel.add_row fila, :style => headerTitleInfo
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Unidad'
        fila << ''
        fila << @planning_process_company_unit.company_unit.nombre
        (total_cols-3).times{ fila << ''}

        reporte_excel.add_row fila, :style => headerTitleInfo
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Área'
        fila << ''
        fila << @company_unit_area.nombre
        (total_cols-3).times{ fila << ''}

        reporte_excel.add_row fila, :style => headerTitleInfo
        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << '#'
        filaHeader <<  headerRight

        fila << 'Nombre del Curso o Tema del Capacitación'
        filaHeader <<  headerRight

        fila << 'Modalidad de Capacitación'
        filaHeader <<  headerRight

        fila << 'Proveedor'
        filaHeader <<  headerRight


        fila << 'Impacto'
        filaHeader <<  headerCenter
        (cats.length).times do
          fila << ''
          filaHeader <<  headerCenter
        end
        fila << 'Dictado del Curso'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        reporte_excel.add_row fila, :style => filaHeader
        #reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[total_p1-1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[total_p1]+fila_actual.to_s+':'+letras[total_p1+total_p2-1]+fila_actual.to_s)
        reporte_excel.merge_cells(letras[total_p1+total_p2]+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

        fila_actual += 1

        fila2 = Array.new
        fila2Header = Array.new
        cols_widths = Array.new

        fila2 << ''
        fila2Header <<  headerRight
        cols_widths << 3

        fila2 << ''
        fila2Header <<  headerLeft
        cols_widths << 25

        fila2 << ''
        fila2Header << headerCenter
        cols_widths << 15

        fila2 << ''
        fila2Header << headerLeft
        cols_widths << 12

        cats.each do |cat|
          fila2 << cat.nombre
          fila2Header << headerRotated
          cols_widths << 3
        end

        fila2 << 'TOTAL'
        fila2Header << headerRotated
        cols_widths << 6

        fila2 << 'Número de Participantes'
        fila2Header << headerCenter
        cols_widths << 15

        fila2 << 'Horas de Capacitación'
        fila2Header << headerCenter
        cols_widths << 15

        fila2 << 'Total H-H'
        fila2Header << headerCenter
        cols_widths << 10

        fila2 << 'Costo directo (USD)'
        fila2Header << headerRight
        cols_widths << 10

        fila2 << 'Costo indirecto (USD)'
        fila2Header << headerRight
        cols_widths << 10

        fila2 << 'Costo total (USD)'
        fila2Header << headerRight
        cols_widths << 10

        fila2 << 'Mes'
        fila2Header << headerLeft
        cols_widths << 12

        reporte_excel.add_row fila2, style: fila2Header, :height => 120
        reporte_excel.merge_cells('A'+(fila_actual-1).to_s+':A'+fila_actual.to_s)
        reporte_excel.merge_cells('B'+(fila_actual-1).to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+(fila_actual-1).to_s+':C'+fila_actual.to_s)
        reporte_excel.merge_cells('D'+(fila_actual-1).to_s+':D'+fila_actual.to_s)


        fila_actual += 1

        fila_inicio_contenidos = fila_actual

        total_duracion = 0

        TrainingType.all.each do |tt|

          fila = Array.new
          fila << tt.nombre
          (total_cols-1).times{ fila << '' }

          reporte_excel.add_row fila, style: fieldLeftTT[tt.id.to_s]
          reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[total_cols-1]+fila_actual.to_s)

          fila_actual += 1



          @planning_process_company_unit.dncs_area_training_type(@company_unit_area, tt).each_with_index do |dnc, index|

            fila = Array.new
            fileStyle = Array.new

            fila << index+1
            fileStyle << fieldRight

            fila << dnc.tema_capacitacion
            fileStyle << fieldLeft

            fila << dnc.training_mode.nombre
            fileStyle << fieldCenter

            if dnc.training_provider
              fila << dnc.training_provider.nombre
            else
              fila << '--Por definir--'
            end

            fileStyle << fieldLeft

            cats.each_with_index do |cat, index1|
              i = dnc.dnc_training_impacts.find_by_training_impact_category_id(cat.id)

              if i
                fila << i.training_impact_value.valor
                fileStyle << fieldCenter
              else
                fila << ''
              end
            end

            fila << number_with_precision(dnc.impacto_total, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter)
            fileStyle << fieldCenter

            fila << dnc.dnc_students.count
            fileStyle << fieldCenter

            total_duracion += dnc.tiempo_capacitacion

            hh = dnc.tiempo_capacitacion/60
            hh = '0'+hh.to_s if hh < 10
            mm = dnc.tiempo_capacitacion%60
            mm = '0'+mm.to_s if mm < 10

            fila << hh.to_s+':'+mm.to_s
            fileStyle << fieldCenter


            total_hh_aux = dnc.dnc_students.count*dnc.tiempo_capacitacion/60
            fila << total_hh_aux
            fileStyle << fieldCenter

            fila << number_with_precision(dnc.costo_directo, precision: 0)
            fileStyle << fieldRight

            fila << number_with_precision(dnc.costo_indirecto, precision: 0)
            fileStyle << fieldRight

            fila << number_with_precision(dnc.costo_directo + dnc.costo_indirecto, precision: 0)
            fileStyle << fieldRight

            fila << meses[dnc.meses.to_i]
            fileStyle << fieldLeft

            reporte_excel.add_row fila, style: fileStyle

            fila_actual += 1

          end

        end


        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerRight
        fila << ''
        filaHeader << headerRight
        fila << ''
        filaHeader << headerRight
        fila << ''
        filaHeader << headerRight
        (cats.length).times do
          fila << ''
          filaHeader << headerRight
        end
        fila << 'Total'
        filaHeader << headerRight

        fila << '=SUM('+letras[total_p1+cats.length+1]+fila_inicio_contenidos.to_s+':'+letras[total_p1+cats.length+1]+(fila_actual-1).to_s+')'
        filaHeader << headerCenter

        hh = total_duracion/60
        hh = '0'+hh.to_s if hh < 10
        mm = total_duracion%60
        mm = '0'+mm.to_s if mm < 10

        fila << hh.to_s+':'+mm.to_s
        filaHeader << headerCenter

        fila << '=SUM('+letras[total_p1+cats.length+3]+fila_inicio_contenidos.to_s+':'+letras[total_p1+cats.length+3]+(fila_actual-1).to_s+')'
        filaHeader << headerCenter

        fila << '=SUM('+letras[total_p1+cats.length+4]+fila_inicio_contenidos.to_s+':'+letras[total_p1+cats.length+4]+(fila_actual-1).to_s+')'
        filaHeader << headerRight

        fila << '=SUM('+letras[total_p1+cats.length+5]+fila_inicio_contenidos.to_s+':'+letras[total_p1+cats.length+5]+(fila_actual-1).to_s+')'
        filaHeader << headerRight

        fila << '=SUM('+letras[total_p1+cats.length+6]+fila_inicio_contenidos.to_s+':'+letras[total_p1+cats.length+6]+(fila_actual-1).to_s+')'
        filaHeader << headerRight

        reporte_excel.add_row fila, style: filaHeader

        reporte_excel.merge_cells('A'+fila_actual.to_s+':'+letras[total_p1+cats.length-1]+fila_actual.to_s)

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

        #reporte_excel.column_widths cols_widths


      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'PACE-'+@planning_process.year.to_s+'-'+@planning_process_company_unit.company_unit.nombre+'-'+@company_unit_area.nombre+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def empty_students_list_xlsx

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @grupo = params[:grupo]

    program_anual = Program.find_by_plan_anual(true)
    course = @dncp.course
    program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    @user_courses = UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ? AND anulado = ? ', program_course.id, @dncp.id, @grupo, false).joins(:user).order('apellidos, nombre')

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}



      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}





      wb.add_worksheet(:name => 'Lista') do |reporte_excel|

        characteristics = Characteristic.where('planning_process = ?', true).reorder('orden_planning_process')

        fila_actual = 1

        fila = Array.new
        fila << 'Curso'
        fila << ''
        fila << @dncp.tema_capacitacion
        fila << ''

        reporte_excel.add_row fila, :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':D'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Fecha'
        fila << ''
        fila << (localize  @user_courses.first.desde, format: :day_month_year)+' - '+(localize  @user_courses.first.hasta, format: :day_month_year)
        fila << ''

        reporte_excel.add_row fila, :style => headerTitleInfo, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':D'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 3

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        characteristics.each do |c|
          fila << c.nombre
          filaHeader <<  headerLeft
          cols_widths << 22
        end

        fila << ''
        filaHeader <<  headerLeft
        cols_widths << 20


        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          characteristics.each do |c|

            u_cha = uc.enrollment.user.characteristic_by_id(c.id)
            if u_cha
              fila << u_cha.valor if uc
            else
              fila << ''
            end

            fileStyle << fieldLeft

          end

          fila << ''
          fileStyle << fieldLeft

          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Lista - '+@dncp.tema_capacitacion+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  def acta_students_xlsx

    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @dncp = Dncp.find(params[:dncp_id])

    @grupo = params[:grupo]

    program_anual = Program.find_by_plan_anual(true)
    course = @dncp.course
    program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', program_anual.id, course.id).first

    @user_courses = UserCourse.where('program_course_id = ? AND dncp_id = ? AND grupo = ? AND anulado = ? ', program_course.id, @dncp.id, @grupo, false).joins(:user).order('apellidos, nombre')

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    p = Axlsx::Package.new
    wb = p.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 12,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 11,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 11,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}



      fieldLeft = s.add_style :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 11,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}



      wb.add_worksheet(:name => 'Acta') do |reporte_excel|

        characteristics = Characteristic.where('planning_process = ?', true).reorder('orden_planning_process')

        fila_actual = 1

        fila = Array.new
        fila << 'Curso'
        fila << ''
        fila << @dncp.tema_capacitacion
        fila << ''
        fila << ''

        reporte_excel.add_row fila, :style => headerTitle, height: 30
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('C'+fila_actual.to_s+':F'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << 'Fecha'
        fila << ''
        fila << (localize  @user_courses.first.desde, format: :day_month_year)+' - '+(localize  @user_courses.first.hasta, format: :day_month_year)
        fila << 'Nota Min.'
        fila << @dncp.nota_minima
        fila << ''

        reporte_excel.add_row fila, :style => headerTitleInfo, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        reporte_excel.merge_cells('E'+fila_actual.to_s+':F'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 3

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        characteristics.each do |c|
          fila << c.nombre
          filaHeader <<  headerLeft
          cols_widths << 22
        end

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Nota'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10


        reporte_excel.add_row fila, :style => filaHeader, height: 20


        fila_actual += 1

        @user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          characteristics.each do |c|

            u_cha = uc.enrollment.user.characteristic_by_id(c.id)
            if u_cha
              fila << u_cha.valor if uc
            else
              fila << ''
            end

            fileStyle << fieldLeft

          end

          if uc.asistencia
            fila << 'Sí' if uc.asistencia
          else
            fila << 'NO'
          end

          fileStyle << fieldLeft

          fila << uc.nota

          if uc.aprobado
            fileStyle << fieldLeftAprob
            fila << 'Aprobado'
            fileStyle << fieldLeftAprob

          else
            fileStyle << fieldLeftReprob
            if uc.nota
              fila << 'Reprobado'
            else
              fila << ''
            end
            fileStyle << fieldLeftReprob


          end


          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



      end

    end



    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    p.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Acta - '+@dncp.tema_capacitacion+'-'+(localize  @user_courses.first.desde, format: :day_snmonth)+'-'+(localize  @user_courses.first.hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'



  end

  private


  def ejecuta_migra_excel(archivo_temporal)

    tic1 = TrainingImpactCategory.all[0]
    tic2 = TrainingImpactCategory.all[1]
    tic3 = TrainingImpactCategory.all[2]
    tic4 = TrainingImpactCategory.all[3]

    tiv1 = TrainingImpactValue.all[0]
    tiv2 = TrainingImpactValue.all[1]
    tiv3 = TrainingImpactValue.all[2]
    tiv4 = TrainingImpactValue.all[3]

    archivo = RubyXL::Parser.parse archivo_temporal

    planning_process = PlanningProcess.first

    program_anual = Program.find_by_plan_anual(true)


    archivo.worksheets[0].extract_data.each_with_index do |fila, index|

      if index > 0

        company_unit = CompanyUnit.where('nombre = ?', fila[0]).first
        unless company_unit
          company_unit = CompanyUnit.new
          company_unit.country_id = 1
          company_unit.planning_process_id = planning_process.id
          company_unit.nombre = fila[0]

          company_unit.save
        end

        if company_unit
          ppcu = planning_process.planning_process_company_units.where('company_unit_id = ?', company_unit.id).first
          unless ppcu
            ppcu = planning_process.planning_process_company_units.build
            ppcu.company_unit_id = company_unit.id
            ppcu.fecha_fin = lms_time-4.months
            ppcu.save
          end

          if ppcu

            pp_bg1 = ppcu.planning_process_goals.where('descripcion = ?', 'CREC. NEGO').first
            unless pp_bg1
              pp_bg1 = ppcu.planning_process_goals.build
              pp_bg1.descripcion = 'CREC. NEGO'
              pp_bg1.numero = 1
              unless pp_bg1.save

              end
            end

            pp_bg2 = ppcu.planning_process_goals.where('descripcion = ?', 'EXC. OPER.').first
            unless pp_bg2
              pp_bg2 = ppcu.planning_process_goals.build
              pp_bg2.descripcion = 'EXC. OPER.'
              pp_bg2.numero = 1
              pp_bg2.save
            end

            pp_bg3 = ppcu.planning_process_goals.where('descripcion = ?', 'ORG. ALTO DES.').first
            unless pp_bg3
              pp_bg3 = ppcu.planning_process_goals.build
              pp_bg3.descripcion = 'ORG. ALTO DES.'
              pp_bg3.numero = 1
              pp_bg3.save
            end

            pp_bg4 = ppcu.planning_process_goals.where('descripcion = ?', 'RSE').first
            unless pp_bg4
              pp_bg4 = ppcu.planning_process_goals.build
              pp_bg4.descripcion = 'RSE'
              pp_bg4.numero = 1
              pp_bg4.save
            end

            cua = company_unit.company_unit_areas.where('nombre = ?', fila[1]).first

            unless cua

              cua = company_unit.company_unit_areas.build
              cua.nombre = fila[1]
              cua.save

            end

            if cua

              dnc = planning_process.dncs.where('planning_process_company_unit_id = ? AND company_unit_area_id = ? AND tema_capacitacion = ?',ppcu.id, cua.id, fila[2]).first

              unless dnc

                dnc = planning_process.dncs.build
                dnc.planning_process_company_unit_id = ppcu.id
                dnc.company_unit_area_id = cua.id
                dnc.oportunidad_mejora = 'no definida'
                dnc.tema_capacitacion = fila[2]


                dnc.objetivos_curso = 'no definidos'
                dnc.costo_directo = 0
                dnc.costo_indirecto = 0

                tm = TrainingMode.where('nombre = ?', fila[18]).first
                unless tm
                  tm = TrainingMode.new
                  tm.nombre = fila[18]
                  tm.save
                end

                dnc.training_mode_id = tm.id if tm

                tt = TrainingType.where('nombre = ?', fila[6]).first
                unless tt
                  tt = TrainingType.new
                  tt.nombre = fila[6]
                  tt.color = '#ff8585'
                  unless tt.save

                  end
                end
                dnc.training_type_id = tt.id if tt

                dnc.meses = fila[21]
                dnc.tiempo_capacitacion = fila[20].nil? || fila[20].blank? ? 30 : fila[20]

                tp = TrainingProvider.where('nombre = ?', fila[23]).first
                unless tp
                  tp = TrainingProvider.new
                  tp.nombre = fila[23]
                  tp.save
                end
                dnc.training_provider_id = tp.id if tp

                dnc.enviado_aprobar_area = 1
                dnc.requiere_modificaciones_area = 0
                dnc.aprobado_area = 1
                dnc.aprobado_unidad = 1
                #dnc.observaciones = fila[]
                dnc.number_students = fila[22]
                dnc.eval_eficacia = fila[19]

                tprog = TrainingProgram.where('nombre = ?', fila[7]).first
                unless tprog
                  tprog = TrainingProgram.new
                  tprog.nombre = fila[7]
                  tprog.save
                end
                dnc.training_program_id = tprog.id if tprog

                if dnc.save

                  if fila[9] == 1

                    dnc_goal1 = dnc.dnc_planning_process_goals.build
                    dnc_goal1.planning_process_goal_id = pp_bg1.id
                    unless dnc_goal1.save

                    end

                  end

                  if fila[10] == 1

                    dnc_goal2 = dnc.dnc_planning_process_goals.build
                    dnc_goal2.planning_process_goal_id = pp_bg2.id
                    unless dnc_goal2.save

                    end

                  end

                  if fila[11] == 1

                    dnc_goal3 = dnc.dnc_planning_process_goals.build
                    dnc_goal3.planning_process_goal_id = pp_bg3.id
                    unless dnc_goal3.save

                    end

                  end

                  if fila[12] == 1

                    dnc_goal4 = dnc.dnc_planning_process_goals.build
                    dnc_goal4.planning_process_goal_id = pp_bg4.id
                    unless dnc_goal4.save

                    end

                  end

                  dnc_ti1 = dnc.dnc_training_impacts.build
                  dnc_ti1.training_impact_category_id = tic1.id
                  dnc_ti1.training_impact_value_id = case fila[14]
                                                       when 1
                                                         tiv1.id
                                                       when 2
                                                         tiv2.id
                                                       when 3
                                                         tiv3.id
                                                       when 4
                                                         tiv4.id
                                                     end
                  dnc_ti1.save

                  dnc_ti2 = dnc.dnc_training_impacts.build
                  dnc_ti2.training_impact_category_id = tic2.id
                  dnc_ti2.training_impact_value_id = case fila[15]
                                                       when 1
                                                         tiv1.id
                                                       when 2
                                                         tiv2.id
                                                       when 3
                                                         tiv3.id
                                                       when 4
                                                         tiv4.id
                                                     end
                  dnc_ti2.save

                  dnc_ti3 = dnc.dnc_training_impacts.build
                  dnc_ti3.training_impact_category_id = tic3.id
                  dnc_ti3.training_impact_value_id = case fila[16]
                                                       when 1
                                                         tiv1.id
                                                       when 2
                                                         tiv2.id
                                                       when 3
                                                         tiv3.id
                                                       when 4
                                                         tiv4.id
                                                     end
                  dnc_ti3.save

                  dnc_ti4 = dnc.dnc_training_impacts.build
                  dnc_ti4.training_impact_category_id = tic4.id
                  dnc_ti4.training_impact_value_id = case fila[17]
                                                       when 1
                                                         tiv1.id
                                                       when 2
                                                         tiv2.id
                                                       when 3
                                                         tiv3.id
                                                       when 4
                                                         tiv4.id
                                                     end
                  dnc_ti4.save

                  dnc.impacto_total = (dnc_ti1.training_impact_value.valor+dnc_ti2.training_impact_value.valor+dnc_ti3.training_impact_value.valor+dnc_ti4.training_impact_value.valor)
                  dnc.save



                else

                end

              end

              if dnc

                user = User.find_by_codigo(fila[3])
                unless user
                  user = User.new
                  user.codigo = fila[3]
                  user.apellidos = fila[4]
                  user.nombre = fila[5]
                  user.password = fila[3]
                  user.password_confirmation = fila[3]
                end

                if user.save

                  dnc_student = dnc.dnc_students.build
                  dnc_student.user = user
                  dnc_student.save

                  dnc.costo_directo += fila[24].nil? || fila[24].blank? ? 0 : fila[24]
                  dnc.save

                end

              end

            end

          end

        end

      end

    end


    if program_anual && program_anual.levels.length > 0

      planning_process.reload

      @planning_process = planning_process

      planning_process.dncs.each do |dnc|

        dnc.number_students = dnc.dnc_students.size
        dnc.save

        unless dnc.dncp

          dncp = Dncp.new

          dncp.dnc_id = dnc.id

          dncp.planning_process = dnc.planning_process
          dncp.planning_process_company_unit = dnc.planning_process_company_unit
          dncp.company_unit_area = dnc.company_unit_area
          dncp.oportunidad_mejora = dnc.oportunidad_mejora
          dncp.tema_capacitacion = dnc.tema_capacitacion
          dncp.objetivos_curso = dnc.objetivos_curso
          dncp.costo_directo = dnc.costo_directo
          dncp.costo_indirecto = dnc.costo_indirecto
          dncp.training_mode = dnc.training_mode
          dncp.training_type = dnc.training_type
          dncp.meses = dnc.meses
          dncp.tiempo_capacitacion = dnc.tiempo_capacitacion
          dncp.training_provider = dnc.training_provider
          dncp.impacto_total = dnc.impacto_total
          dncp.nota_minima = 0
          dncp.number_students = dnc.number_students
          dncp.eval_eficacia = dnc.eval_eficacia
          dncp.training_program_id = dnc.training_program_id

          dncp.observaciones = dnc.observaciones

          dncp.save

          dnc.dnc_planning_process_goals.each do |dnc_goal|

            dncp_goal = dncp.dnc_planning_process_goal_ps.new
            dncp_goal.planning_process_goal = dnc_goal.planning_process_goal
            dncp_goal.save

          end

          dnc.dnc_training_impacts.each do |dnc_impact|

            dncp_impact = dncp.dnc_training_impact_ps.new
            dncp_impact.training_impact_category = dnc_impact.training_impact_category
            dncp_impact.training_impact_value = dnc_impact.training_impact_value
            dncp_impact.save

          end

          dnc.dnc_students.each do |dnc_student|

            dncp_student = dncp.dnc_student_ps.new
            dncp_student.user = dnc_student.user
            dncp_student.save

          end

          create_course_from_dncp program_anual, dncp, nil

        end

      end


    end


  end

    def verify_jefe_capacitacion

      unless user_connected.jefe_capacitacion?
        flash[:danger] = t('security.no_access_planning_process_as_jefe_capacitacion')
        redirect_to root_path
      end

    end

    def verify_planning_process_abierto

      @planning_process = PlanningProcess.find(params[:id])

      unless @planning_process.abierto?
        flash[:danger] = t('security.no_planning_process_abierto')
        redirect_to root_path
      end


    end

    def verify_training_analyst

      permiso = false

      @planning_process = PlanningProcess.find(params[:id])
      @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])

      @planning_process_company_unit.training_analysts.each do |ta|

        if ta.user_id == user_connected.id
          permiso = true
          break
        end

      end

      unless permiso
        flash[:danger] = t('security.no_access_planning_process_as_training_analyst')
        redirect_to root_path
      end

    end

  def verify_area_manager

    permiso = false

    @planning_process = PlanningProcess.find(params[:id])
    @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])
    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @planning_process_company_unit.company_unit_area_managers.each do |am|

      if am.user_id == user_connected.id && am.company_unit_area_id == @company_unit_area.id
        permiso = true
        @is_area_manager = true
        break
      end

    end

    unless permiso
      flash[:danger] = t('security.no_access_planning_process_as_area_manager')
      redirect_to root_path
    end

  end

  def verify_area_superintendent

    permiso = false

    @planning_process = PlanningProcess.find(params[:id])
    @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])
    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    @planning_process_company_unit.area_superintendents.each do |as|

      if as.user_id == user_connected.id && as.company_unit_area_id == @company_unit_area.id
        permiso = true
        @is_area_superintendent = true
        break
      end

    end

    unless permiso
      flash[:danger] = t('security.no_access_planning_process_as_area_manager')
      redirect_to root_path
    end

  end

  def verify_unit_manager

    permiso = false

    @planning_process = PlanningProcess.find(params[:id])
    @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])

    @planning_process_company_unit.company_unit_managers.each do |um|

      if um.user_id == user_connected.id
        permiso = true
        @is_unit_manager = true
        break
      end

    end

    unless permiso
      flash[:danger] = t('security.no_access_planning_process_as_area_manager')
      redirect_to root_path
    end

  end

  def verify_unit_manager_jefe_capacitacion

    permiso_unit_manager = false

    @planning_process = PlanningProcess.find(params[:id])
    @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])

    @planning_process_company_unit.company_unit_managers.each do |um|

      if um.user_id == user_connected.id
        permiso_unit_manager = true
        @is_unit_manager = true
        break
      end

    end

    permiso_jefe_capacitacion = user_connected.jefe_capacitacion



    unless permiso_jefe_capacitacion || permiso_unit_manager
      flash[:danger] = t('security.no_access_planning_process_as_area_manager')
      redirect_to root_path
    end

  end

  def verify_area_manager_superintendent_training_analyst

    @planning_process = PlanningProcess.find(params[:id])
    @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])
    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    permiso_area_manager = false

    @planning_process_company_unit.company_unit_area_managers.each do |am|

      if am.user_id == user_connected.id && am.company_unit_area_id == @company_unit_area.id
        permiso_area_manager = true
        @is_area_manager = true
        break
      end

    end

    permiso_area_superintendent = false

    @planning_process_company_unit.area_superintendents.each do |as|

      if as.user_id == user_connected.id && as.company_unit_area_id == @company_unit_area.id
        permiso_area_superintendent = true
        @is_area_superintendent = true
        break
      end

    end

    permiso_training_analyst = false

    @planning_process_company_unit.training_analysts.each do |ta|

      if ta.user_id == user_connected.id
        permiso_training_analyst = true
        @is_training_analyst = true
        break
      end

    end

    unless permiso_area_manager || permiso_area_superintendent || permiso_training_analyst
      flash[:danger] = t('security.no_access_planning_process_as_area_manager')
      redirect_to root_path
    end

  end


  def verify_any_profile

    @planning_process = PlanningProcess.find(params[:id])
    @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])
    @company_unit_area = CompanyUnitArea.find(params[:company_unit_area_id])

    permiso_unit_manager = false

    @planning_process_company_unit.company_unit_managers.each do |um|

      if um.user_id == user_connected.id
        permiso_unit_manager = true
        @is_unit_manager = true
        break
      end

    end

    permiso_jefe_capacitacion = user_connected.jefe_capacitacion

    permiso_area_manager = false

    @planning_process_company_unit.company_unit_area_managers.each do |am|

      if am.user_id == user_connected.id && am.company_unit_area_id == @company_unit_area.id
        permiso_area_manager = true
        @is_area_manager = true
        break
      end

    end

    permiso_area_superintendent = false

    @planning_process_company_unit.area_superintendents.each do |as|

      if as.user_id == user_connected.id && as.company_unit_area_id == @company_unit_area.id
        permiso_area_superintendent = true
        @is_area_superintendent = true
        break
      end

    end

    permiso_training_analyst = false

    @planning_process_company_unit.training_analysts.each do |ta|

      if ta.user_id == user_connected.id
        permiso_training_analyst = true
        @is_training_analyst = true
        break
      end

    end

    unless permiso_area_manager || permiso_area_superintendent || permiso_training_analyst || permiso_jefe_capacitacion || permiso_unit_manager
      flash[:danger] = t('security.no_access_planning_process_as_area_manager')
      redirect_to root_path
    end

  end

  def program_unit(program_anual)

    dncs = @planning_process_company_unit.dncs_area @company_unit_area

    dncs.each do |dnc|

      #dncp = Dncp.find_by_dnc_id dnc.id

      unless dnc.dncp

        dncp = Dncp.new

        dncp.dnc_id = dnc.id

        dncp.planning_process = dnc.planning_process
        dncp.planning_process_company_unit = dnc.planning_process_company_unit
        dncp.company_unit_area = dnc.company_unit_area
        dncp.oportunidad_mejora = dnc.oportunidad_mejora
        dncp.tema_capacitacion = dnc.tema_capacitacion
        dncp.objetivos_curso = dnc.objetivos_curso
        dncp.costo_directo = dnc.costo_directo
        dncp.costo_indirecto = dnc.costo_indirecto
        dncp.training_mode = dnc.training_mode
        dncp.training_type = dnc.training_type
        dncp.meses = dnc.meses
        dncp.tiempo_capacitacion = dnc.tiempo_capacitacion
        dncp.training_provider = dnc.training_provider
        dncp.impacto_total = dnc.impacto_total
        dncp.nota_minima = 0
        dncp.number_students = dnc.number_students
        dncp.eval_eficacia = dncp.eval_eficacia
        dncp.training_program_id =

        dncp.observaciones = dnc.observaciones

        dncp.save

        dnc.dnc_planning_process_goals.each do |dnc_goal|

          dncp_goal = dncp.dnc_planning_process_goal_ps.new
          dncp_goal.planning_process_goal = dnc_goal.planning_process_goal
          dncp_goal.save

        end

        dnc.dnc_training_impacts.each do |dnc_impact|

          dncp_impact = dncp.dnc_training_impact_ps.new
          dncp_impact.training_impact_category = dnc_impact.training_impact_category
          dncp_impact.training_impact_value = dnc_impact.training_impact_value
          dncp_impact.save

        end

        dnc.dnc_students.each do |dnc_student|

          dncp_student = dncp.dnc_student_ps.new
          dncp_student.user = dnc_student.user
          dncp_student.save

        end

        create_course_from_dncp program_anual, dncp, nil

      end

    end

  end

  def create_course_from_dncp(program_anual, dncp, master_poll_id)

    course = Course.new
    course.dncp_id = dncp.id

    course.codigo = @planning_process.year.to_s+' - '+dncp.id.to_s

    course.nombre = dncp.tema_capacitacion

    course.descripcion = dncp.oportunidad_mejora
    course.nombre_corto = dncp.tema_capacitacion
    course.numero_oportunidades = 1
    course.duracion = 0
    course.objetivos = dncp.objetivos_curso
    course.dedicacion_estimada = dncp.tiempo_capacitacion/60.0
    course.nota_minima = dncp.nota_minima

    course.eval_presencial = dncp.eval_presencial
    course.poll_presencial = dncp.poll_presencial
    course.asistencia_presencial = dncp.asistencia_presencial

    if course.save

      poll = course.polls.new
      poll.numero = 1
      poll.orden = 1
      poll.nombre = t('activerecord.attributes.poll.tipo_satisfaccion')
      poll.tipo_satisfaccion = true

      poll.save

      if master_poll_id

        poll.master_poll_id = master_poll_id
        poll.save

      elsif MasterPoll.find_all_by_plan_anual(true).count == 1

        poll.master_poll_id = MasterPoll.find_by_plan_anual(true).id
        poll.save

      end

      level = program_anual.levels.first

      program_course = program_anual.program_courses.build(course: course, level: level)

      program_course.save

    end

  end

  def update_course_from_dncp(dncp, master_poll_id)

    course = dncp.course

    course.nombre = dncp.tema_capacitacion

    course.descripcion = dncp.oportunidad_mejora
    course.nombre_corto = dncp.tema_capacitacion
    course.objetivos = dncp.objetivos_curso
    course.dedicacion_estimada = dncp.tiempo_capacitacion/60.0
    course.nota_minima = dncp.nota_minima

    course.eval_presencial = dncp.eval_presencial
    course.poll_presencial = dncp.poll_presencial
    course.asistencia_presencial = dncp.asistencia_presencial

    course.save

    course.units.destroy_all
    course.evaluations.destroy_all
    course.polls.destroy_all

    if master_poll_id

      poll = course.polls.find_by_tipo_satisfaccion(true)

      if poll && poll.master_poll_id != master_poll_id
        poll.master_poll_id = master_poll_id
        poll.save
      else
        poll = course.polls.new
        poll.numero = 1
        poll.orden = 1
        poll.nombre = t('activerecord.attributes.poll.tipo_satisfaccion')
        poll.tipo_satisfaccion = true
        poll.master_poll_id = master_poll_id
        poll.save
      end

    end


  end

  def xls_programming_course_evaluation_xls(course, user_courses, dncp)

    letras = %w(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI AJ AK AL AM AN AO AP AQ AR AS AT AU AV AW AX AY AZ BA BB BC BD BE BF BG BH BI BJ BK BL BM BN BO BP BQ BR BS BT BU BV BW BX BY BZ CA CB CC CD CE CF CG CH CI CJ CK CL CM CN CO CP CQ CR CS CT CU CV CW CX CY CZ)

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => 'Registro de Resultados') do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << course.nombre
        filaHeader <<  headerCenter
        cols_widths << 10

        reporte_excel.add_row fila, :style => filaHeader, height: 30
        reporte_excel.merge_cells('A1:D1')

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerTitle
        cols_widths << 10

        fila << alias_username
        filaHeader <<  headerTitle
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerTitle
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerTitle
        cols_widths << 20

        if dncp.asistencia_presencial

          fila << 'Asistencia (0/1)'
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        if dncp.eval_presencial

          fila << 'Evaluación'
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        user_courses.each_with_index do |uc, index|

          fila = Array.new
          filaHeader = Array.new

          fila << index+1
          filaHeader <<  fieldLeft

          fila << uc.enrollment.user.codigo
          filaHeader <<  fieldLeft

          fila << uc.enrollment.user.apellidos
          filaHeader <<  fieldLeft

          fila << uc.enrollment.user.nombre
          filaHeader <<  fieldLeft

          if dncp.asistencia_presencial
            if uc.asistencia
              fila << 1
            else
              fila << 0
            end
            filaHeader <<  fieldLeft
          end

          if dncp.eval_presencial
            fila << uc.nota
            filaHeader <<  fieldLeft
          end

          reporte_excel.add_row fila, :style => filaHeader, height: 20


        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def update_program_course_evaluation_results_from_excel(archivo_temporal, program_course, dncp)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 código

      if index > 1

        user = User.where('codigo = ?',fila[1]).first

        if user

          level = program_course.program.levels.first

          enrollment = Enrollment.where('program_id = ? AND user_id = ? AND level_id = ?', program_course.program.id, user.id, level.id).first

          if enrollment

            uc = program_course.user_courses.where('enrollment_id = ?', enrollment.id).first


            f = 4

            if dncp.asistencia_presencial

              if fila[f] == 1
                uc.iniciado = true
                uc.finalizado = true
                uc.asistencia = true
                uc.porcentaje_avance = 100
                uc.inicio = uc.desde if uc.inicio.nil?
                uc.fin = uc.hasta if uc.fin.nil?
              else
                uc.asistencia = false
              end

              uc.save
              f = f+1

            end

            if dncp.eval_presencial

              uc.nota = fila[f]

              uc.inicio = uc.desde if uc.inicio.nil?

              if uc.save

                if uc.course.nota_minima == 0
                  uc.aprobado = true
                elsif !uc.nota.nil?
                  uc.aprobado = true
                  uc.aprobado = false if (uc.nota < uc.course.nota_minima) || uc.nota == ''
                elsif
                  uc.aprobado = false
                end

                uc.save

              end

            end

          end

        end

      end


    end

  end

  def check_students_data_for_enroll(archivo_temporal)

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 código

      if index > 0

        user = User.where('codigo = ?',fila[0]).first

        unless user

          @lineas_error.push index+1

          begin
            @lineas_error_detalle.push (alias_username+': '+fila[0].to_s).force_encoding('UTF-8')
          rescue Exception => e
            @lineas_error_detalle.push ('exception: '+e.message)
          end

          @lineas_error_messages.push ['El alumno no existe']

        end

      end


    end

  end

  def enroll_students_dnc_from_excel(archivo_temporal, dnc)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 código

      if index > 0

        user = User.where('codigo = ?',fila[0]).first

        if user

          dnc_student = dnc.dnc_students.new
          dnc_student.user_id = user.id
          dnc_student.save

          dnc.reload

          dnc.number_students = dnc.dnc_students.size
          dnc.save

        end

      end


    end

  end

  def enroll_students_dncp_from_excel(archivo_temporal, dncp)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      #0 código

      if index > 0

        user = User.where('codigo = ?',fila[0]).first

        if user

          dncp_student = dncp.dnc_student_ps.new
          dncp_student.user_id = user.id
          dncp_student.save

          dncp.reload

          dncp.number_students = dncp.dnc_student_ps.size
          dncp.save

        end

      end


    end

  end

end
