class MasterPollsController < ApplicationController

  require 'rubyXL'

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  before_filter :check_started_poll, only: [:upload_file_form, :upload_file]

  def index
    @master_polls = MasterPoll.all
  end

  def show
    @master_poll = MasterPoll.find(params[:id])
  end

  def new
    @master_poll = MasterPoll.new
  end

  def edit
    @master_poll = MasterPoll.find(params[:id])
  end

  def upload_file_form
    @master_poll = MasterPoll.find(params[:id])
  end

  def delete
    @master_poll = MasterPoll.find(params[:id])
  end


  def create
    @master_poll = MasterPoll.new(params[:master_poll])

    if @master_poll.save
      flash[:success] = t('activerecord.success.model.master_poll.create_ok')
      redirect_to master_polls_url
    else
      render action: 'new'
    end

  end

  def update
    @master_poll = MasterPoll.find(params[:id])

    if @master_poll.update_attributes(params[:master_poll])
      flash[:success] = t('activerecord.success.model.master_poll.update_ok')
      redirect_to @master_poll

    else
      render action: 'edit'

    end

  end

  def upload_file

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        check_data archivo_temporal, @master_poll

        if @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.master_poll.update_file_error')
          render 'upload_file_form'
        else
          save_questions archivo_temporal, @master_poll

          flash[:success] = t('activerecord.success.model.master_poll.update_file_ok')

          redirect_to @master_poll
        end

      else

        flash.now[:danger] = t('activerecord.error.model.master_poll.upload_file_no_xlsx')
        render 'upload_file_form'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.master_poll.upload_file_no_file')
      render 'upload_file_form'

    end

  end

  def destroy

    @master_poll = MasterPoll.find(params[:id])

    if verify_recaptcha

      if @master_poll.destroy
        flash[:success] = t('activerecord.success.model.master_poll.delete_ok')
        redirect_to master_polls_url
      else
        flash[:danger] = t('activerecord.success.model.master_poll.delete_error')
        redirect_to delete_master_poll_path(@master_poll)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.master_poll.captcha_error')
      redirect_to delete_master_poll_path(@master_poll)

    end

  end

  private

  def save_questions(archivo_temporal, master_poll)

    alternatives = Hash.new

    letras = 'abcdefghijklmnopqrstuvwxyz'

    archivo = RubyXL::Parser.parse archivo_temporal

    master_poll.master_poll_questions.destroy_all

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        #0 grupo *
        #1 requisito *
        #2 texto
        #3 resp. unica/multiple/texto
        #4 obligatoria
        #5 alt 1 *
        #6 alt 2 *
        #7 alt 3 *
        #8 alt 4 *
        #9 altN *

        # (*) opcional

        master_poll_question = MasterPollQuestion.new

        master_poll_question.group = CGI.unescapeHTML fila[0].to_s
        master_poll_question.texto = CGI.unescapeHTML fila[2].to_s
        master_poll_question.respuesta_unica = fila[3]=='unica' ? true : false
        master_poll_question.required = (fila[4].to_s.downcase == 'obligatoria') ? true : false


        answer_type = if fila[3] == 'unica'
                        0
                      elsif fila[3] == 'multiple'
                        1
                      else
                        2
                      end

        master_poll_question.answer_type = answer_type

        master_poll_question.master_poll_id = master_poll.id

        if master_poll_question.save

          unless fila[1].blank?

            fila[1].split(',').each do |required_alternative|

              required_alternative.strip!

              if alternatives[required_alternative]

                master_poll_alternative_requirement = master_poll_question.master_poll_alternative_requirements.build
                master_poll_alternative_requirement.master_poll_alternative = alternatives[required_alternative]
                master_poll_alternative_requirement.save

              end

            end

          end

          alt_pos = 5
          numero_alt = 1

          while fila[alt_pos]

            master_poll_alternative = MasterPollAlternative.new

            master_poll_alternative.numero = numero_alt
            master_poll_alternative.texto = CGI.unescapeHTML fila[alt_pos].to_s
            master_poll_alternative.master_poll_question_id = master_poll_question.id

            alternatives[(index+1).to_s+letras[alt_pos]] = master_poll_alternative if master_poll_alternative.save

            numero_alt += 1
            alt_pos += 1

          end

        end

      end

    end



  end

  def check_data(archivo_temporal, master_poll)


    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []
    num_linea = 1

    archivo = RubyXL::Parser.parse archivo_temporal



    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0

        #0 grupo *
        #1 requisito *
        #2 texto
        #3 resp. unica/multiple/texto
        #4 obligatoria
        #5 alt 1 *
        #6 alt 2 *
        #7 alt 3 *
        #8 alt 4 *
        #9 altN *

        # (*) opcional

        master_poll_question = MasterPollQuestion.new

        master_poll_question.texto = CGI.unescapeHTML fila[2].to_s
        master_poll_question.respuesta_unica = fila[3]=='unica' ? true : false
        master_poll_question.master_poll_id = master_poll.id

        if master_poll_question.valid?

          alt_pos = 5
          numero_alt = 1

          while fila[alt_pos]

            if fila[alt_pos].to_s.strip == ''

              @lineas_error.push index+1
              @lineas_error_messages.push ['Alternativa '+numero_alt.to_s+' no puede ser vacia']

            end

            numero_alt += 1
            alt_pos += 1

          end


        else

          master_poll_question.save

          @lineas_error.push index+1
          @lineas_error_messages.push master_poll_question.errors.full_messages

        end

      end

    end



  end

  def check_started_poll

    @master_poll = MasterPoll.find(params[:id])

    if @master_poll.user_course_polls.count > 0

      flash[:danger] = t('activerecord.error.model.master_poll.poll_exist')
      redirect_to @master_poll

    end

  end

end
