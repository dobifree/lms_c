class SupportTicketTypesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @support_ticket_types = SupportTicketType.all
  end


  def show
    @support_ticket_type = SupportTicketType.find(params[:id])

  end


  def new
    @support_ticket_type = SupportTicketType.new

  end


  def edit
    @support_ticket_type = SupportTicketType.find(params[:id])
  end

  def delete
    @support_ticket_type = SupportTicketType.find(params[:id])
  end


  def create
    @support_ticket_type = SupportTicketType.new(params[:support_ticket_type])


      if @support_ticket_type.save
        flash[:success] = t('activerecord.success.model.support_ticket_type.create_ok')
        redirect_to @support_ticket_type
      else
        render action: 'new'
      end

  end


  def update
    @support_ticket_type = SupportTicketType.find(params[:id])


      if @support_ticket_type.update_attributes(params[:support_ticket_type])
        flash[:success] = t('activerecord.success.model.support_ticket_type.update_ok')
        redirect_to @support_ticket_type
      else
        render action: 'edit'
      end

  end



  def destroy

    @support_ticket_type = SupportTicketType.find(params[:id])

    if verify_recaptcha

      if @support_ticket_type.destroy
        flash[:success] = t('activerecord.success.model.support_ticket_type.delete_ok')
        redirect_to support_ticket_types_url
      else
        flash[:danger] = t('activerecord.error.model.support_ticket_type.delete_error')
        redirect_to delete_support_ticket_type_path(@support_ticket_type)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.support_ticket_type.captcha_error')
      redirect_to delete_support_ticket_type_path(@support_ticket_type)

    end

  end

end
