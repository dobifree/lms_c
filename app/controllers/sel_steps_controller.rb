class SelStepsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /sel_steps
  # GET /sel_steps.json
  def index
    @sel_steps = SelStep.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sel_steps }
    end
  end

  # GET /sel_steps/new
  # GET /sel_steps/new.json
  def new
    @sel_template = SelTemplate.find(params[:template_id])
    @sel_step = SelStep.new
    @sel_step.sel_template = @sel_template

  end

  # GET /sel_steps/1/edit
  def edit
    @sel_step = SelStep.find(params[:id])
    @sel_template = @sel_step.sel_template
  end

  # POST /sel_steps
  # POST /sel_steps.json
  def create
    @sel_step = SelStep.new(params[:sel_step])

    respond_to do |format|
      if @sel_step.save
        flash[:success] = 'El paso fue añadido correctamente'
        format.html { redirect_to sel_templates_show_open_pill_path(@sel_step.sel_template, 'steps')  }
        format.json { render json: @sel_step, status: :created, location: @sel_step }
      else
        #flash[:danger] = 'El paso necesita que se llenen todos los campos'
        #format.html { redirect_to :back }
        @sel_template = SelTemplate.find(@sel_step.sel_template_id)
        format.html { render 'new' }
        format.json { render json: @sel_step.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sel_steps/1
  # PUT /sel_steps/1.json
  def update
    @sel_step = SelStep.find(params[:id])
    @sel_template = SelTemplate.find(@sel_step.sel_template_id)

    respond_to do |format|
      if @sel_step.update_attributes(params[:sel_step])
        flash[:success] = 'El paso fue actualizado correctamente'
        format.html { redirect_to sel_templates_show_open_pill_path(@sel_step.sel_template, 'steps') }
        format.json { head :no_content }
      else
        @sel_template = SelTemplate.find(@sel_step.sel_template_id)
        format.html { render 'edit' }
        format.json { render json: @sel_step.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sel_steps/1
  # DELETE /sel_steps/1.json
  def destroy
    @sel_step = SelStep.find(params[:id])
    @sel_template = SelTemplate.find(@sel_step.sel_template_id)
    @sel_step.destroy

    respond_to do |format|
      flash[:success] = 'El paso fue eliminado correctamente'
      format.html { redirect_to sel_templates_show_open_pill_path(@sel_template, 'steps') }
      format.json { head :no_content }
    end
  end
end
