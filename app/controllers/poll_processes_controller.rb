class PollProcessesController < ApplicationController
  # GET /poll_processes
  # GET /poll_processes.json

  before_filter :authenticate_user
  before_filter :authenticate_user_admin, except: [:execute, :view_report, :index_manager, :export_report_manager]
  before_filter :validate_access_answer, only: [:execute]
  before_filter :validate_access_results, only: [:view_report]
  before_filter :validate_access_manager, only: [:index_manager, :export_report_manager]

  def index
    @poll_processes = PollProcess.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @poll_processes }
    end
  end

  def index_manager
    @poll_processes = PollProcess.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @poll_processes }
    end
  end

  # GET /poll_processes/1
  # GET /poll_processes/1.json
  def show
    @poll_process = PollProcess.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @poll_process }
    end
  end

  # GET /poll_processes/new
  # GET /poll_processes/new.json
  def new
    @poll_process = PollProcess.new
    #@characteristics = Characteristic.all
    #@characteristics_prev = {}
    #@mostrar_todos = true

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @poll_process }
    end
  end

  # GET /poll_processes/1/edit
  def edit
    @poll_process = PollProcess.find(params[:id])

    if PollProcessUser.find_all_by_poll_process_id(params[:id]).count > 0
      @is_poll_process_started = true
    else
      @is_poll_process_started = false
    end

  end


  def edit_report_chars
    @poll_process = PollProcess.find(params[:id])
    @usable_report_chars = PollCharacteristic.reporting.map { |poll_char| [poll_char.characteristic.nombre, poll_char.characteristic_id] }
  end

  # POST /poll_processes
  # POST /poll_processes.json
  def create
    @poll_process = PollProcess.new(params[:poll_process])

    respond_to do |format|
      if @poll_process.save
        save_characteristics @poll_process.id

        flash[:success] = t('activerecord.success.model.poll_process.create_ok')
        format.html { redirect_to poll_processes_url }
        format.json { render json: @poll_process, status: :created, location: @poll_process }
      else
        format.html { render action: "new" }
        format.json { render json: @poll_process.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /poll_processes/1
  # PUT /poll_processes/1.json
  def update

    @poll_process = PollProcess.find(params[:id])


    finished_original = @poll_process.finished
      if @poll_process.update_attributes(params[:poll_process])

        #save_characteristics @poll_process.id


        @poll_process2 = PollProcess.find(params[:id])
        finished_final = @poll_process2.finished
        if !finished_original && finished_final
          @poll_process.finished_at = Time.now
          @poll_process.finished_by_user_id = session[:admin_connected_id]
          @poll_process.save()
        end

        flash[:success] = t('activerecord.success.model.poll_process.update_ok')
        redirect_to poll_processes_url
      else
        if !params[:poll_process][:poll_process_report_chars_attributes]
          render action: "edit"
        else
          @usable_report_chars = PollCharacteristic.reporting.map { |poll_char| [poll_char.characteristic.nombre, poll_char.characteristic_id] }
          render action: 'edit_report_chars'
        end

    end

  end

  # DELETE /poll_processes/1
  # DELETE /poll_processes/1.json
  def destroy
    @poll_process = PollProcess.find(params[:id])
    @poll_process.destroy

    respond_to do |format|
      format.html { redirect_to poll_processes_url }
      format.json { head :no_content }
    end
  end

  def form_update_to_everyone
    @poll_process = PollProcess.find(params[:id])
    @mostrar_todos = @poll_process.to_everyone
    render partial: 'form_update_to_everyone'
  end

  def update_to_everyone
    poll_process = PollProcess.find(params[:id])
    poll_process.update_attribute(:to_everyone, params[('to_everyone_' + params[:id]).to_s.to_sym] ? true : false)

    flash[:success] = 'Los cambios fueron guardados correctamente.'
    redirect_to poll_processes_path
  end

  def form_set_chars
    @poll_process = PollProcess.find(params[:id])

    @characteristics = Characteristic.all

    @characteristics_prev = {}

    if PollProcessCharacteristic.find_all_by_poll_process_id(params[:id]).count > 0

      @characteristics.each do |characteristic|
        @characteristics_buffer = []
        PollProcessCharacteristic.where(:poll_process_id => params[:id], :characteristic_id => characteristic.id).each do |poll_process_characteristic|
          @characteristics_buffer.push(poll_process_characteristic.match_value)
        end
        if @characteristics_buffer.length > 0
          @characteristics_prev[characteristic.id] = @characteristics_buffer
        end
      end
    end

    render layout: 'flat'

  end

  def set_characteristics
    @poll_process = PollProcess.find(params[:id])
    # se actualizan las characteristics asociadas para filtrar
    characteristics = Characteristic.all
    PollProcessCharacteristic.delete_all(:poll_process_id => @poll_process.id)

    if params[:characteristic]
      characteristics.each do |characteristic|
        params[:characteristic][characteristic.id.to_s.to_sym].slice! 0
        params[:characteristic][characteristic.id.to_s.to_sym].each do |match_value|
          PollProcessCharacteristic.new(:poll_process_id => @poll_process.id, :characteristic_id => characteristic.id, :match_value => match_value).save!
        end
      end
    end

    flash[:success] = 'Las caracteristicas se actualizaron correctamente'
    redirect_to poll_processes_form_set_chars_path @poll_process.id
  end

  def form_set_users
    @poll_process = PollProcess.find(params[:id])

    @characteristics = Characteristic.all

    @characteristics_prev = {}

    @nombre = params[:user] && params[:user][:nombre] ? params[:user][:nombre] : ''
    @apellidos = params[:user] && params[:user][:apellidos] ? params[:user][:apellidos] : ''
    @codigo = params[:user] && params[:user][:codigo] ? params[:user][:codigo] : ''

    if params[:user]
      @users = search_users_with_chars(params[:user], params[:characteristic])
      @characteristics.each do |characteristic|
        if params[:characteristic][characteristic.id.to_s.to_sym] && params[:characteristic][characteristic.id.to_s.to_sym].length > 0
          @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]
        end
      end
    end

    render layout: 'flat'
  end

  def set_users
    poll_process = PollProcess.find(params[:id])

    users_to_enroll = params[:user_id]

    users_to_enroll.delete_if { |key, value| value == '0' }

    users_to_enroll.each_key do |user_id|
      enrollment = PollProcessUser.where(:user_id => user_id, :poll_process_id => poll_process.id).first
      if enrollment
        if !enrollment.enrolled?
          enrollment.update_attribute(:enrolled, true)
        end
      else
        enrollment = PollProcessUser.new()
        enrollment.user_id = user_id
        enrollment.poll_process_id = poll_process.id
        enrollment.enrolled = true
        enrollment.done = false
        enrollment.save
      end
    end

    flash[:success] = 'Los usuarios se asignaron correctamente al proceso'
    redirect_to poll_processes_form_set_users_path(poll_process.id)
  end

  def execute
    @poll_process = PollProcess.find(params[:id])
    @poll_process_users = PollProcessUser.new

    @dependencies = MasterPollAlternativeRequirement.joins(:master_poll_question).where(master_poll_questions: {:master_poll_id => @poll_process.master_poll_id})

    if PollProcessUser.where(:poll_process_id => @poll_process.id, :user_id => session[:user_connected_id], :done => true).count == 1
      @is_poll_process_user_complete = true
      @disabled_checkbox_radio = 'disabled'
    else
      @is_poll_process_user_complete = false
      @disabled_checkbox_radio = ''
    end

    @questions_triggered_by = {}
    @dependencies.each do |alternative_requirement|

      @questions_triggered_by[alternative_requirement.master_poll_question_id] = [] if !@questions_triggered_by.has_key?(alternative_requirement.master_poll_question_id)
      @questions_triggered_by[alternative_requirement.master_poll_question_id] << alternative_requirement.master_poll_alternative_id

    end

    @groups = []
    @questions = {}
    last_group = 'alsjf sajfpoiajfpoijfpoiqwjepfoiqwje porijqweporij wpqeoirjp'

    @poll_process.master_poll.master_poll_questions.each do |question|
      unless last_group == question.group
        @groups << question.group
        last_group = question.group
      end
      @questions[last_group] = [] unless @questions.has_key?(last_group)
      @questions[last_group] << question
    end

  end

  def view_report
    @poll_process = PollProcess.find(params[:id])
    @poll_process_users = PollProcessUser.where(:poll_process_id => params[:id], :done => true).sort_by &:created_at
    @poll_process_answers = PollProcessUserAnswer.find_all_by_poll_process_user_id(params[:id])
    @poll_process_users_uncompleted = PollProcessUser.where(:poll_process_id => params[:id], :done => false).count
  end

  def export_report
    @poll_process = PollProcess.find(params[:id])
    @poll_process_users = PollProcessUser.where(:poll_process_id => params[:id], :done => true).sort_by &:created_at
    @poll_process_answers = PollProcessUserAnswer.find_all_by_poll_process_user_id(params[:id])
    @poll_process_users_uncompleted = PollProcessUser.where(:poll_process_id => params[:id], :done => false).count


    @users_match = @poll_process.to_everyone ? User.find_all_by_activo(true) : User.find_by_sql('select u.*
                                                                            from users as u inner join poll_process_users as pu
                                                                            on (u.id = pu.user_id)
                                                                            where u.activo = 1
                                                                            and pu.poll_process_id = ' + @poll_process.id.to_s + '
                                                                            UNION
                                                                            select u.*
                                                                            from users as u inner join user_characteristics as uc
                                                                            on (u.id = uc.user_id) inner join poll_process_characteristics as pc
                                                                            on (uc.characteristic_id = pc.characteristic_id and uc.valor = pc.match_value) left join poll_process_users as pu
                                                                            on (u.id = pu.user_id)
                                                                            where u.activo = 1
                                                                            and pc.poll_process_id = ' + @poll_process.id.to_s)


    filename = 'resumen_respuestas_' + @poll_process.name.gsub(/\s/, '_') + '.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end
  end

  def export_report_manager
    @poll_process = PollProcess.find(params[:id])
    @poll_process_users = PollProcessUser.where(:poll_process_id => params[:id], :done => true).sort_by &:created_at
    @poll_process_answers = PollProcessUserAnswer.find_all_by_poll_process_user_id(params[:id])
    @poll_process_users_uncompleted = PollProcessUser.where(:poll_process_id => params[:id], :done => false).count


    @users_match = @poll_process.to_everyone ? User.find_all_by_activo(true) : User.find_by_sql('select u.*
                                                                            from users as u inner join poll_process_users as pu
                                                                            on (u.id = pu.user_id)
                                                                            where u.activo = 1
                                                                            and pu.poll_process_id = ' + @poll_process.id.to_s + '
                                                                            UNION
                                                                            select u.*
                                                                            from users as u inner join user_characteristics as uc
                                                                            on (u.id = uc.user_id) inner join poll_process_characteristics as pc
                                                                            on (uc.characteristic_id = pc.characteristic_id and uc.valor = pc.match_value) left join poll_process_users as pu
                                                                            on (u.id = pu.user_id)
                                                                            where u.activo = 1
                                                                            and pc.poll_process_id = ' + @poll_process.id.to_s)


    filename = 'resumen_respuestas_' + @poll_process.name.gsub(/\s/, '_') + '.xls'

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end

  private

  def validate_access_answer
    poll_process = PollProcess.find(params[:id])

    if admin_logged_in? ? true : !PollProcess.actives_for_user_to_answer(session[:user_connected_id], lms_time).include?(poll_process)
      flash[:danger] = 'No tiene permitido responder esta encuesta'
      redirect_to root_path
    end

  end

  def validate_access_results
    poll_process = PollProcess.find(params[:id])

    if admin_logged_in? ? false : !PollProcess.actives_for_user_view_results(session[:user_connected_id], lms_time).include?(poll_process)
      flash[:danger] = 'No tiene permitido ver los resultados de esta encuesta'
      redirect_to root_path
    end

  end

  def validate_access_manager
    unless is_ct_module_manager?('polls', user_connected)
      flash[:danger] = 'No tiene privilegios necesarios para ver esta opción'
      redirect_to root_path
    end
  end

  def save_characteristics(poll_process_id)
    # se actualizan las characteristics asociadas para filtrar
    characteristics = Characteristic.all
    PollProcessCharacteristic.delete_all(:poll_process_id => poll_process_id)

    if !params[:poll_process_todos]
      if params[:characteristic]
        characteristics.each do |characteristic|
          params[:characteristic][characteristic.id.to_s.to_sym].slice! 0
          params[:characteristic][characteristic.id.to_s.to_sym].each do |match_value|
            PollProcessCharacteristic.new(:poll_process_id => poll_process_id, :characteristic_id => characteristic.id, :match_value => match_value).save!
          end
        end
      end
    end
  end


end
