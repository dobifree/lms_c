class HrProcessUserCommentsController < ApplicationController
  # GET /hr_process_user_comments
  # GET /hr_process_user_comments.json
  def index
    @hr_process_user_comments = HrProcessUserComment.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @hr_process_user_comments }
    end
  end

  # GET /hr_process_user_comments/1
  # GET /hr_process_user_comments/1.json
  def show
    @hr_process_user_comment = HrProcessUserComment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erbrb
      format.json { render json: @hr_process_user_comment }
    end
  end

  # GET /hr_process_user_comments/new
  # GET /hr_process_user_comments/new.json
  def new
    @hr_process_user_comment = HrProcessUserComment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @hr_process_user_comment }
    end
  end

  # GET /hr_process_user_comments/1/edit
  def edit
    @hr_process_user_comment = HrProcessUserComment.find(params[:id])
  end

  def create

    hr_process_user_comment = HrProcessUserComment.new(params[:hr_process_user_comment])

    hr_process_user_comment.fecha = lms_time

    hr_process_user_comment.save

    render partial: 'hr_process_user_comments/index',
           locals: {
               hr_process_user: hr_process_user_comment.hr_process_user,
               hr_process_user_connected: hr_process_user_comment.hr_process_user_eval
           }

  end

  # PUT /hr_process_user_comments/1
  # PUT /hr_process_user_comments/1.json
  def update
    @hr_process_user_comment = HrProcessUserComment.find(params[:id])

    respond_to do |format|
      if @hr_process_user_comment.update_attributes(params[:hr_process_user_comment])
        format.html { redirect_to @hr_process_user_comment, notice: 'Hr process user comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @hr_process_user_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hr_process_user_comments/1
  # DELETE /hr_process_user_comments/1.json
  def destroy
    @hr_process_user_comment = HrProcessUserComment.find(params[:id])
    @hr_process_user_comment.destroy

    respond_to do |format|
      format.html { redirect_to hr_process_user_comments_url }
      format.json { head :no_content }
    end
  end
end
