class DncCategoriesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /dnc_categories
  # GET /dnc_categories.json
  def index
    @dnc_categories = DncCategory.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dnc_categories }
    end
  end

  # GET /dnc_categories/1
  # GET /dnc_categories/1.json
  def show
    @dnc_category = DncCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dnc_category }
    end
  end

  # GET /dnc_categories/new
  # GET /dnc_categories/new.json
  def new
    @dnc_category = DncCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dnc_category }
    end
  end

  # GET /dnc_categories/1/edit
  def edit
    @dnc_category = DncCategory.find(params[:id])
  end

  # POST /dnc_categories
  # POST /dnc_categories.json
  def create
    @dnc_category = DncCategory.new(params[:dnc_category])

    respond_to do |format|
      if @dnc_category.save
        format.html { redirect_to @dnc_category, notice: 'Dnc category was successfully created.' }
        format.json { render json: @dnc_category, status: :created, location: @dnc_category }
      else
        format.html { render action: "new" }
        format.json { render json: @dnc_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dnc_categories/1
  # PUT /dnc_categories/1.json
  def update
    @dnc_category = DncCategory.find(params[:id])

    respond_to do |format|
      if @dnc_category.update_attributes(params[:dnc_category])
        format.html { redirect_to @dnc_category, notice: 'Dnc category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dnc_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dnc_categories/1
  # DELETE /dnc_categories/1.json
  def destroy
    @dnc_category = DncCategory.find(params[:id])
    @dnc_category.destroy

    respond_to do |format|
      format.html { redirect_to dnc_categories_url }
      format.json { head :no_content }
    end
  end
end
