class CompanyUnitAreasController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_jefe_capacitacion


  def new

    @company_unit = CompanyUnit.find(params[:company_unit_id])
    @company_unit_area = @company_unit.company_unit_areas.new

  end

  def edit
    @company_unit_area = CompanyUnitArea.find(params[:id])
  end

  def delete
    @company_unit_area = CompanyUnitArea.find(params[:id])
  end

  def create

    @company_unit = CompanyUnit.find(params[:company_unit][:id])
    @company_unit_area = @company_unit.company_unit_areas.new(params[:company_unit_area])

    if @company_unit_area.save
      flash[:success] = t('activerecord.success.model.company_unit_area.create_ok')
      redirect_to company_unit_path @company_unit
    else
      render action: 'new'
    end

  end

  def update
    @company_unit_area = CompanyUnitArea.find(params[:id])

    if @company_unit_area.update_attributes(params[:company_unit_area])
      flash[:success] = t('activerecord.success.model.company_unit_area.update_ok')
      redirect_to @company_unit_area.company_unit
    else
        render action: 'edit'
    end
  end

  def destroy

    @company_unit_area = CompanyUnitArea.find(params[:id])
    company_unit = @company_unit_area.company_unit

    if verify_recaptcha

      if @company_unit_area.destroy
        flash[:success] = t('activerecord.success.model.company_unit_area.delete_ok')
        redirect_to company_unit
      else
        flash[:danger] = t('activerecord.success.model.company_unit_area.delete_error')
        redirect_to delete_company_unit_area_path(@company_unit_area)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.company_unit_area.captcha_error')
      redirect_to delete_company_unit_area_path(@company_unit_area)

    end

  end

  private

    def verify_jefe_capacitacion

      unless user_connected.jefe_capacitacion?
        flash[:danger] = t('security.no_access_planning_process_as_jefe_capacitacion')
        redirect_to root_path
      end

    end

end
