class TrackingFormsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @tracking_process = TrackingProcess.find(params[:tracking_process_id])
    @tracking_forms = @tracking_process.tracking_forms
    render :layout => false
  end

  # GET /tracking_forms/1
  # GET /tracking_forms/1.json
  def show
    @tracking_form = TrackingForm.find(params[:id])
    if params[:pill_open]
      @show_pill_open = params[:pill_open]
    end
  end

  # GET /tracking_forms/new
  # GET /tracking_forms/new.json
  def new
    @tracking_process = TrackingProcess.find(params[:tracking_process_id])
    @tracking_form = @tracking_process.tracking_forms.build
  end

  # GET /tracking_forms/1/edit
  def edit
    @tracking_form = TrackingForm.find(params[:id])
  end

  # POST /tracking_forms
  # POST /tracking_forms.json
  def create
    @tracking_form = TrackingForm.new(params[:tracking_form])
      if @tracking_form.save
        flash['success'] = 'Formulario creado correctamente'
        redirect_to @tracking_form
      else
        flash.now['danger'] = 'Formulario no pudo ser creado correctamente'
        render action: "new"
      end
  end

  # PUT /tracking_forms/1
  # PUT /tracking_forms/1.json
  def update
    @tracking_form = TrackingForm.find(params[:id])

      if @tracking_form.update_attributes(params[:tracking_form])
        flash['success'] = 'Formulario actualizado correctamente'
        redirect_to @tracking_form
      else
        flash['danger'] = 'Formulario no pudo ser actualizado correctamente'
        render action: "edit"
      end
  end

  # DELETE /tracking_forms/1
  # DELETE /tracking_forms/1.json
  def destroy
    @tracking_form = TrackingForm.find(params[:id])
    @tracking_form.destroy

    respond_to do |format|
      format.html { redirect_to tracking_forms_url }
      format.json { head :no_content }
    end

  end
end
