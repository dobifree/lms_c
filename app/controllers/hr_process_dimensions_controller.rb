class HrProcessDimensionsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def show
    @hr_process_dimension = HrProcessDimension.find(params[:id])
  end


  def new

    @hr_process_template = HrProcessTemplate.find(params[:hr_process_template_id])

    @hr_process_dimension = @hr_process_template.hr_process_dimensions.new


  end


  def edit

    @hr_process_dimension = HrProcessDimension.find(params[:id])

    @hr_process_template = @hr_process_dimension.hr_process_template

  end

  def create

    @hr_process_dimension = HrProcessDimension.new(params[:hr_process_dimension])
    @hr_process_template = @hr_process_dimension.hr_process_template

    if @hr_process_dimension.save
      flash[:success] = t('activerecord.success.model.hr_process_dimension.create_ok')
      redirect_to @hr_process_template
    else

      render action: 'new'
    end

  end

  def update
    @hr_process_dimension = HrProcessDimension.find(params[:id])
    @hr_process_template = @hr_process_dimension.hr_process_template

    if @hr_process_dimension.update_attributes(params[:hr_process_dimension])
      flash[:success] = t('activerecord.success.model.hr_process_dimension.update_ok')
      redirect_to @hr_process_template
    else
      render action: 'edit'
    end

  end

  def delete
    @hr_process_dimension = HrProcessDimension.find(params[:id])

    @hr_process_template = @hr_process_dimension.hr_process_template

  end


  def destroy

    @hr_process_dimension = HrProcessDimension.find(params[:id])
    @hr_process_template = @hr_process_dimension.hr_process_template

    if verify_recaptcha

      if @hr_process_dimension.destroy
        flash[:success] = t('activerecord.success.model.hr_process_dimension.delete_ok')
        redirect_to @hr_process_template
      else
        flash[:danger] = t('activerecord.success.model.hr_process_dimension.delete_error')
        redirect_to delete_hr_process_dimension_path(@hr_process_dimension)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.hr_process_dimension.captcha_error')
      redirect_to delete_hr_process_dimension_path(@hr_process_dimension)

    end

  end

end
