class HrProcessEvaluationQClassificationsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @hr_process_evaluation_q_classifications = HrProcessEvaluationQClassification.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @hr_process_evaluation_q_classifications }
    end
  end

  # GET /hr_process_evaluation_q_classifications/1
  # GET /hr_process_evaluation_q_classifications/1.json
  def show
    @hr_process_evaluation_q_classification = HrProcessEvaluationQClassification.find(params[:id])

    respond_to do |format|
      format.html # managege.html.erb
      format.json { render json: @hr_process_evaluation_q_classification }
    end
  end

  def new

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_process = @hr_process_evaluation.hr_process

    @hr_process_evaluation_q_classification = @hr_process_evaluation.hr_process_evaluation_q_classifications.new


  end

  def edit
    @hr_process_evaluation_q_classification = HrProcessEvaluationQClassification.find(params[:id])
    @hr_process_evaluation = @hr_process_evaluation_q_classification.hr_process_evaluation
    @hr_process = @hr_process_evaluation.hr_process
  end

  def create

    @hr_process_evaluation_q_classification = HrProcessEvaluationQClassification.new(params[:hr_process_evaluation_q_classification])

    if @hr_process_evaluation_q_classification.save
      flash[:success] = t('activerecord.success.model.hr_process_evaluation_q_classification.create_ok')
      redirect_to @hr_process_evaluation_q_classification.hr_process_evaluation.hr_process
    else

      @hr_process_evaluation = HrProcessEvaluation.find(@hr_process_evaluation_q_classification.hr_process_evaluation_id)
      @hr_process = @hr_process_evaluation.hr_process

      render action: 'new'
    end

  end

  def update
    @hr_process_evaluation_q_classification = HrProcessEvaluationQClassification.find(params[:id])

    if @hr_process_evaluation_q_classification.update_attributes(params[:hr_process_evaluation_q_classification])
      flash[:success] = t('activerecord.success.model.hr_process_evaluation_q_classification.update_ok')
      redirect_to @hr_process_evaluation_q_classification.hr_process_evaluation.hr_process
    else
      @hr_process_evaluation =@hr_process_evaluation_q_classification.hr_process_evaluation
      @hr_process = @hr_process_evaluation.hr_process

      render action: 'edit'
    end

  end

  # DELETE /hr_process_evaluation_q_classifications/1
  # DELETE /hr_process_evaluation_q_classifications/1.json
  def destroy
    @hr_process_evaluation_q_classification = HrProcessEvaluationQClassification.find(params[:id])
    @hr_process_evaluation_q_classification.destroy

    respond_to do |format|
      format.html { redirect_to hr_process_evaluation_q_classifications_url }
      format.json { head :no_content }
    end
  end
end
