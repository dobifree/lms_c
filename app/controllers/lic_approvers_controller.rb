class LicApproversController < ApplicationController
  include LicApproversHelper

  before_filter :authenticate_user
  before_filter :verify_access_module
  before_filter :verify_access_approver, except: [:index]
  before_filter :available_approver_to_edit, only: [:edit,
                                                    :update,
                                                    :change_status]

  before_filter :verify_approver_or_manager, only: [:index]

  def index
    @manager = !params[:as_manager].blank?
  end

  def manage
    @lic_event = LicEvent.find(params[:lic_event_id])
  end

  def edit

  end

  def update

  end

  def change_status
    request = LicRequest.find(params[:lic_request_id])
    updated_request = complete_change_status(request.id, params[:lic_request])
    updated_request.status = params[:status]

    if updated_request.valid? && (updated_request.rejected? || updated_request.approved?)
      updated_request.save
      request = deactivate_request(request.id)
      request.save

      VacsMailer.approved_lic_request(updated_request,@company).deliver if updated_request.approved?
      VacsMailer.rejected_lic_request(updated_request, @company).deliver if updated_request.rejected?

      if updated_request.rejected?
        flash[:success] = t('views.lic_approvers.flash_messages.success_rejected')
        redirect_to lic_approvers_index_path
      else
        flash[:success] = t('views.lic_approvers.flash_messages.success_approve') if updated_request.approved?
        redirect_to lic_approvers_manage_path(updated_request.lic_event)
      end
    else
      flash[:danger] = t('views.lic_approvers.flash_messages.danger_not_valid')
      redirect_to lic_approvers_manage_path(request.lic_event)
    end
  end

  def change_status_modal
    request = LicRequest.find(params[:lic_request_id])
    request.status = params[:status].to_i
    render partial: 'lic_approvers/status_description_modal',
           locals: { request: request,
                     required: request.status_require_description? }
  end

  private

  def available_approver_to_edit
    return unless defined?(params[:lic_request_id])
    request = LicRequest.find(params[:lic_request_id])
    return if request && request.available_approver_to_edit
    flash[:danger] = t('views.lic_registers.flash_messages.danger_not_available_to_register')
    redirect_to lic_approvers_index_path
  end

  def verify_access_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'licenses', true).first
    return if ct_module
    flash[:danger] = 'No dispone de privilegios necesarios para ingresar a este módulo'
    redirect_to root_path
  end

  def verify_access_approver
    approver = LicApprover.where(user_id: user_connected.id, active: true).first
    return if approver
    flash[:danger] = 'No dispone de privilegios necesarios para aprobar solicitudes'
    redirect_to root_path
  end

  def verify_approver_or_manager
    approver = LicApprover.where(user_id: user_connected.id, active: true).first
    ct_module = CtModule.where('cod = ? AND active = ?', 'licenses', true).first
    manager = ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
    return if approver || manager
    flash[:danger] = 'No dispone de privilegios necesarios para aprobar solicitudes'
    redirect_to root_path
  end
end
