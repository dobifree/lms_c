class BpItemOptionsController < ApplicationController
  before_filter :authenticate_user
  before_filter :authenticate_user_admin
  # before_filter :active_form, unless: [:editable_item]


  def editable_item_option
    render partial: 'bp_forms/item_option_row_show_form',
           locals: { option: BpItemOption.find(params[:id]),
                     editable: true}
  end

  def delete
    item = deactivate_item(params[:id])
    if item.valid?
      item.save
      render partial: 'shared/td_message',
             locals: { type_generic_message: 'alert-success',
                       generic_message: t('views.bp_events_files.flash_messages.success_deleted'),
                       colspan: 4,
                       tr: true }
    else
      render partial: 'bp_forms/item_option_row_show',
             locals: { option: BpItemOption.find(params[:id]),
                       editable: true }
    end
  end

  def non_editable
    render partial: 'bp_forms/item_option_row_show',
           locals: { option: BpItemOption.find(params[:id]),
                     editable: true }
  end

  def save
    item = complete_save_item(params[:id], params[:bp_item_option])
    if item.valid?
      item.save
      prev_item = deactivate_item(params[:id])
      prev_item.save
      render partial: 'bp_forms/item_option_row_show',
             locals: {option: item}
    else
      item_1 = BpItemOption.find(params[:id])
      item_1.assign_attributes(params[:bp_item_option])
      item_1.valid?
      render partial: 'bp_forms/item_option_row_show_form',
             locals: {option: item_1,
                      editable: true}
    end
  end

  private

  def active_form
    return unless defined?(params[:id])
    item = BpItemOption.find(params[:id])
    return unless item
    return if item.bp_form.active
    flash[:danger] = 'No tiene acceso a registro'
    redirect_to root_path
  end

  def active_item
    return unless defined?(params[:id])
    item = BpItem.find(params[:id])
    return unless item
    return if item.active
    flash[:danger] = 'No tiene acceso a registro'
    redirect_to root_path
  end

  def active_item_option
    return unless defined?(params[:id])
    item = BpItem.find(params[:id])
    return unless item
    return if item.active
    flash[:danger] = 'No tiene acceso a registro'
    redirect_to root_path
  end

  def deactivate_item(item_id)
    item = BpItemOption.find(item_id)
    item.active = false
    item.deactivated_at = lms_time
    item.deactivated_by_admin_id = admin_connected.id
    return item
  end

  def complete_save_item(item_id, params)
    prev_item = BpItemOption.find(item_id)
    item = prev_item.dup
    item.assign_attributes(params)
    item.registered_at = lms_time
    item.registered_by_admin_id = admin_connected.id
    item.prev_item_option_id = prev_item.id
    return item
  end
end
