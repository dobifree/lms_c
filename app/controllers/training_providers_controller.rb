class TrainingProvidersController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_jefe_capacitacion_or_admin, only: [:index, :show, :new, :edit, :create, :update, :delete, :destroy]


  def index
    @training_providers = TrainingProvider.all
  end


  def show
    @training_provider = TrainingProvider.find(params[:id])
  end


  def new
    @training_provider = TrainingProvider.new
    @training_providers = TrainingProvider.all
  end


  def edit
    @training_provider = TrainingProvider.find(params[:id])
    @training_providers = TrainingProvider.all
  end

  def delete
    @training_provider = TrainingProvider.find(params[:id])
  end


  def create

    params[:training_provider][:nombre] = params[:training_provider][:nombre].strip

    @training_provider = TrainingProvider.new(params[:training_provider])


      if @training_provider.save
        flash[:success] = t('activerecord.success.model.training_provider.create_ok')
        redirect_to training_providers_path
      else
        @training_providers = TrainingProvider.all
        render action: 'new'
      end

  end


  def update

    params[:training_provider][:nombre] = params[:training_provider][:nombre].strip

    @training_provider = TrainingProvider.find(params[:id])


      if @training_provider.update_attributes(params[:training_provider])
        flash[:success] = t('activerecord.success.model.training_provider.update_ok')
        redirect_to training_providers_path
      else
        @training_providers = TrainingProvider.all
        render action: 'edit'
      end

  end



  def destroy

    @training_provider = TrainingProvider.find(params[:id])

    if @training_provider.dncs.count > 0 || @training_provider.dncps.count > 0

      flash[:danger] = t('activerecord.error.model.training_provider.dncs_error')
      redirect_to delete_training_provider_path(@training_provider)

    else

      #if verify_recaptcha

        if @training_provider.destroy
          flash[:success] = t('activerecord.success.model.training_provider.delete_ok')
          redirect_to training_providers_path
        else
          flash[:danger] = t('activerecord.success.model.training_provider.delete_error')
          redirect_to delete_training_provider_path(@training_provider)
        end
=begin
      else

        flash.delete(:recaptcha_error)

        flash[:danger] = t('activerecord.error.model.training_provider.captcha_error')
        redirect_to delete_training_provider_path(@training_provider)

      end
=end

    end

  end

  private

    def verify_training_analyst_or_jefe_capacitacion_or_admin

      unless user_connected.jefe_capacitacion? || user_connected.training_analysts.count > 0 || admin_logged_in?
        flash[:danger] = t('security.no_access_planning_process_as_jefe_capacitacion')

        redirect_to root_path
      end

    end

    def verify_jefe_capacitacion_or_admin

      unless admin_logged_in? || user_connected.jefe_capacitacion?
        flash[:danger] = t('security.no_access_planning_process_as_jefe_capacitacion')

        redirect_to root_path
      end

    end

    def verify_jefe_capacitacion

      unless user_connected.jefe_capacitacion?
        flash[:danger] = t('security.no_access_user_information')
        redirect_to root_path
      end

    end

end
