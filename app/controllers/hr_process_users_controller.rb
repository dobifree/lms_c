class HrProcessUsersController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @hr_process = HrProcess.find(params[:hr_process_id])
    #@hr_process_users = @hr_process.hr_process_users
    @hr_process_users = @hr_process.hr_process_users_order_by_apellidos_nombre

  end

  def carga_participantes

    @hr_process = HrProcess.find(params[:hr_process_id])

    if params[:upload]

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      if params[:reemplazar_todo][:yes] == '1'

        @hr_process.hr_process_users.each do |hr_process_user|

          if hr_process_user.hr_process_assessment_ds.size == 0 &&
             hr_process_user.hr_process_assessment_es.size == 0 &&
             hr_process_user.hr_process_assessment_eval_es.size == 0 &&
             hr_process_user.hr_process_assessment_efs.size == 0 &&
             hr_process_user.hr_process_assessment_qs.size == 0 &&
             hr_process_user.hr_process_evaluation_manual_qs.size == 0

             hr_process_user.destroy

          else

            @lineas_error.push -1
            @lineas_error_detalle.push ('codigo: '+hr_process_user.user.codigo)
            @lineas_error_messages.push ['No se pudo eliminar el participante porque ya ha sido evaluado o ha evaluado a otro participante']

          end

        end

      end

      text = params[:upload]['datafile'].read
      text.gsub!(/\r\n?/, "\n")

      num_linea = 1

      text.each_line do |line|

        if line.strip != ''

          datos = line.split(';')

            #0 usuario
            #1 nivel
            #{tipo_rel:usuario,tipo_rel:usuario,...}

            user = User.where('codigo = ? AND activo = ?', datos[0], true).first

            if user

              datos[1].delete!("\n")
              datos[1].delete!("\r")

              level = @hr_process.hr_process_levels.find_by_nombre(datos[1])

              if level

                hr_process_user = nil

                #hr_process_user = @hr_process.hr_process_users.joins(:user).where('codigo = ?', user.codigo).first

                @hr_process.hr_process_users.each do |pu|

                  if pu.user.codigo == user.codigo

                    hr_process_user = pu
                    break

                  end

                end

                unless hr_process_user

                  hr_process_user = @hr_process.hr_process_users.build
                  hr_process_user.user = user

                end

                hr_process_user.hr_process_level = level

                if hr_process_user.save

                  #hr_process_user.hr_process_es_evaluado_rels.destroy_all

                  if datos[2]

                    datos[2].delete!("\n")
                    datos[2].delete!("\r")

                    datos[2] = datos[2].strip.slice 1..-2

                    relaciones = datos[2].split(',')

                    relaciones.each do |relacion|

                      relacion = relacion.split(':')

                      hr_p_user1 = @hr_process.hr_process_users.joins(:user).where('users.codigo = ?', relacion[1]).first

                      unless hr_p_user1

                        hr_p_user1 = @hr_process.hr_process_users.build

                        user = User.where('codigo = ? AND activo = ?', relacion[1], true).first

                        if user

                          hr_p_user1.user = user
                          hr_p_user1.save

                        else

                          hr_p_user1 = nil

                          @lineas_error.push num_linea
                          @lineas_error_detalle.push ('rel: '+relacion[0]+', codigo: '+relacion[1]).force_encoding('UTF-8')
                          @lineas_error_messages.push ['El usuario no existe, no está activo o es un administrador']

                        end

                      end

                      hr_p_user2 = hr_process_user

                      if hr_p_user1

                        if relacion[0] == 'jefe' || relacion[0] == 'sub' || relacion[0] == 'par' || relacion[0] == 'resp' || relacion[0] == 'cli' || relacion[0] == 'prov'

                          if hr_p_user1.id == hr_p_user2.id

                            @lineas_error.push num_linea
                            @lineas_error_detalle.push ('rel: '+relacion[0]+', codigo: '+relacion[1]).force_encoding('UTF-8')
                            @lineas_error_messages.push ['Son la misma persona']

                          else

                            hr_process_user_rel = hr_p_user2.hr_process_es_evaluado_rels.where('hr_process_user1_id = ? and tipo = ?', hr_p_user1.id, relacion[0]).first

                            unless hr_process_user_rel

                              hr_process_user_rel = @hr_process.hr_process_user_rels.new
                              hr_process_user_rel.hr_process_user1_id = hr_p_user1.id
                              hr_process_user_rel.hr_process_user2_id = hr_p_user2.id
                              hr_process_user_rel.tipo = relacion[0]

                            end

                            unless hr_process_user_rel.save

                              @lineas_error.push num_linea
                              @lineas_error_detalle.push ('rel: '+relacion[0]+', codigo: '+relacion[1]).force_encoding('UTF-8')
                              @lineas_error_messages.push hr_process_user_rel.errors.full_messages

                            end

                          end

                        else

                          @lineas_error.push num_linea
                          @lineas_error_detalle.push ('rel: '+relacion[0]+', codigo: '+relacion[1]).force_encoding('UTF-8')
                          @lineas_error_messages.push ['La relación no es válida']

                        end

                      else

                        unless hr_p_user1

                          @lineas_error.push num_linea
                          @lineas_error_detalle.push ('codigo: '+relacion[1]).force_encoding('UTF-8')
                          @lineas_error_messages.push ['El usuario '+relacion[1]+' no es participante del proceso']

                        end

                      end


                    end

                  end

                else

                  @lineas_error.push num_linea
                  @lineas_error_detalle.push ('codigo: '+datos[0]+'; nivel: '+datos[1]).force_encoding('UTF-8')
                  @lineas_error_messages.push hr_process_user.errors.full_messages

                end

              else

                @lineas_error.push num_linea
                @lineas_error_detalle.push ('codigo: '+datos[0]+'; nivel: '+datos[1]).force_encoding('UTF-8')
                @lineas_error_messages.push ['No existe el nivel']

              end

            else

              @lineas_error.push num_linea
              @lineas_error_detalle.push ('codigo: '+datos[0]+'; nivel: '+datos[1]).force_encoding('UTF-8')
              @lineas_error_messages.push ['El usuario no existe, no está activo o es un administrador']


            end

        end

        num_linea += 1

      end

      flash.now[:success] = t('activerecord.success.model.hr_process_user.carga_masiva_ok')
      flash.now[:danger] = t('activerecord.error.model.hr_process_user.carga_masiva_error') if @lineas_error.length > 0

    else
      flash.now[:danger] = t('activerecord.error.model.user.carga_masiva_no_file')

    end

    @hr_process_users = @hr_process.hr_process_users_order_by_apellidos_nombre
    render 'index'

  end

  # GET /hr_process_users/1
  # GET /hr_process_users/1.json
  def show
    @hr_process_user = HrProcessUser.find(params[:id])

    respond_to do |format|
      format.html # managege.html.erb
      format.json { render json: @hr_process_user }
    end
  end

  # GET /hr_process_users/new
  # GET /hr_process_users/new.json
  def new
    @hr_process_user = HrProcessUser.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @hr_process_user }
    end
  end

  # GET /hr_process_users/1/edit
  def edit
    @hr_process_user = HrProcessUser.find(params[:id])
  end

  # POST /hr_process_users
  # POST /hr_process_users.json
  def create
    @hr_process_user = HrProcessUser.new(params[:hr_process_user])

    respond_to do |format|
      if @hr_process_user.save
        format.html { redirect_to @hr_process_user, notice: 'Hr process user was successfully created.' }
        format.json { render json: @hr_process_user, status: :created, location: @hr_process_user }
      else
        format.html { render action: "new" }
        format.json { render json: @hr_process_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /hr_process_users/1
  # PUT /hr_process_users/1.json
  def update
    @hr_process_user = HrProcessUser.find(params[:id])

    respond_to do |format|
      if @hr_process_user.update_attributes(params[:hr_process_user])
        format.html { redirect_to @hr_process_user, notice: 'Hr process user was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @hr_process_user.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy

    hr_process_user = HrProcessUser.find(params[:id])

    hr_process = hr_process_user.hr_process

    hr_process_user.hr_process_assessment_ds.destroy_all
    hr_process_user.hr_process_assessment_es.destroy_all
    hr_process_user.hr_process_assessment_eval_es.destroy_all
    hr_process_user.hr_process_assessment_efs.destroy_all
    hr_process_user.hr_process_assessment_qs.destroy_all

    hr_process_user.hr_process_es_evaluado_rels.destroy_all

    hr_process_user.destroy if hr_process_user.hr_process_evalua_rels.size == 0

    flash[:success] = t('activerecord.success.model.hr_process_user.delete_ok')



    redirect_to  hr_process_users_path(hr_process)

  end

end
