class PeQuestionActivityFieldListItemsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /pe_question_activity_field_list_items/1
  # GET /pe_question_activity_field_list_items/1.json
  def show
    @pe_question_activity_field_list_item = PeQuestionActivityFieldListItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_question_activity_field_list_item }
    end
  end

  def new

    @pe_question_activity_field_list = PeQuestionActivityFieldList.find(params[:pe_question_activity_field_list_id])

    @pe_evaluation = @pe_question_activity_field_list.pe_evaluation

    @pe_process = @pe_evaluation.pe_process

    @pe_question_activity_field_list_item = @pe_question_activity_field_list.pe_question_activity_field_list_items.build

  end

  def create

    @pe_question_activity_field_list_item = PeQuestionActivityFieldListItem.new(params[:pe_question_activity_field_list_item])

    if @pe_question_activity_field_list_item.save
      redirect_to @pe_question_activity_field_list_item.pe_question_activity_field_list
    else
      @pe_question_activity_field_list = @pe_question_activity_field_list_item.pe_question_activity_field_list
      @pe_evaluation = @pe_question_activity_field_list.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'new'
    end

  end


  def edit
    @pe_question_activity_field_list_item = PeQuestionActivityFieldListItem.find(params[:id])

    @pe_question_activity_field_list = @pe_question_activity_field_list_item.pe_question_activity_field_list
    @pe_evaluation = @pe_question_activity_field_list.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def update

    @pe_question_activity_field_list_item = PeQuestionActivityFieldListItem.find(params[:id])

    if @pe_question_activity_field_list_item.update_attributes(params[:pe_question_activity_field_list_item])
      redirect_to @pe_question_activity_field_list_item.pe_question_activity_field_list
    else
      @pe_question_activity_field_list = @pe_question_activity_field_list_item.pe_question_activity_field_list
      @pe_evaluation = @pe_question_activity_field_list.pe_evaluation
      @pe_process = @pe_evaluation.pe_process
      render action: 'edit'
    end

  end

  # DELETE /pe_question_activity_field_list_items/1
  # DELETE /pe_question_activity_field_list_items/1.json
  def destroy
    @pe_question_activity_field_list_item = PeQuestionActivityFieldListItem.find(params[:id])
    @pe_question_activity_field_list_item.destroy

    respond_to do |format|
      format.html { redirect_to pe_question_activity_field_list_items_url }
      format.json { head :no_content }
    end
  end
end
