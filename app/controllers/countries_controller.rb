class CountriesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @countries = Country.all
  end

  def show
    @country = Country.find(params[:id])
  end

  def new
    @country = Country.new
  end

  def edit
    @country = Country.find(params[:id])
  end

  def delete
    @country = Country.find(params[:id])
  end

  def create
    @country = Country.new(params[:country])


      if @country.save
        flash[:success] = t('activerecord.success.model.country.create_ok')
        redirect_to countries_path
      else
        render action: 'new'
      end

  end

  def update
    @country = Country.find(params[:id])


      if @country.update_attributes(params[:country])
        flash[:success] = t('activerecord.success.model.country.update_ok')
        redirect_to countries_path
      else
        render action: 'edit'
      end

  end



  def destroy

    @country = Country.find(params[:id])

    if verify_recaptcha

      if @country.destroy
        flash[:success] = t('activerecord.success.model.country.delete_ok')
        redirect_to countries_path
      else
        flash[:danger] = t('activerecord.success.model.country.delete_error')
        redirect_to delete_country_path(@country)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.country.captcha_error')
      redirect_to delete_country_path(@country)

    end

  end

end
