class JmCandidatesController < ApplicationController
  layout 'job_market'

  before_filter :candidate_authenticate, except: [:new, :create]

  # GET /jm_candidates/1
  # GET /jm_candidates/1.json
  def show
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
  end

  # GET /jm_candidates/new
  # GET /jm_candidates/new.json
  def new
    @jm_candidate = JmCandidate.new
    @jm_candidate.email = params[:email] if params[:email]
  end

  # GET /jm_candidates/1/edit
  def edit
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
  end

  def edit_email
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
    @jm_candidate.email_to_switch = nil
  end

  def update_email_to_switch
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
    if @jm_candidate.authenticate(params[:jm_candidate][:password])
      if JmCandidate.where(email: params[:jm_candidate][:email_to_switch]).count.zero?
        @jm_candidate.email_to_switch = params[:jm_candidate][:email_to_switch]
        @jm_candidate.codigo_rec_pass = SecureRandom.hex(3).to_s.upcase
        @jm_candidate.fecha_rec_pass = lms_time
        @jm_candidate.save

        notification_template = SelNotificationTemplate.where(event_id: 15, active: true).first
        if notification_template
          notification = notification_template.generate_notification({jm_candidate: @jm_candidate}, nil, lms_time)
          SelMailer.sel_notification(notification, @company).deliver
        else
          LmsMailer.jm_switch_email(@jm_candidate, @company).deliver
        end
        flash[:success] = 'Se ha enviado un código de validación al email que ingresó para finalizar el proceso.'
        redirect_to jm_candidate_show_validate_email_to_switch_path
      else
        @jm_candidate.email_to_switch = params[:jm_candidate][:email_to_switch]
        @jm_candidate.errors.add :email_to_switch, :taken
        render 'edit_email'
      end
    else
      @jm_candidate.errors.add :password
      render 'edit_email'
    end
  end

  def show_validate_email_to_switch
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
    unless @jm_candidate.email_to_switch.present?
      flash[:danger] = 'No existe ningún proceso de cambio de email abierto.'
      redirect_to edit_jm_candidate_path
    end
  end

  def update_email
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
    if @jm_candidate.email_to_switch.present? && @jm_candidate.codigo_rec_pass == params[:jm_candidate][:codigo_rec_pass]
      @jm_candidate.email = @jm_candidate.email_to_switch
      @jm_candidate.email_to_switch = nil
      @jm_candidate.codigo_rec_pass = nil
      @jm_candidate.fecha_rec_pass = nil
      @jm_candidate.save
      flash[:success] = 'El email fue actualizado correctamente'
      redirect_to jm_my_profile_path
    elsif @jm_candidate.codigo_rec_pass == params[:jm_candidate][:codigo_rec_pass]
      @jm_candidate.errors.add :codigo_rec_pass, :invalid
      render 'show_validate_email_to_switch'
    else
      flash[:danger] = 'No existe ningún proceso de cambio de email abierto.'
      redirect_to edit_jm_candidate_path
    end
  end

  # POST /jm_candidates
  # POST /jm_candidates.json
  def create
    @jm_candidate = JmCandidate.new(params[:jm_candidate])
    ## generación de contraseña críptica y del código de validación de email
    @jm_candidate.password = SecureRandom.hex(6).to_s
    @jm_candidate.codigo_rec_pass = SecureRandom.hex(3).to_s.upcase
    @jm_candidate.fecha_rec_pass = lms_time

    if !@jm_candidate.email.blank? && @jm_candidate.save

      if (params[:photo]) && save_photo(params[:photo])
        photo = @jm_candidate.build_jm_candidate_photo
        photo.assign_attributes(:crypted_name => params[:photo].original_filename, :file_extension => File.extname(params[:photo].original_filename))
        photo.save
      end

      flash[:success] = t('rec_pass.jm_paso2_ok')
      notification_template = SelNotificationTemplate.where(event_id: 1, active: true).first
      if notification_template
        notification = notification_template.generate_notification({jm_candidate: @jm_candidate}, nil, lms_time)
        SelMailer.sel_notification(notification, @company).deliver
      else
        LmsMailer.jm_recuperar_pass(@jm_candidate, @company).deliver
      end
      redirect_to jm_recuperar_pass_paso3_path(@jm_candidate)
    else
      @jm_candidate.valid?
      @jm_candidate.errors.add(:email, 'no puede ser vacío') if @jm_candidate.email.blank?
      flash.now[:danger] = 'El registro no se realizó corectamente'
      render action: "new"
    end
  end

  # PUT /jm_candidates/1
  # PUT /jm_candidates/1.json
  def update
    @jm_candidate = JmCandidate.find(session[:candidate_connected_id])
    if (params[:photo]) && save_photo(params[:photo])
      @jm_candidate.build_jm_candidate_photo.assign_attributes(:crypted_name => params[:photo].original_filename, :file_extension => File.extname(params[:photo].original_filename))
    end

    if @jm_candidate.update_attributes(params[:jm_candidate])
      flash[:success] = t('activerecord.success.model.jm_candidate.update_ok')
      redirect_to jm_my_profile_path
    else
      render action: "edit"
    end
  end


  private

  def save_photo(file)
    directory = @company.directorio+'/'+ @company.codigo+'/jm_candidates/' + @jm_candidate.id.to_s + '/profile_photo/'
    file_crypted_name = file.original_filename

    FileUtils.mkdir_p directory unless File.directory? directory
    File.open(directory + file_crypted_name, 'wb') { |f| f.write(file.read) }
  end

  def generate_validation_email_code(candidate)
    candidate.codigo_rec_pass = SecureRandom.hex(3).to_s.upcase
    candidate.fecha_rec_pass = lms_time
    candidate.save
    flash[:success] = t('rec_pass.paso2_ok')

    notification_template = SelNotificationTemplate.where(event_id: 15, active: true).first
    if notification_template
      notification = notification_template.generate_notification({jm_candidate: candidate}, nil, lms_time)
      SelMailer.sel_notification(notification, @company).deliver
    else
      LmsMailer.jm_recuperar_pass(candidate, @company).deliver
    end
    redirect_to jm_recuperar_pass_paso3_path(candidate)
  end

  def candidate_authenticate
    if session[:candidate_connected_id]
      true
    else
      flash[:danger] = 'no se puede postular sin iniciar sesión'
      redirect_to list_jm_offers_path
    end
  end

end
