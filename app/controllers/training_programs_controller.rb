class TrainingProgramsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_jefe_capacitacion_or_admin, only: [:index, :show, :new, :edit, :create, :update, :delete, :destroy]

  def index
    @training_programs = TrainingProgram.all
  end


  def show
    @training_program = TrainingProgram.find(params[:id])
  end


  def new
    @training_program = TrainingProgram.new
    @training_programs = TrainingProgram.all
  end


  def edit
    @training_program = TrainingProgram.find(params[:id])
    @training_programs = TrainingProgram.all
  end

  def delete
    @training_program = TrainingProgram.find(params[:id])
  end


  def create

    params[:training_program][:nombre] = params[:training_program][:nombre].strip

    @training_program = TrainingProgram.new(params[:training_program])


    if @training_program.save
      flash[:success] = t('activerecord.success.model.training_program.create_ok')
      redirect_to training_programs_path
    else
      @training_programs = TrainingProgram.all
      render action: 'new'
    end

  end


  def update

    params[:training_program][:nombre] = params[:training_program][:nombre].strip

    @training_program = TrainingProgram.find(params[:id])


    if @training_program.update_attributes(params[:training_program])
      flash[:success] = t('activerecord.success.model.training_program.update_ok')
      redirect_to training_programs_path
    else
      @training_programs = TrainingProgram.all
      render action: 'edit'
    end

  end



  def destroy

    @training_program = TrainingProgram.find(params[:id])

    if @training_program.dncs.count > 0 || @training_program.dncps.count > 0

      flash[:danger] = t('activerecord.error.model.training_program.dncs_error')
      redirect_to delete_training_program_path(@training_program)

    else

      #if verify_recaptcha

        if @training_program.destroy
          flash[:success] = t('activerecord.success.model.training_program.delete_ok')
          redirect_to training_programs_path
        else
          flash[:danger] = t('activerecord.success.model.training_program.delete_error')
          redirect_to delete_training_program_path(@training_program)
        end
=begin
      else

        flash.delete(:recaptcha_error)

        flash[:danger] = t('activerecord.error.model.training_program.captcha_error')
        redirect_to delete_training_program_path(@training_program)

      end
=end
    end

  end

  private

  def verify_training_analyst_or_jefe_capacitacion_or_admin

    unless user_connected.jefe_capacitacion? || user_connected.training_analysts.count > 0 || admin_logged_in?
      flash[:danger] = t('security.no_access_planning_process_as_jefe_capacitacion')

      redirect_to root_path
    end

  end

  def verify_jefe_capacitacion_or_admin

    unless user_connected.jefe_capacitacion? || admin_logged_in?
      flash[:danger] = t('security.no_access_planning_process_as_jefe_capacitacion')

      redirect_to root_path
    end

  end

  def verify_jefe_capacitacion

    unless user_connected.jefe_capacitacion?
      flash[:danger] = t('security.no_access_user_information')
      redirect_to root_path
    end

  end

end
