class TrackingFormGroupsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @tracking_form = TrackingForm.find(params[:tracking_form_id])
    @tracking_form_groups = @tracking_form.tracking_form_groups

    render :layout => false
  end

  # GET /tracking_form_groups/1
  # GET /tracking_form_groups/1.json
  def show
    @tracking_form_group = TrackingFormGroup.find(params[:id])
  end

  # GET /tracking_form_groups/new
  # GET /tracking_form_groups/new.json
  def new
    @tracking_form = TrackingForm.find(params[:tracking_form_id])
    @tracking_form_group = @tracking_form.tracking_form_groups.build
    @tracking_form_group.tracking_form_questions.build.tracking_form_items.build
  end

  # GET /tracking_form_groups/1/edit
  def edit
    @tracking_form_group = TrackingFormGroup.find(params[:id])
    @tracking_form_group.tracking_form_questions.build.tracking_form_items.build unless @tracking_form_group.tracking_form_questions.count > 0
  end

  # POST /tracking_form_groups
  # POST /tracking_form_groups.json
  def create
    @tracking_form_group = TrackingFormGroup.new(params[:tracking_form_group])

      if @tracking_form_group.save
        flash['success'] = 'Grupo de preguntas creado correctamente'
        redirect_to tracking_form_pill_open_path(@tracking_form_group.tracking_form, 'questions')
      else
        flash.now['danger'] = 'Grupo de preguntas no puedo ser creado correctamente'
        render action: "new"
      end
  end

  # PUT /tracking_form_groups/1
  # PUT /tracking_form_groups/1.json
  def update
    @tracking_form_group = TrackingFormGroup.find(params[:id])

      if @tracking_form_group.update_attributes(params[:tracking_form_group])
        flash['success'] = 'Grupo de preguntas actualizado correctamente'
        redirect_to tracking_form_pill_open_path(@tracking_form_group.tracking_form, 'questions')
      else
        flash.now['danger'] = 'Grupo de preguntas no puedo ser actualizado correctamente'
        render action: "edit"
      end
  end

  # DELETE /tracking_form_groups/1
  # DELETE /tracking_form_groups/1.json
  def destroy
    @tracking_form_group = TrackingFormGroup.find(params[:id])
    @tracking_form_group.destroy

    respond_to do |format|
      format.html { redirect_to tracking_form_groups_url }
      format.json { head :no_content }
    end
  end
end
