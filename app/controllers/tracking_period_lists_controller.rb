class TrackingPeriodListsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /tracking_period_lists
  # GET /tracking_period_lists.json
  def index
    @tracking_period_lists = TrackingPeriodList.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tracking_period_lists }
    end
  end

  # GET /tracking_period_lists/1
  # GET /tracking_period_lists/1.json
  def show
    @tracking_period_list = TrackingPeriodList.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tracking_period_list }
    end
  end

  # GET /tracking_period_lists/new
  # GET /tracking_period_lists/new.json
  def new
    @tracking_period_list = TrackingPeriodList.new

    3.times do
      @tracking_period_list.tracking_period_items.build
    end


    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tracking_period_list }
    end
  end

  # GET /tracking_period_lists/1/edit
  def edit
    @tracking_period_list = TrackingPeriodList.find(params[:id])
  end

  # POST /tracking_period_lists
  # POST /tracking_period_lists.json
  def create
    @tracking_period_list = TrackingPeriodList.new(params[:tracking_period_list])


      if @tracking_period_list.save
        flash[:success] = t('activerecord.success.model.tracking_period_list.create_ok')
        redirect_to tracking_period_lists_path
      else
        render 'new'
      end

  end

  # PUT /tracking_period_lists/1
  # PUT /tracking_period_lists/1.json
  def update
    @tracking_period_list = TrackingPeriodList.find(params[:id])

      if @tracking_period_list.update_attributes(params[:tracking_period_list])
        flash[:success] = t('activerecord.success.model.tracking_period_list.update_ok')
        redirect_to tracking_period_lists_path
      else
        render 'edit'
      end

  end

  # DELETE /tracking_period_lists/1
  # DELETE /tracking_period_lists/1.json
  def destroy
    @tracking_period_list = TrackingPeriodList.find(params[:id])
    @tracking_period_list.destroy

    respond_to do |format|
      format.html { redirect_to tracking_period_lists_url }
      format.json { head :no_content }
    end
  end
end
