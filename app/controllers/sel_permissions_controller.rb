class SelPermissionsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authorized_to_manage_processes

  # GET /sel_permissions
  # GET /sel_permissions.json
  def index
    @user_ct_module_privileges = UserCtModulePrivileges.joins(:user).where('sel_make_requirement = 1 OR sel_approve_requirements = 1').reorder('apellidos, nombre')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sel_permissions }
      format.xlsx { headers['Content-Disposition'] = 'attachment; filename=permisos_seleccion.xlsx'}
    end
  end

  # GET /sel_permissions/1
  # GET /sel_permissions/1.json
  def show
    @sel_permission = SelPermission.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sel_permission }
    end
  end

  # GET /sel_permissions/new
  # GET /sel_permissions/new.json
  def new
    @users = User.find_by_sql('select distinct users.*
                              from users left join user_ct_module_privileges
                              ON (users.id = user_ct_module_privileges.user_id)
                              WHERE users.activo = 1
                              AND (user_ct_module_privileges.sel_make_requirement = 0
                                OR isnull(user_ct_module_privileges.sel_make_requirement))
                              AND (user_ct_module_privileges.sel_approve_requirements = 0
                                OR isnull(user_ct_module_privileges.sel_approve_requirements))
                              order by apellidos, nombre').
        map { |user| [user.codigo + ' - ' + user.comma_full_name, user.id] }

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sel_permission }
    end
  end

  # GET /sel_permissions/1/edit
  def edit
    @user_ct_module_privilege = UserCtModulePrivileges.find(params[:id])
  end

  # POST /sel_permissions
  # POST /sel_permissions.json
  def create
    #@sel_permission = SelPermission.new(params[:sel_permission])

    permission = UserCtModulePrivileges.where(:user_id => params[:sel_permission][:user_id]).first_or_initialize

    permission.sel_make_requirement = params[:sel_permission][:sel_make_requirement]
    permission.sel_approve_requirements = params[:sel_permission][:sel_approve_requirements]

    respond_to do |format|
      if permission.save
        flash[:success] = 'Permisos registrados correctamente'
        format.html { redirect_to sel_permissions_path }
        format.json { render json: @sel_permission, status: :created, location: @sel_permission }
      else
        format.html { render action: "new" }
        format.json { render json: @sel_permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sel_permissions/1
  # PUT /sel_permissions/1.json
  def update
    @user_ct_module_privilege = UserCtModulePrivileges.find(params[:id])

    if params[:sel_permission] && params[:sel_permission][:sel_make_requirement]
      @user_ct_module_privilege.sel_make_requirement = true
    else
      @user_ct_module_privilege.sel_make_requirement = false
    end

    if params[:sel_permission] && params[:sel_permission][:sel_approve_requirements]
      @user_ct_module_privilege.sel_approve_requirements = true
    else
      @user_ct_module_privilege.sel_approve_requirements = false
    end


    respond_to do |format|
      if @user_ct_module_privilege.save
        flash[:success] = 'Permisos registrados correctamente'
        format.html { redirect_to sel_permissions_path}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sel_permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sel_permissions/1
  # DELETE /sel_permissions/1.json
  def destroy
    @sel_permission = SelPermission.find(params[:id])
    @sel_permission.destroy

    respond_to do |format|
      format.html { redirect_to sel_permissions_url }
      format.json { head :no_content }
    end
  end

  def massive_update
  end

  def massive_upload
    if params[:permissions] && File.extname(params[:permissions].original_filename) == '.xlsx'




      directorio_temporal = '/tmp/'
      nombre_temporal = SecureRandom.hex(5) + '.xlsx'
      archivo_temporal = directorio_temporal + nombre_temporal
      path = File.join(directorio_temporal, nombre_temporal)
      File.open(path, 'wb') { |f| f.write(params[:permissions].read) }

      file = RubyXL::Parser.parse archivo_temporal
      data = file.worksheets[0].extract_data


      data.each_with_index do |row, index|
        user_cod = User.where(codigo: row[0].to_s).first
        next unless user_cod
        can_make_requirements = row[2]
        can_approve_requirements = row[3]
        privilege = UserCtModulePrivileges.where(user_id: user_cod).first_or_initialize
        privilege.sel_make_requirement = can_make_requirements
        privilege.sel_approve_requirements = can_approve_requirements
        privilege.save
      end

      flash[:success] = 'Carga masiva realizada correctamente'
      redirect_to sel_permissions_path
    else
      flash.now[:danger]='Debe cargar un archivo (.xlsx) con el formato descargado.'
      render action: 'massive_update'
    end
  end
end
