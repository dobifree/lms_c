class PollCharacteristicsController < ApplicationController
  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @characteristics = Characteristic.not_grouped_characteristics
    @characteristic_types = CharacteristicType.all
  end

  def edit
    @poll_characteristic = PollCharacteristic.find(params[:id])
    @characteristic = @poll_characteristic.characteristic
  end

  def update
    @poll_characteristic = PollCharacteristic.find(params[:id])
    redirect_to poll_characteristics_index_path if @poll_characteristic.update_attributes(params[:poll_characteristic])
  end
end