class KpiIndicatorRolsController < ApplicationController

  include UsersHelper

  before_filter :authenticate_user
  before_filter :verify_access_manage_dashboard_1, only:[:new]
  before_filter :verify_access_manage_dashboard_2, only:[:create]
  before_filter :verify_access_manage_dashboard_3, only:[:destroy]

  def new

    @kpi_indicator = KpiIndicator.find(params[:kpi_indicator_id])
    @kpi_dashboard = @kpi_indicator.kpi_set.kpi_dashboard
    @user_search = User.new(params[:user])

    @users_search = search_normal_users(@user_search.apellidos, @user_search.nombre)

  end

  def create

    kpi_indicator = KpiIndicator.find(params[:kpi_indicator_id])
    kpi_indicator_rol = kpi_indicator.kpi_indicator_rols.build

    kpi_indicator_rol.user_id = params[:user_id]
    kpi_indicator_rol.kpi_rol_id = params[:kpi_indicator_rol][:kpi_rol_id]

    kpi_indicator_rol.save

    flash[:success] =  t('activerecord.success.model.kpi_indicator_rol.create_ok')
    redirect_to new_kpi_indicator_rol_path kpi_indicator

  end

  def destroy

    kpi_indicator_rol = KpiIndicatorRol.find(params[:id])
    kpi_indicator = kpi_indicator_rol.kpi_indicator
    kpi_indicator_rol.destroy

    flash[:success] =  t('activerecord.success.model.kpi_indicator_rol.delete_ok')
    redirect_to new_kpi_indicator_rol_path kpi_indicator

  end

  private

  def verify_access_manage_dashboard_1

    @kpi_indicator = KpiIndicator.find(params[:kpi_indicator_id])

    unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',@kpi_indicator.kpi_set.kpi_dashboard_id).count > 0
      flash[:danger] = t('security.no_access_manage_kpi_dashboard')
      redirect_to root_path
    end
  end

  def verify_access_manage_dashboard_2

    kpi_indicator = KpiIndicator.find(params[:kpi_indicator_rol][:kpi_indicator_id])

    unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?', kpi_indicator.kpi_set.kpi_dashboard_id).count > 0
      flash[:danger] = t('security.no_access_manage_kpi_dashboard')
      redirect_to root_path
    end
  end

  def verify_access_manage_dashboard_3

    @kpi_indicator_rol = KpiIndicatorRol.find(params[:id])

    unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?', @kpi_indicator_rol.kpi_indicator.kpi_set.kpi_dashboard_id).count > 0
      flash[:danger] = t('security.no_access_manage_kpi_dashboard')
      redirect_to root_path
    end
  end

end
