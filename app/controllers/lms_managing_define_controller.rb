class LmsManagingDefineController < ApplicationController

  include LmsManagingDefineModule

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:show_program, :edit_program, :update_program, :add_study_plan, :create_study_plan]
  before_filter :get_data_2, only: [:edit_course, :update_course, :edit_course_image, :update_course_image, :new_unit, :create_unit, :new_evaluation, :create_evaluation, :new_poll, :create_poll]
  before_filter :get_data_3, only: [:edit_unit, :update_unit]
  before_filter :get_data_4, only: [:edit_evaluation, :update_evaluation]
  before_filter :get_data_5, only: [:edit_poll, :update_poll]

  before_filter :verify_access_manage_module

  before_filter :verify_access_to_manipulate_program, only: [:show_program, :edit_program, :update_program, :add_study_plan, :create_study_plan]
  before_filter :verify_access_to_manipulate_course, only: [:edit_course, :update_course, :edit_course_image, :update_course_image,
                                                            :new_unit, :create_unit, :edit_unit, :update_unit,
                                                            :new_evaluation, :create_evaluation, :edit_evaluation, :update_evaluation,
                                                            :new_poll, :create_poll, :edit_poll, :update_poll]

=begin
  , only: [:define,
                                                     :massive_charge_programs, :massive_charge_programs_download_format,
                                                     :massive_charge_courses, :massive_charge_courses_download_format,
                                                     :show_program, :new_program, :create_program, :edit_program, :update_program,
                                                     :show_course, :new_course, :create_course, :edit_course, :update_course,
                                                     :edit_course_image, :update_course_image,
                                                     :new_unit, :create_unit, :edit_unit, :update_unit,
                                                     :new_evaluation, :create_evaluation, :edit_evaluation, :update_evaluation,
                                                     :new_poll, :create_poll, :edit_poll, :update_poll,
                                                     :show_study_plan]
=end

  def define

    @programs = Program.where('managed_by_manager = 1').reorder('managed_by_manager DESC, activo DESC, orden ASC, nombre ASC')
    @courses = Course.where('managed_by_manager = 1').reorder('managed_by_manager DESC, nombre ASC')

   #@company = session[:company]

  end

  def massive_charge_programs

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        massive_charge_programs_from_excel archivo_temporal

        if @lineas_error.size == 0
          flash[:success] = t('activerecord.success.model.lms_managing.update_file_ok')
          redirect_to lms_managing_define_massive_charge_programs_path
        else
          flash.now[:success] = t('activerecord.success.model.lms_managing.update_file_ok_with_errors')
        end

      else

        flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_file')
    end

  end

  def massive_charge_programs_download_format

    reporte_excel = massive_charge_programs_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_carga_programas.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end


  def massive_charge_courses

    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []

   #@company = session[:company]

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        massive_charge_courses_from_excel archivo_temporal

        if @lineas_error.size == 0
          flash[:success] = t('activerecord.success.model.lms_managing.update_file_ok')
          redirect_to lms_managing_define_massive_charge_courses_path
        else
          flash.now[:success] = t('activerecord.success.model.lms_managing.update_file_ok_with_errors')
        end

      else

        flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_xlsx')

      end

    elsif request.post?

      flash.now[:danger] = t('activerecord.error.model.lms_managing.upload_file_no_file')
    end

  end

  def massive_charge_courses_download_format

    reporte_excel = massive_charge_courses_excel_format

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Formato_carga_cursos.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def show_program

  end

  def new_program

    @program = Program.new

  end

  def create_program

    @program = Program.new(params[:program])
    @program.managed_by_manager = true

    if @program.save
      flash[:success] = t('activerecord.success.model.program.create_ok')
      redirect_to lms_managing_define_path
    else
      render action: 'new_program'
    end

  end

  def edit_program

  end

  def update_program

    if @program.update_attributes(params[:program])
      flash[:success] = t('activerecord.success.model.program.update_ok')
      redirect_to lms_managing_define_path
    else
      render action: 'edit_program'
    end

  end

  def show_course

    @course = Course.find(params[:course_id])
    order_course_elements @course
    @course_file = CourseFile.new
   #@company = session[:company]

  end

  def new_course

    @course = Course.new
   #@company = session[:company]

    unless @company.alias_unit.blank?
      u = @company.alias_unit.split(',')
      @course.unit_alias = u[0].downcase.strip unless u[0].blank?
    end

  end

  def create_course

    @course = Course.new(params[:course])

    @course.codigo = (Course.all.size+1).to_s if @course.codigo.blank?

    @course.managed_by_manager = true

    @course.nombre = @course.nombre.mb_chars.upcase if @company.upper_course_name

    if @course.save

      TrainingCharacteristic.for_course.each do |training_characteristic|

        ctc = @course.course_training_characteristic(training_characteristic)

        unless ctc

          ctc = @course.course_training_characteristics.build
          ctc.training_characteristic = training_characteristic

        end

        if training_characteristic.data_type_id == 0
          ctc.value_string = params['training_characteristic_'+training_characteristic.id.to_s]
        elsif training_characteristic.data_type_id == 1
          ctc.value_int = params['training_characteristic_'+training_characteristic.id.to_s]
        elsif training_characteristic.data_type_id == 2
          ctc.training_characteristic_value_id = params['training_characteristic_'+training_characteristic.id.to_s]
        end

        ctc.save

      end

      program = Program.find params[:program_id]

      if program

        level = program.levels.first

        unless level
          level = program.levels.build
          level.nombre 'Único'
          level.orden = 1
          level = nil unless level.save
        end

        if level

          pc = @course.program_courses.build
          pc.program = program
          pc.level = level
          pc.save

        end

      end

      flash[:success] = t('activerecord.success.model.course.create_ok')
      redirect_to lms_managing_define_show_course_path @course
    else
     #@company = session[:company]
      render action: 'new_course'
    end

  end

  def edit_course

   #@company = session[:company]

  end

  def update_course

    if @course.update_attributes(params[:course])

      @course.nombre = @course.nombre.mb_chars.upcase if @company.upper_course_name
      @course.save

      TrainingCharacteristic.for_course.each do |training_characteristic|

        ctc = @course.course_training_characteristic(training_characteristic)

        unless ctc

          ctc = @course.course_training_characteristics.build
          ctc.training_characteristic = training_characteristic

        end

        if training_characteristic.data_type_id == 0
          ctc.value_string = params['training_characteristic_'+training_characteristic.id.to_s]
        elsif training_characteristic.data_type_id == 1
          ctc.value_int = params['training_characteristic_'+training_characteristic.id.to_s]
        elsif training_characteristic.data_type_id == 2
          ctc.training_characteristic_value_id = params['training_characteristic_'+training_characteristic.id.to_s]
        end

        ctc.save

      end



      flash[:success] = t('activerecord.success.model.course.update_ok')
      redirect_to lms_managing_define_show_course_path @course
    else
     #@company = session[:company]
      render action: 'edit_course'
    end

  end

  def edit_course_image

   #@company = session[:company]

  end

  def update_course_image

   company = @company

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext.downcase != '.jpg' && ext.downcase != '.jpeg'

        flash[:danger] = t('activerecord.error.model.course.update_imagen_wrong_format')
        redirect_to lms_managing_define_edit_course_image_path @course

      elsif File.size(params[:upload]['datafile'].tempfile) > 100.kilobytes

        flash[:danger] = t('activerecord.error.model.course.update_imagen_wrong_wrong_size')
        redirect_to lms_managing_define_edit_course_image_path @course

      else

        save_imagen params[:upload], @course, company

        flash[:success] = t('activerecord.success.model.course.update_imagen_ok')

        redirect_to lms_managing_define_edit_course_image_path @course

      end



    else

      flash[:danger] = t('activerecord.error.model.course.update_imagen_empty_file')

      redirect_to lms_managing_define_edit_course_image_path @course

    end

  end

  def new_unit

    @unit = @course.units.build
   #@company = session[:company]

  end

  def create_unit

    @unit = @course.units.build(params[:unit])

    if @unit.save
      flash[:success] = t('activerecord.success.model.unit.create_ok')
      redirect_to lms_managing_define_show_course_path @course
    else
      render action: 'new_unit'
    end

  end

  def edit_unit

   #@company = session[:company]

  end

  def update_unit

    if @unit.update_attributes(params[:unit])
      flash[:success] = t('activerecord.success.model.unit.update_ok')
      redirect_to lms_managing_define_show_course_path @unit.course
    else
      render action: 'edit_unit'
    end
  end

  def new_evaluation

    @evaluation = @course.evaluations.build
   #@company = session[:company]

  end

  def create_evaluation

    @evaluation = @course.evaluations.build(params[:evaluation])

    if @evaluation.save
      flash[:success] = t('activerecord.success.model.evaluation.create_ok')
      redirect_to lms_managing_define_show_course_path @course
    else
      render action: 'new_evaluation'
    end

  end

  def edit_evaluation

   #@company = session[:company]

  end

  def update_evaluation

    if @evaluation.update_attributes(params[:evaluation])
      flash[:success] = t('activerecord.success.model.evaluation.update_ok')
      redirect_to lms_managing_define_show_course_path @evaluation.course
    else
      render action: 'edit_evaluation'
    end
  end

  def new_poll

    @poll = @course.polls.build
   #@company = session[:company]

  end

  def create_poll

    @poll = @course.polls.build(params[:poll])

    if @poll.save
      flash[:success] = t('activerecord.success.model.poll.create_ok')
      redirect_to lms_managing_define_show_course_path @course
    else
      render action: 'new_poll'
    end

  end

  def edit_poll
   #@company = session[:company]
  end

  def update_poll

    if @poll.update_attributes(params[:poll])
      flash[:success] = t('activerecord.success.model.poll.update_ok')
      redirect_to lms_managing_define_show_course_path @poll.course
    else
      render action: 'edit_poll'
    end
  end

  def show_study_plan

    @program = Program.find(params[:program_id])

    @agregarcurso = false;

    if @program.courses.count < Course.all.count && @program.levels.count > 0
      @agregarcurso = true
    end

  end

  def add_study_plan

    @courses = Course.active_courses_managed_by_manager

  end

  def create_study_plan

    course = Course.find(params[:course_id])

    level = @program.levels.first

    unless level
      level = @program.levels.build
      level.nombre 'Único'
      level.orden = 1
      level = nil unless level.save
    end

    if level

      program_course = @program.program_course(course,level)

      unless program_course

        program_course = @program.program_courses.build

        program_course.course = course
        program_course.level = level

        if program_course.save

        end

      end

    end

    flash[:success] = t('activerecord.success.model.program_course.create_ok')

    redirect_to lms_managing_define_show_study_plan_path @program


  end

  private

  def get_data_1
    @program = Program.find(params[:program_id])
  end

  def get_data_2
    @course = Course.find(params[:course_id])
  end

  def get_data_3
    @unit = Unit.find(params[:unit_id])
    @course = @unit.course
  end

  def get_data_4
    @evaluation = Evaluation.find(params[:evaluation_id])
    @course = @evaluation.course
  end

  def get_data_5
    @poll = Poll.find(params[:poll_id])
    @course = @poll.course
  end
  
  def verify_access_manage_module

    ct_module = CtModule.where('cod = ? AND active = ?', 'lms', true).first

    unless (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1) || user_connected.lms_define
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def verify_access_to_manipulate_program

    unless @program.managed_by_manager?
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def verify_access_to_manipulate_course

    unless @course.managed_by_manager?
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end

  end

  def save_imagen(upload, course, company)

    ext = File.extname(upload['datafile'].original_filename)

    if ext == '.jpg' || ext == '.jpeg'

      if File.size(upload['datafile'].tempfile) <= 100.kilobytes

        directory = company.directorio+'/'+company.codigo+'/imagenes_cursos/'

        FileUtils.mkdir_p directory unless File.directory? directory

        if course.imagen
          file = directory+course.imagen
          File.delete(file) if File.exist? file
        end

        course.set_imagen
        course.save

        file = directory+course.imagen

        File.open(file, 'wb') { |f| f.write(upload['datafile'].read) }

      end

    end

  end

end
