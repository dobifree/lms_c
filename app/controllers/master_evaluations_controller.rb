class MasterEvaluationsController < ApplicationController

  require 'rubyXL'

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  before_filter :check_started_evaluation, only: [:upload_file_form, :upload_file]

  def index
    @master_evaluations = MasterEvaluation.all
  end

  def show
    @master_evaluation = MasterEvaluation.find(params[:id])
  end

  def new
    @master_evaluation = MasterEvaluation.new
  end

  def edit
    @master_evaluation = MasterEvaluation.find(params[:id])
  end

  def upload_file_form
    @master_evaluation = MasterEvaluation.find(params[:id])
  end

  def delete
    @master_evaluation = MasterEvaluation.find(params[:id])
  end

  def create
    @master_evaluation = MasterEvaluation.new(params[:master_evaluation])

    if @master_evaluation.save
      flash[:success] = t('activerecord.success.model.master_evaluation.create_ok')
      redirect_to @master_evaluation
    else
      render action: 'new'
    end

  end


  def update
    @master_evaluation = MasterEvaluation.find(params[:id])


    if @master_evaluation.update_attributes(params[:master_evaluation])
      flash[:success] = t('activerecord.success.model.master_evaluation.update_ok')
      redirect_to @master_evaluation

    else
      render action: 'edit'

    end

  end

  def edit_question

    @master_evaluation = MasterEvaluation.find(params[:id])
    @master_evaluation_question = MasterEvaluationQuestion.find(params[:master_evaluation_question_id])


  end

  def update_question

    @master_evaluation = MasterEvaluation.find(params[:id])
    @master_evaluation_question = MasterEvaluationQuestion.find(params[:master_evaluation_question_id])

    params[:master_evaluation_question][:tiempo] = nil if params[:master_evaluation_question][:tiempo] == ''

    if @master_evaluation_question.update_attributes(params[:master_evaluation_question])

      @master_evaluation_question.master_evaluation_alternatives.each do |alt|

        alt.texto = params["master_evaluation_alternative_text_#{alt.id}".to_sym]
        alt.correcta = false
        alt.save

      end


      @master_evaluation_question.master_evaluation_alternatives.each do |alt|

        if @master_evaluation_question.respuesta_unica

          if params[:master_evaluation_alternative_right] && alt.id.to_s == params[:master_evaluation_alternative_right]

            alt.correcta = true
            alt.save

          end

        else

          if params["master_evaluation_alternative_right_#{alt.id}".to_sym] && params["master_evaluation_alternative_right_#{alt.id}".to_sym] == '1'

            alt.correcta = true
            alt.save

          end

        end

      end

      if @master_evaluation_question.respuesta_unica

        @master_evaluation_question.master_evaluation_alternatives.each do |alt|

          @master_evaluation_question.user_course_evaluation_questions.where('alternativas = ?',alt.id).each do |user_course_evaluation_question|

            user_course_evaluation_question.alternativas = params["change_answer_to_#{alt.id}"]

            if params["mark_as_#{alt.id}"] == '1'
              user_course_evaluation_question.correcta = true
            elsif params["mark_as_#{alt.id}"] == '0'
              user_course_evaluation_question.correcta = false
            end

            user_course_evaluation_question.save

            user_course_evaluation = user_course_evaluation_question.user_course_evaluation

            total_pregs = user_course_evaluation.user_course_evaluation_questions.count

            total_pregs_resps = user_course_evaluation.user_course_evaluation_questions.find_all_by_respondida(true).count

            if total_pregs == total_pregs_resps

              user_course_evaluation.finalizada = true

              puntaje = 0

              user_course_evaluation.user_course_evaluation_questions.find_all_by_correcta(true).each do |q|

                puntaje += q.master_evaluation_question.puntaje

              end

              user_course_evaluation.nota = puntaje + user_course_evaluation.evaluation.nota_base

              if user_course_evaluation.user_course.course.calculate_percentage

                puntaje_total = 0

                user_course_evaluation.user_course_evaluation_questions.each do |q|

                  puntaje_total += q.master_evaluation_question.puntaje

                end

                user_course_evaluation.nota = user_course_evaluation.nota*100/puntaje_total.to_f

              end

              if user_course_evaluation.nota >= user_course_evaluation.evaluation.course.nota_minima
                user_course_evaluation.aprobada = true
              else
                user_course_evaluation.aprobada = false
              end

              user_course_evaluation.save

            end

            user_course = user_course_evaluation.user_course

            units = user_course.course.units
            evaluations = user_course.course.evaluations
            valid_evaluations = user_course.course.evaluations.where('weight > 0')
            polls = user_course.course.polls.find_all_by_obligatoria(true)

            todo_terminado = true

            units.each do |unit|
              uc_unit = user_course.user_course_units.find_by_unit_id(unit.id)
              todo_terminado = false unless uc_unit && uc_unit.finalizada
              break unless todo_terminado
            end

            if todo_terminado
              evaluations.each do |evaluation|
                uc_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(evaluation.id)
                todo_terminado = false unless uc_evaluation && uc_evaluation.finalizada
                break unless todo_terminado
              end

            end

            if todo_terminado
              polls.each do |poll|
                uc_poll = user_course.user_course_polls.find_by_poll_id(poll.id)
                todo_terminado = false unless uc_poll && uc_poll.finalizada
                break unless todo_terminado
              end

            end

            if todo_terminado

              nota = nil

              if valid_evaluations.length > 0
                suma = 0
                suma_pesos = 0
                valid_evaluations.each do |evaluation|
                  uc_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(evaluation.id)
                  suma += uc_evaluation.nota*evaluation.weight
                  suma_pesos += evaluation.weight
                end
                nota = suma*1.0/suma_pesos*1.0
              end

              user_course.finalizado = true
              user_course.nota = nota

              user_course.aprobado = true

              if nota && nota < user_course.course.nota_minima
                user_course.aprobado = false
              end

              if user_course.course.dncp

                user_course.asistencia = true unless user_course.course.dncp.asistencia_presencial

              else
                user_course.asistencia = true
              end

              user_course.save

            end

          end

        end

      end

      flash[:success] = t('activerecord.success.model.master_evaluation.update_question_ok')
      redirect_to @master_evaluation

    else

      render 'edit_question'

    end


  end

  def upload_file

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        if @master_evaluation.has_topics?

          check_data_with_topics archivo_temporal, @master_evaluation

        else

          check_data archivo_temporal, @master_evaluation

        end

        if @lineas_error.length > 0
          flash.now[:danger] = t('activerecord.error.model.master_evaluation.update_file_error')
          render 'upload_file_form'
        else

          if @master_evaluation.has_topics?

            save_questions_with_topics archivo_temporal, @master_evaluation

          else

            save_questions archivo_temporal, @master_evaluation

          end


          if @lineas_error.length > 0
            flash.now[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

            render 'upload_file_form'


          else
            flash[:success] = t('activerecord.success.model.master_evaluation.update_file_ok')

            redirect_to @master_evaluation

          end



        end

      else

        flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_xlsx')
        render 'upload_file_form'

      end

    else
      flash.now[:danger] = t('activerecord.error.model.master_evaluation.upload_file_no_file')
      render 'upload_file_form'

    end

  end

  def destroy

    @master_evaluation = MasterEvaluation.find(params[:id])

    if verify_recaptcha

      if @master_evaluation.destroy
        flash[:success] = t('activerecord.success.model.master_evaluation.delete_ok')
        redirect_to master_evaluations_url
      else
        flash[:danger] = t('activerecord.success.model.master_poll.delete_error')
        redirect_to delete_master_evaluation_path(@master_evaluation)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.master_evaluation.captcha_error')
      redirect_to delete_master_evaluation_path(@master_evaluation)

    end

  end

  private

    def save_questions(archivo_temporal, master_evaluation)

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []

      archivo = RubyXL::Parser.parse archivo_temporal

      if archivo.worksheets[0].sheet_name == 'preguntas'

        master_evaluation.master_evaluation_questions.destroy_all

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0

            #0 texto
            #1 obligatoria/opcional
            #2 puntaje
            #3 tiempo
            #4 resp. unica/multiple
            #5 respuesta
            #6 alt 1
            #7 alt 2
            #8 alt 3
            #9 alt 4
            #10 altN

            master_evaluation_question = MasterEvaluationQuestion.new

            master_evaluation_question.texto = CGI.unescapeHTML fila[0].to_s
            master_evaluation_question.obligatoria = fila[1]=='obligatoria' ? true : false
            master_evaluation_question.puntaje = fila[2]
            master_evaluation_question.tiempo = fila[3]
            master_evaluation_question.respuesta_unica = fila[4]=='unica' ? true : false
            master_evaluation_question.master_evaluation_topic_id = nil
            master_evaluation_question.master_evaluation_id = master_evaluation.id

            if master_evaluation_question.save

              alt_pos = 6
              numero_alt = 1

              resps = fila[5].to_s.split('-')

              while fila[alt_pos]

                master_evaluation_alternative = MasterEvaluationAlternative.new

                master_evaluation_alternative.numero = numero_alt
                master_evaluation_alternative.texto = CGI.unescapeHTML fila[alt_pos].to_s

                correcta = false

                resps.each do |resp|
                  if resp.strip.to_i == numero_alt
                    correcta = true
                    break
                  end
                end

                master_evaluation_alternative.correcta = correcta

                master_evaluation_alternative.master_evaluation_question_id = master_evaluation_question.id

                master_evaluation_alternative.save

                numero_alt += 1
                alt_pos += 1

              end

            else

              @lineas_error.push index+1
              @lineas_error_messages.push master_evaluation_question.errors.full_messages


            end

          end

        end

      end


    end


    def check_data(archivo_temporal, master_evaluation)


      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      archivo = RubyXL::Parser.parse archivo_temporal

      if archivo.worksheets[0].sheet_name == 'preguntas'

        master_evaluation.master_evaluation_questions.destroy_all

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0

            #0 texto
            #1 obligatoria/opcional
            #2 puntaje
            #3 tiempo
            #4 resp. unica/multiple
            #5 respuesta
            #6 alt 1
            #7 alt 2
            #8 alt 3
            #9 alt 4
            #10 altN


            #@lineas_error.push index+1

            master_evaluation_question = MasterEvaluationQuestion.new

            master_evaluation_question.texto = CGI.unescapeHTML fila[0].to_s
            master_evaluation_question.obligatoria = fila[1]=='obligatoria' ? true : false
            master_evaluation_question.puntaje = fila[2]
            master_evaluation_question.tiempo = fila[3]
            master_evaluation_question.respuesta_unica = fila[4]=='unica' ? true : false
            master_evaluation_question.master_evaluation_topic_id = nil
            master_evaluation_question.master_evaluation_id = master_evaluation.id

            if master_evaluation_question.valid?

              alt_pos = 6
              numero_alt = 1

              resps = fila[5].to_s.split('-')

              while fila[alt_pos]

                if fila[alt_pos].to_s.strip == ''

                  @lineas_error.push index+1
                  @lineas_error_messages.push ['Alternativa '+numero_alt.to_s+' no puede ser vacia']

                end

                numero_alt += 1
                alt_pos += 1

              end

              numero_alt -= 1

              if resps.length == 0

                @lineas_error.push index+1
                @lineas_error_messages.push ['No hay alternativa correcta']

              else

                resps.each do |resp|

                  begin
                    resp = Integer(resp.strip)
                  rescue

                  end

                  if resp.is_a?(Numeric)

                    if resp< 1 || resp > numero_alt

                      @lineas_error.push index+1
                      @lineas_error_messages.push ['Alternativa fuera de rango, no existe '+resp.to_s+', total: '+numero_alt.to_s]

                    end

                  else

                    @lineas_error.push index+1
                    @lineas_error_messages.push ['Alternativa '+resp.to_s+' incorrecta']

                  end


                end

              end





            else

              master_evaluation_question.save

              @lineas_error.push index+1
              @lineas_error_messages.push master_evaluation_question.errors.full_messages

            end




          end

        end

      else

        @lineas_error.push 'hoja'
        @lineas_error_messages.push ['Nombre de hoja incorrecto']


      end



    end

    def check_data_with_topics(archivo_temporal, master_evaluation)

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []
      num_linea = 1

      archivo = RubyXL::Parser.parse archivo_temporal

      if archivo.worksheets[0].sheet_name == 'preguntas'

        master_evaluation.master_evaluation_questions.destroy_all

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0

            #0 tema
            #1 texto
            #2 obligatoria/opcional
            #3 puntaje
            #4 tiempo
            #5 resp. unica/multiple
            #6 respuesta
            #7 alt 1
            #8 alt 2
            #9 alt 3
            #10 alt 4
            #11 altN


            #@lineas_error.push index+1

            master_evaluation_question = MasterEvaluationQuestion.new

            master_evaluation_question.texto = CGI.unescapeHTML fila[1].to_s
            master_evaluation_question.obligatoria = fila[2]=='obligatoria' ? true : false
            master_evaluation_question.puntaje = fila[3]
            master_evaluation_question.tiempo = fila[4]
            master_evaluation_question.respuesta_unica = fila[5]=='unica' ? true : false
            master_evaluation_question.master_evaluation_topic_id = nil
            master_evaluation_question.master_evaluation_id = master_evaluation.id

            if master_evaluation_question.valid?

              alt_pos = 7
              numero_alt = 1

              resps = fila[6].to_s.split('-')

              while fila[alt_pos]

                if fila[alt_pos].to_s.strip == ''

                  @lineas_error.push index+1
                  @lineas_error_messages.push ['Alternativa '+numero_alt.to_s+' no puede ser vacia']

                end

                numero_alt += 1
                alt_pos += 1

              end

              numero_alt -= 1

              if resps.length == 0

                @lineas_error.push index+1
                @lineas_error_messages.push ['No hay alternativa correcta']

              else

                resps.each do |resp|

                  begin
                    resp = Integer(resp.strip)
                  rescue

                  end

                  if resp.is_a?(Numeric)

                    if resp< 1 || resp > numero_alt

                      @lineas_error.push index+1
                      @lineas_error_messages.push ['Alternativa fuera de rango, no existe '+resp.to_s+', total: '+numero_alt.to_s]

                    end

                  else

                    @lineas_error.push index+1
                    @lineas_error_messages.push ['Alternativa '+resp.to_s+' incorrecta']
                  end


                end

              end





            else

              master_evaluation_question.save

              @lineas_error.push index+1
              @lineas_error_messages.push master_evaluation_question.errors.full_messages

            end




          end

        end

      else

        @lineas_error.push 'hoja'
        @lineas_error_messages.push ['Nombre de hoja incorrecto']


      end



    end

    def save_questions_with_topics(archivo_temporal, master_evaluation)

      @lineas_error = []
      @lineas_error_detalle = []
      @lineas_error_messages = []


      num_topic = 0

      archivo = RubyXL::Parser.parse archivo_temporal

      if archivo.worksheets[0].sheet_name == 'preguntas'

        master_evaluation.master_evaluation_topics.destroy_all
        master_evaluation.master_evaluation_questions.destroy_all

        data = archivo.worksheets[0].extract_data

        data.each_with_index do |fila, index|

          if index > 0

            #0 tema
            #1 texto
            #2 obligatoria/opcional
            #3 puntaje
            #4 tiempo
            #5 resp. unica/multiple
            #6 respuesta
            #7 alt 1
            #8 alt 2
            #9 alt 3
            #10 alt 4
            #11 altN

            master_evaluation_topic = master_evaluation.master_evaluation_topics.where('nombre = ?', CGI.unescapeHTML(fila[0].to_s)).first

            unless master_evaluation_topic

              master_evaluation_topic = master_evaluation.master_evaluation_topics.build

              master_evaluation_topic.nombre = CGI.unescapeHTML fila[0].to_s
              master_evaluation_topic.numero = num_topic + 1
              master_evaluation_topic.save

            end

            num_topic = master_evaluation_topic.numero


            master_evaluation_question = MasterEvaluationQuestion.new

            master_evaluation_question.texto = CGI.unescapeHTML fila[1].to_s
            master_evaluation_question.obligatoria = fila[2]=='obligatoria' ? true : false
            master_evaluation_question.puntaje = fila[3]
            master_evaluation_question.tiempo = fila[4]
            master_evaluation_question.respuesta_unica = fila[5]=='unica' ? true : false
            master_evaluation_question.master_evaluation_topic_id = master_evaluation_topic.id
            master_evaluation_question.master_evaluation_id = master_evaluation.id

            if master_evaluation_question.save

              alt_pos = 7
              numero_alt = 1

              resps = fila[6].to_s.split('-')

              while fila[alt_pos]

                master_evaluation_alternative = MasterEvaluationAlternative.new

                master_evaluation_alternative.numero = numero_alt
                master_evaluation_alternative.texto = CGI.unescapeHTML fila[alt_pos].to_s

                correcta = false

                resps.each do |resp|
                  if resp.strip.to_i == numero_alt
                    correcta = true
                    break
                  end
                end

                master_evaluation_alternative.correcta = correcta

                master_evaluation_alternative.master_evaluation_question_id = master_evaluation_question.id

                master_evaluation_alternative.save

                numero_alt += 1
                alt_pos += 1

              end

            else

              @lineas_error.push index+1
              @lineas_error_messages.push master_evaluation_question.errors.full_messages




            end

          end

        end

      end


    end

    def check_started_evaluation

      @master_evaluation = MasterEvaluation.find(params[:id])

      if @master_evaluation.user_course_evaluations.count > 0

        flash[:danger] = t('activerecord.error.model.master_evaluation.evaluation_exist')
        redirect_to @master_evaluation

      end

    end

end
