class PeMemberRelsController < ApplicationController
  # GET /pe_member_rels
  # GET /pe_member_rels.json
  def index
    @pe_member_rels = PeMemberRel.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pe_member_rels }
    end
  end

  # GET /pe_member_rels/1
  # GET /pe_member_rels/1.json
  def show
    @pe_member_rel = PeMemberRel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pe_member_rel }
    end
  end

  # GET /pe_member_rels/new
  # GET /pe_member_rels/new.json
  def new
    @pe_member_rel = PeMemberRel.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pe_member_rel }
    end
  end

  # GET /pe_member_rels/1/edit
  def edit
    @pe_member_rel = PeMemberRel.find(params[:id])
  end

  # POST /pe_member_rels
  # POST /pe_member_rels.json
  def create
    @pe_member_rel = PeMemberRel.new(params[:pe_member_rel])

    respond_to do |format|
      if @pe_member_rel.save
        format.html { redirect_to @pe_member_rel, notice: 'Pe member rel was successfully created.' }
        format.json { render json: @pe_member_rel, status: :created, location: @pe_member_rel }
      else
        format.html { render action: "new" }
        format.json { render json: @pe_member_rel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pe_member_rels/1
  # PUT /pe_member_rels/1.json
  def update
    @pe_member_rel = PeMemberRel.find(params[:id])

    respond_to do |format|
      if @pe_member_rel.update_attributes(params[:pe_member_rel])
        format.html { redirect_to @pe_member_rel, notice: 'Pe member rel was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pe_member_rel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pe_member_rels/1
  # DELETE /pe_member_rels/1.json
  def destroy
    @pe_member_rel = PeMemberRel.find(params[:id])
    @pe_member_rel.destroy

    respond_to do |format|
      format.html { redirect_to pe_member_rels_url }
      format.json { head :no_content }
    end
  end
end
