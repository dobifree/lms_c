class PeCalibrationController < ApplicationController

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:list_sessions]
  before_filter :get_data_2, only: [:show_session, :active_finish_session]
  before_filter :get_data_3, only: [:calibrate]

  before_filter :validate_open_process, only: [:list_sessions, :show_session, :active_finish_session, :calibrate]
  before_filter :validate_is_a_process_pe_cal_committee, only: [:list_sessions]
  before_filter :validate_is_a_session_pe_cal_committee, only: [:show_session]
  before_filter :validate_is_a_session_pe_cal_committee_manager, only: [:active_finish_session, :calibrate]


  def list_sessions



  end

  def show_session

    @is_pe_cal_session_manager = @pe_cal_session.is_pe_cal_committee_manager? user_connected

  end

  def active_finish_session
    @pe_cal_session.active = !@pe_cal_session.active
    if @pe_cal_session.save
      flash[:success] =  t('activerecord.success.model.pe_cal_session.change_state_ok')
    end

    redirect_to pe_calibration_show_session_path @pe_cal_session

  end

  def calibrate

    pe_member = @pe_cal_member.pe_member

    if pe_member.step_assessment

      pe_member.pe_box_before_calibration = pe_member.pe_box unless pe_member.step_calibration

      pe_member.pe_box_id = params[:pe_member][:pe_box_id]
      pe_member.calibration_comment = params[:pe_member][:calibration_comment]
      pe_member.step_calibration = true
      pe_member.step_calibration_date = lms_time

      if pe_member.save
        flash[:success] =  t('activerecord.success.model.pe_member.calibrate_ok')
      end

    end

    redirect_to pe_calibration_show_session_path @pe_cal_session

  end


  private

  def get_data_1
    @pe_process = PeProcess.find(params[:pe_process_id])
  end

  def get_data_2
    @pe_cal_session = PeCalSession.find(params[:pe_cal_session_id])
    @pe_process = @pe_cal_session.pe_process
  end

  def get_data_3
    @pe_cal_member = PeCalMember.find(params[:pe_cal_member_id])
    @pe_cal_session = @pe_cal_member.pe_cal_session
    @pe_process = @pe_cal_session.pe_process
  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_is_a_process_pe_cal_committee

    unless @pe_process.is_a_pe_cal_committee? user_connected
      flash[:danger] = t('activerecord.error.model.pe_process_calibration.is_not_cal_committee')
      redirect_to root_path
    end

  end

  def validate_is_a_session_pe_cal_committee

    unless @pe_cal_session.is_pe_cal_committee? user_connected
      flash[:danger] = t('activerecord.error.model.pe_process_calibration.is_not_session_pe_cal_committee')
      redirect_to root_path
    end

  end

  def validate_is_a_session_pe_cal_committee_manager

    unless @pe_cal_session.is_pe_cal_committee_manager? user_connected
      flash[:danger] = t('activerecord.error.model.pe_process_calibration.is_not_session_pe_cal_committee_manager')
      redirect_to root_path
    end

  end

end
