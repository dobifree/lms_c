class TrainingReportsForAdminController < ApplicationController

  include CsvReportsHelper

  before_filter :authenticate_user
  before_filter :authenticate_user_admin


  def download_reporte_general_remote

    queued_report = QueuedReport.where('code = ?',params[:code]).first

    if queued_report

     company = @company

      comando = "curl -u ctqreps:QWEasd123 'ftp://54.225.77.117/"+company.codigo+'/queued_reports/'+queued_report.code+"' -o /tmp/"+queued_report.code

      #system comando

      if File.exist?('/tmp/'+queued_report.code+'.'+queued_report.extension)

        send_file '/tmp/'+queued_report.code+'.'+queued_report.extension,
                  filename: queued_report.name+'.'+queued_report.extension,
                  type: 'application/octet-stream',
                  disposition: 'attachment'

        #FileUtils.rm('/tmp/'+queued_report.code)

      end

    end

  end

  def reportes

  end

  def reporte_general

   company = @company

    programs = Program.all

    courses = Array.new
    program_courses = Array.new
    programs_abiertos_ids = Array.new

    query = ''
    n_query = 0

    programs.each do |program|

      cargar_abierto = false

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            unless program.especifico
              cargar_abierto = true
              program_courses.push program_course
            end

            courses.push program_course.course

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

      programs_abiertos_ids.push program.id if cargar_abierto

    end

    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    ultimo_estado = false

    if query != ''

      if params[:solo_ultimo_estado] && params[:solo_ultimo_estado] == '1'

        ultimo_estado = true

        user_courses_ids = case params[:tipo_fecha]
                             when '1'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '2'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '3'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '4'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '5'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '6'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '7'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           end

        user_courses = Array.new

        user_courses_ids.each do |uc_id|

          uc_tmp = UserCourse.find(uc_id.id)

          estado_tmp = uc_tmp.estado lms_time

          if estado_tmp == :aprobado || estado_tmp == :reprobado
            user_courses << uc_tmp
          else
            user_courses_tmp = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', uc_tmp.enrollment_id, uc_tmp.program_course_id, false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).reorder('id DESC')

            estado_reprobado = false
            user_courses_tmp.each do |uc_tmp_r|

              if uc_tmp_r.iniciado && uc_tmp_r.finalizado && !uc_tmp_r.aprobado

                estado_reprobado = true
                user_courses << uc_tmp_r
                break

              end

            end

            unless estado_reprobado
              user_courses << uc_tmp
            end

          end


        end

      else

        user_courses = case params[:tipo_fecha]
                         when '1'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                       end

      end



    else
      user_courses = Array.new
    end

    enrollments = Enrollment.where('program_id IN (?)', programs_abiertos_ids).joins(:user).reorder('apellidos, nombre')

    reporte_general_csv enrollments, user_courses, program_courses, courses, ultimo_estado, true, nil

  end

  def reporte_general_2

   company = @company

    programs = Program.all

    courses = Array.new
    program_courses = Array.new
    programs_abiertos_ids = Array.new

    query = ''
    n_query = 0

    programs.each do |program|

      cargar_abierto = false

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            unless program.especifico
              cargar_abierto = true
              program_courses.push program_course
            end

            courses.push program_course.course

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

      programs_abiertos_ids.push program.id if cargar_abierto

    end

    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    ultimo_estado = false

    if query != ''

      if params[:solo_ultimo_estado] && params[:solo_ultimo_estado] == '1'

        ultimo_estado = true

        user_courses_ids = case params[:tipo_fecha]
                             when '1'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '2'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '3'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '4'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '5'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '6'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '7'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           end

        user_courses = Array.new

        user_courses_ids.each do |uc_id|

          uc_tmp = UserCourse.find(uc_id.id)

          user_courses << uc_tmp

        end

      else

        user_courses = case params[:tipo_fecha]
                         when '1'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                       end

      end



    else
      user_courses = Array.new
    end

    enrollments = Enrollment.where('program_id IN (?)', programs_abiertos_ids).joins(:user).reorder('apellidos, nombre')

    reporte_general_csv enrollments, user_courses, program_courses, courses, ultimo_estado, true, nil

  end

  def reporte_general_remote

    queued_report = QueuedReport.new

    begin
      queued_report.code = SecureRandom.hex(7)
    end until queued_report.save

    #queued_report.description = params[:description]
    queued_report.status = false

    queued_report.name = 'ReporteGeneral'
    queued_report.name += 'UltimoEstado' if params[:solo_ultimo_estado] && params[:solo_ultimo_estado] == '1'
    queued_report.extension = '.csv'
    queued_report.from_date = lms_time
    queued_report.save

   company = @company

    programs = Program.all

    courses = Array.new
    program_courses = Array.new
    programs_abiertos_ids = Array.new

    query = ''
    n_query = 0

    programs.each do |program|

      cargar_abierto = false

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            unless program.especifico
              cargar_abierto = true
              program_courses.push program_course
            end

            courses.push program_course.course

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

      programs_abiertos_ids.push program.id if cargar_abierto

    end

    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    ultimo_estado = false

    if query != ''

      if params[:solo_ultimo_estado] && params[:solo_ultimo_estado] == '1'

        ultimo_estado = true

        user_courses_ids = case params[:tipo_fecha]
                             when '1'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '2'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '3'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '4'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '5'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '6'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '7'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           end

        user_courses = Array.new

        user_courses_ids.each do |uc_id|

          uc_tmp = UserCourse.find(uc_id.id)

          estado_tmp = uc_tmp.estado lms_time

          if estado_tmp == :aprobado || estado_tmp == :reprobado
            user_courses << uc_tmp
          else
            user_courses_tmp = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', uc_tmp.enrollment_id, uc_tmp.program_course_id, false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).reorder('id DESC')

            estado_reprobado = false
            user_courses_tmp.each do |uc_tmp_r|

              if uc_tmp_r.iniciado && uc_tmp_r.finalizado && !uc_tmp_r.aprobado

                estado_reprobado = true
                user_courses << uc_tmp_r
                break

              end

            end

            unless estado_reprobado
              user_courses << uc_tmp
            end

          end


        end

      else

        user_courses = case params[:tipo_fecha]
                         when '1'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, enrollment_id, program_course_id, user_courses.id DESC')
                       end

      end



    else
      user_courses = Array.new
    end

    enrollments = Enrollment.where('program_id IN (?)', programs_abiertos_ids).joins(:user).reorder('apellidos, nombre')

    path_to_file = reporte_general_csv_remote enrollments, user_courses, program_courses, courses, ultimo_estado, true

    directory = '/var/www/queued_reports/'+company.codigo+'/'

    directory = '/tmp/queued_reports/'

    FileUtils.mkdir_p directory unless File.directory? directory

    FileUtils.mv(path_to_file, directory+queued_report.code)

    queued_report.status = true
    queued_report.created_in = lms_time - queued_report.from_date
    queued_report.save


  end

  def detalle_evaluaciones


   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])


    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)


    query = ' program_course_id = '+program_course.id.to_s+' '

    if query != ''

      user_courses = case params[:tipo_fecha]
                       when '1'
                         UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '2'
                         UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '3'
                         UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '4'
                         UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '5'
                         UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '6'
                         UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '7'
                         UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                     end

    else
      user_courses = Array.new
    end

    reporte_detalle_evaluaciones_csv program_course, user_courses


  end

  def reporte_por_programa


   company = @company

    program = Program.find(params['program_id'])

    query = ''
    n_query = 0

    program_courses = Array.new

    program.levels.each do |level|

      level.courses.each_with_index do |course|

        program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

        if params["pc_#{program_course.id}".to_sym] == '1'

          program_courses.push program_course

          if n_query == 0
            query += ' program_course_id = '+program_course.id.to_s+' '
            n_query += 1
          else
            query += ' OR program_course_id = '+program_course.id.to_s+' '

          end

        end

      end

    end

    desdehasta = params[:desdehasta_por_programa].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)

    enrollments = Enrollment.where('program_id = ?', program.id).joins(:user).reorder('apellidos, nombre')

    reporte_csv_por_programa program_courses, enrollments, query, desde, hasta

  end

  def reporte_encuesta_satisfaccion

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])


    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2],desde_d[1],desde_d[0],0,0,0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2],hasta_d[1],hasta_d[0],23,59,59)


    query = ' program_course_id = '+program_course.id.to_s+' '

    if query != ''

      user_courses = case params[:tipo_fecha]
                       when '1'
                         UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '2'
                         UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '3'
                         UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '4'
                         UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '5'
                         UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '6'
                         UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       when '7'
                         UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                     end

    else
      user_courses = Array.new
    end

    reporte_encuesta_satisfaccion_csv program_course, user_courses

  end

end
