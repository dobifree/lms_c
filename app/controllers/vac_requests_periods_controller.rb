class VacRequestsPeriodsController < ApplicationController
  before_filter :authenticate_user

  def index
    @vac_requests_periods = VacRequestsPeriod.all
    @vac_approvers = VacApprover.all
  end

  def show
    #MANAGE
    @vac_requests_period = VacRequestsPeriod.find(params[:id])
  end

  def new
    @vac_requests_period = VacRequestsPeriod.new
  end

  def edit
    @vac_requests_period = VacRequestsPeriod.find(params[:id])
  end

  def create
    period = complete_create(params[:vac_requests_period])
    if period.save
      flash[:success] = t('views.vac_requests_periods.flash_messages.success_created')
      redirect_to period
    else
      @vac_requests_period = period
      render action: 'new'
    end
  end

  def update
    @vac_requests_period = VacRequestsPeriod.find(params[:id])
    if @vac_requests_period.update_attributes(params[:vac_requests_period])
      flash[:success] = t('views.vac_requests_periods.flash_messages.success_changed')
      redirect_to @vac_requests_period
    else
      render action: "edit"
    end
  end

  private

  def complete_create(params)
    period = VacRequestsPeriod.new(params)
    period.registered_at = lms_time
    period.registered_by_user_id = user_connected.id
    return period
  end
end