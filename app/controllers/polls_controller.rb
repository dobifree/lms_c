class PollsController < ApplicationController

  include UserCoursesModule

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new
    
    @course = Course.find(params[:course_id])
    @poll = @course.polls.build()

  end

  def edit
    @poll = Poll.find(params[:id])
    @course = @poll.course
  end

  def edit_master_poll
    @poll = Poll.find(params[:id])
    @course = @poll.course
  end

  def delete
    @poll = Poll.find(params[:id])
    @course = @poll.course
  end

  def create
    
    @course = Course.find(params[:course][:id])

    @poll = @course.polls.build(params[:poll])

    if @poll.save

      if @poll.tipo_satisfaccion

        @course.polls.each do |poll|
          if poll.id != @poll.id
            poll.tipo_satisfaccion = false
            poll.save
          end
        end

      end


      flash[:success] = t('activerecord.success.model.poll.create_ok')
      redirect_to @course
    else
      render action: "new"
    end

  end

  
  def update
    
    @poll = Poll.find(params[:id])

    if @poll.update_attributes(params[:poll])

      unless @poll.obligatoria

        @poll.course.user_courses.each do |user_course|

          unless user_course.finalizado

            update_percentage_advance(user_course)

            finish_course(user_course)

          end

        end

      end

      if @poll.tipo_satisfaccion

        @poll.course.polls.each do |poll|
          if poll.id != @poll.id
            poll.tipo_satisfaccion = false
            poll.save
          end
        end

      end

      flash[:success] = t('activerecord.success.model.poll.update_ok')
      redirect_to @poll.course
    else
      @course = @poll.course
      render action: 'edit'
    end


  end

  def update_master_poll

    @poll = Poll.find(params[:poll][:id])

    if @poll.update_attributes(master_poll_id: params[:poll][:master_poll_id])
      flash[:success] = t('activerecord.success.model.poll.update_ok')
      @poll.after_clean_master_poll if @poll.master_poll.nil?
      redirect_to @poll.course
    else
      @course = @poll.course
      render action: 'edit_master_poll'
    end

  end

  def destroy
    
    @poll = Poll.find(params[:id])

    if verify_recaptcha

      units_sequence = @poll.despues_de_units + @poll.antes_de_units
      evaluations_sequence = @poll.despues_de_evaluations + @poll.antes_de_evaluations
      polls_sequence = @poll.despues_de_polls + @poll.antes_de_polls
    
      if @poll.destroy

        units_sequence.each { |unit| unit.reload.update_libre }
        evaluations_sequence.each { |evaluation| evaluation.reload.update_libre_course }
        polls_sequence.each { |poll| poll.reload.update_libre_course }

        flash[:success] = t('activerecord.success.model.poll.delete_ok')
        redirect_to course_path(@poll.course)
      else
        flash[:danger] = t('activerecord.success.model.poll.delete_error')
        redirect_to delete_poll_path(@poll)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.poll.captcha_error')
      redirect_to delete_poll_path(@poll)

    end

  end
  
end
