class CompaniesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
   #@company = session[:company]

    file = @company.directorio+'/'+@company.codigo+'/design/custom_login.css'

    @login_css_text = ''

    if File.file?(file)

      File.open(file, 'r') do |f|
        f.each_line do |line|
          @login_css_text += line
        end
      end

    end

  end


  def new
    @company = Company.new
  end

  def edit
   #@company = session[:company]
  end

  def edit_emails
   #@company = session[:company]
  end

  def edit_design
   #@company = session[:company]
  end

  def edit_header_img

   #@company = session[:company]

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename).downcase == '.jpg' || File.extname(params[:upload]['datafile'].original_filename).downcase == '.jpeg'

        directory = @company.directorio+'/'+@company.codigo+'/design/'

        FileUtils.mkdir_p directory unless File.directory? directory

        file = directory+'header.jpg'

        File.open(file, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        flash[:success] = t('activerecord.success.model.company.upload_image_ok')

        redirect_to companies_path

      else

        flash.now[:danger] = t('activerecord.error.model.company.image_wrong_format')

      end

    elsif request.post?
      flash.now[:danger] = t('activerecord.error.model.company.upload_file_no_file')
    end

  end

  def edit_login_img

   #@company = session[:company]

    @res = params[:res]

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename).downcase == '.jpg' || File.extname(params[:upload]['datafile'].original_filename).downcase == '.jpeg'

        directory = @company.directorio+'/'+@company.codigo+'/design/'

        FileUtils.mkdir_p directory unless File.directory? directory

        file = directory+'bg_login_'+@res+'.jpg'

        File.open(file, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        flash[:success] = t('activerecord.success.model.company.upload_image_ok')

        redirect_to companies_path

      else

        flash.now[:danger] = t('activerecord.error.model.company.image_wrong_format')

      end

    elsif request.post?
      flash.now[:danger] = t('activerecord.error.model.company.upload_file_no_file')
    end

  end

  def delete_login_img

   #@company = session[:company]

    @res = params[:res]

    directory = @company.directorio+'/'+@company.codigo+'/design/'

    file = directory+'bg_login_'+@res+'.jpg'

    File.delete(file) if File.file?(file)

    flash[:success] = t('activerecord.success.model.company.delete_image_ok')

    redirect_to companies_path

  end

  def edit_login_css

   #@company = session[:company]

    file = @company.directorio+'/'+@company.codigo+'/design/custom_login.css'

    @login_css_text = ''

    if File.file?(file)

      File.open(file, 'r') do |f|
        f.each_line do |line|
          @login_css_text += line
        end
      end

    end

  end

  def update_login_css

   #@company = session[:company]

    directory = @company.directorio+'/'+@company.codigo+'/design/'

    FileUtils.mkdir_p directory unless File.directory? directory

    file = directory+'custom_login.css'

    File.open(file, 'w') { |f| f.write(params[:css_login]) }

    flash[:success] = t('activerecord.success.model.company.update_login_css_ok')
    redirect_to companies_edit_login_css_path

  end

  def create
    @company = Company.new(params[:company])
    @company.save
    redirect_to companies_url
  end

  def update

   #@company = session[:company]
    if @company.update_attributes(params[:company])

      #session[:company] = @company

    end

    redirect_to companies_url
  end

end
