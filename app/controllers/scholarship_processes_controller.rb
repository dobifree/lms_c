class ScholarshipProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /scholarship_processes
  # GET /scholarship_processes.json
  def index
    @scholarship_processes = ScholarshipProcess.all
  end

  # GET /scholarship_processes/1
  # GET /scholarship_processes/1.json
  def show
    @scholarship_process = ScholarshipProcess.find(params[:id])
  end

  # GET /scholarship_processes/new
  # GET /scholarship_processes/new.json
  def new
    @scholarship_process = ScholarshipProcess.new
  end

  # GET /scholarship_processes/1/edit
  def edit
    @scholarship_process = ScholarshipProcess.find(params[:id])
  end

  # POST /scholarship_processes
  # POST /scholarship_processes.json
  def create
    @scholarship_process = ScholarshipProcess.new(params[:scholarship_process])
    if @scholarship_process.save
      flash[:success] = 'Proceso creado correctamente'
      redirect_to @scholarship_process
    else
      flash.now[:danger] = 'El proceso no pudo ser creado correctamente'
      render 'new'
    end
  end

  # PUT /scholarship_processes/1
  # PUT /scholarship_processes/1.json
  def update
    @scholarship_process = ScholarshipProcess.find(params[:id])

    if @scholarship_process.update_attributes(params[:scholarship_process])
      flash[:success] = 'Proceso actualizado correctamente'
      redirect_to @scholarship_process
    else
      flash.now[:danger] = 'El proceso no pudo ser actualizado correctamente'
      render 'edit'
    end
  end

  # DELETE /scholarship_processes/1
  # DELETE /scholarship_processes/1.json
  def destroy
    @scholarship_process = ScholarshipProcess.find(params[:id])
    @scholarship_process.destroy
    flash[:success] = 'Proceso eliminado correctamente'
    redirect_to scholarship_processes_path
  end

  def edit_groups
    @scholarship_process = ScholarshipProcess.find(params[:id])
  end

  def update_groups
    @scholarship_process = ScholarshipProcess.find(params[:id])
    if @scholarship_process.update_attributes(params[:scholarship_process])
      flash[:success] = 'Proceso actualizado correctamente'
      redirect_to @scholarship_process
    else
      flash.now[:danger] = 'El proceso no pudo ser actualizado correctamente'
      render 'edit_groups'
    end
  end
end
