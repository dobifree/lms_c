class KpiDashboardGuestsController < ApplicationController

  include UsersHelper

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  def new

    @kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])
    @kpi_dashboard_guest = @kpi_dashboard.kpi_dashboard_guests.build
    @user_search = User.new(params[:user])

    @users_search = search_and_list_normal_users(@user_search.apellidos, @user_search.nombre)

  end

  def create

    kpi_dashboard = KpiDashboard.find(params[:kpi_dashboard_id])

    if params[:list_item]

      params[:list_item].each do |user_id,value|

        if value != '0'

          kpi_dashboard_guest = kpi_dashboard.kpi_dashboard_guests.build

          kpi_dashboard_guest.user_id = user_id

          kpi_dashboard_guest.save

        end

      end

      flash[:success] =  t('activerecord.success.model.kpi_dashboard_guest.create_ok')

    end

    redirect_to new_kpi_dashboard_guest_path kpi_dashboard

  end

  def destroy

    kpi_dashboard_guest = KpiDashboardGuest.find(params[:id])
    kpi_dashboard = kpi_dashboard_guest.kpi_dashboard
    kpi_dashboard_guest.destroy

    flash[:success] =  t('activerecord.success.model.kpi_dashboard_guest.delete_ok')
    redirect_to kpi_dashboard

  end

  private

    def verify_access_manage_module

      ct_module = CtModule.where('cod = ? AND active = ?', 'kpis', true).first

      unless admin_logged_in? || (ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1)
        flash[:danger] = t('security.no_access_to_manage_module')
        redirect_to root_path
      end
    end
  
end
