include ElecManageProcessesHelper
class ElecManageProcessesController < ApplicationController
  before_filter :authenticate_user
  before_filter :authorized_to_manage_elec_processes

  def index
    @elec_processes = ElecProcess.where(active: true)
  end

  def show
    @elec_process = ElecProcess.find(params[:id])
    # params[:what_to_show] == true => histórico
    @what_to_show = params[:what_to_show]

    ## para mostrar el round abierto
    if params[:elec_round_id]
      @elec_round = ElecRound.find(params[:elec_round_id])
    end
  end

  def show_round_detail
    @elec_round = ElecRound.find(params[:elec_round_id])
    render :layout => false
  end

  def show_rounds_list
    @elec_process = ElecProcess.find(params[:id])
    # params[:what_to_show] == true => histórico
    @what_to_show = params[:what_to_show]


    if @what_to_show == 'hist'
      # se muestran los eventos que tienen rondas finalizadas
      @elec_current_events = @elec_process.elec_events.joins(:elec_rounds).where(:active => true, elec_rounds: {:done => true})
    else
      # se muestran los eventos que tienen rondas abiertas
      @elec_current_events = @elec_process.elec_events.joins(:elec_rounds).where(:active => true, elec_rounds: {:done => false}).where('elec_rounds.from_date < ?', lms_time)
    end


    render :layout => false
  end

  def show_category_detail
    @elec_round = ElecRound.find(params[:elec_round_id])
    @elec_category = ElecProcessCategory.find(params[:elec_process_category_id])

    @group_values = []
    set_group_values

    render :layout => false

  end

  def toggle_winner_status_candidate
    @elec_round = ElecRound.find(params[:elec_round_id])

    if round_allow_to_toggle_status_winners(@elec_round)


      @elec_category = ElecProcessCategory.find(params[:elec_process_category_id])

      answer = {}

      if winner_register = @elec_round.elec_round_winners.where(:elec_process_category_id => @elec_category.id, :user_id => params[:user_id]).first
        winner_register.destroy
        answer[:final_status] = false
        #answer[:message] = 'Eliminado'
      else
        @elec_round.elec_round_winners.create(:elec_process_category_id => @elec_category.id, :user_id => params[:user_id], :registered_by_user_id => user_connected.id, :registered_at => lms_time)
        answer[:final_status] = true
        #answer[:message] = 'Registrado'
      end

      render json: answer

    end
  end

  def finish_round
    @elec_round = ElecRound.find(params[:elec_round_id])
    if round_allow_to_finish(@elec_round)
      @elec_round.done = true
      @elec_round.done_at = lms_time
      @elec_round.done_by_user_id = user_connected.id
      @elec_round.save

      flash[:success] = 'Ronda finalizada correctamente'
    else
      flash[:danger] = 'La Ronda no pudo ser finalizada correctamente'
    end
    redirect_to elec_manage_processes_show_round_open_path(@elec_round.elec_process_round.elec_process, 'open', @elec_round)
  end

  def export_round_results
    @elec_round = ElecRound.find(params[:elec_round_id])

    @group_values = []
    set_group_values

    @chars_to_export = ElecCharacteristic.where(:can_be_used_in_reports => true).order(:can_be_used_in_reports_position)

    filename = 'resultados_ronda_' + @elec_round.elec_process_round.name.gsub(/\s/, '_') + '.xls'
    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end
  end


  private

  def set_group_values
    #TODO: solo funciona para el primer agrupador
    if group_by_field = @elec_round.elec_process_round.elec_process.elec_characteristics.where(:used_to_group_by => true).first

      @group_values = group_by_field.characteristic.user_characteristics.
          map { |uc| uc.formatted_value.upcase }.uniq.compact #-
          #@elec_category.elec_category_characteristics.where(:characteristic_id => group_by_field.characteristic_id, :filter_candidate => true).map { |elec_cat_char| elec_cat_char.match_value }
    end



  end

end
