class MyPeopleInformationController < ApplicationController

  include PeReportingHelper
  include UsersHelper

  before_filter :authenticate_user
  before_filter :get_data1, only: [:list_pe_members2]
  before_filter :get_data2, only:[:pe_rep_detailed_final_results, :pe_rep_detailed_final_results_pdf]
  before_filter :get_data_5, only:[:pe_rep_detailed_final_results_from_pe, :pe_rep_detailed_final_results_from_pe_pdf]
  before_filter :get_data_3, only: [:download_characteristic_file]
  before_filter :get_data_4, only: [:download_user_characteristic_file]

  before_filter :verify_access, only: [:user_profile, :download_informative_document, :download_characteristic_file, :download_user_characteristic_file]
  before_filter :verify_access_8, only: [:index3]
  before_filter :verify_access_7, only: [:pe_rep_detailed_final_results, :pe_rep_detailed_final_results_pdf]
  before_filter :verify_access_9, only: [:pe_rep_detailed_final_results_from_pe, :pe_rep_detailed_final_results_from_pe_pdf]
  before_filter :verify_access_2, only: [:show_pe_member]
  before_filter :verify_access_3, only: [:list_pe_members2]
  before_filter :verify_access_4, only: [:render_graph_row, :render_graph_row_selected]
  before_filter :verify_access_5, only: [:search]
  before_filter :verify_access_6, only: [:render_graph_row3, :render_graph_row_selected3]


  before_filter :validate_pe_rep_detailed_final_results, only: [:pe_rep_detailed_final_results]
  before_filter :validate_pe_rep_detailed_final_results_pdf, only: [:pe_rep_detailed_final_results_pdf]

  def index
    @root_nodes = user_connected.owned_nodes
  end

  def index2
    @ct_module_mpi = CtModule.find_by_cod('mpi')
    @members = user_connected.members
   #@company = session[:company]
  end

  def index3
    @members = @user.members
   #@company = session[:company]
  end

  def render_graph_row

   #@company = session[:company]
    row = params[:row].to_i

    render partial: 'graph_row2',
           locals: {
               members: @user.members,
               row: row,
               render_selected: @user.num_members == 1 ? true : false
           }

  end

  def render_graph_row_selected

   #@company = session[:company]
    row = params[:row].to_i

    render partial: 'graph_row2',
           locals: {
               members: [@user],
               row: row,
               render_selected: true
           }

  end

  def render_graph_row3

   #@company = session[:company]
    row = params[:row].to_i

    render partial: 'graph_row3',
           locals: {
               members: @user.members,
               row: row,
               render_selected: @user.num_members == 1 ? true : false
           }

  end

  def render_graph_row_selected3

   #@company = session[:company]
    row = params[:row].to_i

    render partial: 'graph_row3',
           locals: {
               members: [@user],
               row: row,
               render_selected: true
           }

  end

  def user_profile
   #@company = session[:company]


  end

  def search

    @user = User.new
    @users = {}

    if params[:user]

      @users = search_employees(params[:user][:codigo], params[:user][:apellidos], params[:user][:nombre])
      @user = User.new(params[:user])

    end

  end

  def dncs
    @active_members_full_hierarchy = user_connected.active_members_full_hierarchy.sort_by {|u| u.apellidos+' '+u.nombre}

    @dnc_characteristics = DncCharacteristic.all
  end

  def list_pe_processes

  end

  def list_pe_members

    @hr_process = HrProcess.find(params[:hr_process_id])

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_template = @hr_process.hr_process_template

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @root_nodes = user_connected.owned_nodes

  end

  def list_pe_members2

    @root_nodes = user_connected.owned_nodes

  end

  def show_pe_member
    @hr_process = @hr_process_user.hr_process
    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions
    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)
   #@company = session[:company]
  end

  def download_characteristic_file

    directory = @company.directorio+'/'+@company.codigo+'/user_profiles/'+@user.id.to_s+'/characteristics/'+@characteristic.id.to_s+'/'


    send_file directory+@user_characteristic.value_file,
              filename: @characteristic.nombre+'_'+@user_characteristic.value_file,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_user_characteristic_file

    directory = @company.directorio+'/'+@company.codigo+'/user_profiles/'+@user.id.to_s+'/characteristics/'+@characteristic.id.to_s+'/'


    send_file directory+@user_characteristic.value_file,
              filename: @characteristic.nombre+'_'+@user_characteristic.value_file,
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def download_informative_document

    document = InformativeDocument.find(params[:id])

    profile = document.reader_profile(@user, user_connected)
    read_permission = document.profile_has_permission(profile)

    if read_permission

   company = @company

    file = company.directorio+'/'+company.codigo+'/informative_documents/'+document.id.to_s+'/'+@user.codigo+'.pdf'


    send_file file,
              filename: document.name+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

    else
      flash[:danger] = 'Usted no tiene los privilegios necesarios para acceder a este documento.'
      redirect_to root_path
    end

  end

  def pe_rep_detailed_final_results


  end

  def pe_rep_detailed_final_results_pdf


    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_for_my_people, @pe_process.rep_detailed_final_results_show_calibration_for_my_people, @pe_process.public_pe_box, @pe_process.public_pe_dimension_name, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def pe_rep_detailed_final_results_from_pe


  end

  def pe_rep_detailed_final_results_from_pe_pdf


    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    generate_rep_detailed_final_results_pdf archivo_temporal, @pe_member, @pe_process, @pe_process.rep_detailed_final_results_show_feedback_for_my_people, @pe_process.rep_detailed_final_results_show_calibration_for_my_people, @pe_process.public_pe_box, @pe_process.public_pe_dimension_name, true

    send_file archivo_temporal,
              filename: 'EvaluacionDesempeño - '+@pe_member.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  private

  def get_data1
    @pe_process = PeProcess.find(params[:pe_process_id])
  end

  def get_data2

    @pe_process = PeProcess.find(params[:pe_process_id])
    @user = User.find params[:user_id]
    @pe_member = @pe_process.pe_member_by_user @user
   #@company = session[:company]

  end

  def get_data_3
   #@company = session[:company]
    @user = User.find(params[:user_id])

    @characteristic = Characteristic.find_by_id(params[:characteristic_id])
    @user_characteristic = @user.user_characteristics.find_by_characteristic_id(params[:characteristic_id])

  end

  def get_data_4
   #@company = session[:company]
    @user = User.find(params[:user_id])

    @user_characteristic = @user.user_characteristics.find_by_id(params[:user_characteristic_id])
    @characteristic = @user_characteristic ? @user_characteristic.characteristic : nil

  end

  def get_data_5

    @current_pe_process = PeProcess.find(params[:current_pe_process_id])
    @pe_process = PeProcess.find(params[:pe_process_id])
    @user = User.find params[:user_id]
    @pe_member = @pe_process.pe_member_by_user @user
   #@company = session[:company]

  end

  def verify_access

    @user = User.find(params[:user_id])

    @es_jefe_de = user_connected.es_jefe_de(@user)

    unless @es_jefe_de || user_connected.mpi_view_users
      flash[:danger] = t('security.no_access_view_user_profile')
      redirect_to root_path
    end

  end

  def verify_access_2

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    @user = @hr_process_user.user

    unless user_connected.es_jefe_de @user
      flash[:danger] = t('security.no_access_view_user_profile')
      redirect_to root_path
    end

  end

  def verify_access_3

    unless @pe_process.display_results_in_my_people?
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end

  end

  def verify_access_4

    @user = User.find(params[:user_id])

    unless user_connected.es_jefe_de(@user) || user_connected.id == @user.id
      flash[:danger] = t('security.no_access_view_user_members')
      redirect_to root_path
    end

  end

  def verify_access_5

    unless user_connected.mpi_view_users

      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path

    end

  end

  def verify_access_6

    @user = User.find(params[:user_id])

    unless user_connected.mpi_view_users || ct_module_um_manager? || user_connected.um_manage_users

      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path

    end

  end

  def verify_access_7

    @user = User.find(params[:user_id])

    @es_jefe_de = user_connected.es_jefe_de(@user)

    unless @es_jefe_de
      flash[:danger] = t('security.no_access_view_user_profile')
      redirect_to root_path
    end

  end

  def verify_access_8

    @user = User.find(params[:user_id])

    @es_jefe_de = user_connected.es_jefe_de(@user)

    unless @es_jefe_de || user_connected.mpi_view_users || ct_module_um_manager? || user_connected.um_manage_users
      flash[:danger] = t('security.no_access_view_user_profile')
      redirect_to root_path
    end

  end

  def verify_access_9

    @user = User.find(params[:user_id])

    pe_rel_boss = @current_pe_process.pe_rel_by_id 1
    pe_member_evaluated = @current_pe_process.pe_member_by_user @user
    pe_member_evaluator = @current_pe_process.pe_member_by_user user_connected

    es_jefe_de = false

    if pe_rel_boss && pe_member_evaluated && pe_member_evaluator

      pe_member_rel = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_member_evaluator

      es_jefe_de = true if pe_member_rel && pe_member_rel.pe_rel.id == pe_rel_boss.id

    end

    unless es_jefe_de
      flash[:danger] = t('security.no_access_view_user_profile')
      redirect_to root_path
    end

  end

  def validate_pe_rep_detailed_final_results
    unless @pe_process.display_results_in_my_people? && @pe_process.display_my_people_show_rep_dfr?
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

  def validate_pe_rep_detailed_final_results_pdf
    unless @pe_process.display_results_in_my_people? && @pe_process.display_my_people_show_rep_dfr_pdf?
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

end
