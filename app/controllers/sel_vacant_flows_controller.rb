class SelVacantFlowsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /sel_vacant_flows
  # GET /sel_vacant_flows.json
  def index
    @sel_vacant_flows = SelVacantFlow.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sel_vacant_flows }
    end
  end

  # GET /sel_vacant_flows/1
  # GET /sel_vacant_flows/1.json
  def show
    @sel_vacant_flow = SelVacantFlow.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sel_vacant_flow }
    end
  end

  # GET /sel_vacant_flows/new
  # GET /sel_vacant_flows/new.json
  def new
    @sel_vacant_flow = SelVacantFlow.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sel_vacant_flow }
    end
  end

  # GET /sel_vacant_flows/1/edit
  def edit
    @sel_vacant_flow = SelVacantFlow.find(params[:id])
  end

  # POST /sel_vacant_flows
  # POST /sel_vacant_flows.json
  def create
    @sel_vacant_flow = SelVacantFlow.new(params[:sel_vacant_flow])

    respond_to do |format|
      if @sel_vacant_flow.save
        format.html { redirect_to @sel_vacant_flow, notice: 'Sel vacant flow was successfully created.' }
        format.json { render json: @sel_vacant_flow, status: :created, location: @sel_vacant_flow }
      else
        format.html { render action: "new" }
        format.json { render json: @sel_vacant_flow.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sel_vacant_flows/1
  # PUT /sel_vacant_flows/1.json
  def update
    @sel_vacant_flow = SelVacantFlow.find(params[:id])

    respond_to do |format|
      if @sel_vacant_flow.update_attributes(params[:sel_vacant_flow])
        format.html { redirect_to @sel_vacant_flow, notice: 'Sel vacant flow was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sel_vacant_flow.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sel_vacant_flows/1
  # DELETE /sel_vacant_flows/1.json
  def destroy
    @sel_vacant_flow = SelVacantFlow.find(params[:id])
    @sel_vacant_flow.destroy

    respond_to do |format|
      format.html { redirect_to sel_vacant_flows_url }
      format.json { head :no_content }
    end
  end






  ##################################################
  ##### APPROVERS ##################################


  def new_approver
    @sel_vacant_flow = SelVacantFlow.find(params[:id])
    @sel_flow_approver = @sel_vacant_flow.sel_flow_approvers.build
    @sel_flow_approver.build_sel_flow_specific_approver
  render 'sel_vacant_flows/approvers/new'
  end

  def create_approver
    @sel_vacant_flow = SelVacantFlow.find(params[:id])

    @sel_flow_approver = @sel_vacant_flow.sel_flow_approvers.build(params[:sel_flow_approver])

    if @sel_vacant_flow.save
      flash[:success] = 'El Aprobador fue creado correctamente'
      redirect_to @sel_vacant_flow
    else
      @sel_flow_approver.build_sel_flow_specific_approver unless @sel_flow_approver.sel_flow_specific_approver
      flash.now[:danger] = 'El Aprobador no pudo ser creado correctamente'
      render 'sel_vacant_flows/approvers/new'
    end

  end

  def edit_approver
    @sel_vacant_flow = SelVacantFlow.find(params[:id])
    @sel_flow_approver = @sel_vacant_flow.sel_flow_approvers.find(params[:sel_flow_approver_id])
    @sel_flow_approver.build_sel_flow_specific_approver unless @sel_flow_approver.sel_flow_specific_approver
    render 'sel_vacant_flows/approvers/edit'
  end


  def update_approver
    @sel_vacant_flow = SelVacantFlow.find(params[:id])

    @sel_flow_approver = @sel_vacant_flow.sel_flow_approvers.find(params[:sel_flow_approver_id])

    if @sel_flow_approver.update_attributes(params[:sel_flow_approver])
      flash[:success] = 'El Aprobador fue actualizado correctamente'
      redirect_to @sel_vacant_flow
    else
      @sel_flow_approver.build_sel_flow_specific_approver unless @sel_flow_approver.sel_flow_specific_approver
      flash.now[:danger] = 'El Aprobador no pudo ser actualizado correctamente'
      render 'sel_vacant_flows/approvers/edit'
    end

  end

  def destroy_approver
    @sel_vacant_flow = SelVacantFlow.find(params[:id])

    @sel_flow_approver = @sel_vacant_flow.sel_flow_approvers.find(params[:sel_flow_approver_id])

    @sel_flow_approver.destroy

    if @sel_flow_approver.destroyed?
      flash[:success] = 'El Aprobador fue eliminado correctamente'
    else
      flash.now[:danger] = 'El Aprobador no pudo ser eliminado correctamente'
    end

    redirect_to @sel_vacant_flow
  end


  def load_characteristic_values
    values = CharacteristicValue.where(:characteristic_id =>  params[:characteristic_id]).reorder(:value_string).map{|char_value| [char_value.value_string, char_value.id]}
    render json: values
  end


  def load_user_characteristic_values
    values = UserCharacteristic.where(:characteristic_id =>  params[:characteristic_id]).reorder(:valor).map{|user_characteristic| [user_characteristic.value_string]}.uniq
    render json: values
  end

end
