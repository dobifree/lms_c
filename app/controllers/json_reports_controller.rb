class JsonReportsController < ApplicationController

  include UsersHelper
  include JsonReportsHelper

  before_filter :authenticate_user
  before_filter :verify_access_manage_module, only: [ :show]

  def index
    @users = User.all

  end

  def show
    @user = User.find(params[:user_id])
    @user = active_user_characteristics(@user)
    render json: @user
  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

end
