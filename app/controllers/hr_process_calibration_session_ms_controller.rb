class HrProcessCalibrationSessionMsController < ApplicationController

  before_filter :authenticate_user

  before_filter :acceso_gestionar_proceso, only: [:index, :search, :create, :create_grupo]
  before_filter :acceso_gestionar_proceso2, only: [:destroy]

  def index
    @hr_process_calibration_session_ms = @hr_process_calibration_session.hr_process_calibration_session_ms_order_by_apellidos_nombre
    @user = User.new
    @hr_process_users = nil
    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

    @characteristics_prev = {}
  end

  def search

    @hr_process_calibration_session_ms = @hr_process_calibration_session.hr_process_calibration_session_ms_order_by_apellidos_nombre


    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')
    @characteristics_prev = {}


    @hr_process_users = nil
    @user = User.new

    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND valor = "'+@characteristics_prev[characteristic.id]+'"'

          n_q += 1

        end

      end
    end

    params[:user][:apellidos] = '%' if params[:user][:apellidos].blank?
    params[:user][:nombre] = '%' if params[:user][:nombre].blank?

    if queries.length > 0

      queries.each_with_index do |query,index|

        if index == 0

          if params[:evaluador][:id] && params[:evaluador][:id] != ''

            @hr_process_users = HrProcessUser.where(
                'apellidos LIKE ? AND nombre LIKE ? AND hr_process_users.hr_process_id = ? AND
                 hr_process_user_rels.hr_process_user1_id = ? AND hr_process_user_rels.tipo = "jefe" AND '+query,
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                @hr_process.id,
                params[:evaluador][:id]).joins(:hr_process_es_evaluado_rels, user: [:user_characteristics]).order('apellidos, nombre')

          else

            @hr_process_users = HrProcessUser.where(
                'apellidos LIKE ? AND nombre LIKE ? AND hr_process_id = ? AND '+query,
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                @hr_process.id).joins(user: [:user_characteristics]).order('apellidos, nombre')

          end

        else

          lista_hr_process_users_ids = @hr_process_users.map {|hpu| hpu.id }

          if params[:evaluador][:id] && params[:evaluador][:id] != ''

            @hr_process_users = HrProcessUser.where(
                'apellidos LIKE ? AND nombre LIKE ? AND hr_process_users.hr_process_id = ? AND
                hr_process_user_rels.hr_process_user1_id = ? AND hr_process_user_rels.tipo = "jefe" AND '+query+' AND hr_process_users.id IN (?) ',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                @hr_process.id,
                params[:evaluador][:id],
                lista_hr_process_users_ids).joins(:hr_process_es_evaluado_rels, user: [:user_characteristics]).order('apellidos, nombre')

          else

            @hr_process_users = HrProcessUser.where(
                'apellidos LIKE ? AND nombre LIKE ? AND hr_process_id = ? AND '+query+' AND hr_process_users.id IN (?) ',
                '%'+params[:user][:apellidos]+'%',
                '%'+params[:user][:nombre]+'%',
                @hr_process.id, lista_hr_process_users_ids).joins(user: [:user_characteristics]).order('apellidos, nombre')

          end

        end

      end

    else

      if params[:evaluador][:id] && params[:evaluador][:id] != ''

        @hr_process_users = HrProcessUser.where(
            'apellidos LIKE ? AND nombre LIKE ? AND hr_process_users.hr_process_id = ? AND
           hr_process_user_rels.hr_process_user1_id = ? AND hr_process_user_rels.tipo = "jefe" ',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%',
            @hr_process.id,
            params[:evaluador][:id]).joins(:user, :hr_process_es_evaluado_rels).order('apellidos, nombre')
      else

        @hr_process_users = HrProcessUser.where(
            'apellidos LIKE ? AND nombre LIKE ? AND hr_process_users.hr_process_id = ? ',
            '%'+params[:user][:apellidos]+'%',
            '%'+params[:user][:nombre]+'%',
            @hr_process.id).joins(:user).order('apellidos, nombre')

      end

    end

    @user.apellidos = params[:user][:apellidos] unless params[:user][:apellidos] == '%'
    @user.nombre = params[:user][:nombre] unless params[:user][:nombre] == '%'

    render 'index'

  end

  def create

    hr_process_calibration_session_m = @hr_process_calibration_session.hr_process_calibration_session_ms.build
    hr_process_calibration_session_m.hr_process_user_id = params[:hr_process_user_id]

    if hr_process_calibration_session_m.save
      flash[:success] = t('activerecord.success.model.hr_process_calibration_session_m.create_ok')
    end

    redirect_to listar_hr_process_calibration_session_ms_path(@hr_process_calibration_session)

  end

  def create_grupo

    if params[:hr_process_user_id]

      params[:hr_process_user_id].each do |hr_process_user_id,value|

        if value != '0'

          hr_process_calibration_session_m = @hr_process_calibration_session.hr_process_calibration_session_ms.build

          hr_process_calibration_session_m.hr_process_user_id = hr_process_user_id

          hr_process_calibration_session_m.save

        end

      end

      flash[:success] = t('activerecord.success.model.hr_process_calibration_session_m.create_grupo_ok')

    end

    redirect_to listar_hr_process_calibration_session_ms_path(@hr_process_calibration_session)

  end

  def destroy

    @hr_process_calibration_session_m.hr_process_assessment_d_cals.destroy_all
    @hr_process_calibration_session_m.hr_process_assessment_qua_cals.destroy_all

    @hr_process_calibration_session_m.destroy

    flash[:success] = t('activerecord.success.model.hr_process_calibration_session_m.delete_ok')

    redirect_to listar_hr_process_calibration_session_ms_path(@hr_process_calibration_session)

  end

  # GET /hr_process_calibration_session_ms/1
  # GET /hr_process_calibration_session_ms/1.json
  def show
    @hr_process_calibration_session_m = HrProcessCalibrationSessionM.find(params[:id])

    respond_to do |format|
      format.html # show.html.erbrb
      format.json { render json: @hr_process_calibration_session_m }
    end
  end

  # GET /hr_process_calibration_session_ms/new
  # GET /hr_process_calibration_session_ms/new.json
  def new
    @hr_process_calibration_session_m = HrProcessCalibrationSessionM.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @hr_process_calibration_session_m }
    end
  end

  # GET /hr_process_calibration_session_ms/1/edit
  def edit
    @hr_process_calibration_session_m = HrProcessCalibrationSessionM.find(params[:id])
  end

  # POST /hr_process_calibration_session_ms
  # POST /hr_process_calibration_session_ms.json


  # PUT /hr_process_calibration_session_ms/1
  # PUT /hr_process_calibration_session_ms/1.json
  def update
    @hr_process_calibration_session_m = HrProcessCalibrationSessionM.find(params[:id])

    respond_to do |format|
      if @hr_process_calibration_session_m.update_attributes(params[:hr_process_calibration_session_m])
        format.html { redirect_to @hr_process_calibration_session_m, notice: 'Hr process calibration session m was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @hr_process_calibration_session_m.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hr_process_calibration_session_ms/1
  # DELETE /hr_process_calibration_session_ms/1.json


  private

    def acceso_gestionar_proceso

      @hr_process_calibration_session = HrProcessCalibrationSession.find(params[:hr_process_calibration_session_id])

      @hr_process = @hr_process_calibration_session.hr_process

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

      unless hr_process_manager

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end

    def acceso_gestionar_proceso2

      @hr_process_calibration_session_m = HrProcessCalibrationSessionM.find(params[:id])

      @hr_process_calibration_session = @hr_process_calibration_session_m.hr_process_calibration_session

      hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process_calibration_session.hr_process.id).first

      unless hr_process_manager

        flash[:danger] = t('security.no_access_hr_process_manager')
        redirect_to root_path

      end

    end


end
