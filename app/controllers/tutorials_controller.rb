class TutorialsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /tutorials
  # GET /tutorials.json
  def index
    @tutorials = Tutorial.all
  end

  # GET /tutorials/1
  # GET /tutorials/1.json
  def show
    @tutorial = Tutorial.find(params[:id])
  end

  # GET /tutorials/new
  # GET /tutorials/new.json
  def new
    @tutorial = Tutorial.new
  end

  # GET /tutorials/1/edit
  def edit
    @tutorial = Tutorial.find(params[:id])
  end

  # POST /tutorials
  # POST /tutorials.json
  def create
    @tutorial = Tutorial.new(params[:tutorial])

      if @tutorial.save
        flash[:success] = 'El tutorial fue creado correctamente'
        redirect_to tutorials_path
      else
        flash.now[:danger] = 'El tutorial no fue creado correctamente'
        render 'new'
      end
  end

  # PUT /tutorials/1
  # PUT /tutorials/1.json
  def update
    @tutorial = Tutorial.find(params[:id])

      if @tutorial.update_attributes(params[:tutorial])
        flash[:success] = 'El tutorial fue actualizado correctamente'
        redirect_to tutorials_path
      else
        flash.now[:danger] = 'El tutorial no fue actualizado correctamente'
        render 'edit'
      end
  end

  # DELETE /tutorials/1
  # DELETE /tutorials/1.json
  def destroy
    @tutorial = Tutorial.find(params[:id])
    @tutorial.destroy

    flash[:success] = 'El tutorial fue eliminado correctamente'
    redirect_to tutorials_path

  end

  def form_upload_content
    @tutorial = Tutorial.find(params[:id])
  end

  def upload_content
    @tutorial = Tutorial.find(params[:id])


    if params[:upload]
      upload = params[:upload]
      if File.extname(upload['datafile'].original_filename) == '.zip'
        @tutorial.crypted_name = @tutorial.id.to_s + '_' + SecureRandom.hex(5) unless @tutorial.crypted_name
        save_file params[:upload], @tutorial
        @tutorial.save
        flash[:success] = t('activerecord.success.model.banner.complement_upload_ok')
        redirect_to tutorials_path
      else
        flash.now[:danger] = t('activerecord.error.model.banner.complement_file_must_zip')
        render 'form_upload_content'
      end
    else
      flash.now[:danger] = t('activerecord.error.model.banner.file_form_no_file_selected')
      render 'form_upload_content'
    end

  end




  private

  def save_file(upload, tutorial)

    directory = @company.directorio+'/'+ @company.codigo+'/tutorials/'+tutorial.crypted_name+'/'
    name = 'tut_'+tutorial.id.to_s+'.zip'
    path = File.join(directory, name)

    complement_path_hash = {:directory => directory, :name => name, :full_path => path}

    FileUtils.remove_dir complement_path_hash[:directory] if File.directory? complement_path_hash[:directory]
    FileUtils.mkdir_p complement_path_hash[:directory]

    File.open(complement_path_hash[:full_path], 'wb') { |f| f.write(upload['datafile'].read) }
    system "unzip -o -q \"#{complement_path_hash[:full_path]}\" -d \"#{complement_path_hash[:directory]}\" "
    File.delete complement_path_hash[:full_path]
  end
end
