class ElecEventsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /elec_events
  # GET /elec_events.json
  def index
    @elec_events = ElecEvent.where(:elec_process_id => params[:elec_process_id],:elec_recurrence_id => nil)
    render :layout => false
  end

  # GET /elec_events/1
  # GET /elec_events/1.json
  def show
    @elec_event = ElecEvent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @elec_event }
    end
  end

  # GET /elec_events/new
  # GET /elec_events/new.json
  def new
    @elec_process = ElecProcess.find(params[:elec_process_id])
    @elec_event = ElecEvent.new
    @elec_event.elec_process = @elec_process

    set_info_first_round

  end

  # GET /elec_events/1/edit
  def edit
    @elec_event = ElecEvent.find(params[:id])
    set_info_first_round
  end

  # POST /elec_events
  # POST /elec_events.json
  def create
    @elec_event = ElecEvent.new(params[:elec_event])
    set_info_first_round

      if @elec_event.save
        create_or_update_first_elec_round
        flash[:success] = 'El agendamiendo fue creado correctamente'
        redirect_to elec_processes_show_with_open_pill_path(@elec_event.elec_process_id, 'schedule')
      else
        render action: "new"
      end
  end

  # PUT /elec_events/1
  # PUT /elec_events/1.json
  def update
    @elec_event = ElecEvent.find(params[:id])

      if @elec_event.update_attributes(params[:elec_event])
        set_info_first_round
        create_or_update_first_elec_round
        flash[:success] = 'El agendamiendo fue actualizado correctamente'
        redirect_to elec_processes_show_with_open_pill_path(@elec_event.elec_process_id, 'schedule')
      else
        render action: "edit"
      end
  end

  # DELETE /elec_events/1
  # DELETE /elec_events/1.json
  def destroy
    @elec_event = ElecEvent.find(params[:id])
    elec_process_id = @elec_event.elec_process_id
    @elec_event.destroy

    flash[:success] = 'El agendamiendo fue eliminado correctamente'
    redirect_to elec_processes_show_with_open_pill_path(elec_process_id, 'schedule')

  end

  private

  def set_info_first_round
    @first_round = @elec_event.elec_process.elec_process_rounds.first
  end

  def create_or_update_first_elec_round
    if @first_round
      round = ElecRound.where(:elec_event_id => @elec_event.id, :elec_process_round_id => @first_round.id).first_or_initialize()
      to_date = @elec_event.from_date + @first_round.lifespan.minutes

      round.from_date = @elec_event.from_date
      round.to_date = to_date

      round.save!
    end
  end
end
