class KpiMeasurementsController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_dashboard, only:[:create, :update]

  def create

    params[:kpi_measurement][:value] = fix_form_floats params[:kpi_measurement][:value]

    @kpi_measurement = KpiMeasurement.new(params[:kpi_measurement])

    if @kpi_measurement.save

      kpi_set = @kpi_indicator.kpi_set
      kpi_set.set_last_updates lms_time
      kpi_set.save

      flash[:success] =  t('activerecord.success.model.kpi_measurement.create_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard

    else
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard
    end

  end

  def update

    @kpi_measurement = KpiMeasurement.find(params[:id])

    params[:kpi_measurement][:value] = fix_form_floats params[:kpi_measurement][:value]

    if @kpi_measurement.update_attributes(params[:kpi_measurement])

      kpi_set = @kpi_indicator.kpi_set
      kpi_set.set_last_updates lms_time
      kpi_set.save

      flash[:success] =  t('activerecord.success.model.kpi_measurement.update_ok')
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard

    else
      redirect_to kpi_managing_dashboard_manage_indicators_path @kpi_indicator.kpi_set.kpi_dashboard
    end

  end

  private

    def verify_access_manage_dashboard

      @kpi_indicator = KpiIndicator.find(params[:kpi_measurement][:kpi_indicator_id])

      unless user_connected.kpi_dashboard_managers.where('kpi_dashboard_id = ?',@kpi_indicator.kpi_set.kpi_dashboard_id).count > 0
        flash[:danger] = t('security.no_access_manage_kpi_dashboard')
        redirect_to root_path
      end
    end

end
