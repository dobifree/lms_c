class PeCalibrationEvaluationController < ApplicationController

  include PeAssessmentHelper

  before_filter :authenticate_user

  before_filter :get_data_1, only: [:list_sessions]
  before_filter :get_data_2, only: [:show_session, :active_finish_session]
  before_filter :get_data_3, only: [:calibrate]

  before_filter :validate_open_process, only: [:list_sessions, :show_session, :active_finish_session, :calibrate]
  before_filter :validate_is_a_evaluation_pe_cal_committee, only: [:list_sessions]
  before_filter :validate_is_a_session_pe_cal_committee, only: [:show_session]
  before_filter :validate_is_a_session_pe_cal_committee_manager, only: [:active_finish_session, :calibrate]


  def list_sessions



  end

  def show_session

    @is_pe_cal_session_manager = @pe_cal_session.is_pe_cal_committee_manager? user_connected

  end

  def active_finish_session
    @pe_cal_session.active = !@pe_cal_session.active
    if @pe_cal_session.save
      flash[:success] =  t('activerecord.success.model.pe_cal_session.change_state_ok')
    end

    redirect_to pe_calibration_evaluations_show_session_path @pe_cal_session

  end

  def calibrate

    @pe_cal_session.pe_cal_members_ordered.readonly(false).each_with_index do |pe_cal_member, index|

      pe_assessment_final_evaluation = pe_cal_member.pe_member.pe_assessment_final_evaluation(@pe_evaluation)

      if pe_assessment_final_evaluation

        if params['cal_'+pe_cal_member.pe_member.id.to_s] && !params['cal_'+pe_cal_member.pe_member.id.to_s].blank?

          unless pe_assessment_final_evaluation.original_percentage
            pe_assessment_final_evaluation.original_percentage = pe_assessment_final_evaluation.percentage
            pe_assessment_final_evaluation.pe_original_dimension_group = pe_assessment_final_evaluation.pe_dimension_group
          end

          pe_assessment_final_evaluation.percentage = params['cal_'+pe_cal_member.pe_member.id.to_s]

          pe_assessment_final_evaluation.percentage = @pe_evaluation.min_percentage if pe_assessment_final_evaluation.percentage < @pe_evaluation.min_percentage
          pe_assessment_final_evaluation.percentage = @pe_evaluation.max_percentage if pe_assessment_final_evaluation.percentage > @pe_evaluation.max_percentage

          @pe_evaluation.pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

            if pe_assessment_final_evaluation.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_final_evaluation.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
              pe_assessment_final_evaluation.pe_dimension_group = pe_dimension_group
              break
            end

          end

        else
          if params['cal_'+pe_cal_member.pe_member.id.to_s] && params['cal_'+pe_cal_member.pe_member.id.to_s].blank?

            if pe_assessment_final_evaluation.original_percentage
              pe_assessment_final_evaluation.percentage = pe_assessment_final_evaluation.original_percentage
              pe_assessment_final_evaluation.original_percentage = nil
              pe_assessment_final_evaluation.pe_dimension_group = pe_assessment_final_evaluation.pe_original_dimension_group
              pe_assessment_final_evaluation.pe_original_dimension_group = nil
            end


          end
        end

        pe_assessment_final_evaluation.calibration_comment = params['cal_'+pe_cal_member.pe_member.id.to_s+'_comment']

        pe_assessment_final_evaluation.save

        pe_cal_member.step_calibration_date = lms_time
        pe_cal_member.save

        @pe_process.pe_dimensions.each do |pe_dimension|
          calculate_assessment_dimension pe_cal_member.pe_member, pe_dimension
          break unless @pe_process.two_dimensions?
        end



        pe_member_evaluated = pe_cal_member.pe_member

        pe_member_evaluated.set_pe_box
        pe_member_evaluated.save

        calculate_assessment_groups pe_member_evaluated

        unless @pe_process.of_persons?

          pe_area = pe_member_evaluated.pe_area
          pe_area.set_pe_box
          pe_area.save

        end

      end

    end

    redirect_to pe_calibration_evaluations_show_session_path @pe_cal_session

  end


  private

  def get_data_1
    @pe_evaluation = PeEvaluation.find(params[:pe_evaluation_id])
    @pe_process = @pe_evaluation.pe_process
  end

  def get_data_2
    @pe_cal_session = PeCalSession.find(params[:pe_cal_session_id])
    @pe_evaluation = @pe_cal_session.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def get_data_3
    @pe_cal_session = PeCalSession.find(params[:pe_cal_session_id])
    @pe_evaluation = @pe_cal_session.pe_evaluation
    @pe_process = @pe_evaluation.pe_process
  end

  def validate_open_process

    unless @pe_process.active && @pe_process.from_date <= lms_time && lms_time <= @pe_process.to_date
      flash[:danger] = t('activerecord.error.model.pe_process_assessment.out_of_time')
      redirect_to root_path
    end

  end

  def validate_is_a_evaluation_pe_cal_committee

    unless @pe_process.is_a_pe_cal_committee? user_connected
      flash[:danger] = t('activerecord.error.model.pe_process_calibration.is_not_cal_committee')
      redirect_to root_path
    end

  end

  def validate_is_a_session_pe_cal_committee

    unless @pe_cal_session.is_pe_cal_committee? user_connected
      flash[:danger] = t('activerecord.error.model.pe_process_calibration.is_not_session_pe_cal_committee')
      redirect_to root_path
    end

  end

  def validate_is_a_session_pe_cal_committee_manager

    unless @pe_cal_session.is_pe_cal_committee_manager? user_connected
      flash[:danger] = t('activerecord.error.model.pe_process_calibration.is_not_session_pe_cal_committee_manager')
      redirect_to root_path
    end

  end


end
