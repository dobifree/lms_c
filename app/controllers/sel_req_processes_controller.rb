class SelReqProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_manager, except: [:index_for_approver, :show_for_approver, :evaluate]
  before_filter :validates_if_its_closable, only: [:close, :restart]

  # GET /sel_req_processes
  # GET /sel_req_processes.json
  def index
    @sel_req_processes = SelReqProcess.all
  end

  # GET /sel_req_processes/1
  # GET /sel_req_processes/1.json
  def show
    @sel_req_process = SelReqProcess.find(params[:id])
  end

  # GET /sel_req_processes/new
  # GET /sel_req_processes/new.json
  def new
    @sel_req_process = SelReqProcess.new
  end

  # GET /sel_req_processes/1/edit
  def edit
    @sel_req_process = SelReqProcess.find(params[:id])
    @sel_req_approvers = []
    @not_startable = true
    sel_req_approver_ids_found = []
    if params[:sel_vacant_flow_id]
      @template_id = params[:sel_vacant_flow_id]
      template = SelVacantFlow.find(@template_id)
      template.sel_flow_approvers.each do |flow_approver|
        if flow_approver.approver_type == 0 # usuario específico
          found = false
          @sel_req_process.sel_req_approvers.where(user_id: flow_approver.sel_flow_specific_approver.user_id).each do |approver|
            approver.position = flow_approver.position
            approver.mandatory = flow_approver.mandatory
            approver.conditions_rejection = flow_approver.conditions_rejection
            approver.value = flow_approver.weight
            approver.name = flow_approver.name
            found = true
            sel_req_approver_ids_found << approver.id
            @sel_req_approvers << approver
          end
          unless found
            @sel_req_process.sel_req_approvers.new(user_id: flow_approver.sel_flow_specific_approver.user_id) do |approver|
              approver.position = flow_approver.position
              approver.mandatory = flow_approver.mandatory
              approver.conditions_rejection = flow_approver.conditions_rejection
              approver.value = flow_approver.weight
              approver.name = flow_approver.name
              @sel_req_approvers << approver
            end
          end

        end
      end
      @sel_req_process.sel_req_approvers.each do |approver|
        @sel_req_approvers << approver if !approver.new_record? && !sel_req_approver_ids_found.include?(approver.id)
      end
    elsif @sel_req_process.sel_req_approvers.any?
      @sel_req_approvers = @sel_req_process.sel_req_approvers.all
    elsif prev_process = @sel_req_process.sel_requirement.sel_req_processes.where(active: false).last
      prev_process.sel_req_approvers.each do |sel_req_approver|
        @sel_req_approvers << sel_req_approver.dup
      end
    end
  end

  # POST /sel_req_processes
  # POST /sel_req_processes.json
  def create
    @sel_req_process = SelReqProcess.new(params[:sel_req_process])

    respond_to do |format|
      if @sel_req_process.save
        format.html {redirect_to @sel_req_process, notice: 'Sel req process was successfully created.'}
        format.json {render json: @sel_req_process, status: :created, location: @sel_req_process}
      else
        format.html {render action: "new"}
        format.json {render json: @sel_req_process.errors, status: :unprocessable_entity}
      end
    end
  end

  # PUT /sel_req_processes/1
  # PUT /sel_req_processes/1.json
  def update
    @sel_req_process = SelReqProcess.find(params[:id])

    respond_to do |format|
      if @sel_req_process.update_attributes(params[:sel_req_process])
        format.html {redirect_to @sel_req_process, notice: 'Sel req process was successfully updated.'}
        format.json {head :no_content}
      else
        format.html {render action: "edit"}
        format.json {render json: @sel_req_process.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /sel_req_processes/1
  # DELETE /sel_req_processes/1.json
  def destroy
    @sel_req_process = SelReqProcess.find(params[:id])
    @sel_req_process.destroy

    respond_to do |format|
      format.html {redirect_to sel_req_processes_url}
      format.json {head :no_content}
    end
  end

  def start
    @sel_req_process = SelReqProcess.find(params[:id])
    if @sel_req_process.total_config_value_approvers >= @sel_req_process.value_to_approval
      @sel_req_process.started = true
      @sel_req_process.started_at = lms_time
      @sel_req_process.started_by_user = user_connected
      @sel_req_process.save
      in_approvation_log = @sel_req_process.sel_requirement.sel_req_logs.build
      in_approvation_log.log_state_id = 6 # en aprobación
      in_approvation_log.registered_at = lms_time
      in_approvation_log.registered_by_user = user_connected
      in_approvation_log.save
      flash[:success] = 'Proceso iniciado correctamente'
    else
      flash[:danger] = 'Debe configurar el peso de los aprobadores de tal manera que superen el umbrarl de aprobación (' + @sel_req_process.value_to_approval + ')'
    end
    redirect_to @sel_req_process

  end

  def index_for_approver
    @sel_req_processes = SelReqProcess.where(started: true, active: true).joins(:sel_req_approvers).where(sel_req_approvers: {user_id: user_connected.id})
  end

  def show_for_approver
    @sel_req_process = SelReqProcess.find(params[:id])
    @sel_req_approver = @sel_req_process.sel_req_approvers.where(user_id: user_connected.id).first
    @sel_req_approvers_done = @sel_req_process.sel_req_approvers.joins(:sel_req_approver_eval)
    unless @sel_req_approver
      flash[:danger] = 'Usted no está autorizado a evaluar este proceso.'
      redirect_to sel_req_index_for_approver_path
    end
  end

  def evaluate
    @sel_req_process = SelReqProcess.find(params[:id])
    sel_req_approver = SelReqApprover.find(params[:sel_req_approver_id])
    if sel_req_approver && sel_req_approver.user == user_connected && sel_req_approver.sel_req_approver_eval.nil?

      eval = sel_req_approver.build_sel_req_approver_eval(params[:sel_req_approver_eval])
      eval.registered_at = lms_time
      eval.save

      post_evaluation(eval)
      flash[:success] = 'Evaluación realizada correctamente'
      redirect_to sel_req_show_for_approver_path(@sel_req_process)
    else
      flash[:danger] = 'Usted no está autorizado a evaluar este proceso.'
    end
  end

  def close
    # cerrar el proceso
    begin
      @sel_req_process.active = false
      @sel_req_process.transaction do
        @sel_req_process.save!
        # generar el log que cambia el estado previo del 'en atención' (registrado)
        log = @sel_req_process.sel_requirement.sel_req_logs.new
        log.log_state_id = 1 # registrada
        log.registered_at = lms_time
        log.registered_by_user = user_connected
        #log.comment = 'Registro automático (eliminar proceso)'
        log.save!
      end
      flash[:success] = 'Proceso eliminado correctamente.'
    rescue Exception => exception
      #puts exception
      #puts exception.backtrace
      raise
      flash[:danger] = 'Ocurrió un problema al intentar Eliminar el proceso, por favor intente nuevamente.'
    end
    redirect_to sel_requirements_unattended_path
  end

  def restart
    # cerrar el proceso
    begin
      @sel_req_process.active = false
      @sel_req_process.transaction do
        @sel_req_process.save!

        # generar nuevo requerimiento
        sel_requirement = @sel_req_process.sel_requirement
        #sel_requirement.sel_req_processes.where(active: true).update_all(active: false)
        new_sel_req_process = sel_requirement.sel_req_processes.build
        new_sel_req_process.registered_at = lms_time
        new_sel_req_process.registered_by_user_id = user_connected.id
        new_sel_req_process.active = true
        new_sel_req_process.save!

        # generación del log de 'en atención' de la solicitud
        if sel_requirement && sel_requirement.current_state_log.log_state_id != 5
          log = sel_requirement.sel_req_logs.new
          log.log_state_id = 5
          log.registered_at = lms_time
          log.registered_by_user_id = user_connected.id
          log.save!
        end
        # generación del log de 'en atención' de la solicitud
        #  flash[:success] = 'Proceso de aprobación de solicitud reiniciado correctamente. Por favor configure los evaluadores.'
        redirect_to edit_sel_req_process_path new_sel_req_process
      end
    rescue Exception => exception
      #puts exception
      #puts exception.backtrace
      raise
      flash[:danger] = 'Ocurrió un problema al intentar reiniciar el proceso, por favor intente nuevamente.'
      redirect_to @sel_req_process
    end
  end

  def notify_assign_approver
    sel_req_process = SelReqProcess.find(params[:id])
    notification_template = SelNotificationTemplate.where(active: true, event_id: 14).first
    if notification_template
      sel_req_approvers = []
      if params[:sel_req_approver_id]
        approver = SelReqApprover.find(params[:sel_req_approver_id])
        sel_req_approvers << approver unless approver.sel_req_approver_eval
      else
        sel_req_process.sel_req_approvers.each do |approver|
          sel_req_approvers << approver unless approver.sel_req_approver_eval
        end
      end
      sel_req_approvers.each do |approver|
        notification = notification_template.generate_notification({sel_req_approver: approver, trigger_user: user_connected}, user_connected, lms_time)
        SelMailer.sel_notification(notification, @company).deliver
      end
      flash[:success] = 'Notificación enviada correctamente'
    else
      flash[:danger] = 'No es posible enviar una notificación.'
    end
    redirect_to sel_req_process
  end

  private

  def post_evaluation(trigger_evaluation)
    must_finish_process = false
    total_approved_value = 0
    total_to_eval_value = 0
    total_to_eval_required_value = 0
    final_evaluation = false

    evaluations = @sel_req_process.sel_req_approvers.joins(:sel_req_approver_eval)

    # existe una evaluación condicionante ya rechazada
    if !must_finish_process && evaluations.where(sel_req_approvers: {conditions_rejection: true}, sel_req_approver_evals: {evaluation: false}).any?
      must_finish_process = true
      final_evaluation = false
    end

    # no es aprobable (la suma de lo que falta para evaluar, mas los que ya fue aprobado no iguala o supera el valor para aprobar)
    unless must_finish_process
      @sel_req_process.sel_req_approvers.each do |approver|
        if approver.sel_req_approver_eval
          total_approved_value += approver.value if approver.sel_req_approver_eval.evaluation
        else
          total_to_eval_value += approver.value
          total_to_eval_required_value += approver.value if approver.mandatory?
        end
      end
      if total_approved_value + total_to_eval_value < @sel_req_process.value_to_approval
        must_finish_process = true
        final_evaluation = false
      end
    end

    # es aprobable porque se igual o supera el valor para aprobar, y no faltan evaluaciones obligatorias.
    unless must_finish_process
      if total_approved_value >= @sel_req_process.value_to_approval && total_to_eval_required_value == 0
        must_finish_process = true
        final_evaluation = true
      end
    end
    finish_process_evaluation(final_evaluation, trigger_evaluation) if must_finish_process
  end

  def finish_process_evaluation(final_evaluation, trigger_evaluation)
    evaluation_log = @sel_req_process.sel_requirement.sel_req_logs.new()
    evaluation_log.log_state_id = final_evaluation ? 2 : 8 # aprobada : rechazada
    evaluation_log.registered_at = lms_time
    evaluation_log.registered_by_user = trigger_evaluation.sel_req_approver.user
    evaluation_log.comment = 'Registro automático (proceso de evaluación)'
    evaluation_log.save
  end

  def authenticate_manager
    unless ct_module_sel_manager?
      flash[:danger] = t('security.no_access_generic')
      redirect_to root_path
    end
  end

  def validates_if_its_closable
    @sel_req_process = SelReqProcess.find(params[:id])
    if @sel_req_process.sel_requirement.is_attended?
      flash[:danger] = 'No se pueden realizar cambios en un proceso que ya está siendo atendido con un proceso de Selección'
      redirect_to sel_requirements_unattended_path
    end
  end

end
