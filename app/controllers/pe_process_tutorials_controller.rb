class PeProcessTutorialsController < ApplicationController
  # GET /pe_process_tutorials
  # GET /pe_process_tutorials.json

  def show
    @pe_process_tutorial = PeProcessTutorial.find(params[:id])
    @pe_process = @pe_process_tutorial.pe_process
  end

  def new

    @pe_process = PeProcess.find(params[:pe_process_id])
    @pe_process_tutorial = @pe_process.pe_process_tutorials.build

  end

  def create
    @pe_process_tutorial = PeProcessTutorial.new(params[:pe_process_tutorial])

    if @pe_process_tutorial.save
      redirect_to pe_process_tab_url(@pe_process_tutorial.pe_process, 'steps')
    else
      @pe_process = @pe_process_tutorial.pe_process
      render action: 'new'

    end

  end

  def edit
    @pe_process_tutorial = PeProcessTutorial.find(params[:id])
    @pe_process = @pe_process_tutorial.pe_process
  end


  def update
    @pe_process_tutorial = PeProcessTutorial.find(params[:id])

      if @pe_process_tutorial.update_attributes(params[:pe_process_tutorial])
        redirect_to @pe_process_tutorial
      else
        render action: 'edit'
      end

  end

  def destroy

    @pe_process_tutorial = PeProcessTutorial.find(params[:id])

    pe_process = @pe_process_tutorial.pe_process

    @pe_process_tutorial.destroy

    redirect_to pe_process_tab_url(pe_process, 'steps')

  end

end
