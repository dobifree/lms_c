class TrainingReportsController < ApplicationController

  include ActionView::Helpers::NumberHelper
  include ExcelReportsHelper
  include CsvReportsHelper

  before_filter :authenticate_user
  before_filter :verify_programs_inspector_jefe_cap
  before_filter :verify_program_inspector_jefe_cap, only: [:indicadores_curso]
  before_filter :verify_programs_inspector_jefe_cap, only: [:indicadores_cursos_consolidado]

  def index


  end

  def index_dnc_1

    @planning_processes = PlanningProcess.all

  end

  def index_dnc_2

    @planning_process = PlanningProcess.find(params[:planning_process_id])


  end

  def index_dnc

    @planning_process = PlanningProcess.find(params[:planning_process_id])
    @planning_process_company_unit = PlanningProcessCompanyUnit.find(params[:planning_process_company_unit_id])


  end

  def reporte_general

   company = @company

    programs = Program.all

    courses = Array.new
    program_courses = Array.new
    programs_abiertos_ids = Array.new

    query = ''
    n_query = 0

    programs.each do |program|

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            unless program.especifico
              cargar_abierto = true
              program_courses.push program_course
            end

            courses.push program_course.course

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

    end

    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2], desde_d[1], desde_d[0], 0, 0, 0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2], hasta_d[1], hasta_d[0], 23, 59, 59)

    ultimo_estado = false

    if query != ''

      if params[:solo_ultimo_estado] && params[:solo_ultimo_estado] == '1'

        ultimo_estado = true

        user_courses_ids = case params[:tipo_fecha]
                             when '1'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '2'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '3'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '4'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '5'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '6'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '7'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           end

        user_courses = Array.new

        user_courses_ids.each do |uc_id|

          uc_tmp = UserCourse.find(uc_id.id)

          estado_tmp = uc_tmp.estado lms_time

          if estado_tmp == :aprobado || estado_tmp == :reprobado
            user_courses << uc_tmp
          else
            user_courses_tmp = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', uc_tmp.enrollment_id, uc_tmp.program_course_id, false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).reorder('id DESC')

            estado_reprobado = false
            user_courses_tmp.each do |uc_tmp_r|

              if uc_tmp_r.iniciado && uc_tmp_r.finalizado && !uc_tmp_r.aprobado

                estado_reprobado = true
                user_courses << uc_tmp_r
                break

              end

            end

            unless estado_reprobado
              user_courses << uc_tmp
            end

          end


        end

      else

        user_courses = case params[:tipo_fecha]
                         when '1'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       end

      end


    else
      user_courses = Array.new
    end

    enrollments = Enrollment.where('program_id IN (?)', programs_abiertos_ids).joins(:user).reorder('apellidos, nombre')


    #if params[:reporte_general_pivote].blank?
      reporte_general_csv enrollments, user_courses, program_courses, courses, ultimo_estado, false, nil
    #else
    #  files_to_zip = {}
    #  UserCharacteristic.where(:characteristic_id => params[:reporte_general_pivote]).pluck('DISTINCT valor').each do |valor|
    #    pivot = {params[:reporte_general_pivote] => valor}
    #    files_to_zip[generate_reporte_general_csv(enrollments, user_courses, program_courses, courses, ultimo_estado, false, pivot)] = valor
    #  end
    #  reporte_general_csv_con_pivote(files_to_zip, ultimo_estado)


    #end

  end

  def reporte_det_evals

   company = @company

    programs = Program.all

    courses = Array.new
    program_courses = Array.new
    programs_abiertos_ids = Array.new

    query = ''
    n_query = 0

    programs.each do |program|

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            unless program.especifico
              cargar_abierto = true
              program_courses.push program_course
            end

            courses.push program_course.course

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

    end

    desdehasta = params[:desdehasta].split('-')

    desde_t = desdehasta[0].strip
    desde_d = desde_t.split('/').map { |s| s.to_i }

    desde = DateTime.new(desde_d[2], desde_d[1], desde_d[0], 0, 0, 0)

    hasta_t = desdehasta[1].strip
    hasta_d = hasta_t.split('/').map { |s| s.to_i }

    hasta = DateTime.new(hasta_d[2], hasta_d[1], hasta_d[0], 23, 59, 59)

    ultimo_estado = false

    if query != ''

      if params[:solo_ultimo_estado] && params[:solo_ultimo_estado] == '1'

        ultimo_estado = true

        user_courses_ids = case params[:tipo_fecha]
                             when '1'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '2'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '3'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '4'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '5'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '6'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                             when '7'
                               UserCourse.select('MAX(user_courses.id) AS id').where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).group('enrollment_id, program_course_id').reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                           end

        user_courses = Array.new

        user_courses_ids.each do |uc_id|

          uc_tmp = UserCourse.find(uc_id.id)

          estado_tmp = uc_tmp.estado lms_time

          if estado_tmp == :aprobado || estado_tmp == :reprobado
            user_courses << uc_tmp
          else
            user_courses_tmp = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', uc_tmp.enrollment_id, uc_tmp.program_course_id, false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).reorder('id DESC')

            estado_reprobado = false
            user_courses_tmp.each do |uc_tmp_r|

              if uc_tmp_r.iniciado && uc_tmp_r.finalizado && !uc_tmp_r.aprobado

                estado_reprobado = true
                user_courses << uc_tmp_r
                break

              end

            end

            unless estado_reprobado
              user_courses << uc_tmp
            end

          end


        end

      else

        user_courses = case params[:tipo_fecha]
                         when '1'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '2'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?))', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '3'
                           UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '4'
                           UserCourse.where('('+query+') AND anulado = ? AND ((hasta >= ? AND hasta <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '5'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '6'
                           UserCourse.where('('+query+') AND anulado = ? AND ((inicio >= ? AND inicio <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                         when '7'
                           UserCourse.where('('+query+') AND anulado = ? AND ((fin >= ? AND fin <= ?))', false, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, user_courses.id DESC')
                       end

      end


    else
      user_courses = Array.new
    end

    enrollments = Enrollment.where('program_id IN (?)', programs_abiertos_ids).joins(:user).reorder('apellidos, nombre')


    #if params[:reporte_general_pivote].blank?
    reporte_det_evals_csv enrollments, user_courses, program_courses, courses, ultimo_estado, false, nil
    #else
    #  files_to_zip = {}
    #  UserCharacteristic.where(:characteristic_id => params[:reporte_general_pivote]).pluck('DISTINCT valor').each do |valor|
    #    pivot = {params[:reporte_general_pivote] => valor}
    #    files_to_zip[generate_reporte_general_csv(enrollments, user_courses, program_courses, courses, ultimo_estado, false, pivot)] = valor
    #  end
    #  reporte_general_csv_con_pivote(files_to_zip, ultimo_estado)


    #end

  end

  def reporte_cursos_excel_rango

   company = @company

    programs = Program.all

    query = ''
    n_query = 0

    programs.each do |program|

      program.levels.each do |level|

        level.courses.each_with_index do |course|

          program_course = program.program_courses.where('level_id = ? AND course_id = ? ', level.id, course.id).first

          if params["pc_#{program_course.id}".to_sym] == '1'

            if n_query == 0
              query += ' program_course_id = '+program_course.id.to_s+' '
              n_query += 1
            else
              query += ' OR program_course_id = '+program_course.id.to_s+' '

            end

          end

        end

      end

    end

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2], desde_d[1], desde_d[0], 0, 0, 0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2], hasta_d[1], hasta_d[0], 23, 59, 59)

    end

    if query != ''

      if rango_fechas
        user_courses = UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')
      else
        user_courses = UserCourse.where('('+query+') AND anulado = ? ', false).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')
      end
    else
      user_courses = Array.new
    end

    if rango_fechas
      reporte_excel = reporte_curso_excel('Reporte', nil, user_courses, company, rango_fechas, desde_t, hasta_t, false)
    else
      reporte_excel = reporte_curso_excel('Reporte', nil, user_courses, company, nil, nil, nil, false)
    end

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Reporte General -'+(localize desde, format: :day_snmonth)+'-'+(localize hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def certificados_cursos_rango

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])

    directory = company.directorio+'/'+company.codigo+'/plantillas_certificados_cursos/'

    rango_fechas = false

    if params[:desdehasta_cert]

      rango_fechas = true

      desdehasta = params[:desdehasta_cert].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2], desde_d[1], desde_d[0], 0, 0, 0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2], hasta_d[1], hasta_d[0], 23, 59, 59)

    end

    if rango_fechas

      require 'prawn/measurement_extensions'

      directorio_temporal = '/tmp/'
      nombre_temporal = SecureRandom.hex(5)+'.pdf'
      archivo_temporal = directorio_temporal+nombre_temporal

      user_connected_time_zone_pdf = user_connected_time_zone

      Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: [720, 540]) do |pdf|

        pdf.font_families.update('Foco Light' => {:normal => '/var/www/remote-storage/fonts/Foco_Std_Lt_0.ttf'})
        pdf.font_families.update('HaasGrotDisp Light' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-45Light_1.ttf'})
        pdf.font_families.update('HaasGrotDisp Normal' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-65Medium_1.ttf'})
        pdf.font_families.update('HaasGrotDisp Bold' => {:normal => '/var/www/remote-storage/fonts/HaasGrotDisp-75Bold_1.ttf'})

        program_course.matriculas_aprobadas_rango_fechas(desde, hasta).each do |user_course|

          cc = user_course.course.course_certificates.where('rango_desde <= ? AND rango_hasta >= ?', user_course.fin, user_course.fin).first

          if cc && cc.template

            plantilla = directory+(cc.template)+'.jpg'

            pdf.start_new_page
            pdf.image plantilla, at: [-36, 504], width: 720

            nombre = user_course.user.nombre+' '+user_course.user.apellidos

            nombre_curso = user_course.course.nombre

            nota = ''

            if user_course.nota
              if user_course.nota.to_i == user_course.nota
                nota = user_course.nota.to_i
              else
                nota = user_course.nota
              end
            end

            nota = nota.to_s

            desde = ''
            desde = I18n.localize(user_course.desde, format: cc.desde_formato.to_sym) if user_course.desde
            hasta = ''
            hasta = I18n.localize(user_course.hasta, format: cc.hasta_formato.to_sym) if user_course.hasta

            inicio = ''
            inicio = I18n.localize(user_course.inicio, format: cc.inicio_formato.to_sym) if user_course.inicio
            fin = ''
            fin = I18n.localize(user_course.fin, format: cc.fin_formato.to_sym) if user_course.fin

            fecha = user_course.fin
            fecha = user_course.hasta if user_course.hasta && cc.fecha_source == 'fin_convocatoria'

            fecha = fecha+cc.fecha_source_delay.days
            fecha = I18n.localize(fecha, format: :full_date)


            if cc.nombre
              if cc.nombre_x == -1
                pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :size => cc.nombre_size}], :at => [0, cc.nombre_y.mm], :align => :center
              else
                pdf.formatted_text_box [{text: nombre, :color => cc.nombre_color, :font => cc.nombre_letra, :size => cc.nombre_size}], :at => [cc.nombre_x.mm, cc.nombre_y.mm]
              end
            end

            if cc.nombre_curso

              if cc.nombre_curso_x == -1
                pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :size => cc.nombre_curso_size}], :at => [0, cc.nombre_curso_y.mm], :align => :center
              else
                pdf.formatted_text_box [{text: nombre_curso, :color => cc.nombre_curso_color, :font => cc.nombre_curso_letra, :size => cc.nombre_curso_size}], :at => [cc.nombre_curso_x.mm, cc.nombre_curso_y.mm]
              end
            end

            if cc.fecha
              if cc.fecha_x == -1
                pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :size => cc.fecha_size}], :at => [0, cc.fecha_y.mm], :align => :center
              else
                pdf.formatted_text_box [{text: fecha, :color => cc.fecha_color, :font => cc.fecha_letra, :size => cc.fecha_size}], :at => [cc.fecha_x.mm, cc.fecha_y.mm]
              end
            end

            if cc.desde
              if cc.desde_x == -1
                pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :size => cc.desde_size}], :at => [0, cc.desde_y.mm], :align => :center
              else
                pdf.formatted_text_box [{text: desde, :color => cc.desde_color, :font => cc.desde_letra, :size => cc.desde_size}], :at => [cc.desde_x.mm, cc.desde_y.mm]
              end
            end

            if cc.hasta
              if cc.hasta_x == -1
                pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :size => cc.hasta_size}], :at => [0, cc.hasta_y.mm], :align => :center
              else
                pdf.formatted_text_box [{text: hasta, :color => cc.hasta_color, :font => cc.hasta_letra, :size => cc.hasta_size}], :at => [cc.hasta_x.mm, cc.hasta_y.mm]
              end
            end

            if cc.inicio
              if cc.inicio_x == -1
                pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :size => cc.inicio_size}], :at => [0, cc.inicio_y.mm], :align => :center
              else
                pdf.formatted_text_box [{text: inicio, :color => cc.inicio_color, :font => cc.inicio_letra, :size => cc.inicio_size}], :at => [cc.inicio_x.mm, cc.inicio_y.mm]
              end
            end

            if cc.fin
              if cc.fin_x == -1
                pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :size => cc.fin_size}], :at => [0, cc.fin_y.mm], :align => :center
              else
                pdf.formatted_text_box [{text: fin, :color => cc.fin_color, :font => cc.fin_letra, :size => cc.fin_size}], :at => [cc.fin_x.mm, cc.fin_y.mm]
              end
            end

            if cc.nota
              if cc.nota_x == -1
                pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :size => cc.nota_size}], :at => [0, cc.nota_y.mm], :align => :center
              else
                pdf.formatted_text_box [{text: nota, :color => cc.nota_color, :font => cc.nota_letra, :size => cc.nota_size}], :at => [cc.nota_x.mm, cc.nota_y.mm]
              end
            end

          end

        end

      end

      send_file archivo_temporal,
                filename: 'Certificados - '+program_course.course.nombre+'.pdf',
                type: 'application/octet-stream',
                disposition: 'attachment'


    end

  end

  def reporte_encuesta_satisfaccion

   company = @company

    program_course = ProgramCourse.find(params[:program_course_id])

    rango_fechas = false

    if params[:desdehasta]

      rango_fechas = true

      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      desde = DateTime.new(desde_d[2], desde_d[1], desde_d[0], 0, 0, 0)

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      hasta = DateTime.new(hasta_d[2], hasta_d[1], hasta_d[0], 23, 59, 59)

    end

    query = ' program_course_id = '+program_course.id.to_s+' '

    if query != ''
      if rango_fechas
        user_courses = UserCourse.where('('+query+') AND anulado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', false, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')

      else
        user_courses = UserCourse.where('('+query+') AND anulado = ? ', false).joins(:user).reorder('apellidos, nombre, program_course_id, numero_oportunidad')
      end
    else
      user_courses = Array.new
    end

    if rango_fechas
      reporte_excel = reporte_encuesta_satisfaccion_excel('Reporte', program_course, user_courses, company, rango_fechas, desde, hasta, desde_t, hasta_t)
    else
      reporte_excel = reporte_encuesta_satisfaccion_excel('Reporte', program_course, user_courses, company, nil, nil, nil, nil, nil)
    end

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.xlsx'
    archivo_temporal = directorio_temporal+nombre_temporal

    reporte_excel.serialize archivo_temporal

    send_file archivo_temporal,
              filename: 'Encuesta de Satisfacción - '+(localize desde, format: :day_snmonth)+'-'+(localize hasta, format: :day_snmonth)+'.xlsx',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def lista_cursos

  end

  def indicadores_curso

    if params[:desdehasta]


      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      @desde = Date.new(desde_d[2], desde_d[1], desde_d[0])

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      @hasta = Date.new(hasta_d[2], hasta_d[1], hasta_d[0])

    else

      @desde = Date.new(((lms_time-11.month).strftime '%Y').to_i, ((lms_time-11.month).strftime '%m').to_i)
      if lms_time.strftime('%m').to_i == 12
        @hasta = Date.new(((lms_time).strftime '%Y').to_i, 12, 31)
      else
        @hasta = Date.new(((lms_time).strftime '%Y').to_i, ((lms_time).strftime '%m').to_i + 1, 1) - 1.day
      end


    end

  end

  def indicadores_cursos_consolidado

    if params[:desdehasta]


      desdehasta = params[:desdehasta].split('-')

      desde_t = desdehasta[0].strip
      desde_d = desde_t.split('/').map { |s| s.to_i }

      @desde = Date.new(desde_d[2], desde_d[1], desde_d[0])

      hasta_t = desdehasta[1].strip
      hasta_d = hasta_t.split('/').map { |s| s.to_i }

      @hasta = Date.new(hasta_d[2], hasta_d[1], hasta_d[0])

    else

      @desde = Date.new(((lms_time-11.month).strftime '%Y').to_i, ((lms_time-11.month).strftime '%m').to_i)
      if lms_time.strftime('%m').to_i == 12
        @hasta = Date.new(((lms_time).strftime '%Y').to_i, 12, 31)
      else
        @hasta = Date.new(((lms_time).strftime '%Y').to_i, ((lms_time).strftime '%m').to_i + 1, 1) - 1.day
      end

    end

  end

  private

  def verify_programs_inspector_jefe_cap

    if user_connected.program_inspectors.count == 0 && !user_connected.jefe_capacitacion?
      flash[:danger] = t('security.no_access_training_reports')
      redirect_to root_path
    end

  end

  def verify_program_inspector_jefe_cap
    @program_course = ProgramCourse.find params[:program_course_id]
    if user_connected.program_inspectors.where('program_id = ? ', @program_course.program.id).size == 0 && !user_connected.jefe_capacitacion?
      flash[:danger] = t('security.no_access_training_reports')
      redirect_to root_path
    end
  end

end
