class ElecCharacteristicsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /elec_characteristics
  # GET /elec_characteristics.json
  def index
    @elec_characteristics = ElecCharacteristic.find_all_by_elec_process_id(params[:elec_process_id])

    render :layout => false
  end

  # GET /elec_characteristics/1
  # GET /elec_characteristics/1.json
  def show
    @elec_characteristic = ElecCharacteristic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @elec_characteristic }
    end
  end

  # GET /elec_characteristics/new
  # GET /elec_characteristics/new.json
  def new
    @elec_process = ElecProcess.find(params[:elec_process_id])
    @elec_characteristic = ElecCharacteristic.new

    @elec_characteristic.elec_process = @elec_process

    @characteristics = unused_characteristics

  end

  # GET /elec_characteristics/1/edit
  def edit
    @elec_characteristic = ElecCharacteristic.find(params[:id])
    @characteristics = unused_characteristics
  end

  # POST /elec_characteristics
  # POST /elec_characteristics.json
  def create
    @elec_characteristic = ElecCharacteristic.new(params[:elec_characteristic])

      if @elec_characteristic.save
        flash[:success] = 'La característica fue configurada correctamente'
        redirect_to elec_processes_show_with_open_pill_path(@elec_characteristic.elec_process_id, 'characteristics')
      else
        @characteristics = unused_characteristics
        render action: "new"
      end
  end

  # PUT /elec_characteristics/1
  # PUT /elec_characteristics/1.json
  def update
    @elec_characteristic = ElecCharacteristic.find(params[:id])

      if @elec_characteristic.update_attributes(params[:elec_characteristic])
        flash[:success] = 'La característica fue actualizada correctamente'
        redirect_to elec_processes_show_with_open_pill_path(@elec_characteristic.elec_process_id, 'characteristics')
      else
        render action: "edit"
      end
  end

  # DELETE /elec_characteristics/1
  # DELETE /elec_characteristics/1.json
  def destroy
    @elec_characteristic = ElecCharacteristic.find(params[:id])
    elec_process_id = @elec_characteristic.elec_process_id
    @elec_characteristic.destroy

    flash[:success] = 'La característica fue eliminada correctamente'
    redirect_to elec_processes_show_with_open_pill_path(elec_process_id, 'characteristics')

  end

  private
  def unused_characteristics
    Characteristic.all.map{|characteristic| [characteristic.nombre, characteristic.id]}
  end
end
