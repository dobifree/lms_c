class LiqProcessesController < ApplicationController

  before_filter :authenticate_user
  before_filter :verify_access_manage_module

  # GET /liq_processes
  # GET /liq_processes.json
  def index
    @liq_processes = LiqProcess.all
  end

  # GET /liq_processes/1
  # GET /liq_processes/1.json
  def show
    @liq_process = LiqProcess.find(params[:id])
  end

  def show_detail
    @liq_process = LiqProcess.find(params[:id])
  end

  # GET /liq_processes/new
  # GET /liq_processes/new.json
  def new
    @liq_process = LiqProcess.new
  end

  # GET /liq_processes/1/edit
  def edit
    @liq_process = LiqProcess.find(params[:id])

    unless @liq_process.is_editable?
      flash[:danger] = 'El proceso ya tiene datos cargados, ya no se puede editar.'
      redirect_to @liq_process
    end
  end

  # POST /liq_processes
  # POST /liq_processes.json
  def create
    @liq_process = LiqProcess.new(params[:liq_process])

      if @liq_process.save
        flash[:success] = 'El Proceso fue creado correctamente'
        redirect_to @liq_process
      else
        flash.now[:danger] = 'El proceso no pudo ser creado correctamente'
        render action: "new"
      end
  end

  # PUT /liq_processes/1
  # PUT /liq_processes/1.json
  def update
    @liq_process = LiqProcess.find(params[:id])

    if !@liq_process.is_editable?
      flash[:danger] = 'El proceso ya tiene datos cargados, ya no se puede editar.'
      redirect_to @liq_process
    else
        if @liq_process.update_attributes(params[:liq_process])
          flash[:success] = 'El Proceso fue actualizado correctamente'
          redirect_to @liq_process
        else
          flash.now[:danger] = 'El proceso no pudo ser actualizado correctamente'
          render action: "edit"
        end
      end
  end

  # DELETE /liq_processes/1
  # DELETE /liq_processes/1.json
  def destroy
    @liq_process = LiqProcess.find(params[:id])
    if @liq_process.is_deletable?
      @liq_process.destroy
      flash[:success] = 'El proceso fue eliminado correctamente.'
      redirect_to liq_processes_path
    else
      flash[:danger] = 'El proceso ya es utilizado por otros procesos (BRG, BRC) y no puede ser eliminado.'
      redirect_to @liq_process
    end
  end

  def clean_items
    liq_process = LiqProcess.find(params[:id])
    #liq_process.liq_items.destroy_all
    LiqItem.delete_all(liq_process_id: liq_process.id)

    flash[:success] = 'Items eliminados correctamente'
    redirect_to liq_process
  end

  def load_data_from_payroll
    @liq_process = LiqProcess.find(params[:id])
    if File.extname(params['payroll_file'].original_filename) == '.xlsx'

      directorio_temporal = '/tmp/'
      nombre_temporal = SecureRandom.hex(5) + '.xlsx'
      archivo_temporal = directorio_temporal + nombre_temporal
      path = File.join(directorio_temporal, nombre_temporal)
      File.open(path, 'wb') {|f| f.write(params['payroll_file'].read)}

      should_save = true
      @excel_errors = []

      begin
        file = RubyXL::Parser.parse archivo_temporal
        data = file.worksheets[0].extract_data
      rescue Exception => e
        data = []
        should_save = false
        @excel_errors << ['-', 'El archivo no tiene el formato correcto.', '-', '-']
      end

      users_hash = User.all.map{ |user| [user.codigo, user.id] }.to_h
      cohades_hash = SecuCohade.all.map{ |cohade| [cohade.name, cohade.id] }.to_h

      rut_index = 0
      period_validator_index = 1
      cohade_index = 3
      description_index = 5
      monto_index = 7
      monto_o_index = 8
      cod_empresa_index = 12
      cod_cecos_index = 14

      liq_items_array = []

      data.each_with_index do |row, index|
        should_save_this = true
        next if index.zero?

        break if !row[rut_index]

        rut_formatted = row[rut_index].to_s.gsub('-', '').gsub(' ', '')
        cohade = row[cohade_index].to_s.gsub(' ', '')

        user_exists = users_hash.has_key?(rut_formatted) if row[rut_index] || !row[rut_index].to_s.empty?
        secu_cohade_exists = cohades_hash.has_key?(cohade)
        period_year = row[period_validator_index].to_s[0..3].to_i
        period_month = row[period_validator_index].to_s[4..5].to_i
        period_validation = @liq_process.month == period_month && @liq_process.year == period_year

        if !user_exists
          should_save_this = false
          @excel_errors << [(index + 1).to_s, 'El ' + alias_username + ' no existe (' + rut_formatted + ')', row[rut_index]]
        end

        if !period_validation
          should_save_this = false
          @excel_errors << [(index + 1).to_s, 'El periodo no corresponde (' + row[period_validator_index].to_s + ')', row[rut_index]]
        end

        if !secu_cohade_exists
          should_save_this = false
          @excel_errors << [(index + 1).to_s, 'El cohade no corresponde con ninguno de los registrados (' + cohade + ')', row[rut_index]]
        end

        if row[description_index].blank?
          should_save_this = false
          @excel_errors << [(index + 1).to_s, 'La descripción no puede ser vacía', row[rut_index]]
        end

        if user_exists && (!row[monto_index] || !row[monto_o_index])
          should_save_this = false
          @excel_errors << [(index + 1).to_s, 'El monto no puede ser vacío', row[rut_index]]
        end

        if should_save_this
          item = LiqItem.new(liq_process_id: @liq_process.id, user_id: users_hash[rut_formatted], secu_cohade_id: cohades_hash[cohade])
          item.description = row[description_index]
          item.monto = row[monto_index]
          item.monto_o = row[monto_o_index]
          item.cod_empresa_payroll = row[cod_empresa_index]
          item.cod_cecos = row[cod_cecos_index]
          liq_items_array << item
        end

        should_save = should_save_this if should_save
      end

      if should_save
        @liq_process.liq_items.delete_all
        LiqItem.bulk_import(liq_items_array, batch_size: 5000, validate: false)
        flash[:success] = 'El archivo se procesó correctamente.'
        redirect_to @liq_process
      else
        flash.now[:danger] = 'Hay errores en el archivo, favor revise el detalle para que los puede corregir y vuelva a itentarlo'
        render 'show'
      end
    else
      flash[:danger] = 'El archivo no tiene el formato correcto (.xlsx)'
      redirect_to @liq_process
    end
  end

  private

  def verify_access_manage_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'liq', true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      flash[:danger] = t('security.no_access_to_manage_module')
      redirect_to root_path
    end
  end

end
