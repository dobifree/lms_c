class CoursesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def index
    @courses = Course.find_all_by_dncp_id(nil)
    @courses_dnc = Course.where('dncp_id IS NOT NULL')
  end

  def show
    @course = Course.find(params[:id])
    order_course_elements @course

   #@company = session[:company]
    @course_file = CourseFile.new

  end

  def preview
    @course = Course.find(params[:id])
   #@company = session[:company]

    if params[:unit_id]

      @unit = Unit.find(params[:unit_id])

    end

    order_course_elements @course

  end

  def new
    @course = Course.new
   #@company = session[:company]
  end

  def edit
    @course = Course.find(params[:id])
   #@company = session[:company]
  end

  def delete
    @course = Course.find(params[:id])
  end

  def edit_unit_sequence
    @course = Course.find(params[:id])
    @unit = @course.units.find(params[:unit_id])
  end

  def edit_evaluation_sequence
    @course = Course.find(params[:id])
    @evaluation = @course.evaluations.find(params[:evaluation_id])
  end

  def edit_poll_sequence
    @course = Course.find(params[:id])
    @poll = @course.polls.find(params[:poll_id])
  end

  def select_clone_course

    @course = Course.find(params[:id])


  end

  def clone_course

    @course = Course.find(params[:id])

    if params[:clone_course][:id] && params[:clone_course][:id] != ''

      course_to_clone = Course.find(params[:clone_course][:id])

      @course.units.destroy_all

      max_orden = 0

      course_to_clone.units.each do |unit_shape|

        unit = unit_shape.dup
        unit.course = @course
        unit.save

        max_orden = unit.orden if unit.orden > max_orden

      end

      @course.evaluations.destroy_all

      course_to_clone.evaluations.each do |eval_shape|

        eval = eval_shape.dup
        eval.course = @course
        eval.save

        max_orden = eval.orden if eval.orden > max_orden

      end

      @course.polls.find_all_by_tipo_satisfaccion(false).each do |current_poll|
        current_poll.destroy
      end

      max_numero_poll = 0;

      course_to_clone.polls.find_all_by_tipo_satisfaccion(false).each do |poll_shape|

        poll = poll_shape.dup
        poll.course = @course
        poll.save

        max_numero_poll = poll.numero if poll.numero > max_numero_poll

        max_orden = poll.orden if poll.orden > max_orden

      end

      current_poll_satisfaccion = @course.polls.find_by_tipo_satisfaccion(true)

      if current_poll_satisfaccion
        current_poll_satisfaccion.numero = max_numero_poll+1
        current_poll_satisfaccion.orden = max_orden+1
        current_poll_satisfaccion.save
      end

      if @course.dncp_id

        dncp = @course.dncp
        dncp.course_id = course_to_clone.id
        dncp.save

      end

      flash[:success] = t('activerecord.success.model.course.clone_ok')

    elsif params[:clone_course][:id] == ''

      @course.units.destroy_all
      @course.evaluations.destroy_all
      @course.polls.find_all_by_tipo_satisfaccion(false).each do |current_poll|
        current_poll.destroy
      end

      current_poll_satisfaccion = @course.polls.find_by_tipo_satisfaccion(true)

      if current_poll_satisfaccion
        current_poll_satisfaccion.numero = 1
        current_poll_satisfaccion.orden = 1
        current_poll_satisfaccion.save
      end

      if @course.dncp_id

        dncp = @course.dncp
        dncp.course_id = nil
        dncp.save

      end

    end

    redirect_to @course

  end

  def update_poll_obligatory

    @course = Course.find(params[:id])
    poll = @course.polls.find(params[:poll_id])

    poll.update_attribute(:obligatoria, !poll.obligatoria)

    flash[:success] = t('activerecord.success.model.poll.update_obligatory_ok')

    redirect_to poll.course


  end



  def create

    @course = Course.new(params[:course])

    if @course.save
      flash[:success] = t('activerecord.success.model.course.create_ok')
      redirect_to @course
    else
     #@company = session[:company]
      render action: 'new'
    end

  end

  def create_file

    @course = Course.find(params[:id])
   #@company = session[:company]
    @course_file = CourseFile.new
    @course_file.course = @course

    @course_file.nombre = params[:course_file][:nombre]
    @course_file.descripcion = params[:course_file][:descripcion]

    if params[:upload]

      @course_file.extension = File.extname(params[:upload]['datafile'].original_filename)
      @course_file.extension.slice! 0
      @course_file.size = File.size params[:upload]['datafile'].tempfile

      if @course_file.save

        save_file params[:upload], @course_file, @company

        flash[:success] = t('activerecord.success.model.course_file.update_file_ok')
        redirect_to @course
      else
        order_course_elements @course
        render 'show'
      end
    else
      order_course_elements @course
      @course_file.errors.add(:archivo, t('activerecord.errors.models.course_file.attributes.archivo.blank'))
      render 'show'
    end




  end

  def destroy_file

    @course = Course.find(params[:id])
    @course_file = CourseFile.find(params[:course_file_id])

    file_name = @company.directorio+'/'+@company.codigo+'/material_apoyo/'+@course_file.crypted_name+'.'+@course_file.extension

    File.delete(file_name) if File.exist? file_name

    @course_file.destroy

    flash[:success] = t('activerecord.success.model.course_file.destroy_file_ok')
    redirect_to @course

  end

  def update

    @course = Course.find(params[:id])

    if @course.update_attributes(params[:course])

      if @course.dncp

        dncp = @course.dncp

        dncp.tema_capacitacion = @course.nombre

        dncp.oportunidad_mejora = @course.descripcion
        dncp.objetivos_curso = @course.objetivos
        dncp.tiempo_capacitacion = (@course.dedicacion_estimada*60).round
        dncp.nota_minima = @course.nota_minima

        dncp.eval_presencial = @course.eval_presencial
        dncp.poll_presencial = @course.poll_presencial
        dncp.asistencia_presencial = @course.asistencia_presencial

        dncp.save

      end

      flash[:success] = t('activerecord.success.model.course.update_ok')
      redirect_to @course
    else
     #@company = session[:company]
      render action: 'edit'
    end

  end

  def destroy

    @course = Course.find(params[:id])

    if verify_recaptcha

      if @course.destroy
        flash[:success] = t('activerecord.success.model.course.delete_ok')
        redirect_to courses_url
      else
        flash[:danger] = t('activerecord.error.model.course.delete_error')
        redirect_to delete_course_path(@course)
      end

    else

      flash.delete(:recaptcha_error)

      flash[:danger] = t('activerecord.error.model.course.captcha_error')
      redirect_to delete_course_path(@course)

    end

  end

  def update_unit_sequence

    @course = Course.find(params[:course][:id])
    @unit = @course.units.find(params[:unit][:id])

    libre = true

    despues_de_unit_id = NIL
    despues_de_evaluation_id = NIL
    despues_de_poll_id = NIL

    antes_de_unit_id = NIL
    antes_de_evaluation_id = NIL
    antes_de_poll_id = NIL

    unless params[:despues_de][:id].blank?

      libre = false

      elemento = params[:despues_de][:id].split('-')
      despues_de_unit_id = elemento[1] if elemento[0]=='u'
      despues_de_evaluation_id = elemento[1] if elemento[0]=='e'
      despues_de_poll_id = elemento[1] if elemento[0]=='p'

    end

    unless params[:antes_de][:id].blank?

      libre = false

      elemento = params[:antes_de][:id].split('-')
      antes_de_unit_id = elemento[1] if elemento[0]=='u'
      antes_de_evaluation_id = elemento[1] if elemento[0]=='e'
      antes_de_poll_id = elemento[1] if elemento[0]=='p'

    end

    @unit.update_attributes(libre: libre, despues_de_unit_id: despues_de_unit_id,
        despues_de_evaluation_id: despues_de_evaluation_id, despues_de_poll_id: despues_de_poll_id,
        antes_de_unit_id: antes_de_unit_id, antes_de_evaluation_id: antes_de_evaluation_id,
        antes_de_poll_id: antes_de_poll_id)

    order_course_elements @course
   #@company = session[:company]
    @course_file = CourseFile.new
    render 'show'

  end

  def update_evaluation_sequence

    @course = Course.find(params[:course][:id])
    @evaluation = @course.evaluations.find(params[:evaluation][:id])

    libre = true

    despues_de_unit_id = NIL
    despues_de_evaluation_id = NIL
    despues_de_poll_id = NIL

    antes_de_unit_id = NIL
    antes_de_evaluation_id = NIL
    antes_de_poll_id = NIL

    unless params[:despues_de][:id].blank?

      libre = false

      elemento = params[:despues_de][:id].split('-')
      despues_de_unit_id = elemento[1] if elemento[0]=='u'
      despues_de_evaluation_id = elemento[1] if elemento[0]=='e'
      despues_de_poll_id = elemento[1] if elemento[0]=='p'

    end

    unless params[:antes_de][:id].blank?

      libre = false

      elemento = params[:antes_de][:id].split('-')
      antes_de_unit_id = elemento[1] if elemento[0]=='u'
      antes_de_evaluation_id = elemento[1] if elemento[0]=='e'
      antes_de_poll_id = elemento[1] if elemento[0]=='p'

    end

    @evaluation.update_attributes(libre: libre, despues_de_unit_id: despues_de_unit_id,
        despues_de_evaluation_id: despues_de_evaluation_id, despues_de_poll_id: despues_de_poll_id,
        antes_de_unit_id: antes_de_unit_id, antes_de_evaluation_id: antes_de_evaluation_id,
        antes_de_poll_id: antes_de_poll_id)

    order_course_elements @course
   #@company = session[:company]
    @course_file = CourseFile.new
    render 'show'

  end

  def update_poll_sequence

    @course = Course.find(params[:course][:id])
    @poll = @course.polls.find(params[:poll][:id])

    libre = true

    despues_de_unit_id = NIL
    despues_de_evaluation_id = NIL
    despues_de_poll_id = NIL

    antes_de_unit_id = NIL
    antes_de_evaluation_id = NIL
    antes_de_poll_id = NIL

    unless params[:despues_de][:id].blank?

      libre = false

      elemento = params[:despues_de][:id].split('-')
      despues_de_unit_id = elemento[1] if elemento[0]=='u'
      despues_de_evaluation_id = elemento[1] if elemento[0]=='e'
      despues_de_poll_id = elemento[1] if elemento[0]=='p'

    end

    unless params[:antes_de][:id].blank?

      libre = false

      elemento = params[:antes_de][:id].split('-')
      antes_de_unit_id = elemento[1] if elemento[0]=='u'
      antes_de_evaluation_id = elemento[1] if elemento[0]=='e'
      antes_de_poll_id = elemento[1] if elemento[0]=='p'

    end

    @poll.update_attributes(libre: libre, despues_de_unit_id: despues_de_unit_id,
        despues_de_evaluation_id: despues_de_evaluation_id, despues_de_poll_id: despues_de_poll_id,
        antes_de_unit_id: antes_de_unit_id, antes_de_evaluation_id: antes_de_evaluation_id,
        antes_de_poll_id: antes_de_poll_id)

    order_course_elements @course
   #@company = session[:company]
    @course_file = CourseFile.new
    render 'show'

  end

  def upload_imagen

    @course = Course.find(params[:id])

   company = @company

    if params[:upload]

      ext = File.extname(params[:upload]['datafile'].original_filename)

      if ext != '.jpg' && ext != '.jpeg'

        flash[:danger] = t('activerecord.error.model.course.update_imagen_wrong_format')
        redirect_to @course

      elsif File.size(params[:upload]['datafile'].tempfile) > 100.kilobytes

        flash[:danger] = t('activerecord.error.model.course.update_imagen_wrong_wrong_size')
        redirect_to @course

      else

        save_imagen params[:upload], @course, company

        flash[:success] = t('activerecord.success.model.course.update_imagen_ok')

        redirect_to @course

      end



    else

      flash[:danger] = t('activerecord.error.model.course.update_imagen_empty_file')

      redirect_to @course

    end

  end

  private

    def save_file(upload, course_file, company)

      directory = company.directorio+'/'+company.codigo+'/material_apoyo/'

      FileUtils.mkdir_p directory unless File.directory?(directory)

      name = course_file.crypted_name+File.extname(upload['datafile'].original_filename)
      path = File.join(directory, name)
      File.open(path, 'wb') { |f| f.write(upload['datafile'].read) }

    end

    def save_imagen(upload, course, company)

      ext = File.extname(upload['datafile'].original_filename)

      if ext == '.jpg' || ext == '.jpeg'

        if File.size(upload['datafile'].tempfile) <= 100.kilobytes

          directory = company.directorio+'/'+company.codigo+'/imagenes_cursos/'

          FileUtils.mkdir_p directory unless File.directory? directory

          if course.imagen
            file = directory+course.imagen
            File.delete(file) if File.exist? file
          end

          course.set_imagen
          course.save

          file = directory+course.imagen

          File.open(file, 'wb') { |f| f.write(upload['datafile'].read) }

        end

      end

    end

end
