class BpRuleVacationsController < ApplicationController

  def deactivate
    rule_vacation = BpRuleVacation.find(params[:bp_rule_vacation_id])
    rule_vacation.active = false
    rule_vacation.deactivated_at = lms_time
    rule_vacation.deactivated_by_admin_id = admin_connected.id

    if rule_vacation.save
      flash[:success] = t('views.bp_rule_vacations.flash_messages.success_deactivated')
      redirect_to bp_groups_path
    else
      flash[:danger] = t('views.bp_rule_vacations.flash_messages.danger_deactivated')
      redirect_to bp_groups_path
    end
  end

  def new
    @bp_rule_vacation = BpRuleVacation.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bp_rule_vacation }
    end
  end

  def create
    bp_rule_vacation = BpRuleVacation.new(params[:bp_rule_vacation])
    bp_rule_vacation.registered_by_admin_id = admin_connected.id
    bp_rule_vacation.registered_at = lms_time

    if bp_rule_vacation.save
      flash[:success] = t('views.bp_rule_vacations.flash_messages.success_created')
      redirect_to bp_groups_path
    else
      @bp_rule_vacation = bp_rule_vacation
      render action: "new"
    end
  end

end
