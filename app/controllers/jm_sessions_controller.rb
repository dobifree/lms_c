class JmSessionsController < ApplicationController
  layout 'job_market'

  before_filter :authenticate_candidate, only: [:destroy]
  before_filter :authenticate_user, only: [:sign_in_user]

  def new
    prepare_data_login()
    render 'jm_candidates/login'
  end

  def create

    candidate = JmCandidate.where(:email => params[:jm_session][:email].downcase).first

    if params[:jm_session][:password]
      if (candidate && candidate.authenticate(params[:jm_session][:password]))
        create_jm_session(candidate)
        # el progress negativo es cuando no hay preguntas obligatorias. Y menor a 100 es que hay obligatorias pero no están completas
        if candidate.my_profile_progress >= 0 && candidate.my_profile_progress < 100
          flash[:success] = 'Tu perfil no está completo, por favor completa tu perfil.'
          redirect_to edit_jm_my_profile_path
        else
          redirect_to list_jm_offers_path
        end
      else
        flash.now[:danger] = 'Contraseña incorrecta'
        #redirect_to jm_login_path
        prepare_data_login()
        render 'jm_candidates/login'
      end
    else
      prepare_data_login()
      render 'jm_candidates/login'
    end
  end

  def destroy
    sign_out
    redirect_to list_jm_offers_path
  end

  def sign_out_candidate
    reset_session
  end

  def authenticate_candidate

    unless session[:candidate_connected_id]
      redirect_to list_jm_offers_path
    end

  end

  def download_terms_conditions
    redirect_to :back unless @company.jm_has_terms_conditions?

    send_file @company.jm_term_conditions_path,
              filename: 'Terminos y condiciones.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'
  end

  private

  def prepare_data_login
    if params[:jm_session] && !params[:jm_session][:email].blank?
      @inserted_email = params[:jm_session][:email].downcase
      @jm_candidate = JmCandidate.where(email: @inserted_email).first
      unless @jm_candidate
        @show_email_not_registered = true
      end
    end
  end


end
