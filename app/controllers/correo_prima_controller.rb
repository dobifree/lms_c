class CorreoPrimaController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def formulario

  end

  def enviar_correo

    if params[:upload]

      if File.extname(params[:upload]['datafile'].original_filename) == '.xlsx'

        directorio_temporal = '/tmp/'
        nombre_temporal = SecureRandom.hex(5)+'.xlsx'
        archivo_temporal = directorio_temporal+nombre_temporal

        path = File.join(directorio_temporal, nombre_temporal)
        File.open(path, 'wb') { |f| f.write(params[:upload]['datafile'].read) }

        data = RubyXL::Parser.parse(archivo_temporal).worksheets[0].extract_data.each_with_index do |fila, index|

          #0 codigo
          #1 nombre
          #2 correo
          #3 area
          #4 servicio
          #5 seccion
          #6 horas

          LmsMailer.correo_prima(fila[0], fila[1], fila[2], fila[3], fila[4], fila[5], fila[6]).deliver if index > 0

        end


        flash[:success] = 'Enviado'

        redirect_to correo_prima_formulario_url

      else

        render 'formulario'

      end

    else

      render 'formulario'

    end

  end

end
