class DncCoursesController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /dnc_courses
  # GET /dnc_courses.json
  def index
    @dnc_courses = DncCourse.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dnc_courses }
    end
  end

  # GET /dnc_courses/1
  # GET /dnc_courses/1.json
  def show
    @dnc_course = DncCourse.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dnc_course }
    end
  end

  # GET /dnc_courses/new
  # GET /dnc_courses/new.json
  def new
    @dnc_course = DncCourse.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dnc_course }
    end
  end

  # GET /dnc_courses/1/edit
  def edit
    @dnc_course = DncCourse.find(params[:id])
  end

  # POST /dnc_courses
  # POST /dnc_courses.json
  def create
    @dnc_course = DncCourse.new(params[:dnc_course])

    respond_to do |format|
      if @dnc_course.save
        format.html { redirect_to @dnc_course, notice: 'Dnc course was successfully created.' }
        format.json { render json: @dnc_course, status: :created, location: @dnc_course }
      else
        format.html { render action: "new" }
        format.json { render json: @dnc_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dnc_courses/1
  # PUT /dnc_courses/1.json
  def update
    @dnc_course = DncCourse.find(params[:id])

    respond_to do |format|
      if @dnc_course.update_attributes(params[:dnc_course])
        format.html { redirect_to @dnc_course, notice: 'Dnc course was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dnc_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dnc_courses/1
  # DELETE /dnc_courses/1.json
  def destroy
    @dnc_course = DncCourse.find(params[:id])
    @dnc_course.destroy

    respond_to do |format|
      format.html { redirect_to dnc_courses_url }
      format.json { head :no_content }
    end
  end
end
