class SelReqItemsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /sel_req_items
  # GET /sel_req_items.json
  def index
    @sel_req_items = SelReqItem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sel_req_items }
    end
  end

  # GET /sel_req_items/1
  # GET /sel_req_items/1.json
  def show
    @sel_req_item = SelReqItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sel_req_item }
    end
  end

  # GET /sel_req_items/new
  # GET /sel_req_items/new.json
  def new
    @sel_req_template = SelReqTemplate.find(params[:sel_req_template_id])
    @sel_req_item = SelReqItem.new
    @sel_req_item.sel_req_template = @sel_req_template

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sel_req_item }
    end
  end

  # GET /sel_req_items/1/edit
  def edit
    @sel_req_item = SelReqItem.find(params[:id])
  end

  # POST /sel_req_items
  # POST /sel_req_items.json
  def create
    @sel_req_item = SelReqItem.new(params[:sel_req_item])

    if !@sel_req_item.sel_req_template.is_in_use?

      respond_to do |format|
        if @sel_req_item.save

          #################################################################
          ## parche que elimina las opciones que pudieran haber estado en 'formulario' y no es de tipo 'seleccion'
          @sel_req_item.sel_req_options.destroy_all unless @sel_req_item.item_type == 3
          #################################################################

          format.html {
            flash[:success] = 'El item fue creado correctamente'
            redirect_to @sel_req_item.sel_req_template
          }
          format.json { render json: @sel_req_item, status: :created, location: @sel_req_item }
        else
          format.html { render action: "new" }
          format.json { render json: @sel_req_item.errors, status: :unprocessable_entity }
        end
      end
    else
      flash[:success]='La plantilla no puede ser modificada por que ya está en uso.'
      redirect_to @sel_req_item.sel_req_template
    end
  end

  # PUT /sel_req_items/1
  # PUT /sel_req_items/1.json
  def update
    @sel_req_item = SelReqItem.find(params[:id])

    #if !@sel_req_item.sel_req_template.is_in_use?

      respond_to do |format|
        if @sel_req_item.update_attributes(params[:sel_req_item])
          #################################################################
          ## parche que elimina las opciones que pudieran haber estado en 'formulario' y no es de tipo 'seleccion'
          @sel_req_item.sel_req_options.destroy_all unless @sel_req_item.item_type == 3
          #################################################################
          format.html {
            flash[:success] = 'El item fue actualizado correctamente'
            redirect_to @sel_req_item.sel_req_template
          }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @sel_req_item.errors, status: :unprocessable_entity }
        end
      end
    #else
     # flash[:success]='La plantilla no puede ser modificada por que ya está en uso.'
     # redirect_to @sel_req_item.sel_req_template
    #end
  end

  # DELETE /sel_req_items/1
  # DELETE /sel_req_items/1.json
  def destroy
    @sel_req_item = SelReqItem.find(params[:id])
    @sel_req_template = @sel_req_item.sel_req_template

    if !@sel_req_item.sel_requirement_values.any?
      @sel_req_item.destroy

      respond_to do |format|
        format.html {
          flash[:success] = 'El item fue eliminado correctamente'
          redirect_to @sel_req_template
        }
        format.json { head :no_content }
      end
    else
      flash[:danger]='El item no puede ser eliminado por que ya está en uso.'
      redirect_to @sel_req_template
    end
  end


end
