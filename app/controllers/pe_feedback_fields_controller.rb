class PeFeedbackFieldsController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  def new

    @pe_process = PeProcess.find(params[:pe_process_id])

    @pe_feedback_field = @pe_process.pe_feedback_fields.build

  end

  def create

    @pe_feedback_field = PeFeedbackField.new(params[:pe_feedback_field])

    if @pe_feedback_field.save

      params[:compound_field_name_new].each_with_index do |compound_field_name, index|

        unless compound_field_name.blank?

          pe_feedback_compound_field = @pe_feedback_field.pe_feedback_compound_fields.build

          pe_feedback_compound_field.position = params[:compound_field_pos_new][index]
          pe_feedback_compound_field.name = compound_field_name
          pe_feedback_compound_field.field_type = params[:compound_field_type_new][index]
          pe_feedback_compound_field.required = params[:compound_field_required_new][index]
          pe_feedback_compound_field.pe_feedback_field_list_id = params[:compound_field_list_new][index] if params[:compound_field_type_new][index] == 2 && params[:compound_field_list_new][index]
          pe_feedback_compound_field.pe_feedback_field_list_item_id = params[:compound_field_list_item_new][index]
          pe_feedback_compound_field.list_item_selected = params[:compound_field_list_item_selected_new][index]

          pe_feedback_compound_field.save

        end


      end

      redirect_to pe_process_tab_path @pe_feedback_field.pe_process, 'feedback'
    else
      @pe_process = @pe_feedback_field.pe_process
      render action: 'new'
    end

  end

  def edit
    @pe_feedback_field = PeFeedbackField.find(params[:id])
    @pe_process = @pe_feedback_field.pe_process
  end

  def update
    @pe_feedback_field = PeFeedbackField.find(params[:id])

    if @pe_feedback_field.update_attributes(params[:pe_feedback_field])

      @pe_feedback_field.pe_feedback_compound_fields.each do |pe_feedback_compound_field|

        if params['compound_field_name_'+pe_feedback_compound_field.id.to_s].blank?

          if pe_feedback_compound_field.pe_member_compound_feedbacks.size == 0

            pe_feedback_compound_field.destroy

          end

        else

          pe_feedback_compound_field.name = params['compound_field_name_'+pe_feedback_compound_field.id.to_s]
          pe_feedback_compound_field.position = params['compound_field_pos_'+pe_feedback_compound_field.id.to_s]
          pe_feedback_compound_field.field_type = params['compound_field_type_'+pe_feedback_compound_field.id.to_s]
          pe_feedback_compound_field.required = params['compound_field_required_'+pe_feedback_compound_field.id.to_s]
          pe_feedback_compound_field.pe_feedback_field_list_id = params['compound_field_list_'+pe_feedback_compound_field.id.to_s]
          pe_feedback_compound_field.pe_feedback_field_list_item_id = params['compound_field_list_item_'+pe_feedback_compound_field.id.to_s]
          pe_feedback_compound_field.list_item_selected = params['compound_field_list_item_selected_'+pe_feedback_compound_field.id.to_s]

          pe_feedback_compound_field.save

        end

      end

      if params[:compound_field_name_new]

        params[:compound_field_name_new].each_with_index do |compound_field_name, index|

          unless compound_field_name.blank?

            pe_feedback_compound_field = @pe_feedback_field.pe_feedback_compound_fields.build

            pe_feedback_compound_field.position = params[:compound_field_pos_new][index]
            pe_feedback_compound_field.name = compound_field_name
            pe_feedback_compound_field.field_type = params[:compound_field_type_new][index]
            pe_feedback_compound_field.required = params[:compound_field_required_new][index]
            pe_feedback_compound_field.pe_feedback_field_list_id = params[:compound_field_list_new][index]
            pe_feedback_compound_field.list_item_selected = params[:compound_field_list_item_selected_new][index]

            pe_feedback_compound_field.save

          end

        end

      end

      redirect_to pe_process_tab_path @pe_feedback_field.pe_process, 'feedback'
    else
      @pe_process = @pe_feedback_field.pe_process
      render action: 'edit'
    end

  end


  def destroy

    @pe_feedback_field = PeFeedbackField.find(params[:id])

    pe_process = @pe_feedback_field.pe_process

    @pe_feedback_field.destroy if @pe_feedback_field.pe_member_feedbacks.size == 0

    redirect_to pe_process_tab_path pe_process, 'feedback'

  end

end
