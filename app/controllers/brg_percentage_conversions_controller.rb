class BrgPercentageConversionsController < ApplicationController
  include BrgModule

  before_filter :authenticate_user
  before_filter :active_module
  before_filter :brg_manager
  before_filter :get_process, only: %i[new
                                       create
                                       massive_upload
                                       massive_upload_2]

  def new
    @percentage_conversion = BrgPercentageConversion.new(brg_process_id: @process.id)
  end

  def create
    percentage_conversion = BrgPercentageConversion.new(params[:brg_percentage_conversion])
    percentage_conversion = set_registered(percentage_conversion)
    percentage_conversion.brg_process_id = @process.id
    if percentage_conversion.save
      flash[:success] = t('views.brg_module.flash_messages.create_success')
      redirect_to brg_groups_index_path(t('views.brg_module.tabs.manager_index.percentage_conversions'))
    else
      @percentage_conversion = percentage_conversion
      render 'new'
    end
  end

  def edit
    @percentage_conversion = BrgPercentageConversion.find(params[:brg_percentage_conversion_id])
  end

  def update
    percentage_conversion = BrgPercentageConversion.find(params[:brg_percentage_conversion_id]).dup
    percentage_conversion.registered_at = lms_time
    percentage_conversion.registered_by_user_id = user_connected.id
    if percentage_conversion.update_attributes(params[:brg_percentage_conversion])
      bpc_conditional_delete(params[:brg_percentage_conversion_id])
      flash[:success] = t('views.brg_module.flash_messages.change_success')
      redirect_to brg_groups_index_path(t('views.brg_module.tabs.manager_index.percentage_conversions'))
    else
      @percentage_conversion = percentage_conversion
      render action: 'edit'
    end
  end

  def deactivate_pc
    bpc_conditional_delete(params[:brg_percentage_conversion_id])
    flash[:success] = t('views.brg_module.flash_messages.delete_success')
    redirect_to brg_groups_index_path(t('views.brg_module.tabs.manager_index.percentage_conversions'))
  end

  def deactivate_pc_ajax
    bpc_conditional_delete(params[:brg_percentage_conversion_id])
    render nothing: true
  end

  def massive_upload

  end

  def massive_upload_2
    message = verify_excel(params[:upload])
    if message.size.zero?
      file = get_temporal_file(params[:upload])
      if its_safe_percentage_conversion file, @process.id
        flash.now[:success] = t('activerecord.success.model.ben_cellphone_number.massive_update_ok')
      else
        flash.now[:danger] = t('activerecord.success.model.ben_cellphone_number.massive_update_error')
      end
    else
      flash.now[:danger] = message
    end
    render 'massive_upload'
  end
end
