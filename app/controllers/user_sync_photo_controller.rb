class UserSyncPhotoController < ApplicationController

  skip_before_filter :verify_authenticity_token

  def update_photo
    errors = []
    crypted_user_code = params[:user_code_crypted_base64]

    #####
    setting_config = File.join(Rails.root, 'config', 'user_sync_photo.yml')
    raise "#{setting_config} is missing!" unless File.exists? setting_config
    @user_sync_photo_config = YAML.load_file(setting_config)[@company.connection_string] || {}
    #####

    begin
      cipher = OpenSSL::Cipher.new @user_sync_photo_config['user_code']['encriptation_method']
      cipher.decrypt
      # La llave se recibe como parámetro opcional, en caso de no venir debe estar en el secrets.yml
      cipher.key = @user_sync_photo_config['user_code']['cipher_key']
      decrypted_user_cod = decrypt_user_from_custom_http_auth(crypted_user_code, cipher)
        # Se retorna el dato desencriptado
    rescue Exception => e
      error_decrypt = true
      errors << 'Problemas para desencriptar'
      puts 'error_decrypt: ' + error_decrypt.inspect
      puts 'traza: ' + e.inspect
    end

    user = User.where(codigo: decrypted_user_cod, activo: true).first unless decrypted_user_cod.blank?

    if user
      directory = @company.directorio + '/' + @company.codigo + '/fotos_usuarios/'
      FileUtils.mkdir_p directory unless File.directory? directory
      if user.foto
        old_file_path = File.join(directory, user.foto)
        #File.delete(old_file_path) if File.exist? old_file_path
      end
      user.set_foto
      new_file_path = File.join(directory, user.foto)
      regexp = /\Adata:([-\w]+\/[-\w\+\.]+)?;base64,(.*)/m
      data_uri_parts = params[:photo].match(regexp) || []
      extension = MIME::Types[data_uri_parts[1]].first.sub_type
      if extension == 'jpeg'
        user.transaction do
          convert_data_url_to_image(data_uri_parts[2], new_file_path)
          user.save!
          File.delete(old_file_path) if old_file_path && File.exist?(old_file_path)
        end
      else
        errors << 'Archivo no está en formato jpeg'
      end
      #render :json => true
    else
      #ActionMailer::Base.mail(from: 'Notificaciones EXA <notificaciones@exa.pe>', to: 'cesar@capitalteam.pe', cc: 'Innovacioncultura@security.cl', subject: 'Error en autenticación SSO Interna - Pruebas', body: 'Usuario no registrado. active_directory_id: ' + decrypted_active_directory_id.to_s).deliver
      #render :json => false
      errors << 'Usuario no encontrado'
    end

    if errors.any?
      render json: {response: false, errors: errors}
    else
      render json: {response: true, errors: errors}
    end

  end

  private

  def decrypt_user_from_custom_http_auth(data, cipher)
    # Proceso de desencriptación
    unescaped = CGI.unescape(data) # Se le quita el urlencode
    decoded = Base64.decode64(unescaped) # Se descodifica de base64
    cipher.iv = decoded[0..15] # Se carga el IV. Este corresponde a los primeros 16 caracteres de la data recibida
    decrypted = cipher.update(decoded[16..decoded.length - 1]) # Se hace el primer paso de desencriptación
    decrypted << cipher.final # Se finaliza la desencriptación
    # Se considera que esté dentro de 1 minuto la solicitud
    timestamp = decrypted[-10..(decrypted.length - 1)].to_i
    if ((timestamp - 30)..(timestamp + 30)).include?(Time.now.to_i)
      # Se retorna el dato del usuariodesencriptado
      return decrypted[0..(decrypted.length - 11)]
    else
      #puts "Error de coincidencia de timestamp. Hora local: " + Time.now.to_i.to_s + " - timestamp: " + timestamp.to_s
      raise "Error de coincidencia de timestamp. Hora local: " + Time.now.to_i.to_s + " - timestamp: " + timestamp.to_s
    end
  end


  def convert_data_url_to_image(data_url, file_path)
    imageDataBinary = Base64.decode64(data_url)
    File.open("#{file_path}", "wb") {|f| f.write(imageDataBinary)}
    return true
  end

=begin
  def splitBase64(uri)
    if uri.match(%r{^data:(.*?);(.*?),(.*)$})
      return {
          :type =>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $1, # "image/png"
          :encoder =>&nbsp;&nbsp; $2, # "base64"
          :data =>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $3, # data string
          :extension => $1.split('/')[1] # "png"
          }
    end

  end
=end

end
