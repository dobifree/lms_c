class InfDocFoldersController < ApplicationController

  before_filter :authenticate_user
  before_filter :authenticate_user_admin

  # GET /inf_doc_folders
  # GET /inf_doc_folders.json
  def index
    @inf_doc_folders = InfDocFolder.all
  end

  # GET /inf_doc_folders/1
  # GET /inf_doc_folders/1.json
  def show
    @inf_doc_folder = InfDocFolder.find(params[:id])
  end

  # GET /inf_doc_folders/new
  # GET /inf_doc_folders/new.json
  def new
    @inf_doc_folder = InfDocFolder.new
  end

  # GET /inf_doc_folders/1/edit
  def edit
    @inf_doc_folder = InfDocFolder.find(params[:id])
  end

  # POST /inf_doc_folders
  # POST /inf_doc_folders.json
  def create
    @inf_doc_folder = InfDocFolder.new(params[:inf_doc_folder])

      if @inf_doc_folder.save
        flash[:success] = 'La carpeta fue creada correctamente'
        redirect_to informative_documents_by_folder_path(@inf_doc_folder)
      else
        flash.now[:danger] = 'La carpeta no pudo ser creada correctamente'
        render action: 'new'
      end
  end

  # PUT /inf_doc_folders/1
  # PUT /inf_doc_folders/1.json
  def update
    @inf_doc_folder = InfDocFolder.find(params[:id])

      if @inf_doc_folder.update_attributes(params[:inf_doc_folder])
        flash[:success] = 'La carpeta fue actualizada correctamente'
        redirect_to informative_documents_by_folder_path(@inf_doc_folder)
      else
        flash.now[:danger] = 'La carpeta no pudo ser actualizada correctamente'
        render action: 'edit'
      end
  end

  # DELETE /inf_doc_folders/1
  # DELETE /inf_doc_folders/1.json
  def destroy
    @inf_doc_folder = InfDocFolder.find(params[:id])
    @inf_doc_folder.destroy

    redirect_to informative_documents_path
  end
end
