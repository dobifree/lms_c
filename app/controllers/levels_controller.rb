class LevelsController < ApplicationController
  
  before_filter :authenticate_user
  before_filter :authenticate_user_admin
  
  def index
    @programs = Program.all
  end

  def new
    
    @program = Program.find(params[:program_id])
    @level = @program.levels.build()

  end

  def edit
    @level = Level.find(params[:id])
    @program = @level.program
  end

  def create

    @program = Program.find(params[:program][:id])

    @level = @program.levels.build(params[:level])

    if @level.save
      flash[:success] = t('activerecord.success.model.level.create_ok')
      redirect_to programs_path
    else
      render action: 'new'
    end
  end

  def update

    @level = Level.find(params[:id])

    if @level.update_attributes(params[:level])
      flash[:success] = t('activerecord.success.model.level.update_ok')
      redirect_to programs_path
    else
      @program = @level.program
      render action: 'edit'
    end
  end

  def delete

    @level = Level.find(params[:id])
    @program = @level.program

  end

  def destroy
    
    @level = Level.find(params[:id])

    if verify_recaptcha
    
      if @level.destroy
        flash[:success] = t('activerecord.success.model.level.delete_ok')
        redirect_to programs_path
      else
        flash[:danger] = t('activerecord.error.model.level.delete_error')
        redirect_to delete_level_path(@level)
      end

    else

      flash.delete(:recaptcha_error)
      
      flash[:danger] = t('activerecord.error.model.level.captcha_error')
      redirect_to delete_level_path(@level)

    end


  end
end
