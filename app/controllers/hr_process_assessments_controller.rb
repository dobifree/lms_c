class HrProcessAssessmentsController < ApplicationController

  before_filter :authenticate_user

  before_filter :acceso_ver_proceso, only: [:ver_proceso_evaluar, :ver_proceso_resultados, :ver_proceso_feedback, :ver_sesiones_calibracion, :carga_manual,
                                            :porcentaje_logro, :porcentaje_logro_usuario, :evaluar, :estado_avance_para_jefe, :resultado_final_para_jefe, :feedback, :responder_eval,
                                            :responder_porcentaje_logro, :confirmar_eval, :confirmar_feedback, :create_comment,
                                            :destroy_comment, :ver_proceso_calibrar, :make_calibrar, :add_manual_q_comment, :destroy_manual_q_comment]

  before_filter :accesso_ver_evaluar, only: [:evaluar, :estado_avance_para_jefe, :resultado_final_para_jefe, :feedback, :responder_eval, :responder_porcentaje_logro,
                                             :confirmar_eval, :confirmar_feedback, :create_comment, :destroy_comment, :add_manual_q_comment, :destroy_manual_q_comment]

  before_filter :accesso_evaluar, only: [:responder_eval, :responder_porcentaje_logro, :confirmar_eval, :add_manual_q_comment, :destroy_manual_q_comment]

  before_filter :accesso_confirmar_feedback, only: [:confirmar_feedback]

  before_filter :acceso_ver_calibrar, only: [:ver_proceso_calibrar, :ver_sesiones_calibracion]

  before_filter :acceso_calibrar, only: [:make_calibrar]

  before_filter :acceso_gestionar_proceso, only: [:autorizar_modificaciones_1, :autorizar_modificaciones_2, :autorizar_modificaciones_auto,
                                                  :gestionar_evaluadores, :gestionar_evaluadores1, :gestionar_evaluadores_add, :eliminar_evaluador,
                                                  :gestionar_responsables, :gestionar_responsables1, :gestionar_responsables_add, :eliminar_responsable,
                                                  :finalizar_evaluado]

  before_filter :acceso_reportes_proceso, only: [:ver_proceso_lista_reportes, :reporte_estado_general,
                                                 :reporte_con_filtros_ver_filtros, :reporte_con_filtros, :resultado_para_reporte,
                                                  :reporte_por_evaluado_lista, :reporte_por_evaluado, :reporte_por_evaluado_pdf]

  def listar_procesos

    #@hr_processes = HrProcess.where('abierto = ? AND fecha_inicio <= ? AND fecha_fin >= ?',true, lms_time, lms_time)

    @hr_processes = HrProcess.where('abierto = ?', true)


  end

  def listar_procesos_gestionar


  end

  def autorizar_modificaciones_1

    @hr_process_users = @hr_process.hr_process_users_order_by_apellidos_nombre

  end

  def autorizar_modificaciones_2

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    hr_process_user_rel = HrProcessUserRel.find(params[:hr_process_user_rel_id])

    hr_process_user_rel.finalizada = false
    hr_process_user_rel.save

    hr_process_user.finalizado = false
    hr_process_user.feedback_confirmado = false
    hr_process_user.save

    flash[:success] = t('activerecord.success.model.hr_process_assessment.autorization_ok')

    #redirect_to hr_process_assessment_autorizar_modificaciones_1_path @hr_process
    redirect_to hr_process_assessment_gestionar_evaluadores_1_path @hr_process, hr_process_user

  end

  def autorizar_modificaciones_auto

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    hr_process_user.finalizado = false
    hr_process_user.finalizada_auto = false
    hr_process_user.save

    flash[:success] = t('activerecord.success.model.hr_process_assessment.autorization_ok')

    #redirect_to hr_process_assessment_autorizar_modificaciones_1_path @hr_process
    redirect_to hr_process_assessment_gestionar_evaluadores_1_path @hr_process, hr_process_user

  end

  def gestionar_evaluadores



  end

  def gestionar_evaluadores1

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    @user_search = User.new

    if params[:user] && (!params[:user][:apellidos].blank? || !params[:user][:nombre].blank?)

        @users_search = User.where(
          'apellidos LIKE ? AND nombre LIKE ?',
          '%'+params[:user][:apellidos]+'%', '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

      @user_search.apellidos = params[:user][:apellidos]
      @user_search.nombre = params[:user][:nombre]

    else

    end

  end

  def gestionar_evaluadores_add

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    user_eval = User.find(params[:user_id])

    @user_search = user_eval


    if params[:tipo][:rel].blank?

      @users_search = User.where(
        'apellidos LIKE ? AND nombre LIKE ?',
        '%'+user_eval.apellidos+'%', '%'+user_eval.nombre+'%').order('apellidos, nombre')

      flash.now[:danger] = t('activerecord.error.model.hr_process_assessment.add_evaluador_error')

    else

      hr_process_user_eval = nil

      @hr_process.hr_process_users.each do |pu|

        if pu.user.codigo == user_eval.codigo

          hr_process_user_eval = pu
          break

        end

      end

      unless hr_process_user_eval

        hr_process_user_eval = @hr_process.hr_process_users.build
        hr_process_user_eval.user = user_eval

      end

      if hr_process_user_eval.save

        hr_process_user_rel = @hr_process.hr_process_user_rels.new
        hr_process_user_rel.hr_process_user1_id = hr_process_user_eval.id
        hr_process_user_rel.hr_process_user2_id = @hr_process_user.id
        hr_process_user_rel.tipo = params[:tipo][:rel]

        if hr_process_user_rel.save
          @hr_process_user.finalizado = false
          @hr_process_user.save
          flash.now[:success] = t('activerecord.success.model.hr_process_assessment.add_evaluador_ok')
        end

      end

    end


    render 'gestionar_evaluadores1'


  end

  def eliminar_evaluador

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    hr_process_user_rel = HrProcessUserRel.find(params[:hr_process_user_rel_id])

    @hr_process_user = hr_process_user


    if hr_process_user_rel.hr_process_user2_id == hr_process_user.id

      HrProcessAssessmentE.where('hr_process_user_id = ? AND hr_process_user_eval_id = ? AND tipo_rel = ?', hr_process_user_rel.hr_process_user2_id, hr_process_user_rel.hr_process_user1_id, hr_process_user_rel.tipo).each do |hr_process_assessment_e|

        hr_process_dimension = hr_process_assessment_e.hr_process_evaluation.hr_process_dimension_e.hr_process_dimension

        hr_process_assessment_e.destroy

        calculate_final_results hr_process_dimension

      end

    end

    hr_process_user_rel.destroy

    if debe_realizar_autoevaluacion_proceso(@hr_process, hr_process_user)

      hr_process_user.finalizado = true if hr_process_user.hr_process_es_evaluado_rels.where('finalizada = 0').count == 0 && hr_process_user.finalizada_auto

    else

      hr_process_user.finalizado = true if hr_process_user.hr_process_es_evaluado_rels.where('finalizada = 0').count == 0

    end

    hr_process_user.save

    flash[:success] = t('activerecord.success.model.hr_process_assessment.delete_evaluador_ok')

    redirect_to hr_process_assessment_gestionar_evaluadores_1_path @hr_process, hr_process_user

  end

  def gestionar_responsables



  end

  def gestionar_responsables1

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    @user_search = User.new

    if params[:user] && (!params[:user][:apellidos].blank? || !params[:user][:nombre].blank?)

      @users_search = User.where(
        'apellidos LIKE ? AND nombre LIKE ?',
        '%'+params[:user][:apellidos]+'%', '%'+params[:user][:nombre]+'%').order('apellidos, nombre')

      @user_search.apellidos = params[:user][:apellidos]
      @user_search.nombre = params[:user][:nombre]

    else

    end

  end

  def gestionar_responsables_add

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    user_eval = User.find(params[:user_id])

    @user_search = user_eval


    if params[:tipo][:rel].blank?

      @users_search = User.where(
        'apellidos LIKE ? AND nombre LIKE ?',
        '%'+user_eval.apellidos+'%', '%'+user_eval.nombre+'%').order('apellidos, nombre')

      flash.now[:danger] = t('activerecord.error.model.hr_process_assessment.add_responsable_error')

    else

      hr_process_user_eval = nil

      @hr_process.hr_process_users.each do |pu|

        if pu.user.codigo == user_eval.codigo

          hr_process_user_eval = pu
          break

        end

      end

      unless hr_process_user_eval

        hr_process_user_eval = @hr_process.hr_process_users.build
        hr_process_user_eval.user = user_eval

      end

      if hr_process_user_eval.save

        hr_process_user_rel = @hr_process.hr_process_user_rels.new
        hr_process_user_rel.hr_process_user1_id = hr_process_user_eval.id
        hr_process_user_rel.hr_process_user2_id = @hr_process_user.id
        hr_process_user_rel.tipo = params[:tipo][:rel]

        if hr_process_user_rel.save
          flash.now[:success] = t('activerecord.success.model.hr_process_assessment.add_responsable_ok')
        end

      end

    end


    render 'gestionar_responsables1'


  end

  def eliminar_responsable

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    hr_process_user_rel = HrProcessUserRel.find(params[:hr_process_user_rel_id])

    @hr_process_user = hr_process_user

    hr_process_user_rel.destroy

    flash[:success] = t('activerecord.success.model.hr_process_assessment.delete_responsable_ok')

    redirect_to hr_process_assessment_gestionar_responsables_1_path @hr_process, hr_process_user

  end

  def finalizar_evaluado

    hr_process_user = HrProcessUser.find(params[:hr_process_user_id])
    hr_process_user.finalizado = true
    hr_process_user.save


    flash[:success] = t('activerecord.success.model.hr_process_assessment.finalizar_evaluado_ok')

    redirect_to hr_process_assessment_gestionar_evaluadores_1_path @hr_process, hr_process_user

  end


  def listar_procesos_reportes

  end

  def ver_proceso_lista_reportes

  end

  def reporte_estado_general

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

  end

  def reporte_estado_general_old

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

  end

  def reporte_con_filtros_ver_filtros

    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

  end

  def reporte_con_filtros

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @characteristics = Characteristic.where('filter_assessment_report = ?', true).reorder('order_filter_assessment_report')

    @characteristics_selected = []

    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        params[:characteristic][characteristic.id.to_s.to_sym].slice! 0

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_selected.push params[:characteristic][characteristic.id.to_s.to_sym].map { |c| c.titleize }

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND ( '

          params[:characteristic][characteristic.id.to_s.to_sym].each_with_index do |valor, index|

            if index > 0

              queries[n_q] += ' OR '

            end

            queries[n_q] += 'valor = "'+valor+'" '

          end

          queries[n_q] += ' ) '

          n_q += 1

        end

      end

      if queries.length > 0

        queries.each_with_index do |query, index|

          if index == 0

            @users = User.joins(:user_characteristics).where(query).order('apellidos, nombre')

          else

            lista_users_ids = @users.map { |u| u.id }

            @users = User.joins(:user_characteristics).where(query+' AND users.id IN (?) ', lista_users_ids).order('apellidos, nombre')

          end

        end

      else

        @users = User.all

      end

    end

  end

  def reporte_por_evaluado_lista


  end

  def reporte_por_evaluado

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

  end

  def reporte_por_evaluado_pdf

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    user = @hr_process_user.user

    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5)+'.pdf'
    archivo_temporal = directorio_temporal+nombre_temporal

    require 'prawn/measurement_extensions'
    require 'prawn/table'


    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: 'A4') do |pdf|
      pdf.font_size 10

      pdf.start_new_page

      table_header = Array.new
      table_header_row = Array.new

      table_header_row = [content: '<font size="12"><b><color rgb="ffffff">RESULTADO INDIVIDUAL DE EVALUACIÓN DE DESEMEPEÑO</color></b></font>', colspan: 2]
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Proceso</b>'
      table_header_row.push @hr_process.nombre+' '+@hr_process.year.to_s
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Colaborador evaluado</b>'
      table_header_row.push user.apellidos+', '+user.nombre
      table_header.push table_header_row
=begin
      Characteristic.where('hr_process_assessment = ?', true).reorder('orden_hr_process_assessment').each do |characteristic|
        uc = user.user_characteristics.find_by_characteristic_id(characteristic.id)
        if uc
          table_header_row = Array.new
          table_header_row.push '<b>'+characteristic.nombre+'</b>'
          table_header_row.push uc.valor
          table_header.push table_header_row
        end
      end
=end


      pdf.table(table_header, :header => false, :width => 520, :column_widths => [150], :cell_style => { :inline_format => true }) do |table|

        table.column(0).background_color = 'dddddd'
        table.column(0).row(0).background_color = '333333'

      end



      pdf.move_down 20

      table_res_final = Array.new
      table_res_final_row = Array.new

      c_aux = 0

      table_res_final_row = [content: '<font size="12"><b><color rgb="ffffff">1. RESUMEN EJECUTIVO</color></b></font>', colspan: 5]
      table_res_final.push table_res_final_row

      c_aux += 1

      table_res_final_row = Array.new
      table_res_final_row = [content: '<b>1.1. Valoración final</b>', colspan: 5]
      table_res_final.push table_res_final_row

      c_aux += 1

      comentario_calib_por_dim = false


      @hr_process_dimensions.each do |hr_process_dimension|

        c_aux += 1

        table_res_final_row = Array.new


        hr_process_assessment_d =  @hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

        if hr_process_assessment_d

          dim_g =  hr_process_assessment_d.hr_process_dimension_g

          tmp = number_with_precision(hr_process_assessment_d.resultado_porcentaje, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'% '
          tmp +=  dim_g.nombre if dim_g && dim_g.nombre


        else
          tmp = ''
        end

        table_res_final_row = ['<b>'+hr_process_dimension.nombre+'</b>', {content: tmp, colspan: 4}]
        table_res_final.push table_res_final_row

        hr_process_assessment_d_cal = hr_process_assessment_d.hr_process_assessment_d_cal if hr_process_assessment_d

        if comentario_calib_por_dim == false && hr_process_assessment_d_cal && hr_process_assessment_d_cal.comentario

          table_res_final_row = Array.new
          table_res_final_row = ['<b>Comentario de la calibración</b>', {content: hr_process_assessment_d_cal.comentario, colspan: 4}]
          table_res_final.push table_res_final_row

          c_aux += 1

          comentario_calib_por_dim = true

        end

      end

      if @hr_process.muestra_cajas? || @hr_process.muestra_cuadrantes_calib || @hr_process.calib_cuadrante

        hr_process_assessment_qua = @hr_process_user.hr_process_assessment_qua

        if hr_process_assessment_qua

          hr_process_quadrant = hr_process_assessment_qua.hr_process_quadrant

          if hr_process_quadrant && hr_process_quadrant.nombre
            tmp = hr_process_quadrant.nombre+' '+('('+hr_process_quadrant.descripcion+')' if hr_process_quadrant.descripcion)

          else
            tmp = 'No evaluado'
          end

        end

        table_res_final_row = Array.new
        table_res_final_row = ['<b>Valoración</b>', {content: tmp, colspan: 4}]
        table_res_final.push table_res_final_row

        c_aux += 1


        table_res_final_row = Array.new
        table_res_final_row = ['<b>Descripción de la valoración</b>', {content: hr_process_quadrant && hr_process_quadrant.descripcion_detalle ? hr_process_quadrant.descripcion_detalle : '', colspan: 4}]
        table_res_final.push table_res_final_row

        c_aux += 1

        hr_process_calibration_session_m = @hr_process_user.hr_process_calibration_session_ms.first

        if hr_process_calibration_session_m

          hr_process_assessment_qua_cal = HrProcessAssessmentQuaCal.where('hr_process_calibration_session_id = ? AND  hr_process_calibration_session_m_id = ?', hr_process_calibration_session_m.hr_process_calibration_session.id, hr_process_calibration_session_m.id).first

          if hr_process_assessment_qua_cal && hr_process_assessment_qua_cal.comentario

            table_res_final_row = Array.new
            table_res_final_row = ['<b>Comentario de la calibración</b>', {content: hr_process_assessment_qua_cal.comentario, colspan: 4}]
            table_res_final.push table_res_final_row

            c_aux += 1

          end

        end




      end

      table_res_final_row = Array.new
      table_res_final_row = [content: '<b>1.2. Valoración por dimensión y evaluaciones</b>', colspan: 5]
      table_res_final.push table_res_final_row

      c_aux += 1

      table_res_final_row = Array.new
      table_res_final_row.push '<b>Dimensión</b>'
      table_res_final_row.push '<b>Evaluación</b>'
      table_res_final_row.push '<b>Peso</b>'
      table_res_final_row.push '<b>Resultado</b>'
      table_res_final_row.push '<b>Valoración dimensión</b>'
      table_res_final.push table_res_final_row

      table_res_final_row = Array.new
      @hr_process_dimensions.each do |hr_process_dimension|


        hr_process_dimension.hr_process_dimension_es.each_with_index do |hr_process_dimension_e, numero_eval|


          hr_process_evaluation = hr_process_dimension_e.hr_process_evaluations.where('hr_process_id = ? ', @hr_process.id).first

          if hr_process_evaluation

            hr_process_assessment_ef = HrProcessAssessmentEf.where('hr_process_evaluation_id = ? AND hr_process_user_id = ?', hr_process_evaluation.id, @hr_process_user.id).first

            if hr_process_assessment_ef

              tmp = number_with_precision(hr_process_assessment_ef.resultado_porcentaje, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

            else
              tmp = ''
            end

          end

          table_res_final_row = Array.new


          if numero_eval == 0

            hr_process_assessment_d =  @hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

            if hr_process_assessment_d

              tmp_d = number_with_precision(hr_process_assessment_d.resultado_porcentaje, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'% '

            else
              tmp_d = ''
            end


            table_res_final_row = [{content: hr_process_dimension.nombre, rowspan: hr_process_dimension.hr_process_dimension_es.size},
                                   hr_process_dimension_e.hr_evaluation_type.nombre,
                                   hr_process_dimension_e.porcentaje.to_s+'%',
                                   tmp,
                                   {content: tmp_d, rowspan: hr_process_dimension.hr_process_dimension_es.size}]
          else

            table_res_final_row = [hr_process_dimension_e.hr_evaluation_type.nombre,
                                   hr_process_dimension_e.porcentaje.to_s+'%',
                                   tmp]


          end


          table_res_final.push table_res_final_row

        end

      end

      pdf.table(table_res_final, :header => false, :width => 500, :column_widths => [150], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'
        table.column(0).row(c_aux-1).background_color = 'dddddd'

        table.column(2).style :align => :center
        table.column(3).style :align => :center
        table.column(4).style :align => :center

      end

      pdf.move_down 20

      table_res_jefe = Array.new

      table_res_jefe_row = Array.new

      table_res_jefe_row = [content: '<font size="12"><b><color rgb="ffffff">2. EVALUACIÓN DEL SUPERVISOR</color></b></font>', colspan: 2]
      table_res_jefe.push table_res_jefe_row

      c_aux_2 = 0
      c_aux_2_a = Array.new
      c_aux_2p_a = Array.new

      c_aux = 0

      @hr_process_evaluations.each do |hr_process_evaluation|



        if hr_process_evaluation.hr_process_evaluation_qs.count > 0

          c_aux += 1

          hr_process_dimension_gs = hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC')

          hr_process_evaluation_q = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL').first

          if hr_process_evaluation_q && hr_process_evaluation_q.hr_evaluation_type_element.aplica_evaluacion

            hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL')

          else

            hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NOT NULL')

          end

          pregs_por_nombre_suma_jefe = Hash.new
          pregs_por_nombre_num_jefe = Hash.new

          hr_process_evaluation_qs.each_with_index do |hr_process_evaluation_q, num_preg|

            s_jefe = 0
            t_jefe = 0

            @hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).each do |hr_process_assessment_q|

              if hr_process_assessment_q.resultado_puntos && hr_process_assessment_q.resultado_puntos >= 0 && hr_process_assessment_q.resultado_porcentaje && hr_process_assessment_q.resultado_porcentaje >= 0

                if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'jefe'
                  s_jefe += hr_process_assessment_q.resultado_porcentaje
                  t_jefe += 1
                end

              end

            end

            pregs_por_nombre_suma_jefe[hr_process_evaluation_q.texto] = 0 unless pregs_por_nombre_suma_jefe[hr_process_evaluation_q.texto]
            pregs_por_nombre_num_jefe[hr_process_evaluation_q.texto] = 0 unless pregs_por_nombre_num_jefe[hr_process_evaluation_q.texto]

            pregs_por_nombre_suma_jefe[hr_process_evaluation_q.texto] += s_jefe
            pregs_por_nombre_num_jefe[hr_process_evaluation_q.texto] += t_jefe

          end

          num_preg = 0


          table_res_jefe_row = Array.new

          c_aux_2 += 1

          c_aux_2_a.push c_aux_2

          table_res_jefe_row = [content: '<b>2.'+c_aux.to_s+'. '+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre+'</b>', colspan: 2]
          table_res_jefe.push table_res_jefe_row

          t = 0
          s = 0
          pregs_por_nombre_suma_jefe.sort.each do |nombre_preg, value|

            if value > 0

              c_aux_2 +=1

              table_res_jefe_row = Array.new

              res = pregs_por_nombre_num_jefe[nombre_preg] > 0 ? (value/pregs_por_nombre_num_jefe[nombre_preg]).round(1) : 0

              s = s + res
              t = t + 1

              num_preg += 1

              res_width = res > 100 ? 100 : res

              color = ''
              hr_process_dimension_gs.each do |g|

                if g.valor_minimo <= res && res <= g.valor_maximo
                  color = g.color_text
                  break
                end

              end

              color = ''

              table_res_jefe_row.push ActionView::Base.full_sanitizer.sanitize nombre_preg.dup
              table_res_jefe_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

              table_res_jefe.push table_res_jefe_row

            end

          end

          prom = t > 0 ? (s/t).round(1) : 0

          prom_width = prom > 100 ? 100 : prom

          color = ''
          hr_process_dimension_gs.each do |g|

            if g.valor_minimo <= prom && prom <= g.valor_maximo
              color = g.color_text
              break
            end

          end

          color = ''

          c_aux_2 +=1

          c_aux_2p_a.push c_aux_2

          table_res_jefe_row = Array.new
          table_res_jefe_row.push '<b>PROMEDIO</b>'
          table_res_jefe_row.push '<b>'+number_with_precision(prom, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%</b>'

          table_res_jefe.push table_res_jefe_row



        end

      end

      pdf.table(table_res_jefe, :header => false, :width => 520, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|

        table.column(1).style :align => :center

        table.column(0).row(0).background_color = '333333'

        c_aux_2_a.each do |n|
          table.column(0).row(n).background_color = 'dddddd'
        end

        c_aux_2p_a.each do |n|
          table.column(0).row(n).style :align => :right
        end

      end

      pdf.move_down 20


      table_res_todos = Array.new

      table_res_todos_row = Array.new

      table_res_todos_row = [content: '<font size="12"><b><color rgb="ffffff">3. EVALUACIÓN DE TODOS LOS EVALUADORES</color></b></font>', colspan: 2]
      table_res_todos.push table_res_todos_row

      mostrar_evals_todos_evaluadores = false

      c_aux_2 = 0
      c_aux_2_a = Array.new
      c_aux_2p_a = Array.new

      c_aux = 0

      @hr_process_evaluations.each do |hr_process_evaluation|

        si_auto = false
        si_par = false
        si_sub = false
        si_cli = false
        si_prov = false

        si_par = true if hr_process_evaluation.hr_process_dimension_e.eval_par && hr_process_evaluation.hr_process_dimension_e.eval_par_peso > 0
        si_auto = true if hr_process_evaluation.hr_process_dimension_e.eval_auto && hr_process_evaluation.hr_process_dimension_e.eval_auto_peso > 0
        si_sub = true if hr_process_evaluation.hr_process_dimension_e.eval_sub && hr_process_evaluation.hr_process_dimension_e.eval_sub_peso > 0
        si_cli = true if hr_process_evaluation.hr_process_dimension_e.eval_cli && hr_process_evaluation.hr_process_dimension_e.eval_cli_peso > 0
        si_prov = true if hr_process_evaluation.hr_process_dimension_e.eval_prov && hr_process_evaluation.hr_process_dimension_e.eval_prov_peso > 0

        if (si_par || si_auto || si_sub || si_cli || si_prov) && hr_process_evaluation.hr_process_evaluation_qs.count > 0

          c_aux += 1

          mostrar_evals_todos_evaluadores = true

          hr_process_dimension_gs = hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC')

          hr_process_evaluation_q = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL').first

          if hr_process_evaluation_q && hr_process_evaluation_q.hr_evaluation_type_element.aplica_evaluacion

            hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL')

          else

            hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NOT NULL')

          end

          pregs_por_nombre_suma = Hash.new
          pregs_por_nombre_num = Hash.new

          hr_process_evaluation_qs.each_with_index do |hr_process_evaluation_q, num_preg|

            s = 0
            t = 0

            @hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).each do |hr_process_assessment_q|

              if hr_process_assessment_q.resultado_puntos && hr_process_assessment_q.resultado_puntos >= 0 && hr_process_assessment_q.resultado_porcentaje && hr_process_assessment_q.resultado_porcentaje >= 0

                peso = 0
                peso = hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'jefe'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_par_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'par'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_sub_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'sub'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_cli_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'cli'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_prov_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'prov'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_auto_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == nil

                s += hr_process_assessment_q.resultado_porcentaje*peso
                t += peso

              end

            end


            pregs_por_nombre_suma[hr_process_evaluation_q.texto] = 0 unless pregs_por_nombre_suma[hr_process_evaluation_q.texto]
            pregs_por_nombre_num[hr_process_evaluation_q.texto] = 0 unless pregs_por_nombre_num[hr_process_evaluation_q.texto]

            pregs_por_nombre_suma[hr_process_evaluation_q.texto] += s
            pregs_por_nombre_num[hr_process_evaluation_q.texto] += t

          end

          c_aux_2 += 1
          c_aux_2_a.push c_aux_2

          num_preg = 0

          table_res_todos_row = [content: '<b>3.'+c_aux.to_s+'.'+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre+'</b>', colspan: 2]

          table_res_todos.push table_res_todos_row

          t = 0
          s = 0

          pregs_por_nombre_suma.sort.each do |nombre_preg, value|

            if value > 0

              c_aux_2 += 1

              res = pregs_por_nombre_num[nombre_preg] > 0 ? (value/pregs_por_nombre_num[nombre_preg]).round(1) : 0
              s = s + res
              t = t + 1
              num_preg += 1

              res_width = res > 100 ? 100 : res

              color = ''

              hr_process_dimension_gs.each do |g|

                if g.valor_minimo <= res && res <= g.valor_maximo
                  color = g.color_text
                  break
                end

              end

              color = ''

              table_res_todos_row = Array.new

              table_res_todos_row.push ActionView::Base.full_sanitizer.sanitize nombre_preg.dup
              table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

              table_res_todos.push table_res_todos_row

            end

          end



          prom = t > 0 ? (s/t).round(1) : 0

          prom_width = prom > 100 ? 100 : prom

          color = ''

      
          hr_process_dimension_gs.each do |g|

            if g.valor_minimo <= prom && prom <= g.valor_maximo
              color = g.color_text
              break
            end

          end

          color = ''

          c_aux_2 += 1

          c_aux_2p_a.push c_aux_2

          table_res_todos_row = Array.new

          table_res_todos_row.push '<b>PROMEDIO</b>'
          table_res_todos_row.push '<b>'+number_with_precision(prom, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%</b>'

          table_res_todos.push table_res_todos_row

          res_jefe = 0
          t_jefe = 0
          res_par = 0
          t_par = 0
          res_cli = 0
          t_cli = 0
          res_sub = 0
          t_sub = 0
          res_prov = 0
          t_prov = 0
          res_auto = 0

          hr_process_evaluation.hr_process_assessment_es.where('hr_process_user_id = ?', @hr_process_user.id).each do |hr_process_assessment_e|

            if hr_process_assessment_e.resultado_puntos && hr_process_assessment_e.resultado_puntos >= 0 && hr_process_assessment_e.resultado_porcentaje && hr_process_assessment_e.resultado_porcentaje >= 0

              case hr_process_assessment_e.tipo_rel
                when 'jefe'
                  res_jefe += hr_process_assessment_e.resultado_porcentaje
                  t_jefe += 1
                when 'par'
                  res_par += hr_process_assessment_e.resultado_porcentaje
                  t_par += 1
                when 'sub'
                  res_sub += hr_process_assessment_e.resultado_porcentaje
                  t_sub += 1
                when 'cli'
                  res_cli += hr_process_assessment_e.resultado_porcentaje
                  t_cli += 1
                when 'prov'
                  res_prov += hr_process_assessment_e.resultado_porcentaje
                  t_prov += 1
                when nil
                  res_auto = hr_process_assessment_e.resultado_porcentaje
                end

            end

          end


          table_res_todos_row = Array.new

          c_aux_2 += 1
          c_aux_2_a.push c_aux_2

          table_res_todos_row = [content: '<b>3.'+c_aux.to_s+'.1. Resultado de '+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre+' por tipo de evaluador</b>', colspan: 2]

          table_res_todos.push table_res_todos_row

          res = t_jefe > 0 ? res_jefe/t_jefe : -1

          if res > -1

            c_aux_2 += 1

            table_res_todos_row = Array.new

            table_res_todos_row.push 'Supervisor'

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'
            table_res_todos.push table_res_todos_row

          end

          res = t_par > 0 ? res_par/t_par : -1
          if res > -1
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push 'Par'

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'
            table_res_todos.push table_res_todos_row
          end


          res = t_sub > 0 ? res_sub/t_sub : -1
          if res > -1
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push 'Colaborador'

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'
            table_res_todos.push table_res_todos_row
          end

          res = t_cli > 0 ? res_cli/t_cli : -1
          if res > -1
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push 'Cliente'

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'
            table_res_todos.push table_res_todos_row

          end

          res = t_prov > 0 ? res_prov/t_prov : -1
          if res > -1
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push 'Proveedor'

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'
            table_res_todos.push table_res_todos_row

          end

          if res_auto > 0
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push 'Autoevaluación'

            res_width = res_auto > 100 ? 100 : res_auto
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res_auto && res_auto <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res_auto, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'
            table_res_todos.push table_res_todos_row

          end

          pdf.table(table_res_todos, :header => false, :width => 520, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|
            table.column(1).style :align => :center

            table.column(0).row(0).background_color = '333333'

            c_aux_2_a.each do |n|
              table.column(0).row(n).background_color = 'dddddd'
            end

            c_aux_2p_a.each do |n|
              table.column(0).row(n).style :align => :right
            end


          end




        end

      end

      c_aux_det = 3

      @hr_process_evaluations.each do |hr_process_evaluation|


        if hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NOT NULL').count > 0

          c_aux_det +=1


          c_aux_2 = 0
          c_aux_2_a = Array.new
          c_aux_2p_a = Array.new

          c_aux = 0

          table_res_det = Array.new
          table_res_det_row = Array.new

          table_res_det_row = [content: '<font size="12"><b><color rgb="ffffff">'+c_aux_det.to_s+'. DETALLE DE '+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre.upcase+'</color></b></font>', colspan: 8]
          table_res_det.push table_res_det_row

          nombre_2nd_nivel = hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = 2').first.nombre

          hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL').each do |hr_process_evaluation_q|

            if @hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).count > 0

              c_aux += 1

              table_res_det_row = Array.new

              c_aux_2 += 1

              c_aux_2_a.push c_aux_2

              table_res_det_row = [content: '<b>'+c_aux_det.to_s+'.'+c_aux.to_s+'. '+ActionView::Base.full_sanitizer.sanitize(hr_process_evaluation_q.texto.dup+'<b>'), colspan: 8]
              table_res_det.push table_res_det_row

              c_aux_2 += 1

              table_res_det_row = Array.new

              table_res_det_row.push '<b>'+nombre_2nd_nivel+'</b>'
              table_res_det_row.push '<b>Sup</b>'
              table_res_det_row.push '<b>Par</b>'
              table_res_det_row.push '<b>Col</b>'
              table_res_det_row.push '<b>Cli</b>'
              table_res_det_row.push '<b>Prov</b>'
              table_res_det_row.push '<b>Aut</b>'
              table_res_det_row.push '<b>Promedio</b>'

              table_res_det.push table_res_det_row

              hr_process_evaluation_q.hr_process_evaluation_qs.each do |hr_process_evaluation_q_hijo|

                res_jefe = 0
                t_jefe = 0
                res_par = 0
                t_par = 0
                res_cli = 0
                t_cli = 0
                res_sub = 0
                t_sub = 0
                res_prov = 0
                t_prov = 0
                res_auto = 0
                t_auto = 0

                @hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q_hijo.id).each do |hr_process_assessment_q|


                  if hr_process_assessment_q.resultado_puntos && hr_process_assessment_q.resultado_puntos >= 0 && hr_process_assessment_q.resultado_porcentaje && hr_process_assessment_q.resultado_porcentaje >= 0

                    if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'jefe'
                      res_jefe += hr_process_assessment_q.resultado_porcentaje
                      t_jefe += 1
                    end

                    if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'par'
                       res_par += hr_process_assessment_q.resultado_porcentaje
                       t_par += 1
                    end

                    if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'sub'
                       res_sub += hr_process_assessment_q.resultado_porcentaje
                       t_sub += 1
                    end

                    if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'cli'
                       res_cli += hr_process_assessment_q.resultado_porcentaje
                       t_cli += 1
                    end

                    if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'prov'
                       res_prov += hr_process_assessment_q.resultado_porcentaje
                       t_prov += 1
                    end

                    if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == nil
                       res_auto = hr_process_assessment_q.resultado_porcentaje
                       t_auto += 1
                    end


                  end

                end

                s_preg_nivel_2 = 0
                t_preg_nivel_2 = 0
                if t_jefe > 0
                  s_preg_nivel_2 += res_jefe/t_jefe
                  t_preg_nivel_2 += 1
                end

                if t_par > 0
                  s_preg_nivel_2 += res_par/t_par
                  t_preg_nivel_2 += 1
                end

                if t_sub > 0
                  s_preg_nivel_2 += res_sub/t_sub
                  t_preg_nivel_2 += 1
                end

                if t_cli > 0
                  s_preg_nivel_2 += res_cli/t_cli
                  t_preg_nivel_2 += 1
                end

                if t_prov > 0
                  s_preg_nivel_2 += res_prov/t_prov
                  t_preg_nivel_2 += 1
                end

                if t_auto > 0
                  s_preg_nivel_2 += res_auto
                  t_preg_nivel_2 += 1
                end

                c_aux_2 += 1


                table_res_det_row = Array.new

                table_res_det_row.push hr_process_evaluation_q_hijo.texto
                table_res_det_row.push t_jefe > 0 ? number_with_precision(res_jefe/t_jefe, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true).to_s+'%' : ''
                table_res_det_row.push t_par > 0 ? number_with_precision(res_par/t_par, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true).to_s+'%' : ''
                table_res_det_row.push t_sub > 0 ? number_with_precision(res_sub/t_sub, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true).to_s+'%' : ''
                table_res_det_row.push t_cli > 0 ? number_with_precision(res_cli/t_cli, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true).to_s+'%' : ''
                table_res_det_row.push t_prov > 0 ? number_with_precision(res_prov/t_prov, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true).to_s+'%' : ''
                table_res_det_row.push t_auto > 0 ? number_with_precision(res_auto, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true).to_s+'%' : ''
                table_res_det_row.push t_preg_nivel_2 > 0 ? number_with_precision(s_preg_nivel_2/t_preg_nivel_2, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true).to_s+'%' : ''

                table_res_det.push table_res_det_row.push

              end


            end

          end


          pdf.move_down 20

          pdf.table(table_res_det, :header => false, :width => 520, :column_widths => [200,60], :cell_style => { :inline_format => true }) do |table|
            #table.column(1).style :align => :center

            table.column(0).row(0).background_color = '333333'

            c_aux_2_a.each do |n|
              table.row(n).background_color = 'dddddd'
            end

          end


        end

      end

    end

    send_file archivo_temporal,
              filename: 'EvaluacionDesempenio - '+@hr_process_user.user.codigo+'.pdf',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end


  def ver_proceso_evaluar

   #@company = session[:company]

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_template = @hr_process.hr_process_template

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

  end

  def ver_proceso_resultados

   #@company = session[:company]

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_template = @hr_process.hr_process_template

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

  end

  def ver_proceso_feedback

   #@company = session[:company]

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_template = @hr_process.hr_process_template

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

  end

  def ver_sesiones_calibracion

    @hr_process_calibration_sessions = @hr_process.hr_process_calibration_sessions


  end

  def ver_proceso_calibrar

    @hr_process_calibration_session = HrProcessCalibrationSession.find(params[:hr_process_calibration_session_id])

    @hr_process_calibration_session_cs = @hr_process_calibration_session.hr_process_calibration_session_cs_order_by_apellidos_nombre

    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @characteristics = Characteristic.where('hr_process_assessment = ?', true).reorder('orden_hr_process_assessment')

    @characteristics_prev = {}

    @hr_process_users = {}

    queries = []

    n_q = 0

    if params[:characteristic]

      @characteristics.each do |characteristic|

        if params[:characteristic][characteristic.id.to_s.to_sym].length > 0

          @characteristics_prev[characteristic.id] = params[:characteristic][characteristic.id.to_s.to_sym]

          queries[n_q] = 'characteristic_id = '+characteristic.id.to_s+' AND valor = "'+@characteristics_prev[characteristic.id]+'"'

          n_q += 1

        end

      end



      if queries.length > 0

        lista_hr_process_users_ids = Array.new

        queries.each_with_index do |query,index|

          if index == 0

            @hr_process_users = HrProcessUser.where('hr_process_id = ? AND '+query,@hr_process.id).joins(user: [:user_characteristics])

            lista_hr_process_users_ids = @hr_process_users.map {|hpu| hpu.id }
          else

            lista_hr_process_users_ids = @hr_process_users.map {|hpu| hpu.id }

            @hr_process_users = HrProcessUser.where('hr_process_id = ? AND '+query+' AND hr_process_users.id IN (?) ',@hr_process.id, lista_hr_process_users_ids).joins(user: [:user_characteristics])

          end

        end



        @hr_process_calibration_session_ms = @hr_process_calibration_session.hr_process_calibration_session_ms_order_by_apellidos_nombre.where('hr_process_user_id IN (?)',lista_hr_process_users_ids)

      else

        @hr_process_calibration_session_ms = @hr_process_calibration_session.hr_process_calibration_session_ms_order_by_apellidos_nombre

      end



    else

      @hr_process_calibration_session_ms = @hr_process_calibration_session.hr_process_calibration_session_ms_order_by_apellidos_nombre

    end


  end

  def make_calibrar

    done_calibrate = -1

    hr_process_calibration_session_m = HrProcessCalibrationSessionM.find(params[:hr_process_calibration_session_m_id])

    if @hr_process.calib_dimension

      hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

      hr_process_dimensions.each do |hr_process_dimension|

        hr_process_dimension_g = HrProcessDimensionG.find(params["hr_process_dimension_#{hr_process_dimension.id}".to_sym])

        if hr_process_calibration_session_m.hr_process_user.finalizado

          eval_dimension = hr_process_calibration_session_m.hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

          if eval_dimension

            #if hr_process_dimension_g.valor_maximo < eval_dimension.resultado_porcentaje || hr_process_dimension_g.valor_minimo > eval_dimension.resultado_porcentaje

            hr_process_assessment_d_cal = eval_dimension.hr_process_assessment_d_cal

            unless hr_process_assessment_d_cal
              hr_process_assessment_d_cal = HrProcessAssessmentDCal.new

              hr_process_assessment_d_cal.hr_process_assessment_d_id = eval_dimension.id
              hr_process_assessment_d_cal.hr_process_calibration_session_id = @hr_process_calibration_session.id
              hr_process_assessment_d_cal.hr_process_calibration_session_m_id = hr_process_calibration_session_m.id
              hr_process_assessment_d_cal.resultado_porcentaje = eval_dimension.resultado_porcentaje
              hr_process_assessment_d_cal.hr_process_dimension_g = eval_dimension.hr_process_dimension_g
              hr_process_assessment_d_cal.fecha = eval_dimension.fecha
            end

            hr_process_assessment_d_cal.comentario = params[:comentario_calib]

            if hr_process_assessment_d_cal.save

              eval_dimension.fecha = lms_time

              if hr_process_dimension_g.valor_maximo < eval_dimension.resultado_porcentaje

                eval_dimension.resultado_porcentaje = hr_process_dimension_g.valor_maximo

              elsif hr_process_dimension_g.valor_minimo > eval_dimension.resultado_porcentaje

                eval_dimension.resultado_porcentaje = hr_process_dimension_g.valor_minimo

              end

              eval_dimension.hr_process_dimension_g = hr_process_dimension_g

              if eval_dimension.save
                done_calibrate = 1

              else
                done_calibrate = -1 unless done_calibrate == 1

              end

            end

            #else
            #  done_calibrate = 0 unless done_calibrate == 1
            #end

          else
            done_calibrate = -1 unless done_calibrate == 1
          end

        end

      end


    end

    if @hr_process.calib_cuadrante

      hr_process_assessment_qua = hr_process_calibration_session_m.hr_process_user.hr_process_assessment_qua

      if hr_process_assessment_qua

        hr_process_assessment_qua_cal = HrProcessAssessmentQuaCal.where('hr_process_calibration_session_id = ? AND  hr_process_calibration_session_m_id = ?', @hr_process_calibration_session.id, hr_process_calibration_session_m.id).first

        unless hr_process_assessment_qua_cal
          hr_process_assessment_qua_cal = HrProcessAssessmentQuaCal.new
          hr_process_assessment_qua_cal.hr_process_calibration_session_id = @hr_process_calibration_session.id
          hr_process_assessment_qua_cal.hr_process_calibration_session_m_id = hr_process_calibration_session_m.id
          hr_process_assessment_qua_cal.hr_process_quadrant = hr_process_assessment_qua.hr_process_quadrant
          hr_process_assessment_qua_cal.fecha = hr_process_assessment_qua.fecha
        end


        hr_process_assessment_qua_cal.comentario = params[:comentario_calib]


        if hr_process_assessment_qua_cal.save
          done_calibrate = 1 #unless done_calibrate == 0

          hr_process_assessment_qua.fecha = lms_time
          hr_process_assessment_qua.hr_process_quadrant_id = params[:hr_process_quadrant]
          hr_process_assessment_qua.save

        else
          done_calibrate = -1 #unless done_calibrate == 1
        end

      else
        done_calibrate == -1
      end
    end

    if done_calibrate == 1
      flash[:success] = t('activerecord.success.model.hr_process_assessment.calibrate_ok')
    elsif done_calibrate == 0
      flash[:danger] = t('activerecord.error.model.hr_process_assessment.calibrate_no_change')
    elsif done_calibrate == -1
      flash[:danger] = t('activerecord.error.model.hr_process_assessment.calibrate_error')
    end

    redirect_to hr_process_assessment_ver_proceso_calibrar_path(@hr_process, @hr_process_calibration_session)

  end

  def carga_manual

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])
    @hr_process_evaluation_type_element_nivel_2 = @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 2).first

    #@hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    #@hr_process_template = @hr_process.hr_process_template

    #@hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

  end

  def porcentaje_logro

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])

    @hr_process_evaluation_type_element_nivel_2 = @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 2).first

    #@hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id user_connected.id

    #@hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    #@hr_process_template = @hr_process.hr_process_template

    #@hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

  end

  def porcentaje_logro_usuario


   #@company = session[:company]

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)

    @hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])
    @hr_evaluation_type_element = HrEvaluationTypeElement.find(params[:hr_evaluation_type_element_id])

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    @hr_process_evaluation_type_element_nivel_2 = @hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 2).first

  end

  def evaluar

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

   #@company = session[:company]

    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions


  end

  def responder_eval

    hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])

    if @rel

      es_rel_jefe = true if @rel.tipo == 'jefe'
      es_otra_rel = true if @rel.tipo == 'par'
      es_otra_rel = true if @rel.tipo == 'sub'
      es_otra_rel = true if @rel.tipo == 'cli'
      es_otra_rel = true if @rel.tipo == 'prov'

      rel_tipo = @rel.tipo

    else

      rel_tipo = nil

    end

    responder_eval_aux(hr_process_evaluation, es_rel_jefe, false, es_otra_rel, rel_tipo)

    hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.each do |hr_evaluation_type_element|

      if hr_evaluation_type_element.evaluacion_colaborativa
        if es_rel_jefe && hr_process_evaluation.hr_process_dimension_e.eval_auto
          #guardar la auto, hacer q el evaluador es el evaluado
          @hr_process_user_connected = @hr_process_user
          responder_eval_aux(hr_process_evaluation, false, false)

        elsif hr_process_evaluation.hr_process_dimension_e.eval_auto
          #guardar la jefe, hacer q el evaluador es el jefe
          @hr_process_user.hr_process_es_evaluado_rels.each do |rel|

            if rel.tipo == 'jefe'

              @hr_process_user_connected = rel.hr_process_user1
              responder_eval_aux(hr_process_evaluation, true, false)

            end

          end

        end

        break

      end

    end

    flash[:success] = t('activerecord.success.model.hr_process_assessment.evaluate_ok')

    redirect_to hr_process_assessment_evaluar_path(@hr_process, @hr_process_user)


  end


  def estado_avance_para_jefe

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

   #@company = session[:company]

    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions


  end

  def resultado_final_para_jefe

    @hr_process_dimensions = @hr_process.hr_process_template.hr_process_dimensions

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

  end

  def resultado_para_reporte

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

   #@company = session[:company]

    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions


  end

  def feedback

    @hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

   #@company = session[:company]

    @hr_process_template = @hr_process.hr_process_template
    @hr_process_dimensions = @hr_process_template.hr_process_dimensions

  end

  def responder_porcentaje_logro

    hr_process_evaluation = HrProcessEvaluation.find(params[:hr_process_evaluation_id])

    if @rel

      if @rel.tipo == 'jefe'
        es_rel_jefe = true
      end

    end

    responder_eval_aux(hr_process_evaluation, es_rel_jefe, true)

    evalua_colaborador = true

    if @hr_process_user_connected.id == @hr_process_user.id

      evalua_colaborador = false

    end

    hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.each do |hr_evaluation_type_element|

      if hr_evaluation_type_element.evaluacion_colaborativa
        if es_rel_jefe && hr_process_evaluation.hr_process_dimension_e.eval_auto
          #guardar la auto, hacer q el evaluador es el evaluado
          @hr_process_user_connected = @hr_process_user
          responder_eval_aux(hr_process_evaluation, false, false)

        elsif hr_process_evaluation.hr_process_dimension_e.eval_auto
          #guardar la jefe, hacer q el evaluador es el jefe
          @hr_process_user.hr_process_es_evaluado_rels.each do |rel|

            if rel.tipo == 'jefe'

              @hr_process_user_connected = rel.hr_process_user1
              responder_eval_aux(hr_process_evaluation, true, false)

            end

          end

        end

        break

      end

    end

    flash[:success] = t('activerecord.success.model.hr_process_assessment.porcentaje_logro_ok')

    if evalua_colaborador
      redirect_to hr_process_assessment_porcentaje_logro_usuario_path(@hr_process, hr_process_evaluation, hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.first, @hr_process_user)
    else
      redirect_to hr_process_assessment_porcentaje_logro_path(@hr_process, hr_process_evaluation, hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.first)
    end

  end

  def add_manual_q_comment


    hr_process_evaluaion_manual_q = HrProcessEvaluationManualQ.find(params[:hr_process_manual_q_id])

    manual_comment = hr_process_evaluaion_manual_q.hr_process_evaluation_manual_qcs.build
    manual_comment.comentario = params[:comentario]
    manual_comment.fecha = lms_time
    manual_comment.save

    redirect_to hr_process_assessment_porcentaje_logro_usuario_path(@hr_process, hr_process_evaluaion_manual_q.hr_process_evaluation, hr_process_evaluaion_manual_q.hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.first, @hr_process_user)

  end

  def destroy_manual_q_comment


    hr_process_evaluaion_manual_q = HrProcessEvaluationManualQ.find(params[:hr_process_manual_q_id])

    hr_process_evaluation = hr_process_evaluaion_manual_q.hr_process_evaluation

    hr_process_evaluaion_manual_qc = HrProcessEvaluationManualQc.find(params[:hr_process_manual_qc_id])
    hr_process_evaluaion_manual_qc.destroy

    redirect_to hr_process_assessment_porcentaje_logro_usuario_path(@hr_process, hr_process_evaluation, hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.first, @hr_process_user)

  end


  def confirmar_eval


    if params[:hr_process_user_rel_id] == 'autoeval'

      @hr_process_user_connected.finalizada_auto = params[:hr_process_user][:finalizada_auto]
      @hr_process_user_connected.save

      if @hr_process_user_connected.finalizada_auto && @hr_process.ae_avisa_jefe_email
        @hr_process_user_connected.hr_process_es_evaluado_users.where('tipo = ?', 'jefe').each do |jefe|

          LmsMailer.informar_jefe_autoeval_terminada(jefe.user, @hr_process_user_connected.user, @hr_process, @company).deliver

        end
      end

      #if HrProcessAssessmentE.where('hr_process_user_id = ? AND hr_process_user_eval_id = ? AND resultado_puntos = -1', @hr_process_user_connected.id, @hr_process_user_connected.id).count
      #  hr_process_user_rel.no_aplica = true
      #  hr_process_user_rel.save
      #end

      if @hr_process_user_connected.hr_process_es_evaluado_rels.where('finalizada = 0 and tipo <> ?', 'resp').count == 0 && @hr_process_user_connected.finalizada_auto
        @hr_process_user_connected.finalizado = true
        @hr_process_user_connected.save



      end

    else

      hr_process_user_rel = HrProcessUserRel.find(params[:hr_process_user_rel_id])

      hr_process_user_rel.finalizada = params[:hr_process_user_rel][:finalizada]

      hr_process_user_rel.save

      if HrProcessAssessmentE.where('hr_process_user_id = ? AND hr_process_user_eval_id = ? AND resultado_puntos = -1', @hr_process_user.id, @hr_process_user_connected.id).count>0
        hr_process_user_rel.no_aplica = true
        hr_process_user_rel.save
      end

      if debe_realizar_autoevaluacion_proceso(@hr_process, @hr_process_user)

        if @hr_process_user.hr_process_es_evaluado_rels.where('finalizada = 0 and tipo <> ?', 'resp').count == 0 && @hr_process_user.finalizada_auto
          @hr_process_user.finalizado = true
          @hr_process_user.save
        end

      else

        if @hr_process_user.hr_process_es_evaluado_rels.where('finalizada = 0 and tipo <> ?', 'resp').count == 0
          @hr_process_user.finalizado = true
          @hr_process_user.save
        end

      end

    end




    redirect_to hr_process_assessment_evaluar_path(@hr_process, @hr_process_user)

  end

  def confirmar_feedback

    @hr_process_user.feedback_confirmado = params[:hr_process_user][:feedback_confirmado]
    @hr_process_user.texto_feedback = params[:hr_process_user][:texto_feedback]

    @hr_process_user.save

    redirect_to hr_process_assessment_feedback_path(@hr_process, @hr_process_user)

  end

  def create_comment

    hr_process_user_comment = HrProcessUserComment.new(params[:hr_process_user_comment])

    hr_process_user_comment.fecha = lms_time

    hr_process_user_comment.save

    flash[:success] = t('activerecord.success.model.hr_process_user_comment.create_ok')

    if params[:from] == 'logro'
      redirect_to hr_process_assessment_porcentaje_logro_usuario_path(params[:hr_process_id], params[:hr_process_evaluation_id], params[:hr_evaluation_type_element_id], params[:hr_process_user_id])
    elsif params[:from] == 'eval'
      redirect_to pe_assessment_show_path(params[:hr_process_user_id])
    elsif params[:from] == 'feedback'
      redirect_to hr_process_assessment_feedback_path(params[:hr_process_id], params[:hr_process_user_id])
    end

  end

  def destroy_comment

    hr_process_user_comment = HrProcessUserComment.find(params[:id])
    hr_process_user_comment.destroy

    flash[:success] = t('activerecord.success.model.hr_process_user_comment.delete_ok')

    if params[:from] == 'logro'
      redirect_to hr_process_assessment_porcentaje_logro_usuario_path(params[:hr_process_id], params[:hr_process_evaluation_id], params[:hr_evaluation_type_element_id], params[:hr_process_user_id])
    elsif params[:from] == 'eval'
      redirect_to hr_process_assessment_evaluar_path(params[:hr_process_id], params[:hr_process_user_id])
    elsif params[:from] == 'feedback'
      redirect_to hr_process_assessment_feedback_path(params[:hr_process_id], params[:hr_process_user_id])
    end

  end

  private

  def responder_eval_aux(hr_process_evaluation, es_rel_jefe, es_porcentaje_logro, es_otra_rel = false, rel_tipo = 'jefe')

    if es_rel_jefe || es_otra_rel
      HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND hr_process_user_eval_id = ? AND tipo_rel = ?', hr_process_evaluation.id, @hr_process_user.id, @hr_process_user_connected.id, rel_tipo).destroy_all
    elsif hr_process_evaluation.hr_process_dimension_e.eval_auto && @hr_process_user_connected.id == @hr_process_user.id
      HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND hr_process_user_eval_id = ? AND tipo_rel IS NULL', hr_process_evaluation.id, @hr_process_user.id, @hr_process_user_connected.id).destroy_all
    end

    #hr_process_assessment_e.destroy if hr_process_assessment_e

    hr_process_assessment_e = HrProcessAssessmentE.new
    hr_process_assessment_e.fecha = lms_time
    hr_process_assessment_e.hr_process_evaluation = hr_process_evaluation
    hr_process_assessment_e.hr_process_user = @hr_process_user
    hr_process_assessment_e.hr_process_user_eval_id = @hr_process_user_connected.id

    if es_rel_jefe || es_otra_rel
      hr_process_assessment_e.tipo_rel = rel_tipo
    elsif hr_process_evaluation.hr_process_dimension_e.eval_auto && @hr_process_user_connected.id == @hr_process_user.id
      hr_process_assessment_e.tipo_rel = nil
    end

    if hr_process_assessment_e.save

      total_puntos_eval = 0
      total_pesos_eval = 0
      max_total_puntos_eval = 0
      formula_promedio_eval = false
      se_queda_en_no_aplica_eval = true

      if hr_process_evaluation.carga_manual?
        hr_process_evaluation_qs = hr_process_evaluation.manual_questions_nivel_1(@hr_process_user)
      else
        hr_process_evaluation_qs = hr_process_evaluation.questions_nivel_1(@hr_process_user.hr_process_level.id)
      end

      total_puntos_eval_nivel_1_no_aplica = 0
      total_pesos_eval_nivel_1_no_aplica = 0
      max_total_puntos_eval_nivel_1_no_aplica = 0

      hr_process_evaluation_qs.each do |hr_process_evaluation_q|

        total_pesos_eval += hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
        max_total_puntos_eval += hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)
        formula_promedio_eval = hr_process_evaluation_q.hr_evaluation_type_element.formula_promedio

        hr_process_assessment_q = HrProcessAssessmentQ.new
        hr_process_assessment_q.hr_process_assessment_e = hr_process_assessment_e

        if hr_process_evaluation.carga_manual?
          hr_process_assessment_q.hr_process_evaluation_manual_q = hr_process_evaluation_q
        else
          hr_process_assessment_q.hr_process_evaluation_q = hr_process_evaluation_q
        end

        hr_process_assessment_q.hr_process_user = @hr_process_user
        hr_process_assessment_q.fecha = lms_time

        if !hr_process_evaluation.carga_manual? && hr_process_evaluation_q.comentario
          hr_process_assessment_q.comentario = params["preg_#{hr_process_evaluation_q.id}_comment".to_sym]
        end

        if hr_process_assessment_q.save

          if hr_process_evaluation.carga_manual?
            num_pregs = hr_process_evaluation_q.hr_process_evaluation_manual_qs.length
            num_alts = 0
          else
            num_pregs = hr_process_evaluation_q.hr_process_evaluation_qs.length
            num_alts = hr_process_evaluation_q.hr_process_evaluation_as.length
          end

          if num_pregs == 0

            #se asume que se evalúa y no es sólo un agrupador

            if num_alts == 0

              if params["preg_#{hr_process_evaluation_q.id}".to_sym]

                hr_process_assessment_q.resultado_puntos = params["preg_#{hr_process_evaluation_q.id}".to_sym].to_f

                if hr_process_evaluation_q.hr_evaluation_type_element.indicador_logro

                  hr_process_assessment_q.logro_indicador = params["preg_#{hr_process_evaluation_q.id}".to_sym].to_f

                  if hr_process_assessment_q.logro_indicador

                    case hr_process_evaluation_q.tipo_indicador
                      when 1
                        hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador && hr_process_evaluation_q.meta_indicador > 0 ? hr_process_assessment_q.logro_indicador*100/hr_process_evaluation_q.meta_indicador : 0
                      when 2
                        hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador ? hr_process_evaluation_q.meta_indicador*100/hr_process_assessment_q.logro_indicador : 0
                      when 3
                        if hr_process_assessment_q.logro_indicador >= hr_process_evaluation_q.meta_indicador*-1 && hr_process_assessment_q.logro_indicador <= hr_process_evaluation_q.meta_indicador
                          hr_process_assessment_q.resultado_puntos = 100
                        else
                          hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.meta_indicador ? 100-(hr_process_assessment_q.logro_indicador-hr_process_evaluation_q.meta_indicador).abs*100/hr_process_evaluation_q.meta_indicador : 0
                        end
                      when 4
                        if hr_process_evaluation_q.pe_question_ranks.size > 0
                          metas = Array.new
                          porcentajes = Array.new

                          hr_process_evaluation_q.pe_question_ranks.each do |pe_question_rank|

                            metas.push pe_question_rank.goal
                            porcentajes.push pe_question_rank.percentage
                          end

                          if porcentajes.first <= porcentajes.last
                            #directo

                            if hr_process_assessment_q.logro_indicador < metas.first
                              hr_process_assessment_q.resultado_puntos = 0
                              elsif hr_process_assessment_q.logro_indicador >= metas.last
                              hr_process_assessment_q.resultado_puntos = porcentajes.last
                              else

                              meta_inf = meta_dif = 0
                              por_inf = por_dif = 0


                              (0..metas.length-1).each do |num_meta|
                                if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= hr_process_assessment_q.logro_indicador && hr_process_assessment_q.logro_indicador <= metas[num_meta+1]
                                  meta_dif = metas[num_meta+1] - metas[num_meta]
                                  meta_inf = metas[num_meta]
                                  por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                  por_inf = porcentajes[num_meta]
                                  break
                                end
                              end

                              if meta_dif == 0
                                hr_process_assessment_q.resultado_puntos = por_inf
                              elsif meta_dif > 0
                                hr_process_assessment_q.resultado_puntos = ((hr_process_assessment_q.logro_indicador-meta_inf)*por_dif/meta_dif)+por_inf
                              else
                                hr_process_assessment_q.resultado_puntos = 0
                              end

                            end

                          else
                            #inverso
                            if hr_process_assessment_q.logro_indicador <= metas.first
                              hr_process_assessment_q.resultado_puntos = porcentajes.first
                            elsif hr_process_assessment_q.logro_indicador > metas.last
                              hr_process_assessment_q.resultado_puntos = 0
                            else

                              meta_sup = meta_dif = 0
                              por_inf = por_dif = 0


                              (0..metas.length-1).each do |num_meta|
                                if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= hr_process_assessment_q.logro_indicador && hr_process_assessment_q.logro_indicador <= metas[num_meta+1]
                                  meta_dif = metas[num_meta+1] - metas[num_meta]
                                  meta_sup = metas[num_meta+1]
                                  por_dif = porcentajes[num_meta] - porcentajes[num_meta+1]
                                  por_inf = porcentajes[num_meta+1]
                                  break
                                end
                              end

                              if meta_dif == 0
                                hr_process_assessment_q.resultado_puntos = por_inf
                              elsif meta_dif > 0
                                hr_process_assessment_q.resultado_puntos = ((meta_sup-hr_process_assessment_q.logro_indicador)*por_dif/meta_dif)+por_inf
                              else
                                hr_process_assessment_q.resultado_puntos = 0
                              end

                            end

                          end

                        else
                          hr_process_assessment_q.resultado_puntos = 0
                        end


                    end

                    hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.hr_evaluation_type_element.valor_minimo if hr_process_assessment_q.resultado_puntos < hr_process_evaluation_q.hr_evaluation_type_element.valor_minimo
                    hr_process_assessment_q.resultado_puntos = hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo if hr_process_assessment_q.resultado_puntos > hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo


                  end



                end

                if hr_process_evaluation.cien_x_cien_puntos

                  hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                else

                  hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo

                end

                if hr_process_assessment_q.save

                  total_puntos_eval += hr_process_assessment_q.resultado_puntos*hr_process_evaluation_q.peso

                  if hr_process_evaluation_q.hr_evaluation_type_element.indicador_logro && es_rel_jefe && es_porcentaje_logro

                    hr_process_assessment_l = HrProcessAssessmentL.new
                    hr_process_assessment_l.hr_process_evaluation_q_id = hr_process_assessment_q.hr_process_evaluation_q_id
                    hr_process_assessment_l.hr_process_evaluation_manual_q_id = hr_process_assessment_q.hr_process_evaluation_manual_q_id
                    hr_process_assessment_l.hr_process_user_id = hr_process_assessment_q.hr_process_user_id
                    hr_process_assessment_l.hr_process_user_eval_id = @hr_process_user_connected.id
                    hr_process_assessment_l.resultado_puntos = hr_process_assessment_q.resultado_puntos
                    hr_process_assessment_l.resultado_porcentaje = hr_process_assessment_q.resultado_porcentaje
                    hr_process_assessment_l.logro_indicador = hr_process_assessment_q.logro_indicador
                    hr_process_assessment_l.fecha = lms_time

                    hr_process_assessment_l.save

                  end

                end

              end

            else

              if params["preg_#{hr_process_evaluation_q.id}".to_sym]

                hr_process_evaluation_q.hr_process_evaluation_as.each do |hr_process_evaluation_a|

                  if params["preg_#{hr_process_evaluation_q.id}".to_sym] == hr_process_evaluation_a.id.to_s

                    hr_process_assessment_q.hr_process_evaluation_a = hr_process_evaluation_a
                    hr_process_assessment_q.resultado_puntos = hr_process_evaluation_a.valor
                    if hr_process_evaluation.cien_x_cien_puntos
                      hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos
                    else
                      hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo
                    end


                    if hr_process_assessment_q.save

                      if hr_process_evaluation_a.valor > -1

                        se_queda_en_no_aplica_eval=false

                        total_puntos_eval += hr_process_assessment_q.resultado_puntos*hr_process_evaluation_q.peso
                      else

                        total_pesos_eval -= hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
                        max_total_puntos_eval -= hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)
                      end

                    end

                  end


                end

              end

            end

          else
            total_puntos_preg = 0
            total_pesos_preg = 0
            max_total_puntos_preg = 0
            formula_promedio_preg = false
            se_queda_en_no_aplica = true

            if hr_process_evaluation.carga_manual?
              hr_process_evaluation_qs = hr_process_evaluation_q.hr_process_evaluation_manual_qs
            else
              hr_process_evaluation_qs = hr_process_evaluation_q.hr_process_evaluation_qs
            end

            hr_process_evaluation_qs.each do |hr_process_evaluation_q1|

              total_pesos_preg += hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0
              max_total_puntos_preg += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
              formula_promedio_preg = hr_process_evaluation_q1.hr_evaluation_type_element.formula_promedio

              hr_process_assessment_q1 = HrProcessAssessmentQ.new
              hr_process_assessment_q1.hr_process_assessment_e = hr_process_assessment_e

              if hr_process_evaluation.carga_manual?
                hr_process_assessment_q1.hr_process_evaluation_manual_q = hr_process_evaluation_q1
              else
                hr_process_assessment_q1.hr_process_evaluation_q = hr_process_evaluation_q1
              end
              hr_process_assessment_q1.hr_process_user = @hr_process_user
              hr_process_assessment_q1.fecha = lms_time


              if !hr_process_evaluation.carga_manual? && hr_process_evaluation_q1.comentario
                hr_process_assessment_q1.comentario = params["preg_#{hr_process_evaluation_q1.id}_comment".to_sym]
              end

              if hr_process_assessment_q1.save

                if hr_process_evaluation.carga_manual? || hr_process_evaluation_q1.hr_process_evaluation_as.length == 0

                  if params["preg_#{hr_process_evaluation_q1.id}".to_sym]

                    hr_process_assessment_q1.resultado_puntos = params["preg_#{hr_process_evaluation_q1.id}".to_sym].to_f

                    if hr_process_evaluation_q1.hr_evaluation_type_element.indicador_logro

                      hr_process_assessment_q1.logro_indicador = params["preg_#{hr_process_evaluation_q1.id}".to_sym].to_f

                      if hr_process_assessment_q1.logro_indicador

                        case hr_process_evaluation_q1.tipo_indicador
                          when 1
                            hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador && hr_process_evaluation_q1.meta_indicador > 0 ? hr_process_assessment_q1.logro_indicador*100/hr_process_evaluation_q1.meta_indicador : 0
                          when 2
                            hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador ? hr_process_evaluation_q1.meta_indicador*100/hr_process_assessment_q1.logro_indicador : 0
                          when 3
                            if hr_process_assessment_q1.logro_indicador >= hr_process_evaluation_q1.meta_indicador*-1 && hr_process_assessment_q1.logro_indicador <= hr_process_evaluation_q1.meta_indicador
                              hr_process_assessment_q1.resultado_puntos = 100
                            else
                              hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.meta_indicador ? 100-(hr_process_assessment_q1.logro_indicador-hr_process_evaluation_q1.meta_indicador).abs*100/hr_process_evaluation_q1.meta_indicador : 0
                            end

                          when 4
                            if hr_process_evaluation_q1.pe_question_ranks.size > 0
                              metas = Array.new
                              porcentajes = Array.new

                              hr_process_evaluation_q1.pe_question_ranks.each do |pe_question_rank|

                                metas.push pe_question_rank.goal
                                porcentajes.push pe_question_rank.percentage
                              end


                              if porcentajes.first <= porcentajes.last

                                #directo
                                if hr_process_assessment_q1.logro_indicador < metas.first
                                  hr_process_assessment_q1.resultado_puntos = 0
                                elsif hr_process_assessment_q1.logro_indicador >= metas.last
                                  hr_process_assessment_q1.resultado_puntos = porcentajes.last
                                else

                                  meta_inf = meta_dif = 0
                                  por_inf = por_dif = 0


                                  (0..metas.length-1).each do |num_meta|
                                    if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= hr_process_assessment_q1.logro_indicador && hr_process_assessment_q1.logro_indicador <= metas[num_meta+1]
                                      meta_dif = metas[num_meta+1] - metas[num_meta]
                                      meta_inf = metas[num_meta]
                                      por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                                      por_inf = porcentajes[num_meta]
                                      break
                                    end
                                  end

                                  if meta_dif == 0
                                    hr_process_assessment_q1.resultado_puntos = por_inf
                                  elsif meta_dif > 0
                                    hr_process_assessment_q1.resultado_puntos = ((hr_process_assessment_q1.logro_indicador-meta_inf)*por_dif/meta_dif)+por_inf
                                  else
                                    hr_process_assessment_q1.resultado_puntos = 0
                                  end

                                end

                              else
                                #inverso

                                if hr_process_assessment_q1.logro_indicador <= metas.first
                                  hr_process_assessment_q1.resultado_puntos = porcentajes.first
                                elsif hr_process_assessment_q1.logro_indicador > metas.last
                                  hr_process_assessment_q1.resultado_puntos = 0
                                else

                                  meta_sup = meta_dif = 0
                                  por_inf = por_dif = 0


                                  (0..metas.length-1).each do |num_meta|
                                    if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= hr_process_assessment_q1.logro_indicador && hr_process_assessment_q1.logro_indicador <= metas[num_meta+1]
                                      meta_dif = metas[num_meta+1] - metas[num_meta]
                                      meta_sup = metas[num_meta+1]
                                      por_dif = porcentajes[num_meta] - porcentajes[num_meta+1]
                                      por_inf = porcentajes[num_meta+1]
                                      break
                                    end
                                  end

                                  if meta_dif == 0
                                    hr_process_assessment_q1.resultado_puntos = por_inf
                                  elsif meta_dif > 0
                                    hr_process_assessment_q1.resultado_puntos = ((meta_sup-hr_process_assessment_q1.logro_indicador)*por_dif/meta_dif)+por_inf
                                  else
                                    hr_process_assessment_q1.resultado_puntos = 0
                                  end

                                end


                              end

                            else
                              hr_process_assessment_q1.resultado_puntos = 0
                            end


                        end

                        hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.hr_evaluation_type_element.valor_minimo if hr_process_assessment_q1.resultado_puntos < hr_process_evaluation_q1.hr_evaluation_type_element.valor_minimo
                        hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo if hr_process_assessment_q1.resultado_puntos > hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo



                      end

                    end


                    if hr_process_evaluation.cien_x_cien_puntos

                      hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                    else

                      hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo

                    end



                    if hr_process_assessment_q1.save

                      total_puntos_preg += hr_process_assessment_q1.resultado_puntos*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                      total_puntos_eval_nivel_1_no_aplica += hr_process_assessment_q1.resultado_puntos*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
                      total_pesos_eval_nivel_1_no_aplica += (hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                      max_total_puntos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)

                    end

                  end

                elsif hr_process_evaluation_q1.hr_process_evaluation_as.length > 0

                  # si tiene alternativas no puede haber sido carga manual

                  if params["preg_#{hr_process_evaluation_q1.id}".to_sym]

                    hr_process_evaluation_q1.hr_process_evaluation_as.each do |hr_process_evaluation_a|

                      if params["preg_#{hr_process_evaluation_q1.id}".to_sym] == hr_process_evaluation_a.id.to_s

                        hr_process_assessment_q1.hr_process_evaluation_a = hr_process_evaluation_a
                        hr_process_assessment_q1.resultado_puntos = hr_process_evaluation_a.valor

                        if hr_process_evaluation.cien_x_cien_puntos

                          hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation.cien_x_cien_puntos

                        else

                          hr_process_assessment_q1.resultado_porcentaje = hr_process_assessment_q1.resultado_puntos*100/hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo

                        end

                        if hr_process_assessment_q1.save


                          if hr_process_evaluation_a.valor > -1

                            se_queda_en_no_aplica = false

                            total_puntos_preg += hr_process_assessment_q1.resultado_puntos*hr_process_evaluation_q1.peso

                            total_puntos_eval_nivel_1_no_aplica += hr_process_assessment_q1.resultado_puntos*hr_process_evaluation_q1.peso
                            total_pesos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.peso

                            max_total_puntos_eval_nivel_1_no_aplica += hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*hr_process_evaluation_q1.peso
                          else
                            total_pesos_preg -= hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0
                            max_total_puntos_preg -= hr_process_evaluation_q1.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q1.peso ? hr_process_evaluation_q1.peso : 0)
                          end

                        end

                      end

                    end

                  end

                end

              end

            end



            if se_queda_en_no_aplica

              hr_process_assessment_q.resultado_puntos = -1

            else

              se_queda_en_no_aplica_eval = false

              if formula_promedio_preg

                if total_pesos_preg > 0

                  hr_process_assessment_q.resultado_puntos = total_puntos_preg/total_pesos_preg
                  if hr_process_evaluation.cien_x_cien_puntos
                    max_total_puntos_preg = hr_process_evaluation.cien_x_cien_puntos
                  else
                    max_total_puntos_preg = max_total_puntos_preg/total_pesos_preg
                  end

                else

                  hr_process_assessment_q.resultado_puntos = 0
                  max_total_puntos_preg = 0

                end

              else

                hr_process_assessment_q.resultado_puntos = total_puntos_preg

              end

            end

            if max_total_puntos_preg > 0

              hr_process_assessment_q.resultado_porcentaje = hr_process_assessment_q.resultado_puntos*100/max_total_puntos_preg

            else

              hr_process_assessment_q.resultado_porcentaje = 0

            end



            if hr_process_assessment_q.save

              if hr_process_assessment_q.resultado_puntos > -1

                total_puntos_eval += hr_process_assessment_q.resultado_puntos*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)

              else

                total_pesos_eval -= hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0
                max_total_puntos_eval -= hr_process_evaluation_q.hr_evaluation_type_element.valor_maximo*(hr_process_evaluation_q.peso ? hr_process_evaluation_q.peso : 0)

              end

            end


          end

        end



      end



      if formula_promedio_eval


        if hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 1).first.aplica_evaluacion

          if total_pesos_eval > 0

            if se_queda_en_no_aplica_eval
              hr_process_assessment_e.resultado_puntos = -1
            else
              hr_process_assessment_e.resultado_puntos = total_puntos_eval/total_pesos_eval
            end

            max_total_puntos_eval = max_total_puntos_eval/total_pesos_eval

          else

            if se_queda_en_no_aplica_eval
              hr_process_assessment_e.resultado_puntos = -1
            else
              hr_process_assessment_e.resultado_puntos = 0
            end

            max_total_puntos_eval = 0

          end

        else

          if total_pesos_eval_nivel_1_no_aplica > 0



            hr_process_assessment_e.resultado_puntos = total_puntos_eval_nivel_1_no_aplica/total_pesos_eval_nivel_1_no_aplica
            max_total_puntos_eval = max_total_puntos_eval_nivel_1_no_aplica/total_pesos_eval_nivel_1_no_aplica

          else

            hr_process_assessment_e.resultado_puntos = 0
            max_total_puntos_eval = 0

          end

        end


      else

        if hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 1).first.aplica_evaluacion

          if se_queda_en_no_aplica_eval
            hr_process_assessment_e.resultado_puntos = -1
          else
            hr_process_assessment_e.resultado_puntos = total_puntos_eval
          end

        else

          if se_queda_en_no_aplica_eval

            hr_process_assessment_e.resultado_puntos = -1
          else
            hr_process_assessment_e.resultado_puntos = total_puntos_eval_nivel_1_no_aplica
          end

        end

      end

      max_total_puntos_eval = hr_process_evaluation.cien_x_cien_puntos if hr_process_evaluation.cien_x_cien_puntos

      if max_total_puntos_eval > 0

        hr_process_assessment_e.resultado_porcentaje = hr_process_assessment_e.resultado_puntos*100/max_total_puntos_eval

      else

        hr_process_assessment_e.resultado_porcentaje = 0

      end

      hr_process_assessment_e.save

      if hr_process_evaluation.tope_no_aplica

        type_element_second_orden = hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.last
        if type_element_second_orden
          total_preguntas_contestadas_no_aplica = hr_process_assessment_e.hr_process_assessment_qs.where('resultado_puntos = -1 AND hr_process_evaluation_q_id NOT IN(?)',hr_process_evaluation.questions_nivel_1(@hr_process_user.hr_process_level.id).map(&:id)).count if hr_process_assessment_e
          total_preguntas_por_contestar_no_aplica = hr_process_evaluation.total_number_of_questions_type_element(@hr_process_user.hr_process_level.id, type_element_second_orden.id)
        else
          total_preguntas_contestadas_no_aplica = hr_process_assessment_e.hr_process_assessment_qs.where('resultado_puntos = -1').count if hr_process_assessment_e
          total_preguntas_por_contestar_no_aplica = total_preguntas_por_contestar
        end

        if (total_preguntas_contestadas_no_aplica*100/total_preguntas_por_contestar_no_aplica)>hr_process_evaluation.tope_no_aplica
          hr_process_assessment_e.resultado_puntos = -1
          hr_process_assessment_e.save
        end

      end

      calculate_final_results hr_process_assessment_e.hr_process_evaluation.hr_process_dimension_e.hr_process_dimension

    end

  end

  def calculate_final_results(hr_process_dimension)

    hr_process_evaluations = @hr_process.hr_process_evaluations.find_all_by_vigente(true)

    dimension_es = hr_process_dimension.hr_process_dimension_es

    resultado_dimension = 0

    hr_process_evaluations.each do |hr_process_evaluation|

      if hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.id == hr_process_dimension.id


        HrProcessAssessmentEf.where('hr_process_evaluation_id = ? AND hr_process_user_id = ?', hr_process_evaluation.id, @hr_process_user.id).destroy_all


        resultado_evaluation = 0
        suma_pesos_evaluation = 0
        total_puntos_evaluation = 0

        HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND resultado_puntos <> - 1', hr_process_evaluation.id, @hr_process_user.id).each do |hr_process_assessment_e|

          peso = 0

          if hr_process_assessment_e.tipo_rel == 'jefe'
            peso = hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso
          elsif hr_process_assessment_e.tipo_rel.nil?
            peso = hr_process_evaluation.hr_process_dimension_e.eval_auto_peso
          elsif hr_process_assessment_e.tipo_rel == 'sub'
            peso = hr_process_evaluation.hr_process_dimension_e.eval_sub_peso
          elsif hr_process_assessment_e.tipo_rel == 'par'
            peso = hr_process_evaluation.hr_process_dimension_e.eval_par_peso
          elsif hr_process_assessment_e.tipo_rel == 'cli'
            peso = hr_process_evaluation.hr_process_dimension_e.eval_cli_peso
          elsif hr_process_assessment_e.tipo_rel == 'prov'
            peso = hr_process_evaluation.hr_process_dimension_e.eval_prov_peso
          end

          suma_pesos_evaluation += peso
          resultado_evaluation += hr_process_assessment_e.resultado_porcentaje * peso
          total_puntos_evaluation += 100 * peso
          #if peso > 0
          # puts '...'
          # puts hr_process_assessment_e.resultado_porcentaje
          # puts resultado_evaluation
          #end
        end

        if suma_pesos_evaluation > 0

          porcentaje_evaluation = resultado_evaluation*100/total_puntos_evaluation
          resultado_evaluation = resultado_evaluation/suma_pesos_evaluation

          hr_process_assessment_ef = HrProcessAssessmentEf.new
          hr_process_assessment_ef.fecha = lms_time
          hr_process_assessment_ef.hr_process_evaluation = hr_process_evaluation
          hr_process_assessment_ef.hr_process_user = @hr_process_user
          hr_process_assessment_ef.resultado_puntos = resultado_evaluation
          hr_process_assessment_ef.resultado_porcentaje = porcentaje_evaluation
          hr_process_assessment_ef.save


          resultado_dimension += porcentaje_evaluation*hr_process_evaluation.hr_process_dimension_e.porcentaje

        end

      end

    end

    resultado_dimension = resultado_dimension/100

    hr_process_assessment_d = @hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

    unless hr_process_assessment_d

      hr_process_assessment_d = @hr_process_user.hr_process_assessment_ds.new
      hr_process_assessment_d.hr_process_dimension = hr_process_dimension

    end

    hr_process_assessment_d.resultado_porcentaje = resultado_dimension



    #hr_process_dimension_g_match = hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').where('valor_minimo <= ? AND valor_maximo >= ?', hr_process_assessment_d.resultado_porcentaje, hr_process_assessment_d.resultado_porcentaje).first


    hr_process_dimension_g_match = nil

    hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

      if hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

        hr_process_dimension_g_match = hr_process_dimension_g
        break

      end

    end

    hr_process_assessment_d.hr_process_dimension_g = hr_process_dimension_g_match if hr_process_dimension_g_match

    hr_process_assessment_d.fecha = lms_time

    hr_process_assessment_d.save

    hr_process_quadrant = @hr_process_user.quadrant

    if hr_process_quadrant
      qua = @hr_process_user.hr_process_assessment_qua
      unless qua
        qua = HrProcessAssessmentQua.new
        qua.hr_process_user = @hr_process_user
      end
      qua.fecha = lms_time
      qua.hr_process_quadrant = hr_process_quadrant
      qua.save

    end
  end

  def acceso_ver_proceso

    @hr_process = HrProcess.find(params[:hr_process_id])

    #unless @hr_process.fecha_inicio <= lms_time && lms_time <= @hr_process.fecha_fin
    unless @hr_process.abierto && @hr_process.fecha_inicio < lms_time

      flash[:danger] = t('security.no_access_hr_perform_evaluation_time')
      redirect_to root_path

    end

  end

  def acceso_gestionar_proceso

    @hr_process = HrProcess.find(params[:hr_process_id])

    hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

    unless hr_process_manager

      flash[:danger] = t('security.no_access_hr_process_manager')
      redirect_to root_path

    end

  end

  def acceso_reportes_proceso

    @hr_process = HrProcess.find(params[:hr_process_id])

    hr_process_manager = user_connected.hr_process_managers.where('hr_process_id = ?', @hr_process.id).first

    eval_desemp_resp = false

    user_connected.hr_process_users.where('hr_process_id = ?', @hr_process.id).each do |hr_process_user|

      if hr_process_user.hr_process_evalua_rels.where('tipo = "resp"').size > 0
        eval_desemp_resp = true
        break
      end
    end

    ver_calibrar = true

    if params[:cs_id] && params[:cs_id].to_i > 0
      @hr_process_calibration_session = HrProcessCalibrationSession.find params[:cs_id]

      hr_process_calibration_session_c = @hr_process_calibration_session.hr_process_calibration_session_cs.where('user_id = ? AND manager = ?', user_connected.id, true).first
      hr_process_calibration_session_m = @hr_process_calibration_session.hr_process_calibration_session_ms.where('hr_process_user_id = ? ', params[:hr_process_user_id]).first

      unless hr_process_calibration_session_c && hr_process_calibration_session_m
        ver_calibrar = false
      end

    end

    unless hr_process_manager || eval_desemp_resp || ver_calibrar

      flash[:danger] = t('security.no_access_hr_process_manager')
      redirect_to root_path

    end

  end

  def accesso_ver_evaluar

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    @hr_process_user_connected = @hr_process.hr_process_users.find_by_user_id(user_connected.id)

    @rel = @hr_process_user_connected.hr_process_evalua_rels.where('hr_process_user2_id = ?', @hr_process_user.id).first


    unless @rel || @hr_process_user.id == @hr_process_user_connected.id

      flash[:danger] = t('security.no_access_hr_perform_evaluation')
      redirect_to root_path

    end

  end

  def accesso_evaluar

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    if @hr_process_user.finalizado

      flash[:danger] = t('security.no_access_hr_perform_evaluation')
      redirect_to root_path

    end

  end

  def accesso_confirmar_feedback

    @hr_process_user = HrProcessUser.find(params[:hr_process_user_id])

    unless @hr_process_user.finalizado

      flash[:danger] = t('security.no_access_hr_perform_evaluation')
      redirect_to root_path

    end

  end



  def acceso_ver_calibrar

    acceso = false

    @hr_process.hr_process_calibration_sessions.each do |calibration_session|

      hay = calibration_session.hr_process_calibration_session_cs.where('user_id = ?', user_connected.id).size

      if hay > 0
        acceso = true
        break
      end

    end

    unless acceso

      flash[:danger] = t('security.no_access_hr_process_manager')
      redirect_to root_path

    end

  end

  def acceso_calibrar

    @hr_process_calibration_session = HrProcessCalibrationSession.find(params[:hr_process_calibration_session_id])

    @hr_process = @hr_process_calibration_session.hr_process

    hr_process_calibration_session_c = @hr_process_calibration_session.hr_process_calibration_session_cs.where('user_id = ? AND manager = ?', user_connected.id, true).first

    unless hr_process_calibration_session_c

      flash[:danger] = t('security.no_access_hr_process_manager')
      redirect_to root_path

    end

  end


end
