# == Schema Information
#
# Table name: ben_user_custom_reports
#
#  id                   :integer          not null, primary key
#  ben_custom_report_id :integer
#  user_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class BenUserCustomReport < ActiveRecord::Base
  belongs_to :ben_custom_report
  belongs_to :user
  attr_accessible :ben_custom_report_id,
                  :user_id

  def full_name
    return nil unless user
    return user.apellidos.to_s+', '+user.nombre.to_s
  end
end
