# == Schema Information
#
# Table name: pe_assessment_groups
#
#  id                     :integer          not null, primary key
#  pe_process_id          :integer
#  pe_evaluation_group_id :integer
#  pe_member_id           :integer
#  percentage             :float
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class PeAssessmentGroup < ActiveRecord::Base
  belongs_to :pe_process
  belongs_to :pe_evaluation_group
  belongs_to :pe_member
  attr_accessible :percentage
end
