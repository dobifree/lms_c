# == Schema Information
#
# Table name: tracking_form_answers
#
#  id                        :integer          not null, primary key
#  tracking_form_instance_id :integer
#  position                  :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  tracking_form_question_id :integer
#

class TrackingFormAnswer < ActiveRecord::Base
  belongs_to :tracking_form_instance
  belongs_to :tracking_form_question
  has_one :tracking_form_due_date, :dependent => :destroy
  has_many :tracking_form_values, :dependent => :destroy
  accepts_nested_attributes_for :tracking_form_due_date
  accepts_nested_attributes_for :tracking_form_values, :allow_destroy => true
  attr_accessible :position, :tracking_form_instance_id, :tracking_form_question_id, :tracking_form_due_date_attributes, :tracking_form_values_attributes

  default_scope { order(:position) }

end
