# == Schema Information
#
# Table name: sc_reportings
#
#  id                     :integer          not null, primary key
#  sc_process_id          :integer
#  reported_at            :datetime
#  reported_by_user_id    :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  partial_report         :boolean          default(FALSE)
#

class ScReporting < ActiveRecord::Base
  belongs_to :sc_process
  belongs_to :reported_by_user, class_name: 'User', foreign_key: :reported_by_user_id
  has_many :sc_reported_records

  attr_accessible :reported_at,
                  :reported_by_user_id,
                  :sc_process_id,
                  :partial_report
end
