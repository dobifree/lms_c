# == Schema Information
#
# Table name: kpi_rols
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  kpi_dashboard_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class KpiRol < ActiveRecord::Base

  belongs_to :kpi_dashboard

  has_many :kpi_indicator_rols, dependent: :destroy

  attr_accessible :name, :kpi_dashboard_id

  validates :name, presence: true

  default_scope order('name')

end
