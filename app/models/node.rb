# == Schema Information
#
# Table name: nodes
#
#  id         :integer          not null, primary key
#  nombre     :text
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  node_id    :integer
#

class Node < ActiveRecord::Base

  belongs_to :owner, class_name: 'User', foreign_key: 'user_id' #dueño del nodo/puesto
  belongs_to :node

  has_many :members, class_name: 'User', foreign_key: 'node_id' #dependen del puesto
  has_many :nodes

  attr_accessible :nombre, :node_id

  def self.root_nodes
    Node.where('node_id IS NULL')
  end

  def self.create_graph


    nodes = Node.eager_load(:owner)

    return nodes

  end

  def self.create_graph1


    nodes = Node

    return nodes

  end

end
