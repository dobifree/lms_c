# == Schema Information
#
# Table name: pe_question_descriptions
#
#  id                        :integer          not null, primary key
#  description               :text
#  pe_question_id            :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  pe_element_description_id :integer
#

class PeQuestionDescription < ActiveRecord::Base

  belongs_to :pe_question
  belongs_to :pe_element_description

  attr_accessible :description

  validates :description, presence: true

end
