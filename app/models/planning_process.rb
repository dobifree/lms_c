# == Schema Information
#
# Table name: planning_processes
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  abierto    :boolean          default(TRUE)
#  fecha_fin  :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  year       :integer
#

class PlanningProcess < ActiveRecord::Base

  has_many :planning_process_company_units
  has_many :company_units, through: :planning_process_company_units

  has_many :dncs
  has_one :program

  attr_accessible :abierto, :fecha_fin, :nombre, :year

  before_save :ajusta_fecha_fin

  validates :nombre, presence: true, uniqueness: { case_sensitive: false, scope: :year }
  validates :fecha_fin, presence: true
  #validates :year, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: lms_time.strftime('%Y').to_i }, on: :create
  #validates :year, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: lms_time.strftime('%Y').to_i-1 }, on: :update
  validates :year, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: Time.now.utc.strftime('%Y').to_i-10 }

  default_scope order('abierto DESC, year DESC, nombre')

  def ajusta_fecha_fin

    if self.fecha_fin

      self.fecha_fin = self.fecha_fin+((24*60*60)-1).seconds

    end

  end

  def not_company_units
    CompanyUnit.where('id NOT IN (?) ', self.company_units.map {|cu| cu.id})
  end

end
