# == Schema Information
#
# Table name: j_functions
#
#  id            :integer          not null, primary key
#  description   :text
#  main_function :boolean
#  j_job_id      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class JFunction < ActiveRecord::Base

  belongs_to :j_job

  attr_accessible :description, :main_function, :j_job_id

  validates :description, presence: true, uniqueness: { case_sensitive: false, scope: :j_job_id  }
  validates :j_job_id, presence: true

  default_scope order('main_function DESC, description')

end
