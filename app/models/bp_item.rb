# == Schema Information
#
# Table name: bp_items
#
#  id                      :integer          not null, primary key
#  bp_form_id              :integer
#  name                    :string(255)
#  description             :text
#  position                :integer
#  item_type               :integer
#  mandatory               :boolean          default(TRUE)
#  active                  :boolean          default(TRUE)
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#  prev_item_id            :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  next_item_id            :integer
#  regularizable           :boolean          default(FALSE)
#

class BpItem < ActiveRecord::Base
  belongs_to :bp_form
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id
  belongs_to :prev_item, class_name: 'BpItem', foreign_key: :prev_item_id

  has_many :bp_item_options
  has_many :lic_item_values

  accepts_nested_attributes_for :bp_item_options, :allow_destroy => true


  attr_accessible :bp_form_id,
                  :name,
                  :description,
                  :position,
                  :mandatory,
                  :item_type,
                  :active,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id,
                  :prev_item_id,
                  :next_item_id,
                  :regularizable,
                  :bp_item_options_attributes

  validates :name, presence: true
  validates :position, presence: true
  validates :mandatory, inclusion: { in: [true, false] }
  validates :regularizable, inclusion: { in: [true, false] }
  validates :item_type, presence: true
  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_admin_id, presence: true, unless: :active?
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true
  validates :prev_item_id, uniqueness: true, allow_nil: true

  validate :position_uniqueness

  def active_options
    bp_item_options.where(active: true).reorder('position ASC')
  end

  def select_active_options
    select_options = []
    active_options.each { |option| select_options << [option.name, option.id] }
    select_options
  end

  def self.all_data_types
    [:string, :text, :date, :int, :float, :list, :file, :ct_image_url, :boss_code, :boss_full_name, :boss_characteristic, :register, :user_code, :first_owned_node_string, :first_owned_node_list, :decimal, :datetime, :time]
    #   0        1      2     3       4     5      6         7              8              9                 10               11          12                13                       14               15        16        17
  end

  def self.all_data_types_names
    ['Cadena', 'Texto', 'Fecha', 'Entero', 'Flotante', 'Lista', 'Archivo','','','','','','', '','','Decimal', 'Fecha y hora', 'Hora']
    #   0          1      2         3          4          5         6     7  8  9 10 11  12  13 14   15        16               17
  end

  def self.supported_data_types
    [0, 1, 2, 3, 4, 5, 6, 15, 16, 17]
  end

  def self.available_data_types
    supported_char_data_types = BpItem.supported_data_types
    available_data_types = []
    supported_char_data_types.each { |pos| available_data_types << BpItem.all_data_types[pos] }
    available_data_types
  end

  def self.available_data_types_names
    supported_char_data_types = BpItem.supported_data_types
    available_data_types = []
    supported_char_data_types.each { |pos| available_data_types << BpItem.all_data_types_names[pos] }
    available_data_types
  end

  def self.select_available_data_types
    supported_char_data_types = BpItem.supported_data_types
    available_data_types = []
    supported_char_data_types.each { |pos| available_data_types << [BpItem.all_data_types_names[pos], pos] }
    available_data_types
  end


  def item_type_name
    return unless item_type
    BpItem.all_data_types_names[item_type]
  end

  def item_string?
    return false unless item_type
    item_type == 0
  end

  def item_text?
    return false unless item_type
    item_type == 1
  end

  def item_date?
    return false unless item_type
    item_type == 2
  end

  def item_int?
    return false unless item_type
    item_type == 3
  end

  def item_float?
    return false unless item_type
    item_type == 4
  end

  def item_list?
    return false unless item_type
    item_type == 5
  end

  def item_file?
    return false unless item_type
    item_type == 6
  end

  def item_decimal?
    return false unless item_type
    item_type == 15
  end

  def item_datetime?
    return false unless item_type
    item_type == 16
  end

  def item_time?
    return false unless item_type
    item_type == 17
  end

  private

  def position_uniqueness
    return unless position
    return unless bp_form_id
    return unless active
    items = BpItem.where(bp_form_id: bp_form_id, active: true)
    items.each do |item|
      next unless item.position == position
      next if (id && item.id == id) || !id || (prev_item_id == item.id)
      errors.add(:position, 'no puede repetirse para el mismo formulario')
    end
  end
end
