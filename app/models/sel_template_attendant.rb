# == Schema Information
#
# Table name: sel_template_attendants
#
#  id          :integer          not null, primary key
#  sel_step_id :integer
#  user_id     :integer
#  role        :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SelTemplateAttendant < ActiveRecord::Base
  belongs_to :sel_step
  belongs_to :user
  attr_accessible :role,
                  :sel_step, :sel_step_id,
                  :user, :user_id
end
