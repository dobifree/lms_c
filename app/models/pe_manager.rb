# == Schema Information
#
# Table name: pe_managers
#
#  id            :integer          not null, primary key
#  pe_process_id :integer
#  user_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class PeManager < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :user

  # attr_accessible :title, :body

  validates :pe_process, presence: true
  validates :user, presence: true
  validates :user_id, uniqueness: { scope: :pe_process_id }

end
