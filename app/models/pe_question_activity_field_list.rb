# == Schema Information
#
# Table name: pe_question_activity_field_lists
#
#  id               :integer          not null, primary key
#  pe_evaluation_id :integer
#  name             :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PeQuestionActivityFieldList < ActiveRecord::Base

  belongs_to :pe_evaluation
  has_many :pe_question_activity_field_list_items

  attr_accessible :name, :pe_evaluation_id

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_evaluation_id }
  validates :pe_evaluation_id, presence: true

  default_scope order('name ASC')

end
