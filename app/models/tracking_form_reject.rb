# == Schema Information
#
# Table name: tracking_form_rejects
#
#  id                    :integer          not null, primary key
#  tracking_form_log_id  :integer
#  comment               :text
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class TrackingFormReject < ActiveRecord::Base
  belongs_to :tracking_form_log
  belongs_to :registered_by_user, :class_name => 'User'
  attr_accessible :comment, :registered_at, :registered_by_user_id, :tracking_form_log_id
end
