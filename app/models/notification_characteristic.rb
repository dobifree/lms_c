# == Schema Information
#
# Table name: notification_characteristics
#
#  id                :integer          not null, primary key
#  notification_id   :integer
#  characteristic_id :integer
#  match_value       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class NotificationCharacteristic < ActiveRecord::Base
  belongs_to :notification
  belongs_to :characteristic
  attr_accessible :match_value, :notification, :notification_id, :characteristic, :characteristic_id
end
