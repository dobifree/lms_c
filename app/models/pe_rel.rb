# == Schema Information
#
# Table name: pe_rels
#
#  id                                  :integer          not null, primary key
#  pe_process_id                       :integer
#  rel                                 :integer
#  alias                               :string(255)
#  alias_reverse                       :string(255)
#  alias_plural                        :string(255)
#  alias_reverse_plural                :string(255)
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  position                            :integer          default(0)
#  allows_shared_comments              :boolean          default(FALSE)
#  display_shared_comments_in_feedback :boolean          default(FALSE)
#  available_in_selection              :boolean          default(FALSE)
#  max_number_to_select                :integer          default(1)
#  max_num_times_to_be_selected        :integer          default(1)
#  available_in_assign                 :boolean          default(FALSE)
#  max_number_to_assign                :integer          default(1)
#  min_number_to_assign                :integer          default(1)
#  assign_total_count                  :boolean          default(FALSE)
#

class PeRel < ActiveRecord::Base

  belongs_to :pe_process

  attr_accessible :alias, :alias_plural, :alias_reverse, :alias_reverse_plural, :rel, :pe_process_id, :position,
                  :allows_shared_comments, :display_shared_comments_in_feedback,
                  :available_in_selection, :max_number_to_select, :max_num_times_to_be_selected,
                  :available_in_assign, :max_number_to_assign, :min_number_to_assign, :assign_total_count

  validates :position, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :rel, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, uniqueness: { scope: :pe_process_id }
  validates :pe_process_id, presence: true
  validates :alias, presence: true
  validates :alias_reverse, presence: true
  validates :alias_plural, presence: true
  validates :alias_reverse_plural, presence: true

  default_scope order('position, rel')

  def rel_id
    self.rel
  end

  def self.rels
    #nunca mover
    [:auto, :jefe, :par, :sub, :cli, :prov]
    #  0       1     2     3     4      5
  end

  def self.rel_id(rel)
    PeRel.rels.index(rel.to_sym)
  end

  def rel_name
    PeRel.rels[self.rel]
  end

end
