# == Schema Information
#
# Table name: dnc_processes
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  period     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DncProcess < ActiveRecord::Base

  attr_accessible :name, :period

  has_many :dnc_requirements
  has_many :dnc_process_periods

  validates :name, presence: true

end
