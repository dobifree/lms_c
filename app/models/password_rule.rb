# == Schema Information
#
# Table name: password_rules
#
#  id                  :integer          not null, primary key
#  password_policy_id  :integer
#  group_name          :string(255)
#  priority            :integer
#  name                :string(255)
#  description         :text
#  regular_expression  :string(255)
#  mandatory           :boolean
#  positive_match      :boolean
#  error_match_message :string(255)
#  strongness          :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class PasswordRule < ActiveRecord::Base
  belongs_to :password_policy
  attr_accessible :description, :error_match_message, :group_name, :mandatory,
                  :name, :positive_match, :priority, :regular_expression,
                  :strongness
end
