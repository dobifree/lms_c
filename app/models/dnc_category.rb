# == Schema Information
#
# Table name: dnc_categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DncCategory < ActiveRecord::Base

  attr_accessible :name

  validates :name, presence: true

end
