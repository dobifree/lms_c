# == Schema Information
#
# Table name: brg_groups
#
#  id                     :integer          not null, primary key
#  group_cod              :string(255)
#  name                   :string(255)
#  description            :text
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  brg_process_id         :integer
#

class BrgGroup < ActiveRecord::Base
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :brg_process
  has_many :brg_group_users
  has_many :brg_manage_relations

  attr_accessible :group_cod,
                  :brg_process_id,
                  :name,
                  :description,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id

  validates :name, presence: true
  validates :group_cod, presence: true, uniqueness:  {scope: :brg_process_id}
  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  default_scope order(:group_cod)
  scope :active, -> { where(active: true)}
  scope :group_cod, -> (cod) { where(group_cod: cod)}

  def self.this_process(proc_id); where(brg_process_id: proc_id) end
  def full_name
    name+' ('+group_cod+')'
  end

  def bonus_sum_by_this_manager(user_id)
    manager_amount = 0
    brg_group_users.active.each do |gu|
      gu.brg_bonuses.active.each do |bonus|
        next unless bonus.my_turn_manager?(user_id)
        manager_amount += bonus.actual_value
      end
    end
    manager_amount
  end

  def active_relations; brg_group_users.active end
  def self.by_process(this_id); where(brg_process_id: this_id) end
  def editable?; active end
  def full_group_name; group_cod+' - '+name end

  def full_registered_by_user(show_cod)
    return nil unless registered_by_user
    full_user_gen(registered_by_user, show_cod)
  end

  def full_deactivated_by_user(show_cod)
    return nil unless deactivated_by_user
    full_user_gen(deactivated_by_user, show_cod)
  end

  def full_user_gen(user, show_cod)
    full_user =  user.apellidos.to_s+', '+user.nombre.to_s
    full_user = user.codigo.to_s+' '+full_user if show_cod
    full_user
  end

  def self.active_bonuses(groups)
  BrgBonus.joins(brg_group_user: :brg_group).where(brg_group_users: {active: true}, paid: [true, nil], active: true, brg_groups: { id: groups.collect(&:id)}).all
  end

  def self.groups_managed_by(managers, process)
    groups_managed = []
    groups = BrgGroup.active.this_process(process.id)
    groups.each do |group|
      managers.each do |manager|
        next unless group.managed_by?(manager.user_id)
        groups_managed << group
        break
      end
    end
    groups_managed
  end

  def active_managers
    relations = BrgManageRelation.where(group_to_manage_id: id, active: true)
    managers = []
    relations.each {|rel| managers << rel.manager if rel.manager && rel.manager.active}
    managers
  end

  def managed_by?(user_id)
    manager = BrgManager.where(active: true, user_id: user_id).first
    return nil unless manager
    active_managers.each do |manager|
      return true if manager.user_id == user_id.to_i
    end
    false
  end

  def self.last_id
    obj = reorder('created_at desc').first
    if obj
      obj.id
    else
      0
    end
  end
end
