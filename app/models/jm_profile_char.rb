# == Schema Information
#
# Table name: jm_profile_chars
#
#  id                    :integer          not null, primary key
#  jm_profile_form_id    :integer
#  jm_characteristic_id  :integer
#  position              :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  jm_profile_section_id :integer
#  mandatory             :boolean          default(FALSE)
#

class JmProfileChar < ActiveRecord::Base
  belongs_to :jm_profile_form
  belongs_to :jm_characteristic
  belongs_to :jm_profile_section
  has_many :jm_profile_char_to_reports, :dependent => :destroy
  accepts_nested_attributes_for :jm_profile_char_to_reports, :allow_destroy => true

  attr_accessible :position, :jm_characteristic_id, :jm_profile_section_id, :mandatory, :jm_profile_char_to_reports_attributes

  default_scope order('position ASC')

  validates :position, :numericality => true
  validates :jm_characteristic_id, :presence => true
end
