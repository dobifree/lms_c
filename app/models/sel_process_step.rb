# == Schema Information
#
# Table name: sel_process_steps
#
#  id             :integer          not null, primary key
#  sel_process_id :integer
#  sel_step_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class SelProcessStep < ActiveRecord::Base
  belongs_to :sel_process
  belongs_to :sel_step
  has_many :sel_step_candidates, :dependent => :destroy
  has_many :sel_process_attendants, :dependent => :destroy
  has_many :sel_appointments, :dependent => :destroy
  has_many :sel_process_characteristics, :dependent => :destroy
  has_many :sel_step_results, :through => :sel_process_characteristics

  accepts_nested_attributes_for :sel_appointments, :allow_destroy => true
  accepts_nested_attributes_for :sel_process_characteristics, :allow_destroy => true

  attr_accessible :sel_process_id, :sel_process, :sel_step_id, :sel_step,
                  :sel_step_candidates,
                  :sel_appointments, :sel_appointments_attributes,
                  :sel_process_characteristics, :sel_process_characteristics_attributes
                  :sel_process_attendants

  def sel_step_candidates_ordered_by_name
    self.sel_step_candidates.joins(:jm_candidate).reorder('surname, name')
  end

  def sel_step_candidates_ordered_by_name_not_excluded
    self.sel_step_candidates.joins(:jm_candidate => :sel_applicants).where(:sel_applicants => {sel_process_id: self.sel_process_id, excluded: false}).reorder('surname, name')
  end

end
