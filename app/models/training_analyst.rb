# == Schema Information
#
# Table name: training_analysts
#
#  id                               :integer          not null, primary key
#  planning_process_company_unit_id :integer
#  user_id                          :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#

class TrainingAnalyst < ActiveRecord::Base

  belongs_to :planning_process_company_unit
  belongs_to :user
  has_many :company_unit, through: :planning_process_company_unit

  validates :planning_process_company_unit_id, presence: true
  validates :user_id, presence: true, uniqueness: {scope: :planning_process_company_unit_id}

  attr_accessible :user_id

  default_scope joins(:company_unit).order('company_units.nombre ASC')

end
