# == Schema Information
#
# Table name: hr_evaluation_types
#
#  id            :integer          not null, primary key
#  nombre        :string(255)
#  activo        :boolean          default(TRUE)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  descripcion   :text
#  explanation   :text
#  from_jobs     :boolean
#  from_jobs_com :boolean
#

class HrEvaluationType < ActiveRecord::Base

  has_many :hr_evaluation_type_elements

  attr_accessible :activo, :nombre, :descripcion, :explanation, :from_jobs, :from_jobs_com

  validates :nombre, presence: true

  default_scope order('nombre ASC')

end
