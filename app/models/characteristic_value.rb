# == Schema Information
#
# Table name: characteristic_values
#
#  id                :integer          not null, primary key
#  characteristic_id :integer
#  value_string      :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  position          :integer          default(1)
#  active            :boolean          default(TRUE)
#  value_string_2    :string(255)
#

class CharacteristicValue < ActiveRecord::Base

  belongs_to :characteristic

  has_many :characteristic_value_parents

  attr_accessible :value_string, :position, :active, :value_string_2

  validates :value_string, presence: true, uniqueness: { scope: :characteristic_id, case_sensitive: false }
  validates :position, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('active DESC, position, value_string')

end
