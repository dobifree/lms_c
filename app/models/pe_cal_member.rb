# == Schema Information
#
# Table name: pe_cal_members
#
#  id                    :integer          not null, primary key
#  pe_cal_session_id     :integer
#  pe_process_id         :integer
#  pe_member_id          :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  step_calibration_date :datetime
#

class PeCalMember < ActiveRecord::Base

  belongs_to :pe_cal_session
  belongs_to :pe_process
  belongs_to :pe_member

  attr_accessible :pe_member_id, :step_calibration_date

  validates :pe_member_id, uniqueness: { scope: :pe_process_id }

  before_destroy :reverse_calibration

  def reverse_calibration

    pe_member = self.pe_member

    pe_member.reverse_calibration

    pe_member.save

  end

  def has_been_calibrated
    if pe_cal_session.pe_evaluation
      self.pe_member.pe_assessment_final_evaluations.where('pe_evaluation_id = ? and original_percentage is not null', self.pe_cal_session.pe_evaluation_id).count == 1 ? true : false
    else
      self.pe_member.step_calibration
    end
  end

end
