# == Schema Information
#
# Table name: bp_events
#
#  id                      :integer          not null, primary key
#  name                    :string(255)
#  description             :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  since                   :datetime
#  until                   :datetime
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#  event_date_name         :string(255)      default("Fecha de evento")
#  position                :integer          default(1)
#  available               :boolean          default(TRUE)
#  unavailable_at          :datetime
#  unavailable_by_admin_id :integer
#

class BpEvent < ActiveRecord::Base
  has_many :bp_event_files
  has_many :bp_licenses
  has_many :bp_forms

  belongs_to :unavailable_by_admin, class_name: 'Admin', foreign_key: :unavailable_by_admin_id
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id

  attr_accessible :name,
                  :description,
                  :since,
                  :until,
                  :position,
                  :event_date_name

  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :name, presence: true
  validates :since, presence: true
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  validate :since_before_until


  def deactivated_by_admin_formatted
    return deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  def active?(now)
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  def self.available; where(available: true) end

  def active_form
    bp_forms.where(active: true).first
  end

  private

  def since_before_until
    return unless self.until && since && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end

end
