# == Schema Information
#
# Table name: scholarship_list_items
#
#  id                  :integer          not null, primary key
#  scholarship_list_id :integer
#  name                :string(255)
#  description         :text
#  active              :boolean
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  position            :integer
#

class ScholarshipListItem < ActiveRecord::Base
  belongs_to :scholarship_list
  attr_accessible :active, :description, :name, :position
  validates :position, presence: true, numericality: true
  validates :name, presence: true

  default_scope order('position ASC')

end
