# == Schema Information
#
# Table name: hr_process_evaluation_q_schedules
#
#  id                            :integer          not null, primary key
#  hr_process_evaluation_id      :integer
#  hr_evaluation_type_element_id :integer
#  inicio                        :date
#  fin                           :date
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#

class HrProcessEvaluationQSchedule < ActiveRecord::Base

  belongs_to :hr_process_evaluation
  belongs_to :hr_evaluation_type_element

  attr_accessible :fin, :inicio, :hr_process_evaluation_id, :hr_evaluation_type_element_id

  validates :inicio, presence: true
  validates :fin, presence: true

  default_scope order('inicio ASC, fin ASC')

end
