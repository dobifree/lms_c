# == Schema Information
#
# Table name: hr_process_assessment_ds
#
#  id                        :integer          not null, primary key
#  hr_process_dimension_id   :integer
#  hr_process_user_id        :integer
#  resultado_porcentaje      :float
#  fecha                     :datetime
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  finalizada                :boolean          default(FALSE)
#  hr_process_dimension_g_id :integer
#

class HrProcessAssessmentD < ActiveRecord::Base

  belongs_to :hr_process_dimension
  belongs_to :hr_process_dimension_g
  belongs_to :hr_process_user

  has_one :hr_process_assessment_d_cal

  attr_accessible :fecha, :resultado_porcentaje, :finalizada

end
