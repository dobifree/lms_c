# == Schema Information
#
# Table name: j_job_ccs
#
#  id               :integer          not null, primary key
#  j_job_id         :integer
#  j_cost_center_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class JJobCc < ActiveRecord::Base
  belongs_to :j_job
  belongs_to :j_cost_center

  attr_accessible :j_cost_center_id

  validates :j_cost_center_id, uniqueness: { scope: :j_job_id }


  #
  #
end
