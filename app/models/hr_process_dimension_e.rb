# == Schema Information
#
# Table name: hr_process_dimension_es
#
#  id                      :integer          not null, primary key
#  orden                   :integer
#  porcentaje              :integer
#  hr_evaluation_type_id   :integer
#  hr_process_dimension_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  eval_jefe               :boolean          default(FALSE)
#  eval_auto               :boolean          default(FALSE)
#  eval_par                :boolean          default(FALSE)
#  eval_sub                :boolean          default(FALSE)
#  eval_jefe_peso          :integer          default(0)
#  eval_auto_peso          :integer          default(0)
#  eval_par_peso           :integer          default(0)
#  eval_sub_peso           :integer          default(0)
#  eval_cli                :boolean          default(FALSE)
#  eval_prov               :boolean          default(FALSE)
#  eval_cli_peso           :integer          default(0)
#  eval_prov_peso          :integer          default(0)
#  registro_por_gestor     :boolean          default(FALSE)
#

class HrProcessDimensionE < ActiveRecord::Base

  belongs_to :hr_evaluation_type
  belongs_to :hr_process_dimension

  has_many :hr_process_evaluations

  attr_accessible :orden, :porcentaje, :hr_evaluation_type_id, :hr_process_dimension_id,
                  :eval_jefe, :eval_auto, :eval_par, :eval_sub, :eval_cli, :eval_prov,
                  :eval_jefe_peso, :eval_auto_peso, :eval_par_peso,
                  :eval_sub_peso, :eval_cli_peso, :eval_prov_peso, :registro_por_gestor

  validates :hr_evaluation_type_id, presence: true, uniqueness: {scope: :hr_process_dimension_id}
  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :porcentaje, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0, less_than_or_equal_to: 100 }
  validates :eval_jefe_peso, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0}
  validates :eval_auto_peso, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0}
  validates :eval_par_peso, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0}
  validates :eval_sub_peso, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0}
  validates :eval_cli_peso, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0}
  validates :eval_prov_peso, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0}

  default_scope order('orden ASC')

end
