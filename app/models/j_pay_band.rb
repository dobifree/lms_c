# == Schema Information
#
# Table name: j_pay_bands
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  min_value  :decimal(15, 4)
#  max_value  :decimal(15, 4)
#  active     :boolean          default(TRUE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  q1         :decimal(15, 4)
#  q2         :decimal(15, 4)
#  q3         :decimal(15, 4)
#

class JPayBand < ActiveRecord::Base

  has_many :j_jobs

  attr_accessible :active, :max_value, :max_value, :min_value, :min_value, :name, :q1, :q2, :q3

  validates :name, presence: true, uniqueness: { case_sensitive: false}
  validates :min_value, presence: true, numericality: {greater_than_or_equal_to: 0 }
  validates :q1, presence: true, numericality: {:greater_than_or_equal_to => :min_value}
  validates :q2, presence: true, numericality: {:greater_than_or_equal_to => :q1}
  validates :q3, presence: true, numericality: {:greater_than_or_equal_to => :q2}
  validates :max_value, presence: true, numericality: {:greater_than_or_equal_to => :q3}

  default_scope order('active DESC, name')

  def self.actives
    self.where('active = ?', true)
  end

end
