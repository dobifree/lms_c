# == Schema Information
#
# Table name: pe_groups
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  pe_evaluation_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PeGroup < ActiveRecord::Base

  belongs_to :pe_evaluation

  has_many :pe_member_groups, dependent: :destroy #

  attr_accessible :name, :pe_evaluation_id

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_evaluation_id }
  validates :pe_evaluation_id, presence: true

  default_scope order('name')

end
