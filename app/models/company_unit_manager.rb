# == Schema Information
#
# Table name: company_unit_managers
#
#  id                               :integer          not null, primary key
#  planning_process_company_unit_id :integer
#  user_id                          :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#

class CompanyUnitManager  < ActiveRecord::Base

  belongs_to :planning_process_company_unit
  belongs_to :user

  validates :planning_process_company_unit_id, presence: true
  validates :user_id, presence: true, uniqueness: {scope: :planning_process_company_unit_id}


  attr_accessible :user_id

end
