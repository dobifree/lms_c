# == Schema Information
#
# Table name: vac_accumulateds
#
#  id                              :integer          not null, primary key
#  user_id                         :integer
#  bp_general_rules_vacation_id    :integer
#  accumulated_days                :decimal(25, 20)
#  accumulated_real_days           :decimal(25, 20)
#  accumulated_progressive_days    :integer
#  active                          :boolean          default(TRUE)
#  deactivated_at                  :datetime
#  deactivated_by_user_id          :integer
#  registered_at                   :datetime
#  registered_by_user_id           :integer
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  deactivated_description         :text
#  registered_manually             :boolean
#  registered_manually_description :text
#  vac_request_id                  :integer
#  vac_record_id                   :integer
#  up_to_date                      :date
#  next_vac_accumulated_id         :integer
#  prev_vac_accumulated_id         :integer
#  auto_update_legal               :boolean          default(FALSE)
#  auto_update_prog                :boolean          default(FALSE)
#

class VacAccumulated < ActiveRecord::Base
  belongs_to :user
  belongs_to :bp_general_rules_vacation
  belongs_to :vac_request
  has_one :vac_record

  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :user_id,
                  :vac_request_id,
                  :vac_record_id,
                  :bp_general_rules_vacation_id,
                  :accumulated_days,
                  :accumulated_progressive_days,
                  :accumulated_real_days,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :deactivated_description,
                  :registered_at,
                  :registered_by_user_id

  validates :user_id, presence: true
  validates :accumulated_days, presence: true
  validates :accumulated_progressive_days, presence: true
  validates :accumulated_real_days, presence: true


  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_description, presence: true, unless: :active?

  # validates :registered_manually, inclusion: { in: [true, false] }
  # validates :registered_manually_description, presence: true, if: :registered_manually?
  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def available_days_of_vacations(general_rules_id, accumulated_vacs_id, year)
    rules = BpGeneralRulesVacation.find(general_rules_id)
    accumulated = VacAccumulated.find(accumulated_vacs_id)

  end

  def self.active; where(active: true) end

  def self.users
    User.where(activo: true).joins(:vac_accumulateds).where(vac_accumulateds: {active: true}).order('apellidos ASC')
  end

  def self.user_first(user_id)
    VacAccumulated.where(active: true, user_id: user_id).order('up_to_date DESC, registered_at DESC, id DESC').last
  end

  def self.user_latest(user_id)
    VacAccumulated.where(active: true, user_id: user_id).order('up_to_date DESC, registered_at DESC, id DESC').first
  end

  def self.user_latest_prog(user_id)
    VacAccumulated.where(active: true, user_id: user_id, auto_update_prog: true).order('up_to_date DESC, registered_at DESC, id DESC').first
  end

  def self.user_latest_legal(user_id)
    VacAccumulated.where(active: true, user_id: user_id, auto_update_legal: true).order('up_to_date DESC, registered_at DESC, id DESC').first
  end

  def self.since(specified_date, user_id)
    accums_to_return = []
    accums = VacAccumulated.where(active: true, user_id: user_id).order('up_to_date ASC')
    accums.each do |accum|
      next unless specified_date.to_date >= accum.up_to_date
      accums_to_return << accum
    end
    accums_to_return
  end

end
