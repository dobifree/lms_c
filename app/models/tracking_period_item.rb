# == Schema Information
#
# Table name: tracking_period_items
#
#  id                      :integer          not null, primary key
#  tracking_period_list_id :integer
#  position                :integer
#  name                    :string(255)
#  from_date               :datetime
#  to_date                 :datetime
#  active                  :boolean
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class TrackingPeriodItem < ActiveRecord::Base
  belongs_to :tracking_period_list
  attr_accessible :active, :from_date, :name, :position, :to_date

  validates :position, :numericality => true
  validates :name, :presence => true
  validates :from_date, :presence => true
  validates :to_date, :presence => true

  default_scope { order(:position, :name) }

end
