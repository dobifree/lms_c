# == Schema Information
#
# Table name: tracking_form_notifications
#
#  id                      :integer          not null, primary key
#  action_type             :integer
#  tracking_form_action_id :integer
#  name                    :string(255)
#  description             :text
#  active                  :boolean
#  to                      :string(255)
#  cc                      :string(255)
#  subject                 :string(255)
#  body                    :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class TrackingFormNotification < ActiveRecord::Base
  belongs_to :tracking_form_action
  attr_accessible :action_type, :active, :body, :cc, :description, :name, :subject, :to

  validates :name, :presence => true
  validates :action_type, :presence => true
  validates :to, :presence => true
  validates :subject, :presence => true
  validates :body, :presence => true

end
