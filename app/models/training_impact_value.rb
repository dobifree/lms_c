# == Schema Information
#
# Table name: training_impact_values
#
#  id         :integer          not null, primary key
#  valor      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  nombre     :string(255)
#

class TrainingImpactValue < ActiveRecord::Base

  has_many :dnc_training_impacts
  has_many :dnc_training_impact_ps

  attr_accessible :nombre, :valor

  validates :nombre, presence: true
  validates :valor, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('valor ASC')


end
