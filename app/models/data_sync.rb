# == Schema Information
#
# Table name: data_syncs
#
#  id          :integer          not null, primary key
#  description :string(255)
#  executed_at :datetime
#  completed   :boolean
#  sync_errors :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class DataSync < ActiveRecord::Base
  attr_accessible :completed, :description, :executed_at, :sync_errors
end
