# == Schema Information
#
# Table name: pe_process_tutorials
#
#  id            :integer          not null, primary key
#  pe_process_id :integer
#  tutorial_id   :integer
#  name          :string(255)
#  active        :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  position      :integer
#

class PeProcessTutorial < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :tutorial
  attr_accessible :active, :name, :tutorial_id, :pe_process_id, :position

  default_scope order('position ASC, name ASC')

end
