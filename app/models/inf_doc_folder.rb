# == Schema Information
#
# Table name: inf_doc_folders
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  position    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class InfDocFolder < ActiveRecord::Base
  has_many :informative_documents, dependent: :nullify
  attr_accessible :description, :name, :position

  validates :name, uniqueness: true

  default_scope order('position ASC, name ASC')

end
