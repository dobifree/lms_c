# == Schema Information
#
# Table name: sel_flow_approvers
#
#  id                    :integer          not null, primary key
#  sel_vacant_flow_id    :integer
#  position              :integer
#  name                  :string(255)
#  description           :text
#  mandatory             :boolean
#  weight                :float
#  conditions_rejection  :boolean
#  prerequisite_position :integer
#  approver_type         :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class SelFlowApprover < ActiveRecord::Base
  belongs_to :sel_vacant_flow
  has_one :sel_flow_specific_approver, :dependent => :destroy
  has_many :sel_flow_approver_chars, :dependent => :destroy
  accepts_nested_attributes_for :sel_flow_specific_approver
  accepts_nested_attributes_for :sel_flow_approver_chars, :allow_destroy => true
  attr_accessible :approver_type, :conditions_rejection, :description, :mandatory, :name, :position, :prerequisite_position, :weight,
                  :sel_flow_specific_approver_attributes,
                  :sel_flow_approver_chars_attributes


  before_validation :reset_orphan_children

  validates :position, :numericality => true
  validates :name, :presence => true
  validates :weight, :numericality => {:greater_than => 0}

  default_scope { order(:position, :name) }

  def self.options_approver_type
    ['Usuario específico', 'Jefe directo', 'Características']
  end

  def option_approver_type
    SelFlowApprover.options_approver_type[self.approver_type]
  end

  def reset_orphan_children
    case self.approver_type
      when 0
        self.sel_flow_approver_chars.destroy_all
      when 1
        self.sel_flow_specific_approver.destroy
        self.sel_flow_approver_chars.destroy_all
      when 2
        self.sel_flow_specific_approver.destroy
    end
  end

end
