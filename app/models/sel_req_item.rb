# == Schema Information
#
# Table name: sel_req_items
#
#  id                  :integer          not null, primary key
#  sel_req_template_id :integer
#  name                :string(255)
#  description         :text
#  item_type           :integer
#  mandatory           :boolean          default(FALSE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  position            :integer
#  select2_list        :boolean          default(FALSE)
#  show_on_table       :boolean          default(FALSE)
#  active              :boolean          default(TRUE)
#  characteristic_id   :integer
#

class SelReqItem < ActiveRecord::Base
  belongs_to :sel_req_template
  belongs_to :characteristic
  has_many :sel_req_options, dependent: :destroy
  has_many :sel_requirement_values, dependent: :destroy
  accepts_nested_attributes_for :sel_req_options, allow_destroy: true
  attr_accessible :description, :item_type, :mandatory, :name, :position,
                  :sel_req_template, :sel_req_template_id, :select2_list,
                  :show_on_table, :active, :characteristic_id,
                  :sel_req_options, :sel_req_options_attributes

  default_scope order('position')

  before_save :reset_select2_list
  before_save :reset_characteristic_id

  def self.alternative_item_type
    [:texto, :numero, :fecha, :seleccion, :archivo, :texto_largo, :usuario_aprobador, :caracteristica_ficha]
  end

  private

  def reset_select2_list
    unless self.item_type == 3
      self.select2_list = false
    end
    return true
  end

  def reset_characteristic_id
    unless self.item_type == 7
      self.characteristic_id = nil
    end
    return true
  end

end
