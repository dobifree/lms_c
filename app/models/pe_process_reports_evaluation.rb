# == Schema Information
#
# Table name: pe_process_reports_evaluations
#
#  id                                    :integer          not null, primary key
#  pe_process_id                         :integer
#  pe_evaluation_id                      :integer
#  definition_status                     :boolean          default(FALSE)
#  created_at                            :datetime         not null
#  updated_at                            :datetime         not null
#  rep_detailed_final_results_evaluation :boolean          default(FALSE)
#  detailed_definition                   :boolean          default(FALSE)
#  members_configuration                 :boolean
#  tracking_status                       :boolean          default(FALSE)
#  detailed_definition_i                 :boolean          default(FALSE)
#

class PeProcessReportsEvaluation < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_evaluation

  attr_accessible :definition_status, :rep_detailed_final_results_evaluation, :detailed_definition,
                  :members_configuration, :tracking_status, :detailed_definition_i

end
