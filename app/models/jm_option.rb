# == Schema Information
#
# Table name: jm_options
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  position    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  jm_list_id  :integer
#

class JmOption < ActiveRecord::Base
  belongs_to :jm_list
  has_many :jm_answer_values, :foreign_key => :value_option_id
  attr_accessible :description, :name, :position, :jm_list

  validates :name, :presence => true
  validates :position, :presence => true

  default_scope order('position ASC')
end
