# == Schema Information
#
# Table name: jcc_characteristics
#
#  id                      :integer          not null, primary key
#  j_cost_center_id        :integer
#  characteristic_id       :integer
#  value_string            :string(255)
#  characteristic_value_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class JccCharacteristic < ActiveRecord::Base
  belongs_to :j_cost_center
  belongs_to :characteristic
  belongs_to :characteristic_value
  attr_accessible :value_string

  def value
    if self.characteristic.data_type_id == 0
      self.value_string
    elsif self.characteristic.data_type_id == 5
      self.characteristic_value ? self.characteristic_value.id : nil
    end
  end

  def formatted_value
    if self.characteristic.data_type_id == 0
      self.value_string
    elsif self.characteristic.data_type_id == 5
      self.characteristic_value ? self.characteristic_value.value_string : nil
    end

  end


end
