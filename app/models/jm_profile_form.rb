# == Schema Information
#
# Table name: jm_profile_forms
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  layout      :text
#  active      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class JmProfileForm < ActiveRecord::Base

  has_many :jm_profile_chars, :dependent => :destroy
  has_many :jm_profile_sections, :through => :jm_profile_chars
  accepts_nested_attributes_for :jm_profile_chars, :allow_destroy => true

  attr_accessible :active, :description, :layout, :name, :jm_profile_chars_attributes
end
