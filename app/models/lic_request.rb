# == Schema Information
#
# Table name: lic_requests
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  bp_license_id          :integer
#  event_date             :date
#  begin                  :datetime
#  end                    :datetime
#  days                   :integer
#  hours                  :integer
#  paid                   :boolean
#  paid_at                :datetime
#  paid_by_user_id        :integer
#  status                 :integer
#  status_description     :text
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  prev_lic_request_id    :integer
#  next_lic_request_id    :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  bp_form_id             :integer
#  go_back_date           :date
#  lic_event_id           :integer
#  money                  :decimal(12, 4)
#  paid_description       :text
#

class LicRequest < ActiveRecord::Base
  belongs_to :lic_event
  belongs_to :bp_license
  belongs_to :user

  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :paid_by_user, class_name: 'User', foreign_key: :paid_by_user_id
  belongs_to :prev_lic_request, class_name: 'LicRequest', foreign_key: :prev_lic_request_id
  belongs_to :next_lic_request, class_name: 'LicRequest', foreign_key: :next_lic_request_id

  attr_accessible :begin,
                  :end,
                  :go_back_date,
                  :days,
                  :hours,
                  :paid,
                  :paid_at,
                  :paid_by_user_id,
                  :paid_description,
                  :status,
                  :status_description,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :prev_lic_request_id,
                  :next_lic_request_id,
                  :registered_at,
                  :registered_by_user_id,
                  :lic_event_id,
                  :bp_license_id,
                  :user_id

  validates :paid, inclusion: { in: [true, false, nil] }
  validates :paid_at, presence: true, if: :paid
  validates :paid_by_user_id, presence: true, if: :paid

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_user_id, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validate :verify_license_available
  validate :at_least_one_approver

  before_save :set_approved_if_auto_approval

  def self.approver_visible_1(user_id)
    approver = LicApprover.where(active: true, user_id: user_id).first
    return [] unless approver
    approvees_user_id = approver.active_approvees_user_id
    where(user_id: approvees_user_id)
  end

  def self.this_status(status)
    where(status: status)
  end

  def self.index_pending
    LicRequest.where(active: true, status: [VacRequest.pending, VacRequest.approved]).order('begin asc').all
  end

  def self.taken_by(user_id, bp_licenses_id)
    return nil unless bp_licenses_id && bp_licenses_id.size > 0
    LicRequest.where(active: true,
                     paid: [nil, true],
                     status: LicRequest.in_process_status).joins(:lic_event).where(lic_events: {active: true, user_id: user_id}, bp_license_id: bp_licenses_id).all
  end

  def self.all_taken_by(user_id, bp_licenses_id)
    return nil unless bp_licenses_id && bp_licenses_id.size > 0
    LicRequest.where(active: true,
                     paid: [nil, true],
                     status: LicRequest.register_visible_status).joins(:lic_event).where(lic_events: {active: true, user_id: user_id}, bp_license_id: bp_licenses_id).all
  end

  def full_user
    return nil unless lic_event
    return nil unless lic_event.user
    user = lic_event.user
    user.codigo.to_s + ' ' + user.apellidos.to_s + ', ' + user.nombre.to_s
  end

  def full_registered_by_user
    return nil unless registered_by_user
    registered_by_user.codigo.to_s + ' ' + registered_by_user.apellidos.to_s + ', ' + registered_by_user.nombre.to_s
  end

  def self.historical_info(array, request_id)
    request = LicRequest.find(request_id)
    array << request
    if request.prev_lic_request
      LicRequest.historical_info(array, request.prev_lic_request_id)
    end
    array.sort_by(&:registered_at).reverse
  end

  def self.manager_editable_non_partial
    requests = []
    events = LicEvent.manager_editable_non_partial
    events.each { |event| requests << event.lic_requests.where(active: true).all }
    requests
  end

  def self.manager_money_requests
    requests = []
    all_requests = LicRequest.where(active: true, status: [LicRequest.approved, LicRequest.closed], paid: nil).all
    all_requests.each { |request| requests << request if request.money && request.money > 0 }
    requests
  end

  def significant_history
    requests = self.history([])
    requests.each do |request|

    end
  end

  def history(prev_history)
    prev_history << self
    if prev_lic_request_id
      prev_lic_request.history(prev_history)
    else
      prev_history
    end
  end

  def self.to_approve_by(user_id)
    approver = LicApprover.where(active: true, user_id: user_id).first
    return LicRequest.where(active: -1)  unless approver

    users = approver.users_to_approve

    return LicRequest.where(active: -1)  unless users.size > 0

    users_id = []
    users.each { |user| users_id << user.id }

   LicRequest.where(active: true, status: [LicRequest.pending, LicRequest.approved] ).joins(:lic_event).where(lic_events: {user_id: users_id}).all
  end

  def self.to_approve_by_rel(user_id)
    approver = LicApprover.where(active: true, user_id: user_id).first
    return LicRequest.where(active: -1) unless approver

    users = approver.users_to_approve

    return LicRequest.where(active: -1) unless users.size > 0

    users_id = []
    users.each { |user| users_id << user.id }

    LicRequest.where(active: true, status: [LicRequest.pending, LicRequest.approved] ).joins(:lic_event).where(lic_events: {user_id: users_id})
  end

  def self.manager_editable_partial
    requests = []
    events = LicEvent.manager_editable_partial
    events.each { |event| requests << event.lic_requests.where(active: true).all }
    requests
  end

  def self.register_editable_non_partial(user_id)
    requests = []
    events = LicEvent.register_editable_non_partial(user_id)
    events.each { |event| requests += event.lic_requests.where(active: true).all }
    requests
  end

  def self.register_editable_partial(user_id)
    requests = []
    events = LicEvent.register_editable_partial(user_id)
    events.each { |event| requests += event.lic_requests.where(active: true).all }
    requests
  end

  def show_to_manager
    return false unless status
    approved? || closed?
  end

  def show_to_register
    return false unless status
    draft? || pending? || approved? || closed? || rejected?
  end

  def show_to_approver
    return false unless status
    pending? || approved? || closed?
  end

  def available_manager_to_edit
    return false unless status
    approved?
  end

  def available_register_to_edit
    return false unless status
    draft? || rejected?
  end

  def available_approver_to_edit
    return false unless status
    pending?
  end

  def status_require_description?
    return false unless status
    draft? || rejected?
  end

  def paid_require_description?
    return true if paid == false
    false
  end

  def paid_colour
    case paid
      when nil ; 0
      when true ; 2
      when false ; 5
      else ; nil
    end
  end

  def paid_message
    if money && money > 0
      case paid
        when nil ; 'Pendiente de pago'
        when true ; 'Pagado'
        when false ; 'No pagado'
        else ; nil
      end
    else
      'Sin beneficio monetario'
    end
  end

  def self.active
    where(active: true)
  end

  def payable?
    return false unless approved? || closed?
    return false unless money
    return false unless paid.nil?
    lic_event.regularizable_items(true, true).size == 0
  end

  # STATUS GUIDE
  # 0: Borrador
  # 1: Pendiente
  # 2: Aprobado
  # 3: Cerrado
  # 4: Rechazado

  def status_colour
    case status
      when 0 ; 0
      when 1 ; 0
      when 2 ; 2
      when 3 ; 5
      when 4 ; 5
      else ; nil
    end
  end

  def status_message
    case status
      when 0 ; 'Borrador'
      when 1 ; 'Pendiente'
      when 2 ; 'Aprobado'
      when 3 ; 'Cerrado'
      when 4 ; 'Rechazado'
      else ; nil
    end
  end

  def draft?
    return nil unless status
    status == LicRequest.draft
  end

  def pending?
    return nil unless status
    status == LicRequest.pending
  end

  def approved?
    return nil unless status
    status == LicRequest.approved
  end

  def closed?
    return nil unless status
    status == LicRequest.closed
  end

  def rejected?
    return nil unless status
    status == LicRequest.rejected
  end

  def on_going?(now)
    return nil unless status
    return false unless status == 2
    now > self.begin && now < self.end
  end

  def past?(now)
    return nil unless status
    return false unless status == 2
    now > self.end
  end

  def only_approved?(now)
    return nil unless status
    !(on_going?(now) || past?(now))
  end

  def set_draft; self.status = 0; end
  def set_pending; self.status = 1; end
  def set_approved; self.status = 2; end
  def set_closed; self.status = 3; end
  def set_rejected; self.status = 4; end

  def self.draft; 0; end
  def self.pending; 1; end
  def self.approved; 2; end
  def self.closed ;3; end
  def self.rejected ;4; end

  def visible_by_register?
    return nil unless status
    return false unless active
    LicRequest.register_visible_status.include?(status)
  end

  def visible_by_approver?(user_id)
    return nil unless status
    return false unless active
    return false unless LicRequest.register_visible_status.include?(status)
    approver = LicApprover.where(active: true, user_id: user_id).first
    return [] unless approver
    approvees_user_id = approver.active_approvees_user_id
    approvees_user_id.include?(self.user_id)
  end

  def visible_by_manager?
    return nil unless status
    return false unless active
    LicRequest.manager_visible_status.include?(status)
  end

  def editable_by_manager?
    return unless self.visible_by_manager?
    LicRequest.manager_editable_status.include?(status)
  end

  def editable_by_approver?(user_id)
    return unless self.visible_by_approver?(user_id)
    LicRequest.approver_editable_status.include?(status)
  end

  def editable_by_register?
    return unless self.visible_by_register?
    LicRequest.register_editable_status.include?(status)
  end

  def self.register_visible(user_id)
    LicRequest.where(user_id: user_id, active: true, status: LicRequest.register_visible_status)
  end

  def self.register_editable(user_id)
    LicRequest.where(user_id: user_id, active: true, status: LicRequest.register_editable_status)
  end

  def self.approver_visible(user_id)
    approver = LicApprover.where(active: true, user_id: user_id).first
    return LicRequest.where(active: -1) unless approver
    approvees_user_id = approver.active_approvees_user_id
    LicRequest.where(user_id: approvees_user_id, active: true, status: LicRequest.approver_visible_status)
  end

  def self.approver_editable(user_id)
    approver = LicApprover.where(active: true, user_id: user_id).first
    return LicRequest.where(active: -1) unless approver
    approvees_user_id = approver.active_approvees_user_id
    LicRequest.where(user_id: approvees_user_id, active: true, status: LicRequest.approver_visible_status)
  end

  def self.manager_visible
    LicRequest.where(active: true, status: LicRequest.manager_visible_status)
  end

  def self.manager_editable
    LicRequest.where(active: true, status: LicRequest.manager_editable_status)
  end

  def self.register_visible_status
    [LicRequest.draft, LicRequest.pending, LicRequest.approved, LicRequest.rejected, LicRequest.closed]
  end

  def in_process?
    return nil unless status
    LicRequest.in_process_status.include?(status)
  end

  def self.in_process_status
    [LicRequest.pending, LicRequest.approved, LicRequest.closed]
  end

  def self.prev_process_status
    [LicRequest.draft, LicRequest.rejected]
  end

  def self.approver_visible_status
    [LicRequest.pending, LicRequest.approved, LicRequest.closed]
  end

  def self.manager_visible_status
    [LicRequest.approved, LicRequest.closed]
  end

  def self.register_editable_status
    [LicRequest.draft, LicRequest.rejected]
  end

  def self.approver_editable_status
    [LicRequest.pending]
  end

  def self.manager_editable_status
    [LicRequest.approved, LicRequest.closed]
  end

  private

  def at_least_one_approver
    return unless active
    return unless user_id
    return unless self.pending?
    return unless LicApprover.user_approvers(user_id).empty?
    errors.add(:base, 'Debe tener por lo menos un aprobador activo asignado')
  end

  def verify_license_available
  end

  def set_approved_if_auto_approval
    return unless bp_license_id
    return if bp_license.need_approval
    return unless self.pending?
    self.status = LicRequest.approved
    self.status_description = '' if self.status_description.nil?
    self.status_description += '  *No necesita aprobación'
  end

end
