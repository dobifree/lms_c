# == Schema Information
#
# Table name: pe_feedback_field_list_items
#
#  id                             :integer          not null, primary key
#  description                    :string(255)
#  pe_feedback_field_list_id      :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  position                       :integer
#  pe_feedback_field_list_item_id :integer
#  list_item_selected             :boolean          default(TRUE)
#

class PeFeedbackFieldListItem < ActiveRecord::Base

  belongs_to :pe_feedback_field_list
  belongs_to :pe_feedback_field_list_item

  has_many :pe_feedback_field_list_items

  has_many :pe_member_feedbacks, foreign_key: 'list_item_id'
  has_many :pe_member_compound_feedbacks, foreign_key: 'list_item_id'

  has_many :pe_feedback_fields
  has_many :pe_feedback_compound_fields

  attr_accessible :description, :pe_feedback_field_list_id, :position, :pe_feedback_field_list_item_id, :list_item_selected

  validates :description, presence: true, uniqueness: { case_sensitive: false, scope: :pe_feedback_field_list_id }

  default_scope order('position, description')



end
