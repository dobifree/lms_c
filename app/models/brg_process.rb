# == Schema Information
#
# Table name: brg_processes
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  description            :text
#  period_begin           :datetime
#  period_end             :datetime
#  due_date               :datetime
#  status                 :integer          default(1)
#  status_description     :text
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  pe_process_id          :integer
#  quali_evaluation_id    :integer
#  quanti_evaluation_id   :integer
#  quali_percentage       :float
#  quanti_percentage      :float
#  reference_year         :integer
#  alias                  :string(255)      default("Bono Rol General")
#

class BrgProcess < ActiveRecord::Base
  belongs_to :quali_evaluation, class_name: 'PeEvaluation', foreign_key: :quali_evaluation_id
  belongs_to :quanti_evaluation, class_name: 'PeEvaluation', foreign_key: :quanti_evaluation_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :pe_process
  has_many :brg_bonuses
  has_many :brg_groups
  has_many :brg_percentage_conversions
  has_many :brg_managers


  has_many :brg_lvl_percentages, :dependent => :destroy
  has_many :brg_char_percentages, :dependent => :destroy

  accepts_nested_attributes_for :brg_lvl_percentages, :allow_destroy => true
  accepts_nested_attributes_for :brg_char_percentages, :allow_destroy => true

  attr_accessible :alias,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :description,
                  :due_date,
                  :name,
                  :reference_year,
                  :period_begin,
                  :period_end,
                  :registered_at,
                  :registered_by_user_id,
                  :status,
                  :status_description,
                  :pe_process_id,
                  :quali_evaluation_id,
                  :quanti_evaluation_id,
                  :quali_percentage,
                  :quanti_percentage,
                  :brg_lvl_percentages_attributes,
                  :brg_char_percentages_attributes

  validates :quali_evaluation_id, presence: true
  validates :quanti_evaluation_id, presence: true
  validates :quali_percentage, presence: true
  validates :quanti_percentage, presence: true

  validates :pe_process_id, presence: true
  validates :name, presence: true
  validates :due_date, presence: true
  validates :reference_year, presence: true, numericality: { only_integer: true }
  validates :period_begin, presence: true
  validates :period_end, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def gimme_2017_evals
    PeEvaluation.where(id: [27,28,29,30,31])
  end

  def gimme_2018_evals
    PeEvaluation.where(id: [56,57,58,59,60])
  end

  def self.all_private
  where('alias like "%privado%"')
  end

  def self.not_private
    where('alias not like "%privado%"')
  end

  def private?
    self.alias.downcase.include?('privado')
  end

  def last_year_process
    BrgProcess.active.from_year(reference_year - 1).first
  end

  def amount_bonus_pending
    amount = 0
    brg_bonuses.active.each do |bon|
      next if bon.next_managers.size > 0
      amount += 1
    end
    amount
  end

  def amount_bonus_total
    brg_bonuses.active.size
  end

  def groups_id
    array = []
    brg_groups.active.each { |group| array << group.id }
    array
  end

  def editable_bonus_by_group_sum(manager_user_id)
    #Devuelve un array que contiene tantos [group_name+group_cod - amount] como brg_groups hay dentro del proceso.
    array = []
    brg_groups.active.each do |group|
      group_response = sum_registered_by(manager_user_id, [true, false], [true, false], group.id)
      group_response += sum_pen_wio_regist_by(manager_user_id, group.id)
      next unless group_response > 0
      array << [group.id, group.full_name, group_response]
    end
    array
  end

  def editable_bonus_by_company_sum(manager_user_id)
    #Devuelve un array que contiene tantos [group_name+group_cod - amount] como brg_groups hay dentro del proceso.
    array = []
    company = Characteristic.get_characteristic_company
    # User.characteristic_value(company)
    company_values = company.characteristic_values
    return [] unless company_values
    company_values.each do |company_val|
      company_response = sum_registered_by_company(manager_user_id, [true, false], [true, false], company_val.id)
      company_response += sum_pen_wio_regist_by_company(manager_user_id, company_val.id)
      next unless company_response > 0
      array << [company_val.id, company_val.value_string, company_response]
    end
    array
  end


  def editable_bonus_by_nivel_1(my_user_id, bonuses, groups, managers)
    #Devuelve un array que contiene tantos [group_name+group_cod - amount] como brg_groups hay dentro del proceso.
    array = []
    company = Characteristic.where(id: 64).first
    company_values = company ? company.characteristic_values : nil
    users_1 = User.where(activo: true, id: bonuses.collect(&:user_id))
    return [] unless company_values
    company_values.each do |company_val|
      users = User.fulfill_this_value_char(company_val.id, company, users_1)
      company_response = sum_registered_by_company_1(bonuses, my_user_id, [true, false], [true, false], users)
      company_response += sum_pen_wio_regist_company_1(bonuses, my_user_id, users)
      next unless company_response > 0

      com_resp_1 = sum_first_version_company_1(bonuses, my_user_id, users)
      com_resp_1 += sum_regis_by_arr_company_1(bonuses, my_user_id, users, [true, false], [true, false])

      array << [company_val.id, company_val.value_string, com_resp_1, company_response]
    end
    array
  end


  def editable_bonus_by_company_1(my_user_id, bonuses, groups, managers)
    #Devuelve un array que contiene tantos [group_name+group_cod - amount] como brg_groups hay dentro del proceso.
    array = []
    company = Characteristic.get_characteristic_company
    company_values = company.characteristic_values
    users_1 = User.where(activo: true, id: bonuses.collect(&:user_id))
    return [] unless company_values
    company_values.each do |company_val|
      users = User.fulfill_this_value_char(company_val.id, company, users_1)
      company_response = sum_registered_by_company_1(bonuses, my_user_id, [true, false], [true, false], users)
      company_response += sum_pen_wio_regist_company_1(bonuses, my_user_id, users)
      next unless company_response > 0

      com_resp_1 = sum_first_version_company_1(bonuses, my_user_id, users)
      com_resp_1 += sum_regis_by_arr_company_1(bonuses, my_user_id, users, [true, false], [true, false])

      array << [company_val.id, company_val.value_string, com_resp_1, company_response]
    end
    array
  end

  def sum_regis_by_arr_company_1(bonuses, this_user_id, users, ready_to_go, active_status)
    user_ids = users.collect(&:id)
    sum1 = 0
    bonuses.each do |bonus|
      next unless user_ids.include?(bonus.user_id)
      new_bonus = bonus.last_version_regis_by(this_user_id)
      next unless new_bonus
      next unless ready_to_go.include?(new_bonus.ready_to_go)
      next unless active_status.include?(new_bonus.active)
      sum1 += bonus.brg_process.private? ? bonus.bonus_target : BrgBonus.historical_info([], bonus.id).first.actual_value
    end
    sum1
  end

  def sum_first_version_company_1(bonuses, this_user_id, users)
    sum1 = 0
    user_ids = users.collect(&:id)
    bonuses.each do |bonus|
      next unless user_ids.include?(bonus.user_id)
      next unless bonus.my_turn_manager?(this_user_id)
      next if bonus.registered_by_user_id.to_i == this_user_id.to_i
      sum1 += bonus.brg_process.private? ? bonus.bonus_target : BrgBonus.historical_info([], bonus.id).first.actual_value
    end
    sum1
  end


  def sum_pen_wio_regist_company_1(bonuses, this_user_id, users)
    user_ids = users.collect(&:id)
    sum = 0
    bonuses.each do |bonus|
      next unless user_ids.include?(bonus.user_id)
      next unless bonus.my_turn_manager?(this_user_id)
      next if bonus.registered_by_user_id.to_i == this_user_id.to_i
      sum += bonus.actual_value
    end
    sum
  end

  def sum_registered_by_company_1(bonuses, this_user_id, ready_to_go, active_status, users)
    sum = 0
    user_ids = users.collect(&:id)
    bonuses.each do |bonus|
      next unless user_ids.include?(bonus.user_id)
      new_bonus = bonus.last_version_regis_by(this_user_id)
      next unless new_bonus
      next unless ready_to_go.include?(new_bonus.ready_to_go)
      next unless active_status.include?(new_bonus.active)
      sum += bonus.actual_value
    end
    sum
  end

  def registered_by(this_user_id, ready_to_go, active_status, groups_id)
    bonuses = BrgBonus.process(id).active.payable.this_group(groups_id)
    new_bonuses = []
    bonuses.each do |bonus|
      new_bonus = bonus.last_version_regis_by(this_user_id)
      next unless new_bonus
      next unless ready_to_go.include?(new_bonus.ready_to_go)
      next unless active_status.include?(new_bonus.active)
      new_bonuses << new_bonus
    end
    new_bonuses
  end

  def registered_by_company(this_user_id, ready_to_go, active_status, company_id)
    bonuses = BrgBonus.process(id).active.payable.this_company_id(company_id)
    new_bonuses = []
    bonuses.each do |bonus|
      new_bonus = bonus.last_version_regis_by(this_user_id)
      next unless new_bonus
      next unless ready_to_go.include?(new_bonus.ready_to_go)
      next unless active_status.include?(new_bonus.active)
      new_bonuses << new_bonus
    end
    return new_bonuses
  end

  def sum_registered_by(this_user_id, ready_to_go, active_status, groups_id)
    array = registered_by(this_user_id, ready_to_go, active_status, groups_id)
    sum = 0
    array.each { |bonus|sum += bonus.actual_value }
    sum
  end

  def sum_registered_by_company(this_user_id, ready_to_go, active_status, company_id)
    array = registered_by_company(this_user_id, ready_to_go, active_status, company_id)
    sum = 0
    array.each { |bonus|sum += bonus.actual_value }
    sum
  end

  def approved_by(this_user_id)
    bonuses = BrgBonus.process(id).regis_by(this_user_id)
    amount = 0
    bonuses.each { |bonus| amount += bonus.actual_value if bonus.ready_to_go }
    amount
  end

  def pending_wio_regis_by(this_user_id, groups_id)
    bonuses = BrgBonus.process(id).active.payable.this_group(groups_id)
    new_bonuses = []
    bonuses.each do |bonus|
      next unless bonus.my_turn_manager?(this_user_id)
      next if bonus.registered_by_user_id.to_i == this_user_id.to_i
      new_bonuses << bonus
    end
    new_bonuses
  end

  def pending_wio_regis_by_company(this_user_id, company_id)
    bonuses = BrgBonus.process(id).active.payable.this_company_id(company_id)
    new_bonuses = []
    bonuses.each do |bonus|
      next unless bonus.my_turn_manager?(this_user_id)
      next if bonus.registered_by_user_id.to_i == this_user_id.to_i
      new_bonuses << bonus
    end
    new_bonuses
  end

  def sum_pen_wio_regist_by(this_user_id, groups_id)
    array = pending_wio_regis_by(this_user_id, groups_id)
    sum = 0
    array.each { |bonus| sum += bonus.actual_value }
    sum
  end

  def sum_pen_wio_regist_by_company(this_user_id, company_id)
    array = pending_wio_regis_by_company(this_user_id, company_id)
    sum = 0
    array.each { |bonus| sum += bonus.actual_value }
    sum
  end

  def sum_first_version(this_user_id, groups_id)
    array = pending_wio_regis_by(this_user_id, groups_id)
    sum = 0
    sum1 = 0
    array.each do |bonus|
      sum += bonus.actual_value
      sum1 += bonus.brg_process.private? ? bonus.bonus_target : BrgBonus.historical_info([], bonus.id).first.actual_value
    end
    [sum,sum1]
  end

  def sum_first_version_company(this_user_id, groups_id)
    array = pending_wio_regis_by_company(this_user_id, groups_id)
    sum = 0
    sum1 = 0
    array.each do |bonus|
      sum += bonus.actual_value
      sum1 += bonus.brg_process.private? ? BrgBonus.historical_info([], bonus.id).first.first_value : BrgBonus.historical_info([], bonus.id).first.actual_value
    end
    [sum,sum1]
  end

  def sum_regis_by_arr(this_user_id, ready_to_go, active_status, groups_id)
    array = registered_by(this_user_id, ready_to_go, active_status, groups_id)
    sum = 0
    sum1 = 0
    array.each do |bonus|
      sum += bonus.actual_value
      sum1 += bonus.brg_process.private? ? bonus.bonus_target : BrgBonus.historical_info([], bonus.id).first.actual_value
    end
    [sum, sum1]
  end

  def sum_regis_by_arr_company(this_user_id, ready_to_go, active_status, company_id)
    array = registered_by_company(this_user_id, ready_to_go, active_status, company_id)
    sum = 0
    sum1 = 0
    array.each do |bonus|
      sum += bonus.actual_value
      sum1 += bonus.brg_process.private? ? BrgBonus.historical_info([], bonus.id).first.first_value : BrgBonus.historical_info([], bonus.id).first.actual_value
    end
    [sum, sum1]
  end

  def approved_by_user_group(this_user_id, group_id)
    bonuses = BrgBonus.process(id).regis_by(this_user_id).this_group(group_id)
    amount = 0
    bonuses.each { |bonus| amount += bonus.actual_value }
    amount
  end

  def sum_ind_percentages(user_id)
    quali = quali_result_for_user(user_id)
    quanti = quanti_result_for_user(user_id)
    return nil unless quali && quanti
    (quali*quali_percentage + quanti*quanti_percentage)
  end

  def quali_result_for_user(user_id)
    val = quali_percentage_for_user(user_id)
    return nil unless val
    val.after
  end

  def quali_percentage_for_user(user_id)
    user = User.find(user_id)
    pe_member = pe_process.pe_member_by_user(user)
    return nil unless pe_member
    eval = pe_member.pe_assessment_final_evaluation(quali_evaluation)
    return nil unless eval
    eval = (eval.percentage/100).to_d.round(2)
    perc_conversion = BrgPercentageConversion.active.by_individual.by_type(BrgPercentageConversion.quali_type)
    perc_conversion = by_smart_before(eval, perc_conversion)
    return nil unless perc_conversion
    perc_conversion
  end

  def gimme_final_value_rol_privado(user, raw, decimals, pe_process)
    dim_x = pe_process.dimension_x
    pe_member = pe_process.pe_member_by_user(user)
    return nil unless pe_member
    pe_assessment_dimension = pe_member.pe_assessment_dimension(dim_x) if pe_member.step_assessment?
    return pe_assessment_dimension.percentage if pe_assessment_dimension
    return nil
  end

  def gimme_eval_value_rp(user,evaluation, raw, decimals, pe_process)
    pe_member = pe_process.pe_member_by_user(user)
    return nil unless pe_member
    eval = pe_member.pe_assessment_final_evaluation(evaluation)
    return nil unless eval
    return eval.percentage/100 if raw
    (eval.percentage/100).to_d.round(decimals ? decimals : 2)
  end

  def gimme_eval_value(user,evaluation, raw, decimals)
    pe_member = pe_process.pe_member_by_user(user)
    return nil unless pe_member
    eval = pe_member.pe_assessment_final_evaluation(evaluation)
    return nil unless eval
    return eval.percentage/100 if raw
    (eval.percentage/100).to_d.round(decimals ? decimals : 2)
  end

  def quanti_result_for_user(user_id)
    val = quanti_percentage_for_user(user_id)
    return nil unless val
    val.after
  end

  def quanti_percentage_for_user(user_id)
    user = User.find(user_id)
    pe_member = pe_process.pe_member_by_user(user)
    return nil unless pe_member
    eval = pe_member.pe_assessment_final_evaluation(quanti_evaluation)
    return nil unless eval
    eval = (eval.percentage/100).to_d.round(2)
    perc_conversion = BrgPercentageConversion.active.by_individual.by_type(BrgPercentageConversion.quanti_type)
    perc_conversion = by_smart_before(eval, perc_conversion)
    return nil unless perc_conversion
    perc_conversion
  end

  def company_percentage_for_user(user_id)
    char_per = BrgCharPercentage.active.from_process(id).for_user(user_id)
    return nil unless char_per
    char_per = char_per.first
    return nil unless char_per
    char_per.percentage
  end

  def company_result_for(user_id)
    company_percentage = company_percentage_for_user(user_id)
    return nil unless company_percentage
    company_percentage = company_percentage.to_d.round(2)
    perc_conversion = BrgPercentageConversion.active.by_group.by_type(BrgPercentageConversion.quanti_type)
    perc_conversion = by_smart_before(company_percentage, perc_conversion)
    return nil unless perc_conversion
    perc_conversion.after
  end

  def by_smart_before(this_before, conversions)
    conversions.each {|pc| return pc if pc.before.to_d.round(2) == this_before.to_f.to_d.round(2) }
    nil
  end

  def lvl_percentage_for(user_id)
    lvl_percentage_user = BrgLvlPercentage.by_process(id).by_user(user_id).first
    return nil unless lvl_percentage_user
    lvl_percentage_user
  end

  def self.active ; where(active: true) end
  def self.from_year(this_year); where(reference_year: this_year) end
  def self.get_in_process; where(status: BrgProcess.in_process) end

  def brg_bonuses
    BrgBonus.process(id)
  end

  def users_related
    users = []
    brg_bonuses.each {|bonus| users << bonus.brg_group_user.user unless users.include?(bonus.brg_group_user.user) }
    users
  end

  # STATUS GUIDE
  # 0: En proceso
  # 0.5: Sin pendientes (1)
  # 2: Cerrado (only show)

  def status_colour
    case status
      when 0; pending_bonus? ? 2 : 4
      when 1; 5
      else ; nil
    end
  end

  def status_message
    case status
      when 0 ; pending_bonus? ? 'En proceso' : 'Sin pendientes'
      when 1 ; 'Cerrado'
      else ; nil
    end
  end

  def pending_bonus?
    brg_bonuses.active.each { |bonus| return true if bonus.pending_manager? }
    false
  end

  def no_pending_manager_bonuses
    bonuses = []
    brg_bonuses.payable.active.each {|bonus| bonuses << bonus unless bonus.pending_manager?}
    bonuses
  end

  def latest_report_partially_bonus
    bonuses = []
    report = BrgReport.only_partial.from_process(id).sort_by(&:registered_at).reverse.first
    if report
      report.brg_reported_bonuses.each {|r_bonus| bonuses << r_bonus.brg_bonus if (r_bonus.brg_bonus.paid || r_bonus.brg_bonus.paid.nil?) }
    else
      no_pending_manager_bonuses
    end
  end

  def pending_paid_bonus
    bonuses = []
    brg_bonuses.active.each { |bonus| bonuses << bonus if bonus.paid.nil? }
    bonuses
  end

  def self.in_process; 0; end
  def self.closed; 1; end

  def closed?
    return nil unless status
    status == BrgProcess.closed
  end

  def in_process?
    return nil unless status
    status == BrgProcess.in_process
  end

  def managers_chains
    managee_ids = BrgManageRelation.where('active = 1 and managee_id is not null').pluck(:managee_id)
    managee_ids.size > 0 ? brg_managers.where('active = 1 and id NOT IN(?)', managee_ids) : brg_managers.active
  end

  def not_assoc_managers
    managers_id = BrgManageRelation.active.pluck(:manager_id).to_a
    managers_id.size > 0 ? brg_managers.where('active = 1 and id NOT IN(?)', managers_id) : brg_managers.active
  end

end

