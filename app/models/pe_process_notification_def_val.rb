# == Schema Information
#
# Table name: pe_process_notification_def_vals
#
#  id                     :integer          not null, primary key
#  pe_process_id          :integer
#  before_accepted        :boolean          default(FALSE)
#  num_step               :integer
#  send_email_when_accept :boolean          default(FALSE)
#  send_email_when_reject :boolean          default(FALSE)
#  accepted_subject       :string(255)
#  accepted_body          :text
#  rejected_subject       :string(255)
#  rejected_body          :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class PeProcessNotificationDefVal < ActiveRecord::Base

  belongs_to :pe_process
  attr_accessible :accepted_body, :accepted_subject, :before_accepted, :num_step, :rejected_body, :rejected_subject, :send_email_when_accept, :send_email_when_reject

end
