# == Schema Information
#
# Table name: jm_profile_sections
#
#  id          :integer          not null, primary key
#  position    :integer
#  name        :string(255)
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class JmProfileSection < ActiveRecord::Base

  has_many :jm_profile_chars, :dependent => :nullify

  attr_accessible :description, :name, :position

  validates :position, :numericality => true
  validates :name, :presence => true

  default_scope order('position ASC')

end
