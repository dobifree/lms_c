# == Schema Information
#
# Table name: blog_form_permissions
#
#  id           :integer          not null, primary key
#  blog_form_id :integer
#  perm_type    :integer
#  employee     :boolean
#  boss         :boolean
#  superior     :boolean
#  others       :boolean
#  active       :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class BlogFormPermission < ActiveRecord::Base
  belongs_to :blog_form
  attr_accessible :active, :boss, :employee, :others, :perm_type, :superior

  def self.perm_type_options
    ['Lectura', 'Escritura'] # [0, 1]
  end
end
