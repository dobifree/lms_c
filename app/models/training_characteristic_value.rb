# == Schema Information
#
# Table name: training_characteristic_values
#
#  id                         :integer          not null, primary key
#  name                       :string(255)
#  position                   :integer
#  value_string               :string(255)
#  training_characteristic_id :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class TrainingCharacteristicValue < ActiveRecord::Base
  belongs_to :training_characteristic
  attr_accessible :name, :position, :value_string
end
