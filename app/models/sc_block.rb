# == Schema Information
#
# Table name: sc_blocks
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  description    :text
#  day            :integer
#  sc_schedule_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  begin_time     :time
#  end_time       :time
#  next_day       :boolean          default(FALSE)
#  registered_at  :datetime
#  active         :boolean          default(TRUE)
#  deactivated_at :datetime
#  workable       :boolean          default(TRUE)
#  free_time      :integer          default(0)
#

class ScBlock < ActiveRecord::Base
  has_many :sc_record_blocks
  has_many :sc_he_block_params

  belongs_to :sc_schedule

  accepts_nested_attributes_for :sc_he_block_params, :allow_destroy => true

  attr_accessible :name,
                  :description,
                  :begin_time,
                  :end_time,
                  :free_time,
                  :day,
                  :next_day,
                  :active,
                  :workable,
                  :sc_schedule_id,
                  :sc_he_block_params_attributes


  validates :name, :presence => true
  validates :begin_time, :presence => true
  validates :end_time, :presence => true
  validates :day, :presence => true


  validates :next_day, inclusion: { in: [true, false] }
  validates :active, inclusion: { in: [true, false] }
  validates :workable, inclusion: { in: [true, false] }

  validate :day_range
  validate :end_and_begin
  validate :free_time_in_range
  validate :other_blocks_conflict


  def other_blocks_conflict
    if self.begin_time && self.end_time && self.day >= 0 && self.day < 6
      should_save = true
      schedule_blocks = ScBlock.where(:sc_schedule_id => self.sc_schedule_id, :active => true)
      schedule_blocks.each do |block|
        next_day = block.next_day ? block.day+1 : block.day
        if block.begin_formatted <= self.end_formatted && block.day <= self.day
          if block.end_formatted >= self.begin_formatted && next_day >= self.day
            should_save = false if self.id && self.id != block.id
          end
        end
      end
      errors.add(:begin_time, 'o Fin, entran en conflicto con otro bloque') unless should_save
      errors.add(:end_time, 'o Inicio, entran en conflicto con otro bloque') unless should_save
    end
  end

  def day_range
    errors.add(:day, 'debe ser especificado') if self.day && (self.day < 0 || self.day > 6)
  end

  def end_and_begin
    if self.begin_time and self.end_time
      errors.add(:end_time, 'debe ser después de la hora de inicio') if !self.next_day and self.begin_time > self.end_time
      errors.add(:end_time, 'debe ser corresponder a un bloque menor de 24h') if self.next_day and self.begin_time < self.end_time
    end
  end

  def free_time_in_range
    if self.begin_time && self.end_time
    block_effective_time = self.end_time - self.begin_time - self.free_time
    block_effective_time += 24 if self.next_day
    errors.add(:free_time, 'debe ser menor al tiempo de trabajo comprendido entre inicio y fin de bloque') if block_effective_time < 0
    end
  end

  def day_formatted
    if self.day
    days = ['Domingo','Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']
    return days[self.day]
    end
  end

  def begin_formatted
    self.begin_time.strftime('%H:%M') if begin_time
  end

  def end_formatted
    self.end_time.strftime('%H:%M') if end_time
  end

  def different?(edited_block)
    if self.name == edited_block.name and
        self.description == edited_block.description and
        self.day == edited_block.day and
        self.begin_time == edited_block.begin_time and
        self.end_time == edited_block.end_time and
        self.next_day == edited_block.next_day and
        self.workable == edited_block.workable and
        self.free_time == edited_block.free_time
      return false
    else
      return true
    end
  end

end
