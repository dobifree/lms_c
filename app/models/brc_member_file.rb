# == Schema Information
#
# Table name: brc_member_files
#
#  id            :integer          not null, primary key
#  brc_member_id :integer
#  name          :string(255)
#  file_name     :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class BrcMemberFile < ActiveRecord::Base

  belongs_to :brc_member

  attr_accessible :file_name, :name, :brc_member_id

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :brc_member_id }

  default_scope order('name')

  def set_file_name(ext)

    self.file_name = self.brc_member.user.codigo+'_'+SecureRandom.hex(5)+'.'+ext

  end

end
