# == Schema Information
#
# Table name: pe_question_rank_models
#
#  id            :integer          not null, primary key
#  percentage    :float
#  goal          :decimal(30, 6)
#  discrete_goal :string(255)
#  pe_element_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  discrete      :boolean          default(FALSE)
#

class PeQuestionRankModel < ActiveRecord::Base

  belongs_to :pe_element

  attr_accessible :discrete_goal, :goal, :percentage, :discrete

  before_save { self.discrete_goal = nil if self.discrete_goal.blank?}

  default_scope order('percentage, goal')



end
