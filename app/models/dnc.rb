# == Schema Information
#
# Table name: dncs
#
#  id                               :integer          not null, primary key
#  planning_process_id              :integer
#  planning_process_company_unit_id :integer
#  company_unit_area_id             :integer
#  oportunidad_mejora               :text
#  tema_capacitacion                :string(255)
#  objetivos_curso                  :text
#  costo_directo                    :float
#  costo_indirecto                  :float
#  training_mode_id                 :integer
#  training_type_id                 :integer
#  meses                            :string(255)
#  tiempo_capacitacion              :integer
#  training_provider_id             :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  impacto_total                    :float
#  enviado_aprobar_area             :boolean          default(FALSE)
#  requiere_modificaciones_area     :boolean          default(FALSE)
#  aprobado_area                    :boolean          default(FALSE)
#  aprobado_unidad                  :boolean          default(FALSE)
#  observaciones                    :text
#  number_students                  :integer          default(0)
#  eval_eficacia                    :boolean
#  training_program_id              :integer
#

class Dnc < ActiveRecord::Base

  belongs_to :planning_process
  belongs_to :planning_process_company_unit
  belongs_to :company_unit_area
  belongs_to :training_mode
  belongs_to :training_type
  belongs_to :training_provider
  belongs_to :training_program

  has_many :dnc_planning_process_goals
  has_many :dnc_training_impacts
  has_many :dnc_students
  has_many :dnc_area_corrections
  has_one :dncp

  attr_accessible :aprobado_area, :aprobado_unidad, :costo_directo, :costo_indirecto, :enviado_aprobar_area, :impacto_total, :meses, :objetivos_curso, :oportunidad_mejora,
                  :tema_capacitacion, :tiempo_capacitacion,
                  :planning_process_id, :planning_process_company_unit_id, :company_unit_area_id,
                  :requiere_modificaciones_area, :training_mode_id, :training_type_id, :training_provider_id, :observaciones,
                  :number_students, :eval_eficacia, :training_program_id

  validates :planning_process_id, presence: true
  validates :planning_process_company_unit_id, presence: true
  validates :company_unit_area_id, presence: true
  validates :oportunidad_mejora, presence: true
  validates :tema_capacitacion, presence: true
  validates :objetivos_curso, presence: true

  validates :costo_directo, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :costo_indirecto, presence: true, numericality: { greater_than_or_equal_to: 0 }

  validates :training_mode_id, presence: true
  validates :training_type_id, presence: true

  validates :tiempo_capacitacion, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 30 }

  #validates :training_provider_id, presence: true

  default_scope order('impacto_total DESC')

end
