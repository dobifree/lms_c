# == Schema Information
#
# Table name: password_policies
#
#  id                :integer          not null, primary key
#  priority          :integer
#  name              :string(255)
#  use_in_exa        :boolean
#  use_in_job_market :boolean
#  default           :boolean
#  active            :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PasswordPolicy < ActiveRecord::Base
  has_many :password_rules, dependent: :destroy
  accepts_nested_attributes_for :password_rules, allow_destroy: true
  attr_accessible :name, :active, :default, :priority,
                  #:use_in_exa, :use_in_job_market,
                  :password_rules_attributes
end
