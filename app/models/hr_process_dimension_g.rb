# == Schema Information
#
# Table name: hr_process_dimension_gs
#
#  id                      :integer          not null, primary key
#  orden                   :integer
#  valor_minimo            :integer
#  valor_maximo            :integer
#  hr_process_dimension_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  nombre                  :string(255)
#  color_text              :string(255)
#  descripcion             :text
#  valor_minimo_signo      :string(255)      default(">=")
#  valor_maximo_signo      :string(255)      default("<=")
#

class HrProcessDimensionG < ActiveRecord::Base

  belongs_to :hr_process_dimension
  #has_many :hr_process_quadrant_gs

  before_save { self.descripcion = nil if self.descripcion.blank?}

  attr_accessible :orden, :nombre, :valor_maximo, :valor_minimo, :hr_process_dimension_id,
                  :color_text, :descripcion, :valor_minimo_signo, :valor_maximo_signo

  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :nombre, presence: true
  validates :valor_minimo, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0, less_than_or_equal_to: 200 }
  validates :valor_maximo, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 200 }

  default_scope order('orden ASC')

end
