# == Schema Information
#
# Table name: pe_feedback_compound_fields
#
#  id                             :integer          not null, primary key
#  pe_feedback_field_id           :integer
#  position                       :integer
#  name                           :string(255)
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  field_type                     :integer          default(0)
#  pe_feedback_field_list_id      :integer
#  required                       :boolean          default(TRUE)
#  pe_feedback_field_list_item_id :integer
#  list_item_selected             :boolean          default(TRUE)
#

class PeFeedbackCompoundField < ActiveRecord::Base

  belongs_to :pe_feedback_field_list_item

  belongs_to :pe_feedback_field
  belongs_to :pe_feedback_field_list

  has_many :pe_member_compound_feedbacks

  attr_accessible :name, :position, :field_type, :pe_feedback_field_list_id, :required,
                  :pe_feedback_field_list_item_id, :list_item_selected

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_feedback_field_id }

  default_scope order('position, name')

  def field_type_name
    PeFeedbackField.field_types[self.field_type]
  end

end
