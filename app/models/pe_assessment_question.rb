# == Schema Information
#
# Table name: pe_assessment_questions
#
#  id                             :integer          not null, primary key
#  pe_assessment_evaluation_id    :integer
#  pe_assessment_question_id      :integer
#  points                         :decimal(30, 6)
#  percentage                     :float
#  comment                        :text
#  pe_alternative_id              :integer
#  indicator_achievement          :decimal(30, 6)
#  last_update                    :datetime
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  pe_question_id                 :integer
#  indicator_discrete_achievement :string(255)
#  comment_2                      :text
#

class PeAssessmentQuestion < ActiveRecord::Base

  belongs_to :pe_assessment_evaluation
  belongs_to :pe_alternative
  belongs_to :pe_assessment_question
  belongs_to :pe_question

  has_many :pe_assessment_questions, dependent: :destroy

  attr_accessible :comment, :indicator_achievement, :last_update, :pe_assessment_question_id, :percentage, :points,
                  :indicator_discrete_achievement, :comment_2

  before_save { self.points = nil if self.points.blank? }
  before_save { self.percentage = nil if self.percentage.blank? }
  before_save { self.comment = nil if self.comment.blank? }
  before_save { self.pe_alternative_id = nil if self.pe_alternative_id.blank? }
  before_save { self.indicator_achievement = nil if self.indicator_achievement.blank? }

end
