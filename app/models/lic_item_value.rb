# == Schema Information
#
# Table name: lic_item_values
#
#  id                     :integer          not null, primary key
#  bp_item_id             :integer
#  value                  :text
#  value_string           :string(255)
#  value_text             :text
#  value_int              :integer
#  value_float            :float
#  value_decimal          :decimal(12, 4)
#  value_datetime         :datetime
#  value_time             :time
#  value_option_id        :integer
#  value_file             :text
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  lic_request_id         :integer
#  value_date             :date
#  prev_lic_item_value_id :integer
#  next_lic_item_value_id :integer
#  lic_event_id           :integer
#

class LicItemValue < ActiveRecord::Base
  belongs_to :lic_request
  belongs_to :lic_event

  belongs_to :bp_item
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :lic_event_id,
                  :bp_item_id,
                  :value,
                  :value_string,
                  :value_text,
                  :value_date,
                  :value_int,
                  :value_float,
                  :value_option_id,
                  :value_file,
                  :value_decimal,
                  :value_datetime,
                  :value_time,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id,
                  :prev_lic_item_value_id,
                  :next_lic_item_value_id

  # validates :lic_request_id, presence: true
  validates :bp_item_id, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_user_id, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validate :value_item
  before_save :set_value

  private

  def value_item
    return unless bp_item_id
    errors.add(:value_string, bp_item.name + ' no puede ser vacío') if bp_item.item_string? && (bp_item.mandatory && !bp_item.regularizable) && !value_string
    errors.add(:value_text, bp_item.name + ' no puede ser vacío') if bp_item.item_text? && (bp_item.mandatory && !bp_item.regularizable) && !value_text
    errors.add(:value_date, bp_item.name + ' no puede ser vacío') if bp_item.item_date? && (bp_item.mandatory && !bp_item.regularizable) && !value_date
    errors.add(:value_int, bp_item.name + ' no puede ser vacío') if bp_item.item_int? && (bp_item.mandatory && !bp_item.regularizable) && !value_int
    errors.add(:value_float, bp_item.name + ' no puede ser vacío') if bp_item.item_float? && (bp_item.mandatory && !bp_item.regularizable) && !value_float
    errors.add(:value_option_id, bp_item.name + ' no puede ser vacío') if bp_item.item_list? && (bp_item.mandatory && !bp_item.regularizable) && !value_option_id
    errors.add(:value_file, bp_item.name + ' no puede ser vacío') if bp_item.item_file? && (bp_item.mandatory && !bp_item.regularizable) && !value_file
    errors.add(:value_decimal, bp_item.name + ' no puede ser vacío') if bp_item.item_decimal? && (bp_item.mandatory && !bp_item.regularizable) && !value_decimal
    errors.add(:value_datetime, bp_item.name + ' no puede ser vacío') if bp_item.item_datetime? && (bp_item.mandatory && !bp_item.regularizable) && !value_datetime
    errors.add(:value_time, bp_item.name + ' no puede ser vacío') if bp_item.item_time? && (bp_item.mandatory && !bp_item.regularizable) && !value_time
  end

  def set_value
    return unless bp_item_id
    self.value = value_string if bp_item.item_string?
    self.value = value_text if bp_item.item_text?
    self.value = value_date.strftime('%d/%m/%Y') if bp_item.item_date?
    self.value = value_int if bp_item.item_int?
    self.value = value_float if bp_item.item_float?
    self.value = bp_item.bp_item_options.where(id: value_option_id).first.name if bp_item.item_list?
    self.value = value_decimal if bp_item.item_decimal?
    self.value = value_datetime.strftime('%d/%m/%Y %H:%M') if bp_item.item_datetime?
    self.value = value_time.strftime('%H:%M') if bp_item.item_time?
  end
end
