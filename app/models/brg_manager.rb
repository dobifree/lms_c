# == Schema Information
#
# Table name: brg_managers
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  name                   :string(255)
#  description            :text
#  level                  :integer
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  brg_process_id         :integer
#

class BrgManager < ActiveRecord::Base
  belongs_to :user
  belongs_to :brg_process
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  has_many :brg_manage_relations, foreign_key: :manager_id

  attr_accessible :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :description,
                  :name,
                  :user_id,
                  :brg_process_id,
                  :registered_at,
                  :registered_by_user_id

  validates :user_id, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validate :manager_user


  scope :active, -> { where(active: true)}
  def self.by_user(this_id); where(user_id: this_id) end

  def active_relations
    BrgManageRelation.active.where(manager_id: id)
  end

  def active_managees
    managees = []
    relations = BrgManageRelation.where(active: true, manager_id: id)
    relations.each { |assoc| managees << assoc.managee if assoc.managee && assoc.managee.active }
    managees
  end

  def active_groups_to_manage
    managees = []
    relations = BrgManageRelation.where(active: true, manager_id: id)
    relations.each { |assoc| managees << assoc.group_to_manage if assoc.group_to_manage && assoc.group_to_manage.active }
    managees
  end

  def self.managers_of_group(group_id)
    relations = BrgManageRelation.active.where(group_to_manage_id: group_id)
    managers = []
    relations.each { |rel| managers << rel.manager if rel.manager.active }
    managers
  end

  def self.by_process(this_id); where(brg_process_id: this_id) end

  def self.managers_of_manager(user_id, process_id)
    managers = []
    manager = BrgManager.active.where(user_id: user_id, brg_process_id: process_id).first
    return [] unless manager
    relations = BrgManageRelation.active.where(managee_id: manager.id)
    relations.each { |rel| managers << rel.manager if rel.manager.active }
    managers
  end

  def editable?
    active
  end

  def full_registered_by_user(show_cod)
    return nil unless registered_by_user
    full_user_gen(registered_by_user, show_cod)
  end

  def full_deactivated_by_user(show_cod)
    return nil unless deactivated_by_user
    full_user_gen(deactivated_by_user, show_cod)
  end

  def full_user(show_cod)
    full_user_gen(user, show_cod)
  end

  def full_user_gen(user, show_cod)
    full_user =  user.apellidos.to_s+', '+user.nombre.to_s
    full_user = user.codigo.to_s+' '+full_user if show_cod
    full_user
  end

  def self.exists_as_active_manager?(user_id)
    !active.where(user_id: user_id).first.nil?
  end

  private

  def manager_user
    return if id
    return unless BrgManager.active.where(user_id: user_id, brg_process_id: brg_process_id).first
    errors.add(:user_id, 'ya se encuentra activo como responsable en este proceso')
  end

end
