# == Schema Information
#
# Table name: hr_evaluation_type_elements
#
#  id                          :integer          not null, primary key
#  nombre                      :string(255)
#  nivel                       :integer
#  valor_minimo                :float
#  valor_maximo                :float
#  formula_promedio            :boolean          default(TRUE)
#  peso_minimo                 :integer
#  peso_maximo                 :integer
#  suma_total_pesos            :integer
#  plan_accion                 :boolean          default(FALSE)
#  porcentaje_logro            :boolean          default(FALSE)
#  indicador_logro             :boolean          default(FALSE)
#  hr_evaluation_type_id       :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  cantidad_maxima             :integer
#  carga_manual                :boolean
#  evaluacion_colaborativa     :boolean
#  tiene_clasificacion         :boolean
#  nombre_clasificacion        :string(255)
#  aplica_evaluacion           :boolean          default(TRUE)
#  indicador_logro_descripcion :boolean          default(TRUE)
#

class HrEvaluationTypeElement < ActiveRecord::Base

  belongs_to :hr_evaluation_type

  attr_accessible :nivel, :nombre,
                  :peso_maximo, :peso_minimo, :suma_total_pesos,
                  :plan_accion, :indicador_logro, :porcentaje_logro,
                  :valor_maximo, :valor_minimo, :formula_promedio,
                  :hr_evaluation_type_id, :cantidad_maxima,
                  :carga_manual, :evaluacion_colaborativa,
                  :tiene_clasificacion, :nombre_clasificacion, :aplica_evaluacion,
                  :indicador_logro_descripcion

  validates :nombre, presence: true, uniqueness: { case_sensitive: false, scope: :hr_evaluation_type_id }
  validates :nivel, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :valor_minimo, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :valor_maximo, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :peso_minimo, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :peso_maximo, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :suma_total_pesos, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :cantidad_maxima, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  default_scope order('nivel ASC')

end
