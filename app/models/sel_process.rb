# == Schema Information
#
# Table name: sel_processes
#
#  id                    :integer          not null, primary key
#  sel_template_id       :integer
#  name                  :string(255)
#  description           :text
#  qty_required          :integer
#  source                :integer
#  apply_available       :boolean
#  from_date             :date
#  to_date               :date
#  finished_at           :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  file                  :string(255)
#  claimant_area         :string(255)
#  code                  :string(255)
#  sel_requirement_id    :integer
#  registered_at         :datetime
#  registered_by_user_id :integer
#  finished_by_user_id   :integer
#  finished              :boolean          default(FALSE)
#  traceable             :boolean          default(TRUE)
#

class SelProcess < ActiveRecord::Base
  belongs_to :sel_template
  has_many :sel_owners, :dependent => :destroy
  has_many :sel_applicants, :dependent => :destroy
  has_many :jm_candidates, through: :sel_applicants
  has_many :sel_process_steps, :dependent => :destroy
  has_many :sel_step_candidates, through: :sel_process_steps
  has_many :sel_claimants, :dependent => :destroy
  has_many :sel_process_attendants, through: :sel_process_steps
  has_many :sel_apply_specific_questions, :dependent => :destroy
  belongs_to :sel_requirement
  belongs_to :registered_by_user, class_name: 'User'
  belongs_to :finished_by_user, class_name: 'User'
  accepts_nested_attributes_for :sel_owners, :allow_destroy => true
  accepts_nested_attributes_for :sel_claimants, :allow_destroy => true
  accepts_nested_attributes_for :sel_apply_specific_questions, :allow_destroy => true
  attr_accessible :apply_available, :description, :finished, :finished_at, :finished_by_user_id, :registered_at, :registered_by_user_id, :from_date, :name, :qty_required, :source, :to_date, :claimant_area, :file, :code,
                  :sel_template, :sel_template_id,
                  :sel_owners, :sel_owners_attributes,
                  :sel_applicants,
                  :sel_process_steps,
                  :sel_step_candidates,
                  :sel_claimants,
                  :sel_claimants_attributes,
                  :sel_apply_specific_questions_attributes,
                  :sel_requirement, :sel_requirement_id,
                  :sel_process_attendants,
                  :traceable

  validates :code, presence: true
  validates :code, uniqueness: true
  validates :name, presence: true
  validates :qty_required, presence: true, :numericality => {:greater_than_or_equal_to => 0}
  validates :source, presence: true
  validates :claimant_area, presence: true
  #validates_associated :sel_owners


  scope :active, where(:finished => false)
  scope :in_time, ->(lms_time) { where('? between from_date AND to_date', "#{lms_time}%") }
  scope :applicable, where(:apply_available => true)
  scope :internal, where(:source => [0, 2])
  scope :external, where(:source => [1, 2])
  scope :owner, -> (user_id) { joins(:sel_owners).where('sel_owners.user_id = ?', user_id) }
  scope :claimant, -> (user_id) { joins(:sel_claimants).where('sel_claimants.user_id = ?', user_id) }
  scope :tracker, -> (user_id) { find_by_sql('select distinct sel_processes.*
                                              from sel_processes inner join sel_claimants
                                              on (sel_processes.id = sel_claimants.sel_process_id)
                                              where sel_claimants.user_id = ' + user_id.to_s + '
                                              and sel_processes.finished = 0
                                              and sel_processes.traceable = 1
                                              union
                                              select distinct sel_processes.*
                                              from sel_processes inner join sel_requirements
                                              on (sel_processes.sel_requirement_id = sel_requirements.id)
                                              where sel_requirements.registered_by_user_id = ' + user_id.to_s + '
                                              and sel_processes.finished = 0
                                              and sel_processes.traceable = 1') }


  def self.alternatives_source
    [:interna, :externa, :mixta]
  end

  def self.alternatives_source_style
    [:success, :primary, :warning]
  end

  def accept_internal_candidates?
    [0, 2].include?(self.source)
  end

  def accept_external_candidates?
    [1, 2].include?(self.source)
  end

  def sel_applicants_ordered_by_name
    self.sel_applicants.joins(:jm_candidate).reorder('surname, name')
  end

  def sel_applicants_ordered_by_priority
    self.sel_applicants.joins(:jm_candidate).where(excluded: false).reorder('selected DESC, CASE WHEN priority IS NULL THEN 1 ELSE 0 END, priority ASC, surname ASC, name ASC')
  end

  def sel_selected_applicants_ordered_by_priority
    self.sel_applicants.joins(:jm_candidate).where(selected: true, excluded: false).reorder('selected DESC, CASE WHEN priority IS NULL THEN 1 ELSE 0 END, priority ASC, surname ASC, name ASC')
  end

  def sel_process_steps_non_filter
    self.sel_process_steps.joins(:sel_step).where(sel_steps: {:step_type => 0})
  end

  def accept_external_invitations?(lms_time)
    !self.finished? &&
        self.apply_available &&
        self.from_date.to_datetime <= lms_time && self.to_date.to_datetime >= lms_time &&
        accept_external_candidates?

  end

end
