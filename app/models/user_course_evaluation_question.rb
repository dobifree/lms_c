# == Schema Information
#
# Table name: user_course_evaluation_questions
#
#  id                            :integer          not null, primary key
#  numero                        :integer
#  user_course_evaluation_id     :integer
#  master_evaluation_id          :integer
#  master_evaluation_question_id :integer
#  respondida                    :boolean
#  correcta                      :boolean
#  alternativas                  :string(255)
#  fecha_respuesta               :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  fecha_inicio                  :datetime
#

class UserCourseEvaluationQuestion < ActiveRecord::Base

  belongs_to :user_course_evaluation
  belongs_to :master_evaluation
  belongs_to :master_evaluation_question

  attr_accessible :alternativas, :correcta, :fecha_inicio, :fecha_respuesta, :respondida

  validates :numero, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validate :valid_master_evaluation
  validate :valid_master_evaluation_question

  default_scope order('numero ASC')

  def valid_master_evaluation
    unless self.user_course_evaluation.evaluation.master_evaluation.id == self.master_evaluation_id
      errors.add(:invalid_master_evaluation, 'El pool de preguntas no corresponde a la evaluacion')
    end
  end

  def valid_master_evaluation_question
    unless self.master_evaluation.master_evaluation_questions.include? self.master_evaluation_question
      errors.add(:invalid_master_evaluation_question, 'La pregunta no corresponde al pool de preguntas')
    end
  end



end
