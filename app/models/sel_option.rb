# == Schema Information
#
# Table name: sel_options
#
#  id                    :integer          not null, primary key
#  sel_characteristic_id :integer
#  name                  :string(255)
#  description           :text
#  color                 :string(255)
#  position              :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  min_value             :integer
#  max_value             :integer
#  min_sign              :string(255)
#  max_sign              :string(255)
#

class SelOption < ActiveRecord::Base
  belongs_to :sel_characteristic, :inverse_of => :sel_options
  attr_accessible :color, :description, :name, :position, :min_value, :max_value, :min_sign, :max_sign
                  :sel_characteristic

  validates :name, :presence => true
  validates :position, :presence => true
  validates :color, :presence => true

  validates_presence_of :min_value, :if => :is_range?
  validates_presence_of :max_value, :if => :is_range?

  default_scope order('position ASC')

  def is_range?
    self.sel_characteristic.option_type == 1
  end

  def color
    self[:color] ||= 'ffffff' # blanco, si no está definido
  end
end
