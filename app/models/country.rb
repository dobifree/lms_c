# == Schema Information
#
# Table name: countries
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Country  < ActiveRecord::Base

  has_many :company_units

  attr_accessible :codigo, :nombre

  validates :codigo, presence: true, uniqueness: { case_sensitive: false }
  validates :nombre, presence: true, uniqueness: { case_sensitive: false }

  default_scope order('nombre ASC')

end
