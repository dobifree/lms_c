# == Schema Information
#
# Table name: poll_characteristics
#
#  id                :integer          not null, primary key
#  active_reporting  :boolean          default(FALSE)
#  pos_reporting     :integer
#  characteristic_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PollCharacteristic < ActiveRecord::Base
  belongs_to :characteristic
  validates :characteristic_id, uniqueness: true

  attr_accessible :active_reporting, :pos_reporting

  def self.reporting
    PollCharacteristic.where('active_reporting = ?', true).order('pos_reporting')
  end

  def self.by_characterisctic(characteristic)
    PollCharacteristic.where(:characteristic_id => characteristic.id).first_or_create!
  end

  def name
    self.characteristic.nombre
  end

  def data_type_id
    self.characteristic.data_type_id
  end
end
