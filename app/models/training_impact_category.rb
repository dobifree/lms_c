# == Schema Information
#
# Table name: training_impact_categories
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  peso       :float
#  orden      :integer
#

class TrainingImpactCategory < ActiveRecord::Base

  has_many :dnc_training_impacts
  has_many :dnc_training_impact_ps

  attr_accessible :nombre, :orden, :peso

  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :nombre, presence: true, uniqueness: { case_sensitive: false }
  validates :peso, presence: true, numericality: { only_integer: false, greater_than: 0 }

  default_scope order('orden ASC')

end
