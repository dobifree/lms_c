# == Schema Information
#
# Table name: medlic_year_days_licenses
#
#  id         :integer          not null, primary key
#  year       :integer
#  days       :integer          default(0)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class MedlicYearDaysLicense < ActiveRecord::Base
  belongs_to :user
  attr_accessible :days, :year, :user_id

  def self.days_for(user_id, this_year)
    this_days = where(user_id: user_id, year: this_year)
    return nil unless this_days
    this_days = this_days.first
    return nil unless this_days
    this_days.days
  end

end
