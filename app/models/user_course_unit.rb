# == Schema Information
#
# Table name: user_course_units
#
#  id             :integer          not null, primary key
#  user_course_id :integer
#  unit_id        :integer
#  inicio         :datetime
#  fin            :datetime
#  finalizada     :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  ultimo_acceso  :datetime
#  milestone      :integer          default(0)
#  from_date      :datetime
#  to_date        :datetime
#

class UserCourseUnit < ActiveRecord::Base

  belongs_to :user_course
  belongs_to :unit

  attr_accessible :fin, :finalizada, :inicio, :user_course, :unit, :ultimo_acceso, :milestone, :from_date, :to_date

  validates :user_course_id, presence: true
  validates :unit_id, presence: true
  #validates :inicio, presence: true
  #validates :ultimo_acceso, presence: true

  validate :valid_unit

  def valid_unit
    unless self.user_course.course.units.include? self.unit
      errors.add(:invalid_unit, 'La unidad no pertenece al curso')
    end
  end

end
