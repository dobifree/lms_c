# == Schema Information
#
# Table name: vac_requests
#
#  id                              :integer          not null, primary key
#  user_id                         :integer
#  vac_requests_period_id          :integer
#  begin                           :date
#  end                             :date
#  days                            :integer          default(0)
#  days_progressive                :integer          default(0)
#  status                          :integer          default(0)
#  active                          :boolean          default(TRUE)
#  deactivated_at                  :datetime
#  deactivated_by_user_id          :integer
#  prev_request_id                 :integer
#  registered_at                   :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  registered_by_user_id           :integer
#  status_changed_by_user_id       :integer
#  status_description              :text
#  registered_manually             :boolean          default(FALSE)
#  registered_manually_description :text
#  next_request_id                 :integer
#  days_manually                   :decimal(25, 20)
#  end_vacations                   :date
#  historical                      :boolean          default(FALSE)
#  auto_update_legal               :boolean          default(FALSE)
#  auto_update_prog                :boolean          default(FALSE)
#  bp_warning_exception_id         :integer
#

class VacRequest < ActiveRecord::Base
  has_many :vac_records
  has_many :vac_rule_records
  has_many :vac_accumulateds

  belongs_to :bp_warning_exception
  belongs_to :user
  belongs_to :vac_requests_period

  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :status_changed_by_user, class_name: 'User', foreign_key: :status_changed_by_user_id
  belongs_to :prev_request, class_name: 'VacRequest', foreign_key: :prev_request_id
  belongs_to :next_request, class_name: 'VacRequest', foreign_key: :next_request_id

  attr_accessible :vac_requests_period_id,
                  :user_id,
                  :begin,
                  :end,
                  :days,
                  :days_progressive,
                  :days_manually,
                  :status,
                  :status_changed_by_user_id,
                  :status_description,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :prev_request_id,
                  :registered_at,
                  :registered_by_user_id,
                  :registered_manually,
                  :registered_manually_description,
                  :next_request_id,
                  :historical,
                  :bp_warning_exception_id

  validates :user_id, presence: true
  validates :begin, presence: true

  validate :verify_conflict_vacations
  validate :verify_conflict_benefits

  before_save :vac_accumulateds_unactive

  def self.by_user(this_user_id); where(user_id: this_user_id) end
  def self.active; where(active: true) end
  def self.with_warning; where('bp_warning_exception_id is not null') end
  def self.using_this_exception(this_exception_id); where(bp_warning_exception_id: this_exception_id) end

  def exception_available?; bp_warning_exception ? bp_warning_exception.available : nil end

  def closable?
    requests = VacRequest.where(user_id: user_id, active: true, registered_manually: false, historical: false, status: VacRequest.approved).reorder('begin DESC')
    requests.each do |request|
      next unless request.id != id
      next if prev_request_id && prev_request_id == request.id
      next unless request.begin <= self.begin
      return false
    end
    true
  end

  def closable_back_day?(this_date)

  end

  def first_closable
    VacRequest.where(user_id: user_id, active: true, registered_manually: false, historical: false, status: VacRequest.approved).reorder('begin asc').first
  end

  def gimme_all_days_bitch
    #NO TOMA EN CUENTA EL DÍA FINAL REAL DE LOS DÍAS DE BONOS.
    all_days = 0
    all_days += days if days
    all_days += days_progressive if days_progressive
    vac_rule_records.where(active: true).each do |rule_record|
      rule_record_days = rule_record.bp_condition_vacation.bonus_days
      next unless rule_record_days
      all_days += rule_record_days
    end
    all_days
  end

  def status_description_required?
    return nil unless status
    [VacRequest.rejected].include?(status)
  end

  # * Fecha de ingreso (último registro de guardado
  # * Fecha de envío a validación (último registro de envío a validación)
  # * Fecha de aprobación (último registro de aprobación)
  # * Fecha de retorno

  def sent_to_validation_version
    requests = VacRequest.historical_info([], id)
    requests.each do |request|
      next unless request.pending?
      return request
    end
    VacRequestGuest.new
  end

  def approved_version
    requests = VacRequest.historical_info([], id)
    requests.each do |request|
      next unless request.approved?
      return request
    end
    VacRequestGuest.new
  end

  def self.historical_info(array, request_id)
    request = VacRequest.find(request_id)
    array << request
    if request.prev_request
      VacRequest.historical_info(array, request.prev_request_id)
    end
    array.sort_by(&:registered_at).reverse
  end

  def self.management
    VacRequest.where(active: true, status: VacRequest.approved )
  end

  def self.register(user_id)
    VacRequest.where(active: true, user_id: user_id, registered_manually: false, historical: false)
  end

  def in_process?
    VacRequest.status_in_process.include?(status)
  end

  def self.approver
    status_allowed = [VacRequest.pending, VacRequest.approved, VacRequest.closed]
    VacRequest.where(active: true, registered_manually: false, historical: false, status: status_allowed)
  end

  def self.in_process(user_id)
    VacRequest.where(active: true, user_id: user_id, registered_manually: false, historical: false, status: VacRequest.status_in_process)
  end

  def self.prev_in_process(user_id)
    VacRequest.where(active: true, user_id: user_id, registered_manually: false, historical: false, status: VacRequest.status_prev_in_process)
  end

  def self.status_in_process
    [VacRequest.approved, VacRequest.pending]
  end

  def self.status_prev_in_process
    [VacRequest.draft, VacRequest.rejected]
  end

  def visible_by_register?
    return nil unless status
    return false unless active
    VacRequest.register_visible_status.include?(status)
  end

  def visible_by_approver?(user_id)
    return nil unless status
    return false unless active
    return false unless VacRequest.register_visible_status.include?(status)
    approver = VacApprover.where(active: true, user_id: user_id).first
    return [] unless approver
    approvees_user_id = approver.active_approvees_user_id
    approvees_user_id.include?(self.user_id)
  end

  def visible_by_manager?
    return nil unless status
    return false unless active
    VacRequest.manager_visible_status.include?(status)
  end

  def editable_by_manager?
    return unless self.visible_by_manager?
    VacRequest.manager_editable_status.include?(status)
  end

  def editable_by_approver?(user_id, now)
    return unless self.visible_by_approver?(user_id)
    VacRequest.approver_editable_status.include?(status) || (approved? && self.begin.to_date > now.to_date)
  end

  def editable_by_register?
    return unless self.visible_by_register?
    VacRequest.register_editable_status.include?(status)
  end

  def self.register_visible(user_id)
    VacRequest.where(user_id: user_id, active: true, registered_manually: false, historical: false, status: VacRequest.register_visible_status)
  end

  def self.register_editable(user_id)
    VacRequest.where(user_id: user_id, active: true, registered_manually: false, historical: false, status: VacRequest.register_editable_status)
  end

  def self.approver_editable_of(user_id)
    VacRequest.where(user_id: user_id, active: true, registered_manually: false, historical: false, status: VacRequest.approver_editable_status)
  end

  def self.approver_visible(user_id)
    approver = VacApprover.where(active: true, user_id: user_id).first
    return [] unless approver
    approvees_user_id = approver.active_approvees_user_id
    VacRequest.where(user_id: approvees_user_id, active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status)
  end

  def self.approver_editable(user_id)
    approver = VacApprover.where(active: true, user_id: user_id).first
    return [] unless approver
    approvees_user_id = approver.active_approvees_user_id
    VacRequest.where(user_id: approvees_user_id, active: true, registered_manually: false, historical: false, status: VacRequest.approver_editable_status)
  end

  def self.approver_visible_1(user_id)
    approver = VacApprover.where(active: true, user_id: user_id).first
    return [] unless approver
    approvees_user_id = approver.active_approvees_user_id
    where(user_id: approvees_user_id)
  end

  def self.only_this_status(array_status)
    where(status: array_status)
  end

  def self.default_requests
    VacRequest.where(active: true, registered_manually: false, historical: false)
  end

  def self.manager_visible
    VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.manager_visible_status)
  end

  def self.manager_editable
    VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.manager_editable_status)
  end

  def self.register_visible_status
    [VacRequest.draft, VacRequest.pending, VacRequest.approved, VacRequest.rejected, VacRequest.closed]
  end

  def self.approver_visible_status
    [VacRequest.pending, VacRequest.approved, VacRequest.closed]
  end

  def self.manager_visible_status
    [VacRequest.approved, VacRequest.closed]
  end

  def self.register_editable_status
    [VacRequest.draft, VacRequest.rejected]
  end

  def self.approver_editable_status
    [VacRequest.pending]
  end

  def self.manager_editable_status
    [VacRequest.approved, VacRequest.closed]
  end

  def self.requests_by_status_for(user_id, status)
    VacRequest.where(user_id: user_id, active: true, status: status, historical: false).all
  end

  def self.sum_days_by_status_after(user_id, status, this_date)
    requests = VacRequest.requests_by_status_for(user_id, status)
    sum_days = 0
    requests.each { |request| sum_days += request.days if request.days && this_date.to_date < request.begin }
    sum_days
  end

  def self.sum_progressive_days_by_status_after(user_id, status, this_date)
    requests = VacRequest.requests_by_status_for(user_id, status)
    sum_days = 0
    requests.each { |request| sum_days += request.days_progressive if request.days_progressive && this_date.to_date < request.begin }
    sum_days
  end

  def self.sum_days_by_status(user_id, status)
    requests = VacRequest.requests_by_status_for(user_id, status)
    sum_days = 0
    requests.each { |request| sum_days += request.days if request.days }
    sum_days
  end

  def self.sum_progressive_days_by_status(user_id, status)
    requests = VacRequest.requests_by_status_for(user_id, status)
    sum_days = 0
    requests.each { |request| sum_days += request.days_progressive if request.days_progressive }
    sum_days
  end

  def self.sum_total_generated(user_id)
    requests = VacRequest.requests_by_status_for(user_id, VacRequest.closed )
    sum_days = 0
    requests.each { |request| sum_days += request.days_manually.abs if request.days_manually && request.days_manually < 0 }
    sum_days
  end

  def self.sum_prog_total_generated(user_id)
    requests = VacRequest.requests_by_status_for(user_id, VacRequest.closed )
    sum_days = 0
    requests.each {|request| sum_days += request.days_progressive.abs if request.days_progressive && request.days_progressive < 0 }
    sum_days
  end

  def self.sum_total_taken(user_id)
    requests = VacRequest.requests_by_status_for(user_id, [VacRequest.closed, VacRequest.approved] )
    sum_days = 0
    requests.each do |request|
      sum_days += request.days_manually.abs if request.days_manually && request.days_manually > 0
      sum_days += request.days.abs if request.days && request.days > 0
    end
    sum_days
  end

  def self.sum_prog_total_taken(user_id)
    requests = VacRequest.requests_by_status_for(user_id, [VacRequest.closed, VacRequest.approved] )
    sum_days = 0
    requests.each {|request| sum_days += request.days_progressive.abs if request.days_progressive && request.days_progressive > 0 }
    sum_days
  end


  def self.requests_after(up_to_date, user_id)
    # OBTIENE REQUESTS CERRADAS O MANUALES CON INICIO MAYOR A UP_TO_DATE DEL USER_ID
    up_to_date = up_to_date.to_date
    requests = []
    user_requests = VacRequest.where(user_id: user_id, active: true, historical: false).order('begin ASC, registered_at ASC, id ASC')
    user_requests.each do |request|
      if request.registered_manually
        requests << request if request.begin.to_date > up_to_date
      else
        requests << request if request.closed? && request.begin.to_date > up_to_date
      end
    end
    return requests
  end

  def self.requests_this_date(up_to_date, user_id)
    # OBTIENE REQUESTS CERRADAS O MANUALES CON INICIO IGUAL A UP_TO_DATE DEL USER_ID
    up_to_date = up_to_date.to_date
    requests = []
    user_requests = VacRequest.where(user_id: user_id, active: true, historical: false).order('begin ASC, registered_at ASC, id ASC')
    user_requests.each do |request|
      if request.registered_manually
        requests << request if request.begin.to_date == up_to_date
      else
        requests << request if request.closed? && request.begin.to_date == up_to_date
      end
    end
    return requests
  end

  def self.requests_before(up_to_date, user_id)
    #OBTIENE REQUESTES CERRADAS O MANUALES CON INICIO ANTERIOR AL UP_TO_DATE DEL USER_ID
    up_to_date = up_to_date.to_date
    requests = []
    user_requests = VacRequest.where(user_id: user_id, active: true, historical: false).order('begin ASC, registered_at ASC, id ASC')
    user_requests.each do |request|
      if request.registered_manually
        requests << request if request.begin.to_date < up_to_date
      else
        requests << request if request.closed? && request.begin.to_date < up_to_date
      end
    end
    return requests
  end

  def self.all_registered_manually
    requests = VacRequest.where(registered_manually: true, historical: false, active: true).joins(:user).order('users.apellidos ASC, users.nombre ASC, begin DESC, registered_at DESC, id DESC').all
    requests.size > 0 ? requests : []
  end

  def self.registered_manually_of(user_id)
    VacRequest.where(registered_manually: true, historical: false, active: true, user_id: user_id).joins(:user).order('users.apellidos ASC, users.nombre ASC, begin DESC, registered_at DESC, id DESC')
  end

  def self.all_historical
    where(historical: true, active: true).joins(:user).order('users.apellidos ASC, users.nombre ASC, begin DESC, id DESC')
  end

  def latest_accumulated
    VacAccumulated.where(active: true, user_id: self.user_id, vac_request_id: self.id).order('up_to_date DESC, registered_at desc, id DESC').first
  end

  def before_request(status_array)
    actual_request = VacRequest.find(id)
    requests = VacRequest.where(user_id: user_id, active: true, status: status_array, historical: false).order('begin DESC, registered_at DESC, id DESC')

    requests.each do |request|
      next if request.id == id
      next unless request.registered_manually || request.closed?
      if request.begin < actual_request.begin
        return request
      else
        if request.begin == actual_request.begin && request.registered_at < actual_request.registered_at
          return request
        else
          if request.begin == actual_request.begin && request.registered_at == actual_request.registered_at && request.id < id
            return request
          end
        end
      end
    end
    nil
  end

  def before_accumulated
    bef_request = self.before_request([VacRequest.closed])
    return nil unless bef_request
    VacAccumulated.where(active: true, user_id: bef_request.user_id, vac_request_id: bef_request.id).first
  end

  def latest_version
    return id unless next_request_id
    request = VacRequest.find(next_request_id)
    return request.latest_version
  end

  def latest_record?
    vac_records.where(active: true).first
  end

  def benefits_pending_refund
    vac_rule_records.where(active: true, refund_pending: true).all
  end

  def benefits_active
    vac_rule_records.where(active: true, refund_pending: false).all
  end

  def copy_active_rule_records(request_id)
    benefits_active.each do |benefit|
      new_benefit = benefit.dup
      benefit.active = false
      benefit.save
      new_benefit.vac_request_id = request_id
      new_benefit.save
    end
  end

  def full_registered_by_user
    return nil unless registered_by_user
    registered_by_user.codigo.to_s+' '+registered_by_user.apellidos.to_s+', '+registered_by_user.nombre.to_s
  end

  def full_user
    return nil unless user_id
    user.codigo.to_s+' '+user.apellidos.to_s+', '+user.nombre.to_s
  end

  # STATUS GUIDE
  # 0: Borrador
  # 1: Pendiente
  # 2: Aprobado
  # 3: Cerrado
  # 4: Rechazado
  # 5: Eliminado

  def status_colour
    case status
      when 0
        0
      when 1
        4
      when 2
        2
      when 3
        5
      when 4
        5
      when 5
        5
      else
        nil
    end
  end

  def status_message
    case status
      when 0
        'Borrador'
      when 1
        'Pendiente'
      when 2
        'Aprobado'
      when 3
        'Histórico'
      when 4
        'Rechazado'
      when 5
        'Eliminado'
      else
        nil
    end
  end

  def self.status_messages
    ['Borrador', 'Pendiente', 'Aprobado', 'Histórico', 'Rechazado', 'Eliminado']
  end

  def self.status_all
    [0,1,2,3,4,5]
  end

  def self.status_numbers_messages
    [['Borrador',0], ['Pendiente',1], ['Aprobado',2], ['Histórico',3], ['Rechazado', 4], ['Eliminado', 5]]
  end

  def self.status_message(status_num)
    return nil unless VacRequest.status_all.include?(status_num.to_i)
    VacRequest.status_messages[status_num.to_i]
  end

  def deleted?;  status ? status == 5 : nil end

  def draft?
    return nil unless status
    status.zero?
  end

  def pending?
    return nil unless status
    status == 1
  end

  def approved?
    return nil unless status
    status == 2
  end

  def closed?
    return nil unless status
    status == 3
  end

  def rejected?
    return nil unless status
    status == 4
  end

  def on_going?(now)
    return nil unless status
    return false unless status == 2
    now > self.begin && now < self.end
  end

  def past?(now)
    return nil unless status
    return false unless status == 2
    now > self.end
  end

  def only_approved?(now)
    return nil unless status
    !(on_going?(now) || past?(now))
  end

  def set_draft
    self.status = 0
  end

  def set_pending
    self.status = 1
  end

  def set_approved
    self.status = 2
  end

  def set_closed
    self.status = 3
  end

  def set_rejected
    self.status = 4
  end

  def set_deleted
    self.status = 5
  end

  def self.draft; 0 end

  def self.pending; 1 end

  def self.approved; 2 end

  def self.closed; 3 end

  def self.rejected; 4 end

  def self.deleted; 5 end

  private

  def vac_accumulateds_unactive
    return if registered_manually || historical
    return unless active
    prev_accumulated = latest_accumulated
    return unless prev_accumulated
    prev_accumulated.active = false
    prev_accumulated.deactivated_at = deactivated_at
    prev_accumulated.deactivated_by_user_id = deactivated_by_user_id
    prev_accumulated.deactivated_description = 'Se desactivó solicitud'
    prev_accumulated.save
  end

  def restrictive_warnings
    return unless self.begin
    return unless self.user_id
    rules = BpGeneralRulesVacation.user_active(self.begin.to_datetime, user_id)
    return unless rules
    warnings = [] #rules.doesnt_satisfy_restrictive_warnings(days)
    return unless warnings.size > 0
    errors.add(:days, 'Tiene por lo menos una restricción')
  end

  def verify_conflict_benefits
    return
    return if registered_manually || historical || !active
    return if status == VacRequest.draft || status == VacRequest.rejected

    requests = LicRequest.where(active: true).joins(:lic_event).where(lic_events: { active: true, user_id: user_id})
    message = ''
    begin_date = self.begin.to_date
    end_date = self.end.to_date

    requests.each do |request|
      next unless (request.begin < end_date && request.end > begin_date)
      message += request.begin.strftime('%d/%m/%Y')+' - '+request.end.strftime('%d/%m/%Y')+' ('+request.status_message+'). '
    end
    errors.add(:base, 'Conflicto con beneficio: ' + message) if message.size > 0
  end

  def verify_conflict_vacations
    return if registered_manually || historical || !active
    return if status == VacRequest.draft || status == VacRequest.rejected
    requests = VacRequest.where(active: true, registered_manually: false, historical: false, status: VacRequest.approver_visible_status, user_id: user_id)
    message = ''
    begin_date = self.begin.to_date
    end_date = self.end.to_date

    requests.each do |request|
      next if (id && request.id.to_i == id.to_i) || (prev_request_id && request.id.to_i == prev_request_id.to_i)
      next unless (request.begin <= end_date && request.end >= begin_date)
      message += request.begin.strftime('%d/%m/%Y')+' - '+request.end.strftime('%d/%m/%Y')+' ('+request.status_message+'). '
    end
    message = 'tiene conflicto con vacaciones: ' + message if message.size > 0
    errors.add(:begin, ' '+message) if message.size > 0
  end
end
