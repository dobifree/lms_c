# == Schema Information
#
# Table name: scholarship_application_item_ranges
#
#  id                              :integer          not null, primary key
#  scholarship_application_item_id :integer
#  scholarship_list_item_id        :integer
#  position                        :integer
#  min_val                         :float
#  min_sign                        :integer
#  max_val                         :float
#  max_sign                        :integer
#  is_default                      :boolean
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

class ScholarshipApplicationItemRange < ActiveRecord::Base
  belongs_to :scholarship_application_item
  belongs_to :scholarship_list_item
  attr_accessible :is_default, :max_sign, :max_val, :min_sign, :min_val,
                  :position, :scholarship_list_item_id

  default_scope order('position ASC')

  def self.min_sign_options
    [' > ', ' >= ']
  end

  def self.max_sign_options
    [' < ', ' <= ']
  end
end
