# == Schema Information
#
# Table name: pe_member_observers
#
#  id                    :integer          not null, primary key
#  pe_member_observed_id :integer
#  observer_id           :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  pe_process_id         :integer
#

class PeMemberObserver < ActiveRecord::Base

  belongs_to :pe_member_observed, class_name: 'PeMember'

  belongs_to :observer, class_name: 'User'

  belongs_to :pe_process

  attr_accessible :observer_id, :pe_member_observed_id

  validates :observer_id, uniqueness: { scope: :pe_member_observed_id }

end
