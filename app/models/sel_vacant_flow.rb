# == Schema Information
#
# Table name: sel_vacant_flows
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  description            :text
#  active                 :boolean
#  threshold_for_approval :float
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class SelVacantFlow < ActiveRecord::Base

  has_many :sel_flow_approvers, :dependent => :destroy
  attr_accessible :active, :description, :name, :threshold_for_approval
end
