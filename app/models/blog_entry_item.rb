# == Schema Information
#
# Table name: blog_entry_items
#
#  id                :integer          not null, primary key
#  blog_entry_id     :integer
#  blog_form_item_id :integer
#  value             :text
#  blog_list_item_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class BlogEntryItem < ActiveRecord::Base
  belongs_to :blog_entry
  belongs_to :blog_form_item
  belongs_to :blog_list_item
  attr_accessible :value, :blog_list_item_id, :blog_form_item_id

  before_save :set_value_to_list_item

  def set_value_to_list_item
    if self.blog_list_item_id && self.blog_form_item.blog_list_id
      self.value = self.blog_list_item.name
    end
  end
end
