# == Schema Information
#
# Table name: user_j_jobs
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  j_job_id   :integer
#  from_date  :date
#  to_date    :date
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserJJob < ActiveRecord::Base

  belongs_to :user
  belongs_to :j_job

  attr_accessible :from_date, :to_date, :user_id, :j_job_id

  default_scope order('from_date, to_date')

  after_create :set_company

  private

  def set_company

    if self.j_job

      characteristic_company = Characteristic.get_characteristic_company

      if characteristic_company

        j_job = self.j_job

        j_job_company = j_job.j_job_characteristic characteristic_company

        if j_job_company

          value = j_job_company.value


          if value

            self.user.update_user_characteristic(characteristic_company, value, self.from_date.to_datetime, nil, nil, nil, nil, @company)

          end


        end

      end

    end

  end

end
