# == Schema Information
#
# Table name: training_providers
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TrainingProvider < ActiveRecord::Base

  has_many :dncs
  has_many :dncps

  attr_accessible :nombre

  validates :nombre, presence: true, uniqueness: { case_sensitive: false }

  default_scope order('nombre ASC')


end
