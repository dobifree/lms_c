# == Schema Information
#
# Table name: master_evaluation_topics
#
#  id                   :integer          not null, primary key
#  numero               :integer
#  nombre               :text
#  master_evaluation_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class MasterEvaluationTopic < ActiveRecord::Base

  belongs_to :master_evaluation

  has_many :master_evaluation_questions, dependent: :destroy

  attr_accessible :nombre, :numero

  validates :nombre, presence: true
  validates :numero, presence: true, uniqueness: { scope: :master_evaluation_id }, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('numero ASC')

end
