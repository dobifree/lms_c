# == Schema Information
#
# Table name: vac_requests_periods
#
#  id                    :integer          not null, primary key
#  request_since         :datetime
#  request_until         :datetime
#  approve_since         :datetime
#  approve_until         :datetime
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class VacRequestsPeriod < ActiveRecord::Base
  belongs_to :registered_by_user, class_name: 'User', foreign_key: 'registered_by_user_id'

  attr_accessible :approve_since,
                  :approve_until,
                  :request_since,
                  :request_until,
                  :registered_at,
                  :registered_by_user_id

  validates :approve_since, presence: true
  validates :approve_until, presence: true
  validates :request_since, presence: true
  validates :request_until, presence: true
  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def approve_active?(now)
    return true if approve_until && approve_since < now && approve_until > now
    return true if !approve_until && now > approve_since
    false
  end

  def request_active?(now)
    return true if request_until && request_since < now && request_until > now
    return true if !request_until && now > request_since
    false
  end

end
