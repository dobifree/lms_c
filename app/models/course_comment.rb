# == Schema Information
#
# Table name: course_comments
#
#  id         :integer          not null, primary key
#  course_id  :integer
#  user_id    :integer
#  fecha      :datetime
#  comentario :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CourseComment < ActiveRecord::Base
  belongs_to :course
  belongs_to :user

  attr_accessible :comentario, :fecha

  validates :comentario, presence: true

end
