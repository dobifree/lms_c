# == Schema Information
#
# Table name: pe_questions
#
#  id                             :integer          not null, primary key
#  description                    :text
#  weight                         :float            default(1.0)
#  pe_evaluation_id               :integer
#  pe_group_id                    :integer
#  pe_element_id                  :integer
#  pe_question_id                 :integer
#  has_comment                    :boolean
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  pe_member_evaluator_id         :integer
#  pe_member_evaluated_id         :integer
#  indicator_type                 :integer          default(0)
#  indicator_unit                 :string(255)
#  indicator_goal                 :decimal(30, 6)
#  indicator_discrete_goal        :string(255)
#  kpi_indicator_id               :integer
#  confirmed                      :boolean          default(TRUE)
#  pe_rel_id                      :integer
#  informative                    :boolean          default(FALSE)
#  accepted                       :boolean          default(TRUE)
#  ready                          :boolean          default(TRUE)
#  validated                      :boolean          default(TRUE)
#  creator_id                     :integer
#  entered_by_manager             :boolean          default(FALSE)
#  points                         :float
#  pe_alternative_id              :integer
#  indicator_achievement          :float
#  indicator_discrete_achievement :string(255)
#  stored_image_id                :integer
#  pe_question_model_id           :integer
#  power                          :float            default(1.0)
#  leverage                       :float            default(0.0)
#  number_of_activities           :integer          default(0)
#  comment                        :text
#  note                           :text
#  has_comment_compulsory         :boolean          default(FALSE)
#  questions_grouped_id           :integer
#  has_comment_2                  :boolean          default(FALSE)
#  has_comment_2_compulsory       :boolean          default(FALSE)
#

class PeQuestion < ActiveRecord::Base

  belongs_to :pe_evaluation
  belongs_to :pe_group
  belongs_to :pe_member_evaluator, class_name: 'PeMember'
  belongs_to :pe_member_evaluated, class_name: 'PeMember'
  belongs_to :pe_rel
  belongs_to :pe_element
  belongs_to :pe_question
  belongs_to :kpi_indicator
  belongs_to :creator, class_name: 'User'
  belongs_to :pe_alternative
  belongs_to :stored_image
  belongs_to :pe_question_model

  has_many :pe_question_descriptions, dependent: :destroy #
  has_many :pe_questions, dependent: :destroy #
  has_many :pe_alternatives, dependent: :destroy #
  has_many :pe_question_ranks, dependent: :destroy #

  has_many :pe_assessment_questions, dependent: :destroy
  has_many :pe_assessment_final_questions, dependent: :destroy
  has_many :pe_question_comments, dependent: :destroy #

  has_many :pe_question_files, dependent: :destroy #

  has_many :pe_question_validations, dependent: :destroy #
  has_many :pe_question_activities, dependent: :destroy


  attr_accessible :description, :has_comment, :pe_question_id, :weight, :pe_evaluation_id, :pe_group_id, :pe_element_id,
                  :indicator_type, :indicator_unit, :indicator_goal, :indicator_discrete_goal, :pe_member_evaluated,
                  :kpi_indicator_id, :confirmed, :accepted, :ready, :pe_rel_id, :informative, :entered_by_manager,
                  :points, :indicator_achievement, :indicator_discrete_achievement, :stored_image_id,
                  :power, :leverage, :comment, :note, :has_comment_compulsory,
                  :has_comment_2, :has_comment_2_compulsory

  #before_save { self.weight = 0 if self.weight.nil? || self.weight.blank?}

  before_save :set_status

  before_validation :set_description

  validates :description, presence: true, uniqueness: { case_sensitive: false, scope: [:pe_evaluation_id, :pe_group_id, :pe_member_evaluator_id, :pe_member_evaluated_id, :pe_element_id, :pe_question_id] }
  validates :weight, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :pe_evaluation_id, presence: true
  validates :pe_element_id, presence: true
  validates :indicator_unit, presence: true, if: :validates_unit
  validate :validates_unit_no_numbers
  validates :indicator_goal, presence: true, numericality: { greater_than_or_equal_to: 0 }, if: :validates_goal
  validates :indicator_goal, presence: true, if: :validates_goal_security
  validates :indicator_goal, presence: true, numericality: { greater_than: 0 }, if: :validates_goal_greater_than_zero
  validates :power, presence: true, numericality: { greater_than_or_equal_to: -1 }, if: :validates_security_indicator
  validates :leverage, presence: true, numericality: { greater_than_or_equal_to: 0 }, if: :validates_security_indicator
  validate :validates_note

  default_scope joins('left join pe_question_models on pe_questions.pe_question_model_id = pe_question_models.id').order('pe_question_models.id, pe_questions.id').readonly(false)

  def validates_unit

    if self.kpi_indicator_id
      false
    elsif self.pe_element.assessed && self.pe_element.assessment_method == 2 && self.indicator_type != 4
      true
    else
      false
    end

  end

  def validates_unit_no_numbers
    if !self.pe_element.unit_allow_digits? && self.indicator_unit =~ /\d/
      errors.add(:indicator_unit, :not_numbers)
      false
    else
      true
    end
  end

  def validates_security_indicator
    if self.kpi_indicator_id
      false
    elsif self.pe_element.assessed && self.pe_element.assessment_method == 2 && self.indicator_type == 5
      true
    else
      false
    end
  end

  def validates_goal

    if self.kpi_indicator_id
      false
    elsif self.pe_element.assessed && self.pe_element.assessment_method == 2 && (self.indicator_type == 0 || self.indicator_type == 1 || self.indicator_type == 2)
      true
    else
      false
    end

  end

  def validates_goal_security

    if self.kpi_indicator_id
      false
    elsif self.pe_element.assessed && self.pe_element.assessment_method == 2 && (self.indicator_type == 5)
      true
    else
      false
    end

  end

  def validates_goal_greater_than_zero
    self.pe_element.assessed && self.pe_element.assessment_method == 2 && (self.indicator_type == 0 || self.indicator_type == 1)
  end

  def validates_note
    if self.pe_element.note_compulsory && self.note.blank?
      self.errors.add(self.pe_element.alias_note, 'no puede ser vacía')
      false
    else
      true
    end

  end

  def self.indicator_types
    [:directo, :inverso, :desviacion, :rangos, :discreto, :especial]
  end

  def pe_question_descriptions_ordered

    self.pe_question_descriptions.joins(:pe_element_description).reorder('pe_element_descriptions.position, pe_element_descriptions.name')

  end

  def pe_question_description_by_pe_element_description(pe_element_description)
    self.pe_question_descriptions.where('pe_element_description_id = ?', pe_element_description.id).first
  end



  def self.indicator_types_array_for_select

    array_for_select = Array.new

    self.indicator_types.each_with_index do |it, index|

      array_for_select.push [it, index]

    end

    return array_for_select

  end

  def indicator_type_name
    PeQuestion.indicator_types[self.indicator_type]
  end

  def set_goal_rank
    if self.indicator_type == 3
      pe_question_rank = self.pe_question_ranks.where('percentage = 100').first
      if pe_question_rank
        self.indicator_goal = pe_question_rank.goal
      end
    end

    if self.indicator_type == 4
      pe_question_rank = self.pe_question_ranks.where('percentage = 100').first
      if pe_question_rank
        self.indicator_discrete_goal = pe_question_rank.discrete_goal
        self.indicator_goal = 100
      end
    end

  end

  def set_description
    self.description = self.kpi_indicator.name if self.kpi_indicator
  end

  def pe_question_comments_by_pe_member_rel(pe_member_rel)
    self.pe_question_comments.where('pe_member_rel_id = ?', pe_member_rel.id)
  end

  def pe_question_comments_by_pe_member_rels(pe_member_rels)
    self.pe_question_comments.where('pe_member_rel_id IN(?)', pe_member_rels.map {|pmr| pmr.id })
  end

  def pe_question_files_by_pe_member(pe_member)
    self.pe_question_files.where('pe_member_id = ?', pe_member.id)
  end



  def validated_before_accept

    validated_before = true

    (1..self.pe_evaluation.num_steps_definition_by_users_validated_before_accepted).each do |step_number|

      if self.pe_member_evaluated.pe_definition_by_user_validator(self.pe_evaluation, step_number, true)

        validated_before = false if self.pe_question_validations.where('step_number = ? AND before_accept = ? AND validated = ?', step_number, true, true).size == 0

      end

    end

    validated_before

  end

  def validated_after_accept

    validated_after = true

    (1..self.pe_evaluation.num_steps_definition_by_users_validated_after_accepted).each do |step_number|

      if self.pe_member_evaluated.pe_definition_by_user_validator(self.pe_evaluation, step_number, false)

        validated_after = false if self.pe_question_validations.where('step_number = ? AND before_accept = ? AND validated = ?', step_number, false, true).size == 0

      end

    end

    validated_after

  end

  def set_status

    if self.confirmed?

      unless self.ready

        if self.pe_evaluation.pe_process.has_step_definition_by_users && self.pe_evaluation.allow_definition_by_users && self.pe_element.element_def_by == 1

          to_be_accepted = self.pe_evaluation.needs_definition_by_users_accept ? true : false
          to_be_validated_before_accept = self.pe_evaluation.needs_definition_by_users_validation_before_accepted ? true : false
          to_be_validated_after_accept = self.pe_evaluation.needs_definition_by_users_validation_after_accepted ? true : false

          validated_before = self.validated_before_accept
          validated_after = self.validated_after_accept

          if to_be_accepted && to_be_validated_before_accept && to_be_validated_after_accept

            if self.accepted && validated_before && validated_after

              self.validated = true
              self.ready = true

            end

          elsif to_be_accepted && to_be_validated_before_accept

            if self.accepted && validated_before

              self.validated = true
              self.ready = true

            end

          elsif to_be_accepted && to_be_validated_after_accept

            if self.accepted && validated_after

              self.validated = true
              self.ready = true

            end

          elsif to_be_validated_before_accept && to_be_validated_after_accept

            if validated_before && validated_after

              self.validated = true
              self.ready = true

            end

          elsif to_be_accepted

            if self.accepted

              self.ready = true

            end

          elsif to_be_validated_before_accept

            if validated_before

              self.validated = true
              self.ready = true

            end

          elsif to_be_validated_after_accept

            if validated_after

              self.validated = true
              self.ready = true

            end

          else

            self.ready = true

          end

        else

          self.ready = true

        end

      end

    else

      self.accepted = false
      self.validated = false
      self.ready = false

    end

    nil

  end

  def pe_question_validations_ordered

    self.pe_question_validations.reorder('before_accept DESC, step_number ASC')

  end

  def pe_question_validation(step_number, before_accept)
    self.pe_question_validations.where('step_number = ? AND before_accept = ?', step_number, before_accept).first
  end

  def pending_validations
    self.pe_question_validations.where('validated = ? AND attended = ?', false, false)
  end

  def pe_assessment_questions_by_pe_assessment_evaluations(pe_assessment_evaluations_id)
    self.pe_assessment_questions.where('pe_assessment_evaluation_id in (?)', pe_assessment_evaluations_id)
  end

  def pe_question_ranks_discrete
    self.pe_question_ranks.where('discrete_goal IS NOT NULL')
  end

  def pe_question_ranks_non_discrete
    self.pe_question_ranks.where('discrete_goal IS NULL')
  end

  def pe_question_ranks_by_pe_question_rank_model(pe_question_rank_model)
    self.id ? self.pe_question_ranks.where('pe_question_rank_model_id = ?', pe_question_rank_model.id).first : nil
  end

end
