# == Schema Information
#
# Table name: scholarship_lists
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  active      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ScholarshipList < ActiveRecord::Base

  has_many :scholarship_list_items, dependent: :destroy
  accepts_nested_attributes_for :scholarship_list_items, allow_destroy: true

  attr_accessible :active, :description, :name, :scholarship_list_items_attributes

  validates :name, presence: true

end
