# == Schema Information
#
# Table name: blog_form_items
#
#  id           :integer          not null, primary key
#  blog_form_id :integer
#  position     :integer
#  name         :string(255)
#  description  :text
#  item_type    :integer
#  blog_list_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  mandatory    :boolean          default(FALSE)
#

class BlogFormItem < ActiveRecord::Base
  belongs_to :blog_form
  belongs_to :blog_list
  attr_accessible :description, :item_type, :name, :position, :blog_list_id, :mandatory

  validates :position, :numericality => true
  validates :name, :presence => true
  validates :item_type, :presence => true
  validates :blog_list_id, :presence => {:if => :need_a_list?}

  default_scope { order(:position, :name) }

  def self.item_type_options
    ['Texto', 'Texto largo', 'Fecha', 'Lista']
  end

  def need_a_list?
    if self.item_type == 3
      return true
    else
      return false
    end
  end

end
