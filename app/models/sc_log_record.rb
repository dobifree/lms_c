# == Schema Information
#
# Table name: sc_log_records
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  date          :datetime
#  record_before :text
#  record_after  :text
#  action        :string(255)
#  sc_record_id  :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class ScLogRecord < ActiveRecord::Base
  belongs_to :user
  belongs_to :sc_record
  attr_accessible :action,
                  :date,
                  :record_after,
                  :record_before,
                  :user_id,
                  :sc_record_id
end
