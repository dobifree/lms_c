# == Schema Information
#
# Table name: pe_evaluation_rels
#
#  id                        :integer          not null, primary key
#  pe_evaluation_id          :integer
#  pe_rel_id                 :integer
#  weight                    :float            default(0.0)
#  from_date                 :datetime
#  to_date                   :datetime
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  final_temporal_assessment :boolean          default(FALSE)
#

class PeEvaluationRel < ActiveRecord::Base

  belongs_to :pe_evaluation
  belongs_to :pe_rel

  has_many :pe_evaluation_rel_group2s, :dependent => :destroy #

  attr_accessible :from_date, :to_date, :weight, :pe_evaluation_id, :pe_rel_id, :final_temporal_assessment

  before_save :correct_to_date

  validates :pe_evaluation_id, presence: true
  validates :pe_rel_id, presence: true
  validates :from_date, presence: true
  validates :to_date, presence: true
  validates :weight, presence: true, numericality: { greater_than_or_equal_to: 0 }

  def correct_to_date

    if self.to_date

      self.to_date = self.to_date+((24*60*60)-1).seconds

    end

  end

end
