# == Schema Information
#
# Table name: program_course_instance_evaluations
#
#  id                         :integer          not null, primary key
#  program_course_instance_id :integer
#  evaluation_id              :integer
#  from_date                  :datetime
#  to_date                    :datetime
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class ProgramCourseInstanceEvaluation < ActiveRecord::Base
  belongs_to :program_course_instance
  belongs_to :evaluation
  attr_accessible :from_date, :to_date
end
