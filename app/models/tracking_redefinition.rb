# == Schema Information
#
# Table name: tracking_redefinitions
#
#  id                        :integer          not null, primary key
#  tracking_form_instance_id :integer
#  required_by_user_id       :integer
#  required_at               :datetime
#  original_data             :text
#  final_data                :text
#  finished                  :boolean          default(FALSE)
#  finished_at               :datetime
#  applied                   :boolean          default(FALSE)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#

class TrackingRedefinition < ActiveRecord::Base
  belongs_to :tracking_form_instance
  attr_accessible :applied, :final_data, :finished, :finished_at, :original_data, :required_at, :required_by_user_id
end
