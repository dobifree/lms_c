# == Schema Information
#
# Table name: pe_areas
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  pe_process_id        :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  pe_box_id            :integer
#  is_evaluated         :boolean          default(FALSE)
#  is_evaluator         :boolean          default(FALSE)
#  step_assessment      :boolean          default(FALSE)
#  step_assessment_date :datetime
#

class PeArea < ActiveRecord::Base

  belongs_to :pe_process
  has_many :pe_members
  belongs_to :pe_box

  has_many :pe_assessment_final_evaluations
  has_many :pe_assessment_dimensions

  attr_accessible :name, :is_evaluated, :is_evaluator, :step_assessment, :step_assessment_date

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_process_id }

  default_scope order('name')

  def pe_assessment_final_evaluation(pe_evaluation)

    self.pe_assessment_final_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

  end

  def pe_assessment_dimension(pe_dimension)

    self.pe_assessment_dimensions.where('pe_dimension_id = ?', pe_dimension.id).first

  end

  def set_pe_box

    pe_box = nil

    pe_assessment_dim_x = self.pe_assessment_dimension self.pe_process.dimension_x

    if self.pe_process.two_dimensions

      pe_assessment_dim_y = self.pe_assessment_dimension self.pe_process.dimension_y

      pe_box = self.pe_process.pe_box_2d(pe_assessment_dim_x.pe_dimension_group.id, pe_assessment_dim_y.pe_dimension_group.id) if pe_assessment_dim_x && pe_assessment_dim_x.pe_dimension_group && pe_assessment_dim_y && pe_assessment_dim_y.pe_dimension_group

    else

      pe_box = self.pe_process.pe_box_1d(pe_assessment_dim_x.pe_dimension_group.id) if pe_assessment_dim_x && pe_assessment_dim_x.pe_dimension_group


    end

    self.pe_box = pe_box

  end

end
