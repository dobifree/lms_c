# == Schema Information
#
# Table name: pe_members
#
#  id                           :integer          not null, primary key
#  pe_process_id                :integer
#  user_id                      :integer
#  is_evaluated                 :boolean          default(FALSE)
#  is_evaluator                 :boolean          default(FALSE)
#  step_assessment              :boolean          default(FALSE)
#  step_assessment_date         :datetime
#  step_calibration             :boolean          default(FALSE)
#  step_calibration_date        :datetime
#  step_feedback                :boolean          default(FALSE)
#  step_feedback_date           :datetime
#  step_feedback_accepted       :boolean          default(FALSE)
#  step_feedback_accepted_date  :datetime
#  step_feedback_text           :text
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  pe_box_id                    :integer
#  pe_box_before_cal_id         :integer
#  calibration_comment          :text
#  feedback_provider_id         :integer
#  pe_area_id                   :integer
#  step_validation              :boolean          default(FALSE)
#  step_validation_date         :datetime
#  require_validation           :boolean          default(FALSE)
#  can_select_evaluated         :boolean          default(FALSE)
#  can_be_selected_as_evaluated :boolean          default(FALSE)
#  removed                      :boolean          default(FALSE)
#  step_assign                  :boolean          default(FALSE)
#  step_assign_date             :datetime
#

class PeMember < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :user
  belongs_to :pe_box
  belongs_to :pe_box_before_calibration, class_name: 'PeBox', foreign_key: 'pe_box_before_cal_id'
  belongs_to :pe_area

  belongs_to :feedback_provider, class_name: 'User'

  has_many :pe_member_evaluations, dependent: :destroy #

  has_many :pe_member_groups, dependent: :destroy #

  has_many :pe_member_rels_is_evaluator, class_name: 'PeMemberRel', foreign_key: 'pe_member_evaluator_id', dependent: :destroy #
  has_many :pe_member_rels_is_evaluated, class_name: 'PeMemberRel', foreign_key: 'pe_member_evaluated_id', dependent: :destroy #

  has_many :pe_definition_by_user_validators, dependent: :destroy #

  has_many :validators, through: :pe_member_rels_is_evaluated #

  has_many :pe_member_rel_shared_comments, through: :pe_member_rels_is_evaluated

  has_many :pe_member_observers, foreign_key: 'pe_member_observed_id' #

  has_many :pe_questions_is_evaluated, class_name: 'PeQuestion', foreign_key: 'pe_member_evaluated_id', dependent: :destroy #
  has_many :pe_questions_is_evaluator, class_name: 'PeQuestion', foreign_key: 'pe_member_evaluator_id', dependent: :destroy #

  has_many :pe_assessment_final_evaluations, dependent: :destroy #
  has_many :pe_assessment_dimensions, dependent: :destroy #

  has_many :pe_assessment_groups

  has_many :pe_member_characteristics, dependent: :destroy

  has_one  :pe_cal_member, dependent: :destroy #

  has_many :pe_member_feedbacks, dependent: :destroy #
  has_many :pe_member_accepted_feedbacks, dependent: :destroy #



  has_one :pe_member_group2

#  has_many :pe_assessment_evaluations, through: :pe_member_rels_is_evaluated

  attr_accessible :is_evaluated, :is_evaluator, :step_assessment, :step_assessment_date,
                  :require_validation, :step_validation, :step_validation_date,
                  :step_calibration, :step_calibration_date, :step_feedback, :step_feedback_accepted,
                  :step_feedback_accepted_date, :step_feedback_date, :step_feedback_text,
                  :calibration_comment, :pe_box_id, :pe_box_before_cal_id,
                  :feedback_provider_id, :can_select_evaluated, :can_be_selected_as_evaluated,
                  :step_assign, :step_assign_date

  validates :pe_process_id, presence: true
  validates :user_id, presence: true, uniqueness: {scope: [:pe_process_id, :pe_area_id]}

  def started_participation?

    return false

  end

  def remove_as_assessed

    self.pe_member_rels_is_evaluated.destroy_all
    self.pe_definition_by_user_validators.destroy_all
    self.pe_questions_is_evaluated.destroy_all
    self.pe_assessment_final_evaluations.destroy_all
    self.pe_assessment_dimensions.destroy_all
    self.pe_box = nil
    self.pe_box_before_calibration = nil
    self.pe_member_groups.destroy_all
    self.pe_member_evaluations.destroy_all
    self.pe_cal_member.destroy if self.pe_cal_member
    self.pe_member_feedbacks.destroy_all
    self.pe_member_accepted_feedbacks.destroy_all
    self.is_evaluated = false

  end

  def reset_evaluation(pe_evaluation)

    self.pe_questions_is_evaluated.where('pe_evaluation_id = ?', pe_evaluation.id).each do |pe_question|

      pe_question.pe_assessment_questions.destroy_all
      pe_question.pe_question_comments.destroy_all
      pe_question.pe_question_activities.destroy_all

    end

    self.pe_member_rels_is_evaluated.each do |pe_member_rel_evaluated|

      pe_assessment_evaluation = pe_member_rel_evaluated.pe_assessment_evaluation pe_evaluation

      if pe_assessment_evaluation

        pe_assessment_evaluation.destroy

        pe_member_rel_evaluated.finished = false
        pe_member_rel_evaluated.valid_evaluator = true
        pe_member_rel_evaluated.validated = false
        pe_member_rel_evaluated.validation_comment = nil
        pe_member_rel_evaluated.finished_at = nil
        pe_member_rel_evaluated.validated_at = nil
        pe_member_rel_evaluated.save

      end

    end

  end

  def reset_assessed

    self.pe_questions_is_evaluated.each do |pe_question|

      pe_question.pe_assessment_questions.destroy_all
      pe_question.pe_question_comments.destroy_all
      pe_question.pe_question_activities.destroy_all

    end

    self.pe_member_rels_is_evaluated.each do |pe_member_rel_evaluated|

      pe_member_rel_evaluated.pe_assessment_evaluations.destroy_all
      pe_member_rel_evaluated.pe_member_rel_shared_comments.destroy_all
      pe_member_rel_evaluated.pe_question_comments.destroy_all

      pe_member_rel_evaluated.finished = false
      pe_member_rel_evaluated.valid_evaluator = true
      pe_member_rel_evaluated.validated = false
      pe_member_rel_evaluated.validation_comment = nil
      pe_member_rel_evaluated.finished_at = nil
      pe_member_rel_evaluated.validated_at = nil
      pe_member_rel_evaluated.save

    end

    self.pe_assessment_final_evaluations.destroy_all
    self.pe_assessment_dimensions.destroy_all
    self.pe_box = nil
    self.pe_box_before_calibration = nil
    self.pe_member_feedbacks.destroy_all
    self.pe_member_accepted_feedbacks.destroy_all
    self.step_assessment = false
    self.step_assessment_date = nil
    self.step_calibration = false
    self.step_calibration_date = nil
    self.step_feedback = false
    self.step_feedback_date = nil
    self.step_feedback_accepted = false
    self.step_feedback_accepted_date = nil
    self.step_feedback_text = nil
    self.calibration_comment = nil
    self.step_validation = false
    self.step_validation_date = nil

  end

  def pe_member_rel_is_evaluated_by_pe_member(pe_member)
    self.pe_member_rels_is_evaluated.where('pe_member_evaluator_id = ?', pe_member.id).first
  end

  def pe_member_rel_is_evaluated_by_user(user)
    self.pe_member_rels_is_evaluated.joins(pe_member_evaluator: [:user]).where('users.id = ?', user.id).first
  end

  def pe_member_group(pe_evaluation)
    self.pe_member_groups.where('pe_evaluation_id = ?', pe_evaluation.id).first
  end

  def has_to_asses_as_pe_rel(pe_rel)
    self.pe_member_rels_is_evaluator.where('pe_rel_id = ?', pe_rel.id).count > 0 ? true : false
  end

  def has_to_asses_as_rel_id(rel_id)
    self.pe_member_rels_is_evaluator.joins(:pe_rel).where('pe_rels.rel = ?', rel_id).count > 0 ? true : false
  end

  def has_to_be_assesed_as_pe_rel(pe_rel)
    self.pe_member_rels_is_evaluated.where('pe_rel_id = ?', pe_rel.id).count > 0 ? true : false
  end

  def has_to_be_assesed_by_rel_id(rel_id)
    self.pe_member_rels_is_evaluated.joins(:pe_rel).where('pe_rels.rel = ?', rel_id).count > 0 ? true : false
  end

  def pe_member_evaluation(pe_evaluation)
    self.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first
  end

  def has_to_be_assessed_in_evaluation(pe_evaluation)
    self.pe_member_evaluations.where('pe_evaluation_id = ? and weight = ?', pe_evaluation.id, -1).size == 0 ? true : false
  end

  def weight_in_evaluation(pe_evaluation)

    pe_member_evaluation = self.pe_member_evaluation(pe_evaluation)

    pe_member_evaluation ? pe_member_evaluation.weight : pe_evaluation.weight

  end

  def num_missing_assessments
    self.pe_member_rels_is_evaluator.where('finished = ?', false).count
  end

  def num_missing_assessments_as_rel(pe_rel)
    n = 0
    self.pe_member_rels_is_evaluator.where('pe_rel_id = ? AND finished = ?', pe_rel.id, false).each do |pmr|
      n += 1 unless pmr.pe_member_evaluated.removed
    end
    n
  end

  def num_assessments_as_rel(pe_rel)
    n = 0
    self.pe_member_rels_is_evaluator.where('pe_rel_id = ?', pe_rel.id).each do |pmr|
      n += 1 unless pmr.pe_member_evaluated.removed
    end
    n
  end

  def pe_member_rel_boss
    pe_rel_boss = self.pe_process.pe_rel_boss
    self.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss).first
  end

  def pe_member_rels_boss
    pe_rel_boss = self.pe_process.pe_rel_boss
    self.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_boss)
  end

  def pe_member_rels_is_evaluated_as_pe_rel(pe_rel)
    self.pe_member_rels_is_evaluated.where('pe_rel_id = ?', pe_rel.id)
  end

  def pe_member_rels_is_evaluated_as_pe_area(pe_area)
    self.pe_member_rels_is_evaluated.joins(:pe_member_evaluator).where('pe_members.pe_area_id = ?', pe_area.id)
  end

  def pe_member_rels_is_evaluated_as_pe_rel_ordered(pe_rel)
    self.pe_member_rels_is_evaluated.joins(pe_member_evaluator: [:user]).where('pe_rel_id = ?', pe_rel.id).reorder('apellidos, nombre')
  end

  def pe_member_rels_is_evaluated_as_rel_id_ordered(rel_id)
    self.pe_member_rels_is_evaluated.joins(:pe_rel).where('pe_rels.rel = ?', rel_id)
  end

  def is_evaluated_as_rel_id_by_pe_member(rel_id, pe_member_evaluator)
    self.pe_member_rels_is_evaluated.joins(:pe_rel).where('pe_rels.rel = ? and pe_member_evaluator_id = ?', rel_id, pe_member_evaluator.id).size == 1 ? true : false
  end

  def is_evaluated_as_pe_rel?(pe_rel)
    self.pe_member_rels_is_evaluated.where('pe_rel_id = ?', pe_rel.id).count > 0 ? true : false
  end

  def pe_member_rels_is_evaluator_ordered
    self.pe_member_rels_is_evaluator.joins(pe_member_evaluated: [:user]).reorder('apellidos, nombre')
  end

  def pe_member_rels_is_evaluator_as_pe_rel(pe_rel)
    self.pe_member_rels_is_evaluator.where('pe_rel_id = ?', pe_rel.id)
  end

  def pe_member_rels_is_evaluator_as_pe_rel_ordered(pe_rel)
    self.pe_member_rels_is_evaluator.joins(pe_member_evaluated: [:user]).where('pe_rel_id = ?', pe_rel.id).reorder('apellidos, nombre')
  end

  def pe_member_rels_is_evaluator_as_rel_ordered_by_pe_area(pe_rel)
    self.pe_member_rels_is_evaluator.joins(pe_member_evaluated: [:pe_area]).where('pe_rel_id = ?', pe_rel.id).reorder('pe_areas.name')
  end

  def pe_member_rels_is_evaluated_ordered
    self.pe_member_rels_is_evaluated.joins(pe_member_evaluator: [:user]).reorder('apellidos, nombre')
  end

  def pe_member_rels_is_evaluated_ordered_rel
    self.pe_member_rels_is_evaluated.joins(:pe_rel, pe_member_evaluator: [:user]).reorder('pe_rels.position, pe_rels.rel, apellidos, nombre')
  end

  def pe_member_rels_is_evaluated_with_valid_evaluator_ordered
    self.pe_member_rels_is_evaluated.joins(pe_member_evaluator: [:user]).where('valid_evaluator = ?', true).reorder('apellidos, nombre')
  end

  def pe_member_rels_is_evaluated_ordered_by_pe_area
    self.pe_member_rels_is_evaluated.joins('join pe_members as pe_members_aux on pe_member_rels.pe_member_evaluator_id = pe_members_aux.id join pe_areas on pe_members_aux.pe_area_id = pe_areas.id').reorder('pe_areas.name')
  end

  def pe_member_evaluator_boss
    pe_member_rel_boss = self.pe_member_rels_is_evaluated.joins(:pe_rel).where('pe_rels.rel = ?', 1).first
    pe_member_rel_boss ? pe_member_rel_boss.pe_member_evaluator : nil
  end

  def pe_member_feedbacks_by_field(pe_feedback_field)
    self.pe_member_feedbacks.where('pe_feedback_field_id = ?', pe_feedback_field.id).first
  end

  def pe_member_accepted_feedbacks_by_field(pe_feedback_accepted_field)
    self.pe_member_accepted_feedbacks.where('pe_feedback_accepted_field_id = ?', pe_feedback_accepted_field.id).first
  end

#  def pe_assessment_evaluations(pe_evaluation)

#    self.pe_assessment_evaluations pe_evaluation

#  end

  def has_been_assessed_in_evaluation(pe_evaluation)

    has_been = false

    self.pe_member_rels_is_evaluated.each do |pe_member_rel|

      if pe_member_rel.pe_assessment_evaluation pe_evaluation
        has_been = true
        break
      end

    end

    return has_been

  end

  def pe_assessment_final_evaluation(pe_evaluation)

    self.pe_assessment_final_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

  end

  def pe_assessment_dimension(pe_dimension)
    self.pe_assessment_dimensions.where('pe_dimension_id = ?', pe_dimension.id).first
  end

  def set_pe_box

    pe_box = nil

    pe_assessment_dim_x = self.pe_assessment_dimension self.pe_process.dimension_x

    if self.pe_process.two_dimensions

      pe_assessment_dim_y = self.pe_assessment_dimension self.pe_process.dimension_y

      pe_box = self.pe_process.pe_box_2d(pe_assessment_dim_x.pe_dimension_group.id, pe_assessment_dim_y.pe_dimension_group.id) if pe_assessment_dim_x && pe_assessment_dim_x.pe_dimension_group && pe_assessment_dim_y && pe_assessment_dim_y.pe_dimension_group

    else

      pe_box = self.pe_process.pe_box_1d(pe_assessment_dim_x.pe_dimension_group.id) if pe_assessment_dim_x && pe_assessment_dim_x.pe_dimension_group


    end

    self.pe_box = pe_box

  end

  def pe_member_characteristic_by_id(id_c)

    self.pe_member_characteristics.where('characteristic_id = ?', id_c).first

  end

  def reverse_calibration
    if self.step_calibration?
      self.step_calibration = false
      self.step_calibration_date = nil
      self.pe_box_id = self.pe_box_before_cal_id
      self.pe_box_before_cal_id = nil
      self.calibration_comment = nil
    end
  end

  def pe_member_rel_shared_comments_by_pe_rel(pe_rel)
    self.pe_member_rel_shared_comments.where('pe_member_rels.pe_rel_id = ?', pe_rel.id).reorder('pe_member_rel_shared_comments.registered_at')
  end

  def has_pe_member_rel_shared_comments_to_display

    has_to_display = false

    self.pe_member_rels_is_evaluated.each do |pe_member_rel|

      if pe_member_rel.pe_rel.allows_shared_comments && pe_member_rel.pe_rel.display_shared_comments_in_feedback && self.pe_member_rel_shared_comments.where('pe_member_rels.pe_rel_id = ?', pe_member_rel.pe_rel.id).count > 0
        has_to_display = true
        break
      end

    end

    has_to_display

  end

  def pe_definition_by_user_validator(pe_evaluation, step_number, before_accept)
    self.pe_definition_by_user_validators.where('pe_evaluation_id = ? AND step_number = ? AND before_accept = ?', pe_evaluation.id, step_number, before_accept).first
  end

  def needs_definition_by_users_validation_before_accepted(pe_evaluation)
    pe_evaluation.pe_process.has_step_definition_by_users_validated && pe_evaluation.num_steps_definition_by_users_validated_before_accepted > 0 && self.pe_definition_by_user_validators.where('pe_evaluation_id = ? AND before_accept = ?', pe_evaluation.id, true).size > 0
  end

  def pe_assessment_group(pe_evaluation_group)
    self.pe_assessment_groups.where('pe_evaluation_group_id = ?', pe_evaluation_group.id).first
  end

  def pe_member_rels_is_evaluated_assign_total_count
    t = 0
    self.pe_process.pe_rels_assign_total_count.each do |pe_rel|
      t+= self.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).size
    end
    t
  end

=begin
  def validators

    validators = Array.new

    self.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
      validators.push pe_member_rel_is_evaluated.validator if pe_member_rel_is_evaluated.validator_id
    end

    return validators

  end
=end
end
