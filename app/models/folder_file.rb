# == Schema Information
#
# Table name: folder_files
#
#  id                    :integer          not null, primary key
#  nombre                :string(255)
#  crypted_name          :string(255)
#  folder_id             :integer
#  user_id               :integer
#  descargable           :boolean          default(TRUE)
#  extension             :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  descripcion           :text
#  size                  :integer
#  valor                 :float            default(0.0)
#  oculto                :boolean          default(FALSE)
#  folder_file_father_id :integer
#  is_last               :boolean          default(TRUE)
#  download_token        :string(255)
#

class FolderFile < ActiveRecord::Base

  belongs_to :folder
  belongs_to :user
  belongs_to :folder_file_father, :class_name => 'FolderFile'
  has_many :folder_file_comments, :dependent => :destroy
  has_many :folder_file_values, :dependent => :destroy
  has_many :log_folder_files, :dependent => :destroy

  after_create :set_crypted_name

  validates :nombre, presence: true
  validates_inclusion_of :extension, :in => %w(doc docx gif jpg jpeg pdf png ppt pptx txt xls xlsx zip mp4), :allow_nil => false

  attr_accessible :crypted_name, :descargable, :descripcion, :extension, :nombre, :oculto, :size, :folder_file_father_id, :is_last

  default_scope order('nombre ASC')

  def set_crypted_name

    self.crypted_name = self.id.to_s+'_'+SecureRandom.hex(5)
    self.save

  end

  def full_path(company)
    directory = company.directorio+'/'+company.codigo+'/mediateca/'

    return File.join(directory, self.crypted_name + '.' + self.extension)
  end

  def list_ancestor_ids
    ids = []
    current_folder_file = self
    while current_folder_file.folder_file_father
      father = current_folder_file.folder_file_father
      ids.push(father.id)
      current_folder_file = father
    end
    return ids
  end

  def comments_including_ancestors
    folder_file_ids = list_ancestor_ids
    folder_file_ids.push(self.id)
    return FolderFileComment.where(:folder_file_id => folder_file_ids)#.reorder(:created_at)
  end

  def values_including_ancestors
    folder_file_ids = list_ancestor_ids
    folder_file_ids.push(self.id)
    return FolderFileValue.where(:folder_file_id => folder_file_ids)#.reorder(:created_at)
  end

end
