# == Schema Information
#
# Table name: j_characteristic_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class JCharacteristicType < ActiveRecord::Base
  has_many :jj_characteristics

  attr_accessible :name, :position

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :position, presence: true,  numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def public_jj_characteristics
    self.jj_characteristics.where('public = ?', true)
  end


end
