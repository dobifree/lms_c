# == Schema Information
#
# Table name: he_ct_module_privilages
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  recorder               :boolean          default(FALSE)
#  validator              :boolean          default(FALSE)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  active                 :boolean          default(TRUE)
#

class HeCtModulePrivilage < ActiveRecord::Base
  belongs_to :user
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id

  has_many :sc_user_to_validates
  has_many :sc_user_to_registers
  has_many :sc_register_to_validates

  attr_accessible :recorder,
                  :validator,
                  :user_id,
                  :active

  validates :recorder, inclusion: { in: [true, false] }
  validates :validator, inclusion: { in: [true, false] }
  validates :active, inclusion: { in: [true, false] }
  validates :user_id, uniqueness: true

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validates_presence_of :deactivated_at, :unless => :active?
  validates_presence_of :deactivated_by_user_id, :unless => :active?

end
