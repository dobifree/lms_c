# == Schema Information
#
# Table name: brc_bonus_items
#
#  id              :integer          not null, primary key
#  brc_process_id  :integer
#  brc_member_id   :integer
#  brc_category_id :integer
#  p_a             :boolean          default(TRUE)
#  percentage      :float
#  amount          :decimal(20, 4)   default(0.0)
#  rejected        :boolean          default(FALSE)
#  val_comment     :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  secu_cohade_id  :integer
#  amount_c        :decimal(20, 4)   default(0.0)
#  validated       :boolean          default(FALSE)
#

class BrcBonusItem < ActiveRecord::Base
  belongs_to :brc_process
  belongs_to :brc_member
  belongs_to :brc_category
  belongs_to :secu_cohade

  attr_accessible :amount, :p_a, :percentage, :rejected, :val_comment, :brc_process_id, :brc_member_id, :brc_category_id,
                  :secu_cohade_id, :amount_c, :validated

end
