# == Schema Information
#
# Table name: sc_documentations
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  sc_period_id           :integer
#  doc_path               :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string(255)
#  description            :text
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  begin                  :date
#  deleted                :boolean          default(FALSE)
#  deleted_description    :text
#  registered_at          :datetime
#  registered_by_user_id  :integer
#

class ScDocumentation < ActiveRecord::Base
  belongs_to :user
  belongs_to :sc_period
  belongs_to :deactivated_by_user, :class_name => 'User'

  attr_accessible :doc_path,
                  :description,
                  :name,
                  :user_id,
                  :sc_period_id,
                  :deactivated_by_user_id,
                  :deactivated_at,
                  :active,
                  :begin,
                  :registered_by_user_id,
                  :registered_at

  validates :name, presence: true
end
