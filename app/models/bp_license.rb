# == Schema Information
#
# Table name: bp_licenses
#
#  id                           :integer          not null, primary key
#  bp_group_id                  :integer
#  bp_event_id                  :integer
#  name                         :string(255)
#  description                  :text
#  days                         :integer
#  money                        :decimal(12, 4)
#  currency                     :string(255)
#  antiquity                    :integer
#  days_limit                   :integer
#  days_limit_only_laborable    :boolean          default(TRUE)
#  days_since                   :integer
#  only_laborable_days          :boolean          default(TRUE)
#  period_times                 :integer
#  period_anniversary           :boolean          default(TRUE)
#  period_begin_1               :datetime
#  period_begin_2               :datetime
#  period_condition_begin       :boolean          default(TRUE)
#  period_condition_min_days    :integer
#  period_condition_min_percent :float
#  bp_season_id                 :integer
#  season_condition_begin       :boolean          default(TRUE)
#  season_condition_min_days    :integer
#  season_condition_min_percent :float
#  deactivated_at               :datetime
#  deactivated_by_admin_id      :integer
#  registered_at                :datetime
#  registered_by_admin_id       :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  since                        :datetime
#  until                        :datetime
#  days_only_laborable_day      :boolean          default(TRUE)
#  refundable                   :boolean          default(TRUE)
#  partially_taken              :boolean          default(FALSE)
#  min_by_request               :integer
#  max_by_request               :integer
#  whenever                     :boolean          default(TRUE)
#  days_until                   :integer
#  hours                        :integer
#  with_period                  :boolean          default(TRUE)
#  with_event                   :boolean          default(TRUE)
#  period_times_type            :integer          default(0)
#  available                    :boolean          default(TRUE)
#  prev_license_id              :integer
#  next_license_id              :integer
#  unavailable_at               :datetime
#  unavailable_by_admin_id      :integer
#  indef_contract_required      :boolean          default(FALSE)
#  need_approval                :boolean          default(TRUE)
#

class BpLicense < ActiveRecord::Base
  belongs_to :bp_group
  belongs_to :bp_event
  belongs_to :bp_season
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id

  has_many :lic_requests
  has_many :lic_events

  attr_accessible :bp_group_id,
                  :bp_event_id,
                  :name,
                  :description,
                  :days,
                  :only_laborable_days,
                  :money,
                  :currency,
                  :days_limit_only_laborable,
                  :whenever,
                  :days_since,
                  :days_until,
                  :partially_taken,
                  :min_by_request,
                  :max_by_request,
                  :antiquity,
                  :period_times,
                  :period_anniversary,
                  :period_begin_1,
                  :period_begin_2,
                  :period_condition_begin,
                  :period_condition_min_days,
                  :period_condition_min_percent,
                  :bp_season_id,
                  :season_condition_begin,
                  :season_condition_min_days,
                  :season_condition_min_percent,
                  :since,
                  :until,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id,
                  :with_period,
                  :hours,
                  :with_event,
                  :available,
                  :period_times_type,
                  :indef_contract_required,
                  :need_approval

  validates :bp_group_id, :presence => true
  validates :bp_event_id, :presence => true
  validates :name, :presence => true

  validates :hours, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :days, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :money, numericality: { greater_than_or_equal_to: 0 }, :if => :money?
  validates :money, :presence => true, :if => :currency?
  validates :currency, :presence => true, :if => :money?

  validates :need_approval, inclusion: { in: [true, false] }
  validates :only_laborable_days, inclusion: { in: [true, false] }
  validates :antiquity, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true

  validates :partially_taken, inclusion: { in: [true, false] }
  validates :min_by_request, numericality: { only_integer: true }, if: :partially_taken?
  validates :max_by_request, numericality: { only_integer: true }, if: :partially_taken?

  validates :whenever, inclusion: { in: [true, false] }
  validates :days_since, numericality: { only_integer: true}, if: :whenever?
  validates :days_until, numericality: { only_integer: true}, if: :whenever?
  validates :days_limit_only_laborable, inclusion: { in: [true, false] }

  validates :period_times, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, if: :with_period?
  validates :period_begin_1, :presence => true, :unless => (:period_anniversary? || !(:with_period?))
  validates :period_condition_begin, inclusion: { in: [true, false] }
  validates :period_condition_min_days, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :period_condition_min_percent, numericality: { greater_than_or_equal_to: 0 }, :allow_nil => true

  validates :season_condition_begin, inclusion: { in: [true, false] }
  validates :season_condition_min_days, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :if => :bp_season_id?
  validates :season_condition_min_percent, numericality: { greater_than_or_equal_to: 0 }, :if => :bp_season_id?

  validates :since, :presence => true

  validates :registered_at, :presence => true
  validates :registered_by_admin_id, :presence => true

  validate :any_bonus
  validate :periods_anniversary
  validate :season_id_nil
  validate :since_before_until
  validate :range_since_concordance_1
  validate :range_since_concordance_2
  validate :range_until_concordance_1


  def self.prev_ids(array, license_id)
    license = BpLicense.find(license_id)
    array << license
    if license.prev_license_id
      BpLicense.prev_ids(array, license.prev_license_id)
    end
    array.sort_by(&:registered_at)
  end

  def self.next_ids(array, license_id)
    license = BpLicense.find(license_id)
    array << license
    if license.next_license_id
      BpLicense.next_ids(array, license.next_license_id)
    end
    array.sort_by(&:registered_at)
  end

  def bring_all_version_ids
    array = []
    array << self
    array = BpLicense.prev_ids(array, prev_license_id) if prev_license_id
    array
  end

  def bring_active_versions_event
    array = []
    bp_event.bp_licenses.where(available: true).each do |lic|
      array << lic.id
    end
    array
  end

  def bring_all_versions_event
    array = []
    bp_event.bp_licenses.each do |lic|
      array << lic.id
    end
    array
  end

  def def_ids
    case period_times_type
      when 0 ; bring_all_version_ids
      when 1 ; [id]
      when 2 ; bring_active_versions_event
      when 3 ; bring_all_versions_event
      else ; nil
    end
  end

  def self.period_times_types
    [0,1,2,3]
  end

  def self.period_times_types_name
    ['Todas las versiones de este beneficio',
     'Esta versión del beneficio',
     'Versiones activas de evento',
     'Todas las versiones de evento']
  end

  def self.period_times_array
    [[BpLicense.period_times_types_name[1], BpLicense.period_times_types[1]],
     [BpLicense.period_times_types_name[0], BpLicense.period_times_types[0]],
     [BpLicense.period_times_types_name[2], BpLicense.period_times_types[2]],
     [BpLicense.period_times_types_name[3], BpLicense.period_times_types[3]]]
  end

  def in_process_instances
    lic_requests.where(active: true, status: [LicRequest.pending, LicRequest.approved, LicRequest.closed])
  end

  def self.by_group(group_id)
    where(bp_group_id: group_id)
  end

  def self.only_available
    where(available: true)
  end

  def satisfy?(begin_date, end_date, amount, anniversary, user_id, event_id, request_id)
    messages = []
    messages << 'No activo para fecha indicada' unless (active?(begin_date.to_datetime) || active?(end_date.to_datetime))
    messages << 'Cantidad no se encuentra dentro de rango' unless satisfy_partially_requested?(amount)
    messages << 'No puede solicitar más de lo disponible' unless satisfy_partially_total_amount?(event_id, amount, request_id)
    messages << 'No cumple con antigüedad' unless satisfy_antiquity?(anniversary, begin_date)
    messages << 'No cumple condiciones de temporada' unless satisfy_season_conditions(begin_date, end_date)
    messages << 'Cantidad solicitada no cumple condiciones de período' unless satisfy_periods_conditions(anniversary, user_id, begin_date, end_date, event_id, request_id)
    messages << 'No cumple condición de tipo de contrato' unless satisfy_indefined_contract_required(user_id, begin_date, end_date)
    messages
  end

  def satisfy_indefined_contract_required(user_id, begin_date, end_date)
    return true unless indef_contract_required
    user = User.find(user_id)
    user.secu_tiene_contrato_indefinido(begin_date.to_date)
  end

  def satisfy_partially_requested?(amount)
    return true unless partially_taken
    return true unless amount
    return true unless min_by_request || max_by_request
    amount =  amount.to_i
    return amount >= min_by_request if  min_by_request && !max_by_request
    return amount <= max_by_request if  !min_by_request && max_by_request
    (amount >= min_by_request && amount <= max_by_request)
  end

  def satisfy_season_conditions(begin_lics, end_lics)
    return true unless bp_season_id
    begin_lics = begin_lics.to_date
    end_lics = end_lics.to_date
    season_periods = []

    BpSeasonPeriod.all.each do |season_period|
      if season_period.until
        next unless season_period.since.to_date <= begin_lics && begin_lics <= season_period.until.to_date
      else
        next unless season_period.since.to_date <= begin_lics
      end
      season_periods << season_period
    end

    return false unless season_periods.size > 0

    periods_overlap = []
    season_periods.each do |season_period|
      periods_overlap += seasons_overlap_month_acc(season_period.begin,season_period.end, begin_lics, end_lics, season_period.bp_season_id)
    end

    if periods_overlap.size > 0
      chosen_period = evaluate_season_conditions(periods_overlap, begin_lics, end_lics)
    else
      chosen_period = false
    end

    unless chosen_period
      return false
    end

    chosen_period[2].to_i == bp_season_id.to_i
  end

  def evaluate_season_conditions(periods, begin_lics, end_lics)
    begin_lics = begin_lics.to_date
    end_lics = end_lics.to_date

    periods_ok = []
    periods.each do |period|
      next unless period[0].to_date < end_lics && period[1].to_date >= begin_lics
      satisfy_1 = satisfy_season_min_days?(period[0], period[1], begin_lics, end_lics)
      satisfy_2 = satisfy_season_min_percent?(period[0], period[1], begin_lics, end_lics)
      periods_ok << period if (satisfy_1 && satisfy_2)
    end

    if periods_ok.size > 1
      return get_default_season(periods_ok, begin_lics, end_lics)
    else
      if periods_ok.size > 0
        return periods_ok[0]
      else
        return nil
      end
    end
  end

  def seasons_overlap_month_acc(begin_period, end_period, date_begin, date_end, season_id)
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    date_begin = date_begin.to_date
    date_end = date_end.to_date
    periods = []

    begin_period = begin_period.change(year: date_begin.year)
    loop do
      break if begin_period <= date_begin
      begin_period -= 1.year
    end
    end_period = end_period.change(year: date_begin.year)
    end_period += 1.year if end_period.year < begin_period.year

    loop do
      periods << [begin_period, end_period, season_id] if begin_period < date_end && end_period >= date_begin
      begin_period += 1.year
      end_period += 1.year
      break if begin_period > date_end
    end

    return periods
  end

  def satisfy_season_min_days?(begin_period, end_period, begin_lics, end_lics)
    return true unless season_condition_min_days
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    begin_lics = begin_lics.to_date
    end_lics = end_lics.to_date

    max_begin_date = begin_period > begin_lics ? begin_period : begin_lics
    min_end_date = end_period < end_lics ? end_period : end_lics
    vacs_days = (min_end_date - max_begin_date).to_i + 1

    vacs_days >= season_condition_min_days
  end

  def satisfy_season_min_percent?(begin_period, end_period, begin_lics, end_lics)
    return true unless season_condition_min_percent
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    begin_lics = begin_lics.to_date
    end_lics = end_lics.to_date

    max_begin_date = (begin_period > begin_lics ? begin_period : begin_lics)
    min_end_date = (end_period < end_lics ? end_period : end_lics)

    period_days = (min_end_date - max_begin_date).to_i + 1
    vacs_days = (end_period - begin_period).to_i + 1

    return ((period_days/vacs_days.to_f)*100) >= season_condition_min_percent
  end

  def satisfy_partially_taken(anniversary, user_id, begin_lics, end_lics)
    days = 0
    hours = 0
    periods_to_evaluate_overlap = define_period_to_evaluate(anniversary)
    periods_overlap = []
    periods_to_evaluate_overlap.each do |period|
      periods_overlap += periods_overlap_month_acc(period[0],period[1], begin_lics, end_lics)
    end

    if periods_overlap.size > 0
      chosen_period = evaluate_periods_conditions(periods_overlap, begin_lics, end_lics)
    else
      chosen_period = false
    end

    unless chosen_period
      return false
    end

    usages = get_usages(chosen_period[0], chosen_period[1], user_id)

    usages.each do |usage|
      days += usage.days if usage.days
      hours += usage.hours if usage.hours
    end

    return evaluation
  end

  def event_in(anniversary, user_id, begin_lics)
    if with_period
      end_lics = begin_lics.to_date
      periods_to_evaluate_overlap = define_period_to_evaluate(anniversary)
      periods_overlap = []
      periods_to_evaluate_overlap.each do |period|
        periods_overlap += periods_overlap_month_acc(period[0],period[1], begin_lics, end_lics)
      end

      if periods_overlap.size > 0
        chosen_period = evaluate_periods_conditions(periods_overlap, begin_lics, end_lics)
      else
        chosen_period = false
      end

      unless chosen_period
        return false
      end

      usages = get_usages(chosen_period[0], chosen_period[1], user_id)
    else
      usages = LicRequest.taken_by(user_id, def_ids)
    end

    if usages.size > 0
      usages.first.lic_event
    else
      LicEvent.new(date_event: begin_lics, bp_event_id: bp_event_id)
    end
  end

  def event_in1(anniversary, user_id, begin_lics)
    if with_period
      end_lics = begin_lics.to_date
      periods_to_evaluate_overlap = define_period_to_evaluate(anniversary)
      periods_overlap = []
      periods_to_evaluate_overlap.each do |period|
        periods_overlap += periods_overlap_month_acc(period[0],period[1], begin_lics, end_lics)
      end

      if periods_overlap.size > 0
        chosen_period = evaluate_periods_conditions(periods_overlap, begin_lics, end_lics)
      else
        chosen_period = false
      end

      unless chosen_period
        return false
      end

      usages = get_usages1(chosen_period[0], chosen_period[1], user_id)
    else
      usages = LicRequest.all_taken_by(user_id, def_ids)
    end

    if usages.size > 0
      usages.first.lic_event
    else
      LicEvent.new(date_event: begin_lics, bp_event_id: bp_event_id)
    end
  end

  def satisfy_periods_conditions(anniversary, user_id, begin_lics, end_lics, event_id, request_id)
    if with_period
      periods_to_evaluate_overlap = define_period_to_evaluate(anniversary)
      periods_overlap = []
      periods_to_evaluate_overlap.each do |period|
        periods_overlap += periods_overlap_month_acc(period[0],period[1], begin_lics, end_lics)
      end

      if periods_overlap.size > 0
        chosen_period = evaluate_periods_conditions(periods_overlap, begin_lics, end_lics)
      else
        chosen_period = false
      end

      unless chosen_period
        return false
      end

      evaluation = evaluate_taken_times(chosen_period[0], chosen_period[1], user_id)

      return evaluation
    else
      return true unless period_times
      ids = def_ids
      usages = LicRequest.taken_by(user_id, ids)
      # usages = LicEvent.taken_by(user_id, def_ids)
      return usages.size < period_times
    end
  end

  def self.get_period(event_id)
    event = LicEvent.find(event_id)
    license = event.bp_event.bp_licenses.first
    request = event.lic_requests.first
    anniversary = '2018-09-09'.to_date
    if license.with_period && request
      periods_to_evaluate_overlap = license.define_period_to_evaluate(anniversary)
      periods_overlap = []
      periods_to_evaluate_overlap.each do |period|
        periods_overlap += license.periods_overlap_month_acc(period[0],period[1], request.begin, request.end)
      end

      if periods_overlap.size > 0
        chosen_period = license.evaluate_periods_conditions(periods_overlap, request.begin, request.end)
      else
        chosen_period = false
      end

      return chosen_period
    else
      ['De por vida']
    end
  end

  def get_request_period(request_begin, request_end, user_id, user_anniversary)
    anniversary = user_anniversary.to_date
    if with_period
      periods_to_evaluate_overlap = define_period_to_evaluate(anniversary)
      periods_overlap = []
      periods_to_evaluate_overlap.each do |period|
        periods_overlap += periods_overlap_month_acc(period[0],period[1], request_begin, request_end)
      end

      if periods_overlap.size > 0
        chosen_period = evaluate_periods_conditions(periods_overlap, request_begin, request_end)
      else
        chosen_period = false
      end

      chosen_period
    else
      ['De por vida']
    end
  end

  def periods_overlap_month_acc(begin_period, end_period, date_begin, date_end)
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    date_begin = date_begin.to_date
    date_end = date_end.to_date
    periods = []

    begin_period = begin_period.change(year: date_begin.year)
    loop do
      break if begin_period <= date_begin
      begin_period -= 1.year
    end
    end_period = end_period.change(year: date_begin.year)
    end_period += 1.year if end_period.year < begin_period.year

    loop do
      periods << [begin_period, end_period] if begin_period < date_end && end_period >= date_begin
      begin_period += 1.year
      end_period += 1.year
      break if begin_period > date_end
    end

    return periods
  end

  def define_period_to_evaluate(anniversary)
    anniversary = anniversary ? anniversary.to_date : nil
    periods = []

    if self.period_anniversary
      if anniversary
        period_begin = anniversary
        period_end = anniversary - 1.day
        periods << [period_begin, period_end]
      end
      return periods
    end

    if period_begin_2
      period_begin = period_begin_1
      period_end = period_begin_2 - 1.day
      periods << [period_begin, period_end]

      period_begin = period_begin_2
      period_end = period_begin_1 - 1.day
      periods << [period_begin, period_end]

      return periods
    end

    period_begin = period_begin_1
    period_end = period_begin_1 - 1.day
    periods << [period_begin, period_end]
    return periods
  end

  def evaluate_periods_conditions(periods, begin_lics, end_lics)
    begin_lics = begin_lics.to_date
    end_lics = end_lics.to_date

    periods_ok = []
    periods.each do |period|
      next unless period[0].to_date < end_lics && period[1].to_date >= begin_lics
      satisfy_1 = satisfy_period_condition_min_days?(period[0], period[1], begin_lics, end_lics)
      satisfy_2 = satisfy_period_condition_min_percent?(period[0], period[1], begin_lics, end_lics)
      periods_ok << period if (satisfy_1 && satisfy_2)
    end

    if periods_ok.size > 1
      return get_default_period(periods_ok, begin_lics, end_lics)
    else
      if periods_ok.size > 0
        return periods_ok[0]
      else
        return nil
      end
    end
  end

  def satisfy_period_condition_min_days?(begin_period, end_period, begin_lics, end_lics)
    return true unless period_condition_min_days
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    begin_lics = begin_lics.to_date
    end_lics = end_lics.to_date

    max_begin_date = begin_period > begin_lics ? begin_period : begin_lics
    min_end_date = end_period < end_lics ? end_period : end_lics

    vacs_days = (min_end_date - max_begin_date).to_i + 1
    vacs_days >= period_condition_min_days
  end

  def satisfy_period_condition_min_percent?(begin_period, end_period, begin_lics, end_lics)
    return true unless period_condition_min_percent
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    begin_lics = begin_lics.to_date
    end_lics = end_lics.to_date

    max_begin_date = (begin_period > begin_lics ? begin_period : begin_lics)
    min_end_date = (end_period < end_lics ? end_period : end_lics)

    period_days = (min_end_date - max_begin_date).to_i + 1
    vacs_days = (end_lics - begin_lics).to_i + 1

    (((period_days/vacs_days.to_f)*100) >= period_condition_min_percent)
  end

  def get_default_period(periods_ok, begin_lics, end_lics)
    date_to_evaluate = period_condition_begin ? begin_lics : end_lics
    date_to_evaluate = date_to_evaluate.to_date
    periods_ok.each do | period |
      return period if date_to_evaluate >= period[0].to_date && date_to_evaluate <= period[1].to_date
    end
    nil
  end

  def evaluate_taken_times(begin_period, end_period, user_id)
    usages = get_usages(begin_period, end_period, user_id)
    # event_usages = []
    # usages.each {|us| event_usages << us.lic_event_id unless event_usages.include?(us.lic_event_id) }
    usages.size < period_times
  end

  def get_usages(begin_period, end_period, user_id)
    ids = def_ids
    usages = LicRequest.taken_by(user_id, ids)
    periods = [[begin_period.to_date, end_period.to_date]]
    all_usages = []

    usages.each do |usage|
      # next unless (usage.user_id != user_id.to_i && usage.lic_event.user_id != user_id.to_i)
      evaluation = evaluate_periods_conditions(periods, usage.begin, usage.end)
      next unless evaluation && evaluation.size > 0
      all_usages << usage
    end
    all_usages
  end

  def get_usages1(begin_period, end_period, user_id)
    ids = def_ids
    usages = LicRequest.all_taken_by(user_id, ids)
    periods = [[begin_period.to_date, end_period.to_date]]
    all_usages = []

    usages.each do |usage|
      # next unless (usage.user_id != user_id.to_i && usage.lic_event.user_id != user_id.to_i)
      evaluation = evaluate_periods_conditions(periods, usage.begin, usage.end)
      next unless evaluation && evaluation.size > 0
      all_usages << usage
    end
    all_usages
  end

  def satisfy_partially_total_amount?(event_id, amount, request_id)
    return true unless days || hours
    event = LicEvent.find(event_id) if event_id
    request = request_id && request_id.size > 0 ? LicRequest.find(request_id) : nil

    amount = amount.to_i
    amount -= request.days if request && request.in_process? && request.days
    amount -= request.hours if request && request.in_process? && request.hours

    if event
      return amount <= (days - event.sum_days_for(id)) if days
      return amount <= (hours - event.sum_hours_for(id)) if hours
    else
      return amount <= days if days
      return amount <= hours if hours
    end
    false
  end

  def satisfy_season(begin_date, end_date)
    return true unless bp_season_id
    period = period_season_lic(begin_date, end_date)
    return true if period && period.size > 0
    false
  end

  def period_season_lic(begin_date, end_date)
    begin_date = begin_date.to_date
    end_date = end_date.to_date

    return unless bp_season_id
    return unless bp_season.active?(begin_date)
    period = bp_season.active_period(begin_date)
    return unless period

    begin_period = Date.new().change(year: begin_date.year, month: period.begin.month, day: period.begin.day)
    end_period_year = period.begin.month < period.end.month ? begin_date.year : (begin_date.year+1.year)
    end_period = Date.new().change(year: end_period_year, month: period.end.month, day: period.end.day)

    first_period = evaluate_season_period(begin_date, end_date, begin_period, end_period)
    second_period = evaluate_season_period(begin_date, end_date, end_period-1.year, begin_period-1.year)

    period_1 = []

    period_1[0] = begin_period if first_period
    period_1[1] = end_period if first_period

    period_1[0] = begin_period-1.year if second_period
    period_1[1] = end_period-1.year if second_period

    if first_period && second_period
      if period_condition_begin && begin_date > begin_period && begin_date < end_period
        period_1[0] = begin_period
        period_1[1] = end_period
      end
      if period_condition_begin && begin_date > begin_period-1.year && begin_date < end_period-1.year
        period_1[0] = begin_period-1.year
        period_1[1] = end_period-1.year
      end
      if !period_condition_begin && end_date > begin_period && end_date < end_period
        period_1[0] = begin_period
        period_1[1] = end_period
      end
      if !period_condition_begin && end_date > begin_period-1.year && end_date < end_period-1.year
        period_1[0] = begin_period-1.year
        period_1[1] = end_period-1.year
      end
    end
    period_1
  end

  def evaluate_season_period(begin_date, end_date, period_begin, period_end)
    fulfill_requirement = true
    first_date = period_begin > begin_date ? period_begin : begin_date
    final_date = period_end > end_date ? end_date : period_end

    if season_condition_min_days
      fulfill_requirement = (final_date - first_date).to_i >= season_condition_min_days
    end

    if period_condition_min_percent
      fulfill_requirement = (final_date - first_date)/(period_end - period_begin) >= period_condition_min_percent
    end

    fulfill_requirement
  end


  def satisfy_antiquity?(anniversary, begin_date)
    return true unless antiquity
    begin_date = begin_date.to_date
    anniversary =  anniversary.to_date

    months = (begin_date.year - anniversary.year)*12
    months += begin_date.month - anniversary.month
    months += 1 if (begin_date.month == anniversary.month && begin_date.days > anniversary.days)

    (antiquity <= months)
  end

  def show_licenses(now, user_id)
    group = BpGroup.user_active_group(now, user_id)
    return unless group
    group.bp_licenses
  end

  def active_form
    bp_event.active_form
  end

  def active?(now)
    now = now.to_datetime
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  def period_condition_min_percent_formatted
    period_condition_min_percent.to_s + '%' if period_condition_min_percent
  end

  def season_condition_min_percent_formatted
    season_condition_min_percent.to_s + '%' if season_condition_min_percent
  end

  private

  def any_bonus
    return unless !currency && !days && !money
    errors.add(:bonus_days, 'debe escoger por lo menos un beneficio')
  end

  def periods_anniversary
    return unless period_anniversary? && (period_begin_1 || period_begin_2)
    errors.add(:period_anniversary, 'no admite períodos')
  end

  def season_id_nil
    return unless season_condition_min_days || season_condition_min_percent
    return if bp_season_id
    errors.add(:bp_season_id, 'no puede ser vacío si se han escrito condiciones para temporada')
  end

  def since_before_until
    return unless self.until && since && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end

  def range_since_concordance_1
    return unless bp_group_id
    return unless since
    return unless bp_group.since && since < bp_group.since
    errors.add(:since, 'debe ser mayor a "desde" del grupo')
  end

  def range_since_concordance_2
    return unless bp_group_id
    return unless since
    return unless bp_group.since && bp_group.until && since > bp_group.until
    errors.add(:since, 'debe ser menor a "hasta" del grupo')
  end

  def range_until_concordance_1
    return unless bp_group_id
    return unless self.until && bp_group.since && bp_group.until && self.until > bp_group.until
    errors.add(:until, 'debe ser menor a "hasta" del grupo')
  end
end
