# == Schema Information
#
# Table name: pe_definition_by_user_validators
#
#  id                 :integer          not null, primary key
#  pe_member_id       :integer
#  pe_evaluation_id   :integer
#  step_number        :integer
#  before_accept      :boolean
#  pe_process_id      :integer
#  validator_id       :integer
#  checked            :boolean          default(FALSE)
#  validated          :boolean          default(FALSE)
#  validation_comment :text
#  validated_at       :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class PeDefinitionByUserValidator < ActiveRecord::Base

  belongs_to :pe_member
  belongs_to :pe_evaluation
  belongs_to :pe_process
  belongs_to :validator, class_name: 'User'

  attr_accessible :before_accept, :checked, :step_number, :validated, :validated_at, :validation_comment, :validator_id

  def after_confirmation
    !self.before_accept
  end

end
