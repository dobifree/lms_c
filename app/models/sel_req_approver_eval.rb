# == Schema Information
#
# Table name: sel_req_approver_evals
#
#  id                  :integer          not null, primary key
#  sel_req_approver_id :integer
#  evaluation          :boolean
#  comment             :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  registered_at       :datetime
#

class SelReqApproverEval < ActiveRecord::Base
  belongs_to :sel_req_approver
  attr_accessible :comment, :evaluation
end
