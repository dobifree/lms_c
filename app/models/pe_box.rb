# == Schema Information
#
# Table name: pe_boxes
#
#  id                :integer          not null, primary key
#  position          :integer
#  short_name        :string(255)
#  name              :string(255)
#  description       :text
#  text_color        :string(255)
#  bg_color          :string(255)
#  soft_text_color   :string(255)
#  soft_bg_color     :string(255)
#  pe_process_id     :integer
#  pe_dim_group_x_id :integer
#  pe_dim_group_y_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PeBox < ActiveRecord::Base

  belongs_to :pe_process

  belongs_to :pe_dimension_group_x, class_name: 'PeDimensionGroup', foreign_key: :pe_dim_group_x_id
  belongs_to :pe_dimension_group_y, class_name: 'PeDimensionGroup', foreign_key: :pe_dim_group_y_id

  attr_accessible :bg_color, :description, :name, :pe_dim_group_x_id, :pe_dim_group_y_id, :position,
                  :short_name, :soft_bg_color, :soft_text_color, :text_color, :pe_process_id

  before_save { self.short_name = nil if self.short_name.blank? }

  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :pe_dim_group_x_id, presence: true
  validates :name, presence: true
  validates :text_color, presence: true
  validates :bg_color, presence: true
  validates :soft_text_color, presence: true
  validates :soft_bg_color, presence: true


  default_scope order('position, short_name, name')

end
