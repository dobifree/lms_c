# == Schema Information
#
# Table name: brg_bonus
#
#  id                     :integer          not null, primary key
#  prev_bonus_id          :integer
#  brg_process_id         :integer
#  brg_group_user_id      :integer
#  actual_value           :decimal(20, 5)
#  registered_value       :decimal(20, 5)
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  registered_description :text
#  paid                   :boolean
#  paid_at                :datetime
#  paid_by_user_id        :integer
#  paid_description       :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  brg_lvl_percentage_id  :integer
#  ready_to_go            :boolean          default(FALSE)
#  ready_to_go_at         :datetime
#  income_times           :decimal(12, 4)
#  bonus_target           :decimal(20, 5)   default(0.0)
#  first_value            :decimal(20, 5)   default(0.0)
#

class BrgBonus < ActiveRecord::Base
  belongs_to :prev_bonus, class_name: 'BrgBonus', foreign_key: :prev_bonus_id
  belongs_to :brg_lvl_percentage
  belongs_to :brg_process
  belongs_to :brg_group_user
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  has_many :brg_bonus_percentages

  attr_accessible :active,
                  :brg_process_id,
                  :brg_group_user_id,
                  :brg_lvl_percentage_id,
                  :actual_value,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :paid,
                  :paid_at,
                  :paid_by_user_id,
                  :paid_description,
                  :prev_bonus_id,
                  :ready_to_go,
                  :ready_to_go_at,
                  :registered_at,
                  :registered_by_user_id,
                  :registered_description,
                  :registered_value,
                  :income_times, :bonus_target

  validates :actual_value, presence: true
  validates :brg_process_id, presence: true
  validates :brg_group_user_id, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  before_save :set_income_times

  def last_year_bonus
    prev_process = BrgProcess.where('reference_year = 2017 and alias like "%privado%"').first
    return nil unless prev_process
    prev_process.brg_bonuses.active.of_user(brg_group_user.user_id).first
  end

  def self.gimme_first_one(this_bonus)
    if this_bonus.prev_bonus_id
      BrgBonus.gimme_first_one (this_bonus.prev_bonus)
    else
      return this_bonus
    end
  end

  def am_i_manager(user_id)
    all_bonuses = BrgBonus.historical_info([], id)
    all_bonuses.each do |this_bonus|
      this_bonus.next_managers_1.each do |manager|
        return true if manager.user_id == user_id.to_i
      end
    end
    false
  end

  def am_i_manager_1(user_id)
    group = brg_group_user.brg_group
    group_managers = BrgManager.managers_of_group(group.id)
    group_managers.each do |manager|
      return true if manager_managers_is_there(manager, user_id)
    end
    return false
  end

  def manager_managers_is_there(manager, user_id)
    return true if manager.user_id.to_i == user_id.to_i
    manager_managers = BrgManager.managers_of_manager(manager.user_id, self.brg_process_id)
    if manager_managers.size > 0
      manager_managers.each do |manager_1|
        return manager_managers_is_there(manager_1, user_id)
      end
    else
      return manager.user_id.to_i == user_id.to_i
    end
  end

  def show_me_income_times_target
    user = brg_group_user.user
    lvl = brg_process.brg_lvl_percentages.by_user(user.id)
    return nil unless lvl
    lvl = lvl.first
    return nil unless lvl
    lvl.income_times_target
  end

  def show_me_income_times_target_for(this_value)
    lvl = BrgLvlPercentage.gimme_by_value(this_value, brg_process_id)
    return nil unless lvl
    lvl = lvl.first
    return nil unless lvl
    lvl.income_times_target
  end

  def user_id; brg_group_user.user_id end

  def show_me_sueldo_base(cohade_id, liq_process_id)
    return 0 unless cohade_id
    return 0 unless liq_process_id
    liq_item = LiqItem.where(secu_cohade_id: cohade_id, liq_process_id: liq_process_id, user_id: brg_group_user.user_id).first
    return 0 unless liq_item
    liq_item.monto_o
  end

  def original_value(user_id)
    return prev_bonus.actual_value if registered_by_user_id == user_id.to_i && prev_bonus
    actual_value
  end

  def paid_colour
    case paid
      when true ; 2
      when false ; 5
      else ; 0
    end
  end

  def paid_message
    case paid
      when true ; 'Pagado'
      when false ; 'No pagado'
      else ; 'Pendiente'
    end
  end

  def paid_action_message
    case paid
      when true ; 'Pagado'
      when false ; 'No pagar'
      else ; 'Pagar'
    end
  end

  def paid_action_colour
    case paid
      when true ; '2'
      when false ; '5'
      else ; '2'
    end
  end

  def self.all_pending
    active.joins([:brg_group_user => :user] , :brg_process).reorder('apellidos asc, nombre asc, due_date asc')
    #.where('paid is null')
  end

  def self.payable; where(paid: [true, nil]) end

  def self.this_company_id(company_id)
    company = Characteristic.find(5)
    users_id = joins([:brg_group_user => :user]).pluck('users.id')
    good_ones = []
    users_id.each do |user_id|
      user_val = User.find(user_id).characteristic_value(company)
      next unless company_id.to_i == user_val.to_i
      good_ones << user_id
    end
    joins([brg_group_user: :user]).where(brg_group_users: {user_id: good_ones}).readonly(false)
  end

  def self.active_processes
    joins(:brg_process).where(brg_processes: { active: true })
  end

  def self.open_processes
    joins(:brg_process).where(brg_processes: { status: BrgProcess.in_process })
  end

  def next_managers
    bonus = prev_v_with_manager
    if bonus.prev_bonus_id
      BrgManager.managers_of_manager(bonus.registered_by_user_id, brg_process_id)
    else
      BrgManager.managers_of_group(bonus.brg_group_user.brg_group_id)
    end
  end

  def next_managers_1
    if prev_bonus_id
      BrgManager.managers_of_manager(registered_by_user_id, brg_process_id)
    else
      BrgManager.managers_of_group(brg_group_user.brg_group_id)
    end
  end

  def prev_v_with_manager
    history = BrgBonus.historical_info([], id).sort_by(&:registered_at).reverse
    history.each do |bonus|
      next unless bonus.ready_to_go
      return bonus if BrgManager.active.where(user_id: bonus.registered_by_user_id).first && bonus.prev_bonus_id
    end
    history.last
  end

  def pending_manager?
    next_managers.size > 0
  end

  def last_version_regis_by(this_user_id)
    if registered_by_user_id.to_i == this_user_id.to_i
      self
    else
      if prev_bonus_id
        prev_bonus.last_version_regis_by(this_user_id)
      else
       return  nil
      end
    end
  end

  def my_turn_manager?(this_user_id)
    next_managers.each do |manager|
      return true if  manager.user_id.to_s == this_user_id.to_s
    end
    false
  end

  def self.historical_info(array, bonus_id)
    bonus = BrgBonus.find(bonus_id)
    array << bonus
    if bonus.prev_bonus_id
      BrgBonus.historical_info(array, bonus.prev_bonus_id)
    end
    array.sort_by(&:registered_at)
  end
  
  def all_managers_assigned
    group = brg_group_user.brg_group
    managers = BrgManager.managers_of_group(group.id)
    managers << BrgManager.managers_till_end(managers)
  end

  def self.waiting_manage_by(user_id)
  end

  def already?(user_id)
    historical_bonus = BrgBonus.historical_info([],id)
    historical_bonus.each do |bonus|
      return true if bonus.registered_by_user_id == user_id && bonus.ready_to_go
    end
    false
  end

  def self.active
    where(active: true)
  end

  def self.regis_by(id)
    where(registered_by_user_id: id)
  end

  def self.process(id)
    where(brg_process_id: id)
  end

  def self.group_user(id)
    where(brg_group_user_id: id)
  end

  def self.this_group(id)
    joins([brg_group_user: :brg_group]).where(brg_group_users: {brg_group_id: id}).readonly(false)
  end

  def self.of_user(id)
    joins([brg_group_user: :user]).where(brg_group_users: {user_id: id}).readonly(false)
  end

  def self.user_order
  joins([:brg_group_user => :user] , :brg_process).reorder('apellidos asc, nombre asc, due_date asc')
  end

  def self.group_user_order
    joins([:brg_group_user => [:user, :brg_group]] , :brg_process).reorder('brg_groups.name asc, apellidos asc, nombre asc, due_date asc')
  end

  private

  def set_income_times
    actual = actual_value
    return unless actual && actual != 0

    liq_process_id =  LiqProcess.where(year: brg_process.reference_year, month: 12).pluck(:id).first
    cohade_subase_id = SecuCohade.where(name: 'SUBASE').pluck(:id).first

    sueldo_base = show_me_sueldo_base(cohade_subase_id, liq_process_id)
    return unless sueldo_base && sueldo_base != 0

    income_times = actual/sueldo_base
    return unless income_times

    self.income_times = income_times
  end

end
