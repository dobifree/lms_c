# == Schema Information
#
# Table name: sel_attendants
#
#  id                    :integer          not null, primary key
#  sel_step_candidate_id :integer
#  user_id               :integer
#  role                  :boolean
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  automatic_assign      :boolean          default(FALSE)
#

class SelAttendant < ActiveRecord::Base
  belongs_to :sel_step_candidate
  belongs_to :user
  attr_accessible :role, :sel_step_candidate_id, :user_id

  validates :sel_step_candidate_id, uniqueness: {scope: :user_id}
  validates :sel_step_candidate_id, uniqueness: {scope: :role}, if: :es_responsable?

  def self.alternatives_role
    [:responsable, :participante]
  end

  def es_responsable?
    :role == 0
  end
end
