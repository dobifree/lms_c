# == Schema Information
#
# Table name: folder_file_comments
#
#  id             :integer          not null, primary key
#  folder_file_id :integer
#  user_id        :integer
#  fecha          :datetime
#  comentario     :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class FolderFileComment < ActiveRecord::Base

  belongs_to :folder_file
  belongs_to :user

  attr_accessible :comentario, :fecha

  validates :comentario, presence: true

end
