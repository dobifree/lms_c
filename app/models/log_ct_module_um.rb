# == Schema Information
#
# Table name: log_ct_module_ums
#
#  id                      :integer          not null, primary key
#  task                    :string(255)
#  performed_at            :datetime
#  user_id                 :integer
#  description             :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  performed_by_manager_id :integer
#  performed_by_admin_id   :integer
#

class LogCtModuleUm < ActiveRecord::Base

  belongs_to :user

  belongs_to :performed_by_manager, class_name: 'User'
  belongs_to :performed_by_admin, class_name: 'Admin'

  attr_accessible :description, :performed_at, :task

end
