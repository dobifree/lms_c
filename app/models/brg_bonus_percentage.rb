# == Schema Information
#
# Table name: brg_bonus_percentages
#
#  id                           :integer          not null, primary key
#  brg_bonus_id                 :integer
#  brg_percentage_conversion_id :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

class BrgBonusPercentage < ActiveRecord::Base
  belongs_to :brg_bonus
  belongs_to :brg_percentage_conversion

  attr_accessible :brg_bonus_id,
                  :brg_percentage_conversion_id

  validates :brg_bonus_id, presence: true
  validates :brg_percentage_conversion_id, presence: true

  def self.exists_for(bonus_id, pc_id)
    return nil unless bonus_id || pc_id
    return BrgBonusPercentage.by_bonus_id(bonus_id).first if bonus_id && !pc_id
    return BrgBonusPercentage.by_pc_id(pc_id).first if !bonus_id && pc_id
    BrgBonusPercentage.by_pc_id(pc_id).by_bonus_id(bonus_id).first
  end

  def self.by_bonus_id(bonus_id)
    where(brg_bonus_id: bonus_id)
  end

  def self.by_pc_id(pc_id)
    where(brg_percentage_conversion_id: pc_id)
  end
end
