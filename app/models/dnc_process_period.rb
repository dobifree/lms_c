# == Schema Information
#
# Table name: dnc_process_periods
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  dnc_process_id :integer
#  from_date      :date
#  to_date        :date
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class DncProcessPeriod < ActiveRecord::Base

  belongs_to :dnc_process

  attr_accessible :from_date, :name, :to_date

  validates :name, presence: true

  default_scope order('from_date, name')

end
