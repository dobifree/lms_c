# == Schema Information
#
# Table name: tutorials
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  description  :text
#  active       :boolean
#  crypted_name :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Tutorial < ActiveRecord::Base
  attr_accessible :active, :description, :name

  validates :name, :presence => true

  def visualization_path(company)
    if self.crypted_name
      return '/storage/' + company.codigo + '/tutorials/'+ self.crypted_name+'/'
    else
      return nil
    end
  end

end
