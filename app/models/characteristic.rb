# == Schema Information
#
# Table name: characteristics
#
#  id                             :integer          not null, primary key
#  nombre                         :string(255)
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  publica                        :boolean          default(FALSE)
#  mini_ficha                     :boolean          default(FALSE)
#  orden_mini_ficha               :integer
#  planning_process               :boolean
#  orden_planning_process         :integer
#  porcentaje_participante_sence  :boolean          default(FALSE)
#  basic_report                   :boolean          default(FALSE)
#  orden_basic_report             :integer
#  hr_process_assessment          :boolean
#  orden_hr_process_assessment    :integer
#  position                       :integer
#  filter_assessment_report       :boolean          default(FALSE)
#  order_filter_assessment_report :integer
#  characteristic_type_id         :integer
#  pe_rep_consolidate_results     :boolean
#  data_type_id                   :integer          default(0)
#  updated_by_manager             :boolean          default(FALSE)
#  updated_by_user                :boolean          default(FALSE)
#  boss_characteristic_id         :integer
#  register_characteristic_id     :integer
#  parent_characteristic_id       :integer
#  register_position              :integer
#  restricted_to_managers         :boolean          default(FALSE)
#  has_history                    :boolean          default(TRUE)
#  is_company                     :boolean          default(FALSE)
#  encrypted                      :boolean          default(FALSE)
#  subase                         :boolean          default(FALSE)
#  subaso                         :boolean          default(FALSE)
#  query_by_boss                  :boolean          default(TRUE)
#  query_by_user                  :boolean          default(TRUE)
#  query_by_others                :boolean          default(TRUE)
#  currency_subaso                :boolean          default(FALSE)
#  from_date_work                 :boolean          default(FALSE)
#  from_date_progresivas          :boolean          default(FALSE)
#  from_date_r_progresivas        :boolean          default(FALSE)
#  rol_security                   :boolean
#  has_children                   :boolean
#  required_new                   :boolean
#  characteristic_value_id        :integer
#  char_value_selected            :boolean          default(TRUE)
#  contrato_plazo_secu            :boolean          default(FALSE)
#  tipo_contrato_secu             :boolean          default(FALSE)
#  required_new_position          :integer
#  required_new_c                 :boolean          default(FALSE)
#

class Characteristic  < ActiveRecord::Base

  belongs_to :characteristic_type
  belongs_to :boss_characteristic, class_name: 'Characteristic'
  belongs_to :register_characteristic, class_name: 'Characteristic'
  belongs_to :parent_characteristic, class_name: 'Characteristic'

  belongs_to :characteristic_value

  has_many :user_characteristics
  has_many :users, through: :user_characteristics
  has_many :folder_members
  has_many :pe_characteristics
  has_many :pe_member_characteristics
  has_many :characteristic_values

  has_many :elements_characteristics, class_name: 'Characteristic', foreign_key: 'register_characteristic_id'
  has_many :children_characteristics, class_name: 'Characteristic', foreign_key: 'parent_characteristic_id'

  has_many :tracking_process_chars

  has_one :lms_characteristic
  has_one :um_characteristic
  has_one :lib_characteristic
  has_one :ben_cellphone_characteristic

  attr_accessible :nombre, :publica, :position, :mini_ficha, :orden_mini_ficha, :planning_process,
                  :orden_planning_process, :porcentaje_participante_sence,
                  :basic_report, :orden_basic_report, :hr_process_assessment, :orden_hr_process_assessment,
                  :filter_assessment_report, :order_filter_assessment_report, :characteristic_type_id,
                  :data_type_id, :updated_by_manager, :updated_by_user, :boss_characteristic_id,
                  :register_characteristic_id, :parent_characteristic_id, :register_position,
                  :restricted_to_managers, :has_history, :is_company, :encrypted, :subase, :subaso,
                  :query_by_boss, :query_by_user, :query_by_others, :currency_subaso,
                  :from_date_work, :from_date_progresivas, :from_date_r_progresivas, :rol_security, :has_children, :required_new,
                  :characteristic_value_id, :char_value_selected, :contrato_plazo_secu, :tipo_contrato_secu,
                  :required_new_position, :required_new_c


  validates :nombre, presence: true, uniqueness: { case_sensitive: false }
  validates :position, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true
  validates :orden_mini_ficha, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true
  validates :orden_planning_process, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true
  validates :orden_basic_report, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true

  after_create :create_lms_characteristic

  default_scope order('position, nombre')

  def self.data_types
    [:string, :text, :date, :int, :float, :list, :file, :ct_image_url, :boss_code, :boss_full_name, :boss_characteristic, :register, :user_code, :first_owned_node_string, :first_owned_node_list, :cc, :benefit]
    #   0        1      2     3       4     5      6         7              8              9                 10               11          12                13                       14             15      16
  end

  def self.excel_data_types
    [:String, :String, :DateTime, :Number, :Number, :String, :String, :String, :String, :String, :String, :String, :String, :String, :String, :String, :String]
    #   0        1         2        3       4         5        6       7        8         9       10       11       12       13       14          15      16
  end

  def excel_data_type_name
    Characteristic.excel_data_types[self.data_type_id]
  end

  def data_type_name
    Characteristic.data_types[self.data_type_id]
  end

  def self.not_grouped_characteristics
    Characteristic.where('characteristic_type_id IS NULL', true)
  end

  def self.not_grouped_public_characteristics
    Characteristic.where('publica = ? AND characteristic_type_id IS NULL', true)
  end

  def self.not_grouped_restricted_to_managers_characteristics
    Characteristic.where('publica = ? AND restricted_to_managers = ? AND characteristic_type_id IS NULL', true, true)
  end

  def self.not_grouped_private_characteristics
    Characteristic.where('publica = ? AND characteristic_type_id IS NULL', false)
  end

  #def self.not_grouped_characteristics_available_to_users
  #  Characteristic.where('publica = ? AND restricted_to_managers = ? AND characteristic_type_id IS NULL', true, false)
  #end

  def self.not_grouped_characteristics_available_to_boss
    Characteristic.where('publica = ? AND restricted_to_managers = ? AND characteristic_type_id IS NULL AND query_by_boss = ?', true, false, true)
  end

  def self.not_grouped_characteristics_available_to_own_user
    Characteristic.where('publica = ? AND restricted_to_managers = ? AND characteristic_type_id IS NULL AND query_by_user = ?', true, false, true)
  end

  def self.not_grouped_characteristics_available_to_others
    Characteristic.where('publica = ? AND restricted_to_managers = ? AND characteristic_type_id IS NULL AND query_by_others = ?', true, false, true)
  end

  def self.not_grouped_characteristics_updated_by_users
    Characteristic.where('publica = ? AND restricted_to_managers = ? AND characteristic_type_id IS NULL AND updated_by_user = ?', true, false, true)
  end

  def self.not_grouped_characteristics_available_to_managers
    Characteristic.where('publica = ? AND characteristic_type_id IS NULL', true)
  end

  def self.not_grouped_characteristics_updated_by_managers
    Characteristic.where('publica = ? AND characteristic_type_id IS NULL AND updated_by_manager = ?', true, true)
  end

  def active_characteristic_values
    self.characteristic_values.where('active = ?', true)
  end

  def create_lms_characteristic
    lms_c = LmsCharacteristic.new
    lms_c.characteristic = self
    lms_c.save
  end

  def self.available_all_to_other_modules
    Characteristic.where('data_type_id IN(0,1,2,3,4,5,13) AND register_characteristic_id IS NULL')
  end

  def self.available_to_other_modules
    Characteristic.available_all_to_other_modules.where(publica: true)
  end

  def self.excel_report_available_characteristics
    Characteristic.where('publica = ? AND data_type_id NOT IN (6,7,11) AND register_characteristic_id IS NULL AND characteristic_type_id IS NULL', true)
  end

  #def self.not_grouped_characteristics_available_to_users_mobile
  #  Characteristic.where('publica = ? AND data_type_id IN(0,1,2,3,4,5,12) AND restricted_to_managers = ? AND characteristic_type_id IS NULL', true, false)
  #end

  def self.not_grouped_characteristics_available_to_boss_mobile
    Characteristic.where('publica = ? AND data_type_id IN(0,1,2,3,4,5,12) AND restricted_to_managers = ? AND characteristic_type_id IS NULL AND query_by_boss = ?', true, false, true)
  end

  def self.not_grouped_characteristics_available_to_own_user_mobile
    Characteristic.where('publica = ? AND data_type_id IN(0,1,2,3,4,5,12) AND restricted_to_managers = ? AND characteristic_type_id IS NULL AND query_by_user = ?', true, false, true)
  end

  def self.not_grouped_characteristics_available_to_others_mobile
    Characteristic.where('publica = ? AND data_type_id IN(0,1,2,3,4,5,12) AND restricted_to_managers = ? AND characteristic_type_id IS NULL AND query_by_others = ?', true, false, true)
  end

  #def self.grouped_characteristics_available_to_users_mobile(char_type_id)
  #  Characteristic.where('publica = ? AND data_type_id IN(0,1,2,3,4,5,12) AND restricted_to_managers = ? AND characteristic_type_id = ?', true, false, char_type_id)
  #end

  def self.grouped_characteristics_available_to_boss_mobile(char_type_id)
    Characteristic.where('publica = ? AND data_type_id IN(0,1,2,3,4,5,12) AND restricted_to_managers = ? AND characteristic_type_id = ? AND query_by_boss = ?', true, false, char_type_id, true)
  end

  def self.grouped_characteristics_available_to_own_user_mobile(char_type_id)
    Characteristic.where('publica = ? AND data_type_id IN(0,1,2,3,4,5,12) AND restricted_to_managers = ? AND characteristic_type_id = ? AND query_by_user = ?', true, false, char_type_id, true)
  end

  def self.grouped_characteristics_available_to_others_mobile(char_type_id)
    Characteristic.where('publica = ? AND data_type_id IN(0,1,2,3,4,5,12) AND restricted_to_managers = ? AND characteristic_type_id = ? AND query_by_others = ?', true, false, char_type_id, true)
  end

  def self.get_characteristic_company
    Characteristic.where('is_company = ?', true).first
  end

  def self.get_characteristic_subase
    Characteristic.where('subase = ?', true).first
  end

  def self.get_characteristic_subaso
    Characteristic.where('subaso = ?', true).first
  end

  def self.get_characteristic_from_date_work
    Characteristic.where('from_date_work = ?', true).first
  end

  def self.get_characteristic_from_date_progresivas
    Characteristic.where('from_date_progresivas = ?', true).first
  end

  def self.get_characteristic_from_date_r_progresivas
    Characteristic.where('from_date_r_progresivas = ?', true).first
  end

  def self.get_characteristic_rol_security
    Characteristic.where('rol_security = ?', true).first
  end

  def self.get_characteristic_tipo_contrato_secu
    Characteristic.where('tipo_contrato_secu = ?', true).first
  end

  def self.get_characteristic_contrato_plazo_secu
    Characteristic.where('contrato_plazo_secu = ?', true).first
  end

end
