# == Schema Information
#
# Table name: planning_process_company_unit_area_corrections
#
#  id                               :integer          not null, primary key
#  correccion                       :text
#  revisado                         :boolean          default(FALSE)
#  planning_process_company_unit_id :integer
#  company_unit_area_id             :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#

class PlanningProcessCompanyUnitAreaCorrection < ActiveRecord::Base
  belongs_to :planning_process_company_unit
  belongs_to :company_unit_area
  attr_accessible :correccion, :revisado
end
