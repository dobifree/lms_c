# == Schema Information
#
# Table name: jm_candidate_photos
#
#  id              :integer          not null, primary key
#  jm_candidate_id :integer
#  file_extension  :string(255)
#  crypted_name    :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class JmCandidatePhoto < ActiveRecord::Base

  belongs_to :jm_candidate

  attr_accessible :file_extension, :crypted_name

end
