# == Schema Information
#
# Table name: folder_readers
#
#  id         :integer          not null, primary key
#  folder_id  :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FolderReader < ActiveRecord::Base

  belongs_to :folder
  belongs_to :user

  # attr_accessible :title, :body

  validates :folder_id, uniqueness: { scope: :user_id }

end
