# == Schema Information
#
# Table name: medlic_reported_licenses
#
#  id                    :integer          not null, primary key
#  medlic_report_id      :integer
#  medlic_license_id     :integer
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class MedlicReportedLicense < ActiveRecord::Base
  belongs_to :medlic_report
  belongs_to :medlic_license  

  attr_accessible :registered_at,
                  :medlic_report_id,
                  :medlic_license_id,
                  :registered_by_user_id
end
