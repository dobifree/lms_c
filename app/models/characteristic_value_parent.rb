# == Schema Information
#
# Table name: characteristic_value_parents
#
#  id                             :integer          not null, primary key
#  characteristic_value_id        :integer
#  characteristic_parent_id       :integer
#  characteristic_value_parent_id :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#

class CharacteristicValueParent < ActiveRecord::Base

  belongs_to :characteristic_value
  belongs_to :characteristic_parent, class_name: 'Characteristic'
  belongs_to :characteristic_value_parent, class_name: 'CharacteristicValue'

  attr_accessible :characteristic_parent_id, :characteristic_value_parent_id

  validates :characteristic_value_parent_id, uniqueness: { scope: [:characteristic_value_id, :characteristic_parent_id] }


end
