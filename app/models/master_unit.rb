# == Schema Information
#
# Table name: master_units
#
#  id           :integer          not null, primary key
#  numero       :integer
#  nombre       :string(255)
#  descripcion  :text
#  tags         :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  crypted_name :string(255)
#  content_type :integer          default(0)
#  height       :integer
#

class MasterUnit < ActiveRecord::Base

  has_many :units

  attr_accessible :descripcion, :nombre, :tags , :crypted_name, :content_type, :height

  before_validation :set_numero, on: :create
  before_validation :set_crypted_name, on: :create

  validates :nombre, presence: true
  validates :numero, presence: true, uniqueness: true, numericality: { only_integer: true, greater_than: 0 }
  validates :crypted_name, presence: true, uniqueness: { case_sensitive: false }


  default_scope order('nombre ASC')

  def set_numero
    self.numero = 1
    max_master_unit = MasterUnit.reorder('numero DESC').first
    self.numero = max_master_unit.numero+1 if max_master_unit
  end

  def set_crypted_name

    self.crypted_name = self.numero.to_s+'_'+SecureRandom.hex(5)

  end

  def self.content_types
    [:flash, :flipping_book, :html5, :pdf, :video]
  end

  def content_type_name
    MasterUnit.content_types[self.content_type]
  end

end

