# == Schema Information
#
# Table name: pe_question_models
#
#  id               :integer          not null, primary key
#  description      :string(255)
#  weight           :float            default(0.0)
#  pe_evaluation_id :integer
#  pe_element_id    :integer
#  has_comment      :boolean
#  stored_image_id  :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  min_weight       :integer
#  max_weight       :integer
#  weight_sum       :integer
#  min_number       :integer
#  max_number       :integer
#

class PeQuestionModel < ActiveRecord::Base

  belongs_to :pe_evaluation
  belongs_to :pe_element
  belongs_to :stored_image

  has_many :pe_questions

  before_save :set_pe_evaluation

  attr_accessible :description, :has_comment, :weight, :pe_element_id,
                  :min_weight, :max_weight, :weight_sum, :min_number, :max_number


  def set_pe_evaluation
    self.pe_evaluation = pe_element.pe_evaluation
  end

end
