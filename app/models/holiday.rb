# == Schema Information
#
# Table name: holidays
#
#  id                     :integer          not null, primary key
#  hol_date               :date
#  hol_type               :integer
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  description            :text
#  name                   :string(255)      default("Sin nombre")
#  prev_holiday_id        :integer
#

class Holiday < ActiveRecord::Base
  belongs_to :user
  belongs_to :prev_holiday, class_name: 'Holiday', foreign_key: :prev_holiday_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  has_many :holiday_users
  has_many :holiday_characteristics

  attr_accessible :hol_date,
                  :hol_type,
                  :active,
                  :name,
                  :description,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id,
                  :prev_holiday_id

  validates :hol_date, presence: true
  validates :hol_type, presence: true
  validates :name, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def self.def_order; order('hol_date asc, name asc') end
  def self.active; where(active: true) end
  def self.global; where(hol_type: Holiday.global_type) end
  def self.char; where(hol_type: Holiday.characteristic_type) end
  def self.users; where(hol_type: Holiday.user_type) end
  def self.by_date(date_same); where(hol_date: date_same.to_date) end
  def self.by_customized; where(hol_type: Holiday.customized_types) end
  def active?; active end

  def active_users_assoc
    all = Holiday.historical_info([], id)
    all_hol_u = []
    all.each { |holiday| all_hol_u += holiday.holiday_users.active }
    all_hol_u
  end

  def active_chars
    all = Holiday.historical_info([], id)
    all_hol_c = []
    all.each { |holiday| all_hol_c += holiday.holiday_characteristics.active }
    all_hol_c
  end

  def self.historical_info(array, holiday_id)
    holiday = Holiday.find(holiday_id)
    array << holiday
    Holiday.historical_info(array, holiday.prev_holiday_id) if holiday.prev_holiday_id
    array.sort_by(&:registered_at).reverse
  end

  def type_name
    case hol_type
      when 0
        return 'Global'
      when 1
        return 'Características'
      when 2
        return 'Usuario'
      else
        nil
    end
  end

  def name_formatted; 'Sin nombre' == name ? 'Feriado' : name end
  def type_list; ['Global', 'Características', 'Usuario'] end
  def type_number_list; [0,1,2] end

  def global?
    return nil unless hol_type
    hol_type == Holiday.global_type
  end

  def characteristics?
    return nil unless hol_type
    hol_type == Holiday.characteristic_type
  end

  def user?
    return nil unless hol_type
    hol_type == Holiday.user_type
  end

  def self.global_type ; 0 end
  def self.characteristic_type; 1 end
  def self.user_type; 2 end
  def self.customized_types; [1,2] end
end
