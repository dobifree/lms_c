# == Schema Information
#
# Table name: pe_step_feedback_group2s
#
#  id            :integer          not null, primary key
#  pe_process_id :integer
#  pe_group2_id  :integer
#  from_date     :datetime
#  to_date       :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class PeStepFeedbackGroup2 < ActiveRecord::Base
  belongs_to :pe_process
  belongs_to :pe_group2
  attr_accessible :from_date, :to_date

  before_save :correct_to_date

  validates :from_date, presence: true
  validates :to_date, presence: true

  def correct_to_date

    if self.to_date

      self.to_date = self.to_date+((24*60*60)-1).seconds

    end

  end

end
