# == Schema Information
#
# Table name: ct_modules
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  active              :boolean
#  user_menu_alias     :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  cod                 :string(255)
#  has_managers        :boolean
#  alias               :string(255)
#  create_delete_users :boolean          default(FALSE)
#

class CtModule < ActiveRecord::Base

  has_many :ct_module_managers

  attr_accessible :active, :name, :cod, :has_managers, :user_menu_alias, :alias, :create_delete_users

  default_scope order('name')

  before_save { self.user_menu_alias = nil if self.user_menu_alias.blank? }
  before_save { self.alias = self.name if self.alias.blank? }

  def alias
    self[:alias].blank? ? self.name : self[:alias]
  end

end
