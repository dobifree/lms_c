# == Schema Information
#
# Table name: pe_question_files
#
#  id             :integer          not null, primary key
#  description    :string(255)
#  file_name      :string(255)
#  pe_question_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  registered_at  :datetime
#  pe_member_id   :integer
#  user_id        :integer
#

class PeQuestionFile < ActiveRecord::Base

  belongs_to :pe_question
  belongs_to :pe_member
  belongs_to :user

  attr_accessible :description, :file_name, :registered_at, :user_id

  validates :description, presence: true
  validates :file_name, presence: true

  default_scope order('registered_at')

end
