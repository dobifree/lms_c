# == Schema Information
#
# Table name: poll_process_characteristics
#
#  id                :integer          not null, primary key
#  poll_process_id   :integer
#  characteristic_id :integer
#  match_value       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PollProcessCharacteristic < ActiveRecord::Base
  belongs_to :poll_process
  belongs_to :characteristic
  attr_accessible :match_value, :poll_process, :poll_process_id ,:characteristic, :characteristic_id
end
