# == Schema Information
#
# Table name: levels
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  orden      :integer
#  program_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Level < ActiveRecord::Base
  
  belongs_to :program
  has_many :program_courses
  has_many :courses, through: :program_courses
  
  attr_accessible :nombre, :orden

  validates :nombre, presence: true
  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('orden ASC')

  def courses_ordered
    self.courses.reorder('orden, nombre')
  end

  def courses_ordered_by_name
    self.courses.reorder('courses.nombre')
  end

  def courses_ordered_con_estado
    self.courses.reorder('activo DESC, orden, nombre')
  end

  def courses_ordered_by_estado
    self.courses.reorder('activo DESC, nombre')
  end

  def courses_actives_ordered_by_name
    self.courses.where('activo = ?', true).reorder('nombre')
  end

  def program_courses_ordered
    self.program_courses.joins(:course).order('courses.nombre')
  end

  def visible_program_courses_ordered
    self.program_courses.joins(:course).where('hidden_in_user_courses_list = ?', false).order('courses.nombre')
  end

  def visible_program_courses_by_training_type_ordered(training_type)
    if training_type
      self.program_courses.joins(:course).where('hidden_in_user_courses_list = ? AND training_type_id = ?', false, training_type.id).uniq.order('courses.nombre')
    else
      self.program_courses.joins(:course).where('hidden_in_user_courses_list = ? AND training_type_id IS NULL', false).uniq.order('courses.nombre')
    end
  end



end
