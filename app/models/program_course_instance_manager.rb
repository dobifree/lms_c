# == Schema Information
#
# Table name: program_course_instance_managers
#
#  id                         :integer          not null, primary key
#  program_course_instance_id :integer
#  user_id                    :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class ProgramCourseInstanceManager < ActiveRecord::Base
  belongs_to :program_course_instance
  belongs_to :user
  # attr_accessible :title, :body

  attr_accessible :user_id

  validates :user_id, presence: true, uniqueness: { case_sensitive: false, scope: :program_course_instance_id }

end
