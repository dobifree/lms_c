# == Schema Information
#
# Table name: brc_categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BrcCategory < ActiveRecord::Base

  has_many :brc_process_categ_vals

  attr_accessible :name

  validates :name, presence: true, uniqueness: true

  default_scope order('name ASC')

end
