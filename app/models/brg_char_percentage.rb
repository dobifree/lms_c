# == Schema Information
#
# Table name: brg_char_percentages
#
#  id                      :integer          not null, primary key
#  brg_process_id          :integer
#  cod                     :string(255)
#  percentage              :float
#  active                  :boolean          default(TRUE)
#  deactivated_at          :datetime
#  deactivated_by_user_id  :integer
#  registered_at           :datetime
#  registered_by_user_id   :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  characteristic_value_id :integer
#

class BrgCharPercentage < ActiveRecord::Base
  belongs_to :characteristic_value
  belongs_to :brg_process
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :active,
                  :cod,
                  :characteristic_value_id,
                  :brg_process_id,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :percentage,
                  :registered_at,
                  :registered_by_user_id

  validates :characteristic_value_id, presence: true
  # validates :brg_process_id, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  before_save :set_cod

  def self.active
    where(active: true)
  end

  def self.from_process(id)
    where(brg_process_id: id)
  end

  def self.for_user(user_id)
    user = User.find(user_id)
    characteristic_company = Characteristic.get_characteristic_company
    return unless characteristic_company
    uc_c = user.user_characteristic(characteristic_company)
    return unless uc_c
    where(characteristic_value_id: uc_c.characteristic_value_id)
  end

  private

  def set_cod
    self.cod = brg_process_id.to_s + '-' + characteristic_value_id.to_s + '-' + percentage.to_s
  end


end
