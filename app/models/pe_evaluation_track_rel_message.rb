# == Schema Information
#
# Table name: pe_evaluation_track_rel_messages
#
#  id               :integer          not null, primary key
#  pe_evaluation_id :integer
#  pe_rel_id        :integer
#  message          :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PeEvaluationTrackRelMessage < ActiveRecord::Base
  belongs_to :pe_evaluation
  belongs_to :pe_rel
  attr_accessible :message

  validates :pe_rel_id, uniqueness: { scope: :pe_evaluation_id }
end
