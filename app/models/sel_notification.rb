# == Schema Information
#
# Table name: sel_notifications
#
#  id                           :integer          not null, primary key
#  sel_notification_template_id :integer
#  to                           :string(255)
#  cc                           :string(255)
#  subject                      :string(255)
#  body                         :text
#  triggered_at                 :datetime
#  triggered_by_user_id         :integer
#  sent                         :boolean          default(FALSE)
#  sent_at                      :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  event_id                     :integer
#  jm_candidate_id              :integer
#  sel_process_id               :integer
#  sel_req_approver_id          :integer
#

class SelNotification < ActiveRecord::Base
  belongs_to :sel_notification_template
  belongs_to :triggered_by_user, class_name: 'User'
  belongs_to :jm_candidate
  belongs_to :sel_process
  belongs_to :sel_req_approver
  attr_accessible :body, :cc, :sent, :sent_at, :subject, :to, :sel_notification_template_id,
                  :triggered_at, :triggered_by_user_id, :event_id
end
