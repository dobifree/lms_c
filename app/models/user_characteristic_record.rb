# == Schema Information
#
# Table name: user_characteristic_records
#
#  id                      :integer          not null, primary key
#  user_id                 :integer
#  characteristic_id       :integer
#  value                   :text
#  from_date               :datetime
#  to_date                 :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  value_string            :string(255)
#  value_text              :text
#  value_date              :date
#  value_int               :integer
#  value_float             :float
#  value_file              :string(255)
#  characteristic_value_id :integer
#  created_by_user_id      :integer
#  created_by_admin_id     :integer
#  finished_by_user_id     :integer
#  finished_by_admin_id    :integer
#  value_ct_image_url      :string(255)
#  value_encrypted         :binary
#

class UserCharacteristicRecord < ActiveRecord::Base

  #include CtCipherModule
  include CtCipherModule

  belongs_to :user
  belongs_to :characteristic
  belongs_to :characteristic_value

  belongs_to :created_by_user, class_name: 'User'
  belongs_to :finished_by_user, class_name: 'User'

  belongs_to :created_by_admin, class_name: 'Admin'
  belongs_to :finished_by_admin, class_name: 'Admin'

  attr_accessible :from_date, :to_date, :value, :value_string, :value_text, :value_date, :value_int,
                  :value_float, :value_file, :characteristic_value_id, :value_ct_image_url, :value_encrypted

  default_scope order('from_date DESC, to_date DESC')

  def raw_value

    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 11 || self.characteristic.data_type_id == 13
      self.value_string
    elsif self.characteristic.data_type_id == 1
      self.value_text
    elsif self.characteristic.data_type_id == 2
      self.value_date
    elsif self.characteristic.data_type_id == 3
      self.value_int
    elsif self.characteristic.data_type_id == 4
      self.value_float
    elsif self.characteristic.data_type_id == 5 || self.characteristic.data_type_id == 14
      self.characteristic_value ? self.characteristic_value.id : nil
    elsif self.characteristic.data_type_id == 6
      self.value_file
    elsif self.characteristic.data_type_id == 7
      self.value_ct_image_url
    end

    #las características de tipo de dato      8,             9,                10,              12         no devuelven ningún valor
    #                                    :boss_code, :boss_full_name, :boss_characteristic, :user_code


  end

  def formatted_value
    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 13
      self.value_string
    elsif self.characteristic.data_type_id == 1
      self.value_text
    elsif self.characteristic.data_type_id == 2
      self.value_date ? I18n.localize(self.value_date, format: :day_snmonth_year) : self.value
    elsif self.characteristic.data_type_id == 3
      self.value_int
    elsif self.characteristic.data_type_id == 4
      self.value_float
    elsif self.characteristic.data_type_id == 5
      self.characteristic_value ? self.characteristic_value.value_string : nil
    elsif self.characteristic.data_type_id == 6
      self.value_file
    elsif self.characteristic.data_type_id == 7
      self.value_ct_image_url
    else
      self.value
    end
  end

  def excel_formatted_value
    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 11 || self.characteristic.data_type_id == 13
      self.value_string
    elsif self.characteristic.data_type_id == 1
      self.value_text
    elsif self.characteristic.data_type_id == 2
      self.value_date
    elsif self.characteristic.data_type_id == 3
      self.value_int
    elsif self.characteristic.data_type_id == 4
      self.value_float
    elsif self.characteristic.data_type_id == 5 || self.characteristic.data_type_id == 14
      self.characteristic_value ? self.characteristic_value.value_string : nil
    elsif self.characteristic.data_type_id == 6
      self.value_file
    elsif self.characteristic.data_type_id == 7
      self.value_ct_image_url
    end

    #las características de tipo de dato 8, 9, 10, 12 no devuelven ningún valor

  end

  def clone_from_user_characteristic(user_characteristic, from_date)

    self.user = user_characteristic.user
    self.characteristic = user_characteristic.characteristic
    
    self.from_date = from_date

    self.value = user_characteristic.valor
    self.value_string = user_characteristic.value_string
    self.value_text = user_characteristic.value_text
    self.value_date = user_characteristic.value_date
    self.value_int = user_characteristic.value_int
    self.value_float = user_characteristic.value_float
    self.characteristic_value_id = user_characteristic.characteristic_value_id
    self.value_file = user_characteristic.value_file
    self.value_ct_image_url = user_characteristic.value_ct_image_url
    self.value_encrypted = user_characteristic.value_encrypted
  end

  def update_record(new_value, from_date, to_date)

    if self.characteristic.data_type_id == 13

      user_j_job = self.user.user_j_jobs.where('from_date = ? AND to_date = ?', self.from_date.to_date, self.to_date.to_date).first

      if user_j_job

        user_j_job.from_date = from_date.to_date
        user_j_job.to_date = to_date.to_date
        user_j_job.save

      end

    end

    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 13
      #string o first_owned_node
      if self.characteristic.encrypted?
        #self.value_encrypted = encrypt_string(new_value)
        new_value = ''
      end
      self.value_string = new_value
    elsif self.characteristic.data_type_id == 1
      #text
      if self.characteristic.encrypted?
        #self.value_encrypted = encrypt_string(new_value)
        new_value = ''
      end
      self.value_text = new_value
    elsif self.characteristic.data_type_id == 2
      #date
      self.value_date = new_value
    elsif self.characteristic.data_type_id == 3
      #int
      if self.characteristic.encrypted?
        #self.value_encrypted = encrypt_int(new_value)
        new_value = nil
      end
      self.value_int = new_value
    elsif self.characteristic.data_type_id == 4
      #float
      self.value_float = new_value
    elsif self.characteristic.data_type_id == 5 || self.characteristic.data_type_id == 14
      #lista
      self.characteristic_value = characteristic.characteristic_values.where('id = ?',new_value).first
    elsif self.characteristic.data_type_id == 6
      #file
    elsif self.characteristic.data_type_id == 7
      #ct_image_url
      self.value_ct_image_url = new_value.strip
    end

    self.value = self.formatted_value.to_s

    self.from_date = from_date
    self.to_date = to_date


  end

  def update_record_value(new_value)

    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 13
      #string o first_owned_node
      if self.characteristic.encrypted?
        #self.value_encrypted = encrypt_string(new_value)
        new_value = ''
      end
      self.value_string = new_value
    elsif self.characteristic.data_type_id == 1
      #text
      if self.characteristic.encrypted?
        #self.value_encrypted = encrypt_string(new_value)
        new_value = ''
      end
      self.value_text = new_value
    elsif self.characteristic.data_type_id == 2
      #date
      self.value_date = new_value
    elsif self.characteristic.data_type_id == 3
      #int
      if self.characteristic.encrypted?
        #self.value_encrypted = encrypt_int(new_value)
        new_value = nil
      end
      self.value_int = new_value
    elsif self.characteristic.data_type_id == 4
      #float
      self.value_float = new_value
    elsif self.characteristic.data_type_id == 5 || self.characteristic.data_type_id == 14
      #lista
      self.characteristic_value = characteristic.characteristic_values.where('id = ?',new_value).first
    elsif self.characteristic.data_type_id == 6
      #file
    elsif self.characteristic.data_type_id == 7
      #ct_image_url
      self.value_ct_image_url = new_value.strip
    end

    self.value = self.formatted_value.to_s

  end

end
