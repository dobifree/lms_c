# == Schema Information
#
# Table name: ben_user_cellphone_processes
#
#  id                        :integer          not null, primary key
#  user_id                   :integer
#  ben_cellphone_number_id   :integer
#  ben_cellphone_process_id  :integer
#  total_bill                :decimal(12, 4)
#  configured_subsidy        :decimal(12, 4)
#  applied_subsidy           :decimal(12, 4)
#  total_charged             :decimal(12, 4)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  registered_manually_total :decimal(12, 4)   default(0.0)
#  plan_bill                 :decimal(12, 4)
#  equipment_bill            :decimal(12, 4)
#

class BenUserCellphoneProcess < ActiveRecord::Base
  belongs_to :user
  belongs_to :ben_cellphone_number
  belongs_to :ben_cellphone_process
  has_many :ben_cellphone_bill_items, dependent: :nullify

  attr_accessible :applied_subsidy,
                  :configured_subsidy,
                  :total_bill,
                  :equipment_bill,
                  :plan_bill,
                  :total_charged,
                  :registered_manually_total,
                  :user_id,
                  :ben_cellphone_number_id,
                  :ben_cellphone_process_id

end
