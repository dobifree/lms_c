# == Schema Information
#
# Table name: folder_members
#
#  id                :integer          not null, primary key
#  folder_id         :integer
#  characteristic_id :integer
#  valor             :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class FolderMember < ActiveRecord::Base
  belongs_to :folder
  belongs_to :characteristic
  attr_accessible :valor
end
