# == Schema Information
#
# Table name: folders
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  creador_id :integer
#  padre_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  public     :boolean          default(FALSE)
#

class Folder < ActiveRecord::Base

  belongs_to :creador, class_name: 'User', foreign_key: 'creador_id'
  belongs_to :padre, class_name: 'Folder', foreign_key: 'padre_id'
  has_many :hijos, class_name: 'Folder', foreign_key: 'padre_id', :dependent => :destroy
  has_many :folder_owners, :dependent => :destroy
  has_many :folder_members, :dependent => :destroy
  has_many :folder_readers, :dependent => :destroy
  has_many :folder_files, :dependent => :destroy

  validates :nombre, presence: true
  validates :nombre, uniqueness: {scope: :padre_id}

  attr_accessible :creador_id, :padre_id, :nombre, :public

  default_scope order('nombre ASC')

  def is_public?
    priv = false
    if self.public?
      priv = true
    else
      self.get_folder_ancestros().each do |ancentro|
        if ancentro.public?
          priv = true
          break
        end
      end
    end

    return priv

  end

  def get_folder_ancestros()

    ancestros = []

    if self.padre
      ancestros += self.padre.get_folder_ancestros
      ancestros.push self.padre
    end

    return ancestros

  end

end
