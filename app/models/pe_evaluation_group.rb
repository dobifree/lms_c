# == Schema Information
#
# Table name: pe_evaluation_groups
#
#  id            :integer          not null, primary key
#  position      :integer
#  name          :string(255)
#  pe_process_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class PeEvaluationGroup < ActiveRecord::Base
  belongs_to :pe_process
  has_many :pe_evaluations
  has_many :pe_assessment_groups
  attr_accessible :name, :position, :pe_process_id

  default_scope order('position ASC, name ASC')

end
