# == Schema Information
#
# Table name: brc_apv_institutions
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BrcApvInstitution < ActiveRecord::Base

  has_many :brc_members

  attr_accessible :name

  validates :name, presence: true, uniqueness: true

  default_scope order('name ASC')
end
