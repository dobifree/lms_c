# == Schema Information
#
# Table name: sel_requirements
#
#  id                    :integer          not null, primary key
#  sel_req_template_id   :integer
#  registered_by_user_id :integer
#  done                  :boolean
#  done_by_user_id       :integer
#  done_at               :datetime
#  declined              :boolean
#  declined_comment      :text
#  declined_by_user_id   :integer
#  declined_at           :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  registered_at         :datetime
#

class SelRequirement < ActiveRecord::Base
  belongs_to :sel_req_template
  belongs_to :registered_by_user, class_name: 'User'
  belongs_to :done_by_user, class_name: 'User'
  belongs_to :declined_by_user, class_name: 'User'
  has_one :sel_process
  has_many :sel_req_logs, dependent: :destroy
  has_many :sel_requirement_values, dependent: :destroy
  has_many :sel_req_processes, dependent: :destroy
  attr_accessible :declined, :declined_at, :declined_by_user_id, :declined_comment, :done, :done_at, :done_by_user_id, :registered_at, :registered_by_user_id,
                  :sel_req_template, :sel_req_template_id

  scope :attended, -> {joins(:sel_process)}
  #scope :unattended, -> {joins('LEFT JOIN sel_processes ON sel_requirements.id = sel_processes.sel_requirement_id').where(sel_processes: {:sel_requirement_id => nil})}


  def self.unattended
    sel_requirements = []
    sel_requirements_aux= self.joins('LEFT JOIN sel_processes ON sel_requirements.id = sel_processes.sel_requirement_id').where(sel_processes: {:sel_requirement_id => nil})
    sel_requirements_aux.each do |sel_req|
      sel_requirements << sel_req if sel_req.is_unattended?
    end
    sel_requirements
  end

  def is_attended?
    return true if self.sel_process

    return false
  end

  def current_state_log
    sel_req_logs.last
  end

  def is_editable?
    return true if [0, 3].include?(current_state_log.log_state_id)
  end

  def is_deletable?
    return true if [0, 3].include?(current_state_log.log_state_id)
  end

  def deleted?
    return true if current_state_log.log_state_id == 4
  end

  def is_processable?
    # el 7 es para soportar cuando se elimina un proceso
    # y no necesariamente se cambie el estado del requerimiento
    return true if [2, 7].include?(current_state_log.log_state_id) && !sel_process
  end

  def is_attendable?
    return true if [1, 8].include?(current_state_log.log_state_id)
  end

  # rechazable por el gestor (devuelta)
  def is_rejectable?
    return true if [1, 8].include?(current_state_log.log_state_id)
  end

  def is_unattended?
    return true if [1, 2, 5, 6, 8].include?(current_state_log.log_state_id) && !sel_process
  end

end
