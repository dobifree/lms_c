# == Schema Information
#
# Table name: bp_groups_users
#
#  id                      :integer          not null, primary key
#  user_id                 :integer
#  bp_group_id             :integer
#  since                   :datetime
#  until                   :datetime
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  registered_by_user_id   :integer
#  deactivated_by_user_id  :integer
#

class BpGroupsUser < ActiveRecord::Base
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  belongs_to :bp_group
  belongs_to :user

  attr_accessible :user_id,
                  :bp_group_id,
                  :since,
                  :until,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_admin_id,
                  :registered_by_user_id

  validates :user_id, presence: true
  validates :bp_group_id, presence: true
  validates :since, presence: true
  validates :registered_at, presence: true

  validate :overlap_range_since
  validate :since_before_until
  validate :range_since_concordance_1
  validate :range_since_concordance_2
  validate :range_until_concordance_1

  def user_formatted
    user.codigo+' - '+user.apellidos+', '+user.nombre
  end

  def deactivated_by_admin_formatted
    deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  def active?(now)
    now  = now.to_datetime
    return true if self.until && since <= now && self.until > now
    return true if !self.until && now >= since
    false
  end

  def active_at?(now)
    active?(now)
  end

  def self.active?(now, user_id)
    BpGroupsUser.where(user_id: user_id).each do |group_user|
      return group_user if group_user.active?(now)
    end
    false
  end

  private

  def since_before_until
    return unless self.until && since && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end

  def overlap_range_since
    return if any_open_period?
    return unless bp_group_id
    return unless since
    overlap_periods.each do |period|
      error_message = 'se traslapa con otra asociación. '
      error_message += 'Grupo: ' + period.bp_group.name + ', '
      error_message += 'desde: ' + period.since.strftime('%d/%m/%Y %H:%M')
      error_message += ', hasta: ' + period.until.strftime('%d/%m/%Y %H:%M')
      error_message += '. '
      errors.add(:since, error_message)
    end
  end

  def any_open_period?
    return false unless user_id
    period = BpGroupsUser.where('until is null and user_id = ?', user_id).first
    if (period && !id) || (period && id && period.id != id)
      error_message = 'no puede definir habiendo una asociación sin cerrar para el mismo usuario de inicio: ' + period.since.strftime('%d/%m/%Y %H:%M')
      errors.add(:until, error_message)
      true
    else
      false
    end
  end

  def overlap_periods
    overlap_periods = []
    periods = BpGroupsUser.where('until is not null and user_id = ?', user_id)
    periods.each do |period|
      if self.until
        next unless period.since < self.until && period.until > since
      else
        next unless period.until > since
      end
      next if id && period.id == id
      overlap_periods << period
    end
    return overlap_periods
  end

  def range_since_concordance_1
    return unless bp_group_id
    return unless since
    return unless bp_group.since && since < bp_group.since
    errors.add(:since, 'debe ser mayor a "desde" del grupo')
  end

  def range_since_concordance_2
    return unless bp_group_id
    return unless since
    return unless bp_group.since && bp_group.until && since > bp_group.until
    errors.add(:since, 'debe ser menor a "hasta" del grupo')
  end

  def range_until_concordance_1
    return unless bp_group_id
    return unless self.until && bp_group.since && bp_group.until && self.until > bp_group.until
    errors.add(:until, 'debe ser menor a "hasta" del grupo')
  end

end
