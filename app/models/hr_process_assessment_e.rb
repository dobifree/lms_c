# == Schema Information
#
# Table name: hr_process_assessment_es
#
#  id                       :integer          not null, primary key
#  hr_process_evaluation_id :integer
#  hr_process_user_id       :integer
#  resultado_puntos         :float
#  resultado_porcentaje     :float
#  fecha                    :datetime
#  hr_process_user_eval_id  :integer
#  tipo_rel                 :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class HrProcessAssessmentE < ActiveRecord::Base

  belongs_to :hr_process_evaluation
  belongs_to :hr_process_user

  belongs_to :hr_process_user_eval, class_name: 'HrProcessUser', foreign_key: :hr_process_user_eval_id

  has_many :hr_process_assessment_qs, dependent: :destroy

  attr_accessible :fecha, :hr_process_user_eval_id, :resultado_porcentaje, :resultado_puntos, :tipo_rel

end
