# == Schema Information
#
# Table name: support_tickets
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  support_ticket_type_id :integer
#  descripcion            :text
#  fecha_registro         :datetime
#  atendido               :boolean          default(FALSE)
#  respuesta              :text
#  fecha_respuesta        :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  asunto                 :string(255)
#  curso                  :string(255)
#  envio_email            :boolean          default(FALSE)
#

class SupportTicket < ActiveRecord::Base

  belongs_to :user
  belongs_to :support_ticket_type

  attr_accessible :asunto, :atendido, :curso, :descripcion, :fecha_registro, :fecha_respuesta,
                  :respuesta, :support_ticket_type_id, :user_id, :envio_email

  before_save { self.curso = nil if self.curso.blank? }

  validates :user_id, presence: true
  validates :support_ticket_type_id, presence: true
  validates :asunto, presence: true
  validates :descripcion, presence: true
  validates :fecha_registro, presence: true

  default_scope order('atendido ASC, fecha_registro ASC')


end
