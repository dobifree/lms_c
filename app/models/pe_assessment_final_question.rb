# == Schema Information
#
# Table name: pe_assessment_final_questions
#
#  id                                :integer          not null, primary key
#  pe_question_id                    :integer
#  points                            :decimal(30, 6)
#  percentage                        :float
#  pe_assessment_final_question_id   :integer
#  pe_assessment_final_evaluation_id :integer
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#

class PeAssessmentFinalQuestion < ActiveRecord::Base

  belongs_to :pe_question
  belongs_to :pe_assessment_final_evaluation
  belongs_to :pe_assessment_final_question

  has_many :pe_assessment_final_questions, dependent: :destroy

  attr_accessible :pe_assessment_final_question_id, :percentage, :points

end
