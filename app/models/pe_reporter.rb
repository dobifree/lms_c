# == Schema Information
#
# Table name: pe_reporters
#
#  id            :integer          not null, primary key
#  pe_process_id :integer
#  user_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  general       :boolean          default(TRUE)
#

class PeReporter < ActiveRecord::Base
  belongs_to :pe_process
  belongs_to :user

  has_many :pe_reporter_characteristic_values, dependent: :destroy

  # attr_accessible :title, :body

  attr_accessible :general

  validates :pe_process, presence: true
  validates :user, presence: true
  validates :user_id, uniqueness: { scope: :pe_process_id }

end
