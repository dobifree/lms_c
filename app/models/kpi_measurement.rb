# == Schema Information
#
# Table name: kpi_measurements
#
#  id               :integer          not null, primary key
#  value            :float
#  percentage       :float
#  kpi_indicator_id :integer
#  measured_at      :date
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  kpi_set_id       :integer
#  kpi_subset_id    :integer
#

class KpiMeasurement < ActiveRecord::Base

  belongs_to :kpi_indicator
  belongs_to :kpi_set
  belongs_to :kpi_subset

  attr_accessible :measured_at, :percentage, :value, :kpi_indicator_id

  before_save :set_percentage
  after_save :set_kpi_set_indicator_percentage

  default_scope order('measured_at')

  def set_percentage

    if self.kpi_indicator_id

      kpi_dashboard = self.kpi_indicator.kpi_set.kpi_dashboard

      kpi_indicator_detail = self.kpi_indicator.kpi_indicator_details.where('measured_at = ?',self.measured_at).first

      if kpi_indicator_detail

        self.percentage = KpiManagingController.helpers.calculate_percentage(self.value, kpi_indicator_detail.goal, self.kpi_indicator, kpi_indicator_detail.kpi_indicator_ranks, kpi_dashboard.upper_limit, kpi_dashboard.lower_limit)

      end

    end

  end

  def set_kpi_set_indicator_percentage

    if self.kpi_indicator

      kpi_indicator = self.kpi_indicator

      KpiManagingController.helpers.update_subset_measurement(kpi_indicator.kpi_subset, self.measured_at) if kpi_indicator.kpi_subset
      KpiManagingController.helpers.update_set_measurement(kpi_indicator.kpi_set, self.measured_at)

      kpi_indicator.set_final_result

      kpi_indicator.save

=begin
      kpi_indicator.pe_questions.each do |pe_question|

        pe_question.pe_assessment_questions.each do |pe_assessment_question|

          if pe_question.indicator_type == 4
            pe_assessment_question.indicator_discrete_achievement = kpi_indicator.value
          else
            pe_assessment_question.indicator_achievement = kpi_indicator.value
          end

          pe_assessment_question.percentage = kpi_indicator.percentage

          pe_assessment_question.points = kpi_indicator.percentage

          pe_assessment_question.save

        end

      end
=end

      #save kpi_subset percentage

      if kpi_indicator.kpi_subset

        kpi_subset = kpi_indicator.kpi_subset

        kpi_subset.reload

        kpi_subset.set_percentage

        kpi_subset.save

      end

      #save kpi_set percentage

      kpi_set = kpi_indicator.kpi_set

      kpi_set.reload

      kpi_set.set_percentage

      kpi_set.save

    end

  end

end
