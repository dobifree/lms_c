# == Schema Information
#
# Table name: jm_lists
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class JmList < ActiveRecord::Base
  has_many :jm_options, :dependent => :destroy
  has_many :jm_subcharacteristics
  accepts_nested_attributes_for :jm_options, :allow_destroy => true
  attr_accessible :description, :name, :jm_options, :jm_options_attributes
end
