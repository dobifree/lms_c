# == Schema Information
#
# Table name: hr_process_assessment_d_cals
#
#  id                                  :integer          not null, primary key
#  hr_process_assessment_d_id          :integer
#  hr_process_calibration_session_id   :integer
#  hr_process_calibration_session_m_id :integer
#  resultado_porcentaje                :float
#  fecha                               :datetime
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  comentario                          :text
#  hr_process_dimension_g_id           :integer
#

class HrProcessAssessmentDCal < ActiveRecord::Base
  belongs_to :hr_process_assessment_d
  belongs_to :hr_process_dimension_g
  belongs_to :hr_process_calibration_session
  belongs_to :hr_process_calibration_session_m
  attr_accessible :fecha, :resultado_porcentaje, :comentario, :hr_process_assessment_d_id, :hr_process_calibration_session_id,
                  :hr_process_calibration_session_m_id
end
