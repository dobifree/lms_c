# == Schema Information
#
# Table name: sel_apply_forms
#
#  id                         :integer          not null, primary key
#  sel_template_id            :integer
#  jm_characteristic_id       :integer
#  mandatory                  :boolean
#  position                   :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  show_in_list_of_candidates :boolean          default(TRUE)
#

class SelApplyForm < ActiveRecord::Base
  belongs_to :sel_template
  belongs_to :jm_characteristic
  attr_accessible :mandatory, :position, :sel_template, :sel_template_id, :jm_characteristic, :jm_characteristic_id, :show_in_list_of_candidates

  validates :position, :presence => true

  default_scope order('position ASC')
end
