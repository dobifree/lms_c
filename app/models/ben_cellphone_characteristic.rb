# == Schema Information
#
# Table name: ben_cellphone_characteristics
#
#  id                   :integer          not null, primary key
#  characteristic_id    :integer
#  active_search_people :boolean          default(FALSE)
#  pos_search_people    :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  active_reporting     :boolean          default(FALSE)
#

class BenCellphoneCharacteristic < ActiveRecord::Base
  belongs_to :characteristic
  has_many :ben_custom_report_characteristics

  attr_accessible :active_reporting,
                  :active_search_people,
                  :pos_search_people

  validates :characteristic_id, uniqueness: true

  def self.search_people
    BenCellphoneCharacteristic.where('active_search_people = ?', true).order('pos_search_people')
  end

  def self.reporting
    # char_data_types = [0, 1, 2, 3, 4, 5]
    # reporting_array = []
    # report_ben_cell_char = BenCellphoneCharacteristic.where(active_reporting: true).all
    # report_ben_cell_char.each {|ben| reporting_array << ben.characteristic if char_data_types.include?(ben.data_type_id)}
    # return reporting_array

    chars_id = []
    available_chars = Characteristic.available_to_other_modules
    available_chars.each { |char| chars_id << char.id }

    reporting_array = []
    report_ben_cell_char = BenCellphoneCharacteristic.where(active_reporting: true, characteristic_id: chars_id).all
    report_ben_cell_char.each {|ben| reporting_array << ben.characteristic }
    return reporting_array

  end

  def self.by_characterisctic(characteristic)
    BenCellphoneCharacteristic.where(:characteristic_id => characteristic.id).first_or_create!
  end

  def name
    characteristic.nombre
  end

  def data_type_id
    characteristic.data_type_id
  end
end
