# == Schema Information
#
# Table name: hr_process_calibration_sessions
#
#  id            :integer          not null, primary key
#  hr_process_id :integer
#  inicio        :date
#  fin           :date
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  abierta       :boolean          default(TRUE)
#  descripcion   :text
#

class HrProcessCalibrationSession < ActiveRecord::Base

  belongs_to :hr_process

  has_many :hr_process_calibration_session_cs
  has_many :users, through: :hr_process_calibration_session_cs

  has_many :hr_process_calibration_session_ms
  has_many :hr_process_users, through: :hr_process_calibration_session_ms

  attr_accessible :fin, :inicio, :hr_process_id, :abierta, :descripcion

  validates :inicio, presence: true
  validates :fin, presence: true

  default_scope  order('descripcion, inicio ASC, fin ASC')

  def hr_process_calibration_session_cs_order_by_apellidos_nombre

    self.hr_process_calibration_session_cs.joins(:user).order('apellidos, nombre')

  end

  def hr_process_calibration_session_ms_order_by_apellidos_nombre

    self.hr_process_calibration_session_ms.joins('INNER JOIN hr_process_users ON hr_process_users.id = hr_process_calibration_session_ms.hr_process_user_id INNER JOIN users ON users.id = hr_process_users.user_id ').order('apellidos, nombre')
  end

end
