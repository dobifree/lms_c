# == Schema Information
#
# Table name: courses
#
#  id                            :integer          not null, primary key
#  codigo                        :string(255)
#  nombre                        :string(255)
#  descripcion                   :text
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  nombre_corto                  :string(255)
#  numero_oportunidades          :integer          default(1)
#  duracion                      :integer          default(0)
#  objetivos                     :text
#  dedicacion_estimada           :float            default(0.0)
#  nota_minima                   :integer
#  dncp_id                       :integer
#  clonable                      :boolean          default(FALSE)
#  imagen                        :string(255)
#  eval_presencial               :boolean          default(FALSE)
#  poll_presencial               :boolean          default(FALSE)
#  asistencia_presencial         :boolean          default(FALSE)
#  sence_code                    :string(255)
#  sence_hours                   :float
#  sence_student_value           :float
#  muestra_porcentajes           :boolean          default(FALSE)
#  percentage_minimum_grade      :boolean          default(FALSE)
#  sence_hour_value              :float
#  sence_students_number         :integer
#  sence_area                    :string(255)
#  sence_speciality              :string(255)
#  sence_group                   :string(255)
#  sence_training_type           :string(255)
#  sence_authorization_date      :date
#  sence_end_validity_date       :date
#  sence_name                    :string(255)
#  sence_otec_name               :string(255)
#  sence_otec_address            :string(255)
#  sence_otec_telephone          :string(255)
#  sence_region                  :string(255)
#  minimum_assistance            :integer          default(0)
#  sence_maximum_students_number :integer
#  sence_minimum_students_number :integer
#  maximum_reference_value       :float
#  calculate_percentage          :boolean          default(FALSE)
#  unit_alias                    :string(255)      default("unidad")
#  first_unit_number             :integer          default(1)
#  training_type_id              :integer
#  managed_by_manager            :boolean          default(FALSE)
#  active                        :boolean          default(TRUE)
#  finish_msg_success            :text
#  finish_msg_failure            :text
#  finish_msg_failure_oportunity :text
#  evaluado                      :boolean          default(TRUE)
#  control_total_asistencia      :boolean          default(FALSE)
#  asistencia_matricula          :boolean          default(FALSE)
#  training_mode_id              :integer
#  asistencia_minima             :integer          default(100)
#

class Course < ActiveRecord::Base

  belongs_to :dncp
  belongs_to :training_type
  belongs_to :training_mode

	has_many :program_courses, dependent: :destroy
	has_many :programs, through: :program_courses
	has_many :units, dependent: :destroy
	has_many :evaluations, dependent: :destroy, 
						conditions: 'program_id IS NULL AND level_id IS NULL'
  has_many :polls, dependent: :destroy, 
            conditions: 'program_id IS NULL AND level_id IS NULL'
  has_many :user_courses, dependent: :destroy
  has_many :course_comments, dependent: :destroy
  has_many :course_files, dependent: :destroy

  has_many :course_certificates

  has_many :course_training_characteristics, :dependent => :destroy


  attr_accessible :codigo, :descripcion, :duracion, :dedicacion_estimada, :nombre, :nombre_corto,
                  :nota_minima, :numero_oportunidades, :objetivos, :clonable, :dncp_id,
                  :asistencia_presencial, :eval_presencial, :poll_presencial, :muestra_porcentajes,
                  :minimum_assistance,
                  :sence_code, :sence_hours, :sence_student_value, :sence_hour_value,
                  :sence_students_number, :sence_area, :sence_speciality, :sence_group,
                  :sence_training_type, :sence_authorization_date, :sence_end_validity_date,
                  :sence_name, :sence_otec_name, :sence_otec_address, :sence_otec_telephone,
                  :sence_region, :sence_maximum_students_number, :sence_minimum_students_number, :maximum_reference_value,
                  :calculate_percentage, :unit_alias, :first_unit_number,
                  :training_type_id, :training_mode_id,
                  :managed_by_manager, :active, :finish_msg_success, :finish_msg_failure, :finish_msg_failure_oportunity,
                  :evaluado, :control_total_asistencia, :asistencia_matricula, :asistencia_minima

  before_save { self.sence_hour_value = nil if self.sence_hour_value.blank? }
  before_save { self.sence_student_value = nil if self.sence_student_value.blank? }
  before_validation { self.nota_minima = 0 unless self.evaluado}



  validates :codigo, presence: true, uniqueness: { case_sensitive: false }
  validates :nombre, presence: true
  validates :numero_oportunidades, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :duracion, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :dedicacion_estimada, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :nota_minima, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :minimum_assistance, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :sence_hour_value, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  validates :sence_student_value, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  validates :unit_alias, presence: true
  validates :asistencia_minima, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  default_scope order('nombre ASC')

  def self.active_courses
    Course.where('active = ?', true)
  end

  def self.active_courses_managed_by_manager
    Course.where('active = ? AND managed_by_manager = ?', true, true)
  end

  def units_without(unit)
    self.units.delete_if { |u| u.id == unit.id }
  end

  def evaluations_without(evaluation)
    self.evaluations.delete_if { |e| e.id == evaluation.id }
  end

  def polls_without(poll)
    self.polls.delete_if { |p| p.id == poll.id }
  end

  def set_imagen

    self.imagen = self.id.to_s+'_'+SecureRandom.hex(5)+'.jpg'

  end

  def programs_by_name
    self.programs.reorder('nombre')
  end

  def ordered_elements

    elements = []
    elements_ids = []

    units = self.units
    evaluations = self.evaluations
    polls = self.polls

    total = units.length + evaluations.length + polls.length

    total = units.last.orden if !units.empty? && total < units.last.orden
    total = evaluations.last.orden if !evaluations.empty? && total < evaluations.last.orden
    total = polls.last.orden if !polls.empty? && total < polls.last.orden

    (1..total).each do |pos|

      units.each do |unit|
        if unit.orden == pos
          elements.push unit
          elements_ids.push 'u-'+unit.id.to_s
        end
      end

      evaluations.each do |evaluation|
        if evaluation.orden == pos
          elements.push evaluation
          elements_ids.push 'e-'+evaluation.id.to_s
        end
      end

      polls.each do |poll|
        if poll.orden == pos
          elements.push poll
          elements_ids.push 'p-'+poll.id.to_s
        end
      end

    end

    return elements, elements_ids

  end

  def has_virtual_elements

    has = false

    self.units.each do |unit|
      if unit.virtual
        has = true
        break
      end
    end

    unless has

      self.evaluations.each do |evaluation|
        if evaluation.virtual
          has = true
          break
        end
      end

    end

    unless has

      self.polls.each do |poll|
        if poll.virtual
          has = true
          break
        end
      end

    end

    return has

  end

  def has_not_virtual_elements

    has = false

    self.units.each do |unit|
      unless unit.virtual
        has = true
        break
      end
    end

    unless has

      self.evaluations.each do |evaluation|
        unless evaluation.virtual
          has = true
          break
        end
      end

    end

    unless has

      self.polls.each do |poll|
        unless poll.virtual
          has = true
          break
        end
      end

    end

    return has

  end

  def has_virtual_units
    has = false

    self.units.each do |unit|
      if unit.virtual
        has = true
        break
      end
    end

    return has
  end

  def has_virtual_evaluations
    has = false

    self.evaluations.each do |eval|
      if eval.virtual
        has = true
        break
      end
    end

    return has
  end

  def course_training_characteristic(training_characteristic)
    self.course_training_characteristics.where('training_characteristic_id = ?', training_characteristic.id).first
  end

  def training_characteristic_value(training_characteristic)
    ctc = self.course_training_characteristic training_characteristic
    ctc.value if ctc
  end

  def formatted_training_characteristic_value(training_characteristic)
    ctc = self.course_training_characteristic training_characteristic
    ctc.formatted_value if ctc
  end

  def excel_formatted_training_characteristic_value(training_characteristic)
    ctc = self.course_training_characteristic training_characteristic
    ctc.excel_formatted_value if ctc
  end


end
