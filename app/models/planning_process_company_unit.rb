# == Schema Information
#
# Table name: planning_process_company_units
#
#  id                  :integer          not null, primary key
#  planning_process_id :integer
#  company_unit_id     :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  fecha_fin           :datetime
#

class PlanningProcessCompanyUnit < ActiveRecord::Base

  belongs_to :planning_process
  belongs_to :company_unit

  has_many :training_analysts
  has_many :company_unit_managers

  has_many :company_unit_area_managers
  has_many :area_superintendents

  has_many :planning_process_goals
  has_many :dncs
  has_many :planning_process_company_unit_areas
  has_many :planning_process_company_unit_area_corrections
  has_many :dncps

  before_validation :ajusta_fecha_fin

  validates :planning_process_id, presence: true
  validates :company_unit, presence: true
  #validates :fecha_fin, presence: true, on: :update
  validate :tope_fecha_fin

  attr_accessible :fecha_fin, :planning_process_id, :company_unit_id

  #default_scope joins(:company_unit).order('company_units.nombre ASC')

  def tope_fecha_fin

    if self.fecha_fin && self.fecha_fin > self.planning_process.fecha_fin
        errors.add(:fecha, 'mayor que '+(I18n.localize self.planning_process.fecha_fin, format: :day_snmonth_syear))
    end

  end

  def ajusta_fecha_fin

    if self.fecha_fin

      self.fecha_fin = self.fecha_fin+((24*60*60)-1).seconds

    end

  end

  def dncs_area(cua)

    self.dncs.find_all_by_company_unit_area_id(cua.id)

  end

  def dncps_area(cua)

    self.dncps.find_all_by_company_unit_area_id(cua.id)

  end

  def dncs_mes(mes)

    self.dncs.where('meses = ?', mes)

  end

  def dncps_mes(mes)

    self.dncps.where('meses = ?', mes)

  end

  def dncs_area_mes(cua,mes)

    self.dncs.where('company_unit_area_id = ? AND meses = ?',cua.id, mes)

  end

  def dncps_area_mes(cua,mes)

    self.dncps.where('company_unit_area_id = ? AND meses = ?',cua.id, mes)

  end

  def planning_process_company_unit_areas_area(cua)

    self.planning_process_company_unit_areas.find_by_company_unit_area_id(cua.id)

  end

  def dncs_area_training_type(cua,tt)

    self.dncs.where('company_unit_area_id = ? AND training_type_id = ?',cua.id, tt.id)

  end

  def dncps_area_training_type_mes(cua,tt,mes)

    self.dncps.where('company_unit_area_id = ? AND training_type_id = ? AND meses = ?',cua.id, tt.id, mes)

  end



  def area_corrections(cua)

    self.planning_process_company_unit_area_corrections.find_all_by_company_unit_area_id(cua.id)

  end

  def area_corrections_revisado(cua,rev)

    self.planning_process_company_unit_area_corrections.where('company_unit_area_id = ? AND revisado = ?', cua.id, rev)

  end


end
