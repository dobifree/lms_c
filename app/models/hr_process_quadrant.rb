# == Schema Information
#
# Table name: hr_process_quadrants
#
#  id                          :integer          not null, primary key
#  nombre                      :string(255)
#  hr_process_template_id      :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  color                       :string(255)
#  color_text                  :string(255)
#  color_fondo_suave           :string(255)
#  descripcion                 :string(255)
#  descripcion_detalle         :text
#  hr_process_dimension_g_x_id :integer
#  hr_process_dimension_g_y_id :integer
#  orden_grafico               :integer          default(1)
#

class HrProcessQuadrant < ActiveRecord::Base

  belongs_to :hr_process_template

  belongs_to :hr_process_dimension_g_x, class_name: 'HrProcessDimensionG', foreign_key: :hr_process_dimension_g_x_id
  belongs_to :hr_process_dimension_g_y, class_name: 'HrProcessDimensionG', foreign_key: :hr_process_dimension_g_y_id

  #has_many :hr_process_quadrant_gs, dependent: :destroy

  attr_accessible :nombre, :descripcion, :color, :color_text, :color_fondo_suave, :descripcion_detalle,
                  :hr_process_dimension_g_x_id, :hr_process_dimension_g_y_id, :orden_grafico

  #validates :nombre, presence: true, uniqueness: { case_sensitive: false, scope: :hr_process_template_id }
  validates :nombre, presence: true

  default_scope order('nombre, descripcion')

end
