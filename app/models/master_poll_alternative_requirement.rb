# == Schema Information
#
# Table name: master_poll_alternative_requirements
#
#  id                         :integer          not null, primary key
#  master_poll_alternative_id :integer
#  master_poll_question_id    :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class MasterPollAlternativeRequirement < ActiveRecord::Base

  belongs_to :master_poll_alternative
  belongs_to :master_poll_question

  attr_accessible :master_poll_alternative, :master_poll_alternative_id,
                  :master_poll_question, :master_poll_question_id

end
