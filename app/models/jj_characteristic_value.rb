# == Schema Information
#
# Table name: jj_characteristic_values
#
#  id                   :integer          not null, primary key
#  value_string         :string(255)
#  position             :integer
#  active               :boolean          default(TRUE)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  jj_characteristic_id :integer
#

class JjCharacteristicValue < ActiveRecord::Base
  belongs_to :jj_characteristic
  has_many :j_job_jj_characteristics

  attr_accessible :active, :position, :value_string, :jj_characteristic_id


  validates :value_string, presence: true, uniqueness: { scope: :jj_characteristic_id, case_sensitive: false }
  validates :position, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('active DESC, position, value_string')


end
