# == Schema Information
#
# Table name: training_types
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  color      :string(255)
#  active     :boolean          default(TRUE)
#

class TrainingType < ActiveRecord::Base

  has_many :dncs
  has_many :dncps
  has_many :courses

  attr_accessible :color, :nombre, :active

  validates :nombre, presence: true, uniqueness: { case_sensitive: false }
  validates :color, presence: true

  default_scope order('active DESC, nombre ASC')

  def self.active_types
    TrainingType.where('active = ?', true)
  end


end
