# == Schema Information
#
# Table name: user_delete_types
#
#  id          :integer          not null, primary key
#  position    :integer
#  description :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class UserDeleteType < ActiveRecord::Base
  attr_accessible :description, :position
end
