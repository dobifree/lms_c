# == Schema Information
#
# Table name: tracking_milestones
#
#  id                        :integer          not null, primary key
#  tracking_form_due_date_id :integer
#  tracking_form_state_id    :integer
#  registered_at             :datetime
#  registered_by_user_id     :integer
#  fulfilment_date           :datetime
#  comment                   :text
#  validated                 :boolean          default(FALSE)
#  validated_at              :datetime
#  validated_by_user_id      :integer
#  validated_comment         :text
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  replied_comment           :text
#  replied_by_user_id        :integer
#  replied_at                :datetime
#

class TrackingMilestone < ActiveRecord::Base
  belongs_to :tracking_form_due_date
  belongs_to :tracking_form_state
  belongs_to :registered_by_user, :class_name => 'User'
  belongs_to :validated_by_user, :class_name => 'User'
  belongs_to :replied_by_user, :class_name => 'User'
  attr_accessible :comment, :fulfilment_date,
                  :registered_at, :registered_by_user_id, :validated,
                  :validated_at, :validated_by_user_id, :validated_comment,
                  :replied_at, :replied_by_user_id, :replied_comment,
                  :tracking_form_state_id, :tracking_form_due_date_id

  validates :fulfilment_date, :presence => true
  validates :tracking_form_state_id, :presence => true

  default_scope { order(:fulfilment_date, :registered_at) }

  def is_registered_by_user_or_same_role?(user_id)
    participant = self.tracking_form_due_date.tracking_form_answer.tracking_form_instance.tracking_participant
    # validación positiva natural (no se rompe la validación si el usuario no es el que registró)
    return true if (self.registered_by_user_id == user_id)
    # si el usuario es el evaluador && el evaluado no es el que registró && yo no soy el que registró => se asume que fue mi predecesor (same role) (hubo cambio de attendant)
    return true if participant.attendant_user_id == user_id && self.registered_by_user_id != participant.subject_user_id && self.registered_by_user_id != user_id
    false
  end

end
