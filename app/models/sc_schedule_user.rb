# == Schema Information
#
# Table name: sc_schedule_users
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  sc_schedule_id         :integer
#  he_able                :boolean          default(FALSE)
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  active                 :boolean          default(FALSE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  registered_manually    :boolean          default(FALSE)
#  deactivated_manually   :boolean          default(FALSE)
#  active_since           :datetime
#  active_end             :datetime
#

class ScScheduleUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :sc_schedule

  belongs_to :registered_by_user, :class_name => 'User'
  belongs_to :deactivated_by_user, :class_name => 'User'
  belongs_to :activated_by_user, :class_name => 'User'


  attr_accessible :user_id,
                  :sc_schedule_id,
                  :he_able,
                  :active,
                  :active_since,
                  :active_end,
                  :registered_by_user_id,
                  :registered_at

  validates_presence_of :deactivated_at, :unless => :active?
  validates_presence_of :deactivated_by_user_id, :unless => :active?

  validate :active_sc_schedule
  validate :uniqueness_user_id
  validate :active_range, :if => :active_since?

  def prev
    schedule_user = ScScheduleUser.where(:user_id => self.user_id,
                                         :sc_schedule_id => self.sc_schedule_id,
                                         :active => true).first
  end

  def active_range
    error = false
    all_schedule_users = ScScheduleUser.where("active_since is not null and user_id = ? and active = true", self.user_id)
    all_schedule_users.each do |schedule_user|
      if schedule_user.active_since < self.active_end
        if schedule_user.active_end > self.active_since
          if self.id
            error = true unless schedule_user.id = self.id
          else
            error = true
          end
        end
      end
    end
    errors.add(:active_end, 'se traslapa con otra asociación') if error
  end

  def active_sc_schedule
    return unless active
    return unless sc_schedule_id
    return if sc_schedule.active
    errors.add(:sc_schedule_id, 'no está activo')
  end

  def uniqueness_user_id
    return if active_since
    schedule_user = ScScheduleUser.where("active_since is null and user_id = ? and active = true", user_id).first
    errors.add(:user_id, 'repetido para asosiación con tiempo indefinido') if schedule_user && (!self.id || schedule_user.id != self.id)
  end

  def active_since_time_formatted
    self.active_since.strftime('%H:%M') if active_since
  end

  def active_end_time_formatted
    self.active_end.strftime('%H:%M') if active_end
  end

end
