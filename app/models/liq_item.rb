# == Schema Information
#
# Table name: liq_items
#
#  id                  :integer          not null, primary key
#  liq_process_id      :integer
#  user_id             :integer
#  secu_cohade_id      :integer
#  description         :string(255)
#  monto               :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  monto_o             :integer
#  cod_empresa_payroll :integer
#  cod_cecos           :string(255)
#

class LiqItem < ActiveRecord::Base

  belongs_to :liq_process
  belongs_to :user
  belongs_to :secu_cohade

  attr_accessible :description, :monto, :monto_o, :cod_empresa_payroll,
                  :cod_cecos, :user_id, :secu_cohade_id, :liq_process_id

  validates :secu_cohade_id, uniqueness: { scope: [:liq_process_id, :user_id] }

end
