# == Schema Information
#
# Table name: brc_definers
#
#  id               :integer          not null, primary key
#  brc_process_id   :integer
#  user_id          :integer
#  user_validate_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class BrcDefiner < ActiveRecord::Base

  belongs_to :brc_process
  belongs_to :user
  belongs_to :user_validate, class_name: 'User'

  has_many :brc_definer_companies

  attr_accessible :brc_process_id, :user_validate_id, :user_id

  def define_company_id(company_id)
    self.brc_definer_companies.where('characteristic_value_id = ?', company_id).size == 1 ? true : false
  end

end
