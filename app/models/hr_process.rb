# == Schema Information
#
# Table name: hr_processes
#
#  id                                    :integer          not null, primary key
#  nombre                                :string(255)
#  abierto                               :boolean          default(TRUE)
#  fecha_inicio                          :datetime
#  fecha_fin                             :datetime
#  year                                  :integer
#  created_at                            :datetime         not null
#  updated_at                            :datetime         not null
#  hr_process_template_id                :integer
#  muestra_cajas                         :boolean
#  muestra_grupos                        :boolean
#  tiene_paso_feedback                   :boolean
#  calib_cuadrante                       :boolean
#  calib_dimension                       :boolean
#  muestra_cuadrantes_calib              :boolean
#  show_quien_evaluo                     :boolean          default(TRUE)
#  jefe_consulta_resultados              :boolean          default(FALSE)
#  jefe_ve_autoeval                      :boolean          default(FALSE)
#  ae_avisa_jefe_email                   :boolean          default(FALSE)
#  alias_jefe                            :string(255)      default("Supervisor")
#  alias_jefe_inverso                    :string(255)      default("Colaborador")
#  alias_jefe_plu                        :string(255)      default("Supervisores")
#  alias_jefe_inverso_plu                :string(255)      default("Colaboradores")
#  alias_par                             :string(255)      default("Par")
#  alias_par_inverso                     :string(255)      default("Par")
#  alias_par_plu                         :string(255)      default("Pares")
#  alias_par_inverso_plu                 :string(255)      default("Pares")
#  alias_sub                             :string(255)      default("Colaborador")
#  alias_sub_inverso                     :string(255)      default("Supervisor")
#  alias_sub_plu                         :string(255)      default("Colaboradores")
#  alias_sub_inverso_plu                 :string(255)      default("Supervisores")
#  alias_cli                             :string(255)      default("Cliente")
#  alias_cli_inverso                     :string(255)      default("Proveedor")
#  alias_cli_plu                         :string(255)      default("Clientes")
#  alias_cli_inverso_plu                 :string(255)      default("Proveedores")
#  alias_prov                            :string(255)      default("Proveedor")
#  alias_prov_inverso                    :string(255)      default("Cliente")
#  alias_prov_plu                        :string(255)      default("Proveedores")
#  alias_prov_inverso_plu                :string(255)      default("Clientes")
#  jefe_cons_estado_avance               :boolean          default(FALSE)
#  jefe_cons_res_final                   :boolean          default(FALSE)
#  positions_final_results_report        :string(255)
#  boss_check_results_while_assess       :boolean
#  boss_check_final_report_on_feedback   :boolean
#  step_feedback_confirmation            :boolean
#  step_feedback_confirmation_mail       :boolean
#  boss_check_final_results_while_assess :boolean
#  message_boss_while_assess             :text
#  jefe_consulta_resultados_rec          :boolean
#  show_user_profile                     :boolean
#  tiene_paso_feedback_modo              :integer          default(1)
#  label_1_t                             :string(255)
#  label_1_c                             :text
#  label_2_t                             :string(255)
#  label_2_c                             :text
#  label_3_t                             :string(255)
#  label_3_c                             :text
#

class HrProcess < ActiveRecord::Base

  belongs_to :hr_process_template
  has_many :hr_process_levels
  has_many :hr_process_users
  has_many :hr_process_user_rels
  has_many :hr_process_managers
  has_many :hr_process_evaluations
  has_many :hr_process_calibration_sessions

  attr_accessible :abierto, :fecha_fin, :fecha_inicio, :nombre, :year, :hr_process_template_id, :muestra_cajas,
                  :muestra_grupos, :tiene_paso_feedback, :calib_cuadrante, :calib_dimension,
                  :muestra_cuadrantes_calib, :show_quien_evaluo, :jefe_consulta_resultados, :jefe_ve_autoeval,
                  :ae_avisa_jefe_email,
                  :alias_jefe, :alias_jefe_inverso, :alias_jefe_plu, :alias_jefe_inverso_plu,
                  :alias_par, :alias_par_inverso, :alias_par_plu, :alias_par_inverso_plu,
                  :alias_sub, :alias_sub_inverso, :alias_sub_plu, :alias_sub_inverso_plu,
                  :alias_cli, :alias_cli_inverso, :alias_cli_plu, :alias_cli_inverso_plu, :alias_prov,
                  :alias_prov_inverso, :alias_prov_plu, :alias_prov_inverso_plu,
                  :jefe_cons_estado_avance, :jefe_cons_res_final, :positions_final_results_report,
                  :boss_check_results_while_assess, :boss_check_final_results_while_assess,
                  :message_boss_while_assess,
                  :boss_check_final_report_on_feedback,
                  :step_feedback_confirmation, :step_feedback_confirmation_mail, :jefe_consulta_resultados_rec,
                  :show_user_profile, :tiene_paso_feedback_modo,
                  :label_1_t, :label_1_c, :label_2_t, :label_2_c, :label_3_t, :label_3_c


  before_save :ajusta_fecha_fin

  validates :nombre, presence: true, uniqueness: { case_sensitive: false, scope: :year }
  validates :fecha_inicio, presence: true
  validates :fecha_fin, presence: true
  #validates :year, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: lms_time.strftime('%Y').to_i }, on: :create
  #validates :year, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: lms_time.strftime('%Y').to_i-1 }, on: :update
  validates :year, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: Time.now.utc.strftime('%Y').to_i-10 }



  default_scope order('nombre DESC')

  def ajusta_fecha_fin

    if self.fecha_fin

      self.fecha_fin = self.fecha_fin+((24*60*60)-1).seconds

    end

  end

  def level_by_nombre(nombre)

    self.hr_process_levels.each do |pl|

      if pl.nombre == nombre
        return pl
      end
    end

    return nil

  end

  def hr_process_users_order_by_apellidos_nombre

    self.hr_process_users.joins(:user).order('apellidos, nombre')

  end

  def hr_process_users_assessed_order_by_apellidos_nombre

    self.hr_process_users.where('finalizado = ?', true).joins(:user).order('apellidos, nombre')

  end

  def hr_process_managers_order_by_apellidos_nombre

    self.hr_process_managers.joins(:user).order('apellidos, nombre')

  end

  def hr_process_users_jefe_order_by_apellidos_nombre

    self.hr_process_users_order_by_apellidos_nombre.joins(:hr_process_evalua_rels).where('tipo = ?', 'jefe').uniq

  end

  def hr_process_users_evaluados
    self.hr_process_users.joins(:hr_process_es_evaluado_rels)
  end

  def hr_process_users_evaluados_order_by_apellidos_nombre
    self.hr_process_users.joins(:hr_process_es_evaluado_rels).joins(:user).uniq.order('apellidos, nombre')
  end

  def current_hr_process_evaluations
    self.hr_process_evaluations.where('vigente = ?', true)
  end

end
