# == Schema Information
#
# Table name: blog_forms
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  active      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class BlogForm < ActiveRecord::Base

  has_many :blog_form_items, dependent: :destroy
  has_many :blog_form_permissions, dependent: :destroy
  has_many :blog_entries
  accepts_nested_attributes_for :blog_form_items, allow_destroy: true
  accepts_nested_attributes_for :blog_form_permissions, allow_destroy: false
  attr_accessible :active, :description, :name, :blog_form_items_attributes, :blog_form_permissions_attributes

  validates :name, presence: true

  def current_permissions
    blog_form_permissions.where(active: true)
  end

  def reader_writer_profile(employee_user, reader_writer_user)
    case true
      when employee_user == reader_writer_user
        0 # employee
      when employee_user.boss == reader_writer_user
        1 # boss
      when reader_writer_user.es_jefe_de(employee_user)
        2 # superior
      else
        3 # others
    end
  end

  def profile_has_permission(permission_type, profile_id)
    case profile_id
      when 0 # employee
        current_permissions.where(perm_type: permission_type, employee: true).any?
      when 1 # boss
        current_permissions.where(perm_type: permission_type, boss: true).any?
      when 2 # superior
        current_permissions.where(perm_type: permission_type, superior: true).any?
      when 3 # others
        current_permissions.where(perm_type: permission_type, others: true).any?
    end
  end

  def self.user_have_permissions_to(employee_user, reader_writer_user)
    blog_form_with_access = []
    where(active: true).each do |blog_form|
      profile = blog_form.reader_writer_profile(employee_user, reader_writer_user)
      # puts blog_form.name.inspect
      # puts profile.inspect
      # puts blog_form.profile_has_permission([0,1], profile)
      # puts '-----------------------------------'
      blog_form_with_access << blog_form if blog_form.profile_has_permission([0,1], profile)
    end
    blog_form_with_access
  end



end
