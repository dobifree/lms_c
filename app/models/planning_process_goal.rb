# == Schema Information
#
# Table name: planning_process_goals
#
#  id                               :integer          not null, primary key
#  descripcion                      :text
#  planning_process_company_unit_id :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  numero                           :integer
#

class PlanningProcessGoal < ActiveRecord::Base

  belongs_to :planning_process_company_unit

  has_many :dnc_planning_process_goals
  has_many :dnc_planning_process_goal_ps

  attr_accessible :descripcion, :numero

  validates :descripcion, presence: true
  validates :numero, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('numero ASC')

end
