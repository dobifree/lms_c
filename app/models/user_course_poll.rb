# == Schema Information
#
# Table name: user_course_polls
#
#  id             :integer          not null, primary key
#  user_course_id :integer
#  poll_id        :integer
#  inicio         :datetime
#  finalizada     :boolean
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  fin            :datetime
#  ultimo_acceso  :datetime
#  from_date      :datetime
#  to_date        :datetime
#

class UserCoursePoll < ActiveRecord::Base

  belongs_to :user_course
  belongs_to :poll

  has_many :user_course_poll_questions, dependent: :destroy

  validates :user_course_id, presence: true
  validates :poll_id, presence: true

  attr_accessible :inicio, :poll, :fin, :finalizada, :ultimo_acceso, :user_course, :from_date, :to_date

end
