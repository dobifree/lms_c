# == Schema Information
#
# Table name: sel_step_results
#
#  id                            :integer          not null, primary key
#  sel_step_characteristic_id    :integer
#  sel_option_id                 :integer
#  sel_step_candidate_id         :integer
#  registered_by_user_id         :integer
#  non_discrete_result           :integer
#  comment                       :text
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  registered_at                 :datetime
#  sel_process_characteristic_id :integer
#

class SelStepResult < ActiveRecord::Base
  belongs_to :sel_step_characteristic
  belongs_to :sel_option
  belongs_to :sel_step_candidate
  belongs_to :registered_by_user, class_name: 'User'
  belongs_to :sel_process_characteristic
  has_one :sel_step, through: :sel_step_characteristic
  has_one :sel_characteristic, through: :sel_step_characteristic
  attr_accessible :comment, :non_discrete_result, :registered_by_user_id, :registered_at, :sel_process_characteristic_id,
      :sel_step_characteristic, :sel_step_characteristic_id,
      :sel_step_candidate, :sel_step_candidate_id,
      :sel_option, :sel_option_id,
      :sel_step,
      :sel_characteristic,
      :sel_process_characteristic

end
