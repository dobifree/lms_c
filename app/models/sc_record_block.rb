# == Schema Information
#
# Table name: sc_record_blocks
#
#  id                     :integer          not null, primary key
#  sc_record_id           :integer
#  sc_block_id            :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#

class ScRecordBlock < ActiveRecord::Base
  belongs_to :sc_record
  belongs_to :sc_block

  attr_accessible :sc_block_id,
                  :sc_record_id

  validates :sc_block_id, uniqueness: {scope: :sc_record_id}, :if => :active?

end
