# == Schema Information
#
# Table name: bp_forms
#
#  id                      :integer          not null, primary key
#  bp_event_id             :integer
#  name                    :string(255)
#  description             :text
#  active                  :boolean          default(TRUE)
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class BpForm < ActiveRecord::Base
  belongs_to :bp_event
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id

  has_many :bp_items


  has_many :lic_requests
  has_many :lic_events


  accepts_nested_attributes_for :bp_items, :allow_destroy => true

  attr_accessible :bp_event_id,
                  :name,
                  :description,
                  :active,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id,
                  :bp_items_attributes

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_admin_id, presence: true, unless: :active?
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  def active_items
    bp_items.where(active: true).reorder('position ASC')
  end

  def is_there_a_file?
    active_items.each { |item| return true if item.item_file? }
    false
  end
end