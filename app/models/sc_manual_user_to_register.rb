# == Schema Information
#
# Table name: sc_manual_user_to_registers
#
#  id                        :integer          not null, primary key
#  sc_process_id             :integer
#  user_id                   :integer
#  he_ct_module_privilage_id :integer
#  registered_at             :datetime
#  registered_by_user_id     :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#

class ScManualUserToRegister < ActiveRecord::Base
  belongs_to :sc_process
  belongs_to :user
  belongs_to :he_ct_module_privilage
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :registered_at,
                  :registered_by_user_id,
                  :sc_process_id,
                  :user_id,
                  :he_ct_module_privilage_id

  validates :sc_process_id, presence: true
  validates :he_ct_module_privilage_id, presence: true
  validates :user_id, presence: true
  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def self.of_user(id)
    register = HeCtModulePrivilage.where(user_id: id, active: true, recorder: true).first
    return ScManualUserToRegister.where(user_id: -1)  unless register
    ScManualUserToRegister.where(he_ct_module_privilage_id: register.id)
  end
  def self.process(id); ScManualUserToRegister.where(sc_process_id: id) end
  def self.of_register(id); ScManualUserToRegister.where(he_ct_module_privilage_id: id) end

  def self.gimme_users_id(relations)
    users_id = []
    relations.each { |rel| users_id << rel.user_id }
    users_id
  end

end
