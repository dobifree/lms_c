# == Schema Information
#
# Table name: user_ct_module_privileges
#
#  id                                   :integer          not null, primary key
#  user_id                              :integer
#  created_at                           :datetime         not null
#  updated_at                           :datetime         not null
#  sel_make_requirement                 :boolean          default(FALSE)
#  mpi_view_users                       :boolean          default(FALSE)
#  mpi_view_users_data                  :boolean          default(FALSE)
#  mpi_view_users_training_history      :boolean          default(FALSE)
#  mpi_view_users_pe_history            :boolean          default(FALSE)
#  mpi_view_users_informative_documents :boolean          default(FALSE)
#  um_manage_users                      :boolean          default(FALSE)
#  um_manage_users_data                 :boolean          default(FALSE)
#  um_manage_users_password             :boolean          default(FALSE)
#  sel_approve_requirements             :boolean          default(FALSE)
#  um_manage_users_email                :boolean          default(FALSE)
#  um_query_reports                     :boolean          default(FALSE)
#  lms_define                           :boolean          default(FALSE)
#  lms_enroll                           :boolean          default(FALSE)
#  lms_manage_enroll                    :boolean          default(FALSE)
#  lms_reports                          :boolean          default(FALSE)
#  um_create_users                      :boolean          default(FALSE)
#  um_delete_users                      :boolean          default(FALSE)
#  um_edit_users                        :boolean          default(FALSE)
#  um_manage_users_photo                :boolean          default(FALSE)
#

class UserCtModulePrivileges < ActiveRecord::Base

  belongs_to :user
  has_many :user_ct_module_privileges_um_custom_reports

  attr_accessible :sel_make_requirement,
                  :sel_approve_requirements,
                  :mpi_view_users,
                  :mpi_view_users_data,
                  :mpi_view_users_training_history,
                  :mpi_view_users_pe_history,
                  :mpi_view_users_informative_documents,
                  :um_manage_users,
                  :um_manage_users_data,
                  :um_manage_users_password,
                  :um_manage_users_email,
                  :um_query_reports,
                  :um_create_users,
                  :um_delete_users,
                  :lms_define,
                  :lms_enroll,
                  :lms_manage_enroll,
                  :lms_reports,
                  :user_id,
                  :um_edit_users,
                  :um_manage_users_photo

  def user_ct_module_privileges_um_custom_report(um_custom_report)
    self.user_ct_module_privileges_um_custom_reports.where('um_custom_report_id = ?', um_custom_report.id).first
  end

end
