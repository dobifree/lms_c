# == Schema Information
#
# Table name: pe_slider_groups
#
#  id               :integer          not null, primary key
#  position         :integer
#  name             :string(255)
#  description      :text
#  text_color       :string(255)
#  bg_color         :string(255)
#  min_sign         :string(255)
#  min_value        :integer
#  max_sign         :string(255)
#  max_value        :integer
#  pe_evaluation_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PeSliderGroup < ActiveRecord::Base

  belongs_to :pe_evaluation

  attr_accessible :bg_color, :description, :max_sign, :max_value, :min_sign, :min_value, :name, :position, :text_color, :pe_evaluation_id

  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :name, presence: true
  validates :text_color, presence: true
  validates :min_sign, presence: true
  validates :min_value, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :max_sign, presence: true
  validates :max_value, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  default_scope order('position, name')

end
