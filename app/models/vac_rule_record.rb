# == Schema Information
#
# Table name: vac_rule_records
#
#  id                           :integer          not null, primary key
#  vac_request_id               :integer
#  bp_condition_vacation_id     :integer
#  days_begin                   :date
#  days_end                     :date
#  days_real_end                :date
#  paid                         :boolean
#  paid_at                      :datetime
#  paid_by_user_id              :integer
#  refund_pending               :boolean          default(FALSE)
#  refund_requested_at          :datetime
#  refund_requested_by_user_id  :integer
#  refund_done_at               :datetime
#  refund_done_by_user_id       :integer
#  active                       :boolean          default(TRUE)
#  deactivated_at               :datetime
#  deactivated_by_user_id       :integer
#  registered_at                :datetime
#  registered_by_user_id        :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  available                    :boolean          default(TRUE)
#  available_changed_at         :datetime
#  available_changed_by_user_id :integer
#  paid_description             :text
#

class VacRuleRecord < ActiveRecord::Base
  belongs_to :vac_request
  belongs_to :bp_condition_vacation

  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :money_paid_by_user, class_name: 'User', foreign_key: :money_paid_by_user_id
  belongs_to :refund_done_by_user, class_name: 'User', foreign_key: :refund_done_by_user_id
  belongs_to :refund_requested_by_user, class_name: 'User', foreign_key: :refund_requested_by_user_id

  attr_accessible :vac_request_id,
                  :bp_condition_vacation_id,
                  :days_begin,
                  :days_end,
                  :days_real_end,
                  :money_paid,
                  :money_paid_at,
                  :money_paid_by_user_id,
                  :refund_pending,
                  :refund_requested_at,
                  :refund_requested_by_user_id,
                  :refund_done_at,
                  :refund_done_by_user_id,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id,
                  :paid_description,
                  :paid,
                  :paid_by_user_id

  validate :group_concordance

  def bonus_full_name
    bonus_days = bp_condition_vacation.bonus_days ? bp_condition_vacation.bonus_days.to_s+' días' : ''
    bonus_money = bp_condition_vacation.bonus_money.to_s+' '+bp_condition_vacation.bonus_currency
    bp_condition_vacation.bp_rule_vacation.name+': '+bonus_days+' '+bonus_money
  end

  def self.taken_by(user_id, bp_conditions_id)
    if bp_conditions_id && bp_conditions_id.size > 0
      VacRuleRecord.where(active: true, paid: [nil, true], bp_condition_vacation_id: bp_conditions_id).joins(:vac_request).where(vac_requests: { user_id: user_id, active: true}).all
    else
      VacRuleRecord.where(active: true, paid: [nil, true], bp_condition_vacation_id: bp_conditions_id).joins(:vac_request).where(vac_requests: { user_id: user_id, active: true}).all
    end
  end

  def paid_colour
    case paid
      when false
        5
      when true
        2
      when nil
        0
      else
        0
    end
  end

  def paid_text
    case paid
      when false
        'No'
      when true
        'Sí '
      when nil
        if bp_condition_vacation.bonus_money
          'Pendiente de pago'
        else
          'Pendiente'
        end
      else
        ''
    end
  end

  def available
    case available
      when false
        'No se pagará'
      when true
        'Pendiente de pago'
      else
        ''
    end
  end

  def available_colour
    case available
      when false
        5
      when true
        2
      else
        0
    end
  end

  private

  def group_concordance
    group = bp_condition_vacation.bp_rule_vacation.bp_group
    user_group = BpGroup.user_active_group(vac_request.begin, vac_request.user_id)
    errors.add(:bp_condition_vacation_id, 'no pertenece a un grupo activo para el inicio de la solicitud') unless group.active?(vac_request.begin)
    errors.add(:bp_condition_vacation_id, 'no concuerda con un usuario sin grupo activo para el inicio de la solicitud') unless user_group
    errors.add(:bp_condition_vacation_id, 'no concuerda con el grupo asociado al usuario de la solicitud') if user_group && group.id != user_group.id
  end
end
