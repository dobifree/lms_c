# == Schema Information
#
# Table name: users
#
#  id                      :integer          not null, primary key
#  codigo                  :string(255)
#  email                   :string(255)
#  password_digest         :string(255)
#  nombre                  :string(255)
#  apellidos               :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  codigo_rec_pass         :string(255)
#  fecha_rec_pass          :string(255)
#  jefe_capacitacion       :boolean          default(FALSE)
#  foto                    :string(255)
#  activo                  :boolean          default(TRUE)
#  requiere_cambio_pass    :boolean          default(FALSE)
#  jefe_biblioteca         :boolean          default(FALSE)
#  node_id                 :integer
#  j_job_id                :integer
#  perpetual_active        :boolean
#  employee                :boolean          default(TRUE)
#  private_key             :text
#  public_key              :text
#  active_directory_id     :string(255)
#  he_able                 :boolean          default(FALSE)
#  j_cost_center_id        :integer
#  delete_date             :date
#  user_delete_reason_id   :integer
#  delete_comment          :text
#  from_date               :date
#  last_name_1             :string(255)
#  last_name_2             :string(255)
#  user_delete_type_id     :integer
#  show_azure_landing_page :boolean          default(TRUE)
#  flexible_sc             :boolean          default(FALSE)
#  secu_external           :boolean
#

require 'valid_email'

class User < ActiveRecord::Base

  extend UsersModule
  include UserCoursesHelper
  include BpModule

  include ActiveModel::Dirty

  belongs_to :node_to_which_belongs, class_name: 'Node', foreign_key: 'node_id' #puesto del que depende
  has_many :owned_nodes, class_name: 'Node', foreign_key: 'user_id' #puestos que tiene
  has_many :members, through: :owned_nodes

  belongs_to :j_cost_center

  belongs_to :user_delete_reason
  belongs_to :user_delete_type

  belongs_to :j_job
  has_many :user_j_jobs
  has_many :j_jobs, through: :user_j_jobs

  has_many :enrollments
  has_many :programs, through: :enrollments

  has_many :user_characteristics
  has_many :characteristics, through: :user_characteristics
  has_many :user_characteristic_records

  has_many :folder_owners

  has_many :training_analysts
  has_many :company_unit_managers

  has_many :company_unit_area_managers
  has_many :area_superintendents

  has_many :dnc_students

  has_many :support_tickets

  has_many :program_course_managers
  has_many :program_inspectors
  has_many :programs_inspector, through: :program_inspectors, source: 'program'

  has_many :hr_process_users
  has_many :hr_process_managers

  has_many :hr_process_assessment_ds, through: :hr_process_users

  has_many :user_courses, through: :enrollments

  has_many :hr_process_calibration_session_cs

  has_many :pe_members
  has_many :pe_managers
  has_many :pe_reporters
  has_many :pe_member_characteristics
  has_many :pe_member_observers, foreign_key: 'observer_id'

  has_many :pe_cal_committees

  has_many :kpi_dashboard_managers
  has_many :managed_kpi_dashboards, through: :kpi_dashboard_managers, source: :kpi_dashboard

  has_many :kpi_dashboard_guests
  has_many :guest_kpi_dashboards, through: :kpi_dashboard_guests, source: :kpi_dashboard

  has_many :kpi_set_guests
  has_many :kpi_sets, through: :kpi_set_guests
  has_many :guest_kpi_dashboards_from_set, through: :kpi_sets, source: :kpi_dashboard, :uniq => true

  has_many :kpi_indicator_rols, dependent: :destroy

  has_one :user_ct_module_privileges, dependent: :destroy

  has_many :elec_historic_characteristics, dependent: :nullify

  has_many :log_logins

  has_many :dnc_requirements

  has_many :blog_entries

  has_many :tracking_evaluations, :class_name => 'TrackingParticipant', :foreign_key => :subject_user_id

  has_many :ben_user_cellphone_processes
  has_many :ben_cellphone_numbers
  has_many :ben_user_custom_reports


  has_many :program_course_instances, :foreign_key => :creator_id

  has_many :sc_user_processes
  has_many :sc_records
  has_many :sc_register_to_validates
  has_many :sc_user_to_registers
  has_many :sc_user_to_validates
  has_many :he_ct_module_privilages
  has_many :sc_log_records
  has_many :sc_documentations
  has_many :calendar_dates
  has_many :bp_groups_users
  has_many :bp_general_rules_vacations


  has_many :vac_approvers
  has_many :vac_user_to_approves
  has_many :vac_records

  has_many :lic_approvers
  has_many :lic_user_to_approves
  has_many :vac_accumulateds

  has_many :brc_members
  has_many :brc_users_define, class_name: 'BrcMember', foreign_key: 'user_define_id'
  has_many :brc_users_validate, class_name: 'BrcMember', foreign_key: 'user_validate_id'

  has_many :brg_group_users

  attr_accessible :apellidos, :codigo, :codigo_rec_pass, :email,
                  :fecha_rec_pass, :jefe_capacitacion, :jefe_biblioteca,
                  :nombre,
                  :password, :password_confirmation, :foto, :activo,
                  :requiere_cambio_pass, :j_job_id, :perpetual_active, :employee,
                  :delete_date, :delete_comment, :user_delete_reason_id, :user_delete_type_id,
                  :from_date, :j_cost_center_id, :last_name_1, :last_name_2, :secu_external

  has_secure_password

  #before_save { self.codigo.upcase! unless self.codigo.nil? }
  #before_save { self.apellidos.upcase! unless self.apellidos.nil? }
  #before_save { self.nombre.upcase! unless self.nombre.nil? }

  before_validation :fix_last_name

  before_save { self.email.downcase! unless self.email.nil? }
  before_save { self.email = nil if self.email.blank? }
  before_create :set_pgp
  
  validates :codigo, presence: true, uniqueness: { case_sensitive: false }
	validates :nombre, presence: true
  validates :apellidos, presence: true
  #validates :active_directory_id, presence: true, uniqueness: { case_sensitive: false }


  #VALID_EMAIL_REGEX = /\A[\w+\-.]+@[A-Z\d\-.]+\.[A-Z]+\z/i
  #validates :email, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }, allow_blank: true

  #validates :email, email: true, uniqueness: { case_sensitive: false }, allow_blank: true
  validates :email, email: true, allow_blank: true
  
  validates :password, length: { minimum: 1 }, allow_blank: true

  default_scope order('apellidos ASC, nombre ASC')

  def self.all_anniversary(this_date)
    this_date.to_date
    users = User.where(activo = 1)
    users_ann = []
    users.each do |user|
      general_rules = BpGeneralRulesVacation.user_active(this_date.to_datetime, user.id)
      next unless general_rules
      ann = user.from_date_inicio_laboral
      next unless ann
      ann = ann.change(year: this_date.year)
      next unless ann.to_date == this_date
     users_ann << user
    end
    users_ann
  end

  def user_cod_formatted
    return codigo.insert(-2, '-')
  end

  def manager_of_module(module_cod)
    ct_module = CtModule.where('cod = ? AND active = ?', module_cod, true).first
    unless ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', id).count == 1
      return false
    end
    true
  end

  def full_name
    self.apellidos+' '+self.nombre
  end

  def comma_full_name
    self.apellidos+', '+self.nombre
  end

  def characteristic(nombre)
    self.user_characteristics.joins(:characteristic).where('nombre = ?', nombre).first
  end

  def characteristic_by_id(id_c)
    self.user_characteristics.joins(:characteristic).where('characteristics.id = ?', id_c).first
  end

  def user_characteristic(characteristic)
    self.user_characteristics.where('characteristic_id = ?', characteristic.id).first
  end

  def user_characteristics_registered(characteristic)
    self.user_characteristics.where('characteristic_id = ?', characteristic.id)
  end

  def characteristic_value(characteristic)

    if characteristic.data_type_id != 8 && characteristic.data_type_id != 9 && characteristic.data_type_id != 10 && characteristic.data_type_id != 12 && characteristic.data_type_id != 13 && characteristic.data_type_id != 14 && characteristic.data_type_id != 15

      uc = self.user_characteristics.where('characteristic_id = ?', characteristic.id).first
      uc.value if uc

    elsif characteristic.data_type_id == 8

      self.boss.codigo if self.boss

    elsif characteristic.data_type_id == 9

      self.boss.nombre+' '+self.boss.apellidos if self.boss

    elsif characteristic.data_type_id == 10

      boss_char = characteristic.boss_characteristic
      boss = self.boss

      boss.characteristic_value(boss_char) if boss && boss_char

    elsif characteristic.data_type_id == 12

      self.codigo

    elsif characteristic.data_type_id == 13

      first_owned_node = self.first_owned_node

      first_owned_node.nombre if first_owned_node

    elsif characteristic.data_type_id == 14

      first_owned_node = self.first_owned_node

      if first_owned_node

        cv = characteristic.active_characteristic_values.where('value_string = ?', first_owned_node.nombre ).first

        cv.id if cv

      end

    elsif characteristic.data_type_id == 15
      jcc = self.j_cost_center
      if jcc
        jcc.cc_id+' '+jcc.name
      end

    elsif characteristic.data_type_id == 16
      bpg = BpGroup.user_active_group(lms_time, self.id)
      bpg.name if bpg
    end

  end

  def self.fulfill_this_value_char(value, char, users_1)
    users = []
    users_1.each do |user|
      user_val = user.characteristic_value(char)
      next unless user_val == value
      users << user
    end
    users
  end

  def self.fulfill_this_value_char_1(values, char, users_1)
    users = []
    User.where(activo: true, id: users_id).each do |user|
      user_val = user.characteristic_value(char)
      users_1 = []
      values.each do |val|
        next unless val.to_i == user_val.to_i
        users_1 << user
      end
      users << users_1
    end
    users
  end

  def self.fulfill_this_value_char_2(values, char, users_id)
    users = []
    users_all = User.where(activo: true, id: users_id)
    values.each do |val|
      users_1 = []
      users_all.each do |user|
        user_val = user.characteristic_value(char)
        next unless val.to_i == user_val.to_i
        users_1 << user
        break
      end
      users << users_1
    end
    users
  end



  def formatted_characteristic_value(characteristic, company = nil, lms_time = nil)

    if characteristic.data_type_id != 8 && characteristic.data_type_id != 9 && characteristic.data_type_id != 10 && characteristic.data_type_id != 12 && characteristic.data_type_id != 13 && characteristic.data_type_id != 14 && characteristic.data_type_id != 15 && characteristic.data_type_id != 16

      uc = self.user_characteristics.where('characteristic_id = ?', characteristic.id).first
      uc.formatted_value(company) if uc

    elsif characteristic.data_type_id == 8

      self.boss.codigo if self.boss

    elsif characteristic.data_type_id == 9

      self.boss.nombre+' '+self.boss.apellidos if self.boss

    elsif characteristic.data_type_id == 10

      boss_char = characteristic.boss_characteristic
      boss = self.boss

      boss.formatted_characteristic_value(boss_char) if boss && boss_char

    elsif characteristic.data_type_id == 12

      self.codigo

    elsif characteristic.data_type_id == 13

      first_owned_node = self.first_owned_node

      first_owned_node.nombre if first_owned_node

    elsif characteristic.data_type_id == 14

      first_owned_node = self.first_owned_node

      first_owned_node.nombre if first_owned_node

    elsif characteristic.data_type_id == 15
      jcc = self.j_cost_center
      if jcc
        jcc.cc_id+' '+jcc.name
      end
    elsif characteristic.data_type_id == 16
      bpg = BpGroup.user_active_group(lms_time, self.id)
      bpg.description if bpg
    end

  end

  def decrypted_value(characteristic, company)
    dv = nil
    if characteristic.data_type_id == 0 || characteristic.data_type_id == 1 || characteristic.data_type_id == 3 || characteristic.data_type_id == 4
      uc = self.user_characteristics.where('characteristic_id = ?', characteristic.id).first
      dv = uc.decrypted_value(company) if uc
    end
    dv
  end


  def excel_formatted_characteristic_value(characteristic)

    if characteristic.data_type_id != 8 && characteristic.data_type_id != 9 && characteristic.data_type_id != 10 && characteristic.data_type_id != 12 && characteristic.data_type_id != 13 && characteristic.data_type_id != 14 && characteristic.data_type_id != 15

      uc = self.user_characteristics.where('characteristic_id = ?', characteristic.id).first
      uc.excel_formatted_value if uc

    elsif characteristic.data_type_id == 8

      self.boss.codigo if self.boss

    elsif characteristic.data_type_id == 9

      self.boss.nombre+' '+self.boss.apellidos if self.boss

    elsif characteristic.data_type_id == 10

      boss_char = characteristic.boss_characteristic
      boss = self.boss

      boss.formatted_characteristic_value(boss_char) if boss && boss_char

    elsif characteristic.data_type_id == 12

      self.codigo

    elsif characteristic.data_type_id == 13

      first_owned_node = self.first_owned_node

      first_owned_node.nombre if first_owned_node

    elsif characteristic.data_type_id == 14

      first_owned_node = self.first_owned_node

      first_owned_node.nombre if first_owned_node

    elsif characteristic.data_type_id == 15
      jcc = self.j_cost_center
      if jcc
        jcc.cc_id+' '+jcc.name
      else
        ''
      end

    end

  end

  def user_characteristic_records_by_characteristic(characteristic)
    self.user_characteristic_records.where('characteristic_id = ?', characteristic.id)
  end

  def last_user_characteristic_record(characteristic)
    self.user_characteristic_records.where('characteristic_id = ?', characteristic.id).first
  end

  def last_open_user_characteristic_record(characteristic)
    self.user_characteristic_records.where('characteristic_id = ? AND to_date IS NULL', characteristic.id).first
  end

  def add_user_chatacteristic_record(characteristic, value, from_date, to_date, created_by_user = nil, finished_by_user = nil, created_by_admin = nil, finished_by_admin = nil, company)

    new_user_characteristic_record = self.user_characteristic_records.build
    new_user_characteristic_record.characteristic = characteristic
    new_user_characteristic_record.value = value
    new_user_characteristic_record.value_string = value
    new_user_characteristic_record.from_date = from_date
    new_user_characteristic_record.to_date = to_date
    new_user_characteristic_record.created_by_user = created_by_user
    new_user_characteristic_record.finished_by_user = finished_by_user
    new_user_characteristic_record.created_by_admin = created_by_admin
    new_user_characteristic_record.finished_by_admin = finished_by_admin

    new_user_characteristic_record.save

  end

  def add_user_characteristic(characteristic, new_value, current_date, created_by_user = nil, finished_by_user = nil, created_by_admin = nil, finished_by_admin = nil, company)

    error = true

    uc = nil

    if new_value

      uc = self.user_characteristics.build
      uc.characteristic = characteristic

      uc.update_value new_value, company
      if uc.save
        new_user_characteristic_record = self.user_characteristic_records.build
        new_user_characteristic_record.clone_from_user_characteristic uc, current_date
        new_user_characteristic_record.created_by_user = created_by_user
        new_user_characteristic_record.created_by_admin = created_by_admin
        new_user_characteristic_record.save
        error = false
      end

    end

    return error, uc

  end

  def update_user_characteristic(characteristic, new_value, current_date, created_by_user = nil, finished_by_user = nil, created_by_admin = nil, finished_by_admin = nil, company)

    begin

      if new_value

        new_value = new_value.to_s.strip if characteristic.data_type_id == 0 #string
        new_value = new_value.to_s.strip if characteristic.data_type_id == 1 #text
        new_value = new_value.to_date if characteristic.data_type_id == 2 #date
        new_value = new_value.to_i if characteristic.data_type_id == 3 #int
        new_value = new_value.to_f if characteristic.data_type_id == 4 #float
        new_value = new_value.to_i if characteristic.data_type_id == 5 #list
        new_value = new_value.to_s.strip if characteristic.data_type_id == 7 #image
        new_value = new_value.to_s.strip if characteristic.data_type_id == 13 #node_string
        new_value = new_value.to_i if characteristic.data_type_id == 14 #node_list
        new_value = new_value.to_s.strip if characteristic.data_type_id == 15 #cc
        new_value = new_value.to_i if characteristic.data_type_id == 9 #boss
        new_value = new_value.to_i if characteristic.data_type_id == 16 #benefit

      end

      if characteristic.data_type_id == 11 || characteristic.register_characteristic
        #característica de tipo register o característica que es componente de una característica de tipo register, sólo añade y no registra historial

        error = true

        uc = nil

        if new_value

          uc = self.user_characteristics.build
          uc.characteristic = characteristic

          uc.update_value new_value, company
          error = false if uc.save

        end

      elsif characteristic.data_type_id == 15
        #centro de costo

        jcc = JCostCenter.find_by_cc_id new_value

        if jcc
          self.j_cost_center = jcc
          self.save
        end

      elsif characteristic.data_type_id == 9
        #jefe

        user_boss = User.find(new_value)

        unless user_boss.owned_nodes.first

          node = user_boss.owned_nodes.build
          node.nombre = ''
          node.save

        end

        self.node_to_which_belongs = user_boss.owned_nodes.first

        self.save

      elsif characteristic.data_type_id == 16
        #grupo beneficios
        create_or_update_group_assoc(created_by_user, current_date, self.id, new_value)
      elsif characteristic.data_type_id != 8 && characteristic.data_type_id != 9 && characteristic.data_type_id != 10 && characteristic.data_type_id != 12 && characteristic.data_type_id != 15
        #características que no son referencia a código de jefe, nombre de jefe, característica de jefe o a codigo de usuario

        error = false

        uc = self.user_characteristic characteristic

        if uc

          last_open_uc_record = self.last_open_user_characteristic_record characteristic

          unless last_open_uc_record

            #este caso se dará cuando no habría registro histórico, primer uso de user_characteristic_records

            last_open_uc_record = self.user_characteristic_records.build
            last_open_uc_record.clone_from_user_characteristic uc, current_date
            last_open_uc_record.save

          end

          if new_value

            make_change = false

            if characteristic.data_type_id == 0 && new_value != uc.value
              make_change = true
            elsif characteristic.data_type_id == 1 && new_value != uc.value
              make_change = true
            elsif characteristic.data_type_id == 2 && new_value != uc.value
              make_change = true
            elsif characteristic.data_type_id == 3 && new_value != uc.value
              make_change = true
            elsif characteristic.data_type_id == 4 && new_value != uc.value
              make_change = true
            elsif characteristic.data_type_id == 5 && new_value != uc.value
              make_change = true
            elsif characteristic.data_type_id == 6
              make_change = true
            elsif characteristic.data_type_id == 7 && new_value != uc.value
              make_change = true
            elsif characteristic.data_type_id == 13 && new_value != uc.value
              make_change = true
            elsif characteristic.data_type_id == 14 && new_value != uc.value
              make_change = true
            end

            if make_change

              uc.update_value new_value, company

              begin

                if uc.save

                  if characteristic.data_type_id == 13

                    self.update_first_owned_node(new_value)

                  elsif characteristic.data_type_id == 14

                    cv = characteristic.active_characteristic_values.where('id = ?', new_value).first
                    self.update_first_owned_node(cv.value_string) if cv

                  end

                  if last_open_uc_record

                    last_open_uc_record.to_date = current_date-1.second
                    last_open_uc_record.finished_by_user = finished_by_user
                    last_open_uc_record.finished_by_admin = finished_by_admin
                    last_open_uc_record.save

                  end

                  new_user_characteristic_record = self.user_characteristic_records.build
                  new_user_characteristic_record.clone_from_user_characteristic uc, current_date
                  new_user_characteristic_record.created_by_user = created_by_user
                  new_user_characteristic_record.created_by_admin = created_by_admin
                  new_user_characteristic_record.save

                else
                  error = true
                end

              rescue
                error = true
              end

            end

          else

            if characteristic.data_type_id != 6
              # != file
              if last_open_uc_record

                last_open_uc_record.to_date = current_date-1.second
                last_open_uc_record.finished_by_user = finished_by_user
                last_open_uc_record.finished_by_admin = finished_by_admin
                last_open_uc_record.save

              end

              uc.destroy

              if characteristic.data_type_id == 13

                self.update_first_owned_node(new_value)

              elsif characteristic.data_type_id == 14

                self.update_first_owned_node(new_value)

              end

            end

          end

        else

          if new_value

            uc = self.user_characteristics.build
            uc.characteristic = characteristic

            uc.update_value new_value, company
            if uc.save

              if characteristic.data_type_id == 13

                self.update_first_owned_node(new_value)

              elsif characteristic.data_type_id == 14

                cv = characteristic.active_characteristic_values.where('id = ?', new_value).first

                self.update_first_owned_node(cv.value_string) if cv

              end

              new_user_characteristic_record = self.user_characteristic_records.build
              new_user_characteristic_record.clone_from_user_characteristic uc, current_date
              new_user_characteristic_record.created_by_user = created_by_user
              new_user_characteristic_record.created_by_admin = created_by_admin
              new_user_characteristic_record.save

            else
              error = true
            end

          end

        end

      end

    rescue

      error = true

      uc = nil

    end

    if uc && !error && characteristic.has_children && characteristic.data_type_id == 5

      CharacteristicValueParent.where('characteristic_parent_id = ? AND characteristic_value_parent_id = ?', characteristic.id, new_value).each do |cvp|

        n_new_value = cvp.characteristic_value_id
        n_characteristic = cvp.characteristic_value.characteristic

        self.update_user_characteristic(n_characteristic, n_new_value, current_date, created_by_user, finished_by_user, created_by_admin, finished_by_admin, company)

      end

    end

    return error, uc

  end

  def set_foto

    self.foto = self.id.to_s+'_'+SecureRandom.hex(5)+'.jpg'

  end

  def enrollments_order_by_program
    self.enrollments.joins(:program).order('orden ASC, nombre ASC')
  end

  def numero_cursos_por_llevar(fecha_hoy)

    n = 0

    self.enrollments.each do |enrollment|
      if enrollment.program.activo
        enrollment.level.program_courses.where('hidden_in_user_courses_list = ?', false).each do |program_course|
          estado, estado_boton, user_course, user_courses = user_course_status(enrollment, program_course, fecha_hoy)
          n += 1 if estado_boton == :iniciar
        end
      end
    end

    n

  end

  def es_jefe_de(user)

    permiso = false

    n = user.node_to_which_belongs

    while n

      if n.owner.id == self.id
        permiso = true
        break
      else
        n = n.node
      end

    end

    permiso

  end

  def boss_chain

    c = Array.new

    u = self

    while u.node_to_which_belongs && u.node_to_which_belongs.owner

      c.push u.node_to_which_belongs.owner

      u = u.node_to_which_belongs.owner

    end

    return c.reverse

  end

  def boss

    self.node_to_which_belongs ? self.node_to_which_belongs.owner : nil

  end

  def first_owned_node
    self.owned_nodes.first
  end

  def update_first_owned_node(name)

    node = self.first_owned_node

    node = self.owned_nodes.build unless node

    node.nombre = name

    node.save

  end

=begin
  def update_j_job(name)

    if name.blank?

      self.j_job = nil
      self.save

    else

      j_job = JJob.where('name = ? and current = ?', name, true).first

      unless j_job

        j_job = JJob.new
        j_job.name = name
        j_job.j_job_level = JJobLevel.first
        j_job.version = 1
        j_job.current = true
        j_job.from_date = lms_time
        j_job.save

      end

      self.j_job = j_job
      self.save

    end


  end
=end

  def manage_ct_module_um

    ct_module = CtModule.where('cod = ? AND active = ?', 'um', true).first

    return ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', self.id).count == 1

  end

  def mpi_view_users
    (self.user_ct_module_privileges && self.user_ct_module_privileges.mpi_view_users) ? true : false
  end

  def mpi_view_users_data
    (self.user_ct_module_privileges && self.user_ct_module_privileges.mpi_view_users && self.user_ct_module_privileges.mpi_view_users_data) ? true : false
  end

  def mpi_view_users_training_history
    (self.user_ct_module_privileges && self.user_ct_module_privileges.mpi_view_users && self.user_ct_module_privileges.mpi_view_users_training_history) ? true : false
  end

  def mpi_view_users_pe_history
    (self.user_ct_module_privileges && self.user_ct_module_privileges.mpi_view_users && self.user_ct_module_privileges.mpi_view_users_pe_history) ? true : false
  end

  def mpi_view_users_informative_documents
    (self.user_ct_module_privileges && self.user_ct_module_privileges.mpi_view_users && self.user_ct_module_privileges.mpi_view_users_informative_documents) ? true : false
  end

  def um_manage_users
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_manage_users) ? true : false
  end

  def um_edit_users
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_manage_users && self.user_ct_module_privileges.um_edit_users) ? true : false
  end

  def um_manage_users_data
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_manage_users && self.user_ct_module_privileges.um_manage_users_data) ? true : false
  end

  def um_manage_users_password
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_manage_users && self.user_ct_module_privileges.um_manage_users_password) ? true : false
  end

  def um_manage_users_email
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_manage_users && self.user_ct_module_privileges.um_manage_users_email) ? true : false
  end

  def um_manage_users_photo
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_manage_users && self.user_ct_module_privileges.um_manage_users_photo) ? true : false
  end
  
  def um_create_users
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_manage_users && self.user_ct_module_privileges.um_create_users) ? true : false
  end

  def um_delete_users
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_manage_users && self.user_ct_module_privileges.um_delete_users) ? true : false
  end

  def um_query_reports?
    (self.user_ct_module_privileges && self.user_ct_module_privileges.um_query_reports) ? true : false
  end

  def um_query_report?(um_custom_report)
    (self.user_ct_module_privileges && self.user_ct_module_privileges.user_ct_module_privileges_um_custom_report(um_custom_report)) ? true : false
  end

  def lms_define
    (self.user_ct_module_privileges && self.user_ct_module_privileges.lms_define) ? true : false
  end

  def lms_enroll
    (self.user_ct_module_privileges && self.user_ct_module_privileges.lms_enroll) ? true : false
  end



  def lms_manage_enroll
    (self.user_ct_module_privileges && self.user_ct_module_privileges.lms_manage_enroll) ? true : false
  end

  def lms_reports
    (self.user_ct_module_privileges && self.user_ct_module_privileges.lms_reports) ? true : false
  end

  def num_members
    self.members.count
  end

  def active_members
    self.members.where('activo = ?', true)
  end

  def active_members_full_hierarchy

    m = self.members

    self.members.each do |member|

      m += member.active_members_full_hierarchy

    end

    return m

  end

  def hr_process_users_ordered_date_desc
    self.hr_process_users.joins(:hr_process).order('fecha_inicio DESC')
  end

  def pe_managers_ordered_by_process
    self.pe_managers.joins(:pe_process).reorder('active DESC, period DESC, name ASC')
  end

  def pe_member_observers_ordered(pe_process)
    self.pe_member_observers.where('pe_member_observers.pe_process_id = ?', pe_process.id).joins(:pe_member_observed => [:user]).reorder('apellidos, nombre');
  end

  def pe_reporters_ordered_by_process
    self.pe_reporters.joins(:pe_process).reorder('active DESC, period DESC, name ASC')
  end

  def members_ordered_by_node
    self.members.joins('join nodes as nodes_o on nodes_o.user_id = users.id').reorder('nodes_o.nombre ASC, apellidos, nombre')
  end

  def self.users_with_mpi_view_users_privileges
    User.joins(:user_ct_module_privileges).where('mpi_view_users = ?', true)
  end

  def self.users_with_um_manage_users_privileges
    User.joins(:user_ct_module_privileges).where('um_manage_users = ?', true)
  end

  def self.users_with_query_reports_privileges
    User.joins(:user_ct_module_privileges).where('um_query_reports = ?', true)
  end

  def self.users_with_lms_privileges
    User.joins(:user_ct_module_privileges).where('lms_define = ? OR lms_enroll = ? OR lms_manage_enroll = ? OR lms_reports = ?', true, true, true, true)
  end

  def brc_member(brc_process)
    brc_process.brc_members.where('user_id = ?', self.id).first
  end

  def active_brc_processes_is_member
    BrcProcess.joins(:brc_members).where('brc_processes.active = ? AND brc_members.user_id = ?', true, self.id).uniq
  end

  def active_brc_processes_to_define
    #(BrcProcess.joins(:brc_members).where('brc_processes.active = ? AND brc_members.user_define_id = ?', true, self.id)+BrcProcess.joins(:brc_definers).where('brc_processes.active = ? AND brc_definers.user_id = ?', true, self.id)).uniq
    BrcProcess.joins(:brc_definers).where('brc_processes.active = ? AND brc_definers.user_id = ?', true, self.id)
  end

  def brc_members_to_define_by_process_ordered(brc_process)
    self.brc_users_define.joins(:user).where('brc_process_id = ?', brc_process.id).reorder('apellidos, nombre')
  end

  def num_brc_members_to_define_by_process_pending(brc_process)
      self.brc_users_define.where('brc_process_id = ? AND status_def = ?', brc_process.id, false).count
  end

  def active_brc_processes_to_validate
    #BrcProcess.joins(:brc_members).where('brc_processes.active = ? AND brc_members.user_validate_id = ?', true, self.id).uniq
    BrcProcess.joins(:brc_definers).where('brc_processes.active = ? AND brc_definers.user_validate_id = ?', true, self.id).uniq
  end

  def brc_members_to_validate_by_process_ordered(brc_process)
    self.brc_users_validate.joins(:user).where('brc_process_id = ?', brc_process.id).reorder('apellidos, nombre')
  end

  def num_brc_members_to_validate_by_process_pending(brc_process)
    self.brc_users_validate.where('brc_process_id = ? AND status_val = ?', brc_process.id, false).count
  end

  def fix_last_name

    if self.new_record?
      #para create
      if self.apellidos.blank? && !self.last_name_1.blank?
        #esta condición se puede dar al crear un nuevo usuario y si se usan dos apellidos
        self.apellidos = self.last_name_1+' '+self.last_name_2
      elsif !self.apellidos.blank? && self.last_name_1.blank?
        #esta condición se puede dar al crear un nuevo usuario y si se usa un apellido
        self.last_name_1 = self.apellidos
      end
    else
      #para update
      if self.apellidos_changed?
        #esto será válido cuando se gestione un sólo apellido
        self.last_name_1 = self.apellidos
        self.last_name_2 = ''
      elsif self.last_name_1_changed? || self.last_name_2_changed?
        self.apellidos = self.last_name_1+' '+self.last_name_2
      end

    end

  end

  def from_date_inicio_laboral
    self.characteristic_value(Characteristic.get_characteristic_from_date_work)
  end

  def secu_from_date_inicio_progresivas
    self.characteristic_value(Characteristic.get_characteristic_from_date_progresivas)
  end

  def secu_from_date_certificado_progresivas
    self.characteristic_value(Characteristic.get_characteristic_from_date_r_progresivas)
  end

  def secu_rol

    # Comercial
    # General
    # Privado

    r = nil
    cv_id = self.characteristic_value(Characteristic.get_characteristic_rol_security)
    if cv_id
      cv = CharacteristicValue.find cv_id
      if cv
        r = cv.value_string_2
      end
    end
    return r
  end

  def tipo_contrato_secu(date_ref = nil)
    # indefinido
    # plazo

    r = nil

    if date_ref

      date_ref.to_datetime

      if date_ref < self.from_date_inicio_laboral || (self.delete_date && date_ref > self.delete_date)
        return nil
      else
        characteristic = Characteristic.get_characteristic_tipo_contrato_secu

        self.user_characteristic_records_by_characteristic(characteristic).each do |ucr|

          if ucr.from_date.to_date <= date_ref && ( (ucr.to_date && date_ref <= ucr.to_date.to_date) || ucr.to_date.nil? )
            cv = CharacteristicValue.find ucr.characteristic_value_id
            r = cv.value_string_2
          end

        end


      end
    else
      cv_id = self.characteristic_value(Characteristic.get_characteristic_tipo_contrato_secu)
      if cv_id
        cv = CharacteristicValue.find cv_id
        if cv
          r = cv.value_string_2
        end
      end
    end
    return r
  end

  def secu_tiene_contrato_plazo(date_ref)

    fechas = Array.new

    fechas.push self.from_date_inicio_laboral

    characteristic = Characteristic.get_characteristic_contrato_plazo_secu

    self.user_characteristics_registered(characteristic).each_with_index do |user_characteristic_registered|

      characteristic.elements_characteristics.reorder('register_position').each_with_index do |element_characteristic|

        if element_characteristic.contrato_plazo_secu
          ucr = user_characteristic_registered.elements_user_characteristics.where('register_user_characteristic_id = ? AND characteristic_id = ?',user_characteristic_registered.id, element_characteristic.id).first
          fechas.push ucr.value if ucr
        end

      end

    end

    fechas.sort!

    if date_ref < fechas.first || (self.delete_date && date_ref > self.delete_date)
      return nil
    elsif date_ref >= fechas.first && date_ref <= fechas.last
      return true
    else
      return false
    end

  end

  def secu_tiene_contrato_indefinido(date_ref = nil)
    r = tipo_contrato_secu(date_ref)
    r = true if r == 'indefinido'
    r = false if r == 'plazo'
    r
  end

=begin
  module Net
    class FTP
      def makepasv # :nodoc:
        if @sock.peeraddr[0] == "AF_INET"
          #host, port = parse227(sendcmd("PASV")) #WAS!
          host, port = parse229(sendcmd("EPSV"))
        else
          host, port = parse229(sendcmd("EPSV"))
        end
        return host, port
      end
    end
  end
=end
  def self.integrate_hoc_satp

    #require 'net/ftp'
    #ftp = Net::FTP::new#('54.208.249.121')
    #ftp.connect('54.208.249.121',21)
    #ftp.login('usrexa', '357*exa09')
    #ftp.passive = true
    #ftp.getbinaryfile('HOC_EXA_'+(Time.now - 1.day).strftime('%Y%m%d')+'.txt', '/tmp/sync_hoc.txt')
    #ftp.close

    ActiveRecord::Base.establish_connection 'production_hochschild'

    date_file = (Time.now - 1.day).strftime('%Y%m%d')
    comando = "curl -u usrexa:357*exa09 'ftp://54.208.249.121/HOC_EXA_"+date_file+".txt' -o /tmp/sync_hoc.txt"

    system comando


    if File.exist?('/tmp/sync_hoc.txt')

     company = @company

      text = File.read('/tmp/sync_hoc.txt')
      text.gsub!(/\r\n?/, "\n")

      text.each_line do |line|

        if line.strip != ''

          datos_tmp = line.split('{')

          datos = datos_tmp[0].split(';')

          unless datos[4].nil?

            #0 usuario
            #1 nombres
            #2 apellidos
            #3 correo, si se pone asterisco no se actualizará
            #4 activo/inactivo
            #5 atributos

            user = User.find_by_codigo datos[0]

            user = User.new unless user

            user.codigo = datos[0]
            user.nombre = datos[1]
            user.apellidos = datos[2]
            user.password = user.password_confirmation = SecureRandom.hex(5) unless user.id
            user.email = datos[3].strip.downcase if datos[3] != ''

            if datos[4] == 'activo'
              user.activo = true
            elsif !user.perpetual_active
              user.activo = false
            end


            if user.save

              if datos_tmp[1]

                datos[5] = datos_tmp[1].strip.slice 0..-2

                atributos = datos[5].split(';')

                atributos.each do |atributo|

                  atributo = atributo.split(':')

                  caracteristica = Characteristic.find_by_nombre(atributo[0])

                  if caracteristica && caracteristica.data_type_id != 6

                    new_value = nil

                    error = false

                    if atributo[1] && !atributo[1].blank?

                      if caracteristica.data_type_id == 5
                        characteristic_value = caracteristica.characteristic_values.where('value_string = ?', atributo[1].strip).first
                        if characteristic_value
                          new_value = characteristic_value.id.to_s
                        else
                          error = true
                        end
                      else
                        new_value = atributo[1].strip
                      end

                    end

                    tz = Time.now.utc.in_time_zone('America/Lima').strftime('%::z').split(':')

                    time_peru = Time.now.utc+((tz[0].to_i*60*60)+(tz[1].to_i*60)+tz[2].to_i).seconds

                    user.update_user_characteristic(caracteristica, new_value, time_peru, nil, nil, nil, nil, company) unless error

=begin
                  if caracteristica

                    valor_incorrecto = false

                    user_caracteristica = UserCharacteristic.where('user_id = ? AND characteristic_id = ?', user.id, caracteristica.id).first
                    nuevo = false
                    unless user_caracteristica

                      user_caracteristica = UserCharacteristic.new
                      user_caracteristica.user = user
                      user_caracteristica.characteristic = caracteristica
                      nuevo = true
                    end

                    if atributo[1]
                      if atributo[1] == '*' && nuevo
                        valor_incorrecto = true
                      else
                        user_caracteristica.valor = atributo[1].strip

                        if caracteristica.data_type_id == 0
                          #string
                          user_caracteristica.value_string = atributo[1].strip
                        elsif caracteristica.data_type_id == 1
                          #text
                          user_caracteristica.value_text = atributo[1].strip
                        elsif caracteristica.data_type_id == 2
                          #date
                          user_caracteristica.value_date = atributo[1].strip
                        elsif caracteristica.data_type_id == 3
                          #int
                          user_caracteristica.value_int = atributo[1].strip
                        elsif caracteristica.data_type_id == 4
                          #float
                          user_caracteristica.value_float = atributo[1].strip
                        elsif caracteristica.data_type_id == 5
                          #lista
                          user_caracteristica.characteristic_value = caracteristica.characteristic_values.where('value_string = ?', atributo[1].strip).first
                        end

                      end

                    else
                      user_caracteristica.valor = ''
                    end

                    unless valor_incorrecto

                      user_caracteristica.save

                    end
=end
                  end

                end

              end

            end

          end

        end

      end

      FileUtils.rm('/tmp/sync_hoc.txt')

    end

  end

  def jj_characteristic_value(jj_characteristic)

    j_job = self.j_job
    j_job.jj_characteristic_value jj_characteristic if j_job

  end

  def set_pgp
    require 'openssl'
    rsa_key = OpenSSL::PKey::RSA.new(2048)
    private_key = rsa_key.to_pem
    public_key = rsa_key.public_key.to_pem

    self.private_key = private_key
    self.public_key = public_key

  end

  def validate_password_policies(password)
    policy = PasswordPolicy.where(use_in_exa: true, active: true, default: true).last
    evaluation = {valid: false, strongness: 0, errors: []}
    if policy
      policy.password_rules.each do |rule|
        error_match = false
        regex_pattern = Regexp.new rule.regular_expression
        match = regex_pattern.match(password)
        error_match = true if (match && !rule.positive_match) || (match.nil? && rule.positive_match)
        evaluation[:strongness] += rule.strongness if match
        evaluation[:errors] << rule if rule.mandatory && error_match
      end
    end
    evaluation[:valid] = true if evaluation[:errors].count.zero?
    return evaluation
  end

  def secu_clasif_puest
    jj_brc_characteristic = JjCharacteristic.brc_characteristic
    j_job = self.j_job
    if j_job && jj_brc_characteristic
      j_job.j_job_jj_characteristic jj_brc_characteristic
    else
      nil
    end
  end
end
