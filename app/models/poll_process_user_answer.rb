# == Schema Information
#
# Table name: poll_process_user_answers
#
#  id                         :integer          not null, primary key
#  poll_process_user_id       :integer
#  master_poll_question_id    :integer
#  master_poll_alternative_id :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  long_answer                :text
#

class PollProcessUserAnswer < ActiveRecord::Base
  belongs_to :poll_process_user
  belongs_to :master_poll_question
  belongs_to :master_poll_alternative
  attr_accessible :poll_process_user_id, :master_poll_question_id, :master_poll_alternative_id, :long_answer,
      :master_poll_question
end
