# == Schema Information
#
# Table name: holiday_characteristics
#
#  id                     :integer          not null, primary key
#  characteristic_id      :integer
#  holiday_id             :integer
#  condition_type         :integer
#  match_value_string     :string(255)
#  match_value_text       :text
#  match_value_date       :date
#  match_value_int        :integer
#  match_value_float      :float
#  match_value_char_id    :integer
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  prev_hol_char_id       :integer
#

class HolidayCharacteristic < ActiveRecord::Base
  belongs_to :characteristic
  belongs_to :holiday

  attr_accessible :active,
                  :characteristic_id,
                  :condition_type,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :match_value_char_id,
                  :match_value_date,
                  :match_value_float,
                  :match_value_int,
                  :match_value_string,
                  :match_value_text

  validates :characteristic_id, presence: true
  validates :holiday_id, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validates :characteristic_id, presence: true
  validates :match_value_float, presence: true, if: 'characteristic.data_type_id == 4'
  validates :match_value_int, presence: true, if: 'characteristic.data_type_id == 3'
  validates :match_value_text, presence: true, if: 'characteristic.data_type_id == 1'
  validates :match_value_string, presence: true, if: 'characteristic.data_type_id == 0'
  validates :match_value_char_id, presence: true, if: 'characteristic.data_type_id == 5'
  validates :match_value_date, presence: true, if: 'characteristic.data_type_id == 2'

  def self.active; where(active: true) end

  def match_value_formatted
      case characteristic.data_type_id
        when 0
          match_value_string
        when 1
          match_value_text
        when 2
          match_value_date.strftime('%d/%m/%Y')
        when 3
          match_value_int.to_s
        when 4
          match_value_float.to_s
        when 5
          CharacteristicValue.find(match_value_char_id).value_string
        else
          ''
      end
  end

  def match_value
    case characteristic.data_type_id
      when 0
        match_value_string
      when 1
        match_value_text
      when 2
        match_value_date
      when 3
        match_value_int
      when 4
        match_value_float
      when 5
        match_value_char_id
      else
        ''
    end
  end

  def char_name; characteristic.nombre end

end
