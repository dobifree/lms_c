# == Schema Information
#
# Table name: ben_cellphone_numbers
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  number     :string(255)
#  nemo_plan  :string(255)
#  active     :boolean
#  subsidy    :decimal(12, 4)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BenCellphoneNumber < ActiveRecord::Base
  belongs_to :user
  has_many :ben_user_cellphone_processes

  attr_accessible :active,
                  :nemo_plan,
                  :number,
                  :subsidy,
                  :user_id

  validates :active, inclusion: { in: [true, false] }
  validates :number, presence: true, uniqueness: true
  validates :subsidy, :numericality => {:greater_than_or_equal_to => 0}

  validate :val_number
  validate :cod_id

  def val_number
    unless self.number.empty?
      errors.add(:number, 'debe tener 9 dígitos') unless self.number.size == 9
      errors.add(:number, 'solo puede ser numérico') unless self.number =~ /\A\d+\z/
    end
  end

  def cod_id
    errors.add(:user_id, 'no se ha encontrado') unless self.user_id != -1
  end

  def number_formatted
    self.number.insert(3,' ').insert(7,' ')
  end



end
