# == Schema Information
#
# Table name: hr_process_user_comments
#
#  id                      :integer          not null, primary key
#  hr_process_user_id      :integer
#  comentario              :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  hr_process_user_eval_id :integer
#  fecha                   :datetime
#

class HrProcessUserComment < ActiveRecord::Base

  belongs_to :hr_process_user
  belongs_to :hr_process_user_eval, class_name: 'HrProcessUser', foreign_key: :hr_process_user_eval_id

  attr_accessible :hr_process_user_id, :comentario, :hr_process_user_eval_id, :fecha

  default_scope order('fecha DESC')

end
