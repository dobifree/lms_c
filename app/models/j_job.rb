# == Schema Information
#
# Table name: j_jobs
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  mission        :string(255)
#  j_job_level_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  version        :integer
#  current        :boolean          default(TRUE)
#  j_job_id       :integer
#  from_date      :date
#  to_date        :date
#  j_pay_band_id  :integer
#  j_code         :string(255)
#  alert_band     :boolean
#

class JJob < ActiveRecord::Base

  belongs_to :j_job_level
  belongs_to :j_job
  belongs_to :j_pay_band

  has_many :j_functions, dependent: :destroy
  has_many :j_jobs
  has_many :users

  has_many :j_job_characteristics
  has_many :j_job_jj_characteristics

  has_many :j_job_ccs
  has_many :j_cost_centers, :through => :j_job_ccs

  #accepts_nested_attributes_for :j_job_ccs

  attr_accessible :mission, :name, :j_job_level_id, :version, :current, :j_job_id,
                  :from_date, :to_date, :j_pay_band_id, :j_code, :alert_band

  validates :j_code, presence: true
  validates :name, presence: true
  #validates :j_job_level_id, presence: true
  #validates :j_pay_band_id, presence: true
  validates :version, presence: true
  validate :cod_name

  after_create :set_alert_band_1
  before_update :set_alert_band

  default_scope order('name')


  def j_job_current

    if self.current
      self
    elsif self.j_job.nil?
      self.j_jobs.where('current', true).first
    else
      self.j_job.j_jobs.where('current', true).first
    end

  end

  def j_job_characteristic(characteristic)
    self.j_job_characteristics.where('characteristic_id = ?', characteristic.id).first
  end

  def j_job_characteristic_value(characteristic)
    jjc = self.j_job_characteristic characteristic
    jjc.value if jjc
  end

  def formatted_j_job_characteristic_value(characteristic)
    jjc = self.j_job_characteristic characteristic
    jjc.formatted_value if jjc
  end

  def j_job_jj_characteristic(jj_characteristic)
    self.j_job_jj_characteristics.where('jj_characteristic_id = ?', jj_characteristic.id).first
  end

  def jj_characteristic_value(jj_characteristic)
    jjc = self.j_job_jj_characteristic jj_characteristic
    jjc.value if jjc
  end

  def formatted_jj_characteristic_value(jj_characteristic)
    jjc = self.j_job_jj_characteristic jj_characteristic
    jjc.formatted_value if jjc
  end

  def j_job_jj_characteristics_registered(jj_characteristic)
    self.j_job_jj_characteristics.where('jj_characteristic_id = ?', jj_characteristic.id)
  end

  def add_j_job_jj_characteristic(jj_characteristic, new_value, company)

    saved_in = false

    #jjc = self.j_job_jj_characteristic(jj_characteristic)

    #unless jjc

    #end

    if new_value

      jjc = self.j_job_jj_characteristics.build
      jjc.jj_characteristic = jj_characteristic

      jjc.update_value new_value, company

      saved_in = true if jjc.save

    end

    return saved_in, jjc

  end

  def cod_name

    j = JJob.find_by_j_code self.j_code

    if j && j.name != self.name
      errors.add(:datos_incorrectos, ": El código #{j.j_code} corresponde al puesto #{j.name}")
    end

  end

  def set_alert_band

    set_alert = false

    jj_characteristic = JjCharacteristic.find_by_val_sec_1 true

    j_char_company = JCharacteristic.first

    if jj_characteristic && j_char_company

      j_c_company = self.j_job_characteristic j_char_company.characteristic

      j_job_jj_characteristic = self.j_job_jj_characteristic jj_characteristic

      if jj_characteristic && j_char_company && j_c_company && j_job_jj_characteristic

        nivel_p = j_job_jj_characteristic.jj_characteristic_value.position # nivel puesto

        JJob.where('j_code = ? AND id <> ?', self.j_code, self.id).each do |j_o|

          j_c_o = j_o.j_job_characteristic j_char_company.characteristic

          banda_p = self.j_pay_band.name.to_i if self.j_pay_band

          banda_o_p = j_o.j_pay_band.name.to_i if j_o.j_pay_band

          if j_c_o && banda_p && banda_o_p

            if j_c_company.characteristic_value_id == j_c_o.characteristic_value_id #que sean de la misma empresa

              jj_c_o = j_o.j_job_jj_characteristic jj_characteristic

              if jj_c_o

                nivel_o_p = jj_c_o.jj_characteristic_value.position #nivel otro puesto

                if (nivel_p+1 == nivel_o_p && banda_p + 1 < banda_o_p) || (nivel_p-1 == nivel_o_p && banda_p - 1 > banda_o_p)
                  set_alert = true
                  break
                end

              end


            end

          end

        end

      end

      self.alert_band = set_alert
      true

    end

  end

  def set_alert_band_1
    self.set_alert_band
    self.save
  end

  def full_select_name

    j_c = self.j_code ? self.j_code+' - ' : ''

    j_c += self.name+' ( '

    JCharacteristic.all.each do |j_characteristic|
      j_c += ' '+self.formatted_j_job_characteristic_value(j_characteristic.characteristic)
    end

    j_c += ' ) '

    j_c += '   '

    JjCharacteristic.where('val_sec_1 = ?', true).each_with_index do |jj_characteristic, index_jjc|
      fc = self.formatted_jj_characteristic_value(jj_characteristic)
      j_c += '__' if index_jjc > 0
      j_c += fc unless fc.nil?
    end

    j_c

  end

end
