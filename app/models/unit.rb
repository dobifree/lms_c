# == Schema Information
#
# Table name: units
#
#  id                       :integer          not null, primary key
#  numero                   :integer
#  nombre                   :string(255)
#  descripcion              :text
#  course_id                :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  libre                    :boolean          default(TRUE)
#  activa                   :boolean          default(TRUE)
#  antes_de_unit_id         :integer
#  despues_de_unit_id       :integer
#  antes_de_evaluation_id   :integer
#  despues_de_evaluation_id :integer
#  antes_de_poll_id         :integer
#  despues_de_poll_id       :integer
#  master_unit_id           :integer
#  orden                    :integer          default(1)
#  finaliza_automaticamente :boolean          default(TRUE)
#  virtual                  :boolean          default(TRUE)
#

class Unit < ActiveRecord::Base
  
  belongs_to :course
  belongs_to :master_unit

  belongs_to :antes_de_unit, class_name: 'Unit'
  has_many :despues_de_units, class_name: 'Unit', foreign_key: 'antes_de_unit_id', dependent: :nullify

  belongs_to :despues_de_unit, class_name: 'Unit'#, foreign_key: "despues_de_unit_id"
  has_many :antes_de_units, class_name: 'Unit', foreign_key: 'despues_de_unit_id', dependent: :nullify

  belongs_to :antes_de_evaluation, class_name: 'Evaluation'#, foreign_key: "antes_de_evaluation_id"
  has_many :despues_de_evaluations, class_name: 'Evaluation', foreign_key: 'antes_de_unit_id', dependent: :nullify

  belongs_to :despues_de_evaluation, class_name: 'Evaluation'#, foreign_key: "despues_de_evaluation_id"
  has_many :antes_de_evaluations, class_name: 'Evaluation', foreign_key: 'despues_de_unit_id', dependent: :nullify

  belongs_to :antes_de_poll, class_name: 'Poll'#, foreign_key: "antes_de_poll_id"
  has_many :despues_de_polls, class_name: 'Poll', foreign_key: 'antes_de_unit_id', dependent: :nullify

  belongs_to :despues_de_poll, class_name: 'Poll'#, foreign_key: "despues_de_poll_id"
  has_many :antes_de_polls, class_name: 'Poll', foreign_key: 'despues_de_unit_id', dependent: :nullify

  has_many :user_course_units
  
  attr_accessible :antes_de_evaluation_id, :antes_de_poll_id, :antes_de_unit_id, 
                  :despues_de_evaluation_id, :despues_de_poll_id, :despues_de_unit_id,
                  :descripcion, :finaliza_automaticamente, :libre, :master_unit_id, :nombre, :numero, :orden, :virtual

  validates :nombre, presence: true
  validates :numero, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('orden ASC, numero ASC')

  def update_libre

    if( self.antes_de_unit_id.nil? && self.despues_de_unit_id.nil? && self.antes_de_evaluation_id.nil? && 
      self.despues_de_evaluation_id.nil? && self.antes_de_poll_id.nil? && self.despues_de_poll_id.nil? )
      self.libre = true
    else
      self.libre = false
    end

    self.save
    
  end

end
