# == Schema Information
#
# Table name: master_polls
#
#  id          :integer          not null, primary key
#  numero      :integer
#  nombre      :string(255)
#  descripcion :text
#  tags        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  plan_anual  :boolean          default(FALSE)
#  modo_grid   :boolean          default(FALSE)
#  to_managers :boolean          default(FALSE)
#

class MasterPoll < ActiveRecord::Base

  has_many :master_poll_questions, dependent: :destroy
  has_many :polls
  has_many :user_course_polls, through: :polls
  has_many :poll_processes
  has_many :poll_process_users, through: :poll_processes

  attr_accessible :descripcion, :nombre, :numero, :plan_anual, :tags, :modo_grid, :to_managers

  before_validation :set_numero, on: :create

  validates :nombre, presence: true
  validates :numero, presence: true, uniqueness: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('numero ASC')

  def set_numero
    self.numero = 1
    max_master_poll = MasterPoll.reorder('numero DESC').first
    self.numero = max_master_poll.numero+1 if max_master_poll
  end

end
