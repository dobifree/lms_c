# == Schema Information
#
# Table name: brg_reported_bonus
#
#  id            :integer          not null, primary key
#  brg_bonus_id  :integer
#  brg_report_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class BrgReportedBonus < ActiveRecord::Base
  belongs_to :brg_bonus
  belongs_to :brg_report

  attr_accessible :brg_bonus_id,
                  :brg_report_id

  validates :brg_bonus_id, presence: true
  validates :brg_report_id, presence: true

end
