# == Schema Information
#
# Table name: poll_process_users
#
#  id              :integer          not null, primary key
#  poll_process_id :integer
#  user_id         :integer
#  done            :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  enrolled        :boolean          default(FALSE)
#

class PollProcessUser < ActiveRecord::Base
  belongs_to :poll_process
  belongs_to :user
  has_many :poll_process_user_answers, dependent: :destroy
  attr_accessible :done, :poll_process, :user, :poll_process_id, :user_id, :poll_process_user_answers, :enrolled?

  validates :poll_process_id, uniqueness: {scope: :user_id}
end
