# == Schema Information
#
# Table name: sc_records
#
#  id                                 :integer          not null, primary key
#  begin                              :datetime
#  end                                :datetime
#  user_id                            :integer
#  registered_at                      :datetime
#  registered_by_user_id              :integer
#  sc_process_id                      :integer
#  sc_user_process_id                 :integer
#  he                                 :decimal(12, 4)
#  dont_charge_to_payroll             :boolean          default(FALSE)
#  dont_charge_to_payroll_at          :datetime
#  dont_charge_to_payroll_by_user_id  :integer
#  dont_charge_to_payroll_description :text
#  status                             :integer          default(0)
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  effective_time                     :integer
#  rejected_at                        :datetime
#  rejected_by_user_id                :integer
#  rejected_description               :text
#  sc_user_to_register_id             :integer
#  he_ct_module_privilege_id          :integer
#  sent_to_payroll                    :boolean          default(FALSE)
#  begin_sc                           :datetime
#  end_sc                             :datetime
#

class ScRecord < ActiveRecord::Base
  has_many :sc_reported_records
  has_many :sc_record_blocks
  has_many :sc_log_records

  belongs_to :user
  belongs_to :sc_process
  belongs_to :sc_user_process
  belongs_to :he_ct_module_privilege, class_name: 'HeCtModulePrivilage', foreign_key: :he_ct_module_privilege_id
  belongs_to :sc_user_to_register, class_name: 'ScUserToRegister', foreign_key: :sc_user_to_register_id
  belongs_to :dont_charge_to_payroll_by_user, class_name: 'User', foreign_key: :dont_charge_to_payroll_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :rejected_by_user, class_name: 'User', foreign_key: :rejected_by_user_id

  attr_accessible :begin,
                  :dont_charge_to_payroll,
                  :dont_charge_to_payroll_at,
                  :dont_charge_to_payroll_by_user_id,
                  :dont_charge_to_payroll_description,
                  :end,
                  :he,
                  :registered_at,
                  :registered_by_user_id,
                  :status,
                  :sc_user_process_id,
                  :rejected_by_user_id,
                  :rejected_at,
                  :rejected_description,
                  :user_id,
                  :he_ct_module_privilege_id,
                  :sc_user_to_register_id,
                  :sent_to_payroll,
                  :begin_sc,
                  :end_sc

  validates :begin, :presence => true
  validates :end, :presence => true
  validates :registered_at, :presence => true
  validates :registered_by_user_id, :presence => true
  validates :status, :presence => true
  validates_presence_of :dont_charge_to_payroll_at, :if => :dont_charge_to_payroll?
  validates_presence_of :dont_charge_to_payroll_by_user_id, :if => :dont_charge_to_payroll?
  validates_presence_of :dont_charge_to_payroll_description, :if => :dont_charge_to_payroll?

  validate :begin_end_range
  validate :end_process
  validate :has_schedule
  validate :verify_licenses

  def user_cod_formatted
    return nil unless user
    return user.codigo.insert(-2, '-')
  end

  def begin_end_range
    if self.begin && self.end
      if self.begin > self.end
        errors.add(:end, 'debe ser después del inicio')
      else
        if self.begin + 24.hours < self.end
        errors.add(:end, 'no puede conformar una asistencia mayor a  24 horas')
        else
          sc_records = ScRecord.where(:sc_process_id => self.sc_process_id, :user_id => self.user_id)
          traslape_records = []
          sc_records.each { |record| traslape_records << record if record.id != self.id && record.begin <= self.end && record.end >= self.begin }
          errors.add(:end, 'no puede conformar una asistencia que traslape con otra de inicio: '+traslape_records[0].begin_formatted+' y fin: '+traslape_records[0].end_formatted) if traslape_records.size > 0
        end
      end
    end
  end

  def approved_by
    return nil unless status == 3
    log_record = sc_log_records.where(action: 'Aprobar').order('date DESC').first
    return nil unless log_record
    log_record.user
  end

  # status: 0:Pendiente de enviar validación, 1:Pendiente de ser validado. 2:En discusión. 3:Validado. 4:Rechazado

  def status_label_color
    # color: 0 => default, 1: primary, 2: success, 3:info, 4:warning, 5:danger
    # status: 0:Pendiente de enviar validación, 1:Pendiente de ser validado. 2:En discusión. 3:Validado. 4:Rechazado

    case self.status
      when 0
        return 0
      when 1
        return 0
      when 2
        return 4
      when 3
        return 2
      when 4
        return 5
    end
  end

  def status_label_message
    # color: 0 => default, 1: primary, 2: success, 3:info, 4:warning, 5:danger

    # STATUS GUIDE
    # 0 : Pendiente de enviar validación,
    # 1 : Por validar.
    # 2 : Ya no existe.
    # 3 : Validado.
    # 4 : Rechazado

    case self.status
      when 0
        return 'Falta enviar a validación'
      when 1
        return 'Por validar'
      when 2
        return 'En discusión'
      when 3
        return 'Aprobado'
      when 4
        return 'Rechazado'
    end
  end

  def calculate_he

  end

  def search_match_user_process

  end

  def begin_formatted
    self.begin.strftime('%H:%M') if self.begin
  end

  def end_formatted
    self.end.strftime('%H:%M') if self.end
  end

  def he_formatted
    "%02d:%02d" % [self.he/60, self.he%60] if self.he
  end

  private

  def verify_licenses
    ct_module = CtModule.where('cod = ? AND active = ?', 'medlic', true).first
    return unless ct_module
    return unless self.begin && self.end
    licenses = MedlicLicense.be_license(self.begin.to_date, self.end.to_date, user_id)
    return unless licenses.size > 0
    licenses.each do |lic|
      errors.add(:begin, ' tiene conflicto con '+lic.absence_type+': '+lic.date_begin.strftime('%d-%m-%Y')+' - '+lic.date_end.strftime('%d-%m-%Y'))
    end
  end

  def end_process
    return unless self.begin && self.end
    errors.add(:begin, 'debe entrar en el rango disponible de registro del proceso: '+sc_process.records_since.strftime('%d/%m/%Y')+' - '+sc_process.records_until.strftime('%d/%m/%Y')) if sc_process.records_since > self.begin
    errors.add(:end, 'debe entrar en el rango disponible de registro del proceso: '+sc_process.records_since.strftime('%d/%m/%Y')+' - '+sc_process.records_until.strftime('%d/%m/%Y')) if sc_process.records_until < self.end
  end

  def has_schedule
    date = self.begin
    schedules_user = ScScheduleUser.where(active: true, user_id: user_id)
    period_time_sche = []
    indef_sche = nil

    schedules_user.each do |schedule_user|
      if schedule_user.active_since && schedule_user.active_since <= date && schedule_user.active_end >= date
        period_time_sche << schedule_user
      else
        indef_sche = schedule_user
      end
    end

    if period_time_sche.size > 0
      period_time_sche.sort_by!(&:active_since)
      schedule =  period_time_sche.first
    else
      schedule = indef_sche
    end

    return if schedule
    errors.add(:begin, 'no hay un horario asignado')
  end
end
