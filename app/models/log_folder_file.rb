# == Schema Information
#
# Table name: log_folder_files
#
#  id             :integer          not null, primary key
#  folder_file_id :integer
#  fecha          :datetime
#  elemento       :string(255)
#  accion         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#

class LogFolderFile < ActiveRecord::Base
  belongs_to :folder_file
  belongs_to :user
  attr_accessible :accion, :elemento, :fecha
end
