# == Schema Information
#
# Table name: tracking_form_groups
#
#  id               :integer          not null, primary key
#  tracking_form_id :integer
#  position         :integer
#  name             :string(255)
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class TrackingFormGroup < ActiveRecord::Base
  belongs_to :tracking_form
  has_many :tracking_form_questions, :dependent => :destroy
  accepts_nested_attributes_for :tracking_form_questions, :allow_destroy => true

  attr_accessible :description, :name, :position, :tracking_form_id, :tracking_form_questions_attributes

  validates :position, :numericality => true
  validates :name, :presence => true

  default_scope { order(:position, :name) }

end
