# == Schema Information
#
# Table name: dnc_characteristics
#
#  id                :integer          not null, primary key
#  characteristic_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class DncCharacteristic < ActiveRecord::Base

  belongs_to :characteristic

  attr_accessible :characteristic_id

  validates :characteristic_id, uniqueness: true

  default_scope joins(:characteristic).order('characteristics.nombre')

  def name
    self.characteristic.nombre
  end

end
