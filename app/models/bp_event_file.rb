# == Schema Information
#
# Table name: bp_event_files
#
#  id                      :integer          not null, primary key
#  bp_event_id             :integer
#  name                    :string(255)
#  description             :text
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  since                   :datetime
#  until                   :datetime
#

class BpEventFile < ActiveRecord::Base
  belongs_to :bp_event
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id

  attr_accessible :deactivated_at,
                  :deactivated_by_admin_id,
                  :description,
                  :name,
                  :since,
                  :until,
                  :registered_at,
                  :registered_by_admin_id

  validates :bp_event_id, presence: true
  validates :name, presence: true
  validates :since, presence: true
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  validate :since_before_until
  validate :range_since_concordance_1
  validate :range_since_concordance_2
  validate :range_until_concordance_1

  def deactivated_by_admin_formatted
    return deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  def active?(now)
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  private

  def since_before_until
    return unless self.until && since && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end

  def range_since_concordance_1
    return unless bp_event_id
    return unless since
    return unless bp_event.since && since < bp_event.since
    errors.add(:since, 'no puede ser menor a la fecha de inicio de activación (desde) del evento')
  end

  def range_since_concordance_2
    return unless bp_event_id
    return unless since
    return unless bp_event.since && bp_event.until && since > bp_event.until
    errors.add(:since, 'no puede ser mayor a la fecha de fin de activación (hasta) del evento')
  end

  def range_until_concordance_1
    return unless bp_event_id
    return unless self.until && bp_event.since && bp_event.until && self.until > bp_event.until
    errors.add(:until, 'no puede ser mayor a la fecha de fin de activación (hasta) del evento')
  end

end


