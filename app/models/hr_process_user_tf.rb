# == Schema Information
#
# Table name: hr_process_user_tfs
#
#  id                 :integer          not null, primary key
#  hr_process_user_id :integer
#  what               :text
#  how                :text
#  when               :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class HrProcessUserTf < ActiveRecord::Base

  belongs_to :hr_process_user

  attr_accessible :how, :what, :when

end
