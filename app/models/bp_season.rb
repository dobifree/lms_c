# == Schema Information
#
# Table name: bp_seasons
#
#  id                      :integer          not null, primary key
#  name                    :string(255)
#  description             :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  since                   :datetime
#  until                   :datetime
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#

class BpSeason < ActiveRecord::Base
  has_many :bp_season_periods
  has_many :bp_condition_vacations
  has_many :bp_licenses

  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id


  attr_accessible :description,
                  :name,
                  :since,
                  :until

  validates :name, presence: true
  validates :since, presence: true

  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  validate :since_before_until

  def deactivated_by_admin_formatted
    deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  def active?(now)
    now = now.to_datetime
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  def active_period(date)
    season_periods = bp_season_periods.all
    season_periods.each do |season_period|
      return season_period if season_period.active?(date)
    end
    return false
  end


  private

  def since_before_until
    return unless self.until && since && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end
end
