# == Schema Information
#
# Table name: brc_definer_companies
#
#  id                      :integer          not null, primary key
#  brc_definer_id          :integer
#  characteristic_value_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class BrcDefinerCompany < ActiveRecord::Base
  belongs_to :brc_definer
  belongs_to :characteristic_value
  # attr_accessible :title, :body
end
