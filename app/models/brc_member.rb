# == Schema Information
#
# Table name: brc_members
#
#  id                      :integer          not null, primary key
#  brc_process_id          :integer
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  user_define_id          :integer
#  user_validate_id        :integer
#  status_def              :boolean          default(FALSE)
#  status_val              :boolean          default(FALSE)
#  status_bon_cal          :boolean          default(FALSE)
#  bonus_total             :decimal(20, 4)   default(0.0)
#  bonus_liquido           :decimal(20, 4)   default(0.0)
#  bonus_retencion         :decimal(20, 4)   default(0.0)
#  bonus_p_retencion       :float            default(0.0)
#  status_choose           :boolean          default(FALSE)
#  bonus_apv               :decimal(20, 4)   default(0.0)
#  bonus_convenio          :decimal(20, 4)   default(0.0)
#  status_rej              :boolean          default(FALSE)
#  bonus_cc                :boolean          default(TRUE)
#  brc_apv_institution_id  :integer
#  brc_con_institution_id  :integer
#  bonus_con_uf            :decimal(20, 4)   default(0.0)
#  choose_by_own           :boolean          default(FALSE)
#  choose_date             :datetime
#  format_rejected         :boolean          default(FALSE)
#  format_rejected_comment :string(255)
#

class BrcMember < ActiveRecord::Base

  belongs_to :brc_process
  belongs_to :user

  belongs_to :user_define, class_name: 'User'
  belongs_to :user_validate, class_name: 'User'

  belongs_to :brc_apv_institution
  belongs_to :brc_con_institution, class_name: 'BrcApvInstitution'

  has_many :brc_bonus_items, dependent: :destroy
  has_many :brc_member_files, dependent: :destroy

  attr_accessible :status_def, :status_val, :status_bon_cal, :bonus_total, :bonus_liquido,
                  :bonus_retencion, :bonus_p_retencion, :status_choose, :bonus_apv, :bonus_convenio, :status_rej,
                  :bonus_cc, :brc_apv_institution_id, :brc_con_institution_id, :choose_by_own, :choose_date, :format_rejected,
                  :format_rejected_comment

  before_save :set_bonus

  validate :bonus_limits
  validates :brc_apv_institution_id, presence: true, :if => lambda { |brc_m| !brc_m.bonus_cc && brc_m.bonus_apv > 0 }
  validates :brc_con_institution_id, presence: true, :if => lambda { |brc_m| !brc_m.bonus_cc && brc_m.bonus_convenio > 0 }

  def set_bonus
    if self.bonus_cc
      self.bonus_apv = 0
      self.bonus_convenio = 0
      self.brc_apv_institution = nil
      self.brc_con_institution = nil
    else
      self.bonus_con_uf = self.bonus_convenio/self.brc_process.uf_value
    end
  end

  def convenio_acumulado_year

    t_bc = 0

    LiqProcess.where('year = ? AND month < ? ', self.brc_process.year, self.brc_process.liq_process.month).each do |lp|

      m = lp.get_depcon_brc_user(self.user)

      t_bc += m / self.brc_process.uf_value if m

    end

    t_bc

  end

  def sum_cohades_diff_50_month
    liq_process = self.brc_process.liq_process

    t = 0

    if liq_process

      v1 = liq_process.get_apvfmu_brc_user self.user
      v2 = liq_process.get_apvexe_brc_user self.user
      v3 = liq_process.get_ahopre_brc_user self.user
      v4 = liq_process.get_apvliq_brc_user self.user

      t += v1 if v1
      t += v2 if v2
      t += v3 if v3
      t += v4 if v4

    end

    t

  end

  def bonus_limits
    self.bonus_apv = 0 if self.bonus_apv.nil?
    self.bonus_convenio = 0 if self.bonus_convenio.nil?

    if !self.bonus_cc && ((self.bonus_apv + self.bonus_convenio) > bonus_total)
      errors.add(:bono_bruto, " no puede ser superado por la suma de APV y Dep. Convenido")
    end

    if self.bonus_apv/self.brc_process.uf_value > (50 - self.sum_cohades_diff_50_month/self.brc_process.uf_value)
    #if self.bonus_apv/self.brc_process.uf_value > 600 rol_privado
      errors.add(:bono, " APV no puede superar las 50 UF")
    end

    #if (self.bonus_convenio/self.brc_process.uf_value) > 900 rol_privado
    if (self.bonus_convenio/self.brc_process.uf_value + self.convenio_acumulado_year) > 900
      errors.add(:bono_convenido, " no puede superar las 900 UF anuales")
    end

  end


end
