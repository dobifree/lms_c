# == Schema Information
#
# Table name: pe_group2s
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  pe_process_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class PeGroup2 < ActiveRecord::Base

  belongs_to :pe_process
  attr_accessible :name

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_process_id }


end
