# == Schema Information
#
# Table name: notifications
#
#  id         :integer          not null, primary key
#  orden      :integer
#  asunto     :string(255)
#  texto      :text
#  fecha      :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Notification < ActiveRecord::Base

  attr_accessible :asunto, :fecha, :orden, :texto

  validates :asunto, presence: true
  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('orden ASC, updated_at DESC')


  def self.actives_for_user(user_id)

    actives_notifications_no_filter = Notification.all

    filter_by_characteristics(actives_notifications_no_filter, user_id)

  end

  private

  def self.filter_by_characteristics(actives_notifications_no_filter, user_id)
    notifications_to_show = []

    user_characteristics = UserCharacteristic.find_all_by_user_id(user_id)

    actives_notifications_no_filter.each do |notification|
      notification_characteristics = NotificationCharacteristic.find_all_by_notification_id(notification.id)
      #solo se filtra si la notification  tiene alguna caracteristica asociada
      if notification_characteristics.count > 0
        notification_characteristics.each do |notification_characteristic|
          user_characteristics.each do |user_characteristic|
            if notification_characteristic.match_value.to_s == user_characteristic.valor.to_s
              notifications_to_show.push(notification) unless notifications_to_show.include?(notification)
              break
            end
          end
        end
      else
        #si no hay caracteristicas asociadas la notification, se muestra nomas
        notifications_to_show.push(notification) unless notifications_to_show.include?(notification)
      end
    end


    return notifications_to_show

  end

end
