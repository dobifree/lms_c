# == Schema Information
#
# Table name: polls
#
#  id                         :integer          not null, primary key
#  nombre                     :string(255)
#  descripcion                :text
#  program_id                 :integer
#  level_id                   :integer
#  course_id                  :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  libre                      :boolean          default(TRUE)
#  numero                     :integer
#  activa                     :boolean          default(TRUE)
#  antes_de_unit_id           :integer
#  despues_de_unit_id         :integer
#  antes_de_evaluation_id     :integer
#  despues_de_evaluation_id   :integer
#  antes_de_poll_id           :integer
#  despues_de_poll_id         :integer
#  orden                      :integer          default(1)
#  obligatoria                :boolean          default(FALSE)
#  master_poll_id             :integer
#  tipo_satisfaccion          :boolean          default(FALSE)
#  program_course_instance_id :integer
#  virtual                    :boolean          default(TRUE)
#

class Poll < ActiveRecord::Base

  belongs_to :program
  belongs_to :level
  belongs_to :course
  belongs_to :master_poll
  belongs_to :program_course_instance

  belongs_to :antes_de_unit, class_name: 'Unit'#, foreign_key: "antes_de_unit_id"
  has_many :despues_de_units, class_name: 'Unit', foreign_key: 'antes_de_poll_id', dependent: :nullify

  belongs_to :despues_de_unit, class_name: 'Unit'#, foreign_key: "despues_de_unit_id"
  has_many :antes_de_units, class_name: 'Unit', foreign_key: 'despues_de_poll_id', dependent: :nullify

  belongs_to :antes_de_evaluation, class_name: 'Evaluation'#, foreign_key: "antes_de_evaluation_id"
  has_many :despues_de_evaluations, class_name: 'Evaluation', foreign_key: 'antes_de_poll_id', dependent: :nullify

  belongs_to :despues_de_evaluation, class_name: 'Evaluation'#, foreign_key: "despues_de_evaluation_id"
  has_many :antes_de_evaluations, class_name: 'Evaluation', foreign_key: 'despues_de_poll_id', dependent: :nullify

  belongs_to :antes_de_poll, class_name: 'Poll'#, foreign_key: "antes_de_poll_id"
  has_many :despues_de_polls, class_name: 'Poll', foreign_key: 'antes_de_poll_id', dependent: :nullify

  belongs_to :despues_de_poll, class_name: 'Poll'#, foreign_key: "despues_de_poll_id"
  has_many :antes_de_polls, class_name: 'Poll', foreign_key: 'despues_de_poll_id', dependent: :nullify

  has_many :user_course_polls

  attr_accessible :antes_de_evaluation_id, :antes_de_poll_id, :antes_de_unit_id, 
                  :despues_de_evaluation_id, :despues_de_poll_id, :despues_de_unit_id,
                  :descripcion, :libre, :master_poll_id, :nombre, :numero, :obligatoria, :orden,
                  :tipo_satisfaccion, :program_course_instance_id, :virtual

  validates :nombre, presence: true
  validates :numero, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('orden ASC, numero ASC')

  def update_libre_course

    if( self.antes_de_unit_id.nil? && self.despues_de_unit_id.nil? && self.antes_de_evaluation_id.nil? && 
      self.despues_de_evaluation_id.nil? && self.antes_de_poll_id.nil? && self.despues_de_poll_id.nil? )
      self.libre = true
    else
      self.libre = false
    end

    self.save
    
  end

end
