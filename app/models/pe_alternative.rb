# == Schema Information
#
# Table name: pe_alternatives
#
#  id             :integer          not null, primary key
#  description    :text
#  value          :integer
#  pe_question_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  fixed          :boolean          default(FALSE)
#

class PeAlternative < ActiveRecord::Base

  belongs_to :pe_question

  attr_accessible :description, :value, :pe_question_id, :fixed

  validates :description, presence: true, uniqueness: { case_sensitive: false, scope: :pe_question_id }
  validates :value, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: -1 }

  default_scope order('value ASC, description ASC')

end
