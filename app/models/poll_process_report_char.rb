# == Schema Information
#
# Table name: poll_process_report_chars
#
#  id                :integer          not null, primary key
#  poll_process_id   :integer
#  characteristic_id :integer
#  position          :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PollProcessReportChar < ActiveRecord::Base
  belongs_to :poll_process
  belongs_to :characteristic
  attr_accessible :position, :characteristic_id

  validates :position, :numericality => true
  validates :characteristic_id, :presence => true

end
