# == Schema Information
#
# Table name: queued_reports
#
#  id          :integer          not null, primary key
#  description :text
#  code        :string(255)
#  name        :string(255)
#  status      :boolean
#  from_date   :datetime
#  created_in  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  extension   :string(255)
#

class QueuedReport < ActiveRecord::Base

  attr_accessible :code, :created_in, :description, :from_date, :name, :status, :extension

  validates :code, presence: true, uniqueness: { case_sensitive: false }

  default_scope order('from_date DESC')

end
