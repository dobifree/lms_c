# == Schema Information
#
# Table name: sel_req_templates
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  active      :boolean          default(FALSE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SelReqTemplate < ActiveRecord::Base
  has_many :sel_req_items, :dependent => :destroy
  has_many :sel_requirements, :dependent => :nullify
  attr_accessible :active, :description, :name,
                  :sel_req_items

  #attr_reader :is_in_use?

  def is_in_use?
    return true if self.sel_requirements.any?
    return false
  end
end
