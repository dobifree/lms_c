# == Schema Information
#
# Table name: hr_process_evaluation_q_classifications
#
#  id                            :integer          not null, primary key
#  orden                         :integer
#  nombre                        :text
#  hr_process_evaluation_id      :integer
#  hr_evaluation_type_element_id :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  color                         :string(255)
#

class HrProcessEvaluationQClassification < ActiveRecord::Base

  belongs_to :hr_process_evaluation
  belongs_to :hr_evaluation_type_element

  attr_accessible :nombre, :orden, :hr_process_evaluation_id, :hr_evaluation_type_element_id,
                  :color

  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :nombre, presence: true, uniqueness:  {case_sensitive: false, scope: [:hr_process_evaluation_id, :hr_evaluation_type_element_id]}

  default_scope order('orden ASC')

end
