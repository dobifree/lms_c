# == Schema Information
#
# Table name: blog_list_items
#
#  id           :integer          not null, primary key
#  blog_list_id :integer
#  position     :integer
#  name         :string(255)
#  description  :text
#  active       :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class BlogListItem < ActiveRecord::Base
  belongs_to :blog_list
  attr_accessible :active, :description, :name, :position

  validates :position, :numericality => true
  validates :name, :presence => true

  default_scope { order(:position, :name) }
end
