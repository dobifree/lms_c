# == Schema Information
#
# Table name: sel_appointments
#
#  id                    :integer          not null, primary key
#  sel_process_step_id   :integer
#  description           :text
#  from_datetime         :datetime
#  to_datetime           :datetime
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class SelAppointment < ActiveRecord::Base
  belongs_to :sel_process_step
  has_many :sel_step_candidates, :dependent => :nullify
  attr_accessible :description, :from_datetime, :registered_at, :registered_by_user_id, :to_datetime,
                  :sel_process_step, :sel_process_step_id

  validates :description, :presence => true
  validates :from_datetime, :presence => true
  validates :to_datetime, :presence => true

  default_scope order(:from_datetime, :to_datetime, :description)
end
