# == Schema Information
#
# Table name: dnc_courses
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  dnc_program_id  :integer
#  dnc_category_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  duration        :integer
#

class DncCourse < ActiveRecord::Base

  belongs_to :dnc_program
  belongs_to :dnc_category

  attr_accessible :name, :duration

  validates :name, presence: true

end
