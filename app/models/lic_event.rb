# == Schema Information
#
# Table name: lic_events
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  bp_license_id          :integer
#  bp_form_id             :integer
#  prev_lic_event_id      :integer
#  date_event             :date
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  bp_event_id            :integer
#

class LicEvent < ActiveRecord::Base
  belongs_to :user
  belongs_to :bp_event
  belongs_to :bp_form
  belongs_to :bp_license
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :prev_lic_event, class_name: 'LicEvent', foreign_key: :prev_lic_event_id

  has_many :lic_requests
  has_many :lic_item_values

  accepts_nested_attributes_for :lic_requests, :allow_destroy => true
  accepts_nested_attributes_for :lic_item_values, :allow_destroy => true


  attr_accessible :user_id,
                  :bp_event_id,
                  :bp_form_id,
                  :date_event,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :prev_lic_event_id,
                  :registered_at,
                  :registered_by_user_id,
                  :lic_requests_attributes,
                  :lic_item_values_attributes

  validates :user_id, presence: true
  validates :bp_event_id, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_user_id, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def mandatory_items?
    return false unless bp_form
    bp_form.active_items.each { |item| return true if item.mandatory }
    false
  end

  def regularizable_items?
    return false unless bp_form
    bp_form.active_items.each { |item| return true if item.regularizable }
    false
  end

  def mand_reg(mand, reg)
    bp_form.active_items.each do |item|
      next unless item.mandatory == mand
      next unless item.regularizable == reg
      return true
    end
    false
  end

  def licenses
    group = BpGroup.user_active_group(date_event.to_datetime, user_id)
    licenses = []
    BpLicense.where(bp_group_id: group.id, bp_event_id: bp_event_id).each do |license|
      next unless (license.active?(date_event) && license.available) || lic_requests.where(active: true, bp_license_id: license.id).first
      licenses << license
    end
    licenses
  end

  def requests_for_register
    LicRequest.where(active: true, lic_event_id: id).all
  end

  def manager_showable_requests
    LicRequest.where(active: true, lic_event_id: id, status: [LicRequest.approved, LicRequest.closed]).all
  end

  def self.register_editable_non_partial(user_id)
    LicEvent.where(active: true, user_id: user_id).joins(:bp_license, :lic_requests).where(bp_licenses: {partially_taken: false}, lic_requests: {active: true}).all
  end

  def self.register_editable_partial(user_id)
    LicEvent.where(active: true, user_id: user_id).joins(:bp_license, :lic_requests).where(bp_licenses: {partially_taken: true}, lic_requests: {active: true}).all
  end

  def self.manager_editable_non_partial
    LicEvent.where(active: true).joins(:bp_license, :lic_requests).where(bp_licenses: {partially_taken: false}, lic_requests: {active: true, status: [LicRequest.approved, LicRequest.closed]}).all
  end

  def self.manager_editable_partial
    LicEvent.where(active: true).joins(:bp_license, :lic_requests).where(bp_licenses: {partially_taken: true}, lic_requests: {active: true, status: [LicRequest.approved, LicRequest.closed]}).all
  end

  def self.register_visible(user_id)
    events = LicEvent.where(active: true, user_id: user_id).all
    events = events.uniq_by { |var| var.id }
    events
  end

  def self.manager_visible
    events = LicEvent.where(active: true).joins(:lic_requests).where(lic_requests: {active: true, status: [LicRequest.approved, LicRequest.closed]}).all
    events = events.uniq_by { |var| var.id }
    events
  end

  def self.approver_visible
    LicEvent.where(active: true, user_id: user_id).joins(:lic_requests).where(lic_requests: {active: true}, status: [LicRequest.pending, LicRequest.approved, LicRequest.closed]).all
  end

  def self.approver_editable_partial(user_id)
    requests_to_approve = LicRequest.to_approve_by(user_id)

    return [] unless requests_to_approve.size > 0

    events = []

    requests_to_approve.each do |request|
      events << request.lic_event if request.lic_event && !events.include?(request.lic_event)
    end

    return events
  end

  def any_request_editable?
    response = false
    lic_requests.active.each { |request| response = true if request.available_register_to_edit }
    response
  end

  def any_request_addible?
    not_complete
  end

  def available_register_to_edit
    response = true
    lic_requests.active.each { |request| response = false unless request.available_register_to_edit }
    response
  end

  def available_approver_to_edit
    response = false
    lic_requests.active.each { |request| response = true if request.available_approver_to_edit }
    response
  end

  def exists_item?(item_id)
    self.lic_item_values.where(active: true, bp_item_id: item_id).first
  end

  def regularizable_items(only_pending, only_mandatory)
    array = []
    return array unless bp_form
    return array unless bp_form.active_items
    bp_form.active_items.each do |item|
      next unless item.regularizable
      next unless item.mandatory && only_mandatory
      value = exists_item?(item.id)
      next if value && value.value && value.value.size > 0 && only_pending
      array << item
    end
    array
  end

  def paid_requests
    lic_requests.where(active: true, paid: true).order('begin DESC')
  end

  def self.taken_by(user_id, bp_licenses_id)
    return nil unless bp_licenses_id && bp_licenses_id.size > 0
    LicRequest.where(active: true, paid: [nil, true], status: LicRequest.in_process_status).joins(:lic_event).where(lic_events: {active: true, user_id: user_id, bp_license_id: bp_licenses_id}).all
  end

  def sum_days_for(bp_license_id)
    days = 0
    lic_requests.where(active: true, paid: [nil, true], status: LicRequest.in_process_status, bp_license_id: bp_license_id).
        each { |request| days += request.days if request.days }
    days
  end

  def sum_money_for(bp_license_id)
    money = 0
    lic_requests.where(active: true, paid: [nil, true], status: LicRequest.in_process_status, bp_license_id: bp_license_id).
        each { |request| money += request.money if request.money }
    money
  end

  def sum_hours_for(bp_license_id)
    hours = 0
    lic_requests.where(active: true, paid: [nil, true], status: LicRequest.in_process_status, bp_license_id: bp_license_id).
        each { |request| hours += request.hours if request.hours }
    hours
  end

  def requests_for(bp_license_id)
    lic_requests.where(active: true, bp_license_id: bp_license_id)
  end


  def sum_days
    days = 0
    lic_requests.where(active: true, paid: [nil, true], status: LicRequest.in_process_status).each { |request| days += request.days if request.days }
    days
  end

  def sum_hours
    hours = 0
    lic_requests.where(active: true, paid: [nil, true], status: LicRequest.in_process_status).each { |request| hours += request.hours if request.hours }
    hours
  end

  def sum_money
    money = 0
    lic_requests.where(active: true, paid: [nil, true], status: LicRequest.in_process_status).each { |request| money += request.money if request.money }
    money
  end

  def not_complete
    return false unless bp_license.partially_taken

    if bp_license.days
      sum_days < bp_license.days
    else
      if bp_license.hours
        sum_hours < bp_license.hours
      else
        false
      end
    end
  end

end
