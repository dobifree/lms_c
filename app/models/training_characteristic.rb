# == Schema Information
#
# Table name: training_characteristics
#
#  id                    :integer          not null, primary key
#  name                  :string(255)
#  type                  :string(255)
#  data_type_id          :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  position              :integer
#  position_rep_2        :integer
#  position_rep_3        :integer
#  position_user_profile :integer
#  total_cost            :boolean          default(FALSE)
#  course_instance       :boolean          default(TRUE)
#  position_filtro       :integer
#  instances_list        :integer
#

class TrainingCharacteristic < ActiveRecord::Base

  has_many :training_characteristic_values, dependent: :destroy
  has_many :course_training_characteristics

  attr_accessible :data_type_id, :name, :position, :position_rep_2,
                  :position_rep_3, :position_user_profile, :total_cost, :course_instance, :position_filtro,
                  :instances_list

  default_scope order('position, name')

  def self.data_types
    [:string, :int, :list]
  end

  def data_type_name
    TrainingCharacteristic.data_types[self.data_type_id]
  end

  def self.rep2_characteristics
    TrainingCharacteristic.where('position_rep_2 is not null').reorder('position_rep_2')
  end

  def self.rep3_characteristics
    TrainingCharacteristic.where('position_rep_3 is not null').reorder('position_rep_3')
  end

  def self.filtro_characteristics
    TrainingCharacteristic.where('position_filtro is not null').reorder('position_filtro')
  end

  def self.position_user_profile_characteristics
    TrainingCharacteristic.where('position_user_profile is not null').reorder('position_user_profile')
  end

  def self.instances_list
    TrainingCharacteristic.where('instances_list is not null').reorder('instances_list')
  end

  def self.excel_data_types
    [:String, :Number, :String]
    #   0        1         2
  end

  def excel_data_type_name
    TrainingCharacteristic.excel_data_types[self.data_type_id]
  end

  def self.is_there_a_total_cost
    TrainingCharacteristic.where('total_cost = ?', true).size > 0 ? true : false
  end

  def self.for_course
    TrainingCharacteristic.where('course_instance = ?', true)
  end

  def self.for_instance
    TrainingCharacteristic.where('course_instance = ?', false)
  end

  def for_course
    self.course_instance
  end

end
