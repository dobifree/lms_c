# == Schema Information
#
# Table name: course_files
#
#  id           :integer          not null, primary key
#  nombre       :string(255)
#  crypted_name :string(255)
#  extension    :string(255)
#  descripcion  :text
#  course_id    :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  size         :integer
#

class CourseFile < ActiveRecord::Base

  belongs_to :course

  after_create :set_crypted_name

  validates :nombre, presence: true

  attr_accessible :crypted_name, :descripcion, :extension, :nombre, :size

  default_scope order('nombre ASC')

  def set_crypted_name

    self.crypted_name = self.id.to_s+'_'+SecureRandom.hex(5)
    self.save

  end

end
