# == Schema Information
#
# Table name: support_ticket_types
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SupportTicketType < ActiveRecord::Base

  has_many :support_tickets

  attr_accessible :nombre

  validates :nombre, presence: true, uniqueness: { case_sensitive: false }

  default_scope order('nombre ASC')

end
