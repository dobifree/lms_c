# == Schema Information
#
# Table name: dnc_student_ps
#
#  id         :integer          not null, primary key
#  dncp_id    :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DncStudentP < ActiveRecord::Base

  belongs_to :dncp
  belongs_to :user

  validates :dncp_id, presence: true
  validates :user_id, presence: true
  validates :user_id, uniqueness: { scope: :dncp_id }

  attr_accessible :user_id

  default_scope joins(:user).order('users.apellidos ASC, users.nombre ASC')

end
