# == Schema Information
#
# Table name: kpi_indicators
#
#  id                  :integer          not null, primary key
#  position            :integer
#  name                :string(255)
#  weight              :float
#  kpi_set_id          :integer
#  goal                :float
#  unit                :string(255)
#  indicator_type      :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  value               :float
#  percentage          :float
#  final_result_method :integer          default(0)
#  kpi_subset_id       :integer
#  power               :float            default(1.0)
#  leverage            :float            default(0.0)
#  opf_annual_goal     :string(255)
#

class KpiIndicator < ActiveRecord::Base

  belongs_to :kpi_set
  belongs_to :kpi_subset

  has_many :kpi_indicator_details, dependent: :destroy
  has_many :kpi_indicator_ranks, dependent: :destroy
  has_many :kpi_measurements, dependent: :destroy
  has_many :kpi_indicator_rols, dependent: :destroy
  has_many :kpi_colors, dependent: :destroy

  has_many :pe_questions

  has_many :kpi_indicator_details_ranks, through: :kpi_indicator_details, source: :kpi_indicator_ranks

  attr_accessible :goal, :name, :position, :indicator_type, :unit, :weight, :kpi_set_id, :kpi_subset_id, :value, :percentage,
                  :final_result_method, :power, :leverage, :opf_annual_goal

  validates :name, presence: true, uniqueness:  {case_sensitive: false, scope: [:kpi_set_id, :kpi_subset_id]}
  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :weight, presence: true, numericality: {greater_than_or_equal_to: 0 }
  validates :goal, numericality: {greater_than_or_equal_to: 0 }, allow_nil: true

  #before_save :set_percentage

  default_scope order('position, name')

  def set_goal_rank
    if self.indicator_type == 4

      kpi_indicator_rank = self.kpi_indicator_ranks.where('percentage = 100').first
      if kpi_indicator_rank
        self.goal = kpi_indicator_rank.goal
      end

    end
  end

=begin
  def set_percentage

    kpi_dashboard = self.kpi_set.kpi_dashboard

    self.percentage = KpiManagingController.helpers.calculate_percentage(self.value, self, kpi_dashboard.upper_limit, kpi_dashboard.lower_limit)

  end
=end


  def indicator_type_name

    case self.indicator_type
      when 1
        'Indicador directo'
      when 2
        'Indicador inverso'
      when 3
        'Indicador por desviación +/-'
      when 4
        'Indicador por rangos'
      when 5
        'Discreto'
      when 6
        'Security'
    end


  end

  def self.final_result_methods
    [:promedio, :ultima_medida, :suma, :registro_manual]
  end

  def set_final_result

    if self.final_result_method == 0

      #promedio

      if self.indicator_type != 4

        #no puede ser aplicado para rangos

        p = 0
        g = 0
        s = 0

        self.kpi_measurements.each do |kpi_measurement|

          if kpi_measurement.percentage
            p += 1
            s += kpi_measurement.value

            kpi_indicator_detail = self.kpi_indicator_details.where('measured_at = ?', kpi_measurement.measured_at).first

            g += kpi_indicator_detail.goal if kpi_indicator_detail

          end

        end

        if p > 0
          self.value = s/p
          self.goal = g/p

          self.percentage = KpiManagingController.helpers.calculate_percentage(self.value, self.goal, self, nil, self.kpi_set.kpi_dashboard.upper_limit, self.kpi_set.kpi_dashboard.lower_limit)

        else
          self.percentage = nil
          self.value = nil

          p = 0
          g = 0
          self.kpi_indicator_details.each do |kpi_indicator_detail|
            p += 1
            g += kpi_indicator_detail.goal
          end

          self.goal = p > 0 ? g/p : nil

        end

      end

    elsif self.final_result_method == 1

      #ultima_medida

      last_kpi_measurement = self.kpi_measurements.last

      if last_kpi_measurement
        self.percentage = last_kpi_measurement.percentage
        self.value = last_kpi_measurement.value

        kpi_indicator_detail = self.kpi_indicator_details.where('measured_at = ?', last_kpi_measurement.measured_at).first

        self.goal = kpi_indicator_detail ? kpi_indicator_detail.goal : nil

        if self.indicator_type == 4

          self.kpi_indicator_ranks.destroy_all

          kpi_indicator_detail.kpi_indicator_ranks.each do |kpi_indicator_rank|

            kir = self.kpi_indicator_ranks.build
            kir.goal = kpi_indicator_rank.goal
            kir.percentage = kpi_indicator_rank.percentage
            kir.save

          end

        end

      else
        self.percentage = nil
        self.value = nil

        kpi_indicator_detail = self.kpi_indicator_details.last

        if kpi_indicator_detail
          self.goal = kpi_indicator_detail.goal

          self.kpi_indicator_ranks.destroy_all

          kpi_indicator_detail.kpi_indicator_ranks.each do |kpi_indicator_rank|

            kir = self.kpi_indicator_ranks.build
            kir.goal = kpi_indicator_rank.goal
            kir.percentage = kpi_indicator_rank.percentage
            kir.save

          end

        else
          self.goal = nil
        end

      end

    elsif self.final_result_method == 2

      #suma

      if self.indicator_type != 4

        #no puede ser aplicado para rangos

        p = 0
        g = 0
        s = 0

        self.kpi_measurements.each do |kpi_measurement|

          if kpi_measurement.percentage
            p += 1
            s += kpi_measurement.value

            kpi_indicator_detail = self.kpi_indicator_details.where('measured_at = ?', kpi_measurement.measured_at).first

            g += kpi_indicator_detail.goal if kpi_indicator_detail

          end

        end

        if p > 0
          self.value = s
          self.goal = g

          self.percentage = KpiManagingController.helpers.calculate_percentage(self.value, self.goal, self, nil, self.kpi_set.kpi_dashboard.upper_limit, self.kpi_set.kpi_dashboard.lower_limit)

        else
          self.percentage = nil
          self.value = nil
          self.goal = nil

          p = 0
          g = 0
          self.kpi_indicator_details.each do |kpi_indicator_detail|
            p += 1
            g += kpi_indicator_detail.goal
          end

          self.goal = p > 0 ? g : nil

        end

      end

    elsif self.final_result_method == 3
      #manual

      self.percentage = KpiManagingController.helpers.calculate_percentage(self.value, self.goal, self, self.kpi_indicator_ranks, self.kpi_set.kpi_dashboard.upper_limit, self.kpi_set.kpi_dashboard.lower_limit)

    end


  end

end
