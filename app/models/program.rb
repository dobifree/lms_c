# == Schema Information
#
# Table name: programs
#
#  id                 :integer          not null, primary key
#  nombre             :string(255)
#  descripcion        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  nombre_corto       :string(255)
#  codigo             :string(255)
#  color              :string(255)
#  especifico         :boolean          default(TRUE)
#  plan_anual         :boolean          default(FALSE)
#  activo             :boolean          default(TRUE)
#  orden              :integer
#  managed_by_manager :boolean          default(FALSE)
#

class Program < ActiveRecord::Base

	has_many :levels, order: 'orden'
	
	has_many :program_courses
	has_many :courses, through: :program_courses

	has_many :enrollments
	has_many :users, through: :enrollments

  has_many :program_inspectors

  has_many :program_instances

  has_many :course_certificates

  #belongs_to :planning_process

  attr_accessible :orden, :codigo, :color, :descripcion, :especifico, :nombre, :nombre_corto, :plan_anual, :activo, :managed_by_manager

  validates :codigo, presence: true, uniqueness: { case_sensitive: false }
  #validates :orden, numericality: { only_integer: true, greater_than: 0 }
  validates :nombre, presence: true
  #validates :nombre_corto, presence: true
  validates :color, presence: true

  before_save { self.nombre.strip! unless self.nombre.nil? }
  before_save { self.descripcion.strip! unless self.descripcion.nil? }

  after_create :create_level

  default_scope order('activo DESC, orden ASC, nombre ASC')

  def create_level

    level = self.levels.build
    level.orden = 1
    level.nombre ='Único'
    level.save

  end

  def self.active_programs
    Program.where('activo = ?', true)
  end

  def self.active_programs_managed_by_manager
    Program.where('activo = ? AND managed_by_manager = ?', true, true)
  end

  def program_course(course,level)
    self.program_courses.where('course_id = ? AND level_id = ?', course.id, level.id).first
  end

  def program_courses_order_by_name
    self.program_courses.joins(:course).reorder('nombre ASC')
  end

  def active_program_courses_order_by_name
    self.program_courses.joins(:course).where('program_courses.activo = ?', true).reorder('nombre ASC')
  end

  def active_program_courses_managed_by_manager_order_by_name
    self.program_courses.joins(:course).where('program_courses.activo = ? AND courses.managed_by_manager = ?', true, true).reorder('nombre ASC')
  end

  def training_types
    TrainingType.where('id IN(?)', self.courses.pluck(:training_type_id))
  end

  def has_non_training_type_course
    self.courses.where('training_type_id IS NULL').count > 0 ? true : false
  end

  def program_courses_by_training_type_order_by_name(training_type)
    if training_type
      self.program_courses.joins(:course).where('training_type_id = ?', training_type).reorder('courses.nombre ASC')
    else
      self.program_courses.joins(:course).where('training_type_id IS NULL').reorder('courses.nombre ASC')
    end
  end

  def enrollment(user)
    self.enrollments.where('user_id = ?', user.id).first
  end

end
