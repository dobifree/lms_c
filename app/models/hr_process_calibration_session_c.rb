# == Schema Information
#
# Table name: hr_process_calibration_session_cs
#
#  id                                :integer          not null, primary key
#  hr_process_calibration_session_id :integer
#  user_id                           :integer
#  manager                           :boolean
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#

class HrProcessCalibrationSessionC < ActiveRecord::Base

  belongs_to :hr_process_calibration_session
  belongs_to :user

  validates :user_id, presence: true, uniqueness: {scope: :hr_process_calibration_session_id}

  attr_accessible :manager, :hr_process_calibration_session_id, :user_id

end
