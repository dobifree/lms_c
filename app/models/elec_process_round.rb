# == Schema Information
#
# Table name: elec_process_rounds
#
#  id                                :integer          not null, primary key
#  elec_process_id                   :integer
#  name                              :string(255)
#  description                       :text
#  position                          :integer
#  mandatory                         :boolean          default(FALSE)
#  lifespan                          :integer
#  qty_required                      :integer          default(1)
#  show_live_results                 :boolean          default(FALSE)
#  show_final_results                :boolean          default(FALSE)
#  threshold_pctg_universe           :integer          default(0)
#  threshold_pctg_voters             :integer          default(0)
#  threshold_qty_voters              :integer          default(1)
#  qty_min_podium                    :integer          default(1)
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  show_live_results_extra_time      :integer          default(0)
#  show_final_results_extra_time     :integer          default(0)
#  show_list_categories_in_dashboard :boolean          default(FALSE)
#  show_live_my_voters               :boolean          default(FALSE)
#  show_live_my_voters_extra_time    :integer          default(0)
#  show_final_my_voters              :boolean          default(FALSE)
#  show_final_my_voters_extra_time   :integer          default(0)
#  show_my_voters_since_qty_votes    :integer          default(0)
#  show_my_voters_detail             :boolean          default(FALSE)
#  list_like_minificha               :boolean          default(FALSE)
#

class ElecProcessRound < ActiveRecord::Base
  belongs_to :elec_process
  attr_accessible :description, :lifespan, :mandatory, :name, :position, :qty_min_podium, :qty_required, :show_final_results, :show_live_results,
                  :threshold_pctg_universe, :threshold_pctg_voters, :threshold_qty_voters,
                  :show_live_results_extra_time, :show_final_results_extra_time,
                  :show_list_categories_in_dashboard, :show_live_my_voters, :show_live_my_voters_extra_time,
                  :show_final_my_voters, :show_final_my_voters_extra_time, :show_my_voters_since_qty_votes,
                  :show_my_voters_detail,
                  :elec_process_id, :list_like_minificha

  validates :name, :presence => true
  validates :position, :presence => true, :numericality => { :only_integer => true}
  validates :qty_required, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 1}
  validates :qty_min_podium, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 1}
  validates :lifespan, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 1}
  validates :threshold_pctg_universe, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0}
  validates :threshold_pctg_voters, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0}
  validates :threshold_qty_voters, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 1}

  validates :show_live_results_extra_time, :numericality => { :only_integer => true}
  validates :show_final_results_extra_time, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0}

  validates :show_live_my_voters_extra_time, :numericality => { :only_integer => true}
  validates :show_final_results_extra_time, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0}

  default_scope order(:position)

end
