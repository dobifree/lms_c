# == Schema Information
#
# Table name: informative_documents
#
#  id                :integer          not null, primary key
#  position          :integer
#  name              :string(255)
#  description       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  inf_doc_folder_id :integer
#  employee_access   :boolean          default(TRUE)
#  boss_access       :boolean          default(TRUE)
#  superior_access   :boolean          default(TRUE)
#

class InformativeDocument < ActiveRecord::Base

  belongs_to :inf_doc_folder
  attr_accessible :description, :name, :position, :inf_doc_folder_id,
                  :employee_access, :boss_access, :superior_access

  default_scope order('position DESC, name DESC')


  def reader_profile(employee_user, reader_user)
    case true
      when employee_user == reader_user
        0 # employee
      when employee_user.boss == reader_user
        1 # boss
      when reader_user.es_jefe_de(employee_user)
        2 # superior
      else
        3 # others
    end
  end

  def profile_has_permission(profile_id)
    case profile_id
      when 0 # employee
        employee_access?
      when 1 # boss
        boss_access?
      when 2 # superior
        superior_access?
      when 3 # others
        false
    end
  end
end
