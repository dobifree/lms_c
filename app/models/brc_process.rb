# == Schema Information
#
# Table name: brc_processes
#
#  id             :integer          not null, primary key
#  period         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  active         :boolean          default(TRUE)
#  year           :integer
#  uf_value       :float
#  from_date      :datetime
#  to_date        :datetime
#  choose_date    :datetime
#  liq_process_id :integer
#  def_date       :datetime
#  val_date       :datetime
#  secu_cohade_id :integer
#  alias          :string(255)      default("Bono Rol Comercial")
#

class BrcProcess < ActiveRecord::Base

  belongs_to :liq_process
  belongs_to :secu_cohade
  has_many :brc_members, dependent: :destroy
  has_many :brc_definers, dependent: :destroy
  has_many :brc_process_categ_vals, dependent: :destroy

  attr_accessible :period, :active, :year, :uf_value, :from_date, :to_date, :choose_date, :liq_process_id, :def_date,
                  :val_date, :secu_cohade_id, :alias

  validates :period, presence: true
  validates :year, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :uf_value, presence: true, numericality: { greater_than: 0 }
  #validates :from_date, presence: true
  #validates :to_date, presence: true
  validates :def_date, presence: true
  validates :choose_date, presence: true
  validates :val_date, presence: true
  validates :liq_process_id, presence: true

  before_validation :set_liq_p
  before_save :correct_dates

  default_scope order('active DESC, year DESC, period DESC')

  def period_name
    BrcProcess.periods[self.period]
  end

  def brc_members_ordered
    self.brc_members.joins(:user).reorder('apellidos, nombre')
  end

  def brc_members_ordered_by_companies_ids(companies_ids)

    characteristic_company = Characteristic.get_characteristic_company

    users_id = UserCharacteristic.where('characteristic_id = ? AND characteristic_value_id IN (?)', characteristic_company.id, companies_ids).pluck('user_id')

    self.brc_members.joins(:user).where('user_id IN (?)', users_id).reorder('apellidos, nombre')

  end

  def brc_members_ordered_by_companies_ids_editable(companies_ids)

    characteristic_company = Characteristic.get_characteristic_company

    users_id = UserCharacteristic.where('characteristic_id = ? AND characteristic_value_id IN (?)', characteristic_company.id, companies_ids).pluck('user_id')

    self.brc_members.joins(:user).where('user_id IN (?)', users_id).reorder('apellidos, nombre').readonly(false)

  end

  def brc_members_ordered
    self.brc_members.joins(:user).reorder('activo, apellidos, nombre')
  end

  def brc_definers_ordered
    self.brc_definers.joins(:user).reorder('apellidos, nombre')
  end

  def brc_member(user)
    self.brc_members.where('user_id = ?', user.id).first
  end

  def brc_definer(user)
    self.brc_definers.where('user_id = ?', user.id).first
  end

  def brc_definers_by_validator(user)
    self.brc_definers.where('user_validate_id = ?', user.id)
  end

  def num_brc_members_to_define_by_companies_ids(companies_ids)

    characteristic_company = Characteristic.get_characteristic_company

    users_id = UserCharacteristic.where('characteristic_id = ? AND characteristic_value_id IN (?)', characteristic_company.id, companies_ids).pluck('user_id')

    self.brc_members.where('user_id IN (?)', users_id).count
  end

  def num_brc_members_to_define_by_companies_ids_pending(companies_ids)

    characteristic_company = Characteristic.get_characteristic_company

    users_id = UserCharacteristic.where('characteristic_id = ? AND characteristic_value_id IN (?)', characteristic_company.id, companies_ids).pluck('user_id')

    self.brc_members.where('user_id IN (?) AND status_def = ?', users_id, false).count
  end

  def num_brc_members_to_validate_by_companies_ids_pending(companies_ids)

    characteristic_company = Characteristic.get_characteristic_company

    users_id = UserCharacteristic.where('characteristic_id = ? AND characteristic_value_id IN (?)', characteristic_company.id, companies_ids).pluck('user_id')

    self.brc_members.where('user_id IN (?) AND status_val = ?', users_id, false).count
  end

  def self.periods

    { 'a1' => 'Anual',
      's1' => 'Semestre 1', 's2' => 'Semestre 2',
      'c1' => 'Cuatrimestre 1', 'c2' => 'Cuatrimestre 2', 'c3' => 'Cuatrimestre 3',
      't1' => 'Trimestre 1', 't2' => 'Trimestre 2', 't3' => 'Trimestre 3', 't4' => 'Trimestre 4',
      'b1' => 'Bimestre 1', 'b2' => 'Bimestre 2', 'b3' => 'Bimestre 3', 'b4' => 'Bimestre 4', 'b5' => 'Bimestre 5', 'b6' => 'Bimestre 6'
    }

  end

  def self.periods_select_array
    [['Anual','a1'],
     ['Semestre 1','s1'],['Semestre 2','s2'],
     ['Cuatrimestre 1','c1'],['Cuatrimestre 2','c2'],['Cuatrimestre 3','c3'],
     ['Trimestre 1','t1'],['Trimestre 2','t2'],['Trimestre 3','t3'],['Trimestre 4','t4'],
     ['Bimestre 1','b1'],['Bimestre 2','b2'],['Bimestre 3','b3'],['Bimestre 4','b4'],['Bimestre 5','b5'],['Bimestre 6','b6']]
  end

  def set_liq_p

    if self.year

      m = if self.period == 'a1'
            12
          elsif self.period == 's1'
            6
          elsif self.period == 's2'
            12
          elsif self.period == 'c1'
            4
          elsif self.period == 'c2'
            8
          elsif self.period == 'c3'
            12
          elsif self.period == 't1'
            3
          elsif self.period == 't2'
            6
          elsif self.period == 't3'
            9
          elsif self.period == 't4'
            12
          elsif self.period == 'b1'
            2
          elsif self.period == 'b2'
            4
          elsif self.period == 'b3'
            6
          elsif self.period == 'b4'
            8
          elsif self.period == 'b5'
            10
          elsif self.period == 'b6'
            12
          end

      lp = LiqProcess.where('year = ? and month = ?', self.year, m).first
      self.liq_process = lp

    else
      self.liq_process = nil
    end

  end

  def  correct_dates
    self.to_date = self.to_date+((24*60*60)-1).seconds if self.to_date
    self.def_date = self.def_date+((24*60*60)-1).seconds if self.def_date
    self.val_date = self.val_date+((24*60*60)-1).seconds if self.val_date
    self.choose_date = self.choose_date+((24*60*60)-1).seconds if self.choose_date
  end

end
