# == Schema Information
#
# Table name: user_ct_module_privileges_um_custom_reports
#
#  id                           :integer          not null, primary key
#  user_ct_module_privileges_id :integer
#  um_custom_report_id          :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

class UserCtModulePrivilegesUmCustomReport < ActiveRecord::Base
  belongs_to :user_ct_module_privileges
  belongs_to :um_custom_report
  # attr_accessible :title, :body
end
