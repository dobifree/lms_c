# == Schema Information
#
# Table name: log_logins
#
#  id                    :integer          not null, primary key
#  user_id               :integer
#  fecha                 :datetime
#  http_user_agent       :string(255)
#  browser               :string(255)
#  version               :string(255)
#  platform              :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  ip                    :string(255)
#  active_directory_json :text
#

class LogLogin < ActiveRecord::Base
  belongs_to :user
  attr_accessible :browser, :fecha, :http_user_agent, :ip, :platform, :version
end
