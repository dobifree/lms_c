# == Schema Information
#
# Table name: pe_dimension_groups
#
#  id              :integer          not null, primary key
#  position        :integer
#  name            :string(255)
#  description     :text
#  min_sign        :string(255)
#  min_percentage  :float
#  max_sign        :string(255)
#  max_percentage  :float
#  pe_dimension_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  text_color      :string(255)
#

class PeDimensionGroup < ActiveRecord::Base

  belongs_to :pe_dimension

  attr_accessible :description, :max_percentage, :max_sign, :min_percentage, :min_sign, :name, :position, :text_color, :pe_dimension_id

  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :name, presence: true
  validates :text_color, presence: true
  validates :min_sign, presence: true
  #validates :min_percentage, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :min_percentage, presence: true
  validates :max_sign, presence: true
  #validates :max_percentage, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :max_percentage, presence: true

  default_scope order('position ASC, name ASC')

end
