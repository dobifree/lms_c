# == Schema Information
#
# Table name: company_units
#
#  id                  :integer          not null, primary key
#  nombre              :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  country_id          :integer
#  planning_process_id :integer
#

class CompanyUnit  < ActiveRecord::Base

  belongs_to :country
  belongs_to :planning_process

  has_many :company_unit_areas
  has_many :planning_process_company_units
  has_many :planning_processes, through: :planning_process_company_units

  attr_accessible :country_id, :nombre, :planning_process_id

  validates :nombre, presence: true, uniqueness: { scope: :planning_process_id, case_sensitive: false }
  validates :country_id, presence: true

  default_scope order('nombre ASC')

end
