# == Schema Information
#
# Table name: pe_elements
#
#  id                                  :integer          not null, primary key
#  name                                :string(255)
#  pe_evaluation_id                    :integer
#  pe_element_id                       :integer
#  assessed                            :boolean          default(TRUE)
#  visible                             :boolean          default(TRUE)
#  assessment_method                   :integer
#  calculus_method                     :integer
#  max_number                          :integer
#  min_number                          :integer
#  max_weight                          :integer
#  min_weight                          :integer
#  weight_sum                          :integer
#  element_def_by                      :integer
#  element_def_by_rol                  :string(255)
#  assessment_method_def_by            :integer
#  assessment_method_def_by_rol        :string(255)
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  display_name                        :boolean          default(TRUE)
#  alternatives_display_method         :integer
#  position                            :integer          default(1)
#  simple                              :boolean          default(TRUE)
#  kpi_dashboard_id                    :integer
#  allows_individual_comments          :boolean          default(FALSE)
#  available_indicator_types           :string(255)
#  display_weight                      :boolean          default(FALSE)
#  min_numeric_value                   :integer
#  max_numeric_value                   :integer
#  display_result                      :boolean          default(FALSE)
#  plural_name                         :string(255)      default("")
#  allow_individual_comments_with_boss :boolean          default(FALSE)
#  fix_rank_models                     :integer          default(0)
#  allow_activities                    :boolean          default(FALSE)
#  allow_action_plan                   :boolean          default(FALSE)
#  allow_note                          :boolean          default(FALSE)
#  alias_note                          :string(255)      default("Nota")
#  alias_comment                       :string(255)      default("Comentario")
#  alias_comment_2                     :string(255)      default("Comentario")
#  desc_placeholder                    :string(255)
#  note_placeholder                    :string(255)
#  default_goal                        :string(255)
#  default_unit                        :string(255)
#  note_compulsory                     :boolean          default(FALSE)
#  cant_change_goal                    :boolean          default(FALSE)
#  unit_allow_digits                   :boolean          default(TRUE)
#

class PeElement < ActiveRecord::Base

  belongs_to :pe_evaluation
  belongs_to :pe_element
  belongs_to :kpi_dashboard

  has_many :pe_elements, dependent: :destroy #
  has_many :pe_element_descriptions, dependent: :destroy #
  #has_many :pe_question_ranks, dependent: :destroy
  has_many :pe_question_models, dependent: :destroy #
  has_many :pe_question_rank_models, dependent: :destroy #
  #has_many :pe_question_activity_fields

  attr_accessible :assessed, :assessment_method, :assessment_method_def_by, :assessment_method_def_by_rol,
                  :calculus_method, :element_def_by, :element_def_by_rol, :max_number, :max_weight, :min_number,
                  :min_weight, :name, :pe_element_id, :visible, :display_name, :display_weight, :alternatives_display_method,
                  :weight_sum, :pe_evaluation_id, :position, :simple, :kpi_dashboard_id, :allows_individual_comments,
                  :available_indicator_types, :min_numeric_value, :max_numeric_value, :display_result, :plural_name,
                  :allow_individual_comments_with_boss, :fix_rank_models, :allow_action_plan,
                  :allow_note, :alias_note, :alias_comment, :alias_comment_2, :desc_placeholder, :note_placeholder,
                  :default_goal, :default_unit, :note_compulsory, :cant_change_goal, :unit_allow_digits

  before_save { self.max_number = nil if self.max_number.blank? }
  before_save { self.min_number = nil if self.min_number.blank? }
  before_save { self.max_weight = nil if self.max_weight.blank? }
  before_save { self.min_weight = nil if self.min_weight.blank? }
  before_save { self.weight_sum = nil if self.weight_sum.blank? }
  before_save { self.element_def_by_rol = nil if self.element_def_by_rol.blank? }
  before_save { self.assessment_method_def_by_rol = nil if self.assessment_method_def_by_rol.blank? }
  before_save { self.available_indicator_types = nil if self.available_indicator_types.blank? }
  before_save { self.kpi_dashboard_id = nil if self.assessment_method != 2}

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_evaluation_id }
  validates :plural_name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_evaluation_id }
  validates :pe_evaluation_id, presence: true

  default_scope order('pe_elements.position ASC')


  def self.element_def_by_list
    [:administrador, :usuarios, :funciones_de_puestos]
  end

  def self.assessment_methods
    [:calculo_automatico, :alternativas, :indicador, :valor_numerico]
  end

  def self.calculus_methods
    [:promedio_ponderado, :suma_ponderada]
  end

  def self.assessment_method_def_by_list
    [:administrador, :usuarios]
  end

  def self.alternatives_display_methods
    [:combo_select, :radio_filas, :radio_columnas]
  end

  def self.fix_rank_models_list
    [:sin_restriccion, :meta, :porcentaje_logro]
  end

  def has_limited_available_indicator_types?

    available_indicator_types_selected_array = Array.new
    available_indicator_types_selected_array = self.available_indicator_types.split('-') if self.available_indicator_types

    if available_indicator_types_selected_array.size > 0
      true
    else
      false
    end

  end

  def indicator_types_array_for_select

    array_for_select = Array.new

    available_indicator_types_selected_array = Array.new
    available_indicator_types_selected_array = self.available_indicator_types.split('-') if self.available_indicator_types

    PeQuestion.indicator_types.each_with_index do |it, index|

      if available_indicator_types_selected_array.size > 0 && available_indicator_types_selected_array.include?(index.to_s)

        array_for_select.push [it, index]

      #elsif available_indicator_types_selected_array.size == 0

      #  array_for_select.push [it, index]

      end

    end

    return array_for_select

  end

  def pe_question_rank_models_discrete

    self.pe_question_rank_models.where('discrete = ?', true)

  end

  def pe_question_rank_models_non_discrete

    self.pe_question_rank_models.where('discrete = ?', false)

  end

end
