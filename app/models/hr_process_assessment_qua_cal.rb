# == Schema Information
#
# Table name: hr_process_assessment_qua_cals
#
#  id                                  :integer          not null, primary key
#  hr_process_calibration_session_id   :integer
#  hr_process_calibration_session_m_id :integer
#  hr_process_quadrant_id              :integer
#  fecha                               :datetime
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  comentario                          :text
#

class HrProcessAssessmentQuaCal < ActiveRecord::Base
  belongs_to :hr_process_calibration_session
  belongs_to :hr_process_calibration_session_m
  belongs_to :hr_process_quadrant
  attr_accessible :fecha, :comentario, :hr_process_calibration_session_id, :hr_process_calibration_session_m_id, :hr_process_quadrant_id
end
