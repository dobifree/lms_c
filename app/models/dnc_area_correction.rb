# == Schema Information
#
# Table name: dnc_area_corrections
#
#  id         :integer          not null, primary key
#  correccion :text
#  dnc_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  revisado   :boolean          default(FALSE)
#

class DncAreaCorrection < ActiveRecord::Base
  belongs_to :dnc
  attr_accessible :correccion, :revisado
end
