# == Schema Information
#
# Table name: pe_question_validations
#
#  id               :integer          not null, primary key
#  pe_question_id   :integer
#  step_number      :integer
#  before_accept    :boolean
#  validated        :boolean          default(FALSE)
#  comment          :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  pe_evaluation_id :integer
#  attended         :boolean          default(FALSE)
#

class PeQuestionValidation < ActiveRecord::Base

  belongs_to :pe_question
  belongs_to :pe_evaluation

  attr_accessible :before_accept, :comment, :step_number, :validated, :attended

end
