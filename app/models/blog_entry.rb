# == Schema Information
#
# Table name: blog_entries
#
#  id                    :integer          not null, primary key
#  user_id               :integer
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  blog_form_id          :integer
#

class BlogEntry < ActiveRecord::Base
  belongs_to :user
  belongs_to :registered_by_user, :class_name => 'User'
  belongs_to :blog_form
  has_many :blog_entry_items, :dependent => :destroy
  accepts_nested_attributes_for :blog_entry_items, :allow_destroy => true
  attr_accessible :registered_at, :registered_by_user_id, :blog_form_id,
                  :blog_entry_items_attributes, :user_id
end
