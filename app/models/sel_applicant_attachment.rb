# == Schema Information
#
# Table name: sel_applicant_attachments
#
#  id                    :integer          not null, primary key
#  sel_applicant_id      :integer
#  description           :string(255)
#  crypted_name          :string(255)
#  original_filename     :string(255)
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class SelApplicantAttachment < ActiveRecord::Base
  belongs_to :sel_applicant
  belongs_to :registered_by_user, class_name: 'User'
  attr_accessible :crypted_name, :description, :original_filename, :registered_at,
                  :registered_by_user, :registered_by_user_id,
                  :sel_applicant, :sel_applicant_id

  def self.find_by_complex_crypted_name(complex_crypted_name)
    underscore_index = complex_crypted_name.index('_')
    id = complex_crypted_name[0..underscore_index]
    crypted_name = complex_crypted_name[underscore_index+1..-1]
    self.where(:id => id, :crypted_name => crypted_name).first
  end

  def _crypted_name
    self.id.to_s + '_' + self.crypted_name
  end
end
