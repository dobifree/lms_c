# == Schema Information
#
# Table name: characteristic_types
#
#  id         :integer          not null, primary key
#  position   :integer
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CharacteristicType < ActiveRecord::Base

  has_many :characteristics

  attr_accessible :name, :position

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :position, presence: true,  numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  default_scope order('position ASC, name ASC')

  def public_characteristics
    self.characteristics.where('publica = ?', true)
  end

  def restricted_to_managers_characteristics
    self.characteristics.where('publica = ? AND restricted_to_managers = ?', true, true)
  end

  def private_characteristics
    self.characteristics.where('publica = ?', false)
  end

  #def characteristics_available_to_users
  #  self.characteristics.where('publica = ? AND restricted_to_managers = ?', true, false)
  #end

  def characteristics_available_to_boss
    self.characteristics.where('publica = ? AND restricted_to_managers = ? AND query_by_boss = ?', true, false, true)
  end

  def characteristics_available_to_own_user
    self.characteristics.where('publica = ? AND restricted_to_managers = ? AND query_by_user = ?', true, false, true)
  end

  def characteristics_available_to_others
    self.characteristics.where('publica = ? AND restricted_to_managers = ? AND query_by_others = ?', true, false, true)
  end

  def characteristics_updated_by_users
    self.characteristics.where('publica = ? AND restricted_to_managers = ? AND updated_by_user = ?', true, false, true)
  end

  def characteristics_available_to_managers
    self.characteristics.where('publica = ?', true)
  end

  def characteristics_updated_by_managers
    self.characteristics.where('publica = ? AND updated_by_manager = ?', true, true)
  end

  def excel_report_available_characteristics
    self.characteristics.where('publica = ? AND data_type_id NOT IN (6,7,11) AND register_characteristic_id IS NULL', true)
  end

end
