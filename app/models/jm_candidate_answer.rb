# == Schema Information
#
# Table name: jm_candidate_answers
#
#  id                    :integer          not null, primary key
#  jm_candidate_id       :integer
#  jm_characteristic_id  :integer
#  position              :integer
#  active                :boolean          default(TRUE)
#  registered_at         :datetime
#  registered_by_user_id :integer
#  prev_answer_id        :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class JmCandidateAnswer < ActiveRecord::Base
  belongs_to :jm_candidate
  belongs_to :jm_characteristic
  has_many :jm_answer_values
  has_one :prev_answer, :class_name => 'JmCandidateAnswer'
  has_many :sel_applicant_records
  accepts_nested_attributes_for :jm_answer_values
  accepts_nested_attributes_for :sel_applicant_records
  attr_accessible :active, :position, :prev_answer_id, :registered_at, :registered_by_user_id, :jm_characteristic_id,
                  :jm_answer_values_attributes,
                  :sel_applicant_records_attributes
end
