# == Schema Information
#
# Table name: program_course_instances
#
#  id                            :integer          not null, primary key
#  program_course_id             :integer
#  program_instance_id           :integer
#  from_date                     :datetime
#  to_date                       :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  sence_hour_value              :float
#  sence_student_value           :float
#  sence_hours                   :float
#  sence_training_type           :string(255)
#  sence_region                  :string(255)
#  sence_otec_name               :string(255)
#  sence_otec_address            :string(255)
#  sence_otec_telephone          :string(255)
#  place                         :string(255)
#  contract_type                 :string(255)
#  minimum_grade                 :integer
#  minimum_assistance            :integer
#  sence_maximum_students_number :integer
#  sence_cbipartito              :string(255)
#  sence_dnc                     :string(255)
#  sence_comment                 :string(255)
#  sence_schedule                :string(255)
#  sale_hour_value               :float
#  sencenet                      :string(255)
#  purchase_order_number         :string(255)
#  sk_financing                  :string(255)
#  sk_external_certification     :boolean
#  sence_minimum_students_number :integer
#  city                          :string(255)
#  obra                          :string(255)
#  proyecto_ci                   :string(255)
#  description                   :string(255)
#  creator_id                    :integer
#

class ProgramCourseInstance < ActiveRecord::Base

  belongs_to :program_course
  belongs_to :program_instance
  belongs_to :creator, class_name: 'User', foreign_key: 'creator_id'

  has_many :user_courses
  has_many :polls
  has_many :program_course_poll_summaries

  has_many :program_course_instance_units, :dependent => :destroy
  has_many :program_course_instance_evaluations, :dependent => :destroy
  has_many :program_course_instance_polls, :dependent => :destroy

  has_many :course_training_characteristics, :dependent => :destroy
  has_many :program_course_instance_managers, :dependent => :destroy

  attr_accessible :from_date, :to_date, :program_course_id, :program_instance_id,
                  :sence_hour_value, :sence_student_value, :sence_hours, :sence_training_type,
                  :sence_region, :sence_otec_name, :sence_otec_address, :sence_otec_telephone,
                  :place, :contract_type, :minimum_grade, :minimum_assistance, :sence_maximum_students_number,
                  :sence_cbipartito, :sence_dnc, :sence_comment, :sence_schedule,
                  :sale_hour_value, :sencenet, :purchase_order_number,
                  :sk_financing, :sk_external_certification, :sence_minimum_students_number,
                  :city, :obra, :proyecto_ci, :description, :creator_id

  before_save { self.sence_hour_value = nil if self.sence_hour_value.blank? }
  before_save { self.sence_student_value = nil if self.sence_student_value.blank? }

  #validates :minimum_grade, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  #validates :minimum_assistance, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  #validates :sence_hour_value, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  #validates :sence_student_value, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  #validates :sale_hour_value, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true

  def matriculas_vigentes

    self.user_courses.where('anulado = ? ', false).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_vigentes_asistieron_algo

    self.user_courses.where('anulado = ? AND asistencia_unidades > ?', false, 0).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_vigentes_read_only_false

    self.user_courses.where('anulado = ? ', false).joins(:user).reorder('apellidos, nombre, numero_oportunidad').readonly(false)

  end

  def user_course_by_user_read_only_false(user)
    self.user_courses.joins(:user).where('users.id = ?', user.id).readonly(false).first
  end

  def num_matriculas_vigentes_asistieron_algo
    self.user_courses.where('anulado = ? AND asistencia_unidades > ?', false, 0).size
  end

  def total_horas
    if self.program_course_instance_units.size > 0
      th = 0
      self.program_course_instance_units.each { |pciu| th += (pciu.to_date-pciu.from_date)/3600 }
    else
      th = self.program_course.course.dedicacion_estimada
    end
    th

  end

  def course_training_characteristic(training_characteristic)
    self.course_training_characteristics.where('training_characteristic_id = ?', training_characteristic.id).first
  end

  def training_characteristic_value(training_characteristic)
    ctc = self.course_training_characteristic training_characteristic
    ctc.value if ctc
  end

  def formatted_training_characteristic_value(training_characteristic)
    ctc = self.course_training_characteristic training_characteristic
    ctc.formatted_value if ctc
  end

  def excel_formatted_training_characteristic_value(training_characteristic)
    ctc = self.course_training_characteristic training_characteristic
    ctc.excel_formatted_value if ctc
  end

  def managers_ordered
    self.program_course_instance_managers.joins(:user).order('apellidos, nombre')
  end

  def get_program_course_poll_summary(poll, master_poll_question, master_poll_alternative)
    self.program_course_poll_summaries.where('poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ?', poll.id, master_poll_question.id, master_poll_alternative.id).first
  end

end
