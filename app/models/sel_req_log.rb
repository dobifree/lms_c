# == Schema Information
#
# Table name: sel_req_logs
#
#  id                    :integer          not null, primary key
#  sel_requirement_id    :integer
#  log_state_id          :integer
#  registered_at         :datetime
#  registered_by_user_id :integer
#  comment               :text
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class SelReqLog < ActiveRecord::Base
  belongs_to :sel_requirement
  belongs_to :registered_by_user, class_name: 'User'
  attr_accessible :comment, :log_state_id, :registered_at, :registered_by_user_id

  def log_state_name
    case log_state_id
      when 0
        'Borrador'
      when 1
        'Registrada'
      when 2
        'Aprobada'
      when 3
        'Devuelta'
      when 4
        'Eliminada'
      when 5
        'En Atención'
      when 6
        'En Aprobación'
      when 7
        'En Curso'
      when 8
        'Rechazada'
    end
  end
end
