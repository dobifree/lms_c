# == Schema Information
#
# Table name: pe_member_group2s
#
#  id            :integer          not null, primary key
#  pe_process_id :integer
#  pe_member_id  :integer
#  pe_group2_id  :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class PeMemberGroup2 < ActiveRecord::Base
  belongs_to :pe_process
  belongs_to :pe_member
  belongs_to :pe_group2
  # attr_accessible :title, :body
end
