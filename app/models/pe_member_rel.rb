# == Schema Information
#
# Table name: pe_member_rels
#
#  id                     :integer          not null, primary key
#  pe_process_id          :integer
#  finished               :boolean          default(FALSE)
#  valid_evaluator        :boolean          default(TRUE)
#  pe_member_evaluator_id :integer
#  pe_rel_id              :integer
#  pe_member_evaluated_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  weight                 :float            default(1.0)
#  validator_id           :integer
#  validated              :boolean          default(FALSE)
#  validation_comment     :text
#  finished_at            :datetime
#  validated_at           :datetime
#  assigned_by_boss       :boolean          default(FALSE)
#

class PeMemberRel < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_rel

  belongs_to :pe_member_evaluator, class_name: 'PeMember'
  belongs_to :pe_member_evaluated, class_name: 'PeMember'

  belongs_to :validator, class_name: 'User'

  has_many :pe_assessment_evaluations, dependent: :destroy #

  has_many :pe_member_rel_shared_comments, dependent: :destroy
  has_many :pe_question_comments, dependent: :destroy #

  attr_accessible :finished, :pe_member_evaluated_id, :pe_member_evaluator_id, :valid_evaluator, :weight,
                  :validator_id, :validated, :validation_comment, :finished_at, :validated_at,
                  :assigned_by_boss

  validates :pe_process_id, presence: true
  validates :pe_member_evaluator_id, presence: true, uniqueness: {scope: [:pe_process_id, :pe_member_evaluated_id] }
  validates :pe_member_evaluated_id, presence: true, uniqueness: {scope: [:pe_process_id, :pe_member_evaluator_id] }

  validates :pe_rel_id, presence: true, uniqueness: {scope: [:pe_process_id, :pe_member_evaluator_id, :pe_member_evaluated_id] }

  def pe_assessment_evaluation(pe_evaluation)
    self.pe_assessment_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first
  end

end
