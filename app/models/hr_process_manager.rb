# == Schema Information
#
# Table name: hr_process_managers
#
#  id            :integer          not null, primary key
#  hr_process_id :integer
#  user_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class HrProcessManager < ActiveRecord::Base

  belongs_to :hr_process
  belongs_to :user

  attr_accessible :hr_process_id, :user_id

  validates :user_id, presence: true, uniqueness: {scope: :hr_process_id}

end
