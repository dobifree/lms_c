# == Schema Information
#
# Table name: master_evaluation_alternatives
#
#  id                            :integer          not null, primary key
#  numero                        :integer
#  texto                         :text
#  correcta                      :boolean
#  master_evaluation_question_id :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#

class MasterEvaluationAlternative < ActiveRecord::Base

  belongs_to :master_evaluation_question

  attr_accessible :correcta, :numero, :texto

  validates :numero, presence: true, uniqueness: { scope: :master_evaluation_question_id }, numericality: { only_integer: true, greater_than: 0 }
  validates :texto, presence: true, uniqueness: { scope: :master_evaluation_question_id }
  #validates :correcta, presence: true

  default_scope order('numero ASC')

  def letra

    letras = %w(a b c d e f g h i j k l m n o p q r s t u v w x y z)
    return letras[self.numero-1]

  end

end
