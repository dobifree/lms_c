# == Schema Information
#
# Table name: scholarship_item_groups
#
#  id                     :integer          not null, primary key
#  scholarship_process_id :integer
#  name                   :string(255)
#  description            :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  position               :integer
#

class ScholarshipItemGroup < ActiveRecord::Base
  belongs_to :scholarship_process
  has_many :scholarship_application_items
  accepts_nested_attributes_for :scholarship_application_items, allow_destroy: true
  attr_accessible :description, :name, :position, :scholarship_application_items_attributes

  default_scope order('position ASC')

  validates :position, presence: true, numericality: true
  validates :name, presence: true
end
