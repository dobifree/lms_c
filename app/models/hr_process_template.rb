# == Schema Information
#
# Table name: hr_process_templates
#
#  id          :integer          not null, primary key
#  nombre      :string(255)
#  descripcion :string(255)
#  activo      :boolean          default(TRUE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class HrProcessTemplate < ActiveRecord::Base

  has_many :hr_process_dimensions
  has_many :hr_process_quadrants
  has_many :hr_processes

  attr_accessible :activo, :descripcion, :nombre

  validates :nombre, presence: true, uniqueness: { case_sensitive: false }

  default_scope order('nombre ASC')

  def quadrant_1d(hr_process_dimension_g_x_id)

    self.hr_process_quadrants.where('hr_process_dimension_g_x_id = ?', hr_process_dimension_g_x_id).first

  end

  def quadrant_2d(hr_process_dimension_g_x_id, hr_process_dimension_g_y_id)

    self.hr_process_quadrants.where('hr_process_dimension_g_x_id = ? AND hr_process_dimension_g_y_id = ?', hr_process_dimension_g_x_id, hr_process_dimension_g_y_id).first

  end

  def hr_process_quadrants_orden_grafico
    self.hr_process_quadrants.reorder(:orden_grafico)
  end

end
