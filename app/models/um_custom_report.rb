# == Schema Information
#
# Table name: um_custom_reports
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UmCustomReport < ActiveRecord::Base

  has_many :um_custom_report_characteristics, dependent: :destroy
  has_many :user_ct_module_privileges_um_custom_reports, dependent: :destroy

  attr_accessible :name

  validates :name, presence: true, uniqueness: { case_sensitive: false }

  default_scope order('name')

  def um_custom_report_characteristic(um_characteristic)
    self.um_custom_report_characteristics.where('um_characteristic_id = ?', um_characteristic.id).first
  end

end
