# == Schema Information
#
# Table name: company_unit_areas
#
#  id              :integer          not null, primary key
#  nombre          :string(255)
#  company_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class CompanyUnitArea  < ActiveRecord::Base

  belongs_to :company_unit

  has_many :company_unit_area_managers
  has_many :area_superintendents

  has_many :dncs
  has_many :dncps


  attr_accessible :nombre

  validates :nombre, presence: true, uniqueness: { scope: :company_unit_id, case_sensitive: false }
  validates :company_unit_id, presence: true

  default_scope order('nombre ASC')

end
