# == Schema Information
#
# Table name: tracking_form_due_dates
#
#  id                      :integer          not null, primary key
#  tracking_form_answer_id :integer
#  due_date                :datetime
#  comment                 :text
#  finished                :boolean          default(FALSE)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  tracking_period_item_id :integer
#

class TrackingFormDueDate < ActiveRecord::Base
  belongs_to :tracking_form_answer
  belongs_to :tracking_period_item
  has_many :tracking_milestones, :dependent => :destroy
  accepts_nested_attributes_for :tracking_milestones, :allow_destroy => true

  attr_accessible :comment, :due_date, :finished, :tracking_period_item_id, :tracking_milestones_attributes, :tracking_form_answer_id
end
