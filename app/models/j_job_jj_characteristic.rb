# == Schema Information
#
# Table name: j_job_jj_characteristics
#
#  id                                :integer          not null, primary key
#  j_job_id                          :integer
#  jj_characteristic_id              :integer
#  jj_characteristic_value_id        :integer
#  register_job_jj_characteristic_id :integer
#  value_string                      :string(255)
#  value_text                        :text
#  value_date                        :date
#  value_int                         :integer
#  value_float                       :float
#  value_file                        :string(255)
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#

class JJobJjCharacteristic < ActiveRecord::Base
  belongs_to :j_job
  belongs_to :jj_characteristic
  belongs_to :jj_characteristic_value
  belongs_to :register_job_jj_characteristic, class_name: 'JJobJjCharacteristic'


  has_many :elements_job_characteristics, class_name: 'JJobJjCharacteristic', foreign_key: 'register_job_jj_characteristic_id', dependent: :destroy


  attr_accessible :jj_characteristic_value_id,
                  :register_job_jj_characteristic_id,
                  :value_date,
                  :value_file,
                  :value_float,
                  :value_int,
                  :value_string,
                  :value_text

  def value
    if self.jj_characteristic.data_type_id == 0
      self.value_string
    elsif self.jj_characteristic.data_type_id == 1
      self.value_text
    elsif self.jj_characteristic.data_type_id == 2
      self.value_date
    elsif self.jj_characteristic.data_type_id == 3
      self.value_int
    elsif self.jj_characteristic.data_type_id == 4
      self.value_float
    elsif self.jj_characteristic.data_type_id == 5
      self.jj_characteristic_value ? self.jj_characteristic_value.id : nil
    elsif self.jj_characteristic.data_type_id == 6
      self.value_file
    end

  end

  def formatted_value
    if self.jj_characteristic.data_type_id == 0
      self.value_string
    elsif self.jj_characteristic.data_type_id == 1
      self.value_text
    elsif self.jj_characteristic.data_type_id == 2
      self.value_date ? I18n.localize(self.value_date, format: :day_snmonth_year) : nil
    elsif self.jj_characteristic.data_type_id == 3
      self.value_int
    elsif self.jj_characteristic.data_type_id == 4
      self.value_float
    elsif self.jj_characteristic.data_type_id == 5
      self.jj_characteristic_value ? self.jj_characteristic_value.value_string : nil
    elsif self.jj_characteristic.data_type_id == 6
      self.value_file
    end

  end

  def update_value(new_value, company)

    #self.valor = new_value.strip unless self.characteristic.data_type_id == 6

    if self.jj_characteristic.data_type_id == 0
      #string
      self.value_string = new_value
    elsif self.jj_characteristic.data_type_id == 1
      #text
      self.value_text = new_value
    elsif self.jj_characteristic.data_type_id == 2
      #date
      self.value_date = new_value
    elsif self.jj_characteristic.data_type_id == 3
      #int
      self.value_int = new_value
    elsif self.jj_characteristic.data_type_id == 4
      #float
      self.value_float = new_value
    elsif self.jj_characteristic.data_type_id == 5
      #lista
      self.jj_characteristic_value = jj_characteristic.jj_characteristic_values.where('id = ?',new_value).first
    elsif self.jj_characteristic.data_type_id == 6
      #file

      self.delete_file company

      directory = company.directorio+'/'+company.codigo+'/j_job_profiles/'+self.j_job.id.to_s+'/characteristics/'+self.jj_characteristic.id.to_s+'/'

      FileUtils.mkdir_p directory unless File.directory? directory

      name = self.jj_characteristic.id.to_s+'_'+SecureRandom.hex(5)+File.extname(new_value.original_filename)
      path = File.join(directory, name)
      File.open(path, 'wb') { |f| f.write(new_value.read) }

      self.value_file = name

    elsif self.jj_characteristic.data_type_id == 11
      #register
      self.value_string = new_value.strip
    end

  end

  def delete_file(company)

    if self.jj_characteristic.data_type_id == 6 && self.value_file

      if File.exist?(company.directorio+'/'+company.codigo+'/j_job_profiles/'+self.j_job.id.to_s+'/characteristics/'+self.jj_characteristic.id.to_s+'/'+self.value_file)

        File.delete company.directorio+'/'+company.codigo+'/j_job_profiles/'+self.j_job.id.to_s+'/characteristics/'+self.jj_characteristic.id.to_s+'/'+self.value_file

      end

    end

  end

  def elements_j_job_jj_characteristics_by_characteristic(jj_characteristic)
    self.elements_job_characteristics.where('jj_characteristic_id = ?', jj_characteristic.id).first
  end

end
