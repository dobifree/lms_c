# == Schema Information
#
# Table name: kpi_dashboards
#
#  id                          :integer          not null, primary key
#  name                        :string(255)
#  period                      :string(255)
#  from                        :date
#  to                          :date
#  frequency                   :string(255)
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  upper_limit                 :float
#  lower_limit                 :float
#  csv                         :boolean
#  kpi_dashboard_type_id       :integer
#  def_view_kpis               :boolean          default(TRUE)
#  def_view_table              :boolean          default(TRUE)
#  def_view_percentage         :boolean          default(TRUE)
#  pt_rows                     :text
#  pt_cols                     :text
#  pt_aggregator               :text
#  pt_aggregator_field         :text
#  show_indicator_final_result :boolean          default(FALSE)
#  allow_subsets               :boolean          default(FALSE)
#  subset_alias                :string(255)
#  show_set_final_result       :boolean          default(FALSE)
#  show_set_results            :boolean
#  kpi_field_annual_goal       :boolean          default(FALSE)
#  def_view_goal               :boolean          default(TRUE)
#  allow_set_colors            :boolean          default(FALSE)
#  allow_indicator_colors      :boolean          default(FALSE)
#  w_p                         :boolean          default(TRUE)
#

class KpiDashboard < ActiveRecord::Base

  belongs_to :kpi_dashboard_type

  has_many :kpi_dashboard_managers, dependent: :destroy
  has_many :managers, through: :kpi_dashboard_managers, source: :user

  has_many :kpi_dashboard_guests, dependent: :destroy
  has_many :guests, through: :kpi_dashboard_guests, source: :user

  has_many :kpi_sets, dependent: :destroy

  has_many :kpi_colors, dependent: :destroy

  has_many :kpi_rols, dependent: :destroy

  attr_accessible :frequency, :from, :name, :period, :to, :upper_limit, :lower_limit, :csv, :kpi_dashboard_type_id,
                  :def_view_kpis, :def_view_table, :def_view_percentage, :def_view_goal,
                  :pt_rows, :pt_cols, :pt_aggregator, :pt_aggregator_field,
                  :show_indicator_final_result, :allow_subsets, :subset_alias,
                  :show_set_final_result, :show_set_results, :kpi_field_annual_goal,
                  :allow_set_colors, :allow_indicator_colors, :w_p

  validates :name, presence: true
  validates :from, presence: true
  validates :to, presence: true
  validates :frequency, presence: true
  validates :upper_limit, numericality: {greater_than_or_equal_to: 0 }, allow_nil: true
  validates :lower_limit, numericality: {greater_than_or_equal_to: 0 }, allow_nil: true

  default_scope order('period DESC, name ASC')

  def kpi_dashboard_managers_ordered

    self.kpi_dashboard_managers.joins(:user).order('apellidos, nombre')

  end

  def kpi_dashboard_guests_ordered

    self.kpi_dashboard_guests.joins(:user).order('apellidos, nombre')

  end



end
