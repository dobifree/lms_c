# == Schema Information
#
# Table name: sc_he_block_params
#
#  id                     :integer          not null, primary key
#  entry_min              :integer
#  entry_max              :integer
#  entry_include_min      :boolean          default(TRUE)
#  exit_min               :integer
#  exit_max               :integer
#  exit_include_min       :boolean          default(TRUE)
#  sc_block_id            :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  entry_active           :boolean          default(TRUE)
#  exit_active            :boolean          default(TRUE)
#  registered_at          :datetime
#  registered_by_user_id  :integer
#

class ScHeBlockParam < ActiveRecord::Base
  belongs_to :sc_block
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :entry_min,
                  :entry_max,
                  :entry_include_min,
                  :entry_active,
                  :exit_min,
                  :exit_max,
                  :exit_include_min,
                  :exit_active,
                  :active,
                  :sc_block_id,
                  :deactivated_at,
                  :deactivated_by_user_id

  validates :entry_min, :allow_nil => true, :numericality => {:greater_than_or_equal_to => 0}
  validates :entry_max, :allow_nil => true, :numericality => {:greater_than_or_equal_to => 0}
  validates :exit_min, :allow_nil => true, :numericality => {:greater_than_or_equal_to => 0}
  validates :exit_max, :allow_nil => true, :numericality => {:greater_than_or_equal_to => 0}

  validates :entry_include_min, inclusion: { in: [true, false] }
  validates :exit_include_min, inclusion: { in: [true, false] }
  validates :active, inclusion: { in: [true, false] }


  def different?(edited_sc_he_block_params)
    if self.entry_min == edited_sc_he_block_params.entry_min and
        self.entry_max == edited_sc_he_block_params.entry_max and
        self.entry_include_min == edited_sc_he_block_params.entry_include_min and
        self.entry_active == edited_sc_he_block_params.entry_active and
        self.exit_min == edited_sc_he_block_params.exit_min and
        self.exit_max == edited_sc_he_block_params.exit_max and
        self.exit_include_min == edited_sc_he_block_params.exit_include_min and
        self.exit_active == edited_sc_he_block_params.exit_active
      return false
    else
      return true
    end
  end

end
