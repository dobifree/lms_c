# == Schema Information
#
# Table name: scholarship_application_values
#
#  id                              :integer          not null, primary key
#  scholarship_application_id      :integer
#  scholarship_application_item_id :integer
#  value_int                       :integer
#  value_list_item_id              :integer
#  value_string                    :string(255)
#  value_text                      :text
#  value_date                      :date
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

class ScholarshipApplicationValue < ActiveRecord::Base
  belongs_to :scholarship_application
  belongs_to :scholarship_application_item
  belongs_to :value_list_item, class_name: 'ScholarshipListItem'
  attr_accessible :value_date, :value_int, :value_list_item_id, :value_string,
                  :value_text, :scholarship_application_item_id
  def formatted_value
    case scholarship_application_item.item_type
      when 1
        value_string
      when 2
        value_int
      when 3
        value_date ? I18n.localize(value_date, format: :day_snmonth_year) : nil
      when 4
        value_list_item.name if value_list_item
      when 5
        'Archivo no soportado'
      when 6
        value_text
      when 7
        value_list_item.name if value_list_item
    end
  end
end
