# == Schema Information
#
# Table name: dnc_students
#
#  id         :integer          not null, primary key
#  dnc_id     :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DncStudent < ActiveRecord::Base

  belongs_to :dnc
  belongs_to :user

  validates :dnc_id, presence: true
  validates :user_id, presence: true
  validates :user_id, uniqueness: { scope: :dnc_id }

  attr_accessible :user_id

  default_scope joins(:user).order('users.apellidos ASC, users.nombre ASC')

end
