# == Schema Information
#
# Table name: program_inspectors
#
#  id         :integer          not null, primary key
#  program_id :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProgramInspector < ActiveRecord::Base

  belongs_to :program
  belongs_to :user

  attr_accessible :user_id

  validates :user_id, uniqueness: { scope: :program_id }

end
