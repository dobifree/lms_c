# == Schema Information
#
# Table name: hr_process_assessment_quas
#
#  id                     :integer          not null, primary key
#  hr_process_user_id     :integer
#  hr_process_quadrant_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  fecha                  :datetime
#

class HrProcessAssessmentQua < ActiveRecord::Base
  belongs_to :hr_process_user
  belongs_to :hr_process_quadrant
  attr_accessible :hr_process_user_id, :hr_process_quadrant_id, :fecha
end
