# == Schema Information
#
# Table name: sel_applicant_records
#
#  id                     :integer          not null, primary key
#  sel_applicant_id       :integer
#  jm_candidate_answer_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class SelApplicantRecord < ActiveRecord::Base
  belongs_to :sel_applicant
  belongs_to :jm_candidate_answer
  attr_accessible :sel_applicant_id
end
