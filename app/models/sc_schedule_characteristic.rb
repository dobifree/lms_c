# == Schema Information
#
# Table name: sc_schedule_characteristics
#
#  id                :integer          not null, primary key
#  active_gui        :boolean          default(FALSE)
#  pos_gui           :integer
#  active_condition  :boolean          default(FALSE)
#  pos_condition     :integer
#  characteristic_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class ScScheduleCharacteristic < ActiveRecord::Base
  belongs_to :characteristic
  attr_accessible :active_condition, :active_gui, :pos_condition, :pos_gui

  def self.condition
    ScScheduleCharacteristic.where(:active_condition => true).order(:pos_condition)
  end

  def self.gui
    ScScheduleCharacteristic.where(:active_gui => true).order(:pos_gui)
  end

  def self.by_characteristic characteristic
    ScScheduleCharacteristic.where(:characteristic_id => characteristic.id).first_or_create!
  end

  def name; self.characteristic.nombre end
  def data_type_id; self.characteristic.data_type_id end
end
