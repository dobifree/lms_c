# == Schema Information
#
# Table name: folder_file_values
#
#  id             :integer          not null, primary key
#  folder_file_id :integer
#  user_id        :integer
#  fecha          :datetime
#  valor          :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class FolderFileValue < ActiveRecord::Base

  belongs_to :folder_file
  belongs_to :user

  attr_accessible :fecha, :valor

  validates :valor, presence: true
  validates :folder_file_id, uniqueness: { scope: :user_id }

end
