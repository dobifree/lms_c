# == Schema Information
#
# Table name: elec_rounds
#
#  id                    :integer          not null, primary key
#  elec_event_id         :integer
#  elec_process_round_id :integer
#  from_date             :datetime
#  to_date               :datetime
#  done                  :boolean          default(FALSE)
#  final                 :boolean          default(FALSE)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  done_by_user_id       :integer
#  done_at               :datetime
#

class ElecRound < ActiveRecord::Base
  belongs_to :elec_event
  belongs_to :elec_process_round
  belongs_to :done_by_user, class_name: 'User'
  has_many :elec_votes
  has_many :elec_round_winners
  attr_accessible :done, :final, :from_date, :to_date, :done_by_user_id, :done_at

  def process_name_with_name_event
    self.elec_process_round.elec_process.name + (self.elec_event.name.blank? ? '' : ' - ' + self.elec_event.name)
  end

  def process_name_with_round_name
    self.elec_process_round.elec_process.name + (self.elec_process_round.name.blank? ? '' : ' - ' + self.elec_process_round.name)
  end

  def self.actives_to_user_to_answer(user_id, lms_time)
    actives_to_user_to_answer = []

    self.joins(:elec_process_round => :elec_process).where(:done => false, elec_processes: {:active => true}).where('? BETWEEN from_date AND to_date', lms_time).each do |round|

      if round_pass_filter_characteristics?(round, user_id)
        actives_to_user_to_answer.append(round)
      end

    end

    #TODO : revisar porqué necesité pornerle el parche del uniq
    return actives_to_user_to_answer.uniq

  end

  def self.actives_to_user_to_see_live_results_extra_time(user_id, lms_time)
    actives_to_user_to_answer = []

    self.joins(:elec_process_round => :elec_process).where(:done => false, elec_processes: {:active => true}, elec_process_rounds: {:show_live_results => true}).where('to_date < ?', lms_time).each do |round|
      if round.to_date >= (lms_time - round.elec_process_round.show_live_results_extra_time.minutes)

        if round_pass_filter_characteristics?(round, user_id)
          actives_to_user_to_answer.append(round)
        end

      end
    end


    #TODO : revisar porqué necesité pornerle el parche del uniq
    return actives_to_user_to_answer.uniq

  end

  def self.actives_to_user_to_see_final_results_extra_time(user_id, lms_time)
    actives_to_user_to_answer = []

    self.joins(:elec_process_round => :elec_process).where(:done => true, elec_processes: {:active => true}, elec_process_rounds: {:show_final_results => true}).each do |round|
      if round.to_date >= (lms_time - round.elec_process_round.show_final_results_extra_time.minutes)

        if round_pass_filter_characteristics?(round, user_id)
          actives_to_user_to_answer.append(round)
        end
      end

    end


    #TODO : revisar porqué necesité pornerle el parche del uniq
    return actives_to_user_to_answer.uniq

  end

  def self.users_list_voted_by_category(round, category, user, char_group_value = nil)
    user_list_no_group = users_list_voted_by_category_no_group(round, category)

    #TODO: solo funciona con el primer filtro
    elec_char_group = round.elec_process_round.elec_process.elec_characteristics.where(:used_to_group_by => true).first

    if !elec_char_group.nil?
      if char_group_value.nil?
        return filter_user_list_by_group(user_list_no_group, elec_char_group, user, round.elec_event)
      else
        return filter_user_list_by_group_by_value(user_list_no_group, elec_char_group, user, round.elec_event, char_group_value)
      end
    else
      return user_list_no_group
    end

  end

  private

  def self.filter_user_list_by_group(user_list_no_group, elec_characteristic, user, elec_event)
    #TODO: solo funciona con el primer filtro
    user_list_filtered = []

    user_list_no_group.each do |user_candidate|


      if user_candidate.elec_historic_characteristics.where(:elec_event_id => elec_event.id, :characteristic_id => elec_characteristic.characteristic_id).first.value ==
          user.elec_historic_characteristics.where(:elec_event_id => elec_event.id, :characteristic_id => elec_characteristic.characteristic_id).first.value
        user_list_filtered << user_candidate
      end
    end

    return user_list_filtered
  end

  def self.filter_user_list_by_group_by_value(user_list_no_group, elec_characteristic, user, elec_event, group_value)

    #TODO: solo funciona con el primer filtro
    user_list_filtered = []


    user_list_no_group.each do |user_candidate|
      if user_candidate.elec_historic_characteristics.where(:elec_event_id => elec_event.id, :characteristic_id => elec_characteristic.characteristic_id).first.value.upcase == group_value.upcase
        user_list_filtered << user_candidate
      end
    end

    return user_list_filtered

  end

  def self.users_list_voted_by_category_no_group(round, category)

    User.find_by_sql ['select distinct users.*, count(*) as votos
                      from users inner join elec_votes
                      on (users.id = elec_votes.candidate_user_id)
                      where elec_votes.elec_round_id = ?
                      and elec_votes.elec_process_category_id = ?
                      group by users.id
                      order by count(*) DESC, users.apellidos, users.nombre', round, category]
  end

  def self.round_pass_filter_characteristics?(round, user_id)
    round.elec_process_round.elec_process.elec_process_categories.each do |category|
      if category.elec_category_characteristics.where(:filter_voter => true).each do |filter_voter|
        if value = UserCharacteristic.where(:user_id => user_id, :characteristic_id => filter_voter.characteristic_id).first
          #TODO: validar forma de comparación para las mayúsculas y las tildes
          if value.formatted_value.upcase == filter_voter.match_value.upcase
            return true
          end
        end
      end.empty?
        return true
      end
    end

    return false

  end

end
