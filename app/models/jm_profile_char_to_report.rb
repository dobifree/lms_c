# == Schema Information
#
# Table name: jm_profile_char_to_reports
#
#  id                         :integer          not null, primary key
#  jm_profile_char_id         :integer
#  jm_subcharacteristic_id    :integer
#  use_to_filter_candidates   :boolean
#  position_filter_candidates :integer
#  use_to_list_candidates     :boolean
#  position_list_candidates   :integer
#  alias                      :string(255)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class JmProfileCharToReport < ActiveRecord::Base
  belongs_to :jm_profile_char
  belongs_to :jm_subcharacteristic
  attr_accessible :alias, :position_filter_candidates, :position_list_candidates, :use_to_filter_candidates, :use_to_list_candidates, :jm_subcharacteristic_id
end
