# == Schema Information
#
# Table name: tracking_form_logs
#
#  id                        :integer          not null, primary key
#  tracking_form_instance_id :integer
#  tracking_form_action_id   :integer
#  registered_at             :datetime
#  registered_by_user_id     :integer
#  comment                   :text
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  redefinition_group        :integer
#

class TrackingFormLog < ActiveRecord::Base
  belongs_to :tracking_form_instance
  belongs_to :tracking_form_action
  belongs_to :registered_by_user, :class_name => 'User'
  has_one :tracking_form_reject, :dependent => :destroy
  accepts_nested_attributes_for :tracking_form_reject
  attr_accessible :comment, :registered_at, :registered_by_user_id, :tracking_form_action_id, :tracking_form_instance_id, :redefinition_group,
      :tracking_form_reject_attributes

  before_save :assign_redefinition_group

  def assign_redefinition_group
    if self.tracking_form_instance.tracking_participant.in_redefinition?
      self.redefinition_group = self.tracking_form_instance.tracking_redefinitions.count
    end
  end

  def is_registered_by_user_or_same_role?(user_id)
    participant = self.tracking_form_instance.tracking_participant
    # validación positiva natural (no se rompe la validación si el usuario no es el que registró)
    return true if (self.registered_by_user_id == user_id)
    # si el usuario es el evaluador && el evaluado no es el que registró && yo no soy el que registró => se asume que fue mi predecesor (same role) (hubo cambio de attendant)
    return true if participant.attendant_user_id == user_id && self.registered_by_user_id != participant.subject_user_id && self.registered_by_user_id != user_id
    false
  end

end
