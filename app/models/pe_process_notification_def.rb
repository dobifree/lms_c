# == Schema Information
#
# Table name: pe_process_notification_defs
#
#  id                                   :integer          not null, primary key
#  pe_process_id                        :integer
#  pe_evaluation_id                     :integer
#  send_email_to_validator_when_defined :boolean
#  to_validator_when_defined_subject    :string(255)
#  to_validator_when_defined_body       :text
#  created_at                           :datetime         not null
#  updated_at                           :datetime         not null
#

class PeProcessNotificationDef < ActiveRecord::Base
  belongs_to :pe_process
  belongs_to :pe_evaluation
  attr_accessible :send_email_to_validator_when_defined, :to_validator_when_defined_body, :to_validator_when_defined_subject
end
