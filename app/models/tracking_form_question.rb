# == Schema Information
#
# Table name: tracking_form_questions
#
#  id                      :integer          not null, primary key
#  tracking_form_group_id  :integer
#  position                :integer
#  name                    :string(255)
#  description             :text
#  min_qty_answers         :integer
#  max_qty_answers         :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  traceable               :boolean          default(FALSE)
#  tracking_period_list_id :integer
#

class TrackingFormQuestion < ActiveRecord::Base
  belongs_to :tracking_form_group
  belongs_to :tracking_period_list
  has_many :tracking_form_items, :dependent => :destroy
  has_many :tracking_form_answers, :dependent => :destroy
  accepts_nested_attributes_for :tracking_form_items, :allow_destroy => true

  attr_accessible :description, :max_qty_answers, :min_qty_answers, :name, :position, :traceable, :tracking_period_list_id,
                  :tracking_form_items_attributes

  validates :position, :numericality => true
  validates :name, :presence => true
  validates :min_qty_answers, :numericality => {:greater_than_or_equal_to => 0}
  validates :max_qty_answers, :numericality => {:greater_than_or_equal_to => :min_qty_answers}

  default_scope { order(:position, :name) }

  def use_discrete_due_dates?
    !self.tracking_period_list_id.nil?
  end

end
