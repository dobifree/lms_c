# == Schema Information
#
# Table name: program_course_poll_summaries
#
#  id                         :integer          not null, primary key
#  program_course_id          :integer
#  poll_id                    :integer
#  grupo                      :integer
#  master_poll_question_id    :integer
#  master_poll_alternative_id :integer
#  numero_respuestas          :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  program_course_instance_id :integer
#

class ProgramCoursePollSummary < ActiveRecord::Base

  belongs_to :program_course
  belongs_to :program_course_instance
  belongs_to :poll
  belongs_to :master_poll_question
  belongs_to :master_poll_alternative


  attr_accessible :grupo, :numero_respuestas, :master_poll_alternative_id, :program_course_instance_id

  validates :numero_respuestas, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }



end
