# == Schema Information
#
# Table name: sc_user_processes
#
#  id                        :integer          not null, primary key
#  user_id                   :integer
#  sc_process_id             :integer
#  he_total                  :decimal(12, 4)   default(0.0)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  he_send_to_validation     :decimal(12, 4)   default(0.0)
#  he_sent_to_validation     :decimal(12, 4)   default(0.0)
#  he_approved               :decimal(12, 4)   default(0.0)
#  he_rejected               :decimal(12, 4)   default(0.0)
#  he_not_charged_to_payroll :decimal(12, 4)   default(0.0)
#

class ScUserProcess < ActiveRecord::Base
  has_many :sc_records
  belongs_to :user
  belongs_to :sc_process

  attr_accessible :he_total,
                  :sc_process_id,
                  :user_id,
                  :he_send_to_validation,
                  :he_sent_to_validation,
                  :he_approved,
                  :he_rejected,
                  :he_not_charged_to_payroll

  validates :user_id, presence: true, :uniqueness => {:scope => :sc_process_id}
  validates :he_total, :presence => true
  validates :sc_process_id, :presence => true

  def he_total_formatted
    "%02d:%02d" % [self.he_total/60, self.he_total%60] if self.he_total
  end

  def user_cod_formatted
    return nil unless user
    return user.codigo.insert(-2, '-')
  end

end
