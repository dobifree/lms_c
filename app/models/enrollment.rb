# == Schema Information
#
# Table name: enrollments
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  program_id          :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  level_id            :integer
#  program_instance_id :integer
#  aprobado            :boolean
#  active              :boolean          default(TRUE)
#

class Enrollment < ActiveRecord::Base
  
  belongs_to :program
  belongs_to :program_instance
  belongs_to :user
  belongs_to :level

  has_many :user_courses
  has_many :program_courses, through: :user_courses
  
  attr_accessible :program, :user, :level, :program_instance, :aprobado, :active

  validates :program, presence: true
  validates :user, presence: true
  validates :user_id, uniqueness: { scope: [:program_id, :program_instance_id] }
  validates :level, presence: true


  validate :program_level
 
  def program_level
  	
  	unless level_id.nil?

  		if Level.find(level_id).program.id != program_id
	      errors.add(:mismatch_program_level, 'El nivel debe pertenecer al mismo programa')
	    end	

  	end
    
  end

  def user_courses_order_by_nombre_secuencia
    self.user_courses.joins(:course).where('anulado = ?', 0).order('nombre, id')
  end

  def user_courses_order_by_nombre_asc_secuencia_desc
    self.user_courses.joins(:course).where('anulado = ?', 0).order('nombre, id DESC')
  end

  def user_courses_by_program_course_secuencia_desc(program_course)
    self.user_courses.where('program_course_id = ? AND anulado = ?', program_course.id, 0).reorder('id DESC')
  end

  def years
    f = self.user_courses.where('desde IS NOT NULL').reorder('desde DESC').pluck(:desde).map { |fecha| fecha.year }
    f.uniq
  end

  def visible_program_courses_by_year_ordered(year)
    self.program_courses.joins(:course).where('hidden_in_user_courses_list = ? AND year(user_courses.desde) = ?', false, year).uniq.order('courses.nombre')
  end

end
