# == Schema Information
#
# Table name: tracking_period_lists
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  code        :string(255)
#  description :text
#  active      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class TrackingPeriodList < ActiveRecord::Base

  has_many :tracking_period_items, :dependent => :destroy
  accepts_nested_attributes_for :tracking_period_items, :allow_destroy => true

  attr_accessible :active, :code, :description, :name, :tracking_period_items_attributes

  validates :code, :presence => true
  validates :name, :presence => true
end
