# == Schema Information
#
# Table name: master_evaluations
#
#  id          :integer          not null, primary key
#  numero      :integer
#  nombre      :string(255)
#  descripcion :text
#  tags        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  has_topics  :boolean          default(FALSE)
#

class MasterEvaluation < ActiveRecord::Base

  has_many :master_evaluation_topics, dependent: :destroy
  has_many :master_evaluation_questions, dependent: :destroy
  has_many :user_course_evaluation_questions
  has_many :evaluations
  has_many :user_course_evaluations, through: :evaluations


  attr_accessible :descripcion, :nombre, :numero, :tags, :has_topics

  before_validation :set_numero, on: :create

  validates :nombre, presence: true
  validates :numero, presence: true, uniqueness: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('numero ASC')

  def set_numero
    self.numero = 1
    max_master_evaluation = MasterEvaluation.reorder('numero DESC').first
    self.numero = max_master_evaluation.numero+1 if max_master_evaluation
  end

  def simple?

    simple = false

    simple = true if self.master_evaluation_topics.count == 0

    return simple

  end


end
