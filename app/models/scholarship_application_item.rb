# == Schema Information
#
# Table name: scholarship_application_items
#
#  id                        :integer          not null, primary key
#  scholarship_item_group_id :integer
#  position                  :integer
#  name                      :string(255)
#  description               :text
#  mandatory                 :boolean
#  item_type                 :integer
#  scholarship_list_id       :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  reference_date_today      :boolean
#  reference_date_specific   :date
#  reference_char_id         :integer
#

class ScholarshipApplicationItem < ActiveRecord::Base
  belongs_to :scholarship_item_group
  belongs_to :scholarship_list
  belongs_to :reference_char, class_name: 'Characteristic'
  has_many :scholarship_application_item_ranges, dependent: :destroy
  accepts_nested_attributes_for :scholarship_application_item_ranges, allow_destroy: true
  attr_accessible :description, :item_type, :name, :position, :mandatory,
                  :scholarship_list_id, :reference_date_today,
                  :reference_date_specific, :reference_char_id,
                  :scholarship_application_item_ranges_attributes

  default_scope order('position ASC')

  before_validation :reset_not_needed_data

  validates :position, numericality: true
  validates :name, presence: true
  validates :item_type, presence: true
  validates :scholarship_list_id, presence: true, if: :item_type_needs_a_list?
  validates :reference_date_specific, presence: true, if: :item_type_needs_a_reference_date?
  validates :reference_char_id, presence: true, if: :item_type_needs_a_reference_char?

  def self.alternative_item_type_active_ordered
    Hash[alternative_item_type_hash.delete_if {|_id, type| !type[:active]}.sort_by {|_id, type| type[:position]}]
  end

  def self.alternative_item_type_hash
    {1 => { name: 'Texto', position: 1, need_a_list: false,
           its_a_file: false, auto_fill: false, need_a_reference_date: false,
            need_a_reference_char: false, active: true },
     2 => { name: 'Número', position: 2, need_a_list: false,
           its_a_file: false, auto_fill: false, need_a_reference_date: false,
            need_a_reference_char: false, active: true },
     3 => { name: 'Fecha', position: 3, need_a_list: false,
           its_a_file: false, auto_fill: false, need_a_reference_date: false,
            need_a_reference_char: false, active: true },
     4 => { name: 'Selección', position: 4, need_a_list: true,
           its_a_file: false, auto_fill: false, need_a_reference_date: false,
            need_a_reference_char: false, active: true },
     5 => { name: 'Archivo', position: 5, need_a_list: false,
           its_a_file: true, auto_fill: false, need_a_reference_date: false,
            need_a_reference_char: false, active: false },
     6 => { name: 'Texto largo', position: 6, need_a_list: false,
           its_a_file: false, auto_fill: false, need_a_reference_date: false,
            need_a_reference_char: false, active: true },
     7 => { name: 'Antigüedad', position: 7, need_a_list: true,
           its_a_file: false, auto_fill: true, need_a_reference_date: true,
            need_a_reference_char: true, active: true }
    }
  end

  def item_type_needs_a_list?
    !item_type.nil? &&
      self.class.alternative_item_type_hash[item_type][:need_a_list]
  end

  def item_type_needs_a_reference_date?
    !item_type.nil? && !reference_date_today &&
      self.class.alternative_item_type_hash[item_type][:need_a_reference_date]
  end

  def item_type_needs_a_reference_char?
    !item_type.nil? &&
      self.class.alternative_item_type_hash[item_type][:need_a_reference_char]
  end

  private

  def reset_not_needed_data
    self.scholarship_list_id = nil unless item_type_needs_a_list?
    self.reference_date_specific = nil unless item_type_needs_a_reference_date?
    #self.reference_date_today = nil unless item_type_needs_a_reference_date?
    self.reference_char_id = nil unless item_type_needs_a_reference_char?
  end

end
