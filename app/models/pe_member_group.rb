# == Schema Information
#
# Table name: pe_member_groups
#
#  id               :integer          not null, primary key
#  pe_process_id    :integer
#  pe_member_id     :integer
#  pe_evaluation_id :integer
#  pe_group_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PeMemberGroup < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_member
  belongs_to :pe_evaluation
  belongs_to :pe_group

  validates :pe_process_id, presence: true
  validates :pe_member_id, presence: true
  validates :pe_evaluation_id, presence: true
  validates :pe_group_id, presence: true, uniqueness: {scope: [:pe_process_id, :pe_member_id, :pe_evaluation_id] }

  # attr_accessible :title, :body


end
