# == Schema Information
#
# Table name: medlic_reports
#
#  id                     :integer          not null, primary key
#  partial                :boolean          default(FALSE)
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  medlic_process_id      :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class MedlicReport < ActiveRecord::Base
  belongs_to :medlic_process
  has_many :medlic_reported_licenses

  attr_accessible :partial,
                  :medlic_process_id

  def self.active
    where(active: true)
  end

  def self.only_partial
    where(partial: true)
  end

  def self.non_partial
    where(partial: false)
  end

  def self.from_process(id)
    where(medlic_process_id: id)
  end

  def self.except_this(id)
    where('id != ?', id)
  end

end
