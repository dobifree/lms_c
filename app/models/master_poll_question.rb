# == Schema Information
#
# Table name: master_poll_questions
#
#  id                      :integer          not null, primary key
#  texto                   :text
#  master_poll_id          :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  respuesta_unica         :boolean
#  answer_type             :integer          default(0)
#  required_alternative_id :integer
#  group                   :string(255)
#  required                :boolean          default(FALSE)
#

class MasterPollQuestion < ActiveRecord::Base

  belongs_to :master_poll
  belongs_to :required_alternative, class_name: 'MasterPollAlternative'

  has_many :master_poll_alternatives
  has_many :master_poll_alternative_requirements

  attr_accessible :texto, :master_poll, :master_poll_id, :group, :required

  validates :texto, presence: true

  default_scope order('id ASC')

  def self.answer_type_alternatives
    ['Única'.to_sym, 'Múltiple'.to_sym, 'Texto'.to_sym]
  end

end
