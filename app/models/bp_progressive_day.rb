# == Schema Information
#
# Table name: bp_progressive_days
#
#  id                           :integer          not null, primary key
#  bp_general_rules_vacation_id :integer
#  year                         :integer
#  days                         :integer
#  active                       :boolean          default(TRUE)
#  deactivated_at               :datetime
#  deactivated_by_admin_id      :integer
#  registered_at                :datetime
#  registered_by_admin_id       :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

class BpProgressiveDay < ActiveRecord::Base
  belongs_to :bp_general_rules_vacation
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id

  attr_accessible :bp_general_rules_vacation_id,
                  :year,
                  :days,
                  :active,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id
end
