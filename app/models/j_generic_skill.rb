# == Schema Information
#
# Table name: j_generic_skills
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  description    :text
#  j_job_level_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class JGenericSkill < ActiveRecord::Base

  belongs_to :j_job_level

  attr_accessible :description, :name, :j_job_level_id

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :j_job_level_id }
  validates :j_job_level_id, presence: true

  default_scope order('name')

end
