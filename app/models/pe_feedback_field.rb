# == Schema Information
#
# Table name: pe_feedback_fields
#
#  id                               :integer          not null, primary key
#  pe_process_id                    :integer
#  position                         :integer
#  name                             :string(255)
#  simple                           :boolean          default(TRUE)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  field_type                       :integer          default(0)
#  pe_feedback_field_list_id        :integer
#  max_number_of_compound_feedbacks :integer          default(3)
#  required                         :boolean          default(TRUE)
#  pe_feedback_field_list_item_id   :integer
#  list_item_selected               :boolean          default(TRUE)
#

class PeFeedbackField < ActiveRecord::Base


  belongs_to :pe_feedback_field_list_item

  belongs_to :pe_process
  belongs_to :pe_feedback_field_list

  has_many :pe_member_feedbacks, dependent: :destroy

  has_many :pe_feedback_compound_fields

  attr_accessible :name, :position, :simple, :pe_process_id, :field_type, :pe_feedback_field_list_id,
                  :max_number_of_compound_feedbacks, :required, :pe_feedback_field_list_item_id, :list_item_selected

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_process_id }

  default_scope order('position, name')

  def self.field_types
    [:texto, :fecha, :lista]
  end

  def field_type_name
    PeFeedbackField.field_types[self.field_type]
  end

end
