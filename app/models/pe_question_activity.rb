# == Schema Information
#
# Table name: pe_question_activities
#
#  id                                      :integer          not null, primary key
#  position                                :integer
#  pe_question_activity_field_id           :integer
#  value_string                            :string(255)
#  value_date                              :date
#  pe_question_activity_field_list_item_id :integer
#  user_creator_id                         :integer
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  pe_question_id                          :integer
#  pe_question_activity_id                 :integer
#

class PeQuestionActivity < ActiveRecord::Base

  belongs_to :pe_question
  belongs_to :pe_question_activity
  belongs_to :pe_question_activity_field
  belongs_to :pe_question_activity_field_list_item

  belongs_to :user_creator, class_name: 'User'

  has_many :pe_question_activities, dependent: :destroy

  attr_accessible :position, :user_creator_id, :value_date, :value_string, :pe_question_activity_field_list_item_id, :pe_question_id

  validates :value_string, :presence => true, :if => :require_string?
  validates :value_date, :presence => true, :if => :require_date?
  validates :pe_question_activity_field_list_item_id, :presence => true, :if => :require_list?

  def require_string?
    self.pe_question_activity && self.pe_question_activity_field.field_type == 0 && self.pe_question_activity_field.required
  end

  def require_date?
    self.pe_question_activity && self.pe_question_activity_field.field_type == 1 && self.pe_question_activity_field.required
  end

  def require_list?
    self.pe_question_activity && self.pe_question_activity_field.field_type == 2 && self.pe_question_activity_field.required
  end

  def formatted_value
    if pe_question_activity_field.field_type == 0
      #text
      self.value_string
    elsif pe_question_activity_field.field_type == 1
      #date
      self.value_date ? I18n.localize(self.value_date, format: :day_snmonth_year) : nil
    elsif pe_question_activity_field.field_type == 2
      #list
      self.pe_question_activity_field_list_item.description if self.pe_question_activity_field_list_item
    end
  end

  def pe_question_activity_by_field(pe_question_activity_field)
    self.pe_question_activities.where('pe_question_activity_field_id = ?', pe_question_activity_field.id).first
  end

end
