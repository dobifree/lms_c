# == Schema Information
#
# Table name: course_training_characteristics
#
#  id                               :integer          not null, primary key
#  course_id                        :integer
#  training_characteristic_id       :integer
#  value_string                     :string(255)
#  value_int                        :integer
#  training_characteristic_value_id :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  program_course_instance_id       :integer
#

class CourseTrainingCharacteristic < ActiveRecord::Base

  belongs_to :course
  belongs_to :training_characteristic
  belongs_to :training_characteristic_value

  attr_accessible :value_int, :value_string, :training_characteristic_value_id, :training_characteristic_id, :program_course_instance_id

  def value

    if self.training_characteristic.data_type_id == 0
      self.value_string
    elsif self.training_characteristic.data_type_id == 1
      self.value_int
    elsif self.training_characteristic.data_type_id == 2
      self.training_characteristic_value ? self.training_characteristic_value.id : nil
    end

  end

  def formatted_value

    if self.training_characteristic.data_type_id == 0
      self.value_string
    elsif self.training_characteristic.data_type_id == 1
      self.value_int
    elsif self.training_characteristic.data_type_id == 2
      self.training_characteristic_value ? self.training_characteristic_value.value_string : nil
    end

  end

  def excel_formatted_value
    if self.training_characteristic.data_type_id == 0
      self.value_string
    elsif self.training_characteristic.data_type_id == 1
      self.value_int
    elsif self.training_characteristic.data_type_id == 2
      self.training_characteristic_value ? self.training_characteristic_value.value_string : nil
    end

  end

end
