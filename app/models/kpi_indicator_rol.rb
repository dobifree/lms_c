# == Schema Information
#
# Table name: kpi_indicator_rols
#
#  id               :integer          not null, primary key
#  kpi_indicator_id :integer
#  kpi_rol_id       :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#

class KpiIndicatorRol < ActiveRecord::Base

  belongs_to :kpi_indicator
  belongs_to :kpi_rol
  belongs_to :user

  attr_accessible :kpi_indicator_id, :kpi_rol_id, :user_id


end
