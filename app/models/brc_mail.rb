# == Schema Information
#
# Table name: brc_mails
#
#  id                       :integer          not null, primary key
#  def_begin_subject        :string(255)
#  def_begin_body           :text
#  def_rej_subject          :string(255)
#  def_rej_body             :text
#  val_begin_subject        :string(255)
#  val_begin_body           :text
#  def_remind_subject       :string(255)
#  def_remind_body          :text
#  val_remind_subject       :string(255)
#  val_remind_body          :text
#  member_begin_subject     :string(255)
#  member_begin_body        :text
#  member_begin_extra1_body :text
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class BrcMail < ActiveRecord::Base
  attr_accessible :def_begin_body, :def_begin_subject, :def_rej_body, :def_rej_subject, :def_remind_body, :def_remind_subject, :member_begin_body, :member_begin_extra1_body, :member_begin_subject, :val_begin_body, :val_begin_subject, :val_remind_body, :val_remind_subject
end
