# == Schema Information
#
# Table name: pe_assessment_final_evaluations
#
#  id                             :integer          not null, primary key
#  pe_process_id                  :integer
#  pe_member_id                   :integer
#  pe_evaluation_id               :integer
#  points                         :float
#  percentage                     :float
#  last_update                    :datetime
#  registered_by_manager          :boolean          default(FALSE)
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  pe_dimension_group_id          :integer
#  pe_area_id                     :integer
#  pe_original_dimension_group_id :integer
#  calibration_comment            :text
#

class PeAssessmentFinalEvaluation < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_member
  belongs_to :pe_area
  belongs_to :pe_evaluation
  belongs_to :pe_dimension_group
  belongs_to :pe_original_dimension_group, class_name: 'PeDimensionGroup'

  has_many :pe_assessment_final_questions, dependent: :destroy

  attr_accessible :last_update, :percentage, :points, :registered_by_manager, :original_percentage, :calibration_comment

  validates :percentage, presence: true, numericality: true
  validates :points, presence: true, numericality: true

  validates :pe_evaluation_id, presence: true, uniqueness: {scope: [:pe_member_id, :pe_area_id]}

  def pe_assessment_final_question(pe_question)
    self.pe_assessment_final_questions.where('pe_question_id = ?', pe_question.id).first
  end

end
