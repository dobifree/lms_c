# == Schema Information
#
# Table name: brg_manage_relations
#
#  id                     :integer          not null, primary key
#  manager_id             :integer
#  managee_id             :integer
#  group_to_manage_id     :integer
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class BrgManageRelation < ActiveRecord::Base

  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :manager, class_name: 'BrgManager', foreign_key: :manager_id
  belongs_to :managee, class_name: 'BrgManager', foreign_key: :managee_id
  belongs_to :group_to_manage, class_name: 'BrgGroup', foreign_key: :group_to_manage_id

  attr_accessible :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :group_to_manage_id,
                  :managee_id,
                  :manager_id,
                  :registered_at,
                  :registered_by_user_id

  validates :manager_id, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validate :verify_group_or_managee
  validate :verify_manager_group
  validate :verify_manager_managee
  validate :verify_circle

  def self.my_managees_for(manager_user_id, process)
    managers = []
    groups = process.brg_groups.active.pluck(:id)
    group_managers = BrgManager.managers_of_group(groups)
    group_managers.each do |manager|
      next unless  manager_managers_is_there(manager, manager_user_id, process)
      managers << manager
    end
    managers
  end

  def self.manager_managers_is_there(manager, user_id, process)
    return true if manager.user_id == user_id.to_i
    manager_managers = BrgManager.managers_of_manager(manager.user_id, process.id)
    if manager_managers.size > 0
      manager_managers.each do |manager_1|
        return manager_managers_is_there(manager_1, user_id, process)
      end
    else
      manager.user_id.to_i == user_id.to_i
    end
  end

  def editable?
    active
  end

  def self.by_process(this_id)
    joins([manager: :brg_process]).where(brg_managers: {brg_process_id: this_id}).readonly(false)
  end

  def full_registered_by_user(show_cod)
    return nil unless registered_by_user
    full_user_gen(registered_by_user, show_cod)
  end

  def full_deactivated_by_user(show_cod)
    return nil unless deactivated_by_user
    full_user_gen(deactivated_by_user, show_cod)
  end

  def full_managee(show_cod)
    if managee
      full_user_gen(managee.user, show_cod)
    else
      if group_to_manage
        group_to_manage.full_group_name
      else
        ''
      end
    end
  end

  def full_user_gen(user, show_cod)
    full_user =  user.apellidos.to_s+', '+user.nombre.to_s
    full_user = user.codigo.to_s+' '+full_user if show_cod
    full_user
  end

  def self.active
    where(active: true)
  end

  def self.only_with_groups
    where('group_to_manage_id is not null')
  end

  def self.only_with_managees
    where('managee_id is not null')
  end

  def self.relation_of(manager_id, managee_id, group_id)
    return nil unless managee_id || group_id
    if managee_id
      BrgManageRelation.active.where(manager_id: manager_id, managee_id: managee_id).first
    else
      BrgManageRelation.active.where(manager_id: manager_id, group_to_manage_id: group_id).first
    end
  end

  private

  def verify_group_or_managee
    return if group_to_manage_id || managee_id
    errors.add(:managee_id, 'no puede ser vacío y grupo también')
  end

  def verify_manager_group
    return if id
    return unless group_to_manage_id
    return unless BrgManageRelation.where(active: true, manager_id: manager_id, group_to_manage_id: group_to_manage_id).first
    errors.add(:manager_id, 'ya se encuentra activo como subgestor asignado')
  end

  def verify_manager_managee
    return if id
    return unless managee_id
    return unless BrgManageRelation.where(active: true, manager_id: manager_id, managee_id: managee_id).first
    errors.add(:manager_id, 'ya se encuentra activo como subgestor asignado')
  end

  def verify_circle
    return unless manager_id
    return unless managee_id
    array = []
    BrgManageRelation.only_with_managees.active.each { |manage_rel| array << manage_rel.manager.user_id.to_s+'.'+manage_rel.managee.user_id.to_s if manage_rel.managee.active && manage_rel.manager.active }
    array << manager.user_id.to_s+'.'+managee.user_id.to_s
    result = evaluate_circle(array)
    return unless result.size > 0
    message = 'Hay conflicto con de redundancia en relación de responsables: '
    result.each do |res|
      splited = res.split('.')
      managee = User.find(splited[1])
      manager = User.find(splited[0])
      message += manager.comma_full_name+' ('+manager.codigo+')'+' valida a '+managee.comma_full_name+' ('+managee.codigo+') - '
    end
    errors.add(:managee_id, message)
  end

  def evaluate_circle(array)
    array_1 = array.dup
    new_arr = ','+(array.dup * ",")
    next_it = false

    array.each do |subarr|
      next if new_arr.include?(','+subarr.split('.')[1]+'.')
      array_1.delete(subarr)
      next_it = true
    end

    if next_it
      evaluate_circle(array_1)
    else
      erase_not(array_1)
    end
  end

  def erase_not(array)
    array_1 = array.dup
    new_arr = (array.dup * ",")+','
    array.each { |subarr| array_1.delete(subarr) unless new_arr.include?('.'+subarr.split('.')[0]+',') }
    array_1
  end

end
