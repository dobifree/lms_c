# == Schema Information
#
# Table name: j_job_levels
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class JJobLevel < ActiveRecord::Base

  has_many :j_jobs
  has_many :j_generic_skills

  attr_accessible :name

  validates :name, presence: true, uniqueness: { case_sensitive: false }

  default_scope order('name')

end
