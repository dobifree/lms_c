# == Schema Information
#
# Table name: planning_process_company_unit_areas
#
#  id                               :integer          not null, primary key
#  planning_process_id              :integer
#  planning_process_company_unit_id :integer
#  company_unit_area_id             :integer
#  objetivos                        :string(255)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#

class PlanningProcessCompanyUnitArea < ActiveRecord::Base

  belongs_to :planning_process
  belongs_to :company_unit_area
  belongs_to :planning_process_company_unit

  attr_accessible :objetivos

end
