# == Schema Information
#
# Table name: vac_approvers
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class VacApprover < ActiveRecord::Base
  has_many :vac_user_to_approves

  belongs_to :user
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :user_id,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id

  validates :user_id, presence: true
  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def vac_user_to_approves
    VacUserToApprove.where(vac_approver_id: id)
  end

  def active_approvees
    VacUserToApprove.where(vac_approver_id: id, active: true).joins(:user).where(users: { activo: true})
  end

  def active_approvees_id
    array = active_approvees
    array_2 = []
    array.each { |var| array_2 << var.id }
    array_2
  end

  def active_approvees_user_id
    array = active_approvees
    array_2 = []
    array.each { |var| array_2 << var.user_id }
    array_2
  end

  def self.active_approvers
    VacApprover.where(active: true).joins(:user).where(users: {activo: true})
  end

  def self.approvers_of(approvee_user_id)
    approvee_relations = VacUserToApprove.where(active: true, user_id: approvee_user_id)
    approvers = []
    approvee_relations.each do |ap|
      approver = ap.vac_approver
      next unless approver.active && approver.user.activo
      approvers << ap.vac_approver
    end
    approvers
  end

end
