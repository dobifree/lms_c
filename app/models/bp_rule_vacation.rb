# == Schema Information
#
# Table name: bp_rule_vacations
#
#  id                      :integer          not null, primary key
#  bp_group_id             :integer
#  name                    :string(255)
#  description             :text
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  since                   :datetime
#  until                   :datetime
#  available               :boolean          default(TRUE)
#  unavailable_at          :datetime
#  unavailable_by_admin_id :integer
#

class BpRuleVacation < ActiveRecord::Base
  has_many :bp_condition_vacations

  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id
  belongs_to :bp_group

  attr_accessible :bp_group_id,
                  :name,
                  :description,
                  :since,
                  :until,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id

  validates :bp_group_id, presence: true
  validates :name, presence: true
  validates :since, presence: true
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  validate :since_before_until
  validate :range_since_concordance_1
  validate :range_since_concordance_2
  validate :range_until_concordance_1

  def active?(now)
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  def self.only_available
    where(available: true)
  end

  def self.active_rule_vacations(now)
    rule_vacations = BpRuleVacation.reorder('bp_group_id ASC, name ASC').all
    rule_vacations_to_return = []
    rule_vacations.each do |rule_vacation|
      next unless rule_vacation.active?(now)
      rule_vacations_to_return << rule_vacation
    end
    return rule_vacations_to_return
  end

  def deactivated_by_admin_formatted
    return deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  def self.active_rules_vacations(now, now_2, user_id)
    return false unless now
    return false unless now_2
    return false unless user_id
    group = BpGroup.user_active_group(now, user_id)
    return false unless group
    rules_vacation = []
    group.bp_rule_vacations.each do |rule_vacation|
      next unless rule_vacation.active?(now) || rule_vacation.active?(now_2)
      next unless rule_vacation.any_available_condition_vacation?
      rules_vacation << rule_vacation
    end
    return false unless rules_vacation.size > 0
    rules_vacation
  end

  def any_available_condition_vacation?
    bp_condition_vacations.where(available: true).any?
  end

  def active_conditions(now, now_2)
    conditions = []
    bp_condition_vacations.each do |condition_vacation|
      next unless (condition_vacation.active?(now) || condition_vacation.active?(now_2))
      next unless condition_vacation.available
      conditions << condition_vacation
    end
    return false if conditions.empty?
    conditions
  end

  def evaluate_benefits(now, begin_vacs, end_vacs, days_vacs, days_prog, anniversary, user_id, next_vac, next_pro_days, request)
    conditions = active_conditions(begin_vacs.to_datetime, end_vacs.to_datetime)
    return false unless conditions
    conditions = conditions.sort_by(&:priority)
    conditions.each do |condition|
      next unless condition.satisfy?(begin_vacs, end_vacs, days_vacs, days_prog, anniversary, user_id, next_vac, next_pro_days, request)
      return condition
    end
    nil
  end

  private

  def since_before_until
    return unless self.until && self.since
    return unless self.until < self.since
    errors.add(:until, 'debe ser después de Desde')
  end

  def range_since_concordance_1
    return unless bp_group_id
    return unless since
    return unless bp_group.since && since < bp_group.since
    errors.add(:since, 'no puede ser menor a la fecha de inicio de activación (desde) del grupo')
  end

  def range_since_concordance_2
    return unless bp_group_id
    return unless since
    return unless bp_group.since && bp_group.until && since > bp_group.until
    errors.add(:since, 'no puede ser mayor a la fecha de fin de activación (hasta) del grupo')
  end

  def range_until_concordance_1
    return unless bp_group_id
    return unless self.until && bp_group.since && bp_group.until && self.until > bp_group.until
    errors.add(:until, 'no puede ser mayor a la fecha de fin de activación (hasta) del grupo')
  end
end
