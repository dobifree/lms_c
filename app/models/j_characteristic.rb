# == Schema Information
#
# Table name: j_characteristics
#
#  id                :integer          not null, primary key
#  characteristic_id :integer
#  position          :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class JCharacteristic < ActiveRecord::Base

  belongs_to :characteristic

  attr_accessible :position, :characteristic_id

  validates :characteristic_id, uniqueness: true

  default_scope order('position')

  def name
    self.characteristic.nombre
  end

end
