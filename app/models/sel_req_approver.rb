# == Schema Information
#
# Table name: sel_req_approvers
#
#  id                    :integer          not null, primary key
#  sel_req_process_id    :integer
#  name                  :string(255)
#  position              :integer
#  mandatory             :boolean
#  value                 :integer
#  conditions_rejection  :boolean
#  prerequisite_position :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  user_id               :integer
#

class SelReqApprover < ActiveRecord::Base
  belongs_to :sel_req_process
  belongs_to :user
  has_one :sel_req_approver_eval
  attr_accessible :conditions_rejection, :mandatory, :name, :position, :prerequisite_position, :value, :user_id
end
