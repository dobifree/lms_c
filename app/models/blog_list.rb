# == Schema Information
#
# Table name: blog_lists
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  active      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class BlogList < ActiveRecord::Base

  has_many :blog_list_items, :dependent => :destroy
  accepts_nested_attributes_for :blog_list_items, :allow_destroy => true

  attr_accessible :active, :description, :name, :blog_list_items_attributes

  validates :name, :presence => true
end
