# == Schema Information
#
# Table name: brc_process_categ_vals
#
#  id                      :integer          not null, primary key
#  brc_process_id          :integer
#  brc_category_id         :integer
#  characteristic_value_id :integer
#  comment_val             :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class BrcProcessCategVal < ActiveRecord::Base
  belongs_to :brc_process
  belongs_to :brc_category
  belongs_to :characteristic_value
  attr_accessible :comment_val
end
