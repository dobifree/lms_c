# == Schema Information
#
# Table name: elec_process_categories
#
#  id              :integer          not null, primary key
#  elec_process_id :integer
#  name            :string(255)
#  description     :text
#  max_votes       :integer          default(1)
#  self_vote       :boolean          default(TRUE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  position        :integer
#  ask_for_comment :boolean          default(FALSE)
#

class ElecProcessCategory < ActiveRecord::Base
  belongs_to :elec_process
  has_many :elec_category_characteristics, :dependent => :destroy
  accepts_nested_attributes_for :elec_category_characteristics, :allow_destroy => true

  attr_accessible :description, :max_votes, :name, :self_vote, :position, :ask_for_comment,
                  :elec_process_id,
                  :elec_category_characteristics_attributes

  validates :name, :presence => true
  validates :position, :presence => true, :numericality => {:only_integer => true}
  validates :max_votes, :presence => true, :numericality => {:only_integer => true, :greater_than_or_equal_to => 1}

  default_scope order(:position)

end
