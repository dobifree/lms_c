# == Schema Information
#
# Table name: jm_messages
#
#  id              :integer          not null, primary key
#  jm_candidate_id :integer
#  sender_user_id  :integer
#  sel_process_id  :integer
#  subject         :string(255)
#  body            :text
#  read            :boolean
#  read_at         :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class JmMessage < ActiveRecord::Base
  belongs_to :jm_candidate
  belongs_to :sel_process
  belongs_to :sender_user, :class_name => 'User'
  attr_accessible :body, :read, :read_at, :sender_user_id, :subject, :sel_process_id, :jm_candidate_id
end
