# == Schema Information
#
# Table name: bp_groups
#
#  id                      :integer          not null, primary key
#  name                    :string(255)
#  description             :text
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  since                   :datetime
#  until                   :datetime
#

class BpGroup < ActiveRecord::Base
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id

  has_many :bp_licenses
  has_many :bp_groups_users
  has_many :bp_general_rules_vacations
  has_many :bp_rule_vacations

  attr_accessible :name,
                  :description,
                  :since,
                  :until,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id

  validates :name, presence: true
  validates :since, presence: true
  validates :registered_by_admin_id, presence: true
  validates :registered_at, presence: true

  validate :since_before_until

  def any_bonus?(this_datetime)
    this_datetime.to_datetime
    bp_licenses.each do |license|
      next unless license.active?(this_datetime)
      return true
    end
    false
  end

  def deactivated_by_admin_formatted
    deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  def active?(now)
    now = now.to_datetime
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  def self.user_active_group(now, user_id)
    user_group = BpGroupsUser.active?(now, user_id)
    return false unless user_group
    return user_group.bp_group if user_group.bp_group.active?(now)
    false
  end

  def self.active_groups(now)
    groups = BpGroup.all
    groups_to_return = []
    groups.each do |group|
      next unless group.active?(now)
      groups_to_return << group
    end
    return groups_to_return
  end

  private

  def since_before_until
    return unless self.until && since && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end
end
