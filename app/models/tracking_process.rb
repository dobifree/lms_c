# == Schema Information
#
# Table name: tracking_processes
#
#  id                       :integer          not null, primary key
#  name                     :string(255)
#  description              :text
#  from_date                :datetime
#  to_date                  :datetime
#  active                   :boolean
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  label_due_date           :string(255)
#  label_comment_due_date   :string(255)
#  ask_for_comment_due_date :boolean          default(FALSE)
#  accepts_redefinitions    :boolean          default(FALSE)
#  not_started_label        :string(255)
#  not_started_color        :string(255)
#  in_definition_label      :string(255)
#  in_definition_color      :string(255)
#  in_redefinition_label    :string(255)
#  in_redefinition_color    :string(255)
#  log_label                :string(255)
#  mailer_name              :string(255)
#  label_attendant          :string(255)
#  label_subject            :string(255)
#  label_attendant_plural   :string(255)
#  label_subject_plural     :string(255)
#

class TrackingProcess < ActiveRecord::Base
  has_many :tracking_participants, :dependent => :destroy
  has_many :tracking_forms, :dependent => :destroy
  has_many :tracking_process_chars, :dependent => :destroy
  has_many :characteristics, :through => :tracking_process_chars
  accepts_nested_attributes_for :tracking_process_chars, :allow_destroy => true
  attr_accessible :active, :description, :from_date, :name, :to_date, :label_due_date, :label_comment_due_date, :ask_for_comment_due_date, :accepts_redefinitions,
                  :not_started_label, :not_started_color, :in_definition_label, :in_definition_color, :in_redefinition_label, :in_redefinition_color,
                  :log_label, :mailer_name,
                  :label_attendant, :label_subject,
                  :label_attendant_plural, :label_subject_plural,
                  :tracking_process_chars_attributes

  CALCULATED_STATUS_NOT_STARTED = 'not_started'
  CALCULATED_STATUS_IN_DEFINITION = 'in_definition'
  CALCULATED_STATUS_IN_REDEFINITION = 'in_redefinition'

  validates :name, :presence => true
  validates :from_date, :presence => true
  validates :to_date, :presence => true
  validates :label_attendant, :presence => true
  validates :label_subject, :presence => true
  validates :label_attendant_plural, :presence => true
  validates :label_subject_plural, :presence => true


  #def mailer_name
   # 'Desarrollo Potencial Security'
  #end

  def self.current_processes(user_id)
    self.joins(:tracking_participants => :tracking_form_instances).where(:active => true).where('tracking_participants.attendant_user_id = ? OR tracking_participants.subject_user_id = ?', user_id, user_id).uniq
  end

  def self.current_processes_manager()
    self.where(:active => true)
  end

end
