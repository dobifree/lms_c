# == Schema Information
#
# Table name: ct_module_managers
#
#  id           :integer          not null, primary key
#  ct_module_id :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class CtModuleManager < ActiveRecord::Base

  belongs_to :ct_module
  belongs_to :user

  attr_accessible :ct_module_id, :user_id

  validates :user_id, presence: true, uniqueness: {scope: :ct_module_id}

  # attr_accessible :title, :body

end
