# == Schema Information
#
# Table name: hr_process_assessment_qs
#
#  id                                :integer          not null, primary key
#  hr_process_assessment_e_id        :integer
#  hr_process_evaluation_q_id        :integer
#  hr_process_user_id                :integer
#  resultado_puntos                  :float
#  resultado_porcentaje              :float
#  hr_process_evaluation_a_id        :integer
#  fecha                             :datetime
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  comentario                        :text
#  hr_process_evaluation_manual_q_id :integer
#  logro_indicador                   :float
#  logro_indicador_d                 :string(255)      default("")
#

class HrProcessAssessmentQ < ActiveRecord::Base

  belongs_to :hr_process_assessment_e
  belongs_to :hr_process_evaluation_q
  belongs_to :hr_process_evaluation_manual_q
  belongs_to :hr_process_user
  belongs_to :hr_process_evaluation_a



  attr_accessible :fecha, :resultado_porcentaje, :resultado_puntos, :comentario, :logro_indicador, :logro_indicador_d

  before_save :fix_infinity

  def color

    hr_process_assessment_e.hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

      if resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

        return hr_process_dimension_g.color_text

      end

    end

    return ''

  end

  def fix_infinity
    self.resultado_porcentaje = 0 if self.resultado_porcentaje == Float::INFINITY
    self.resultado_puntos = 0 if self.resultado_puntos == Float::INFINITY
  end

end
