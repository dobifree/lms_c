# == Schema Information
#
# Table name: kpi_subsets
#
#  id                      :integer          not null, primary key
#  position                :integer
#  name                    :string(255)
#  kpi_set_id              :integer
#  value                   :float
#  percentage              :float
#  last_update             :datetime
#  last_measurement_update :date
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class KpiSubset < ActiveRecord::Base

  belongs_to :kpi_set

  has_many :kpi_indicators, dependent: :destroy
  has_many :kpi_measurements, dependent: :destroy

  attr_accessible :last_measurement_update, :last_update, :name, :percentage, :position, :value, :kpi_set_id

  validates :name, presence: true
  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('position, name')

  def kpi_dashboard
    self.kpi_set.kpi_dashboard
  end

  def set_last_updates(lms_time)
    self.last_update = lms_time
    measurement = self.kpi_measurements.reorder('measured_at DESC').first
    self.last_measurement_update = measurement.measured_at if measurement
  end

  def get_last_measurement_date

    last_measurement_date = self.kpi_measurements.where('percentage is not null').last

    last_measurement_date = last_measurement_date.measured_at if last_measurement_date

    return last_measurement_date

  end

  def set_percentage

    p = 0
    s = 0

    self.kpi_indicators.each do |kpi_indicator|

      if kpi_indicator.percentage

        p += kpi_indicator.weight
        s += kpi_indicator.percentage*kpi_indicator.weight

      end

    end

    if p > 0
      self.percentage = s/p
    else
      self.percentage = nil
    end

  end

end
