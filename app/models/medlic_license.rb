# == Schema Information
#
# Table name: medlic_licenses
#
#  id                         :integer          not null, primary key
#  user_id                    :integer
#  medlic_process_id          :integer
#  license_number             :string(255)
#  continuous                 :boolean
#  days                       :integer
#  date_begin                 :date
#  date_end                   :date
#  absence_type               :string(255)
#  specialty                  :string(255)
#  partial                    :boolean
#  user_status                :string(255)
#  registered_at              :datetime
#  registered_by_user_id      :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  cc                         :string(255)
#  cc_description             :string(255)
#  company                    :string(255)
#  sent_to_payroll            :boolean          default(FALSE)
#  sent_to_payroll_at         :datetime
#  sent_to_payroll_by_user_id :integer
#

# == Schema Information
#
# Table name: medlic_licenses
#
#  id                    :integer          not null, primary key
#  user_id               :integer
#  medlic_process_id     :integer
#  license_number        :string(255)
#  continuous            :boolean
#  days                  :integer
#  date_begin            :date
#  date_end              :date
#  absence_type          :string(255)
#  specialty             :string(255)
#  partial               :boolean
#  user_status           :string(255)
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  cc                    :string(255)
#  cc_description        :string(255)
#  company               :string(255)
#
class MedlicLicense < ActiveRecord::Base
  belongs_to :user
  belongs_to :medlic_process

  validate :verify_pk

  def self.order_by_begin; joins(:user).order('date_begin ASC, users.codigo ASC') end
  def self.order_by_begin_desc; joins(:user).order('date_begin DESC, users.codigo ASC') end
  def self.by_user(user_id); where(user_id: user_id) end
  def self.by_process(process_id); where(medlic_process_id: process_id) end

  def self.license_for(date_to_e, user_id)
    return [] unless date_to_e && user_id
    date_to_e = date_to_e.to_date
    licenses = MedlicLicense.by_user(user_id)
    return [] unless licenses
    licenses_to_r = []
    licenses.each do |license|
      next unless license.date_begin <= date_to_e && license.date_end >= date_to_e
      licenses_to_r << license
    end
    licenses_to_r
  end

  def self.be_license(begin_date, end_date, user_id)
    return [] unless begin_date && end_date && user_id
    licenses_to_r = []
    pivot = begin_date.to_date
    loop do
      this_licenses = MedlicLicense.license_for(pivot, user_id)
      this_licenses.each { |lic| licenses_to_r << lic unless licenses_to_r.include?(lic) }
      pivot += 1.day
      break if pivot > end_date.to_date
    end
    licenses_to_r
  end

  private

  def verify_pk
    return unless date_begin && user_id
    found = MedlicLicense.where(date_begin: date_begin, user_id: user_id).first
    return unless found
    return if found.id == id
    errors.add(:date_begin, ' ya tiene un registro para usuario especificado')
  end

end
