# == Schema Information
#
# Table name: bp_warning_exceptions
#
#  id                      :integer          not null, primary key
#  bp_groups_user_id       :integer
#  max_usages              :integer
#  active_since            :datetime
#  active_until            :datetime
#  available               :boolean          default(TRUE)
#  unavailable_at          :datetime
#  unavailable_by_user_id  :integer
#  unavailable_description :text
#  registered_at           :datetime
#  registered_by_user_id   :integer
#  registered_description  :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class BpWarningException < ActiveRecord::Base
  belongs_to :bp_groups_user
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :unavailable_by_user, class_name: 'User', foreign_key: :unavailable_by_user_id
  has_many :vac_requests

  attr_accessible :bp_groups_user_id,
                  :available,
                  :max_usages,
                  :registered_at,
                  :registered_by_user_id,
                  :registered_description,
                  :active_since,
                  :active_until,
                  :unavailable_at,
                  :unavailable_by_user_id,
                  :unavailable_description

  validate :group_user_id_presence
  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true
  validates :active_since, presence: true
  validate :deletable?
  validate :creatable_only_one


  def status_message(this_date)
    return 'Disponible' if usable?(this_date)
    'Usado'
  end

  def status_colour(this_date)
    return 2 if usable?(this_date)
    5
  end

  def self.active_group_for_user_at(user_id, this_date)
    group_user = BpGroupsUser.active?(this_date, user_id)
    return unless group_user
    where(bp_groups_user_id: group_user.id)
  end

  def group_user_active?(user_id,this_date)
    group_user = BpGroupsUser.active?(this_date, user_id)
    return false unless group_user
    true
  end

  def self.active_for_at(user_id, this_date)
    evaluate_active = BpWarningException.available_val(true).active_group_for_user_at(user_id, this_date)
    return unless evaluate_active
    evaluate_active.eachh do |eval_active|
      next unless eval_active.active_at(this_date)
      evaluate_active
    end
    return
  end

  def active_at(this_date)
    return nil unless this_date
    this_date = this_date.to_datetime
    return true if !active_until && active_since <= this_date
    return true if active_until && active_since <= this_date && this_date <= active_until
    false
  end

  def self.available_val(value); where(available: value) end
  def self.by_group_user(value); where(bp_groups_user_id:  value) end
  def self.order_by_registered; order('active_since DESC') end
  def self.by_user(value); joins(:bp_groups_user).where(bp_groups_users: { user_id: value}) end


  def usable?(this_date)
    return false unless available
    return false unless active_at(this_date)
    return false unless bp_groups_user.active_at?(this_date)
    return true unless max_usages
    max_usages > usages.size
  end

  def usages
    VacRequest.using_this_exception(self.id).active.by_user(bp_groups_user.user_id)
  end

  def self.user_usages(user_id)
    exception_ids = VacRequest.active.by_user(user_id).with_warning.pluck(:bp_warning_exception_id)
    return [] if exception_ids.size < 1
    BpWarningException.where(id: with_warning)
  end

  def self.first_usable_for_at(user_id, this_date)
    exceptions = BpWarningException.all_usables_for_at(user_id, this_date)
    BpWarningException.max_priority(exceptions)
  end

  def self.all_usables_for_at(user_id, this_date)
    exceptions = []
    all_available_exceptions = BpWarningException.available_val(true).by_user(user_id).all
    all_available_exceptions.each { |exception| exceptions << exception if exception.usable?(this_date) }
    exceptions
  end

  def self.max_priority(these)
    #Priodridades:
    # Los que tienen until y max usages (until más cercano y le quedan menos usos más prioritario  )
    # Los que tienen until y no tienen max usages (until más cercano más prioritario)
    # Los que no tienen until (ID menor más prioritario)
    return nil unless these
    these.sort_by(&:id).first
  end

  def self.is_there_at_least_one_to_go(user_id)
    exceptions = BpWarningException.available_val(true).by_user(user_id)
    at_least_one_to_go = false
    exceptions.each { |exception|  at_least_one_to_go = true if exception.usages.size.zero? }
    at_least_one_to_go
  end

  private

  def creatable_only_one
    return unless available
    return unless BpWarningException.is_there_at_least_one_to_go(bp_groups_user.user_id)
    errors.add(:base, 'Hay una excepción sin usar')
  end

  def deletable?
    return if available
    return unless usages.size > 0
    errors.add(:base, 'Ya ha sido usado')
  end

  def group_user_id_presence
    return if bp_groups_user_id
    errors.add(:bp_groups_user_id, 'no existe o no se encuentra activo' )
  end
end
