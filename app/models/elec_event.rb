# == Schema Information
#
# Table name: elec_events
#
#  id                 :integer          not null, primary key
#  elec_process_id    :integer
#  elec_recurrence_id :integer
#  manual             :boolean          default(TRUE)
#  active             :boolean          default(TRUE)
#  from_date          :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :text
#

class ElecEvent < ActiveRecord::Base
  belongs_to :elec_process
  belongs_to :elec_recurrence
  has_many :elec_rounds
  attr_accessible :active, :from_date, :manual, :name,
                  :elec_process_id, :elec_recurrence_id

  validates :from_date, :presence => true
  validates :name, :presence => true

  default_scope order(:from_date, :created_at)

end
