# == Schema Information
#
# Table name: log_mailers
#
#  id            :integer          not null, primary key
#  module        :string(255)
#  step          :string(255)
#  description   :text
#  registered_at :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class LogMailer < ActiveRecord::Base
  attr_accessible :description, :module, :registered_at, :step
end
