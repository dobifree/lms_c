# == Schema Information
#
# Table name: program_instances
#
#  id                       :integer          not null, primary key
#  name                     :string(255)
#  program_id               :integer
#  from_date                :date
#  to_date                  :date
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  minimum_grade            :float
#  percentage_minimum_grade :boolean
#  active                   :boolean          default(TRUE)
#  master_poll_id           :integer
#  num_passed_courses       :integer
#  sk_client                :string(255)
#  sk_rut_client            :string(255)
#

class ProgramInstance < ActiveRecord::Base

  belongs_to :program
  belongs_to :master_poll

  has_many :program_course_instances

  has_many :enrollments

  has_many :course_certificates

  attr_accessible :from_date, :name, :to_date, :program_id,
                  :percentage_minimum_grade, :minimum_grade, :active,
                  :master_poll_id, :num_passed_courses, :sk_client, :sk_rut_client

  validates :program_id, presence: true
  validates :name, presence: true
  validates :from_date, presence: true
  validates :to_date, presence: true

  validates :minimum_grade, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  validates :num_passed_courses, presence: true, numericality: {greater_than_or_equal_to: 0 }

  default_scope order('active DESC, from_date DESC, to_date DESC, name ASC')

  def enrollments_ordered_by_name

    self.enrollments.joins(:user).reorder('apellidos, nombre')

  end

end
