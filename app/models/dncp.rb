# == Schema Information
#
# Table name: dncps
#
#  id                               :integer          not null, primary key
#  planning_process_id              :integer
#  planning_process_company_unit_id :integer
#  company_unit_area_id             :integer
#  oportunidad_mejora               :text
#  tema_capacitacion                :string(255)
#  objetivos_curso                  :text
#  costo_directo                    :float
#  costo_indirecto                  :float
#  training_mode_id                 :integer
#  training_type_id                 :integer
#  meses                            :string(255)
#  tiempo_capacitacion              :integer
#  training_provider_id             :integer
#  impacto_total                    :float
#  dnc_id                           :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  nota_minima                      :integer
#  eval_presencial                  :boolean          default(TRUE)
#  poll_presencial                  :boolean          default(TRUE)
#  asistencia_presencial            :boolean          default(TRUE)
#  course_id                        :integer
#  observaciones                    :text
#  presup_ampliado                  :boolean
#  number_students                  :integer
#  eval_eficacia                    :boolean
#  training_program_id              :integer
#

class Dncp < ActiveRecord::Base

  belongs_to :planning_process
  belongs_to :planning_process_company_unit
  belongs_to :company_unit_area
  belongs_to :training_mode
  belongs_to :training_type
  belongs_to :training_provider
  belongs_to :training_program
  belongs_to :dnc
  belongs_to :course
  belongs_to :course_shape, class_name: 'Course', foreign_key: 'course_id'

  has_many :dnc_planning_process_goal_ps
  has_many :dnc_training_impact_ps
  has_many :dnc_student_ps

  has_one :course
  has_many :user_courses


  attr_accessible :course_id, :dnc_id, :costo_directo, :costo_indirecto, :eval_presencial, :poll_presencial, :asistencia_presencial, :impacto_total,
                  :meses, :nota_minima, :objetivos_curso, :oportunidad_mejora,
                  :tema_capacitacion, :tiempo_capacitacion,
                  :planning_process_id, :planning_process_company_unit_id, :company_unit_area_id,
                  :training_mode_id, :training_type_id, :training_provider_id, :observaciones, :presup_ampliado, :number_students,
                  :eval_eficacia, :training_program_id

  validates :planning_process_id, presence: true
  validates :planning_process_company_unit_id, presence: true
  validates :company_unit_area_id, presence: true
  validates :oportunidad_mejora, presence: true
  validates :tema_capacitacion, presence: true
  validates :objetivos_curso, presence: true

  validates :costo_directo, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :costo_indirecto, presence: true, numericality: { greater_than_or_equal_to: 0 }

  validates :training_mode_id, presence: true
  validates :training_type_id, presence: true

  validates :tiempo_capacitacion, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 30 }

  #validates :training_provider_id, presence: true

  validates :nota_minima, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  default_scope order('tema_capacitacion')

  def group_ids

    self.user_courses.group('grupo')

  end

end
