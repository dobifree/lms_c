# == Schema Information
#
# Table name: pe_question_comments
#
#  id               :integer          not null, primary key
#  pe_question_id   :integer
#  pe_member_rel_id :integer
#  comment          :text
#  registered_at    :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PeQuestionComment < ActiveRecord::Base

  belongs_to :pe_question
  belongs_to :pe_member_rel
  attr_accessible :comment, :registered_at

  validates :comment, presence: true

  default_scope order('registered_at')

end
