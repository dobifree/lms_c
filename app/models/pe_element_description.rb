# == Schema Information
#
# Table name: pe_element_descriptions
#
#  id            :integer          not null, primary key
#  position      :integer
#  name          :string(255)
#  pe_element_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class PeElementDescription < ActiveRecord::Base

  belongs_to :pe_element

  has_many :pe_question_descriptions

  attr_accessible :name, :position, :pe_element_id

  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_element_id }

  default_scope order('position ASC, name ASC')

end
