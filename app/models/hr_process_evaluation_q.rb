# == Schema Information
#
# Table name: hr_process_evaluation_qs
#
#  id                            :integer          not null, primary key
#  texto                         :text
#  peso                          :integer
#  hr_process_evaluation_id      :integer
#  hr_process_level_id           :integer
#  hr_evaluation_type_element_id :integer
#  hr_process_evaluation_q_id    :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  comentario                    :boolean          default(FALSE)
#  discrete_display_in_reports   :boolean          default(FALSE)
#

class HrProcessEvaluationQ < ActiveRecord::Base

  belongs_to :hr_process_evaluation
  belongs_to :hr_process_level
  belongs_to :hr_evaluation_type_element
  belongs_to :hr_process_evaluation_q

  has_many :hr_process_evaluation_qs
  has_many :hr_process_evaluation_as, dependent: :destroy

  has_many :hr_process_assessment_qs

  attr_accessible :hr_process_evaluation_q_id, :peso, :texto, :hr_process_level_id, :comentario,
                  :discrete_display_in_reports

  validates :texto, presence: true, uniqueness:  {case_sensitive: false, scope: [:hr_process_evaluation_id, :hr_process_evaluation_q_id, :hr_process_level_id]}
  validates :peso, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: 0 }
  validates :hr_evaluation_type_element_id, presence: true

end
