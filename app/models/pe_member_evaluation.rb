# == Schema Information
#
# Table name: pe_member_evaluations
#
#  id               :integer          not null, primary key
#  pe_member_id     :integer
#  pe_evaluation_id :integer
#  pe_process_id    :integer
#  weight           :float
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PeMemberEvaluation < ActiveRecord::Base

  belongs_to :pe_member
  belongs_to :pe_evaluation
  belongs_to :pe_process

  attr_accessible :weight

end
