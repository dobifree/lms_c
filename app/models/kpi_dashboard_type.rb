# == Schema Information
#
# Table name: kpi_dashboard_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  position   :integer
#

class KpiDashboardType < ActiveRecord::Base

  has_many :kpi_dashboards

  attr_accessible :name, :position

  validates :name, presence: true, uniqueness: {case_sensitive: false}
  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('position ASC, name ASC')

end
