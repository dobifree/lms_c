# == Schema Information
#
# Table name: sc_reported_records
#
#  id              :integer          not null, primary key
#  sc_reporting_id :integer
#  sc_record_id    :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class ScReportedRecord < ActiveRecord::Base
  belongs_to :sc_reporting
  belongs_to :sc_record
  attr_accessible :sc_reporting_id, :sc_record_id
end
