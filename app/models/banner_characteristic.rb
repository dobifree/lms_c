# == Schema Information
#
# Table name: banner_characteristics
#
#  id                :integer          not null, primary key
#  banner_id         :integer
#  characteristic_id :integer
#  match_value       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class BannerCharacteristic < ActiveRecord::Base
  belongs_to :banner
  belongs_to :characteristic
  attr_accessible :match_value, :banner, :banner_id, :characteristic, :characteristic_id
end
