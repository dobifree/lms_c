# == Schema Information
#
# Table name: ben_cellphone_processes
#
#  id                                                 :integer          not null, primary key
#  month                                              :integer
#  year                                               :integer
#  charged_to_payroll                                 :boolean          default(FALSE)
#  charged_to_payroll_at                              :datetime
#  charged_to_payroll_by_user_id                      :integer
#  created_at                                         :datetime         not null
#  updated_at                                         :datetime         not null
#  generated_registered_not_manually_excel_at         :datetime
#  generated_registered_manually_excel_at             :datetime
#  generated_registered_not_manually_excel_by_user_id :integer
#  generated_registered_manually_excel_by_user_id     :integer
#

class BenCellphoneProcess < ActiveRecord::Base

  has_many :ben_cellphone_bill_items
  has_many :ben_user_cellphone_processes
  belongs_to :charged_to_payroll_by_user, class_name: 'User', foreign_key: :charged_to_payroll_by_user_id
  belongs_to :generated_registered_manually_excel_by_user, class_name: 'User', foreign_key: :generated_registered_manually_excel_by_user_id
  belongs_to :generated_registered_not_manually_excel_by_user, class_name: 'User', foreign_key: :generated_registered_not_manually_excel_by_user_id

  attr_accessible  :month, :year,
                   :generated_registered_not_manually_excel_at,
                   :generated_registered_manually_excel_at,
                   :generated_registered_not_manually_excel_by_user_id,
                   :generated_registered_manually_excel_by_user_id

  validates :month, presence: true, :uniqueness => {:scope => :year}

  def charged_to_payroll_at_formatted
    self[:charged_to_payroll_at].strftime('%F')
  end

  def charged_to_payroll_at_long_formatted
    self[:charged_to_payroll_at].strftime('%H:%M')
  end

  def charged_by_user_information
    return self.charged_to_payroll_by_user.codigo+' - '+self.charged_to_payroll_by_user.apellidos+', '+self.charged_to_payroll_by_user.nombre
  end

  # def period_process
  #   month = t('date.month_names')
  #   return t('date.month_names')[self.month].to_s+' '+self.year.to_s
  # end

end
