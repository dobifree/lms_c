# == Schema Information
#
# Table name: hr_process_evaluation_manual_qs
#
#  id                                        :integer          not null, primary key
#  texto                                     :string(255)
#  peso                                      :float
#  hr_process_evaluation_id                  :integer
#  hr_evaluation_type_element_id             :integer
#  hr_process_evaluation_manual_q_id         :integer
#  hr_process_user_id                        :integer
#  created_at                                :datetime         not null
#  updated_at                                :datetime         not null
#  hr_process_evaluation_q_classification_id :integer
#  descripcion_indicador                     :string(255)
#  meta_indicador                            :float
#  unidad_medida_indicador                   :string(255)
#  confirmado                                :boolean
#  tipo_indicador                            :integer          default(1)
#  meta_indicador_d                          :string(255)      default("")
#

class HrProcessEvaluationManualQ < ActiveRecord::Base

  belongs_to :hr_process_evaluation
  belongs_to :hr_evaluation_type_element
  belongs_to :hr_process_user
  belongs_to :hr_process_evaluation_manual_q
  belongs_to :hr_process_evaluation_q_classification

  has_many :hr_process_evaluation_manual_qs

  has_many :hr_process_assessment_qs

  has_many :hr_process_evaluation_manual_qcs

  has_many :hr_process_assessment_qs

  has_many :pe_question_ranks

  attr_accessible :peso, :texto, :hr_process_evaluation_id, :hr_evaluation_type_element_id, :hr_process_evaluation_manual_q_id,
                  :hr_process_user_id, :hr_process_evaluation_q_classification_id,
                  :descripcion_indicador, :meta_indicador, :unidad_medida_indicador, :tipo_indicador, :confirmado, :meta_indicador_d

  validates :texto, presence: true, uniqueness:  {case_sensitive: false, scope: [:hr_process_evaluation_id, :hr_process_evaluation_manual_q_id, :hr_process_user_id]}
  validates :peso, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  validates :hr_evaluation_type_element_id, presence: true
  validates :meta_indicador, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true

  validate :validate_weight
  validate :validate_meta
  validate :validate_unidad_medida_indicador

  def set_goal_rank
    if self.tipo_indicador == 4
      pe_question_rank = self.pe_question_ranks.where('percentage = 100').first
      if pe_question_rank
        self.meta_indicador = pe_question_rank.goal
      end
    end

    if self.tipo_indicador == 5
      pe_question_rank = self.pe_question_ranks.where('percentage = 100').first
      if pe_question_rank
        self.meta_indicador_d = pe_question_rank.discrete_goal
        self.meta_indicador = 100
      end
    end

  end


  private

    def validate_weight
      if self.hr_evaluation_type_element.aplica_evaluacion
        if self.peso.blank?
          errors.add(:peso, 'no puede ser vacío' )
        elsif (self.hr_evaluation_type_element.peso_minimo && self.peso < self.hr_evaluation_type_element.peso_minimo) || (self.hr_evaluation_type_element.peso_maximo && self.peso > self.hr_evaluation_type_element.peso_maximo)
          errors.add(:peso, 'no puede ser menor que '+self.hr_evaluation_type_element.peso_minimo.to_s+' o mayor que '+self.hr_evaluation_type_element.peso_maximo.to_s )
        end
      end
    end

    def validate_meta
      if self.hr_evaluation_type_element.aplica_evaluacion && self.hr_evaluation_type_element.indicador_logro && self.tipo_indicador != 4 && self.tipo_indicador != 5 && self.meta_indicador.blank?
        errors.add(:meta_indicador, 'no puede ser vacía')
      end
    end

    def validate_unidad_medida_indicador
      if self.hr_evaluation_type_element.aplica_evaluacion && self.hr_evaluation_type_element.indicador_logro && self.unidad_medida_indicador.blank?
        errors.add(:unidad_medida_indicador, 'no puede ser vacía')
      end
    end

  #@hr_evaluation_type_element.aplica_evaluacion && ( params[:hr_process_evaluation_manual_q][:peso].blank? || (@hr_evaluation_type_element.peso_minimo && params[:hr_process_evaluation_manual_q][:peso].to_f < @hr_evaluation_type_element.peso_minimo) || (@hr_evaluation_type_element.peso_maximo && params[:hr_process_evaluation_manual_q][:peso].to_f > @hr_evaluation_type_element.peso_maximo))

end
