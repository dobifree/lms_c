# == Schema Information
#
# Table name: brg_group_users
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  brg_group_id           :integer
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class BrgGroupUser < ActiveRecord::Base
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :user
  belongs_to :brg_group
  has_many :brg_bonuses
  has_many :brg_group_user_comments

  attr_accessible :user_id,
                  :brg_group_id,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id

  validates :brg_group_id, presence: true
  validates :user_id, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validate :users_uniqueness
  before_save :deactivation_bonus

  default_scope joins(:brg_group, :user).order('group_cod ASC, apellidos ASC, nombre ASC').readonly(false)
  scope :active, -> { where(active: true)}
  scope :group_by_cod, -> (group_cod) { active.joins(:brg_group).where(group_cod: group_cod) }

  def self.by_user(id); where(user_id: id) end
  def self.by_process(process_id); joins(:brg_group).where(brg_groups: { brg_process_id: process_id } ) end

  def active_comments; brg_group_user_comments.active end

  def brg_bonuses
    BrgBonus.group_user(id)
  end

  def show_me_cargo
    return unless user
    j_job = user.j_job
    j_job ? j_job.name : ''
  end

  def show_me_tipo_cargo
    return unless user
    puesto = user.secu_clasif_puest
    return unless puesto
    puesto
  end

  def show_me_fecha_ingreso
    return unless user
    user.from_date_inicio_laboral
  end

  def editable?
    active && brg_group.active
  end

  def active_text
    active ? 'Activo' : 'No activo'
  end

  def full_registered_by_user(show_cod)
    return nil unless registered_by_user
    full_user_gen(registered_by_user, show_cod)
  end

  def full_deactivated_by_user(show_cod)
    return nil unless deactivated_by_user
    full_user_gen(deactivated_by_user, show_cod)
  end

  def full_user(show_cod)
    return nil unless user
    full_user_gen(user, show_cod)
  end

  def self.active_groups
    joins(:brg_group).where(brg_groups: { active: true })
  end

  private

  def deactivation_bonus
    return if active
    return unless deactivated_by_user_id
    return unless deactivated_at
    BrgBonus.group_user(self.id).active.each do |bonus|
      bonus.active = false
      bonus.deactivated_at = deactivated_at
      bonus.deactivated_by_user_id = deactivated_by_user_id
      bonus.save
    end
  end

  def full_user_gen(user, show_cod)
    full_user =  user.apellidos.to_s+', '+user.nombre.to_s
    full_user = user.codigo.to_s+' '+full_user if show_cod
    full_user
  end

  def users_uniqueness
    return unless active
    return unless user_id
    group_user = BrgGroupUser.joins(:brg_group).where(brg_groups: { active: true, brg_process_id: brg_group.brg_process_id }, active: true, user_id: user_id).first
    return unless group_user
    if id
      return unless group_user.id == id
    end
    errors.add(:user_id, 'se encuentra activo en el grupo: '+group_user.brg_group.group_cod+' - '+group_user.brg_group.name)
  end

end
