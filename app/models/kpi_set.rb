# == Schema Information
#
# Table name: kpi_sets
#
#  id                      :integer          not null, primary key
#  position                :integer
#  name                    :string(255)
#  kpi_dashboard_id        :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  value                   :float
#  percentage              :float
#  last_update             :datetime
#  last_measurement_update :date
#



class KpiSet < ActiveRecord::Base

  belongs_to :kpi_dashboard

  has_many :kpi_subsets, dependent: :destroy
  has_many :kpi_indicators, dependent: :destroy
  has_many :kpi_measurements, dependent: :destroy
  has_many :kpi_set_guests, dependent: :destroy
  has_many :kpi_colors, dependent: :destroy


  attr_accessible :name, :position, :kpi_dashboard_id, :value, :percentage, :last_update, :last_measurement_update

  validates :name, presence: true
  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('position, name')

  def set_last_updates(lms_time)
    self.last_update = lms_time
    measurement = self.kpi_measurements.reorder('measured_at DESC').first
    self.last_measurement_update = measurement.measured_at if measurement
  end

  def kpi_set_guests_ordered

    self.kpi_set_guests.joins(:user).order('apellidos, nombre')

  end

  def get_last_measurement_date

    last_measurement_date = self.kpi_measurements.where('percentage is not null').last

    last_measurement_date = last_measurement_date.measured_at if last_measurement_date

    return last_measurement_date

  end

  def kpi_indicators_ordered_by_subset
    self.kpi_indicators.joins(:kpi_subset).reorder('kpi_subsets.position, kpi_subsets.name, kpi_indicators.position, kpi_indicators.name').readonly(false)
  end

  def set_percentage

    p = 0
    s = 0

    self.kpi_indicators.each do |kpi_indicator|

      if kpi_indicator.percentage

        p += kpi_indicator.weight
        s += kpi_indicator.percentage*kpi_indicator.weight

      end

    end

    if p > 0
      self.percentage = s/p
    else
      self.percentage = nil
    end

  end

end
