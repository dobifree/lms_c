# == Schema Information
#
# Table name: tracking_process_chars
#
#  id                  :integer          not null, primary key
#  tracking_process_id :integer
#  characteristic_id   :integer
#  position            :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class TrackingProcessChar < ActiveRecord::Base
  belongs_to :tracking_process
  belongs_to :characteristic
  attr_accessible :position, :characteristic_id

  default_scope { order(:position, :characteristic_id) }
end
