# == Schema Information
#
# Table name: hr_process_evaluation_as
#
#  id                         :integer          not null, primary key
#  valor                      :integer
#  texto                      :text
#  hr_process_evaluation_q_id :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class HrProcessEvaluationA < ActiveRecord::Base

  belongs_to :hr_process_evaluation_q

  attr_accessible :texto, :valor

  validates :texto, presence: true
  validates :valor, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: -1 }


end
