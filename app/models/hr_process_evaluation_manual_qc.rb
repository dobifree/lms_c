# == Schema Information
#
# Table name: hr_process_evaluation_manual_qcs
#
#  id                                :integer          not null, primary key
#  comentario                        :text
#  fecha                             :datetime
#  hr_process_evaluation_manual_q_id :integer
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  hr_process_user_id                :integer
#

class HrProcessEvaluationManualQc < ActiveRecord::Base

  belongs_to :hr_process_evaluation_manual_q
  belongs_to :hr_process_user

  attr_accessible :comentario, :fecha
end
