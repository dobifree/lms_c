# == Schema Information
#
# Table name: kpi_indicator_details
#
#  id               :integer          not null, primary key
#  kpi_indicator_id :integer
#  measured_at      :date
#  goal             :float
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class KpiIndicatorDetail < ActiveRecord::Base

  belongs_to :kpi_indicator

  has_many :kpi_indicator_ranks, dependent: :destroy

  attr_accessible :goal, :measured_at, :kpi_indicator_id

  validates :goal, numericality: {greater_than_or_equal_to: 0 }, allow_nil: true

  default_scope order('measured_at')

  def set_goal_rank
    if self.kpi_indicator.indicator_type == 4
      kpi_indicator_rank = self.kpi_indicator_ranks.where('percentage = 100').first
      if kpi_indicator_rank
        self.goal = kpi_indicator_rank.goal
      else
        self.goal = nil
      end
    end
  end

end
