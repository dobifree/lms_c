# == Schema Information
#
# Table name: sel_applicants
#
#  id                  :integer          not null, primary key
#  sel_process_id      :integer
#  jm_candidate_id     :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  selected            :boolean          default(FALSE)
#  selected_at         :datetime
#  selected_by         :integer
#  hired               :boolean          default(FALSE)
#  hired_at            :datetime
#  hired_by            :integer
#  priority            :integer
#  comment             :text
#  comment_by_user_id  :integer
#  comment_at          :datetime
#  excluded            :boolean          default(FALSE)
#  excluded_at         :datetime
#  excluded_by_user_id :integer
#

class SelApplicant < ActiveRecord::Base
  belongs_to :sel_process
  belongs_to :jm_candidate
  belongs_to :comment_by_user, class_name: 'User'
  has_many :sel_applicant_attachments, :dependent => :destroy
  has_many :sel_applicant_records, :dependent => :destroy
  has_many :sel_applicant_specific_answers, :dependent => :destroy
  has_many :jm_candidate_answers, :through => :sel_applicant_records
  accepts_nested_attributes_for :sel_applicant_specific_answers, :reject_if => lambda { |a| a[:answer].blank? }, :allow_destroy => false


  attr_accessible :priority, :comment, :comment_at, :comment_by_user_id,
                  :sel_process, :sel_process_id,
                  :jm_candidate, :jm_candidate_id,
                  :selected, :selected_at, :selected_by, :hired, :hired_at, :hired_by,
                  :sel_applicant_attachments,
                  :sel_applicant_specific_answers_attributes

  validates :sel_process_id, :presence => true
  validates :jm_candidate_id, :presence => true
  validates :sel_process_id, uniqueness: {scope: :jm_candidate_id}
end
