# == Schema Information
#
# Table name: j_cost_centers
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  budget          :decimal(60, 4)
#  active          :boolean          default(TRUE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  cc_id           :string(255)
#  total_positions :integer          default(1)
#

class JCostCenter < ActiveRecord::Base

  has_many :j_jobs
  has_many :jcc_characteristics

  has_many :j_job_ccs
  has_many :j_jobs, :through => :j_job_ccs

  has_many :users

  attr_accessible :active, :budget, :name, :cc_id, :total_positions

  validates :name, presence: true, uniqueness: { case_sensitive: false}
  validates :cc_id, presence: true, uniqueness: { case_sensitive: false}
  validates :budget, presence: true, numericality: {greater_than_or_equal_to: 0 }

  validates :total_positions, numericality: {only_integer: true, greater_than_or_equal_to: 1 }

  default_scope order('active DESC, name')

  def self.actives
    self.where('active = ?', true)
  end

  def jcc_characteristic(characteristic)
    self.jcc_characteristics.where('characteristic_id = ?', characteristic.id).first
  end

  def jcc_characteristic_value(characteristic)
    jjc = self.jcc_characteristic characteristic
    jjc.value if jjc
  end

  def formatted_jcc_characteristic_value(characteristic)
    jjc = self.jcc_characteristic characteristic
    jjc.formatted_value if jjc
  end

end
