# == Schema Information
#
# Table name: dnc_requirements
#
#  id                    :integer          not null, primary key
#  user_id               :integer
#  dnc_process_id        :integer
#  dnc_course_id         :integer
#  dnc_process_period_id :integer
#  dnc_provider_id       :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  duration              :integer
#

class DncRequirement < ActiveRecord::Base

  belongs_to :user
  belongs_to :dnc_process
  belongs_to :dnc_course
  belongs_to :dnc_process_period
  belongs_to :dnc_provider

  # attr_accessible :title, :body

  default_scope joins(:dnc_process_period).joins(:dnc_course).order('dnc_process_periods.from_date, dnc_process_periods.name, dnc_courses.name')

end
