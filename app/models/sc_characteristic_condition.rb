# == Schema Information
#
# Table name: sc_characteristic_conditions
#
#  id                  :integer          not null, primary key
#  characteristic_id   :integer
#  condition           :integer
#  match_value_string  :string(255)
#  match_value_int     :integer
#  match_value_float   :float
#  match_value_text    :text
#  match_value_date    :date
#  match_value_char_id :integer
#  sc_schedule_id      :integer
#  registered_at       :datetime
#  active              :boolean          default(FALSE)
#  deactivated_at      :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class ScCharacteristicCondition < ActiveRecord::Base
  belongs_to :characteristic
  belongs_to :sc_schedule

  attr_accessible :characteristic_id,
                  :condition,
                  :match_value_string,
                  :match_value_text,
                  :match_value_date,
                  :match_value_int,
                  :match_value_float,
                  :match_value_char_id,
                  :sc_schedule_id,
                  :active

  def conditions
    string_conditions = [['igual a',0], ['diferente a',1]]
    numbers_conditions = [['mayor a',0], ['mayor igual a',1], ['igual a',2], ['menor igual a',3], ['menor a',4], ['diferente de',5]]
    list_conditions = [['igual a',0], ['diferente a',1]]
    date_conditions = [['antes de',0], ['igual a',1], ['después de',2], ['diferente a',3]]

    case self.characteristic.data_type_id
      when 0
        return string_conditions
      when 1
        return string_conditions
      when 2
        return date_conditions
      when 3
        return numbers_conditions
      when 4
        return numbers_conditions
      when 5
        return list_conditions
    end
  end

  def condition_name
    conditions = ['-','Igual a']
    return conditions[self.condition] if self.condition
  end

  def get_value
    case self.characteristic.data_type_id
      when 0
        self.match_value_string
      when 1
        self.match_value_text
      when 2
        self.match_value_date
      when 3
        self.match_value_int
      when 4
        self.match_value_float
      when 5
        self.characteristic.characteristic_values.each do |val|
          return val.id if val.position == self.match_value_char_id
        end
      else
        return false
    end
  end

  def value
    if self.characteristic.data_type_id == 0
      self.match_value_string
    elsif self.characteristic.data_type_id == 1
      self.match_value_text
    elsif self.characteristic.data_type_id == 2
      self.match_value_date
    elsif self.characteristic.data_type_id == 3
      self.match_value_int
    elsif self.characteristic.data_type_id == 4
      self.match_value_float
    elsif self.characteristic.data_type_id == 5
      self.characteristic.characteristic_values.each do |val|
        return val.id if val.id == self.match_value_char_id
      end
    end
  end



  def list_value
    if self.characteristic
      self.characteristic.characteristic_values.each do |val|
        return val.value_string if val.id == self.match_value_char_id
      end
    end
    return nil
  end

  def different?(edited_condition)
    if self.characteristic_id == edited_condition.characteristic_id and
        self.condition == edited_condition.condition and
        self.sc_schedule_id == edited_condition.sc_schedule_id and
        self.match_value_string == edited_condition.match_value_string and
        self.match_value_text == edited_condition.match_value_text and
        self.match_value_date == edited_condition.match_value_date and
        self.match_value_int == edited_condition.match_value_int and
        self.match_value_float == edited_condition.match_value_float and
        self.match_value_char_id == edited_condition.match_value_char_id and
      return false
    else
      return true
    end
  end
  #
  # def values_list
  #   puts 'VVV'
  #   values = []
  #   if self.characteristic
  #     values = CharacteristicValue.where(:characteristic_id =>  self-characteristic_id).reorder(:value_string).map{|char_value| [char_value.value_string, char_value.id]}
  #     puts 'BBBB'
  #     puts values.inspect
  #   end
  #   return values
  # end



end
