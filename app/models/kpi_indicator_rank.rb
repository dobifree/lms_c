# == Schema Information
#
# Table name: kpi_indicator_ranks
#
#  id                      :integer          not null, primary key
#  kpi_indicator_id        :integer
#  percentage              :float
#  goal                    :float
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  kpi_indicator_detail_id :integer
#

class KpiIndicatorRank < ActiveRecord::Base

  belongs_to :kpi_indicator
  belongs_to :kpi_indicator_detail

  attr_accessible :goal, :percentage

  validates :goal, presence: true, numericality: {greater_than_or_equal_to: 0 }
  validates :percentage, presence: true, numericality: {greater_than_or_equal_to: 0 }

  default_scope order('percentage, goal')

end
