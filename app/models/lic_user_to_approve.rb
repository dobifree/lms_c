# == Schema Information
#
# Table name: lic_user_to_approves
#
#  id                     :integer          not null, primary key
#  lic_approver_id        :integer
#  user_id                :integer
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class LicUserToApprove < ActiveRecord::Base
  belongs_to :lic_approver
  belongs_to :user
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :lic_approver_id,
                  :user_id,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id

  validates :lic_approver_id, presence: true
  validates :user_id, presence: true, uniqueness: { scope: :lic_approver_id }
  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true
end
