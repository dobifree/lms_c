# == Schema Information
#
# Table name: kpi_dashboard_guests
#
#  id               :integer          not null, primary key
#  kpi_dashboard_id :integer
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class KpiDashboardGuest < ActiveRecord::Base

  belongs_to :kpi_dashboard
  belongs_to :user

  # attr_accessible :title, :body

  validates :user_id, uniqueness: { scope: :kpi_dashboard_id }

end
