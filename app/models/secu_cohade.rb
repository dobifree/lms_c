# == Schema Information
#
# Table name: secu_cohades
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  description      :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  available_to_brc :boolean          default(FALSE)
#

class SecuCohade < ActiveRecord::Base

  has_many :brc_bonus_items
  has_many :brc_processes

  attr_accessible :description, :name, :available_to_brc

  validates :name, presence: true, uniqueness: true

  default_scope order('name ASC')

  def self.cohades_brc_for_select
    SecuCohade.where('available_to_brc = ?', true).map {|s| [s.name, s.id]}
  end


end
