# == Schema Information
#
# Table name: program_course_managers
#
#  id                :integer          not null, primary key
#  program_course_id :integer
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class ProgramCourseManager < ActiveRecord::Base

  belongs_to :program_course
  belongs_to :user

  attr_accessible :user_id

  validates :user_id, uniqueness: { scope: :program_course_id }

end
