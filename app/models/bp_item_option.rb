# == Schema Information
#
# Table name: bp_item_options
#
#  id                      :integer          not null, primary key
#  bp_item_id              :integer
#  name                    :string(255)
#  description             :text
#  position                :integer
#  active                  :boolean          default(TRUE)
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#  prev_item_option_id     :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class BpItemOption < ActiveRecord::Base
  belongs_to :bp_item
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id
  belongs_to :prev_item_option, class_name: 'BpItemOption', foreign_key: :prev_item_option_id

  attr_accessible :bp_item_id,
                  :name,
                  :description,
                  :position,
                  :active,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id,
                  :prev_item_option_id

  validates :name, presence: true
  validates :position, presence: true
  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_admin_id, presence: true, unless: :active?
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

end
