# == Schema Information
#
# Table name: ben_custom_report_characteristics
#
#  id                              :integer          not null, primary key
#  ben_custom_report_id            :integer
#  ben_cellphone_characteristic_id :integer
#  position                        :integer
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  relation_type                   :integer
#  match_value_string              :string(255)
#  match_value_text                :text
#  match_value_date                :date
#  match_value_int                 :integer
#  match_value_float               :float
#  match_value_char_id             :integer
#

class BenCustomReportCharacteristic < ActiveRecord::Base
  belongs_to :ben_custom_report
  belongs_to :ben_cellphone_characteristic

  attr_accessible :position,
                  :ben_custom_report_id,
                  :ben_cellphone_characteristic_id,
                  :relation_type,
                  :match_value_string,
                  :match_value_text,
                  :match_value_date,
                  :match_value_int,
                  :match_value_float,
                  :match_value_char_id

  validate :match_value_presence

  def relation_type_name
    return nil unless relation_type
    names = ['Mostrar en reporte', 'Filtro: igual a', 'Filtro: según usuario']
    names[relation_type]
  end

  def self.relation_type_names
    ['mostrar en reporte', 'filtro: igual a', 'filtro: según usuario']
  end

  def value
    return nil unless ben_cellphone_characteristic
    data_type_id = characteristic_data_type_id
    if data_type_id.zero?
      match_value_string
    elsif data_type_id == 1
      match_value_text
    elsif data_type_id == 2
      match_value_date
    elsif data_type_id == 3
      match_value_int
    elsif data_type_id == 4
      match_value_float
    elsif data_type_id == 5
      ben_cellphone_characteristic.characteristic.characteristic_values.each do |val|
        return val.id if val.id == match_value_char_id
      end
    elsif data_type_id == 13
      match_value_string
    end
  end

  def value_content
    return nil unless ben_cellphone_characteristic
    data_type_id = characteristic_data_type_id
    if data_type_id.zero?
      match_value_string
    elsif data_type_id == 1
      match_value_text
    elsif data_type_id == 2
      match_value_date
    elsif data_type_id == 3
      match_value_int
    elsif data_type_id == 4
      match_value_float
    elsif data_type_id == 5
      ben_cellphone_characteristic.characteristic.characteristic_values.each do |val|
        return val.value_string if val.id == match_value_char_id
      end
    elsif data_type_id == 13
      match_value_string
    end
  end

  def user_value(user_id)
    ucs = UserCharacteristic.where(user_id: user_id, characteristic_id: characteristic_id)
    ucs.any? ? ucs.first.value : nil
  end

  def match_user?(user_id)
    user_value = user_value(user_id)
    return nil unless user_value
    return true if user_value == value
    false
  end

  def characteristic
    return nil unless ben_cellphone_characteristic
    ben_cellphone_characteristic.characteristic
  end

  def characteristic_id
    return nil unless ben_cellphone_characteristic
    ben_cellphone_characteristic.characteristic_id
  end

  def characteristic_name
    return nil unless ben_cellphone_characteristic
    ben_cellphone_characteristic.characteristic.nombre
  end

  def characteristic_data_type_id
    return nil unless ben_cellphone_characteristic
    ben_cellphone_characteristic.characteristic.data_type_id
  end

  def match_users_by_self_value
    # relation_type = 1
    users_id = []
    user_characteristics = UserCharacteristic.where(:characteristic_id => characteristic_id)
    user_characteristics.each do |user_characteristic|
      next unless user_characteristic.value == value && !users_id.include?(user_characteristic.user_id)
      users_id << user_characteristic.user_id
    end
    return users_id
  end

  def match_users_by_user_connected(user_connected_id)
    # relation_type = 2
    users_id = []

    value_to_compare = UserCharacteristic.where(characteristic_id: characteristic_id, user_id: user_connected_id).first
    value_to_compare = value_to_compare.value.to_s

    user_characteristics = UserCharacteristic.where(:characteristic_id => characteristic_id)
    user_characteristics.each do |user_characteristic|
      next unless user_characteristic.value.to_s == value_to_compare && !users_id.include?(user_characteristic.user_id)
      users_id << user_characteristic.user_id
    end
    return users_id
  end

  private

  def match_value_presence
    return unless ben_cellphone_characteristic_id
    return unless relation_type
    return if relation_type != 1
    data_type_id = characteristic_data_type_id
    if data_type_id.zero?
      return unless match_value_string.nil?
      errors.add(:match_value_string, 'no puede ser vacío')
    elsif data_type_id == 1
      return unless match_value_text.nil?
      errors.add(:match_value_text, 'no puede ser vacío')
    elsif data_type_id == 2
      return unless match_value_date.nil?
      errors.add(:match_value_date, 'no puede ser vacío')
    elsif data_type_id == 3
      return unless match_value_int.nil?
      errors.add(:match_value_int, 'no puede ser vacío')
    elsif data_type_id == 4
      return unless match_value_float.nil?
      errors.add(:match_value_float, 'no puede ser vacío')
    elsif data_type_id == 5
      return unless match_value_char_id.nil?
      errors.add(:match_value_char_id, 'no puede ser vacío')
    elsif data_type_id == 13
      return unless match_value_string.nil?
      errors.add(:match_value_string, 'no puede ser vacío')
    end
  end
end
