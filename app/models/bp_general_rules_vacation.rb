# == Schema Information
#
# Table name: bp_general_rules_vacations
#
#  id                        :integer          not null, primary key
#  legal_days                :integer
#  progressive_days_step     :integer
#  progressive_max_days      :integer
#  request_min_days          :integer
#  request_max_days          :integer
#  accumulated_max_days      :integer
#  accumulated_max_real_days :integer
#  deactivated_at            :datetime
#  deactivated_by_admin_id   :integer
#  registered_at             :datetime
#  registered_by_admin_id    :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  bp_group_id               :integer
#  since                     :datetime
#  until                     :datetime
#  only_laborable_day        :boolean          default(TRUE)
#  user_id                   :integer
#  accumulated_type          :integer          default(2)
#  description               :text
#

class BpGeneralRulesVacation < ActiveRecord::Base
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id
  belongs_to :bp_group
  belongs_to :user
  has_many :bp_rules_warnings
  has_many :bp_progressive_days

  attr_accessible :accumulated_max_days,
                  :accumulated_max_real_days,
                  :since,
                  :until,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :legal_days,
                  :progressive_days_step,
                  :progressive_max_days,
                  :registered_at,
                  :registered_by_admin_id,
                  :request_max_days,
                  :request_min_days,
                  :bp_group_id,
                  :user_id,
                  :accumulated_type,
                  :description

  validates :bp_group_id, presence: true, unless: :user_id?
  validates :user_id, presence: true, unless: :bp_group_id?

  validates :legal_days, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0 }
  validates :progressive_days_step, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0 }
  validates :progressive_max_days, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0 }
  validates :request_max_days, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0 }
  validates :request_min_days, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0 }
  validates :accumulated_max_days, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0 }
  validates :accumulated_max_real_days, presence: true, numericality: { only_integer: true, greater_than_or_equal_to:0 }

  validates :since, presence: true
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  validate :days_by_request
  validate :progressive_days_con
  validate :since_before_until
  validate :overlap_range_since
  validate :range_since_concordance_1
  validate :range_since_concordance_2
  validate :range_until_concordance_1

  validate :overlap_range_since_user

  def self.accumulated_types
    [0,1,2,3]
  end

  def self.accumulated_type_names
    ['Solamente legales',
     'Solamente progresivos',
     'Legales + Progresivos',
     'Ninguno']
  end

  def self.accumulated_types_array
    [
        [BpGeneralRulesVacation.accumulated_type_names[0], BpGeneralRulesVacation.accumulated_types[0]],
        [BpGeneralRulesVacation.accumulated_type_names[1], BpGeneralRulesVacation.accumulated_types[1]],
        [BpGeneralRulesVacation.accumulated_type_names[2], BpGeneralRulesVacation.accumulated_types[2]]
    ]
  end

  def accumulative_progressive?
    [1,2].include?(accumulated_type)
  end

  def accumulative_legal?
    [0,2].include?(accumulated_type)
  end

  def active?(now)
    now = now.to_datetime
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  def self.group_active?(now, group_id)
    BpGeneralRulesVacation.where(bp_group_id: group_id).each do |general_rules_vacation|
      return general_rules_vacation if general_rules_vacation.active?(now)
    end
    false
  end

  def self.user_active?(now, user_id)
    BpGeneralRulesVacation.where(user_id: user_id).each do |general_rules_vacation|
      return general_rules_vacation if general_rules_vacation.active?(now)
    end
    false
  end

  def self.group_user_active?(now, user_id)
    group = BpGroup.user_active_group(now, user_id)
    return false unless group
    group_rules = BpGeneralRulesVacation.group_active?(now, group.id)
    group_rules ? group_rules : false
  end

  def self.user_active(now, user_id)
    rules = user_active?(now, user_id)
    return rules if rules
    rules = group_user_active?(now, user_id)
    rules ? rules : false
  end

  def active_warnings
    bp_rules_warnings.where(active: true)
  end

  def active_restrictive_warnings
    bp_rules_warnings.where(active: true, restrictive: true)
  end

  def doesnt_satisfy_warnings(vacs_days, next_pro_days)
    warnings = active_warnings
    warnings_1 = []
    warnings.each { |warning| warnings_1 << warning if warning.doesnt_satisfy(vacs_days, next_pro_days) }
    return warnings_1
  end

  def doesnt_satisfy_restrictive_warnings(vacs_days, next_pro_days)
    warnings = active_restrictive_warnings
    warnings_1 = []
    warnings.each { |warning| warnings_1 << warning if warning.doesnt_satisfy(vacs_days, next_pro_days) }
    return warnings_1
  end

  def deactivated_by_admin_formatted
    return deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  private

  def days_by_request
    return unless request_max_days && request_min_days && request_max_days < request_min_days
    errors.add(:request_max_days, 'debe ser mayor o igual a el número mínimo')
  end

  def progressive_days_con
    return unless  progressive_max_days && progressive_days_step && progressive_max_days < progressive_days_step
    errors.add(:progressive_max_days, 'debe ser mayor o igual a el número mínimo')
  end

  def since_before_until
    return unless  self.until && since && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end

  def overlap_range_since
    return if any_open_period?
    return unless bp_group_id
    return unless since
    error_message = 'se traslapa con otra asociación. '
    op = overlap_periods
    op.each do |period|
      error_message += 'Período '
      error_message += 'desde: ' + period.since.strftime('%d/%m/%Y %H:%M')
      error_message += ', hasta: ' + period.until.strftime('%d/%m/%Y %H:%M')
      error_message += '. '
    end
    errors.add(:since, error_message) unless op.empty?
  end

  def any_open_period?
    return false unless bp_group_id
    period = BpGeneralRulesVacation.where('until is null and bp_group_id = ?', bp_group_id).first
    return false unless period && !id
    return false unless period && !id && period.id != id
    error_message = 'no puede definir habiendo una asociación sin cerrar para el mismo grupo de inicio: '+period.since.strftime('%d/%m/%Y %H:%M')
    errors.add(:until, error_message)
    true
  end

  def overlap_periods
    overlap_periods = []
    periods = BpGeneralRulesVacation.where('until is not null and bp_group_id = ?', bp_group_id)
    periods.each do |period|
      if self.until
        next unless period.since < self.until && period.until > since
      else
        next unless period.until > since
      end
      next if id && period.id == id
      overlap_periods << period
    end
    return overlap_periods
  end

  def overlap_range_since_user
    return if any_open_period_user?
    return unless user_id
    return unless since
    error_message = 'se traslapa con otra asociación. '
    op = overlap_periods_user
    op.each do |period|
      error_message += 'Período '
      error_message += 'desde: ' + period.since.strftime('%d/%m/%Y %H:%M')
      error_message += ', hasta: ' + period.until.strftime('%d/%m/%Y %H:%M')
      error_message += '. '
    end
    errors.add(:since, error_message) unless op.empty?
  end

  def overlap_periods_user
    overlap_periods = []
    periods = BpGeneralRulesVacation.where('until is not null and user_id = ?', user_id)
    periods.each do |period|
      if self.until
        next unless period.since < self.until && period.until > since
      else
        next unless period.until > since
      end
      next if id && period.id == id
      overlap_periods << period
    end
    return overlap_periods
  end

  def any_open_period_user?
    return false unless user_id
    period = BpGeneralRulesVacation.where('until is null and user_id = ?', user_id).first
    return false unless period && !id
    return false unless period && !id && period.id != id
    error_message = 'no puede definir habiendo una asociación sin cerrar para el mismo usuario con inicio: '+period.since.strftime('%d/%m/%Y %H:%M')
    errors.add(:until, error_message)
    true
  end

  def range_since_concordance_1
    return unless bp_group_id
    return unless since
    return unless bp_group.since && since < bp_group.since
    errors.add(:since, 'debe ser mayor a "desde" del grupo')
  end

  def range_since_concordance_2
    return unless bp_group_id
    return unless since
    return unless bp_group.since && bp_group.until && since > bp_group.until
    errors.add(:since, 'debe ser menor a "hasta" del grupo')
  end

  def range_until_concordance_1
    return unless bp_group_id
    return unless self.until && bp_group.since && bp_group.until && self.until > bp_group.until
    errors.add(:until, 'debe ser menor a "hasta" del grupo')
  end
end
