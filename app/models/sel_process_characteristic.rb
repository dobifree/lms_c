# == Schema Information
#
# Table name: sel_process_characteristics
#
#  id                    :integer          not null, primary key
#  sel_characteristic_id :integer
#  sel_process_step_id   :integer
#  name                  :string(255)
#  description           :text
#  position              :integer
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class SelProcessCharacteristic < ActiveRecord::Base
  belongs_to :sel_characteristic
  belongs_to :sel_process_step
  has_many :sel_step_results, dependent: :destroy
  attr_accessible :description, :name, :position, :registered_at, :registered_by_user_id, :sel_characteristic_id

  validates :name, :presence => true
  validates :position, :presence => true

  default_scope order('position, name')
end
