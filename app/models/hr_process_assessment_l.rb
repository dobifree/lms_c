# == Schema Information
#
# Table name: hr_process_assessment_ls
#
#  id                                :integer          not null, primary key
#  hr_process_evaluation_q_id        :integer
#  hr_process_evaluation_manual_q_id :integer
#  hr_process_user_id                :integer
#  hr_process_user_eval_id           :integer
#  resultado_puntos                  :float
#  resultado_porcentaje              :float
#  logro_indicador                   :float
#  fecha                             :datetime
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#

class HrProcessAssessmentL < ActiveRecord::Base

  belongs_to :hr_process_evaluation_q
  belongs_to :hr_process_evaluation_manual_q
  belongs_to :hr_process_user
  belongs_to :hr_process_user_eval, class_name: 'HrProcessUser', foreign_key: :hr_process_user_eval_id

  attr_accessible :fecha, :hr_process_user_eval_id, :logro_indicador, :resultado_porcentaje, :resultado_puntos,
                  :hr_process_evaluation_q_id, :hr_process_evaluation_manual_q_id, :hr_process_user_id

end
