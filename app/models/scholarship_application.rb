# == Schema Information
#
# Table name: scholarship_applications
#
#  id                     :integer          not null, primary key
#  registered_by_user_id  :integer
#  registered_at          :datetime
#  done                   :boolean
#  done_at                :datetime
#  evaluated              :boolean
#  evaluated_by_user_id   :integer
#  evaluated_at           :datetime
#  evaluation             :boolean
#  evaluation_comment     :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  subject_user_id        :integer
#  scholarship_process_id :integer
#

class ScholarshipApplication < ActiveRecord::Base
  belongs_to :subject_user, class_name: 'User'
  belongs_to :evaluated_by_user, class_name: 'User'
  belongs_to :registered_by_user, class_name: 'User'
  belongs_to :scholarship_process
  has_many :scholarship_application_values, dependent: :destroy
  accepts_nested_attributes_for :scholarship_application_values, allow_destroy: true
  attr_accessible :done, :done_at, :evaluated, :evaluated_at, :subject_user_id,
                  :evaluated_by_user_id, :evaluation, :evaluation_comment,
                  :registered_at, :registered_by_user_id, :scholarship_process_id,
                  :scholarship_application_values_attributes
end
