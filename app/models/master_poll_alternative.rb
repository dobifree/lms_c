# == Schema Information
#
# Table name: master_poll_alternatives
#
#  id                      :integer          not null, primary key
#  numero                  :integer
#  texto                   :text
#  master_poll_question_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class MasterPollAlternative < ActiveRecord::Base

  belongs_to :master_poll_question
  has_many :master_poll_alternative_requirements
  has_many :master_poll_questions, through: :master_poll_alternative_requirements

  attr_accessible :numero, :texto,
                  :master_poll_alternative_requirements,
                  :master_poll_questions

  validates :numero, presence: true, uniqueness: { scope: :master_poll_question_id }, numericality: { only_integer: true, greater_than: 0 }
  validates :texto, presence: true

  default_scope order('numero ASC')

  def letra

    letras = %w(a b c d e f g h i j k l m n o p q r s t u v w x y z)
    return letras[self.numero-1]

  end

end
