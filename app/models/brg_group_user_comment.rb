# == Schema Information
#
# Table name: brg_group_user_comments
#
#  id                    :integer          not null, primary key
#  brg_group_user_id     :integer
#  comment               :text
#  registered_at         :datetime
#  registered_by_user_id :integer
#  active                :boolean          default(TRUE)
#  prev_comment_id       :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class BrgGroupUserComment < ActiveRecord::Base
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :prev_comment, class_name: 'BrgGroupUserComment', foreign_key: :prev_comment_id
  belongs_to :brg_group_user

  attr_accessible :brg_group_user_id,
                  :comment,
                  :active,
                  :registered_at,
                  :registered_by_user_id,
                  :prev_comment_id

  validates :brg_group_user_id, presence: true
  validates :comment, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def self.active; where(active: true) end

end
