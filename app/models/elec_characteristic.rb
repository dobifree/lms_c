# == Schema Information
#
# Table name: elec_characteristics
#
#  id                              :integer          not null, primary key
#  elec_process_id                 :integer
#  characteristic_id               :integer
#  used_to_group_by                :boolean
#  can_be_used_in_reports          :boolean
#  can_be_used_in_reports_position :integer
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

class ElecCharacteristic < ActiveRecord::Base
  belongs_to :elec_process
  belongs_to :characteristic
  attr_accessible :can_be_used_in_reports, :can_be_used_in_reports_position, :used_to_group_by,
                  :elec_process_id, :characteristic_id

  validates :characteristic_id, :presence => true
  validates :can_be_used_in_reports_position, :numericality => {:only_integer => true}, :if => :can_be_used_in_reports
  validates :can_be_used_in_reports_position, :presence => true, :if => :can_be_used_in_reports


end
