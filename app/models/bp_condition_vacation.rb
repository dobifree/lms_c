# == Schema Information
#
# Table name: bp_condition_vacations
#
#  id                           :integer          not null, primary key
#  bp_season_id                 :integer
#  bp_rule_vacation_id          :integer
#  priority                     :integer
#  bonus_days                   :integer
#  bonus_money                  :float
#  bonus_currency               :string(255)
#  days_since                   :integer
#  days_until                   :integer
#  antiquity                    :integer
#  since                        :datetime
#  until                        :datetime
#  period_times                 :integer
#  period_anniversary           :boolean          default(TRUE)
#  period_begin_1               :date
#  period_begin_2               :date
#  period_condition_begin       :boolean          default(TRUE)
#  period_condition_min_days    :integer
#  period_condition_min_percent :float
#  season_condition_begin       :boolean          default(TRUE)
#  season_condition_min_days    :integer
#  season_condition_min_percent :float
#  deactivated_at               :datetime
#  deactivated_by_admin_id      :integer
#  registered_at                :datetime
#  registered_by_admin_id       :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  bonus_only_laborable_day     :boolean          default(TRUE)
#  refundable                   :boolean          default(TRUE)
#  days_max_accumulated         :integer
#  days_min_accumulated         :integer
#  period_times_type            :integer          default(0)
#  available                    :boolean          default(TRUE)
#  prev_cond_vac_id             :integer
#  next_cond_vac_id             :integer
#  unavailable_at               :datetime
#  unavailable_by_admin_id      :integer
#  accumulated_type             :integer          default(0)
#  indef_contract_required      :boolean          default(FALSE)
#

class BpConditionVacation < ActiveRecord::Base
  has_many :vac_rule_records

  belongs_to :bp_season
  belongs_to :bp_rule_vacation
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id

  attr_accessible :bp_rule_vacation_id,
                  :priority,
                  :bonus_days,
                  :bonus_money,
                  :bonus_currency,
                  :days_since,
                  :days_until,
                  :antiquity,
                  :since,
                  :until,
                  :days_max_accumulated,
                  :days_min_accumulated,
                  :period_times,
                  :period_anniversary,
                  :period_begin_1,
                  :period_begin_2,
                  :period_condition_begin,
                  :period_condition_min_days,
                  :period_condition_min_percent,
                  :bp_season_id,
                  :season_condition_begin,
                  :season_condition_min_days,
                  :season_condition_min_percent,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id,
                  :period_times_type,
                  :accumulated_type,
                  :indef_contract_required

  validates :bp_rule_vacation_id, presence: true
  validates :priority, presence: true,
            numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  validates :bonus_days, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :bonus_money, numericality: { greater_than_or_equal_to: 0 }, :if => :bonus_money

  validates :bonus_money, presence: true, :if => :bonus_currency?
  validates :bonus_currency, presence: true, :if => :bonus_money?

  validates :days_since, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :days_until, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :days_min_accumulated, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :days_max_accumulated, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :antiquity, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true

  validates :period_times, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, allow_nil: true
  validates :period_begin_1, presence: true, :unless => :period_anniversary?
  validates :period_condition_begin, inclusion: { in: [true, false] }
  validates :period_condition_min_days, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :period_condition_min_percent, numericality: { greater_than_or_equal_to: 0 }, :allow_nil => true

  validates :season_condition_begin, inclusion: { in: [true, false] }
  validates :season_condition_min_days, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, :allow_nil => true
  validates :season_condition_min_percent, numericality: { greater_than_or_equal_to: 0 }, :allow_nil => true

  validates :since, presence: true
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  validate :any_bonus
  validate :periods_anniversary
  validate :season_id_nil
  validate :since_before_until
  validate :range_since_concordance_1
  validate :range_since_concordance_2
  validate :range_until_concordance_1

  validate :days_accum_concor
  validate :days_since_until_concor

  validate :uniqueness_priority

  def self.accumulated_types
    [0,1,2]
  end

  def self.accumulated_type_names
    ['Solamente legales',
     'Solamente progresivos',
     'Legales + Progresivos']
  end

  def self.accumulated_types_array
    [
        [BpConditionVacation.accumulated_type_names[0], BpConditionVacation.accumulated_types[0]],
        [BpConditionVacation.accumulated_type_names[1], BpConditionVacation.accumulated_types[1]],
        [BpConditionVacation.accumulated_type_names[2], BpConditionVacation.accumulated_types[2]]
    ]
  end

  def self.prev_ids(array, cond_vac_id)
    cond_vac = BpConditionVacation.find(cond_vac_id)
    array << cond_vac
    if cond_vac.prev_cond_vac_id
      BpConditionVacation.prev_ids(array, cond_vac.prev_cond_vac_id)
    end
    array.sort_by(&:registered_at)
  end

  def self.next_ids(array, cond_vac_id)
    cond_vac = BpConditionVacation.find(cond_vac_id)
    array << cond_vac
    if cond_vac.next_cond_vac_id
      BpConditionVacation.next_ids(array, cond_vac.next_cond_vac_id)
    end
    array.sort_by(&:registered_at)
  end

  def bring_all_version_ids
    array = []
    array << self
    array = BpConditionVacation.prev_ids(array, prev_cond_vac_id) if prev_cond_vac_id
    array
  end

  def bring_active_versions_vac_rule
    array = []
    bp_rule_vacation.bp_condition_vacations.where(available: true).each do |lic|
      array << lic.id
    end
    array
  end

  def bring_all_versions_vac_rule
    array = []
    bp_rule_vacation.bp_condition_vacations.each do |lic|
      array << lic.id
    end
    array
  end

  def def_ids
    case period_times_type
      when 0 ; bring_all_version_ids
      when 1 ; [id]
      when 2 ; bring_active_versions_vac_rule
      when 3 ; bring_all_versions_vac_rule
      else ; nil
    end
  end

  def self.period_times_types
    [0,1,2,3]
  end

  def self.period_times_types_name
    ['Todas las versiones de este beneficio',
     'Esta versión del beneficio',
     'Versiones activas de agrupador',
     'Todas las versiones de agrupador']
  end

  def self.period_times_array
    [[BpLicense.period_times_types_name[1], BpLicense.period_times_types[1]],
     [BpLicense.period_times_types_name[0], BpLicense.period_times_types[0]],
     [BpLicense.period_times_types_name[2], BpLicense.period_times_types[2]],
     [BpLicense.period_times_types_name[3], BpLicense.period_times_types[3]]]
  end


  def active?(now)
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  def full_name
    name = bonus_days ? bonus_days.to_s+' días ' : bonus_money.to_s+' '+bonus_currency
    name += '('+bp_season.name+')' if bp_season_id
    name
  end

  def self.active_condition_vacations(now)
    # condition_vacations = BpConditionVacation.reorder('bp_group_id ASC, bp_rule_vacation_id ASC').all
    condition_vacations = BpConditionVacation.joins([:bp_rule_vacation => :bp_group]).reorder('bp_group_id ASC, bp_rule_vacation_id ASC').all
    condition_vacation_to_r = []
    condition_vacations.each do |condition_vacation|
      next unless condition_vacation.active?(now)
      condition_vacation_to_r << condition_vacation
    end
    return condition_vacation_to_r
  end

  def deactivated_by_admin_formatted
    deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  def satisfy?(begin_vacs, end_vacs, days_vacs, days_prog, anniversary, user_id, next_vac, next_pro_days, request)
    return false unless (active?(begin_vacs.to_datetime) || active?(end_vacs.to_datetime))
    return false unless satisfy_days?(days_vacs, days_prog)
    return false unless satisfy_season_conditions(user_id, begin_vacs, end_vacs)
    return false unless satisfy_max_min_days_accum(user_id, next_vac, next_pro_days)
    return false unless satisfy_antiquity?(anniversary, begin_vacs)
    return false unless satisfy_periods_conditions(anniversary, user_id, begin_vacs, end_vacs, request)
    return false unless satisfy_indefined_contract_required(user_id, begin_vacs, end_vacs)
    true
  end

  def satisfy_indefined_contract_required(user_id, begin_vacs, end_vacs)
    return true unless indef_contract_required
    user = User.find(user_id)
    user.secu_tiene_contrato_indefinido(begin_vacs.to_date)
  end

  def satisfy_season_conditions(user_id, begin_vacs, end_vacs)
    return true unless bp_season_id
    begin_vacs = begin_vacs.to_date
    end_vacs = end_vacs.to_date
    season_periods = []

    BpSeasonPeriod.all.each do |season_period|
      next unless season_period.bp_season_id.to_i == bp_season_id.to_i
      if season_period.until
        next unless season_period.since.to_date <= begin_vacs && begin_vacs <= season_period.until.to_date
      else
        next unless season_period.since.to_date <= begin_vacs
      end
      season_periods << season_period
    end

    return false unless season_periods.size > 0

    periods_overlap = []
    season_periods.each do |season_period|
      periods_overlap += seasons_overlap_month_acc(season_period.begin,season_period.end, begin_vacs, end_vacs, season_period.bp_season_id)
    end

    if periods_overlap.size > 0
      chosen_period = evaluate_season_conditions(periods_overlap, begin_vacs, end_vacs)
    else
      chosen_period = false
    end

    unless chosen_period
      return false
    end
    chosen_period[2].to_i == bp_season_id.to_i
  end

  def evaluate_season_conditions(periods, begin_vacs, end_vacs)
    begin_vacs = begin_vacs.to_date
    end_vacs = end_vacs.to_date

    periods_ok = []
    periods.each do |period|
      next unless period[0].to_date < end_vacs && period[1].to_date >= begin_vacs
      satisfy_1 = satisfy_season_min_days?(period[0], period[1], begin_vacs, end_vacs)
      satisfy_2 = satisfy_season_min_percent?(period[0], period[1], begin_vacs, end_vacs)
      periods_ok << period if (satisfy_1 && satisfy_2)
    end

    if periods_ok.size > 1
      return get_default_season(periods_ok, begin_vacs, end_vacs)
    else
      if periods_ok.size > 0
        return periods_ok[0]
      else
        return nil
      end
    end
  end

  def seasons_overlap_month_acc(begin_period, end_period, date_begin, date_end, season_id)
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    date_begin = date_begin.to_date
    date_end = date_end.to_date
    periods = []

    begin_period = begin_period.change(year: date_begin.year)
    loop do
      break if begin_period <= date_begin
      begin_period -= 1.year
    end
    end_period = end_period.change(year: begin_period.year)
    end_period += 1.year if end_period < begin_period

    loop do
      periods << [begin_period, end_period, season_id] if begin_period < date_end && end_period >= date_begin
      begin_period += 1.year
      end_period += 1.year
      break if begin_period > date_end
    end

    return periods
  end

  def satisfy_season_min_days?(begin_period, end_period, begin_vacs, end_vacs)
    return true unless season_condition_min_days
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    begin_vacs = begin_vacs.to_date
    end_vacs = end_vacs.to_date

    max_begin_date = begin_period > begin_vacs ? begin_period : begin_vacs
    min_end_date = end_period < end_vacs ? end_period : end_vacs
    vacs_days = (min_end_date - max_begin_date).to_i + 1

    vacs_days >= season_condition_min_days
  end

  def satisfy_season_min_percent?(begin_period, end_period, begin_vacs, end_vacs)
    return true unless season_condition_min_percent
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    begin_vacs = begin_vacs.to_date
    end_vacs = end_vacs.to_date

    max_begin_date = (begin_period > begin_vacs ? begin_period : begin_vacs)
    min_end_date = (end_period < end_vacs ? end_period : end_vacs)

    period_days = (min_end_date - max_begin_date).to_i + 1
    vacs_days = (end_vacs - begin_vacs).to_i + 1

    return ((period_days/vacs_days.to_f)*100) >= season_condition_min_percent
  end

  def get_default_season(periods_ok, begin_vacs, end_vacs)
    date_to_evaluate = season_condition_begin ? begin_vacs : end_vacs
    date_to_evaluate = date_to_evaluate.to_date
    periods_ok.each do |period|
      return period if date_to_evaluate >= period[0].to_date && date_to_evaluate <= period[1].to_date
    end
    return nil
    end

  def satisfy_periods_conditions(anniversary, user_id, begin_vacs, end_vacs, request)
    return true unless period_times
    periods_to_evaluate_overlap = define_period_to_evaluate(anniversary)
    periods_overlap = []
    periods_to_evaluate_overlap.each do |period|
      periods_overlap += periods_overlap_month_acc(period[0],period[1], begin_vacs, end_vacs)
    end

    if periods_overlap.size > 0
      chosen_period = evaluate_periods_conditions(periods_overlap, begin_vacs, end_vacs)
    else
      chosen_period = false
    end

    unless chosen_period
      return false
    end

    evaluation = evaluate_taken_times(chosen_period[0], chosen_period[1], user_id, request)

    return evaluation
  end

  def periods_overlap_month_acc(begin_period, end_period, date_begin, date_end)
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    date_begin = date_begin.to_date
    date_end = date_end.to_date
    periods = []

    begin_period = begin_period.change(year: date_begin.year)
    loop do
      break if begin_period <= date_begin
      begin_period -= 1.year
    end
    end_period = end_period.change(year: date_begin.year)
    end_period += 1.year if end_period.year < begin_period.year

    loop do
      periods << [begin_period, end_period] if begin_period < date_end && end_period >= date_begin
      begin_period += 1.year
      end_period += 1.year
      break if begin_period > date_end
    end

    return periods
  end

  def define_period_to_evaluate(anniversary)
    anniversary = anniversary.to_date if anniversary
    periods = []

    if period_anniversary
      period_begin = anniversary
      period_end = anniversary - 1.day
      periods << [period_begin, period_end]
      return periods
    end

    if period_begin_2
      period_begin = period_begin_1
      period_end = period_begin_2 - 1.day
      periods << [period_begin, period_end]

      period_begin = period_begin_2
      period_end = period_begin_1 - 1.day
      periods << [period_begin, period_end]

      return periods
    end

    period_begin = period_begin_1
    period_end = period_begin_1 - 1.day
    periods << [period_begin, period_end]
    return periods
  end

  def evaluate_periods_conditions(periods, begin_vacs, end_vacs)
    begin_vacs = begin_vacs.to_date
    end_vacs = end_vacs.to_date

    periods_ok = []
    periods.each do |period|
      next unless period[0].to_date < end_vacs && period[1].to_date >= begin_vacs
      satisfy_1 = satisfy_period_condition_min_days?(period[0], period[1], begin_vacs, end_vacs)
      satisfy_2 = satisfy_period_condition_min_percent?(period[0], period[1], begin_vacs, end_vacs)
      periods_ok << period if (satisfy_1 && satisfy_2)
    end

    if periods_ok.size > 1
      return get_default_period(periods_ok, begin_vacs, end_vacs)
    else
      if periods_ok.size > 0
        return periods_ok[0]
      else
        return nil
      end
    end
  end

  def satisfy_period_condition_min_days?(begin_period, end_period, begin_vacs, end_vacs)
    return true unless period_condition_min_days
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    begin_vacs = begin_vacs.to_date
    end_vacs = end_vacs.to_date

    max_begin_date = begin_period > begin_vacs ? begin_period : begin_vacs
    min_end_date = end_period < end_vacs ? end_period : end_vacs

    vacs_days = (min_end_date - max_begin_date).to_i + 1
    return vacs_days >= period_condition_min_days
  end

  def satisfy_period_condition_min_percent?(begin_period, end_period, begin_vacs, end_vacs)
    return true unless period_condition_min_percent
    begin_period = begin_period.to_date
    end_period = end_period.to_date
    begin_vacs = begin_vacs.to_date
    end_vacs = end_vacs.to_date

    max_begin_date = (begin_period > begin_vacs ? begin_period : begin_vacs)
    min_end_date = (end_period < end_vacs ? end_period : end_vacs)

    period_days = (min_end_date - max_begin_date).to_i + 1
    vacs_days = (end_vacs - begin_vacs).to_i + 1

    return ((period_days/vacs_days.to_f)*100) >= period_condition_min_percent
  end

  def get_default_period(periods_ok, begin_vacs, end_vacs)
    date_to_evaluate = period_condition_begin ? begin_vacs : end_vacs
    date_to_evaluate = date_to_evaluate.to_date
    periods_ok.each do |period|
      return period if date_to_evaluate >= period[0].to_date && date_to_evaluate <= period[1].to_date
    end
    return nil
  end

  def evaluate_taken_times(begin_period, end_period, user_id, request)
    usages = get_usages(begin_period, end_period, user_id, request)
    usages.size < period_times
  end

  def get_usages(begin_period, end_period, user_id, request)
    usages = VacRuleRecord.taken_by(user_id, def_ids)
    periods = [[begin_period.to_date, end_period.to_date]]
    all_usages = []

    usages.each do |usage|
      next if request && usage.vac_request.id == request.id
      next unless usage.vac_request.active && usage.vac_request.in_process?
      next unless usage.vac_request.user_id.to_i == user_id.to_i
      evaluation = evaluate_periods_conditions(periods, usage.vac_request.begin, usage.vac_request.end)
      next unless evaluation && evaluation.size > 0
      all_usages << usage
    end
    return all_usages
  end

  def satisfy_days?(days_vacs, days_prog)
    return true unless days_vacs || days_prog
    return true unless days_since || days_until

    days_vacs_total = 0
    days_vacs_total += days_vacs.to_i  if days_vacs && days_vacs.to_i > 0
    days_vacs_total += days_prog.to_i  if days_prog && days_prog.to_i > 0

    return days_vacs_total >= days_since if  days_since && !days_until
    return days_vacs_total <= days_until if  !days_since && days_until
    (days_vacs_total >= days_since && days_vacs_total <= days_until)
  end

  def satisfy_antiquity?(anniversary, begin_vacs)
    return true unless antiquity
    begin_vacs = begin_vacs.to_date
    anniversary = anniversary.to_date

    months = (begin_vacs.year - anniversary.year)*12
    months += begin_vacs.month - anniversary.month
    months += 1 if (begin_vacs.month == anniversary.month && begin_vacs.day > anniversary.day)

    (antiquity <= months)
  end

  def satisfy_max_min_days_accum(user_id, next_vac, next_pro_days)
    return true unless days_max_accumulated || days_min_accumulated
    return true unless next_vac

    case accumulated_type
      when 0
        vac_to_evaluate = next_vac
      when 1
        vac_to_evaluate = next_pro_days ? next_pro_days : 0
      when 2
        vac_to_evaluate = next_pro_days ? (next_vac + next_pro_days) : 0
      else
        vac_to_evaluate = next_vac
    end

    return vac_to_evaluate >= days_min_accumulated if  days_min_accumulated && !days_max_accumulated
    return vac_to_evaluate <= days_max_accumulated if  !days_min_accumulated && days_max_accumulated
    (vac_to_evaluate >= days_min_accumulated && vac_to_evaluate <= days_max_accumulated)
  end

  def period_season_vacs(vacs_begin, vacs_end)
    vacs_begin = vacs_begin.to_date
    vacs_end = vacs_end.to_date

    return unless bp_season_id
    return unless bp_season.active?(vacs_begin)
    period = bp_season.active_period(vacs_begin)
    return unless period

    begin_period = Date.new().change(year: vacs_begin.year, month: period.begin.month, day: period.begin.day)
    end_period_year = period.begin.month < period.end.month ? vacs_begin.year : (vacs_begin.year+1.year)
    end_period = Date.new().change(year: end_period_year, month: period.end.month, day: period.end.day)

    first_period = evaluate_season_period(vacs_begin, vacs_end, begin_period, end_period)
    second_period = evaluate_season_period(vacs_begin, vacs_end, end_period-1.year, begin_period-1.year)

    period_1 = []

    period_1[0] = begin_period if first_period
    period_1[1] = end_period if first_period

    period_1[0] = begin_period-1.year if second_period
    period_1[1] = end_period-1.year if second_period

    if first_period && second_period
      if period_condition_begin && vacs_begin > begin_period && vacs_begin < end_period
        period_1[0] = begin_period
        period_1[1] = end_period
      end
      if period_condition_begin && vacs_begin > begin_period-1.year && vacs_begin < end_period-1.year
        period_1[0] = begin_period-1.year
        period_1[1] = end_period-1.year
      end
      if !period_condition_begin && vacs_end > begin_period && vacs_end < end_period
        period_1[0] = begin_period
        period_1[1] = end_period
      end
      if !period_condition_begin && vacs_end > begin_period-1.year && vacs_end < end_period-1.year
        period_1[0] = begin_period-1.year
        period_1[1] = end_period-1.year
      end
    end
    period_1
  end

  def evaluate_season_period(begin_date, end_date, period_begin, period_end)
    fulfill_requirement = true
    first_date = period_begin > begin_date ? period_begin : begin_date
    final_date = period_end > end_date ? end_date : period_end

    if season_condition_min_days
      fulfill_requirement = (final_date - first_date).to_i >= season_condition_min_days
    end

    if period_condition_min_percent
      fulfill_requirement = (final_date - first_date)/(period_end - period_begin) >= period_condition_min_percent
    end

    fulfill_requirement
  end

  def define_period(first_period, second_period, begin_period, end_period)
    period = []
    if first_period
      if second_period
        if period_condition_begin
          period << begin_period
          period << end_period
        else
          if period_begin_2
            aux = begin_period
            period << end_period + 1.day
            period << aux + 1.year
          else
            period << begin_period + 1.year
            period << end_period + 1.year
          end

        end
      else
        period << begin_period
        period << end_period
      end
    else
      if second_period
        if period_begin_2
          aux = begin_period
          period << end_period + 1.day
          period << aux + 1.year
        else
          period << begin_period + 1.year
          period << end_period + 1.year
        end
      end
    end
    period
  end

  private

  def any_bonus
    return unless !bonus_currency && !bonus_days && !bonus_money
    errors.add(:bonus_days, 'debe escoger por lo menos un bono')
  end

  def days_accum_concor
    return unless days_max_accumulated && days_min_accumulated
    return unless days_max_accumulated < days_min_accumulated
    errors.add(:days_max_accumulated, 'debe ser mayor a días mínimos acumulados por usuario')
  end

  def days_since_until_concor
    return unless days_until && days_since
    return unless days_until < days_since
    errors.add(:days_until, 'debe ser mayor a días mínimo')
  end

  def periods_anniversary
    return unless period_anniversary? && (period_begin_1 || period_begin_2)
    errors.add(:period_anniversary, 'no admite períodos')
  end

  def season_id_nil
    return unless season_condition_min_days || season_condition_min_percent
    return if bp_season_id
    errors.add(:bp_season_id, 'no puede ser vacío si se ha escrito condiciones para temporada')
  end

  def since_before_until
    return unless self.until && since && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end

  def range_since_concordance_1
    return unless bp_rule_vacation_id
    return unless since
    return unless bp_rule_vacation.since && since < bp_rule_vacation.since
    errors.add(:since, 'debe ser mayor a "desde" del bono')
  end

  def range_since_concordance_2
    return unless bp_rule_vacation_id
    return unless since
    return unless bp_rule_vacation.since && bp_rule_vacation.until && since > bp_rule_vacation.until
    errors.add(:since, 'debe ser menor a "hasta" del bono')
  end

  def range_until_concordance_1
    return unless bp_rule_vacation_id
    return unless self.until && bp_rule_vacation.since && bp_rule_vacation.until && self.until > bp_rule_vacation.until
    errors.add(:until, 'debe ser menor a "hasta" del bono')
  end

  def uniqueness_priority
    return unless priority
    return unless available
    conds = BpConditionVacation.where(available: true, bp_rule_vacation_id: bp_rule_vacation_id, priority: priority)

    conds.each do |cond|
      next unless cond.id != prev_cond_vac_id
      next unless cond.id != next_cond_vac_id
      errors.add(:priority, 'no puede repetirse para el mismo Bono')
    end
  end
end

#  bp_season_id                 :integer
#   TEMPORADA EN LA QUE DEBEN ESTAR LAS VACACIONES PARA DAR EL BONO.
#   CONDICIONA LA OBLIGATORIEDAD DE LOS ATRIBUTOS: SEASON_...

#  bp_rule_vacation_id          :integer
#   BONO AL QUE PERTENECE. AGRUPA CONDICIONES. TODOS DENTRO DE UNO SOLO SE COMPARAN CON UN Y. LOS GRUPOS SE COMPARAN CON O, PUEDEN SER VARIOS.

#  priority                     :integer
  #DENTRO DEL CONJUNTO DE DE BONOS, ¿CUÁL ESCOJO SI ES QUE HAY MÁS DE UNO QUE CUMPLE CON TODAS LAS CONDICIONES?

#  bonus_days                   :integer
#   BONO EN DÍAS QUE SE DA, DE CUMPLIR ESTE CONJUNTO DE CONDICIONES

#  bonus_money                  :float
#   BONO EN DINERO QUE SE DA DE CUMPLIR ESTE CONJUNTO DE CONDICIONES

#  bonus_currency               :string(255)
#   MONEDA DEL BONO EN DINERO

#  days_since                   :integer
#   CANTIDAD DE DÍAS MÍNIMO PARA RECIBIR BONO

#  days_until                   :integer
#   CANTIDAD DE DÍAS MÁXIMOS PARA RECIBIR BONO: DE 5 A 10 DÍAS

#  antiquity                    :integer
    #ANTIGUEDAD EN AÑOS PARA RECIBIR BONO.

#  since                        :date
#   BONO ACTIVO DESDE: FECHA

#  until                        :date
#   BONO ACTIVO HASTA: FECHA

#  period_times                 :integer
#   EN UN PERÍODO: PUEDE SER ANIVERSARIO, O FIJO (ANUAL O BIANUAL), ¿CUÁNTAS VECES PUEDES LLEGAR A RECIBIR EL BONO

#  period_anniversary           :boolean          default(TRUE)
#   PERÍODO A CONSIDERAR ES EL AÑO DE ANIVERSARIO DE LA PERSONA

#  period_begin_1               :date
#   INICIO DE PERÍODO 1, DE NO SER ANIVERSARIO. SI SOLO ESTÁ ESTE PARAMETRO, INDICA ANUALIDAD.

#  period_begin_2               :date
#   INICIO DE PERÍODO 2, DE NO SER ANIVERSARIO. SI ESTÁ ESTE PARÁMETRO, INDICA BIANUALIDAD. NO PUEDE ESTAR ESTE PARÁMETRO Y EL ANTERIOR NO.

#  period_condition_begin       :boolean          default(TRUE)
#   PARA SABER QUÉ FECHA ES EL DEFAULT PARA CONSIDERAR VACACIONES EN ESE PERIODO U OTRO: TRUE=INICIO, FALSE=FINAL

#  period_condition_min_days    :integer mayor o igual
#   MÍNIMA CANTIDAD DE DÍAS QUE DEBEN ESTAR DICHAS VACACIONES PARA CONSIDERARSE DE ESE PERÍODO

#  period_condition_min_percent :float mayor o igual
#   MÍNIMO PORCENTAJE QUE DEBEN ESTAR DICHAS VACACIONES PARA CONSIDERARSE DE ESE PERÍODO

#  season_condition_begin       :boolean          default(TRUE)
#   PARA SABER QUÉ FECHA ES EL DEFAULT PARA CONSIDERAR VACACIONES EN ESA TEMPORADA U OTRA: TRUE=INICIO, FALSE=FINAL

#  season_condition_min_days    :integer
#   MÍNIMA CANTIDAD DE DÍAS QUE DEBEN ESTAR DICHAS VACACIONES PARA CONSIDERARSE DE ESA TEMPORADA

#  season_condition_min_percent :float
#   MÍNIMO PORCENTAJE QUE DEBEN ESTAR DICHAS VACACIONES PARA CONSIDERARSE DE ESA TEMPORADA

#  active                       :boolean          default(TRUE)
#  deactivated_at               :datetime
#  deactivated_by_admin_id      :integer
#  registered_at                :datetime
#  registered_by_admin_id       :integer
