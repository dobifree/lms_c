# == Schema Information
#
# Table name: pe_assessment_evaluations
#
#  id                    :integer          not null, primary key
#  pe_process_id         :integer
#  pe_member_rel_id      :integer
#  pe_evaluation_id      :integer
#  points                :float
#  percentage            :float
#  last_update           :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  pe_dimension_group_id :integer
#  valid_evaluation      :boolean          default(TRUE)
#

class PeAssessmentEvaluation < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_member_rel
  belongs_to :pe_evaluation
  belongs_to :pe_dimension_group

  has_many :pe_assessment_questions, dependent: :destroy

  attr_accessible :last_update, :percentage, :points, :pe_member_rel_id, :pe_process_id, :pe_evaluation_id, :valid_evaluation

  validates :percentage, presence: true, numericality: true
  validates :points, presence: true, numericality: true

  def pe_assessment_question(pe_question)

    self.pe_assessment_questions.where('pe_question_id = ?', pe_question.id).first

  end

  def pe_assessment_questions_1st_level(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_evaluation.pe_elements.first

      pe_questions_id = self.pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel).pluck(:id)

      #pe_questions_id = self.pe_evaluation.pe_questions_by_group_1st_level(pe_group.id).pluck(:id)

      self.pe_assessment_questions.where('pe_question_id IN (?)', pe_questions_id)

    end

  end

  def pe_assessment_questions_2nd_level(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_evaluation.pe_elements.second

      pe_questions_id = self.pe_evaluation.pe_questions_by_pe_member_2nd_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel).pluck(:id)

      #pe_questions_id = self.pe_evaluation.pe_questions_by_group_2nd_level(pe_group.id).pluck(:id)

      self.pe_assessment_questions.where('pe_question_id IN (?)', pe_questions_id)

    end

  end

  def pe_assessment_questions_3rd_level(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_evaluation.pe_elements[2]

      pe_questions_id = self.pe_evaluation.pe_questions_by_pe_member_3rd_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel).pluck(:id)

      #pe_questions_id = self.pe_evaluation.pe_questions_by_group_2nd_level(pe_group.id).pluck(:id)

      self.pe_assessment_questions.where('pe_question_id IN (?)', pe_questions_id)

    end

  end

end
