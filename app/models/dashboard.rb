class Dashboard

  include UserCoursesHelper
  include PlanningProcessesHelper

  def initialize(user, lms_time)
    @user = user
    @characteristics_mini_ficha = Characteristic.where('mini_ficha = ?', true).reorder('orden_mini_ficha')
    @first_owned_node = @user.owned_nodes.first
    @members = @user.active_members

    @ct_module_mpi = CtModule.find_by_cod('mpi')
    @ct_module_pe = CtModule.find_by_cod('pe')
    @ct_module_lms = CtModule.find_by_cod('lms')
    @ct_module_trkn = CtModule.find_by_cod('trkn')

    @ct_menu_tr = CtMenu.find_by_cod('tr')
    @ct_menu_pe = CtMenu.find_by_cod('pe')

    @mpi_view_users = user.mpi_view_users

    @notifications = Notification.actives_for_user(@user.id)
    @banners = Banner.actives_for_user(@user.id, Banner.alternative_location.index(:dashboard_user))
    @poll_processes_active_answer = PollProcess.actives_for_user_to_answer(@user.id, lms_time)
    @poll_processes_active_report = PollProcess.actives_for_user_view_results(@user.id, lms_time)
    @sel_processes_applicable = SelProcess.active.in_time(lms_time).applicable.internal
    @elec_process_active_answer = ElecRound.actives_to_user_to_answer(@user.id, lms_time)
    @elec_process_to_see_live_results_extra_time = ElecRound.actives_to_user_to_see_live_results_extra_time(@user.id, lms_time)
    @elec_process_to_see_final_results_extra_time = ElecRound.actives_to_user_to_see_final_results_extra_time(@user.id, lms_time)
    @tracking_processes = TrackingProcess.current_processes(@user.id)


    @pending_user_courses = Array.new
    @pending_button_states = Array.new

    @user.enrollments.each do |enrollment|

      if enrollment.program.activo

        enrollment.level.program_courses.where('hidden_in_user_courses_list = ?', false).each do |program_course|

          #program_course = ProgramCourse.where('program_id = ? AND course_id = ? ', enrollment.program.id, course.id).first

          estado, estado_boton, user_course, user_courses = user_course_status(enrollment, program_course, lms_time)

          mostrar = false

          if estado_boton == :resultados

            encuesta_satisfaccion_pendiente = false

            program_course.course.polls.each do |poll|

              if poll.tipo_satisfaccion && user_course.dncp && !user_course.dncp.poll_presencial

                user_course_poll = user_course.user_course_polls.find_by_poll_id(poll.id)

                if user_course_poll

                  encuesta_satisfaccion_pendiente = true unless user_course_poll.finalizada

                else

                  encuesta_satisfaccion_pendiente = true

                end

              end

            end

            mostrar = true if encuesta_satisfaccion_pendiente

          elsif estado_boton == :continuar

            mostrar = true

          elsif estado_boton == :iniciar

            mostrar = true if enrollment.program.especifico

          end

          if mostrar

            @pending_user_courses.push user_course
            @pending_button_states.push estado_boton

          end


        end

      end

    end

    @dnc_cua_pendientes = Array.new
    @dnc_pp_pendientes = Array.new
    @dnc_estado_pendientes = Array.new

    if can_show_planning_process_training_analyst_menu? @user

      cua_pendientes, pp_pendientes, estado_pendientes = pending_training_analyst_activities @user

      @dnc_cua_pendientes += cua_pendientes
      @dnc_pp_pendientes += pp_pendientes
      @dnc_estado_pendientes += estado_pendientes

    end

    if can_show_planning_process_area_manager_menu? @user

      cua_pendientes, pp_pendientes, estado_pendientes = pending_area_manager_activities @user

      @dnc_cua_pendientes += cua_pendientes
      @dnc_pp_pendientes += pp_pendientes
      @dnc_estado_pendientes += estado_pendientes

    end

    @pace_cua_as_pendientes = Array.new
    @pace_pp_as_pendientes = Array.new
    @pace_estado_as_pendientes = Array.new

    if can_show_planning_process_area_superintendent_menu? @user

      cua_pendientes, pp_pendientes, estado_pendientes = pending_area_superintendent_activities @user

      @pace_cua_as_pendientes += cua_pendientes
      @pace_pp_as_pendientes += pp_pendientes
      @pace_estado_as_pendientes += estado_pendientes

    end

    @pace_cua_um_pendientes = Array.new
    @pace_pp_um_pendientes = Array.new

    if can_show_planning_process_unit_manager_menu? @user

      cua_pendientes, pp_pendientes = pending_unit_manager_activities @user
      @pace_cua_um_pendientes += cua_pendientes
      @pace_pp_um_pendientes += pp_pendientes

    end

    #######

    @pe_processes = Array.new

    @pe_evaluations_to_define = Array.new
    @num_pending_define = Array.new

    @pe_evaluations_to_accept = Array.new

    @pe_evaluations_to_validate = Array.new
    @num_pending_validate = Array.new
    @num_pending_validate_alias = Array.new


    @pe_evaluations_to_track = Array.new
    @pe_process_to_assess = Array.new
    @pe_process_to_validate = Array.new
    @pe_process_to_calibrate = Array.new
    @pe_process_to_provide_feedback = Array.new
    @pe_process_to_accept_feedback = Array.new

    PeProcess.where('active = ? AND from_date <= ? AND to_date >= ?', true, lms_time, lms_time).each do |pe_process|

      pe_evaluations_where_has_to_define_questions = pe_process.pe_evaluations_where_has_to_define_questions @user

      has_to_define_questions = true if pe_process.has_step_definition_by_users? && pe_evaluations_where_has_to_define_questions.size > 0

      pe_evaluations_where_has_to_define_questions_pending = Array.new
      num_pending_define = Array.new


      pe_evaluations_where_has_to_accept_defined_questions = pe_process.pe_evaluations_where_has_to_accept_defined_questions @user

      pe_evaluations_where_has_to_accept_defined_questions_pending = Array.new

      has_to_accept_defined_questions = true if pe_process.has_step_definition_by_users? && pe_process.has_step_definition_by_users_accepted? && pe_evaluations_where_has_to_accept_defined_questions.size > 0

      pe_evaluations_where_has_to_validate_defined_questions = pe_process.pe_evaluations_where_has_to_validate_defined_questions @user

      has_to_validate_defined_questions = true if pe_process.has_step_definition_by_users? && pe_process.has_step_definition_by_users_validated? && pe_evaluations_where_has_to_validate_defined_questions.size > 0

      pe_evaluations_where_has_to_validate_defined_questions_pending = Array.new
      num_pending_validate = Array.new
      num_pending_validate_alias = Array.new

      pe_members_connected = pe_process.pe_members_by_user @user

      if has_to_accept_defined_questions

        pe_members_connected.each do |pe_member_connected|

          pe_evaluations_where_has_to_accept_defined_questions.each do |pe_eval|

            status = pe_eval.pe_member_status_to_be_accepted(pe_member_connected)
            if status == 'ready'
              pe_evaluations_where_has_to_accept_defined_questions_pending.push pe_eval
            end

          end

        end

      end

      if has_to_define_questions

        pe_process.pe_rels.each do |pe_rel|

          pe_members_connected.each do |pe_member_connected|

            pe_evaluations_where_has_to_define_questions.each do |pe_eval|

              pe_eval.rel_ids_to_define_elements.each do |rel_id|

                if rel_id == pe_rel.rel

                  num_pend = 0

                  pe_member_connected.pe_member_rels_is_evaluator_as_pe_rel_ordered(pe_rel).each_with_index do |pe_member_rel_evaluated|

                    status_def, step_label, step_label_smart_table = pe_eval.pe_member_definition_by_users_status pe_member_rel_evaluated.pe_member_evaluated

                    num_pend += 1 if status_def == 'not_initiated' || status_def == 'not_confirmed' || status_def == 'in_definition'

                  end

                  if num_pend > 0

                    if pe_evaluations_where_has_to_define_questions_pending.include? pe_eval
                      # para evitar mensaje duplicado por def auto 'colaborativa'

                      pe_evaluations_where_has_to_define_questions_pending.each_with_index do |pe_eval_pend, index_pend|
                        num_pending_define[index_pend] += num_pend if pe_eval_pend.id == pe_eval.id
                      end

                    else

                      pe_evaluations_where_has_to_define_questions_pending.push pe_eval
                      num_pending_define.push num_pend

                    end

                  end

                end


              end

            end

          end

        end

      end

      if has_to_validate_defined_questions

        pe_evaluations_where_has_to_validate_defined_questions.each do |pe_eval|

          (1..pe_eval.num_steps_definition_by_users_validated_before_accepted).each do |num_validation_step|

            if pe_eval.is_pe_definition_by_user_validator?(@user, num_validation_step, true)

              num_pend = 0

              pe_eval.pe_definition_by_user_validators_by_validator_ordered_by_user(@user, num_validation_step, true).each_with_index do |pe_definition_by_user_validator|

                unless pe_definition_by_user_validator.pe_member.removed

                  status_validate = pe_eval.pe_member_status_to_be_validated(pe_definition_by_user_validator.pe_member, num_validation_step, true)

                  if status_validate == 'not_ready'
                    num_pend += 1
                  elsif status_validate == 'ready'
                    num_pend += 1
                  elsif status_validate == 'not_validated'
                    num_pend += 1
                  end
                end

              end

              if num_pend > 0

                pe_evaluations_where_has_to_validate_defined_questions_pending.push pe_eval
                num_pending_validate.push num_pend
                num_pending_validate_alias.push pe_eval.alias_step(num_validation_step, true)


              end

            end

          end

          (1..pe_eval.num_steps_definition_by_users_validated_after_accepted).each do |num_validation_step|

            if pe_eval.is_pe_definition_by_user_validator?(@user, num_validation_step, false)

              num_pend = 0

              pe_eval.pe_definition_by_user_validators_by_validator_ordered_by_user(@user, num_validation_step, false).each_with_index do |pe_definition_by_user_validator|

                unless pe_definition_by_user_validator.pe_member.removed

                  status_validate = pe_eval.pe_member_status_to_be_validated(pe_definition_by_user_validator.pe_member, num_validation_step, false)

                  if status_validate == 'not_ready'
                    num_pend += 1
                  elsif status_validate == 'ready'
                    num_pend += 1
                  elsif status_validate == 'not_validated'
                    num_pend += 1
                  end

                end

              end

              if num_pend > 0

                pe_evaluations_where_has_to_validate_defined_questions_pending.push pe_eval
                num_pending_validate.push num_pend
                num_pending_validate_alias.push pe_eval.alias_step(num_validation_step, false)


              end


            end


          end


        end


      end


      pe_evaluations_where_has_to_track = pe_process.pe_evaluations_where_has_to_track @user
      has_to_track = true if pe_process.has_step_tracking? && pe_evaluations_where_has_to_track.size > 0

      if pe_process.has_step_assessment? && pe_process.is_a_pe_member_evaluator?(@user)

        num_missing_assessments = 0
        pe_process.pe_members.where('user_id = ?', @user.id).each do |pe_member|
          num_missing_assessments += pe_member.num_missing_assessments
        end

        has_to_assess = true if num_missing_assessments > 0

      end

      has_to_validate = true if pe_process.has_step_validation? && pe_process.is_a_validator?(@user)

      has_to_calibrate = true if pe_process.has_step_calibration? && pe_process.is_a_pe_cal_committee?(@user)

      has_to_provide_feedback = true if pe_process.has_step_feedback? && pe_process.is_a_feedback_provider?(@user) && pe_process.pe_members_to_feedback_pending(@user).size > 0

      has_to_accept_feedback = pe_process.has_to_accept_feedback_pending?(@user)

      if (has_to_define_questions && pe_evaluations_where_has_to_define_questions_pending.size > 0)|| has_to_accept_defined_questions || has_to_validate_defined_questions || has_to_assess || has_to_validate || has_to_calibrate || has_to_provide_feedback || has_to_accept_feedback

        @pe_processes.push pe_process

        if has_to_accept_defined_questions

          @pe_evaluations_to_accept = pe_evaluations_where_has_to_accept_defined_questions_pending

        end

        if has_to_define_questions

          @pe_evaluations_to_define = pe_evaluations_where_has_to_define_questions_pending
          @num_pending_define = num_pending_define

        end

        if has_to_validate_defined_questions

          @pe_evaluations_to_validate = pe_evaluations_where_has_to_validate_defined_questions_pending
          @num_pending_validate = num_pending_validate
          @num_pending_validate_alias = num_pending_validate_alias

        end

        num_pending_validate_alias

        if has_to_track

          pe_evaluations_where_has_to_track.each do |pe_evaluation|
            @pe_evaluations_to_track.push pe_evaluation
          end

        end

        if has_to_assess
          @pe_process_to_assess.push pe_process
        end

        if has_to_validate

          if pe_process.pe_member_rels.where('validator_id = ? and validated = 0', @user.id).count > 0

            @pe_process_to_validate.push pe_process

          end



        end

        if has_to_calibrate
          @pe_process_to_calibrate.push pe_process
        end

        if has_to_provide_feedback
          @pe_process_to_provide_feedback.push pe_process
        end

        if has_to_accept_feedback
          @pe_process_to_accept_feedback.push pe_process
        end

      end

    end


    @tracking_processes_pending_definition = Hash.new

    TrackingProcess.current_processes(@user.id).each do |tracking_process|

      pending_actions = Array.new
      tracking_form_participants = tracking_process.tracking_participants.
          where('tracking_participants.attendant_user_id = ? OR tracking_participants.subject_user_id = ?', @user.id, @user.id)

      tracking_form_participants.each do |tracking_participant|
        instance = tracking_participant.tracking_form_instances.last
        if action = instance.current_action(@user)
          pending_actions.append(action) unless pending_actions.any? && pending_actions.include?(action)
        end
      end

      if pending_actions.any?
        @tracking_processes_pending_definition[tracking_process] = pending_actions
      end

    end

  end

  def user
    @user
  end

  def first_owned_node
    @first_owned_node
  end

  def characteristics_mini_ficha
    @characteristics_mini_ficha
  end

  def members
    @members
  end

  def mpi_view_users
    @mpi_view_users
  end

  def notifications
    @notifications
  end

  def banners
    @banners
  end

  def poll_processes_active_answer
    @poll_processes_active_answer
  end

  def poll_processes_active_report
    @poll_processes_active_report
  end

  def sel_processes_applicable
    @sel_processes_applicable
  end

  def elec_process_active_answer
    @elec_process_active_answer
  end

  def elec_process_to_see_live_results_extra_time
    @elec_process_to_see_live_results_extra_time
  end

  def elec_process_to_see_final_results_extra_time
    @elec_process_to_see_final_results_extra_time
  end

  def pending_user_courses
    @pending_user_courses
  end

  def pending_button_states
    @pending_button_states
  end

  def dnc_cua_pendientes
    @dnc_cua_pendientes
  end

  def dnc_pp_pendientes
    @dnc_pp_pendientes
  end

  def dnc_estado_pendientes
    @dnc_estado_pendientes
  end

  def pace_cua_as_pendientes
    @pace_cua_as_pendientes
  end

  def pace_pp_as_pendientes
    @pace_pp_as_pendientes
  end

  def pace_estado_as_pendientes
    @pace_estado_as_pendientes
  end

  def pace_cua_um_pendientes
    @pace_cua_um_pendientes
  end

  def pace_pp_um_pendientes
    @pace_pp_um_pendientes
  end

  def pe_evaluations_to_define
    return @pe_evaluations_to_define, @num_pending_define
  end

  def pe_evaluations_to_accept
    @pe_evaluations_to_accept
  end

  def pe_evaluations_to_validate
    return @pe_evaluations_to_validate, @num_pending_validate, @num_pending_validate_alias

  end

  def pe_evaluations_to_track
    @pe_evaluations_to_track
  end

  def pe_process_to_assess
    @pe_process_to_assess
  end

  def pe_process_to_validate
    @pe_process_to_validate
  end

  def pe_process_to_calibrate
    @pe_process_to_calibrate
  end

  def pe_process_to_provide_feedback
    @pe_process_to_provide_feedback
  end

  def pe_process_to_accept_feedback
    @pe_process_to_accept_feedback
  end

  def pe_processes
    @pe_processes
  end

  def tracking_processes_pending_definition
    @tracking_processes_pending_definition
  end

  def ct_module_mpi
    @ct_module_mpi
  end

  def ct_module_lms
    @ct_module_lms
  end

  def ct_module_pe
    @ct_module_pe
  end

  def ct_module_trkn
    @ct_module_trkn
  end

  def ct_menu_tr
    @ct_menu_tr
  end

  def ct_menu_pe
    @ct_menu_pe
  end

  def tracking_processes
    @tracking_processes
  end

end