# == Schema Information
#
# Table name: sel_applicant_specific_answers
#
#  id                             :integer          not null, primary key
#  sel_applicant_id               :integer
#  sel_apply_specific_question_id :integer
#  answer                         :text
#  registered_at                  :datetime
#  registered_by_user_id          :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  attachment_code                :string(255)
#  attachment_name                :string(255)
#

class SelApplicantSpecificAnswer < ActiveRecord::Base
  belongs_to :sel_applicant
  belongs_to :sel_apply_specific_question
  belongs_to :registered_by_user, :class_name => 'User'
  attr_accessible :answer, :registered_at, :registered_by_user_id, :sel_apply_specific_question_id, :attachment_code, :attachment_name
end
