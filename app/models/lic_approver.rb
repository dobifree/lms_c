# == Schema Information
#
# Table name: lic_approvers
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class LicApprover < ActiveRecord::Base
  belongs_to :user
  has_many :lic_user_to_approves
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :user_id,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id


  validates :user_id, uniqueness: true
  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def lic_user_to_approves
    LicUserToApprove.where(lic_approver_id: id)
  end

  def any_approvee?
    rel = active_approvees
    return nil unless rel
    rel.size > 0
  end

  def active_approvees
    return nil unless lic_user_to_approves
    lic_user_to_approves.where(active: true)
  end

  def self.request_approvers(request_id)
    request = LicRequest.find(request_id)
    approvees = LicUserToApprove.where(user_id: request.lic_event.user_id, active: true)
    approvers = []
    approvees.each { |approvee| approvers << approvee.lic_approver if approvee.lic_approver.active }
    approvers
  end

  def self.user_approvers(user_id)
    approvees = LicUserToApprove.where(user_id: user_id,  active: true)
    approvers = []
    approvees.each { |approvee| approvers << approvee.lic_approver if approvee.lic_approver.active }
    approvers
  end

  def users_to_approve
    lic_users = active_approvees
    users = []
    lic_users.each { |lic_user| users << lic_user.user }
    return users
  end

  def requests
    users = users_to_approve
    requests = []
    users.each do |user|
      requests_users = LicRequest.where(active: true).all
      requests_users.each do |request_user|
        next unless request_user.lic_event_id
        next unless request_user.lic_event.user_id == user.id
        next if request_user.bp_license.partially_taken
        next unless request_user && (request_user.pending? || request_user.approved?)
        requests << request_user
      end
    end
    requests
  end

  def self.approver_requests(user_id)
    approver = LicApprover.where(user_id: user_id, active: true).first
    requests = approver ? approver.requests : []
    return requests
  end

  def active_approvees_user
    return nil unless lic_user_to_approves
    lic_user_to_approves.where(active: true).joins(:user).where(users: {activo: true})
  end

  def active_approvees_id
    array = active_approvees_user
    array_2 = []
    array.each {|var| array_2 << var.id}
    array_2
  end

  def active_approvees_user_id
    array = active_approvees_user
    array_2 = []
    array.each {|var| array_2 << var.user_id}
    array_2
  end

  def self.active_approvers_user
    LicApprover.where(active: true).joins(:user).where(users: {activo: true})
  end

end
