# == Schema Information
#
# Table name: sel_req_options
#
#  id              :integer          not null, primary key
#  sel_req_item_id :integer
#  name            :string(255)
#  description     :text
#  position        :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class SelReqOption < ActiveRecord::Base
  belongs_to :sel_req_item
  has_many :sel_requirement_values, :dependent => :destroy
  attr_accessible :description, :name, :position

  default_scope order('position')
end
