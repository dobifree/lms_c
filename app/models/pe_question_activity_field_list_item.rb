# == Schema Information
#
# Table name: pe_question_activity_field_list_items
#
#  id                                 :integer          not null, primary key
#  pe_question_activity_field_list_id :integer
#  description                        :string(255)
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#

class PeQuestionActivityFieldListItem < ActiveRecord::Base
  belongs_to :pe_question_activity_field_list

  attr_accessible :description, :pe_question_activity_field_list_id

  validates :description, presence: true, uniqueness: { case_sensitive: false, scope: :pe_question_activity_field_list_id }
  validates :pe_question_activity_field_list_id, presence: true

  default_scope order('description ASC')

end
