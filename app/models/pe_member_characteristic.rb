# == Schema Information
#
# Table name: pe_member_characteristics
#
#  id                :integer          not null, primary key
#  pe_member_id      :integer
#  pe_process_id     :integer
#  characteristic_id :integer
#  value             :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PeMemberCharacteristic < ActiveRecord::Base

  belongs_to :pe_member
  belongs_to :pe_process
  belongs_to :characteristic

  attr_accessible :value

  validates :pe_member_id, uniqueness: {scope: :characteristic_id}

end
