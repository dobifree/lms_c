# == Schema Information
#
# Table name: pe_member_accepted_feedbacks
#
#  id                            :integer          not null, primary key
#  pe_member_id                  :integer
#  pe_process_id                 :integer
#  pe_feedback_accepted_field_id :integer
#  comment                       :text
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  list_item_id                  :integer
#

class PeMemberAcceptedFeedback < ActiveRecord::Base

  belongs_to :pe_member
  belongs_to :pe_process
  belongs_to :pe_feedback_accepted_field
  belongs_to :pe_feedback_field_list_item, foreign_key: 'list_item_id'

  attr_accessible :comment, :list_item_id

  validates :comment, presence: true, :if => lambda { |pe_member_accepted_feedback| pe_member_accepted_feedback.pe_feedback_accepted_field.required}

  def formatted_comment

    if self.pe_feedback_accepted_field.field_type == 0

      self.comment ? self.comment.html_safe : ''

    elsif self.pe_feedback_accepted_field.field_type == 2

      self.pe_feedback_field_list_item ? self.pe_feedback_field_list_item.description : ''

    end

  end


end
