# == Schema Information
#
# Table name: tracking_participants
#
#  id                  :integer          not null, primary key
#  tracking_process_id :integer
#  attendant_user_id   :integer
#  subject_user_id     :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  in_redefinition     :boolean          default(FALSE)
#

class TrackingParticipant < ActiveRecord::Base
  belongs_to :tracking_process
  belongs_to :attendant_user, :class_name => 'User'
  belongs_to :subject_user, :class_name => 'User'
  has_many :tracking_form_instances, :dependent => :destroy
  attr_accessible :attendant_user_id, :subject_user_id, :in_redefinition, :tracking_process_id

  default_scope includes(:subject_user).order('users.apellidos ASC')

  validates :attendant_user_id, :presence => true
  validates :subject_user_id, :presence => true
  validates :subject_user_id, :uniqueness => {:scope => :tracking_process_id}

  def done?
    !self.in_redefinition? && self.complete?
  end

  def complete?
    self.tracking_form_instances.where(:done => true).count == self.tracking_process.tracking_forms.count
  end

  def traceable?
    !self.in_redefinition? && self.done? && self.tracking_form_instances.joins(:tracking_form_answers => :tracking_form_due_date).any?
  end

  def data_for_tracking_list(user_connected)
    builder = []
    filler = []
    counterpart = []

    self.tracking_form_instances.each do |t_f_instance|
      if t_f_instance.tracking_form.is_traceable?

        builder_aux = []
        filler_aux = []
        counterpart_aux = []

        builder_aux, filler_aux, counterpart_aux = t_f_instance.data_for_tracking_form(user_connected)

        builder = builder + builder_aux
        filler = filler + filler_aux
        counterpart = counterpart + counterpart_aux

      end
    end

    return builder, filler, counterpart

  end

  def current_form
    done_form_ids = self.tracking_form_instances.where(:done => true).pluck(:tracking_form_id)

    if done_form_ids.count == 0
      form = self.tracking_process.tracking_forms.first
    else
      form = self.tracking_process.tracking_forms.where('tracking_forms.id NOT IN (?)', done_form_ids).first
    end

    return form

  end

  def last_registered_milestone
    self.tracking_form_instances.joins(:tracking_form_answers => {:tracking_form_due_date => :tracking_milestones}).select('tracking_milestones.*').last
  end
end
