# == Schema Information
#
# Table name: elec_votes
#
#  id                       :integer          not null, primary key
#  elec_round_id            :integer
#  elec_process_category_id :integer
#  user_id                  :integer
#  candidate_user_id        :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  comment                  :text
#  registered_at            :datetime
#

class ElecVote < ActiveRecord::Base
  belongs_to :elec_round
  belongs_to :elec_process_category
  belongs_to :user
  belongs_to :candidate_user, class_name: 'User'
  attr_accessible :candidate_user_id, :elec_round_id, :comment, :registered_at,
                  :elec_process_category_id, :user_id

  validates :candidate_user_id, :presence => true
end
