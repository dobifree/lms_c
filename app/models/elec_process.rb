# == Schema Information
#
# Table name: elec_processes
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  active      :boolean          default(FALSE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ElecProcess < ActiveRecord::Base

  has_many :elec_process_rounds
  has_many :elec_events
  has_many :elec_characteristics
  has_many :elec_process_categories

  attr_accessible :active, :description, :name

  validates :name, :presence => true


end
