# == Schema Information
#
# Table name: pe_dimensions
#
#  id            :integer          not null, primary key
#  dimension     :integer
#  name          :string(255)
#  pe_process_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class PeDimension < ActiveRecord::Base

  belongs_to :pe_process
  has_many :pe_evaluations #
  has_many :pe_dimension_groups #

  attr_accessible :dimension, :name

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_process_id }
  validates :pe_process_id, presence: true

  default_scope order('dimension ASC, name ASC')


end
