# == Schema Information
#
# Table name: medlic_processes
#
#  id                    :integer          not null, primary key
#  name                  :string(255)
#  description           :text
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class MedlicProcess < ActiveRecord::Base
  has_many :medlic_licenses

  attr_accessible :description,
                  :name

  validates :name, presence: true

  def deletable?; medlic_licenses.size == 0 end

  def la_licenses_reported_partially(except_report_id)
    licenses = []
    report = MedlicReport.only_partial.from_process(id).except_this(except_report_id).sort_by(&:registered_at).reverse.first
    return [] unless report
    report.medlic_reported_licenses.each {|r_license| licenses << r_license.medlic_license}
    licenses
  end

  def pending_paid_license
    licenses = []
    medlic_licenses.each { |license| licenses << license unless license.sent_to_payroll }
    licenses
  end

end
