# == Schema Information
#
# Table name: pe_feedback_field_lists
#
#  id            :integer          not null, primary key
#  pe_process_id :integer
#  name          :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class PeFeedbackFieldList < ActiveRecord::Base

  belongs_to :pe_process

  has_many :pe_feedback_field_list_items #
  has_many :pe_feedback_fields
  has_many :pe_feedback_compound_fields

  attr_accessible :name, :pe_process_id

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_process_id }

  default_scope order('name')

end
