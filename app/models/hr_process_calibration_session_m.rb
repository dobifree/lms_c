# == Schema Information
#
# Table name: hr_process_calibration_session_ms
#
#  id                                :integer          not null, primary key
#  hr_process_calibration_session_id :integer
#  hr_process_user_id                :integer
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#

class HrProcessCalibrationSessionM < ActiveRecord::Base

  belongs_to :hr_process_calibration_session
  belongs_to :hr_process_user

  has_many :hr_process_assessment_d_cals
  has_many :hr_process_assessment_qua_cals

  validates :hr_process_user_id, presence: true, uniqueness: {scope: :hr_process_calibration_session_id}

  attr_accessible :hr_process_calibration_session_id, :hr_process_user_id

end
