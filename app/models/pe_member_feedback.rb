# == Schema Information
#
# Table name: pe_member_feedbacks
#
#  id                           :integer          not null, primary key
#  pe_member_id                 :integer
#  pe_process_id                :integer
#  pe_feedback_field_id         :integer
#  comment                      :text
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  number_of_compound_feedbacks :integer
#  comment_date                 :date
#  list_item_id                 :integer
#

class PeMemberFeedback < ActiveRecord::Base

  belongs_to :pe_member
  belongs_to :pe_process
  belongs_to :pe_feedback_field
  belongs_to :pe_feedback_field_list_item, foreign_key: 'list_item_id'

  has_many :pe_member_compound_feedbacks, dependent: :destroy #

  attr_accessible :comment, :number_of_compound_feedbacks, :comment_date, :list_item_id

  #validates :comment, presence: true, :if => lambda { |pe_member_feedback| pe_member_feedback.pe_feedback_field.required}

  validates :comment, presence: true, :if => :validate_required_field

  def pe_member_compound_feedbacks_by_field(position, pe_feedback_compound_field)
    self.pe_member_compound_feedbacks.where('position = ? AND pe_feedback_compound_field_id = ?', position, pe_feedback_compound_field.id).first
  end
  
  def formatted_comment

    if self.pe_feedback_field.field_type == 0

      self.comment ? self.comment.html_safe : ''

    elsif self.pe_feedback_field.field_type == 1

      self.comment_date ? (I18n.localize self.comment_date, format: :full_date) : ''

    elsif self.pe_feedback_field.field_type == 2

      self.pe_feedback_field_list_item ? self.pe_feedback_field_list_item.description : ''

    end
    
  end

  private

  def validate_required_field

    pe_feedback_field = self.pe_feedback_field

    if pe_feedback_field.pe_feedback_field_list_item
      item_id = pe_feedback_field.pe_feedback_field_list_item_id

      pe_feedback_field_parent = pe_feedback_field.pe_feedback_field_list_item.pe_feedback_field_list.pe_feedback_fields.first

      if pe_feedback_field_parent

        pe_member_feedback_parent = self.pe_member.pe_member_feedbacks_by_field(pe_feedback_field_parent)

        if pe_member_feedback_parent

          if pe_feedback_field.list_item_selected
            if pe_member_feedback_parent.list_item_id == item_id
              return self.pe_feedback_field.required
            else
              return false
            end
          else
            if pe_member_feedback_parent.list_item_id == item_id
              return false
            else
              return self.pe_feedback_field.required
            end
          end
        end
      else
        return false
      end
    else
      return self.pe_feedback_field.required
    end
  end

end
