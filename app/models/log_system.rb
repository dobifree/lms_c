# == Schema Information
#
# Table name: log_systems
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  description        :text
#  registered_at      :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  params             :text
#  controller         :string(255)
#  action             :string(255)
#  request_raw        :text
#  session_raw        :text
#  server_name        :string(255)
#  remote_ip          :string(255)
#  remote_host        :string(255)
#  path_info          :string(255)
#  request_method     :string(255)
#  original_url       :string(255)
#  url_referer        :string(255)
#  request_xhr        :boolean          default(FALSE)
#  commit_id          :string(255)
#  commit_deployed_at :datetime
#

class LogSystem < ActiveRecord::Base
  belongs_to :user
  attr_accessible :description, :registered_at, :params
end
