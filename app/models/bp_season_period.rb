# == Schema Information
#
# Table name: bp_season_periods
#
#  id                      :integer          not null, primary key
#  bp_season_id            :integer
#  begin                   :date
#  end                     :date
#  deactivated_at          :datetime
#  deactivated_by_admin_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  since                   :datetime
#  until                   :datetime
#  registered_at           :datetime
#  registered_by_admin_id  :integer
#

class BpSeasonPeriod < ActiveRecord::Base
  belongs_to :bp_season
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id

  attr_accessible :bp_season_id,
                  :begin,
                  :end,
                  :since,
                  :until

  validates :bp_season_id, presence: true
  validates :begin, presence: true
  validates :end, presence: true
  validates :since, presence: true
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  validate :range_begin_end
  validate :since_before_until
  validate :range_since_concordance_1
  validate :range_since_concordance_2
  validate :range_until_concordance_1
  validate :overlap_range_since

  def deactivated_by_admin_formatted
    deactivated_by_admin.nombre if deactivated_by_admin_id
  end

  def active?(now)
    now = now.to_datetime
    return true if self.until && since < now && self.until > now
    return true if !self.until && now > since
    false
  end

  private

  def range_begin_end
    # return unless self.begin && self.end && self.begin > self.end
    # errors.add(:end, 'debe ser después del inicio')
  end

  def since_before_until
    return unless self.until && self.until < since
    errors.add(:until, 'debe ser después de Desde')
  end

  def range_since_concordance_1
    return unless bp_season_id
    return unless since
    return unless bp_season.since && since < bp_season.since
    errors.add(:since, 'debe ser mayor a "desde" de la temporada')
  end

  def range_since_concordance_2
    return unless bp_season_id
    return unless since
    return unless bp_season.since && bp_season.until && since > bp_season.until
    errors.add(:since, 'debe ser menor a "hasta" de la temporada')
  end

  def range_until_concordance_1
    return unless bp_season_id
    return unless self.until && bp_season.since && bp_season.until && self.until > bp_season.until
    errors.add(:until, 'debe ser menor a "hasta" de la temporada')
  end

  def overlap_range_since
    return if any_open_period?
    return unless bp_season_id
    return unless since
    error_message = 'se traslapa con otra asociación. '
    op = overlap_periods
    op.each do |period|
      error_message += 'Período '
      error_message += 'desde: ' + period.since.strftime('%d/%m/%Y %H:%M')
      error_message += ', hasta: ' + period.until.strftime('%d/%m/%Y %H:%M')
      error_message += '. '
    end
    errors.add(:since, error_message) unless op.empty?
  end

  def any_open_period?
    return false unless bp_season_id
    period = BpSeasonPeriod.where('until is null and bp_season_id = ?', bp_season_id).first
    return false unless period && !id
    return false unless period && id && period.id != id
    error_message = 'no puede definir habiendo una asociación sin cerrar para la misma temporada de inicio: '+period.since.strftime('%d/%m/%Y %H:%M')
    errors.add(:until, error_message)
    true
  end

  def overlap_periods
    overlap_periods = []
    periods = BpSeasonPeriod.where('until is not null and bp_season_id = ?', bp_season_id)
    periods.each do |period|
      if self.until
        next unless period.since < self.until && period.until > since
      else
        next unless period.until > since
      end
      next if id && period.id == id
      overlap_periods << period
    end
    return overlap_periods
  end
end
