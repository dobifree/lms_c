# == Schema Information
#
# Table name: hr_process_dimensions
#
#  id                     :integer          not null, primary key
#  nombre                 :string(255)
#  hr_process_template_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  orden                  :integer
#

class HrProcessDimension < ActiveRecord::Base

  belongs_to :hr_process_template
  has_many :hr_process_dimension_gs
  has_many :hr_process_dimension_es

  attr_accessible :nombre, :orden, :hr_process_template_id

  validates :nombre, presence: true, uniqueness: { case_sensitive: false, scope: :hr_process_template_id }
  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('orden ASC')

  def eje
    if self.orden == 1
      'X'
    elsif self.orden == 2
      'Y'
    end
  end

end
