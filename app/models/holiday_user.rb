# == Schema Information
#
# Table name: holiday_users
#
#  id                     :integer          not null, primary key
#  holiday_id             :integer
#  user_id                :integer
#  manually_assoc         :boolean          default(FALSE)
#  active                 :boolean          default(TRUE)
#  deactivated_manually   :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  prev_hol_user_id       :integer
#

class HolidayUser < ActiveRecord::Base
  belongs_to :holiday
  belongs_to :user
  belongs_to :prev_hol_user, class_name: 'HolidayUser', foreign_key: :prev_hol_user_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :holiday_id,
                  :user_id,
                  :active,
                  :prev_hol_user_id,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :deactivated_manually,
                  :manually_assoc,
                  :registered_at,
                  :registered_by_user_id

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def self.active; where(active: true) end
  def self.by_user(this_user_id); where(user_id: this_user_id) end

end
