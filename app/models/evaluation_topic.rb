# == Schema Information
#
# Table name: evaluation_topics
#
#  id                         :integer          not null, primary key
#  evaluation_id              :integer
#  master_evaluation_topic_id :integer
#  position                   :integer
#  number_of_questions        :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class EvaluationTopic < ActiveRecord::Base

  belongs_to :evaluation
  belongs_to :master_evaluation_topic

  attr_accessible :number_of_questions, :position

  validates :number_of_questions, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('position ASC')

end
