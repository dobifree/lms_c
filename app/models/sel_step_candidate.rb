# == Schema Information
#
# Table name: sel_step_candidates
#
#  id                     :integer          not null, primary key
#  jm_candidate_id        :integer
#  sel_process_step_id    :integer
#  exonerated             :boolean          default(FALSE)
#  done                   :boolean          default(FALSE)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  exonerated_at          :datetime
#  exonerated_by_user_id  :integer
#  done_at                :datetime
#  done_by_user_id        :integer
#  sel_appointment_id     :integer
#  exonerated_comment     :text
#  final_appointment_date :datetime
#  copied_from_id         :integer
#

class SelStepCandidate < ActiveRecord::Base
  belongs_to :jm_candidate
  belongs_to :sel_process_step
  belongs_to :sel_appointment
  belongs_to :registered_by_user, class_name: 'User'
  belongs_to :exonerated_by_user, class_name: 'User'
  belongs_to :done_by_user, class_name: 'User'
  belongs_to :copied_from, class_name: 'SelStepCandidate'
  has_many :sel_step_results, :dependent => :destroy
  has_many :sel_attendants, :dependent => :destroy
  has_many :sel_evaluation_attachments, :dependent => :destroy
  accepts_nested_attributes_for :sel_evaluation_attachments, :reject_if => lambda { |a| a[:description].blank? }, :allow_destroy => true


  attr_accessible :done, :done_at, :done_by_user_id, :exonerated,
                  :exonerated_comment, :exonerated_at, :exonerated_by_user_id,
                  :registered_at, :registered_by_user_id, :jm_candidate,
                  :jm_candidate_id, :sel_process_step, :sel_process_step_id,
                  :copied_from_id, :final_appointment_date,
                  :sel_step_results,
                  :sel_attendants,
                  :sel_evaluation_attachments_attributes


  def prev_sel_step_candidates
    SelStepCandidate.joins(:sel_process_step, :sel_step_results).where('sel_step_candidates.id <> ?', id).where(done: true, exonerated: false,jm_candidate_id: jm_candidate_id, sel_process_steps: {sel_step_id: sel_process_step.sel_step_id}).uniq
  end

end
