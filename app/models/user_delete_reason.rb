# == Schema Information
#
# Table name: user_delete_reasons
#
#  id          :integer          not null, primary key
#  position    :integer
#  description :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class UserDeleteReason < ActiveRecord::Base


  attr_accessible :description, :position

  default_scope order('position, description')

end
