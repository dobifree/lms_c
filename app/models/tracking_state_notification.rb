# == Schema Information
#
# Table name: tracking_state_notifications
#
#  id                     :integer          not null, primary key
#  tracking_form_state_id :integer
#  name                   :string(255)
#  description            :text
#  active                 :boolean
#  to                     :string(255)
#  cc                     :string(255)
#  subject                :string(255)
#  body                   :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class TrackingStateNotification < ActiveRecord::Base
  belongs_to :tracking_form_state
  attr_accessible :active, :body, :cc, :description, :name, :subject, :to

  validates :name, :presence => true
  validates :to, :presence => true
  validates :subject, :presence => true
  validates :body, :presence => true
end
