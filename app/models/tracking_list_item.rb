# == Schema Information
#
# Table name: tracking_list_items
#
#  id                           :integer          not null, primary key
#  tracking_list_id             :integer
#  position                     :integer
#  name                         :string(255)
#  description                  :text
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  active                       :boolean          default(TRUE)
#  tracking_list_item_father_id :integer
#

class TrackingListItem < ActiveRecord::Base
  belongs_to :tracking_list
  belongs_to :tracking_list_item_father, :class_name => 'TrackingListItem'
  has_many :tracking_list_item_children, :foreign_key => :tracking_list_item_father_id,  :class_name => 'TrackingListItem'
  attr_accessible :description, :name, :position, :active, :tracking_list_item_father_id

  validates :position, :presence => true, :numericality => true
  validates :name, :presence => true

  default_scope { order(:position, :name) }

end
