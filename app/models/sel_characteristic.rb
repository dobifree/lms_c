# == Schema Information
#
# Table name: sel_characteristics
#
#  id              :integer          not null, primary key
#  sel_template_id :integer
#  name            :string(255)
#  description     :text
#  option_type     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  configurable    :boolean          default(FALSE)
#

class SelCharacteristic < ActiveRecord::Base
  belongs_to :sel_template
  has_many :sel_options, :dependent => :destroy, :inverse_of => :sel_characteristic
  has_many :sel_steps, through: :sel_step_characteristics
  has_many :sel_process_characteristics
  accepts_nested_attributes_for :sel_options, :allow_destroy => true

  attr_accessible :description, :name, :option_type, :configurable,
                  :sel_template, :sel_template_id,
                  :sel_options, :sel_options_attributes,
                  :sel_steps

  validates :name, :presence => true
  validates :option_type, :presence => true

  default_scope order('name ASC')

  def self.alternatives_option_type
    [:discretas, :de_rango]
  end
end
