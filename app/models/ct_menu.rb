# == Schema Information
#
# Table name: ct_menus
#
#  id              :integer          not null, primary key
#  position        :integer
#  cod             :string(255)
#  name            :string(255)
#  user_menu_alias :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class CtMenu < ActiveRecord::Base

  attr_accessible :cod, :name, :position, :user_menu_alias

  validates :position, numericality: { only_integer: true, greater_than: 0 }
  validates :user_menu_alias, presence: true

  default_scope order('position')

end
