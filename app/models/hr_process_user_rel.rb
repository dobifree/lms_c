# == Schema Information
#
# Table name: hr_process_user_rels
#
#  id                  :integer          not null, primary key
#  hr_process_user1_id :integer
#  tipo                :string(255)
#  hr_process_user2_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  hr_process_id       :integer
#  finalizada          :boolean          default(FALSE)
#  no_aplica           :boolean          default(FALSE)
#

class HrProcessUserRel < ActiveRecord::Base


  belongs_to :hr_process
  belongs_to :hr_process_user1, class_name: 'HrProcessUser' #evalua
  belongs_to :hr_process_user2, class_name: 'HrProcessUser' #es evaluado

  attr_accessible :hr_process_user1_id, :tipo, :hr_process_user2_id, :hr_process_id,
                  :finalizada, :no_aplica

  #tipo: jefe, sub, par, resp, cli, prov
  #el resp es el responsable, no evalua solo consulta
  validates :tipo, presence: true, uniqueness: { case_sensitive: false, scope: [:hr_process_id, :hr_process_user1_id, :hr_process_user2_id] }

end
