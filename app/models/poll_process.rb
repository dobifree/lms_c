# == Schema Information
#
# Table name: poll_processes
#
#  id                        :integer          not null, primary key
#  name                      :string(255)
#  description               :text
#  active                    :boolean
#  from_date                 :datetime
#  to_date                   :datetime
#  master_poll_id            :integer
#  finished                  :boolean
#  finished_at               :datetime
#  finished_by_user_id       :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  show_preview_results_user :boolean          default(FALSE)
#  to_everyone               :boolean          default(FALSE)
#  allow_pause               :boolean          default(FALSE)
#  grid_view                 :boolean          default(FALSE)
#  flow                      :integer          default(0)
#  show_group_names          :boolean          default(FALSE)
#

class PollProcess < ActiveRecord::Base
  belongs_to :master_poll
  belongs_to :finished_by_user, class_name: 'Admin' #, foreign_key: "antes_de_unit_id"
  has_many :poll_process_users, :dependent => :destroy
  has_many :poll_process_characteristics, :dependent => :destroy
  has_many :characteristics, through: :poll_process_characteristics
  has_many :poll_process_user_answers, through: :poll_process_users
  has_many :poll_process_report_chars, :dependent => :destroy

  accepts_nested_attributes_for :poll_process_report_chars, :allow_destroy => true

  attr_accessible :active, :description, :finished, :finished_at,
                  :finished_by_user_id, :from_date, :name, :to_date, :to_everyone, :allow_pause,
                  :master_poll_id, :show_preview_results_user, :grid_view, :show_group_names, :flow,
                  :poll_process_users,
                  :poll_process_user_answers,
                  :poll_process_report_chars_attributes

  validates :name, presence: true
  validates :description, presence: true
  validates :master_poll_id, presence: true
  validates :from_date, presence: true
  validates :to_date, presence: true
  validates :flow, presence: true

  before_save { self.finished = nil if self.finished.blank? }
  before_save { self.finished_at = nil if self.finished_at.blank? }
  before_save { self.finished_by_user_id = nil if self.finished_by_user_id.blank? }
  before_save :correct_to_date

  scope :active, where(:finished => nil, :active => true)
  scope :in_time, ->(lms_time) { where('? between from_date AND to_date', "#{lms_time}%") }


  def self.flow_options
    ['Sábana', 'Por grupos']
  end

  def self.actives_for_user_to_answer(user_id, lms_time)

    list_access_user(user_id, lms_time, false)

  end

  def self.actives_for_user_view_results(user_id, lms_time)

    list_access_user(user_id, lms_time, true)

  end


  def correct_to_date
    #self.to_date = self.to_date+((24*60*60)-1).seconds if self.to_date
    self.to_date = self.to_date.midnight+((24*60*60)-1).seconds if self.to_date
  end

  def configured_to_everyone?
    self.to_everyone
  end

  def configured_to_group?

    self.poll_process_users.where(:enrolled => true).any?
  end

  def configured_to_chars?
    self.characteristics.any?
  end

  def configured_visibility?

    (configured_to_everyone? || configured_to_group? || configured_to_chars?) ? true : false

  end

  private

  def self.list_access_user(user_id, lms_time, done)

    poll_processes_filtered = []

    if done == true #mostrar resultados. Todas las encuestas 'activas' contestadas por el usuario y que muestran resultados
      PollProcess.in_time(lms_time).active.joins(:poll_process_users).where(:show_preview_results_user => true,poll_process_users: {:user_id => user_id, :done => done}).each do |poll_process|
        poll_process_user = PollProcessUser.where(:user_id => user_id, :poll_process_id => poll_process.id).first
        #primer grupo: para todos, y ya la ha contestado (se filtra en el join where de arriba)
        if poll_process.to_everyone == true ||
            #segundo grupo: enrolado, y ya la ha contestado (se filtra en el join where de arriba)
            (poll_process_user.enrolled == true && poll_process_user.done == true) ||
            #tercer grupo: match de características, y ya la ha contestado (se filtra en el join where de arriba)
            (has_access_by_characteristics?(poll_process, user_id) && (poll_process_user.done == true))
          poll_processes_filtered.push(poll_process)
        end
      end
    else #para responder
      PollProcess.in_time(lms_time).active.each do |poll_process|
        poll_process_user = PollProcessUser.where(:user_id => user_id, :poll_process_id => poll_process.id).first
        #primer grupo: para todos, pero no la ha contestado
        if (poll_process.to_everyone == true && !(poll_process_user ? poll_process_user.done : false)) ||
            #segundo grupo: enrolado, pero no la ha contestado
            (poll_process_user && poll_process_user.enrolled == true && poll_process_user.done == false) ||
            #tercer grupo: match de características, pero no la ha contestado
            (has_access_by_characteristics?(poll_process, user_id) && !(poll_process_user ? poll_process_user.done : false))
          poll_processes_filtered.push(poll_process)
        end
      end
    end


    return poll_processes_filtered

  end

  def self.has_access_by_characteristics?(poll_process, user_id)
    match = false
    poll_process.poll_process_characteristics.each do |poll_char_value|
      if UserCharacteristic.where(:user_id => user_id, :characteristic_id => poll_char_value.characteristic_id, :valor => poll_char_value.match_value).count > 0
        match = true
      end
      break if match
    end
    match
  end

  def self.filter_by_characteristics(actives_poll_process_no_filter, user_id)
    poll_process_to_show = []

    user_characteristics = UserCharacteristic.find_all_by_user_id(user_id)

    actives_poll_process_no_filter.each do |poll_process|
      poll_process_characteristics = PollProcessCharacteristic.find_all_by_poll_process_id(poll_process.id)
      #solo se filtra si el poll process tiene alguna caracteristica asociada
      if poll_process_characteristics.count > 0
        poll_process_characteristics.each do |poll_process_characteristic|
          user_characteristics.each do |user_characteristic|
            if poll_process_characteristic.match_value.to_s == user_characteristic.valor.to_s
              poll_process_to_show.push(poll_process) unless poll_process_to_show.include?(poll_process)
              break
            end
          end
        end
      else
        #si no hay caracteristicas asociadas al poll process, se muestra nomas
        poll_process_to_show.push(poll_process) unless poll_process_to_show.include?(poll_process)
      end
    end


    return poll_process_to_show

  end

end
