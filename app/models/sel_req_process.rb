# == Schema Information
#
# Table name: sel_req_processes
#
#  id                    :integer          not null, primary key
#  sel_requirement_id    :integer
#  registered_at         :datetime
#  registered_by_user_id :integer
#  active                :boolean
#  started               :boolean
#  started_at            :datetime
#  started_by_user_id    :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class SelReqProcess < ActiveRecord::Base
  belongs_to :sel_requirement
  belongs_to :registered_by_user, class_name: 'User'
  belongs_to :started_by_user, class_name: 'User'
  has_many :sel_req_approvers, dependent: :destroy
  accepts_nested_attributes_for :sel_req_approvers, allow_destroy: true

  attr_accessible :sel_req_approvers_attributes

  scope :assigned_to, ->(user_id) { joins(:sel_req_approvers).where(sel_req_approvers: {user_id: user_id}, active: true, started: true) }

  def value_to_approval
    100
  end

  def approval_pctg_fixed
    total_approved_value = 0
    total_to_eval_required_value = 0
    sel_req_approvers.each do |approver|
      if approver.sel_req_approver_eval
        total_approved_value += approver.value if approver.sel_req_approver_eval.evaluation
      else
        total_to_eval_required_value += approver.value if approver.mandatory?
      end
    end
    value_to_approval_with_mandatory = total_approved_value + total_to_eval_required_value
    value_to_approval_fixed = value_to_approval_with_mandatory > value_to_approval ? value_to_approval_with_mandatory : value_to_approval
    (100 * (total_approved_value.to_f / value_to_approval_fixed.to_f)).to_i
  end

  def total_config_value_approvers
    total = 0
    sel_req_approvers.each do |approver|
      total += approver.value
    end
    return total
  end

  def self.pending_sel_req_processes_to(user_id)
    assigned_to(user_id).joins('LEFT OUTER JOIN sel_req_approver_evals ON sel_req_approvers.id = sel_req_approver_evals.sel_req_approver_id').where('sel_req_approver_evals.id IS NULL')
  end

end
