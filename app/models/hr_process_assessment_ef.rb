# == Schema Information
#
# Table name: hr_process_assessment_efs
#
#  id                       :integer          not null, primary key
#  hr_process_evaluation_id :integer
#  hr_process_user_id       :integer
#  resultado_puntos         :float
#  resultado_porcentaje     :float
#  fecha                    :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  registered_by_manager    :boolean          default(FALSE)
#

class HrProcessAssessmentEf < ActiveRecord::Base

  belongs_to :hr_process_evaluation
  belongs_to :hr_process_user

  attr_accessible :fecha, :resultado_porcentaje, :resultado_puntos, :registered_by_manager

  validates :resultado_porcentaje, presence: true, numericality: true
  validates :resultado_puntos, presence: true, numericality: true


end
