# == Schema Information
#
# Table name: ben_custom_reports
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BenCustomReport < ActiveRecord::Base
  has_many :ben_custom_report_characteristics, dependent: :destroy
  has_many :ben_custom_report_attributes, dependent: :destroy
  has_many :ben_user_custom_reports, dependent: :destroy

  accepts_nested_attributes_for :ben_custom_report_characteristics, allow_destroy: true
  accepts_nested_attributes_for :ben_custom_report_attributes, allow_destroy: true


  attr_accessible :name,
                  :ben_custom_report_characteristics_attributes,
                  :ben_custom_report_attributes_attributes

  validates :name, presence: true
end
