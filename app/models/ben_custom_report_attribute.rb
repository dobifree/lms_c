# == Schema Information
#
# Table name: ben_custom_report_attributes
#
#  id                   :integer          not null, primary key
#  position             :integer
#  name                 :string(255)
#  active               :boolean          default(FALSE)
#  ben_custom_report_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class BenCustomReportAttribute < ActiveRecord::Base
  belongs_to :ben_custom_report
  attr_accessible :name,
                  :position,
                  :ben_custom_report_id,
                  :active

  validates :name, presence: true
  validates :position, presence:true, if: :active?
  #validates :position, uniqueness: { scope: :active }, if: :active?

end
