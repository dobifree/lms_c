# == Schema Information
#
# Table name: pe_assessment_dimensions
#
#  id                    :integer          not null, primary key
#  pe_process_id         :integer
#  pe_member_id          :integer
#  pe_dimension_id       :integer
#  pe_dimension_group_id :integer
#  percentage            :float
#  last_update           :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  pe_area_id            :integer
#

class PeAssessmentDimension < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_member
  belongs_to :pe_area
  belongs_to :pe_dimension
  belongs_to :pe_dimension_group

  attr_accessible :last_update, :percentage

  validates :percentage, presence: true, numericality: true

  validates :pe_dimension_id, presence: true, uniqueness: {scope: [:pe_member_id, :pe_area_id]}


end
