# == Schema Information
#
# Table name: pe_cal_sessions
#
#  id               :integer          not null, primary key
#  pe_process_id    :integer
#  description      :text
#  from_date        :datetime
#  to_date          :datetime
#  active           :boolean          default(TRUE)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  pe_evaluation_id :integer
#

class PeCalSession < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_evaluation

  has_many :pe_cal_committees, dependent: :destroy #
  has_many :pe_cal_members, dependent: :destroy #

  attr_accessible :active, :description, :from_date, :to_date, :pe_process_id, :pe_evaluation_id

  before_save :correct_to_date

  validates :description, presence: true
  validates :from_date, presence: true
  validates :to_date, presence: true

  default_scope order('description')

  def number_of_members
    self.pe_cal_members.size
  end

  def number_of_calibrated_members

    if self.pe_evaluation
      self.pe_cal_members.joins(pe_member: :pe_assessment_final_evaluations).where('pe_assessment_final_evaluations.pe_evaluation_id = ? and original_percentage is not null', self.pe_evaluation_id).count
    else
      self.pe_cal_members.joins(:pe_member).where('step_calibration = ?', true).count
    end

  end

  def pe_cal_members_ordered
    self.pe_cal_members.joins(pe_member: :user).order('apellidos, nombre')
  end

  def pe_cal_committees_ordered
    self.pe_cal_committees.joins(:user).reorder('pe_cal_committees.manager DESC, apellidos, nombre')
  end

  def pe_cal_committees_manager_ordered
    self.pe_cal_committees.joins(:user).where('pe_cal_committees.manager = ?', true).reorder('apellidos, nombre')
  end

  def correct_to_date
    self.to_date = self.to_date+((24*60*60)-1).seconds if self.to_date
  end

  def is_pe_cal_committee?(user)
    self.pe_cal_committees.where('user_id = ?', user.id).count > 0 ? true : false
  end

  def is_pe_cal_committee_manager?(user)
    self.pe_cal_committees.where('user_id = ? AND manager = ?', user.id, true).count > 0 ? true : false
  end

end
