# == Schema Information
#
# Table name: area_superintendents
#
#  id                               :integer          not null, primary key
#  planning_process_company_unit_id :integer
#  company_unit_area_id             :integer
#  user_id                          :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#

class AreaSuperintendent < ActiveRecord::Base

  belongs_to :planning_process_company_unit
  belongs_to :company_unit_area
  belongs_to :user

  attr_accessible :planning_process_company_unit_id, :company_unit_area_id, :user_id

  validates :planning_process_company_unit_id, presence: true
  validates :company_unit_area_id, presence: true
  validates :user_id, presence: true, uniqueness: {scope: :company_unit_area_id}

  validate :unit_area

  def unit_area
    if self.company_unit_area.company_unit_id != planning_process_company_unit.company_unit_id
      errors.add(:mismatch_unit_area, 'El area debe pertenecer al mismo nivel')
    end
  end

end
