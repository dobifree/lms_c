# == Schema Information
#
# Table name: pe_evaluations
#
#  id                                                            :integer          not null, primary key
#  position                                                      :integer
#  name                                                          :string(255)
#  description                                                   :text
#  weight                                                        :integer
#  entered_by_manager                                            :boolean
#  pe_process_id                                                 :integer
#  pe_dimension_id                                               :integer
#  created_at                                                    :datetime         not null
#  updated_at                                                    :datetime         not null
#  points_to_ohp                                                 :float            default(10.0)
#  max_per_cant_answer                                           :float            default(100.0)
#  max_percentage                                                :float            default(120.0)
#  min_percentage                                                :float            default(0.0)
#  pe_delegated_evaluation_id                                    :integer
#  assessment_layout                                             :integer          default(0)
#  allow_tracking                                                :boolean          default(FALSE)
#  final_evaluation_operation_evaluators                         :integer          default(0)
#  final_evaluation_operation_evaluations                        :integer          default(0)
#  step_assessment_show_auto_to_boss                             :boolean          default(FALSE)
#  allow_definition_by_users                                     :boolean          default(FALSE)
#  allow_definition_by_users_accepted                            :boolean          default(FALSE)
#  num_steps_definition_by_users_validated_before_accepted       :integer          default(0)
#  num_steps_definition_by_users_validated_after_accepted        :integer          default(0)
#  alias_steps_definition_by_users_validated_before_accepted     :string(255)
#  alias_steps_definition_by_users_validated_after_accepted      :string(255)
#  label_not_ready_definition_by_users                           :string(255)      default("not_ready|default")
#  label_ready_definition_by_users                               :string(255)      default("ready|success")
#  label_not_ready_definition_by_users_accepted                  :string(255)      default("not_ready|success")
#  label_ready_definition_by_users_accepted                      :string(255)      default("ready|success")
#  label_not_ready_definition_by_users_validated_before_accepted :string(255)      default("not_ready|primary")
#  label_ready_definition_by_users_validated_before_accepted     :string(255)      default("ready|success")
#  label_rejected_definition_by_users_validated_before_accepted  :string(255)      default("rejected|danger")
#  label_not_ready_definition_by_users_validated_after_accepted  :string(255)      default("not_ready|primary")
#  label_ready_definition_by_users_validated_after_accepted      :string(255)      default("ready|success")
#  label_rejected_definition_by_users_validated_after_accepted   :string(255)      default("rejected|danger")
#  allow_watching_own_tracking                                   :boolean          default(FALSE)
#  allow_watching_own_definition                                 :boolean          default(FALSE)
#  watch_my_definition_only_when_finished                        :boolean          default(TRUE)
#  send_email_to_accept_definition                               :boolean          default(FALSE)
#  email_to_accept_definition_subject                            :string(255)
#  email_to_accept_definition_content                            :text
#  entered_by_manager_questions                                  :boolean          default(FALSE)
#  lay_force_nobr_goal_length                                    :integer          default(100)
#  definition_by_users_rol                                       :string(255)
#  label_not_initiated_definition_by_users                       :string(255)      default("not_initiated|default")
#  allow_tracking_with_boss                                      :boolean          default(FALSE)
#  def_allow_individual_comments_by_boss                         :boolean          default(FALSE)
#  def_allow_watch_individual_comments_by_boss                   :boolean          default(FALSE)
#  def_allow_individual_comments_with_boss                       :boolean          default(FALSE)
#  definition_by_users_accept_message                            :string(255)      default("Confirmo que conozco y estoy de acuerdo con la definición")
#  track_allow_individual_comments_by_boss                       :boolean          default(FALSE)
#  track_allow_watch_individual_comments_by_boss                 :boolean          default(FALSE)
#  track_allow_individual_comments_with_boss                     :boolean          default(FALSE)
#  assess_allow_individual_comments_by_boss                      :boolean          default(FALSE)
#  assess_allow_watch_individual_comments_by_boss                :boolean          default(FALSE)
#  assess_allow_individual_comments_with_boss                    :boolean          default(FALSE)
#  qdefs_allow_individual_comments_by_boss                       :boolean          default(FALSE)
#  qdefs_allow_watch_individual_comments_by_boss                 :boolean          default(FALSE)
#  qdefs_allow_individual_comments_with_boss                     :boolean          default(FALSE)
#  def_allow_action_plan_by_boss                                 :boolean          default(FALSE)
#  def_allow_watch_action_plan_by_boss                           :boolean          default(FALSE)
#  def_allow_action_plan_with_boss                               :boolean          default(FALSE)
#  informative                                                   :boolean          default(FALSE)
#  display_in_qdefs                                              :boolean          default(FALSE)
#  display_weight_in_qdefs                                       :boolean          default(TRUE)
#  definition_by_user_finish_message                             :string(255)      default("Confirmo que he finalizado la definición")
#  definition_by_user_validate_before_message                    :string(255)      default("Confirmo que valido la definición")
#  definition_by_user_validate_after_message                     :string(255)      default("Confirmo que valido la definición")
#  definition_by_user_finish_validated_message                   :string(255)      default("Confirmo que he corregido y finalizado la definición")
#  allow_definition_by_managers                                  :boolean          default(FALSE)
#  definition_by_users_pdf                                       :boolean          default(FALSE)
#  definition_by_users_accept_pdf                                :boolean          default(FALSE)
#  allow_calibration                                             :boolean          default(FALSE)
#  final_temporal_assessment                                     :boolean
#  display_in_qres                                               :boolean          default(FALSE)
#  display_weight_in_qres                                        :boolean          default(FALSE)
#  qres_allow_individual_comments_by_boss                        :boolean          default(FALSE)
#  qres_allow_watch_individual_comments_by_boss                  :boolean          default(FALSE)
#  qres_allow_individual_comments_with_boss                      :boolean          default(FALSE)
#  step_assessment_show_auto_comment_to_boss                     :boolean          default(FALSE)
#  pe_evaluation_group_id                                        :integer
#  display_weight_in_assessment                                  :boolean          default(FALSE)
#  send_email_after_accept_definition                            :boolean          default(FALSE)
#  email_after_accept_definition_subject                         :string(255)
#  email_after_accept_definition_content                         :text
#  definition_by_users_clone                                     :boolean          default(FALSE)
#  def_by_users_validated_before_accepted_manager                :boolean          default(TRUE)
#  def_by_users_validated_after_accepted_manager                 :boolean          default(TRUE)
#  assess_boss_copy_auto                                         :boolean          default(FALSE)
#  assess_allow_files_to_boss                                    :boolean          default(FALSE)
#  cod                                                           :string(255)
#

class PeEvaluation < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_dimension
  belongs_to :pe_evaluation_group

  has_many :pe_evaluation_rels, dependent: :destroy #
  has_many :pe_elements, dependent: :destroy #
  has_many :pe_groups, dependent: :destroy #
  has_many :pe_member_groups #
  has_many :pe_member_evaluations, dependent: :destroy #

  has_many :pe_delegated_evaluations, class_name: 'PeEvaluation', foreign_key: 'pe_delegated_evaluation_id' #
  belongs_to :pe_delegated_evaluation, class_name: 'PeEvaluation', foreign_key: 'pe_delegated_evaluation_id' #

  has_many :pe_evaluation_displayed_def_by_users, dependent: :destroy#
  has_many :pe_evaluations_where_is_displayed_def_by_users, class_name: 'PeEvaluationDisplayedDefByUser', foreign_key: 'pe_displayed_evaluation_id' #

  has_many :pe_evaluation_displayed_trackings, dependent: :destroy #
  has_many :pe_evaluations_where_is_displayed_tracking, class_name: 'PeEvaluationDisplayedTracking', foreign_key: 'pe_displayed_evaluation_id' #

  has_many :pe_questions, dependent: :destroy #

  has_many :pe_assessment_evaluations #

  has_many :pe_definition_by_user_validators #

  has_many :pe_question_validations #

  has_many :pe_slider_groups#

  has_many :pe_question_models #

  has_many :pe_evaluation_def_rel_messages, :dependent => :destroy #
  has_many :pe_evaluation_track_rel_messages, :dependent => :destroy
  has_many :pe_evaluation_assess_rel_messages, :dependent => :destroy

  has_many :pe_question_activity_fields
  has_many :pe_question_activity_field_lists
  has_many :pe_question_activity_statuses

  has_one :pe_process_notification_def

  has_one :pe_process_reports_evaluation, :dependent => :destroy

  has_many :pe_cal_sessions, :dependent => :destroy #

  attr_accessible :description, :entered_by_manager, :entered_by_manager_questions,
                  :name, :position, :weight, :pe_process_id, :pe_dimension_id,
                  :points_to_ohp, :max_per_cant_answer, :min_percentage, :max_percentage,
                  :informative,
                  :allow_definition_by_managers,
                  :assessment_layout, :allow_definition_by_users, :definition_by_users_rol,
                  :allow_definition_by_users_accepted, :definition_by_users_accept_message, :allow_watching_own_definition, :watch_my_definition_only_when_finished,
                  :allow_tracking, :allow_watching_own_tracking, :allow_tracking_with_boss,
                  :track_allow_individual_comments_by_boss, :track_allow_watch_individual_comments_by_boss, :track_allow_individual_comments_with_boss,
                  :assess_allow_individual_comments_by_boss, :assess_allow_watch_individual_comments_by_boss, :assess_allow_individual_comments_with_boss,
                  :final_evaluation_operation_evaluators, :final_evaluation_operation_evaluations,
                  :step_assessment_show_auto_to_boss,
                  :num_steps_definition_by_users_validated_before_accepted,
                  :num_steps_definition_by_users_validated_after_accepted,
                  :alias_steps_definition_by_users_validated_before_accepted,
                  :alias_steps_definition_by_users_validated_after_accepted,
                  :label_not_initiated_definition_by_users,
                  :label_not_ready_definition_by_users,
                  :label_ready_definition_by_users,
                  :label_not_ready_definition_by_users_accepted,
                  :label_ready_definition_by_users_accepted,
                  :label_not_ready_definition_by_users_validated_before_accepted,
                  :label_ready_definition_by_users_validated_before_accepted,
                  :label_rejected_definition_by_users_validated_before_accepted,
                  :label_not_ready_definition_by_users_validated_after_accepted,
                  :label_ready_definition_by_users_validated_after_accepted,
                  :label_rejected_definition_by_users_validated_after_accepted,
                  :send_email_to_accept_definition,
                  :email_to_accept_definition_subject,
                  :email_to_accept_definition_content,
                  :lay_force_nobr_goal_length,
                  :def_allow_individual_comments_by_boss, :def_allow_watch_individual_comments_by_boss, :def_allow_individual_comments_with_boss,
                  :qdefs_allow_individual_comments_by_boss, :qdefs_allow_watch_individual_comments_by_boss, :qdefs_allow_individual_comments_with_boss,
                  :def_allow_action_plan_by_boss, :def_allow_watch_action_plan_by_boss, :def_allow_action_plan_with_boss,
                  :display_in_qdefs, :display_weight_in_qdefs,
                  :definition_by_user_finish_message, :definition_by_user_finish_validated_message,
                  :definition_by_user_validate_before_message, :definition_by_user_validate_after_message,
                  :definition_by_users_pdf, :definition_by_users_accept_pdf,
                  :allow_calibration, :final_temporal_assessment,
                  :display_in_qres,
                  :display_weight_in_qres,
                  :qres_allow_individual_comments_by_boss,
                  :qres_allow_watch_individual_comments_by_boss,
                  :qres_allow_individual_comments_with_boss,
                  :step_assessment_show_auto_comment_to_boss,
                  :display_weight_in_assessment,
                  :send_email_after_accept_definition, :email_after_accept_definition_subject, :email_after_accept_definition_content,
                  :definition_by_users_clone,
                  :def_by_users_validated_before_accepted_manager, :def_by_users_validated_after_accepted_manager,
                  :assess_boss_copy_auto,
                  :assess_allow_files_to_boss, :cod


  before_save { self.definition_by_users_rol = nil if self.definition_by_users_rol.blank? }
  after_create :create_pe_process_notification_def
  after_create :create_pe_process_reports_evaluation

  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :name, presence: true
  validates :pe_dimension_id, presence: true
  validates :pe_process_id, presence: true

  validates :weight, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :points_to_ohp, presence: true, numericality: { greater_than: 0 }
  validates :max_per_cant_answer, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :min_percentage, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :max_percentage, presence: true, numericality: { greater_than_or_equal_to: 0 }

  default_scope order('pe_evaluations.position ASC, pe_evaluations.name ASC')

  def create_pe_process_notification_def
    pe_process_notification_def = self.build_pe_process_notification_def
    pe_process_notification_def.pe_process = self.pe_process
    pe_process_notification_def.save
  end

  def create_pe_process_reports_evaluation

    pe_process_reports_evaluation = self.build_pe_process_reports_evaluation
    pe_process_reports_evaluation.pe_process = self.pe_process
    pe_process_reports_evaluation.save

  end

  def self.assessment_layouts
    [:estandar, :objetivos_e_indicadores, :grid_1_elemento, :grid_2_elementos, :slider_1_elemento, :slider_2_elementos]
  end

  def self.final_evaluation_operations_evaluators
    [:agrupando_relaciones, :sin_agrupar_relaciones, :agrupando_areas]
  end

  def self.final_evaluation_operations_evaluations
    [:tomando_resultados_finales, :tomando_resultados_por_pregunta]
  end

  def pe_evaluation_rel(pe_rel)
    self.pe_evaluation_rels.where('pe_rel_id = ?', pe_rel.id).first
  end

  def has_pe_evaluation_rel(pe_rel)
    self.pe_evaluation_rels.where('pe_rel_id = ?', pe_rel.id).count > 0 ? true : false
  end

  def has_rel_id(rel_id)
    self.pe_evaluation_rels.joins(:pe_rel).where('rel = ?', rel_id).count > 0 ? true : false
  end


  def rel_ids_to_define_elements

    rel_ids = Array.new
=begin
    self.pe_elements.each do |pe_element|

      rel_ids += pe_element.element_def_by_rol.split('-') if pe_element.element_def_by_rol
      #rel_ids += pe_element.assessment_method_def_by_rol.split('-') if pe_element.element_def_by_rol

    end

    return rel_ids.reject(&:empty?).uniq.map(&:to_i)
=end

    rel_ids = definition_by_users_rol.split('-').reject(&:empty?).map(&:to_i) if self.definition_by_users_rol

    return rel_ids

  end

  def has_to_be_defined_by_rel_id(rel_id)
    rel_ids_to_define_elements.include? rel_id
  end

  def has_associations_to_kpis?
    self.pe_elements.where('kpi_dashboard_id IS NOT NULL').size > 0 ? true : false
  end

  def pe_questions_by_question_confirmed(pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_question_id = ? AND confirmed = ?', pe_question_id, true)
    else
      self.pe_questions.where('pe_question_id IS NULL AND confirmed = ?', true)
    end

  end

  def pe_questions_by_question_accepted(pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_question_id = ? AND accepted = ?', pe_question_id, true)
    else
      self.pe_questions.where('pe_question_id IS NULL AND accepted = ?', true)
    end

  end

  def pe_questions_by_question_ready(pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_question_id = ? AND ready = ?', pe_question_id, true)
    else
      self.pe_questions.where('pe_question_id IS NULL AND ready = ?', true)
    end

  end

  def pe_questions_by_question_entered_by_manager_ready(pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_question_id = ? AND ready = ? AND entered_by_manager = ?', pe_question_id, true, true)
    else
      self.pe_questions.where('pe_question_id IS NULL AND ready = ? AND entered_by_manager = ?', true, true)
    end

  end

  def pe_questions_by_question_ready_not_grouped(pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_question_id = ? AND ready = ? AND questions_grouped_id IS NULL', pe_question_id, true)
    else
      self.pe_questions.where('pe_question_id IS NULL AND ready = ? AND questions_grouped_id IS NULL', true)
    end

  end

  def pe_questions_by_question_not_by_pe_member_evaluated_ready(pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_question_id = ? AND pe_member_evaluated_id IS NULL AND ready = ?', pe_question_id, true)
    else
      self.pe_questions.where('pe_question_id IS NULL AND pe_member_evaluated_id IS NULL AND ready = ?', true)
    end

  end

  def pe_questions_by_question(pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_question_id = ?', pe_question_id)
    else
      self.pe_questions.where('pe_question_id IS NULL')
    end

  end

  def pe_questions_by_group_confirmed(pe_group_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_group_id = ? AND pe_question_id = ? AND confirmed = ?', pe_group_id, pe_question_id, true)
    else
      self.pe_questions.where('pe_group_id = ? AND pe_question_id IS NULL AND confirmed = ?', pe_group_id, true)
    end

  end

  def pe_questions_by_group_ready(pe_group_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_group_id = ? AND pe_question_id = ? AND ready = ?', pe_group_id, pe_question_id, true)
    else
      self.pe_questions.where('pe_group_id = ? AND pe_question_id IS NULL AND ready = ?', pe_group_id, true)
    end

  end

  def pe_questions_by_group_ready_and_informative(pe_group_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_group_id = ? AND pe_question_id = ? AND ready = ? AND informative = ?', pe_group_id, pe_question_id, true, true)
    else
      self.pe_questions.where('pe_group_id = ? AND pe_question_id IS NULL AND ready = ? AND informative = ?', pe_group_id, true, true)
    end

  end

  def pe_questions_by_element_confirmed(pe_element_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id = ? AND confirmed = ?', pe_element_id, pe_question_id, true)
    else
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND confirmed = ?', pe_element_id, true)
    end

  end

  def pe_questions_by_element_ready(pe_element_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id = ? AND ready = ?', pe_element_id, pe_question_id, true)
    else
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND ready = ?', pe_element_id, true)
    end

  end
  
  def pe_questions_by_element_confirmed_and_not_informative(pe_element_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id = ? AND confirmed = ? AND informative = ?', pe_element_id, pe_question_id, true, false)
    else
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND confirmed = ? AND informative = ?', pe_element_id, true, false)
    end

  end

  def pe_questions_by_element_ready_and_not_informative(pe_element_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id = ? AND ready = ? AND informative = ?', pe_element_id, pe_question_id, true, false)
    else
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND ready = ? AND informative = ?', pe_element_id, true, false)
    end

  end

  def pe_questions_by_element_confirmed_and_informative(pe_element_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id = ? AND confirmed = ? AND informative = ?', pe_element_id, pe_question_id, true, true)
    else
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND confirmed = ? AND informative = ?', pe_element_id, true, true)
    end

  end

  def pe_questions_by_element_ready_and_informative(pe_element_id, pe_question_id)

    if pe_question_id
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id = ? AND ready = ? AND informative = ?', pe_element_id, pe_question_id, true, true)
    else
      self.pe_questions.where('pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND ready = ? AND informative = ?', pe_element_id, true, true)
    end

  end

  def pe_questions_only_by_element_confirmed(pe_element_id)

    self.pe_questions.where('pe_questions.pe_element_id = ? AND confirmed = ?', pe_element_id, true)

  end

  def pe_questions_only_by_element_ready(pe_element_id)

    self.pe_questions.where('pe_questions.pe_element_id = ? AND ready = ?', pe_element_id, true)

  end

  def total_number_of_questions_by_pe_member_1st_level_confirmed(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND confirmed = ?', true).count

  end

  def total_number_of_questions_by_pe_member_1st_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND ready = ?', true).count

  end

  def pe_questions_only_by_pe_member_and_group_1st_level_ready_and_not_informative(pe_member_evaluated, pe_group)

    q2=q2=''

    q1 = ' pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' '
    q2 = ' OR pe_group_id = '+pe_group.id.to_s if pe_group

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+') AND ready = ? and informative = ?', true, false)

  end

  def total_number_of_questions_by_pe_member_1st_level_ready_need_to_answer(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND ready = ? AND informative = ?', true, false).count

  end

  def total_number_of_questions_by_pe_member_1st_level_ready_need_to_answer_manager(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND ready = ? AND informative = ? AND entered_by_manager = ?', true, false, true).count

  end

  def total_number_of_questions_by_pe_member_2nd_level_confirmed(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    t = 0

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      t = self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND confirmed = ?', self.pe_elements.second.id, true).count

    end

    return t

  end

  def total_number_of_questions_by_pe_member_2nd_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    t = 0

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      t = self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND ready = ?', self.pe_elements.second.id, true).count

    end

    return t

  end

  def total_number_of_questions_by_pe_member_2nd_level_ready_need_to_answer(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    t = 0

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      t = self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND ready = ? AND informative = ?', self.pe_elements.second.id, true, false).count

    end

    return t

  end

  def total_number_of_questions_by_pe_member_3rd_level_confirmed(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    t = 0

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      t = self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND confirmed = ?', self.pe_elements[2].id, true).count

    end

    return t

  end

  def total_number_of_questions_by_pe_member_3rd_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    t = 0

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      t = self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND ready = ?', self.pe_elements[2].id, true).count

    end

    return t

  end

  def total_number_of_questions_by_pe_member_3rd_level_ready_need_to_answer(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    t = 0

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      t = self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND ready = ? AND informative = ?', self.pe_elements[2].id, true, false).count

    end

    return t

  end

  def pe_questions_by_pe_member_1st_level_confirmed(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND confirmed = ?', true)

  end

  def pe_questions_by_pe_member_1st_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND ready = ?', true)

  end

  def pe_questions_by_pe_member_1st_level_ready_all(pe_member_evaluated, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+') '
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND ready = ?', true)

  end

  def pe_questions_by_pe_member_1st_level_entered_by_manager_ready_all(pe_member_evaluated, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+') '
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND ready = ? AND entered_by_manager = ?', true, true)

  end

  def pe_questions_by_pe_member_1st_level_confirmed_and_not_informative(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND confirmed = ? AND informative = ?', true, false)

  end

  def pe_questions_by_pe_member_1st_level_ready_and_not_informative(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+') AND ready = ? AND informative = ?', true, false)

  end

  def pe_questions_only_by_pe_member_1st_level_confirmed(pe_member_evaluated)

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' ) '

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+') AND confirmed = ?', true)

  end

  def pe_questions_only_by_pe_member_1st_level_ready(pe_member_evaluated)
    return self.pe_questions.where('pe_question_id IS NULL AND pe_member_evaluated_id = ? AND ready = ?', pe_member_evaluated.id, true)
  end
  def pe_questions_only_by_pe_member_or_group_1st_level_ready(pe_member_evaluated, pe_group)
    if pe_group
      return self.pe_questions.where('pe_question_id IS NULL AND (pe_member_evaluated_id = ? OR pe_group_id = ?) AND ready = ?', pe_member_evaluated.id, pe_group.id, true)
    else
      return self.pe_questions_only_by_pe_member_1st_level_ready(pe_member_evaluated)
    end

  end

  def pe_questions_only_by_pe_member_1st_level(pe_member_evaluated)
    return self.pe_questions.where('pe_question_id IS NULL AND pe_member_evaluated_id = ?', pe_member_evaluated.id)
  end

  def pe_questions_by_pe_member_2nd_level_confirmed(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      return self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND confirmed = ?', self.pe_elements.second.id, true)

    end

  end

  def pe_questions_by_pe_member_2nd_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      return self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND ready = ?', self.pe_elements.second.id, true)

    end

  end

  def pe_questions_by_pe_member_3rd_level_confirmed(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_elements[2]

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      return self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND confirmed = ?', self.pe_elements[2].id, true)

    end

  end

  def pe_questions_by_pe_member_3rd_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_elements[2]

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      return self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+') AND ready = ?', self.pe_elements[2].id, true)

    end

  end
  
  def pe_questions_by_pe_member_1st_level(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+q2+q3+q4+')')

  end

  def pe_questions_by_pe_member_any_evaluator_1st_level(pe_member_evaluated)

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NOT NULL) '

    return self.pe_questions.where('pe_question_id IS NULL AND ('+q1+')')

  end

  def pe_questions_by_pe_member_any_evaluator_by_element(pe_member_evaluated, pe_element)

    q1 = 'pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id IS NOT NULL AND pe_questions.pe_element_id = '+pe_element.id.to_s

    return self.pe_questions.where('('+q1+')')

  end



  def pe_questions_only_by_pe_member_1st_level_confirmed(pe_member_evaluated)

    return self.pe_questions.where('pe_question_id IS NULL AND pe_member_evaluated_id = ? AND confirmed = ?', pe_member_evaluated.id, true)

  end

  def pe_questions_only_by_pe_member_1st_level_accepted(pe_member_evaluated)

    return self.pe_questions.where('pe_question_id IS NULL AND pe_member_evaluated_id = ? AND accepted = ?', pe_member_evaluated.id, true)

  end



  def pe_questions_by_pe_member_2nd_level(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_elements.second

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated. id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      return self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+')', self.pe_elements.second.id)

    end

  end

  def pe_questions_by_pe_member_3rd_level(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    if self.pe_elements[2]

      q1=q2=q3=q4=''

      q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated. id.to_s+' AND pe_member_evaluator_id IS NULL) '
      q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
      q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
      q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

      return self.pe_questions.where('pe_questions.pe_element_id = ? AND ('+q1+q2+q3+q4+')', self.pe_elements[2].id)

    end

  end

  def pe_questions_by_pe_member_all_levels(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated. id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where(q1+q2+q3+q4)

  end

  def pe_questions_by_pe_member_all_levels_confirmed(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated. id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('('+q1+q2+q3+q4+') AND confirmed = ?', true)

  end

  def pe_questions_by_pe_member_all_levels_not_confirmed(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated. id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('('+q1+q2+q3+q4+') AND confirmed = ?', false)

  end

  def pe_questions_by_pe_member_all_levels_accepted(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated. id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('('+q1+q2+q3+q4+') AND confirmed = ? AND accepted = ?', true, true)

  end

  def pe_questions_by_pe_member_all_levels_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel)

    q1=q2=q3=q4=''

    q1 = ' (pe_member_evaluated_id = '+pe_member_evaluated. id.to_s+' AND pe_member_evaluator_id IS NULL) '
    q2 = ' OR (pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s+') ' if pe_member_evaluator
    q3 = ' OR (pe_group_id = '+pe_group.id.to_s+') ' if pe_group
    q4 = ' OR (pe_rel_id = '+pe_rel.id.to_s+') ' if pe_rel

    return self.pe_questions.where('('+q1+q2+q3+q4+') AND ready = ?', true)

  end

  def pe_questions_by_pe_member_evaluated_evaluator_all_levels(pe_member_evaluated, pe_member_evaluator)

    q = 'pe_member_evaluated_id = '+pe_member_evaluated.id.to_s+' AND pe_member_evaluator_id = '+pe_member_evaluator.id.to_s

    return self.pe_questions.where(q)

  end

  def pe_assessment_evaluation_by_pe_member_rel(pe_member_rel)

    self.pe_assessment_evaluations.where('pe_member_rel_id = ?', pe_member_rel.id).first

  end

  def pe_assessment_evaluations_ids_by_pe_member_rels(pe_member_rels)

    self.pe_assessment_evaluations.where('pe_member_rel_id in (?)', pe_member_rels.map(&:id)).pluck(:id)

  end

  def pe_questions_only_by_pe_member_all_levels(pe_member_evaluated)

    return self.pe_questions.where('pe_member_evaluated_id = ?', pe_member_evaluated.id)

  end

  def total_number_of_questions_only_by_pe_member_all_levels_managed_by_manager(pe_member_evaluated)

    return self.pe_questions.where('pe_member_evaluated_id = ? AND entered_by_manager = ?', pe_member_evaluated.id, true).size

  end

  def pe_questions_only_by_pe_member_all_levels_confirmed(pe_member_evaluated)

    return self.pe_questions.where('pe_member_evaluated_id = ? AND confirmed = ?', pe_member_evaluated.id, true)

  end

  def pe_questions_only_by_pe_member_all_levels_accepted(pe_member_evaluated)

    return self.pe_questions.where('pe_member_evaluated_id = ? AND accepted = ?', pe_member_evaluated.id, true)

  end

  def pe_questions_only_by_pe_member_all_levels_validated(pe_member_evaluated)

    return self.pe_questions.where('pe_member_evaluated_id = ? AND validated = ?', pe_member_evaluated.id, true)

  end

  def pe_questions_only_by_pe_member_all_levels_ready(pe_member_evaluated)

    return self.pe_questions.where('pe_member_evaluated_id = ? AND ready = ?', pe_member_evaluated.id, true)

  end

  def draw_label_not_initiated_definition_by_users

    l = label_not_initiated_definition_by_users.split('|')

    if l[0] && l[1]

      '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

    else

      self.label_not_initiated_definition_by_users

    end


  end

  def draw_label_not_ready_definition_by_users

    l = label_not_ready_definition_by_users.split('|')

    if l[0] && l[1]

      '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

    else

      self.label_not_ready_definition_by_users

    end


  end

  def draw_label_ready_definition_by_users

    l = label_ready_definition_by_users.split('|')

    if l[0] && l[1]

      '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

    else

      self.label_ready_definition_by_users

    end

  end

  def draw_label_not_ready_definition_by_users_accepted

    l = label_not_ready_definition_by_users_accepted.split('|')

    if l[0] && l[1]

      '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

    else

      self.label_not_ready_definition_by_users_accepted

    end

  end

  def draw_label_ready_definition_by_users_accepted

    l = label_ready_definition_by_users_accepted.split('|')

    if l[0] && l[1]

      '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

    else

      self.label_ready_definition_by_users_accepted

    end

  end

  def draw_label_not_ready_definition_by_users_validated_before_accepted(step_number)

    s = label_not_ready_definition_by_users_validated_before_accepted.split(';')

    if s[step_number-1]

      l = s[step_number-1].split('|')

      if l[0] && l[1]

        '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

      else

        self.label_not_ready_definition_by_users_validated_before_accepted

      end

    else

      self.label_not_ready_definition_by_users_validated_before_accepted

    end

  end

  def draw_label_ready_definition_by_users_validated_before_accepted(step_number)

    s = label_ready_definition_by_users_validated_before_accepted.split(';')

    if s[step_number-1]

      l = s[step_number-1].split('|')

      if l[0] && l[1]

        '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

      else

        self.label_ready_definition_by_users_validated_before_accepted

      end

    else

      self.label_ready_definition_by_users_validated_before_accepted

    end

  end

  def draw_label_rejected_definition_by_users_validated_before_accepted(step_number)

    s = label_rejected_definition_by_users_validated_before_accepted.split(';')

    if s[step_number-1]

      l = s[step_number-1].split('|')

      if l[0] && l[1]

        '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

      else

        self.label_rejected_definition_by_users_validated_before_accepted

      end

    else

      self.label_rejected_definition_by_users_validated_before_accepted

    end

  end

  def draw_label_not_ready_definition_by_users_validated_after_accepted(step_number)

    s = label_not_ready_definition_by_users_validated_after_accepted.split(';')

    if s[step_number-1]

      l = s[step_number-1].split('|')

      if l[0] && l[1]

        '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

      else

        self.label_not_ready_definition_by_users_validated_after_accepted

      end

    else

      self.label_not_ready_definition_by_users_validated_after_accepted

    end

  end

  def draw_label_ready_definition_by_users_validated_after_accepted(step_number)

    s = label_ready_definition_by_users_validated_after_accepted.split(';')

    if s[step_number-1]

      l = s[step_number-1].split('|')

      if l[0] && l[1]

        '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

      else

        self.label_ready_definition_by_users_validated_after_accepted

      end

    else

      self.label_ready_definition_by_users_validated_after_accepted

    end

  end

  def draw_label_rejected_definition_by_users_validated_after_accepted(step_number)

    s = label_rejected_definition_by_users_validated_after_accepted.split(';')

    if s[step_number-1]

      l = s[step_number-1].split('|')

      if l[0] && l[1]

        '<span class="label label-'+l[1]+'">'+l[0]+'</span>'

      else

        self.label_rejected_definition_by_users_validated_after_accepted

      end

    else

      self.label_rejected_definition_by_users_validated_after_accepted

    end

  end


  def pe_member_definition_by_users_status(pe_member_evaluated)

    pe_questions = self.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated)

    total_number_of_questions = pe_questions.size

    total_number_of_questions_confirmed = self.pe_questions_only_by_pe_member_all_levels_confirmed(pe_member_evaluated).count

    status = 'not_initiated'

    status_confirmed = false
    status_validated_before = Array.new
    status_accepted = false
    status_validated_after = Array.new

    l = self.label_not_initiated_definition_by_users.split('|')
    status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
    status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'
    status_step = nil
    status_before_accept = nil

    continue = true

    if total_number_of_questions > 0

      status = 'in_definition'

      status_confirmed = false
      status_validated_before = Array.new
      status_accepted = false
      status_validated_after = Array.new

      l = self.label_not_ready_definition_by_users.split('|')
      status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
      status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'
      status_step = nil
      status_before_accept = nil

      if total_number_of_questions == total_number_of_questions_confirmed

        status = 'confirmed'
        status_confirmed = true
        l = self.label_ready_definition_by_users.split('|')
        status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
        status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

      end

      if self.num_steps_definition_by_users_validated_before_accepted > 0

        before_accept = true

        (1..self.num_steps_definition_by_users_validated_before_accepted).each do |step_number|


          if pe_member_evaluated.pe_definition_by_user_validator(self, step_number, true)

            status_validated_before[step_number-1] = false

            status_step_temp = 'cant'

            if status == 'confirmed' && total_number_of_questions == self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ?', pe_questions.pluck(:id), step_number, before_accept, true).size

              status_step_temp = 'ready'

              status_validated_before[step_number-1] = true

            elsif self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ? AND attended = ?', pe_questions.pluck(:id), step_number, before_accept, false, false).size > 0

              status_step_temp = 'rejected'

            else

              if step_number == 1

                if total_number_of_questions == total_number_of_questions_confirmed

                  status_step_temp = 'not_ready'

                end

              elsif step_number > 1

                if total_number_of_questions == total_number_of_questions_confirmed

                  validated_questions = self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ?', pe_questions.pluck(:id), step_number-1, before_accept, true)

                  status_step_temp = 'not_ready' if validated_questions.size == total_number_of_questions

                end

              end

            end

            if status_step_temp == 'cant'

              continue = false
              break

            elsif status_step_temp == 'rejected'

              status = 'in_definition'
              status_step = step_number
              status_before_accept = before_accept

              l = self.label_rejected_definition_by_users_validated_before_accepted.split(';')
              l = l[step_number-1].split('|')
              status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
              status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

            elsif status_step_temp == 'not_ready'

              status_step = step_number
              status_before_accept = before_accept

              l = self.label_not_ready_definition_by_users_validated_before_accepted.split(';')
              l = l[step_number-1].split('|')
              status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
              status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

              continue = false
              break

            else

              status = 'validated_before_'+step_number.to_s
              status_step = step_number
              status_before_accept = before_accept

              l = self.label_ready_definition_by_users_validated_before_accepted.split(';')
              l = l[step_number-1].split('|')
              status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
              status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

            end

          end

        end

      end

      if continue && self.allow_definition_by_users_accepted?

        status_step_temp = 'cant'

        if total_number_of_questions == total_number_of_questions_confirmed && total_number_of_questions_confirmed == self.pe_questions_only_by_pe_member_all_levels_accepted(pe_member_evaluated).size

          status_step_temp = 'ready'

          status_accepted = true

        else

          if total_number_of_questions == total_number_of_questions_confirmed

            if pe_member_evaluated.needs_definition_by_users_validation_before_accepted self

              validated_questions = self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ?', pe_questions.pluck(:id), self.num_steps_definition_by_users_validated_before_accepted, true, true)

              status_step_temp = 'not_ready' if validated_questions.size == total_number_of_questions

            else

              status_step_temp = 'not_ready'

            end

          end

        end

        if status_step_temp == 'cant'

          continue = false

        elsif status_step_temp == 'not_ready'

          status = 'to_be_accepted'

          l = self.label_not_ready_definition_by_users_accepted.split('|')
          status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
          status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

          continue = false

        else

          status = 'accepted'

          l = self.label_ready_definition_by_users_accepted.split('|')
          status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
          status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

        end

      end

      if continue && self.num_steps_definition_by_users_validated_after_accepted > 0

        before_accept = false

        (1..self.num_steps_definition_by_users_validated_after_accepted).each do |step_number|

          if pe_member_evaluated.pe_definition_by_user_validator(self, step_number, false)

            status_validated_after[step_number-1] = false

            status_step_temp = 'cant'

            if total_number_of_questions == self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND after_accept = ? AND validated = ?', pe_questions.pluck(:id), step_number, before_accept, true).size

              status_step_temp = 'ready'

              status_validated_after[step_number-1] = true

            elsif self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND after_accept = ? AND validated = ? AND attended = ?', pe_questions.pluck(:id), step_number, before_accept, false, false).size > 0

              status_step_temp = 'rejected'

            else

              total_number_of_questions_accepted = self.pe_questions_only_by_pe_member_all_levels_accepted(pe_member_evaluated).count

              if step_number == 1

                if total_number_of_questions == total_number_of_questions_accepted

                  status_step_temp = 'not_ready'

                end

              elsif step_number > 1

                if total_number_of_questions == total_number_of_questions_accepted

                  validated_questions = self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND after_accept = ? AND validated = ?', pe_questions.pluck(:id), step_number-1, before_accept, true)

                  status_step_temp = 'not_ready' if validated_questions.size == total_number_of_questions

                end

              end

            end

            if status_step_temp == 'cant'

              continue = false
              break

            elsif status_step_temp == 'rejected'

              status = 'rejected'
              status_step = step_number
              status_before_accept = before_accept

              l = self.label_rejected_definition_by_users_validated_after_accepted.split(';')
              l = l[step_number-1].split('|')
              status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
              status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

            elsif status_step_temp == 'not_ready'

              status = 'not_ready'
              status_step = step_number
              status_before_accept = before_accept

              l = self.label_not_ready_definition_by_users_validated_after_accepted.split(';')
              l = l[step_number-1].split('|')
              status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
              status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

              continue = false
              break

            else

              status = 'ready'
              status_step = step_number
              status_before_accept = before_accept

              l = self.label_ready_definition_by_users_validated_after_accepted.split(';')
              l = l[step_number-1].split('|')
              status_label = '<span class="label label-'+l[1]+'">'+l[0]+'</span>'
              status_label_smart_table = '<span class="label label-'+l[1]+' label-smart-table">'+l[0]+'</span>'

            end

          end

        end

      end

    end

    return status, status_label, status_label_smart_table, status_confirmed, status_validated_before, status_accepted, status_validated_after, total_number_of_questions

  end

  def pe_member_status_to_be_accepted(pe_member_evaluated)

    pe_questions = self.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated)

    total_number_of_questions = pe_questions.count

    total_number_of_questions_confirmed = self.pe_questions_only_by_pe_member_all_levels_confirmed(pe_member_evaluated).count

    status = 'not_ready'

    if total_number_of_questions > 0 && total_number_of_questions == total_number_of_questions_confirmed && total_number_of_questions_confirmed == self.pe_questions_only_by_pe_member_all_levels_accepted(pe_member_evaluated).size

      status = 'finished'

    else

      if total_number_of_questions > 0 && total_number_of_questions == total_number_of_questions_confirmed

        if pe_member_evaluated.needs_definition_by_users_validation_before_accepted self

            validated_questions = self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ?', pe_questions.pluck(:id), self.num_steps_definition_by_users_validated_before_accepted, true, true)

            status = 'ready' if validated_questions.size == total_number_of_questions

        else

            status = 'ready'

        end

      end

    end

    status

  end

  def pe_member_status_to_be_validated(pe_member_evaluated, step_number, before_accept)

    pe_questions = self.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated)

    total_number_of_questions = pe_questions.count

    status = 'not_ready'

    total_number_of_questions_confirmed = self.pe_questions_only_by_pe_member_all_levels_confirmed(pe_member_evaluated).count

    if total_number_of_questions > 0 && total_number_of_questions == total_number_of_questions_confirmed && total_number_of_questions == self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ?', pe_questions.pluck(:id), step_number, before_accept, true).size

      status = 'finished'

    elsif total_number_of_questions > 0 && self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ? AND attended = ?', pe_questions.pluck(:id), step_number, before_accept, false, false).size > 0

      status = 'not_validated'

    else

      if before_accept

        #total_number_of_questions_confirmed = self.pe_questions_only_by_pe_member_all_levels_confirmed(pe_member_evaluated).count

        if step_number == 1

          if total_number_of_questions > 0 && total_number_of_questions == total_number_of_questions_confirmed

            status = 'ready'

          end

        elsif step_number > 1

          if total_number_of_questions > 0 && total_number_of_questions == total_number_of_questions_confirmed

              validated_questions = self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ?', pe_questions.pluck(:id), step_number-1, before_accept, true)

              status = 'ready' if validated_questions.size == total_number_of_questions

          end

        end

      else

        total_number_of_questions_accepted = self.pe_questions_only_by_pe_member_all_levels_accepted(pe_member_evaluated).count

        if step_number == 1

          if total_number_of_questions > 0 && total_number_of_questions == total_number_of_questions_accepted

            status = 'ready'

          end

        elsif step_number > 1

          if total_number_of_questions > 0 && total_number_of_questions == total_number_of_questions_accepted

            validated_questions = self.pe_question_validations.where('pe_question_id IN(?) AND step_number = ? AND before_accept = ? AND validated = ?', pe_questions.pluck(:id), step_number-1, before_accept, true)

            status = 'ready' if validated_questions.size == total_number_of_questions

          end

        end

      end

    end

    status

  end

  def definition_by_users_status_of_pe_member_evaluated(pe_member_evaluated)

    status = 'not_initiated'

    total_number_of_questions = self.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).count

    if total_number_of_questions > 0

      total_number_of_confirmed_questions = self.pe_questions_only_by_pe_member_all_levels_confirmed(pe_member_evaluated).count

      if total_number_of_confirmed_questions < total_number_of_questions

        status = 'not_confirmed'

      else

        total_number_of_accepted_questions = self.pe_questions_only_by_pe_member_all_levels_accepted(pe_member_evaluated).count
        total_number_of_validated_questions = self.pe_questions_only_by_pe_member_all_levels_validated(pe_member_evaluated).count


        if self.needs_definition_by_users_accept && (self.needs_definition_by_users_validation_before_accepted || self.needs_definition_by_users_validation_after_accepted)

          if total_number_of_confirmed_questions == total_number_of_accepted_questions && total_number_of_confirmed_questions == total_number_of_validated_questions
            status = 'finished'
          elsif total_number_of_confirmed_questions == total_number_of_accepted_questions
            status = 'accepted'
          elsif total_number_of_confirmed_questions == total_number_of_validated_questions
            status = 'validated'
          else
            status = 'confirmed'
          end

        elsif self.needs_definition_by_users_accept

          if total_number_of_confirmed_questions == total_number_of_accepted_questions
            status = 'finished'
          else
            status = 'confirmed'
          end

        elsif self.needs_definition_by_users_validation_before_accepted || self.needs_definition_by_users_validation_after_accepted

          if total_number_of_confirmed_questions == total_number_of_accepted_questions
            status = 'finished'
          else
            status = 'confirmed'
          end

        else

          status = 'confirmed'

        end


      end

    end

    status

  end

  def pe_member_has_validations_pending(pe_member_evaluated)

    pe_questions = self.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated)

    self.pe_question_validations.where('pe_question_id IN(?) AND validated = ? AND attended = ?', pe_questions.pluck(:id), false, false).size > 0 ? true : false

  end

  def last_pe_member_pending_validation(pe_member_evaluated)

    pe_questions = self.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated)

    self.pe_question_validations.where('pe_question_id IN(?) AND validated = ? AND attended = ?', pe_questions.pluck(:id), false, false).reorder('before_accept ASC, step_number DESC').first

  end

  def pe_member_status_to_be_tracked(pe_member_evaluated)

    status = 'not_ready'

    pe_questions = self.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated)
    pe_questions_ready = self.pe_questions_only_by_pe_member_all_levels_ready(pe_member_evaluated)

    status = 'ready' if pe_questions.size > 0 && pe_questions.size == pe_questions_ready.size

    status

  end

  def pe_creators_of_pe_member_questions(pe_member_evaluated)
    User.where('id IN(?)', self.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).pluck(:creator_id))
  end

  def needs_definition_by_users_accept
    self.pe_process.has_step_definition_by_users_accepted && self.allow_definition_by_users_accepted
  end

  def needs_definition_by_users_validation
    self.pe_process.has_step_definition_by_users_validated && (self.num_steps_definition_by_users_validated_before_accepted > 0 || self.num_steps_definition_by_users_validated_after_accepted > 0)
  end

  def needs_definition_by_users_validation_before_accepted
    self.pe_process.has_step_definition_by_users_validated && self.num_steps_definition_by_users_validated_before_accepted > 0
  end

  def needs_definition_by_users_validation_after_accepted
    self.pe_process.has_step_definition_by_users_validated && self.num_steps_definition_by_users_validated_after_accepted > 0
  end

  def is_pe_definition_by_user_validator?(user, step_number, before_accept)
    self.pe_definition_by_user_validators.where('validator_id = ? AND step_number = ? AND before_accept = ?', user.id, step_number, before_accept).count > 0 ? true : false
  end

  def pe_definition_by_user_validators_by_validator_ordered_by_user(user, step_number, before_accept)
    self.pe_definition_by_user_validators.where('validator_id = ? AND step_number = ? AND before_accept = ?', user.id, step_number, before_accept).joins(pe_member: :user).reorder('apellidos, nombre')
  end

  def pe_definitions_by_user_validators_by_validator(user)

    self.pe_definition_by_user_validators.where('validator_id = ?', user.id )

  end

  def alias_step(step_number, before_accept)

    alias_steps = Array.new

    if before_accept
      alias_steps = self.alias_steps_definition_by_users_validated_before_accepted.split(';') if self.alias_steps_definition_by_users_validated_after_accepted
    else
      alias_steps = self.alias_steps_definition_by_users_validated_after_accepted.split(';') if self.alias_steps_definition_by_users_validated_after_accepted
    end

    if alias_steps[step_number-1]
      a = alias_steps[step_number-1].split('-')
      if a[0]
        a[0]
      else
        'Validador'
      end
    else
      'Validar'
    end

  end

  def alias_step_responsible(step_number, before_accept)

    alias_steps = Array.new

    if before_accept
      alias_steps = self.alias_steps_definition_by_users_validated_before_accepted.split(';') if self.alias_steps_definition_by_users_validated_after_accepted
    else
      alias_steps = self.alias_steps_definition_by_users_validated_after_accepted.split(';') if self.alias_steps_definition_by_users_validated_after_accepted
    end

    if alias_steps[step_number-1]
      a = alias_steps[step_number-1].split('-')
      if a[1]
        a[1]
      else
        'Validador'
      end
    else
      'Validador'
    end

  end

  def allow_list_individual_comments_qdefs(pe_rel)

    (self.qdefs_allow_individual_comments_by_boss && pe_rel && pe_rel.rel == 1 ) || (self.qdefs_allow_individual_comments_by_boss && self.qdefs_allow_watch_individual_comments_by_boss && (pe_rel.nil? || pe_rel.rel == 0)) || (self.qdefs_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))

  end

  def allow_create_individual_comment_qdefs(pe_rel)
    if pe_rel
      (self.qdefs_allow_individual_comments_by_boss && pe_rel.rel == 1 ) || (self.qdefs_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))
    else
      false
    end
  end

  def allow_list_individual_comments_qres(pe_rel)

    (self.qres_allow_individual_comments_by_boss && pe_rel && pe_rel.rel == 1 ) || (self.qres_allow_individual_comments_by_boss && self.qres_allow_watch_individual_comments_by_boss && (pe_rel.nil? || pe_rel.rel == 0)) || (self.qres_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))

  end

  def allow_create_individual_comment_qres(pe_rel)
    if pe_rel
      (self.qres_allow_individual_comments_by_boss && pe_rel.rel == 1 ) || (self.qres_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))
    else
      false
    end
  end
  
  def allow_list_individual_comments_def(pe_rel)

    (self.def_allow_individual_comments_by_boss && pe_rel && pe_rel.rel == 1 ) || (self.def_allow_individual_comments_by_boss && self.def_allow_watch_individual_comments_by_boss && (pe_rel.nil? || pe_rel.rel == 0)) || (self.def_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))

  end

  def allow_create_individual_comment_def(pe_rel)
    if pe_rel
      (self.def_allow_individual_comments_by_boss && pe_rel.rel == 1 ) || (self.def_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))
    else
      false
    end
  end

  def allow_list_action_plan_def(rel_id, pe_member_evaluator, pe_member_evaluated)

    (self.def_allow_action_plan_by_boss && rel_id == 1 ) || (self.def_allow_action_plan_by_boss && self.def_allow_watch_action_plan_by_boss && pe_member_evaluator.id == pe_member_evaluated.id) || (self.def_allow_action_plan_with_boss && (rel_id == 1 || pe_member_evaluator.id == pe_member_evaluated.id))

  end

  def allow_create_action_plan_def(rel_id, pe_member_evaluator, pe_member_evaluated)

    (self.def_allow_action_plan_by_boss && rel_id == 1 ) || (self.def_allow_action_plan_with_boss && (rel_id == 1 || pe_member_evaluator.id == pe_member_evaluated.id))

  end

  def allow_list_individual_comments_track(pe_rel)
    if pe_rel
      (self.track_allow_individual_comments_by_boss && pe_rel.rel == 1 ) || (self.track_allow_individual_comments_by_boss && self.track_allow_watch_individual_comments_by_boss && pe_rel.rel == 0) || (self.track_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))
    else
      false
    end
  end

  def allow_create_individual_comment_track(pe_rel)
    if pe_rel
      (self.track_allow_individual_comments_by_boss && pe_rel.rel == 1 ) || (self.track_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))
    else
      false
    end
  end

  def allow_list_individual_comments_assess(pe_rel)
    if pe_rel
      (self.assess_allow_individual_comments_by_boss && pe_rel.rel == 1 ) || (self.assess_allow_individual_comments_by_boss && self.assess_allow_watch_individual_comments_by_boss && pe_rel.rel == 0) || (self.assess_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))
    else
      false
    end
  end

  def allow_create_individual_comment_assess(pe_rel)
    if pe_rel
      (self.assess_allow_individual_comments_by_boss && pe_rel.rel == 1 ) || (self.assess_allow_individual_comments_with_boss && (pe_rel.rel == 1 || pe_rel.rel == 0))
    else
      false
    end
  end

  def pe_evaluation_def_rel_message(pe_rel)
    self.pe_evaluation_def_rel_messages.where('pe_rel_id = ?', pe_rel.id).first if pe_rel
  end

  def pe_evaluation_track_rel_message(pe_rel)
    self.pe_evaluation_track_rel_messages.where('pe_rel_id = ?', pe_rel.id).first if pe_rel
  end

  def pe_evaluation_assess_rel_message(pe_rel)
    self.pe_evaluation_assess_rel_messages.where('pe_rel_id = ?', pe_rel.id).first if pe_rel
  end
  
  def cal_sessions_for_pe_member_committee(user)
    self.pe_cal_sessions.joins(:pe_cal_committees).where('user_id = ?', user.id)
  end

end
