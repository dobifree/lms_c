# == Schema Information
#
# Table name: jm_answer_values
#
#  id                      :integer          not null, primary key
#  jm_candidate_answer_id  :integer
#  jm_subcharacteristic_id :integer
#  value_string            :string(255)
#  value_int               :integer
#  value_float             :decimal(12, 4)
#  value_text              :text
#  value_date              :date
#  value_file              :text
#  value_option_id         :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  value_file_crypted_name :string(255)
#

class JmAnswerValue < ActiveRecord::Base
  belongs_to :jm_candidate_answer
  belongs_to :jm_subcharacteristic
  attr_accessible :value_date, :value_file, :value_float, :value_int, :value_option_id, :value_string, :value_text, :jm_subcharacteristic_id



  def value
    #['Texto', 'Entero','Fecha', 'Selección', 'Archivo', 'Texto largo', 'Real']
    case self.jm_subcharacteristic.option_type
      when 0
        value_string
      when 1
        value_int
      when 2
        value_date ? value_date.strftime('%d/%m/%Y') : ''
      when 3
        value_option_id ? self.jm_subcharacteristic.jm_list.jm_options.where(:id => value_option_id).first.name : ''
      when 4
        value_file
      when 5
        value_text
      when 6
        value_float
    end
  end

end
