# == Schema Information
#
# Table name: sc_processes
#
#  id                :integer          not null, primary key
#  month             :integer
#  year              :integer
#  deadline          :datetime
#  closed            :boolean          default(FALSE)
#  closed_at         :datetime
#  closed_by_user_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  records_since     :datetime
#  records_until     :datetime
#

class ScProcess < ActiveRecord::Base
  has_many :sc_user_processes
  has_many :sc_reportings
  has_many :sc_records

  belongs_to :closed_by_user, class_name: 'User', foreign_key: :closed_by_user_id

  attr_accessible :closed,
                  :closed_at,
                  :closed_by_user_id,
                  :deadline,
                  :month,
                  :year,
                  :records_since,
                  :records_until

  validates :month, presence: true, :uniqueness => {:scope => :year}
  validates :year, presence: true
  validates :records_since, presence: true
  validates :records_until, presence: true
  validates :deadline, presence: true
  validates_presence_of :closed_at, :if => :closed?
  validates_presence_of :closed_by_user_id, :if => :closed?

  def closed_by_user_information
    return self.closed_by_user.codigo+' - '+self.closed_by_user.apellidos+', '+self.closed_by_user.nombre if closed_by_user
  end

  def approved_records
    sc_records.where(status: 3, dont_charge_to_payroll: false)
  end

end

