# == Schema Information
#
# Table name: master_evaluation_questions
#
#  id                         :integer          not null, primary key
#  texto                      :text
#  obligatoria                :boolean
#  respuesta_unica            :boolean
#  puntaje                    :float
#  tiempo                     :integer
#  master_evaluation_topic_id :integer
#  master_evaluation_id       :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  disponible                 :boolean          default(TRUE)
#

class MasterEvaluationQuestion < ActiveRecord::Base

  include ActionView::Helpers::NumberHelper

  belongs_to :master_evaluation_topic
  belongs_to :master_evaluation

  has_many :user_course_evaluation_questions

  has_many :master_evaluation_alternatives, dependent: :destroy

  attr_accessible :obligatoria, :puntaje, :respuesta_unica, :texto, :tiempo, :disponible

  validates :texto, presence: true, uniqueness: { scope: [:master_evaluation_id, :master_evaluation_topic_id] }
  validates :puntaje, presence: true, numericality: { greater_than: 0 }
  validates :tiempo, numericality: { only_integer: true, greater_than: 0 }, allow_nil: true

  #validates :obligatoria, presence: true
  #validates :respuesta_unica, presence: true
  #validates :puntaje, presence: true

  default_scope order('id ASC')

  def puntaje_formateado
    self.puntaje.to_i == self.puntaje ? self.puntaje.to_i : self.puntaje
  end

end
