# == Schema Information
#
# Table name: tracking_lists
#
#  id                      :integer          not null, primary key
#  name                    :string(255)
#  code                    :string(255)
#  description             :text
#  active                  :boolean
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  autocomplete            :boolean          default(FALSE)
#  tracking_list_father_id :integer
#

class TrackingList < ActiveRecord::Base

  has_many :tracking_list_items, :dependent => :destroy
  belongs_to :tracking_list_father, :class_name => 'TrackingList'
  has_many :tracking_list_children, :foreign_key => :tracking_list_father_id, :class_name => 'TrackingList'
  accepts_nested_attributes_for :tracking_list_items, :allow_destroy => true
  attr_accessible :active, :code, :description, :name, :autocomplete, :tracking_list_father_id, :tracking_list_items_attributes

  after_update :clean_children_items_on_father_change, if: -> { tracking_list_father_id_changed? }

  validates :code, :presence => true
  validates :name, :presence => true

  def list_possible_fathers


    lists = TrackingList.where(:active => true)

    unless self.new_record?
      excluded_list_ids = [self.id] +  self.tracking_list_children.pluck(:id) unless self.new_record?
      lists = lists.where('id not in (?)', excluded_list_ids)
    end

    return lists.map { |tracking_list| [tracking_list.code + ' - ' + tracking_list.name, tracking_list.id] }

  end

  def list_possible_father_items
    TrackingListItem.where(:active => true, :tracking_list_id => self.tracking_list_father_id).map { |tracking_list_item| [tracking_list_item.name, tracking_list_item.id] }

  end

  def clean_children_items_on_father_change
    self.tracking_list_items.update_all(:tracking_list_item_father_id => nil)
  end

end
