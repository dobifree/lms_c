# == Schema Information
#
# Table name: program_courses
#
#  id                          :integer          not null, primary key
#  program_id                  :integer
#  course_id                   :integer
#  level_id                    :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  orden                       :integer
#  activo                      :boolean          default(TRUE)
#  consultable                 :boolean          default(TRUE)
#  hidden_in_user_courses_list :boolean          default(FALSE)
#

class ProgramCourse < ActiveRecord::Base
  
  belongs_to :program
  belongs_to :course
  belongs_to :level

  has_many :program_course_managers

  has_many :user_courses, order: 'id ASC', dependent: :destroy
  has_many :enrollments, through: :user_courses

  has_many :program_course_instances

  attr_accessible :course, :level, :orden, :activo, :course_id, :level_id, :program_id,
                  :consultable, :hidden_in_user_courses_list
  
  validates :program, presence: true
  validates :course, presence: true
  validates :course_id, uniqueness: { scope: :program_id }
  validates :level, presence: true


  validate :program_level

  default_scope order('orden')

  def program_level
    if Level.find(level_id).program.id != program_id
      errors.add(:mismatch_program_level, 'El nivel debe pertenecer al mismo programa')
    end
  end

  def group_ids

    self.user_courses.group('grupo').reorder('desde DESC, fin DESC, inicio DESC, fin DESC')

  end

  def group_f_ids(from=nil, to=nil)

    if from != nil && to != nil
      self.user_courses.where('hasta >= ? and hasta <= ?', from, to).group('grupo_f').reorder('hasta DESC')
    else
      self.user_courses.group('grupo_f').reorder('hasta DESC')
    end

  end

  def group_f_with_count(from=nil, to=nil)

    if from != nil && to != nil
      self.user_courses.select('hasta, grupo_f, count(*) as count').where('hasta >= ? and hasta <= ?', from, to).group('grupo_f').reorder('hasta DESC')
    else
      self.user_courses.select('hasta, grupo_f, count(*) as count').group('grupo_f').reorder('hasta DESC')
    end

  end

  def matriculas_por_grupo(grupo)

    self.user_courses.where('grupo = ?', grupo).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_por_grupo_f(grupo_f)

    self.user_courses.where('grupo_f = ?', grupo_f).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_por_grupo_para_update(grupo)

    self.user_courses.where('grupo = ?', grupo)

  end

  def matriculas_por_grupo_f_para_update(grupo_f)

    self.user_courses.where('grupo_f = ?', grupo_f)

  end

  def matriculas_vigentes

    self.user_courses.where('anulado = ? ', false).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_vigentes_instances(pcis)

    pcis_array = pcis.map {|pci| pci.id}

    self.user_courses.where('anulado = ? AND program_course_instance_id IN (?)', false, pcis_array).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_vigentes_desde_hasta(desde, hasta)

    self.user_courses.where('anulado = ? AND desde >= ? AND hasta <= ?', false, desde, hasta).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_vigentes_inicio_fin(inicio, fin)

    self.user_courses.where('anulado = ? AND inicio >= ? AND fin <= ?', false, inicio, fin).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def numero_matriculas_vigentes

    self.user_courses.where('anulado = ? ', false).joins(:user).count

  end

  def matriculas_vigentes_por_grupo(grupo)

    self.user_courses.where('anulado = ?  AND grupo = ?', false, grupo).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_vigentes_por_grupo_f(grupo_f)

    self.user_courses.where('anulado = ? AND grupo_f = ?', false, grupo_f).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_vigentes_por_grupo_desde_hasta(grupo, desde, hasta)

    self.user_courses.where('anulado = ? AND grupo = ? AND desde >= ? AND hasta <= ?', false, grupo, desde, hasta).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def numero_matriculas_vigentes_por_grupo(grupo)

    self.user_courses.where('anulado = ? AND grupo = ?', false, grupo).joins(:user).count

  end

  def numero_matriculas_vigentes_por_grupo_f(grupo_f)

    self.user_courses.where('anulado = ? AND grupo_f = ?', false, grupo_f).joins(:user).count

  end

  def numero_matriculas_inc_fantasma_por_grupo_f(grupo_f)

    self.user_courses.where('anulado = ? AND grupo_f = ?', false, grupo_f).count

  end

  def matriculas_aprobadas_desde_hasta(desde, hasta)


    self.user_courses.where('anulado = ? AND fin >= ? AND fin <= ? AND iniciado = ? AND finalizado = ? AND aprobado = ?',
                            false, desde, hasta, true, true, true).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_aprobadas_desde_hasta_convocatoria(desde, hasta)


    self.user_courses.where('anulado = ? AND desde >= ? AND hasta <= ? AND iniciado = ? AND finalizado = ? AND aprobado = ?',
                            false, desde, hasta, true, true, true).joins(:user).reorder('apellidos, nombre, numero_oportunidad')

  end

  def matriculas_aprobadas_rango_fechas(desde, hasta)

    self.user_courses.where('anulado = ? AND iniciado = ? AND finalizado = ? AND user_courses.aprobado = ? AND ((desde >= ? AND desde <= ?) OR (hasta >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )',
                            false, true, true, true, desde, hasta, desde, hasta, desde, hasta, desde, hasta).joins(:user).reorder('apellidos, nombre, user_courses.id')


  end

  def program_course_instance_by_dates(from, to)
    self.program_course_instances.where('from_date = ? AND to_date = ?', from, to).first
  end

  def program_course_instances_ordered_by_date_desc
    self.program_course_instances.reorder('from_date DESC, to_date DESC, description ASC')
  end

  def program_course_instances_by_creator_ordered_by_date_desc(user)
    self.program_course_instances.where('creator_id = ?', user.id).reorder('from_date DESC, to_date DESC, description ASC')
  end

  def program_course_instances_by_manager_ordered_by_date_desc(user)
    self.program_course_instances.joins(:program_course_instance_managers).where('program_course_instance_managers.user_id = ?', user.id).reorder('from_date DESC, to_date DESC, description ASC')
  end

  def program_course_instances_by_creator_manager_ordered_by_date_desc(user)
    program_course_instances_by_creator_ordered_by_date_desc(user)+program_course_instances_by_manager_ordered_by_date_desc(user)
  end

end
