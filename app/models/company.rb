# == Schema Information
#
# Table name: companies
#
#  id                                 :integer          not null, primary key
#  nombre                             :string(255)
#  codigo                             :string(255)
#  url                                :string(255)
#  connection_string                  :string(255)
#  active                             :boolean          default(FALSE)
#  directorio                         :string(255)
#  email_mesa_ayuda                   :string(255)
#  modulo_sence                       :boolean          default(FALSE)
#  show_header_in_login               :boolean          default(TRUE)
#  texto_username_login               :string(255)
#  texto_cambio_password              :text
#  alias_username                     :string(255)      default("Código")
#  alias_user                         :string(255)      default("Usuario,Usuarios")
#  time_zone                          :string(255)      default("America/Lima")
#  display_code_in_user_profile       :boolean          default(FALSE)
#  header_text_login_box              :string(255)      default("Ingrese sus datos de acceso")
#  recover_pass_text_login_box        :string(255)      default("¿Olvidó su contraseña?")
#  user_profile_tab_text_dashboard    :string(255)      default("Perfil de Usuario")
#  locale                             :string(255)      default("es_PE")
#  training_type_alias                :string(255)      default("Tipo de capacitación")
#  training_type_user_courses_submenu :boolean          default(FALSE)
#  notifications_email                :string(255)      default("notificaciones@exa.pe")
#  accepts_comments                   :boolean          default(TRUE)
#  accepts_values                     :boolean          default(TRUE)
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  is_productive                      :boolean          default(TRUE)
#  use_pgp                            :boolean          default(FALSE)
#  year_user_courses_submenu          :boolean          default(FALSE)
#  active_directory                   :boolean          default(FALSE)
#  alias_unit                         :string(255)      default("Unidad,Unidades")
#  upper_course_name                  :boolean          default(FALSE)
#  jobs_extra_info                    :boolean          default(TRUE)
#  is_security                        :boolean          default(FALSE)
#  iv                                 :binary
#  second_last_name                   :boolean          default(FALSE)
#  active_directory_auto_redirection  :boolean          default(TRUE)
#  email_altas                        :string(255)
#  email_bajas                        :string(255)
#

require 'valid_email'
require 'openssl'

class Company < AdminDatabase

  attr_accessible :codigo, :directorio, :nombre, :email_mesa_ayuda, :modulo_sence,
                  :show_header_in_login, :texto_username_login, :texto_cambio_password,
                  :alias_username, :alias_user, :time_zone, :display_code_in_user_profile,
                  :header_text_login_box, :recover_pass_text_login_box, :user_profile_tab_text_dashboard,
                  :locale, :training_type_alias, :training_type_user_courses_submenu,
                  :notifications_email, :accepts_values, :accepts_comments,
                  :url, :connection_string, :active, :is_productive, :use_pgp, :year_user_courses_submenu,
                  :active_directory, :alias_unit, :upper_course_name, :jobs_extra_info, :is_security,
                  :iv, :second_last_name, :email_altas, :email_bajas

  before_save { self.email_mesa_ayuda.downcase! unless self.email_mesa_ayuda.nil? }
  before_save { self.email_mesa_ayuda = nil if self.email_mesa_ayuda.blank? }
  before_save { self.texto_username_login = nil if self.texto_username_login.blank?}
  before_save { self.alias_username = nil if self.alias_username.blank?}
  before_save { self.alias_user = nil if self.alias_user.blank?}
  before_save 'set_iv'


  validates :email_mesa_ayuda, email: true,  allow_blank: true

  default_scope order('nombre ASC')

  def jm_has_terms_conditions?
    File.exist?(jm_term_conditions_path)
  end

  def jm_term_conditions_path
    # por lo pronto, está en duro...
    self.directorio + '/' + self.codigo + '/jm_terms_conditions/current.pdf'
  end

  def set_iv
    if self.iv.nil?
      cipher = OpenSSL::Cipher::AES256.new :CBC
      cipher.encrypt
      self.iv = cipher.random_iv
    end
  end

end
