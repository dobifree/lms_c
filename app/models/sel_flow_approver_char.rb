# == Schema Information
#
# Table name: sel_flow_approver_chars
#
#  id                   :integer          not null, primary key
#  sel_flow_approver_id :integer
#  characteristic_id    :integer
#  match_type           :integer
#  match_value_text     :text
#  match_value_date     :datetime
#  match_value_string   :string(255)
#  match_value_int      :integer
#  match_value_char_id  :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  match_value_float    :float
#

class SelFlowApproverChar < ActiveRecord::Base
  belongs_to :sel_flow_approver
  belongs_to :characteristic
  belongs_to :match_value_char, :class_name => 'CharacteristicValue'

  attr_accessible :match_type, :match_value_char_id, :match_value_date, :match_value_int, :match_value_string, :match_value_text,
                  :characteristic_id, :match_value_float

  validates :characteristic_id, :presence => true
  validates :match_type, :presence => true
  validates :match_value_float, :presence => true, :if => 'characteristic.data_type_id == 4'
  validates :match_value_int, :presence => true, :if => 'characteristic.data_type_id == 3'
  validates :match_value_text, :presence => true, :if => 'characteristic.data_type_id == 1'
  validates :match_value_string, :presence => true, :if => 'characteristic.data_type_id == 0'
  validates :match_value_char_id, :presence => true, :if => 'characteristic.data_type_id == 5'
  validates :match_value_date, :presence => true, :if => 'characteristic.data_type_id == 2'


  def self.options_match_type
    ['Valor específico', 'Mismo valor de vacante']
  end

  def option_match_type
    SelFlowApproverChar.options_match_type[self.match_type]
  end

  def match_value_formatted
    if match_type == 0
      case characteristic.data_type_id
        when 0
          match_value_string
        when 1
          match_value_text
        when 2
          match_value_date.strftime('%d/%m/%Y')
        when 3
          match_value_int.to_s
        when 4
          match_value_float.to_s
        when 5
          CharacteristicValue.find(match_value_char_id).value_string
      end
    else
      ''
    end

  end

end
