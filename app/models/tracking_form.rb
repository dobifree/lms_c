# == Schema Information
#
# Table name: tracking_forms
#
#  id                              :integer          not null, primary key
#  tracking_process_id             :integer
#  position                        :integer
#  name                            :string(255)
#  description                     :text
#  traceable_by_attendant          :boolean
#  traceable_by_subject            :boolean
#  track_validation_by_counterpart :boolean
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  label_unavailable               :text
#

class TrackingForm < ActiveRecord::Base
  belongs_to :tracking_process
  has_many :tracking_form_actions, :dependent => :destroy
  has_many :tracking_form_states, :dependent => :destroy
  has_many :tracking_form_groups, :dependent => :destroy
  has_many :tracking_form_instances, :dependent => :destroy
  has_many :tracking_form_questions, :through => :tracking_form_groups
  attr_accessible :description, :name, :position, :label_unavailable, :track_validation_by_counterpart, :traceable_by_attendant, :traceable_by_subject, :tracking_process_id


  validates :position, :numericality => true
  validates :name, :presence => true
  validates :tracking_process_id, :presence => true

  default_scope { order(:position, :name) }

  scope :traceable, -> { where('traceable_by_attendant = 1 OR traceable_by_subject = 1') }


  def is_traceable?
    self.traceable_by_attendant? || self.traceable_by_subject?
  end
end
