# == Schema Information
#
# Table name: evaluations
#
#  id                           :integer          not null, primary key
#  nombre                       :string(255)
#  descripcion                  :text
#  program_id                   :integer
#  level_id                     :integer
#  course_id                    :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  libre                        :boolean          default(TRUE)
#  numero                       :integer
#  activa                       :boolean          default(TRUE)
#  antes_de_unit_id             :integer
#  despues_de_unit_id           :integer
#  antes_de_evaluation_id       :integer
#  despues_de_evaluation_id     :integer
#  antes_de_poll_id             :integer
#  despues_de_poll_id           :integer
#  orden                        :integer          default(1)
#  master_evaluation_id         :integer
#  numero_preguntas             :integer
#  visualizacion_lote           :boolean
#  visualizar_resultado_detalle :boolean
#  visualizar_resultado_resumen :boolean
#  tiempo                       :integer
#  aplicar_tiempo               :boolean
#  nota_base                    :integer          default(0)
#  can_be_paused                :boolean          default(FALSE)
#  weight                       :integer          default(1)
#  virtual                      :boolean          default(TRUE)
#

class Evaluation < ActiveRecord::Base
  
  belongs_to :program
  belongs_to :level
  belongs_to :course
  belongs_to :master_evaluation

  has_many :user_course_evaluations

  belongs_to :antes_de_unit, class_name: 'Unit'#, foreign_key: "antes_de_unit_id"
  has_many :despues_de_units, class_name: 'Unit', foreign_key: 'antes_de_evaluation_id', dependent: :nullify

  belongs_to :despues_de_unit, class_name: 'Unit'#, foreign_key: "despues_de_unit_id"
  has_many :antes_de_units, class_name: 'Unit', foreign_key: 'despues_de_evaluation_id', dependent: :nullify

  belongs_to :antes_de_evaluation, class_name: 'Evaluation'#, foreign_key: "antes_de_evaluation_id"
  has_many :despues_de_evaluations, class_name: 'Evaluation', foreign_key: 'antes_de_evaluation_id', dependent: :nullify

  belongs_to :despues_de_evaluation, class_name: 'Evaluation'#, foreign_key: "despues_de_evaluation_id"
  has_many :antes_de_evaluations, class_name: 'Evaluation', foreign_key: 'despues_de_evaluation_id', dependent: :nullify

  belongs_to :antes_de_poll, class_name: 'Poll'#, foreign_key: "antes_de_poll_id"
  has_many :despues_de_polls, class_name: 'Poll', foreign_key: 'antes_de_evaluation_id', dependent: :nullify

  belongs_to :despues_de_poll, class_name: 'Poll'#, foreign_key: "despues_de_poll_id"
  has_many :antes_de_polls, class_name: 'Poll', foreign_key: 'despues_de_evaluation_id', dependent: :nullify

  has_many :evaluation_topics

  attr_accessible :antes_de_evaluation_id, :antes_de_poll_id, :antes_de_unit_id, 
                  :aplicar_tiempo, :despues_de_evaluation_id, :despues_de_poll_id, :despues_de_unit_id,
                  :descripcion, :libre, :master_evaluation_id, :nombre, :numero, :numero_preguntas, :orden,
                  :visualizacion_lote, :visualizar_resultado_detalle, :visualizar_resultado_resumen, :tiempo,
                  :nota_base, :can_be_paused, :weight, :virtual

  validates :nombre, presence: true
  validates :numero, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :numero_preguntas, numericality: { only_integer: true, greater_than: 0 }, allow_nil: true
  validates :orden, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :tiempo, numericality: { only_integer: true, greater_than: 0 }, allow_nil: true
  validates :nota_base, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :weight, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }


  validate :numero_preguntas_adecuado
  validate :tiempo_adecuado

  default_scope order('orden ASC, numero ASC')

  def update_libre_course

    if( self.antes_de_unit_id.nil? && self.despues_de_unit_id.nil? && self.antes_de_evaluation_id.nil? && 
      self.despues_de_evaluation_id.nil? && self.antes_de_poll_id.nil? && self.despues_de_poll_id.nil? )
      self.libre = true
    else
      self.libre = false
    end

    self.save
    
  end

  def after_clean_master_evaluation
    if self.master_evaluation_id == nil

      self.numero_preguntas = nil
      self.visualizacion_lote = nil
      self.visualizar_resultado_tabla = nil
      self.visualizar_resultado_resumen = nil
      self.save

    end
  end

  def numero_preguntas_adecuado

    unless self.course.calculate_percentage

      if self.numero_preguntas

        if self.numero_preguntas <= self.master_evaluation.master_evaluation_questions.count

          if self.course.nota_minima

            p = 0

            self.master_evaluation.master_evaluation_questions.each_with_index do |q, index|

              p += q.puntaje

              break if index+1 == self.numero_preguntas

            end

            if p < self.course.nota_minima

              errors.add('El numero de preguntas', 'no permite alcanzar la nota minima.')

            end

          end

        else
          errors.add('El numero de preguntas', 'no puede ser mayor que las preguntas disponibles en el pool de preguntas.')
        end

      end

    end

  end

  def tiempo_adecuado

    if self.visualizacion_lote && self.aplicar_tiempo

      if self.tiempo.nil?

        errors.add('La duracion', 'no puede ser vacia para una visualizacion en lote.')

      end

    end

  end

  def configurada?

    configurada = false

    if self.course.calculate_percentage

      configurada = true

    else

      if self.master_evaluation && self.numero_preguntas && !self.visualizacion_lote.nil? && self.course.nota_minima

        if self.numero_preguntas <= self.master_evaluation.master_evaluation_questions.count

          p = 0

          self.master_evaluation.master_evaluation_questions.each_with_index do |q, index|

            p += q.puntaje

            break if index+1 == self.numero_preguntas

          end

          if p >= self.course.nota_minima

            configurada = true

          end



        end

      end

    end

    return configurada

  end

end
