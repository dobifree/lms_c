# == Schema Information
#
# Table name: jm_candidates
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  surname         :string(255)
#  email           :string(255)
#  phone           :string(255)
#  password_digest :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  codigo_rec_pass :string(255)
#  fecha_rec_pass  :datetime
#  internal_user   :boolean          default(FALSE)
#  user_id         :integer
#  email_to_switch :string(255)
#

class JmCandidate < ActiveRecord::Base

  belongs_to :user
  has_many :sel_applicants
  has_many :jm_candidate_answers
  has_one :jm_candidate_photo
  has_many :jm_candidate_comments
  has_many :jm_messages
  has_many :sel_applicants
  has_many :sel_invited_processes, :through => :jm_messages, :source => :sel_process
  accepts_nested_attributes_for :jm_candidate_answers, :allow_destroy => false
  accepts_nested_attributes_for :jm_candidate_photo, :allow_destroy => false

  has_secure_password

  before_save { self.email = email.downcase }
  #before_save { self.name = name.upcase }
  #before_save { self.surname = surname.upcase }

  validates :name, presence: true, length: { maximum: 255 }
  validates :surname, presence: true, length: {maximum: 255 }
  validates :email, email: true, uniqueness: { case_sensitive: false }, allow_blank: true
  validates :password, length: { minimum: 6 }, allow_blank: true


  attr_accessible :email, :name, :password, :password_confirmation, :phone, :surname,
                  :activated, :activated_at, :activation_digest, :codigo_rec_pass, :fecha_rec_pass, :internal_user,
                  :user, :user_id,
                  :jm_candidate_answers_attributes,
                  :sel_applicants_attributes

  default_scope order('surname ASC, name ASC')

  attr_accessor :full_name

  def _email
    self.internal_user? ? self.user.email : self.email
  end

  def _name
    self.internal_user? ? self.user.nombre : self.name
  end

  def _surname
    self.internal_user? ? self.user.apellidos : self.surname
  end

  def full_name
    self._name + ' ' + self._surname
  end

  def full_name_2
    self._surname+', '+self._name
  end

  def jm_profile_form
    JmProfileForm.where(:active => true).first
  end

  def my_profile_progress
    mandatory_questions = jm_profile_form.jm_profile_chars.where(mandatory: true)
    if mandatory_questions.count.zero?
      progress = -10000
    else
      mandatory_answered = jm_profile_form.jm_profile_chars.where(mandatory: true, jm_characteristic_id: jm_candidate_answers.where(active: true).pluck(:jm_characteristic_id).uniq)
      progress = mandatory_answered.count.zero? ? 0 : 100 * (mandatory_answered.count.to_f/mandatory_questions.count.to_f)
    end
    progress.to_i
  end

end
