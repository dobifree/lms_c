# == Schema Information
#
# Table name: vac_records
#
#  id                              :integer          not null, primary key
#  vac_request_id                  :integer
#  begin                           :date
#  end                             :date
#  days                            :integer
#  days_progressive                :integer
#  active                          :boolean          default(TRUE)
#  deactivated_at                  :datetime
#  deactivated_by_user_id          :integer
#  deactivated_description         :text
#  registered_at                   :datetime
#  registered_by_user_id           :integer
#  registered_manually             :boolean          default(FALSE)
#  registered_manually_description :text
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  user_id                         :integer
#

class VacRecord < ActiveRecord::Base
  belongs_to :vac_request
  belongs_to :user
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id

  attr_accessible :user_id,
                  :vac_request_id,
                  :begin,
                  :end,
                  :days,
                  :days_progressive,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :deactivated_description,
                  :registered_at,
                  :registered_by_user_id,
                  :registered_manually,
                  :registered_manually_description

  validates :user_id, presence: true, if: :registered_manually?
  validates :vac_request_id, presence: true, unless: :registered_manually?

  validates :begin, presence: true, unless: :registered_manually?
  validates :end, presence: true, unless: :registered_manually?
  validates :days, presence: true
  validates :days_progressive, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :registered_manually, inclusion: { in: [true, false] }


  validates :registered_manually_description, presence: true, if: :registered_manually?
  validates :registered_by_user_id, presence: true
  validates :registered_at, presence: true

  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?
  validates :deactivated_description, presence: true, unless: :active?

  def full_registered_by_user
    return nil unless registered_by_user
    registered_by_user.codigo.to_s+' '+registered_by_user.apellidos.to_s+', '+registered_by_user.nombre.to_s
  end

end
