# == Schema Information
#
# Table name: tracking_form_states
#
#  id               :integer          not null, primary key
#  tracking_form_id :integer
#  position         :integer
#  name             :string(255)
#  description      :text
#  color            :string(255)
#  finish_tracking  :boolean
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class TrackingFormState < ActiveRecord::Base
  belongs_to :tracking_form
  has_many :tracking_state_notifications, :dependent => :destroy
  accepts_nested_attributes_for :tracking_state_notifications, :allow_destroy => true
  attr_accessible :color, :description, :finish_tracking, :name, :position, :tracking_form_id, :tracking_state_notifications_attributes

  validates :position, :numericality => true
  validates :name, :presence => true

  default_scope { order(:position, :name) }

end
