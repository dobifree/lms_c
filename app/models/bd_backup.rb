# == Schema Information
#
# Table name: bd_backups
#
#  id                    :integer          not null, primary key
#  file_name             :string(255)
#  description           :text
#  database              :string(255)
#  specific_tables       :text
#  generated_by_admin_id :integer
#  generated_at          :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class BdBackup < AdminDatabase

  belongs_to :generated_by_admin, class_name: 'Admin'

  attr_accessible :database, :description, :file_name, :generated_at, :generated_by_admin_id, :specific_tables

  validates :file_name, uniqueness: true
end
