# == Schema Information
#
# Table name: pe_characteristics
#
#  id                                    :integer          not null, primary key
#  pe_process_id                         :integer
#  characteristic_id                     :integer
#  created_at                            :datetime         not null
#  updated_at                            :datetime         not null
#  pos_rep_assessment_status             :integer
#  active_rep_assessment_status          :boolean          default(FALSE)
#  pos_gui_cal_committee                 :integer
#  active_gui_cal_committee              :boolean          default(FALSE)
#  pos_gui_cal_member                    :integer
#  active_gui_cal_member                 :boolean          default(FALSE)
#  pos_gui_feedback                      :integer
#  active_gui_feedback                   :boolean          default(FALSE)
#  pos_rep_calibration_status            :integer
#  active_rep_calibration_status         :boolean          default(FALSE)
#  pos_rep_feedback_status               :integer
#  active_rep_feedback_status            :boolean          default(FALSE)
#  pos_rep_consolidated_final_results    :integer
#  active_rep_consolidated_final_results :boolean          default(FALSE)
#  pos_gui_validation                    :integer
#  active_gui_validation                 :boolean          default(FALSE)
#  pos_rep_detailed_final_results        :integer
#  active_rep_detailed_final_results     :boolean
#  pos_gui_selection                     :integer
#  active_gui_selection                  :boolean          default(FALSE)
#  pos_reps_configuration                :integer
#  active_reps_configuration             :boolean          default(FALSE)
#  active_segmentation                   :boolean
#

class PeCharacteristic < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :characteristic

  validates :characteristic_id, uniqueness: {scope: :pe_process_id}

  attr_accessible :pos_rep_assessment_status, :active_rep_assessment_status,
                  :pos_rep_calibration_status, :active_rep_calibration_status,
                  :pos_rep_feedback_status, :active_rep_feedback_status,
                  :pos_rep_consolidated_final_results, :active_rep_consolidated_final_results,
                  :pos_rep_detailed_final_results, :active_rep_detailed_final_results,
                  :pos_gui_cal_committee, :active_gui_cal_committee,
                  :pos_gui_cal_member, :active_gui_cal_member,
                  :pos_gui_feedback, :active_gui_feedback,
                  :pos_gui_validation, :active_gui_validation,
                  :pos_gui_selection, :active_gui_selection,
                  :pos_reps_configuration, :active_reps_configuration,
                  :active_segmentation

end
