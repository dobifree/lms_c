# == Schema Information
#
# Table name: lms_characteristics
#
#  id                    :integer          not null, primary key
#  characteristic_id     :integer
#  active_search_people  :boolean          default(FALSE)
#  pos_search_people     :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  active_reporting      :boolean          default(FALSE)
#  pos_reporting         :integer
#  pos_reporting_acta    :integer
#  active_reporting_acta :boolean          default(FALSE)
#  pos_unit              :integer
#  active_unit           :boolean          default(FALSE)
#

class LmsCharacteristic < ActiveRecord::Base

  belongs_to :characteristic

  validates :characteristic_id, uniqueness: true

  attr_accessible :active_search_people, :pos_search_people, :active_reporting, :pos_reporting,
                  :pos_reporting_acta, :active_reporting_acta, :pos_reporting_filtro, :active_reporting_filtro,
                  :pos_unit, :active_unit

  def self.search_people
    LmsCharacteristic.where('active_search_people = ?', true).order('pos_search_people')
  end

  def self.reporting
    LmsCharacteristic.where('active_reporting = ?', true).order('pos_reporting')
  end

  def self.reporting_acta
    LmsCharacteristic.where('active_reporting_acta = ?', true).order('pos_reporting_acta')
  end

  def self.by_characterisctic(characteristic)
    #LmsCharacteristic.where('characteristic_id = ?', characteristic.id).first
    LmsCharacteristic.where(:characteristic_id => characteristic.id).first_or_create!
  end

  def self.unit
    LmsCharacteristic.where('active_unit = ?', true).order('pos_unit')
  end

  def name
    self.characteristic.nombre
  end

end
