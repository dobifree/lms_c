# == Schema Information
#
# Table name: elec_round_winners
#
#  id                       :integer          not null, primary key
#  elec_round_id            :integer
#  elec_process_category_id :integer
#  user_id                  :integer
#  registered_by_user_id    :integer
#  registered_at            :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class ElecRoundWinner < ActiveRecord::Base
  belongs_to :elec_round
  belongs_to :user
  belongs_to :elec_process_category
  belongs_to :registered_by_user, class_name: 'User'
  attr_accessible :registered_at, :registered_by_user_id, :elec_round_id, :user_id, :elec_process_category_id
end
