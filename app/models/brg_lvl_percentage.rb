# == Schema Information
#
# Table name: brg_lvl_percentages
#
#  id                         :integer          not null, primary key
#  jj_characteristic_value_id :integer
#  max_prt_bonus              :float
#  pond_company               :float
#  pond_individual            :float
#  registered_at              :datetime
#  registered_by_user_id      :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  brg_process_id             :integer
#  income_times_target        :float
#

class BrgLvlPercentage < ActiveRecord::Base
  belongs_to :jj_characteristic_value
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  belongs_to :brg_process

  attr_accessible :max_prt_bonus,
                  :pond_company,
                  :pond_individual,
                  :registered_at,
                  :registered_by_user_id,
                  :jj_characteristic_value_id,
                  :brg_process_id,
                  :income_times_target

  validates :max_prt_bonus, presence: true
  validates :pond_company, presence: true
  validates :pond_individual, presence: true
  validates :jj_characteristic_value_id, presence: true
  validates :income_times_target, presence: true, numericality: { greater_than: 0 }

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true
  validate :sum_ponds

  def self.by_process(process_id); where(brg_process_id: process_id) end

  def self.by_user(user_id)
    user = User.find(user_id)
    user_value = user.jj_characteristic_value(JjCharacteristic.brc_characteristic)
    return where('jj_characteristic_value_id is null') unless user_value
    where(jj_characteristic_value_id: user_value)
  end

  def self.gimme_by_value(this_value, this_process_id)
    return unless this_value
    return unless this_process_id
    where(jj_characteristic_value_id: this_value, brg_process_id: this_process_id)
  end

  def max_prt_bonus_formatted

  end

  private

  def sum_ponds
    return unless pond_company && pond_individual
    return unless (pond_company + pond_individual).to_f != 1.to_f
    errors.add(:pond_company, 'debe sumar 1 con Pond Cumpl Individual')
  end
end
