# == Schema Information
#
# Table name: jm_candidate_comments
#
#  id                    :integer          not null, primary key
#  jm_candidate_id       :integer
#  comment               :text
#  registered_at         :datetime
#  registered_by_user_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class JmCandidateComment < ActiveRecord::Base
  belongs_to :jm_candidate
  belongs_to :registered_by_user, :class_name => 'User'

  attr_accessible :comment, :registered_at, :registered_by_user_id
end
