# == Schema Information
#
# Table name: pe_processes
#
#  id                                                     :integer          not null, primary key
#  name                                                   :string(255)
#  period                                                 :string(255)
#  created_at                                             :datetime         not null
#  updated_at                                             :datetime         not null
#  two_dimensions                                         :boolean          default(TRUE)
#  from_date                                              :datetime
#  to_date                                                :datetime
#  active                                                 :boolean          default(FALSE)
#  has_step_calibration                                   :boolean          default(FALSE)
#  has_step_feedback                                      :boolean          default(FALSE)
#  has_step_feedback_accepted                             :boolean          default(FALSE)
#  step_feedback_show_names                               :boolean
#  step_feedback_accepted_send_mail                       :boolean
#  rep_detailed_final_results_elements                    :string(255)
#  of_persons                                             :boolean          default(TRUE)
#  has_step_definition_by_users                           :boolean          default(FALSE)
#  has_step_tracking                                      :boolean          default(FALSE)
#  has_step_validation                                    :boolean          default(FALSE)
#  has_step_assessment                                    :boolean          default(FALSE)
#  description                                            :string(255)
#  rep_detailed_final_results_show_boss                   :boolean
#  rep_detailed_final_results_show_feedback               :string(255)
#  rep_detailed_final_results_show_calibration            :string(255)
#  step_assessment_show_rep_dfr_to_boss                   :boolean          default(FALSE)
#  step_assessment_show_rep_dfr_to_boss_text              :text
#  step_feedback_show_rep_dfr                             :boolean
#  step_feedback_show_rep_dfr_text                        :text
#  step_feedback_show_rep_dfr_pdf                         :boolean
#  step_feedback_show_calibration                         :boolean          default(FALSE)
#  rep_consolidated_final_results_col_boss                :boolean          default(FALSE)
#  has_step_my_results                                    :boolean          default(FALSE)
#  display_results_in_user_profile                        :boolean          default(FALSE)
#  display_results_in_my_people                           :boolean          default(FALSE)
#  display_user_profile_show_rep_dfr                      :boolean          default(FALSE)
#  display_my_people_show_rep_dfr                         :boolean          default(FALSE)
#  display_user_profile_show_rep_dfr_pdf                  :boolean          default(FALSE)
#  display_my_people_show_rep_dfr_pdf                     :boolean          default(FALSE)
#  has_rep_detailed_final_results_only_boss               :boolean          default(FALSE)
#  rep_detailed_final_results_only_boss_show_boss         :boolean          default(FALSE)
#  rep_detailed_final_results_only_boss_elements          :string(255)
#  rep_detailed_final_results_only_boss_show_calibration  :string(255)
#  rep_detailed_final_results_only_boss_show_feedback     :string(255)
#  step_definition_by_users_alias                         :string(255)      default("Definir")
#  step_tracking_alias                                    :string(255)      default("Seguimiento de")
#  step_assessment_alias                                  :string(255)      default("Evaluar")
#  step_calibration_alias                                 :string(255)      default("Calibrar")
#  step_feedback_alias                                    :string(255)      default("Retroalimentar")
#  step_definition_by_users_img                           :string(255)
#  step_tracking_img                                      :string(255)
#  step_assessment_img                                    :string(255)
#  has_step_definition_by_users_accepted                  :boolean          default(FALSE)
#  has_step_definition_by_users_validated                 :boolean          default(FALSE)
#  mailer_name                                            :string(255)
#  has_step_my_subs_results                               :boolean          default(FALSE)
#  public_pe_members_box                                  :boolean          default(TRUE)
#  public_pe_members_dimension_name                       :boolean          default(TRUE)
#  step_assessment_show_final_results_to_boss             :boolean          default(FALSE)
#  step_assessment_show_final_results_to_boss_bf          :boolean          default(FALSE)
#  step_validation_send_email_when_accept                 :boolean          default(FALSE)
#  step_validation_send_email_when_reject                 :boolean          default(FALSE)
#  step_feedback_accepted_show_rep_dfr                    :boolean
#  step_feedback_accepted_show_rep_dfr_text               :text
#  public_pe_box                                          :boolean          default(TRUE)
#  public_pe_dimension_name                               :boolean          default(TRUE)
#  manage_dates                                           :boolean          default(FALSE)
#  manage_definition                                      :boolean          default(FALSE)
#  manage_enter_by_manager                                :boolean          default(FALSE)
#  manage_evaluations                                     :boolean          default(FALSE)
#  manage_calibration                                     :boolean          default(FALSE)
#  manage_feedback                                        :boolean          default(FALSE)
#  rep_detailed_final_results_show_feedback_provider      :boolean
#  has_step_query_definitions                             :boolean          default(FALSE)
#  step_query_definitions_alias                           :string(255)      default("Consultar definición")
#  rep_detailed_final_results_show_rel_weight             :boolean          default(FALSE)
#  has_step_selection                                     :boolean          default(FALSE)
#  step_selection_alias                                   :string(255)      default("Seleccionar")
#  step_assessment_send_email_to_validator                :boolean          default(FALSE)
#  step_assessment_finish_message                         :string(255)      default("Confirmo que he finalizado la evaluación")
#  step_validation_ok_message                             :string(255)      default("Confirmo que valido la evaluación")
#  step_validation_no_message                             :string(255)      default("No estoy de acuerdo con la evaluación realizada")
#  step_feedback_accepted_alias                           :string(255)      default("Confirmar retroalimentación")
#  step_feedback_accepted_mail_to_boss                    :boolean
#  step_feedback_finish_message                           :string(255)      default("Confirmo que he finalizado la retroalimentación.")
#  step_feedback_accepted_finish_message                  :string(255)      default("Confirmo que he recibido la retroalimentación y la información proporcionada es correcta.")
#  rep_detailed_final_results_show_feedback_date          :boolean          default(FALSE)
#  rep_detailed_final_results_show_feedback_accepted_date :boolean          default(FALSE)
#  step_assessment_show_rep_dfr_to_boss_pdf               :boolean          default(FALSE)
#  step_feed_show_rep_dfr_to_boss                         :boolean          default(FALSE)
#  step_feed_show_rep_dfr_to_boss_pdf                     :boolean          default(FALSE)
#  step_feed_show_rep_dfr_to_boss_text                    :text
#  step_feedback_accepted_show_rep_dfr_pdf                :boolean          default(FALSE)
#  step_assessment_final_result_alias                     :string(255)      default("Valoración final")
#  has_step_calibration_evaluation                        :boolean
#  step_calibration_evaluation_alias                      :string(255)      default("Calibrar")
#  has_step_query_results                                 :boolean          default(FALSE)
#  step_query_results_alias                               :string(255)      default("Consultar resultados")
#  step_assessment_can_finish                             :boolean          default(TRUE)
#  step_assessment_summary_group                          :boolean          default(FALSE)
#  step_feedback_summary                                  :boolean          default(TRUE)
#  step_query_show_summary                                :boolean          default(FALSE)
#  step_query_definitions_accept                          :boolean          default(FALSE)
#  step_query_definitions_accept_mail                     :boolean
#  step_query_definitions_accept_mail_to_boss             :boolean
#  step_assessment_show_calculus                          :boolean          default(TRUE)
#  step_assessment_uniq                                   :boolean
#  step_feedback_uniq                                     :boolean
#  step_assessment_menu                                   :boolean          default(TRUE)
#  step_feedback_menu                                     :boolean          default(TRUE)
#  step_assess_uniq_show_temp_res                         :boolean
#  has_step_assign                                        :boolean          default(FALSE)
#  step_assign_alias                                      :string(255)      default("Asignar evaluadores")
#  step_assign_min                                        :integer          default(1)
#  step_assign_max                                        :integer          default(1)
#  step_validation_uniq                                   :boolean
#  step_validation_menu                                   :boolean          default(TRUE)
#  step_assessment_cols_evals                             :boolean          default(TRUE)
#  step_assessment_back_list                              :boolean          default(FALSE)
#  manage_calibration_e                                   :boolean
#  step_assessment_show_results                           :boolean          default(TRUE)
#

class PeProcess < ActiveRecord::Base

  has_many :pe_rels #
  has_many :pe_dimensions #
  has_many :pe_evaluations #
  has_many :pe_evaluation_groups

  has_many :pe_elements, through: :pe_evaluations #

  has_many :pe_groups, through: :pe_evaluations #

  has_many :pe_group2s #
  has_many :pe_step_feedback_group2s #

  has_many :pe_boxes #

  has_many :pe_members #
  has_many :pe_member_rels #
  has_many :pe_member_observers #
  has_many :pe_areas #

  has_many :feedback_providers, through: :pe_members #
  has_many :validators, through: :pe_member_rels #

  has_many :pe_assessment_dimensions #
  has_many :pe_assessment_final_evaluations #
  has_many :pe_assessment_evaluations #
  has_many :pe_assessment_groups

  has_many :pe_managers
  has_many :pe_reporters

  has_many :pe_characteristics
  has_many :pe_member_characteristics

  has_many :pe_cal_sessions #
  has_many :pe_cal_committees #
  has_many :pe_cal_members #

  has_many :pe_feedback_field_lists #
  has_many :pe_feedback_fields #
  has_many :pe_feedback_accepted_fields #

  has_one :pe_process_notification
  has_many :pe_process_notification_defs
  has_many :pe_process_notification_def_vals

  has_one :pe_process_reports
  has_many :pe_process_reports_evaluations

  has_many :pe_process_tutorials



  accepts_nested_attributes_for :pe_process_notification
  accepts_nested_attributes_for :pe_process_reports
  accepts_nested_attributes_for :pe_process_reports_evaluations

  attr_accessible :name, :period, :description, :of_persons, :two_dimensions, :from_date, :to_date, :active,
                  :has_step_selection,
                  :has_step_query_definitions,
                  :has_step_definition_by_users,
                  :has_step_tracking,
                  :has_step_assessment,
                  :step_feed_show_rep_dfr_to_boss,
                  :step_feed_show_rep_dfr_to_boss_pdf,
                  :step_feed_show_rep_dfr_to_boss_text,
                  :step_assessment_show_rep_dfr_to_boss,
                  :step_assessment_show_rep_dfr_to_boss_pdf,
                  :step_assessment_show_rep_dfr_to_boss_text,
                  :step_assessment_show_final_results_to_boss,
                  :step_assessment_show_final_results_to_boss_bf,
                  :step_assessment_finish_message,
                  :step_feedback_show_rep_dfr_pdf,
                  :step_feedback_show_rep_dfr,
                  :step_feedback_show_rep_dfr_text,
                  :step_feedback_accepted_show_rep_dfr,
                  :step_feedback_accepted_show_rep_dfr_text,
                  :step_feedback_accepted_show_rep_dfr_pdf,
                  :has_step_validation,
                  :step_assessment_send_email_to_validator,
                  :step_validation_send_email_when_accept,
                  :step_validation_send_email_when_reject,
                  :step_validation_ok_message,
                  :step_validation_no_message,
                  :has_step_calibration,
                  :has_step_feedback,
                  :step_feedback_show_names,
                  :step_feedback_show_calibration,
                  :has_step_feedback_accepted,
                  :has_step_my_results,
                  :has_step_my_subs_results,
                  :step_feedback_accepted_send_mail,
                  :step_feedback_accepted_mail_to_boss,
                  :rep_detailed_final_results_elements,
                  :rep_detailed_final_results_show_boss,
                  :rep_detailed_final_results_show_feedback_provider,
                  :rep_detailed_final_results_show_feedback,
                  :rep_detailed_final_results_show_calibration,
                  :rep_consolidated_final_results_col_boss,
                  :rep_detailed_final_results_show_rel_weight,
                  :display_results_in_user_profile,
                  :display_results_in_my_people,
                  :display_user_profile_show_rep_dfr,
                  :display_user_profile_show_rep_dfr_pdf,
                  :display_my_people_show_rep_dfr,
                  :display_my_people_show_rep_dfr_pdf,
                  :has_rep_detailed_final_results_only_boss,
                  :rep_detailed_final_results_only_boss_show_boss,
                  :rep_detailed_final_results_only_boss_elements,
                  :rep_detailed_final_results_only_boss_show_calibration,
                  :rep_detailed_final_results_only_boss_show_feedback,
                  :step_definition_by_users_alias,
                  :step_tracking_alias,
                  :step_assessment_alias,
                  :step_calibration_alias,
                  :step_feedback_alias,
                  :step_feedback_accepted_alias,
                  :step_query_definitions_alias,
                  :step_selection_alias,
                  :step_definition_by_users_img,
                  :step_tracking_img,
                  :step_assessment_img,
                  :has_step_definition_by_users_accepted,
                  :has_step_definition_by_users_validated,
                  :mailer_name,
                  :public_pe_box,
                  :public_pe_dimension_name,
                  :public_pe_members_box,
                  :public_pe_members_dimension_name,
                  :manage_dates, :manage_definition, :manage_enter_by_manager, :manage_evaluations,
                  :manage_calibration, :manage_feedback,
                  :pe_process_notification_attributes,
                  :pe_process_reports_attributes,
                  :pe_process_reports_evaluations_attributes,
                  :step_feedback_finish_message, :step_feedback_accepted_finish_message,
                  :rep_detailed_final_results_show_feedback_date, :rep_detailed_final_results_show_feedback_accepted_date,
                  :step_assessment_final_result_alias,
                  :has_step_calibration_evaluation,
                  :step_calibration_evaluation_alias,
                  :has_step_query_results, :step_query_results_alias,
                  :step_assessment_can_finish,
                  :step_assessment_summary_group,
                  :step_feedback_summary,
                  :step_query_show_summary,
                  :step_query_definitions_accept,
                  :step_query_definitions_accept_mail,
                  :step_query_definitions_accept_mail_to_boss,
                  :step_assessment_show_calculus,
                  :step_assessment_uniq,
                  :step_feedback_uniq, :step_assessment_menu, :step_feedback_menu,
                  :step_assess_uniq_show_temp_res,
                  :has_step_assign,
                  :step_assign_alias, :step_assign_min, :step_assign_max,
                  :step_validation_uniq, :step_validation_menu,
                  :step_assessment_cols_evals, :step_assessment_back_list, :manage_calibration_e,
                  :step_assessment_show_results



  before_save :correct_to_date
  before_update :correct_has_step_feedback_accepted
  after_create :create_pe_process_notification
  after_create :create_pe_process_reports

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :period }
  validates :period, presence: true
  validates :from_date, presence: true
  validates :to_date, presence: true

  default_scope order('active DESC, period DESC, name ASC')

  def create_pe_process_notification

    pe_process_notification = self.build_pe_process_notification
    pe_process_notification.save

  end

  def create_pe_process_reports

    pe_process_reports = self.build_pe_process_reports
    pe_process_reports.save

  end

  def correct_has_step_feedback_accepted
     if self.has_step_feedback?
     else
       #self.has_step_feedback_accepted = false
     end
  end

  def one_dimension?
    !self.two_dimensions?
  end

  def dimension_x
    self.pe_dimensions.where('dimension = ?', 1).first
  end

  def dimension_y
    self.pe_dimensions.where('dimension = ?', 2).first
  end

  def pe_box_2d(pe_dim_group_x_id, pe_dim_group_y_id)
    self.pe_boxes.where('pe_dim_group_x_id = ? AND pe_dim_group_y_id = ?', pe_dim_group_x_id, pe_dim_group_y_id).first
  end

  def pe_box_1d(pe_dim_group_x_id)
    self.pe_boxes.where('pe_dim_group_x_id = ? AND pe_dim_group_y_id IS NULL', pe_dim_group_x_id).first
  end

  def  correct_to_date
    self.to_date = self.to_date+((24*60*60)-1).seconds if self.to_date
  end

  def pe_member_by_user(user)
    self.pe_members.where('user_id = ?', user.id).first
  end

  def pe_member_by_user_code(user_code)
    self.pe_members.joins(:user).where('codigo = ?', user_code).readonly(false).first
  end

  def pe_members_by_user(user)
    self.pe_members.where('user_id = ?', user.id)
  end

  def pe_evaluated_member_by_user(user)
    self.pe_members.where('user_id = ? AND is_evaluated = ?', user.id, true).first
  end

  def pe_evaluated_member_by_user_code(user_code)
    self.pe_members.joins(:user).where('codigo = ? AND is_evaluated = ?', user_code, true).readonly(false).first
  end

  def pe_evaluator_member_by_user_code(user_code)
    self.pe_members.joins(:user).where('codigo = ? AND is_evaluator = ?', user_code, true).readonly(false).first
  end

  def pe_evaluated_user_codes
    self.pe_members.joins(:user).where('is_evaluated = ?', true).pluck(:codigo)
  end

  def pe_evaluated_members_by_user(user)
    self.pe_members.where('user_id = ? AND is_evaluated = ?', user.id, true)
  end

  def pe_members_ordered
    self.pe_members.joins(:user).reorder('apellidos, nombre')
  end

  def search_members_ordered(code, last_name, first_name, allow_blank = false, characteristics = nil)

    pe_members = Array.new

    if allow_blank || (!allow_blank && (!code.blank? || !last_name.blank? || !first_name.blank?))

      pe_member_evaluated_ids = nil

      if characteristics

        query = ''
        nc = 0

        characteristics.each do |cha_id,value|

          query += ' UNION ' if nc > 0

          query += PeMemberCharacteristic.select('pe_member_id, value').
              where('characteristic_id = ? AND value = ?', cha_id, value).to_sql

          nc += 1

        end

        pe_member_evaluated_ids = ActiveRecord::Base.connection.execute('SELECT pe_member_id, count(*) FROM ('+query+') as temp group by pe_member_id having count(*) = '+characteristics.size.to_s).map {|r| r[0]} unless query.blank?

      end

      if pe_member_evaluated_ids

        pe_members = self.pe_members_ordered.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND pe_members.id IN (?)',
            '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%', pe_member_evaluated_ids)

      else

        pe_members = self.pe_members_ordered.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%')

      end


    end

    return pe_members

  end

  def can_be_selected_as_evaluated_members_ordered
    self.pe_members.joins(:user).where('can_be_selected_as_evaluated = ?', true).reorder('apellidos, nombre')
  end

  def evaluated_members
    self.pe_members.where('is_evaluated = ?', true)
  end

  def evaluated_members_ordered
    self.pe_members.joins(:user).where('is_evaluated = ?', true).reorder('apellidos, nombre')
  end

  def evaluated_members_segmented_to_report(user_reporter, pe_members_ids = nil)

    pe_members = Array.new

    pe_reporter = self.pe_reporters.where('user_id = ?', user_reporter.id).first

    pe_manager = self.pe_managers.where('user_id = ?', user_reporter.id).first

    if pe_reporter || pe_manager

      if pe_manager || (pe_reporter && pe_reporter.general)

        if pe_members_ids
          pe_members = self.pe_members.joins(:user).where('is_evaluated = ? AND removed = ? AND pe_members.id IN (?)', true, false, pe_members_ids).reorder('apellidos, nombre')
        else
          pe_members = self.pe_members.joins(:user).where('is_evaluated = ? AND removed = ? ', true, false).reorder('apellidos, nombre')
        end

      elsif pe_reporter && !pe_reporter.general

        queries = []

        n_q = 0

        pe_reporter.pe_reporter_characteristic_values.each do |prcv|

          queries[n_q] = 'pe_member_characteristics.characteristic_id = '+prcv.pe_characteristic.characteristic.id.to_s+' AND pe_member_characteristics.value = "'+prcv.value+'" '
          n_q += 1

        end

        pe_members += self.evaluated_members_ordered.where('pe_members.id IN (?)', pe_members_ids) if pe_members_ids

        queries.each_with_index do |query, index|

          pe_members += self.evaluated_members_ordered.joins(:pe_member_characteristics).where(query)

        end

      end

    end

    pe_members.uniq!

    return pe_members.sort_by {|m| m.user.apellidos+m.user.nombre}

  end

  def removed_members_segmented_to_report(user_reporter, pe_members_ids = nil)

    pe_members = Array.new

    pe_reporter = self.pe_reporters.where('user_id = ?', user_reporter.id).first

    pe_manager = self.pe_managers.where('user_id = ?', user_reporter.id).first

    if pe_reporter || pe_manager

      if pe_manager || (pe_reporter && pe_reporter.general)

        if pe_members_ids
          pe_members = self.pe_members.joins(:user).where('is_evaluated = ? AND removed = ? AND pe_members.id IN (?)', false, true, pe_members_ids).reorder('apellidos, nombre')
        else
          pe_members = self.pe_members.joins(:user).where('is_evaluated = ? AND removed = ?', false, true).reorder('apellidos, nombre')
        end

      elsif pe_reporter && !pe_reporter.general

        queries = []

        n_q = 0

        pe_reporter.pe_reporter_characteristic_values.each do |prcv|

          queries[n_q] = 'pe_member_characteristics.characteristic_id = '+prcv.pe_characteristic.characteristic.id.to_s+' AND pe_member_characteristics.value = "'+prcv.value+'" '
          n_q += 1

        end

        pe_members += self.removed_members_ordered.where('pe_members.id IN (?)', pe_members_ids) if pe_members_ids

        queries.each_with_index do |query, index|

          pe_members += self.removed_members_ordered.joins(:pe_member_characteristics).where(query)

        end

      end

    end

    pe_members.uniq!

    return pe_members.sort_by {|m| m.user.apellidos+m.user.nombre}

  end

  def removed_members_ordered
    self.pe_members.joins(:user).where('is_evaluated = ? AND removed = ?', false, true).reorder('apellidos, nombre')
  end

  def evaluated_areas
    self.pe_areas.where('is_evaluated = ?', true)
  end

  def evaluated_members_ordered_by_pe_area
    self.pe_members.joins(:pe_area).where('pe_members.is_evaluated = ?', true).reorder('pe_areas.name')
  end

  def search_can_be_selected_as_evaluated_members_ordered(code, last_name, first_name, allow_blank = false, characteristics = nil)

    pe_members = Array.new

    if allow_blank || (!allow_blank && (!code.blank? || !last_name.blank? || !first_name.blank?))

      pe_member_evaluated_ids = nil

      if characteristics

        query = ''
        nc = 0

        characteristics.each do |cha_id,value|

          query += ' UNION ' if nc > 0

          query += PeMemberCharacteristic.select('pe_member_id, value').
              where('characteristic_id = ? AND value = ?', cha_id, value).to_sql

          nc += 1

        end

        pe_member_evaluated_ids = ActiveRecord::Base.connection.execute('SELECT pe_member_id, count(*) FROM ('+query+') as temp group by pe_member_id having count(*) = '+characteristics.size.to_s).map {|r| r[0]} unless query.blank?

      end

      if pe_member_evaluated_ids

        pe_members = self.can_be_selected_as_evaluated_members_ordered.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND pe_members.id IN (?)',
            '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%', pe_member_evaluated_ids)

      else

        pe_members = self.can_be_selected_as_evaluated_members_ordered.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%')

      end


    end

    return pe_members


  end

  def search_evaluated_members_ordered(code, last_name, first_name, allow_blank = false, characteristics = nil)

    pe_members = Array.new

    if allow_blank || (!allow_blank && (!code.blank? || !last_name.blank? || !first_name.blank?))

      pe_member_evaluated_ids = nil

      if characteristics

        query = ''
        nc = 0

        characteristics.each do |cha_id,value|

          query += ' UNION ' if nc > 0

          query += PeMemberCharacteristic.select('pe_member_id, value').
            where('characteristic_id = ? AND value = ?', cha_id, value).to_sql

          nc += 1

        end

        pe_member_evaluated_ids = ActiveRecord::Base.connection.execute('SELECT pe_member_id, count(*) FROM ('+query+') as temp group by pe_member_id having count(*) = '+characteristics.size.to_s).map {|r| r[0]} unless query.blank?

      end

      if pe_member_evaluated_ids

        pe_members = self.evaluated_members_ordered.where(
          'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND pe_members.id IN (?)',
          '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%', pe_member_evaluated_ids)

      else

        pe_members = self.evaluated_members_ordered.where(
          'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
          '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%')

      end


    end

    return pe_members


  end

  def search_removed_members_ordered(code, last_name, first_name, allow_blank = false, characteristics = nil)

    pe_members = Array.new

    if allow_blank || (!allow_blank && (!code.blank? || !last_name.blank? || !first_name.blank?))

      pe_member_evaluated_ids = nil

      if characteristics

        query = ''
        nc = 0

        characteristics.each do |cha_id,value|

          query += ' UNION ' if nc > 0

          query += PeMemberCharacteristic.select('pe_member_id, value').
              where('characteristic_id = ? AND value = ?', cha_id, value).to_sql

          nc += 1

        end

        pe_member_evaluated_ids = ActiveRecord::Base.connection.execute('SELECT pe_member_id, count(*) FROM ('+query+') as temp group by pe_member_id having count(*) = '+characteristics.size.to_s).map {|r| r[0]} unless query.blank?

      end

      if pe_member_evaluated_ids

        pe_members = self.removed_members_ordered.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND pe_members.id IN (?)',
            '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%', pe_member_evaluated_ids)

      else

        pe_members = self.removed_members_ordered.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%')

      end


    end

    return pe_members


  end

  def search_evaluated_members_by_jefe_ordered(pe_member_evaluator_id, last_name, first_name, allow_blank = false, characteristics = nil)

    pe_members = nil

    pe_rel_jefe = self.pe_rel(:jefe)

    execute = false

    if pe_rel_jefe

      if allow_blank || (!allow_blank && (!last_name.blank? || !first_name.blank?))

        pe_member_evaluated_ids = self.pe_member_rels.where('pe_rel_id = ? AND pe_member_evaluator_id = ?', pe_rel_jefe.id, pe_member_evaluator_id).pluck(:pe_member_evaluated_id)

        if characteristics

          query = ''
          nc = 0

          characteristics.each do |cha_id,value|

            query += ' UNION ' if nc > 0

            query += PeMemberCharacteristic.select('pe_member_id, value').
                   where('pe_member_id IN (?) AND characteristic_id = ? AND value = ?', pe_member_evaluated_ids, cha_id, value).to_sql

            nc += 1

          end

          pe_member_evaluated_ids = ActiveRecord::Base.connection.execute('SELECT pe_member_id, count(*) FROM ('+query+') as temp group by pe_member_id having count(*) = '+characteristics.size.to_s).map {|r| r[0]} unless query.blank?

        end

        pe_members = self.evaluated_members_ordered.where(
          'apellidos LIKE ? AND nombre LIKE ? AND pe_members.id IN (?)',
          '%'+last_name+'%', '%'+first_name+'%', pe_member_evaluated_ids)

      end

    end

    return pe_members

  end

  def can_select_evaluated_members
    self.pe_members.where('can_select_evaluated = ?', true)
  end

  def evaluator_members
    self.pe_members.where('is_evaluator = ?', true)
  end

  def evaluator_members_ordered
    self.pe_members.where('is_evaluator = ?', true).joins(:user).reorder('apellidos, nombre')
  end

  def evaluator_members_as_ordered(pe_rel)
    self.evaluator_members_ordered.joins(:pe_member_rels_is_evaluator).where('pe_member_rels.pe_rel_id = ?', pe_rel.id).uniq
  end

  def search_evaluator_members_ordered(code, last_name, first_name, allow_blank = false, characteristics = nil)

    pe_members = Array.new

    if allow_blank || (!allow_blank && (!code.blank? || !last_name.blank? || !first_name.blank?))

      pe_member_evaluators_ids = nil

      if characteristics

        query = ''
        nc = 0

        characteristics.each do |cha_id,value|

          query += ' UNION ' if nc > 0

          query += PeMemberCharacteristic.select('pe_member_id, value').
              where('characteristic_id = ? AND value = ?', cha_id, value).to_sql

          nc += 1

        end

        pe_member_evaluators_ids = ActiveRecord::Base.connection.execute('SELECT pe_member_id, count(*) FROM ('+query+') as temp group by pe_member_id having count(*) = '+characteristics.size.to_s).map {|r| r[0]} unless query.blank?

      end

      if pe_member_evaluators_ids

        pe_members = self.evaluator_members_ordered.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND pe_members.id IN (?)',
            '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%', pe_member_evaluators_ids)

      else

        pe_members = self.evaluator_members_ordered.where(
            'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? ',
            '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%')

      end


    end

    return pe_members


  end

  def pe_characteristic(characteristic)
    self.pe_characteristics.where('characteristic_id = ?', characteristic.id).first
  end

  def pe_characteristics_reps_configuration
    self.pe_characteristics.where('active_reps_configuration = ?', true).reorder('pos_reps_configuration')
  end

  def pe_characteristics_rep_assessment_status
    self.pe_characteristics.where('active_rep_assessment_status = ?', true).reorder('pos_rep_assessment_status')
  end

  def pe_characteristics_rep_calibration_status
    self.pe_characteristics.where('active_rep_calibration_status = ?', true).reorder('pos_rep_calibration_status')
  end

  def pe_characteristics_rep_feedback_status
    self.pe_characteristics.where('active_rep_feedback_status = ?', true).reorder('pos_rep_feedback_status')
  end

  def pe_characteristics_rep_consolidated_final_results
    self.pe_characteristics.where('active_rep_consolidated_final_results = ?', true).reorder('pos_rep_consolidated_final_results')
  end

  def pe_characteristics_rep_detailed_final_results
    self.pe_characteristics.where('active_rep_detailed_final_results = ?', true).reorder('pos_rep_detailed_final_results')
  end

  def pe_characteristics_gui_cal_committee
    self.pe_characteristics.where('active_gui_cal_committee = ?', true).reorder('pos_gui_cal_committee')
  end

  def pe_characteristics_gui_cal_member
    self.pe_characteristics.where('active_gui_cal_member = ?', true).reorder('pos_gui_cal_member')
  end

  def pe_characteristics_gui_feedback
    self.pe_characteristics.where('active_gui_feedback = ?', true).reorder('pos_gui_feedback')
  end

  def pe_characteristics_gui_validation
    self.pe_characteristics.where('active_gui_validation = ?', true).reorder('pos_gui_validation')
  end

  def pe_characteristics_gui_selection
    self.pe_characteristics.where('active_gui_selection = ?', true).reorder('pos_gui_selection')
  end

  def pe_rel(rel)

    pe_rel = nil

    rel_id = PeRel.rel_id(rel)

    pe_rel = self.pe_rels.where('rel = ?', rel_id).first if rel_id

  end

  def pe_rel_by_id(rel_id)

    self.pe_rels.where('rel = ?', rel_id).first

  end

  def pe_rel_boss
    self.pe_rels.where('rel = ?', 1).first
  end

  def pe_rels_array_for_select

    array_for_select = Array.new

    self.pe_rels.each do |pe_rel|

      array_for_select.push [pe_rel.rel_name, pe_rel.rel]

    end

    return array_for_select

  end

=begin
  def has_to_define_questions?(user)

    if self.has_step_definition_by_users?

      pe_members = self.pe_members_by_user user

      self.pe_evaluations_defined_by_users.each do |pe_evaluation|

        rel_ids = pe_evaluation.rel_ids_to_define_elements

        pe_members.each do |pe_member|

          rel_ids.each do |rel_id|

            return true if pe_member.has_to_asses_as_rel_id rel_id

          end

        end

      end

    end

    return false

  end
=end

  def pe_evaluations_to_display_in_qdefs
    self.pe_evaluations.where('display_in_qdefs = ?', true)
  end

  def pe_evaluations_to_display_in_qres
    self.pe_evaluations.where('display_in_qres = ?', true)
  end

  def pe_evaluations_where_has_to_define_questions(user)

    pe_evaluations_array = Array.new

    pe_members = self.pe_members_by_user user

    self.pe_evaluations_defined_by_users.each do |pe_evaluation|

      rel_ids = pe_evaluation.rel_ids_to_define_elements

      pe_members.each do |pe_member|

        to_break = false

        rel_ids.each do |rel_id|

          if pe_member.has_to_asses_as_rel_id(rel_id) && pe_evaluation.has_to_be_defined_by_rel_id(rel_id)

            pe_evaluations_array.push pe_evaluation
            to_break = true
            break

          end

        end

        break if to_break

      end

    end

    return pe_evaluations_array

  end

  def pe_evaluations_where_has_to_accept_defined_questions(user)

    pe_evaluations_array = Array.new

    pe_members = self.pe_members_by_user user

    self.pe_evaluations_defined_by_users_to_accept.each do |pe_evaluation|

      rel_ids = pe_evaluation.rel_ids_to_define_elements

      pe_members.each do |pe_member|

        if pe_member.has_to_be_assessed_in_evaluation pe_evaluation

          to_break = false

          rel_ids.each do |rel_id|

            if pe_member.has_to_be_assesed_by_rel_id(rel_id) && pe_evaluation.has_rel_id(rel_id)

              pe_evaluations_array.push pe_evaluation
              to_break = true
              break

            end

          end

          break if to_break

        end

      end

    end

    return pe_evaluations_array

  end

  def pe_evaluations_where_can_watch_own_defined_questions(user)

    pe_evaluations_array = Array.new

    pe_members = self.pe_members_by_user user

    self.pe_evaluations_defined_by_users_to_watch.each do |pe_evaluation|

      rel_ids = pe_evaluation.rel_ids_to_define_elements

      pe_members.each do |pe_member|

        if pe_member.has_to_be_assessed_in_evaluation pe_evaluation

          to_break = false

          rel_ids.each do |rel_id|

            if pe_member.has_to_be_assesed_by_rel_id(rel_id) && pe_evaluation.has_rel_id(rel_id)

              pe_evaluations_array.push pe_evaluation
              to_break = true
              break

            end

          end

          break if to_break

        end

      end

    end

    return pe_evaluations_array

  end

  #def pe_Evaluations_where_can_observe_definitions(user)

  #  self.is_as_observer?(user)

    #  self.pe_evaluations

  #  end

  #end

  def pe_evaluations_where_has_to_validate_defined_questions(user)

    pe_evaluations_array = Array.new

    self.pe_evaluations_defined_by_users_to_validate.each do |pe_evaluation|

      pe_evaluations_array.push(pe_evaluation) if pe_evaluation.pe_definition_by_user_validators.where('validator_id = ?', user.id).size > 0

    end

    return pe_evaluations_array

  end

  def pe_evaluations_where_has_to_track(user)

    pe_evaluations_array = Array.new

    pe_members = self.pe_members_by_user user

    self.pe_evaluations.where('allow_tracking = ?', true).each do |pe_evaluation|

      pe_members.each do |pe_member|

        to_break = false

        pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

          if pe_member.has_to_asses_as_pe_rel(pe_evaluation_rel.pe_rel)# && pe_evaluation.has_rel_id(pe_evaluation_rel.pe_rel.rel)

            pe_evaluations_array.push pe_evaluation
            to_break = true
            break

          end

        end

        break if to_break

      end

    end

    return pe_evaluations_array

  end

  def pe_evaluations_where_has_to_be_tracked(user)

    pe_evaluations_array = Array.new

    pe_members = self.pe_members_by_user user

    self.pe_evaluations.where('allow_tracking = ? AND allow_watching_own_tracking = ?', true, true).each do |pe_evaluation|

      pe_members.each do |pe_member|

        if pe_member.has_to_be_assessed_in_evaluation pe_evaluation

          to_break = false

          pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

            if pe_member.has_to_be_assesed_as_pe_rel(pe_evaluation_rel.pe_rel)# && pe_evaluation.has_rel_id(pe_evaluation_rel.pe_rel.rel)

              pe_evaluations_array.push pe_evaluation
              to_break = true
              break

            end

          end

          break if to_break

        end

      end

    end

    return pe_evaluations_array

  end

  def pe_evaluations_defined_by_managers
    self.pe_evaluations.where('allow_definition_by_managers = ?', true)
  end

  def pe_evaluations_defined_by_users
    self.pe_evaluations.where('allow_definition_by_users = ?', true)
  end

  def pe_evaluations_defined_by_users_to_accept
    self.pe_evaluations.where('allow_definition_by_users = ? AND allow_definition_by_users_accepted = ?', true, true)
  end

  def pe_evaluations_defined_by_users_to_watch
    self.pe_evaluations.where('allow_definition_by_users = ? AND allow_watching_own_definition = ?', true, true)
  end

  def pe_evaluations_defined_by_users_to_validate
    self.pe_evaluations.where('allow_definition_by_users = ? AND (num_steps_definition_by_users_validated_before_accepted > ? || num_steps_definition_by_users_validated_after_accepted > ?)', true, 0, 0)
  end

  def is_a_pe_member_evaluated?(user)
    self.evaluated_members.where('user_id = ?', user.id).count > 0 ? true : false
  end

  def is_a_pe_member_evaluator?(user)
    self.evaluator_members.where('user_id = ?', user.id).count > 0 ? true : false
  end

  def is_a_pe_member_can_select_evaluated?(user)
    self.can_select_evaluated_members.where('user_id = ?', user.id).count > 0 ? true : false
  end

  def is_a_pe_member_evaluator_as_boss?(user)

    is_a = false

    self.pe_members_by_user(user).each do |pe_member|

      if pe_member.has_to_asses_as_rel_id(1)

        is_a = true
        break

      end

    end

    return is_a

  end

  def num_not_removed_rels_is_evaluator_as_boss(user)

    t = 0

    self.pe_members_by_user(user).each do |pe_member|

      pe_member.pe_member_rels_is_evaluator.joins(:pe_rel).where('pe_rels.rel = ?', 1).each do |pmr|
        t += 1 unless pmr.pe_member_evaluated.removed
      end

    end

    return t

  end

  def is_a_pe_cal_committee?(user)
    self.pe_cal_committees.where('user_id = ?', user.id).count > 0 ? true : false
  end

  def pe_evaluations_where_has_to_calibrate(user)
    pe_evaluations = Array.new
    self.pe_evaluations.each do |pe_evaluation|
      if pe_evaluation.allow_calibration
        pe_evaluation.pe_cal_sessions.each do |pe_cal_session|
          if pe_cal_session.pe_cal_committees.where('user_id = ?', user.id).count > 0
            pe_evaluations.push pe_evaluation
            break
          end
        end
      end
    end
    pe_evaluations.uniq
  end

  def is_a_feedback_provider?(user)
    self.feedback_providers.where('feedback_provider_id = ?', user.id).count > 0 ? true : false
  end

  def is_a_validator?(user)
    self.validators.where('validator_id = ?', user.id).count > 0 ? true : false
  end

  def is_a_observer?(user)

    self.pe_member_observers.where('observer_id = ?', user.id).count > 0 ? true : false

  end

  def is_observer_of?(user, pe_member)
    self.pe_member_observers.where('observer_id = ? AND pe_member_observed_id = ?', user.id, pe_member.id).count > 0 ? true : false
  end

  def has_to_accept_feedback?(user)
    self.has_step_feedback_accepted? && self.evaluated_members.where('user_id = ? AND step_feedback = ?', user.id, true).count > 0  ? true : false
  end

  def has_to_accept_feedback_pending?(user)
    self.has_step_feedback_accepted? && self.evaluated_members.where('user_id = ? AND step_feedback = ? AND step_feedback_accepted = ?', user.id, true, false).count == 1  ? true : false
  end

  def pe_cal_members_ordered
    self.pe_cal_members.joins(pe_member: [:user]).reorder('apellidos, nombre')
  end

  def pe_cal_members_ordered_segmented_to_report(user_reporter, pe_members_ids = nil)

    pe_members = Array.new

    pe_reporter = self.pe_reporters.where('user_id = ?', user_reporter.id).first
    pe_manager = self.pe_managers.where('user_id = ?', user_reporter.id).first

    if pe_reporter || pe_manager

      if pe_manager || (pe_reporter && pe_reporter.general)

        if pe_members_ids
          pe_members = self.pe_cal_members.joins(pe_member: [:user]).where('pe_members.id IN (?) AND pe_members.removed = ?', pe_members_ids, false).reorder('apellidos, nombre')
        else
          pe_members = self.pe_cal_members.joins(pe_member: [:user]).where('pe_members.removed = ?', false).reorder('apellidos, nombre')
        end

      elsif pe_reporter && !pe_reporter.general

        queries = []

        n_q = 0

        pe_reporter.pe_reporter_characteristic_values.each do |prcv|

          queries[n_q] = 'pe_member_characteristics.characteristic_id = '+prcv.pe_characteristic.characteristic.id.to_s+' AND pe_member_characteristics.value = "'+prcv.value+'" '
          n_q += 1

        end

        pe_members = self.pe_cal_members.joins(pe_member: [:user]).where('pe_members.id IN (?) AND pe_members.removed = ?', pe_members_ids, false).reorder('apellidos, nombre') if pe_members_ids

        queries.each_with_index do |query, index|

          pe_members += self.pe_cal_members.joins(pe_member: [:pe_member_characteristics, :user]).where(query).reorder('apellidos, nombre')

        end

      end

    end

    pe_members.uniq!

    return pe_members.sort_by {|m| m.pe_member.user.apellidos+m.pe_member.user.nombre}

  end

  def pe_members_to_feedback_ordered(user)
    self.evaluated_members_ordered.where('feedback_provider_id = ?', user.id)
  end

  def pe_members_to_feedback_pending(user)
    self.evaluated_members.where('feedback_provider_id = ? AND step_feedback = ?', user.id, false)
  end

  def pe_member_rels_to_validate(user)
    self.pe_member_rels.where('validator_id = ?', user.id)
  end

  def pe_member_rels_as_pe_rel(pe_rel)
    self.pe_member_rels.where('pe_rel_id = ?', pe_rel.id)
  end

  def pe_member_rels_to_validate_ordered(user)
    self.pe_member_rels.joins(:pe_member_evaluated => [:user]).where('validator_id = ?', user.id).reorder('apellidos, nombre')
  end

  def cal_sessions_for_pe_member_committee(user)
    self.pe_cal_sessions.joins(:pe_cal_committees).where('user_id = ?', user.id)
  end

  def rep_detailed_final_results_show_feedback_for_manager
    self.rep_detailed_final_results_show_feedback && self.rep_detailed_final_results_show_feedback.include?('gestor') ? true : false
  end

  def rep_detailed_final_results_show_feedback_only_boss_for_manager
    self.rep_detailed_final_results_only_boss_show_feedback && self.rep_detailed_final_results_only_boss_show_feedback.include?('gestor') ? true : false
  end

  def rep_detailed_final_results_show_feedback_for_user_profile
    self.rep_detailed_final_results_show_feedback && self.rep_detailed_final_results_show_feedback.include?('ficha_usuario') ? true : false
  end

  def rep_detailed_final_results_show_feedback_for_my_people
    self.rep_detailed_final_results_show_feedback && self.rep_detailed_final_results_show_feedback.include?('mis_colaboradores') ? true : false
  end

  def rep_detailed_final_results_show_calibration_for_manager
    self.rep_detailed_final_results_show_calibration && self.rep_detailed_final_results_show_calibration.include?('gestor') ? true : false
  end

  def rep_detailed_final_results_show_calibration_only_boss_for_manager
    self.rep_detailed_final_results_only_boss_show_calibration && self.rep_detailed_final_results_only_boss_show_calibration.include?('gestor') ? true : false
  end

  def rep_detailed_final_results_show_calibration_for_feedback
    self.rep_detailed_final_results_show_calibration && self.rep_detailed_final_results_show_calibration.include?('feedback') ? true : false
  end

  def rep_detailed_final_results_show_calibration_for_user_profile
    self.rep_detailed_final_results_show_calibration && self.rep_detailed_final_results_show_calibration.include?('ficha_usuario') ? true : false
  end

  def rep_detailed_final_results_show_calibration_for_my_people
    self.rep_detailed_final_results_show_calibration && self.rep_detailed_final_results_show_calibration.include?('mis_colaboradores') ? true : false
  end

  def self.pe_processes_of_persons
    self.where('of_persons = ?',true)
  end

  def pe_process_notification_def_val(before_accepted, num_step)
    self.pe_process_notification_def_vals.where('before_accepted = ? AND num_step = ?', before_accepted, num_step).first
  end

  def pe_rels_available_to_select
    self.pe_rels.where('available_in_selection = ?', true)
  end

  def pe_rels_available_to_assign
    self.pe_rels.where('available_in_assign = ?', true)
  end

  def pe_rels_assign_total_count
    self.pe_rels.where('assign_total_count = ?', true)
  end

  def pe_not_informative_evaluations
    self.pe_evaluations.where(:informative => false)
  end

end
