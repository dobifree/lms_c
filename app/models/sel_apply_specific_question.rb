# == Schema Information
#
# Table name: sel_apply_specific_questions
#
#  id                         :integer          not null, primary key
#  sel_process_id             :integer
#  position                   :integer
#  question                   :string(255)
#  description                :text
#  mandatory                  :boolean
#  show_in_list_of_candidates :boolean
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  accepts_attachment         :boolean          default(FALSE)
#

class SelApplySpecificQuestion < ActiveRecord::Base
  belongs_to :sel_process
  attr_accessible :description, :mandatory, :position, :question, :show_in_list_of_candidates, :accepts_attachment

  validates :position, :numericality => true
  validates :question, :presence => true

  default_scope order('position ASC')
end
