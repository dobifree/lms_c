# == Schema Information
#
# Table name: user_courses
#
#  id                         :integer          not null, primary key
#  enrollment_id              :integer
#  program_course_id          :integer
#  desde                      :datetime
#  hasta                      :datetime
#  limite_tiempo              :boolean
#  inicio                     :datetime
#  fin                        :datetime
#  numero_oportunidad         :integer
#  iniciado                   :boolean          default(FALSE)
#  finalizado                 :boolean          default(FALSE)
#  aprobado                   :boolean          default(FALSE)
#  nota                       :float
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  course_id                  :integer
#  porcentaje_avance          :float            default(0.0)
#  dncp_id                    :integer
#  grupo                      :integer
#  asistencia                 :boolean
#  aplica_sence               :boolean          default(FALSE)
#  anulado                    :boolean          default(FALSE)
#  grupo_f                    :integer
#  program_course_instance_id :integer
#  user_id                    :integer
#  asistencia_unidades        :float            default(0.0)
#  enroll_np                  :boolean          default(FALSE)
#


class UserCourse < ActiveRecord::Base


  belongs_to :enrollment
  #belongs_to :user
  belongs_to :program_course
  belongs_to :course
  belongs_to :dncp
  belongs_to :program_course_instance

  has_many :user_course_units, dependent: :destroy
  has_many :user_course_evaluations, dependent: :destroy
  has_many :user_course_polls, dependent: :destroy
  has_many :log_user_courses, dependent: :destroy
  has_many :user_course_uc_characteristics, dependent: :destroy

  has_one :user, through: :enrollment



  attr_accessible :aprobado, :asistencia, :course_id, :desde, :enrollment_id, :fin, :finalizado, :hasta,
  								:iniciado, :inicio, :limite_tiempo, :nota, :numero_oportunidad, :porcentaje_avance,
  								:program_course_id, :dncp_id, :grupo, :grupo_f, :aplica_sence, :anulado,
                  :enrollment, :program_course, :course, :program_course_instance, :user_id, :asistencia_unidades,
                  :enroll_np

  validates :course_id, presence: true
  validates :enrollment_id, presence: true
  validates :program_course_id, presence: true

	validate :match_enrollment_program_course

  before_create :set_grupo
  before_create :set_grupo_f
  before_create { self.user_id = self.enrollment.user_id if self.enrollment }
  #before_create :set_program_course_instance

  def match_enrollment_program_course

    unless self.enrollment.level_id == self.program_course.level_id && 
      self.enrollment.program_id == self.program_course.program_id &&
      self.program_course.course_id == self.course_id

      errors.add(:mismatch_enrollment_program_course, 'El nivel, el programa y el curso deben coincidir')

    end

  end

  def set_grupo

    grupo = 1

    if self.program_course.program.especifico

      uc = UserCourse.where('desde = ? AND hasta = ? AND program_course_id = ?', self.desde, self.hasta, self.program_course_id).first

      if uc && uc.grupo
        grupo = uc.grupo
      else

        uc = UserCourse.where('program_course_id = ?', self.program_course_id).reorder('grupo DESC').first

        if uc && uc.grupo
          grupo = uc.grupo+1
        end

      end

    else

      grupo = self.numero_oportunidad

    end

    self.grupo = grupo

  end

  def set_grupo_f

    grupo_f = 1

    if self.program_course.program.especifico

      uc = UserCourse.where('hasta = ? AND program_course_id = ?', self.hasta, self.program_course_id).first

      if uc && uc.grupo_f
        grupo_f = uc.grupo_f
      else

        uc = UserCourse.where('program_course_id = ?', self.program_course_id).reorder('grupo_f DESC').first

        if uc && uc.grupo_f
          grupo_f = uc.grupo_f+1
        end

      end

    else

      grupo_f = self.numero_oportunidad

    end

    self.grupo_f = grupo_f

  end

  def set_program_course_instance

    unless self.program_course_instance

      if self.program_course.program.especifico

        program_course_instance = self.program_course.program_course_instances.where('from_date = ? and to_date = ?', self.desde, self.hasta).first

        unless program_course_instance

          program_course_instance = self.program_course.program_course_instances.build
          program_course_instance.from_date = self.desde
          program_course_instance.to_date = self.hasta
          program_course_instance.save

        end

      else

        program_course_instance = self.program_course.program_course_instances.where('from_date IS NULL and to_date IS NULL').first

        unless program_course_instance

          program_course_instance = self.program_course.program_course_instances.build
          program_course_instance.save

        end

      end

      self.program_course_instance = program_course_instance if program_course_instance

    end

  end

  def estado(lms_time)

    #estado: :no_iniciado / :en_progreso / :aprobado / :reprobado / :plazo_vencido / :anulado

    if self.anulado
      :anulado
    elsif self.iniciado
      if self.finalizado
        self.aprobado ? :aprobado : :reprobado
      else
        if self.fecha_tope && lms_time > self.fecha_tope
          self.course.has_virtual_units || self.course.has_virtual_evaluations ? :plazo_vencido : :reprobado
        else
          :en_progreso
        end
      end
    else
      #este caso es posible solo para programas especificos, los abiertos siempre han sido iniciados por el alumno, sino no existen
      lms_time > self.hasta ? :plazo_vencido : :no_iniciado
    end

  end

  def fecha_tope
    #retorna nil si no existe fecha tope
    self.limite_tiempo ? self.fin : self.hasta

  end

  def history

    self.enrollment.user_courses.where('program_course_id = ? AND id <= ? AND anulado = ? ', self.program_course_id, self.id, false).order('id DESC')

  end


end
