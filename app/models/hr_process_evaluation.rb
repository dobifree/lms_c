# == Schema Information
#
# Table name: hr_process_evaluations
#
#  id                         :integer          not null, primary key
#  hr_process_id              :integer
#  hr_process_dimension_e_id  :integer
#  fecha_inicio_eval_jefe     :datetime
#  fecha_fin_eval_jefe        :datetime
#  fecha_inicio_eval_auto     :datetime
#  fecha_fin_eval_auto        :datetime
#  fecha_inicio_eval_sub      :datetime
#  fecha_fin_eval_sub         :datetime
#  fecha_inicio_eval_par      :datetime
#  fecha_fin_eval_par         :datetime
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  vigente                    :boolean          default(TRUE)
#  orden                      :integer
#  cien_x_cien_puntos         :integer
#  visualizacion_alternativas :integer          default(1)
#  fecha_inicio_eval_cli      :datetime
#  fecha_fin_eval_cli         :datetime
#  fecha_inicio_eval_prov     :datetime
#  fecha_fin_eval_prov        :datetime
#  show_preg_nivel_1          :boolean          default(TRUE)
#  tope_no_aplica             :integer
#

class HrProcessEvaluation < ActiveRecord::Base

  belongs_to :hr_process
  belongs_to :hr_process_dimension_e
  has_one :hr_process_dimension, :through => :hr_process_dimension_e

  has_many :hr_process_evaluation_qs
  has_many :hr_process_evaluation_manual_qs
  has_many :hr_process_evaluation_q_classifications
  has_many :hr_process_evaluation_q_schedules

  has_many :hr_process_assessment_es

  #before_save :ajusta_fecha_fin

  before_save { self.cien_x_cien_puntos = nil if self.cien_x_cien_puntos.blank?}
  before_save { self.tope_no_aplica = nil if self.tope_no_aplica.blank?}

  attr_accessible :fecha_fin_eval_auto, :fecha_fin_eval_jefe, :fecha_fin_eval_par,
                  :fecha_fin_eval_sub, :fecha_inicio_eval_auto, :fecha_inicio_eval_jefe,
                  :fecha_inicio_eval_par, :fecha_inicio_eval_sub, :vigente,
                  :orden, :cien_x_cien_puntos, :visualizacion_alternativas,
                  :fecha_inicio_eval_cli, :fecha_fin_eval_cli,
                  :fecha_inicio_eval_prov, :fecha_fin_eval_prov, :show_preg_nivel_1, :tope_no_aplica

  #visualizacion_alternativas: 1: radio filas, 2: radio columnas, 3: combo select

  default_scope order('orden ASC')

  def name

    self.hr_process_dimension_e.hr_evaluation_type ? self.hr_process_dimension_e.hr_evaluation_type.nombre : ''

  end

  def ajusta_fecha_fin

    if self.fecha_fin_eval_jefe
      self.fecha_fin_eval_jefe = self.fecha_fin_eval_jefe+((24*60*60)-1).seconds

    end

    if self.fecha_fin_eval_auto

      self.fecha_fin_eval_auto = self.fecha_fin_eval_auto+((24*60*60)-1).seconds

    end

    if self.fecha_fin_eval_sub

      self.fecha_fin_eval_sub = self.fecha_fin_eval_sub+((24*60*60)-1).seconds

    end

    if self.fecha_fin_eval_par

      self.fecha_fin_eval_par = self.fecha_fin_eval_par+((24*60*60)-1).seconds

    end

    if self.fecha_fin_eval_cli

      self.fecha_fin_eval_cli = self.fecha_fin_eval_cli+((24*60*60)-1).seconds

    end

    if self.fecha_fin_eval_prov

      self.fecha_fin_eval_prov = self.fecha_fin_eval_prov+((24*60*60)-1).seconds

    end

  end

  def carga_manual?

    ete = self.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.first

    if ete && ete.carga_manual
      return true
    else
      return false
    end

  end

  def evaluation_type_element_level_1

    self.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 1).first

  end

  def evaluation_type_element_level_2

    self.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = ?', 2).first

  end


  def total_number_of_questions(level_id = nil)

    if level_id == nil

      self.hr_process_evaluation_qs.where('hr_process_level_id IS NULL').size

    else

      self.hr_process_evaluation_qs.where('hr_process_level_id IS NULL OR hr_process_level_id = ?',level_id).size

    end

  end

  def total_number_of_questions_type_element(level_id = nil, type_element_id)

    if level_id == nil

      self.hr_process_evaluation_qs.where('hr_process_level_id IS NULL AND hr_evaluation_type_element_id = ?', type_element_id).size

    else

      self.hr_process_evaluation_qs.where('(hr_process_level_id IS NULL OR hr_process_level_id = ?) AND hr_evaluation_type_element_id = ?',level_id, type_element_id).size

    end

  end

  def total_number_of_manual_questions(hr_process_user)

    self.hr_process_evaluation_manual_qs.where('hr_process_user_id = ?', hr_process_user.id).size

  end


  def questions_nivel_1(level_id = nil)

    if level_id == nil

      self.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND hr_process_level_id IS NULL')

    else

      self.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id IS NULL OR hr_process_level_id = ?)',level_id)

    end

  end

  def manual_questions_nivel_1(hr_process_user)

    self.hr_process_evaluation_manual_qs.where('hr_process_user_id = ? AND hr_process_evaluation_manual_q_id IS NULL', hr_process_user.id)

  end

  def question(hr_process_evaluation_q_padre_id = nil, hr_process_level_id = nil, texto)


    if hr_process_evaluation_q_padre_id.nil?

      if hr_process_level_id.nil?

        return self.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND hr_process_level_id IS NULL AND texto = ?', texto).first

      else

        return self.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND hr_process_level_id = ? AND texto = ?', hr_process_level_id, texto).first

      end

    else

      if hr_process_level_id.nil?

        return self.hr_process_evaluation_qs.where('hr_process_evaluation_q_id = ? AND hr_process_level_id IS NULL AND texto = ?', hr_process_evaluation_q_padre_id, texto).first

      else

        return self.hr_process_evaluation_qs.where('hr_process_evaluation_q_id = ? AND hr_process_level_id = ? AND texto = ?', hr_process_evaluation_q_padre_id, hr_process_level_id, texto).first

      end

    end


  end

  def manual_questions_confirmed(hr_process_user_id)
    self.hr_process_evaluation_manual_qs.where('hr_process_user_id = ? AND confirmado = ?',hr_process_user_id, true).count == 0 ? false : true
  end

  def has_evaluation_for?(rel)
    self.hr_process_dimension_e.send('eval_'+rel)
  end

  def has_autoevaluation?
    self.hr_process_dimension_e.eval_auto
  end

  def from_date_for(rel)
    self.send('fecha_inicio_eval_'+rel)
  end

  def to_date_for(rel)
    self.send('fecha_fin_eval_'+rel)
  end

end
