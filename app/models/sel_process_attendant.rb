# == Schema Information
#
# Table name: sel_process_attendants
#
#  id                  :integer          not null, primary key
#  sel_process_step_id :integer
#  user_id             :integer
#  role                :boolean
#  automatic_assign    :boolean          default(FALSE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class SelProcessAttendant < ActiveRecord::Base
  belongs_to :sel_process_step
  belongs_to :user
  attr_accessible :automatic_assign, :role,
                  :sel_process_step, :sel_process_step_id,
                  :user, :user_id
end
