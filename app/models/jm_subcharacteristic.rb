# == Schema Information
#
# Table name: jm_subcharacteristics
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  description          :text
#  option_type          :integer
#  jm_list_id           :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  jm_characteristic_id :integer
#  position             :integer
#

class JmSubcharacteristic < ActiveRecord::Base
  belongs_to :jm_characteristic
  belongs_to :jm_list
  has_many :jm_options, through: :jm_list
  has_many :jm_answer_values
  has_many :jm_profile_char_to_reports
  attr_accessible :description, :name, :option_type, :position,
                  :jm_characteristic, :jm_characteristic_id,
                  :jm_list, :jm_list_id,
                  :jm_answer_values,
                  :jm_options

  def self.alternative_option_type
    #[:texto, :numero, :fecha, :seleccion, :archivo, :texto_largo]
    ['Texto', 'Entero','Fecha', 'Selección', 'Archivo', 'Texto largo', 'Real']
  end

  default_scope order('position ASC')
end
