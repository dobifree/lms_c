# == Schema Information
#
# Table name: sc_register_to_validates
#
#  id                        :integer          not null, primary key
#  user_id                   :integer
#  active                    :boolean          default(TRUE)
#  registered_at             :datetime
#  registered_by_user_id     :integer
#  deactivated_at            :datetime
#  deactivated_by_user_id    :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  he_ct_module_privilage_id :integer
#  recorder_id               :integer
#

class ScRegisterToValidate < ActiveRecord::Base
  belongs_to :user
  belongs_to :he_ct_module_privilage
  belongs_to :recorder, class_name: 'HeCtModulePrivilage', foreign_key: :recorder_id
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id,
                  :he_ct_module_privilage_id,
                  :user_id,
                  :recorder_id



  validates :user_id, uniqueness: { scope: :he_ct_module_privilage_id }
  validates :active, inclusion: { in: [true, false] }

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validates_presence_of :deactivated_at, :unless => :active?
  validates_presence_of :deactivated_by_user_id, :unless => :active?

  def sc_user_to_registers
    register = HeCtModulePrivilage.where(user_id: user_id, active: true).first
    return nil unless register
    register.sc_user_to_registers
  end
end
