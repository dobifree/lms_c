# == Schema Information
#
# Table name: banners
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  crypted_name       :string(255)
#  display_method     :integer
#  display_content    :integer
#  active             :boolean
#  complement_visible :boolean
#  link               :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  location           :integer          default(0)
#  position           :integer
#  open_at_login      :boolean          default(FALSE)
#

class Banner < ActiveRecord::Base

  has_many :banner_characteristics

  attr_accessible :active, :complement_visible, :crypted_name, :display_content, :display_method, :link, :name, :location, :position, :open_at_login

  before_save { self.active = nil if self.active.blank? }
  before_save { self.complement_visible = nil if self.complement_visible.blank? }

  validates :name, presence: true
  validates :location, presence: true
  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }

  default_scope order('position ASC')

  def self.alternative_display_method
    [:no_display, :pop_over, :link_externo, :link_interno]
  end

  def self.alternative_display_content
    [:imagen_jpg, :archivo_pdf, :animacion]
  end

  def self.alternative_location
    [:dashboard_user, :pantalla_cursos]
  end

  def self.actives_for_user(user_id, location)
    banners_to_show = []
    #active_banners_no_filter = self.find_all_by_active(true)
    active_banners_no_filter = self.where(:active => true, :location => location)
    user_characteristics = UserCharacteristic.find_all_by_user_id(user_id)

    active_banners_no_filter.each do |banner|
      banner_characteristics = BannerCharacteristic.find_all_by_banner_id(banner.id)
      #solo se filtra si el banner tiene alguna caracteristica asociada
      if banner_characteristics.count > 0
        banner_characteristics.each do |banner_characteristic|
          user_characteristics.each do |user_characteristic|
            if banner_characteristic.match_value.to_s == user_characteristic.valor.to_s
              banners_to_show.push(banner) unless banners_to_show.include?(banner)
              break
            end
          end
        end
      else
        #si no hay caracteristicas asociadas al banner, se muestra nomas
        banners_to_show.push(banner) unless banners_to_show.include?(banner)
      end
    end

    return banners_to_show

  end

  def url_image(company)
    url = '/storage/' + company + '/banners/' + self.crypted_name + '/ba' + self.id.to_s + '.jpg'

    return url
  end

  def url_index_complement(company)

    url = '/storage/' + company + '/banners/' + self.crypted_name + '/f2/index.html'
    return url
  end


end
