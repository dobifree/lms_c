# == Schema Information
#
# Table name: vac_user_to_approves
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  vac_approver_id        :integer
#  active                 :boolean          default(FALSE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class VacUserToApprove < ActiveRecord::Base
  belongs_to :user
  belongs_to :vac_approver
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :user_id,
                  :vac_approver_id,
                  :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id

end

