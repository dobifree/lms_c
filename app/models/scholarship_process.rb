# == Schema Information
#
# Table name: scholarship_processes
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  description          :text
#  active               :boolean
#  app_from_date        :date
#  app_to_date          :date
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  app_form_description :text
#

class ScholarshipProcess < ActiveRecord::Base

  has_many :scholarship_item_groups, dependent: :destroy
  accepts_nested_attributes_for :scholarship_item_groups, allow_destroy: true
  has_many :scholarship_applications, dependent: :destroy
  attr_accessible :active, :app_from_date, :app_to_date, :description, :name,
                  :app_form_description,
                  :scholarship_item_groups_attributes

  validates :name, presence: true

  def applicable?(lms_date)
    lms_date >= app_from_date && lms_date <= app_to_date
  end

end
