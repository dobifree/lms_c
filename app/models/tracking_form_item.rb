# == Schema Information
#
# Table name: tracking_form_items
#
#  id                        :integer          not null, primary key
#  tracking_form_question_id :integer
#  position                  :integer
#  name                      :string(255)
#  description               :text
#  item_type                 :integer
#  tracking_list_id          :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  reportable                :boolean          default(FALSE)
#

class TrackingFormItem < ActiveRecord::Base
  belongs_to :tracking_form_question
  belongs_to :tracking_list
  has_many :tracking_form_values, :dependent => :destroy
  attr_accessible :description, :item_type, :name, :position, :tracking_list_id, :reportable

  before_save :reset_tracking_list

  validates :position, :numericality => true
  validates :name, :presence => true
  validates :item_type, :presence => true
  validates :tracking_list_id, :presence => true, :if => :item_type_needs_a_list?

  default_scope { order(:position, :name) }

  def self.alternative_item_type_active_ordered
    Hash[self.alternative_item_type_hash.delete_if { |id, type| !type[:active] }.sort_by { |id, type| type[:position] }]
  end

  def self.alternative_item_type_hash
    {1 => {:name => 'Texto', :position => 1, :need_a_list => false, :its_a_file => false, :active => true},
     2 => {:name => 'Número', :position => 2, :need_a_list => false, :its_a_file => false, :active => true},
     3 => {:name => 'Fecha', :position => 3, :need_a_list => false, :its_a_file => false, :active => true},
     4 => {:name => 'Selección', :position => 4, :need_a_list => true, :its_a_file => false, :active => true},
     5 => {:name => 'Archivo', :position => 5, :need_a_list => false, :its_a_file => true, :active => false},
     6 => {:name => 'Texto largo', :position => 6, :need_a_list => false, :its_a_file => false, :active => true}
    }
  end

  def item_type_needs_a_list?
    !self.item_type.nil? && self.class.alternative_item_type_hash[self.item_type][:need_a_list]
  end

  private
  def reset_tracking_list
    unless self.item_type_needs_a_list?
      self.tracking_list_id = nil
    end
  end

end
