# == Schema Information
#
# Table name: sel_notification_templates
#
#  id          :integer          not null, primary key
#  event_id    :integer
#  name        :string(255)
#  description :text
#  active      :boolean          default(FALSE)
#  to          :string(255)
#  cc          :string(255)
#  subject     :string(255)
#  body        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SelNotificationTemplate < ActiveRecord::Base
  has_many :sel_notifications, dependent: :nullify

  attr_accessible :active, :body, :cc, :description, :event_id, :name,
                  :subject, :to


  def self.events_hash
    {
        1 => {name: 'Job_Market - Generacion de password', position: 1, active: true},
        2 => {name: 'Job_Market - Postulación interna exitosa', position: 2, active: false},
        3 => {name: 'Job_Market - Postulación externa exitosa', position: 3, active: false},
        4 => {name: 'Job_Market - Postulacion por gestor', position: 4, active: false},
        5 => {name: 'Job_Market - Solicitud de actualización de perfil', position: 5, active: false},
        6 => {name: 'Selección Paso Normal - Asignacion de responsables', position: 6, active: false},
        7 => {name: 'Selección Paso Normal - Agendamiento de evaluación', position: 7, active: false},
        8 => {name: 'Selección Paso Filtro - Pasa a la siguiente etapa', position: 8, active: false},
        9 => {name: 'Selección Paso Filtro - Reversa de Pasa a la siguiente etapa', position: 9, active: false},
        10 => {name: 'Selección Lista Candidatos - Excluir candidato externo', position: 10, active: true},
        11 => {name: 'Selección Lista Candidatos - Marcar como apto', position: 11, active: false},
        12 => {name: 'Selección Selección - Marcar como seleccionado', position: 12, active: false},
        13 => {name: 'Selección Contratación - Marcar como contratado', position: 13, active: false},
        14 => {name: 'Aprobación de la vacante - Asignación evaluador', position: 14, active: true},
        15 => {name: 'Job Market - Cambio de correo', position: 15, active: true},
        16 => {name: 'Selección Lista Candidatos - Excluir candidato interno', position: 10, active: true}
    }
  end

  def self.event_options
    Hash[events_hash.delete_if {|_, event| !event[:active]}.sort_by {|_, event| event[:position]}]
  end


  def generate_notification(objects, triggered_user, triggered_at)
    notification = sel_notifications.build
    notification.to = replace_variables(to, objects)
    notification.cc = replace_variables(cc, objects)
    notification.subject = replace_variables(subject, objects)
    notification.body = replace_variables(body, objects)
    notification.triggered_by_user = triggered_user
    notification.triggered_at = triggered_at
    notification.event_id = event_id
    notification.jm_candidate = objects[:jm_candidate] if objects[:jm_candidate]
    notification.sel_process = objects[:sel_process] if objects[:sel_process]
    notification.sel_req_approver = objects[:sel_req_approver] if objects[:sel_req_approver]

    notification.sent = true
    notification.sent_at = triggered_at

    notification.save
    notification
  end

  private

  def replace_variables(text_aux, objects)
      text = text_aux.dup
=begin
    Variables del Candidato:<br>
        %{candidate.email}, <br>
        %{candidate.name}, <br>
        %{candidate.surname}, <br>
        %{candidate.change_password_token}
    <br><br>
        Variables del Proceso de selección:<br>
        %{sel_process.name}, <br>
        %{sel_process.code}
    <br><br>
        Variables del Proceso de aprobación de la vacante:<br>
        %{approver.email}, <br>
        %{approver.nombre},<br>
        %{approver.apellidos}
=end

    if objects[:jm_candidate]
      if objects[:jm_candidate].internal_user?
        text.gsub! '%{candidate.email}', objects[:jm_candidate].user.email
        text.gsub! '%{candidate.name}', objects[:jm_candidate].user.nombre
        text.gsub! '%{candidate.surname}', objects[:jm_candidate].user.apellidos
      else
        text.gsub! '%{candidate.email}', objects[:jm_candidate].email
        text.gsub! '%{candidate.name}', objects[:jm_candidate].name
        text.gsub! '%{candidate.surname}', objects[:jm_candidate].surname
      end
      text.gsub! '%{candidate.email_to_switch}', objects[:jm_candidate].email_to_switch if objects[:jm_candidate].email_to_switch
      text.gsub! '%{candidate.change_password_token}', objects[:jm_candidate].codigo_rec_pass ? objects[:jm_candidate].codigo_rec_pass : ''
    end

    if objects[:sel_process]
      text.gsub! '%{sel_process.code}', objects[:sel_process].code
      text.gsub! '%{sel_process.name}', objects[:sel_process].name
    end

    if objects[:sel_req_approver]
      text.gsub! '%{approver.email}', objects[:sel_req_approver].user.email
      text.gsub! '%{approver.nombre}', objects[:sel_req_approver].user.nombre
      text.gsub! '%{approver.apellidos}', objects[:sel_req_approver].user.apellidos
    end

    if objects[:trigger_user]
      text.gsub! '%{trigger_user.email}', objects[:trigger_user].email
      text.gsub! '%{trigger_user.nombre}', objects[:trigger_user].nombre
      text.gsub! '%{trigger_user.apellidos}', objects[:trigger_user].apellidos
    end

    text
  end

end
