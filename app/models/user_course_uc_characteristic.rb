# == Schema Information
#
# Table name: user_course_uc_characteristics
#
#  id                   :integer          not null, primary key
#  user_course_id       :integer
#  uc_characteristic_id :integer
#  value                :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class UserCourseUcCharacteristic < ActiveRecord::Base

  belongs_to :user_course
  belongs_to :uc_characteristic

  attr_accessible :value

  validates :user_course_id, uniqueness: {scope: :uc_characteristic_id}

end
