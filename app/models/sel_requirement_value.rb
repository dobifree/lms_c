# == Schema Information
#
# Table name: sel_requirement_values
#
#  id                 :integer          not null, primary key
#  sel_requirement_id :integer
#  sel_req_item_id    :integer
#  value              :text
#  sel_req_option_id  :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  char_value_string  :string(255)
#  char_value_text    :text
#  char_value_date    :date
#  char_value_int     :integer
#  char_value_float   :float
#  char_value_id      :integer
#  char_value_jjob_id :integer
#

class SelRequirementValue < ActiveRecord::Base
  belongs_to :sel_requirement
  belongs_to :sel_req_item
  belongs_to :sel_req_option
  belongs_to :char_value, :class_name => 'CharacteristicValue'
  belongs_to :char_value_jjob, :class_name => 'JJob'
  attr_accessible :value,
                  :sel_req_item, :sel_req_item_id,
                  :sel_requirement, :sel_requirement_id,
                  :sel_req_option, :sel_req_option_id,
                  :char_value_string, :char_value_text,
                  :char_value_date, :char_value_int,
                  :char_value_float, :char_value_id,
                  :char_value_jjob_id
end
