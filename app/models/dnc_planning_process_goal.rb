# == Schema Information
#
# Table name: dnc_planning_process_goals
#
#  id                       :integer          not null, primary key
#  dnc_id                   :integer
#  planning_process_goal_id :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class DncPlanningProcessGoal < ActiveRecord::Base

  belongs_to :dnc
  belongs_to :planning_process_goal

  validates :dnc_id, presence: true
  validates :planning_process_goal_id, presence: true

  attr_accessible :planning_process_goal_id

end
