# == Schema Information
#
# Table name: sel_step_characteristics
#
#  id                    :integer          not null, primary key
#  sel_step_id           :integer
#  sel_characteristic_id :integer
#  position              :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class SelStepCharacteristic < ActiveRecord::Base
  belongs_to :sel_step
  belongs_to :sel_characteristic
  attr_accessible :position, :sel_characteristic, :sel_characteristic_id, :sel_step, :sel_step_id

  validates :sel_characteristic_id, uniqueness: {scope: :sel_step_id}
  validates :position, presence: true

  default_scope order('position ASC')
end
