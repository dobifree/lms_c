# == Schema Information
#
# Table name: dnc_planning_process_goal_ps
#
#  id                       :integer          not null, primary key
#  dncp_id                  :integer
#  planning_process_goal_id :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class DncPlanningProcessGoalP < ActiveRecord::Base

  belongs_to :dncp
  belongs_to :planning_process_goal

  # attr_accessible :title, :body

  validates :dncp_id, presence: true
  validates :planning_process_goal_id, presence: true

  attr_accessible :planning_process_goal_id

end
