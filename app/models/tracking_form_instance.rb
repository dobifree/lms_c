# == Schema Information
#
# Table name: tracking_form_instances
#
#  id                      :integer          not null, primary key
#  tracking_form_id        :integer
#  tracking_participant_id :integer
#  filled                  :boolean
#  done                    :boolean
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class TrackingFormInstance < ActiveRecord::Base
  belongs_to :tracking_form
  belongs_to :tracking_participant
  has_many :tracking_form_logs, :dependent => :destroy
  has_many :tracking_form_answers, :dependent => :destroy
  has_many :tracking_redefinitions, :dependent => :destroy
  has_many :tracking_form_due_dates, :through => :tracking_form_answers
  accepts_nested_attributes_for :tracking_form_logs, :allow_destroy => true
  accepts_nested_attributes_for :tracking_form_answers, :allow_destroy => true
  accepts_nested_attributes_for :tracking_redefinitions, :allow_destroy => true

  attr_accessible :done, :filled, :tracking_form_id, :tracking_participant_id, :tracking_form_logs_attributes, :tracking_form_answers_attributes, :tracking_redefinitions_attributes

  def current_action(user)

    action = current_action_just_informative

    if action && !((action.able_to_attendant? && self.tracking_participant.attendant_user == user) || (action.able_to_subject? && self.tracking_participant.subject_user == user))
      action = nil
    end

    return action

  end

  def last_action_done
    current_action = current_action_just_informative
    if current_action
      self.tracking_form.tracking_form_actions.where('position < ?', current_action.position).last
    else
      self.tracking_form.tracking_form_actions.last
    end
  end

  def can_make_editable?(user)
    fill_action = self.tracking_form.tracking_form_actions.where(:fill_form => true).first

    last_fill_in_log = self.tracking_form_logs.where(:tracking_form_action_id => fill_action.id).last

    if !self.done? &&
        self.filled? &&
        !last_fill_in_log.is_registered_by_user_or_same_role?(user.id) &&
        self.tracking_form_logs.joins(:tracking_form_action).where('tracking_form_actions.position > ? AND tracking_form_logs.id > ?', fill_action.position, last_fill_in_log.id).count == 0
      return true
    else
      return false
    end

  end

  def first_due_date
    TrackingFormDueDate.where(:finished => false, :tracking_form_answer_id => self.tracking_form_answers.pluck(:id)).reorder(:due_date).first
  end

  def data_for_tracking_form(user_connected)

    am_i_an_attendant = (self.tracking_participant.attendant_user_id == user_connected.id)
    am_i_a_subject = (self.tracking_participant.subject_user_id == user_connected.id)

    traceable_by_attendant = self.tracking_form.traceable_by_attendant?
    traceable_to_subject = self.tracking_form.traceable_by_subject?

    can_fill_milestones = (am_i_an_attendant && traceable_by_attendant) || (am_i_a_subject && traceable_to_subject)

    builder = []
    filler = []
    counterpart = []
    self.tracking_form_answers.each do |answer|

      # solo si el compromiso no está finalizado
      if answer.tracking_form_question.traceable? && !answer.tracking_form_due_date.finished?
        ## primer caso, no hay hitos sin validar
        # (si se quiere permitir tener muchos hitos sin validar a la vez, se quita la estructura del if
        # y se dejan ambos cuerpos (del if y del else) sin condición al mismo nivel
        if can_fill_milestones && answer.tracking_form_due_date.tracking_milestones.where(:validated => false).count == 0
          builder.append(answer.id)
        else
          answer.tracking_form_due_date.tracking_milestones.each do |milestone|
            # el hito no está validado
            unless milestone.validated?
              #  si fue escrito por mí
              if milestone.is_registered_by_user_or_same_role?(user_connected.id)
                filler.append(milestone.id)
                # si necesita validación de contraparte y no fue escrito por mi (pero soy participante... osea, contraparte)
              elsif self.tracking_form.track_validation_by_counterpart?
                counterpart.append(milestone.id)
              end
            end
          end
        end
      end
    end


    return builder, filler, counterpart

  end


  def current_action_just_informative

    current_redefinition = self.tracking_redefinitions.where(:finished => false).last

    if self.tracking_participant.in_redefinition? && current_redefinition
      puts 'entró al bloque de redefinición ' + self.id.to_s
      redefinition_group = self.tracking_redefinitions.count
      done_action_ids = self.tracking_form_logs.where(:redefinition_group => redefinition_group).pluck(:tracking_form_action_id)
      fill_action = self.tracking_form.tracking_form_actions.where(:fill_form => true).first
      # parche para que se pueda editar un formulario previamente lleno siempre y cuando se haya marcado como filled = false previamente
      if done_action_ids.count > 0 && !self.filled?
        done_action_ids.delete(fill_action.id)
      end
      if done_action_ids.count == 0
        action = fill_action
      else
        action = self.tracking_form.tracking_form_actions.where('tracking_form_actions.position > ? AND tracking_form_actions.id NOT IN (?)', fill_action.position, done_action_ids).first
      end

    else
      done_action_ids = self.tracking_form_logs.pluck(:tracking_form_action_id)
      # parche para que se pueda editar un formulario previamente lleno siempre y cuando se haya marcado como filled = false previamente
      if done_action_ids.count > 0 && !self.filled?
        fill_action = self.tracking_form.tracking_form_actions.where(:fill_form => true).first
        done_action_ids.delete(fill_action.id)
      end
      if done_action_ids.count == 0
        action = self.tracking_form.tracking_form_actions.first
      else
        action = self.tracking_form.tracking_form_actions.where('tracking_form_actions.id NOT IN (?)', done_action_ids).first
      end
    end

    return action
  end

  def json_backup_with_children
    json_backup = self.to_json(:except => [:id, :created_at, :updated_at], :include => {:tracking_form_logs =>
                                                                                            {:except => [:id, :created_at, :updated_at, :tracking_form_instance_id], :include => {:tracking_form_reject => {:except => [:id, :created_at, :updated_at, :tracking_form_log_id]}}},
                                                                                        :tracking_form_answers =>
                                                                                            {:except => [:id, :created_at, :updated_at, :tracking_form_instance_id], :include => {
                                                                                                :tracking_form_values => {:except => [:id, :created_at, :updated_at, :tracking_form_answer_id]},
                                                                                                :tracking_form_due_date => {:except => [:id, :created_at, :updated_at, :tracking_form_answer_id], :include =>
                                                                                                    {:tracking_milestones => {:except => [:id, :created_at, :updated_at, :tracking_form_due_date_id]}}}
                                                                                            }
                                                                                            },
                                                                                        :tracking_redefinitions =>
                                                                                            {:except => [:id, :created_at, :updated_at, :tracking_form_instance_id]}
    }
    )

    json_backup = json_backup.
        gsub('"tracking_form_logs"', '"tracking_form_logs_attributes"').
        gsub('"tracking_form_answers"', '"tracking_form_answers_attributes"').
        gsub('"tracking_redefinitions"', '"tracking_redefinitions_attributes"').
        gsub('"tracking_form_reject"', '"tracking_form_reject_attributes"').
        gsub('"tracking_form_values"', '"tracking_form_values_attributes"').
        gsub('"tracking_form_due_date"', '"tracking_form_due_date_attributes"').
        gsub('"tracking_milestones"', '"tracking_milestones_attributes"')

    return json_backup

  end

  def calculated_status
    if self.tracking_redefinitions.where(:finished => true).count > 0
      return TrackingProcess::CALCULATED_STATUS_IN_REDEFINITION
    end

    if self.tracking_form_answers.count > 0 || self.tracking_form_logs.count > 0
      return TrackingProcess::CALCULATED_STATUS_IN_DEFINITION
    end

    return TrackingProcess::CALCULATED_STATUS_NOT_STARTED
  end

  def label_for_calculated_status

    calculated_status = self.calculated_status

    if calculated_status == TrackingProcess::CALCULATED_STATUS_IN_REDEFINITION
      return self.tracking_participant.tracking_process.in_redefinition_label
    end

    if calculated_status == TrackingProcess::CALCULATED_STATUS_IN_DEFINITION
      return self.tracking_participant.tracking_process.in_definition_label
    end

    if calculated_status == TrackingProcess::CALCULATED_STATUS_NOT_STARTED
      return self.tracking_participant.tracking_process.not_started_label
    end

  end

  def color_for_calculated_status

    calculated_status = self.calculated_status

    if calculated_status == TrackingProcess::CALCULATED_STATUS_IN_REDEFINITION
      return self.tracking_participant.tracking_process.in_redefinition_color
    end

    if calculated_status == TrackingProcess::CALCULATED_STATUS_IN_DEFINITION
      return self.tracking_participant.tracking_process.in_definition_color
    end

    if calculated_status == TrackingProcess::CALCULATED_STATUS_NOT_STARTED
      return self.tracking_participant.tracking_process.not_started_color
    end

  end

end
