# == Schema Information
#
# Table name: pe_process_reports
#
#  id                                         :integer          not null, primary key
#  pe_process_id                              :integer
#  rep_evaluated_evaluators_configuration     :boolean          default(FALSE)
#  created_at                                 :datetime         not null
#  updated_at                                 :datetime         not null
#  rep_assessment_status                      :boolean          default(FALSE)
#  rep_validation_status                      :boolean          default(FALSE)
#  rep_calibration_status                     :boolean          default(FALSE)
#  rep_feedback_status                        :boolean          default(FALSE)
#  rep_assessment_consolidated_results        :boolean          default(FALSE)
#  rep_assessment_detailed_results            :boolean          default(FALSE)
#  rep_evaluations_configutation              :boolean          default(FALSE)
#  rep_general_status                         :boolean          default(FALSE)
#  rep_consolidated_feedback                  :boolean          default(FALSE)
#  rep_detailed_final_results_evaluations     :boolean          default(FALSE)
#  rep_detailed_final_results_questions       :boolean          default(FALSE)
#  rep_consolidated_feedback_accepted         :boolean          default(FALSE)
#  rep_consolidated_feedback_accepted_survey  :boolean          default(FALSE)
#  rep_detailed_auto                          :boolean          default(FALSE)
#  rep_detailed_auto_elements                 :string(255)
#  rep_detailed_text_1                        :text
#  rep_detailed_text_2                        :text
#  rep_rol_privado                            :boolean          default(FALSE)
#  rep_assessment_detailed_results_i          :boolean
#  rep_tracking_status                        :boolean          default(FALSE)
#  rep_detailed_final_results_evaluations_cal :boolean
#  rep_assign_status                          :boolean          default(FALSE)
#  rep_res_habitat_1                          :boolean          default(FALSE)
#

class PeProcessReports < ActiveRecord::Base

  belongs_to :pe_process

  attr_accessible :rep_evaluated_evaluators_configuration,
                  :rep_evaluations_configutation,
                  :rep_general_status,
                  :rep_assessment_status,
                  :rep_validation_status,
                  :rep_calibration_status,
                  :rep_feedback_status,
                  :rep_assessment_consolidated_results,
                  :rep_assessment_detailed_results,
                  :rep_detailed_final_results_evaluations,
                  :rep_consolidated_feedback,
                  :rep_detailed_final_results_questions,
                  :rep_consolidated_feedback_accepted,
                  :rep_consolidated_feedback_accepted_survey,
                  :rep_detailed_auto,
                  :rep_detailed_auto_elements,
                  :rep_detailed_text_1,
                  :rep_detailed_text_2,
                  :rep_rol_privado,
                  :rep_assessment_detailed_results_i,
                  :rep_tracking_status,
                  :rep_detailed_final_results_evaluations_cal,
                  :rep_assign_status,
                  :rep_res_habitat_1


end
