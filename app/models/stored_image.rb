# == Schema Information
#
# Table name: stored_images
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  crypted_name :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class StoredImage < ActiveRecord::Base


  attr_accessible :crypted_name, :name

  validates :name, presence: true, uniqueness: { case_sensitive: false}

  after_create :set_crypted_name

  default_scope order('name')

  def set_crypted_name

    self.crypted_name = self.id.to_s+'_'+SecureRandom.hex(5)+'.jpg'
    self.save

  end

end
