# == Schema Information
#
# Table name: sel_flow_specific_approvers
#
#  id                   :integer          not null, primary key
#  sel_flow_approver_id :integer
#  user_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class SelFlowSpecificApprover < ActiveRecord::Base
  belongs_to :sel_flow_approver
  belongs_to :user
  attr_accessible :user_id

  validates :user_id, :presence => true
end
