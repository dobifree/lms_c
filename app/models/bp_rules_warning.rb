# == Schema Information
#
# Table name: bp_rules_warnings
#
#  id                           :integer          not null, primary key
#  bp_general_rules_vacation_id :integer
#  name                         :string(255)
#  description                  :text
#  restrictive                  :boolean          default(FALSE)
#  active                       :boolean          default(TRUE)
#  min_accumulated_days         :integer
#  max_accumulated_days         :integer
#  registered_at                :datetime
#  registered_by_admin_id       :integer
#  deactivated_at               :datetime
#  deactivated_by_admin_id      :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  accumulated_type             :integer          default(0)
#

class BpRulesWarning < ActiveRecord::Base
  belongs_to :deactivated_by_admin, class_name: 'Admin', foreign_key: :deactivated_by_admin_id
  belongs_to :registered_by_admin, class_name: 'Admin', foreign_key: :registered_by_admin_id
  belongs_to :bp_general_rules_vacation

  attr_accessible :bp_general_rules_vacation_id,
                  :name,
                  :description,
                  :max_accumulated_days,
                  :min_accumulated_days,
                  :restrictive,
                  :active,
                  :deactivated_at,
                  :deactivated_by_admin_id,
                  :registered_at,
                  :registered_by_admin_id,
                  :accumulated_type

  validates :name, presence: true
  validates :description, presence: true
  validates :bp_general_rules_vacation_id, presence: true
  validates :restrictive, inclusion: { in: [true, false] }
  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_admin_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?
  validates :registered_at, presence: true
  validates :registered_by_admin_id, presence: true

  validate :max_and_min_concor
  validate :at_least_one

  def accumulated_type_name
    return nil unless accumulated_type
    BpRulesWarning.accumulated_type_names[accumulated_type]
  end

  def self.accumulated_types
    [0,1,2]
  end

  def self.accumulated_type_names
    ['Solamente legales',
     'Solamente progresivos',
     'Legales + Progresivos']
  end

  def self.accumulated_types_array
    [
        [BpConditionVacation.accumulated_type_names[0], BpConditionVacation.accumulated_types[0]],
        [BpConditionVacation.accumulated_type_names[1], BpConditionVacation.accumulated_types[1]],
        [BpConditionVacation.accumulated_type_names[2], BpConditionVacation.accumulated_types[2]]
    ]
  end

  def doesnt_satisfy(vacs_days, next_pro_days)
    vacs_days = vacs_days.to_i
    next_pro_days = next_pro_days.to_i

    case accumulated_type
      when 0
        amount_to_evaluate = vacs_days
      when 1
        amount_to_evaluate = next_pro_days
      when 2
        amount_to_evaluate = vacs_days + next_pro_days
      else
        amount_to_evaluate = vacs_days
    end

    return (amount_to_evaluate >= min_accumulated_days) unless max_accumulated_days
    return (amount_to_evaluate <= max_accumulated_days) unless min_accumulated_days
    (amount_to_evaluate >= min_accumulated_days && amount_to_evaluate <= max_accumulated_days)
  end


  private

  def max_and_min_concor
    return unless max_accumulated_days && min_accumulated_days
    return unless max_accumulated_days < min_accumulated_days
    errors.add(:max_accumulated_days, 'debe ser mayor a Días mínimos acumulados')
  end

  def at_least_one
    return if max_accumulated_days || min_accumulated_days
    errors.add(:base, 'Por lo menos un mínimo o máximo debe ser definido')
  end
end
