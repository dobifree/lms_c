# == Schema Information
#
# Table name: pe_question_activity_fields
#
#  id                                 :integer          not null, primary key
#  pe_evaluation_id                   :integer
#  position                           :integer
#  name                               :string(255)
#  field_type                         :integer          default(0)
#  pe_question_activity_field_list_id :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  required                           :boolean          default(TRUE)
#

class PeQuestionActivityField < ActiveRecord::Base

  belongs_to :pe_evaluation
  belongs_to :pe_question_activity_field_list

  attr_accessible :field_type, :name, :position, :pe_evaluation_id, :pe_question_activity_field_list_id, :required

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_evaluation_id }
  validates :pe_evaluation_id, presence: true

  default_scope order('position, name')

  def self.field_types
    [:texto, :fecha, :lista]
  end

  def field_type_name
    PeQuestionActivityField.field_types[self.field_type]
  end


end
