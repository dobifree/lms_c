# == Schema Information
#
# Table name: pe_feedback_accepted_fields
#
#  id                        :integer          not null, primary key
#  pe_process_id             :integer
#  position                  :integer
#  name                      :string(255)
#  simple                    :boolean          default(TRUE)
#  field_type                :integer          default(0)
#  required                  :boolean          default(TRUE)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  is_survey                 :boolean          default(FALSE)
#  pe_feedback_field_list_id :integer
#

class PeFeedbackAcceptedField < ActiveRecord::Base

  belongs_to :pe_process
  belongs_to :pe_feedback_field_list

  attr_accessible :field_type, :name, :position, :required, :simple, :pe_process_id, :is_survey, :pe_feedback_field_list_id

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_process_id }

  default_scope order('position, name')

  def self.field_types
    [:texto, :fecha, :lista]
  end

  def field_type_name
    PeFeedbackAcceptedField.field_types[self.field_type]
  end

end
