# == Schema Information
#
# Table name: user_course_evaluations
#
#  id             :integer          not null, primary key
#  user_course_id :integer
#  evaluation_id  :integer
#  inicio         :datetime
#  fin            :datetime
#  finalizada     :boolean
#  ultimo_acceso  :datetime
#  aprobada       :boolean
#  nota           :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  paused         :boolean          default(FALSE)
#  from_date      :datetime
#  to_date        :datetime
#

class UserCourseEvaluation < ActiveRecord::Base

  include ActionView::Helpers::NumberHelper

  belongs_to :user_course
  belongs_to :evaluation

  has_many :user_course_evaluation_questions, dependent: :destroy

  attr_accessible :aprobada, :evaluation, :fin, :finalizada, :inicio, :nota, :ultimo_acceso, :user_course, :paused, :from_date, :to_date

  validates :user_course_id, presence: true
  validates :evaluation_id, presence: true
  #validates :inicio, presence: true
  #validates :ultimo_acceso, presence: true

  validate :valid_evaluation

  def valid_evaluation
    unless self.user_course.course.evaluations.include? self.evaluation
      errors.add(:invalid_evaluation, 'La evaluacion no pertenece al curso')
    end
  end

  def nota_formateada
    self.nota.to_i == self.nota ? self.nota.to_i : self.nota
  end

end
