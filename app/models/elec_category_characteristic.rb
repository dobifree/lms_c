# == Schema Information
#
# Table name: elec_category_characteristics
#
#  id                       :integer          not null, primary key
#  elec_process_category_id :integer
#  characteristic_id        :integer
#  match_value              :string(255)
#  filter_voter             :boolean
#  filter_candidate         :boolean
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class ElecCategoryCharacteristic < ActiveRecord::Base
  belongs_to :elec_process_category
  belongs_to :characteristic
  attr_accessible :filter_candidate, :filter_voter, :match_value,
                  :elec_process_category_id, :characteristic_id


  validates :match_value, :presence => true
  validates :characteristic_id, :presence => true

end
