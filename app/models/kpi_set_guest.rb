# == Schema Information
#
# Table name: kpi_set_guests
#
#  id         :integer          not null, primary key
#  kpi_set_id :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class KpiSetGuest < ActiveRecord::Base

  belongs_to :kpi_set
  belongs_to :user

  attr_accessible :kpi_set_id, :user_id

  validates :user_id, uniqueness: { scope: :kpi_set_id }

end
