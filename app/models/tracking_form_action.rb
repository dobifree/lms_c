# == Schema Information
#
# Table name: tracking_form_actions
#
#  id                :integer          not null, primary key
#  tracking_form_id  :integer
#  position          :integer
#  name              :string(255)
#  description       :text
#  label             :string(255)
#  fill_form         :boolean
#  able_to_attendant :boolean
#  able_to_subject   :boolean
#  require_comment   :boolean
#  label_comment     :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  label_warning     :text
#  label_reject      :string(255)
#  label_done        :string(255)
#  can_be_rejected   :boolean          default(TRUE)
#  color_label_done  :string(255)
#

class TrackingFormAction < ActiveRecord::Base
  belongs_to :tracking_form
  has_many :tracking_form_logs
  has_many :tracking_form_notifications, :dependent => :destroy
  accepts_nested_attributes_for :tracking_form_notifications, :allow_destroy => true
  attr_accessible :able_to_attendant, :able_to_subject, :description, :fill_form, :label, :label_comment,
                  :name, :position, :require_comment, :tracking_form_id, :label_warning, :label_reject, :label_done, :can_be_rejected, :color_label_done,
                  :tracking_form_notifications_attributes

  validates :position, :numericality => true
  validates :name, :presence => true
  validates :label, :presence => true
  validates :fill_form, :uniqueness => {:scope => :tracking_form_id}, if: :fill_form?

  default_scope { order(:position, :name) }


  def action_type_options
    action_type_array = []

    action_type_array.push(self.label)

    action_type_array.push(self.label_reject) if self.can_be_rejected?

    return action_type_array
  end

end
