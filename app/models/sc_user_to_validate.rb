# == Schema Information
#
# Table name: sc_user_to_validates
#
#  id                        :integer          not null, primary key
#  user_id                   :integer
#  active                    :boolean          default(TRUE)
#  registered_at             :datetime
#  registered_by_user_id     :integer
#  deactivated_at            :datetime
#  deactivated_by_user_id    :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  he_ct_module_privilage_id :integer
#

class ScUserToValidate < ActiveRecord::Base
  belongs_to :user
  belongs_to :he_ct_module_privilage
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  attr_accessible :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :registered_at,
                  :registered_by_user_id,
                  :he_ct_module_privilage_id,
                  :user_id

  validates :user_id, presence: true, :uniqueness => { :scope => :he_ct_module_privilage_id }

end
