# == Schema Information
#
# Table name: um_custom_report_characteristics
#
#  id                   :integer          not null, primary key
#  um_custom_report_id  :integer
#  um_characteristic_id :integer
#  position             :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class UmCustomReportCharacteristic < ActiveRecord::Base

  belongs_to :um_custom_report
  belongs_to :um_characteristic

  attr_accessible :position

  validates :position, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true

  default_scope order('position')

end
