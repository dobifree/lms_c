# == Schema Information
#
# Table name: elec_historic_characteristics
#
#  id                :integer          not null, primary key
#  elec_event_id     :integer
#  characteristic_id :integer
#  user_id           :integer
#  value             :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class ElecHistoricCharacteristic < ActiveRecord::Base
  belongs_to :elec_event
  belongs_to :characteristic
  belongs_to :user
  attr_accessible :value, :elec_event_id, :characteristic_id, :user_id

  def self.record_characteristic_values_user(user, elec_event_id)
    if !self.where(:elec_event_id => elec_event_id, :user_id => user.id).any?
      user.user_characteristics.each do |uc|
        elec_hist_char = self.where(:elec_event_id => elec_event_id, :user_id => user.id, :characteristic_id => uc.characteristic_id).first_or_initialize
        elec_hist_char.value = uc.formatted_value
        elec_hist_char.save
      end
    end
  end

end
