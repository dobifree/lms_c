# == Schema Information
#
# Table name: pe_cal_committees
#
#  id                :integer          not null, primary key
#  pe_cal_session_id :integer
#  pe_process_id     :integer
#  user_id           :integer
#  manager           :boolean          default(FALSE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PeCalCommittee < ActiveRecord::Base

  belongs_to :pe_cal_session
  belongs_to :pe_process
  belongs_to :user

  attr_accessible :manager, :user_id

  validates :user_id, uniqueness: { scope: :pe_cal_session_id }

end
