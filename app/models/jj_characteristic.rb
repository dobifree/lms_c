# == Schema Information
#
# Table name: jj_characteristics
#
#  id                         :integer          not null, primary key
#  name                       :string(255)
#  data_type_id               :integer
#  position                   :integer
#  public                     :boolean          default(TRUE)
#  j_characteristic_type_id   :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  register_characteristic_id :integer
#  register_position          :integer
#  brc                        :boolean          default(FALSE)
#  show_list                  :boolean          default(FALSE)
#  val_sec_1                  :boolean
#

class JjCharacteristic < ActiveRecord::Base
  belongs_to :j_characteristic_type
  belongs_to :register_characteristic, class_name: 'JjCharacteristic'

  has_many :jj_characteristic_values
  has_many :elements_characteristics, class_name: 'JjCharacteristic', foreign_key: 'register_characteristic_id'
  has_many :j_job_jj_characteristics

  attr_accessible :data_type_id, :name, :position, :public, :j_characteristic_type_id, :register_characteristic_id, :register_position, :brc,
                  :show_list, :val_sec_1

  validates :j_characteristic_type_id, :presence => true
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :position, numericality: { only_integer: true, greater_than: 0 }, allow_blank: true

  def self.data_types
    [[:string,0], [:text,1], [:date,2], [:int,3], [:float,4], [:list,5], [:file,6], [:register,11]]
  end

  # def self.data_types
  #   [:string, :text, :date, :int, :float, :list, :file, :ct_image_url, :boss_code, :boss_full_name, :boss_characteristic, :register, :user_code, :first_owned_node_string, :first_owned_node_list]
  #   #   0        1      2     3       4     5      6         7              8              9                 10               11          12                13                       14
  # end

  def self.excel_data_types
    [:String, :String, :DateTime, :Number, :Number, :String, :String, :String, :String, :String, :String, :String, :String, :String, :String]
    #   0        1         2        3       4         5        6       7        8         9       10       11       12       13       14
  end

  def excel_data_type_name
    JjCharacteristic.excel_data_types[self.data_type_id]
  end

  def data_type_name
    JjCharacteristic.data_types.each {|dt| return dt[0] if dt[1] == self.data_type_id}
    # JjCharacteristic.data_types[self.data_type_id][0]
  end

  def active_characteristic_values
    self.jj_characteristic_values.where('active = ?', true)
  end

  def find_jj_characteristic_value_string(s)
    self.jj_characteristic_values.where('value_string = ?',s).first
  end

  def self.brc_characteristic
    JjCharacteristic.find_by_brc true
  end

  def self.brc_characteristic_values
    b = JjCharacteristic.brc_characteristic
    return b.jj_characteristic_values if b
    []
  end

end
