# == Schema Information
#
# Table name: dnc_training_impacts
#
#  id                          :integer          not null, primary key
#  dnc_id                      :integer
#  training_impact_category_id :integer
#  training_impact_value_id    :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#

class DncTrainingImpact < ActiveRecord::Base
  belongs_to :dnc
  belongs_to :training_impact_category
  belongs_to :training_impact_value

  attr_accessible :training_impact_category_id, :training_impact_value_id

  validates :dnc_id, presence: true
  validates :training_impact_category_id, uniqueness: { scope: :dnc_id }

end
