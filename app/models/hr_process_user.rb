# == Schema Information
#
# Table name: hr_process_users
#
#  id                  :integer          not null, primary key
#  hr_process_id       :integer
#  user_id             :integer
#  hr_process_level_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  finalizado          :boolean          default(FALSE)
#  feedback_confirmado :boolean
#  texto_feedback      :text
#  finalizada_auto     :boolean          default(FALSE)
#  feedback_recibido   :boolean
#  feedback_date       :datetime
#  tf_1                :text
#  tf_2                :text
#  tf_4                :text
#

class HrProcessUser < ActiveRecord::Base

  belongs_to :hr_process
  belongs_to :user
  belongs_to :hr_process_level

  has_many :hr_process_evalua_rels, class_name: 'HrProcessUserRel', foreign_key: 'hr_process_user1_id', dependent: :destroy
  has_many :hr_process_evalua_users, class_name: 'HrProcessUser', through: :hr_process_evalua_rels, source: :hr_process_user2

  has_many :hr_process_es_evaluado_rels, class_name: 'HrProcessUserRel', foreign_key: 'hr_process_user2_id', dependent: :destroy
  has_many :hr_process_es_evaluado_users, class_name: 'HrProcessUser', through: :hr_process_es_evaluado_rels, source: :hr_process_user1

  has_many :hr_process_assessment_ds
  has_many :hr_process_assessment_es
  has_many :hr_process_assessment_efs
  has_many :hr_process_assessment_qs
  has_many :hr_process_evaluation_manual_qs, dependent: :destroy

  has_many :hr_process_assessment_eval_es, class_name: 'HrProcessAssessmentE', foreign_key: :hr_process_user_eval_id

  has_many :hr_process_user_comments

  has_many :hr_process_calibration_session_ms
  has_one :hr_process_assessment_qua

  has_many :hr_process_user_tfs

  attr_accessible :finalizado, :feedback_confirmado, :texto_feedback, :finalizada_auto, :feedback_recibido, :tf_1, :tf_2, :tf_4

  validates :hr_process, presence: true
  validates :user, presence: true
  #validates :hr_process_level, presence: true

  def has_to_assess?
    self.hr_process_evalua_rels.where('tipo <> ?', 'resp').count > 0 ? true : false
  end

  def has_to_assess_user?(hr_process_user)
    self.hr_process_evalua_rels.where('hr_process_user2_id = ? AND tipo <> ?', hr_process_user.id, 'resp').count > 0 ? true : false
  end

  def has_to_autoassess?

    unless self.hr_process_level_id.nil?

      self.hr_process.hr_process_evaluations.find_all_by_vigente(true).each do |hr_process_evaluation|

        if self.hr_process.hr_process_template &&
          hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_template.id == self.hr_process.hr_process_template.id &&
          hr_process_evaluation.hr_process_dimension_e.eval_auto
          return true
          break
        end

      end

    end

    return false

  end

  def is_super_boss_of(hr_process_user)

    result = false

    self.hr_process_evalua_rels.where('tipo = ?', 'jefe').each do |hr_p_u_rel|

      if hr_p_u_rel.hr_process_user2_id == hr_process_user.id
        result = true
      else
        result_temp = hr_p_u_rel.hr_process_user2.is_super_boss_of hr_process_user
        result = result_temp if result_temp
      end

    end

    return result


  end

  def has_to_assess_as?(rel)
    self.hr_process_evalua_rels.where('tipo = ?', rel).count > 0 ? true : false
  end

  def rel_with(hr_process_user)
    self.hr_process_evalua_rels.where('hr_process_user2_id = ?', hr_process_user.id).first
  end

  def num_hr_process_evalua_rels_pendientes_como(rel)
    self.hr_process_evalua_users.where('tipo = ? AND finalizada = ?', rel, false).count
  end

  def hr_process_evalua_users_uniq_order_by_apellidos_nombre

    self.hr_process_evalua_users.where('tipo <> ?', 'resp').joins(:user).uniq.order('apellidos, nombre')

  end

  def hr_process_evalua_users_como_uniq_order_by_apellidos_nombre(rel)

    self.hr_process_evalua_users.where('tipo = ?', rel).joins(:user).uniq.order('apellidos, nombre')

  end

  def hr_process_responsable_users_uniq_order_by_apellidos_nombre

    self.hr_process_evalua_users.where('tipo = ?', 'resp').joins(:user).uniq.order('apellidos, nombre')

  end

  def hr_process_es_evaluado_users_uniq_order_by_apellidos_nombre

    self.hr_process_es_evaluado_users.where('tipo <> ?', 'resp').joins(:user).uniq.order('apellidos, nombre')

  end

  def hr_process_es_evaluado_users_como_uniq_order_by_apellidos_nombre(rel)

    self.hr_process_es_evaluado_users.where('tipo = ?', rel).joins(:user).uniq.order('apellidos, nombre')

  end



  def quadrant

    hr_process_quadrant = nil


    hr_process_dimensions = self.hr_process.hr_process_template.hr_process_dimensions

    if hr_process_dimensions.length == 1

      hr_process_assessment_d =  self.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimensions.first.id)

      if hr_process_assessment_d

        hr_process_dimensions.first.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

          if hr_process_assessment_d && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

            hr_process_quadrant = self.hr_process.hr_process_template.quadrant_1d(hr_process_dimension_g.id)
            break
          end

        end

      end

    elsif hr_process_dimensions.length == 2

      hr_process_assessment_d =  self.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimensions.last.id)

      porcentaje_rows = nil

      porcentaje_rows = hr_process_assessment_d.resultado_porcentaje if hr_process_assessment_d

      hr_process_assessment_d =  self.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimensions.first.id)

      porcentaje_cols = nil

      porcentaje_cols = hr_process_assessment_d.resultado_porcentaje if hr_process_assessment_d

      hr_process_dimensions.last.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g_row|

        hr_process_dimensions.first.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

          if porcentaje_cols && porcentaje_rows && porcentaje_rows.method(hr_process_dimension_g_row.valor_minimo_signo).(hr_process_dimension_g_row.valor_minimo) && porcentaje_rows.method(hr_process_dimension_g_row.valor_maximo_signo).(hr_process_dimension_g_row.valor_maximo) && porcentaje_cols.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && porcentaje_cols.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

            hr_process_quadrant = self.hr_process.hr_process_template.quadrant_2d(hr_process_dimension_g.id, hr_process_dimension_g_row.id)
            break
          end

        end

        break if hr_process_quadrant

      end

    end


    return hr_process_quadrant

  end



end
