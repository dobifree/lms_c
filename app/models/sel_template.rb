# == Schema Information
#
# Table name: sel_templates
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  description       :text
#  active            :boolean          default(FALSE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  apply_form_layout :text
#

class SelTemplate < ActiveRecord::Base
  has_many :sel_characteristics
  has_many :sel_steps
  has_many :sel_apply_forms
  has_many :jm_characteristics, through: :sel_apply_forms
  attr_accessible :active, :description, :name, :sel_characteristics, :sel_steps, :apply_form_layout,
                  :sel_apply_forms, :jm_characteristics

  validates :name, presence: true

end
