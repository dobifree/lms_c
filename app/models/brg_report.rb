# == Schema Information
#
# Table name: brg_reports
#
#  id                     :integer          not null, primary key
#  brg_process_id         :integer
#  partial_report         :boolean          default(FALSE)
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class BrgReport < ActiveRecord::Base
  belongs_to :brg_process
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id

  has_many :brg_reported_bonuses

  attr_accessible :active,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :partial_report,
                  :registered_at,
                  :registered_by_user_id,
                  :brg_process_id
  

  validates :partial_report, inclusion: { in: [true, false] }

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  def brg_reported_bonuses
    BrgReportedBonus.where(brg_report_id: id)
  end

  def self.active
    where(active: true)
  end

  def self.only_partial
    where(partial_report: true)
  end

  def self.non_partial
    where(partial_report: false)
  end

  def self.from_process(id)
    where(brg_process_id: id)
  end


end
