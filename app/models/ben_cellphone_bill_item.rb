# == Schema Information
#
# Table name: ben_cellphone_bill_items
#
#  id                                :integer          not null, primary key
#  bill_folio                        :string(255)
#  description                       :string(255)
#  cellphone_number                  :string(255)
#  company_rut                       :string(255)
#  net_value                         :decimal(12, 4)
#  discount                          :decimal(12, 4)
#  net_total                         :decimal(12, 4)
#  net_with_taxes                    :decimal(12, 4)
#  ben_cellphone_process_id          :integer
#  ben_cellphone_number_id           :integer
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  bill_type                         :integer
#  not_charge_to_payroll             :boolean          default(FALSE)
#  not_charge_to_payroll_at          :datetime
#  not_charge_to_payroll_by_user_id  :integer
#  not_charge_to_payroll_description :string(255)
#  registered_manually               :boolean          default(FALSE)
#  registered_manually_at            :datetime
#  registered_manually_by_user_id    :integer
#  registered_manually_description   :string(255)
#  ben_user_cellphone_process_id     :integer
#  bill_item_errors                  :string(255)
#

class BenCellphoneBillItem < ActiveRecord::Base
  belongs_to :ben_cellphone_process
  belongs_to :ben_cellphone_number
  belongs_to :ben_user_cellphone_process
  belongs_to :not_charge_to_payroll_by_user, class_name: 'User', foreign_key: :not_charge_to_payroll_by_user_id
  belongs_to :registered_manually_by_user_id, class_name: 'User', foreign_key: :registered_manually_by_user_id

  attr_accessible :bill_folio,
                  :cellphone_number,
                  :company_rut,
                  :description,
                  :discount,
                  :net_total,
                  :net_value,
                  :net_with_taxes,
                  :ben_cellphone_process_id,
                  :ben_cellphone_number_id,
                  :ben_user_cellphone_process_id,
                  :bill_type,
                  :not_charge_to_payroll,
                  :not_charge_to_payroll_at,
                  :not_charge_to_payroll_by_user_id,
                  :not_charge_to_payroll_description,
                  :registered_manually,
                  :registered_manually_at,
                  :registered_manually_by_user_id,
                  :registered_manually_description,
                  :bill_item_errors

  validates :bill_folio, presence: true
  validates :company_rut, presence: true
  validates :description, presence: true
  validates :net_value, presence: true
  validates :discount, :numericality => {:less_than_or_equal_to => 0}
  validates :net_total, presence: true
  validates :net_with_taxes, presence: true
  validates :ben_cellphone_process_id, presence: true

  validate :val_cellphone_number
  validate :not_charge_description
  validate :registered_manually_description1


  # bill_type
  # 0: Plan Bill
  # 1: Equipment Bill
  # 2: Registro Manual

  def val_cellphone_number
    errors.add(:cellphone_number, 'debe tener 9 dígitos') unless self.cellphone_number.size == 9
    errors.add(:cellphone_number, 'solo puede ser numérico') unless self.cellphone_number =~ /\A\d+\z/
  end

  def registered_manually_description1
    if self.registered_manually?
      errors.add(:registered_manually_description, 'no puede ser vacío') unless self.registered_manually_description != ''
    end
  end

  def not_charge_description
    if self.not_charge_to_payroll?
      errors.add(:not_charge_to_payroll_description, 'no puede ser vacío') unless self.not_charge_to_payroll_description != ''
    end
  end

 def not_charge_to_payroll_at
   self[:not_charge_to_payroll_at].strftime('%F, %H:%M')
 end

  def cellphone_number_formatted
    self[:cellphone_number].insert(3,' ').insert(7,' ')
  end

  def not_charged_to_payroll_at_formatted
    self[:not_charged_to_payroll_at].strftime('%F, %H:%M')
  end

  def registered_manually_at_formatted
    self[:registered_manually_at].strftime('%F, %H:%M')
  end

  def no_match_messages
    no_match_messages = []
    unless self.ben_cellphone_number
      no_match_messages << ['Número sin beneficio registrado','Registrar beneficio']
    else
        no_match_messages << ['Número no activo', 'Activar beneficio'] unless self.ben_cellphone_number.active?
        no_match_messages << ['Usuario no activo', 'Activar usuario'] unless self.ben_cellphone_number.user.activo?
    end
    return no_match_messages
  end


end
