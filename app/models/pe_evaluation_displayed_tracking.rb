# == Schema Information
#
# Table name: pe_evaluation_displayed_trackings
#
#  id                         :integer          not null, primary key
#  pe_evaluation_id           :integer
#  pe_displayed_evaluation_id :integer
#  position                   :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class PeEvaluationDisplayedTracking < ActiveRecord::Base

  belongs_to :pe_evaluation
  belongs_to :pe_displayed_evaluation, class_name: 'PeEvaluation'

  attr_accessible :pe_displayed_evaluation_id

  validates :pe_displayed_evaluation_id, uniqueness: { scope: :pe_evaluation_id }

end
