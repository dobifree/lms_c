# == Schema Information
#
# Table name: sel_steps
#
#  id              :integer          not null, primary key
#  sel_template_id :integer
#  name            :string(255)
#  description     :text
#  position        :integer
#  step_type       :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class SelStep < ActiveRecord::Base
  belongs_to :sel_template
  has_many :sel_step_characteristics, :dependent => :destroy
  has_many :sel_characteristics, :through => :sel_step_characteristics
  has_many :sel_template_attendants
  accepts_nested_attributes_for :sel_step_characteristics, :allow_destroy => true

  attr_accessible :description, :name, :position, :step_type,
                  :sel_template, :sel_template_id,
                  :sel_step_characteristics, :sel_step_characteristics_attributes,
                  :sel_characteristics,
                  :sel_template_attendants

  validates :name, :presence => true
  validates :position, :presence => true
  validates :step_type, :presence => true
  #validates_associated :sel_step_characteristics

  def self.alternatives_step_type
    [:normal, :de_filtro]
  end

  default_scope order('sel_template_id ASC ,position ASC, id ASC')

  def sel_attendants_ordered_by_name
    self.sel_template_attendants.joins(:user).reorder('role, apellidos, nombre')
  end

end

