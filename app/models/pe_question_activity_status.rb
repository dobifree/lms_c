# == Schema Information
#
# Table name: pe_question_activity_statuses
#
#  id               :integer          not null, primary key
#  pe_evaluation_id :integer
#  position         :integer
#  name             :string(255)
#  color            :string(255)
#  completed        :boolean
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PeQuestionActivityStatus < ActiveRecord::Base

  belongs_to :pe_evaluation

  attr_accessible :color, :completed, :name, :position, :pe_evaluation_id

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :pe_evaluation_id }
  validates :pe_evaluation_id, presence: true

  default_scope order('position, name')

end
