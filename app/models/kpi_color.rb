# == Schema Information
#
# Table name: kpi_colors
#
#  id               :integer          not null, primary key
#  kpi_dashboard_id :integer
#  percentage       :float
#  color            :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  kpi_set_id       :integer
#  kpi_indicator_id :integer
#

class KpiColor < ActiveRecord::Base

  belongs_to :kpi_dashboard
  belongs_to :kpi_set
  belongs_to :kpi_indicator

  attr_accessible :color, :percentage, :kpi_dashboard_id, :kpi_set_id, :kpi_indicator_id

  validates :color, presence: true
  validates :percentage, presence: true, numericality: {greater_than_or_equal_to: 0 }

  default_scope order('percentage')

end
