# == Schema Information
#
# Table name: tracking_form_values
#
#  id                      :integer          not null, primary key
#  tracking_form_answer_id :integer
#  tracking_form_item_id   :integer
#  value                   :text
#  tracking_list_item_id   :integer
#  hashed_id               :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class TrackingFormValue < ActiveRecord::Base
  belongs_to :tracking_form_answer
  belongs_to :tracking_form_item
  belongs_to :tracking_list_item
  attr_accessible :hashed_id, :value, :tracking_form_item_id, :tracking_list_item_id, :tracking_form_answer_id

  before_save :generate_hash_id
  before_save :set_value_to_list_item_id



  private
  def generate_hash_id
    self.hashed_id = self.id.to_s + SecureRandom.hex(5)
  end

  def set_value_to_list_item_id
    unless  self.tracking_list_item_id.nil?
      self.value = self.tracking_list_item.name
    end
  end

end
