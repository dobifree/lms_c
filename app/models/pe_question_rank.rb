# == Schema Information
#
# Table name: pe_question_ranks
#
#  id                                :integer          not null, primary key
#  percentage                        :float
#  goal                              :decimal(30, 6)
#  hr_process_evaluation_manual_q_id :integer
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  discrete_goal                     :string(255)      default("")
#  pe_question_id                    :integer
#  pe_element_id                     :integer
#  pe_question_rank_model_id         :integer
#

class PeQuestionRank < ActiveRecord::Base

  belongs_to :hr_process_evaluation_manual_q
  belongs_to :pe_question
  belongs_to :pe_element
  belongs_to :pe_question_rank_model

  attr_accessible :goal, :percentage, :discrete_goal, :pe_question_rank_model_id

  validates :goal, presence: true, numericality: true
  validates :discrete_goal, presence: true, allow_blank: false, allow_nil: false, :if => :validates_discrete_goal?
  validates :percentage, presence: true, numericality: {greater_than_or_equal_to: 0 }

  before_save { self.discrete_goal = nil if self.discrete_goal.blank? }

  validate :validate_percentage

  default_scope order('percentage, goal')

  private

    def validate_percentage

      if self.hr_process_evaluation_manual_q_id && self.hr_process_evaluation_manual_q.hr_evaluation_type_element.aplica_evaluacion
        if self.percentage && (self.percentage < self.hr_process_evaluation_manual_q.hr_evaluation_type_element.valor_minimo || self.percentage > self.hr_process_evaluation_manual_q.hr_evaluation_type_element.valor_maximo)
          errors.add(:percentage, 'El logro no puede ser menor que '+self.hr_process_evaluation_manual_q.hr_evaluation_type_element.valor_minimo.to_s+'o mayor que '+self.hr_process_evaluation_manual_q.hr_evaluation_type_element.valor_maximo.to_s )
        end
      end

    end

    def validates_discrete_goal?
      self.pe_question.indicator_type == 4
    end


end
