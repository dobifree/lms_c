# == Schema Information
#
# Table name: log_user_courses
#
#  id             :integer          not null, primary key
#  user_course_id :integer
#  fecha          :datetime
#  elemento       :string(255)
#  accion         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class LogUserCourse < ActiveRecord::Base

  belongs_to :user_course

  attr_accessible :accion, :elemento, :fecha

  default_scope order('fecha')

end
