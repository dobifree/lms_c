# == Schema Information
#
# Table name: uc_characteristics
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  position   :integer
#

class UcCharacteristic < ActiveRecord::Base

  has_many :user_course_uc_characteristics

  attr_accessible :name, :position

  default_scope order('position ASC')

end
