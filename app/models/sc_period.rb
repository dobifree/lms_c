# == Schema Information
#
# Table name: sc_periods
#
#  id                     :integer          not null, primary key
#  begin                  :datetime
#  end                    :datetime
#  description            :text
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class ScPeriod < ActiveRecord::Base
  has_many :sc_documentations
  belongs_to :deactivated_by_user, :class_name => 'User'
  attr_accessible :active,
                  :begin,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :description,
                  :end
  validate :range

  private
  def range
    errors.add(:begin, 'no puede ser después de Fin') if self.begin > self.end
  end
end
