# == Schema Information
#
# Table name: brg_percentage_conversions
#
#  id                     :integer          not null, primary key
#  before                 :float
#  after                  :float
#  type_1                 :integer
#  individual             :boolean          default(FALSE)
#  group                  :boolean          default(FALSE)
#  active                 :boolean          default(TRUE)
#  deactivated_at         :datetime
#  deactivated_by_user_id :integer
#  registered_at          :datetime
#  registered_by_user_id  :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  brg_process_id         :integer
#

class BrgPercentageConversion < ActiveRecord::Base

  belongs_to :brg_process
  belongs_to :deactivated_by_user, class_name: 'User', foreign_key: :deactivated_by_user_id
  belongs_to :registered_by_user, class_name: 'User', foreign_key: :registered_by_user_id
  has_many :brg_bonus_percentages

  attr_accessible :active,
                  :after,
                  :before,
                  :brg_process_id,
                  :deactivated_at,
                  :deactivated_by_user_id,
                  :group,
                  :individual,
                  :registered_at,
                  :registered_by_user_id,
                  :type_1

  validates :after, presence: true
  validates :before, presence: true

  validates :active, inclusion: { in: [true, false] }
  validates :deactivated_by_user_id, presence: true, unless: :active?
  validates :deactivated_at, presence: true, unless: :active?

  validates :registered_at, presence: true
  validates :registered_by_user_id, presence: true

  validate :exclusive_group_ind
  validate :allowed_type
  validate :pk_master

  def self.by_process(this_id); where(brg_process_id: this_id) end
  
  def individual_formatted
    return '<i class="fa fa-check" style="color: green;"></i>' if individual
    '<i class="fa fa-times"></i>'
  end

  def group_formatted
    return '<i class="fa fa-check" style="color: green;"></i>' if group
    '<i class="fa fa-times"></i>'
  end

  def ind_group_formatted
    if individual
      '<span class="label label-success label-smart-table">&nbsp;Individual&nbsp;</span>'
    else
      if group
        '<span class="label label-primary label-smart-table">&nbsp;Grupal&nbsp;</span>'
      end
    end
  end

  def type_formatted
    BrgPercentageConversion.types_names[type_1]
  end

  def self.group_types
    [BrgPercentageConversion.quanti_type]
  end

  def self.individual_types
    [BrgPercentageConversion.quali_type, BrgPercentageConversion.quanti_type]
  end


  def self.group_types_mapped
    [[BrgPercentageConversion.quanti_type_message, BrgPercentageConversion.quanti_type]]
  end

  def self.individual_types_mapped
    [[BrgPercentageConversion.quali_type_message, BrgPercentageConversion.quali_type],
    [BrgPercentageConversion.quanti_type_message, BrgPercentageConversion.quanti_type]]
  end

  def self.types_names; ['Cualitativo', 'Cuantitativo'] end
  def self.types; [0,1] end
  def self.quali_type; BrgPercentageConversion.types[0] end
  def self.quanti_type; BrgPercentageConversion.types[1] end
  def self.quali_type_message; BrgPercentageConversion.types_names[BrgPercentageConversion.quali_type] end
  def self.quanti_type_message; BrgPercentageConversion.types_names[BrgPercentageConversion.quanti_type] end

  def self.active; where(active: true) end
  def self.by_individual; where(individual: true) end
  def self.by_group; where(group: true) end
  def self.by_type(this_type); where(type_1: this_type) end
  def self.by_before(this_before); where(before: this_before) end
  def self.default_order; order('individual asc, active desc, type_1 asc, brg_percentage_conversions.before asc') end

  private
  def exclusive_group_ind
    return unless group == individual
    errors.add(:individual, 'y grupal no pueden tener el mismo valor')
  end

  def allowed_type
    return if BrgPercentageConversion.group_types.include?(type_1) && group
    return if BrgPercentageConversion.individual_types.include?(type_1) && individual
    errors.add(:type_1, 'no es permitido')
  end

  def pk_master
    return if individual && !BrgPercentageConversion.active.by_process(brg_process_id).by_individual.by_type(type_1).by_before(before).first
    return if group && !BrgPercentageConversion.active.by_process(brg_process_id).by_group.by_type(type_1).by_before(before).first
    errors.add(:before, 'ya tiene un valor asignado')
  end

end
