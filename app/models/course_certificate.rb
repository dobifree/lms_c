# == Schema Information
#
# Table name: course_certificates
#
#  id                  :integer          not null, primary key
#  course_id           :integer
#  template            :string(255)
#  rango_desde         :date
#  rango_hasta         :date
#  nombre              :boolean
#  nombre_x            :integer
#  nombre_y            :integer
#  desde               :boolean
#  desde_x             :integer
#  desde_y             :integer
#  hasta               :boolean
#  hasta_x             :integer
#  hasta_y             :integer
#  inicio              :boolean
#  inicio_x            :integer
#  inicio_y            :integer
#  fin                 :boolean
#  fin_x               :integer
#  fin_y               :integer
#  nota                :boolean
#  nota_x              :integer
#  nota_y              :integer
#  fecha_source        :string(255)
#  fecha_source_delay  :integer          default(0)
#  fecha               :boolean
#  fecha_x             :integer
#  fecha_y             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  nombre_curso        :boolean
#  nombre_curso_x      :integer
#  nombre_curso_y      :integer
#  nombre_color        :string(255)      default("000000")
#  nombre_curso_color  :string(255)      default("000000")
#  fecha_color         :string(255)      default("000000")
#  desde_color         :string(255)      default("000000")
#  hasta_color         :string(255)      default("000000")
#  inicio_color        :string(255)      default("000000")
#  fin_color           :string(255)      default("000000")
#  nota_color          :string(255)      default("000000")
#  nombre_letra        :string(255)      default("Helvetica")
#  nombre_curso_letra  :string(255)      default("Helvetica")
#  fecha_letra         :string(255)      default("Helvetica")
#  desde_letra         :string(255)      default("Helvetica")
#  hasta_letra         :string(255)      default("Helvetica")
#  inicio_letra        :string(255)      default("Helvetica")
#  fin_letra           :string(255)      default("Helvetica")
#  nota_letra          :string(255)      default("Helvetica")
#  nombre_size         :integer          default(20)
#  nombre_curso_size   :integer          default(20)
#  fecha_size          :integer          default(20)
#  desde_size          :integer          default(20)
#  hasta_size          :integer          default(20)
#  inicio_size         :integer          default(20)
#  fin_size            :integer          default(20)
#  nota_size           :integer          default(20)
#  desde_formato       :string(255)      default("day_snmonth_year")
#  hasta_formato       :string(255)      default("day_snmonth_year")
#  inicio_formato      :string(255)      default("day_snmonth_year")
#  fin_formato         :string(255)      default("day_snmonth_year")
#  fecha_formato       :string(255)      default("full_date")
#  program_instance_id :integer
#  program_id          :integer
#

class CourseCertificate < ActiveRecord::Base

  belongs_to :course
  belongs_to :program_instance
  belongs_to :program

  attr_accessible :desde, :desde_x, :desde_y,
                  :fecha, :fecha_source, :fecha_source_delay, :fecha_x, :fecha_y,
                  :fin, :fin_x, :fin_y,
                  :hasta, :hasta_x, :hasta_y,
                  :inicio, :inicio_x, :inicio_y,
                  :nombre, :nombre_x, :nombre_y,
                  :nota, :nota_x, :nota_y,
                  :rango_desde, :rango_hasta, :template, :course_id,
                  :nombre_curso, :nombre_curso_x, :nombre_curso_y,
                  :nombre_color, :nombre_curso_color, :fecha_color, :desde_color,
                  :hasta_color, :inicio_color, :fin_color, :nota_color,
                  :nombre_letra,
                  :nombre_curso_letra,
                  :fecha_letra,
                  :desde_letra,
                  :hasta_letra,
                  :inicio_letra,
                  :fin_letra,
                  :nota_letra,
                  :nombre_size,
                  :nombre_curso_size,
                  :fecha_size,
                  :desde_size,
                  :hasta_size,
                  :inicio_size,
                  :fin_size,
                  :nota_size,
                  :desde_formato,
                  :hasta_formato,
                  :inicio_formato,
                  :fin_formato,
                  :fecha_formato,
                  :program_instance_id,
                  :program_id

  validates :rango_desde, presence: true
  validates :rango_hasta, presence: true
  validates :fecha_source, presence: true

  def set_template_name

    self.template = self.id.to_s+'_'+SecureRandom.hex(5)

  end



end
