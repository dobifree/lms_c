# == Schema Information
#
# Table name: user_characteristics
#
#  id                              :integer          not null, primary key
#  user_id                         :integer
#  characteristic_id               :integer
#  valor                           :text
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  value_string                    :string(255)
#  value_text                      :text
#  value_date                      :date
#  value_int                       :integer
#  value_float                     :float
#  characteristic_value_id         :integer
#  value_file                      :string(255)
#  value_ct_image_url              :string(255)
#  register_user_characteristic_id :integer
#  value_encrypted                 :binary
#

class UserCharacteristic < ActiveRecord::Base

  #include CtCipherModule

  include CtCipherModule

  belongs_to :user
  belongs_to :characteristic
  belongs_to :characteristic_value
  belongs_to :boss_characteristic, class_name: 'Characteristic'
  belongs_to :register_user_characteristic, class_name: 'UserCharacteristic'

  has_many :elements_user_characteristics, class_name: 'UserCharacteristic', foreign_key: 'register_user_characteristic_id', dependent: :destroy

  attr_accessible :valor, :value_string, :value_text, :value_date, :value_int, :value_float, :value_file,
                  :characteristic_value_id, :value_ct_image_url, :register_user_characteristic_id, :value_encrypted

  #before_destroy :delete_file


  #validates :user_id, uniqueness: {scope: :characteristic_id}

  def value
    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 11 || self.characteristic.data_type_id == 13
      self.value_string
    elsif self.characteristic.data_type_id == 1
      self.value_text
    elsif self.characteristic.data_type_id == 2
      self.value_date
    elsif self.characteristic.data_type_id == 3
      self.value_int
    elsif self.characteristic.data_type_id == 4
      self.value_float
    elsif self.characteristic.data_type_id == 5 || self.characteristic.data_type_id == 14
      self.characteristic_value ? self.characteristic_value.id : nil
    elsif self.characteristic.data_type_id == 6
      self.value_file
    elsif self.characteristic.data_type_id == 7
      self.value_ct_image_url
    end

    #las características de tipo de dato      8,             9,                10,              12         no devuelven ningún valor
    #                                    :boss_code, :boss_full_name, :boss_characteristic, :user_code

  end

  def formatted_value(company = nil)
    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 11 || self.characteristic.data_type_id == 13
      self.value_string
    elsif self.characteristic.data_type_id == 1
      self.value_text
    elsif self.characteristic.data_type_id == 2
      self.value_date ? I18n.localize(self.value_date, format: :day_snmonth_year) : nil
    elsif self.characteristic.data_type_id == 3
      self.value_int
    elsif self.characteristic.data_type_id == 4
      unless self.characteristic.encrypted?
        self.value_float
      else
        !company.nil? ? decrypt_float(self.value_encrypted, company) : nil
      end
    elsif self.characteristic.data_type_id == 5 || self.characteristic.data_type_id == 14
      self.characteristic_value ? self.characteristic_value.value_string : nil
    elsif self.characteristic.data_type_id == 6
      self.value_file
    elsif self.characteristic.data_type_id == 7
      self.value_ct_image_url
    end

    #las características de tipo de dato 8, 9, 10, 12 no devuelven ningún valor

  end

  def excel_formatted_value
    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 11 || self.characteristic.data_type_id == 13
      self.value_string
    elsif self.characteristic.data_type_id == 1
      self.value_text
    elsif self.characteristic.data_type_id == 2
      self.value_date if self.value_date && self.value_date >= Date.new(1900,1,1)
    elsif self.characteristic.data_type_id == 3
      self.value_int
    elsif self.characteristic.data_type_id == 4
      self.value_float
    elsif self.characteristic.data_type_id == 5 || self.characteristic.data_type_id == 14
      self.characteristic_value ? self.characteristic_value.value_string : nil
    elsif self.characteristic.data_type_id == 6
      self.value_file
    elsif self.characteristic.data_type_id == 7
      self.value_ct_image_url
    end

    #las características de tipo de dato 8, 9, 10, 12 no devuelven ningún valor

  end

  def decrypted_value(company)

    if self.characteristic.encrypted?
      if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 13
        #string o first_owned_node
        decrypt_string self.value_encrypted, company
      elsif self.characteristic.data_type_id == 1
        #text
        decrypt_string self.value_encrypted, company
      elsif self.characteristic.data_type_id == 3
        #int
        decrypt_int self.value_encrypted, company
      elsif self.characteristic.data_type_id == 4
        #float
        decrypt_float self.value_encrypted, company
      end
    end

  end

  def update_value(new_value, company)

    #self.valor = new_value.strip unless self.characteristic.data_type_id == 6

    if self.characteristic.data_type_id == 0 || self.characteristic.data_type_id == 13
      #string o first_owned_node
      if self.characteristic.encrypted?
        self.value_encrypted = encrypt_string(new_value, company)
        new_value = ''
      end
      self.value_string = new_value
    elsif self.characteristic.data_type_id == 1
      #text
      if self.characteristic.encrypted?
        self.value_encrypted = encrypt_string(new_value, company)
        new_value = ''
      end
      self.value_text = new_value
    elsif self.characteristic.data_type_id == 2
      #date
      self.value_date = new_value
    elsif self.characteristic.data_type_id == 3
      #int
      if self.characteristic.encrypted?
        self.value_encrypted = encrypt_int(new_value, company)
        new_value = nil
      end
      self.value_int = new_value
    elsif self.characteristic.data_type_id == 4
      #float
      if self.characteristic.encrypted?
        self.value_encrypted = encrypt_float(new_value, company)
        new_value = nil
      end
      self.value_float = new_value
    elsif self.characteristic.data_type_id == 5 || self.characteristic.data_type_id == 14
      #lista
      self.characteristic_value = characteristic.characteristic_values.where('id = ?',new_value).first
    elsif self.characteristic.data_type_id == 6
      #file

      self.delete_file company

      directory = company.directorio+'/'+company.codigo+'/user_profiles/'+self.user.id.to_s+'/characteristics/'+self.characteristic.id.to_s+'/'

      FileUtils.mkdir_p directory unless File.directory? directory

      name = self.characteristic.id.to_s+'_'+SecureRandom.hex(5)+File.extname(new_value.original_filename)
      path = File.join(directory, name)
      File.open(path, 'wb') { |f| f.write(new_value.read) }

      self.value_file = name

    elsif self.characteristic.data_type_id == 7
      #ct_image_url
      self.value_ct_image_url = new_value.strip
    elsif self.characteristic.data_type_id == 11
      #register
      self.value_string = new_value.strip
    end

    self.valor = self.formatted_value.to_s

  end

  def delete_file(company)

    if self.characteristic.data_type_id == 6 && self.value_file

      if File.exist?(company.directorio+'/'+company.codigo+'/user_profiles/'+self.user.id.to_s+'/characteristics/'+self.characteristic.id.to_s+'/'+self.value_file)

        File.delete company.directorio+'/'+company.codigo+'/user_profiles/'+self.user.id.to_s+'/characteristics/'+self.characteristic.id.to_s+'/'+self.value_file

      end

    end

  end

  def elements_user_characteristics_by_characteristic(characteristic)
    self.elements_user_characteristics.where('characteristic_id = ?', characteristic.id).first
  end

  def self.uniq_select_array_values(characteristic)

    if characteristic.data_type_id == 5
      v = characteristic.characteristic_values.map { |cv| [cv.value_string, cv.id] }
    else

      v = Array.new

      UserCharacteristic.where('characteristic_id = ? AND valor <> ? ', characteristic.id, '').order('valor').each_with_index do |uc, index|

        v[index] = [uc.formatted_value,uc.value]

      end

      v.uniq!

    end

    return v

  end

end
