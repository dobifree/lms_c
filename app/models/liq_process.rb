# == Schema Information
#
# Table name: liq_processes
#
#  id         :integer          not null, primary key
#  year       :integer
#  month      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class LiqProcess < ActiveRecord::Base

  has_many :liq_items, dependent: :destroy
  has_many :brc_processes

  attr_accessible :month, :year

  validates :year, uniqueness: { scope: :month }


  def self.months_alternative_array
    array = {}
    Date::MONTHNAMES.each_with_index do |_month_name, index|
      array[index] =  I18n.l(Date.new(2000, index, 1), format: '%B') if index > 0
    end
    array
  end

  def is_editable?
    !liq_items.any? && !brc_processes.any?
  end

  def is_deletable?
    !brc_processes.any?
  end

  def get_subaso_brc_user(user)

    cohade_subase = SecuCohade.where('name = ?','SUBASE').first

    if cohade_subase
      liq_item = self.liq_items.where('secu_cohade_id = ? AND user_id = ?',cohade_subase.id, user.id).first
      liq_item.monto_o if liq_item
    end

  end

  def get_apvfmu_brc_user(user)
    cohade_apvfmu = SecuCohade.where('name = ?','APVFMU').first
    get_cohade_monto cohade_apvfmu, user
  end

  def get_apvexe_brc_user(user)
    cohade_apvexe = SecuCohade.where('name = ?','APVEXE').first
    get_cohade_monto cohade_apvexe, user
  end

  def get_ahopre_brc_user(user)
    cohade_ahopre = SecuCohade.where('name = ?','AHOPRE').first
    get_cohade_monto cohade_ahopre, user
  end

  def get_apvliq_brc_user(user)
    cohade_apvliq = SecuCohade.where('name = ?','APVLIQ').first
    get_cohade_monto cohade_apvliq, user
  end

  def get_depcon_brc_user(user)
    cohade_depcon = SecuCohade.where('name = ?','DEPCON').first
    get_cohade_monto cohade_depcon, user
  end

  def get_cohade_monto(cohade, user)
    if cohade
      liq_item = self.liq_items.where('secu_cohade_id = ? AND user_id = ?',cohade.id, user.id).first
      liq_item.monto if liq_item
    end
  end


  def self.liq_process_by_year_month(year, month)
    LiqProcess.where('year = ? AND month = ?', year, month).first
  end

end
