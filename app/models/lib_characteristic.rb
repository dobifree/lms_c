# == Schema Information
#
# Table name: lib_characteristics
#
#  id                :integer          not null, primary key
#  characteristic_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class LibCharacteristic < ActiveRecord::Base
  belongs_to :characteristic
  attr_accessible :characteristic_id
  validates :characteristic_id, uniqueness: true


end
