# == Schema Information
#
# Table name: pe_reporter_characteristic_values
#
#  id                   :integer          not null, primary key
#  pe_reporter_id       :integer
#  pe_characteristic_id :integer
#  value                :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class PeReporterCharacteristicValue < ActiveRecord::Base
  belongs_to :pe_reporter
  belongs_to :pe_characteristic
  attr_accessible :value
end
