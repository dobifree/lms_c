# == Schema Information
#
# Table name: sel_claimants
#
#  id             :integer          not null, primary key
#  sel_process_id :integer
#  user_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class SelClaimant < ActiveRecord::Base
  belongs_to :sel_process
  belongs_to :user
  attr_accessible :sel_process, :sel_process_id,
                  :user, :user_id

  validates :user_id, :presence => true

end
