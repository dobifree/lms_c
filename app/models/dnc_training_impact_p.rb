# == Schema Information
#
# Table name: dnc_training_impact_ps
#
#  id                          :integer          not null, primary key
#  dncp_id                     :integer
#  training_impact_category_id :integer
#  training_impact_value_id    :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#

class DncTrainingImpactP < ActiveRecord::Base
  belongs_to :dncp
  belongs_to :training_impact_category
  belongs_to :training_impact_value

  attr_accessible :training_impact_category_id, :training_impact_value_id

  validates :dncp_id, presence: true
  validates :training_impact_category_id, uniqueness: { scope: :dncp_id }

end
