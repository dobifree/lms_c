# == Schema Information
#
# Table name: sc_schedules
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  description    :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  registered_at  :datetime
#  active         :boolean          default(TRUE)
#  deactivated_at :datetime
#

class ScSchedule < ActiveRecord::Base
  has_many :sc_blocks, :dependent => :destroy
  has_many :sc_characteristic_conditions, :dependent => :destroy
  has_many :sc_schedule_users, :dependent => :destroy

  accepts_nested_attributes_for :sc_blocks, :allow_destroy => true
  accepts_nested_attributes_for :sc_characteristic_conditions, :allow_destroy => true
  accepts_nested_attributes_for :sc_schedule_users, :allow_destroy => true

  attr_accessible :name,
                  :description,
                  :active,
                  :sc_blocks_attributes,
                  :sc_characteristic_conditions_attributes,
                  :sc_schedule_users_attributes

  validates :name, presence: true

  def active_blocks
    sc_blocks.where(active: true).order(:day, :begin_time)
  end

  def self.active; where(active: true) end
end
