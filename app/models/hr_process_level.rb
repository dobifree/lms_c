# == Schema Information
#
# Table name: hr_process_levels
#
#  id            :integer          not null, primary key
#  nombre        :string(255)
#  hr_process_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class HrProcessLevel < ActiveRecord::Base

  belongs_to :hr_process
  has_many :hr_process_users
  has_many :hr_process_evaluation_qs

  attr_accessible :nombre, :hr_process_id

  validates :nombre, presence: true, uniqueness: { case_sensitive: false, scope: :hr_process_id }

  default_scope order('nombre ASC')

end
