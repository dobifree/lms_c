# == Schema Information
#
# Table name: jm_characteristics
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  description    :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  max_qty_items  :integer          default(1)
#  code           :string(255)
#  manager_only   :boolean          default(FALSE)
#  tabular_layout :boolean          default(FALSE)
#

class JmCharacteristic < ActiveRecord::Base
  has_many :jm_subcharacteristics, :dependent => :destroy
  has_many :jm_profile_chars
  has_many :sel_apply_forms, :dependent => :destroy
  has_many :sel_templates, through: :sel_apply_forms
  has_many :jm_candidate_answers
  has_many :jm_answer_values, :through => :jm_candidate_answers
  accepts_nested_attributes_for :jm_subcharacteristics, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
  attr_accessible :description, :name, :max_qty_items, :code, :manager_only, :tabular_layout,
                  :jm_subcharacteristics, :jm_subcharacteristics_attributes

  validates :name, :presence => true
  validates :code, :presence => true

  attr_accessor :simple

  def have_answers?
    self.jm_candidate_answers.any?
  end

  def simple
    !self.tabular_layout? && self.max_qty_items==1 && self.jm_subcharacteristics.size == 1
  end

end
