# == Schema Information
#
# Table name: ben_custom_report_attribute_filters
#
#  id                   :integer          not null, primary key
#  ben_custom_report_id :integer
#  name                 :string(255)
#  match_value_string   :string(255)
#  match_value_text     :text
#  match_value_date     :date
#  match_value_int      :integer
#  match_value_float    :float
#  match_value_char_id  :integer
#  match_value_decimal  :decimal(12, 4)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  match_value_boolean  :boolean
#

class BenCustomReportAttributeFilter < ActiveRecord::Base
  belongs_to :ben_custom_report

  attr_accessible :match_value_char_id,
                  :match_value_date,
                  :match_value_float,
                  :match_value_int,
                  :match_value_string,
                  :match_value_text,
                  :name,
                  :match_value_decimal,
                  :match_value_boolean,
                  :ben_custom_report_id

  def value
    case name
      when 'Numero de celular'
        match_value_string
      when 'Folio'
        match_value_string
      when 'Descripción'
        match_value_string
      when 'Valor neto'
        match_value_decimal
      when 'Descuento Movistar'
        match_value_decimal
      when 'Neto total'
        match_value_decimal
      when 'Neto con IVA'
        match_value_decimal
      when 'Tipo de factura'
        match_value_int
      when 'Cargar a Payroll'
        match_value_boolean
      when 'Registrado manualmente'
        match_value_boolean
      when '¿Por qué el registro manual?'
        match_value_string
      when 'Errores del registro'
        match_value_string
      when 'Mes del proceso'
        match_value_int
      when 'Año del proceso'
        match_value_int
      when 'RUT de compañia'
        match_value_string
      else
        nil
    end
  end

  def show_value
    case name
      when 'Numero de celular'
        match_value_string
      when 'Folio'
        match_value_string
      when 'Descripción'
        match_value_string
      when 'Valor neto'
        match_value_decimal
      when 'Descuento Movistar'
        match_value_decimal
      when 'Neto total'
        match_value_decimal
      when 'Neto con IVA'
        match_value_decimal
      when 'Tipo de factura'
        match_value_int
      when 'Cargar a Payroll'
        match_value_boolean ? 'Sí' : 'No'
      when 'Registrado manualmente'
        match_value_boolean ? 'Sí' : 'No'
      when '¿Por qué el registro manual?'
        match_value_string
      when 'Errores del registro'
        match_value_string
      when 'Mes del proceso'
        match_value_int
      when 'Año del proceso'
        match_value_int
      when 'RUT de compañia'
        match_value_string
      else
        nil
    end
  end

end
