# == Schema Information
#
# Table name: dnc_programs
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DncProgram < ActiveRecord::Base

  attr_accessible :name

  validates :name, presence: true

  has_many :dnc_courses

end
