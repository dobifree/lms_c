module PollProcessesHelper

  def link_to_remove_poll_report_chars(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_report_char(this); return false")
  end

  def link_to_add_poll_report_chars(name, f)
    report_char = PollProcessReportChar.new
    fields = f.fields_for(:poll_process_report_chars, report_char, :child_index => "new_report_char") do |builder|
      render('poll_processes/poll_process_report_chars/form', :f_char => builder)
    end
    link_to_function(name, "add_report_char(this, \"#{escape_javascript(fields)}\")")
  end
end
