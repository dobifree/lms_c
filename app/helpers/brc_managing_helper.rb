module BrcManagingHelper

  def categories_excel

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Lista_categorias')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'Categoría'
        filaHeader <<  headerCenter
        cols_widths << 40

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        BrcCategory.all.each do |brc_c|
          fila = Array.new
          fileStyle = Array.new

          fila << brc_c.name
          fileStyle << fieldLeft

          reporte_excel.add_row fila, :style => fileStyle, height: 20
        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

end
