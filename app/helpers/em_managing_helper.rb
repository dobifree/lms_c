module EmManagingHelper

  def search_employees(codigo, last_name, first_name)

    users = Array.new

    unless codigo.blank? && last_name.blank? && first_name.blank?

      users = User.where(
        'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ?',
        '%'+codigo+'%', '%'+last_name+'%', '%'+first_name+'%').order('apellidos, nombre')

    end

    return users

  end

  def letras_excel

    letras = %w(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI AJ AK AL AM AN AO AP AQ AR AS AT AU AV AW AX AY AZ BA BB BC BD BE BF BG BH BI BJ BK BL BM BN BO BP BQ BR BS BT BU BV BW BX BY BZ CA CB CC CD CE CF CG CH CI CJ CK CL CM CN CO CP CQ CR CS CT CU CV CW CX CY CZ)

  end

  def xls_for_massive_create_update(name)

    letras = letras_excel

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => 'Formato_'+name) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Email'
        filaHeader <<  headerCenter
        cols_widths << 20


        Characteristic.where('publica = true').reorder('position, nombre').each do |characteristic|

          fila << characteristic.nombre
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def xls_for_massive_create_update_sk(name)

    letras = letras_excel

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => 'Formato_'+name) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'RUT SIN DÍGITO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DÍGITO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'NOMBRES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO PATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO MATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDOS'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'EMAIL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CÓDIGO ESCOLARIDAD'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CÓDIGO NIVEL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'COMUNA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DIRECCIÓN PARTICULAR'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FECHA NACIMIENTO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'NACIONALIDAD'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RUT'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'SEXO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'TIPO CONTRATO'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

end
