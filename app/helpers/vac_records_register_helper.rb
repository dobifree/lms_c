module VacRecordsRegisterHelper
  include VacationsModule

  def icon_vacs_module_html_safe
    icon_vacs_module.html_safe
  end

  def icon_vacs_module
    '<i class="' + icon_vacs_module_class + '"></i>'
  end

  def icon_vacs_module_class
    'fa fa-suitcase'
  end

  def complete_update(request_id, params)
    request = VacRequest.find(request_id)
    updated_request = request.dup
    updated_request.status_description = nil
    updated_request.assign_attributes(params)
    updated_request.registered_at = lms_time
    updated_request.registered_by_user_id = user_connected.id
    updated_request.prev_request_id = request_id
    updated_request
  end

  def deactivate_request(request_id)
    request = VacRequest.find(request_id)
    request.active = false
    request.deactivated_at = lms_time
    request.deactivated_by_user_id = user_connected.id
    request
  end

  def deactivate_benefit(vac_rule_record_id)
    benefit = VacRuleRecord.find(vac_rule_record_id)
    benefit.active = false
    benefit.deactivated_at = lms_time
    benefit.deactivated_by_user_id = user_connected.id
    benefit
  end

  def create_record(params)
    record = VacRecord.new(params)
    record.registered_by_user_id = user_connected.id
    record.registered_at = lms_time
    record
  end

  def create_record_by_request(request_id)
    request = VacRequest.find(request_id)
    record = VacRecord.new(vac_request_id: request_id,
                           begin: request.begin,
                           end: request.end,
                           days: request.days,
                           days_progressive: request.days_progressive,
                           registered_at: lms_time,
                           registered_by_user_id: user_connected.id)
    record
  end

  def deactivate_latest_record(request_id)
    record = VacRecord.where(vac_request_id: request_id, active: true).first
    record = deactivate_vac_record(record.id) if record
    record
  end

  def deactivate_vac_record(vac_record_id)
    record = VacRecord.find(vac_record_id)
    record.active = false
    record.deactivated_by_user_id = user_connected.id
    record.deactivated_at = lms_time
    record
  end

  def restrictive_warnings(user_id, vacs_begin, vacs_end, vacs_days, vacs_progressive, request_id)
    vacs_progressive = vacs_progressive ? vacs_progressive : 0
    user_id = user_id
    now_time = vacs_begin
    vacs_days = vacs_days
    request = request_id && VacRequest.find(request_id) ? VacRequest.find(request_id) : nil

    rules = BpGeneralRulesVacation.user_active(now_time.to_datetime, user_id)

    if rules
      next_vacs_days = next_vac_accumulated_1(user_id, now_time, vacs_days)
      next_vacs_days += request.days if request && request.days && (request.pending? || request.approved?)
    else
      return false
    end

    actual = VacAccumulated.user_latest(user_id)
    prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
    prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated

    next_pro_days = next_progressive_days(prev_auto_accumulated.up_to_date, now_time, user_id) - VacRequest.sum_progressive_days_by_status(user_id, VacRequest.status_in_process).to_i
    next_pro_days += request.days_progressive if request && request.days_progressive &&  (request.pending? || request.approved?)
    next_pro_days -= vacs_progressive

    warnings = rules ? rules.doesnt_satisfy_warnings(next_vacs_days, next_pro_days) : []

    warnings.each { |warning| return warning.restrictive if warning.restrictive }
    false
  end

  def gimme_restrictive_warnings(user_id, vacs_begin, vacs_end, vacs_days, vacs_progressive, request_id)
    vacs_progressive = vacs_progressive ? vacs_progressive : 0
    user_id = user_id
    now_time = vacs_begin
    vacs_days = vacs_days
    request = request_id && VacRequest.find(request_id) ? VacRequest.find(request_id) : nil

    restrictive_warnings = []

    rules = BpGeneralRulesVacation.user_active(now_time.to_datetime, user_id)

    if rules
      next_vacs_days = next_vac_accumulated_1(user_id, now_time, vacs_days)
      next_vacs_days += request.days if request && request.days && (request.pending? || request.approved?)
    else
      return restrictive_warnings
    end

    actual = VacAccumulated.user_latest(user_id)
    prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
    prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated

    next_pro_days = next_progressive_days(prev_auto_accumulated.up_to_date, now_time, user_id) - VacRequest.sum_progressive_days_by_status(user_id, VacRequest.status_in_process).to_i
    next_pro_days += request.days_progressive if request && request.days_progressive && (request.pending? || request.approved?)
    next_pro_days -= vacs_progressive

    warnings = rules ? rules.doesnt_satisfy_warnings(next_vacs_days, next_pro_days) : []

    warnings.each { |warning| restrictive_warnings << warning if warning.restrictive }

    restrictive_warnings
  end


  def rule_record_complete_create(benefit_id, request_id)
    benefit = BpConditionVacation.find(benefit_id)
    vac_rule_record = VacRuleRecord.new(vac_request_id: request_id, bp_condition_vacation_id: benefit_id)
    request = VacRequest.find(request_id)

    if benefit.bonus_days
      vac_rule_record.days_begin = go_back_vacations_date(request_id)
      vac_rule_record.days_end = vacations_end(benefit.bonus_only_laborable_day, vac_rule_record.days_begin, benefit.bonus_days, request.user_id)
    end

    vac_rule_record.registered_at = lms_time
    vac_rule_record.registered_by_user_id = user_connected.id
    return vac_rule_record
  end

  def rule_record_complete_create_paid(benefit_id, request_id, paid, paid_description)
    benefit = BpConditionVacation.find(benefit_id)
    vac_rule_record = VacRuleRecord.new(vac_request_id: request_id, bp_condition_vacation_id: benefit_id, paid: paid, paid_description: paid_description)
    vac_rule_record.paid_by_user_id = user_connected.id unless vac_rule_record.paid.nil?
    vac_rule_record.paid_at = lms_time unless vac_rule_record.paid.nil?
    request = VacRequest.find(request_id)

    if benefit.bonus_days
      vac_rule_record.days_begin = go_back_vacations_date(request_id)
      vac_rule_record.days_end = vacations_end(benefit.bonus_only_laborable_day, vac_rule_record.days_begin, benefit.bonus_days, request.user_id)
    end

    vac_rule_record.registered_at = lms_time
    vac_rule_record.registered_by_user_id = user_connected.id
    return vac_rule_record
  end

  def update_end_vacations(vac_request_id)
    request = VacRequest.find(vac_request_id)
    end_date = request.end
    request.vac_rule_records.where(active: true, paid: [nil, true]).each do |vac_rule_record|
      benefit = vac_rule_record.bp_condition_vacation
      next unless benefit.bonus_days
      end_date = next_laborable_day?(end_date, request.user_id)
      end_date = vacations_end(benefit.bonus_only_laborable_day, end_date, benefit.bonus_days, request.user_id)
    end
    request.end_vacations = end_date
    request.save
  end

  def close_request_safe request_id, back_to_work_request
    request = VacRequest.find(request_id)
    request.closable? && back_to_work_request.to_date <= lms_time.to_date
  end
end
