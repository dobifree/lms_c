module ScholarshipProcessesHelper

  def icon_scholarship_module_html_safe
    icon_scholarship_module.html_safe
  end

  def icon_scholarship_module
    '<i class="' + icon_scholarship_module_class + '"></i>'
  end

  def icon_scholarship_module_class
    'fa fa-graduation-cap'
  end

  def link_to_remove_scholarship_item_group(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_scholarship_item_group(this)")
  end

  def link_to_add_scholarship_item_group(name, f)
    scholarship_item_group = ScholarshipItemGroup.new
    fields = f.fields_for(:scholarship_item_groups, scholarship_item_group, :child_index => "new_scholarship_item_group") do |builder|
      render('scholarship_processes/form_group', :f => builder)
    end
    link_to(name, '#', onclick: "add_scholarship_item_group(this, \"#{escape_javascript(fields)}\")")
  end

  def link_to_remove_scholarship_application_item(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_scholarship_application_item(this)")
  end

  def link_to_add_scholarship_application_item(name, f)
    scholarship_application_item = ScholarshipApplicationItem.new
    fields = f.fields_for(:scholarship_application_items, scholarship_application_item, :child_index => "new_scholarship_application_item") do |builder|
      render('scholarship_processes/form_item', :f => builder)
    end
    link_to(name, '#', onclick: "add_scholarship_application_item(this, \"#{escape_javascript(fields)}\")")
  end

end
