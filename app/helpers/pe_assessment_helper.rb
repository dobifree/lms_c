module PeAssessmentHelper

  def answer_evaluation(pe_assessment_evaluation)

    answer_evaluation_1_element pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 1

    answer_evaluation_2_elements pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 2

    answer_evaluation_3_elements pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 3

  end

  def answer_evaluation_1_element(pe_assessment_evaluation)

    pe_element = @pe_evaluation.pe_elements.first

    if pe_element.assessed?

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question|

        pe_assessment_question = pe_assessment_evaluation.pe_assessment_questions.build
        pe_assessment_question.pe_question = pe_question

        if pe_element.assessment_method == 1
          #alternativas

          pe_alternative = pe_question.pe_alternatives.where('fixed = ?', true).first

          unless pe_alternative

            pe_alternative = PeAlternative.find(params['pe_question_'+pe_question.id.to_s]) unless params['pe_question_'+pe_question.id.to_s].blank?

          end

          if pe_alternative

            pe_assessment_question.pe_alternative = pe_alternative
            pe_assessment_question.points = pe_alternative.value

            pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          end

        elsif pe_element.assessment_method == 2

          #indicadores

          if pe_element.kpi_dashboard && pe_question.kpi_indicator

            if pe_question.indicator_type == 4
              pe_assessment_question.indicator_discrete_achievement = pe_question.kpi_indicator.value
            else
              pe_assessment_question.indicator_achievement = pe_question.kpi_indicator.value
            end

            pe_assessment_question.percentage = pe_question.kpi_indicator.percentage

            pe_assessment_question.points = pe_question.kpi_indicator.percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          else

            unless params['pe_question_'+pe_question.id.to_s].blank?

              if pe_question.indicator_type == 4
                pe_assessment_question.indicator_discrete_achievement = params['pe_question_'+pe_question.id.to_s]
                pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_discrete_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
              else
                pe_assessment_question.indicator_achievement = fix_form_floats params['pe_question_'+pe_question.id.to_s]
                pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
              end

              pe_assessment_question.points = pe_assessment_question.percentage

              pe_assessment_question.last_update = lms_time
              pe_assessment_question.save

            end

          end

        elsif pe_element.assessment_method == 3
          #valor numérico

          unless params['pe_question_'+pe_question.id.to_s].blank?

            pe_assessment_question.points = params['pe_question_'+pe_question.id.to_s]

            pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          end

        end

        if pe_question.has_comment
          pe_assessment_question.comment = params['pe_question_comment_'+pe_question.id.to_s]
          pe_assessment_question.last_update = lms_time
          pe_assessment_question.save
        end

        if pe_question.has_comment_2
          pe_assessment_question.comment_2 = params['pe_question_comment_2_'+pe_question.id.to_s]
          pe_assessment_question.last_update = lms_time
          pe_assessment_question.save
        end

      end

    end

  end

  def answer_evaluation_2_elements(pe_assessment_evaluation)


    pe_element_1st = @pe_evaluation.pe_elements.first
    pe_element_2nd = @pe_evaluation.pe_elements.second

    if pe_element_2nd.assessed?

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question_1st_level|

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_questions.build
        pe_assessment_question_1st_level.pe_question = pe_question_1st_level
        pe_assessment_question_1st_level.save

        @pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question_2nd_level.pe_question = pe_question_2nd_level
          pe_assessment_question_2nd_level.pe_assessment_question = pe_assessment_question_1st_level

          if pe_element_2nd.assessment_method == 1
            #alternativas

            pe_alternative = pe_question_2nd_level.pe_alternatives.where('fixed = ?', true).first

            unless pe_alternative

              pe_alternative = PeAlternative.find(params['pe_question_'+pe_question_2nd_level.id.to_s]) unless params['pe_question_'+pe_question_2nd_level.id.to_s].blank?

            end

            if pe_alternative

              pe_assessment_question_2nd_level.pe_alternative = pe_alternative
              pe_assessment_question_2nd_level.points = pe_alternative.value

              pe_assessment_question_2nd_level.percentage = calculate_alternative_result pe_assessment_question_2nd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

              pe_assessment_question_2nd_level.last_update = lms_time
              pe_assessment_question_2nd_level.save

            end

          elsif pe_element_2nd.assessment_method == 2
            #indicadores

            if pe_element_2nd.kpi_dashboard && pe_question_2nd_level.kpi_indicator

              if pe_question_2nd_level.indicator_type == 4
                pe_assessment_question_2nd_level.indicator_discrete_achievement = pe_question_2nd_level.kpi_indicator.value
              else
                pe_assessment_question_2nd_level.indicator_achievement = pe_question_2nd_level.kpi_indicator.value
              end

              pe_assessment_question_2nd_level.percentage = pe_question_2nd_level.kpi_indicator.percentage

              pe_assessment_question_2nd_level.points = pe_question_2nd_level.kpi_indicator.percentage

              pe_assessment_question_2nd_level.last_update = lms_time
              pe_assessment_question_2nd_level.save

            else

              unless params['pe_question_'+pe_question_2nd_level.id.to_s].blank?
  
                if pe_question_2nd_level.indicator_type == 4
                  pe_assessment_question_2nd_level.indicator_discrete_achievement = params['pe_question_'+pe_question_2nd_level.id.to_s]
                  pe_assessment_question_2nd_level.percentage = calculate_indicator_achievement pe_assessment_question_2nd_level.indicator_discrete_achievement, pe_question_2nd_level.indicator_goal, pe_question_2nd_level.indicator_type, pe_question_2nd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_2nd_level.power, pe_question_2nd_level.leverage
                else
                  pe_assessment_question_2nd_level.indicator_achievement = fix_form_floats params['pe_question_'+pe_question_2nd_level.id.to_s]
                  pe_assessment_question_2nd_level.percentage = calculate_indicator_achievement pe_assessment_question_2nd_level.indicator_achievement, pe_question_2nd_level.indicator_goal, pe_question_2nd_level.indicator_type, pe_question_2nd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_2nd_level.power, pe_question_2nd_level.leverage
                end
  
                pe_assessment_question_2nd_level.points = pe_assessment_question_2nd_level.percentage

                pe_assessment_question_2nd_level.last_update = lms_time
                pe_assessment_question_2nd_level.save
  
              end
              
            end

          elsif pe_element_2nd.assessment_method == 3
            #valor numérico

            unless params['pe_question_'+pe_question_2nd_level.id.to_s].blank?

              pe_assessment_question_2nd_level.points = params['pe_question_'+pe_question_2nd_level.id.to_s]

              pe_assessment_question_2nd_level.percentage = calculate_alternative_result pe_assessment_question_2nd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

              pe_assessment_question_2nd_level.last_update = lms_time
              pe_assessment_question_2nd_level.save

            end

          end

          if pe_question_2nd_level.has_comment
            pe_assessment_question_2nd_level.comment = params['pe_question_comment_'+pe_question_2nd_level.id.to_s]
            pe_assessment_question_2nd_level.last_update = lms_time
            pe_assessment_question_2nd_level.save
          end

          if pe_question_2nd_level.has_comment_2
            pe_assessment_question_2nd_level.comment_2 = params['pe_question_comment_2_'+pe_question_2nd_level.id.to_s]
            pe_assessment_question_2nd_level.last_update = lms_time
            pe_assessment_question_2nd_level.save
          end

        end

      end

    end

  end

  def answer_evaluation_3_elements(pe_assessment_evaluation)

    pe_element_1st = @pe_evaluation.pe_elements.first
    pe_element_2nd = @pe_evaluation.pe_elements.second
    pe_element_3rd = @pe_evaluation.pe_elements.last

    if pe_element_3rd.assessed?

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question_1st_level|

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_questions.build
        pe_assessment_question_1st_level.pe_question = pe_question_1st_level
        pe_assessment_question_1st_level.save

        @pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question_2nd_level.pe_question = pe_question_2nd_level
          pe_assessment_question_2nd_level.pe_assessment_question = pe_assessment_question_1st_level
          pe_assessment_question_2nd_level.save

          @pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level.id).each do |pe_question_3rd_level|

            pe_assessment_question_3rd_level = pe_assessment_evaluation.pe_assessment_questions.build
            pe_assessment_question_3rd_level.pe_question = pe_question_3rd_level
            pe_assessment_question_3rd_level.pe_assessment_question = pe_assessment_question_2nd_level

            if pe_element_3rd.assessment_method == 1
              #alternativas

              pe_alternative = pe_question_3rd_level.pe_alternatives.where('fixed = ?', true).first

              unless pe_alternative

                pe_alternative = PeAlternative.find(params['pe_question_'+pe_question_3rd_level.id.to_s]) unless params['pe_question_'+pe_question_3rd_level.id.to_s].blank?

              end

              if pe_alternative

                pe_assessment_question_3rd_level.pe_alternative = pe_alternative
                pe_assessment_question_3rd_level.points = pe_alternative.value

                pe_assessment_question_3rd_level.percentage = calculate_alternative_result pe_assessment_question_3rd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

                pe_assessment_question_3rd_level.last_update = lms_time
                pe_assessment_question_3rd_level.save

              end

            elsif pe_element_3rd.assessment_method == 2
              #indicadores

              if pe_element_3rd.kpi_dashboard && pe_question_3rd_level.kpi_indicator

                if pe_question_3rd_level.indicator_type == 4
                  pe_assessment_question_3rd_level.indicator_discrete_achievement = pe_question_3rd_level.kpi_indicator.value
                else
                  pe_assessment_question_3rd_level.indicator_achievement = pe_question_3rd_level.kpi_indicator.value
                end

                pe_assessment_question_3rd_level.percentage = pe_question_3rd_level.kpi_indicator.percentage

                pe_assessment_question_3rd_level.points = pe_question_3rd_level.kpi_indicator.percentage

                pe_assessment_question_3rd_level.last_update = lms_time
                pe_assessment_question_3rd_level.save

              else

                unless params['pe_question_'+pe_question_3rd_level.id.to_s].blank?

                  if pe_question_3rd_level.indicator_type == 4
                    pe_assessment_question_3rd_level.indicator_discrete_achievement = params['pe_question_'+pe_question_3rd_level.id.to_s]
                    pe_assessment_question_3rd_level.percentage = calculate_indicator_achievement pe_assessment_question_3rd_level.indicator_discrete_achievement, pe_question_3rd_level.indicator_discrete_goal, pe_question_3rd_level.indicator_type, pe_question_3rd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_3rd_level.power, pe_question_3rd_level.leverage
                  else
                    pe_assessment_question_3rd_level.indicator_achievement = fix_form_floats params['pe_question_'+pe_question_3rd_level.id.to_s]
                    pe_assessment_question_3rd_level.percentage = calculate_indicator_achievement pe_assessment_question_3rd_level.indicator_achievement, pe_question_3rd_level.indicator_goal, pe_question_3rd_level.indicator_type, pe_question_3rd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_3rd_level.power, pe_question_3rd_level.leverage
                  end

                  pe_assessment_question_3rd_level.points = pe_assessment_question_3rd_level.percentage

                  pe_assessment_question_3rd_level.last_update = lms_time
                  pe_assessment_question_3rd_level.save

                end

              end

            elsif pe_element_3rd.assessment_method == 3
              #valor numérico

              unless params['pe_question_'+pe_question_3rd_level.id.to_s].blank?

                pe_assessment_question_3rd_level.points = params['pe_question_'+pe_question_3rd_level.id.to_s]

                pe_assessment_question_3rd_level.percentage = calculate_alternative_result pe_assessment_question_3rd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

                pe_assessment_question_3rd_level.last_update = lms_time
                pe_assessment_question_3rd_level.save

              end

            end

            if pe_question_3rd_level.has_comment
              pe_assessment_question_3rd_level.comment = params['pe_question_comment_'+pe_question_3rd_level.id.to_s]
              pe_assessment_question_3rd_level.last_update = lms_time
              pe_assessment_question_3rd_level.save
            end

            if pe_question_3rd_level.has_comment_2
              pe_assessment_question_3rd_level.comment_2 = params['pe_question_comment_2_'+pe_question_3rd_level.id.to_s]
              pe_assessment_question_3rd_level.last_update = lms_time
              pe_assessment_question_3rd_level.save
            end

          end

        end

      end

    end

  end

  def sync_assessment_kpis(pe_assessment_evaluation, pe_evaluation, pe_member_evaluated, pe_member_evaluator, pe_rel)

    pe_member_group = pe_member_evaluated.pe_member_group pe_evaluation

    sync_assessment_kpis_1_element(pe_assessment_evaluation, pe_evaluation, pe_member_evaluated, pe_member_evaluator, pe_rel, pe_member_group) if pe_evaluation.pe_elements.size == 1

    sync_assessment_kpis_2_elements_2(pe_assessment_evaluation, pe_evaluation, pe_member_evaluated, pe_member_evaluator, pe_rel, pe_member_group) if pe_evaluation.pe_elements.size == 2

    sync_assessment_kpis_3_elements_3(pe_assessment_evaluation, pe_evaluation, pe_member_evaluated, pe_member_evaluator, pe_rel, pe_member_group) if pe_evaluation.pe_elements.size == 3

  end

  def sync_assessment_kpis_1_element(pe_assessment_evaluation, pe_evaluation, pe_member_evaluated, pe_member_evaluator, pe_rel, pe_member_group)

    pe_element = pe_evaluation.pe_elements.first

    if pe_element.assessed?

      if pe_evaluation.pe_groups.size > 0

        pe_group = pe_member_group ? pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel).each do |pe_question|

        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

        unless pe_assessment_question

          pe_assessment_question = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question.pe_question = pe_question

        end

        if pe_element.assessment_method == 2

          #indicadores

          if pe_element.kpi_dashboard && pe_question.kpi_indicator

            if pe_question.indicator_type == 4
              pe_assessment_question.indicator_discrete_achievement = pe_question.kpi_indicator.value
            else
              pe_assessment_question.indicator_achievement = pe_question.kpi_indicator.value
            end

            pe_assessment_question.percentage = pe_question.kpi_indicator.percentage

            pe_assessment_question.points = pe_question.kpi_indicator.percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          end

        end

      end

    end

  end

  def sync_assessment_kpis_2_elements_2(pe_assessment_evaluation, pe_evaluation, pe_member_evaluated, pe_member_evaluator, pe_rel, pe_member_group)

    pe_element_2nd = pe_evaluation.pe_elements.second

    if pe_element_2nd.assessed?

      if pe_evaluation.pe_groups.size > 0

        pe_group = pe_member_group ? pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel).each do |pe_question_1st_level|

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_question(pe_question_1st_level)

        unless pe_assessment_question_1st_level

          pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question_1st_level.pe_question = pe_question_1st_level
          pe_assessment_question_1st_level.save

        end

        pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd_level)

          unless pe_assessment_question_2nd_level

            pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_questions.build
            pe_assessment_question_2nd_level.pe_question = pe_question_2nd_level
            pe_assessment_question_2nd_level.pe_assessment_question = pe_assessment_question_1st_level

          end

          if pe_element_2nd.assessment_method == 2
            #indicadores

            if pe_element_2nd.kpi_dashboard && pe_question_2nd_level.kpi_indicator

              if pe_question_2nd_level.indicator_type == 4
                pe_assessment_question_2nd_level.indicator_discrete_achievement = pe_question_2nd_level.kpi_indicator.value
              else
                pe_assessment_question_2nd_level.indicator_achievement = pe_question_2nd_level.kpi_indicator.value
              end

              pe_assessment_question_2nd_level.percentage = pe_question_2nd_level.kpi_indicator.percentage

              pe_assessment_question_2nd_level.points = pe_question_2nd_level.kpi_indicator.percentage

              pe_assessment_question_2nd_level.last_update = lms_time
              pe_assessment_question_2nd_level.save

            end

          end

        end

      end

    end

  end

  def sync_assessment_kpis_3_elements_3(pe_assessment_evaluation, pe_evaluation, pe_member_evaluated, pe_member_evaluator, pe_rel, pe_member_group)

    pe_element_3rd = pe_evaluation.pe_elements.last

    if pe_element_3rd.assessed?

      if pe_evaluation.pe_groups.size > 0

        pe_group = pe_member_group ? pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_evaluated, pe_member_evaluator, pe_group, pe_rel).each do |pe_question_1st_level|

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_question(pe_question_1st_level)

        unless pe_assessment_question_1st_level

          pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question_1st_level.pe_question = pe_question_1st_level
          pe_assessment_question_1st_level.save

        end

        pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd_level)

          unless pe_assessment_question_2nd_level

            pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_questions.build
            pe_assessment_question_2nd_level.pe_question = pe_question_2nd_level
            pe_assessment_question_2nd_level.pe_assessment_question = pe_assessment_question_1st_level
            pe_assessment_question_2nd_level.save

          end

          pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level.id).each do |pe_question_3rd_level|

            pe_assessment_question_3rd_level = pe_assessment_evaluation.pe_assessment_question(pe_question_3rd_level)

            unless pe_assessment_question_3rd_level

              pe_assessment_question_3rd_level = pe_assessment_evaluation.pe_assessment_questions.build
              pe_assessment_question_3rd_level.pe_question = pe_question_3rd_level
              pe_assessment_question_3rd_level.pe_assessment_question = pe_assessment_question_2nd_level

            end

            if pe_element_3rd.assessment_method == 2
              #indicadores

              if pe_element_3rd.kpi_dashboard && pe_question_3rd_level.kpi_indicator

                if pe_question_3rd_level.indicator_type == 4
                  pe_assessment_question_3rd_level.indicator_discrete_achievement = pe_question_3rd_level.kpi_indicator.value
                else
                  pe_assessment_question_3rd_level.indicator_achievement = pe_question_3rd_level.kpi_indicator.value
                end

                pe_assessment_question_3rd_level.percentage = pe_question_3rd_level.kpi_indicator.percentage

                pe_assessment_question_3rd_level.points = pe_question_3rd_level.kpi_indicator.percentage

                pe_assessment_question_3rd_level.last_update = lms_time
                pe_assessment_question_3rd_level.save

              end

            end

          end

        end

      end

    end

  end

  def manager_answer_evaluation_xls_1_element(pe_assessment_evaluation, logro, pe_question_id)

    pe_element = @pe_evaluation.pe_elements.first

    if pe_element.assessed?

      pe_question = @pe_evaluation.pe_questions.where('id = ?', pe_question_id).first

      if pe_question

        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question

        unless pe_assessment_question

          pe_assessment_question = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question.pe_question = pe_question

        end

        if pe_element.assessment_method == 1
          #alternativas

          pe_alternative = pe_question.pe_alternatives.where('fixed = ?', true).first

          unless pe_alternative

            pe_alternative = pe_question.pe_alternatives.where('description = ?', logro).first unless logro.blank?

          end

          if pe_alternative

            pe_assessment_question.pe_alternative = pe_alternative
            pe_assessment_question.points = pe_alternative.value

            pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          end

        elsif pe_element.assessment_method == 2

          #indicadores

          if pe_element.kpi_dashboard && pe_question.kpi_indicator

            if pe_question.indicator_type == 4
              pe_assessment_question.indicator_discrete_achievement = pe_question.kpi_indicator.value
            else
              pe_assessment_question.indicator_achievement = pe_question.kpi_indicator.value
            end

            pe_assessment_question.percentage = pe_question.kpi_indicator.percentage

            pe_assessment_question.points = pe_question.kpi_indicator.percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          else

            unless logro.blank?

              if pe_question.indicator_type == 4
                pe_assessment_question.indicator_discrete_achievement = logro
                pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_discrete_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
              else
                pe_assessment_question.indicator_achievement = logro
                pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
              end

              pe_assessment_question.points = pe_assessment_question.percentage

              pe_assessment_question.last_update = lms_time
              pe_assessment_question.save

            end

          end

        elsif pe_element.assessment_method == 3
          #valor numérico

          unless logro.blank?

            pe_assessment_question.points = logro

            pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          end

        end

      end

    end

  end

  def manager_answer_evaluation_xls_2_elements(pe_assessment_evaluation, logro, pe_question_id)


    pe_element_1st = @pe_evaluation.pe_elements.first
    pe_element_2nd = @pe_evaluation.pe_elements.second

    if pe_element_2nd.assessed?

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question_1st_level|

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_questions.build
        pe_assessment_question_1st_level.pe_question = pe_question_1st_level
        pe_assessment_question_1st_level.save

        @pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question_2nd_level.pe_question = pe_question_2nd_level
          pe_assessment_question_2nd_level.pe_assessment_question = pe_assessment_question_1st_level

          if pe_element_2nd.assessment_method == 1
            #alternativas

            pe_alternative = pe_question_2nd_level.pe_alternatives.where('fixed = ?', true).first

            unless pe_alternative

              pe_alternative = PeAlternative.find(params['pe_question_'+pe_question_2nd_level.id.to_s]) unless params['pe_question_'+pe_question_2nd_level.id.to_s].blank?

            end

            if pe_alternative

              pe_assessment_question_2nd_level.pe_alternative = pe_alternative
              pe_assessment_question_2nd_level.points = pe_alternative.value

              pe_assessment_question_2nd_level.percentage = calculate_alternative_result pe_assessment_question_2nd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

              pe_assessment_question_2nd_level.last_update = lms_time
              pe_assessment_question_2nd_level.save

            end

          elsif pe_element_2nd.assessment_method == 2
            #indicadores

            if pe_element_2nd.kpi_dashboard && pe_question_2nd_level.kpi_indicator

              if pe_question_2nd_level.indicator_type == 4
                pe_assessment_question_2nd_level.indicator_discrete_achievement = pe_question_2nd_level.kpi_indicator.value
              else
                pe_assessment_question_2nd_level.indicator_achievement = pe_question_2nd_level.kpi_indicator.value
              end

              pe_assessment_question_2nd_level.percentage = pe_question_2nd_level.kpi_indicator.percentage

              pe_assessment_question_2nd_level.points = pe_question_2nd_level.kpi_indicator.percentage

              pe_assessment_question_2nd_level.last_update = lms_time
              pe_assessment_question_2nd_level.save

            else

              unless params['pe_question_'+pe_question_2nd_level.id.to_s].blank?

                if pe_question_2nd_level.indicator_type == 4
                  pe_assessment_question_2nd_level.indicator_discrete_achievement = params['pe_question_'+pe_question_2nd_level.id.to_s]
                  pe_assessment_question_2nd_level.percentage = calculate_indicator_achievement pe_assessment_question_2nd_level.indicator_discrete_achievement, pe_question_2nd_level.indicator_goal, pe_question_2nd_level.indicator_type, pe_question_2nd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_2nd_level.power, pe_question_2nd_level.leverage
                else
                  pe_assessment_question_2nd_level.indicator_achievement = params['pe_question_'+pe_question_2nd_level.id.to_s].to_f
                  pe_assessment_question_2nd_level.percentage = calculate_indicator_achievement pe_assessment_question_2nd_level.indicator_achievement, pe_question_2nd_level.indicator_goal, pe_question_2nd_level.indicator_type, pe_question_2nd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_2nd_level.power, pe_question_2nd_level.leverage
                end

                pe_assessment_question_2nd_level.points = pe_assessment_question_2nd_level.percentage

                pe_assessment_question_2nd_level.last_update = lms_time
                pe_assessment_question_2nd_level.save

              end

            end

          elsif pe_element_2nd.assessment_method == 3
            #valor numérico

            unless params['pe_question_'+pe_question_2nd_level.id.to_s].blank?

              pe_assessment_question_2nd_level.points = params['pe_question_'+pe_question_2nd_level.id.to_s]

              pe_assessment_question_2nd_level.percentage = calculate_alternative_result pe_assessment_question_2nd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

              pe_assessment_question_2nd_level.last_update = lms_time
              pe_assessment_question_2nd_level.save

            end

          end

          if pe_question_2nd_level.has_comment
            pe_assessment_question_2nd_level.comment = params['pe_question_comment_'+pe_question_2nd_level.id.to_s]
            pe_assessment_question_2nd_level.save
          end

        end

      end

    end

  end

  def manager_answer_evaluation_xls_3_elements(pe_assessment_evaluation, logro, pe_question_id)

    pe_element_3rd = @pe_evaluation.pe_elements.last

    if pe_element_3rd.assessed?

      pe_question = @pe_evaluation.pe_questions.where('id = ?', pe_question_id).first

      if pe_question

        pe_question_2nd_level = pe_question.pe_question
        pe_question_1st_level = pe_question_2nd_level.pe_question

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_question pe_question_1st_level

        unless pe_assessment_question_1st_level

          pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question_1st_level.pe_question = pe_question_1st_level
          pe_assessment_question_1st_level.save

        end

        pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_question pe_question_2nd_level

        unless pe_assessment_question_2nd_level

          pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question_2nd_level.pe_question = pe_question_2nd_level
          pe_assessment_question_2nd_level.pe_assessment_question = pe_assessment_question_1st_level
          pe_assessment_question_2nd_level.save

        end

        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question

        unless pe_assessment_question

          pe_assessment_question = pe_assessment_evaluation.pe_assessment_questions.build
          pe_assessment_question.pe_assessment_question = pe_assessment_question_2nd_level
          pe_assessment_question.pe_question = pe_question

        end

        if pe_element_3rd.assessment_method == 1
          #alternativas

          pe_alternative = pe_question.pe_alternatives.where('fixed = ?', true).first

          unless pe_alternative

            pe_alternative = pe_question.pe_alternatives.where('description = ?', logro).first unless logro.blank?

          end

          if pe_alternative

            pe_assessment_question.pe_alternative = pe_alternative
            pe_assessment_question.points = pe_alternative.value

            pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          end

        elsif pe_element_3rd.assessment_method == 2

          #indicadores

          if pe_element_3rd.kpi_dashboard && pe_question.kpi_indicator

            if pe_question.indicator_type == 4
              pe_assessment_question.indicator_discrete_achievement = pe_question.kpi_indicator.value
            else
              pe_assessment_question.indicator_achievement = pe_question.kpi_indicator.value
            end

            pe_assessment_question.percentage = pe_question.kpi_indicator.percentage

            pe_assessment_question.points = pe_question.kpi_indicator.percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          else

            unless logro.blank?

              if pe_question.indicator_type == 4
                pe_assessment_question.indicator_discrete_achievement = logro
                pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_discrete_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
              else
                pe_assessment_question.indicator_achievement = logro
                pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
              end

              pe_assessment_question.points = pe_assessment_question.percentage

              pe_assessment_question.last_update = lms_time
              pe_assessment_question.save

            end

          end

        elsif pe_element_3rd.assessment_method == 3
          #valor numérico

          unless logro.blank?

            pe_assessment_question.points = logro

            pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage

            pe_assessment_question.last_update = lms_time
            pe_assessment_question.save

          end

        end

      end

    end

  end

  def re_calculate_answer_evaluation(pe_assessment_evaluation)

    re_calculate_answer_evaluation_1_element pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 1

    re_calculate_answer_evaluation_2_elements pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 2

    re_calculate_answer_evaluation_3_elements pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 3


  end

  def re_calculate_answer_evaluation_1_element(pe_assessment_evaluation)

    pe_element = @pe_evaluation.pe_elements.first

    if pe_element.assessed?

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question|

        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question

        if pe_assessment_question

          if pe_element.assessment_method == 1
          #alternativas

          pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage
          pe_assessment_question.save

          elsif pe_element.assessment_method == 2

            #indicadores

            if pe_element.kpi_dashboard && pe_question.kpi_indicator

              pe_assessment_question.percentage = pe_question.kpi_indicator.percentage
              pe_assessment_question.points = pe_question.kpi_indicator.percentage
              pe_assessment_question.save

            else

              if pe_question.indicator_type == 4
                pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_discrete_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
              else
                pe_assessment_question.percentage = calculate_indicator_achievement pe_assessment_question.indicator_achievement, pe_question.indicator_goal, pe_question.indicator_type, pe_question.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question.power, pe_question.leverage
              end

              pe_assessment_question.points = pe_assessment_question.percentage

              pe_assessment_question.save

            end

          elsif pe_element.assessment_method == 3
            #valor numérico

            pe_assessment_question.percentage = calculate_alternative_result pe_assessment_question.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage
            pe_assessment_question.save

          end

        end

      end

    end

  end

  def re_calculate_answer_evaluation_2_elements(pe_assessment_evaluation)


    pe_element_1st = @pe_evaluation.pe_elements.first
    pe_element_2nd = @pe_evaluation.pe_elements.second

    if pe_element_2nd.assessed?

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question_1st_level|

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_question pe_question_1st_level

        if pe_assessment_question_1st_level

          @pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

            pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_question pe_question_2nd_level

            if pe_assessment_question_2nd_level

              if pe_element_2nd.assessment_method == 1
                #alternativas
                if pe_assessment_question_2nd_level.points
                  pe_assessment_question_2nd_level.percentage = calculate_alternative_result pe_assessment_question_2nd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage
                  pe_assessment_question_2nd_level.save
                end

              elsif pe_element_2nd.assessment_method == 2
                #indicadores

                if pe_element_2nd.kpi_dashboard && pe_question_2nd_level.kpi_indicator

                  pe_assessment_question_2nd_level.percentage = pe_question_2nd_level.kpi_indicator.percentage
                  pe_assessment_question_2nd_level.points = pe_question_2nd_level.kpi_indicator.percentage
                  pe_assessment_question_2nd_level.save

                else


                    if pe_question_2nd_level.indicator_type == 4
                      pe_assessment_question_2nd_level.percentage = calculate_indicator_achievement pe_assessment_question_2nd_level.indicator_discrete_achievement, pe_question_2nd_level.indicator_goal, pe_question_2nd_level.indicator_type, pe_question_2nd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_2nd_level.power, pe_question_2nd_level.leverage
                    else
                      pe_assessment_question_2nd_level.percentage = calculate_indicator_achievement pe_assessment_question_2nd_level.indicator_achievement, pe_question_2nd_level.indicator_goal, pe_question_2nd_level.indicator_type, pe_question_2nd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_2nd_level.power, pe_question_2nd_level.leverage
                    end

                    pe_assessment_question_2nd_level.points = pe_assessment_question_2nd_level.percentage
                    pe_assessment_question_2nd_level.save


                end

              elsif pe_element_2nd.assessment_method == 3
                #valor numérico

                pe_assessment_question_2nd_level.percentage = calculate_alternative_result pe_assessment_question_2nd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage
                pe_assessment_question_2nd_level.save

              end


            end

          end

        end

      end

    end

  end

  def re_calculate_answer_evaluation_3_elements(pe_assessment_evaluation)

    pe_element_1st = @pe_evaluation.pe_elements.first
    pe_element_2nd = @pe_evaluation.pe_elements.second
    pe_element_3rd = @pe_evaluation.pe_elements.last

    if pe_element_3rd.assessed?

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question_1st_level|

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_question pe_question_1st_level

        if pe_assessment_question_1st_level

          @pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_question pe_question_2nd_level

            if pe_assessment_question_2nd_level

              @pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level.id).each do |pe_question_3rd_level|

                pe_assessment_question_3rd_level = pe_assessment_evaluation.pe_assessment_question pe_question_3rd_level

                if pe_assessment_question_3rd_level

                  if pe_element_3rd.assessment_method == 1
                  #alternativas

                    pe_assessment_question_3rd_level.percentage = calculate_alternative_result pe_assessment_question_3rd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage
                    pe_assessment_question_3rd_level.save


                  elsif pe_element_3rd.assessment_method == 2
                    #indicadores

                    if pe_element_3rd.kpi_dashboard && pe_question_3rd_level.kpi_indicator

                      pe_assessment_question_3rd_level.percentage = pe_question_3rd_level.kpi_indicator.percentage
                      pe_assessment_question_3rd_level.points = pe_question_3rd_level.kpi_indicator.percentage
                      pe_assessment_question_3rd_level.save

                    else

                      if pe_question_3rd_level.indicator_type == 4
                        pe_assessment_question_3rd_level.percentage = calculate_indicator_achievement pe_assessment_question_3rd_level.indicator_discrete_achievement, pe_question_3rd_level.indicator_discrete_goal, pe_question_3rd_level.indicator_type, pe_question_3rd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_3rd_level.power, pe_question_3rd_level.leverage
                      else
                        pe_assessment_question_3rd_level.percentage = calculate_indicator_achievement pe_assessment_question_3rd_level.indicator_achievement, pe_question_3rd_level.indicator_goal, pe_question_3rd_level.indicator_type, pe_question_3rd_level.pe_question_ranks, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage, pe_question_3rd_level.power, pe_question_3rd_level.leverage
                      end

                      pe_assessment_question_3rd_level.points = pe_assessment_question_3rd_level.percentage
                      pe_assessment_question_3rd_level.save

                    end

                  elsif pe_element_3rd.assessment_method == 3
                    #valor numérico

                    pe_assessment_question_3rd_level.percentage = calculate_alternative_result pe_assessment_question_3rd_level.points, @pe_evaluation.points_to_ohp, @pe_evaluation.min_percentage, @pe_evaluation.max_percentage
                    pe_assessment_question_3rd_level.save

                  end

                end

              end

            end

          end

        end

      end

    end

  end


  def calculate_assessment_evaluation(pe_assessment_evaluation)

    calculate_assessment_evaluation_1_element pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 1

    calculate_assessment_evaluation_2_elements pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 2

    calculate_assessment_evaluation_3_elements pe_assessment_evaluation if @pe_evaluation.pe_elements.size == 3

  end

  def calculate_assessment_evaluation_1_element(pe_assessment_evaluation)

    pe_assessment_evaluation.reload

    pe_element = @pe_evaluation.pe_elements.first

    total_num_questions = 0
    total_num_not_answered_questions = 0.0

    if pe_element.assessed?

      sum_weights = 0

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question|

        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

        if pe_assessment_question && pe_assessment_question.points
          sum_weights += pe_question.weight
        end
        total_num_questions += 1

      end

      sum_points = 0

      pe_assessment_evaluation.pe_assessment_questions.each do |pe_assessment_question|

        if pe_assessment_question.points && pe_assessment_question.points > 0
          sum_points += pe_assessment_question.points*pe_assessment_question.pe_question.weight
        end

        if pe_assessment_question.points == -1
          total_num_not_answered_questions += 1
          sum_weights -= pe_assessment_question.pe_question.weight
        end

      end

      if sum_weights > 0

        sum_points = sum_points/sum_weights.to_f if pe_element.calculus_method == 0 #promedio_ponderado

        pe_assessment_evaluation.points = sum_points

        pe_assessment_evaluation.percentage = sum_points*100/@pe_evaluation.points_to_ohp

        pe_assessment_evaluation.percentage = @pe_evaluation.min_percentage if pe_assessment_evaluation.percentage < @pe_evaluation.min_percentage
        pe_assessment_evaluation.percentage = @pe_evaluation.max_percentage if pe_assessment_evaluation.percentage > @pe_evaluation.max_percentage


        @pe_evaluation.pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

          if pe_assessment_evaluation.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_evaluation.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
            pe_assessment_evaluation.pe_dimension_group = pe_dimension_group
            break
          end

        end

        pe_assessment_evaluation.valid_evaluation = false if (total_num_not_answered_questions/total_num_questions)*100 > @pe_evaluation.max_per_cant_answer

        pe_assessment_evaluation.save

      else

        #pe_assessment_evaluation.destroy

      end

    end

  end

  def calculate_assessment_evaluation_2_elements(pe_assessment_evaluation)

    pe_assessment_evaluation.reload

    pe_element_1st = @pe_evaluation.pe_elements.first
    pe_element_2nd = @pe_evaluation.pe_elements.second

    total_num_questions = 0
    total_num_not_answered_questions = 0.0

    if pe_element_2nd.assessed?

      sum_weights = 0

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question_1st_level|

        sum_weights += pe_question_1st_level.weight if pe_element_1st.assessed?

        sum_weights_2nd_level = 0

        @pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd_level)

          if pe_assessment_question && pe_assessment_question.points

            sum_weights += pe_question_2nd_level.weight unless pe_element_1st.assessed?

            sum_weights_2nd_level += pe_question_2nd_level.weight

          end

          total_num_questions += 1

        end

        sum_points_2nd_level = 0

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_question pe_question_1st_level

        if pe_assessment_question_1st_level

          pe_assessment_question_1st_level.pe_assessment_questions.each do |pe_assessment_question_2nd_level|

            if pe_assessment_question_2nd_level.points && pe_assessment_question_2nd_level.points > 0
              sum_points_2nd_level += pe_assessment_question_2nd_level.points*pe_assessment_question_2nd_level.pe_question.weight
            end

            if pe_assessment_question_2nd_level.points == -1
              total_num_not_answered_questions += 1
              sum_weights_2nd_level -= pe_assessment_question_2nd_level.pe_question.weight
              sum_weights -= pe_assessment_question_2nd_level.pe_question.weight unless pe_element_1st.assessed?
            end

          end

        end

        if sum_weights_2nd_level > 0

          sum_points_2nd_level = sum_points_2nd_level/sum_weights_2nd_level.to_f if pe_element_2nd.calculus_method == 0 #promedio_ponderado

          pe_assessment_question_1st_level.points = sum_points_2nd_level

          pe_assessment_question_1st_level.percentage = sum_points_2nd_level*100/@pe_evaluation.points_to_ohp

          pe_assessment_question_1st_level.percentage = @pe_evaluation.min_percentage if pe_assessment_question_1st_level.percentage < @pe_evaluation.min_percentage
          pe_assessment_question_1st_level.percentage = @pe_evaluation.max_percentage if pe_assessment_question_1st_level.percentage > @pe_evaluation.max_percentage

          pe_assessment_question_1st_level.save

        end

      end

      #pe_assessment_evaluation.reload

      sum_points = 0

      pe_assessment_questions = pe_element_1st.assessed ? pe_assessment_evaluation.pe_assessment_questions_1st_level(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel) : pe_assessment_evaluation.pe_assessment_questions_2nd_level(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel)

      pe_assessment_questions.each do |pe_assessment_question|

        if pe_assessment_question.points && pe_assessment_question.points > 0
          sum_points += pe_assessment_question.points*pe_assessment_question.pe_question.weight
        end

        unless pe_assessment_question.points
          sum_weights -= pe_assessment_question.pe_question.weight if pe_element_1st.assessed?
        end

      end

      if sum_weights > 0

        sum_points = sum_points/sum_weights.to_f if pe_element_1st.calculus_method == 0 #promedio_ponderado

        pe_assessment_evaluation.points = sum_points

        pe_assessment_evaluation.percentage = sum_points*100/@pe_evaluation.points_to_ohp

        pe_assessment_evaluation.percentage = @pe_evaluation.min_percentage if pe_assessment_evaluation.percentage < @pe_evaluation.min_percentage
        pe_assessment_evaluation.percentage = @pe_evaluation.max_percentage if pe_assessment_evaluation.percentage > @pe_evaluation.max_percentage


        @pe_evaluation.pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

          if pe_assessment_evaluation.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_evaluation.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
            pe_assessment_evaluation.pe_dimension_group = pe_dimension_group
            break
          end

        end

        pe_assessment_evaluation.valid_evaluation = false if (total_num_not_answered_questions/total_num_questions)*100 > @pe_evaluation.max_per_cant_answer

        pe_assessment_evaluation.save

      else

        #pe_assessment_evaluation.destroy

      end

    end

  end

  def calculate_assessment_evaluation_3_elements(pe_assessment_evaluation)

    pe_assessment_evaluation.reload

    pe_element_1st = @pe_evaluation.pe_elements.first
    pe_element_2nd = @pe_evaluation.pe_elements.second
    pe_element_3rd = @pe_evaluation.pe_elements.last

    total_num_questions = 0
    total_num_not_answered_questions = 0.0

    if pe_element_3rd.assessed?

      sum_weights = 0

      if @pe_evaluation.pe_groups.size > 0

        pe_group = @pe_member_group ? @pe_member_group.pe_group : nil

      else

        pe_group = nil

      end

      @pe_evaluation.pe_questions_by_pe_member_1st_level_ready(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel).each do |pe_question_1st_level|

        sum_weights += pe_question_1st_level.weight if pe_element_1st.assessed?

        sum_weights_2nd_level = 0

        @pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level.id).each do |pe_question_2nd_level|

          sum_weights += pe_question_2nd_level.weight if !pe_element_1st.assessed? && pe_element_2nd.assessed?

          sum_weights_2nd_level += pe_question_2nd_level.weight

          sum_weights_3rd_level = 0

          @pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level.id).each do |pe_question_3rd_level|

            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_3rd_level)

            if pe_assessment_question && pe_assessment_question.points

              sum_weights += pe_question_3rd_level.weight unless pe_element_2nd.assessed?

              sum_weights_3rd_level += pe_question_3rd_level.weight

            end

            total_num_questions += 1

          end

          sum_points_3rd_level = 0

          pe_assessment_question_2nd_level = pe_assessment_evaluation.pe_assessment_question pe_question_2nd_level

          if pe_assessment_question_2nd_level

            pe_assessment_question_2nd_level.pe_assessment_questions.each do |pe_assessment_question_3rd_level|


              if pe_assessment_question_3rd_level.points && pe_assessment_question_3rd_level.points > 0
                sum_points_3rd_level += pe_assessment_question_3rd_level.points*pe_assessment_question_3rd_level.pe_question.weight
              end

              if pe_assessment_question_3rd_level.points == -1
                total_num_not_answered_questions += 1
                sum_weights_3rd_level -= pe_assessment_question_3rd_level.pe_question.weight
                sum_weights -= pe_assessment_question_3rd_level.pe_question.weight unless pe_element_2nd.assessed?
              end

            end

          end

          if sum_weights_3rd_level > 0

            sum_points_3rd_level = sum_points_3rd_level/sum_weights_3rd_level.to_f if pe_element_3rd.calculus_method == 0 #promedio_ponderado

            pe_assessment_question_2nd_level.points = sum_points_3rd_level

            pe_assessment_question_2nd_level.percentage = sum_points_3rd_level*100/@pe_evaluation.points_to_ohp

            pe_assessment_question_2nd_level.percentage = @pe_evaluation.min_percentage if pe_assessment_question_2nd_level.percentage < @pe_evaluation.min_percentage
            pe_assessment_question_2nd_level.percentage = @pe_evaluation.max_percentage if pe_assessment_question_2nd_level.percentage > @pe_evaluation.max_percentage

            pe_assessment_question_2nd_level.save

          end


        end

        sum_points_2nd_level = 0

        pe_assessment_question_1st_level = pe_assessment_evaluation.pe_assessment_question pe_question_1st_level

        if pe_assessment_question_1st_level

          pe_assessment_question_1st_level.pe_assessment_questions.each do |pe_assessment_question_2nd_level|

            if pe_element_2nd.assessed

              if pe_assessment_question_2nd_level.points && pe_assessment_question_2nd_level.points > 0
                sum_points_2nd_level += pe_assessment_question_2nd_level.points*pe_assessment_question_2nd_level.pe_question.weight
              end

            else

              pe_assessment_question_2nd_level.pe_assessment_questions.each do |pe_assessment_question_3rd_level|

                if pe_assessment_question_3rd_level.points && pe_assessment_question_3rd_level.points > 0
                  sum_points_2nd_level += pe_assessment_question_3rd_level.points*pe_assessment_question_3rd_level.pe_question.weight
                end

              end

            end

          end

        end

        if sum_weights_2nd_level > 0

          sum_points_2nd_level = sum_points_2nd_level/sum_weights_2nd_level.to_f if pe_element_2nd.calculus_method == 0 #promedio_ponderado

          pe_assessment_question_1st_level.points = sum_points_2nd_level

          pe_assessment_question_1st_level.percentage = sum_points_2nd_level*100/@pe_evaluation.points_to_ohp

          pe_assessment_question_1st_level.percentage = @pe_evaluation.min_percentage if pe_assessment_question_1st_level.percentage < @pe_evaluation.min_percentage
          pe_assessment_question_1st_level.percentage = @pe_evaluation.max_percentage if pe_assessment_question_1st_level.percentage > @pe_evaluation.max_percentage

          pe_assessment_question_1st_level.save

        end

      end

      pe_assessment_evaluation.reload

      sum_points = 0

      if pe_element_1st.assessed
        pe_assessment_questions = pe_assessment_evaluation.pe_assessment_questions_1st_level(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel)
      elsif pe_element_2nd.assessed
        pe_assessment_questions = pe_assessment_evaluation.pe_assessment_questions_2nd_level(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel)
      else
        pe_assessment_questions = pe_assessment_evaluation.pe_assessment_questions_3rd_level(@pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel)
      end

      pe_assessment_questions.each do |pe_assessment_question|

        if pe_assessment_question.points && pe_assessment_question.points > 0
          sum_points += pe_assessment_question.points*pe_assessment_question.pe_question.weight
        end

      end

      if sum_weights > 0

        sum_points = sum_points/sum_weights.to_f if pe_element_1st.calculus_method == 0 #promedio_ponderado

        pe_assessment_evaluation.points = sum_points

        pe_assessment_evaluation.percentage = sum_points*100/@pe_evaluation.points_to_ohp

        pe_assessment_evaluation.percentage = @pe_evaluation.min_percentage if pe_assessment_evaluation.percentage < @pe_evaluation.min_percentage
        pe_assessment_evaluation.percentage = @pe_evaluation.max_percentage if pe_assessment_evaluation.percentage > @pe_evaluation.max_percentage

        @pe_evaluation.pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

          if pe_assessment_evaluation.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_evaluation.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
            pe_assessment_evaluation.pe_dimension_group = pe_dimension_group
            break
          end

        end

        pe_assessment_evaluation.valid_evaluation = false if (total_num_not_answered_questions/total_num_questions)*100 > @pe_evaluation.max_per_cant_answer

        pe_assessment_evaluation.save

      else

        #pe_assessment_evaluation.destroy

      end

    end

  end



  def calculate_assessment_final_evaluation(pe_member_evaluated, pe_evaluation, force_calculus = false)

    pe_assessment_final_evaluation = pe_member_evaluated.pe_assessment_final_evaluation(pe_evaluation)

    unless pe_assessment_final_evaluation && pe_assessment_final_evaluation.original_percentage

      pe_member_evaluated.pe_assessment_final_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).destroy_all

      if pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_evaluation)

        pe_assessment_final_evaluation = pe_member_evaluated.pe_assessment_final_evaluations.build
        pe_assessment_final_evaluation.pe_process = pe_member_evaluated.pe_process
        pe_assessment_final_evaluation.pe_evaluation = pe_evaluation
        pe_assessment_final_evaluation.last_update = lms_time

        pe_assessment_final_evaluation.points = -1

        pe_assessment_final_evaluation.percentage = -1

        pe_assessment_final_evaluation.save


        if pe_evaluation.final_evaluation_operation_evaluations == 0
          #por resultado final

          total_weight = 0
          total_points = 0

          if pe_evaluation.final_evaluation_operation_evaluators == 0

            #agrupando relaciones

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

              tmp_weight = 0
              tmp_points = 0

              pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus || pe_evaluation_rel.final_temporal_assessment)

                  pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                  if pe_assessment_evaluation

                    tmp_weight += pe_member_rel_is_evaluated.weight

                    tmp_points += pe_assessment_evaluation.points*pe_member_rel_is_evaluated.weight if pe_assessment_evaluation.points > 0

                  end

                end

              end

              if tmp_weight > 0

                tmp_points = tmp_points/tmp_weight.to_f

                total_points += tmp_points*pe_evaluation_rel.weight
                total_weight += pe_evaluation_rel.weight

              end

            end

          elsif pe_evaluation.final_evaluation_operation_evaluators == 1

            #sin agrupar relaciones

            pe_member_evaluated.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

              pe_evaluation_rel = pe_evaluation.pe_evaluation_rel pe_member_rel_is_evaluated.pe_rel

              if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus || (pe_evaluation_rel && pe_evaluation_rel.final_temporal_assessment))

                pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                if pe_assessment_evaluation

                  pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                  total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                  total_points += pe_assessment_evaluation.points*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.points > 0

                end

              end

            end

          elsif pe_evaluation.final_evaluation_operation_evaluators == 2

            #agrupando areas

            pe_areas_evaluators = Array.new

            pe_member_evaluated.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
              pe_areas_evaluators.push pe_member_rel_is_evaluated.pe_member_evaluator.pe_area
            end

            pe_areas_evaluators.uniq!

            pe_areas_evaluators.each do |pe_area_evaluator|

              tmp_weight = 0
              tmp_points = 0

              pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_area(pe_area_evaluator).each do |pe_member_rel_is_evaluated|

                pe_evaluation_rel = pe_evaluation.pe_evaluation_rel pe_member_rel_is_evaluated.pe_rel

                if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus || (pe_evaluation_rel && pe_evaluation_rel.final_temporal_assessment))

                  pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                  if pe_assessment_evaluation

                    pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                    tmp_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                    tmp_points += pe_assessment_evaluation.points*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.points > 0

                  end

                end

              end

              if tmp_weight > 0

                tmp_points = tmp_points/tmp_weight.to_f

                total_points += tmp_points*1
                total_weight += 1

              end

            end

          end

          if total_weight > 0

            pe_assessment_final_evaluation.points = total_points/total_weight.to_f
            pe_assessment_final_evaluation.percentage = pe_assessment_final_evaluation.points*100/pe_evaluation.points_to_ohp

            pe_assessment_final_evaluation.percentage = pe_evaluation.min_percentage if pe_assessment_final_evaluation.percentage < pe_evaluation.min_percentage
            pe_assessment_final_evaluation.percentage = pe_evaluation.max_percentage if pe_assessment_final_evaluation.percentage > pe_evaluation.max_percentage

            pe_evaluation.pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

              if pe_assessment_final_evaluation.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_final_evaluation.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
                pe_assessment_final_evaluation.pe_dimension_group = pe_dimension_group
                break
              end

            end

          end

        elsif pe_evaluation.final_evaluation_operation_evaluations == 1
          #por pregunta

          if pe_evaluation.pe_elements.size == 1

            calculate_final_questions_1_element(pe_member_evaluated, pe_assessment_final_evaluation, pe_evaluation, force_calculus)

            pe_assessment_final_evaluation = calculate_final_evaluation_1_element(pe_member_evaluated, pe_assessment_final_evaluation, pe_evaluation, force_calculus)

          elsif pe_evaluation.pe_elements.size == 2

            calculate_final_questions_2_elements(pe_member_evaluated, pe_assessment_final_evaluation, pe_evaluation, force_calculus)

            pe_assessment_final_evaluation = calculate_final_evaluation_2_elements(pe_member_evaluated,pe_assessment_final_evaluation, pe_evaluation, force_calculus)

          elsif pe_evaluation.pe_elements.size == 3

          end

        end

        pe_assessment_final_evaluation.save

        unless pe_evaluation.pe_process.of_persons?

          if pe_member_evaluated.pe_area

            pe_area_evaluated = pe_member_evaluated.pe_area

            pe_area_evaluated.pe_assessment_final_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).destroy_all

            pe_assessment_final_evaluation = pe_area_evaluated.pe_assessment_final_evaluations.build
            pe_assessment_final_evaluation.pe_process = pe_member_evaluated.pe_process
            pe_assessment_final_evaluation.pe_evaluation = pe_evaluation
            pe_assessment_final_evaluation.last_update = lms_time

            pe_assessment_final_evaluation.points = -1

            pe_assessment_final_evaluation.percentage = -1

            total_weight = 0
            total_points = 0

            pe_area_evaluated.pe_members.each do |pe_member_evaluated_aux|

              if pe_member_evaluated_aux.is_evaluated

                pe_member_evaluated_aux.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                  if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus)

                    pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                    if pe_assessment_evaluation

                      pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                      total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                      total_points += pe_assessment_evaluation.points*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.points > 0

                    end

                  end

                end

              end

            end

            if total_weight > 0

              pe_assessment_final_evaluation.points = total_points/total_weight.to_f
              pe_assessment_final_evaluation.percentage = pe_assessment_final_evaluation.points*100/pe_evaluation.points_to_ohp

              pe_assessment_final_evaluation.percentage = pe_evaluation.min_percentage if pe_assessment_final_evaluation.percentage < pe_evaluation.min_percentage
              pe_assessment_final_evaluation.percentage = pe_evaluation.max_percentage if pe_assessment_final_evaluation.percentage > pe_evaluation.max_percentage

              pe_evaluation.pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

                if pe_assessment_final_evaluation.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_final_evaluation.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
                  pe_assessment_final_evaluation.pe_dimension_group = pe_dimension_group
                  break
                end

              end

            end

            pe_assessment_final_evaluation.save

          end

        end

      end

    end

  end

  def calculate_final_questions_1_element(pe_member_evaluated, pe_assessment_final_evaluation, pe_evaluation, force_calculus = false)

    pe_element = pe_evaluation.pe_elements.first

    if pe_element.assessed?

      if pe_evaluation.final_evaluation_operation_evaluators == 0

        #agrupando relaciones

        pe_evaluation.pe_questions_only_by_pe_member_1st_level_ready(pe_member_evaluated).each do |pe_question|

          total_weight = 0
          total_points = 0

          pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

            tmp_weight = 0
            tmp_points = 0

            pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

              if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus)

                pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                if pe_assessment_evaluation

                  if pe_question.questions_grouped_id

                    pe_question_same = pe_evaluation.pe_questions.where('ready = true AND pe_member_evaluated_id = ? AND pe_member_evaluator_id = ? AND questions_grouped_id = ? ',pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_question.questions_grouped_id).first

                  else
                    pe_question_same = pe_question
                  end

                  if pe_question_same

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question_same

                    if pe_assessment_question

                      if pe_assessment_question.points > -1

                        tmp_weight += pe_member_rel_is_evaluated.weight

                        tmp_points += pe_assessment_question.points*pe_member_rel_is_evaluated.weight


                      end

                    end

                  end

                end

              end

            end

            if tmp_weight > 0

              tmp_points = tmp_points/tmp_weight.to_f

              total_points += tmp_points*pe_evaluation_rel.weight
              total_weight += pe_evaluation_rel.weight

            end

          end

          if total_weight > 0

            pe_assessment_final_question = pe_assessment_final_evaluation.pe_assessment_final_questions.build

            pe_assessment_final_question.pe_question = pe_question

            pe_assessment_final_question.points = total_points
            pe_assessment_final_question.points = total_points/total_weight.to_f if pe_element.calculus_method == 0 #promedio_ponderado
            pe_assessment_final_question.percentage = pe_assessment_final_question.points*100/pe_evaluation.points_to_ohp

            pe_assessment_final_question.percentage = pe_evaluation.min_percentage if pe_assessment_final_question.percentage < pe_evaluation.min_percentage
            pe_assessment_final_question.percentage = pe_evaluation.max_percentage if pe_assessment_final_question.percentage > pe_evaluation.max_percentage

            pe_assessment_final_question.save

          end

        end


      elsif pe_evaluation.final_evaluation_operation_evaluators == 1

        #sin agrupar relaciones

        pe_evaluation.pe_questions_only_by_pe_member_1st_level_ready(pe_member_evaluated).each do |pe_question|

          total_weight = 0
          total_points = 0

          pe_member_evaluated.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

            if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus)

              pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

              if pe_assessment_evaluation

                if pe_question.questions_grouped_id

                  pe_question_same = pe_evaluation.pe_questions.where('ready = true AND pe_member_evaluated_id = ? AND pe_member_evaluator_id = ? AND questions_grouped_id = ? ',pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_question.questions_grouped_id).first

                else
                  pe_question_same = pe_question
                end

                if pe_question_same

                  pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question_same

                  if pe_assessment_question

                    if pe_assessment_question.points > -1

                      pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                      total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                      total_points += pe_assessment_question.points*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight)

                    end

                  end

                end

              end

            end

          end

          if total_weight > 0

            pe_assessment_final_question = pe_assessment_final_evaluation.pe_assessment_final_questions.build

            pe_assessment_final_question.pe_question = pe_question

            pe_assessment_final_question.points = total_points
            pe_assessment_final_question.points = total_points/total_weight.to_f if pe_element.calculus_method == 0 #promedio_ponderado
            pe_assessment_final_question.percentage = pe_assessment_final_question.points*100/pe_evaluation.points_to_ohp

            pe_assessment_final_question.percentage = pe_evaluation.min_percentage if pe_assessment_final_question.percentage < pe_evaluation.min_percentage
            pe_assessment_final_question.percentage = pe_evaluation.max_percentage if pe_assessment_final_question.percentage > pe_evaluation.max_percentage

            pe_assessment_final_question.save

          end

        end

      end

    end

  end

  def calculate_assessment_groups(pe_member_evaluated)

    pe_member_evaluated.pe_assessment_groups.destroy_all

    pe_member_evaluated.pe_process.pe_evaluation_groups.each do |pe_evaluation_group|

      pe_assessment_group = pe_member_evaluated.pe_assessment_groups.build

      pe_assessment_group.pe_process = pe_member_evaluated.pe_process
      pe_assessment_group.pe_evaluation_group = pe_evaluation_group

      pe_assessment_group.percentage = -1

      sum_weights = 0
      sum_percentage = 0

      pe_evaluation_group.pe_evaluations.where('informative = ?', false).each do |pe_evaluation|

        if pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_evaluation)

          pe_member_evaluation = pe_member_evaluated.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first
          weight = pe_member_evaluation ? pe_member_evaluation.weight : pe_evaluation.weight

          pe_assessment_final_evaluation = pe_member_evaluated.pe_assessment_final_evaluation pe_evaluation

          if pe_assessment_final_evaluation && pe_assessment_final_evaluation.percentage >= 0

            sum_percentage += pe_assessment_final_evaluation.percentage*weight
            sum_weights += weight

          end

        end

      end

      if sum_weights > 0

        pe_assessment_group.percentage = sum_percentage/sum_weights.to_f

      end

      pe_assessment_group.save

    end

  end

  def calculate_final_questions_2_elements(pe_member_evaluated,pe_assessment_final_evaluation, pe_evaluation, force_calculus = false)

    pe_element = pe_evaluation.pe_elements.first
    pe_element_2 = pe_evaluation.pe_elements.second

    if pe_element_2.assessed?

      if pe_evaluation.final_evaluation_operation_evaluators == 0

        #agrupando relaciones

        pe_evaluation.pe_questions_only_by_pe_member_1st_level_ready(pe_member_evaluated).each do |pe_question|

          total_weight = 0
          total_points = 0

          pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

            tmp_weight = 0
            tmp_points = 0

            pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

              if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus)

                pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                if pe_assessment_evaluation

                  if pe_question.questions_grouped_id

                    pe_question_same = pe_evaluation.pe_questions.where('ready = true AND pe_member_evaluated_id = ? AND pe_member_evaluator_id = ? AND questions_grouped_id = ? ',pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_question.questions_grouped_id).first

                  else
                    pe_question_same = pe_question
                  end

                  if pe_question_same

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question_same

                    if pe_assessment_question

                      if pe_assessment_question.points > -1

                        tmp_weight += pe_member_rel_is_evaluated.weight

                        tmp_points += pe_assessment_question.points*pe_member_rel_is_evaluated.weight


                      end

                    end

                  end

                end

              end

            end

            if tmp_weight > 0

              tmp_points = tmp_points/tmp_weight.to_f

              total_points += tmp_points*pe_evaluation_rel.weight
              total_weight += pe_evaluation_rel.weight

            end

          end

          if total_weight > 0

            pe_assessment_final_question = pe_assessment_final_evaluation.pe_assessment_final_questions.build

            pe_assessment_final_question.pe_question = pe_question

            pe_assessment_final_question.points = total_points
            pe_assessment_final_question.points = total_points/total_weight.to_f if pe_element.calculus_method == 0 #promedio_ponderado
            pe_assessment_final_question.percentage = pe_assessment_final_question.points*100/pe_evaluation.points_to_ohp

            pe_assessment_final_question.percentage = pe_evaluation.min_percentage if pe_assessment_final_question.percentage < pe_evaluation.min_percentage
            pe_assessment_final_question.percentage = pe_evaluation.max_percentage if pe_assessment_final_question.percentage > pe_evaluation.max_percentage

            pe_assessment_final_question.save

          end

          pe_evaluation.pe_questions_by_question_ready(pe_question.id).each do |pe_question_2nd|

            total_weight = 0
            total_points = 0

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

              tmp_weight = 0
              tmp_points = 0

              pe_member_evaluated.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus)

                  pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                  if pe_assessment_evaluation

                    if pe_question_2nd.questions_grouped_id

                      pe_question_same = pe_evaluation.pe_questions.where('ready = true AND pe_member_evaluated_id = ? AND pe_member_evaluator_id = ? AND questions_grouped_id = ? ',pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_question_2nd.questions_grouped_id).first

                    else
                      pe_question_same = pe_question_2nd
                    end

                    if pe_question_same

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question_same

                      if pe_assessment_question

                        if pe_assessment_question.points > -1

                          tmp_weight += pe_member_rel_is_evaluated.weight

                          tmp_points += pe_assessment_question.points*pe_member_rel_is_evaluated.weight

                        end

                      end

                    end

                  end

                end

              end

              if tmp_weight > 0

                tmp_points = tmp_points/tmp_weight.to_f

                total_points += tmp_points*pe_evaluation_rel.weight
                total_weight += pe_evaluation_rel.weight

              end

            end

            if total_weight > 0

              pe_assessment_final_question_2 = pe_assessment_final_evaluation.pe_assessment_final_questions.build

              pe_assessment_final_question_2.pe_question = pe_question_2nd

              pe_assessment_final_question_2.pe_assessment_final_question = pe_assessment_final_question

              pe_assessment_final_question_2.points = total_points
              pe_assessment_final_question_2.points = total_points/total_weight.to_f if pe_element_2.calculus_method == 0 #promedio_ponderado
              pe_assessment_final_question_2.percentage = pe_assessment_final_question.points*100/pe_evaluation.points_to_ohp

              pe_assessment_final_question_2.percentage = pe_evaluation.min_percentage if pe_assessment_final_question.percentage < pe_evaluation.min_percentage
              pe_assessment_final_question_2.percentage = pe_evaluation.max_percentage if pe_assessment_final_question.percentage > pe_evaluation.max_percentage

              pe_assessment_final_question_2.save

            end

          end

        end


      elsif pe_evaluation.final_evaluation_operation_evaluators == 1

        #sin agrupar relaciones

        pe_evaluation.pe_questions_only_by_pe_member_1st_level_ready(pe_member_evaluated).each do |pe_question|

          total_weight = 0
          total_points = 0

          pe_member_evaluated.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

            if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus)

              pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

              if pe_assessment_evaluation

                if pe_question.questions_grouped_id

                  pe_question_same = pe_evaluation.pe_questions.where('ready = true AND pe_member_evaluated_id = ? AND pe_member_evaluator_id = ? AND questions_grouped_id = ? ',pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_question.questions_grouped_id).first

                else
                  pe_question_same = pe_question
                end

                if pe_question_same

                  pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question_same

                  if pe_assessment_question && pe_assessment_question.points && pe_assessment_question.points > -1

                    pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                    total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                    total_points += pe_assessment_question.points*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight)

                  end

                end

              end

            end

          end

          if total_weight > 0

            pe_assessment_final_question = pe_assessment_final_evaluation.pe_assessment_final_questions.build

            pe_assessment_final_question.pe_question = pe_question

            pe_assessment_final_question.points = total_points
            pe_assessment_final_question.points = total_points/total_weight.to_f if pe_element.calculus_method == 0 #promedio_ponderado
            pe_assessment_final_question.percentage = pe_assessment_final_question.points*100/pe_evaluation.points_to_ohp

            pe_assessment_final_question.percentage = pe_evaluation.min_percentage if pe_assessment_final_question.percentage < pe_evaluation.min_percentage
            pe_assessment_final_question.percentage = pe_evaluation.max_percentage if pe_assessment_final_question.percentage > pe_evaluation.max_percentage

            pe_assessment_final_question.save

          end

          pe_evaluation.pe_questions_by_question_ready(pe_question).each do |pe_question_2nd|

            total_weight = 0
            total_points = 0

            pe_member_evaluated.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

              if pe_member_rel_is_evaluated.valid_evaluator && (pe_member_rel_is_evaluated.finished || force_calculus)

                pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                if pe_assessment_evaluation

                  if pe_question_2nd.questions_grouped_id

                    pe_question_same = pe_evaluation.pe_questions.where('ready = true AND pe_member_evaluated_id = ? AND pe_member_evaluator_id = ? AND questions_grouped_id = ? ',pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_question_2nd.questions_grouped_id).first

                  else
                    pe_question_same = pe_question_2nd
                  end

                  if pe_question_same

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question pe_question_same

                    if pe_assessment_question && pe_assessment_question.points && pe_assessment_question.points > -1

                        pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                        total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                        total_points += pe_assessment_question.points*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight)

                    end

                  end

                end

              end

            end

            if total_weight > 0

              pe_assessment_final_question_2 = pe_assessment_final_evaluation.pe_assessment_final_questions.build

              pe_assessment_final_question_2.pe_question = pe_question_2nd

              pe_assessment_final_question_2.pe_assessment_final_question = pe_assessment_final_question

              pe_assessment_final_question_2.points = total_points
              pe_assessment_final_question_2.points = total_points/total_weight.to_f if pe_element_2.calculus_method == 0 #promedio_ponderado
              pe_assessment_final_question_2.percentage = pe_assessment_final_question_2.points*100/pe_evaluation.points_to_ohp

              pe_assessment_final_question_2.percentage = pe_evaluation.min_percentage if pe_assessment_final_question_2.percentage < pe_evaluation.min_percentage
              pe_assessment_final_question_2.percentage = pe_evaluation.max_percentage if pe_assessment_final_question_2.percentage > pe_evaluation.max_percentage

              pe_assessment_final_question_2.save

            end

          end

        end

      end

    end

  end

  def calculate_final_evaluation_1_element(pe_member_evaluated, pe_assessment_final_evaluation, pe_evaluation, force_calculus = false)
    #solo para operacion de evaluaciones por pregunta
    pe_element = pe_evaluation.pe_elements.first

    if pe_element.assessed?

      total_weight = 0
      total_points = 0

      repited_pe_questions = Array.new

      pe_evaluation.pe_questions_only_by_pe_member_1st_level_ready(pe_member_evaluated).each do |pe_question|

        unless repited_pe_questions.include? pe_question.questions_grouped_id

          pe_assessment_final_question = pe_assessment_final_evaluation.pe_assessment_final_questions.where('pe_question_id = ?', pe_question.id).first

          if pe_assessment_final_question && pe_assessment_final_question.percentage > -1

            total_weight += pe_question.weight
            total_points += pe_question.weight*pe_assessment_final_question.percentage

          end

          repited_pe_questions.push pe_question.questions_grouped_id if pe_question.questions_grouped_id

        end

      end

      if total_weight > 0

        pe_assessment_final_evaluation.points = total_points
        pe_assessment_final_evaluation.points = total_points/total_weight.to_f if pe_element.calculus_method == 0 #promedio_ponderado
        pe_assessment_final_evaluation.percentage = pe_assessment_final_evaluation.points*100/pe_evaluation.points_to_ohp

        pe_assessment_final_evaluation.percentage = pe_evaluation.min_percentage if pe_assessment_final_evaluation.percentage < pe_evaluation.min_percentage
        pe_assessment_final_evaluation.percentage = pe_evaluation.max_percentage if pe_assessment_final_evaluation.percentage > pe_evaluation.max_percentage

        pe_evaluation.pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

          if pe_assessment_final_evaluation.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_final_evaluation.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
            pe_assessment_final_evaluation.pe_dimension_group = pe_dimension_group
            break
          end

        end

      end

      return pe_assessment_final_evaluation

    end

  end

  def calculate_final_evaluation_2_elements(pe_member_evaluated, pe_assessment_final_evaluation, pe_evaluation, force_calculus = false)
    #solo para operacion de evaluaciones por pregunta
    pe_element = pe_evaluation.pe_elements.first
    pe_element_2 = pe_evaluation.pe_elements.last

    if pe_element_2.assessed?

      total_weight = 0
      total_points = 0

      repited_pe_questions = Array.new

      pe_evaluation.pe_questions_only_by_pe_member_1st_level_ready(pe_member_evaluated).each do |pe_question|

          tmp_weight = 0
          tmp_points = 0

          pe_assessment_final_question = pe_assessment_final_evaluation.pe_assessment_final_questions.where('pe_question_id = ?', pe_question.id).first

          pe_evaluation.pe_questions_by_question_ready(pe_question).each do |pe_question_2nd|

            unless repited_pe_questions.include? pe_question_2nd.questions_grouped_id

              pe_assessment_final_question_2 = pe_assessment_final_evaluation.pe_assessment_final_questions.where('pe_question_id = ?', pe_question_2nd.id).first

              if pe_assessment_final_question_2 && pe_assessment_final_question_2.percentage > -1

                tmp_weight += pe_question_2nd.weight
                tmp_points += pe_question_2nd.weight*pe_assessment_final_question_2.percentage

                unless pe_element.assessed?

                  total_weight += pe_question_2nd.weight
                  total_points += pe_question_2nd.weight*pe_assessment_final_question_2.percentage

                end

              end

              repited_pe_questions.push pe_question_2nd.questions_grouped_id if pe_question_2nd.questions_grouped_id

            end

          end

          if tmp_weight > 0

            pe_assessment_final_question.points = tmp_points/tmp_weight.to_f
            pe_assessment_final_question.percentage = pe_assessment_final_question.points*100/pe_evaluation.points_to_ohp
            pe_assessment_final_question.save

            if pe_element.assessed?

              unless repited_pe_questions.include? pe_question.questions_grouped_id

                total_weight += pe_question.weight
                total_points += pe_question.weight*pe_assessment_final_question.percentage

              end

            end

          end

          repited_pe_questions.push pe_question.questions_grouped_id if pe_question.questions_grouped_id



      end

      if total_weight > 0

        pe_assessment_final_evaluation.points = total_points
        pe_assessment_final_evaluation.points = total_points/total_weight.to_f if pe_element.calculus_method == 0 #promedio_ponderado
        pe_assessment_final_evaluation.percentage = pe_assessment_final_evaluation.points*100/pe_evaluation.points_to_ohp

        pe_assessment_final_evaluation.percentage = pe_evaluation.min_percentage if pe_assessment_final_evaluation.percentage < pe_evaluation.min_percentage
        pe_assessment_final_evaluation.percentage = pe_evaluation.max_percentage if pe_assessment_final_evaluation.percentage > pe_evaluation.max_percentage

        pe_evaluation.pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

          if pe_assessment_final_evaluation.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_final_evaluation.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
            pe_assessment_final_evaluation.pe_dimension_group = pe_dimension_group
            break
          end

        end

      end

      return pe_assessment_final_evaluation

    end

  end

  def calculate_assessment_dimension(pe_member_evaluated, pe_dimension)

    pe_member_evaluated.pe_assessment_dimensions.where('pe_dimension_id = ?', pe_dimension.id).destroy_all

    pe_assessment_dimension = pe_member_evaluated.pe_assessment_dimensions.build

    pe_assessment_dimension.pe_process = pe_member_evaluated.pe_process
    pe_assessment_dimension.pe_dimension = pe_dimension
    pe_assessment_dimension.last_update = lms_time

    pe_assessment_dimension.percentage = -1

    sum_weights = 0
    sum_percentage = 0

    pe_dimension.pe_evaluations.where('informative = ?', false).each do |pe_evaluation|

      if pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_evaluation)

        pe_member_evaluation = pe_member_evaluated.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first
        weight = pe_member_evaluation ? pe_member_evaluation.weight : pe_evaluation.weight

        pe_assessment_final_evaluation = pe_member_evaluated.pe_assessment_final_evaluation pe_evaluation

        if pe_assessment_final_evaluation && pe_assessment_final_evaluation.percentage >= 0
          sum_percentage += pe_assessment_final_evaluation.percentage*weight
          sum_weights += weight
        end

      end

    end

    if sum_weights > 0

      pe_assessment_dimension.percentage = sum_percentage/sum_weights.to_f

      pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

        if pe_assessment_dimension.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_dimension.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
          pe_assessment_dimension.pe_dimension_group = pe_dimension_group
          break
        end

      end

    end

    pe_assessment_dimension.save

    unless @pe_process.of_persons?

      if pe_member_evaluated.pe_area

        pe_area_evaluated = pe_member_evaluated.pe_area

        pe_area_evaluated.pe_assessment_dimensions.where('pe_dimension_id = ?', pe_dimension.id).destroy_all

        pe_assessment_dimension = pe_area_evaluated.pe_assessment_dimensions.build

        pe_assessment_dimension.pe_process = pe_member_evaluated.pe_process
        pe_assessment_dimension.pe_dimension = pe_dimension
        pe_assessment_dimension.last_update = lms_time

        pe_assessment_dimension.percentage = -1

        sum_weights = 0
        sum_percentage = 0

        pe_assessment_dimension.pe_dimension.pe_evaluations.where('informative = ?', false).each do |pe_evaluation|

          sum_weights += pe_evaluation.weight

          pe_assessment_final_evaluation = pe_area_evaluated.pe_assessment_final_evaluation pe_evaluation

          sum_percentage += pe_assessment_final_evaluation.percentage*pe_evaluation.weight if pe_assessment_final_evaluation && pe_assessment_final_evaluation.percentage >= 0

        end

        if sum_weights > 0

          pe_assessment_dimension.percentage = sum_percentage/sum_weights.to_f

          pe_dimension.pe_dimension_groups.reorder('position DESC').each do |pe_dimension_group|

            if pe_assessment_dimension.percentage.method(pe_dimension_group.min_sign).(pe_dimension_group.min_percentage) && pe_assessment_dimension.percentage.method(pe_dimension_group.max_sign).(pe_dimension_group.max_percentage)
              pe_assessment_dimension.pe_dimension_group = pe_dimension_group
              break
            end

          end

        end

        pe_assessment_dimension.save

      end

    end

  end

  def finish_the_whole_assessment(pe_member_evaluated, forced = false)

    if forced

      pe_member_evaluated.step_assessment = true
      pe_member_evaluated.step_assessment_date = lms_time
      pe_member_evaluated.set_pe_box
      pe_member_evaluated.save

      unless pe_member_evaluated.pe_process.of_persons?

        pe_area = pe_member_evaluated.pe_area

        if pe_area
          pe_area.step_assessment = true
          pe_area.step_assessment_date = lms_time
          pe_area.set_pe_box
          pe_area.save
        end

      end

      if pe_member_evaluated.require_validation && !pe_member_evaluated.step_validation

        step_validation = true

        pe_member_evaluated.pe_member_rels_is_evaluated.where('validator_id IS NOT NULL').each do |pe_member_rel_is_evaluated|

          step_validation = false unless pe_member_rel_is_evaluated.validated

        end

        if step_validation

          pe_member_evaluated.step_validation = true
          pe_member_evaluated.step_validation_date = lms_time
          pe_member_evaluated.save

        end

      end

    else

      finished = true

      pe_member_evaluated.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

        unless pe_member_rel_is_evaluated.finished

          pe_rel_has_to_asses = false

          pe_member_evaluated.pe_process.pe_evaluations.each do |pe_evaluation|
            pe_rel_has_to_asses = true if pe_evaluation.has_pe_evaluation_rel pe_member_rel_is_evaluated.pe_rel
          end

          if pe_rel_has_to_asses
            finished = false
          else
            finished = true
            pe_member_rel_is_evaluated.finished = true
            pe_member_rel_is_evaluated.save
          end

        end

      end

      if finished
        pe_member_evaluated.step_assessment = true
        pe_member_evaluated.step_assessment_date = lms_time
        pe_member_evaluated.set_pe_box
        pe_member_evaluated.save

        unless pe_member_evaluated.pe_process.of_persons?

          pe_area = pe_member_evaluated.pe_area

          if pe_area

            unless pe_area.step_assessment

              pe_area_to_be_finished = true

              pe_area.pe_members.each do |pe_member_evaluated_aux|

                if pe_member_evaluated_aux.is_evaluated

                  pe_member_evaluated_aux.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator

                      pe_area_to_be_finished = false unless pe_member_rel_is_evaluated.finished

                    end

                  end

                end

              end

              if pe_area_to_be_finished

                pe_area.step_assessment = true
                pe_area.step_assessment_date = lms_time
                pe_area.set_pe_box
                pe_area.save

              end

            end

          end

        end

        if pe_member_evaluated.require_validation && !pe_member_evaluated.step_validation

          step_validation = true

          pe_member_evaluated.pe_member_rels_is_evaluated.where('validator_id IS NOT NULL').each do |pe_member_rel_is_evaluated|

            step_validation = false unless pe_member_rel_is_evaluated.validated

          end

          if step_validation

            pe_member_evaluated.step_validation = true
            pe_member_evaluated.step_validation_date = lms_time
            pe_member_evaluated.save

          end

        end

      end

    end



  end

  def open_the_whole_assessment(pe_member_evaluated)


    pe_member_evaluated.step_assessment = false
    pe_member_evaluated.step_validation = false
    pe_member_evaluated.step_validation_date = nil
    pe_member_evaluated.step_calibration = false
    pe_member_evaluated.step_calibration_date = nil
    pe_member_evaluated.pe_box = nil
    pe_member_evaluated.pe_box_before_calibration = nil
    pe_member_evaluated.save


    unless pe_member_evaluated.pe_process.of_persons?

      pe_area = pe_member_evaluated.pe_area

      if pe_area

        pe_area.step_assessment = false
        pe_area.save

      end

    end


  end

  def calculate_alternative_result(points, points_to_ohp, min_percentage, max_percentage)

    percentage = points*100/points_to_ohp

    percentage = min_percentage if min_percentage && percentage < min_percentage
    percentage = max_percentage if max_percentage && percentage > max_percentage

    return percentage

  end

  def calculate_indicator_achievement(indicator_achievement, goal, indicator_type, pe_question_ranks, min_percentage, max_percentage, power = 1, leverage = 0)

    percentage = 0

    if indicator_achievement

      case indicator_type
        when 0
          percentage = indicator_achievement*100/goal if goal && goal > 0
        when 1
          percentage = goal*100/indicator_achievement if goal && indicator_achievement && indicator_achievement > 0
        when 2
          if goal
            if indicator_achievement >= goal*-1 && indicator_achievement <= goal
              percentage = 100
            else
              percentage = 100-(indicator_achievement-goal).abs*100/goal
            end
          end
        when 3
          if pe_question_ranks.size > 0
            metas = Array.new
            porcentajes = Array.new

            pe_question_ranks.each do |pe_question_rank|

              metas.push pe_question_rank.goal
              porcentajes.push pe_question_rank.percentage

            end

            if metas.first <= metas.last
              #directo

              if indicator_achievement < metas.first
                percentage = 0
              elsif indicator_achievement >= metas.last
                percentage = porcentajes.last
              else

                meta_inf = meta_dif = 0
                por_inf = por_dif = 0

                (0..metas.length-1).each do |num_meta|
                  if metas[num_meta] && metas[num_meta+1] && metas[num_meta] <= indicator_achievement && indicator_achievement <= metas[num_meta+1]
                    meta_dif = metas[num_meta+1] - metas[num_meta]
                    meta_inf = metas[num_meta]
                    por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                    por_inf = porcentajes[num_meta]
                    break
                  end
                end

                if meta_dif == 0
                  percentage = por_inf
                elsif meta_dif > 0
                  percentage = ((indicator_achievement-meta_inf)*por_dif/meta_dif)+por_inf
                else
                  percentage = 0
                end

              end

            else
              #inverso
              if indicator_achievement > metas.first
                percentage = 0
              elsif indicator_achievement <= metas.last
                percentage = porcentajes.last
              else

                meta_sup = meta_dif = 0
                por_inf = por_dif = 0

                (0..metas.length-1).each do |num_meta|
                  if metas[num_meta] && metas[num_meta+1] && metas[num_meta] >= indicator_achievement && indicator_achievement >= metas[num_meta+1]
                    meta_dif = metas[num_meta] - metas[num_meta+1]
                    meta_sup = metas[num_meta]
                    por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                    por_inf = porcentajes[num_meta]
                    break
                  end
                end

                if meta_dif == 0
                  percentage = por_inf
                elsif meta_dif > 0
                  percentage = ((meta_sup-indicator_achievement)*por_dif/meta_dif)+por_inf
                else
                  percentage = 0
                end

              end

            end

          end

        when 4

          if pe_question_ranks.size > 0

            pe_question_ranks.each do |pe_question_rank|

              if indicator_achievement == pe_question_rank.discrete_goal
                percentage = pe_question_rank.percentage
                break
              end
            end

          end

        when 5

          if goal > 0 && power && leverage
            percentage = (((((indicator_achievement/goal)**power)-1)*leverage)+1)*100
          end

      end

      percentage = min_percentage if min_percentage && percentage < min_percentage
      percentage = max_percentage if max_percentage && percentage > max_percentage

    end

    return percentage

  end

end
