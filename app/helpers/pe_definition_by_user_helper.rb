module PeDefinitionByUserHelper

  def link_to_add_rank_non_discrete()

    form = render partial: 'pe_definition_by_user/pe_question_rank_non_discrete',
                  locals: {
                      pe_question_rank: nil,
                      goal: '',
                      percentage: '',
                      read_only_goal: false,
                      read_only_percentage: false,
                      pe_question_rank_model: nil
                  }

    link_to_function('<i class="fa fa-plus" aria-hidden="true"></i>'.html_safe, "add_rank_non_discrete(\"#{escape_javascript(form)}\")")

  end


  def link_to_add_rank_discrete()

    form = render partial: 'pe_definition_by_user/pe_question_rank_discrete',
                  locals: {
                      pe_question_rank: nil,
                      goal: '',
                      percentage: '',
                      read_only_goal: false,
                      read_only_percentage: false,
                      pe_question_rank_model: nil
                  }

    link_to_function('<i class="fa fa-plus" aria-hidden="true"></i>'.html_safe, "add_rank_discrete(\"#{escape_javascript(form)}\")")

  end

end
