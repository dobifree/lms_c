module LicRegistersHelper
  include LicensesModule

  def period_of(license, request)
    return ''  unless license
    return ''  unless request

    ans = license.get_request_period(request.begin, request.end, request.user_id, anniversary(request.user_id))

    if ans && ans.size > 1
      period = ans[0].to_date.day.to_s + ' ' + t('date.month_names')[ans[0].to_date.month].to_s
      period += ' - '
      period += ans[1].to_date.day.to_s + ' ' + t('date.month_names')[ans[1].to_date.month].to_s
    else
      period = ans[0]
    end
    period
  end
  def access_available?(request_id)
    request = LicRequest.find(request_id)

    ct_module = CtModule.where('cod = ? AND active = ?', 'licenses', true).first

    if ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user_connected.id).count == 1
      return true
    else
      approver = LicApprover.where(active: true, user_id: user_connected.id).first
      if approver
        approver.lic_user_to_approves.where(active: true, user_id: request.lic_event.user_id).all
      end
    end
  end

  def complete_request_update(params, lic_request_id)
    request = LicRequest.find(lic_request_id)
    request.assign_attributes(params)
    request.registered_by_user_id = user_connected.id
    request.registered_at = lms_time
    request.prev_lic_request_id = lic_request_id
    request = stablishing_attr(request.lic_event, request)
    request = request.dup
    return request
  end

  def complete_request_create(event_id, params)
    request = LicRequest.new(params)
    event = LicEvent.find(event_id)
    request.lic_event_id = event_id
    request.user_id = user_connected.id
    request.registered_at = lms_time
    request.registered_by_user_id = user_connected.id
    # request = stablishing_attr(event, request)
    request = set_attr(event, request, request.begin, request.end, request.days, request.hours)
    request
  end

  def complete_event_update(params, lic_event_id)
    prev_event = LicEvent.find(lic_event_id)

    prev_event.assign_attributes(params)
    updated_event_json = prev_event.to_json(:except => [:id, :created_at, :updated_at, :prev_lic_event_id],
                                            :include => {:lic_item_values => {:except => [:id, :created_at, :updated_at, :lic_event_id, :lic_request_id]}})

    updated_event_json = updated_event_json.gsub('"lic_item_values"', '"lic_item_values_attributes"')
    updated_event_json = ActiveSupport::JSON.decode updated_event_json

    updated_event = LicEvent.new(updated_event_json)
    updated_event.user_id = user_connected.id
    updated_event.registered_at = lms_time
    updated_event.registered_by_user_id = user_connected.id
    updated_event.prev_lic_event_id = lic_event_id
    return updated_event
  end

  def complete_deactivate_request(request_id)
    request = LicRequest.find(request_id)
    request.active = false
    request.deactivated_at = lms_time
    request.deactivated_by_user_id = user_connected.id
    return request
  end

  def copy_values(prev_request_id, request_id)
    prev_request = LicRequest.find(prev_request_id)
    request = LicRequest.find(request_id)

    prev_request.lic_item_values.each do |prev_value|
      copy_value = false
      request.lic_item_values.each do |value|
        next if value.bp_item_id == prev_value.bp_item_id
        copy_value = true
      end
      next unless copy_value
      new_value = prev_value.dup
      new_value.lic_request_id = request_id
      new_value.save
    end
  end

  def set_attr(event, request, request_begin, request_end, days, hours)
    license = request.bp_license
    request.days = license.partially_taken ? days : license.days
    request.hours = license.partially_taken ? hours : license.hours

    request.days = license.min_by_request ? license.min_by_request : 0 if license.days && !request.days
    request.hours = license.min_by_request ? license.min_by_request : 0 if license.hours && !request.hours

    if license.days || license.hours
      if license.with_event
        if license.whenever
          request.begin = request_begin.to_date if request_begin
        else
          request.begin = event.date_event
        end
      else
        request.begin = request_begin.to_date if request_begin
      end

      request.begin = event.date_event unless request.begin

      if request.begin
      request.end = vacations_end(license.only_laborable_days, request.begin, request.days, event.user_id ?  event.user_id : request.user_id) if license.days
      request.end = request.begin.to_datetime + request.hours.hours if license.hours
      end
    else
      request.begin = event.date_event
    end

    if license.money
      money = event.sum_money_for(request.bp_license_id)
      request.money = license.money - money unless money == license.money
    end

    request.go_back_date = next_laborable_day?(request.end, event.user_id) if license.days
    request.go_back_date = request.end + 1.minute if license.hours

    request
  end

  def stablishing_attr(event, request)
    license = request.bp_license
    if license.partially_taken
      request.end = vacations_end(license.only_laborable_days, request.begin ? request.begin : event.date_event, request.days, event.user_id) if license.days
      request.end = request.begin.to_datetime + request.hours.hours if license.hours
      #verificar en modelo que el día del begin es el mismo al del evento.
    else
      request.begin = event.date_event if !license.whenever && (license.days || license.hours)
      request.end = vacations_end(license.only_laborable_days, request.begin ? request.begin : event.date_event, license.days, event.user_id) if license.days
      request.end = request.begin.to_datetime + license.hours.hours if license.hours
      request.days = license.days if license.days
      request.hours = license.hours if license.hours
      #verificar en modelo que el día del begin es el mismo al del evento.
    end

    if request.bp_license.money
      money = event.sum_money_for(request.bp_license_id)
      unless money == request.bp_license.money
        request.money = request.bp_license.money - money
      end
    end
    request.go_back_date = next_laborable_day?(request.end, event.user_id) if license.days
    request.go_back_date = request.end + 1.minute if license.hours

    request.begin = event.date_event unless request.begin

    return request
  end

end
