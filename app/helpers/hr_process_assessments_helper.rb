module HrProcessAssessmentsHelper

  def can_show_evaluate_hr_process_menu?

    ver_menu = false

    #hr_processes = HrProcess.where('abierto = ? AND fecha_inicio <= ? AND fecha_fin >= ?',true, lms_time, lms_time)

    hr_processes = HrProcess.where('abierto = ?',true)

    hr_processes.each do |hr_process|

      hr_process_user = hr_process.hr_process_users.find_by_user_id user_connected.id

      if hr_process_user

        hr_process_user_rels = hr_process_user.hr_process_evalua_rels

        eval_jefe = false
        eval_sub = false
        eval_par = false
        eval_cli = false
        eval_prov = false

        hr_process_user_rels.each do |rel|

          eval_jefe = true if rel.tipo == 'jefe'

          eval_sub = true if rel.tipo == 'sub'

          eval_par = true if rel.tipo == 'par'

          eval_cli = true if rel.tipo == 'cli'

          eval_prov = true if rel.tipo == 'prov'


        end

        hr_process_evaluations = hr_process.hr_process_evaluations.find_all_by_vigente(true)

        hr_process_evaluations.each_with_index do |hr_process_evaluation, index|

          if hr_process.hr_process_template && hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_template.id == hr_process.hr_process_template.id

            hay_por_evaluar = false

            if hr_process_evaluation.hr_process_dimension_e.eval_jefe && eval_jefe

              #if hr_process_evaluation.fecha_inicio_eval_jefe && hr_process_evaluation.fecha_fin_eval_jefe && hr_process_evaluation.fecha_inicio_eval_jefe <= lms_time && hr_process_evaluation.fecha_fin_eval_jefe >= lms_time
                 hay_por_evaluar = true
                 ver_menu = true
                 break
              #end

            end

            if hr_process_evaluation.hr_process_dimension_e.eval_sub && eval_sub

              #if hr_process_evaluation.fecha_inicio_eval_sub && hr_process_evaluation.fecha_fin_eval_sub && hr_process_evaluation.fecha_inicio_eval_sub <= lms_time && hr_process_evaluation.fecha_fin_eval_sub >= lms_time
                 hay_por_evaluar = true
                 ver_menu = true
                 break

              #end

            end

            if hr_process_evaluation.hr_process_dimension_e.eval_par && eval_par

              #if hr_process_evaluation.fecha_inicio_eval_par && hr_process_evaluation.fecha_fin_eval_par && hr_process_evaluation.fecha_inicio_eval_par <= lms_time && hr_process_evaluation.fecha_fin_eval_par >= lms_time
                 hay_por_evaluar = true
                 ver_menu = true
                 break
              #end

            end

            if hr_process_evaluation.hr_process_dimension_e.eval_cli && eval_cli

              #if hr_process_evaluation.fecha_inicio_eval_par && hr_process_evaluation.fecha_fin_eval_par && hr_process_evaluation.fecha_inicio_eval_par <= lms_time && hr_process_evaluation.fecha_fin_eval_par >= lms_time
              hay_por_evaluar = true
              ver_menu = true
              break
              #end

            end

            if hr_process_evaluation.hr_process_dimension_e.eval_prov && eval_prov

              #if hr_process_evaluation.fecha_inicio_eval_par && hr_process_evaluation.fecha_fin_eval_par && hr_process_evaluation.fecha_inicio_eval_par <= lms_time && hr_process_evaluation.fecha_fin_eval_par >= lms_time
              hay_por_evaluar = true
              ver_menu = true
              break
              #end

            end

          end

        end

        break if ver_menu

      end

    end

    return ver_menu

  end

  def can_show_autoevaluate_hr_process_menu?

    ver_menu = false

    #hr_processes = HrProcess.where('abierto = ? AND fecha_inicio <= ? AND fecha_fin >= ?',true, lms_time, lms_time)

    hr_processes = HrProcess.where('abierto = ?',true)

    hr_processes.each do |hr_process|

      hr_process_user = hr_process.hr_process_users.find_by_user_id user_connected.id

      if hr_process_user

        hr_process_evaluations = hr_process.hr_process_evaluations.find_all_by_vigente(true)

        hr_process_evaluations.each_with_index do |hr_process_evaluation, index|

          if hr_process.hr_process_template && hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_template.id == hr_process.hr_process_template.id

            hay_por_evaluar = false

            if hr_process_evaluation.hr_process_dimension_e.eval_auto

              #if hr_process_evaluation.fecha_inicio_eval_auto && hr_process_evaluation.fecha_fin_eval_auto && hr_process_evaluation.fecha_inicio_eval_auto <= lms_time && hr_process_evaluation.fecha_fin_eval_auto >= lms_time
              hay_por_evaluar = true
              ver_menu = true
              break
              #end

            end

          end

        end

        break if ver_menu

      end

    end

    return ver_menu

  end

  def can_show_calibraate_hr_process_menu?

    ver_menu = false

    hr_processes = HrProcess.where('abierto = ?',true)

    hr_processes.each do |hr_process|

      hr_process.hr_process_calibration_sessions.each do |calibration_session|

        hay = calibration_session.hr_process_calibration_session_cs.where('user_id = ?', user_connected.id).size

        if hay > 0 && (hr_process.calib_cuadrante || hr_process.calib_dimension)
          ver_menu = true
          break
        end

      end

      break if ver_menu

    end

    return ver_menu

  end

  def accesso_evaluar_proceso(hr_process)

    hr_process_user_connected = hr_process.hr_process_users.find_by_user_id(user_connected.id)

    if hr_process_user_connected && hr_process_user_connected.hr_process_evalua_rels.where('tipo <> ?', 'resp').size > 0
      return true
    else
      return false
    end

  end

  def accesso_evaluar_proceso_como(hr_process, rel)

    hr_process_user_connected = hr_process.hr_process_users.find_by_user_id(user_connected.id)

    if hr_process_user_connected && hr_process_user_connected.hr_process_evalua_rels.where('tipo = ?', rel).size > 0
      return true
    else
      return false
    end

  end

  def accesso_confirmar_feedback(hr_process)

    hr_process_user_connected = hr_process.hr_process_users.find_by_user_id(user_connected.id)

    if hr_process_user_connected && hr_process_user_connected.hr_process_es_evaluado_rels.where('tipo = ?', 'jefe').size > 0 && hr_process.step_feedback_confirmation && hr_process_user_connected.feedback_confirmado

      return true
    else

      return false
    end

  end

  def accesso_confirmar_feedback_2

    HrProcess.where('abierto = ?',true).each do |hr_process|

      hr_process_user_connected = hr_process.hr_process_users.find_by_user_id(user_connected.id)

      if hr_process_user_connected && hr_process_user_connected.hr_process_es_evaluado_rels.where('tipo = ?', 'jefe').size > 0 && hr_process.step_feedback_confirmation && hr_process_user_connected.feedback_confirmado

        return true
      end

    end

    return false

  end

  def accesso_autoevaluar_proceso(hr_process)

    hay_por_evaluar = false

    hr_process_user_connected = hr_process.hr_process_users.find_by_user_id(user_connected.id)

    if hr_process_user_connected
      hr_process_evaluations = hr_process.hr_process_evaluations.find_all_by_vigente(true)

      hr_process_evaluations.each do |hr_process_evaluation|

        if hr_process.hr_process_template &&
            hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_template.id == hr_process.hr_process_template.id &&
            hr_process_evaluation.hr_process_dimension_e.eval_auto
          hay_por_evaluar = true
          break
        end

      end

      if hay_por_evaluar
        if hr_process_user_connected.hr_process_es_evaluado_rels.where('tipo <> ?', 'resp').size == 0
          hay_por_evaluar = false
        end
      end

    end

    return hay_por_evaluar

  end

  def debe_realizar_autoevaluacion_proceso(hr_process, hr_process_user)

    hay_por_evaluar = false

    hr_process_user

    if hr_process_user
      hr_process_evaluations = hr_process.hr_process_evaluations.find_all_by_vigente(true)

      hr_process_evaluations.each do |hr_process_evaluation|

        if hr_process.hr_process_template &&
          hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_template.id == hr_process.hr_process_template.id &&
          hr_process_evaluation.hr_process_dimension_e.eval_auto
          hay_por_evaluar = true
          break
        end

      end

      if hay_por_evaluar
        if hr_process_user.hr_process_es_evaluado_rels.where('tipo <> ?', 'resp').size == 0
          hay_por_evaluar = false
        end
      end

    end

    return hay_por_evaluar

  end

  def accesso_calibrar_proceso(hr_process)

    acceso = false

    hr_process.hr_process_calibration_sessions.each do |calibration_session|

      hay = calibration_session.hr_process_calibration_session_cs.where('user_id = ?', user_connected.id).size

      if hay > 0 && (hr_process.calib_dimension || hr_process.calib_cuadrante)
        acceso = true
        break
      end

    end


    return acceso

  end

  def dimension_group_for_evaluation(hr_process_evaluation, hr_process_assessment_e)

    #hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').where('valor_minimo <= ? AND valor_maximo >= ?', hr_process_assessment_e.resultado_porcentaje, hr_process_assessment_e.resultado_porcentaje).first if hr_process_assessment_e

    hr_process_dimension_g_match = nil

    if hr_process_assessment_e

      hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

        if hr_process_assessment_e.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_e.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

          hr_process_dimension_g_match = hr_process_dimension_g
          break

        end

      end

    end

    return hr_process_dimension_g_match

  end

  def dimension_group_for_final_evaluation(hr_process_evaluation, hr_process_assessment_ef)
    #hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').where('valor_minimo <= ? AND valor_maximo >= ?', hr_process_assessment_ef.resultado_porcentaje, hr_process_assessment_ef.resultado_porcentaje).first if hr_process_assessment_ef

    hr_process_dimension_g_match = nil

    if hr_process_assessment_ef

      hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

        if hr_process_assessment_ef.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_ef.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

          hr_process_dimension_g_match = hr_process_dimension_g
          break

        end

      end

    end

    return hr_process_dimension_g_match


  end

  def dimension_group_for_dimension(hr_process_dimension, hr_process_assessment_d)

    #hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').where('valor_minimo <= ? AND valor_maximo >= ?', hr_process_assessment_d.resultado_porcentaje, hr_process_assessment_d.resultado_porcentaje).first if hr_process_assessment_d

    hr_process_dimension_g_match = nil

    if hr_process_assessment_d

      hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC').each do |hr_process_dimension_g|

        if hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_minimo_signo).(hr_process_dimension_g.valor_minimo) && hr_process_assessment_d.resultado_porcentaje.method(hr_process_dimension_g.valor_maximo_signo).(hr_process_dimension_g.valor_maximo)

          hr_process_dimension_g_match = hr_process_dimension_g
          break

        end

      end

    end

    return hr_process_dimension_g_match


  end

end
