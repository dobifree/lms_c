module PeReportingHelper

  def generate_rep_definition_pdf(archivo_temporal, pe_member, pe_process, pe_evaluation)

    require 'prawn/measurement_extensions'
    require 'prawn/table'

    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: 'A4') do |pdf|

      @orientation = 'V'

      pdf.font_size 10

      pdf.start_new_page

      rep_def_header(pdf, pe_evaluation, pe_member, pe_process)

      rep_def_goals(pdf, pe_evaluation, pe_member, pe_process, 1)

      rep_def_extras_1st_level(pdf, pe_evaluation, pe_member, pe_process, 2)

      #pe_evaluation.pe_evaluation_displayed_def_by_users.each_with_index do |displayed_evaluation, index|

        #if displayed_evaluation.pe_displayed_evaluation.pe_elements.size == 1
          #rep_def_extras_1st_level(pdf, pe_evaluation, pe_member, pe_process, index+2)
        #end

      #end



    end

  end

  def generate_rep_detailed_final_results_pdf(archivo_temporal, pe_member, pe_process, show_feedback, show_calibration, show_pe_member_box, show_pe_member_dimension_name, show_comments)

    require 'prawn/measurement_extensions'
    require 'prawn/table'

    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: 'A4') do |pdf|

      @orientation = 'V'

      pdf.font_size 10

      pdf.start_new_page

      rep_dfr_header(pdf, pe_member, pe_process)

      if pe_process.rep_detailed_final_results_elements

        element_position = 1

        prev_ = ''

        pe_process.rep_detailed_final_results_elements.split(',').each do |rep_element|

          rep_element.strip!

          if rep_element == 'resumen_ejecutivo' || rep_element == 'resumen_ejecutivo_nofb'

            if pe_process.of_persons?

              if rep_element == 'resumen_ejecutivo'

                rep_dfr_summary_person(pdf, pe_member, pe_process, show_feedback, show_calibration, element_position, show_pe_member_box, show_pe_member_dimension_name)

              else

                rep_dfr_summary_person(pdf, pe_member, pe_process, false, false, element_position, false, false)

              end

            else

              rep_dfr_summary_area(pdf, pe_member, pe_process, element_position)

            end

            element_position += 1

          elsif rep_element == 'todos_1er_nivel'

            rep_dfr_everybody_1st_level(pdf, pe_member, pe_process, element_position)

            element_position += 1

          elsif rep_element == 'boxes' && show_pe_member_box && show_pe_member_dimension_name

            if pe_process.two_dimensions?

              rep_dfr_boxes_2d(pdf, pe_member, pe_process, element_position)

            else

            end

            element_position += 1

          elsif rep_element == 'feedback'

            rep_dfr_feedback(pdf, pe_member, pe_process, show_feedback, element_position)

            element_position += 1

          elsif rep_element == 'evaluadores_rels'

            rep_evaluadores_rels(pdf, pe_member, pe_process, element_position)

            element_position += 1

          elsif rep_element == 'texto_explicativo_1'

            rep_dfr_exp_text(pdf, pe_process.pe_process_reports.rep_detailed_text_1)

          elsif rep_element == 'texto_explicativo_2'

            rep_dfr_exp_text(pdf, pe_process.pe_process_reports.rep_detailed_text_2)

          elsif rep_element == 'resumen_1er_nivel'

            rep_dfr_summary_1st_level(pdf, pe_member, pe_process, element_position)

            element_position += 1

          elsif rep_element == 'resumen_tipo_evaluador_1er_nivel'

            rep_dfr_evaluator_type_1st_level(pdf, pe_member, pe_process, element_position)

            element_position += 1

          elsif rep_element == 'todos_detalle_1er_nivel'

            element_position = rep_dfr_everybody_1st_level_det(pdf, pe_member, pe_process, element_position, true)

          elsif rep_element == 'todos_detalle_1er_nivel_sp'

            element_position = rep_dfr_everybody_1st_level_det(pdf, pe_member, pe_process, element_position, false)

          elsif rep_element == 'todos_detalle_2do_nivel'

            element_position = rep_dfr_everybody_2nd_level_det(pdf, pe_member, pe_process, element_position, true)

          elsif rep_element == 'todos_detalle_2do_nivel_sp'

            element_position = rep_dfr_everybody_2nd_level_det(pdf, pe_member, pe_process, element_position, false)

          elsif rep_element == 'jefe_detalle_2do_nivel_sp'

            element_position = rep_dfr_jefe_2nd_level_det(pdf, pe_member, pe_process, element_position, false, false)

          elsif rep_element == 'jefe_detalle_2do_nivel_sp_valores'

            element_position = rep_dfr_jefe_2nd_level_det(pdf, pe_member, pe_process, element_position, false, true)

          elsif rep_element == 'todos_detalle_2do_nivel_prom'

            element_position = rep_dfr_final_results_2nd_level_det(pdf, pe_member, pe_process, element_position, true)

          elsif rep_element == 'por_evaluador_1er_nivel'

            element_position = rep_dfr_per_evaluator_1st_level(pdf, pe_member, pe_process, element_position)

          elsif rep_element == 'por_evaluador_3er_nivel_objetivos'

            element_position = rep_dfr_per_evaluator_3rd_level_goals(pdf, pe_member, pe_process, element_position)

          elsif rep_element == 'jefe_2do_nivel_objetivos'

            element_position = rep_dfr_boss_2nd_level_goals(pdf, pe_member, pe_process, false,element_position)

          elsif rep_element == 'jefe_2do_nivel_objetivos_comments'

            if show_comments
              element_position = rep_dfr_boss_2nd_level_goals(pdf, pe_member, pe_process, true, element_position)
            else
              element_position = rep_dfr_boss_2nd_level_goals(pdf, pe_member, pe_process, false, element_position)
            end

          elsif rep_element == 'jefe_3er_nivel_objetivos'

            element_position = rep_dfr_boss_3rd_level_goals(pdf, pe_member, pe_process, element_position, true)

          elsif rep_element == 'jefe_3er_nivel_objetivos_sin_porcentaje'

            element_position = rep_dfr_boss_3rd_level_goals(pdf, pe_member, pe_process, element_position, false)

          elsif rep_element == 'jefe_auto_3er_nivel_objetivos'

            element_position = rep_dfr_boss_auto_3rd_level_goals(pdf, pe_member, pe_process, element_position)

          elsif rep_element == 'informativas_1er_nivel'

            element_position = rep_dfr_informative_1st_level(pdf, pe_member, pe_process, false,element_position)

          elsif rep_element == 'informativas_1er_nivel_comments'

            if show_comments

              element_position = rep_dfr_informative_1st_level(pdf, pe_member, pe_process, true, element_position)

            else

              element_position = rep_dfr_informative_1st_level(pdf, pe_member, pe_process, false, element_position)

            end

          elsif rep_element == 'informativas_2do_nivel'

            element_position = rep_dfr_informative_2nd_level(pdf, pe_member, pe_process, element_position)

          else

            pe_process.pe_rels.each do |pe_rel|

              if rep_element == pe_rel.rel_name.to_s+'_1er_nivel'

                element_position = rep_dfr_pe_rel_1st_level(pdf, pe_member, pe_process, pe_rel, prev_, true, false,false, element_position)

              end

              if rep_element == pe_rel.rel_name.to_s+'_1er_nivel_sp_valores'

                element_position = rep_dfr_pe_rel_1st_level(pdf, pe_member, pe_process, pe_rel, prev_, false, true,false, element_position)

              end

            end

            pe_process.pe_rels.each do |pe_rel|

              if rep_element == pe_rel.rel_name.to_s+'_1er_nivel_comments'
                if show_comments
                  element_position = rep_dfr_pe_rel_1st_level(pdf, pe_member, pe_process, pe_rel, prev_, true, false, true, element_position)
                else
                  element_position = rep_dfr_pe_rel_1st_level(pdf, pe_member, pe_process, pe_rel, prev_, true, false, false, element_position)
                end

              end

            end

            pe_process.pe_evaluations.each do |pe_evaluation|

              if pe_evaluation.cod && rep_element == pe_evaluation.cod+'_todos_2do_nivel_sp_comments'

                element_position = rep_dfr_eval_everybody_2nd_level(pdf, pe_member, pe_process, element_position, pe_evaluation.cod,false, true)

              end


            end

          end

          prev_ = rep_element

        end

      end

    end

  end

  def generate_rep_detailed_final_results_pdf_only_boss(archivo_temporal, pe_member, pe_process, show_feedback, show_calibration, show_pe_member_box, show_pe_member_dimension_name)

    require 'prawn/measurement_extensions'
    require 'prawn/table'

    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: 'A4') do |pdf|

      @orientation = 'V'

      pdf.font_size 10

      pdf.start_new_page

      rep_dfr_header_only_boss(pdf, pe_member, pe_process)

      if pe_process.rep_detailed_final_results_only_boss_elements

        element_position = 1

        pe_process.rep_detailed_final_results_only_boss_elements.split(',').each do |rep_element|

          rep_element.strip!

          if rep_element == 'resumen_ejecutivo'

            if pe_process.of_persons?

              rep_dfr_summary_person(pdf, pe_member, pe_process, show_feedback, show_calibration, element_position, show_pe_member_box, show_pe_member_dimension_name)

            else

              rep_dfr_summary_area(pdf, pe_member, pe_process, element_position)

            end

            element_position += 1

          elsif rep_element == 'jefe_detalle_2do_nivel'

            element_position = rep_dfr_only_boss_2nd_level_det(pdf, pe_member, pe_process, element_position, true)

          elsif rep_element == 'jefe_detalle_2do_nivel_sp'

            element_position = rep_dfr_only_boss_2nd_level_det(pdf, pe_member, pe_process, element_position, false)

          elsif rep_element == 'por_evaluador_1er_nivel'

            element_position = rep_dfr_per_evaluator_only_boss_1st_level(pdf, pe_member, pe_process, element_position)

          elsif rep_element == 'informativas_2do_nivel'

            element_position = rep_dfr_informative_only_boss_2nd_level(pdf, pe_member, pe_process, element_position)

          else

            if rep_element == 'jefe_1er_nivel'

              pe_rel = pe_process.pe_rel_by_id(1)

              element_position = rep_dfr_pe_rel_1st_level(pdf, pe_member, pe_process, pe_rel, element_position)

            end

          end

        end

      end

    end

  end

  def generate_rep_detailed_final_results_pdf_auto(archivo_temporal, pe_member, pe_process, show_feedback, show_calibration, show_pe_member_box, show_pe_member_dimension_name, show_comments)

    pe_rel = pe_process.pe_rel_by_id 0

    require 'prawn/measurement_extensions'
    require 'prawn/table'

    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: 'A4') do |pdf|

      @orientation = 'V'

      pdf.font_size 10

      pdf.start_new_page

      rep_dfr_header_auto(pdf, pe_member, pe_process)

      if pe_process.pe_process_reports.rep_detailed_auto_elements

        element_position = 1

        prev_ = ''

        pe_process.pe_process_reports.rep_detailed_auto_elements.split(',').each do |rep_element|

          rep_element.strip!

          if rep_element == 'auto_3er_nivel_objetivos'

            element_position = rep_dfr_auto_3rd_level_goals(pdf, pe_member, pe_process, element_position)

          elsif rep_element == 'auto_1er_nivel'

            element_position = rep_dfr_pe_rel_1st_level_auto(pdf, pe_member, pe_process, pe_rel, prev_, false, element_position)

          end

          prev_ = rep_element

        end

      end

    end

  end


  def generate_final_results_pdf(archivo_temporal, user, hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations, is_boss = false, himself = false)

    relaciones = %w(jefe par sub cli prov auto)

    lista_relaciones = {'jefe' => hr_process.alias_jefe, 'par' => hr_process.alias_par, 'sub' => hr_process.alias_sub, 'cli' => hr_process.alias_cli, 'prov' => hr_process.alias_prov}
    lista_relaciones_plu = {'jefe' => hr_process.alias_jefe_plu, 'par' => hr_process.alias_par_plu, 'sub' => hr_process.alias_sub_plu, 'cli' => hr_process.alias_cli_plu, 'prov' => hr_process.alias_prov_plu}

    require 'prawn/measurement_extensions'
    require 'prawn/table'

    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: 'A4') do |pdf|

      @orientation = 'V'

      pdf.font_size 10

      pdf.start_new_page

      fr_header(pdf, hr_process, user, hr_process_user)

      positions_final_results_report = hr_process.positions_final_results_report.split(',')

      #resumen,jefe,par,sub,cli,prov,auto,todos,detalle_evals,discretos

      position = 1

      positions_final_results_report.each do |element|

        element.strip!

        if element == 'resumen'

          if (is_boss && hr_process_user.finalizado) || !is_boss

            fr_final_resume(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, is_boss, himself)
            position += 1

          end

        elsif element == 'manual_evals'

          position = fr_manual_evals(position,pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu)

        elsif element == 'todos'

          fr_all_evaluators(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu)
          position += 1

        elsif element == 'detalle_evals'

          position = fr_evaluation_detail(position,pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu)

        elsif element == 'detalle_evals_sp'

          position = fr_evaluation_detail_sp(position,pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu)

        elsif element == 'discretos'

          position = fr_discretes(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, relaciones, lista_relaciones, lista_relaciones_plu)


        else

           relaciones.each do |relacion|

             if element == relacion

               position = fr_by_evaluator_type(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, relacion, lista_relaciones, lista_relaciones_plu)

             end

           end

        end

      end


    end


  end

  def generate_final_results_only_boss_pdf(archivo_temporal, user, hr_process, hr_process_user, hr_process_dimensions, hr_process_evaluations)

    relaciones = %w(jefe par sub cli prov auto)

    lista_relaciones = {'jefe' => hr_process.alias_jefe, 'par' => hr_process.alias_par, 'sub' => hr_process.alias_sub, 'cli' => hr_process.alias_cli, 'prov' => hr_process.alias_prov}
    lista_relaciones_plu = {'jefe' => hr_process.alias_jefe_plu, 'par' => hr_process.alias_par_plu, 'sub' => hr_process.alias_sub_plu, 'cli' => hr_process.alias_cli_plu, 'prov' => hr_process.alias_prov_plu}

    require 'prawn/measurement_extensions'
    require 'prawn/table'

    Prawn::Document.generate(archivo_temporal, skip_page_creation: true, page_size: 'A4') do |pdf|
      pdf.font_size 10

      pdf.start_new_page

      fr_header(pdf, hr_process, user, hr_process_user)

      positions_final_results_report = hr_process.positions_final_results_report.split(',')

      #resumen,jefe,par,sub,cli,prov,auto,todos,detalle_evals,discretos

      position = 1

      positions_final_results_report.each do |element|

        element.strip!

        if element == 'resumen'

          fr_final_resume(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, true, true)
          position += 1

        elsif element == 'manual_evals'

          position = fr_manual_evals(position,pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu)

        elsif element == 'todos'

          #fr_all_evaluators(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu)
          #position += 1

        elsif element == 'detalle_evals'

          position = fr_evaluation_detail(position,pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu, 'jefe')

        elsif element == 'detalle_evals_sp'

          position = fr_evaluation_detail_sp(position,pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu, 'jefe')

        elsif element == 'discretos'

          position = fr_discretes(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, relaciones, lista_relaciones, lista_relaciones_plu, 'jefe')


        else

          relaciones.each do |relacion|

            if element == relacion

              if relacion == 'jefe'

                position = fr_by_evaluator_type(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, relacion, lista_relaciones, lista_relaciones_plu)

              end

            end

          end

        end

      end

    end


  end

  private

    def rep_def_header(pdf, pe_evaluation, pe_member, pe_process)

      pe_rel = pe_process.pe_rel 'jefe'

      table_header = Array.new
      table_header_row = Array.new

      table_header_row = [content: '<font size="12"><b><color rgb="ffffff">DEFINICION DE '+pe_evaluation.name.upcase+'</color></b></font>', colspan: 2]
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Proceso</b>'
      table_header_row.push pe_process.name+' '+pe_process.period
      table_header.push table_header_row

      if pe_process.of_persons?

        table_header_row = Array.new
        table_header_row.push '<b>'+pe_rel.alias_reverse+'</b>'
        table_header_row.push '<b>'+pe_member.user.apellidos+', '+pe_member.user.nombre+'<b>'
        table_header.push table_header_row

        pe_process.pe_characteristics_rep_detailed_final_results.each do |pe_characteristic|

          pe_mc = pe_member.pe_member_characteristic_by_id pe_characteristic.characteristic.id

          table_header_row = Array.new
          table_header_row.push '<b>'+pe_characteristic.characteristic.nombre+'</b>'
          table_header_row.push (pe_mc ? pe_mc.value : '')
          table_header.push table_header_row

        end

      end

      if pe_rel

        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_as_pe_rel pe_rel

        pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

          table_header_row = Array.new
          table_header_row.push '<b>'+pe_rel.alias+'</b>'
          table_header_row.push pe_member_rel_is_evaluated.pe_member_evaluator.user.apellidos+', '+pe_member_rel_is_evaluated.pe_member_evaluator.user.nombre
          table_header.push table_header_row

        end

      end

      pdf.table(table_header, :header => false, width: 520, :column_widths => [150,370], :cell_style => { :inline_format => true }) do |table|

        table.column(0).background_color = 'dddddd'
        table.column(0).row(0).background_color = '333333'

      end

      pdf.move_down 20

    end

    def rep_def_goals(pdf, pe_evaluation, pe_member, pe_process, position)

      pe_rel = pe_process.pe_rel_by_id 1

      if pe_rel

        table_res = Array.new
        tittle_gray2 = Array.new

        if pe_evaluation.has_pe_evaluation_rel(pe_rel) && pe_evaluation.assessment_layout == 1 && pe_evaluation.pe_elements.size == 1

          c_aux = 0

          cp = 4

          table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+pe_evaluation.name.upcase+'</color></b></font>', colspan: cp]
          table_res.push table_res_row

          c_aux += 1

          pe_element_1st = pe_evaluation.pe_elements.first
          pe_element_2nd = pe_evaluation.pe_elements.second


          num_1st_level = 1

          table_res_row = Array.new
          table_res_row.push ''
          table_res_row.push pe_element_1st.name
          table_res_row.push 'Peso'
          table_res_row.push 'Meta'

          table_res.push table_res_row

          tittle_gray2.push c_aux

          c_aux += 1

          num_1st_level = 1

          pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_member).each do |pe_question_1st_level|

            table_res_row = Array.new
            table_res_row.push num_1st_level.to_s
            desc = (pe_question_1st_level.kpi_indicator ? 'KPI' : '')+(pe_question_1st_level.pe_element.display_name ? (pe_question_1st_level.pe_element.name)+'. ' : '')+((pe_question_1st_level.pe_element.kpi_dashboard && pe_question_1st_level.kpi_indicator) ? (pe_question_1st_level.kpi_indicator.name) : pe_question_1st_level.description )

            if pe_question_1st_level.pe_element.allow_note && !pe_question_1st_level.note.blank?

              desc += '<br><br><strong>'+pe_question_1st_level.pe_element.alias_note+':</strong><br>'+format_html_to_pdf(pe_question_1st_level.note)

            end

            table_res_row.push desc
            table_res_row.push number_with_precision(pe_question_1st_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

            if pe_question_1st_level.indicator_type != 4
              tmp_goal = number_with_precision(pe_question_1st_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
            else
              if pe_question_1st_level.indicator_discrete_goal
                tmp_goal = pe_question_1st_level.indicator_discrete_goal
              else
                tmp_goal = 'meta'
              end
            end

            tmp_goal += ' '+pe_question_1st_level.indicator_unit if pe_question_1st_level.indicator_unit

            table_res_row.push tmp_goal

            table_res.push table_res_row

            c_aux += 1

            num_1st_level += 1

          end

        elsif pe_evaluation.has_pe_evaluation_rel(pe_rel) && pe_evaluation.assessment_layout == 1 && pe_evaluation.pe_elements.size == 2

          c_aux = 0

          cp = 4

          table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+pe_evaluation.name.upcase+'</color></b></font>', colspan: cp]
          table_res.push table_res_row

          c_aux += 1

          pe_element_1st = pe_evaluation.pe_elements.first

          num_1st_level = 1

          pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_member).each_with_index do |pe_question_1st_level|

            if pe_element_1st.visible

              table_res_row = Array.new
              cp = 4
              table_res_row.push pdf.make_cell(content: '<font size="12"><b><color rgb="000000"> '+num_1st_level.to_s+'. '+(pe_element_1st.display_name ? pe_element_1st.name+'. ' : '')+(pe_question_1st_level.description)+'</color></b></font>', colspan: cp)
              table_res.push table_res_row

              c_aux += 1

            end

            table_res_row = Array.new
            table_res_row.push ''
            table_res_row.push pe_evaluation.pe_elements.last.name
            table_res_row.push 'Peso'
            table_res_row.push 'Meta'

            table_res.push table_res_row

            tittle_gray2.push c_aux

            c_aux += 1

            num_2nd_level = 1

            pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level).each do |pe_question_2nd_level|

              table_res_row = Array.new
              table_res_row.push num_1st_level.to_s+'.'+num_2nd_level.to_s
              desc = (pe_question_2nd_level.kpi_indicator ? 'KPI' : '')+(pe_question_2nd_level.pe_element.display_name ? (pe_question_2nd_level.pe_element.name)+'. ' : '')+((pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator) ? (pe_question_2nd_level.kpi_indicator.name) : pe_question_2nd_level.description )

              if pe_question_2nd_level.pe_element.allow_note && !pe_question_2nd_level.note.blank?

                desc += '<br><br><strong>'+pe_question_2nd_level.pe_element.alias_note+':</strong><br>'+format_html_to_pdf(pe_question_2nd_level.note)

              end

              table_res_row.push desc
              table_res_row.push number_with_precision(pe_question_2nd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

              if pe_question_2nd_level.indicator_type != 4
                tmp_goal = number_with_precision(pe_question_2nd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
              else
                if pe_question_2nd_level.indicator_discrete_goal
                  tmp_goal = pe_question_2nd_level.indicator_discrete_goal
                else
                  tmp_goal = 'meta'
                end
              end

              tmp_goal += (' '+pe_question_2nd_level.indicator_unit) if pe_question_2nd_level && pe_question_2nd_level.indicator_unit

              table_res_row.push tmp_goal

              table_res.push table_res_row

              c_aux += 1

              num_2nd_level += 1

            end

            num_1st_level += 1

          end

        elsif pe_evaluation.has_pe_evaluation_rel(pe_rel) && pe_evaluation.assessment_layout == 1 && pe_evaluation.pe_elements.size == 3

          c_aux = 0

          cp = 4

          table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+pe_evaluation.name.upcase+'</color></b></font>', colspan: cp]
          table_res.push table_res_row

          c_aux += 1

          pe_element_1st = pe_evaluation.pe_elements.first
          pe_element_2nd = pe_evaluation.pe_elements.second

          num_1st_level = 1

          pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_member).each_with_index do |pe_question_1st_level|

            if pe_element_1st.visible

              table_res_row = Array.new
              cp = 4
              table_res_row.push pdf.make_cell(content: '<font size="12"><b><color rgb="000000"> '+num_1st_level.to_s+'. '+(pe_element_1st.display_name ? pe_element_1st.name+'. ' : '')+(pe_question_1st_level.description)+'</color></b></font>', colspan: cp)
              table_res.push table_res_row

              c_aux += 1

            end

            num_2nd_level = 1

            pe_evaluation.pe_questions_by_question(pe_question_1st_level).each do |pe_question_2nd_level|

              if pe_element_2nd.visible

                table_res_row = Array.new
                cp = 4
                table_res_row.push pdf.make_cell(content: '<font size="10"><b><color rgb="000000"> '+num_1st_level.to_s+'. '+num_2nd_level.to_s+'. '+(pe_element_2nd.display_name ? pe_element_2nd.name+'. ' : '')+(pe_question_2nd_level.description)+'</color></b></font>', colspan: cp)
                table_res.push table_res_row

                c_aux += 1

              end
              
              table_res_row = Array.new
              table_res_row.push ''
              table_res_row.push pe_evaluation.pe_elements.last.name
              table_res_row.push 'Peso'
              table_res_row.push 'Meta'

              table_res.push table_res_row

              tittle_gray2.push c_aux

              c_aux += 1

              num_3rd_level = 1

              pe_evaluation.pe_questions_by_question(pe_question_2nd_level).each do |pe_question_3rd_level|

                table_res_row = Array.new
                table_res_row.push num_1st_level.to_s+'. '+num_2nd_level.to_s+'.'+num_3rd_level.to_s
                desc = (pe_question_3rd_level.kpi_indicator ? 'KPI' : '')+(pe_question_3rd_level.pe_element.display_name ? (pe_question_3rd_level.pe_element.name)+'. ' : '')+((pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator) ? (pe_question_3rd_level.kpi_indicator.name) : pe_question_3rd_level.description )

                if pe_question_3rd_level.pe_element.allow_note && !pe_question_3rd_level.note.blank?

                  desc += '<br><br><strong>'+pe_question_3rd_level.pe_element.alias_note+':</strong><br>'+format_html_to_pdf(pe_question_3rd_level.note)

                end

                table_res_row.push desc
                table_res_row.push number_with_precision(pe_question_3rd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

                if pe_question_3rd_level.indicator_type != 4
                  tmp_goal = number_with_precision(pe_question_3rd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                else
                  if pe_question_3rd_level.indicator_discrete_goal
                    tmp_goal = pe_question_3rd_level.indicator_discrete_goal
                  else
                    tmp_goal = 'meta'
                  end
                end

                tmp_goal += ' '+(pe_question_3rd_level.indicator_unit ? pe_question_3rd_level.indicator_unit : '')

                table_res_row.push tmp_goal

                table_res.push table_res_row

                c_aux += 1

                num_3rd_level += 1

              end

              num_2nd_level += 1

            end

            num_1st_level += 1

          end

        end

        column_widths = [40,320,60,100]

        pdf.table(table_res, :header => false, width: 520, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

          table.column(0).row(0).background_color = '333333'

          tittle_gray2.each do |tg|

            table.column(0).row(tg).background_color = 'eeeeee'
            table.column(1).row(tg).background_color = 'eeeeee'
            table.column(2).row(tg).background_color = 'eeeeee'
            table.column(3).row(tg).background_color = 'eeeeee'


          end

        end

        pdf.move_down 20

        position += 1


      end

      return position

    end

    def rep_def_extras_1st_level(pdf, pe_evaluation_p, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'

      pe_evaluation_p.pe_evaluation_displayed_def_by_users.each do |pe_de|

        pe_evaluation = pe_de.pe_displayed_evaluation

        eval_inf = false

        pe_group = nil

        if pe_evaluation.pe_groups.size > 0

          pe_member_group = pe_member.pe_member_group(pe_evaluation)

          pe_group = pe_member_group.pe_group if pe_member_group

        end

        num_t_eval = 0

        table_res = Array.new

        tittle_gray = Array.new

        c_aux = 0

        table_res_row = Array.new
        table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+' '+pe_evaluation.name.upcase+'</color></b></font>', colspan: 2]
        table_res.push table_res_row

        c_aux += 1

        pe_evaluation.pe_questions_by_group_ready(pe_group, nil).each do |pe_question_1st|

          eval_inf = true

          num_t = 0

          table_res_temp = Array.new

          table_res_row = Array.new
          table_res_row = [content: '<b>'+pe_question_1st.description+'</b>', colspan: 2]
          table_res_temp.push table_res_row

          tittle_gray.push c_aux

          table_res_temp.each do |r|
            table_res.push r
            c_aux += 1
          end

          num_t_eval += num_t

        end

        if eval_inf

          column_widths = Array.new
          column_widths.push 120
          column_widths.push 400

          pdf.table(table_res, :header => false, width: 520, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

            table.column(0).row(0).background_color = '333333'

          end

          pdf.move_down 20

          position += 1

        end


      end

      return position

    end

    def rep_dfr_header(pdf, pe_member, pe_process)

      table_header = Array.new
      table_header_row = Array.new

      table_header_row = [content: '<font size="12"><b><color rgb="ffffff">RESULTADO INDIVIDUAL DE EVALUACIÓN DE DESEMPEÑO</color></b></font>', colspan: 2]
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Proceso</b>'
      table_header_row.push pe_process.name+' '+pe_process.period
      table_header.push table_header_row

      if pe_process.of_persons?

        table_header_row = Array.new
        table_header_row.push '<b>Colaborador evaluado</b>'
        table_header_row.push '<b>'+pe_member.user.apellidos+', '+pe_member.user.nombre+'<b>'
        table_header.push table_header_row

        pe_process.pe_characteristics_rep_detailed_final_results.each do |pe_characteristic|

          pe_mc = pe_member.pe_member_characteristic_by_id pe_characteristic.characteristic.id

          table_header_row = Array.new
          table_header_row.push '<b>'+pe_characteristic.characteristic.nombre+'</b>'
          table_header_row.push (pe_mc ? pe_mc.value : '')
          table_header.push table_header_row

        end

      else

        table_header_row = Array.new
        table_header_row.push '<b>Área evaluada</b>'
        table_header_row.push '<b>'+pe_member.pe_area.name+'<b>'
        table_header.push table_header_row

        table_header_row = Array.new
        table_header_row.push '<b>Responsable</b>'
        table_header_row.push '<b>'+pe_member.user.apellidos+', '+pe_member.user.nombre+'<b>'
        table_header.push table_header_row

      end

      if pe_process.rep_detailed_final_results_show_boss?

        pe_rel = pe_process.pe_rel 'jefe'

        if pe_rel

          pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_as_pe_rel pe_rel

          pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

            table_header_row = Array.new
            table_header_row.push '<b>'+pe_rel.alias+'</b>'
            table_header_row.push pe_member_rel_is_evaluated.pe_member_evaluator.user.apellidos+', '+pe_member_rel_is_evaluated.pe_member_evaluator.user.nombre
            table_header.push table_header_row

          end

        end

      end

      if pe_process.rep_detailed_final_results_show_feedback_provider

        table_header_row = Array.new
        table_header_row.push '<b>Responsable de '+@pe_process.step_feedback_alias+'</b>'
        table_header_row.push pe_member.feedback_provider.apellidos+', '+pe_member.feedback_provider.nombre
        table_header.push table_header_row

      end

      if pe_process.rep_detailed_final_results_show_feedback_date

        table_header_row = Array.new
        table_header_row.push '<b>Fecha de '+@pe_process.step_feedback_alias+'</b>'
        if pe_member.step_feedback
          table_header_row.push (localize pe_member.step_feedback_date, format: :full_date)
        else
          table_header_row.push ''
        end
        table_header.push table_header_row

      end

      if pe_process.rep_detailed_final_results_show_feedback_accepted_date

        table_header_row = Array.new
        table_header_row.push '<b>Fecha de confirmación de '+@pe_process.step_feedback_alias+'</b>'
        if pe_member.step_feedback_accepted
          table_header_row.push (localize pe_member.step_feedback_date, format: :full_date)
        else
          table_header_row.push ''
        end
        table_header.push table_header_row

      end

      pdf.table(table_header, :header => false, width: 520, :column_widths => [150,370], :cell_style => { :inline_format => true }) do |table|

        table.column(0).background_color = 'dddddd'
        table.column(0).row(0).background_color = '333333'

      end

      pdf.move_down 20

    end

    def rep_dfr_header_auto(pdf, pe_member, pe_process)

      table_header = Array.new
      table_header_row = Array.new

      table_header_row = [content: '<font size="12"><b><color rgb="ffffff">RESULTADO INDIVIDUAL DE EVALUACIÓN DE DESEMPEÑO - AUTOEVALUACIÓN</color></b></font>', colspan: 2]
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Proceso</b>'
      table_header_row.push pe_process.name+' '+pe_process.period
      table_header.push table_header_row

      if pe_process.of_persons?

        table_header_row = Array.new
        table_header_row.push '<b>Colaborador evaluado</b>'
        table_header_row.push '<b>'+pe_member.user.apellidos+', '+pe_member.user.nombre+'<b>'
        table_header.push table_header_row

        pe_process.pe_characteristics_rep_detailed_final_results.each do |pe_characteristic|

          pe_mc = pe_member.pe_member_characteristic_by_id pe_characteristic.characteristic.id

          table_header_row = Array.new
          table_header_row.push '<b>'+pe_characteristic.characteristic.nombre+'</b>'
          table_header_row.push (pe_mc ? pe_mc.value : '')
          table_header.push table_header_row

        end

      else

        table_header_row = Array.new
        table_header_row.push '<b>Área evaluada</b>'
        table_header_row.push '<b>'+pe_member.pe_area.name+'<b>'
        table_header.push table_header_row

        table_header_row = Array.new
        table_header_row.push '<b>Responsable</b>'
        table_header_row.push '<b>'+pe_member.user.apellidos+', '+pe_member.user.nombre+'<b>'
        table_header.push table_header_row

      end



      pdf.table(table_header, :header => false, width: 520, :column_widths => [150,370], :cell_style => { :inline_format => true }) do |table|

        table.column(0).background_color = 'dddddd'
        table.column(0).row(0).background_color = '333333'

      end

      pdf.move_down 20

    end

    def rep_dfr_header_only_boss(pdf, pe_member, pe_process)

      table_header = Array.new
      table_header_row = Array.new

      table_header_row = [content: '<font size="12"><b><color rgb="ffffff">RESULTADO INDIVIDUAL DE EVALUACIÓN DE DESEMPEÑO<br>Sólo evaluación '+pe_process.pe_rel_by_id(1).alias+'</color></b></font>', colspan: 2]
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Proceso</b>'
      table_header_row.push pe_process.name+' '+pe_process.period
      table_header.push table_header_row

      if pe_process.of_persons?

        table_header_row = Array.new
        table_header_row.push '<b>Colaborador evaluado</b>'
        table_header_row.push '<b>'+pe_member.user.apellidos+', '+pe_member.user.nombre+'<b>'
        table_header.push table_header_row

        pe_process.pe_characteristics_rep_detailed_final_results.each do |pe_characteristic|

          pe_mc = pe_member.pe_member_characteristic_by_id pe_characteristic.characteristic.id

          table_header_row = Array.new
          table_header_row.push '<b>'+pe_characteristic.characteristic.nombre+'</b>'
          table_header_row.push (pe_mc ? pe_mc.value : '')
          table_header.push table_header_row

        end

      else

        table_header_row = Array.new
        table_header_row.push '<b>Área evaluada</b>'
        table_header_row.push '<b>'+pe_member.pe_area.name+'<b>'
        table_header.push table_header_row

        table_header_row = Array.new
        table_header_row.push '<b>Responsable</b>'
        table_header_row.push '<b>'+pe_member.user.apellidos+', '+pe_member.user.nombre+'<b>'
        table_header.push table_header_row

      end

      if pe_process.rep_detailed_final_results_only_boss_show_boss?

        pe_rel = pe_process.pe_rel 'jefe'

        if pe_rel

          pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_as_pe_rel pe_rel

          pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

            table_header_row = Array.new
            table_header_row.push '<b>'+pe_rel.alias+'</b>'
            table_header_row.push pe_member_rel_is_evaluated.pe_member_evaluator.user.apellidos+', '+pe_member_rel_is_evaluated.pe_member_evaluator.user.nombre
            table_header.push table_header_row

          end

        end

      end

      if pe_process.rep_detailed_final_results_show_feedback_provider

        table_header_row = Array.new
        table_header_row.push '<b>Retroalimentado por</b>'
        table_header_row.push pe_member.feedback_provider.apellidos+', '+pe_member.feedback_provider.nombre
        table_header.push table_header_row

      end

      if pe_process.rep_detailed_final_results_show_feedback_date

        table_header_row = Array.new
        table_header_row.push '<b>Fecha de retroalimentación</b>'
        if pe_member.step_feedback
          table_header_row.push (localize pe_member.step_feedback_date, format: :full_date)
        else
          table_header_row.push ''
        end
        table_header.push table_header_row

      end

      if pe_process.rep_detailed_final_results_show_feedback_accepted_date

        table_header_row = Array.new
        table_header_row.push '<b>Fecha de confirmación de retroalimentación</b>'
        if pe_member.step_feedback_accepted
          table_header_row.push (localize pe_member.step_feedback_date, format: :full_date)
        else
          table_header_row.push ''
        end
        table_header.push table_header_row

      end

      pdf.table(table_header, :header => false, width: 520, :column_widths => [150,370], :cell_style => { :inline_format => true }) do |table|

        table.column(0).background_color = 'dddddd'
        table.column(0).row(0).background_color = '333333'

      end

      pdf.move_down 20

    end

    def rep_dfr_summary_person(pdf, pe_member, pe_process, show_feedback, show_calibration, position, show_pe_member_box, show_pe_member_dimension_name)

      tittle_gray = Array.new

      table_summary = Array.new
      table_summary_row = Array.new

      c_aux = 0

      table_summary_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. RESUMEN EJECUTIVO</color></b></font>', colspan: 5]
      table_summary.push table_summary_row

      c_aux += 1

      table_summary_row = Array.new
      table_summary_row = [content: '<b>'+position.to_s+'.1. '+pe_process.step_assessment_final_result_alias+'</b>', colspan: 5]
      table_summary.push table_summary_row

      tittle_gray.push c_aux

      c_aux += 1

      pe_process.pe_dimensions.each do |pe_dimension|

        table_summary_row = Array.new
        tmp = ''

        pe_assessment_dimension = pe_member.pe_assessment_dimension(pe_dimension)

        if pe_assessment_dimension && pe_assessment_dimension.percentage >= 0

          tmp += number_with_precision(pe_assessment_dimension.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
          tmp += ' - '+pe_assessment_dimension.pe_dimension_group.name if pe_assessment_dimension.pe_dimension_group && show_pe_member_dimension_name

          break unless pe_process.two_dimensions?

        end

        table_summary_row = ['<b>'+pe_dimension.name+'</b>', {content: tmp, colspan: 4}]
        table_summary.push table_summary_row

        c_aux += 1

      end

      if show_pe_member_box

        tmp = ''

        if pe_member.pe_box

          tmp += pe_member.pe_box.short_name+' ' if pe_member.pe_box.short_name
          tmp += pe_member.pe_box.name

        end

        table_summary_row = ['<b>'+pe_process.step_assessment_final_result_alias+'</b>', {content: tmp, colspan: 4}]
        table_summary.push table_summary_row

        c_aux += 1

        if pe_member.pe_box

          unless pe_member.pe_box.description.blank?

            tmp = pe_member.pe_box.description

            table_summary_row = ['<b>Descripción de la '+pe_process.step_assessment_final_result_alias.downcase+'</b>', {content: tmp, colspan: 4}]
            table_summary.push table_summary_row

            c_aux += 1

          end

        end

      end

      if pe_member.step_calibration && show_calibration

        unless pe_member.calibration_comment.blank?

          table_summary_row = Array.new
          tmp = pe_member.calibration_comment

          table_summary_row = ['<b>Comentario de la calibración</b>', {content: tmp, colspan: 4}]
          table_summary.push table_summary_row

          c_aux += 1

        end

      end


      table_summary_row = Array.new
      table_summary_row = [content: '<b>'+position.to_s+'.2. Valoración por dimensión y evaluación</b>', colspan: 5]
      table_summary.push table_summary_row

      tittle_gray.push c_aux

      c_aux += 1

      table_summary_row = Array.new
      table_summary_row.push '<b>Dimensión</b>'
      table_summary_row.push '<b>Evaluación</b>'
      table_summary_row.push '<b>Peso</b>'
      table_summary_row.push '<b>Resultado</b>'
      table_summary_row.push '<b>Valoración dimensión</b>'
      table_summary.push table_summary_row

      c_aux += 1

      pe_process.pe_dimensions.each do |pe_dimension|

        if pe_dimension.pe_evaluations.where('informative = ? OR (informative = ? and weight > 0)', false, true).size > 0

          tmp_result_dim = ''

          pe_assessment_dimension = pe_member.pe_assessment_dimension(pe_dimension)

          if pe_assessment_dimension && pe_assessment_dimension.percentage >= 0

            tmp_result_dim = number_with_precision(pe_assessment_dimension.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            tmp_result_dim += ' - '+pe_assessment_dimension.pe_dimension_group.name if pe_assessment_dimension.pe_dimension_group && show_pe_member_dimension_name

          end

          num_evals = 0

          pe_dimension.pe_evaluations.where('informative = ? OR (informative = ? and weight > 0)', false, true).each do |pe_evaluation|
            pe_member_evaluation = pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first
            num_evals += 1 if pe_member_evaluation.nil? || (pe_member_evaluation && pe_member_evaluation.weight > -1 )
          end

          pe_eval_index = 0

          pe_dimension.pe_evaluations.where('informative = ? OR (informative = ? and weight > 0)', false, true).each do |pe_evaluation|

            pe_member_evaluation = pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

            if pe_member_evaluation.nil? || (pe_member_evaluation && pe_member_evaluation.weight > -1 )

              table_summary_row = Array.new

              tmp_weight_eval = ''
              tmp_result_eval = ''

              pe_member_evaluation = pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

              if pe_member_evaluation
                tmp_weight_eval = pe_member_evaluation.weight.to_s if pe_member_evaluation.weight > -1
              else
                tmp_weight_eval = pe_evaluation.weight.to_s
              end

              if !pe_evaluation.informative && pe_member.has_to_be_assessed_in_evaluation(pe_evaluation)

                pe_assessment_finalevaluation = pe_member.pe_assessment_final_evaluation(pe_evaluation)

                if pe_assessment_finalevaluation && pe_assessment_finalevaluation.percentage >= 0

                  tmp_result_eval = number_with_precision(pe_assessment_finalevaluation.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                  tmp_result_eval += ' - '+pe_assessment_finalevaluation.pe_dimension_group.name if pe_assessment_finalevaluation.pe_dimension_group && show_pe_member_dimension_name

                end

              end


              if pe_eval_index == 0

                table_summary_row = [{content: pe_dimension.name, rowspan: num_evals},
                                     pe_evaluation.name,
                                     tmp_weight_eval,
                                     tmp_result_eval,
                                     {content: tmp_result_dim, rowspan: num_evals}]
              else

                table_summary_row = [pe_evaluation.name,
                                     tmp_weight_eval,
                                     tmp_result_eval]

              end

              table_summary.push table_summary_row
              c_aux += 1

              pe_eval_index += 1

            end

          end

        end

        break unless pe_process.two_dimensions?

      end

      if pe_member.step_feedback? && show_feedback

        table_summary_row = Array.new
        table_summary_row = [content: '<b>'+position.to_s+'.3. '+pe_process.step_feedback_alias+'</b>', colspan: 5]
        table_summary.push table_summary_row

        tittle_gray.push c_aux

        pe_process.pe_feedback_fields.each_with_index do |pe_feedback_field, index_feedback_field|

          table_summary_row = Array.new
          table_summary_row = [content: '<b>'+(pe_process.pe_feedback_fields.size > 1 ? (index_feedback_field+1).to_s+'. ' : '')+pe_feedback_field.name+'</b>', colspan: 5]
          table_summary.push table_summary_row

          pe_member_feedback = pe_member.pe_member_feedbacks_by_field(pe_feedback_field)

          if pe_feedback_field.simple

            c_aux += 2

            retro = pe_member_feedback ? pe_member_feedback.formatted_comment  : ''
            retro.gsub! '&nbsp;', ' '
            retro.gsub! '<li>', "\n-"
            retro.gsub! '<br>', "\n"

            table_summary_row = Array.new
            table_summary_row = [content: (ActionView::Base.full_sanitizer.sanitize retro), colspan: 5]
            table_summary.push table_summary_row

            if $current_domain.include?('security') && pe_feedback_field.name.include?('solicitar capacita') && (pe_feedback_field.id == 21 || pe_feedback_field.id == 16 || pe_feedback_field.id == 23 || pe_feedback_field.id == 30 || pe_feedback_field.id == 44) && pe_member_feedback && pe_member_feedback.formatted_comment == 'Sí'

              ttttt_sec = 'Todas las solicitudes de capacitación serán aprobadas en base a la evaluación y presupuesto del área.<br><br>Las actividades serán implementadas de acuerdo a la priorización señalada en el formulario y al tiempo disponible por cada persona.'
              ttttt_sec.gsub! '<br>', "\n"

              table_summary_row = Array.new
              table_summary_row = [content: (ActionView::Base.full_sanitizer.sanitize ttttt_sec), colspan: 5]
              table_summary.push table_summary_row

            end


          else

            c_aux += 1

            table_summary_row = Array.new

            if pe_member_feedback

              table_tmp = Array.new

              pe_feedback_field.pe_feedback_compound_fields.each_with_index do |pe_feedback_compound_field, index_c|
                table_summary_row.push '<b>'+pe_feedback_compound_field.name+'</b>'
              end

              table_tmp.push table_summary_row

              width_cell = 520/pe_feedback_field.pe_feedback_compound_fields.size

              width_cells = Array.new

              last_width_cell = 520-(width_cell*(pe_feedback_field.pe_feedback_compound_fields.size-1))

              (1..pe_member_feedback.number_of_compound_feedbacks).each do |number_of_compound_feedback|

                c_aux += 1

                table_summary_row = Array.new

                pe_feedback_field.pe_feedback_compound_fields.each_with_index do |pe_feedback_compound_field, index_c|

                  pe_member_compound_feedback = pe_member_feedback.pe_member_compound_feedbacks_by_field(number_of_compound_feedback, pe_feedback_compound_field)

                  retro = pe_member_compound_feedback ? pe_member_compound_feedback.formatted_comment  : ''
                  retro.gsub! '&nbsp;', ' '
                  retro.gsub! '<li>', "\n-"
                  retro.gsub! '<br>', "\n"

                  table_summary_row.push retro

                end

                table_tmp.push table_summary_row

              end

              pe_feedback_field.pe_feedback_compound_fields.each do |pe_feedback_compound_field|
                width_cells.push width_cell
              end
              width_cells.pop
              width_cells.push last_width_cell

              table_summary.push [content: pdf.make_table(table_tmp, :column_widths => width_cells, :cell_style => { :inline_format => true }), colspan: 5]


            end

          end

        end

        if pe_member.step_feedback_accepted? && pe_member.pe_member_accepted_feedbacks.size > 0  && pe_process.pe_feedback_accepted_fields.where('is_survey = ?', false).size > 0

          table_summary_row = Array.new
          table_summary_row = [content: '<b>'+position.to_s+'.4. '+pe_process.step_feedback_accepted_alias+'</b>', colspan: 5]
          table_summary.push table_summary_row

          c_aux += 1

          tittle_gray.push c_aux

          pe_process.pe_feedback_accepted_fields.where('is_survey = ?', false).each_with_index do |pe_feedback_accepted_field, index_feedback_field|

            table_summary_row = Array.new
            table_summary_row = [content: '<b>'+(pe_process.pe_feedback_accepted_fields.size > 1 ? (index_feedback_field+1).to_s+'. ' : '')+pe_feedback_accepted_field.name+'</b>', colspan: 5]
            table_summary.push table_summary_row

            pe_member_accepted_feedback = pe_member.pe_member_accepted_feedbacks_by_field(pe_feedback_accepted_field)

            if pe_feedback_accepted_field.simple

              retro = pe_member_accepted_feedback ? pe_member_accepted_feedback.formatted_comment  : ''
              retro.gsub! '&nbsp;', ' '
              retro.gsub! '<li>', "\n-"
              retro.gsub! '<br>', "\n"

              table_summary_row = Array.new
              table_summary_row = [content: (ActionView::Base.full_sanitizer.sanitize retro), colspan: 5]
              table_summary.push table_summary_row

            end

          end


        end

      end

      pdf.table(table_summary, :header => false, :column_widths => [150,100,50,110,110], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'

        tittle_gray.each do |tg|

          table.column(0).row(tg).background_color = 'dddddd'

        end

        table.column(2).style :align => :center
        table.column(3).style :align => :center
        table.column(4).style :align => :center

      end



    end

    def rep_dfr_summary_person_auto(pdf, pe_member, pe_process, show_feedback, show_calibration, position, show_pe_member_box, show_pe_member_dimension_name)

      tittle_gray = Array.new

      table_summary = Array.new
      table_summary_row = Array.new

      c_aux = 0

      table_summary_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. RESUMEN EJECUTIVO - AUTOEVALUACION</color></b></font>', colspan: 5]
      table_summary.push table_summary_row

      c_aux += 1

      table_summary_row = Array.new
      table_summary_row = [content: '<b>'+position.to_s+'.1. '+pe_process.step_assessment_final_result_alias+'</b>', colspan: 5]
      table_summary.push table_summary_row

      tittle_gray.push c_aux

      c_aux += 1

      pe_process.pe_dimensions.each do |pe_dimension|

        table_summary_row = Array.new
        tmp = ''

        pe_assessment_dimension = pe_member.pe_assessment_dimension(pe_dimension)

        if pe_assessment_dimension && pe_assessment_dimension.percentage >= 0

          tmp += number_with_precision(pe_assessment_dimension.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
          tmp += ' - '+pe_assessment_dimension.pe_dimension_group.name if pe_assessment_dimension.pe_dimension_group && show_pe_member_dimension_name

          break unless pe_process.two_dimensions?

        end

        table_summary_row = ['<b>'+pe_dimension.name+'</b>', {content: tmp, colspan: 4}]
        table_summary.push table_summary_row

        c_aux += 1

      end

      if show_pe_member_box

        tmp = ''

        if pe_member.pe_box

          tmp += pe_member.pe_box.short_name+' ' if pe_member.pe_box.short_name
          tmp += pe_member.pe_box.name

        end

        table_summary_row = ['<b>'+pe_process.step_assessment_final_result_alias+'</b>', {content: tmp, colspan: 4}]
        table_summary.push table_summary_row

        c_aux += 1

        if pe_member.pe_box

          unless pe_member.pe_box.description.blank?

            tmp = pe_member.pe_box.description

            table_summary_row = ['<b>Descripción de la '+pe_process.step_assessment_final_result_alias.downcase+'</b>', {content: tmp, colspan: 4}]
            table_summary.push table_summary_row

            c_aux += 1

          end

        end

      end

      if pe_member.step_calibration && show_calibration

        unless pe_member.calibration_comment.blank?

          table_summary_row = Array.new
          tmp = pe_member.calibration_comment

          table_summary_row = ['<b>Comentario de la calibración</b>', {content: tmp, colspan: 4}]
          table_summary.push table_summary_row

          c_aux += 1

        end

      end


      table_summary_row = Array.new
      table_summary_row = [content: '<b>'+position.to_s+'.2. Valoración por dimensión y evaluación</b>', colspan: 5]
      table_summary.push table_summary_row

      tittle_gray.push c_aux

      c_aux += 1

      table_summary_row = Array.new
      table_summary_row.push '<b>Dimensión</b>'
      table_summary_row.push '<b>Evaluación</b>'
      table_summary_row.push '<b>Peso</b>'
      table_summary_row.push '<b>Resultado</b>'
      table_summary_row.push '<b>Valoración dimensión</b>'
      table_summary.push table_summary_row

      c_aux += 1

      pe_process.pe_dimensions.each do |pe_dimension|

        if pe_dimension.pe_evaluations.where('informative = ? OR (informative = ? and weight > 0)', false, true).size > 0

          tmp_result_dim = ''

          pe_assessment_dimension = pe_member.pe_assessment_dimension(pe_dimension)

          if pe_assessment_dimension && pe_assessment_dimension.percentage >= 0

            tmp_result_dim = number_with_precision(pe_assessment_dimension.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            tmp_result_dim += ' - '+pe_assessment_dimension.pe_dimension_group.name if pe_assessment_dimension.pe_dimension_group && show_pe_member_dimension_name

          end

          pe_dimension.pe_evaluations.where('informative = ? OR (informative = ? and weight > 0)', false, true).each_with_index do |pe_evaluation, pe_eval_index|

            table_summary_row = Array.new

            tmp_weight_eval = ''
            tmp_result_eval = ''

            pe_member_evaluation = pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

            if pe_member_evaluation
              tmp_weight_eval = pe_member_evaluation.weight.to_s if pe_member_evaluation.weight > -1
            else
              tmp_weight_eval = pe_evaluation.weight.to_s
            end

            if !pe_evaluation.informative && pe_member.has_to_be_assessed_in_evaluation(pe_evaluation)

              pe_assessment_finalevaluation = pe_member.pe_assessment_final_evaluation(pe_evaluation)

              if pe_assessment_finalevaluation && pe_assessment_finalevaluation.percentage >= 0

                tmp_result_eval = number_with_precision(pe_assessment_finalevaluation.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                tmp_result_eval += ' - '+pe_assessment_finalevaluation.pe_dimension_group.name if pe_assessment_finalevaluation.pe_dimension_group && show_pe_member_dimension_name

              end

            end

            if pe_eval_index == 0

              table_summary_row = [{content: pe_dimension.name, rowspan: pe_dimension.pe_evaluations.where('informative = ? OR (informative = ? and weight > 0)', false, true).size},
                                   pe_evaluation.name,
                                   tmp_weight_eval,
                                   tmp_result_eval,
                                   {content: tmp_result_dim, rowspan: pe_dimension.pe_evaluations.where('informative = ? OR (informative = ? and weight > 0)', false, true).size}]
            else

              table_summary_row = [pe_evaluation.name,
                                   tmp_weight_eval,
                                   tmp_result_eval]

            end

            table_summary.push table_summary_row
            c_aux += 1

          end

        end

        break unless pe_process.two_dimensions?

      end

      if pe_member.step_feedback? && show_feedback

        table_summary_row = Array.new
        table_summary_row = [content: '<b>'+position.to_s+'.3. '+pe_process.step_feedback_alias+'</b>', colspan: 5]
        table_summary.push table_summary_row

        tittle_gray.push c_aux

        pe_process.pe_feedback_fields.each_with_index do |pe_feedback_field, index_feedback_field|

          table_summary_row = Array.new
          table_summary_row = [content: '<b>'+(pe_process.pe_feedback_fields.size > 1 ? (index_feedback_field+1).to_s+'. ' : '')+pe_feedback_field.name+'</b>', colspan: 5]
          table_summary.push table_summary_row

          pe_member_feedback = pe_member.pe_member_feedbacks_by_field(pe_feedback_field)

          if pe_feedback_field.simple

            c_aux += 2

            retro = pe_member_feedback ? pe_member_feedback.formatted_comment  : ''
            retro.gsub! '<li>', "\n-"
            retro.gsub! '<br>', "\n"

            table_summary_row = Array.new
            table_summary_row = [content: (ActionView::Base.full_sanitizer.sanitize retro), colspan: 5]
            table_summary.push table_summary_row

          else

            c_aux += 1

            table_summary_row = Array.new

            if pe_member_feedback

              table_tmp = Array.new

              pe_feedback_field.pe_feedback_compound_fields.each_with_index do |pe_feedback_compound_field, index_c|
                table_summary_row.push '<b>'+pe_feedback_compound_field.name+'</b>'
              end

              table_tmp.push table_summary_row

              width_cell = 520/pe_feedback_field.pe_feedback_compound_fields.size

              width_cells = Array.new

              last_width_cell = 520-(width_cell*(pe_feedback_field.pe_feedback_compound_fields.size-1))

              (1..pe_member_feedback.number_of_compound_feedbacks).each do |number_of_compound_feedback|

                c_aux += 1

                table_summary_row = Array.new

                pe_feedback_field.pe_feedback_compound_fields.each_with_index do |pe_feedback_compound_field, index_c|

                  pe_member_compound_feedback = pe_member_feedback.pe_member_compound_feedbacks_by_field(number_of_compound_feedback, pe_feedback_compound_field)

                  retro = pe_member_compound_feedback ? pe_member_compound_feedback.formatted_comment  : ''
                  retro.gsub! '<li>', "\n-"
                  retro.gsub! '<br>', "\n"

                  table_summary_row.push retro

                end

                table_tmp.push table_summary_row

              end

              pe_feedback_field.pe_feedback_compound_fields.each do |pe_feedback_compound_field|
                width_cells.push width_cell
              end
              width_cells.pop
              width_cells.push last_width_cell

              table_summary.push [content: pdf.make_table(table_tmp, :column_widths => width_cells, :cell_style => { :inline_format => true }), colspan: 5]


            end

          end

        end

        if pe_member.step_feedback_accepted? && pe_member.pe_member_accepted_feedbacks.size > 0

          table_summary_row = Array.new
          table_summary_row = [content: '<b>'+position.to_s+'.4. '+pe_process.step_feedback_accepted_alias+'</b>', colspan: 5]
          table_summary.push table_summary_row

          c_aux += 1

          tittle_gray.push c_aux

          pe_process.pe_feedback_accepted_fields.where('is_survey = ?', false).each_with_index do |pe_feedback_accepted_field, index_feedback_field|

            table_summary_row = Array.new
            table_summary_row = [content: '<b>'+(pe_process.pe_feedback_accepted_fields.size > 1 ? (index_feedback_field+1).to_s+'. ' : '')+pe_feedback_accepted_field.name+'</b>', colspan: 5]
            table_summary.push table_summary_row

            pe_member_accepted_feedback = pe_member.pe_member_accepted_feedbacks_by_field(pe_feedback_accepted_field)

            if pe_feedback_accepted_field.simple

              retro = pe_member_accepted_feedback ? pe_member_accepted_feedback.formatted_comment  : ''
              retro.gsub! '<li>', "\n-"
              retro.gsub! '<br>', "\n"

              table_summary_row = Array.new
              table_summary_row = [content: (ActionView::Base.full_sanitizer.sanitize retro), colspan: 5]
              table_summary.push table_summary_row

            end

          end


        end

      end

      pdf.table(table_summary, :header => false, :column_widths => [150,100,50,110,110], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'

        tittle_gray.each do |tg|

          table.column(0).row(tg).background_color = 'dddddd'

        end

        table.column(2).style :align => :center
        table.column(3).style :align => :center
        table.column(4).style :align => :center

      end



    end

    def rep_dfr_summary_area(pdf, pe_member, pe_process, position)

      tittle_gray = Array.new

      table_summary = Array.new
      table_summary_row = Array.new

      c_aux = 0

      table_summary_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. RESUMEN EJECUTIVO</color></b></font>', colspan: 5]
      table_summary.push table_summary_row

      c_aux += 1

      table_summary_row = Array.new
      table_summary_row = [content: '<b>'+position.to_s+'.1. '+pe_process.step_assessment_final_result_alias+'</b>', colspan: 5]
      table_summary.push table_summary_row

      tittle_gray.push c_aux

      c_aux += 1

      pe_process.pe_dimensions.each do |pe_dimension|

        table_summary_row = Array.new
        tmp = ''

        pe_assessment_dimension = pe_member.pe_assessment_dimension(pe_dimension)

        if pe_assessment_dimension

          tmp += number_with_precision(pe_assessment_dimension.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
          tmp += ' - '+pe_assessment_dimension.pe_dimension_group.name if pe_assessment_dimension.pe_dimension_group

          break unless pe_process.two_dimensions?

        end

        table_summary_row = ['<b>'+pe_dimension.name+'</b>', {content: tmp, colspan: 4}]
        table_summary.push table_summary_row

        c_aux += 1

      end

      table_summary_row = Array.new
      tmp = ''

      if pe_member.pe_box

        tmp += pe_member.pe_box.short_name
        tmp += ' - '+pe_member.pe_box.name

      end

      table_summary_row = ['<b>'+pe_process.step_assessment_final_result_alias+'</b>', {content: tmp, colspan: 4}]
      table_summary.push table_summary_row

      c_aux += 1

      if pe_member.pe_box

        unless pe_member.pe_box.description.blank?

          table_summary_row = Array.new
          tmp = pe_member.pe_box.description

          table_summary_row = ['<b>Descripción de la '+pe_process.step_assessment_final_result_alias.downcase+'</b>', {content: tmp, colspan: 4}]
          table_summary.push table_summary_row

          c_aux += 1

        end

      end

      if pe_process.has_step_calibration? && pe_member.step_calibration? && pe_process.rep_detailed_final_results_show_calibration_for_manager

        table_summary_row = Array.new
        tmp = pe_member.calibration_comment

        table_summary_row = ['<b>Comentario de la calibración</b>', {content: tmp, colspan: 4}]
        table_summary.push table_summary_row

        c_aux += 1

      end


      table_summary_row = Array.new
      table_summary_row = [content: '<b>'+position.to_s+'.2. Valoración por dimensión y evaluación</b>', colspan: 5]
      table_summary.push table_summary_row

      tittle_gray.push c_aux

      c_aux += 1

      table_summary_row = Array.new
      table_summary_row.push '<b>Dimensión</b>'
      table_summary_row.push '<b>Evaluación</b>'
      table_summary_row.push '<b>Peso</b>'
      table_summary_row.push '<b>Resultado</b>'
      table_summary_row.push '<b>Valoración dimensión</b>'
      table_summary.push table_summary_row

      c_aux += 1

      pe_process.pe_dimensions.each do |pe_dimension|

        if pe_dimension.pe_evaluations.size > 0

          tmp_result_dim = ''

          pe_assessment_dimension = pe_member.pe_assessment_dimension(pe_dimension)

          if pe_assessment_dimension

            tmp_result_dim = number_with_precision(pe_assessment_dimension.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            tmp_result_dim += ' - '+pe_assessment_dimension.pe_dimension_group.name if pe_assessment_dimension.pe_dimension_group

          end

          pe_dimension.pe_evaluations.each_with_index do |pe_evaluation, pe_eval_index|

            table_summary_row = Array.new

            tmp_weight_eval = ''
            tmp_result_eval = ''

            pe_member_evaluation = pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

            if pe_member_evaluation
              tmp_weight_eval = pe_member_evaluation.weight.to_s
            else
              tmp_weight_eval = pe_evaluation.weight.to_s
            end

            pe_assessment_finalevaluation = pe_member.pe_assessment_final_evaluation(pe_evaluation)

            if pe_assessment_finalevaluation && pe_assessment_finalevaluation.percentage >= 0

              tmp_result_eval = number_with_precision(pe_assessment_finalevaluation.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
              tmp_result_eval += ' - '+pe_assessment_finalevaluation.pe_dimension_group.name if pe_assessment_finalevaluation.pe_dimension_group

            end

            if pe_eval_index == 0

              table_summary_row = [{content: pe_dimension.name, rowspan: pe_dimension.pe_evaluations.size},
                                   pe_evaluation.name,
                                   tmp_weight_eval,
                                   tmp_result_eval,
                                   {content: tmp_result_dim, rowspan: pe_dimension.pe_evaluations.size}]
            else

              table_summary_row = [pe_evaluation.name,
                                   tmp_weight_eval,
                                   tmp_result_eval]

            end

            table_summary.push table_summary_row
            c_aux += 1

          end

        end

        break unless pe_process.two_dimensions?

      end

      table_summary_row = Array.new
      table_summary_row = [content: '<b>'+position.to_s+'.3. Valoración por área</b>', colspan: 5]
      table_summary.push table_summary_row

      tittle_gray.push c_aux

      c_aux += 1

      witdh_cell = 520/(pe_process.pe_evaluations.size+3)

      witdh_cells = Array.new

      last_witdh_cell = 520-(witdh_cell*(pe_process.pe_evaluations.size+2))

      table_summary_temp = Array.new

      table_summary_row = Array.new
      table_summary_row.push '<b>Área</b>'
      table_summary_row.push '<b>Responsable</b>'
      table_summary_row.push '<b>Peso</b>'

      witdh_cells.push witdh_cell
      witdh_cells.push witdh_cell
      witdh_cells.push witdh_cell

      pe_process.pe_evaluations.each do |pe_evaluation|
        table_summary_row.push '<b>'+pe_evaluation.name+'</b>'
        witdh_cells.push witdh_cell
      end
      table_summary_temp.push table_summary_row

      c_aux += 1

      witdh_cells.pop
      witdh_cells.push last_witdh_cell

      pe_member.pe_member_rels_is_evaluated_ordered_by_pe_area.each do |pe_member_rel|

        if pe_member_rel.valid_evaluator && pe_member_rel.finished
          table_summary_row = Array.new
          table_summary_row.push pe_member_rel.pe_member_evaluator.pe_area.name
          table_summary_row.push pe_member_rel.pe_member_evaluator.user.apellidos+', '+pe_member_rel.pe_member_evaluator.user.nombre
          table_summary_row.push pe_member_rel.weight
          pe_process.pe_evaluations.each do |pe_evaluation|

            pe_assessment_evaluation = pe_member_rel.pe_assessment_evaluation pe_evaluation

            if pe_assessment_evaluation
              table_summary_row.push number_with_precision(pe_assessment_evaluation.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'+(pe_assessment_evaluation.pe_dimension_group ? ' - '+pe_assessment_evaluation.pe_dimension_group.name : '' )
            else
              table_summary_row.push ''
            end



          end

          table_summary_temp.push table_summary_row

        end

      end

      table_summary.push [content: pdf.make_table(table_summary_temp, :header => false, :column_widths => witdh_cells, :cell_style => { :inline_format => true }), colspan: 5]


      if pe_process.has_step_feedback? && pe_member.step_feedback? && pe_process.rep_detailed_final_results_show_feedback_for_manager

        table_summary_row = Array.new
        table_summary_row = [content: '<b>'+position.to_s+'.3. Retroalimentación</b>', colspan: 5]
        table_summary.push table_summary_row

        tittle_gray.push c_aux

        c_aux += 1

        pe_process.pe_feedback_fields.each_with_index do |pe_feedback_field, index_feedback_field|

          table_summary_row = Array.new
          table_summary_row = [content: '<b>'+(pe_process.pe_feedback_fields.size > 1 ? (index_feedback_field+1).to_s+'. ' : '')+pe_feedback_field.name+'</b>', colspan: 5]
          table_summary.push table_summary_row

          pe_member_feedback = pe_member.pe_member_feedbacks_by_field(pe_feedback_field)

          if pe_feedback_field.simple

            retro = pe_member_feedback ? pe_member_feedback.formatted_comment  : ''
            retro.gsub! '<li>', "\n-"
            retro.gsub! '<br>', "\n"

            table_summary_row = Array.new
            table_summary_row = [content: (ActionView::Base.full_sanitizer.sanitize retro), colspan: 5]
            table_summary.push table_summary_row

          else

            table_summary_row = Array.new

            if pe_member_feedback

              table_tmp = Array.new

              pe_feedback_field.pe_feedback_compound_fields.each_with_index do |pe_feedback_compound_field, index_c|
                table_summary_row.push '<b>'+pe_feedback_compound_field.name+'</b>'
              end

              table_tmp.push table_summary_row

              witdh_cell = 520/pe_member_feedback.number_of_compound_feedbacks

              witdh_cells = Array.new

              last_witdh_cell = 520-(witdh_cell*(pe_member_feedback.number_of_compound_feedbacks-1))

              (1..pe_member_feedback.number_of_compound_feedbacks).each do |number_of_compound_feedback|

                table_summary_row = Array.new

                pe_feedback_field.pe_feedback_compound_fields.each_with_index do |pe_feedback_compound_field, index_c|

                  pe_member_compound_feedback = pe_member_feedback.pe_member_compound_feedbacks_by_field(number_of_compound_feedback, pe_feedback_compound_field)

                  retro = pe_member_compound_feedback ? pe_member_compound_feedback.formatted_comment  : ''
                  retro.gsub! '<li>', "\n-"
                  retro.gsub! '<br>', "\n"

                  table_summary_row.push retro

                end

                table_tmp.push table_summary_row

                witdh_cells.push witdh_cell

              end

              witdh_cells.pop
              witdh_cells.push last_witdh_cell

              table_summary.push [content: pdf.make_table(table_tmp, :column_widths => witdh_cells, :cell_style => { :inline_format => true }), colspan: 5]


            end

          end

        end

      end

      pdf.table(table_summary, :header => false, :column_widths => [150,100,50,110,110], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'

        tittle_gray.each do |tg|

          table.column(0).row(tg).background_color = 'dddddd'

        end

        table.column(2).style :align => :center
        table.column(3).style :align => :center
        table.column(4).style :align => :center

      end

      pdf.start_new_page


    end

    def rep_evaluadores_rels(pdf, pe_member, pe_process, position)

      pdf.move_down 20

      pe_rel = pe_process.pe_rel 'jefe'

      table_header = Array.new
      table_header_row = Array.new

      table_header_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. Evaluadores</color></b></font>', colspan: 2]
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Evaluador</b>'
      table_header_row.push '<b>Relación</b>'
      table_header.push table_header_row

      pe_process.pe_rels.each do |pe_rel|

        next if pe_rel.rel_id == 0

        pe_member.pe_member_rels_is_evaluated_as_pe_rel_ordered(pe_rel).each do |pe_member_rel|

          table_header_row = Array.new
          table_header_row.push pe_member_rel.pe_member_evaluator.user.apellidos+', '+pe_member_rel.pe_member_evaluator.user.nombre
          table_header_row.push pe_rel.alias
          table_header.push table_header_row

        end

      end




      pdf.table(table_header, :header => false, width: 520, :column_widths => [370,150], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'

      end

      pdf.move_down 20


    end

    def rep_dfr_feedback(pdf, pe_member, pe_process, show_feedback, position)

      tittle_gray = Array.new

      table_summary = Array.new
      table_summary_row = Array.new

      c_aux = 0

      table_summary_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. RETROALIMENTACIÓN</color></b></font>', colspan: 5]
      table_summary.push table_summary_row

      c_aux += 1

      if pe_member.step_feedback? && show_feedback

        table_summary_row = Array.new
        table_summary_row = [content: '<b>Retroalimentación</b>', colspan: 5]
        table_summary.push table_summary_row

        tittle_gray.push c_aux

        pe_process.pe_feedback_fields.each_with_index do |pe_feedback_field, index_feedback_field|

          table_summary_row = Array.new
          table_summary_row = [content: '<b>'+(pe_process.pe_feedback_fields.size > 1 ? (index_feedback_field+1).to_s+'. ' : '')+pe_feedback_field.name+'</b>', colspan: 5]
          table_summary.push table_summary_row

          pe_member_feedback = pe_member.pe_member_feedbacks_by_field(pe_feedback_field)

          if pe_feedback_field.simple

            c_aux += 2

            retro = pe_member_feedback ? pe_member_feedback.formatted_comment  : ''
            retro.gsub! '&nbsp;', ' '
            retro.gsub! '<li>', "\n-"
            retro.gsub! '<br>', "\n"

            table_summary_row = Array.new
            table_summary_row = [content: (ActionView::Base.full_sanitizer.sanitize retro), colspan: 5]
            table_summary.push table_summary_row

          else

            c_aux += 1

            table_summary_row = Array.new

            if pe_member_feedback

              table_tmp = Array.new

              pe_feedback_field.pe_feedback_compound_fields.each_with_index do |pe_feedback_compound_field, index_c|
                table_summary_row.push '<b>'+pe_feedback_compound_field.name+'</b>'
              end

              table_tmp.push table_summary_row

              width_cell = 520/pe_feedback_field.pe_feedback_compound_fields.size

              width_cells = Array.new

              last_width_cell = 520-(width_cell*(pe_feedback_field.pe_feedback_compound_fields.size-1))

              (1..pe_member_feedback.number_of_compound_feedbacks).each do |number_of_compound_feedback|

                c_aux += 1

                table_summary_row = Array.new

                pe_feedback_field.pe_feedback_compound_fields.each_with_index do |pe_feedback_compound_field, index_c|

                  pe_member_compound_feedback = pe_member_feedback.pe_member_compound_feedbacks_by_field(number_of_compound_feedback, pe_feedback_compound_field)

                  retro = pe_member_compound_feedback ? pe_member_compound_feedback.formatted_comment  : ''
                  retro.gsub! '&nbsp;', ' '
                  retro.gsub! '<li>', "\n-"
                  retro.gsub! '<br>', "\n"

                  table_summary_row.push retro

                end

                table_tmp.push table_summary_row

              end

              pe_feedback_field.pe_feedback_compound_fields.each do |pe_feedback_compound_field|
                width_cells.push width_cell
              end
              width_cells.pop
              width_cells.push last_width_cell

              table_summary.push [content: pdf.make_table(table_tmp, :column_widths => width_cells, :cell_style => { :inline_format => true }), colspan: 5]


            end

          end

        end

        if pe_member.step_feedback_accepted? && pe_member.pe_member_accepted_feedbacks.size > 0

          table_summary_row = Array.new
          table_summary_row = [content: '<b>Confirmación de la retroalimentación</b>', colspan: 5]
          table_summary.push table_summary_row

          c_aux += 1

          tittle_gray.push c_aux

          pe_process.pe_feedback_accepted_fields.where('is_survey = ?', false).each_with_index do |pe_feedback_accepted_field, index_feedback_field|

            table_summary_row = Array.new
            table_summary_row = [content: '<b>'+(pe_process.pe_feedback_accepted_fields.size > 1 ? (index_feedback_field+1).to_s+'. ' : '')+pe_feedback_accepted_field.name+'</b>', colspan: 5]
            table_summary.push table_summary_row

            pe_member_accepted_feedback = pe_member.pe_member_accepted_feedbacks_by_field(pe_feedback_accepted_field)

            if pe_feedback_accepted_field.simple

              retro = pe_member_accepted_feedback ? pe_member_accepted_feedback.formatted_comment  : ''
              retro.gsub! '&nbsp;', ' '
              retro.gsub! '<li>', "\n-"
              retro.gsub! '<br>', "\n"

              table_summary_row = Array.new
              table_summary_row = [content: (ActionView::Base.full_sanitizer.sanitize retro), colspan: 5]
              table_summary.push table_summary_row

            end

          end


        end

      end

      pdf.table(table_summary, :header => false, :column_widths => [150,100,50,110,110], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'

        tittle_gray.each do |tg|

          table.column(0).row(tg).background_color = 'dddddd'

        end

        table.column(2).style :align => :center
        table.column(3).style :align => :center
        table.column(4).style :align => :center

      end



    end

    def rep_dfr_exp_text(pdf, exp_text)

      pdf.move_down 20

      table_summary = Array.new
      table_summary_row = Array.new


      table_summary_row.push exp_text.gsub('&nbsp;', ' ')
      table_summary.push table_summary_row

      pdf.table(table_summary, :header => false, :cell_style => { :inline_format => true }) do |table|
        table.column(0).row(0).border_width = 0
      end



    end

    def rep_dfr_boxes_2d(pdf, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'

      dimension_eje_x = pe_process.dimension_x
      dimension_eje_y = pe_process.dimension_y

      content_dimension_y = '<strong>'+dimension_eje_y.name+'</strong><br>'

      dimension_eje_y.pe_evaluations.each do |pe_evaluation|
        content_dimension_y += '- '+pe_evaluation.name+'('+pe_evaluation.weight.to_s+')<br>'
      end

      content_dimension_x = '<strong>'+dimension_eje_x.name+'</strong><br>'

      dimension_eje_x.pe_evaluations.each do |pe_evaluation|
        content_dimension_x += '- '+pe_evaluation.name+'('+pe_evaluation.weight.to_s+')<br>'
      end


      table_final = Array.new

      colors = Array.new(dimension_eje_y.pe_dimension_groups.size+2){Array.new(dimension_eje_x.pe_dimension_groups.size+2)}
      background_colors = Array.new(dimension_eje_y.pe_dimension_groups.size+2){Array.new(dimension_eje_x.pe_dimension_groups.size+2)}
      strongs = Array.new(dimension_eje_y.pe_dimension_groups.size+2){Array.new(dimension_eje_x.pe_dimension_groups.size+2)}

      box_x = nil
      box_y = nil

      dimension_eje_y.pe_dimension_groups.reorder('position DESC').each_with_index do |pe_dimension_group_y, index_r|

        row_1 = Array.new

        row_1[0] = {:content=> content_dimension_y, :rowspan => dimension_eje_y.pe_dimension_groups.size+2} if index_r == 0

        content_pe_dimension_group_y = pe_dimension_group_y.name+'<br>'
        content_pe_dimension_group_y += CGI::escapeHTML(pe_dimension_group_y.min_sign)+' '+pe_dimension_group_y.min_percentage.to_s+' '+CGI::escapeHTML(pe_dimension_group_y.max_sign)+' '+pe_dimension_group_y.max_percentage.to_s

        row_1.push content_pe_dimension_group_y
        colors[index_r][1] = pe_dimension_group_y.text_color
        strongs[index_r][1] = true

        dimension_eje_x.pe_dimension_groups.each_with_index do |pe_dimension_group_x, index_c|

          pe_box = pe_process.pe_box_2d(pe_dimension_group_x.id, pe_dimension_group_y.id)

          if pe_box

            colors[index_r][index_c+2] = pe_member.pe_box == pe_box ? pe_box.text_color : pe_box.soft_text_color
            background_colors[index_r][index_c+2] = pe_member.pe_box == pe_box ? pe_box.bg_color : pe_box.soft_bg_color
            strongs[index_r][index_c+2] = true if pe_member.pe_box == pe_box

            box_y = index_r if pe_member.pe_box == pe_box
            box_x = index_c+2 if pe_member.pe_box == pe_box

            content_box = pe_box.short_name+'<br>'+pe_box.name

            row_1.push content_box

          else
            row_1.push ''
          end

        end

        table_final.push row_1

      end

      row_1 = Array.new

      row_1.push ''

      dimension_eje_x.pe_dimension_groups.each_with_index do |pe_dimension_group_x, index_c|

        content_pe_dimension_group_x = pe_dimension_group_x.name+'<br>'
        content_pe_dimension_group_x += CGI::escapeHTML(pe_dimension_group_x.min_sign)+' '+pe_dimension_group_x.min_percentage.to_s+' '+CGI::escapeHTML(pe_dimension_group_x.max_sign)+' '+pe_dimension_group_x.max_percentage.to_s

        row_1.push content_pe_dimension_group_x

        colors[dimension_eje_y.pe_dimension_groups.size][index_c+2] = pe_dimension_group_x.text_color
        strongs[dimension_eje_y.pe_dimension_groups.size][index_c+2] = true

      end

      table_final.push row_1

      row_1 = Array.new
      row_1.push ''
      row_1[1] = {:content=> content_dimension_x, :colspan => dimension_eje_x.pe_dimension_groups.size}

      table_final.push row_1

      pdf.table(table_final, :cell_style => { :inline_format => true }) do |table|
        (0..dimension_eje_y.pe_dimension_groups.size+1).each do |index_r|
          (0..dimension_eje_x.pe_dimension_groups.size+1).each do |index_c|
            table.column(index_c).row(index_r).border_width = 0.5
            table.column(index_c).row(index_r).text_color = colors[index_r][index_c] if colors[index_r][index_c]
            table.column(index_c).row(index_r).background_color = background_colors[index_r][index_c] if background_colors[index_r][index_c]
            table.column(index_c).row(index_r).font_style = :bold if strongs[index_r][index_c]


            table.column(index_c).row(index_r).align = :center

          end
        end

        table.column(box_x).row(box_y).border_width = 1 if box_y && box_x

      end


      #pdf.table(table_final, :header => false,  :column_widths => [100,100,100], :cell_style => { :inline_format => true }) do |table|

      #end

      pdf.move_down 20

    end

    def rep_dfr_everybody_1st_level(pdf, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'
      table_res = Array.new
      table_res_row = Array.new

      tittle_gray = Array.new
      title_align_right = Array.new

      c_aux = 0

      table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. EVALUACIÓN DE TODOS LOS EVALUADORES</color></b></font>', colspan: 2]
      table_res.push table_res_row

      c_aux += 1

      index_q = 0

      pe_process.pe_evaluations.where('informative = ?', false).each do |pe_evaluation|

        pe_member_evaluation = @pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

        if pe_member_evaluation.nil? || (pe_member_evaluation && pe_member_evaluation.weight > -1 )

          pe_element = pe_evaluation.pe_elements.first

          if pe_element && pe_element.assessed

            index_q += 1

            table_res_row = Array.new

            table_res_row = [content: '<b>'+position.to_s+'.'+(index_q).to_s+'. '+pe_evaluation.name+'</b>', colspan: 2]
            table_res.push table_res_row

            tittle_gray.push c_aux

            c_aux += 1

            pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element, nil).each do |pe_question|

              total_weight = 0
              total_points = 0

              if pe_evaluation.final_evaluation_operation_evaluators == 0

                #agrupando relaciones

                pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                  tmp_weight = 0
                  tmp_points = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                      if pe_assessment_evaluation

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                        if pe_assessment_question && pe_assessment_question.percentage

                          tmp_points += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                          tmp_weight += pe_member_rel_is_evaluated.weight

                        end

                      end

                    end

                  end

                  if tmp_weight > 0

                    tmp_points = tmp_points/tmp_weight.to_f

                    total_points += tmp_points*pe_evaluation_rel.weight
                    total_weight += pe_evaluation_rel.weight

                  end

                end

              elsif pe_evaluation.final_evaluation_operation_evaluators == 1

                #sin agrupar relaciones

                pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                  if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                    pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                    if pe_assessment_evaluation

                      pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                      if pe_assessment_question && pe_assessment_question.percentage

                        total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                        total_points += pe_assessment_question.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_question.percentage > 0

                      end

                    end

                  end

                end

              elsif pe_evaluation.final_evaluation_operation_evaluators == 2

                #agrupando areas

                pe_areas_evaluators = Array.new

                pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
                  pe_areas_evaluators.push pe_member_rel_is_evaluated.pe_member_evaluator.pe_area
                end

                pe_areas_evaluators.uniq!

                pe_areas_evaluators.each do |pe_area_evaluator|

                  tmp_weight = 0
                  tmp_points = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_area(pe_area_evaluator).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                      if pe_assessment_evaluation

                        pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                        if pe_assessment_question && pe_assessment_question.percentage

                          tmp_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                          tmp_points += pe_assessment_evaluation.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.percentage > 0

                        end

                      end

                    end

                  end

                  if tmp_weight > 0

                    tmp_points = tmp_points/tmp_weight.to_f

                    total_points += tmp_points*1
                    total_weight += 1

                  end

                end

              end

              if total_weight > 0
                res = total_points/total_weight.to_f

                res = pe_evaluation.min_percentage if res < pe_evaluation.min_percentage
                res = pe_evaluation.max_percentage if res > pe_evaluation.max_percentage

                table_res_row = Array.new

                table_res_row.push (pe_question.description)
                table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                table_res.push table_res_row

                c_aux += 1

              end

            end

            pe_assessment_final_evaluation = pe_member.pe_assessment_final_evaluation pe_evaluation

            if pe_process.of_persons? && pe_assessment_final_evaluation

              table_res_row = Array.new

              table_res_row.push '<b>Resultado</b>'
              table_res_row.push '<b>'+number_with_precision(pe_assessment_final_evaluation.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'+'</b>'
              table_res.push table_res_row

              title_align_right.push c_aux

              c_aux += 1

            end

            table_res_row = [content: '<b>'+position.to_s+'.'+(index_q).to_s+'.1. Resultado de '+pe_evaluation.name.downcase+' por tipo de evaluador</b>', colspan: 2]
            table_res.push table_res_row

            tittle_gray.push c_aux

            c_aux += 1

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

              if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel

                pe_rel = pe_evaluation_rel.pe_rel

                rel_alias = if pe_rel.rel_name == :jefe
                  pe_rel.alias
                elsif pe_rel.rel_name == :auto
                  pe_rel.alias
                else
                  pe_rel.alias_plural
                end

                table_res_row = Array.new

                table_res_row.push rel_alias +(pe_process.rep_detailed_final_results_show_rel_weight? ? ' ('+number_with_precision(pe_evaluation_rel.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+')' : '')

                sum_t = 0
                num_t = 0

                pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element, nil).each do |pe_question|

                  sum = 0
                  num = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                      if pe_assessment_evaluation

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                        if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                          sum += pe_assessment_question.percentage
                          num += 1

                        end

                      end

                    end

                  end

                  if num > 0

                    sum_t += num > 0 ? (sum/num).round(2) : 0
                    num_t += 1

                  end

                end

                res = num_t > 0 ? (sum_t/num_t).round(2) : 0

                table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                table_res.push table_res_row

                c_aux += 1

              end

            end

          end

        end

      end

      pdf.table(table_res, :header => false, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'

        tittle_gray.each do |tg|

          table.column(0).row(tg).background_color = 'dddddd'

        end

        title_align_right.each do |tr|
          table.column(0).row(tr).style :align => :right
        end

        table.column(1).style :align => :center

      end

      pdf.move_down 20

    end

    def rep_dfr_summary_1st_level(pdf, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'

      table_res = Array.new
      table_res_row = Array.new

      tittle_gray = Array.new
      title_align_right = Array.new

      c_aux = 0

      table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. EVALUACIÓN DE TODOS LOS EVALUADORES</color></b></font>', colspan: 2]
      table_res.push table_res_row

      c_aux += 1

      index_q = 0

      pe_process.pe_evaluations.where('informative = ? ', false).each do |pe_evaluation|

        pe_element = pe_evaluation.pe_elements.first

        if pe_element && pe_element.assessed

          index_q += 1

          table_res_row = Array.new

          table_res_row = [content: '<b>'+position.to_s+'.'+(index_q).to_s+'. '+pe_evaluation.name+'</b>', colspan: 2]
          table_res.push table_res_row

          tittle_gray.push c_aux

          c_aux += 1

          pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element, nil).each do |pe_question|

            total_weight = 0
            total_points = 0

            if pe_evaluation.final_evaluation_operation_evaluators == 0

              #agrupando relaciones

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                tmp_weight = 0
                tmp_points = 0

                pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                  if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                    pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                    if pe_assessment_evaluation

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                      if pe_assessment_question && pe_assessment_question.percentage

                        tmp_points += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                        tmp_weight += pe_member_rel_is_evaluated.weight

                      end

                    end

                  end

                end

                if tmp_weight > 0

                  tmp_points = tmp_points/tmp_weight.to_f

                  total_points += tmp_points*pe_evaluation_rel.weight
                  total_weight += pe_evaluation_rel.weight

                end

              end

            elsif pe_evaluation.final_evaluation_operation_evaluators == 1

              #sin agrupar relaciones

              pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                  pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                  if pe_assessment_evaluation

                    pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                    if pe_assessment_question && pe_assessment_question.percentage

                      total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                      total_points += pe_assessment_question.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_question.percentage > 0

                    end

                  end

                end

              end

            elsif pe_evaluation.final_evaluation_operation_evaluators == 2

              #agrupando areas

              pe_areas_evaluators = Array.new

              pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
                pe_areas_evaluators.push pe_member_rel_is_evaluated.pe_member_evaluator.pe_area
              end

              pe_areas_evaluators.uniq!

              pe_areas_evaluators.each do |pe_area_evaluator|

                tmp_weight = 0
                tmp_points = 0

                pe_member.pe_member_rels_is_evaluated_as_pe_area(pe_area_evaluator).each do |pe_member_rel_is_evaluated|

                  if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                    pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                    if pe_assessment_evaluation

                      pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                      if pe_assessment_question && pe_assessment_question.percentage

                        tmp_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                        tmp_points += pe_assessment_evaluation.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.percentage > 0

                      end

                    end

                  end

                end

                if tmp_weight > 0

                  tmp_points = tmp_points/tmp_weight.to_f

                  total_points += tmp_points*1
                  total_weight += 1

                end

              end

            end

            if total_weight > 0
              res = total_points/total_weight.to_f

              res = pe_evaluation.min_percentage if res < pe_evaluation.min_percentage
              res = pe_evaluation.max_percentage if res > pe_evaluation.max_percentage

              table_res_row = Array.new

              table_res_row.push (pe_question.description)
              table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
              table_res.push table_res_row

              c_aux += 1

            end

          end

          pe_assessment_final_evaluation = pe_member.pe_assessment_final_evaluation pe_evaluation

          if pe_process.of_persons? && pe_assessment_final_evaluation

            table_res_row = Array.new

            table_res_row.push '<b>Resultado</b>'
            table_res_row.push '<b>'+number_with_precision(pe_assessment_final_evaluation.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'+'</b>'
            table_res.push table_res_row

            title_align_right.push c_aux

            c_aux += 1

          end

        end

      end

      pdf.table(table_res, :header => false, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'

        tittle_gray.each do |tg|

          table.column(0).row(tg).background_color = 'dddddd'

        end

        title_align_right.each do |tr|
          table.column(0).row(tr).style :align => :right
        end

        table.column(1).style :align => :center

      end

      pdf.move_down 20

    end

    def rep_dfr_evaluator_type_1st_level(pdf, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'

      table_res = Array.new
      table_res_row = Array.new

      tittle_gray = Array.new
      title_align_right = Array.new

      c_aux = 0

      table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. RESULTADO POR TIPO DE EVALUADOR</color></b></font>', colspan: 2]
      table_res.push table_res_row

      c_aux += 1

      index_q = 0

      pe_process.pe_evaluations.where('informative = ?', false).each do |pe_evaluation|

        pe_element = pe_evaluation.pe_elements.first

        if pe_element && pe_element.assessed

          index_q += 1

          table_res_row = [content: '<b>'+position.to_s+'.'+(index_q).to_s+'. Resultado de '+pe_evaluation.name.downcase+' por tipo de evaluador</b>', colspan: 2]
          table_res.push table_res_row

          tittle_gray.push c_aux

          c_aux += 1

          pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

            if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel

              pe_rel = pe_evaluation_rel.pe_rel

              rel_alias = if pe_rel.rel_name == :jefe
                            pe_rel.alias
                          elsif pe_rel.rel_name == :auto
                            pe_rel.alias
                          else
                            pe_rel.alias_plural
                          end

              table_res_row = Array.new

              table_res_row.push rel_alias +(pe_process.rep_detailed_final_results_show_rel_weight? ? ' ('+number_with_precision(pe_evaluation_rel.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+')' : '')

              sum_t = 0
              num_t = 0

              pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element, nil).each do |pe_question|

                sum = 0
                num = 0

                pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                  if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                    pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                    if pe_assessment_evaluation

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                      if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                        sum += pe_assessment_question.percentage
                        num += 1

                      end

                    end

                  end

                end

                if num > 0

                  sum_t += num > 0 ? (sum/num).round(2) : 0
                  num_t += 1

                end

              end

              res = num_t > 0 ? (sum_t/num_t).round(2) : 0

              table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
              table_res.push table_res_row

              c_aux += 1

            end

          end

        end

      end

      pdf.table(table_res, :header => false, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'

        tittle_gray.each do |tg|

          table.column(0).row(tg).background_color = 'dddddd'

        end

        title_align_right.each do |tr|
          table.column(0).row(tr).style :align => :right
        end

        table.column(1).style :align => :center

      end

      pdf.move_down 20

    end


    def rep_dfr_pe_rel_1st_level(pdf, pe_member, pe_process, pe_rel, prev_, show_average, show_values, with_comments, position)



      num_evals = 0

      table_res = Array.new
      table_res_row = Array.new

      tittle_gray = Array.new
      title_align_right = Array.new

      c_aux = 0

      rel_alias = if pe_rel.rel_name == :jefe
                    'EVALUACIÓN DEL '+pe_rel.alias.upcase
                  elsif pe_rel.rel_name == :auto
                    pe_rel.alias.upcase
                  else
                    'EVALUACIÓN DE '+pe_rel.alias_plural.upcase
                  end

      table_res_row = Array.new
      table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+rel_alias+'</color></b></font>', colspan: 2]
      table_res.push table_res_row

      c_aux += 1

      index_q = 0

      ancho_parejo = false

      pe_process.pe_evaluations.each do |pe_evaluation|

        if pe_evaluation.has_pe_evaluation_rel pe_rel

          pe_element = pe_evaluation.pe_elements.first

          if pe_element && pe_element.assessed

            table_res_temp = Array.new

            index_q += 1

            table_res_row = Array.new

            table_res_row = [content: '<b>'+position.to_s+'.'+(index_q).to_s+'. '+pe_evaluation.name+'</b>', colspan: 2]
            table_res_temp.push table_res_row

            tittle_gray.push c_aux

            c_aux += 1

            sum_t = 0
            num_t = 0

            pe_group = nil

            if pe_evaluation.pe_groups.size > 0

              pe_member_group = pe_member.pe_member_group(pe_evaluation)

              pe_group = pe_member_group.pe_group if pe_member_group

            end

            todos = !pe_member.removed

            if todos
              pe_questions = pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member, nil, pe_group, nil)

              if pe_rel.alias_plural.downcase.include? 'jefe'

                pe_questions = pe_evaluation.pe_questions_only_by_pe_member_or_group_1st_level_ready(pe_member, pe_group)

              end

            else
              pe_questions = pe_evaluation.pe_questions_by_pe_member_1st_level(pe_member, nil, pe_group, nil)
            end

            pe_questions.each_with_index do |pe_question|

              sum = 0
              num = 0

              res_value = ''

              general_comments = Array.new

              pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel)

              pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                if pe_member_rel_is_evaluated.valid_evaluator && ( (pe_member_rel_is_evaluated.finished && todos) || !todos)

                  pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                  if pe_assessment_evaluation

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                    if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                      general_comments.push pe_assessment_question.comment unless pe_assessment_question.comment.blank?

                      sum += pe_assessment_question.percentage
                      num += 1

                      res_value = pe_assessment_question.pe_alternative.description if pe_assessment_question && pe_assessment_question.pe_alternative



                    end
                  elsif !todos
                    sum = -1
                    num = 1
                  end
                end

              end

              if num > 0

                res = num > 0 ? (sum/num).round(1) : 0

                sum_t += res*pe_question.weight
                num_t += pe_question.weight

                table_res_row = Array.new

                #d = ActionView::Base.full_sanitizer.sanitize(pe_question.description)
                d = pe_question.description.gsub('<br>',"\n")
                d = ActionView::Base.full_sanitizer.sanitize(d)

                if general_comments.size > 0
                  d += '<br>'
                  general_comments.each do |c|
                    d += '<br>'
                    d += '<strong>Comentario '+pe_rel.alias+':</strong> '+c
                  end
                end

                #individual_comments = pe_question.pe_question_comments_by_pe_member_rels(pe_member_rels_is_evaluated)

                #if individual_comments.size > 0
                #  d += '<br><br>'
                #  d += '<strong>Comentarios:</strong><br>'

                #  individual_comments.each do |pe_question_comment|
                #    d += '- '+pe_question_comment.comment+'<br>'
                #  end
                #end

                table_res_row.push (d)

                if show_values

                  d = res_value.gsub('<br>',"\n")
                  d = ActionView::Base.full_sanitizer.sanitize(d)

                  table_res_row.push d

                  ancho_parejo = true if res_value.size > 250

                else

                  if sum >= 0
                    table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                  else
                    table_res_row.push ''
                  end

                end

                table_res_temp.push table_res_row

                c_aux += 1

              end

            end

            if num_t > 0

              num_evals += 1

              table_res_temp.each do |r|

                table_res.push r

              end

              if pe_process.of_persons?

                if show_average

                  res = num_t > 0 ? (sum_t/num_t).round(2) : 0

                  table_res_row = Array.new

                  table_res_row.push '<b>Promedio</b>'
                  if res >= 0
                    table_res_row.push '<b>'+number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'+'</b>'
                  else
                    table_res_row.push ''
                  end

                  table_res.push table_res_row

                  title_align_right.push c_aux

                  c_aux += 1

                end

              end

            else

              c_aux -= 1
              tittle_gray.pop
              index_q -= 1

            end

          end

        end

      end

      if num_evals > 0

        unless prev_ == ''

          pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
          @orientation = 'V'

        end

        cw = [400,120]
        cw = [260,260] if ancho_parejo

        pdf.table(table_res, :header => false, :column_widths => cw, :cell_style => { :inline_format => true }) do |table|

          table.column(0).row(0).background_color = '333333'
          table.column(0).row(1).background_color = 'dddddd'

          tittle_gray.each do |tg|

            table.column(0).row(tg).background_color = 'dddddd'

          end

          title_align_right.each do |tr|
            table.column(0).row(tr).style :align => :right
          end

          table.column(1).style :align => :center unless show_values

        end

        pdf.move_down 20

        return position+1

      else

        return position

      end

    end


    def rep_dfr_pe_rel_1st_level_comments(pdf, pe_member, pe_process, pe_rel, prev_, position)

      unless prev_ == ''

        pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
        @orientation = 'V'

      end

      num_evals = 0

      table_res = Array.new
      table_res_row = Array.new

      tittle_gray = Array.new
      title_align_right = Array.new

      c_aux = 0

      rel_alias = if pe_rel.rel_name == :jefe
                    'EVALUACIÓN DEL '+pe_rel.alias.upcase
                  elsif pe_rel.rel_name == :auto
                    pe_rel.alias.upcase
                  else
                    'EVALUACIÓN DE TODOS LOS '+pe_rel.alias_plural.upcase
                  end

      table_res_row = Array.new
      table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+rel_alias+'</color></b></font>', colspan: 2]
      table_res.push table_res_row

      c_aux += 1

      index_q = 0

      pe_process.pe_evaluations.each do |pe_evaluation|

        if pe_evaluation.has_pe_evaluation_rel pe_rel

          pe_element = pe_evaluation.pe_elements.first

          if pe_element && pe_element.assessed

            table_res_temp = Array.new

            index_q += 1

            table_res_row = Array.new

            table_res_row = [content: '<b>'+position.to_s+'.'+(index_q).to_s+'. '+pe_evaluation.name+'</b>', colspan: 2]
            table_res_temp.push table_res_row

            tittle_gray.push c_aux

            c_aux += 1

            sum_t = 0
            num_t = 0

            pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element, nil).each do |pe_question|

              sum = 0
              num = 0

              pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                  pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                  if pe_assessment_evaluation

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                    if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                      sum += pe_assessment_question.percentage
                      num += 1

                    end

                  end
                end

              end

              if num > 0

                res = num > 0 ? (sum/num).round(1) : 0

                sum_t += res*pe_question.weight
                num_t += pe_question.weight

                table_res_row = Array.new

                table_res_row.push (pe_question.description)
                table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                table_res_temp.push table_res_row

                c_aux += 1

              end

            end

            if num_t > 0

              num_evals += 1

              table_res_temp.each do |r|

                table_res.push r

              end

              if pe_process.of_persons?

                res = num_t > 0 ? (sum_t/num_t).round(2) : 0

                table_res_row = Array.new

                table_res_row.push '<b>Promedio</b>'
                table_res_row.push '<b>'+number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'+'</b>'
                table_res.push table_res_row

                title_align_right.push c_aux

                c_aux += 1

              end

            else

              c_aux -= 1
              tittle_gray.pop
              index_q -= 1

            end

          end

        end

      end

      if num_evals > 0

        pdf.table(table_res, :header => false, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|

          table.column(0).row(0).background_color = '333333'
          table.column(0).row(1).background_color = 'dddddd'

          tittle_gray.each do |tg|

            table.column(0).row(tg).background_color = 'dddddd'

          end

          title_align_right.each do |tr|
            table.column(0).row(tr).style :align => :right
          end

          table.column(1).style :align => :center

        end

        pdf.move_down 20

        return position+1

      else

        return position

      end

    end

    def rep_dfr_pe_rel_1st_level_auto(pdf, pe_member, pe_process, pe_rel, prev_, with_comments, position)

      unless prev_ == ''

        pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
        @orientation = 'V'

      end

      num_evals = 0



      rel_alias = if pe_rel.rel_name == :jefe
                    'EVALUACIÓN DEL '+pe_rel.alias.upcase
                  elsif pe_rel.rel_name == :auto
                    pe_rel.alias.upcase
                  else
                    'EVALUACIÓN DE TODOS LOS '+pe_rel.alias_plural.upcase
                  end

      #table_res_row = Array.new
      #table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+rel_alias+'</color></b></font>', colspan: 2]
      #table_res.push table_res_row

      #c_aux += 1

      index_q = 0

      pe_process.pe_evaluations.each do |pe_evaluation|

        if pe_evaluation.has_pe_evaluation_rel pe_rel

          table_res = Array.new
          table_res_row = Array.new

          tittle_gray = Array.new
          title_align_right = Array.new

          c_aux = 0

          pe_element = pe_evaluation.pe_elements.first

          if pe_element && pe_element.assessed

            table_res_temp = Array.new

            index_q += 1

            table_res_row = Array.new

            table_res_row = [content: '<b>'+position.to_s+'. '+pe_evaluation.name+'</b>', colspan: 2]
            table_res_temp.push table_res_row

            tittle_gray.push c_aux

            c_aux += 1

            sum_t = 0
            num_t = 0



            pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element, nil).each do |pe_question|

              sum = 0
              num = 0

              general_comments = Array.new

              pe_member_rels_is_evaluated = @pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel)

              pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                  pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                  if pe_assessment_evaluation

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                    if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                      general_comments.push pe_assessment_question.comment unless pe_assessment_question.comment.blank?

                      sum += pe_assessment_question.percentage
                      num += 1

                    end

                  end
                end

              end

              if num > 0

                res = num > 0 ? (sum/num).round(1) : 0

                sum_t += res*pe_question.weight
                num_t += pe_question.weight

                table_res_row = Array.new

                d = ActionView::Base.full_sanitizer.sanitize(pe_question.description)

                if general_comments.size > 0
                  d += '<br>'
                  general_comments.each do |c|
                    d += '<br>'
                    d += '<strong>Comentario '+pe_rel.alias+':</strong> '+c
                  end
                end

                #individual_comments = pe_question.pe_question_comments_by_pe_member_rels(pe_member_rels_is_evaluated)

                #if individual_comments.size > 0
                #  d += '<br><br>'
                #  d += '<strong>Comentarios:</strong><br>'

                #  individual_comments.each do |pe_question_comment|
                #    d += '- '+pe_question_comment.comment+'<br>'
                #  end
                #end

                table_res_row.push (d)
                table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                table_res_temp.push table_res_row

                c_aux += 1

              else

              end

            end

            if num_t > 0

              num_evals += 1

              table_res_temp.each do |r|

                table_res.push r

              end

              if pe_process.of_persons?

                res = num_t > 0 ? (sum_t/num_t).round(2) : 0

                table_res_row = Array.new

                table_res_row.push '<b>Promedio</b>'
                table_res_row.push '<b>'+number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'+'</b>'
                table_res.push table_res_row

                title_align_right.push c_aux

                c_aux += 1

              end

            else

              num_evals += 1

              table_res_temp.each do |r|

                table_res.push r

              end

            end

            position += 1

            pdf.table(table_res, :header => false, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|

              #table.column(0).row(0).background_color = '333333'
              #table.column(0).row(1).background_color = 'dddddd'

              tittle_gray.each do |tg|

                table.column(0).row(tg).background_color = 'dddddd'

              end

              title_align_right.each do |tr|
                table.column(0).row(tr).style :align => :right
              end

              table.column(1).style :align => :center

            end

            pdf.move_down 20

          end

        end

      end

      if num_evals > 0

        return position+1

      else

        return position

      end

    end

    def rep_dfr_auto_3rd_level_goals(pdf, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      if pe_process.of_persons?
        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_ordered
      else
        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_ordered_by_pe_area
      end

      pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

        if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

          table_res = Array.new
          tittle_gray = Array.new
          tittle_gray2 = Array.new

          existen_datos = false

          pe_rel = pe_member_rel_is_evaluated.pe_rel

          if pe_rel.rel_id == 0

            pe_process.pe_evaluations.each do |pe_evaluation|

            pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

            if pe_evaluation.has_pe_evaluation_rel(pe_rel) && pe_evaluation.assessment_layout == 1 && pe_evaluation.pe_elements.size == 3

              existen_datos = true

              c_aux = 0

              if pe_rel.rel_name == :jefe

                title_aux = 'Evaluación del '+pe_rel.alias.downcase

              elsif pe_rel.rel_name == :auto

                title_aux = pe_rel.alias

              else

                title_aux = 'Evaluación de '+pe_rel.alias.downcase

              end

              table_res_row = [content: '<font size="12"><b><color rgb="000000">'+position.to_s+'. '+pe_evaluation.name.upcase+'</color></b></font>', colspan: 6]
              table_res.push table_res_row

              c_aux += 1

              if pe_assessment_evaluation

                pe_element_1st = pe_evaluation.pe_elements.first

                pe_group = nil

                if pe_evaluation.pe_groups.size > 0

                  pe_member_group = pe_member_rel_is_evaluated.pe_member_evaluated.pe_member_group(pe_evaluation)

                  pe_group = pe_member_group.pe_group if pe_member_group

                end



                pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_rel_is_evaluated.pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_group, pe_rel).each_with_index do |pe_question_1st_level|

                  if pe_element_1st.visible

                    table_res_row = Array.new
                    table_res_row.push pdf.make_cell(content: '<font size="12"><b><color rgb="000000"> '+(pe_element_1st.display_name ? pe_element_1st.name+'. ' : '')+(pe_question_1st_level.description)+'</color></b></font>', colspan: 6)
                    table_res.push table_res_row

                    c_aux += 1

                  end

                  num_1st_level = 1

                  pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level).each do |pe_question_2nd_level|


                    table_res_row = Array.new
                    table_res_row.push pdf.make_cell(content: '<b>'+num_1st_level.to_s+'</b>')
                    table_res_row.push pdf.make_cell(content: '<b>'+(pe_question_2nd_level.pe_element.display_name ? pe_question_2nd_level.pe_element.name+'. ' : '')+(pe_question_2nd_level.description)+'</b>', colspan: 5)

                    table_res.push table_res_row

                    tittle_gray.push c_aux

                    c_aux += 1

                    table_res_row = Array.new
                    table_res_row.push ''
                    table_res_row.push pe_evaluation.pe_elements.last.name
                    table_res_row.push 'Peso'
                    table_res_row.push 'Meta'
                    table_res_row.push 'Meta lograda'
                    table_res_row.push 'Logro'

                    table_res.push table_res_row

                    tittle_gray2.push c_aux

                    c_aux += 1

                    num_2nd_level = 1

                    pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level).each do |pe_question_3rd_level|

                      table_res_row = Array.new
                      table_res_row.push num_1st_level.to_s+'.'+num_2nd_level.to_s
                      table_res_row.push (pe_question_3rd_level.kpi_indicator ? 'KPI' : '')+(pe_question_3rd_level.pe_element.display_name ? (pe_question_3rd_level.pe_element.name)+'. ' : '')+((pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator) ? (pe_question_3rd_level.kpi_indicator.name) : pe_question_3rd_level.description )
                      table_res_row.push number_with_precision(pe_question_3rd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

                      if pe_question_3rd_level.indicator_type != 4
                        tmp_goal = number_with_precision(pe_question_3rd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                      else
                        if pe_question_3rd_level.indicator_discrete_goal
                          tmp_goal = pe_question_3rd_level.indicator_discrete_goal
                        else
                          tmp_goal = 'meta'
                        end
                      end

                      tmp_goal += pe_question_3rd_level.indicator_unit

                      table_res_row.push tmp_goal

                      tmp_achieved_goal = ''

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_3rd_level)

                      unless pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator

                        if pe_question_3rd_level.indicator_type == 4

                          pe_question_3rd_level.pe_question_ranks.each do  |qr|
                            tmp_achieved_goal = qr.discrete_goal if pe_assessment_question && qr.discrete_goal == pe_assessment_question.indicator_discrete_achievement
                          end

                        else
                          tmp_achieved_goal = number_with_precision(pe_assessment_question.indicator_achievement, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true) if pe_assessment_question

                        end

                      end

                      table_res_row.push tmp_achieved_goal

                      if pe_question_3rd_level.indicator_type != 4 || (pe_question_3rd_level.indicator_type == 4 && !pe_assessment_question.indicator_discrete_achievement.blank?)

                        table_res_row.push number_with_precision(pe_assessment_question.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'

                      else
                        table_res_row.push ''
                      end

                      table_res.push table_res_row

                      c_aux += 1

                      num_2nd_level += 1

                    end

                    num_1st_level += 1

                  end



                end

              end

            end

          end

          end

          column_widths = [40,400,70,95,95,70]

          if existen_datos

            pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = 'dddddd'

              tittle_gray.each do |tg|

                table.column(0).row(tg).background_color = 'dddddd'
                table.column(1).row(tg).background_color = 'dddddd'
                table.column(2).row(tg).background_color = 'dddddd'
                table.column(3).row(tg).background_color = 'dddddd'
                table.column(4).row(tg).background_color = 'dddddd'
                table.column(5).row(tg).background_color = 'dddddd'

              end

              tittle_gray2.each do |tg|

                table.column(0).row(tg).background_color = 'eeeeee'
                table.column(1).row(tg).background_color = 'eeeeee'
                table.column(2).row(tg).background_color = 'eeeeee'
                table.column(3).row(tg).background_color = 'eeeeee'
                table.column(4).row(tg).background_color = 'eeeeee'
                table.column(5).row(tg).background_color = 'eeeeee'

              end

            end

            pdf.move_down 20

            position += 1

          end

        end

      end

      return position

    end

    def rep_dfr_per_evaluator_1st_level(pdf, pe_member, pe_process, position)


      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'

      if pe_process.of_persons?
        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_ordered
      else
        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_ordered_by_pe_area
      end

      pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

        if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished


          pdf.start_new_page(:page_size => 'A4', :layout => :landscape)

          pe_rel = pe_member_rel_is_evaluated.pe_rel
      
          num_evals = 0

          table_res = Array.new
          table_res_row = Array.new

          tittle_gray = Array.new
          title_align_right = Array.new



          if pe_rel.rel_name == :auto

            rel_alias = pe_rel.alias

          else

            rel_alias = 'Evaluación de '+pe_rel.alias.downcase+': '

            unless pe_process.of_persons?

              rel_alias += pe_member_rel_is_evaluated.pe_member_evaluator.pe_area.name+' - '

            end

            rel_alias += pe_member_rel_is_evaluated.pe_member_evaluator.user.apellidos+', '+pe_member_rel_is_evaluated.pe_member_evaluator.user.nombre

          end

          index_eval = 0

          pe_process.pe_evaluations.each do |pe_evaluation|

            if pe_evaluation.has_pe_evaluation_rel pe_rel

              pe_element = pe_evaluation.pe_elements.first

              if pe_element && pe_element.assessed

                c_aux = 0

                col_span_name_eval = 1
                col_span_name_eval = pe_element.pe_element_descriptions.size unless pe_element.simple

                table_res_row = Array.new
                table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+rel_alias+'</color></b></font>']
                table_res.push table_res_row

                table_res_temp = Array.new

                index_eval += 1

                table_res_row = Array.new
                table_res_row.push  pdf.make_cell(content: '<b>'+position.to_s+'.'+(index_eval).to_s+'. '+pe_evaluation.name+'</b>', colspan: col_span_name_eval)
                table_res_row.push '<b>Resultado</b>'

                table_res_temp.push table_res_row

                tittle_gray.push c_aux

                c_aux += 1

                witdh_cells = Array.new

                if pe_element.simple

                  witdh_cells.push 600
                  witdh_cells.push 168

                else

                  table_res_row = Array.new

                  witdh_cell = 769/(pe_element.pe_element_descriptions.size+1)

                  pe_element.pe_element_descriptions.each do |pe_element_description|
                    table_res_row.push '<b>'+pe_element_description.name+'</b>'
                    witdh_cells.push witdh_cell
                  end

                  table_res_row.push ''
                  witdh_cells.push 769-(pe_element.pe_element_descriptions.size*witdh_cell)

                  table_res_temp.push table_res_row
                  tittle_gray.push c_aux

                  c_aux += 1

                end



                sum_t = 0
                num_t = 0

                pe_group = nil

                if pe_evaluation.pe_groups.size > 0

                  pe_member_group = pe_member.pe_member_group(pe_evaluation)

                  pe_group = pe_member_group.pe_group if pe_member_group

                end

                pe_evaluation.pe_questions_by_pe_member_1st_level_ready_and_not_informative(pe_member, pe_member_rel_is_evaluated.pe_member_evaluator, pe_group, pe_rel).each do |pe_question|

                  sum = 0
                  num = 0

                  pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                  if pe_assessment_evaluation

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                    if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                      sum += pe_assessment_question.percentage
                      num += 1

                    end

                  end

                  if num > 0

                    res = num > 0 ? (sum/num).round(1) : 0

                    sum_t += res
                    num_t += 1

                    table_res_row = Array.new

                    if pe_element.simple
                      table_res_row.push (pe_question.description)
                    else
                      pe_element.pe_element_descriptions.each do |pe_element_description|

                        pe_question_description = pe_question.pe_question_descriptions.where('pe_element_description_id = ?', pe_element_description.id).first

                        if pe_question_description
                          table_res_row.push (pe_question_description.description)
                        else
                          table_res_row.push ''
                        end

                      end
                    end

                    table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                    table_res_temp.push table_res_row

                    c_aux += 1

                  end

                end

                if num_t > 0

                  num_evals += 1

                  res = num_t > 0 ? (sum_t/num_t).round(1) : 0

                  table_res_row = Array.new

                  table_res_row.push  pdf.make_cell(content: '<b>Promedio</b>', colspan: col_span_name_eval)
                  table_res_row.push '<b>'+number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'+'</b>'
                  table_res_temp.push table_res_row

                  title_align_right.push c_aux

                  c_aux += 1

                  pdf.make_table(table_res_temp, :header => false, :column_widths => witdh_cells, :cell_style => { :inline_format => true }) do |table_temp|

                    tittle_gray.each do |tg|

                      table_temp.row(tg).background_color = 'dddddd'

                    end

                    title_align_right.each do |tr|
                      table_temp.column(0).row(tr).style :align => :right
                    end

                    table_res.push [table_temp]

                  end


                else

                  c_aux -= 1
                  tittle_gray.pop

                  unless pe_element.simple
                    c_aux -= 1
                    tittle_gray.pop
                  end

                end

              end

            end

          end

          if num_evals > 0

            pdf.table(table_res, :header => false, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'
              #table.column(0).row(1).background_color = 'dddddd'

              #tittle_gray.each do |tg|

              #  table.column(0).row(tg).background_color = 'dddddd'

              #end

              #title_align_right.each do |tr|
              #  table.column(0).row(tr).style :align => :right
              #end


            end

          end

          position += 1

        end

      end

      return position

    end


    def rep_dfr_per_evaluator_only_boss_1st_level(pdf, pe_member, pe_process, position)

      pe_rel_boss = pe_process.pe_rel_by_id 1

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      if pe_process.of_persons?
        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_ordered
      else
        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_ordered_by_pe_area
      end

      pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

        if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished && pe_rel_boss.id == pe_member_rel_is_evaluated.pe_rel.id

          pdf.start_new_page(:page_size => 'A4', :layout => :landscape)

          pe_rel = pe_member_rel_is_evaluated.pe_rel

          num_evals = 0

          table_res = Array.new
          table_res_row = Array.new

          tittle_gray = Array.new
          title_align_right = Array.new



          if pe_rel.rel_name == :auto

            rel_alias = pe_rel.alias

          else

            rel_alias = 'Evaluación de '+pe_rel.alias.downcase+': '

            unless pe_process.of_persons?

              rel_alias += pe_member_rel_is_evaluated.pe_member_evaluator.pe_area.name+' - '

            end

            rel_alias += pe_member_rel_is_evaluated.pe_member_evaluator.user.apellidos+', '+pe_member_rel_is_evaluated.pe_member_evaluator.user.nombre

          end

          index_eval = 0

          pe_process.pe_evaluations.each do |pe_evaluation|

            if pe_evaluation.has_pe_evaluation_rel pe_rel

              pe_element = pe_evaluation.pe_elements.first

              if pe_element && pe_element.assessed

                c_aux = 0

                col_span_name_eval = 1
                col_span_name_eval = pe_element.pe_element_descriptions.size unless pe_element.simple

                table_res_row = Array.new
                table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+rel_alias+'</color></b></font>']
                table_res.push table_res_row

                table_res_temp = Array.new

                index_eval += 1

                table_res_row = Array.new
                table_res_row.push  pdf.make_cell(content: '<b>'+position.to_s+'.'+(index_eval).to_s+'. '+pe_evaluation.name+'</b>', colspan: col_span_name_eval)
                table_res_row.push '<b>Resultado</b>'

                table_res_temp.push table_res_row

                tittle_gray.push c_aux

                c_aux += 1

                witdh_cells = Array.new

                if pe_element.simple

                  witdh_cells.push 600
                  witdh_cells.push 168

                else

                  table_res_row = Array.new

                  witdh_cell = 769/(pe_element.pe_element_descriptions.size+1)

                  pe_element.pe_element_descriptions.each do |pe_element_description|
                    table_res_row.push '<b>'+pe_element_description.name+'</b>'
                    witdh_cells.push witdh_cell
                  end

                  table_res_row.push ''
                  witdh_cells.push 769-(pe_element.pe_element_descriptions.size*witdh_cell)

                  table_res_temp.push table_res_row
                  tittle_gray.push c_aux

                  c_aux += 1

                end



                sum_t = 0
                num_t = 0

                pe_group = nil

                if pe_evaluation.pe_groups.size > 0

                  pe_member_group = pe_member.pe_member_group(pe_evaluation)

                  pe_group = pe_member_group.pe_group if pe_member_group

                end

                pe_evaluation.pe_questions_by_pe_member_1st_level_ready_and_not_informative(pe_member, pe_member_rel_is_evaluated.pe_member_evaluator, pe_group, pe_rel).each do |pe_question|

                  sum = 0
                  num = 0

                  pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                  if pe_assessment_evaluation

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question)

                    if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                      sum += pe_assessment_question.percentage
                      num += 1

                    end

                  end

                  if num > 0

                    res = num > 0 ? (sum/num).round(1) : 0

                    sum_t += res
                    num_t += 1

                    table_res_row = Array.new

                    if pe_element.simple
                      table_res_row.push (pe_question.description)
                    else
                      pe_element.pe_element_descriptions.each do |pe_element_description|

                        pe_question_description = pe_question.pe_question_descriptions.where('pe_element_description_id = ?', pe_element_description.id).first

                        if pe_question_description
                          table_res_row.push (pe_question_description.description)
                        else
                          table_res_row.push ''
                        end

                      end
                    end

                    table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                    table_res_temp.push table_res_row

                    c_aux += 1

                  end

                end

                if num_t > 0

                  num_evals += 1

                  res = num_t > 0 ? (sum_t/num_t).round(1) : 0

                  table_res_row = Array.new

                  table_res_row.push  pdf.make_cell(content: '<b>Promedio</b>', colspan: col_span_name_eval)
                  table_res_row.push '<b>'+number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'+'</b>'
                  table_res_temp.push table_res_row

                  title_align_right.push c_aux

                  c_aux += 1

                  pdf.make_table(table_res_temp, :header => false, :column_widths => witdh_cells, :cell_style => { :inline_format => true }) do |table_temp|

                    tittle_gray.each do |tg|

                      table_temp.row(tg).background_color = 'dddddd'

                    end

                    title_align_right.each do |tr|
                      table_temp.column(0).row(tr).style :align => :right
                    end

                    table_res.push [table_temp]

                  end


                else

                  c_aux -= 1
                  tittle_gray.pop

                  unless pe_element.simple
                    c_aux -= 1
                    tittle_gray.pop
                  end

                end

              end

            end

          end

          if num_evals > 0

            pdf.table(table_res, :header => false, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'
              #table.column(0).row(1).background_color = 'dddddd'

              #tittle_gray.each do |tg|

              #  table.column(0).row(tg).background_color = 'dddddd'

              #end

              #title_align_right.each do |tr|
              #  table.column(0).row(tr).style :align => :right
              #end


            end

          end

          position += 1

        end

      end

      return position

    end

    def rep_dfr_everybody_1st_level_det(pdf, pe_member, pe_process, position, show_average)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      pe_process.pe_evaluations.each do |pe_evaluation|

        pe_member_evaluation = pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

        if pe_member_evaluation.nil? || (pe_member_evaluation && pe_member_evaluation.weight > -1 )

          pe_group = nil

          if pe_evaluation.pe_groups.size > 0

            pe_member_group = pe_member.pe_member_group(pe_evaluation)

            pe_group = pe_member_group.pe_group if pe_member_group

          end

          pe_element_1st = pe_evaluation.pe_elements.first

          if pe_element_1st && pe_element_1st.assessed

            table_res = Array.new

            tittle_gray = Array.new

            col_span = 1

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
              col_span += 1 if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
            end

            col_span += 1 if show_average

            c_aux = 0

            table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. CONSOLIDADO '+pe_evaluation.name.upcase+'</color></b></font>', colspan: col_span]
            table_res.push table_res_row

            c_aux += 1

            num_t_eval = 0

            table_res_temp = Array.new

            table_res_row = Array.new
            table_res_row.push '<b>'+pe_element_1st.name+'</b>'

            show_weight = 0

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
              show_weight +=1 if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
            end

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
              if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
                table_res_row.push '<b>'+pe_evaluation_rel.pe_rel.alias+'</b>'+(show_weight > 1 && pe_process.rep_detailed_final_results_show_rel_weight? ? ' ('+number_with_precision(pe_evaluation_rel.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+')': '')
              end
            end

            if show_average
              table_res_row.push '<b>Resultado</b>'
            end

            table_res_temp.push table_res_row

            c_aux += 1

            pe_evaluation.pe_questions_only_by_pe_member_and_group_1st_level_ready_and_not_informative(pe_member, pe_group).each do |pe_question_1st|

              sum_t = 0
              num_t = 0

              table_res_row = Array.new
              table_res_row.push pe_question_1st.description

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel

                  pe_rel = pe_evaluation_rel.pe_rel

                  sum = 0
                  num = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                      if pe_assessment_evaluation

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_1st)

                        if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0 && pe_assessment_question.points >= 0

                          sum += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                          num += pe_member_rel_is_evaluated.weight

                        end

                      end

                    end

                  end

                  if num > 0

                    res = num > 0 ? (sum/num).round(1) : 0

                    sum_t += res
                    num_t += 1

                    table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                  else

                    table_res_row.push ''

                  end

                end

              end

              if show_average

                total_weight = 0
                total_points = 0

                if pe_evaluation.final_evaluation_operation_evaluators == 0

                  #agrupando relaciones

                  pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                    tmp_weight = 0
                    tmp_points = 0

                    pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                      if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                        pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                        if pe_assessment_evaluation

                          pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_1st)

                          if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                            tmp_points += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                            tmp_weight += pe_member_rel_is_evaluated.weight

                          end

                        end

                      end

                    end

                    if tmp_weight > 0

                      tmp_points = tmp_points/tmp_weight.to_f

                      total_points += tmp_points*pe_evaluation_rel.weight
                      total_weight += pe_evaluation_rel.weight

                    end

                  end

                elsif pe_evaluation.final_evaluation_operation_evaluators == 1

                  #sin agrupar relaciones

                  pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                      if pe_assessment_evaluation

                        pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_1st)

                        if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                          total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                          total_points += pe_assessment_question.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_question.percentage > 0

                        end

                      end

                    end

                  end

                elsif pe_evaluation.final_evaluation_operation_evaluators == 2

                  #agrupando areas

                  pe_areas_evaluators = Array.new

                  pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
                    pe_areas_evaluators.push pe_member_rel_is_evaluated.pe_member_evaluator.pe_area
                  end

                  pe_areas_evaluators.uniq!

                  pe_areas_evaluators.each do |pe_area_evaluator|

                    tmp_weight = 0
                    tmp_points = 0

                    pe_member.pe_member_rels_is_evaluated_as_pe_area(pe_area_evaluator).each do |pe_member_rel_is_evaluated|

                      if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                        pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                        if pe_assessment_evaluation

                          pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                          pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_1st)

                          if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                            tmp_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                            tmp_points += pe_assessment_evaluation.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.percentage > 0

                          end

                        end

                      end

                    end

                    if tmp_weight > 0

                      tmp_points = tmp_points/tmp_weight.to_f

                      total_points += tmp_points*1
                      total_weight += 1

                    end

                  end

                end

                if total_weight > 0

                  res = total_points/total_weight.to_f
                  res = pe_evaluation.min_percentage if res < pe_evaluation.min_percentage
                  res = pe_evaluation.max_percentage if res > pe_evaluation.max_percentage

                  table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                else

                  table_res_row.push ''

                end

              end

              if num_t > 0
                table_res_temp.push table_res_row

                c_aux += 1

                num_t_eval += num_t
              end

            end

            if show_average

              table_res_row = Array.new
              table_res_row.push '<b>Resumen</b>'

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                tmp_points = 0
                tmp_weight = 0

                pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                  if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                    pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                    if pe_assessment_evaluation && pe_assessment_evaluation.percentage && pe_assessment_evaluation.percentage >= 0

                      tmp_points += pe_assessment_evaluation.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_evaluation.percentage > 0
                      tmp_weight += pe_member_rel_is_evaluated.weight

                    end

                  end

                end

                if tmp_weight > 0
                  tmp_points = tmp_points/tmp_weight.to_f
                  table_res_row.push number_with_precision(tmp_points, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
                else
                  table_res_row.push ''
                end

              end

              pe_assessment_final_evaluation = pe_member.pe_assessment_final_evaluation pe_evaluation
              if pe_assessment_final_evaluation && pe_assessment_final_evaluation.percentage >= 0
                table_res_row.push number_with_precision(pe_assessment_final_evaluation.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
              else
                table_res_row.push ''
              end


              table_res_temp.push table_res_row


            end

            if num_t_eval > 0

              table_res_temp.each do |r|
                table_res.push r
              end

            end

            column_widths = Array.new
            column_widths.push (770 - ((col_span-1)*83))
            (1..col_span-1).each do |col|
              column_widths.push 83
            end

            pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'
              table.row(1).background_color = 'dddddd'

              (1..col_span).each do |col|
                table.column(col).style :align => :center
              end

            end

            pdf.move_down 20

            position += 1

          end

        end

      end

      return position

    end

    def rep_dfr_everybody_2nd_level_det(pdf, pe_member, pe_process, position, show_average)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      pe_process.pe_evaluations.each do |pe_evaluation|

        pe_member_evaluation = pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

        if pe_member_evaluation.nil? || (pe_member_evaluation && pe_member_evaluation.weight > -1 )

          pe_group = nil

          if pe_evaluation.pe_groups.size > 0

            pe_member_group = pe_member.pe_member_group(pe_evaluation)

            pe_group = pe_member_group.pe_group if pe_member_group

          end

          pe_element_1st = pe_evaluation.pe_elements.first
          pe_element_2nd = pe_evaluation.pe_elements.second

          if pe_element_2nd && pe_element_2nd.assessed

            table_res = Array.new
            table_res_row = Array.new

            tittle_gray = Array.new

            col_span = 1

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
              if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
                col_span += 1
              end
            end

            col_span += 1 if show_average

            c_aux = 0

            table_res_row = Array.new
            table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. DETALLE DE '+pe_evaluation.name.upcase+'</color></b></font>', colspan: col_span]
            table_res.push table_res_row

            c_aux += 1

            pe_evaluation.pe_questions_only_by_pe_member_and_group_1st_level_ready_and_not_informative(pe_member, pe_group).each do |pe_question_1st|

              num_t_eval = 0

              table_res_temp = Array.new

              table_res_row = Array.new
              table_res_row = [content: '<b>'+(pe_question_1st.description)+'</b>', colspan: col_span]
              table_res_temp.push table_res_row

              tittle_gray.push c_aux

              c_aux += 1

              table_res_row = Array.new
              table_res_row.push '<b>'+pe_element_2nd.name+'</b>'

              show_weight = 0

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
                show_weight +=1 if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
              end

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
                if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
                  table_res_row.push '<b>'+pe_evaluation_rel.pe_rel.alias+'</b>'+(show_weight > 1 && pe_process.rep_detailed_final_results_show_rel_weight? ? ' ('+number_with_precision(pe_evaluation_rel.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+')': '')
                end
              end

              if show_average
                table_res_row.push '<b>Resultado</b>'
              end

              table_res_temp.push table_res_row

              c_aux += 1

              pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element_2nd, pe_question_1st).each do |pe_question_2nd|

                sum_t = 0
                num_t = 0

                table_res_row = Array.new
                table_res_row.push pe_question_2nd.description

                pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                  if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel

                    pe_rel = pe_evaluation_rel.pe_rel

                    sum = 0
                    num = 0

                    pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                      if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                        pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                        if pe_assessment_evaluation

                          pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                          if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0 && pe_assessment_question.points >= 0

                            sum += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                            num += pe_member_rel_is_evaluated.weight

                          end

                        end

                      end

                    end

                    if num > 0

                      res = num > 0 ? (sum/num).round(1) : 0

                      sum_t += res
                      num_t += 1

                      table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                    else

                      table_res_row.push ''

                    end

                  end

                end

                if show_average

                  total_weight = 0
                  total_points = 0

                  if pe_evaluation.final_evaluation_operation_evaluators == 0

                    #agrupando relaciones

                    pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                      tmp_weight = 0
                      tmp_points = 0

                      pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                        if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                          pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                          if pe_assessment_evaluation

                            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                            if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                              tmp_points += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                              tmp_weight += pe_member_rel_is_evaluated.weight

                            end

                          end

                        end

                      end

                      if tmp_weight > 0

                        tmp_points = tmp_points/tmp_weight.to_f

                        total_points += tmp_points*pe_evaluation_rel.weight
                        total_weight += pe_evaluation_rel.weight

                      end

                    end

                  elsif pe_evaluation.final_evaluation_operation_evaluators == 1

                    #sin agrupar relaciones

                    pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                      if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                        pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                        if pe_assessment_evaluation

                          pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                          pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                          if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                            total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                            total_points += pe_assessment_question.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_question.percentage > 0

                          end

                        end

                      end

                    end

                  elsif pe_evaluation.final_evaluation_operation_evaluators == 2

                    #agrupando areas

                    pe_areas_evaluators = Array.new

                    pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
                      pe_areas_evaluators.push pe_member_rel_is_evaluated.pe_member_evaluator.pe_area
                    end

                    pe_areas_evaluators.uniq!

                    pe_areas_evaluators.each do |pe_area_evaluator|

                      tmp_weight = 0
                      tmp_points = 0

                      pe_member.pe_member_rels_is_evaluated_as_pe_area(pe_area_evaluator).each do |pe_member_rel_is_evaluated|

                        if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                          pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                          if pe_assessment_evaluation

                            pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                            if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                              tmp_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                              tmp_points += pe_assessment_evaluation.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.percentage > 0

                            end

                          end

                        end

                      end

                      if tmp_weight > 0

                        tmp_points = tmp_points/tmp_weight.to_f

                        total_points += tmp_points*1
                        total_weight += 1

                      end

                    end

                  end

                  if total_weight > 0

                    res = total_points/total_weight.to_f
                    res = pe_evaluation.min_percentage if res < pe_evaluation.min_percentage
                    res = pe_evaluation.max_percentage if res > pe_evaluation.max_percentage

                    table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                  else

                    table_res_row.push ''

                  end

                end

                if num_t > 0
                  table_res_temp.push table_res_row

                  c_aux += 1

                  num_t_eval += num_t
                end

              end

              if num_t_eval > 0

                table_res_temp.each do |r|
                  table_res.push r
                end

              else

                tittle_gray.pop
                c_aux -= 2

              end

            end

            column_widths = Array.new
            column_widths.push (770 - ((col_span-1)*83))
            (1..col_span-1).each do |col|
              column_widths.push 83
            end

            pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'
              table.column(0).row(1).background_color = 'dddddd'

              tittle_gray.each do |tg|

                table.column(0).row(tg).background_color = 'dddddd'

              end

              (1..col_span).each do |col|
                table.column(col).style :align => :center
              end

            end

            pdf.move_down 20

            position += 1

          end

        end

      end

      return position

    end

    def rep_dfr_eval_everybody_2nd_level(pdf, pe_member, pe_process, position, eval_cod, show_average, show_comments)

      pe_member_rels_boss = pe_member.pe_member_rels_boss

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      pe_process.pe_evaluations.each do |pe_evaluation|

        if eval_cod == pe_evaluation.cod && pe_evaluation.pe_elements.size == 2

          pe_member_evaluation = pe_member.pe_member_evaluations.where('pe_evaluation_id = ?', pe_evaluation.id).first

          if pe_member_evaluation.nil? || (pe_member_evaluation && pe_member_evaluation.weight > -1 )

            pe_group = nil

            if pe_evaluation.pe_groups.size > 0

              pe_member_group = pe_member.pe_member_group(pe_evaluation)

              pe_group = pe_member_group.pe_group if pe_member_group

            end

            pe_element_1st = pe_evaluation.pe_elements.first
            pe_element_2nd = pe_evaluation.pe_elements.second

            if pe_element_2nd && pe_element_2nd.assessed

              table_res = Array.new
              table_res_row = Array.new

              tittle_gray = Array.new

              col_span = 1

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
                if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
                  col_span += 1
                end
              end

              col_span += 1 if show_average

              c_aux = 0

              table_res_row = Array.new
              table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. DETALLE DE '+pe_evaluation.name.upcase+'</color></b></font>', colspan: col_span]
              table_res.push table_res_row

              c_aux += 1

              pe_evaluation.pe_questions_only_by_pe_member_and_group_1st_level_ready_and_not_informative(pe_member, pe_group).each do |pe_question_1st|

                num_t_eval = 0

                table_res_temp = Array.new

                table_res_row = Array.new
                table_res_row = [content: '<b>'+(pe_question_1st.description)+'</b>', colspan: col_span]
                table_res_temp.push table_res_row

                tittle_gray.push c_aux

                c_aux += 1

                table_res_row = Array.new
                table_res_row.push '<b>'+pe_element_2nd.name+'</b>'

                show_weight = 0

                pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
                  show_weight +=1 if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
                end

                pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
                  if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
                    table_res_row.push '<b>'+pe_evaluation_rel.pe_rel.alias+'</b>'+(show_weight > 1 && pe_process.rep_detailed_final_results_show_rel_weight? ? ' ('+number_with_precision(pe_evaluation_rel.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+')': '')
                  end
                end

                if show_average
                  table_res_row.push '<b>Resultado</b>'
                end

                table_res_temp.push table_res_row

                c_aux += 1

                pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element_2nd, pe_question_1st).each do |pe_question_2nd|

                  sum_t = 0
                  num_t = 0

                  table_res_row = Array.new
                  table_res_row.push pe_question_2nd.description

                  pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                    if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel

                      pe_rel = pe_evaluation_rel.pe_rel

                      sum = 0
                      num = 0

                      pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                        if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                          pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                          if pe_assessment_evaluation

                            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                            if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0 && pe_assessment_question.points >= 0

                              sum += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                              num += pe_member_rel_is_evaluated.weight

                            end

                          end

                        end

                      end

                      if num > 0

                        res = num > 0 ? (sum/num).round(1) : 0

                        sum_t += res
                        num_t += 1

                        table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                      else

                        table_res_row.push ''

                      end

                    end

                  end

                  if show_average

                    total_weight = 0
                    total_points = 0

                    if pe_evaluation.final_evaluation_operation_evaluators == 0

                      #agrupando relaciones

                      pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                        tmp_weight = 0
                        tmp_points = 0

                        pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                          if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                            pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                            if pe_assessment_evaluation

                              pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                              if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                                tmp_points += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                                tmp_weight += pe_member_rel_is_evaluated.weight

                              end

                            end

                          end

                        end

                        if tmp_weight > 0

                          tmp_points = tmp_points/tmp_weight.to_f

                          total_points += tmp_points*pe_evaluation_rel.weight
                          total_weight += pe_evaluation_rel.weight

                        end

                      end

                    elsif pe_evaluation.final_evaluation_operation_evaluators == 1

                      #sin agrupar relaciones

                      pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                        if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                          pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                          if pe_assessment_evaluation

                            pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                            pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                            if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                              total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                              total_points += pe_assessment_question.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_question.percentage > 0

                            end

                          end

                        end

                      end

                    elsif pe_evaluation.final_evaluation_operation_evaluators == 2

                      #agrupando areas

                      pe_areas_evaluators = Array.new

                      pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
                        pe_areas_evaluators.push pe_member_rel_is_evaluated.pe_member_evaluator.pe_area
                      end

                      pe_areas_evaluators.uniq!

                      pe_areas_evaluators.each do |pe_area_evaluator|

                        tmp_weight = 0
                        tmp_points = 0

                        pe_member.pe_member_rels_is_evaluated_as_pe_area(pe_area_evaluator).each do |pe_member_rel_is_evaluated|

                          if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                            pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                            if pe_assessment_evaluation

                              pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                              pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                              if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                                tmp_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                                tmp_points += pe_assessment_evaluation.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.percentage > 0

                              end

                            end

                          end

                        end

                        if tmp_weight > 0

                          tmp_points = tmp_points/tmp_weight.to_f

                          total_points += tmp_points*1
                          total_weight += 1

                        end

                      end

                    end

                    if total_weight > 0

                      res = total_points/total_weight.to_f
                      res = pe_evaluation.min_percentage if res < pe_evaluation.min_percentage
                      res = pe_evaluation.max_percentage if res > pe_evaluation.max_percentage

                      table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                    else

                      table_res_row.push ''

                    end

                  end

                  if num_t > 0
                    table_res_temp.push table_res_row

                    c_aux += 1

                    num_t_eval += num_t
                  end

                end

                if num_t_eval > 0

                  table_res_temp.each do |r|
                    table_res.push r
                  end

                else

                  tittle_gray.pop
                  c_aux -= 2

                end

              end

              column_widths = Array.new
              column_widths.push (770 - ((col_span-1)*83))
              (1..col_span-1).each do |col|
                column_widths.push 83
              end

              pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

                table.column(0).row(0).background_color = '333333'
                table.column(0).row(1).background_color = 'dddddd'

                tittle_gray.each do |tg|

                  table.column(0).row(tg).background_color = 'dddddd'

                end

                (1..col_span).each do |col|
                  table.column(col).style :align => :center
                end

              end

              pdf.move_down 20

              position += 1

            end

          end

        end

      end

      return position

    end

    def rep_dfr_jefe_2nd_level_det(pdf, pe_member, pe_process, position, show_average, show_values)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      pe_process.pe_evaluations.each do |pe_evaluation|

        pe_group = nil

        if pe_evaluation.pe_groups.size > 0

          pe_member_group = pe_member.pe_member_group(pe_evaluation)

          pe_group = pe_member_group.pe_group if pe_member_group

        end

        pe_element_1st = pe_evaluation.pe_elements.first
        pe_element_2nd = pe_evaluation.pe_elements.second

        if pe_element_2nd && pe_element_2nd.assessed

          table_res = Array.new
          table_res_row = Array.new

          tittle_gray = Array.new

          col_span = 1

          pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
            if pe_evaluation_rel.pe_rel.rel_id == 1 && pe_member.is_evaluated_as_pe_rel?(pe_evaluation_rel.pe_rel)
              col_span += 1
            end
          end

          col_span += 1 if show_average

          c_aux = 0

          table_res_row = Array.new
          table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. DETALLE DE '+pe_evaluation.name.upcase+'</color></b></font>', colspan: col_span]
          table_res.push table_res_row

          c_aux += 1

          pe_evaluation.pe_questions_only_by_pe_member_and_group_1st_level_ready_and_not_informative(pe_member, pe_group).each do |pe_question_1st|

            num_t_eval = 0

            table_res_temp = Array.new

            table_res_row = Array.new
            table_res_row = [content: '<b>'+(pe_question_1st.description)+'</b>', colspan: col_span]
            table_res_temp.push table_res_row

            tittle_gray.push c_aux

            c_aux += 1

            table_res_row = Array.new
            table_res_row.push '<b>'+pe_element_2nd.name+'</b>'

            show_weight = 0

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
              show_weight +=1 if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel
            end

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
              if pe_evaluation_rel.pe_rel.rel_id == 1 && pe_member.is_evaluated_as_pe_rel?(pe_evaluation_rel.pe_rel)
                table_res_row.push '<b>'+pe_evaluation_rel.pe_rel.alias+'</b>'+(show_weight > 1 && pe_process.rep_detailed_final_results_show_rel_weight? ? ' ('+number_with_precision(pe_evaluation_rel.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+')': '')
              end
            end

            if show_average
              table_res_row.push '<b>Resultado</b>'
            end

            table_res_temp.push table_res_row

            c_aux += 1

            pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element_2nd, pe_question_1st).each do |pe_question_2nd|

              sum_t = 0
              num_t = 0

              table_res_row = Array.new
              table_res_row.push pe_question_2nd.description

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                if pe_evaluation_rel.pe_rel.rel_id == 1 && pe_member.is_evaluated_as_pe_rel?(pe_evaluation_rel.pe_rel)

                  pe_rel = pe_evaluation_rel.pe_rel

                  sum = 0
                  num = 0

                  res_value = ''

                  pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                      if pe_assessment_evaluation

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                        if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0 && pe_assessment_question.points >= 0

                          sum += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                          num += pe_member_rel_is_evaluated.weight

                          res_value = pe_assessment_question.pe_alternative.description

                        end

                      end

                    end

                  end

                  if num > 0

                    res = num > 0 ? (sum/num).round(1) : 0

                    sum_t += res
                    num_t += 1

                    if show_values

                      table_res_row.push res_value

                    else

                      table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                    end

                  else

                    table_res_row.push ''

                  end

                end

              end

              if show_average

                total_weight = 0
                total_points = 0

                if pe_evaluation.final_evaluation_operation_evaluators == 0

                  #agrupando relaciones

                  pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                    tmp_weight = 0
                    tmp_points = 0

                    pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                      if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                        pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                        if pe_assessment_evaluation

                          pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                          if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                            tmp_points += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                            tmp_weight += pe_member_rel_is_evaluated.weight

                          end

                        end

                      end

                    end

                    if tmp_weight > 0

                      tmp_points = tmp_points/tmp_weight.to_f

                      total_points += tmp_points*pe_evaluation_rel.weight
                      total_weight += pe_evaluation_rel.weight

                    end

                  end

                elsif pe_evaluation.final_evaluation_operation_evaluators == 1

                  #sin agrupar relaciones

                  pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                      if pe_assessment_evaluation

                        pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                        if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                          total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                          total_points += pe_assessment_question.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_question.percentage > 0

                        end

                      end

                    end

                  end

                elsif pe_evaluation.final_evaluation_operation_evaluators == 2

                  #agrupando areas

                  pe_areas_evaluators = Array.new

                  pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
                    pe_areas_evaluators.push pe_member_rel_is_evaluated.pe_member_evaluator.pe_area
                  end

                  pe_areas_evaluators.uniq!

                  pe_areas_evaluators.each do |pe_area_evaluator|

                    tmp_weight = 0
                    tmp_points = 0

                    pe_member.pe_member_rels_is_evaluated_as_pe_area(pe_area_evaluator).each do |pe_member_rel_is_evaluated|

                      if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                        pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                        if pe_assessment_evaluation

                          pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                          pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                          if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.points >= 0

                            tmp_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                            tmp_points += pe_assessment_evaluation.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.percentage > 0

                          end

                        end

                      end

                    end

                    if tmp_weight > 0

                      tmp_points = tmp_points/tmp_weight.to_f

                      total_points += tmp_points*1
                      total_weight += 1

                    end

                  end

                end

                if total_weight > 0

                  res = total_points/total_weight.to_f
                  res = pe_evaluation.min_percentage if res < pe_evaluation.min_percentage
                  res = pe_evaluation.max_percentage if res > pe_evaluation.max_percentage

                  table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                else

                  table_res_row.push ''

                end

              end

              if num_t > 0
                table_res_temp.push table_res_row

                c_aux += 1

                num_t_eval += num_t
              end

            end

            if num_t_eval > 0

              table_res_temp.each do |r|
                table_res.push r
              end

            else

              tittle_gray.pop
              c_aux -= 2

            end

          end

          column_widths = Array.new
          column_widths.push (770 - ((col_span-1)*83))
          (1..col_span-1).each do |col|
            column_widths.push 83
          end

          pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

            table.column(0).row(0).background_color = '333333'
            table.column(0).row(1).background_color = 'dddddd'

            tittle_gray.each do |tg|

              table.column(0).row(tg).background_color = 'dddddd'

            end

            (1..col_span).each do |col|
              table.column(col).style :align => :center
            end

          end

          pdf.move_down 20

          position += 1

        end

      end

      return position

    end

    def rep_dfr_final_results_2nd_level_det(pdf, pe_member, pe_process, position, show_average)

    pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
    @orientation = 'H'

    pe_process.pe_evaluations.each do |pe_evaluation|

      pe_element_1st = pe_evaluation.pe_elements.first
      pe_element_2nd = pe_evaluation.pe_elements.second

      if pe_element_2nd && pe_element_2nd.assessed

        table_res = Array.new
        table_res_row = Array.new

        tittle_gray = Array.new

        col_span = 2

        c_aux = 0

        table_res_row = Array.new
        table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. DETALLE DE '+pe_evaluation.name.upcase+'</color></b></font>', colspan: col_span]
        table_res.push table_res_row

        c_aux += 1

        pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element_1st, nil).each do |pe_question_1st|

          num_t_eval = 0

          table_res_temp = Array.new

          table_res_row = Array.new
          table_res_row = [content: '<b>'+(pe_question_1st.description)+'</b>', colspan: col_span]
          table_res_temp.push table_res_row

          tittle_gray.push c_aux

          c_aux += 1

          table_res_row = Array.new
          table_res_row.push '<b>'+pe_element_2nd.name+'</b>'

          table_res_row.push '<b>Resultado</b>'

          table_res_temp.push table_res_row

          c_aux += 1

          pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element_2nd, pe_question_1st).each do |pe_question_2nd|

            sum_t = 0
            num_t = 0

            table_res_row = Array.new
            table_res_row.push (pe_question_2nd.description)

            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

              if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel

                pe_rel = pe_evaluation_rel.pe_rel

                sum = 0
                num = 0

                pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                  if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                    pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                    if pe_assessment_evaluation

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                      if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                        sum += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                        num += pe_member_rel_is_evaluated.weight

                      end

                    end

                  end

                end

                if num > 0

                  res = num > 0 ? (sum/num).round(1) : 0

                  sum_t += res
                  num_t += 1

                end

              end

            end



              total_weight = 0
              total_points = 0

              if pe_evaluation.final_evaluation_operation_evaluators == 0

                #agrupando relaciones

                pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                  tmp_weight = 0
                  tmp_points = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_evaluation_rel.pe_rel).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                      if pe_assessment_evaluation

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                        if pe_assessment_question && pe_assessment_question.percentage

                          tmp_points += pe_assessment_question.percentage*pe_member_rel_is_evaluated.weight if pe_assessment_question.percentage > 0
                          tmp_weight += pe_member_rel_is_evaluated.weight

                        end

                      end

                    end

                  end

                  if tmp_weight > 0

                    tmp_points = tmp_points/tmp_weight.to_f

                    total_points += tmp_points*pe_evaluation_rel.weight
                    total_weight += pe_evaluation_rel.weight

                  end

                end

              elsif pe_evaluation.final_evaluation_operation_evaluators == 1

                #sin agrupar relaciones

                pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

                  if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                    pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                    if pe_assessment_evaluation

                      pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                      if pe_assessment_question && pe_assessment_question.percentage

                        total_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                        total_points += pe_assessment_question.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_question.percentage > 0

                      end

                    end

                  end

                end

              elsif pe_evaluation.final_evaluation_operation_evaluators == 2

                #agrupando areas

                pe_areas_evaluators = Array.new

                pe_member.pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|
                  pe_areas_evaluators.push pe_member_rel_is_evaluated.pe_member_evaluator.pe_area
                end

                pe_areas_evaluators.uniq!

                pe_areas_evaluators.each do |pe_area_evaluator|

                  tmp_weight = 0
                  tmp_points = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_area(pe_area_evaluator).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

                      if pe_assessment_evaluation

                        pe_evaluation_rel = pe_assessment_evaluation.pe_evaluation.pe_evaluation_rel pe_assessment_evaluation.pe_member_rel.pe_rel

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                        if pe_assessment_question && pe_assessment_question.percentage

                          tmp_weight += pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight

                          tmp_points += pe_assessment_evaluation.percentage*(pe_evaluation_rel.weight*pe_member_rel_is_evaluated.weight) if pe_assessment_evaluation.percentage > 0

                        end

                      end

                    end

                  end

                  if tmp_weight > 0

                    tmp_points = tmp_points/tmp_weight.to_f

                    total_points += tmp_points*1
                    total_weight += 1

                  end

                end

              end

              if total_weight > 0

                res = total_points/total_weight.to_f
                res = pe_evaluation.min_percentage if res < pe_evaluation.min_percentage
                res = pe_evaluation.max_percentage if res > pe_evaluation.max_percentage

                table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

              else

                table_res_row.push ''

              end


            if num_t > 0
              table_res_temp.push table_res_row

              c_aux += 1

              num_t_eval += num_t
            end

          end

          if num_t_eval > 0

            table_res_temp.each do |r|
              table_res.push r
            end

          else

            tittle_gray.pop
            c_aux -= 2

          end

        end

        column_widths = Array.new
        column_widths.push (770 - ((col_span-1)*83))
        (1..col_span-1).each do |col|
          column_widths.push 83
        end

        pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

          table.column(0).row(0).background_color = '333333'
          table.column(0).row(1).background_color = 'dddddd'

          tittle_gray.each do |tg|

            table.column(0).row(tg).background_color = 'dddddd'

          end

          (1..col_span).each do |col|
            table.column(col).style :align => :center
          end

        end

        pdf.move_down 20

        position += 1

      end

    end

    return position

  end

    def rep_dfr_only_boss_2nd_level_det(pdf, pe_member, pe_process, position, show_average)

      pe_rel_boss = pe_process.pe_rel_by_id 1

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      pe_process.pe_evaluations.each do |pe_evaluation|

        pe_element_1st = pe_evaluation.pe_elements.first
        pe_element_2nd = pe_evaluation.pe_elements.second

        if pe_element_2nd && pe_element_2nd.assessed

          table_res = Array.new
          table_res_row = Array.new

          tittle_gray = Array.new

          col_span = 1

          pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
            if pe_rel_boss.id == pe_evaluation_rel.pe_rel.id && pe_member.is_evaluated_as_pe_rel?(pe_evaluation_rel.pe_rel)
              col_span += 1
            end
          end

          col_span += 1 if show_average

          c_aux = 0

          table_res_row = Array.new
          table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. DETALLE DE '+pe_evaluation.name.upcase+'</color></b></font>', colspan: col_span]
          table_res.push table_res_row

          c_aux += 1

          pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element_1st, nil).each do |pe_question_1st|

            num_t_eval = 0

            table_res_temp = Array.new

            table_res_row = Array.new
            table_res_row = [content: '<b>'+(pe_question_1st.description)+'</b>', colspan: col_span]
            table_res_temp.push table_res_row

            tittle_gray.push c_aux

            c_aux += 1

            table_res_row = Array.new
            table_res_row.push '<b>'+pe_element_2nd.name+'</b>'
            pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|
              if pe_rel_boss.id == pe_evaluation_rel.pe_rel.id && pe_member.is_evaluated_as_pe_rel?(pe_evaluation_rel.pe_rel)
                table_res_row.push '<b>'+pe_evaluation_rel.pe_rel.alias+'</b>'
              end
            end

            if show_average
              table_res_row.push '<b>Promedio</b>'
            end

            table_res_temp.push table_res_row

            c_aux += 1

            pe_evaluation.pe_questions_by_element_ready_and_not_informative(pe_element_2nd, pe_question_1st).each do |pe_question_2nd|

              sum_t = 0
              num_t = 0

              table_res_row = Array.new
              table_res_row.push (pe_question_2nd.description)

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                if pe_rel_boss.id == pe_evaluation_rel.pe_rel.id && pe_member.is_evaluated_as_pe_rel?(pe_evaluation_rel.pe_rel)

                  pe_rel = pe_evaluation_rel.pe_rel

                  sum = 0
                  num = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                      if pe_assessment_evaluation

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                        if pe_assessment_question && pe_assessment_question.percentage && pe_assessment_question.percentage >= 0

                          sum += pe_assessment_question.percentage
                          num += 1

                        end

                      end

                    end

                  end

                  if num > 0

                    res = num > 0 ? (sum/num).round(1) : 0

                    sum_t += res
                    num_t += 1

                    table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                  else

                    table_res_row.push ''

                  end

                end

              end

              if show_average

                res = num_t > 0 ? (sum_t/num_t).round(1) : 0

                table_res_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

              end

              if num_t > 0
                table_res_temp.push table_res_row

                c_aux += 1

                num_t_eval += num_t
              end

            end

            if num_t_eval > 0

              table_res_temp.each do |r|
                table_res.push r
              end

            else

              tittle_gray.pop
              c_aux -= 2

            end

          end

          column_widths = Array.new
          column_widths.push (770 - ((col_span-1)*83))
          (1..col_span-1).each do |col|
            column_widths.push 83
          end

          pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

            table.column(0).row(0).background_color = '333333'
            table.column(0).row(1).background_color = 'dddddd'

            tittle_gray.each do |tg|

              table.column(0).row(tg).background_color = 'dddddd'

            end

            (1..col_span).each do |col|
              table.column(col).style :align => :center
            end

          end

          pdf.move_down 20

          position += 1

        end

      end

      return position

    end

    def rep_dfr_per_evaluator_3rd_level_goals(pdf, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      if pe_process.of_persons?
        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_ordered
      else
        pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_ordered_by_pe_area
      end

      pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

        if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

          table_res = Array.new
          tittle_gray = Array.new
          tittle_gray2 = Array.new

          existen_datos = false

          pe_rel = pe_member_rel_is_evaluated.pe_rel

          pe_process.pe_evaluations.each do |pe_evaluation|

            pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

            if pe_evaluation.has_pe_evaluation_rel(pe_rel) && pe_evaluation.assessment_layout == 1 && pe_evaluation.pe_elements.size == 3

              existen_datos = true

              c_aux = 0

              if pe_rel.rel_name == :jefe

                title_aux = 'Evaluación del '+pe_rel.alias.downcase

              elsif pe_rel.rel_name == :auto

                title_aux = pe_rel.alias

              else

                title_aux = 'Evaluación de '+pe_rel.alias.downcase

              end

              table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+title_aux.upcase+' - '+pe_evaluation.name.upcase+'</color></b></font>', colspan: 6]
              table_res.push table_res_row

              c_aux += 1

              if pe_assessment_evaluation

                pe_element_1st = pe_evaluation.pe_elements.first 

                pe_group = nil

                if pe_evaluation.pe_groups.size > 0

                  pe_member_group = pe_member_rel_is_evaluated.pe_member_evaluated.pe_member_group(pe_evaluation)

                  pe_group = pe_member_group.pe_group if pe_member_group

                end



                pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_rel_is_evaluated.pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_group, pe_rel).each_with_index do |pe_question_1st_level|

                  if pe_element_1st.visible

                    table_res_row = Array.new
                    table_res_row.push pdf.make_cell(content: '<font size="12"><b><color rgb="000000"> '+(pe_element_1st.display_name ? pe_element_1st.name+'. ' : '')+(pe_question_1st_level.description)+'</color></b></font>', colspan: 6)
                    table_res.push table_res_row

                    c_aux += 1

                  end

                  num_1st_level = 1

                  pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level).each do |pe_question_2nd_level|


                    table_res_row = Array.new
                    table_res_row.push pdf.make_cell(content: '<b>'+num_1st_level.to_s+'</b>')
                    table_res_row.push pdf.make_cell(content: '<b>'+(pe_question_2nd_level.pe_element.display_name ? pe_question_2nd_level.pe_element.name+'. ' : '')+(pe_question_2nd_level.description)+'</b>', colspan: 5)

                    table_res.push table_res_row

                    tittle_gray.push c_aux

                    c_aux += 1

                    table_res_row = Array.new
                    table_res_row.push ''
                    table_res_row.push pe_evaluation.pe_elements.last.name
                    table_res_row.push 'Peso'
                    table_res_row.push 'Meta'
                    table_res_row.push 'Meta lograda'
                    table_res_row.push 'Logro'

                    table_res.push table_res_row

                    tittle_gray2.push c_aux

                    c_aux += 1

                    num_2nd_level = 1

                    pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level).each do |pe_question_3rd_level|

                      table_res_row = Array.new
                      table_res_row.push num_1st_level.to_s+'.'+num_2nd_level.to_s
                      table_res_row.push (pe_question_3rd_level.kpi_indicator ? 'KPI' : '')+(pe_question_3rd_level.pe_element.display_name ? (pe_question_3rd_level.pe_element.name)+'. ' : '')+((pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator) ? (pe_question_3rd_level.kpi_indicator.name) : pe_question_3rd_level.description )
                      table_res_row.push number_with_precision(pe_question_3rd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
                      
                      if pe_question_3rd_level.indicator_type != 4
                        tmp_goal = number_with_precision(pe_question_3rd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                      else
                        if pe_question_3rd_level.indicator_discrete_goal
                          tmp_goal = pe_question_3rd_level.indicator_discrete_goal
                        else
                          tmp_goal = 'meta'
                       end 
                      end

                      tmp_goal += pe_question_3rd_level.indicator_unit
                      
                      table_res_row.push tmp_goal

                      tmp_achieved_goal = ''

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_3rd_level)

                      unless pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator

                        if pe_question_3rd_level.indicator_type == 4

                          pe_question_3rd_level.pe_question_ranks.each do  |qr|
                            tmp_achieved_goal = qr.discrete_goal if pe_assessment_question && qr.discrete_goal == pe_assessment_question.indicator_discrete_achievement
                          end

                        else
                          tmp_achieved_goal = number_with_precision(pe_assessment_question.indicator_achievement, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true) if pe_assessment_question

                        end

                      end

                      table_res_row.push tmp_achieved_goal

                      if pe_question_3rd_level.indicator_type != 4 || (pe_question_3rd_level.indicator_type == 4 && !pe_assessment_question.indicator_discrete_achievement.blank?)

                        table_res_row.push number_with_precision(pe_assessment_question.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'

                      else
                        table_res_row.push ''
                      end

                      table_res.push table_res_row

                      c_aux += 1

                      num_2nd_level += 1

                    end

                    num_1st_level += 1

                  end



                end

              end

            end

          end

          column_widths = [40,400,70,95,95,70]

          if existen_datos

          pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

            table.column(0).row(0).background_color = '333333'

            tittle_gray.each do |tg|

              table.column(0).row(tg).background_color = 'dddddd'
              table.column(1).row(tg).background_color = 'dddddd'
              table.column(2).row(tg).background_color = 'dddddd'
              table.column(3).row(tg).background_color = 'dddddd'
              table.column(4).row(tg).background_color = 'dddddd'
              table.column(5).row(tg).background_color = 'dddddd'

            end

            tittle_gray2.each do |tg|

              table.column(0).row(tg).background_color = 'eeeeee'
              table.column(1).row(tg).background_color = 'eeeeee'
              table.column(2).row(tg).background_color = 'eeeeee'
              table.column(3).row(tg).background_color = 'eeeeee'
              table.column(4).row(tg).background_color = 'eeeeee'
              table.column(5).row(tg).background_color = 'eeeeee'

            end

          end

          pdf.move_down 20

          position += 1

          end

        end

      end

      return position

    end

    def rep_dfr_boss_2nd_level_goals(pdf, pe_member, pe_process, with_comments, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      pe_rel = pe_process.pe_rel_by_id 1

      if pe_rel

        if pe_process.of_persons?
          pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_as_pe_rel_ordered(pe_rel)
        else
          pe_member_rels_is_evaluated = nil
        end

        pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

          if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

            table_res = Array.new
            tittle_gray2 = Array.new

            pe_process.pe_evaluations.each do |pe_evaluation|

              pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

              pe_evaluation_rel = pe_evaluation.pe_evaluation_rel pe_rel

              if pe_evaluation.has_pe_evaluation_rel(pe_rel) && pe_evaluation.assessment_layout == 1 && pe_evaluation.pe_elements.size == 2

                c_aux = 0

                cp = 6

                table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+pe_evaluation.name.upcase+'</color></b></font>', colspan: cp]
                table_res.push table_res_row

                c_aux += 1

                if pe_assessment_evaluation

                  pe_element_1st = pe_evaluation.pe_elements.first

                  pe_group = nil

                  if pe_evaluation.pe_groups.size > 0

                    pe_member_group = pe_member_rel_is_evaluated.pe_member_evaluated.pe_member_group(pe_evaluation)

                    pe_group = pe_member_group.pe_group if pe_member_group

                  end

                  num_1st_level = 1

                  pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_rel_is_evaluated.pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_group, pe_rel).each_with_index do |pe_question_1st_level|

                    if pe_element_1st.visible

                      table_res_row = Array.new
                      cp = 6
                      table_res_row.push pdf.make_cell(content: '<font size="12"><b><color rgb="000000"> '+num_1st_level.to_s+'. '+(pe_element_1st.display_name ? pe_element_1st.name+'. ' : '')+(pe_question_1st_level.description)+'</color></b></font>', colspan: cp)
                      table_res.push table_res_row

                      c_aux += 1

                    end

                    table_res_row = Array.new
                    table_res_row.push ''
                    table_res_row.push pe_evaluation.pe_elements.last.name
                    table_res_row.push 'Peso'
                    table_res_row.push 'Meta'
                    table_res_row.push 'Meta lograda'
                    table_res_row.push 'Logro'

                    table_res.push table_res_row

                    tittle_gray2.push c_aux

                    c_aux += 1

                    num_2nd_level = 1

                    pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level).each do |pe_question_2nd_level|

                      table_res_row = Array.new
                      table_res_row.push num_1st_level.to_s+'.'+num_2nd_level.to_s

                      desc = (pe_question_2nd_level.kpi_indicator ? 'KPI' : '')+(pe_question_2nd_level.pe_element.display_name ? (pe_question_2nd_level.pe_element.name)+'. ' : '')+((pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator) ? (pe_question_2nd_level.kpi_indicator.name) : pe_question_2nd_level.description )

                      if pe_question_2nd_level.pe_element.allow_note && !pe_question_2nd_level.note.blank?

                        desc += '<br><br><strong>'+pe_question_2nd_level.pe_element.alias_note+':</strong><br>'+pe_question_2nd_level.note

                      end

                      #if with_comments

                      #  if pe_question_2nd_level.pe_question_comments_by_pe_member_rels(pe_member_rels_is_evaluated).size > 0
                      #    desc += '<br><br><strong>Comentarios:</strong><br>'
                      #    pe_question_2nd_level.pe_question_comments_by_pe_member_rels(pe_member_rels_is_evaluated).each do |pe_question_comment|
                      #      desc += '<br>-'+pe_question_comment.comment
                      #    end
                      #  end

                      #end

                      table_res_row.push desc

                      table_res_row.push number_with_precision(pe_question_2nd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

                      if pe_question_2nd_level.indicator_type != 4
                        tmp_goal = number_with_precision(pe_question_2nd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                      else
                        if pe_question_2nd_level.indicator_discrete_goal
                          tmp_goal = pe_question_2nd_level.indicator_discrete_goal
                        else
                          tmp_goal = 'meta'
                        end
                      end

                      tmp_goal += ' '+pe_question_2nd_level.indicator_unit if pe_question_2nd_level && pe_question_2nd_level.indicator_unit

                      table_res_row.push tmp_goal

                      tmp_achieved_goal = ''

                      pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd_level)

                      unless pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator

                        if pe_question_2nd_level.indicator_type == 4

                          pe_question_2nd_level.pe_question_ranks.each do  |qr|
                            tmp_achieved_goal = qr.discrete_goal if pe_assessment_question && qr.discrete_goal == pe_assessment_question.indicator_discrete_achievement
                          end

                        else
                          tmp_achieved_goal = number_with_precision(pe_assessment_question.indicator_achievement, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true) if pe_assessment_question

                        end

                      end

                      table_res_row.push tmp_achieved_goal

                      if pe_assessment_question && (pe_question_2nd_level.indicator_type != 4 || (pe_question_2nd_level.indicator_type == 4 && !pe_assessment_question.indicator_discrete_achievement.blank?))

                        table_res_row.push number_with_precision(pe_assessment_question.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'

                      else
                        table_res_row.push ''
                      end

                      table_res.push table_res_row

                      c_aux += 1

                      num_2nd_level += 1

                    end

                    num_1st_level += 1

                  end

                end

              end

            end


            column_widths = [40,400,70,95,95,70]

            pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'

              tittle_gray2.each do |tg|

                table.column(0).row(tg).background_color = 'eeeeee'
                table.column(1).row(tg).background_color = 'eeeeee'
                table.column(2).row(tg).background_color = 'eeeeee'
                table.column(3).row(tg).background_color = 'eeeeee'
                table.column(4).row(tg).background_color = 'eeeeee'
                table.column(5).row(tg).background_color = 'eeeeee'


              end

            end

            pdf.move_down 20

            position += 1

          end

        end

      end

      return position

    end

    def rep_dfr_boss_3rd_level_goals(pdf, pe_member, pe_process, position, show_percentage)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      pe_rel = pe_process.pe_rel_by_id 1

      if pe_rel

        if pe_process.of_persons?
          pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_as_pe_rel_ordered(pe_rel)
        else
          pe_member_rels_is_evaluated = nil
        end

        pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

          if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

            table_res = Array.new
            tittle_gray = Array.new
            tittle_gray2 = Array.new

            pe_process.pe_evaluations.each do |pe_evaluation|

              pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

              pe_evaluation_rel = pe_evaluation.pe_evaluation_rel pe_rel

              if pe_evaluation.has_pe_evaluation_rel(pe_rel) && pe_evaluation.assessment_layout == 1 && pe_evaluation.pe_elements.size == 3

                c_aux = 0

                cp = 6

                table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+pe_evaluation.name.upcase+'</color></b></font>', colspan: cp]
                table_res.push table_res_row

                c_aux += 1

                if pe_assessment_evaluation

                  pe_element_1st = pe_evaluation.pe_elements.first

                  pe_group = nil

                  if pe_evaluation.pe_groups.size > 0

                    pe_member_group = pe_member_rel_is_evaluated.pe_member_evaluated.pe_member_group(pe_evaluation)

                    pe_group = pe_member_group.pe_group if pe_member_group

                  end

                  pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_rel_is_evaluated.pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_group, pe_rel).each_with_index do |pe_question_1st_level|

                    if pe_element_1st.visible

                      table_res_row = Array.new
                      cp = show_percentage ? 6 : 5
                      table_res_row.push pdf.make_cell(content: '<font size="12"><b><color rgb="000000"> '+(pe_element_1st.display_name ? pe_element_1st.name+'. ' : '')+(pe_question_1st_level.description)+'</color></b></font>', colspan: cp)
                      table_res.push table_res_row

                      c_aux += 1

                    end

                    num_1st_level = 1

                    pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level).each do |pe_question_2nd_level|


                      table_res_row = Array.new
                      table_res_row.push pdf.make_cell(content: '<b>'+num_1st_level.to_s+'</b>')
                      table_res_row.push pdf.make_cell(content: '<b>'+(pe_question_2nd_level.pe_element.display_name ? pe_question_2nd_level.pe_element.name+'. ' : '')+(pe_question_2nd_level.description)+'</b>', colspan: (show_percentage ? 5 : 4))

                      table_res.push table_res_row

                      tittle_gray.push c_aux

                      c_aux += 1

                      table_res_row = Array.new
                      table_res_row.push ''
                      table_res_row.push pe_evaluation.pe_elements.last.name
                      table_res_row.push 'Peso'
                      table_res_row.push 'Meta'
                      table_res_row.push 'Meta lograda'

                      if show_percentage

                        table_res_row.push 'Logro'

                      end

                      table_res.push table_res_row

                      tittle_gray2.push c_aux

                      c_aux += 1

                      num_2nd_level = 1

                      pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level).each do |pe_question_3rd_level|

                        table_res_row = Array.new
                        table_res_row.push num_1st_level.to_s+'.'+num_2nd_level.to_s
                        table_res_row.push (pe_question_3rd_level.kpi_indicator ? 'KPI' : '')+(pe_question_3rd_level.pe_element.display_name ? (pe_question_3rd_level.pe_element.name)+'. ' : '')+((pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator) ? (pe_question_3rd_level.kpi_indicator.name) : pe_question_3rd_level.description )
                        table_res_row.push number_with_precision(pe_question_3rd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

                        if pe_question_3rd_level.indicator_type != 4
                          tmp_goal = number_with_precision(pe_question_3rd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                        else
                          if pe_question_3rd_level.indicator_discrete_goal
                            tmp_goal = pe_question_3rd_level.indicator_discrete_goal
                          else
                            tmp_goal = 'meta'
                          end
                        end

                        tmp_goal += pe_question_3rd_level.indicator_unit if pe_question_3rd_level.indicator_unit

                        table_res_row.push tmp_goal

                        tmp_achieved_goal = ''

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_3rd_level)

                        unless pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator

                          if pe_question_3rd_level.indicator_type == 4

                            pe_question_3rd_level.pe_question_ranks.each do  |qr|
                              tmp_achieved_goal = qr.discrete_goal if pe_assessment_question && qr.discrete_goal == pe_assessment_question.indicator_discrete_achievement
                            end

                          else
                            tmp_achieved_goal = number_with_precision(pe_assessment_question.indicator_achievement, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true) if pe_assessment_question

                          end

                        end

                        table_res_row.push tmp_achieved_goal

                        if show_percentage

                          if pe_question_3rd_level.indicator_type != 4 || (pe_question_3rd_level.indicator_type == 4 && !pe_assessment_question.indicator_discrete_achievement.blank?)
                            table_res_row.push number_with_precision(pe_assessment_question.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'
                          else
                            table_res_row.push ''
                          end
                        end

                        table_res.push table_res_row

                        c_aux += 1

                        num_2nd_level += 1

                      end

                      num_1st_level += 1

                    end



                  end

                end

              end

            end

            if show_percentage

              column_widths = [40,400,70,95,95,70]

            else

              column_widths = [40,465,70,95,100]

            end

            pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'

              tittle_gray.each do |tg|

                table.column(0).row(tg).background_color = 'dddddd'
                table.column(1).row(tg).background_color = 'dddddd'
                table.column(2).row(tg).background_color = 'dddddd'
                table.column(3).row(tg).background_color = 'dddddd'
                table.column(4).row(tg).background_color = 'dddddd'
                table.column(5).row(tg).background_color = 'dddddd'

              end

              tittle_gray2.each do |tg|

                table.column(0).row(tg).background_color = 'eeeeee'
                table.column(1).row(tg).background_color = 'eeeeee'
                table.column(2).row(tg).background_color = 'eeeeee'
                table.column(3).row(tg).background_color = 'eeeeee'
                table.column(4).row(tg).background_color = 'eeeeee'
                table.column(5).row(tg).background_color = 'eeeeee'

              end

            end

            pdf.move_down 20

            position += 1

          end

        end

      end

      return position

    end

    def rep_dfr_boss_auto_3rd_level_goals(pdf, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      pe_rel = pe_process.pe_rel_by_id 1

      if pe_rel

        pe_rel_auto = pe_process.pe_rel_by_id 0

        pe_member_rel_is_evaluated_auto = pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel_auto).first if pe_rel_auto

        if pe_process.of_persons?
          pe_member_rels_is_evaluated = pe_member.pe_member_rels_is_evaluated_as_pe_rel_ordered(pe_rel)
        else
          pe_member_rels_is_evaluated = nil
        end

        pe_member_rels_is_evaluated.each do |pe_member_rel_is_evaluated|

          if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

            table_res = Array.new
            tittle_gray = Array.new
            tittle_gray2 = Array.new

            pe_process.pe_evaluations.each do |pe_evaluation|

              pe_assessment_evaluation = pe_member_rel_is_evaluated.pe_assessment_evaluation(pe_evaluation)

              pe_evaluation_rel = pe_evaluation.pe_evaluation_rel pe_rel

              if pe_evaluation.has_pe_evaluation_rel(pe_rel) && pe_evaluation.assessment_layout == 1 && pe_evaluation.pe_elements.size == 3

                pe_assessment_evaluation_auto = pe_member_rel_is_evaluated_auto.pe_assessment_evaluation(pe_evaluation) if pe_member_rel_is_evaluated_auto

                pe_evaluation_rel_auto = pe_evaluation.pe_evaluation_rel pe_rel_auto

                c_aux = 0

                cp = pe_rel_auto && pe_member_rel_is_evaluated_auto ? 8 : 6

                table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+pe_evaluation.name.upcase+'</color></b></font>', colspan: cp]
                table_res.push table_res_row

                c_aux += 1

                if pe_assessment_evaluation

                  pe_element_1st = pe_evaluation.pe_elements.first

                  pe_group = nil

                  if pe_evaluation.pe_groups.size > 0

                    pe_member_group = pe_member_rel_is_evaluated.pe_member_evaluated.pe_member_group(pe_evaluation)

                    pe_group = pe_member_group.pe_group if pe_member_group

                  end

                  pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_rel_is_evaluated.pe_member_evaluated, pe_member_rel_is_evaluated.pe_member_evaluator, pe_group, pe_rel).each_with_index do |pe_question_1st_level|

                    if pe_element_1st.visible

                      table_res_row = Array.new
                      cp = pe_rel_auto && pe_member_rel_is_evaluated_auto ? 8 : 6
                      table_res_row.push pdf.make_cell(content: '<font size="12"><b><color rgb="000000"> '+(pe_element_1st.display_name ? pe_element_1st.name+'. ' : '')+(pe_question_1st_level.description)+'</color></b></font>', colspan: cp)
                      table_res.push table_res_row

                      c_aux += 1

                    end

                    num_1st_level = 1

                    pe_evaluation.pe_questions_by_question_ready(pe_question_1st_level).each do |pe_question_2nd_level|


                      table_res_row = Array.new
                      table_res_row.push pdf.make_cell(content: '<b>'+num_1st_level.to_s+'</b>')
                      table_res_row.push pdf.make_cell(content: '<b>'+(pe_question_2nd_level.pe_element.display_name ? pe_question_2nd_level.pe_element.name+'. ' : '')+(pe_question_2nd_level.description)+'</b>', colspan: 3)

                      table_res_row.push pdf.make_cell(content: '<b>'+(pe_rel.alias)+'</b>'+(pe_process.rep_detailed_final_results_show_rel_weight? && pe_evaluation_rel ? ' ('+number_with_precision(pe_evaluation_rel.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+')': ''), colspan: 2)

                      if pe_rel_auto && pe_member_rel_is_evaluated_auto

                        table_res_row.push pdf.make_cell(content: '<b>'+(pe_rel_auto.alias)+'</b>'+(pe_process.rep_detailed_final_results_show_rel_weight? && pe_evaluation_rel_auto ? ' ('+number_with_precision(pe_evaluation_rel_auto.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+')': ''), colspan: 2)

                      end

                      table_res.push table_res_row

                      tittle_gray.push c_aux

                      c_aux += 1

                      table_res_row = Array.new
                      table_res_row.push ''
                      table_res_row.push pe_evaluation.pe_elements.last.name
                      table_res_row.push 'Peso'
                      table_res_row.push 'Meta'
                      table_res_row.push 'Meta lograda'
                      table_res_row.push 'Logro'
                      if pe_rel_auto && pe_member_rel_is_evaluated_auto
                        table_res_row.push 'Meta lograda'
                        table_res_row.push 'Logro'
                      end

                      table_res.push table_res_row

                      tittle_gray2.push c_aux

                      c_aux += 1

                      num_2nd_level = 1

                      pe_evaluation.pe_questions_by_question_ready(pe_question_2nd_level).each do |pe_question_3rd_level|

                        table_res_row = Array.new
                        table_res_row.push num_1st_level.to_s+'.'+num_2nd_level.to_s
                        table_res_row.push (pe_question_3rd_level.kpi_indicator ? 'KPI' : '')+(pe_question_3rd_level.pe_element.display_name ? (pe_question_3rd_level.pe_element.name)+'. ' : '')+((pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator) ? (pe_question_3rd_level.kpi_indicator.name) : pe_question_3rd_level.description )
                        table_res_row.push number_with_precision(pe_question_3rd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)

                        if pe_question_3rd_level.indicator_type != 4
                          tmp_goal = number_with_precision(pe_question_3rd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                        else
                          if pe_question_3rd_level.indicator_discrete_goal
                            tmp_goal = pe_question_3rd_level.indicator_discrete_goal
                          else
                            tmp_goal = 'meta'
                          end
                        end

                        tmp_goal += pe_question_3rd_level.indicator_unit if pe_question_3rd_level.indicator_unit

                        table_res_row.push tmp_goal

                        tmp_achieved_goal = ''

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_3rd_level)

                        unless pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator

                          if pe_question_3rd_level.indicator_type == 4

                            pe_question_3rd_level.pe_question_ranks.each do  |qr|
                              tmp_achieved_goal = qr.discrete_goal if pe_assessment_question && qr.discrete_goal == pe_assessment_question.indicator_discrete_achievement
                            end

                          else
                            tmp_achieved_goal = number_with_precision(pe_assessment_question.indicator_achievement, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true) if pe_assessment_question

                          end

                        end

                        table_res_row.push tmp_achieved_goal

                        if pe_question_3rd_level.indicator_type != 4 || (pe_question_3rd_level.indicator_type == 4 && !pe_assessment_question.indicator_discrete_achievement.blank?)

                          table_res_row.push number_with_precision(pe_assessment_question.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'

                        else
                          table_res_row.push ''
                        end


                        if pe_rel_auto && pe_member_rel_is_evaluated_auto && pe_assessment_evaluation_auto

                          pe_assessment_question = pe_assessment_evaluation_auto.pe_assessment_question(pe_question_3rd_level)

                          unless pe_question_3rd_level.pe_element.kpi_dashboard && pe_question_3rd_level.kpi_indicator

                            if pe_question_3rd_level.indicator_type == 4

                              pe_question_3rd_level.pe_question_ranks.each do  |qr|
                                tmp_achieved_goal = qr.discrete_goal if pe_assessment_question && qr.discrete_goal == pe_assessment_question.indicator_discrete_achievement
                              end

                            else
                              tmp_achieved_goal = number_with_precision(pe_assessment_question.indicator_achievement, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true) if pe_assessment_question

                            end

                          end

                          table_res_row.push tmp_achieved_goal

                          if pe_question_3rd_level.indicator_type != 4 || (pe_question_3rd_level.indicator_type == 4 && !pe_assessment_question.indicator_discrete_achievement.blank?)

                            table_res_row.push number_with_precision(pe_assessment_question.percentage, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)+'%'

                          else
                            table_res_row.push ''
                          end

                        end


                        table_res.push table_res_row

                        c_aux += 1

                        num_2nd_level += 1

                      end

                      num_1st_level += 1

                    end



                  end

                end

              end

            end

            if pe_rel_auto && pe_member_rel_is_evaluated_auto
              column_widths = [40,390,46,86,55,49,55,49]
            else
              column_widths = [40,400,70,95,95,70]
            end

            pdf.table(table_res, :header => false, width: 770, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'

              tittle_gray.each do |tg|

                table.column(0).row(tg).background_color = 'dddddd'
                table.column(1).row(tg).background_color = 'dddddd'
                table.column(2).row(tg).background_color = 'dddddd'
                table.column(3).row(tg).background_color = 'dddddd'
                table.column(4).row(tg).background_color = 'dddddd'
                table.column(5).row(tg).background_color = 'dddddd'

                if pe_rel_auto && pe_member_rel_is_evaluated_auto
                  table.column(6).row(tg).background_color = 'dddddd'
                  table.column(7).row(tg).background_color = 'dddddd'
                end

              end

              tittle_gray2.each do |tg|

                table.column(0).row(tg).background_color = 'eeeeee'
                table.column(1).row(tg).background_color = 'eeeeee'
                table.column(2).row(tg).background_color = 'eeeeee'
                table.column(3).row(tg).background_color = 'eeeeee'
                table.column(4).row(tg).background_color = 'eeeeee'
                table.column(5).row(tg).background_color = 'eeeeee'

                if pe_rel_auto && pe_member_rel_is_evaluated_auto
                  table.column(6).row(tg).background_color = 'eeeeee'
                  table.column(7).row(tg).background_color = 'eeeeee'
                end

              end

            end

            pdf.move_down 20

            position += 1

          end

        end

      end

      return position

    end

    def rep_dfr_informative_1st_level(pdf, pe_member, pe_process, with_comments = true, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'

      pe_process.pe_evaluations.each do |pe_evaluation|

        pe_element_1st = pe_evaluation.pe_elements.first

        eval_inf = false

        pe_group = nil

        if pe_evaluation.pe_groups.size > 0

          pe_member_group = pe_member.pe_member_group(pe_evaluation)

          pe_group = pe_member_group.pe_group if pe_member_group

        end

        num_t_eval = 0

        table_res = Array.new

        tittle_gray = Array.new

        c_aux = 0

        table_res_row = Array.new
        table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+' '+pe_evaluation.name.upcase+'</color></b></font>', colspan: 2]
        table_res.push table_res_row

        c_aux += 1

        pe_evaluation.pe_questions_by_group_ready_and_informative(pe_group, nil).each do |pe_question_1st|

          eval_inf = true

          num_t = 0

          table_res_temp = Array.new

          table_res_row = Array.new
          table_res_row = [content: '<b>'+pe_question_1st.description+'</b>', colspan: 2]
          table_res_temp.push table_res_row

          tittle_gray.push c_aux

          pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

            if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel

              pe_rel = pe_evaluation_rel.pe_rel

              num = 0

              pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                  pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                  if pe_assessment_evaluation

                    pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_1st)

                    if pe_assessment_question

                      table_res_row = Array.new
                      tmp = ''
                      tmp += pe_assessment_question.comment if pe_assessment_question.comment && pe_question_1st.has_comment
                      tmp += pe_assessment_question.pe_alternative.description if pe_element_1st.assessment_method == 1 && pe_assessment_question.pe_alternative

                      table_res_row.push pe_rel.alias
                      table_res_row.push tmp
                      table_res_temp.push table_res_row

                      num += 1

                    else

                      table_res_row = Array.new
                      tmp = ''

                      table_res_row.push pe_rel.alias
                      table_res_row.push tmp
                      table_res_temp.push table_res_row

                    end

                  end

                end

              end

              num_t += 1 if num > 0

            end

          end

          table_res_temp.each do |r|
            table_res.push r
            c_aux += 1
          end

          table_res_temp = Array.new

          if with_comments

            pe_member_rels_comments = @pe_member.pe_member_rels_is_evaluated

            if pe_question_1st.pe_question_comments_by_pe_member_rels(pe_member_rels_comments).size > 0

              table_res_row = Array.new
              table_res_row.push 'Comentarios'

              comments = ''

              pe_question_1st.pe_question_comments_by_pe_member_rels(pe_member_rels_comments).each do |pe_question_comment|
                comments += '<br>-'+pe_question_comment.comment
              end

              table_res_row.push comments

              #table_res_temp.push table_res_row

              table_res_temp.each do |r|
                table_res.push r
                c_aux += 1
              end

            end

          end

          num_t_eval += num_t

        end

        if eval_inf

          column_widths = Array.new
          column_widths.push 120
          column_widths.push 400

          pdf.table(table_res, :header => false, width: 520, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

            table.column(0).row(0).background_color = '333333'

          end

          pdf.move_down 20

          position += 1

        end


      end

      return position

    end

    def rep_dfr_informative_2nd_level(pdf, pe_member, pe_process, position)

      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'

      pe_process.pe_evaluations.each do |pe_evaluation|

        pe_element_1st = pe_evaluation.pe_elements.first
        pe_element_2nd = pe_evaluation.pe_elements.second

        if pe_element_2nd && pe_element_2nd.assessed

          pe_evaluation.pe_questions_by_element_ready_and_informative(pe_element_1st, nil).each do |pe_question_1st|

            num_t_eval = 0

            table_res = Array.new

            tittle_gray = Array.new

            c_aux = 0

            table_res_row = Array.new
            table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. DETALLE DE '+ActionView::Base.full_sanitizer.sanitize(pe_question_1st.description).upcase+'</color></b></font>', colspan: 2]
            table_res.push table_res_row

            c_aux += 1

            pe_evaluation.pe_questions_by_element_ready_and_informative(pe_element_2nd, pe_question_1st).each do |pe_question_2nd|

              num_t = 0

              table_res_temp = Array.new

              table_res_row = Array.new
              table_res_row = [content: '<b>'+(pe_question_2nd.description)+'</b>', colspan: 2]
              table_res_temp.push table_res_row

              tittle_gray.push c_aux

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                if pe_member.is_evaluated_as_pe_rel? pe_evaluation_rel.pe_rel

                  pe_rel = pe_evaluation_rel.pe_rel

                  num = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                      if pe_assessment_evaluation

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                        if pe_assessment_question

                          table_res_row = Array.new
                          tmp = ''
                          tmp += pe_assessment_question.comment if pe_assessment_question.comment && pe_question_2nd.has_comment
                          tmp += pe_assessment_question.pe_alternative.description if pe_element_2nd.assessment_method == 1 && pe_assessment_question.pe_alternative

                          table_res_row.push pe_rel.alias
                          table_res_row.push tmp
                          table_res_temp.push table_res_row

                          num += 1

                        end

                      end

                    end

                  end

                  num_t += 1 if num > 0

                end

              end

              if num_t > 0

                table_res_temp.each do |r|
                  table_res.push r
                  c_aux += 1
                end

                num_t_eval += num_t

              else
                tittle_gray.pop
              end

            end

            if num_t_eval > 0

              column_widths = Array.new
              column_widths.push 120
              column_widths.push 400

              pdf.table(table_res, :header => false, width: 520, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

                table.column(0).row(0).background_color = '333333'
                table.column(0).row(1).background_color = 'dddddd'

                tittle_gray.each do |tg|

                  table.column(0).row(tg).background_color = 'dddddd'

                end


              end

              pdf.move_down 20

              position += 1

            end



          end

        end

      end

      return position

    end

    def rep_dfr_informative_only_boss_2nd_level(pdf, pe_member, pe_process, position)

      pe_rel_boss = pe_process.pe_rel_by_id 1

      pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
      @orientation = 'V'

      pe_process.pe_evaluations.each do |pe_evaluation|

        pe_element_1st = pe_evaluation.pe_elements.first
        pe_element_2nd = pe_evaluation.pe_elements.second

        if pe_element_2nd && pe_element_2nd.assessed

          pe_evaluation.pe_questions_by_element_ready_and_informative(pe_element_1st, nil).each do |pe_question_1st|

            num_t_eval = 0

            table_res = Array.new

            tittle_gray = Array.new

            c_aux = 0

            table_res_row = Array.new
            table_res_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. DETALLE DE '+(pe_question_1st.description).upcase+'</color></b></font>', colspan: 2]
            table_res.push table_res_row

            c_aux += 1

            pe_evaluation.pe_questions_by_element_ready_and_informative(pe_element_2nd, pe_question_1st).each do |pe_question_2nd|

              num_t = 0

              table_res_temp = Array.new

              table_res_row = Array.new
              table_res_row = [content: '<b>'+(pe_question_2nd.description)+'</b>', colspan: 2]
              table_res_temp.push table_res_row

              tittle_gray.push c_aux

              pe_evaluation.pe_evaluation_rels.each do |pe_evaluation_rel|

                if pe_rel_boss.id == pe_evaluation_rel.pe_rel.id && pe_member.is_evaluated_as_pe_rel?(pe_evaluation_rel.pe_rel)

                  pe_rel = pe_evaluation_rel.pe_rel

                  num = 0

                  pe_member.pe_member_rels_is_evaluated_as_pe_rel(pe_rel).each do |pe_member_rel_is_evaluated|

                    if pe_member_rel_is_evaluated.valid_evaluator && pe_member_rel_is_evaluated.finished

                      pe_assessment_evaluation = pe_evaluation.pe_assessment_evaluation_by_pe_member_rel pe_member_rel_is_evaluated

                      if pe_assessment_evaluation

                        pe_assessment_question = pe_assessment_evaluation.pe_assessment_question(pe_question_2nd)

                        if pe_assessment_question

                          table_res_row = Array.new
                          tmp = ''
                          tmp += pe_assessment_question.comment if pe_question_2nd.has_comment
                          tmp += pe_assessment_question.pe_alternative.description if pe_element_2nd.assessment_method == 1 && pe_assessment_question.pe_alternative

                          table_res_row.push pe_rel.alias
                          table_res_row.push tmp
                          table_res_temp.push table_res_row

                          num += 1

                        end

                      end

                    end

                  end

                  num_t += 1 if num > 0

                end

              end

              if num_t > 0

                table_res_temp.each do |r|
                  table_res.push r
                  c_aux += 1
                end

                num_t_eval += num_t

              else
                tittle_gray.pop
              end

            end

            if num_t_eval > 0

              column_widths = Array.new
              column_widths.push 120
              column_widths.push 400

              pdf.table(table_res, :header => false, width: 520, column_widths: column_widths, :cell_style => { :inline_format => true }) do |table|

                table.column(0).row(0).background_color = '333333'
                table.column(0).row(1).background_color = 'dddddd'

                tittle_gray.each do |tg|

                  table.column(0).row(tg).background_color = 'dddddd'

                end


              end

              pdf.move_down 20

              position += 1

            end



          end

        end

      end

      return position

    end

    #

    def fr_header(pdf, hr_process, user, hr_process_user)

      table_header = Array.new
      table_header_row = Array.new

      table_header_row = [content: '<font size="12"><b><color rgb="ffffff">RESULTADO INDIVIDUAL DE EVALUACIÓN DE DESEMPEÑO</color></b></font>', colspan: 2]
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Proceso</b>'
      table_header_row.push hr_process.nombre+' '+hr_process.year.to_s
      table_header.push table_header_row

      table_header_row = Array.new
      table_header_row.push '<b>Colaborador evaluado</b>'
      table_header_row.push '<b>'+user.apellidos+', '+user.nombre+'<b>'
      table_header.push table_header_row

      Characteristic.where('mini_ficha = ?', true).reorder('orden_mini_ficha').each do |characteristic|

        uc = user.user_characteristics.find_by_characteristic_id(characteristic.id)

        table_header_row = Array.new
        table_header_row.push '<b>'+characteristic.nombre+'</b>'
        table_header_row.push (uc ? uc.valor : '')
        table_header.push table_header_row

      end

      hr_process_user_jefe = hr_process_user.hr_process_es_evaluado_users.where('tipo = ?', 'jefe').first

      if hr_process_user_jefe

        table_header_row = Array.new
        table_header_row.push '<b>'+hr_process.alias_jefe+'</b>'
        table_header_row.push hr_process_user_jefe.user.apellidos+', '+hr_process_user_jefe.user.nombre
        table_header.push table_header_row

      end

      pdf.table(table_header, :header => false, :width => 520, :column_widths => [150], :cell_style => { :inline_format => true }) do |table|

        table.column(0).background_color = 'dddddd'
        table.column(0).row(0).background_color = '333333'

      end

      pdf.move_down 20

    end

    def fr_final_resume(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, is_boss=false, himself = false)


      if @orientation == 'H'
        pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
        @orientation = 'V'
      end

      table_res_final = Array.new

      unless hr_process.label_1_t.blank? && hr_process.label_1_c.blank?

        table_res_final = Array.new
        table_res_final_row = Array.new

        table_res_final_row = [content: '<b>'+hr_process.label_1_t+'</b>']
        table_res_final.push table_res_final_row

        table_res_final_row = Array.new
        if hr_process.label_1_c
          table_res_final_row = [content: hr_process.label_1_c.html_safe]
        end
        table_res_final.push table_res_final_row

        pdf.table(table_res_final, :header => false, :width => 520, :cell_style => { :inline_format => true }) do |table|
          table.column(0).row(0).background_color = 'dddddd'
        end

        pdf.move_down 20


      end

      unless hr_process.label_2_t.blank? && hr_process.label_2_c.blank?

        table_res_final = Array.new
        table_res_final_row = Array.new

        table_res_final_row = [content: '<b>'+hr_process.label_2_t+'</b>']
        table_res_final.push table_res_final_row

        table_res_final_row = Array.new
        if hr_process.label_2_c
          table_res_final_row = [content: hr_process.label_2_c.html_safe]
        end

        table_res_final.push table_res_final_row

        pdf.table(table_res_final, :header => false, :width => 520, :cell_style => { :inline_format => true }) do |table|
          table.column(0).row(0).background_color = 'dddddd'
        end

        pdf.move_down 20


      end

      unless hr_process.label_3_t.blank? && hr_process.label_3_c.blank?

        table_res_final = Array.new
        table_res_final_row = Array.new

        table_res_final_row = [content: '<b>'+hr_process.label_3_t+'</b>']
        table_res_final.push table_res_final_row

        table_res_final_row = Array.new
        if hr_process.label_3_c
          table_res_final_row = [content: hr_process.label_3_c.html_safe]
        end
        table_res_final.push table_res_final_row

        pdf.table(table_res_final, :header => false, :width => 520, :cell_style => { :inline_format => true }) do |table|
          table.column(0).row(0).background_color = 'dddddd'
        end

        pdf.move_down 20


      end

      pdf.start_new_page if $current_domain == 'seduc' || $current_domain == 'seduc-training'



      table_res_final = Array.new
      table_res_final_row = Array.new

      c_aux = 0

      table_res_final_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. RESUMEN EJECUTIVO</color></b></font>', colspan: 5]
      table_res_final.push table_res_final_row

      c_aux += 1

      table_res_final_row = Array.new
      table_res_final_row = [content: '<b>'+position.to_s+'.1. Valoración final</b>', colspan: 5]
      table_res_final.push table_res_final_row

      c_aux += 1

      comentario_calib_por_dim = false


      hr_process_dimensions.each do |hr_process_dimension|

        c_aux += 1

        table_res_final_row = Array.new


        hr_process_assessment_d =  hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

        if hr_process_assessment_d

          dim_g =  hr_process_assessment_d.hr_process_dimension_g

          tmp = number_with_precision(hr_process_assessment_d.resultado_porcentaje, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'% '
          tmp +=  dim_g.nombre if dim_g && dim_g.nombre


        else
          tmp = ''
        end

        table_res_final_row = ['<b>'+hr_process_dimension.nombre+'</b>', {content: tmp, colspan: 4}]
        table_res_final.push table_res_final_row

        if !himself

            hr_process_assessment_d_cal = hr_process_assessment_d.hr_process_assessment_d_cal if hr_process_assessment_d

            if comentario_calib_por_dim == false && hr_process_assessment_d_cal && hr_process_assessment_d_cal.comentario

              table_res_final_row = Array.new
              table_res_final_row = ['<b>Comentario de la calibración</b>', {content: hr_process_assessment_d_cal.comentario, colspan: 4}]
              table_res_final.push table_res_final_row

              c_aux += 1

              comentario_calib_por_dim = true

            end



        end

      end

      if (hr_process.muestra_cajas? || hr_process.muestra_cuadrantes_calib || hr_process.calib_cuadrante)

        hr_process_assessment_qua = hr_process_user.hr_process_assessment_qua

        if hr_process_assessment_qua

          hr_process_quadrant = hr_process_assessment_qua.hr_process_quadrant

          if hr_process_quadrant && hr_process_quadrant.nombre
            tmp = hr_process_quadrant.nombre+' '+('('+hr_process_quadrant.descripcion+')' if hr_process_quadrant.descripcion)

          else
            tmp = 'No evaluado'
          end

        end

        table_res_final_row = Array.new
        table_res_final_row = ['<b>Valoración</b>', {content: tmp, colspan: 4}]
        table_res_final.push table_res_final_row

        c_aux += 1

        if hr_process_quadrant && hr_process_quadrant.descripcion_detalle && hr_process_quadrant.descripcion_detalle != ''
          table_res_final_row = Array.new
          table_res_final_row = ['<b>Descripción de la valoración</b>', {content: hr_process_quadrant && hr_process_quadrant.descripcion_detalle ? hr_process_quadrant.descripcion_detalle : '', colspan: 4}]
          table_res_final.push table_res_final_row

          c_aux += 1
        end

        if !himself

            hr_process_calibration_session_m = hr_process_user.hr_process_calibration_session_ms.first

            if hr_process_calibration_session_m

              hr_process_assessment_qua_cal = HrProcessAssessmentQuaCal.where('hr_process_calibration_session_id = ? AND  hr_process_calibration_session_m_id = ?', hr_process_calibration_session_m.hr_process_calibration_session.id, hr_process_calibration_session_m.id).first

              if hr_process_assessment_qua_cal && hr_process_assessment_qua_cal.comentario

                table_res_final_row = Array.new
                table_res_final_row = ['<b>Comentario de la calibración</b>', {content: hr_process_assessment_qua_cal.comentario, colspan: 4}]
                table_res_final.push table_res_final_row

                c_aux += 1

              end

            end



        end

      end

      table_res_final_row = Array.new
      table_res_final_row = [content: '<b>'+position.to_s+'.2. Valoración por dimensión y evaluaciones</b>', colspan: 5]
      table_res_final.push table_res_final_row

      c_aux += 1
      c_aux_2 = c_aux

      table_res_final_row = Array.new
      table_res_final_row.push '<b>Dimensión</b>'
      table_res_final_row.push '<b>Evaluación</b>'
      table_res_final_row.push '<b>Peso</b>'
      table_res_final_row.push '<b>Resultado</b>'
      table_res_final_row.push '<b>Valoración dimensión</b>'
      table_res_final.push table_res_final_row

      table_res_final_row = Array.new
      hr_process_dimensions.each do |hr_process_dimension|


        hr_process_dimension.hr_process_dimension_es.each_with_index do |hr_process_dimension_e, numero_eval|


          hr_process_evaluation = hr_process_dimension_e.hr_process_evaluations.where('hr_process_id = ? ', hr_process.id).first

          if hr_process_evaluation

            hr_process_assessment_ef = HrProcessAssessmentEf.where('hr_process_evaluation_id = ? AND hr_process_user_id = ?', hr_process_evaluation.id, hr_process_user.id).first

            if hr_process_assessment_ef

              tmp = number_with_precision(hr_process_assessment_ef.resultado_porcentaje, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

            else
              tmp = ''
            end

          end

          table_res_final_row = Array.new


          if numero_eval == 0

            hr_process_assessment_d =  hr_process_user.hr_process_assessment_ds.find_by_hr_process_dimension_id(hr_process_dimension.id)

            if hr_process_assessment_d

              tmp_d = number_with_precision(hr_process_assessment_d.resultado_porcentaje, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'% '

            else
              tmp_d = ''
            end


            table_res_final_row = [{content: hr_process_dimension.nombre, rowspan: hr_process_dimension.hr_process_dimension_es.size},
                                   hr_process_dimension_e.hr_evaluation_type.nombre,
                                   hr_process_dimension_e.porcentaje.to_s+'%',
                                   tmp,
                                   {content: tmp_d, rowspan: hr_process_dimension.hr_process_dimension_es.size}]
          else

            table_res_final_row = [hr_process_dimension_e.hr_evaluation_type.nombre,
                                   hr_process_dimension_e.porcentaje.to_s+'%',
                                   tmp]


          end


          table_res_final.push table_res_final_row

          c_aux_2 += 1

        end

      end

      if hr_process.tiene_paso_feedback

        if hr_process_user.feedback_confirmado

          table_res_final_row = Array.new
          table_res_final_row = [content: '<b>'+position.to_s+'.3. Retroalimentación</b>', colspan: 5]
          table_res_final.push table_res_final_row

          c_aux_2 += 1

          table_res_final_row = Array.new

          if hr_process.tiene_paso_feedback_modo == 1

            retro = hr_process_user.texto_feedback.dup
            retro.gsub! '<li>', "\n-"
            retro.gsub! '<br>', "\n"

            table_res_final_row = [content: (ActionView::Base.full_sanitizer.sanitize retro), colspan: 5]

            table_res_final.push table_res_final_row

          else

            retro_1 = hr_process_user.tf_1.dup
            retro_1.gsub! '<li>', "\n-"
            retro_1.gsub! '<br>', "\n"

            if $current_domain == 'epacasmayo'
              table_res_final_row = [content: '<b>1. Fortalezas</b>', colspan: 5]
            else
              table_res_final_row = [content: '<b>1. Atributos positivos de desempeño</b>', colspan: 5]
            end





            table_res_final.push table_res_final_row
            table_res_final_row = [content: (ActionView::Base.full_sanitizer.sanitize retro_1), colspan: 5]
            table_res_final.push table_res_final_row

            retro_2 = hr_process_user.tf_2.dup
            retro_2.gsub! '<li>', "\n-"
            retro_2.gsub! '<br>', "\n"

            table_res_final_row = [content: '<b>2. Oportunidades de mejora </b>', colspan: 5]
            table_res_final.push table_res_final_row
            table_res_final_row = [content: (ActionView::Base.full_sanitizer.sanitize retro_2), colspan: 5]
            table_res_final.push table_res_final_row

            table_res_final_row = [content: '<b>3. Plan de Acción </b>', colspan: 5]
            table_res_final.push table_res_final_row

            if $current_domain == 'epacasmayo'
              table_res_final_row = [{content: '<b>70% (experiencia, asignación de proyectos, retos, etc)</b>', colspan: 1}, {content: '<b>20% (retroalimentación constante y acompañamiento)</b>', colspan: 2}, {content: '<b>10% (capacitación técnica)</b>', colspan: 2}]
            else
              table_res_final_row = [{content: '<b>¿Qué?</b>', colspan: 1}, {content: '<b>¿Cómo?</b>', colspan: 2}, {content: '<b>¿Cuándo?</b>', colspan: 2}]
            end




            table_res_final.push table_res_final_row


            hr_process_user.hr_process_user_tfs.each_with_index do |hr_process_user_tf, index_tf|

              retro_wh = hr_process_user_tf.what.dup
              retro_wh.gsub! '<li>', "\n-"
              retro_wh.gsub! '<br>', "\n"

              retro_h = hr_process_user_tf.how.dup
              retro_h.gsub! '<li>', "\n-"
              retro_h.gsub! '<br>', "\n"

              retro_w = hr_process_user_tf.when.dup
              retro_w.gsub! '<li>', "\n-"
              retro_w.gsub! '<br>', "\n"

              table_res_final_row = [{content: retro_wh, colspan: 1}, {content: retro_h, colspan: 2}, {content: retro_w, colspan: 2}]
              table_res_final.push table_res_final_row


            end

            #retro = hr_process_user.texto_feedback.dup
            #retro.gsub! '<li>', "\n-"
            #retro.gsub! '<br>', "\n"

            if $current_domain == 'epacasmayo'

              retro_4 = hr_process_user.tf_4.dup
              retro_4.gsub! '<li>', "\n-"
              retro_4.gsub! '<br>', "\n"

              table_res_final_row = [content: '<b>4. Comentarios adicionales</b>', colspan: 5]

              table_res_final.push table_res_final_row
              table_res_final_row = [content: (ActionView::Base.full_sanitizer.sanitize retro_4), colspan: 5]
              table_res_final.push table_res_final_row

            end


          end




        end

      end

      pdf.table(table_res_final, :header => false, :width => 520, :column_widths => [150], :cell_style => { :inline_format => true }) do |table|

        table.column(0).row(0).background_color = '333333'
        table.column(0).row(1).background_color = 'dddddd'
        table.column(0).row(c_aux-1).background_color = 'dddddd'
        if hr_process.tiene_paso_feedback
          if hr_process_user.feedback_confirmado
            table.column(0).row(c_aux_2).background_color = 'dddddd'
          end
        end

        table.column(2).style :align => :center
        table.column(3).style :align => :center
        table.column(4).style :align => :center

      end

      #pdf.move_down 20

      if $current_domain == 'seduc' || $current_domain == 'seduc-training'

        pdf.move_down 100

        table_res_final = Array.new
        table_res_final_row = Array.new

        #hr_process_user_jefe = hr_process.hr_process_user_rels.where('tipo = ? AND hr_process_user2_id = ?', 'jefe', hr_process_user.id).first.hr_process_user1

        #table_res_final_row.push hr_process_user_jefe.user.nombre+' '+hr_process_user_jefe.user.apellidos+'<br><i>'+hr_process.alias_jefe+'</i>'
        table_res_final_row.push hr_process.alias_jefe.upcase
        table_res_final_row.push ''
        #table_res_final_row.push hr_process_user.user.nombre+' '+hr_process_user.user.apellidos+'<br><i>Colaborador</i>'
        table_res_final_row.push hr_process.alias_sub.upcase

        table_res_final.push table_res_final_row

        pdf.table(table_res_final, :header => false, :width => 520, :column_widths => [200,120,200], :cell_style => { :inline_format => true }) do |table|
          table.column(0).row(0).border_widths = [1,0,0,0]
          table.column(1).row(0).border_widths = [0,0,0,0]
          table.column(2).row(0).border_widths = [1,0,0,0]

          table.column(0).style :align => :center
          table.column(2).style :align => :center

        end


      end

      pdf.start_new_page

    end

    def fr_by_evaluator_type(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, tipo_evaluador, lista_relaciones, lista_relaciones_plu)

      if @orientation == 'H'
        pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
        @orientation == 'V'
      end

      table_res_jefe = Array.new

      table_res_jefe_row = Array.new

      if tipo_evaluador == 'auto'
        texto_tit = 'AUTOEVALUACIÓN'
        tipo_evaluador = nil
      elsif tipo_evaluador == 'jefe'
        texto_tit = 'EVALUACIÓN DEL '+lista_relaciones['jefe'].upcase
      else
        texto_tit = 'EVALUACIÓN DE '+lista_relaciones_plu[tipo_evaluador].upcase
      end

      table_res_jefe_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+texto_tit+' </color></b></font>', colspan: 2]
      table_res_jefe.push table_res_jefe_row

      c_aux_2 = 0
      c_aux_2_a = Array.new
      c_aux_2p_a = Array.new

      c_aux = 0

      visualiza_todo = false

      hr_process_evaluations.each do |hr_process_evaluation|

        visualiza_eval = false
        visualiza_eval = true if tipo_evaluador.nil? && hr_process_evaluation.hr_process_dimension_e.eval_auto
        visualiza_eval = true if tipo_evaluador == 'jefe' && hr_process_evaluation.hr_process_dimension_e.eval_jefe
        visualiza_eval = true if tipo_evaluador == 'par' && hr_process_evaluation.hr_process_dimension_e.eval_par
        visualiza_eval = true if tipo_evaluador == 'sub' && hr_process_evaluation.hr_process_dimension_e.eval_sub
        visualiza_eval = true if tipo_evaluador == 'cli' && hr_process_evaluation.hr_process_dimension_e.eval_cli
        visualiza_eval = true if tipo_evaluador == 'prov' && hr_process_evaluation.hr_process_dimension_e.eval_prov

        if tipo_evaluador != nil
          visualiza_eval = false unless hr_process_user.hr_process_es_evaluado_rels.where('tipo = ? AND no_aplica = ?', tipo_evaluador, false).count > 0
        end

        if visualiza_eval

          visualiza_todo = true

          if hr_process_evaluation.hr_process_evaluation_qs.where('(hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false).count > 0

            c_aux += 1

            hr_process_dimension_gs = hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC')

            hr_process_evaluation_q = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false).first

            if hr_process_evaluation_q && hr_process_evaluation_q.hr_evaluation_type_element.aplica_evaluacion

              hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false)

            else

              hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NOT NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false)

            end

            pregs_por_nombre_suma_jefe = Hash.new
            pregs_por_nombre_num_jefe = Hash.new

            hr_process_evaluation_qs.each_with_index do |hr_process_evaluation_q, num_preg|

              s_jefe = 0
              t_jefe = 0

              hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).each do |hr_process_assessment_q|

                if hr_process_assessment_q.resultado_puntos && hr_process_assessment_q.resultado_puntos >= 0 && hr_process_assessment_q.resultado_porcentaje && hr_process_assessment_q.resultado_porcentaje >= 0

                  if hr_process_assessment_q.hr_process_assessment_e && hr_process_assessment_q.hr_process_assessment_e.tipo_rel == tipo_evaluador
                    s_jefe += hr_process_assessment_q.resultado_porcentaje
                    t_jefe += 1
                  end

                end

              end

              pregs_por_nombre_suma_jefe[hr_process_evaluation_q.texto] = 0 unless pregs_por_nombre_suma_jefe[hr_process_evaluation_q.texto]
              pregs_por_nombre_num_jefe[hr_process_evaluation_q.texto] = 0 unless pregs_por_nombre_num_jefe[hr_process_evaluation_q.texto]

              pregs_por_nombre_suma_jefe[hr_process_evaluation_q.texto] += s_jefe
              pregs_por_nombre_num_jefe[hr_process_evaluation_q.texto] += t_jefe

            end

            num_preg = 0


            table_res_jefe_row = Array.new

            c_aux_2 += 1

            c_aux_2_a.push c_aux_2

            table_res_jefe_row = [content: '<b>'+position.to_s+'.'+c_aux.to_s+'. '+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre+'</b>', colspan: 2]
            table_res_jefe.push table_res_jefe_row

            t = 0
            s = 0
            pregs_por_nombre_suma_jefe.sort.each do |nombre_preg, value|

              if value > 0

                c_aux_2 +=1

                table_res_jefe_row = Array.new

                res = pregs_por_nombre_num_jefe[nombre_preg] > 0 ? (value/pregs_por_nombre_num_jefe[nombre_preg]).round(1) : 0

                s = s + res
                t = t + 1

                num_preg += 1

                res_width = res > 100 ? 100 : res

                color = ''
                hr_process_dimension_gs.each do |g|

                  if g.valor_minimo <= res && res <= g.valor_maximo
                    color = g.color_text
                    break
                  end

                end

                color = ''

                table_res_jefe_row.push ActionView::Base.full_sanitizer.sanitize nombre_preg.dup.gsub('<br>',': ')
                table_res_jefe_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

                table_res_jefe.push table_res_jefe_row

              end

            end

            prom = t > 0 ? (s/t).round(1) : 0

            prom_width = prom > 100 ? 100 : prom

            color = ''
            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= prom && prom <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            c_aux_2 +=1

            c_aux_2p_a.push c_aux_2

            table_res_jefe_row = Array.new
            table_res_jefe_row.push '<b>PROMEDIO</b>'
            table_res_jefe_row.push '<b>'+number_with_precision(prom, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%</b>'

            table_res_jefe.push table_res_jefe_row



          end

        end

      end

      if visualiza_todo

        pdf.table(table_res_jefe, :header => false, :width => 520, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|

          table.column(1).style :align => :center

          table.column(0).row(0).background_color = '333333'

          c_aux_2_a.each do |n|
            table.column(0).row(n).background_color = 'dddddd'
          end

          c_aux_2p_a.each do |n|
            table.column(0).row(n).style :align => :right
          end

        end

        pdf.move_down 20

        position += 1

      end

      return position

    end

    def fr_all_evaluators(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu)

      if @orientation == 'H'
        pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
        @orientation == 'V'
      end

      table_res_todos = Array.new

      table_res_todos_row = Array.new

      table_res_todos_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. EVALUACIÓN DE TODOS LOS EVALUADORES</color></b></font>', colspan: 2]
      table_res_todos.push table_res_todos_row

      mostrar_evals_todos_evaluadores = false

      c_aux_2 = 0
      c_aux_2_a = Array.new
      c_aux_2p_a = Array.new

      c_aux = 0

      hr_process_evaluations.each do |hr_process_evaluation|

        si_auto = false
        si_par = false
        si_sub = false
        si_cli = false
        si_prov = false

        si_par = true if hr_process_evaluation.hr_process_dimension_e.eval_par
        si_auto = true if hr_process_evaluation.hr_process_dimension_e.eval_auto
        si_sub = true if hr_process_evaluation.hr_process_dimension_e.eval_sub
        si_cli = true if hr_process_evaluation.hr_process_dimension_e.eval_cli
        si_prov = true if hr_process_evaluation.hr_process_dimension_e.eval_prov

        if (si_par || si_auto || si_sub || si_cli || si_prov) && hr_process_evaluation.hr_process_evaluation_qs.count > 0

          c_aux += 1

          mostrar_evals_todos_evaluadores = true

          hr_process_dimension_gs = hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC')

          hr_process_evaluation_q = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false).first

          if hr_process_evaluation_q && hr_process_evaluation_q.hr_evaluation_type_element.aplica_evaluacion

            hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false)

          else

            hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NOT NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false)

          end

          pregs_por_nombre_suma = Hash.new
          pregs_por_nombre_num = Hash.new

          hr_process_evaluation_qs.each_with_index do |hr_process_evaluation_q, num_preg|

            s = 0
            t = 0

            hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).each do |hr_process_assessment_q|

              if hr_process_assessment_q.resultado_puntos && hr_process_assessment_q.resultado_puntos >= 0 && hr_process_assessment_q.resultado_porcentaje && hr_process_assessment_q.resultado_porcentaje >= 0

                peso = 0
                peso = hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'jefe'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_par_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'par'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_sub_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'sub'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_cli_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'cli'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_prov_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'prov'
                peso = hr_process_evaluation.hr_process_dimension_e.eval_auto_peso if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == nil

                s += hr_process_assessment_q.resultado_porcentaje*peso
                t += peso

              end

            end


            pregs_por_nombre_suma[hr_process_evaluation_q.texto] = 0 unless pregs_por_nombre_suma[hr_process_evaluation_q.texto]
            pregs_por_nombre_num[hr_process_evaluation_q.texto] = 0 unless pregs_por_nombre_num[hr_process_evaluation_q.texto]

            pregs_por_nombre_suma[hr_process_evaluation_q.texto] += s
            pregs_por_nombre_num[hr_process_evaluation_q.texto] += t

          end

          c_aux_2 += 1
          c_aux_2_a.push c_aux_2

          num_preg = 0

          table_res_todos_row = [content: '<b>'+position.to_s+'.'+c_aux.to_s+'.'+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre+'</b>', colspan: 2]

          table_res_todos.push table_res_todos_row

          t = 0
          s = 0

          pregs_por_nombre_suma.sort.each do |nombre_preg, value|

            if value > 0

              c_aux_2 += 1

              res = pregs_por_nombre_num[nombre_preg] > 0 ? (value/pregs_por_nombre_num[nombre_preg]).round(1) : 0
              s = s + res
              t = t + 1
              num_preg += 1

              res_width = res > 100 ? 100 : res

              color = ''

              hr_process_dimension_gs.each do |g|

                if g.valor_minimo <= res && res <= g.valor_maximo
                  color = g.color_text
                  break
                end

              end

              color = ''

              table_res_todos_row = Array.new

              table_res_todos_row.push ActionView::Base.full_sanitizer.sanitize nombre_preg.dup.gsub('<br>',': ')
              table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'

              table_res_todos.push table_res_todos_row

            end

          end



          prom = t > 0 ? (s/t).round(1) : 0

          prom_width = prom > 100 ? 100 : prom

          color = ''


          hr_process_dimension_gs.each do |g|

            if g.valor_minimo <= prom && prom <= g.valor_maximo
              color = g.color_text
              break
            end

          end

          color = ''

          c_aux_2 += 1

          c_aux_2p_a.push c_aux_2

          table_res_todos_row = Array.new

          table_res_todos_row.push '<b>PROMEDIO</b>'
          table_res_todos_row.push '<b>'+number_with_precision(prom, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%</b>'

          table_res_todos.push table_res_todos_row

          res_jefe = 0
          t_jefe = 0
          res_par = 0
          t_par = 0
          res_cli = 0
          t_cli = 0
          res_sub = 0
          t_sub = 0
          res_prov = 0
          t_prov = 0
          res_auto = 0

          hr_process_evaluation.hr_process_assessment_es.where('hr_process_user_id = ?', hr_process_user.id).each do |hr_process_assessment_e|

            if hr_process_assessment_e.resultado_puntos && hr_process_assessment_e.resultado_puntos >= 0 && hr_process_assessment_e.resultado_porcentaje && hr_process_assessment_e.resultado_porcentaje >= 0

              case hr_process_assessment_e.tipo_rel
                when 'jefe'
                  res_jefe += hr_process_assessment_e.resultado_porcentaje
                  t_jefe += 1
                when 'par'
                  res_par += hr_process_assessment_e.resultado_porcentaje
                  t_par += 1
                when 'sub'
                  res_sub += hr_process_assessment_e.resultado_porcentaje
                  t_sub += 1
                when 'cli'
                  res_cli += hr_process_assessment_e.resultado_porcentaje
                  t_cli += 1
                when 'prov'
                  res_prov += hr_process_assessment_e.resultado_porcentaje
                  t_prov += 1
                when nil
                  res_auto = hr_process_assessment_e.resultado_porcentaje
              end

            end

          end


          table_res_todos_row = Array.new

          c_aux_2 += 1
          c_aux_2_a.push c_aux_2

          table_res_todos_row = [content: '<b>'+position.to_s+'.'+c_aux.to_s+'.1. Resultado de '+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre+' por tipo de evaluador</b>', colspan: 2]

          table_res_todos.push table_res_todos_row

          res = t_jefe > 0 ? res_jefe/t_jefe : -1

          if res > -1

            c_aux_2 += 1

            table_res_todos_row = Array.new

            table_res_todos_row.push lista_relaciones['jefe']

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            table_res_todos.push table_res_todos_row

          end

          res = t_par > 0 ? res_par/t_par : -1
          if res > -1
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push lista_relaciones_plu['par']

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            table_res_todos.push table_res_todos_row
          end


          res = t_sub > 0 ? res_sub/t_sub : -1
          if res > -1
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push lista_relaciones_plu['sub']

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            table_res_todos.push table_res_todos_row
          end

          res = t_cli > 0 ? res_cli/t_cli : -1
          if res > -1
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push lista_relaciones_plu['cli']

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            table_res_todos.push table_res_todos_row

          end

          res = t_prov > 0 ? res_prov/t_prov : -1
          if res > -1
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push lista_relaciones_plu['prov']

            res_width = res > 100 ? 100 : res
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res && res <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            table_res_todos.push table_res_todos_row

          end

          if res_auto > 0
            c_aux_2 += 1
            table_res_todos_row = Array.new

            table_res_todos_row.push 'Autoevaluación'

            res_width = res_auto > 100 ? 100 : res_auto
            color = ''

            hr_process_dimension_gs.each do |g|

              if g.valor_minimo <= res_auto && res_auto <= g.valor_maximo
                color = g.color_text
                break
              end

            end

            color = ''

            table_res_todos_row.push number_with_precision(res_auto, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)+'%'
            table_res_todos.push table_res_todos_row

          end

          pdf.table(table_res_todos, :header => false, :width => 520, :column_widths => [400,120], :cell_style => { :inline_format => true }) do |table|
            table.column(1).style :align => :center

            table.column(0).row(0).background_color = '333333'

            c_aux_2_a.each do |n|
              table.column(0).row(n).background_color = 'dddddd'
            end

            c_aux_2p_a.each do |n|
              table.column(0).row(n).style :align => :right
            end


          end




        end

      end

      pdf.move_down 20

    end

    def fr_evaluation_detail_sp(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu, only_for = nil)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      c_aux_det = position - 1

      hr_process_evaluations.each do |hr_process_evaluation|

        if hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NOT NULL').count > 0

          unless hr_process_evaluation.hr_process_dimension_e.registro_por_gestor

            c_aux_det += 1 if only_for == nil

            si_jefe = false
            si_auto = false
            si_par = false
            si_sub = false
            si_cli = false
            si_prov = false

            num_cols = 0

            if hr_process_evaluation.hr_process_dimension_e.eval_jefe && (only_for == 'jefe' || only_for == nil )
              si_jefe = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_par && (only_for == 'par' || only_for == nil )
              si_par = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_auto && (only_for == 'auto' || only_for == nil )
              si_auto = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_sub && (only_for == 'sub' || only_for == nil )
              si_sub = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_cli && (only_for == 'cli' || only_for == nil )
              si_cli = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_prov && (only_for == 'prov' || only_for == nil )
              si_prov = true
              num_cols += 1
            end

            if only_for != nil
              num_colspan = num_cols+1
            else
              num_colspan = num_cols+1
            end


            c_aux_2 = 0
            c_aux_2_a = Array.new
            c_aux_2p_a = Array.new

            c_aux = 0

            table_res_det = Array.new
            table_res_det_row = Array.new

            table_res_det_row = [content: '<font size="12"><b><color rgb="ffffff">'+c_aux_det.to_s+'. DETALLE DE '+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre.upcase+'</color></b></font>', colspan: num_colspan]
            table_res_det.push table_res_det_row

            nombre_2nd_nivel = hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = 2').first.nombre

            hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false).each do |hr_process_evaluation_q|

              if hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).count > 0

                c_aux += 1

                table_res_det_row = Array.new

                c_aux_2 += 1

                c_aux_2_a.push c_aux_2


                table_res_det_row = [content: '<b>'+c_aux_det.to_s+'.'+c_aux.to_s+'. '+ActionView::Base.full_sanitizer.sanitize(hr_process_evaluation_q.texto.dup.gsub('<br>',': ')+'<b>'), colspan: num_colspan]
                table_res_det.push table_res_det_row

                c_aux_2 += 1

                table_res_det_row = Array.new

                table_res_det_row.push '<b>'+nombre_2nd_nivel+'</b>'

                if si_jefe
                  table_res_det_row.push '<b>'+lista_relaciones['jefe']+'</b>'
                end

                if si_par
                  table_res_det_row.push '<b>'+lista_relaciones['par']+'</b>'
                end

                if si_sub
                  table_res_det_row.push '<b>'+lista_relaciones['sub']+'</b>'
                end

                if si_cli
                  table_res_det_row.push '<b>'+lista_relaciones['cli']+'</b>'
                end

                if si_prov
                  table_res_det_row.push '<b>'+lista_relaciones['prov']+'</b>'
                end

                if si_auto
                  table_res_det_row.push '<b>Auto</b>'
                end


                table_res_det.push table_res_det_row

                hr_process_evaluation_q.hr_process_evaluation_qs.each do |hr_process_evaluation_q_hijo|

                  res_jefe = 0
                  t_jefe = 0
                  res_par = 0
                  t_par = 0
                  res_cli = 0
                  t_cli = 0
                  res_sub = 0
                  t_sub = 0
                  res_prov = 0
                  t_prov = 0
                  res_auto = 0
                  t_auto = 0

                  hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q_hijo.id).each do |hr_process_assessment_q|


                    if hr_process_assessment_q.resultado_puntos && hr_process_assessment_q.resultado_puntos >= 0 && hr_process_assessment_q.resultado_porcentaje && hr_process_assessment_q.resultado_porcentaje >= 0

                      if hr_process_assessment_q.hr_process_assessment_e && hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'jefe'
                        res_jefe += hr_process_assessment_q.resultado_porcentaje
                        t_jefe += 1
                      end

                      if hr_process_assessment_q.hr_process_assessment_e && hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'par'
                        res_par += hr_process_assessment_q.resultado_porcentaje
                        t_par += 1
                      end

                      if hr_process_assessment_q.hr_process_assessment_e && hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'sub'
                        res_sub += hr_process_assessment_q.resultado_porcentaje
                        t_sub += 1
                      end

                      if hr_process_assessment_q.hr_process_assessment_e && hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'cli'
                        res_cli += hr_process_assessment_q.resultado_porcentaje
                        t_cli += 1
                      end

                      if hr_process_assessment_q.hr_process_assessment_e && hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'prov'
                        res_prov += hr_process_assessment_q.resultado_porcentaje
                        t_prov += 1
                      end

                      if hr_process_assessment_q.hr_process_assessment_e && hr_process_assessment_q.hr_process_assessment_e.tipo_rel == nil
                        res_auto = hr_process_assessment_q.resultado_porcentaje
                        t_auto += 1
                      end


                    end

                  end

                  s_preg_nivel_2 = 0
                  t_preg_nivel_2 = 0

                  if t_jefe > 0
                    s_preg_nivel_2 += (res_jefe/t_jefe)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso
                  end

                  if t_par > 0
                    s_preg_nivel_2 += (res_par/t_par)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_par_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_par_peso
                  end

                  if t_sub > 0
                    s_preg_nivel_2 += (res_sub/t_sub)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_sub_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_sub_peso
                  end

                  if t_cli > 0
                    s_preg_nivel_2 += (res_cli/t_cli)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_cli_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_cli_peso
                  end

                  if t_prov > 0
                    s_preg_nivel_2 += (res_prov/t_prov)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_prov_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_prov_peso
                  end

                  if t_auto > 0
                    s_preg_nivel_2 += res_auto*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_auto_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_auto_peso
                  end

                  c_aux_2 += 1


                  table_res_det_row = Array.new

                  table_res_det_row.push hr_process_evaluation_q_hijo.texto

                  if si_jefe
                    table_res_det_row.push t_jefe > 0 ? number_with_precision(res_jefe/t_jefe, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_par
                    table_res_det_row.push t_par > 0 ? number_with_precision(res_par/t_par, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_sub
                    table_res_det_row.push t_sub > 0 ? number_with_precision(res_sub/t_sub, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_cli
                    table_res_det_row.push t_cli > 0 ? number_with_precision(res_cli/t_cli, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_prov
                    table_res_det_row.push t_prov > 0 ? number_with_precision(res_prov/t_prov, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_auto
                    table_res_det_row.push t_auto > 0 ? number_with_precision(res_auto, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  table_res_det.push table_res_det_row

                end


              end

            end

            #aux_col = 500/(num_cols+1)
            #aux_col = 60 if aux_col < 60

            #aux_col_1 = 500-(aux_col*(num_cols+1))

            aux_col = 460/(num_cols+1)
            aux_col = 60 if aux_col < 60

            aux_col_1 = 320

            aux_cols_array = Array.new
            aux_cols_array.push aux_col_1
            (1..num_cols).each do |n_col|
              aux_cols_array.push aux_col
            end

            aux_cols_array = [500,260] if only_for != nil

            pdf.table(table_res_det, :header => false, :column_widths => aux_cols_array, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'

              c_aux_2_a.each do |n|
                table.row(n).background_color = 'dddddd'
              end

            end

            pdf.move_down 20

          end

        end

      end

      return c_aux_det+1


    end

    def fr_manual_evals(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu, only_for = nil)


      if @orientation == 'H'
        pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
        @orientation == 'V'
      end

      c_aux_det = position

      hr_process_evaluations.each do |hr_process_evaluation|

        if hr_process_evaluation.hr_process_evaluation_manual_qs.where('hr_process_user_id = ?', hr_process_user.id).count > 0

          hr_evaluation_type_element_nivel_1 = hr_process_evaluation.evaluation_type_element_level_1
          hr_evaluation_type_element_nivel_2 = hr_process_evaluation.evaluation_type_element_level_2

          total_cols = 0
          if hr_evaluation_type_element_nivel_2.aplica_evaluacion
            total_cols += 1
            if hr_evaluation_type_element_nivel_2.indicador_logro
              total_cols += 2
            end
          end

          #hr_process_assessment_e = HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND hr_process_user_eval_id = ? AND tipo_rel IS NULL', hr_process_evaluation.id, hr_process_user.id, hr_process_user.id).first

          hr_process_assessment_e = HrProcessAssessmentE.where('hr_process_evaluation_id = ? AND hr_process_user_id = ? AND tipo_rel = "jefe"', hr_process_evaluation.id, hr_process_user.id).first

          hr_process_assessment_e = HrProcessAssessmentE.new unless hr_process_assessment_e

          c_aux_2 = 0
          c_aux_2_a = Array.new
          c_aux_2p_a = Array.new

          c_aux = 0

          num_elemento_nivel_1 = 1

          table_res_det = Array.new

          table_res_det_row = Array.new

          table_res_det_row = [content: '<font size="12"><b><color rgb="ffffff">'+c_aux_det.to_s+'. '+hr_evaluation_type_element_nivel_1.hr_evaluation_type.nombre.upcase+'</color></b></font>', colspan: total_cols]
          table_res_det.push table_res_det_row

          table_res_det_row = Array.new

          q_classifications = hr_evaluation_type_element_nivel_1.tiene_clasificacion ? hr_process_evaluation.hr_process_evaluation_q_classifications.where('hr_evaluation_type_element_id = ? ', hr_evaluation_type_element_nivel_1.id) : Array.new

          q_classifications.each do |q_classification|

            q_classification_id = nil

            if q_classification

              q_classification_id = q_classification.id

              if hr_process_evaluation.manual_questions_nivel_1(hr_process_user).where('hr_process_evaluation_q_classification_id = ?',q_classification_id).count > 0

                table_res_det_row = [content: '<font size="12"><b><color rgb="000000">'+q_classification.nombre+'</color></b></font>', colspan: total_cols]
                table_res_det.push table_res_det_row

                c_aux_2 += 1

              end

            end

            hr_process_evaluation.manual_questions_nivel_1(hr_process_user).where('hr_process_evaluation_q_classification_id = ?',q_classification_id).each do |hr_process_evaluation_manual_q|

              c_aux += 1

              table_res_det_row = Array.new

              table_res_det_row = [content: '<b>'+c_aux_det.to_s+'.'+c_aux.to_s+'. '+ActionView::Base.full_sanitizer.sanitize(hr_process_evaluation_manual_q.texto.dup.gsub('<br>',': ')+'<b>'), colspan: total_cols]
              table_res_det.push table_res_det_row

              c_aux_2 += 1

              table_res_det_row = Array.new



              if hr_evaluation_type_element_nivel_2

                table_res_det_row.push hr_evaluation_type_element_nivel_2.nombre

                if hr_evaluation_type_element_nivel_2.aplica_evaluacion

                  table_res_det_row.push 'Peso'

                  if hr_evaluation_type_element_nivel_2.indicador_logro

                    table_res_det_row.push 'Resultado'

                  end 

                end

              end

              table_res_det.push table_res_det_row

              c_aux_2 += 1

              c_aux_2_a.push c_aux_2

              hr_process_evaluation_manual_q.hr_process_evaluation_manual_qs.each_with_index do |hr_process_evaluation_manual_q_hijo, index_level_2|

                table_res_det_row = Array.new

                table_res_det_row.push hr_process_evaluation_manual_q_hijo.texto


                if hr_evaluation_type_element_nivel_2.aplica_evaluacion

                  table_res_det_row.push number_with_precision(hr_process_evaluation_manual_q_hijo.peso, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%'

                  if hr_evaluation_type_element_nivel_2.indicador_logro

                    hr_process_assessment_q = hr_process_assessment_e.hr_process_assessment_qs.where('hr_process_evaluation_manual_q_id = ?',hr_process_evaluation_manual_q_hijo.id).first


                    if hr_process_assessment_q


                       if hr_process_evaluation_manual_q_hijo.tipo_indicador != 5 || (hr_process_evaluation_manual_q_hijo.tipo_indicador == 5 && !hr_process_assessment_q.logro_indicador_d.blank?)

                         table_res_det_row.push number_with_precision(hr_process_assessment_q.resultado_puntos, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%'

                       end

                    else

                      table_res_det_row.push ''

                    end

                  end

                end

                table_res_det.push table_res_det_row

                c_aux_2 += 1

              end

            end

          end

          pdf.table(table_res_det, :header => false, :column_widths => [380,70,70], :cell_style => { :inline_format => true }) do |table|

            table.column(0).row(0).background_color = '333333'

            c_aux_2_a.each do |n|
              table.row(n).background_color = 'dddddd'
            end

          end

          pdf.move_down 20

        end

      end

      return c_aux_det+1


    end

    def fr_evaluation_detail(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, lista_relaciones, lista_relaciones_plu, only_for = nil)

      pdf.start_new_page(:page_size => 'A4', :layout => :landscape)
      @orientation = 'H'

      c_aux_det = position - 1

      hr_process_evaluations.each do |hr_process_evaluation|

        if hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NOT NULL').count > 0

          unless hr_process_evaluation.hr_process_dimension_e.registro_por_gestor

            c_aux_det += 1 if only_for == nil

            si_jefe = false
            si_auto = false
            si_par = false
            si_sub = false
            si_cli = false
            si_prov = false

            num_cols = 0

            if hr_process_evaluation.hr_process_dimension_e.eval_jefe && (only_for == 'jefe' || only_for == nil )
              si_jefe = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_par && (only_for == 'par' || only_for == nil )
              si_par = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_auto && (only_for == 'auto' || only_for == nil )
              si_auto = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_sub && (only_for == 'sub' || only_for == nil )
              si_sub = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_cli && (only_for == 'cli' || only_for == nil )
              si_cli = true
              num_cols += 1
            end

            if hr_process_evaluation.hr_process_dimension_e.eval_prov && (only_for == 'prov' || only_for == nil )
              si_prov = true
              num_cols += 1
            end

            if only_for != nil
              num_colspan = num_cols+1
            else
              num_colspan = num_cols+2
            end


            c_aux_2 = 0
            c_aux_2_a = Array.new
            c_aux_2p_a = Array.new

            c_aux = 0

            table_res_det = Array.new
            table_res_det_row = Array.new

            table_res_det_row = [content: '<font size="12"><b><color rgb="ffffff">'+c_aux_det.to_s+'. DETALLE DE '+hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.nombre.upcase+'</color></b></font>', colspan: num_colspan]
            table_res_det.push table_res_det_row

            nombre_2nd_nivel = hr_process_evaluation.hr_process_dimension_e.hr_evaluation_type.hr_evaluation_type_elements.where('nivel = 2').first.nombre

            hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, false).each do |hr_process_evaluation_q|

              if hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).count > 0

                c_aux += 1

                table_res_det_row = Array.new

                c_aux_2 += 1

                c_aux_2_a.push c_aux_2


                table_res_det_row = [content: '<b>'+c_aux_det.to_s+'.'+c_aux.to_s+'. '+ActionView::Base.full_sanitizer.sanitize(hr_process_evaluation_q.texto.dup.gsub('<br>',': ')+'<b>'), colspan: num_colspan]
                table_res_det.push table_res_det_row

                c_aux_2 += 1

                table_res_det_row = Array.new

                table_res_det_row.push '<b>'+nombre_2nd_nivel+'</b>'

                if si_jefe
                  table_res_det_row.push '<b>'+lista_relaciones['jefe']+'</b>'
                end

                if si_par
                  table_res_det_row.push '<b>'+lista_relaciones['par']+'</b>'
                end

                if si_sub
                  table_res_det_row.push '<b>'+lista_relaciones['sub']+'</b>'
                end

                if si_cli
                  table_res_det_row.push '<b>'+lista_relaciones['cli']+'</b>'
                end

                if si_prov
                  table_res_det_row.push '<b>'+lista_relaciones['prov']+'</b>'
                end

                if si_auto
                  table_res_det_row.push '<b>Auto</b>'
                end


                table_res_det_row.push '<b>Promedio</b>'  if only_for == nil

                table_res_det.push table_res_det_row

                hr_process_evaluation_q.hr_process_evaluation_qs.each do |hr_process_evaluation_q_hijo|

                  res_jefe = 0
                  t_jefe = 0
                  res_par = 0
                  t_par = 0
                  res_cli = 0
                  t_cli = 0
                  res_sub = 0
                  t_sub = 0
                  res_prov = 0
                  t_prov = 0
                  res_auto = 0
                  t_auto = 0

                  hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q_hijo.id).each do |hr_process_assessment_q|


                    if hr_process_assessment_q.resultado_puntos && hr_process_assessment_q.resultado_puntos >= 0 && hr_process_assessment_q.resultado_porcentaje && hr_process_assessment_q.resultado_porcentaje >= 0

                      if hr_process_assessment_q.hr_process_assessment_e

                        if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'jefe'
                          res_jefe += hr_process_assessment_q.resultado_porcentaje
                          t_jefe += 1
                        end

                        if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'par'
                          res_par += hr_process_assessment_q.resultado_porcentaje
                          t_par += 1
                        end

                        if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'sub'
                          res_sub += hr_process_assessment_q.resultado_porcentaje
                          t_sub += 1
                        end

                        if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'cli'
                          res_cli += hr_process_assessment_q.resultado_porcentaje
                          t_cli += 1
                        end

                        if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == 'prov'
                          res_prov += hr_process_assessment_q.resultado_porcentaje
                          t_prov += 1
                        end

                        if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == nil
                          res_auto = hr_process_assessment_q.resultado_porcentaje
                          t_auto += 1
                        end

                      end

                    end

                  end

                  s_preg_nivel_2 = 0
                  t_preg_nivel_2 = 0

                  if t_jefe > 0
                    s_preg_nivel_2 += (res_jefe/t_jefe)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_jefe_peso
                  end

                  if t_par > 0
                    s_preg_nivel_2 += (res_par/t_par)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_par_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_par_peso
                  end

                  if t_sub > 0
                    s_preg_nivel_2 += (res_sub/t_sub)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_sub_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_sub_peso
                  end

                  if t_cli > 0
                    s_preg_nivel_2 += (res_cli/t_cli)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_cli_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_cli_peso
                  end

                  if t_prov > 0
                    s_preg_nivel_2 += (res_prov/t_prov)*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_prov_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_prov_peso
                  end

                  if t_auto > 0
                    s_preg_nivel_2 += res_auto*hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_auto_peso
                    t_preg_nivel_2 += hr_process_evaluation_q_hijo.hr_process_evaluation.hr_process_dimension_e.eval_auto_peso
                  end

                  c_aux_2 += 1


                  table_res_det_row = Array.new

                  table_res_det_row.push hr_process_evaluation_q_hijo.texto

                  if si_jefe
                    table_res_det_row.push t_jefe > 0 ? number_with_precision(res_jefe/t_jefe, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_par
                    table_res_det_row.push t_par > 0 ? number_with_precision(res_par/t_par, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_sub
                    table_res_det_row.push t_sub > 0 ? number_with_precision(res_sub/t_sub, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_cli
                    table_res_det_row.push t_cli > 0 ? number_with_precision(res_cli/t_cli, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_prov
                    table_res_det_row.push t_prov > 0 ? number_with_precision(res_prov/t_prov, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  if si_auto
                    table_res_det_row.push t_auto > 0 ? number_with_precision(res_auto, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''
                  end

                  table_res_det_row.push t_preg_nivel_2 > 0 ? number_with_precision(s_preg_nivel_2/t_preg_nivel_2, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false).to_s+'%' : ''  if only_for == nil

                  table_res_det.push table_res_det_row

                end


              end

            end

            #aux_col = 500/(num_cols+1)
            #aux_col = 60 if aux_col < 60

            #aux_col_1 = 500-(aux_col*(num_cols+1))

            aux_col = 380/(num_cols+1)
            aux_col = 60 if aux_col < 60

            aux_col_1 = 400

            aux_cols_array = Array.new
            aux_cols_array.push aux_col_1
            (1..num_cols).each do |n_col|
              aux_cols_array.push aux_col
            end

            aux_cols_array = [500,200] if only_for != nil

            pdf.table(table_res_det, :header => false, :column_widths => aux_cols_array, :cell_style => { :inline_format => true }) do |table|

              table.column(0).row(0).background_color = '333333'

              c_aux_2_a.each do |n|
                table.row(n).background_color = 'dddddd'
              end

            end

            pdf.move_down 20

          end

        end

      end

      return c_aux_det+1


    end

    def fr_discretes(position, pdf, hr_process, user, hr_process_user, hr_process_dimensions, hr_process_evaluations, relaciones, lista_relaciones, lista_relaciones_plu, only_for = nil)

      if @orientation == 'H'
        pdf.start_new_page(:page_size => 'A4', :layout => :portrait)
        @orientation == 'V'
      end

      hr_process_evaluations.each do |hr_process_evaluation|

        si_auto = false
        si_par = false
        si_sub = false
        si_cli = false
        si_prov = false

        si_par = true if hr_process_evaluation.hr_process_dimension_e.eval_par
        si_auto = true if hr_process_evaluation.hr_process_dimension_e.eval_auto
        si_sub = true if hr_process_evaluation.hr_process_dimension_e.eval_sub
        si_cli = true if hr_process_evaluation.hr_process_dimension_e.eval_cli
        si_prov = true if hr_process_evaluation.hr_process_dimension_e.eval_prov

        if hr_process_evaluation.hr_process_evaluation_qs.where('discrete_display_in_reports = ?', true).count > 0

          hr_process_dimension_gs = hr_process_evaluation.hr_process_dimension_e.hr_process_dimension.hr_process_dimension_gs.reorder('orden DESC')

          hr_process_evaluation_q = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, true).first

          if hr_process_evaluation_q && hr_process_evaluation_q.hr_evaluation_type_element.aplica_evaluacion

            hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, true)

          else

            hr_process_evaluation_qs = hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id IS NOT NULL AND (hr_process_level_id = ? OR hr_process_level_id IS NULL) AND discrete_display_in_reports = ?', hr_process_user.hr_process_level_id, true)

          end

          pregs_por_nombre_suma = Hash.new
          pregs_por_nombre_num = Hash.new

          hr_process_evaluation_qs.each_with_index do |hr_process_evaluation_q, num_preg|

            colspan_ = 1
            max_colspan_ = 2

            if hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).count > 0

              hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).each do |hr_process_evaluation_q_hijo|

                hr_process_evaluation_a = hr_process_evaluation_q_hijo.hr_process_evaluation_as.first

                hay_alternativas = false

                if hr_process_evaluation_a && hr_process_evaluation_a.texto != '_sin valor_'
                  hay_alternativas = true
                end

                if hay_alternativas
                  colspan_ += 1
                end

                if hr_process_evaluation_q_hijo.comentario
                  colspan_ += 1
                end

                max_colspan_ = colspan_ if colspan_ > max_colspan_
                colspan_ = 1

              end

            else

              hr_process_evaluation_a = hr_process_evaluation_q.hr_process_evaluation_as.first

              hay_alternativas = false

              if hr_process_evaluation_a && hr_process_evaluation_a.texto != '_sin valor_'
                hay_alternativas = true
              end

              if hay_alternativas
                colspan_ += 1
              end

              if hr_process_evaluation_q.comentario
                colspan_ += 1
              end

              max_colspan_ = colspan_ if colspan_ > max_colspan_
              colspan_ = 1

            end

            c_aux_background = 0
            c_aux_background_array = Array.new

            table_discrete = Array.new

            table_discrete_row = Array.new

            table_discrete_row = [content: '<font size="12"><b><color rgb="ffffff">'+position.to_s+'. '+hr_process_evaluation_q.texto+'</color></b></font>', colspan: max_colspan_]
            table_discrete.push table_discrete_row

            if hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).count > 0

              aux = 0

              hr_process_evaluation.hr_process_evaluation_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).each do |hr_process_evaluation_q_hijo|

                aux += 1

                table_discrete_row = Array.new

                table_discrete_row = [content: '<b>'+position.to_s+'.'+aux.to_s+'. '+hr_process_evaluation_q_hijo.texto+'</b>', colspan: max_colspan_]
                table_discrete.push table_discrete_row

                c_aux_background += 1
                c_aux_background_array.push c_aux_background

                hr_process_evaluation_a = hr_process_evaluation_q_hijo.hr_process_evaluation_as.first

                hay_alternativas = false

                if hr_process_evaluation_a && hr_process_evaluation_a.texto != '_sin valor_'
                  hay_alternativas = true
                end

                relaciones.each do |relacion|

                  hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q_hijo.id).each do |hr_process_assessment_q|

                    relacion = nil if relacion == 'auto'

                    if hr_process_assessment_q.hr_process_assessment_e && hr_process_assessment_q.hr_process_assessment_e.tipo_rel == relacion && (only_for == nil || only_for == relacion)

                      table_discrete_row = Array.new

                      c_aux_background += 1

                      if relacion.nil?
                        texto_ = 'Autoevaluación'
                      else
                        texto_ = lista_relaciones[relacion]
                      end

                      table_discrete_row.push texto_

                      if hay_alternativas
                        table_discrete_row.push hr_process_assessment_q.hr_process_evaluation_a ? hr_process_assessment_q.hr_process_evaluation_a.texto : ''
                      end

                      if hr_process_evaluation_q_hijo.comentario
                        table_discrete_row.push hr_process_assessment_q.comentario
                      end

                      table_discrete.push table_discrete_row

                    end

                  end

                end

              end

            else

              hr_process_evaluation_a = hr_process_evaluation_q.hr_process_evaluation_as.first

              hay_alternativas = false

              if hr_process_evaluation_a && hr_process_evaluation_a.texto != '_sin valor_' 
                hay_alternativas = true
              end 

              relaciones.each do |relacion| 

                hr_process_user.hr_process_assessment_qs.where('hr_process_evaluation_q_id = ?', hr_process_evaluation_q.id).each do |hr_process_assessment_q|

                  relacion = nil if relacion == 'auto'

                  if hr_process_assessment_q.hr_process_assessment_e.tipo_rel == relacion && (only_for == nil || only_for == relacion)

                    c_aux_background += 1

                    table_discrete_row = Array.new

                    if relacion.nil?
                      texto_ = 'Autoevaluación'
                    else
                      texto_ = lista_relaciones[relacion]
                    end

                    table_discrete_row.push texto_

                    if hay_alternativas
                      table_discrete_row.push hr_process_assessment_q.hr_process_evaluation_a ? hr_process_assessment_q.hr_process_evaluation_a.texto : ''
                    end

                    if hr_process_evaluation_q.comentario
                      table_discrete_row.push hr_process_assessment_q.comentario
                    end

                    table_discrete.push table_discrete_row

                  end

                end

              end

            end

            if max_colspan_ == 3
              pdf.table(table_discrete, :header => false, :width => 520, :column_widths => [200,160,160], :cell_style => { :inline_format => true }) do |table|
                table.column(0).row(0).background_color = '333333'

                c_aux_background_array.each do |n|
                  table.row(n).background_color = 'dddddd'
                end

              end
            else
              pdf.table(table_discrete, :header => false, :width => 520, :column_widths => [200,320], :cell_style => { :inline_format => true }) do |table|
                table.column(0).row(0).background_color = '333333'

                c_aux_background_array.each do |n|
                  table.row(n).background_color = 'dddddd'
                end

              end
            end


            pdf.move_down 20

            position += 1

          end

        end

      end

      return position

    end




  def xls_for_validation_everybody_admin(pe_process, pe_evaluation)

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Validación_'+pe_evaluation.name).slice(0,31)) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Empresa'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Definición realizada por '
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Validación realizada por '
        filaHeader <<  headerCenter
        cols_widths << 20

        last_pe_element = nil

        pe_evaluation.pe_elements.each do |pe_element|

          last_pe_element = pe_element

          if pe_element.assessed

            if pe_element.simple?

              fila << pe_element.name
              filaHeader <<  headerCenter
              cols_widths << 20

            else

              pe_element.pe_element_descriptions.each do |pe_element_description|

                fila << pe_element_description.name
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

            fila << 'peso'
            filaHeader <<  headerCenter
            cols_widths << 10

          else

            fila << pe_element.name
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        if last_pe_element && last_pe_element.assessed

          if last_pe_element.assessment_method == 2

            fila << 'tipo de indicador'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'unidad de medida'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'meta'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'validar (si/no)'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'comentario'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        step_number = 1
        before_accept = true

        validators_id = pe_evaluation.pe_definition_by_user_validators.where('step_number = ? and before_accept = ?', step_number, before_accept).pluck(:validator_id).uniq

        validators = User.where('id in(?)',validators_id)

        validators.each do |current_validator|

          pe_evaluation.pe_definition_by_user_validators_by_validator_ordered_by_user(current_validator, step_number, before_accept).each_with_index do |pe_definition_by_user_validator, index|

            status_validate = pe_evaluation.pe_member_status_to_be_validated(pe_definition_by_user_validator.pe_member, step_number, before_accept)

            if status_validate  == 'ready' || status_validate == 'finished' || status_validate == 'not_validated'

              row_creator = ''
              pe_evaluation.pe_creators_of_pe_member_questions(pe_definition_by_user_validator.pe_member).each do |creator|
                row_creator += creator.apellidos+', '+creator.nombre
              end

              if pe_evaluation.pe_elements.size == 2

                pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_definition_by_user_validator.pe_member).each do |pe_question_1st_level|

                  pe_evaluation.pe_questions_by_question(pe_question_1st_level).each do |pe_question_2nd_level|

                    #pe_evaluation.pe_questions_by_question(pe_question_2nd_level).each do |pe_question_3rd_level|

                    fila = Array.new
                    fileStyle = Array.new

                    fila << pe_question_2nd_level.id
                    fileStyle << fieldLeft

                    fila << pe_definition_by_user_validator.pe_member.user.codigo
                    fileStyle << fieldLeft

                    fila << pe_definition_by_user_validator.pe_member.user.apellidos
                    fileStyle << fieldLeft

                    fila << pe_definition_by_user_validator.pe_member.user.nombre
                    fileStyle << fieldLeft

                    pe_c = pe_definition_by_user_validator.pe_member.pe_member_characteristic_by_id(5)

                    fila << pe_c.value if pe_c
                    fileStyle << fieldLeft

                    fila << row_creator
                    fileStyle << fieldLeft

                    fila << current_validator.apellidos+', '+current_validator.nombre
                    fileStyle << fieldLeft

                    fila << pe_question_1st_level.description
                    fileStyle << fieldLeft

                    #fila << pe_question_2nd_level.description
                    #fileStyle << fieldLeft

                    if pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator

                      fila << pe_question_2nd_level.kpi_indicator.name
                      fileStyle << fieldLeft

                    else

                      fila << pe_question_2nd_level.description
                      fileStyle << fieldLeft

                    end

                    fila << number_with_precision(pe_question_2nd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
                    fileStyle << fieldLeft

                    if pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator

                      fila << 'KPI'
                      fileStyle << fieldLeft

                      fila << pe_question_2nd_level.kpi_indicator.unit
                      fileStyle << fieldLeft

                      if pe_question_2nd_level.kpi_indicator.goal
                        fila << number_with_precision(pe_question_2nd_level.kpi_indicator.goal, precision: 6, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                        fileStyle << fieldLeft
                      else
                        fila << ''
                        fileStyle << fieldLeft
                      end

                    else

                      indicator_type = case pe_question_2nd_level.indicator_type
                                         when 0
                                           'Indicador directo'
                                         when 1
                                           'Indicador inverso'
                                         when 2
                                           'Indicador por desviación +/-'
                                         when 3
                                           'Indicador por rangos'
                                         when 4
                                           'Indicador discreto'
                                       end

                      fila << indicator_type
                      fileStyle << fieldLeft

                      fila << pe_question_2nd_level.indicator_unit
                      fileStyle << fieldLeft

                      if pe_question_2nd_level.indicator_type != 4
                        fila << number_with_precision(pe_question_2nd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                        fileStyle << fieldLeft
                      else
                        if pe_question_2nd_level.indicator_discrete_goal
                          fila << pe_question_2nd_level.indicator_discrete_goal
                          fileStyle << fieldLeft
                        else
                          fila << ''
                          fileStyle << fieldLeft
                        end
                      end

                    end

                    pe_question_validation = pe_question_2nd_level.pe_question_validation(step_number, before_accept)

                    if (pe_question_validation && pe_question_validation.validated) || pe_question_validation.nil?

                      fila << 'si'
                      fileStyle << fieldLeft

                      fila << ''
                      fileStyle << fieldLeft

                    elsif pe_question_validation

                      fila << 'no'
                      fileStyle << fieldLeft

                      fila << pe_question_validation.comment
                      fileStyle << fieldLeft

                    else

                      fila << ''
                      fileStyle << fieldLeft

                      fila << ''
                      fileStyle << fieldLeft

                    end

                    fila << pe_question_validation.created_at.strftime('%Y-%m-%d') if pe_question_validation
                    fileStyle << fieldLeft

                    reporte_excel.add_row fila, :style => fileStyle, height: 20

                    fila_actual += 1

                    #end

                  end

                end

              end

            end

          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def xls_rol_privado(pe_process)

    pe_members_evaluated = pe_process.evaluated_members_segmented_to_report(user_connected)

    pe_evaluations = pe_process.pe_not_informative_evaluations

    pe_characteristics = pe_process.pe_characteristics_rep_consolidated_final_results
    characteristics = Array.new

    pe_mc_array = Array.new(pe_members_evaluated.size) { Array.new(pe_characteristics.size) }

    evals_weights_array = Array.new(pe_members_evaluated.size) { Array.new(pe_evaluations.size) }

    pe_assessment_evaluations_boss_array_obj = Array.new(pe_members_evaluated.size) { Array.new(pe_evaluations.size) }

    pe_assessment_final_evaluations_array_obj = Array.new(pe_members_evaluated.size) { Array.new(pe_evaluations.size) }
    pe_assessment_final_evaluations_array = Array.new(pe_members_evaluated.size) { Array.new(pe_evaluations.size) }

    pe_assessment_dimensions_array = Array.new

    pe_evaluation_groups = pe_process.pe_evaluation_groups
    pe_evaluation_groups_weights_array = Array.new(pe_members_evaluated.size) { Array.new(pe_evaluation_groups.size) }


    pe_dimension_x = pe_process.dimension_x

    pe_elements_array = Array.new(pe_evaluations.size)

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|



      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      wb.add_worksheet(:name => ('1. Master Pesos')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerCenter
        cols_widths << 10

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        pe_characteristics.each do |pe_characteristic|
          characteristics.push pe_characteristic.characteristic
          fila << pe_characteristic.characteristic.nombre
          filaHeader <<  headerCenter
          cols_widths << 20
        end

        pe_evaluations.each do |pe_evaluation|
          fila << pe_evaluation.name
          filaHeader <<  headerCenter
          cols_widths << 15
        end

        fila << 'Total'
        filaHeader <<  headerCenter
        cols_widths << 10

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila_actual = 1

        pe_members_evaluated.each_with_index do |pe_member_evaluated, index_me|

          pe_member_rel_boss = pe_member_evaluated.pe_member_rel_boss

          pe_assessment_dimensions_array.push (pe_member_evaluated.step_assessment? ? pe_member_evaluated.pe_assessment_dimension(pe_dimension_x).percentage : nil)

          user = pe_member_evaluated.user

          fila = Array.new
          fileStyle = Array.new

          fila << fila_actual
          fileStyle << fieldLeft

          fila << user.codigo
          fileStyle << fieldLeft

          fila << user.apellidos
          fileStyle << fieldLeft

          fila << user.nombre
          fileStyle << fieldLeft

          characteristics.each_with_index do |characteristic, index_c|
            pe_mc = pe_member_evaluated.pe_member_characteristic_by_id characteristic.id
            fila << (pe_mc ? pe_mc.value : '')
            fileStyle << fieldLeft

            pe_mc_array[index_me][index_c] = pe_mc ? pe_mc.value : ''

          end

          s = 0
          pe_evaluations.each_with_index do |pe_evaluation, index_e|
            w = pe_member_evaluated.weight_in_evaluation pe_evaluation
            s += w if w > 0
            fila << (w != -1 ? w : nil)
            fileStyle << fieldLeft

            evals_weights_array[index_me][index_e] = w != -1 ? w : nil

            afe = (w == -1 ? nil : pe_member_evaluated.pe_assessment_final_evaluation(pe_evaluation))

            pe_assessment_final_evaluations_array_obj[index_me][index_e] = afe

            pe_assessment_final_evaluations_array[index_me][index_e] = afe ? afe.percentage : nil

            pe_assessment_evaluations_boss_array_obj[index_me][index_e] = pe_member_rel_boss.pe_assessment_evaluation(pe_evaluation) if pe_member_rel_boss

            pe_elements_array[index_e] = pe_evaluation.pe_elements

          end

          pe_evaluation_groups.each_with_index do |pe_evaluation_group, index_g|

            t = 0
            pe_evaluation_group.pe_evaluations.each do |pe_evaluation|
              w = pe_member_evaluated.weight_in_evaluation(pe_evaluation)
              t += w if w > 0
            end

            pe_evaluation_groups_weights_array[index_me][index_g] = t

          end



          fila << s
          fileStyle << fieldLeft

          reporte_excel.add_row fila, :style => fileStyle, height: 20

          fila_actual += 1

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

      pe_evaluation_group_1 = pe_evaluation_groups.first

      wb.add_worksheet(:name => ('2. Master '+pe_evaluation_group_1.name)) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerCenter
        cols_widths << 10

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        characteristics.each do |characteristic|
          fila << characteristic.nombre
          filaHeader <<  headerCenter
          cols_widths << 20
        end

        fila << 'Evaluación'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Peso evaluación'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Sub bloque'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Peso sub bloque'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Indicador'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Peso indicador'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Potencia'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Apalancamiento'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Unidad'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Meta'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Logro'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Porcentage de logro'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Nota evaluación'
        filaHeader <<  headerCenter
        cols_widths << 15

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila_actual = 1

        pe_members_evaluated.each_with_index do |pe_member_evaluated, index_me|

          user = pe_member_evaluated.user

          pe_evaluations.each_with_index do |pe_evaluation, index_e|

            if pe_evaluation.pe_evaluation_group_id == pe_evaluation_group_1.id

              prev_grouped_ids = Array.new

              pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_member_evaluated).each do |pe_question_1|

                if pe_elements_array[index_e].size == 2

                  s = 0

                  pe_evaluation.pe_questions_by_question(pe_question_1).each { |pe_question_2| s += pe_question_2.weight }

                  pe_evaluation.pe_questions_by_question(pe_question_1).each do |pe_question_2|

                    if pe_question_2.questions_grouped_id.nil? || prev_grouped_ids.exclude?(pe_question_2.questions_grouped_id)

                      fila = Array.new
                      fileStyle = Array.new

                      fila << fila_actual
                      fileStyle << fieldLeft

                      fila << user.codigo
                      fileStyle << fieldLeft

                      fila << user.apellidos
                      fileStyle << fieldLeft

                      fila << user.nombre
                      fileStyle << fieldLeft

                      characteristics.size.times do |index_c|
                        fila << pe_mc_array[index_me][index_c]
                        fileStyle << fieldLeft
                      end

                      fila << pe_evaluation.name
                      fileStyle << fieldLeft

                      fila << evals_weights_array[index_me][index_e]
                      fileStyle << fieldLeft

                      fila << pe_question_1.description
                      fileStyle << fieldLeft

                      fila << s
                      fileStyle << fieldLeft

                      fila << pe_question_2.description
                      fileStyle << fieldLeft

                      fila << pe_question_2.weight
                      fileStyle << fieldLeft

                      if pe_question_2.indicator_type == 5

                        fila << pe_question_2.power
                        fileStyle << fieldLeft

                        fila << pe_question_2.leverage
                        fileStyle << fieldLeft

                      else

                        fila << nil
                        fileStyle << fieldLeft

                        fila << nil
                        fileStyle << fieldLeft

                      end

                      fila << pe_question_2.indicator_unit
                      fileStyle << fieldLeft

                      if pe_question_2.indicator_type == 4

                        #fila << pe_question_2.indicator_discrete_goal
                        fila << pe_question_2.indicator_goal
                        fileStyle << fieldLeft

                      else

                        fila << pe_question_2.indicator_goal
                        fileStyle << fieldLeft

                      end

                      if pe_question_2.questions_grouped_id

                        pe_assessment_final_question = pe_assessment_final_evaluations_array_obj[index_me][index_e] ? pe_assessment_final_evaluations_array_obj[index_me][index_e].pe_assessment_final_question(pe_question_2) : nil

                        pe_assessment_question = pe_assessment_evaluations_boss_array_obj[index_me][index_e] ? pe_assessment_evaluations_boss_array_obj[index_me][index_e].pe_assessment_question(pe_question_2) : nil

                        if pe_assessment_question && pe_assessment_final_question && pe_assessment_question.percentage == pe_assessment_final_question.percentage

                          if pe_question_2.indicator_type == 4

                            #fila << (pe_assessment_question ? pe_assessment_question.indicator_discrete_achievement : nil)
                            fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                            fileStyle << fieldLeft

                          else

                            fila << (pe_assessment_question ? pe_assessment_question.indicator_achievement : nil)
                            fileStyle << fieldLeft

                          end


                          fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                          fileStyle << fieldLeft

                        else

                          fila << (pe_assessment_final_question ? pe_assessment_final_question.percentage : nil)
                          fileStyle << fieldLeft

                          fila << (pe_assessment_final_question ? pe_assessment_final_question.percentage : nil)
                          fileStyle << fieldLeft

                        end

                      else

                        pe_assessment_final_question = pe_assessment_final_evaluations_array_obj[index_me][index_e] ? pe_assessment_final_evaluations_array_obj[index_me][index_e].pe_assessment_final_question(pe_question_2) : nil

                        pe_assessment_question = pe_assessment_evaluations_boss_array_obj[index_me][index_e] ? pe_assessment_evaluations_boss_array_obj[index_me][index_e].pe_assessment_question(pe_question_2) : nil

                        if pe_assessment_question

                          if pe_question_2.indicator_type == 4

                            #fila << (pe_assessment_question ? pe_assessment_question.indicator_discrete_achievement : nil)
                            fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                            fileStyle << fieldLeft

                          else

                            fila << (pe_assessment_question ? pe_assessment_question.indicator_achievement : nil)
                            fileStyle << fieldLeft

                          end

                          fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                          fileStyle << fieldLeft

                        elsif pe_assessment_final_question

                          fila << (pe_assessment_final_question ? pe_assessment_final_question.percentage : nil)
                          fileStyle << fieldLeft

                          fila << (pe_assessment_final_question ? pe_assessment_final_question.percentage : nil)
                          fileStyle << fieldLeft

                        end

                      end

                      fila << pe_assessment_final_evaluations_array[index_me][index_e]
                      fileStyle << fieldLeft

                      reporte_excel.add_row fila, :style => fileStyle, height: 20

                      fila_actual += 1

                      prev_grouped_ids.push(pe_question_2.questions_grouped_id) if pe_question_2.questions_grouped_id

                    end

                  end

                else

                  fila = Array.new
                  fileStyle = Array.new

                  fila << fila_actual
                  fileStyle << fieldLeft

                  fila << user.codigo
                  fileStyle << fieldLeft

                  fila << user.apellidos
                  fileStyle << fieldLeft

                  fila << user.nombre
                  fileStyle << fieldLeft

                  characteristics.size.times do |index_c|
                    fila << pe_mc_array[index_me][index_c]
                    fileStyle << fieldLeft
                  end

                  fila << pe_evaluation.name
                  fileStyle << fieldLeft

                  fila << evals_weights_array[index_me][index_e]
                  fileStyle << fieldLeft

                  fila << nil
                  fileStyle << fieldLeft

                  fila << nil
                  fileStyle << fieldLeft

                  fila << pe_question_1.description
                  fileStyle << fieldLeft

                  fila << pe_question_1.weight
                  fileStyle << fieldLeft

                  if pe_question_1.indicator_type == 5

                    fila << pe_question_1.power
                    fileStyle << fieldLeft

                    fila << pe_question_1.leverage
                    fileStyle << fieldLeft

                  else

                    fila << nil
                    fileStyle << fieldLeft

                    fila << nil
                    fileStyle << fieldLeft

                  end

                  fila << pe_question_1.indicator_unit
                  fileStyle << fieldLeft

                  if pe_question_1.indicator_type == 4

                    #fila << pe_question_1.indicator_discrete_goal
                    fila << pe_question_1.indicator_goal
                    fileStyle << fieldLeft

                  else

                    fila << pe_question_1.indicator_goal
                    fileStyle << fieldLeft

                  end

                  if pe_question_1.questions_grouped_id

                    pe_assessment_final_question = pe_assessment_final_evaluations_array_obj[index_me][index_e] ? pe_assessment_final_evaluations_array_obj[index_me][index_e].pe_assessment_final_question(pe_question_1) : nil

                    pe_assessment_question = pe_assessment_evaluations_boss_array_obj[index_me][index_e] ? pe_assessment_evaluations_boss_array_obj[index_me][index_e].pe_assessment_question(pe_question_1) : nil

                    if pe_assessment_question && pe_assessment_final_question && pe_assessment_question.percentage == pe_assessment_final_question.percentage

                      if pe_question_1.indicator_type == 4

                        #fila << (pe_assessment_question ? pe_assessment_question.indicator_discrete_achievement : nil)
                        fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                        fileStyle << fieldLeft

                      else

                        fila << (pe_assessment_question ? pe_assessment_question.indicator_achievement : nil)
                        fileStyle << fieldLeft

                      end

                      fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                      fileStyle << fieldLeft

                    else

                      fila << (pe_assessment_final_question ? pe_assessment_final_question.percentage : nil)
                      fileStyle << fieldLeft

                      fila << (pe_assessment_final_question ? pe_assessment_final_question.percentage : nil)
                      fileStyle << fieldLeft

                    end

                  else

                    pe_assessment_question = pe_assessment_evaluations_boss_array_obj[index_me][index_e] ? pe_assessment_evaluations_boss_array_obj[index_me][index_e].pe_assessment_question(pe_question_1) : nil

                    if pe_question_1.indicator_type == 4

                      #fila << (pe_assessment_question ? pe_assessment_question.indicator_discrete_achievement : nil)
                      fila << pe_question_1.id #(pe_assessment_question ? pe_assessment_question.percentage : nil)
                      fileStyle << fieldLeft

                    else

                      fila << (pe_assessment_question ? pe_assessment_question.indicator_achievement : nil)
                      fileStyle << fieldLeft

                    end

                    fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                    fileStyle << fieldLeft

                  end

                  fila << pe_assessment_final_evaluations_array[index_me][index_e]
                  fileStyle << fieldLeft

                  reporte_excel.add_row fila, :style => fileStyle, height: 20

                  fila_actual += 1

                end



              end

            end

          end



        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

      wb.add_worksheet(:name => ('3. Master Especiales')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerCenter
        cols_widths << 10

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        characteristics.each do |characteristic|
          fila << characteristic.nombre
          filaHeader <<  headerCenter
          cols_widths << 20
        end

        fila << alias_username+ ' - Evaluador'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos - Evaluador'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre - Evaluador'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Sub bloque'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Indicador'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Ponderación'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Potencia'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Apalancamiento'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Unidad'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Meta'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Logro'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Porcentage de logro'
        filaHeader <<  headerCenter
        cols_widths << 15

        fila << 'Nota final indicador'
        filaHeader <<  headerCenter
        cols_widths << 15

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila_actual = 1

        pe_members_evaluated.each_with_index do |pe_member_evaluated, index_me|

          user = pe_member_evaluated.user

          pe_evaluations.each_with_index do |pe_evaluation, index_e|

            if pe_evaluation.pe_evaluation_group_id == pe_evaluation_group_1.id

              if pe_elements_array[index_e].size == 2

                pe_evaluation.pe_questions_by_pe_member_any_evaluator_by_element(pe_member_evaluated, pe_elements_array[index_e][1]).each do |pe_question_2|

                  pe_question_1 = pe_question_2.pe_question

                  if pe_question_2.questions_grouped_id

                    pe_member_rel_evaluator = pe_member_evaluated.pe_member_rel_is_evaluated_by_pe_member pe_question_2.pe_member_evaluator

                    if pe_member_rel_evaluator

                      user_evaluator = pe_member_rel_evaluator.pe_member_evaluator.user

                      pe_assessment_evaluation = pe_member_rel_evaluator.pe_assessment_evaluation pe_evaluation

                      fila = Array.new
                      fileStyle = Array.new

                      fila << fila_actual
                      fileStyle << fieldLeft

                      fila << user.codigo
                      fileStyle << fieldLeft

                      fila << user.apellidos
                      fileStyle << fieldLeft

                      fila << user.nombre
                      fileStyle << fieldLeft

                      characteristics.size.times do |index_c|
                        fila << pe_mc_array[index_me][index_c]
                        fileStyle << fieldLeft
                      end

                      fila << user_evaluator.codigo
                      fileStyle << fieldLeft

                      fila << user_evaluator.apellidos
                      fileStyle << fieldLeft

                      fila << user_evaluator.nombre
                      fileStyle << fieldLeft

                      fila << pe_question_1.description
                      fileStyle << fieldLeft

                      fila << pe_question_2.description
                      fileStyle << fieldLeft

                      fila << pe_member_rel_evaluator.weight
                      fileStyle << fieldLeft

                      if pe_question_2.indicator_type == 5

                        fila << pe_question_2.power
                        fileStyle << fieldLeft

                        fila << pe_question_2.leverage
                        fileStyle << fieldLeft

                      else

                        fila << nil
                        fileStyle << fieldLeft

                        fila << nil
                        fileStyle << fieldLeft

                      end

                      fila << pe_question_2.indicator_unit
                      fileStyle << fieldLeft

                      if pe_question_2.indicator_type == 4

                        #fila << pe_question_2.indicator_discrete_goal
                        fila << pe_question_2.indicator_goal
                        fileStyle << fieldLeft

                      else

                        fila << pe_question_2.indicator_goal
                        fileStyle << fieldLeft

                      end

                      pe_assessment_question = pe_assessment_evaluation ? pe_assessment_evaluation.pe_assessment_question(pe_question_2) : nil

                      if pe_question_2.indicator_type == 4

                        #fila << (pe_assessment_question ? pe_assessment_question.indicator_discrete_achievement : nil)
                        fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                        fileStyle << fieldLeft

                      else

                        fila << (pe_assessment_question ? pe_assessment_question.indicator_achievement : nil)
                        fileStyle << fieldLeft

                      end

                      fila << (pe_assessment_question ? pe_assessment_question.percentage : nil)
                      fileStyle << fieldLeft


                      pe_assessment_final_question = pe_assessment_final_evaluations_array_obj[index_me][index_e] ? pe_assessment_final_evaluations_array_obj[index_me][index_e].pe_assessment_final_question(pe_question_2) : nil
                      fila << (pe_assessment_final_question ? pe_assessment_final_question.percentage : nil)
                      fileStyle << fieldLeft

                      reporte_excel.add_row fila, :style => fileStyle, height: 20

                      fila_actual += 1

                    end

                  end

                end

              end

            end

          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

      wb.add_worksheet(:name => ('4. Master Nota Final')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerCenter
        cols_widths << 10

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        characteristics.each do |characteristic|
          fila << characteristic.nombre
          filaHeader <<  headerCenter
          cols_widths << 20
        end

        pe_evaluations.each do |pe_evaluation|

          next if pe_evaluation.weight == 0

          fila << pe_evaluation.name
          filaHeader <<  headerCenter
          cols_widths << 15

          fila << 'Peso '+pe_evaluation.name
          filaHeader <<  headerCenter
          cols_widths << 15

        end

        pe_evaluation_groups.each do |pe_evaluation_group|

          fila << pe_evaluation_group.name
          filaHeader <<  headerCenter
          cols_widths << 15

          fila << 'Peso '+pe_evaluation_group.name
          filaHeader <<  headerCenter
          cols_widths << 15

        end

        fila << 'Nota final'
        filaHeader <<  headerCenter
        cols_widths << 10

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila_actual = 1

        pe_members_evaluated.each_with_index do |pe_member_evaluated, index_me|

          user = pe_member_evaluated.user

          fila = Array.new
          fileStyle = Array.new

          fila << fila_actual
          fileStyle << fieldLeft

          fila << user.codigo
          fileStyle << fieldLeft

          fila << user.apellidos
          fileStyle << fieldLeft

          fila << user.nombre
          fileStyle << fieldLeft

          characteristics.size.times do |index_c|
            fila << pe_mc_array[index_me][index_c]
            fileStyle << fieldLeft
          end

          pe_evaluations.size.times do |index_e|

            next if pe_evaluations[index_e].weight == 0

            fila << pe_assessment_final_evaluations_array[index_me][index_e]
            fileStyle << fieldLeft

            fila << evals_weights_array[index_me][index_e]
            fileStyle << fieldLeft
          end

          pe_evaluation_groups.each_with_index do |pe_evaluation_group, index_g|

            pe_assessment_group = pe_member_evaluated.pe_assessment_group(pe_evaluation_group)

            fila << (pe_assessment_group ? pe_assessment_group.percentage : nil)
            fileStyle << fieldLeft

            fila << pe_evaluation_groups_weights_array[index_me][index_g]
            fileStyle << fieldLeft

          end

          fila << pe_assessment_dimensions_array[index_me]
          fileStyle << fieldLeft

          reporte_excel.add_row fila, :style => fileStyle, height: 20

          fila_actual += 1

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end


      wb.add_worksheet(:name => ('5. Nota Final')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerCenter
        cols_widths << 10

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        characteristics.each do |characteristic|
          fila << characteristic.nombre
          filaHeader <<  headerCenter
          cols_widths << 20
        end

        fila << 'Nota final'
        filaHeader <<  headerCenter
        cols_widths << 10

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila_actual = 1

        pe_members_evaluated.each_with_index do |pe_member_evaluated, index_me|

          user = pe_member_evaluated.user

          fila = Array.new
          fileStyle = Array.new

          fila << fila_actual
          fileStyle << fieldLeft

          fila << user.codigo
          fileStyle << fieldLeft

          fila << user.apellidos
          fileStyle << fieldLeft

          fila << user.nombre
          fileStyle << fieldLeft

          characteristics.size.times do |index_c|
            fila << pe_mc_array[index_me][index_c]
            fileStyle << fieldLeft
          end

          fila << pe_assessment_dimensions_array[index_me]
          fileStyle << fieldLeft

          reporte_excel.add_row fila, :style => fileStyle, height: 20

          fila_actual += 1

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

end
