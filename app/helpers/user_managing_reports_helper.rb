module UserManagingReportsHelper

  include UsersModule

  def photos_user_active
    users = list_active_users
    reporte = Axlsx::Package.new
    wb = reporte.workbook
    wb.use_shared_strings = true

    wb.styles do |s|
      black_title, grey_title, normal_cell, date_cell = generate_styles s

      wb.add_worksheet(:name => ('usuarios_fotos')) do |sheet|

        fileStyle = Array.new(7,black_title)
        titles = [alias_username, 'Apellidos', 'Nombres', 'Email', 'Empleado', 'Estado', 'Foto']
        sheet.add_row titles, :style => fileStyle

        fileStyle = Array.new(7,normal_cell)
        users.each do |user|
          record = [user.codigo, user.apellidos, user.nombre, user.email, user.employee ? 'Empleado' : 'Externo', user.activo ? 'Activo' : 'Inactivo', user.foto ? 'Si' : 'No']
          sheet.add_row record, :style => fileStyle
        end
      end
    end
    return reporte
  end


  def active_user_characteristics

    excel_letters = generate_excel_letters

    characteristics = Array.new
    characteristic_types = Array.new
    characteristic_types_sizes = Array.new

    CharacteristicType.all.each do |characteristic_type|
      ec = characteristic_type.excel_report_available_characteristics
      characteristics += ec
      if ec.size > 0
        characteristic_types.push characteristic_type
        characteristic_types_sizes.push ec.size-1
      end
    end

    characteristics += Characteristic.excel_report_available_characteristics

    users = list_active_users

    reporte = Axlsx::Package.new
    wb = reporte.workbook
    wb.use_shared_strings = true

    wb.styles do |s|

      black_title, grey_title, normal_cell, date_cell = generate_styles s

      wb.add_worksheet(name: ('Usuarios Activos') ) do |sheet|

        if characteristic_types.size > 0
          styles = Array.new(6,black_title)
          values = Array.new(6,'')
          pos = 6
          characteristic_types.each_with_index do |ct, index_ct|
            sheet.merge_cells excel_letters[pos].to_s+'1:'+excel_letters[pos+characteristic_types_sizes[index_ct]].to_s+'1'
            values.push ct.name
            styles.push black_title
            characteristic_types_sizes[index_ct].times do
              values.push nil
              styles.push black_title
            end
            pos += characteristic_types_sizes[index_ct]+1
          end
          sheet.add_row values, :style => styles
        end

        titles = [alias_username, 'Apellidos', 'Nombres', 'Email', 'Empleado', 'Estado']
        characteristics.each { |characteristic| titles.push characteristic.nombre }

        sheet.add_row titles, :style => Array.new(6+characteristics.size,grey_title)

        users.each_with_index do |user, index_u|
          styles = Array.new(6, normal_cell)
          record = [user.codigo, user.apellidos, user.nombre, user.email, user.employee ? 'Empleado' : 'Externo', user.activo ? 'Activo' : 'Inactivo']
          characteristics.each do |characteristic|
            record.push user.excel_formatted_characteristic_value(characteristic)
            styles.push characteristic.data_type_id == 2 ? date_cell : normal_cell
          end
          sheet.add_row record, style: styles
          break if index_u == 0
        end

      end

    end
    return reporte
  end

  private

  def generate_excel_letters
    %w('a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bw bx by bz')
  end

  def generate_styles(s)
    black_title = s.add_style :fg_color => '000000',
                              :sz => 10,
                              :b => true,
                              :alignment => { :horizontal => :center, :vertical => :center},
                              :border => Axlsx::STYLE_THIN_BORDER

    grey_title = s.add_style :bg_color => '9c9c9c',
                             :fg_color => '000000',
                             :sz => 10,
                             :alignment => { :horizontal => :center, :vertical => :center},
                             :b => true, :border => Axlsx::STYLE_THIN_BORDER

    normal_cell = s.add_style :b => false, :border => Axlsx::STYLE_THIN_BORDER,
                              :alignment => { :horizontal => :center, :vertical => :center},
                              :sz => 10

    date_cell = s.add_style :b => false, :border => Axlsx::STYLE_THIN_BORDER,
                            :format_code => "dd-mm-yyyy",
                            :alignment => { :horizontal => :center, :vertical => :center},
                            :sz => 10

    return black_title, grey_title, normal_cell, date_cell
  end

end
