module JmCharacteristicsHelper
  def link_to_remove_jm_subchars(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_subchar(this)")
  end

  def link_to_add_jm_subchars(name, f)
    subchar = JmSubcharacteristic.new
    fields = f.fields_for(:jm_subcharacteristics, subchar, :child_index => "new_subchar") do |builder|
      render('jm_characteristics/form_subchar', :f => builder)
    end
    link_to_function(name, "add_subchar(this, \"#{escape_javascript(fields)}\")")
  end
end
