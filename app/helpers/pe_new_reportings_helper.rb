module PeNewReportingsHelper

  def rep_excel_final_results_evaluation(pe_evaluation, pe_process)

    reporte = Axlsx::Package.new
    wb = reporte.workbook
    wb.use_shared_strings = true

    wb.styles do |s|
      black_title, grey_title, normal_cell, date_cell = generate_styles s

      wb.add_worksheet(:name => ('Resultados')) do |sheet|
        values = []
        styles = Array.new(17, black_title)

        if pe_process.of_persons?
          values.push alias_username
          values.push 'Evaluado'

          pe_process.pe_characteristics_rep_consolidated_final_results.each do |pe_characteristic|
            values.push pe_characteristic.characteristic.nombre
          end

          values.push alias_username+' evaluador'
          values.push 'Evaluador'
        else
          values.push 'Área evaluada'
          values.push 'Responsable del área evaluada'
          values.push 'Área evaluadora'
          values.push 'Responsable del área evaluadora'
        end

        values.push 'Relación'
        pe_elements = pe_evaluation.pe_elements

        pe_elements.each do |pe_element|
          if pe_element.simple
            values.push pe_element.name
          else
            pe_element.pe_element_descriptions.each do |pe_element_description|
              values.push pe_element_description.name
            end
          end
        end
        pe_element_last = pe_elements.last
        if pe_element_last && pe_element_last.assessment_method == 2
          if pe_element_last.allow_note?
            values.push pe_element_last.alias_note
          end
          values.push 'Tipo'
          values.push 'Peso'
          values.push 'Meta'

        end
        values.push 'Valoración'
        values.push 'Logro'
        values.push 'Última actualización'

        if pe_element_last && pe_element_last.alias_comment
          values.push pe_element_last.alias_comment
        end
        if pe_element_last && pe_element_last.alias_comment_2
          values.push pe_element_last.alias_comment_2
        end
        sheet.add_row values, :style => styles

        pe_process.evaluated_members_segmented_to_report(user_connected).each_with_index do |pe_member_evaluated, index_e|
          @pe_member_evaluated = pe_member_evaluated
          if pe_member_evaluated.has_to_be_assessed_in_evaluation(pe_evaluation)
            pe_member_evaluated.pe_member_rels_is_evaluated_ordered_rel.each do |pe_member_is_evaluated_rel|
              @pe_rel = pe_member_is_evaluated_rel.pe_rel
              if pe_evaluation.has_pe_evaluation_rel(@pe_rel)
                pe_group = nil
                if pe_evaluation.pe_groups.size > 0
                  pe_member_group = pe_member_evaluated.pe_member_group(pe_evaluation)
                  pe_group = pe_member_group.pe_group if pe_member_group
                end
                @pe_member_evaluator = pe_member_is_evaluated_rel.pe_member_evaluator
                @pe_assessment_evaluation = pe_member_is_evaluated_rel.pe_assessment_evaluation(pe_evaluation)
                pe_questions = pe_evaluation.pe_questions_by_pe_member_1st_level_ready(pe_member_evaluated, @pe_member_evaluator, pe_group, @pe_rel)

                reporte = rep_detailed_final_results_evaluation_item(reporte, normal_cell, date_cell, sheet, pe_questions, '', true, pe_evaluation, pe_member_is_evaluated_rel, pe_evaluation.pe_elements)

              end
            end
          end
        end
      end
    end
    return reporte
  end


  def rep_detailed_final_results_evaluation_item(reporte, normal_cell, date_cell, sheet, pe_questions, pe_questions_parent, first_element, pe_evaluation, pe_member_evaluated_rel, pe_elements)

    pe_questions_parent = Array.new if first_element
    pe_questions.each do |pe_question|

      pe_questions_children = pe_evaluation.pe_questions_by_question_ready(pe_question.id)

      if pe_questions_children.size > 0

        pe_questions_parent.push pe_question
        reporte = rep_detailed_final_results_evaluation_item(reporte, normal_cell, date_cell, sheet, pe_questions_children, pe_questions_parent, false , pe_evaluation, pe_member_evaluated_rel, pe_elements)
        pe_questions_parent.pop

      else
        record = []
        style = []

        if @pe_process.of_persons?
          record.push @pe_member_evaluated.user.codigo
          record.push @pe_member_evaluated.user.apellidos+', '+@pe_member_evaluated.user.nombre
          style.push normal_cell
          style.push normal_cell
          @pe_process.pe_characteristics_rep_consolidated_final_results.each do |pe_characteristic|
            pe_mc = @pe_member_evaluated.pe_member_characteristic_by_id pe_characteristic.characteristic.id
            record.push pe_mc ? pe_mc.value.to_s : ''
            style.push normal_cell
          end
          record.push @pe_member_evaluator.user.codigo
          style.push normal_cell
          record.push @pe_member_evaluator.user.apellidos+', '+@pe_member_evaluator.user.nombre
          style.push normal_cell
        else
          record.push @pe_member_evaluated.pe_area.name
          record.push @pe_member_evaluated.user.codigo+' - '+@pe_member_evaluated.user.apellidos+', '+@pe_member_evaluated.user.nombre
          record.push @pe_member_evaluator.pe_area.name
          record.push @pe_member_evaluator.user.codigo+' - '+@pe_member_evaluator.user.apellidos+'. '+@pe_member_evaluator.user.nombre
          4.times do
            style.push normal_cell
          end
        end
        record.push @pe_rel.alias
        style.push normal_cell

        pe_questions_parent.each do |pe_question_parent|
          if pe_question_parent.pe_element.simple
            record.push pe_question_parent.description ? (ActionView::Base.full_sanitizer.sanitize pe_question_parent.description) : ''
            style.push normal_cell
          else
            pe_question_parent.pe_element.pe_element_descriptions.each do |pe_element_description|
              pe_question_description = pe_question_parent.pe_question_descriptions.where('pe_element_description_id = ?', pe_element_description.id).first
              record.push pe_question_description.description ? (ActionView::Base.full_sanitizer.sanitize pe_question_description.description) : ''
              style.push normal_cell
            end
          end
        end

        if pe_question.pe_element.simple
          record.push pe_question.description ? (ActionView::Base.full_sanitizer.sanitize pe_question.description) : ''
          style.push normal_cell
        else
          pe_question.pe_element.pe_element_descriptions.each do |pe_element_description|
            pe_question_description = pe_question.pe_question_descriptions.where('pe_element_description_id = ?', pe_element_description.id).first
            record.push pe_question_description.description ? (ActionView::Base.full_sanitizer.sanitize pe_question_description.description) : ''
            style.push normal_cell
          end
        end

        if @pe_assessment_evaluation
          pe_assessment_question = @pe_assessment_evaluation.pe_assessment_question(pe_question)
        end

        if pe_assessment_question
          if pe_question.pe_element.assessment_method == 1
            v = pe_assessment_question.pe_alternative ? ActionView::Base.full_sanitizer.sanitize(pe_assessment_question.pe_alternative.description) : ''
          elsif pe_question.pe_element.assessment_method == 2
            v = pe_question.indicator_type == 4 ? ActionView::Base.full_sanitizer.sanitize(pe_assessment_question.indicator_discrete_achievement) : pe_assessment_question.indicator_achievement
          end
        else
          v = ''
        end

        if pe_question.pe_element.assessment_method == 2
          if pe_question.pe_element.allow_note?
            if pe_question.note
              record.push format_html_to_excel(pe_question.note).html_safe
              style.push normal_cell
            end
          end
          record.push pe_question.indicator_type ? pe_question.indicator_type_name : ''
          record.push pe_question.weight ? pe_question.weight : ''
          style.push normal_cell
          style.push normal_cell
          if pe_question.indicator_type == 4
            goal = pe_question.indicator_discrete_goal ? ActionView::Base.full_sanitizer.sanitize(pe_question.indicator_discrete_goal) : ''
            record.push goal
          else
            goal = pe_question.indicator_goal ? pe_question.indicator_goal : ''
          end
          record.push goal.to_s + ' ' + pe_question.indicator_unit.to_s if pe_question.indicator_type != 4
          style.push normal_cell
        else
          if pe_evaluation.id == 33
            4.times do
              record.push ''
              style.push normal_cell
            end
          end
        end

        record.push v
        record.push pe_assessment_question && !pe_question.informative? ? pe_assessment_question.percentage.to_s : ''
        record.push pe_assessment_question && pe_assessment_question.last_update ? (localize pe_assessment_question.last_update, format: :full_date_time_excel) : ''
        style.push normal_cell
        style.push normal_cell
        style.push date_cell

        if pe_question.has_comment
          record.push pe_assessment_question && pe_question.has_comment ? pe_assessment_question.comment : ''
        else
          record.push ''
        end
        style.push normal_cell

        if pe_question.has_comment_2
          record.push pe_assessment_question && pe_question.has_comment_2 ? pe_assessment_question.comment_2 : ''
        else
          record.push ''
        end
        style.push normal_cell

        unless pe_member_evaluated_rel.finished
          record.push '--Evaluación no finalizada--'
        end

        sheet.add_row record, :style => style
      end
    end
    return reporte

  end

  private

  def generate_styles(s)

    black_title = s.add_style :fg_color => '000000',
                              :sz => 10,
                              :b => true,
                              :alignment => { :horizontal => :center, :vertical => :center},
                              :border => Axlsx::STYLE_THIN_BORDER

    grey_title = s.add_style :bg_color => '9c9c9c',
                             :fg_color => '000000',
                             :sz => 10,
                             :alignment => { :horizontal => :center, :vertical => :center},
                             :b => true, :border => Axlsx::STYLE_THIN_BORDER

    normal_cell = s.add_style :b => false, :border => Axlsx::STYLE_THIN_BORDER,
                              :alignment => { :horizontal => :left, :vertical => :center},
                              :sz => 10

    date_cell = s.add_style :b => false, :border => Axlsx::STYLE_THIN_BORDER,
                            :format_code => "dd-mm-yyyy",
                            :alignment => { :horizontal => :left, :vertical => :center},
                            :sz => 10

    return black_title, grey_title, normal_cell, date_cell
  end
end
