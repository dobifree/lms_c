module JmProcessHelper

  def link_to_remove_candidate_answer(name, f)
    f.hidden_field(:_destroy, class: 'jm_answer_destroy_field') +
        link_to(name, '#', onclick: "remove_candidate_answer(this); return false", :class => 'jm_remove_answer_link')
  end

  def link_to_remove_jm_my_profile_answer(name, f)
    f.hidden_field(:active, class: 'jm_answer_active_field') +
        link_to(name, '#', onclick: "remove_jm_my_profile_answer(this); return false", :class => 'jm_remove_answer_link')
  end

  def link_to_add_candidate_answer(name, f, jm_char_id)
    answer = JmCandidateAnswer.new(:jm_characteristic_id => jm_char_id)
    fields = f.fields_for(:jm_candidate_answers, answer, :child_index => "new_candidate_answer") do |builder|
      render('jm_process/form_answer', :f => f, :f_answer => builder)
    end
    link_to(name, '#', onclick: "add_candidate_answer(this, \"#{escape_javascript(fields)}\"); return false")
  end

  def link_to_not_to_use_candidate_answer(name, f_record)
    f_record.hidden_field(:_destroy, class: 'sel_applicant_record_destroy_field') +
        link_to(name, '#', onclick: "not_to_use_candidate_answer(this); return false")
  end

  def link_to_edit_candidate_answer(name, f, f_answer, f_record)

    answer = f_answer.object.dup
    answer.prev_answer_id = f_answer.object.id

    fields = f.fields_for(:jm_candidate_answers, answer, :child_index => "new_candidate_answer") do |builder|
      render('jm_process/form_answer', :f => f, :f_answer => builder, :prev_answer => f_answer.object)
    end
    # este link siempre va junto al de No utilizar (de postulación), y este ya define el campo
    #f_record.hidden_field(:_destroy, class: 'sel_applicant_record_destroy_field') +
    f_answer.hidden_field(:active, class: 'jm_answer_active_field') +
        link_to(name, '#', onclick: "edit_candidate_answer(this, \"#{escape_javascript(fields)}\"); return false")
      #link_to(name, '#', onclick: "edit_candidate_answer(this, \"#{escape_javascript(answer.to_json)}\"); return false")
  end

  def link_to_edit_jm_my_profile_answer(name, f, f_answer)

    answer = f_answer.object.dup
    answer.prev_answer_id = f_answer.object.id

    fields = f.fields_for(:jm_candidate_answers, answer, :child_index => "new_candidate_answer") do |builder|
      render('jm_process/form_answer', :f => f, :f_answer => builder, :prev_answer => f_answer.object)
    end
    # este link siempre va junto al de Eliminar (de my profile), y este ya define el campo
    #f_answer.hidden_field(:active, class: 'jm_answer_active_field') +
    link_to(name, '#', onclick: "edit_jm_my_profile_answer(this, \"#{escape_javascript(fields)}\"); return false")
  end

  def link_to_cancel_edit_candidate_answer(name, f)
    f.hidden_field(:_destroy, class: 'jm_answer_destroy_field') +
        link_to(name, '#', onclick: "revert_edit_candidate_answer(this); return false")
  end
end
