module ScProcessesValidatesHelper
  # STATUS GUIDE
  #     0:Pendiente de enviar validación,
  #     1:Pendiente de ser validado.
  #     3:Validado.
  #     4:Rechazado

  def update_he_user_process(user_id, process_id)
    calculate_send_to_validation(user_id, process_id)
    calculate_sent_to_validation(user_id, process_id)
    calculate_approved(user_id, process_id)
    calculate_rejected(user_id, process_id)
    calculate_not_charged_to_payroll(user_id, process_id)
    calculate_he_total(user_id, process_id)
  end

  def calculate_send_to_validation(user_id, process_id)
    # GUARDA LOS VALORES EN USER_PROCESS DE: HE_SEND_TO_VALIDATION
    he_send_to_validation = get_send_to_validation(user_id, process_id, true)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first
    user_process.update_attributes(he_send_to_validation: he_send_to_validation)
  end

  def calculate_sent_to_validation(user_id, process_id)
    # GUARDA LOS VALORES EN USER_PROCESS DE: HE_SENT_TO_VALIDATION
    he_sent_to_validation = get_sent_to_validation(user_id, process_id, true)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first
    user_process.update_attributes(he_sent_to_validation: he_sent_to_validation)
  end

  def calculate_approved(user_id, process_id)
    # GUARDA LOS VALORES EN USER_PROCESS DE: HE_APPROVED
    he_approved = get_approved(user_id, process_id, true)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first
    user_process.update_attributes(he_approved: he_approved)
  end

  def calculate_he_total(user_id, process_id)
    # GUARDA LOS VALORES EN USER_PROCESS DE: HE_APPROVED AND CHARGE TO PAYROLL
    he_total = get_charge_to_payroll(user_id, process_id, true)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first
    user_process.update_attributes(he_total: he_total)
  end

  def calculate_rejected(user_id, process_id)
    # GUARDA LOS VALORES EN USER_PROCESS DE: HE_REJECTED
    he_rejected = get_rejected(user_id, process_id, true)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first
    user_process.update_attributes(he_rejected: he_rejected)
  end

  def calculate_not_charged_to_payroll(user_id, process_id)
    # GUARDA LOS VALORES EN USER_PROCESS DE: HE_NOT_CHARGED_TO_PAYROLL
    he_total = get_not_charged_to_payroll(user_id, process_id, true)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first
    user_process.update_attributes(he_not_charged_to_payroll: he_total)
  end

  def get_send_to_validation(user_id, process_id, update_record)
    # CALCULA COMO HORAS TOTALES: HORA PENDIENTES DE SER ENVIADAS A VALIDACIÓN.
    # UPDATE_RECORD: LE CREA EL USER_PROCESS AL RECORD SI ES QUE NO LO TIENE
    records = ScRecord.where(user_id: user_id, sc_process_id: process_id, status: 0)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first_or_create if update_record
    he_total = 0
    records.each do |record|
      he_total += record.he
      record.update_attributes(sc_user_process_id: user_process.id) if update_record
    end
    he_total
  end

  def get_sent_to_validation(user_id, process_id, update_record)
    # CALCULA COMO HORAS TOTALES: HORA ENVIADAS A VALIDACIÓN.
    # UPDATE_RECORD: LE CREA EL USER_PROCESS AL RECORD SI ES QUE NO LO TIENE
    records = ScRecord.where(user_id: user_id, sc_process_id: process_id, status: 1)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first_or_create if update_record
    he_total = 0
    records.each do |record|
      he_total += record.he
      record.update_attributes(sc_user_process_id: user_process.id) if update_record
    end
    he_total
  end

  def get_approved(user_id, process_id, update_record)
    # CALCULA COMO HORAS TOTALES: HORA APROBADAS
    # UPDATE_RECORD: LE CREA EL USER_PROCESS AL RECORD SI ES QUE NO LO TIENE
    records = ScRecord.where(user_id: user_id, sc_process_id: process_id, status: 3)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first_or_create if update_record
    he_total = 0
    records.each do |record|
      he_total += record.he
      record.update_attributes(sc_user_process_id: user_process.id) if update_record
    end
    he_total
  end

  def get_rejected(user_id, process_id, update_record)
    # CALCULA COMO HORAS TOTALES: HORA RECHAZADAS
    # UPDATE_RECORD: LE CREA EL USER_PROCESS AL RECORD SI ES QUE NO LO TIENE
    records = ScRecord.where(user_id: user_id, sc_process_id: process_id, status: 4)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first_or_create if update_record
    he_total = 0
    records.each do |record|
      he_total += record.he
      record.update_attributes(sc_user_process_id: user_process.id) if update_record
    end
    he_total
  end

  def get_not_charged_to_payroll(user_id, process_id, update_record)
    # CALCULA COMO HORAS TOTALES: HORA NO CARGADOS A PAYROLL
    # UPDATE_RECORD: LE CREA EL USER_PROCESS AL RECORD SI ES QUE NO LO TIENE
    records = ScRecord.where(user_id: user_id, sc_process_id: process_id, status: 3, dont_charge_to_payroll: true)
    user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first_or_create if update_record
    he_total = 0
    records.each do |record|
      he_total += record.he
      record.update_attributes(sc_user_process_id: user_process.id) if update_record
    end
    he_total
  end

  def get_charge_to_payroll(user_id, process_id, update_record)
  # CALCULA COMO HORAS TOTALES: HORA APROBADA Y NO MARCADA COMO NO CARGAR A PAYROLL
  # UPDATE_RECORD: LE CREA EL USER_PROCESS AL RECORD SI ES QUE NO LO TIENE
  records = ScRecord.where(user_id: user_id, sc_process_id: process_id, status: 3, dont_charge_to_payroll: false)
  user_process = ScUserProcess.where(user_id: user_id, sc_process_id: process_id).first_or_create if update_record
  he_total = 0
  records.each do |record|
    he_total += record.he
    record.update_attributes(sc_user_process_id: user_process.id) if update_record
  end
  he_total
  end

end
