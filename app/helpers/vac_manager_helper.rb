module VacManagerHelper
  include VacationsModule

  def requests_id_before(date_ref)
    requests_id = []
    VacRequest.where(active: true, status: VacRequest.approved).each do |request|
      end_date = request.end
      request.vac_rule_records.where(active: true, paid: [nil, true]).each do |vac_rule_record|
        benefit = vac_rule_record.bp_condition_vacation
        next unless benefit.bonus_days
        end_date = next_laborable_day?(end_date, request.user_id)
        end_date = vacations_end(benefit.bonus_only_laborable_day, end_date, benefit.bonus_days, request.user_id)
      end
      requests_id << request.id if end_date + 30.days < date_ref.to_date
    end
    requests_id
  end

  def manual_request_create(params, user_id)
    request = VacRequest.new(params)
    request.status = VacRequest.closed
    request.user_id = user_id
    request.registered_manually = true
    request.registered_by_user_id = user_connected.id
    request.registered_at = lms_time
    return request
  end

  def copy_paid_benefits(updated_request_id, actual_request_id)
    actual = VacRequest.find(actual_request_id)
    actual.vac_rule_records.where(active: true, paid: [true, false]).each do | rule_record |
      new_rule_record = rule_record.dup
      new_rule_record.vac_request_id = updated_request_id
      new_rule_record.save
      rule_record.active = false
      rule_record.deactivated_at = lms_time
      rule_record.deactivated_by_user_id = user_connected.id
      rule_record.save
    end
  end

  def update_accumulated_since(specified_date, user_id, description)
    after_requests = VacRequest.requests_after(specified_date, user_id)
    this_date_request = VacRequest.requests_this_date(specified_date, user_id)

    requests = this_date_request + after_requests
    requests = requests.sort_by {|t| [t.begin, t.registered_at, t.id]}

    requests.each do |request|
      next unless request.begin.to_date >= specified_date.to_date

      prev_accumulated = request.latest_accumulated

      if prev_accumulated
        prev_accumulated = deactivate_accum(prev_accumulated.id, description)
        prev_accumulated.save
      end

      actual_accumulated = create_accum(request)
      actual_accumulated.save

      if prev_accumulated
        prev_accumulated.next_vac_accumulated_id = actual_accumulated.id
        actual_accumulated.prev_vac_accumulated_id = prev_accumulated.id
        prev_accumulated.save
        actual_accumulated.save
      end
    end
  end

  def deactivate_accum(id, description)
    accum = VacAccumulated.find(id)
    accum.active = false
    accum.deactivated_at = lms_time
    accum.deactivated_by_user_id = user_connected.id
    accum.deactivated_description = description
    return accum
  end

  def create_accum(request)
    general_rules = BpGeneralRulesVacation.user_active(request.begin.to_datetime, request.user_id)
    rules_accumulated_max_days = general_rules ? general_rules.accumulated_max_days : 999999

    prev_accum = request.before_accumulated

    prev_real_days = prev_accum ? prev_accum.accumulated_real_days : 0
    prev_progressive_days = prev_accum ? prev_accum.accumulated_progressive_days : 0

    accumulated = VacAccumulated.new()
    accumulated.bp_general_rules_vacation_id = general_rules ? general_rules.id : nil

    accumulated.accumulated_real_days = prev_real_days

    accumulated.accumulated_real_days -= request.days if request.days
    accumulated.accumulated_real_days -= request.days_manually if request.days_manually

    accumulated.accumulated_days = accumulated.accumulated_real_days > rules_accumulated_max_days ? rules_accumulated_max_days : accumulated.accumulated_real_days

    accumulated.accumulated_progressive_days = prev_progressive_days
    accumulated.accumulated_progressive_days -= request.days_progressive if request.days_progressive

    accumulated.user_id = request.user_id
    accumulated.registered_at = lms_time
    accumulated.registered_by_user_id = user_connected.id
    accumulated.vac_request_id = request.id
    accumulated.up_to_date = request.begin.to_date
    accumulated.auto_update_legal = request.auto_update_legal
    accumulated.auto_update_prog = request.auto_update_prog

    return accumulated
  end

  def update_progressive_days(which_date)
    users = users_with_anniversary(which_date)
    users.each do |user|
      user_certificate_char = Characteristic.get_characteristic_from_date_r_progresivas
      prog_char = Characteristic.get_characteristic_from_date_progresivas
      general_rules = BpGeneralRulesVacation.user_active(which_date.to_datetime, user.id)

      next unless user_certificate_char
      next unless prog_char
      next unless general_rules

      user_certificate = user.user_characteristic(user_certificate_char)
      progressive_day_date = user.user_characteristic(prog_char)
      progressive_day_date = progressive_day_date ? progressive_day_date.value : nil

      next unless progressive_day_date
      next unless user_certificate

      which_date.to_date
      progressive_day_date.to_date

      this_year = which_date.year - progressive_day_date.year
      this_year += 1 if  which_date.month > progressive_day_date.month
      this_year += 1 if (which_date.month == progressive_day_date.month && which_date.days > progressive_day_date.days)

      new_days_progressive = BpProgressiveDay.where(active: true, bp_general_rules_vacation_id: general_rules.id, year: this_year).first
      next unless new_days_progressive

      days_progressive = last_progressive_days(which_date, user.id)
      request = VacRequest.new
      request.days_progressive = days_progressive - new_days_progressive.days
      request.begin = which_date.to_date
      request.status = VacRequest.closed
      request.user_id = user_id
      request.registered_manually = true
      request.registered_at = lms_time
      request.registered_by_user_id = ''
      update_accumulated_since(which_date.to_date, user.id, 'Actualización anual de días progresivos')
    end
  end

  def last_progressive_days(which_date, user_id)
    before_requests = VacRequest.requests_before(which_date, user_id)
    this_date_request = VacRequest.requests_this_date(which_date, user_id)

    requests = this_date_request + before_requests
    requests = requests.sort_by {|t| [t.begin, t.registered_at, t.id]}

    requests.each do |request|
      next unless request.begin.to_date >= specified_date.to_date
    end

    if requests.size > 0
      request_to_evaluate = requests.last
      accumulated = request_to_evaluate.latest_accumulated
      request_to_evaluate.latest_accumulated ? accumulated.accumulated_progressive_days : 0
    else
      0
    end
  end

  def update_legal_days(which_date)
    users = users_with_anniversary(which_date)
    users.each do |user|
      general_rules = BpGeneralRulesVacation.user_active(which_date.to_datetime, user.id)
      next unless general_rules
      request = VacRequest.new()
      request.days_manually = general_rules.legal_days*(-1)
      request.begin = which_date.to_date
      request.status = VacRequest.closed
      request.user_id = user_id
      request.registered_manually = true
      request.registered_at = lms_time
      request.registered_by_user_id = ''
      request.save
      update_accumulated_since(which_date.to_date, user.id, 'actualización anual de días legales')
    end
  end

  def users_with_anniversary(which_date)
    users_anniversary = []
    users = User.where(activo: true)
    users.each do |user|
      # user_anniversary = método que trae el aniversario del usuario
      user_anniversary = which_date.to_date
      user_anniversary = user_anniversary.change(year: which_date.to_date.year)
      next unless user_anniversary == which_date.to_date
      users_anniversary << user
    end
    return users_anniversary
  end


  def its_safe_requests(temp_file)
    should_save = true
    manual_requests = []
    @excel_errors = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-', '-', '-', '-', '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-', '-', '-', '-', '-', '-']

    end

    data.each_with_index do |row, index|
      this_should_save = true
      next if index.zero?

      user_cod = row[0].to_s
      user = User.where(codigo: user_cod, activo: true).first
      user_id = user ? user.id : nil
      begin_date = row[3].to_s
      days_manually = row[4].to_s
      days_progressive = row[5].to_s
      manually_description = row[6].to_s

      break if !row[0] && !row[1] && !row[2] && !row[3] && !row[4] && !row[5] && !row[6]

        # if !user_cod || user_cod.empty?
        #   should_save = false
        #   @excel_errors << [(index + 1).to_s, 'Usuario no puede ser vacío',row[0], row[1], row[2], row[3], row[4], row[5], row[6]]
        # end

        if !begin_date || begin_date.empty?
          should_save = false
          @excel_errors << [(index + 1).to_s, 'Fecha de ingreso no puede ser vacía',row[0], row[1], row[2], row[3], row[4], row[5], row[6]]
        end

      begin
        begin_date.to_date
      rescue Exception => e
        begin_date = nil
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Fecha de ingreso no tiene formato adecuado',row[0], row[1], row[2], row[3], row[4], row[5], row[6]]
      end

        if (!days_manually || days_manually.empty?) && (!days_progressive || days_progressive.empty?)
          should_save = false
          @excel_errors << [(index + 1).to_s, 'Solicitud sin cantidad de días',row[0], row[1], row[2], row[3], row[4], row[5], row[6]]
        end

        if !manually_description || manually_description.empty?
          should_save = false
          @excel_errors << [(index + 1).to_s, 'Justificación no puede ser vacía',row[0], row[1], row[2], row[3], row[4], row[5], row[6]]
        end

        unless user_id
          should_save = false
          @excel_errors << [(index + 1).to_s, 'Usuario no existe en la plataforma o no está activo',row[0], row[1], row[2], row[3], row[4], row[5], row[6]]
        end

        manual_request = VacRequest.new()
        manual_request.begin = row[3].to_date if begin_date
        manual_request.days_manually = row[4].to_d
        manual_request.days_progressive = row[5] ? row[5].to_i : nil
        manual_request.registered_manually = true
        manual_request.registered_manually_description = row[6].to_s
        manual_request.status = VacRequest.closed
        manual_request.user_id = user_id
        manual_request.registered_manually = true
        manual_request.registered_by_user_id = user_connected.id
        manual_request.registered_at = lms_time

        if manual_request.valid?
          manual_requests << manual_request
        else
          should_save = false
          errors = ''
          manual_request.errors.full_messages.each { |error| errors += error.to_s+'. ' }
          @excel_errors << [(index + 1).to_s, errors ,row[0], row[1], row[2], row[3], row[4], row[5], row[6]]
        end

    end

    if should_save
      manual_requests.each do |request|
        request.save
        update_accumulated_since(request.begin.to_date, request.user_id, 'Actualización masiva')
      end
    end

    return should_save
  end

  def its_safe_historical(temp_file)
    should_save = true
    manual_requests = []
    @excel_errors = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-', '-', '-', '-', '-', '-','-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-', '-', '-', '-', '-', '-','-', '-']

    end

    users = User.where(activo: true).all

    data.each_with_index do |row, index|
      this_should_save = true
      next if index.zero?
      user_cod = row[0].to_s.delete('-')
      user = nil
      users.each { |us| next unless  us.codigo == user_cod; user = us; break}
      user_id = user ? user.id : nil
      begin_date = row[3].to_s
      end_date = row[4].to_s
      end_date_1 = row[5].to_s
      days_legal = row[6].to_s
      days_progressive = row[7].to_s
      status = row[8].to_s
      comment = row[9]

      break if !row[0] && !row[1] && !row[2] && !row[3] && !row[4] && !row[5] && !row[6] && !row[7] && !row[8]

      row_info = []
      row_info << row[0]
      row_info << row[1]
      row_info << row[2]
      row_info << row[3]
      row_info << row[4]
      row_info << row[5]
      row_info << row[6]
      row_info << row[7]
      row_info << row[8]
      row_info << row[9]

      unless user_id
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Usuario no existe en la plataforma o no está activo'] + row_info
      end

      user_id = -1 if user.nil? && !user_cod.blank?

      if !end_date || end_date.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Fecha de fin no puede ser vacía'] + row_info
      end

      if !end_date_1 || end_date_1.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Fecha de fin real no puede ser vacía'] + row_info
      end

      begin
        begin_date.to_date
      rescue Exception => e
        begin_date = nil
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Fecha de inicio no tiene formato adecuado'] + row_info
      end

      begin
        end_date.to_date
      rescue Exception => e
        begin_date = nil
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Fecha de fin no tiene formato adecuado'] + row_info
      end

      begin
        end_date_1.to_date
      rescue Exception => e
        begin_date = nil
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Fecha de fin real no tiene formato adecuado'] + row_info
      end

      if (!days_legal || days_legal.empty?) && (!days_progressive || days_progressive.empty?)
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Solicitud sin cantidad de días'] + row_info
      end

      if !status || status.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Se debe especificar tipo: Historial o Borrador'] + row_info
      end

      request = VacRequest.new()
      request.begin = begin_date if begin_date
      request.end = end_date if end_date
      request.end_vacations = end_date_1 if end_date_1
      request.days = days_legal
      request.days_progressive = days_progressive
      request.status = (status.to_i == 0 ? VacRequest.closed : VacRequest.draft)
      request.historical =  (status.to_i == 0)
      request.user_id = user_id
      request.registered_by_user_id = user_connected.id
      request.registered_at = lms_time
      request.status_description = comment

      if request.valid?
        manual_requests << request
      else
        should_save = false
        errors = ''
        request.errors.full_messages.each { |error| errors += error.to_s+'. ' }
        @excel_errors << [(index + 1).to_s, errors] + row_info
      end

    end

    VacRequest.bulk_import(manual_requests, validate: false) if should_save
    # if should_save; manual_requests.each { |request| request.save } end
    should_save
  end



  def its_safe_approvers(temp_file)
    should_save = true
    to_save = []
    @excel_errors = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-']

    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1]

      user_cod = row[0].to_s
      user = User.where(codigo: user_cod, activo: true).first
      user_id = user ? user.id : nil


      user_cod_2 = row[1].to_s
      user_2 = User.where(codigo: user_cod_2, activo: true).first
      user_id_2 = user_2 ? user_2.id : nil


      if !user_cod || user_cod.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Aprobador no puede ser vacío',row[0], row[1]]
      end

      if !user_cod_2 || user_cod_2.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Usuario a aprobar no puede ser vacío',row[0], row[1]]
      end

      unless user_id
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Aprobador no existe en la plataforma o no está activo',row[0], row[1]]
      end

      unless user_id_2
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Usuario a aprobar no existe en la plataforma o no está activo',row[0], row[1]]
      end

      if should_save
        to_save << [user_id, user_id_2]
      end

    end

    if should_save
      to_save.each do |users|
        approver = VacApprover.where(user_id: users[0]).first_or_initialize
        approver.active = true
        approver.registered_at = lms_time
        approver.registered_by_user_id = user_connected.id
        approver.save

        approvee = VacUserToApprove.where(user_id: users[1], vac_approver_id: approver.id).first_or_initialize
        approvee.active = true
        approvee.registered_at = lms_time
        approvee.registered_by_user_id = user_connected.id
        approvee.save
      end
    end

    return should_save
  end

end
