module KpiManagingHelper

  def letras_excel

    letras = %w(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI AJ AK AL AM AN AO AP AQ AR AS AT AU AV AW AX AY AZ BA BB BC BD BE BF BG BH BI BJ BK BL BM BN BO BP BQ BR BS BT BU BV BW BX BY BZ CA CB CC CD CE CF CG CH CI CJ CK CL CM CN CO CP CQ CR CS CT CU CV CW CX CY CZ)

  end

  def xls_for_update_monthly(kpi_dashboard, kpi_sets)

    letras = letras_excel

    meses = t('date.month_names')
    meses_array = meses.each_with_index.map { |m,i| [m, i] }
    meses_array.delete_at 0

    dt_i = Array.new
    dt_f = Array.new

    prev_month = nil

    (kpi_dashboard.from..kpi_dashboard.to).each do |date|

      unless prev_month == date.strftime('%m').to_i

        prev_month = date.strftime('%m').to_i

        dt_i.push date
        dt_f.push date + 1.month

      end

    end

    kpi_indicator_ranks_hash = Hash.new
    kpi_indicators_measurements_hash = Hash.new
    kpi_indicators_details_hash = Hash.new
    kpi_indicator_details_ranks_hash = Hash.new

    kpi_sets.each do |kpi_set|


      kpi_set.kpi_indicators.each do |kpi_indicator|

        kpi_indicator_ranks_hash[kpi_indicator.id.to_s] = Array.new
        kpi_indicator.kpi_indicator_ranks.each do |kpi_indicator_rank|

          kpi_indicator_ranks_hash[kpi_indicator.id.to_s].push kpi_indicator_rank

        end

        kpi_indicators_measurements_hash[kpi_indicator.id.to_s] = Hash.new

        kpi_indicator.kpi_measurements.each do |kpi_measurement|

           kpi_indicators_measurements_hash[kpi_indicator.id.to_s][kpi_measurement.measured_at.to_s] = kpi_measurement

        end

        kpi_indicators_details_hash[kpi_indicator.id.to_s] = Hash.new

        kpi_indicator.kpi_indicator_details.each do |kpi_indicator_detail|

          kpi_indicators_details_hash[kpi_indicator.id.to_s][kpi_indicator_detail.measured_at.to_s] = kpi_indicator_detail

        end

        kpi_indicator_details_ranks_hash[kpi_indicator.id.to_s] = Hash.new

        kpi_indicator.kpi_indicator_details_ranks.each do |kpi_indicator_rank|

          kpi_indicator_details_ranks_hash[kpi_indicator.id.to_s][kpi_indicator_rank.kpi_indicator_detail_id.to_s] = Array.new unless kpi_indicator_details_ranks_hash[kpi_indicator.id.to_s][kpi_indicator_rank.kpi_indicator_detail_id.to_s]

          kpi_indicator_details_ranks_hash[kpi_indicator.id.to_s][kpi_indicator_rank.kpi_indicator_detail_id.to_s].push kpi_indicator_rank

        end

      end

    end

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => 'Indicadores') do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new

        fila << kpi_dashboard.name
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << kpi_dashboard.period
        filaHeader << headerTitleInfo

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila

        fila_actual += 1

        kpi_sets.each do |kpi_set|

          fila = Array.new
          filaHeader = Array.new
          cols_widths = Array.new

          fila << kpi_set.position
          filaHeader <<  headerCenter
          cols_widths << 6

          fila << kpi_set.name
          filaHeader <<  headerCenter
          cols_widths << 20

          fila << 'Peso'
          filaHeader <<  headerCenter
          cols_widths << 6

          fila << 'Unidad'
          filaHeader <<  headerCenter
          cols_widths << 10

          dt_i.each do |fecha|

            fila << fecha.strftime('%Y')+' '+meses_array[(fecha.strftime '%m').to_i-1][0]
            filaHeader <<  headerCenter
            cols_widths << 10

          end

          if kpi_dashboard.show_indicator_final_result

            fila << 'Resultado a la fecha'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

          reporte_excel.add_row fila, :style => filaHeader, height: 20

          fila_actual += 1

          kpi_indicators = kpi_dashboard.allow_subsets? ? kpi_set.kpi_indicators_ordered_by_subset : kpi_set.kpi_indicators

          kpi_indicators.each do |kpi_indicator|

            fila = Array.new
            fileStyle = Array.new

            fila << kpi_set.position.to_s+'.'+kpi_indicator.position.to_s
            fileStyle << fieldLeft

            fila << kpi_indicator.name
            fileStyle << fieldLeft

            #fila << number_with_precision(kpi_indicator.weight, precision: 20, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
            fila << kpi_indicator.weight
            fileStyle << fieldLeft

            fila << kpi_indicator.unit
            fileStyle << fieldLeft

            dt_i.each do |fecha|

              #kpi_measurement = kpi_indicator.kpi_measurements.where('measured_at = ?',fecha).first

              kpi_measurement = kpi_indicators_measurements_hash[kpi_indicator.id.to_s] && kpi_indicators_measurements_hash[kpi_indicator.id.to_s][fecha.to_s] ? kpi_indicators_measurements_hash[kpi_indicator.id.to_s][fecha.to_s] : nil

              if kpi_measurement && kpi_measurement.value
                #fila << number_with_precision(kpi_measurement.value, precision: 20, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                fila << kpi_measurement.value
              else
                fila << ''
              end

              fileStyle << fieldLeft

            end

            if kpi_dashboard.show_indicator_final_result

              if kpi_indicator.value
                #fila << number_with_precision(kpi_indicator.value, precision: 20, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                fila << kpi_indicator.value
              else
                fila << ''
              end
              fileStyle << fieldLeft

            end

            reporte_excel.add_row fila, :style => fileStyle, height: 20



          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

      wb.add_worksheet(:name => 'Metas') do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new

        fila << kpi_dashboard.name
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << kpi_dashboard.period
        filaHeader << headerTitleInfo

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila

        fila_actual += 1

        kpi_sets.each do |kpi_set|

          fila = Array.new
          filaHeader = Array.new
          cols_widths = Array.new

          fila << kpi_set.position
          filaHeader <<  headerCenter
          cols_widths << 6

          fila << kpi_set.name
          filaHeader <<  headerCenter
          cols_widths << 20

          fila << 'Peso'
          filaHeader <<  headerCenter
          cols_widths << 6

          fila << 'Unidad'
          filaHeader <<  headerCenter
          cols_widths << 10

          dt_i.each do |fecha|

            fila << fecha.strftime('%Y')+' '+meses_array[(fecha.strftime '%m').to_i-1][0]
            filaHeader <<  headerCenter
            cols_widths << 10

          end

          if kpi_dashboard.show_indicator_final_result

            fila << 'Resultado a la fecha'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

          reporte_excel.add_row fila, :style => filaHeader, height: 20

          fila_actual += 1

          kpi_indicators = kpi_dashboard.allow_subsets? ? kpi_set.kpi_indicators_ordered_by_subset : kpi_set.kpi_indicators

          kpi_indicators.each do |kpi_indicator|

            fila = Array.new
            fileStyle = Array.new

            fila << kpi_set.position.to_s+'.'+kpi_indicator.position.to_s
            fileStyle << fieldLeft

            fila << kpi_indicator.name+' ('+kpi_indicator.indicator_type_name+')'
            fileStyle << fieldLeft

            #fila << number_with_precision(kpi_indicator.weight, precision: 20, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
            fila << kpi_indicator.weight
            fileStyle << fieldLeft

            fila << kpi_indicator.unit
            fileStyle << fieldLeft

            dt_i.each do |fecha|

              kpi_indicator_detail = kpi_indicators_details_hash[kpi_indicator.id.to_s] && kpi_indicators_details_hash[kpi_indicator.id.to_s][fecha.to_s] ? kpi_indicators_details_hash[kpi_indicator.id.to_s][fecha.to_s] : nil

              if kpi_indicator_detail

                if kpi_indicator.indicator_type == 4
                  goal = ''

                  if kpi_indicator_details_ranks_hash[kpi_indicator.id.to_s] && kpi_indicator_details_ranks_hash[kpi_indicator.id.to_s][kpi_indicator_detail.id.to_s]

                    kpi_indicator_details_ranks_hash[kpi_indicator.id.to_s][kpi_indicator_detail.id.to_s].each_with_index do |kpi_indicator_rank, index_rank|
                      if index_rank == 0
                        goal += kpi_indicator_rank.goal.to_s+':'+kpi_indicator_rank.percentage.to_s
                      else
                        goal += ' | '+kpi_indicator_rank.goal.to_s+':'+kpi_indicator_rank.percentage.to_s
                      end

                    end

                  end

                  #fila << number_with_precision(goal, precision: 20, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                  fila << goal

                else

                  if kpi_indicator_detail.goal
                    #fila << number_with_precision(kpi_indicator_detail.goal, precision: 20, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                    fila << kpi_indicator_detail.goal
                  else
                    fila << ''
                  end

                end

              else
                fila << ''
              end

              fileStyle << fieldLeft

            end

            if kpi_dashboard.show_indicator_final_result

              if kpi_indicator.indicator_type == 4
                goal = ''

                if kpi_indicator_ranks_hash[kpi_indicator.id.to_s]

                  kpi_indicator_ranks_hash[kpi_indicator.id.to_s].each_with_index do |kpi_indicator_rank, index_rank|
                    if index_rank == 0
                      goal += kpi_indicator_rank.goal.to_s+':'+kpi_indicator_rank.percentage.to_s
                    else
                      goal += ' | '+kpi_indicator_rank.goal.to_s+':'+kpi_indicator_rank.percentage.to_s
                    end

                  end

                end

                #fila << number_with_precision(goal, precision: 20, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                fila << goal
              else

                if kpi_indicator.goal
                  #fila << number_with_precision(kpi_indicator.goal, precision: 20, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                  fila << kpi_indicator.goal
                else
                  fila << ''
                end

              end

              fileStyle << fieldLeft

            end

            reporte_excel.add_row fila, :style => fileStyle, height: 20



          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def calculate_percentage(value, goal, kpi_indicator, kpi_indicator_ranks, upper_limit, lower_limit)

    percentage = nil

    if value && goal && kpi_indicator

      case kpi_indicator.indicator_type
        when 1
          #directo
          percentage = value*100/goal if goal > 0
        when 2
          #inverso
          if value > 0
            percentage = goal*100/value
          elsif value == 0
            percentage = upper_limit
          end
        when 3
          #desviación
          if goal*-1  <= value && value <= goal
            percentage = 100
          else
            percentage = 100 - (value - goal).abs*100/goal if goal > 0
          end
        when 4
          #rango
          if kpi_indicator_ranks && kpi_indicator_ranks.count > 0

            metas = Array.new
            porcentajes = Array.new

            kpi_indicator_ranks.each do |kpi_indicator_rank|
              metas.push kpi_indicator_rank.goal
              porcentajes.push kpi_indicator_rank.percentage
            end

            if metas.first <= metas.last
              #directo

              if value < metas.first
                percentage = 0
              elsif value >= metas.last
                percentage = porcentajes.last
              else

                meta_inf = meta_dif = 0
                por_inf = por_dif = 0

                (0..metas.length-1).each do |num_meta|
                  if metas[num_meta] <= value && value <= metas[num_meta+1]
                    meta_dif = metas[num_meta+1] - metas[num_meta]
                    meta_inf = metas[num_meta]
                    por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                    por_inf = porcentajes[num_meta]
                    break
                  end
                end

                if meta_dif == 0
                  percentage = por_inf
                elsif meta_dif > 0
                  percentage = ((value-meta_inf)*por_dif/meta_dif)+por_inf
                end

              end

            else
              #inverso

              if value > metas.first
                percentage = 0
              elsif value <= metas.last
                percentage = porcentajes.last
              else

                meta_sup = meta_dif = 0
                por_inf = por_dif = 0


                (0..metas.length-1).each do |num_meta|
                  if metas[num_meta+1] <= value && value <= metas[num_meta]
                    meta_dif = metas[num_meta] - metas[num_meta+1]
                    meta_sup = metas[num_meta]
                    por_dif = porcentajes[num_meta+1] - porcentajes[num_meta]
                    por_inf = porcentajes[num_meta]
                    break
                  end
                end

                if meta_dif == 0
                  percentage = por_inf
                elsif meta_dif > 0
                  percentage = ((meta_sup-value)*por_dif/meta_dif)+por_inf
                end

              end

            end

          end
        when 6
          #security
          if goal > 0 && kpi_indicator.power && kpi_indicator.leverage
            percentage = (((((value/goal)**kpi_indicator.power)-1)*kpi_indicator.leverage)+1)*100
          end

      end

      if percentage

        percentage = upper_limit if upper_limit && percentage > upper_limit
        percentage = lower_limit if lower_limit && percentage < lower_limit

      end

    end


    return percentage

  end

  def update_subset_measurement(kpi_subset, date)

    p = 0.0
    s = 0.0

    kpi_subset.kpi_indicators.each do |kpi_indicator|

      kpi_measurement = kpi_indicator.kpi_measurements.where('measured_at = ?',date).first

      if kpi_measurement && kpi_measurement.percentage
        p += kpi_indicator.weight
        s += kpi_measurement.percentage*kpi_indicator.weight
      end

    end

    kpi_measurement = kpi_subset.kpi_measurements.where('measured_at = ?',date).first

    unless kpi_measurement
      kpi_measurement = kpi_subset.kpi_measurements.build
      kpi_measurement.measured_at = date
    end

    if p > 0
      kpi_measurement.percentage = s/p
    else
      kpi_measurement.percentage = nil
    end

    kpi_measurement.save

  end

  def update_set_measurement(kpi_set, date)

    p = 0.0
    s = 0.0

    kpi_set.kpi_indicators.each do |kpi_indicator|

      kpi_measurement = kpi_indicator.kpi_measurements.where('measured_at = ?',date).first

      if kpi_measurement && kpi_measurement.percentage
        p += kpi_indicator.weight
        s += kpi_measurement.percentage*kpi_indicator.weight
      end

    end

    kpi_measurement = kpi_set.kpi_measurements.where('measured_at = ?',date).first

    unless kpi_measurement
      kpi_measurement = kpi_set.kpi_measurements.build
      kpi_measurement.measured_at = date
    end

    if p > 0
      kpi_measurement.percentage = s/p
    else
      kpi_measurement.percentage = nil
    end

    kpi_measurement.save

  end

end
