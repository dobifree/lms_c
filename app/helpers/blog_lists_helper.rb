module BlogListsHelper

  def link_to_remove_blog_list_item(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_item(this); return false")
  end

  def link_to_add_blog_list_item(name, f)
    item = BlogListItem.new
    fields = f.fields_for(:blog_list_items, item, :child_index => "new_item") do |builder|
      render('blog_lists/items/form', :f => builder)
    end
    link_to(name, '#', onclick: "add_item(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
