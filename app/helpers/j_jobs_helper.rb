module JJobsHelper

  def link_to_add_register_jj_characteristic_field(jj_characteristic, j_job_jj_characteristic_registered)

    form = render partial: 'j_jobs/form_characteristic_register',
                  locals: {
                      jj_characteristic: jj_characteristic,
                      j_job_jj_characteristic_registered: j_job_jj_characteristic_registered
                  }

    link_to_function('<i class="fa fa-plus fa-lg" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="Añadir"></i>'.html_safe, "add_register_characteristic_field(\"#{escape_javascript(form)}\", #{jj_characteristic.id})")

  end

  def link_to_remove_cc(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_subchar(this)")
  end

  def link_to_add_cc(j_cost_center_id)

    form = render partial: 'j_jobs/form_cc',
                  locals: {
                      j_cost_center_id: j_cost_center_id
                  }

    link_to_function('<i class="fa fa-plus fa-lg" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="Añadir"></i>'.html_safe, "add_cc(\"#{escape_javascript(form)}\")")

  end


end
