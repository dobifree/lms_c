module JManagingHelper

  def search_employees(codigo, last_name, first_name)

    users = Array.new

    unless codigo.blank? && last_name.blank? && first_name.blank?

      users = User.where(
        'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ?',
        '%'+codigo+'%', '%'+last_name+'%', '%'+first_name+'%').order('apellidos, nombre')

    end

    return users

  end

end
