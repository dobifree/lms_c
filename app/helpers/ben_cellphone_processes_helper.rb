module BenCellphoneProcessesHelper
  def its_safe_equipment_bill( temp_file )
    should_save = true
    bill_items = []
    description_col = 0
    cellphone_number_col = 3
    company_rut_col = 9
    bill_folio_col = 13
    total_col = 13
    net_value_col = 14
    discount_col = 15
    net_total_col = 16
    net_with_taxes_col = 17

    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      # puts 'ver excepción: '
      # puts e.inspect
      data = []
      file = []
      should_save = false
      @error_index << '-'
      @error_content << ['-', '-', '-', '-']
      @error_message << t('views.ben_cellphone_number.error.format_error')
    end

    # file = RubyXL::Parser.parse temp_file
    file.each_with_index do |worksheet, index_sheet|
      also_final_row = false
      begin
        data = file[index_sheet].extract_data
        also_final_row = data[data.size-1][total_col].to_s.downcase.delete(' ') == 'totales' ? false : true

      rescue Exception => e
        # puts 'ver excepción: '
        # puts e.inspect
        data = []
        file = []
        should_save = false
        @error_index << '-'
        @error_content << ['-', '-', '-', '-']
        @error_message << t('views.ben_cellphone_number.error.formula_error')
      end

      # data = file[index_sheet].extract_data
      # also_final_row = data[data.size-1][total_col].to_s.downcase.delete(' ') == 'totales' ? false : true

      data.each_with_index do |row, index|
        should_save_register = true
        description = row[description_col]
        cellphone_number = row[cellphone_number_col].to_s
        company_rut = row[company_rut_col].to_s
        bill_folio = row[bill_folio_col].to_s
        net_value = row[net_value_col].to_f.round(4)
        discount = row[discount_col].to_f.round(4)
        net_total = row[net_total_col].to_f.round(4)
        net_with_taxes = row[net_with_taxes_col].to_f.round(4)
        ben_cellphone_number_id = BenCellphoneNumber.where(:number =>  cellphone_number).first ? BenCellphoneNumber.where(:number =>  cellphone_number).first.id : nil

        if index > 0
          if data.size != index+1 or (data.size == index+1 and also_final_row)
            ben_cellphone_bill_item = BenCellphoneBillItem.new(:ben_cellphone_process_id => @ben_cellphone_process.id,
                                                               :ben_cellphone_number_id => ben_cellphone_number_id,
                                                               :description => description,
                                                               :cellphone_number => cellphone_number,
                                                               :company_rut => company_rut,
                                                               :bill_folio => bill_folio,
                                                               :net_value => net_value,
                                                               :discount => discount,
                                                               :net_total => net_total,
                                                               :net_with_taxes => net_with_taxes)

            unless ben_cellphone_bill_item.valid?
              ben_cellphone_bill_item.errors.full_messages.each do |error|
                @error_index_equipment_bill << index + 1
                @error_content_equipment_bill << [worksheet.sheet_name, description, cellphone_number, company_rut, bill_folio, net_value, discount, net_total, net_with_taxes]
                @error_message_equipment_bill << error
              end
              should_save = false
              should_save_register = false
            end

            bill_items << ben_cellphone_bill_item if should_save_register

          end
        end
      end
    end

    if should_save
      bill_items.each do |bill_item|
        bill_to_save = BenCellphoneBillItem.new(:ben_cellphone_process_id => bill_item.ben_cellphone_process_id,
                                   :ben_cellphone_number_id => bill_item.ben_cellphone_number_id,
                                   :description => bill_item.description,
                                   :cellphone_number => bill_item.cellphone_number,
                                   :company_rut => bill_item.company_rut,
                                   :bill_folio => bill_item.bill_folio,
                                   :bill_type => 1, #Para equipos
                                   :net_value => bill_item.net_value,
                                   :discount => bill_item.discount,
                                   :net_total => bill_item.net_total,
                                   :net_with_taxes => bill_item.net_with_taxes)
        bill_to_save.save

      end
    end
    return should_save
  end

  def its_safe_plan( temp_file, worksheet_name )
    found_sheet = false
    should_save = true
    bill_items = []

    description_col = 9
    cellphone_number_col = 8
    company_rut_col = 1
    bill_folio_col = 4
    total_col = 15
    net_value_col = 15
    discount_col = 16
    net_total_col = 17
    net_with_taxes_col = 17


    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      # puts 'ver excepción: '
      # puts e.inspect
      data = []
      file = []
      found_sheet = true
      should_save = false
      @error_index << '-'
      @error_content << ['-', '-', '-', '-']
      @error_message << t('views.ben_cellphone_number.error.format_error')
    end

    # file = RubyXL::Parser.parse temp_file

    file.each_with_index do |worksheet, index_sheet|
       if worksheet.sheet_name == worksheet_name
      # if index_sheet == 3
        found_sheet = true

        also_final_row = false
        begin
          data = file[index_sheet].extract_data
          also_final_row = data[data.size-1][total_col].to_s.downcase.delete(' ') == 'totales' ? false : true
        rescue Exception => e
          # puts 'ver excepción: '
          # puts e.inspect
          data = []
          found_sheet = true
          should_save = false
          @error_index << '-'
          @error_content << ['-', '-', '-', '-']
          @error_message << t('views.ben_cellphone_number.error.formula_error')
        end

        # data = file[index_sheet].extract_data
        # also_final_row = data[data.size-1][total_col].to_s.downcase.delete(' ') == 'totales' ? false : true

        data.each_with_index do |row, index|
          should_save_register = true
          description = row[description_col].to_s+' /'+ row[description_col+1].to_s+' /'+ row[description_col+2].to_s if row[description_col] and row[description_col+1] and row[description_col+2]
          cellphone_number = row[cellphone_number_col].to_s
          company_rut = row[company_rut_col].to_s
          bill_folio = row[bill_folio_col].to_s
          net_value = row[net_value_col].to_f.round(4)
          discount = row[discount_col].to_f.round(4)
          net_total = row[net_total_col].to_f.round(4)
          net_with_taxes = row[net_with_taxes_col].to_f.round(4)

          ben_cellphone_number_id = BenCellphoneNumber.where(:number =>  cellphone_number).first ? BenCellphoneNumber.where(:number =>  cellphone_number).first.id : nil

          if index > 0
            if data.size != index+1 or (data.size == index+1 and also_final_row)
              ben_cellphone_bill_item = BenCellphoneBillItem.new(:ben_cellphone_process_id => @ben_cellphone_process.id,
                                                                 :ben_cellphone_number_id => ben_cellphone_number_id,
                                                                 :description => description,
                                                                 :cellphone_number => cellphone_number,
                                                                 :company_rut => company_rut,
                                                                 :bill_folio => bill_folio,
                                                                 :net_value => net_value,
                                                                 :discount => discount,
                                                                 :net_total => net_total,
                                                                 :net_with_taxes => net_with_taxes)
              unless ben_cellphone_bill_item.valid?
                ben_cellphone_bill_item.errors.full_messages.each do |error|
                  @error_index_plan << index + 1
                  @error_content_plan << [worksheet.sheet_name, description, cellphone_number, company_rut, bill_folio, net_value, discount, net_total, net_with_taxes]
                  @error_message_plan << error
                end
                should_save = false
                should_save_register = false
              end

              bill_items << ben_cellphone_bill_item if should_save_register

            end
          end
        end
      end
    end

    if found_sheet
      if should_save
        bill_items.each do |bill_item|
          bill_to_save = BenCellphoneBillItem.new(:ben_cellphone_process_id => bill_item.ben_cellphone_process_id,
                                                    :ben_cellphone_number_id => bill_item.ben_cellphone_number_id,
                                                    :description => bill_item.description,
                                                    :cellphone_number => bill_item.cellphone_number,
                                                    :company_rut => bill_item.company_rut,
                                                    :bill_folio => bill_item.bill_folio,
                                                    :bill_type => 0, #siempre para planes
                                                    :net_value => bill_item.net_value,
                                                    :discount => bill_item.discount,
                                                    :net_total => bill_item.net_total,
                                                    :net_with_taxes => bill_item.net_with_taxes)
          bill_to_save.save

        end
      end
    else
      should_save = false
      @error_index_plan << '-'
      @error_content_plan << %w[- - - - - - - - -]
      @error_message_plan << 'No hay hoja con nombre: '+worksheet_name
    end

    return should_save
  end

  def show_process (process_id)
    @ben_cellphone_process = BenCellphoneProcess.find(process_id)

    @not_processed = BenCellphoneBillItem.where(:ben_cellphone_process_id => process_id, :ben_user_cellphone_process_id => nil).joins(:ben_cellphone_number => :user).where(:ben_cellphone_numbers => {:active => true}, :users => {:activo => true})
    @not_processed = @not_processed.size

    not_matched_and_charge0 = BenCellphoneBillItem.where(:ben_cellphone_process_id => process_id).joins(:ben_cellphone_number => :user).where('(ben_cellphone_numbers.active=0 or users.activo=0) and ben_cellphone_bill_items.not_charge_to_payroll=0').size
    not_matched_and_charge1 = BenCellphoneBillItem.where(:ben_cellphone_process_id => process_id, :ben_cellphone_number_id => nil, :not_charge_to_payroll => 0).size
    @not_matched_and_charge = not_matched_and_charge0 + not_matched_and_charge1

    @equipment_type = BenCellphoneBillItem.where(:ben_cellphone_process_id => process_id, :bill_type => 1).size
    @plan_type = BenCellphoneBillItem.where(:ben_cellphone_process_id => process_id, :bill_type => 0).size

    no_match_bills0 =BenCellphoneBillItem.where(:ben_cellphone_process_id => process_id).joins(:ben_cellphone_number => :user).where('(ben_cellphone_numbers.active=0 or users.activo=0)').all
    no_match_bills1 =BenCellphoneBillItem.where(:ben_cellphone_process_id => process_id, :ben_cellphone_number_id => nil).all
    @no_match_bills = no_match_bills0 + no_match_bills1

    if @ben_cellphone_process.charged_to_payroll
      @not_matched_and_charge = 0
      @no_match_bills = BenCellphoneBillItem.where(:ben_cellphone_process_id => process_id)
      @no_match_bills = @no_match_bills.where('bill_item_errors is not null').all
    end
  end

  def reprocess(process_id)
    BenCellphoneProcess.find(process_id).ben_user_cellphone_processes.destroy_all
    BenCellphoneNumber.all.each do |benefit|
      bill_items1 = BenCellphoneBillItem.where(:ben_cellphone_number_id => benefit.id, :ben_cellphone_process_id => process_id)
      bill_items = bill_items1.all
      if bill_items.size > 0
        total_bill = 0
        equipment_bill = 0
        plan_bill = 0
        registered_manually_total = 0
        applied_subsidy = benefit.subsidy

        should_create_user_cellphone_process = false
        bill_items.each_with_index do |bill_item|
          if bill_item.no_match_messages.size == 0
            unless bill_item.not_charge_to_payroll
              if bill_item.registered_manually
                registered_manually_total += bill_item.net_with_taxes
              else
                total_bill += bill_item.net_with_taxes
                equipment_bill += bill_item.net_with_taxes if bill_item.bill_type == 1
                plan_bill += bill_item.net_with_taxes if bill_item.bill_type == 0
              end
            end
            should_create_user_cellphone_process = true
          end
        end

        if should_create_user_cellphone_process
          applied_subsidy = total_bill if applied_subsidy > total_bill
          applied_subsidy = 0 if total_bill < 0
          total_charged = total_bill - applied_subsidy

          user_cellphone_process = BenUserCellphoneProcess.create(:user_id => benefit.user.id,
                                                                  :ben_cellphone_number_id => benefit.id,
                                                                  :ben_cellphone_process_id => process_id,
                                                                  :configured_subsidy => benefit.subsidy,
                                                                  :total_bill => total_bill,
                                                                  :plan_bill => plan_bill,
                                                                  :equipment_bill => equipment_bill,
                                                                  :applied_subsidy => applied_subsidy,
                                                                  :total_charged => total_charged,
                                                                  :registered_manually_total => registered_manually_total)

          bill_items1.update_all(:ben_user_cellphone_process_id => user_cellphone_process.id)
        end
      end
    end
  end

  def reprocess_unity( bill_item_id )
    ben_cellphone_bill_item = BenCellphoneBillItem.find(bill_item_id)
    ben_cellphone_number = BenCellphoneNumber.where(:number => ben_cellphone_bill_item.cellphone_number).first
    user_process = ben_cellphone_bill_item.ben_user_cellphone_process
    should_be_new_process = true

    if user_process
      total_bill = 0
      equipment_bill = 0
      plan_bill = 0
      bill_items = user_process.ben_cellphone_bill_items
      bill_items.each do |bill_item|
        if !bill_item.not_charge_to_payroll? and
            bill_item.ben_cellphone_number and
            bill_item.ben_cellphone_number.active? and
            bill_item.ben_cellphone_number.user.activo
          total_bill += bill_item.net_with_taxes
          equipment_bill += bill_item.net_with_taxes if bill_item.bill_type == 1
          plan_bill += bill_item.net_with_taxes if bill_item.bill_type == 0
        end

      end
      user_process.total_bill = total_bill
    else
      if ben_cellphone_number and ben_cellphone_number.active? and ben_cellphone_number.user.activo
        total_bill = ben_cellphone_bill_item.net_with_taxes
        user_process = BenUserCellphoneProcess.new(:user_id => ben_cellphone_number.user.id,
                                                   :ben_cellphone_number_id => ben_cellphone_number.id,
                                                   :ben_cellphone_process_id => ben_cellphone_bill_item.ben_cellphone_process_id,
                                                   :total_bill => total_bill,
                                                   :plan_bill => plan_bill,
                                                   :equipment_bill => equipment_bill,
                                                   :configured_subsidy => ben_cellphone_number.subsidy)
      else
        should_be_new_process = false
      end
    end

    if should_be_new_process
      user_process.applied_subsidy = ben_cellphone_number.subsidy > user_process.total_bill ? user_process.total_bill : ben_cellphone_number.subsidy
      user_process.total_charged = total_bill - user_process.applied_subsidy
      user_process.save
    end
  end


  def reprocess_number( number_id )
    benefit = BenCellphoneNumber.find(number_id)
    processes = BenCellphoneProcess.where(:charged_to_payroll => false)

    processes.each do |process|
      bill_items = BenCellphoneBillItem.where(:cellphone_number => benefit.number, :ben_cellphone_process_id => process.id)
      user_processes = BenUserCellphoneProcess.where(:ben_cellphone_number_id => benefit.id, :ben_cellphone_process_id => process.id)
      user_processes.each { |user_process| user_process.destroy unless user_process.ben_cellphone_process.charged_to_payroll }

      user_process = BenUserCellphoneProcess.new(:user_id => benefit.user.id,
                                                 :ben_cellphone_number_id => benefit.id,
                                                 :ben_cellphone_process_id => process.id,
                                                 :registered_manually_total => 0,
                                                 :total_bill => 0,
                                                 :plan_bill => 0,
                                                 :equipment_bill => 0,
                                                 :configured_subsidy => 0,
                                                 :applied_subsidy => 0,
                                                 :total_charged => 0)
      user_process.save

      bill_items.each do |bill|
        bill.ben_cellphone_number_id = benefit.id
        bill.ben_user_cellphone_process_id = user_process.id
        bill.save

        if !bill.not_charge_to_payroll?
          if bill.registered_manually
            user_process.registered_manually_total += bill.net_with_taxes
          else
            user_process.total_bill += bill.net_with_taxes
            user_process.equipment_bill += bill.net_with_taxes if bill.bill_type == 1
            user_process.plan_bill += bill.net_with_taxes if bill.bill_type == 0
          end
        end
      end
      user_process.configured_subsidy = benefit.subsidy
      user_process.applied_subsidy = benefit.subsidy > user_process.total_bill ? user_process.total_bill : benefit.subsidy
      user_process.total_charged = user_process.total_bill - user_process.applied_subsidy
      user_process.save

      user_process.destroy unless benefit.active and benefit.user.activo
    end
  end

end
