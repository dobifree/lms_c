module UserProfileHelper


  def link_to_add_register_characteristic_field(characteristic, user_characteristic_registered)

    form = render partial: 'user_profile/form_characteristic_register',
                  locals: {
                      characteristic: characteristic,
                      user_characteristic_registered: user_characteristic_registered
                  }

    link_to_function('<i class="fa fa-plus fa-lg" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="Añadir"></i>'.html_safe, "add_register_characteristic_field(\"#{escape_javascript(form)}\", #{characteristic.id})")

  end

end
