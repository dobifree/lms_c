module MenuHelper

  def can_show_manage_menu?

    return true if ct_module_lms_manager?

    return true if ct_module_um_manager?

    return true if is_um_manager?

    return true if is_um_query_reports?

    return true if is_lms_manager?

    return true if ct_module_em_manager?

    return true if ct_module_tc_manager?

    return true if ct_module_tp2_manager?

    return true if ct_module_tr2_manager?

    return true if ct_module_jobs_manager?

    return true if ct_module_kpis_manager?

    return true if ct_module_pe_manager?

    return true if ct_module_pe_reporter?

    return true if ct_module_sel_manager?

    return true if ct_module_elec_manager?

    return true if is_sel_module_owner?(user_connected)

    return true if user_connected.program_inspectors.count > 0 || user_connected.jefe_capacitacion?

    return true if user_connected.jefe_biblioteca

    return true if ct_module_tracking_manager?

    return true if ct_module_ben_cel_sec_manager?

    return true if ct_module_polls_manager?

    return true if ct_module_sc_he_manager?

    return true if ct_module_holidays_manager?

    return true if ct_module_brc_manager?

    return true if ct_module_licenses_manager?

    return true if ct_module_licenses_approver?

    return true if ct_module_brg_manager?

    return true if ct_module_sch_manager?

    return true if ct_module_medlic_manager?

    return true if ct_module_vacs_manager?

    return false

  end

  def ct_module_lms_manager?

    is_ct_module_manager? 'lms', user_connected

  end

  def ct_module_um_manager?

    is_ct_module_manager? 'um', user_connected

  end

  def ct_module_em_manager?

    is_ct_module_manager? 'em', user_connected

  end

  def ct_module_tc_manager?

    is_ct_module_manager? 'tc', user_connected

  end

  def ct_module_tp2_manager?

    is_ct_module_manager? 'tp2', user_connected

  end

  def ct_module_tr2_manager?

    is_ct_module_manager? 'tr2', user_connected

  end

  def ct_module_trep2_manager?

    is_ct_module_manager? 'trep2', user_connected

  end

  def ct_module_jobs_manager?

    is_ct_module_manager? 'jobs', user_connected

  end

  def ct_module_kpis_manager?

    is_ct_module_manager? 'kpis', user_connected

  end

  def ct_module_pe_manager?

    user_connected.pe_managers.count > 0 || user_connected.hr_process_managers.count > 0 ? true : false

  end

  def ct_module_ben_cel_sec_manager?

    is_ct_module_manager? 'ben_cel_sec', user_connected

  end

  def ct_module_polls_manager?

    is_ct_module_manager? 'polls', user_connected

  end

  def ct_module_sc_he_manager?

    is_ct_module_manager? 'sc_he', user_connected

  end

  def ct_module_vacs_manager?

    is_ct_module_manager? 'vacs', user_connected

  end


  def ct_module_ben_params_manager?

    is_ct_module_manager? 'ben_params', user_connected

  end


  def ct_module_sch?
    return CtModule.where('cod = ? AND active = ?', 'sch', true).first
  end

  def ct_module_sc_he?
    return CtModule.where('cod = ? AND active = ?', 'sc_he', true).first
  end

  def ct_module_ben_cel_sec?
    return CtModule.where('cod = ? AND active = ?', 'ben_cel_sec', true).first
  end

  def ct_module_vacs?
    return CtModule.where('cod = ? AND active = ?', 'vacs', true).first
  end

  def ct_module_ben_params?
    return CtModule.where('cod = ? AND active = ?', 'ben_params', true).first
  end

  def ct_module_pe_reporter?

    user_connected.pe_reporters.count > 0 ? true : false

  end

  def ct_module_mpi_manager?

    is_ct_module_manager? 'mpi', user_connected

  end

  def can_show_sel_manager_menu?

    is_ct_module_manager?('sel', user_connected) || is_sel_module_owner?(user_connected)

  end

  def ct_module_sel_manager?

    is_ct_module_manager?('sel', user_connected)

  end

  def ct_module_elec_manager?

    is_ct_module_manager?('elec', user_connected)

  end

  def ct_module_tracking_manager?

    is_ct_module_manager?('trkn', user_connected)

  end

  def ct_module_holidays_manager?
    is_ct_module_manager?('holidays', user_connected)
  end

  def ct_module_licenses_manager?
    is_ct_module_manager?('licenses', user_connected)
  end

  def ct_module_brc_manager?
    is_ct_module_manager?('brc', user_connected)
  end

  def ct_module_brg_manager?
    is_ct_module_manager?('brg', user_connected)
  end

  def ct_module_sch_manager?
    is_ct_module_manager?('sch', user_connected)
  end

  def ct_module_medlic_manager?
    is_ct_module_manager?('medlic', user_connected)
  end

  def ct_module_liq_manager?
    is_ct_module_manager?('liq', user_connected)
  end

  def can_show_menu_ed?

    PeProcess.where('active = ? AND from_date <= ? AND to_date >= ?', true, lms_time, lms_time).each do |pe_process|

      return true if pe_process.has_step_assign? && pe_process.is_a_pe_member_evaluator_as_boss?(user_connected)
      return true if pe_process.has_step_selection? && pe_process.is_a_pe_member_can_select_evaluated?(user_connected)
      return true if pe_process.has_step_query_definitions? && (pe_process.is_a_pe_member_evaluated?(user_connected) || pe_process.is_a_pe_member_evaluator?(user_connected))
      return true if pe_process.has_step_query_results? && (pe_process.is_a_pe_member_evaluated?(user_connected) || pe_process.is_a_pe_member_evaluator?(user_connected))
      return true if pe_process.has_step_definition_by_users? && pe_process.pe_evaluations_where_has_to_define_questions(user_connected).size > 0
      return true if pe_process.has_step_definition_by_users? && pe_process.pe_evaluations_where_can_watch_own_defined_questions(user_connected).size > 0
      return true if pe_process.has_step_definition_by_users? && pe_process.has_step_definition_by_users_accepted? && pe_process.pe_evaluations_where_has_to_accept_defined_questions(user_connected).size > 0
      return true if pe_process.has_step_definition_by_users? && pe_process.has_step_definition_by_users_validated? && pe_process.pe_evaluations_where_has_to_validate_defined_questions(user_connected).size > 0
      return true if pe_process.has_step_tracking? && pe_process.pe_evaluations_where_has_to_track(user_connected).size > 0
      return true if pe_process.has_step_tracking? && pe_process.pe_evaluations_where_has_to_be_tracked(user_connected).size > 0
      return true if pe_process.has_step_assessment? && pe_process.is_a_pe_member_evaluator?(user_connected) && pe_process.step_assessment_menu?
      return true if pe_process.has_step_validation? && pe_process.validators.where('validator_id = ?', user_connected.id).count > 0
      return true if pe_process.has_step_calibration? && pe_process.pe_cal_committees.where('user_id = ?', user_connected.id).count > 0
      return true if pe_process.has_step_feedback? && pe_process.feedback_providers.where('feedback_provider_id = ?', user_connected.id).count > 0 && pe_process.step_feedback_menu?
      return true if pe_process.has_step_feedback? && pe_process.has_step_feedback_accepted? && pe_process.evaluated_members.where('user_id = ? AND step_feedback = ?', user_connected.id, true).count > 0
      return true if pe_process.has_step_my_results? && pe_process.is_a_pe_member_evaluated?(user_connected)
      return true if pe_process.has_step_my_subs_results? && pe_process.is_a_pe_member_evaluator_as_boss?(user_connected)
      return true if pe_process.is_a_observer?(user_connected)
      return true if pe_process.has_step_assessment? && pe_process.is_a_pe_member_evaluator?(user_connected) && pe_process.step_assessment_uniq
      return true if pe_process.has_step_feedback? && pe_process.is_a_feedback_provider?(user_connected) && pe_process.step_feedback_uniq
    end

    return false

  end

  def can_show_menu_brc?
    BrcProcess.where('active = ?', true).each do |brc_process|
      return true if brc_process.brc_definer(user_connected)
      return true if brc_process.brc_definers_by_validator(user_connected).size > 0
      brc_member = user_connected.brc_member(brc_process)
      return true if brc_member && brc_member.status_def && brc_member.status_val && brc_member.status_bon_cal
    end
    return false
  end

  def can_show_menu_brg?
    BrgManager.active.where(user_id: user_connected.id).first
  end

  def can_show_menu_brg_rp?
    BrgManager.active.joins(:brg_process).where(user_id: user_connected.id).where('brg_processes.alias like "%privado%"').first
  end

  def ct_module_lms?
    exists_ct_module? 'lms'
  end

  def ct_module_id?
    exists_ct_module? 'id'
  end

  def ct_module_jobs?
    exists_ct_module? 'jobs'
  end

  def ct_module_kpis?
    exists_ct_module? 'kpis'
  end

  def ct_module_mpi?
    exists_ct_module? 'mpi'
  end

  def ct_module_pe?
    exists_ct_module? 'pe'
  end

  def ct_module_dnc?
    exists_ct_module? 'dnc'
  end

  def ct_module_trkn?
    exists_ct_module? 'trkn'
  end

  def ct_module_blog?
    exists_ct_module? 'blog'
  end

  def ct_module_holidays?
    exists_ct_module? 'holidays'
  end

  def ct_module_licenses?
    exists_ct_module? 'licenses'
  end

  def ct_module_brc?
    exists_ct_module? 'brc'
  end

  def ct_module_brg?
    exists_ct_module? 'brg'
  end

  def ct_module_medlic?
    exists_ct_module? 'medlic'
  end

  private

  def is_ct_module_manager?(cod, user)

    answer = false

    ct_module = CtModule.where('cod = ? AND active = ?', cod, true).first

    answer = true if ct_module && ct_module.has_managers? && ct_module.ct_module_managers.where('user_id = ?', user.id).count == 1

    return answer

  end

  def ct_module_licenses_approver?
    return false unless ct_module_licenses?
    return false unless LicApprover.where(active: true, user_id: user_connected.id).first
    true
  end

  def is_sel_module_owner?(user)

    #TODO: añadir lógica de filtrado por estados del proceso (finalizado, etc)
    SelOwner.find_all_by_user_id(user.id).first

  end

  def is_um_manager?
    user_connected.um_manage_users
  end

  def is_um_query_reports?
    user_connected.um_query_reports?
  end

  def is_lms_manager?
    user_connected.lms_define || user_connected.lms_enroll || user_connected.lms_manage_enroll ||user_connected.lms_reports
  end

  def is_lms_define_manager?
    user_connected.lms_define
  end

  def is_lms_enroll_manager?
    user_connected.lms_enroll
  end

  def is_lms_manage_enroll_manager?
    user_connected.lms_manage_enroll
  end

  def is_lms_reports?
    user_connected.lms_reports
  end

  def exists_ct_module?(cod)

    CtModule.where('cod = ? AND active = ?', cod, true).count == 1 ? true : false

  end

  def get_ct_module(cod)
    CtModule.where('cod = ?', cod).first
  end

  def can_show_selection_menu?
    return true if has_assigned_steps_on_seleccion?(user_connected.id)

    return true if user_has_access_to_set_requirements?(user_connected.id)

    return true if has_processes_to_track?(user_connected.id)

    return true if has_sel_req_process_assigned?(user_connected.id)

    return false
  end

  def has_sel_req_process_assigned?(user_id)
    SelReqProcess.assigned_to(user_id).any?
  end

  def has_processes_to_track?(user_id)

    return true if SelProcess.tracker(user_id).count > 0

    return false

  end


  def user_has_access_to_set_requirements?(user_id)
    #TODO: añadir lógica de filtrado por usuarios asignados y usuarios con historial de requerimientos

    return true if UserCtModulePrivileges.where(:user_id => user_id, :sel_make_requirement => true).count > 0

    return false
  end

  def has_pending_steps_on_seleccion?(user_id)

    pending_process(user_id).count > 0 ? true : false
    #SelStepCandidate.joins([:sel_attendants, {sel_process_step: :sel_process}]).where(:done => false, :exonerated => false, sel_processes: {:finished_at => nil}, sel_attendants: {:user_id => user_id}).count > 0 ? true : false
  end

  def has_assigned_steps_on_seleccion?(user_id)

    assigned_process(user_id).count > 0 ? true : false
    #SelStepCandidate.joins([:sel_attendants, {sel_process_step: :sel_process}]).where(:done => false, :exonerated => false, sel_processes: {:finished_at => nil}, sel_attendants: {:user_id => user_id}).count > 0 ? true : false
  end

  def pending_process(user_id)
    SelProcess.joins(:sel_process_steps => [:sel_step_candidates =>[:sel_attendants, :jm_candidate => :sel_applicants]])
        .where(sel_processes: {:finished => false}, sel_step_candidates: {:done => false, :exonerated => false}, sel_attendants: {:user_id => user_id}, sel_applicants: {:excluded => false}).where('sel_applicants.sel_process_id = sel_processes.id').order('sel_processes.id ASC').select('DISTINCT sel_processes.*')
  end

  def assigned_process(user_id)
    SelProcess.joins(:sel_process_steps => [:sel_step_candidates => [:sel_attendants, :jm_candidate => :sel_applicants]])
        .where(sel_processes: {:finished => false}, sel_attendants: {:user_id => user_id}, sel_applicants: {:excluded => false}).where('sel_applicants.sel_process_id = sel_processes.id').order('sel_processes.id ASC').select('DISTINCT sel_processes.*')
  end


end


