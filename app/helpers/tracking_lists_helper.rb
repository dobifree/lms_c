module TrackingListsHelper

  def link_to_remove_item(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_item(this); return false")
  end

  def link_to_add_list_item(name, f)
    item = TrackingListItem.new
    fields = f.fields_for(:tracking_list_items, item, :child_index => "new_item") do |builder|
      render('tracking_lists/form_tracking_list_item', :f => builder)
    end
    link_to(name, '#', onclick: "add_item(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
