module FoldersHelper

  def get_folder_ancestros(folder)

    ancestros = []

    if folder.padre
      ancestros += get_folder_ancestros folder.padre
      ancestros.push folder.padre
    end

    return ancestros

  end

  def get_folder_progeny(folder)

    progeny = []

    folder.hijos.each do |hijo|
      progeny.push hijo
      progeny += get_folder_progeny hijo
    end

    return progeny

  end

  def folder_write_privileges?(folder,user)

    priv = false

    folder.folder_owners.each do |folder_owner|

      if folder_owner.user.id == user.id
        priv = true
        break
      end

    end

    unless priv

      get_folder_ancestros(folder).each do |ancentro|

        ancentro.folder_owners.each do |folder_owner|

          if folder_owner.user.id == user.id
            priv = true
            break
          end

        end

        break if priv

      end

    end

    return priv

  end

  def folder_read_privileges?(folder,user)

    if folder.public?
      priv = true

    else

    priv = false

    folder.folder_members.each do |folder_member|

      user.user_characteristics.each do |u_c|

        if u_c.valor == folder_member.valor

          priv = true
          break

        end

      end

      break if priv

    end

    unless priv

      get_folder_ancestros(folder).each do |ancentro|

        ancentro.folder_members.each do |folder_member|

          user.user_characteristics.each do |u_c|

            if u_c.valor == folder_member.valor

              priv = true
              break

            end

          end

          break if priv

        end

        break if priv

      end

    end

    unless priv

      folder.folder_readers.each do |folder_reader|

        if folder_reader.user.id == user.id
          priv = true
          break
        end

      end

    end

    unless priv

      get_folder_ancestros(folder).each do |ancentro|

        ancentro.folder_readers.each do |folder_reader|

          if folder_reader.user.id == user.id
            priv = true
            break
          end

        end

        break if priv

      end

    end

    unless priv
      get_folder_ancestros(folder).each do |ancentro|

        if ancentro.public?
          priv = true
          break
        end

        break if priv

      end
    end
    end


    return priv

  end

  def folder_navigate_privileges?(folder,user)

    priv = false

    progeny = get_folder_progeny(folder)

    progeny.each do |folder|

      # permitir carpetas publicas dentro de carpetas no publicas
      priv = true if folder.public?
      break if priv
      # permitir carpetas publicas dentro de carpetas no publicas

      folder.folder_members.each do |folder_member|

        user.user_characteristics.each do |u_c|

          if u_c.valor == folder_member.valor

            priv = true
            break

          end

        end

        break if priv

      end

      break if priv

    end

    unless priv

      progeny.each do |folder|

        folder.folder_owners.each do |folder_owner|

          if folder_owner.user.id == user.id
            priv = true
            break
          end

        end

        break if priv

      end

    end

    unless priv

      progeny.each do |folder|

        folder.folder_readers.each do |folder_reader|

          if folder_reader.user.id == user.id
            priv = true
            break
          end

        end

        break if priv

      end

    end

    return priv

  end



end
