module ApplicationHelper

  include ActionView::Helpers::NumberHelper

  def field_with_errors(f, key)

    'has-error' if f.object.errors.has_key? key.to_sym

  end

  def field_with_errors_object(o, key)

    'has-error' if o.errors.has_key? key.to_sym

  end

  def format_html_to_excel(str)

    str.gsub!('</li>', '&#13;&#10;')

    n = 0

    str.gsub!("<li>") do |s|
      n += 1
      n.to_s+'. '
    end

    str.gsub!('<br>','&#13;&#10;')
    str.gsub!('<strong>','')
    str.gsub!('</strong>','')
    str.gsub!('<span>','')
    str.gsub!('</span>','')
    str.gsub!('<ul>','')
    str.gsub!('</ul>','')
    str.gsub!('<ol>','')
    str.gsub!('</ol>','')
    str.gsub!('<b>','')
    str.gsub!('</b>','')
    str.gsub!('<div>','')
    str.gsub!('</div>','')
    str.gsub!('<','&lt;')
    str.gsub('>','&gt;')

    #document = Nokogiri::HTML.parse(str)
    #document.css("br").each { |node| node.replace("\n") }
    #document.text

  end

  def format_html_to_pdf(str)

    str.gsub!('<span>','')
    str.gsub!('</span>','')
    str.gsub!('<br>',"\n")
    str.gsub!('<div>','')
    str.gsub!('</div>',"\n")
    str.gsub!('&nbsp;',' ')
    str
  end

  def format_text_to_html(str)
    str.gsub("\n", "<br>")
  end

  def fix_form_floats(n)
    n.delete(user_connected_locale_delimiter).gsub(',','.')
  end

  def format_decimal_to_html(n)

    r = nil

    if n

      p = n.to_s.split('.')

      e = number_with_precision(p[0].to_i, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, precision: 0)

      r = p[1].to_i > 0 ?  e + user_connected_locale_separator + p[1] : e

    end

    return r

  end

  def lms_time

    tz = Time.now.utc.in_time_zone(user_connected_time_zone).strftime('%::z').split(':')

    return Time.now.utc+((tz[0].to_i*60*60)+(tz[1].to_i*60)+tz[2].to_i).seconds

  end

  def lms_time_tz

    Time.now.utc.in_time_zone(user_connected_time_zone)

  end

  def lms_date

    tz = Time.now.utc.in_time_zone(user_connected_time_zone).strftime('%::z').split(':')

    return (Time.now.utc+((tz[0].to_i*60*60)+(tz[1].to_i*60)+tz[2].to_i).seconds).to_date

  end

  def format_nota(nota)
    number_with_precision(nota, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
  end

  def tipo_nota(course, puntaje = 2)

    p = puntaje == 1 ? 'punto' : 'puntos'

    course.muestra_porcentajes ? '%' : p

  end

  def get_from_to(from_to)

    from_to = from_to.split('-')

    from_to[0].strip!
    from_to[1].strip!

    from = from_to[0].split('/').map { |s| s.to_i }
    from = Date.new(from[2], from[1], from[0])

    to = from_to[1].split('/').map { |s| s.to_i }
    to = Date.new(to[2], to[1], to[0])

    return from, to

  end

  def icon_true_false_in_show(value)
    if value
      '<i class="fa fa-check"></i>'.html_safe
    else
      '<i class="fa fa-minus-square"></i>'.html_safe
    end
  end

  def label_yes_not(value)
    if value
      '<span class="label label-success">&nbsp;Sí&nbsp;</span>'.html_safe
    else
      '<span class="label label-danger">&nbsp;No&nbsp;</span>'.html_safe
    end
  end

  def label_yes_not_datable(value)
    if value
      '<span class="label label-success label-smart-table">&nbsp;Sí&nbsp;</span>'.html_safe
    else
      '<span class="label label-danger label-smart-table">&nbsp;No&nbsp;</span>'.html_safe
    end
  end

  def label_yes_not_datable_inverso(value)
    if value
      '<span class="label label-danger label-smart-table">&nbsp;Sí&nbsp;</span>'.html_safe
    else
      '<span class="label label-success label-smart-table">&nbsp;No&nbsp;</span>'.html_safe
    end
  end

  def label_message_datable(color, message)
    # color: 0 => default, 1: primary, 2: success, 3:info, 4:warning, 5:danger
    case color
      when 0
        ('<span class="label label-default label-smart-table">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 1
        ('<span class="label label-primary label-smart-table">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 2
        ('<span class="label label-success label-smart-table">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 3
        ('<span class="label label-info label-smart-table">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 4
        ('<span class="label label-warning label-smart-table">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 5
        ('<span class="label label-danger label-smart-table">&nbsp;'+message+'&nbsp;</span>').html_safe
    end
  end

  def label_message(color, message)
    # color: 0 => default, 1: primary, 2: success, 3:info, 4:warning, 5:danger
    case color
      when 0
        ('<span class="label label-default">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 1
        ('<span class="label label-primary">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 2
        ('<span class="label label-success">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 3
        ('<span class="label label-info">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 4
        ('<span class="label label-warning">&nbsp;'+message+'&nbsp;</span>').html_safe
      when 5
        ('<span class="label label-danger">&nbsp;'+message+'&nbsp;</span>').html_safe
    end
  end


  def label_active_inactive(value)
    if value
      '<span class="label label-success">Activo</span>'.html_safe
    else
      '<span class="label label-danger">Inactivo</span>'.html_safe
    end
  end

end
