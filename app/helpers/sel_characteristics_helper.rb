module SelCharacteristicsHelper
  def option_color(f)
    if f.object.color
      f.object.color
    else
      '#ffffff'
    end
  end

  def link_to_remove_char_options(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_option(this)")
  end

  def link_to_add_char_options(name, f)
    option = SelOption.new
    fields = f.fields_for(:sel_options, option, :child_index => "new_option") do |builder|
      render('sel_characteristics/form_option', :f => builder)
    end
    link_to_function(name, "add_option(this, \"#{escape_javascript(fields)}\")")
  end
end
