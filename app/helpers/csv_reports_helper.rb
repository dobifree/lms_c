module CsvReportsHelper

  require 'csv'
  #require 'zip/zip'

  def reporte_general_csv(enrollments, user_courses, program_courses, courses, ultimo_estado = false, muestra_char_priv, filtered_chars)

    file_csv_name = generate_reporte_general_csv(enrollments, user_courses, program_courses, courses, ultimo_estado = false, muestra_char_priv, filtered_chars)

    download_file_name = 'ReporteDGeneral'
    #download_file_name += name_modifier
    download_file_name += 'UltimoEstado' if ultimo_estado
    download_file_name += '.csv'

    send_file file_csv_name,
              filename: download_file_name,
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def reporte_det_evals_csv(enrollments, user_courses, program_courses, courses, ultimo_estado = false, muestra_char_priv, filtered_chars)

    file_csv_name = generate_reporte_det_evals_csv(enrollments, user_courses, program_courses, courses, ultimo_estado = false, muestra_char_priv, filtered_chars)

    download_file_name = 'ReporteDetalleEvaluaciones'
    #download_file_name += name_modifier
    download_file_name += 'UltimoEstado' if ultimo_estado
    download_file_name += '.csv'

    send_file file_csv_name,
              filename: download_file_name,
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def reporte_general_csv_con_pivote(files, ultimo_estado)

    zip_tmp_dir = SecureRandom.hex(10)
    zipfile_name = zip_tmp_dir+'.zip'

    file_names = ''

    Dir.mkdir('/tmp/' + zip_tmp_dir)

    files.each do |filename, value|

      report_file_name = 'ReporteGeneral'
      report_file_name += value == '' ? '__Sin Valor__' : '_' + value.gsub(' ', '_')
      report_file_name += 'UltimoEstado' if ultimo_estado
      report_file_name += '.csv'

      file_names += ' ' + File.basename(report_file_name)

      Dir.chdir('/tmp/') {
        File.rename(filename, File.join(zip_tmp_dir, report_file_name))
      }

    end

    Dir.chdir('/tmp/' + zip_tmp_dir + '/') {
      system "zip " + zipfile_name + file_names
    }

    send_file '/tmp/' + zip_tmp_dir + '/' + zipfile_name,
              filename: 'reporte_general.zip',
              type: 'application/octet-stream',
              disposition: 'attachment'

  end

  def reporte_general_csv_remote(enrollments, user_courses, program_courses, courses, ultimo_estado = false, muestra_char_priv)


    characteristics_pub = Characteristic.find_all_by_publica(true)
    characteristics_priv = Characteristic.find_all_by_publica(false)

    characteristics_to_show = LmsCharacteristic.where(:active_reporting => true).count > 0 ? LmsCharacteristic.where(:active_reporting => true).pluck(:characteristic_id) : Characteristic.pluck(:id)

    file_csv_name = SecureRandom.hex(5)+'.csv'

    CSV.open('/tmp/'+file_csv_name, 'wb', {:col_sep => "\t", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#"
      columns.push 'Código'
      columns.push 'Apellidos'
      columns.push 'Nombre'
      columns.push 'Correo Electrónico'
      columns.push '#'
      columns.push 'Curso'
      columns.push 'Programa'
      columns.push 'Duración'
      columns.push 'Fecha Inicio'
      columns.push 'Fecha Fin'
      columns.push 'Estado'

      max_num_evals = 0

      courses.each do |course|

        max_num_evals = course.evaluations.size if course.evaluations.size > max_num_evals

      end


      if max_num_evals > 1

        (1..max_num_evals).each do |num_eval|

          columns.push 'Evaluación '+num_eval.to_s

        end

      end

      columns.push 'Nota Final'
      columns.push 'Resultado Final'
      columns.push 'Asistencia'
      columns.push 'Inicio Real'
      columns.push 'Fin Real'
      characteristics_pub.each do |cha|
        if characteristics_to_show.include?(cha.id)
          columns.push cha.nombre
        end
      end

      if muestra_char_priv

        characteristics_priv.each do |cha|
          if characteristics_to_show.include?(cha.id)
            columns.push cha.nombre
          end
        end

      end

      csv << columns

      user_chars = Hash.new

      user_prev_id = nil
      program_courses_check = nil

      user_courses.each_with_index do |uc, index|

        while !enrollments.empty? && enrollments.first.user_id != uc.user.id && enrollments.first.user.full_name < uc.user.full_name

          enrollment = enrollments.first
          user = enrollment.user

          user_chars[user.id] = user.user_characteristics unless user_chars[user.id]

          program_courses.each do |pc|

            if pc.program_id == enrollment.program_id

              columns = Array.new
              columns.push index+1
              columns.push '="'+user.codigo+'"'
              columns.push user.apellidos
              columns.push user.nombre
              columns.push user.email
              columns.push ''
              columns.push pc.course.nombre
              columns.push pc.program.nombre

              dedicacion = pc.course.dedicacion_estimada*60

              dedicacion_h, dedicacion_m = dedicacion.divmod 60

              dedicacion_h = dedicacion_h < 10 ? '0'+dedicacion_h.to_s : dedicacion_h.to_s
              dedicacion_m = dedicacion_m < 10 ? '0'+dedicacion_m.to_i.to_s : dedicacion_m.to_i.to_s


              columns.push dedicacion_h+':'+dedicacion_m
              columns.push ' '
              columns.push ' '
              columns.push 'No iniciado'

              if max_num_evals > 1

                tmp_num_eval = 1

                pc.course.evaluations.each do |eval|

                  tmp_num_eval += 1

                  columns.push ' '

                end

                if tmp_num_eval <= max_num_evals

                  (tmp_num_eval..max_num_evals).each do |rest_eval|
                    columns.push ' '
                  end

                end

              end

              5.times { columns.push ' ' }

              characteristics_pub.each do |cha|
                if characteristics_to_show.include?(cha.id)
                  user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                  columns.push user_cha ? user_cha.valor : '-'
                end
              end

              if muestra_char_priv

                characteristics_priv.each do |cha|
                  if characteristics_to_show.include?(cha.id)
                    user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                    columns.push user_cha ? user_cha.valor : '-'
                  end
                end

              end

              csv << columns

            end

          end

          enrollments.delete_at 0

        end

        enrollments.delete_at 0 if !enrollments.empty? && enrollments.first.user_id == uc.user.id

        user = uc.user

        program_course = uc.program_course

        user_chars[user.id] = user.user_characteristics unless user_chars[user.id]
        columns = Array.new
        columns.push index+1
        columns.push '="'+user.codigo+'"'
        columns.push user.apellidos
        columns.push user.nombre
        columns.push user.email
        columns.push uc.numero_oportunidad
        columns.push uc.course.nombre
        columns.push program_course.program.nombre

        dedicacion = uc.course.dedicacion_estimada*60

        dedicacion_h, dedicacion_m = dedicacion.divmod 60

        dedicacion_h = dedicacion_h < 10 ? '0'+dedicacion_h.to_s : dedicacion_h.to_s
        dedicacion_m = dedicacion_m < 10 ? '0'+dedicacion_m.to_i.to_s : dedicacion_m.to_i.to_s

        columns.push dedicacion_h+':'+dedicacion_m
        columns.push uc.desde ? localize(uc.desde, format: :day_month_year) : '-'
        columns.push uc.hasta ? localize(uc.hasta, format: :day_month_year) : '-'

        if !uc.iniciado
          columns.push 'No iniciado'
        elsif uc.finalizado
          columns.push 'Terminado'
        else
          columns.push 'Iniciado'
        end

        if max_num_evals > 1

          tmp_num_eval = 1

          uc.course.evaluations.each do |eval|

            tmp_num_eval += 1

            uc_eval = UserCourseEvaluation.where('user_course_id = ? AND evaluation_id = ?', uc.id, eval.id).first

            columns.push (uc_eval && uc_eval.nota) ? uc_eval.nota : '-'

          end

          if tmp_num_eval <= max_num_evals

            (tmp_num_eval..max_num_evals).each do |rest_eval|
              columns.push ' '
            end

          end

        end

        columns.push uc.nota ? uc.nota : '-'


        if uc.aprobado
          columns.push 'aprobado'
        elsif uc.finalizado
          columns.push 'reprobado'
        else
          columns.push '-'
        end

        columns.push uc.porcentaje_avance.to_i.to_s
        columns.push uc.inicio ? localize(uc.inicio, format: :day_month_year) : '-'
        columns.push uc.fin ? localize(uc.fin, format: :day_month_year) : '-'

        characteristics_pub.each do |cha|
          if characteristics_to_show.include?(cha.id)
            user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
            columns.push user_cha ? user_cha.valor : '-'
          end
        end

        if muestra_char_priv

          characteristics_priv.each do |cha|
            if characteristics_to_show.include?(cha.id)
              user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
              columns.push user_cha ? user_cha.valor : '-'
            end
          end

        end

        csv << columns

      end

    end

    return '/tmp/'+file_csv_name

=begin


    download_file_name = 'ReporteGeneral'
    download_file_name += 'UltimoEstado' if ultimo_estado
    download_file_name += '.csv'

    send_file '/tmp/'+file_csv_name,
              filename: download_file_name,
              type: 'application/octet-stream',
              disposition: 'attachment'

=end

  end

  def reporte_csv_por_programa(program_courses, enrollments, query, desde, hasta)


    characteristics_pub = Characteristic.find_all_by_publica(true)
    characteristics_priv = Characteristic.find_all_by_publica(false)

    characteristics_to_show = LmsCharacteristic.where(:active_reporting => true).count > 0 ? LmsCharacteristic.where(:active_reporting => true).pluck(:characteristic_id) : Characteristic.pluck(:id)

    file_csv_name = SecureRandom.hex(5)+'.csv'

    CSV.open('/tmp/'+file_csv_name, 'wb', {:col_sep => "\t", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#"
      columns.push 'Código'
      columns.push 'Apellidos'
      columns.push 'Nombre'
      columns.push 'Correo Electrónico'

      program_courses.each do |program_course|

        columns.push 'Curso'
        columns.push '#'
        columns.push 'Fecha Inicio'
        columns.push 'Fecha Fin'
        columns.push 'Estado'
        columns.push 'Nota Final'
        columns.push 'Resultado Final'
        columns.push 'Asistencia'
        columns.push 'Inicio Real'
        columns.push 'Fin Real'

      end

      characteristics_pub.each do |cha|
        if characteristics_to_show.include?(cha.id)
          columns.push cha.nombre
        end
      end
      characteristics_priv.each do |cha|
        if characteristics_to_show.include?(cha.id)
          columns.push cha.nombre
        end
      end

      csv << columns

      user_chars = Hash.new

      enrollments.each_with_index do |enrollment, index|

        user = enrollment.user

        if enrollment.user_courses.size == 0

          user_chars[user.id] = user.user_characteristics unless user_chars[user.id]
          columns = Array.new
          columns.push index+1
          columns.push '="'+user.codigo+'"'
          columns.push user.apellidos
          columns.push user.nombre
          columns.push user.email

          program_courses.each do |program_course|

            10.times do

              columns.push ''

            end

          end

          characteristics_pub.each do |cha|
            if characteristics_to_show.include?(cha.id)
              user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
              columns.push user_cha ? user_cha.valor : '-'
            end
          end

          characteristics_priv.each do |cha|
            if characteristics_to_show.include?(cha.id)
              user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
              columns.push user_cha ? user_cha.valor : '-'
            end
          end

          csv << columns

        else

          existe_oportunidad = true
          numero_oportunidad = 0

          while existe_oportunidad

            numero_oportunidad += 1

            existen = UserCourse.where('('+query+') AND enrollment_id = ? AND numero_oportunidad = ? AND anulado = ? AND ((desde >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', enrollment.id, numero_oportunidad, false, desde, hasta, desde, hasta, desde, hasta).size

            if existen > 0

              user_chars[user.id] = user.user_characteristics unless user_chars[user.id]

              columns = Array.new
              columns.push index+1
              columns.push '="'+user.codigo+'"'
              columns.push user.apellidos
              columns.push user.nombre
              columns.push user.email

              program_courses.each do |program_course|


                uc = UserCourse.where('enrollment_id = ? AND program_course_id = ? AND numero_oportunidad = ? AND anulado = ? AND ((desde >= ? AND hasta <= ?) OR (inicio >= ? AND inicio <= ?) OR (fin >= ? AND fin <= ?) )', enrollment.id, program_course.id, numero_oportunidad, false, desde, hasta, desde, hasta, desde, hasta).first

                if uc

                  columns.push program_course.course.nombre
                  columns.push uc.numero_oportunidad
                  columns.push uc.desde ? (localize uc.desde, format: :day_month_year) : '-'
                  columns.push uc.hasta ? (localize uc.hasta, format: :day_month_year) : '-'

                  if !uc.iniciado
                    columns.push 'No iniciado'
                  elsif uc.finalizado
                    columns.push 'Terminado'
                  else
                    columns.push 'Iniciado'
                  end

                  columns.push uc.nota ? uc.nota : '-'

                  if uc.aprobado
                    columns.push 'aprobado'
                  elsif uc.finalizado
                    columns.push 'reprobado'
                  else
                    columns.push '-'
                  end

                  columns.push uc.porcentaje_avance.to_i.to_s
                  columns.push uc.inicio ? localize(uc.inicio, format: :day_month_year) : '-'
                  columns.push uc.fin ? localize(uc.fin, format: :day_month_year) : '-'

                else

                  10.times do
                    columns.push ''
                  end

                end

              end

              characteristics_pub.each do |cha|
                if characteristics_to_show.include?(cha.id)
                  user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                  columns.push user_cha ? user_cha.valor : '-'
                end
              end

              characteristics_priv.each do |cha|
                if characteristics_to_show.include?(cha.id)
                  user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                  columns.push user_cha ? user_cha.valor : '-'
                end
              end

              csv << columns

            else

              existe_oportunidad = false

            end

          end

        end


      end

    end


    download_file_name = 'ReportePorPrograma'
    download_file_name += '.csv'

    send_file '/tmp/'+file_csv_name,
              filename: download_file_name,
              type: 'application/octet-stream',
              disposition: 'attachment'


  end

  def reporte_detalle_evaluaciones_csv(program_course, user_courses)

    file_csv_name = SecureRandom.hex(5)+'.csv'

    CSV.open('/tmp/'+file_csv_name, 'wb', {:col_sep => "\t", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#"
      columns.push program_course.course.nombre

      csv << columns

      program_course.course.evaluations.each do |evaluation|

        columns = Array.new
        columns.push 'Evaluación '+evaluation.numero.to_s
        columns.push '# veces preguntada'
        columns.push '# veces respondida'
        columns.push '# veces correcta'

        csv << columns

        evaluation.master_evaluation.master_evaluation_questions.each_with_index do |master_evaluation, index|

          uceqs = UserCourseEvaluationQuestion.joins(:user_course_evaluation).where('evaluation_id = ? AND master_evaluation_question_id = ? AND user_course_id IN (?)',
                                                                                    evaluation.id, master_evaluation.id, user_courses.map { |uc| uc.id })

          columns = Array.new
          texto_q = master_evaluation.texto.split('<br>')

          texto_q_joined = ''

          texto_q.each_with_index do |t_q, numero_lineas|

            if numero_lineas == 0
              texto_q_joined = ActionView::Base.full_sanitizer.sanitize(t_q)
            else
              texto_q_joined += " \n"+ActionView::Base.full_sanitizer.sanitize(t_q)
            end
          end

          columns.push (index+1).to_s+'. '+texto_q_joined

          n_q = 0
          n_q_c = 0
          n_q_nc = 0

          uceqs.each do |uc_evaluation_q|

            n_q += 1

            n_q_nc +=1 if uc_evaluation_q.respondida

            n_q_c += 1 if uc_evaluation_q.respondida && uc_evaluation_q.correcta


          end

          columns.push n_q
          columns.push n_q_nc
          columns.push n_q_c

          csv << columns

        end


      end


    end


    download_file_name = 'DetalleEvaluaciones'
    download_file_name += '.csv'

    send_file '/tmp/'+file_csv_name,
              filename: download_file_name,
              type: 'application/octet-stream',
              disposition: 'attachment'


  end


  def reporte_encuesta_satisfaccion_csv(program_course, user_courses)


    file_csv_name = SecureRandom.hex(5)+'.csv'

    CSV.open('/tmp/'+file_csv_name, 'wb', {:col_sep => "\t", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#"
      columns.push program_course.course.nombre

      csv << columns

      poll = program_course.course.polls.where('tipo_satisfaccion = ?', true).first

      if poll && poll.master_poll

        master_questions = poll.master_poll.master_poll_questions

        master_questions.each do |master_question|

          ucpqs = UserCoursePollQuestion.joins(:user_course_poll).where('poll_id = ? AND finalizada = ? AND master_poll_question_id = ? AND user_course_id IN (?)',
                                                                        poll.id, true, master_question.id, user_courses.map { |uc| uc.id })

          columns = Array.new
          texto_q = master_question.texto.split('<br>')

          texto_q_joined = ''

          texto_q.each_with_index do |t_q, numero_lineas|

            if numero_lineas == 0
              texto_q_joined = ActionView::Base.full_sanitizer.sanitize(t_q)
            else
              texto_q_joined += " \n"+ActionView::Base.full_sanitizer.sanitize(t_q)
            end
          end

          columns.push texto_q_joined

          csv << columns

          lista_alternativas = master_question.master_poll_alternatives

          lista_alternativas.each_with_index do |alt|

            total_resp_alts = 0

            columns = Array.new

            columns.push alt.letra

            texto_a = alt.texto.split('<br>')

            texto_a_joined = ''
            alto_fila = 0
            texto_a.each_with_index do |t_a, numero_lineas|

              if numero_lineas == 0
                texto_a_joined = ActionView::Base.full_sanitizer.sanitize(t_a)
              else
                texto_a_joined += " \n"+ActionView::Base.full_sanitizer.sanitize(t_a)
              end

            end

            columns.push texto_a_joined

            ucpqs.each do |uc_poll_q|

              if uc_poll_q.alternativas

                uc_poll_q_alts = uc_poll_q.alternativas.split('-')

                uc_poll_q_alts.each do |uc_poll_q_alt|

                  total_resp_alts += 1 if uc_poll_q_alt == alt.id.to_s

                end

              end

            end

            columns.push total_resp_alts
            csv << columns

          end

        end


      end


    end


    download_file_name = 'EncuestaSatisfaccion'
    download_file_name += '.csv'

    send_file '/tmp/'+file_csv_name,
              filename: download_file_name,
              type: 'application/octet-stream',
              disposition: 'attachment'


  end


  def generate_reporte_general_csv(enrollments, user_courses, program_courses, courses, ultimo_estado = false, muestra_char_priv, filtered_chars)


    characteristics_pub = Characteristic.find_all_by_publica(true)
    characteristics_priv = Characteristic.find_all_by_publica(false)

    if admin_logged_in?
      characteristics_to_show = Characteristic.pluck(:id)
    else
      characteristics_to_show = LmsCharacteristic.where(:active_reporting => true).count > 0 ? LmsCharacteristic.where(:active_reporting => true).pluck(:characteristic_id) : Characteristic.pluck(:id)
    end

    file_csv_name = SecureRandom.hex(5)+'.csv'

    CSV.open('/tmp/'+file_csv_name, 'wb', {:col_sep => "\t", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#"
      columns.push 'Código'
      columns.push 'Apellidos'
      columns.push 'Nombre'
      columns.push 'Correo Electrónico'
      columns.push '#'
      columns.push 'Curso'
      columns.push 'Programa'
      columns.push 'Duración'
      columns.push 'Fecha Inicio'
      columns.push 'Hora Inicio'
      columns.push 'Fecha Fin'
      columns.push 'Hora Fin'
      columns.push 'Estado'

      max_num_evals = 0

      courses.each do |course|

        max_num_evals = course.evaluations.size if course.evaluations.size > max_num_evals

      end


      if max_num_evals > 1

        (1..max_num_evals).each do |num_eval|

          columns.push 'Evaluación '+num_eval.to_s

        end

      end

      columns.push 'Nota Final'
      columns.push 'Resultado Final'
      columns.push 'Asistencia'
      columns.push 'Fecha Inicio Real'
      columns.push 'Hora Inicio Real'
      columns.push 'Fecha Fin Real'
      columns.push 'Hora Fin Real'
      characteristics_pub.each do |cha|
        if characteristics_to_show.include?(cha.id)
          columns.push cha.nombre
        end
      end

      if muestra_char_priv
        characteristics_priv.each do |cha|
          if characteristics_to_show.include?(cha.id)
            columns.push cha.nombre
          end
        end

      end

      csv << columns

      user_chars = Hash.new

      user_prev_id = nil
      program_courses_check = nil

      user_courses.each_with_index do |uc, index|

        while !enrollments.empty? && enrollments.first.user_id != uc.user.id && enrollments.first.user.full_name < uc.user.full_name

          enrollment = enrollments.first
          user = enrollment.user

          show_register = true

          filtered_chars.each do |key, value|
            if user.user_characteristics.where(:characteristic_id => key, :valor => value).count == 0
              show_register = false
            end
          end

          user_chars[user.id] = user.user_characteristics unless user_chars[user.id]

          program_courses.each do |pc|

            if show_register && pc.program_id == enrollment.program_id

              columns = Array.new
              columns.push index+1
              columns.push '="'+user.codigo+'"'
              columns.push user.apellidos
              columns.push user.nombre
              columns.push user.email
              columns.push ''
              columns.push pc.course.nombre
              columns.push pc.program.nombre

              dedicacion = pc.course.dedicacion_estimada*60

              dedicacion_h, dedicacion_m = dedicacion.divmod 60

              dedicacion_h = dedicacion_h < 10 ? '0'+dedicacion_h.to_s : dedicacion_h.to_s
              dedicacion_m = dedicacion_m < 10 ? '0'+dedicacion_m.to_i.to_s : dedicacion_m.to_i.to_s


              columns.push dedicacion_h+':'+dedicacion_m
              columns.push ' '
              columns.push ' '
              columns.push ' '
              columns.push ' '
              columns.push 'No iniciado'

              if max_num_evals > 1

                tmp_num_eval = 1

                pc.course.evaluations.each do |eval|

                  tmp_num_eval += 1

                  columns.push ' '

                end

                if tmp_num_eval <= max_num_evals

                  (tmp_num_eval..max_num_evals).each do |rest_eval|
                    columns.push ' '
                  end

                end

              end

              5.times { columns.push ' ' }

              characteristics_pub.each do |cha|
                if characteristics_to_show.include?(cha.id)
                  user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                  columns.push user_cha ? user_cha.valor : '-'
                end
              end

              if muestra_char_priv

                characteristics_priv.each do |cha|
                  if characteristics_to_show.include?(cha.id)
                    user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                    columns.push user_cha ? user_cha.valor : '-'
                  end
                end

              end

              csv << columns

            end

          end

          enrollments.delete_at 0

        end

        enrollments.delete_at 0 if !enrollments.empty? && enrollments.first.user_id == uc.user.id

        user = uc.user

        show_register = true

        if filtered_chars
          filtered_chars.each do |key, value|
            if user.user_characteristics.where(:characteristic_id => key, :valor => value).count == 0
              show_register = false
            end
          end
        end

        if show_register
          program_course = uc.program_course

          user_chars[user.id] = user.user_characteristics unless user_chars[user.id]
          columns = Array.new
          columns.push index+1
          columns.push '="'+user.codigo+'"'
          columns.push user.apellidos
          columns.push user.nombre
          columns.push user.email
          columns.push uc.numero_oportunidad
          columns.push uc.course.nombre
          columns.push program_course.program.nombre

          dedicacion = uc.course.dedicacion_estimada*60

          dedicacion_h, dedicacion_m = dedicacion.divmod 60

          dedicacion_h = dedicacion_h < 10 ? '0'+dedicacion_h.to_s : dedicacion_h.to_s
          dedicacion_m = dedicacion_m < 10 ? '0'+dedicacion_m.to_i.to_s : dedicacion_m.to_i.to_s

          columns.push dedicacion_h+':'+dedicacion_m
          columns.push uc.desde ? localize(uc.desde, format: :day_month_year) : '-'
          columns.push uc.desde ? localize(uc.desde, format: :time) : '-'
          columns.push uc.hasta ? localize(uc.hasta, format: :day_month_year) : '-'
          columns.push uc.hasta ? localize(uc.hasta, format: :time) : '-'


          if !uc.iniciado
            columns.push 'No iniciado'
          elsif uc.finalizado
            columns.push 'Terminado'
          else
            columns.push 'Iniciado'
          end

          if max_num_evals > 1

            tmp_num_eval = 1

            uc.course.evaluations.each do |eval|

              tmp_num_eval += 1

              uc_eval = UserCourseEvaluation.where('user_course_id = ? AND evaluation_id = ?', uc.id, eval.id).first

              columns.push (uc_eval && uc_eval.nota) ? uc_eval.nota : '-'

            end

            if tmp_num_eval <= max_num_evals

              (tmp_num_eval..max_num_evals).each do |rest_eval|
                columns.push ' '
              end

            end

          end

          columns.push uc.nota ? uc.nota : '-'


          if uc.aprobado
            columns.push 'aprobado'
          elsif uc.finalizado
            columns.push 'reprobado'
          else
            columns.push '-'
          end

          columns.push uc.porcentaje_avance.to_i.to_s
          columns.push uc.inicio ? localize(uc.inicio, format: :day_month_year) : '-'
          columns.push uc.inicio ? localize(uc.inicio, format: :time) : '-'
          columns.push uc.fin ? localize(uc.fin, format: :day_month_year) : '-'
          columns.push uc.fin ? localize(uc.fin, format: :time) : '-'

          characteristics_pub.each do |cha|
            if characteristics_to_show.include?(cha.id)
              user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
              columns.push user_cha ? user_cha.valor : '-'
            end
          end

          if muestra_char_priv

            characteristics_priv.each do |cha|
              if characteristics_to_show.include?(cha.id)
                user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                columns.push user_cha ? user_cha.valor : '-'
              end
            end

          end

          csv << columns
        end


      end

    end


    return '/tmp/'+file_csv_name


  end

  def generate_reporte_det_evals_csv(enrollments, user_courses, program_courses, courses, ultimo_estado = false, muestra_char_priv, filtered_chars)


    characteristics_pub = Characteristic.find_all_by_publica(true)
    characteristics_priv = Characteristic.find_all_by_publica(false)

    if admin_logged_in?
      characteristics_to_show = Characteristic.pluck(:id)
    else
      characteristics_to_show = LmsCharacteristic.where(:active_reporting => true).count > 0 ? LmsCharacteristic.where(:active_reporting => true).pluck(:characteristic_id) : Characteristic.pluck(:id)
    end

    file_csv_name = SecureRandom.hex(5)+'.csv'

    CSV.open('/tmp/'+file_csv_name, 'wb', {:col_sep => "\t", :encoding => 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFF#"
      columns.push 'Código'
      columns.push 'Apellidos'
      columns.push 'Nombre'
      columns.push 'Correo Electrónico'
      columns.push '#'
      columns.push 'Curso'
      columns.push 'Programa'
      columns.push 'Duración'
      columns.push 'Fecha Inicio'
      columns.push 'Hora Inicio'
      columns.push 'Fecha Fin'
      columns.push 'Hora Fin'
      columns.push 'Estado'

      max_num_evals = 0

      courses.each do |course|

        max_num_evals = course.evaluations.size if course.evaluations.size > max_num_evals

      end


      if max_num_evals >= 1

        (1..max_num_evals).each do |num_eval|

          columns.push 'Evaluación '+num_eval.to_s+ ' correctas'
          columns.push 'Evaluación '+num_eval.to_s+ ' incorrectas'
          columns.push 'Evaluación '+num_eval.to_s+ ' sin contestar'
          columns.push 'Evaluación '+num_eval.to_s+ ' resultado'

        end

      end

      columns.push 'Nota Final'
      columns.push 'Resultado Final'
      columns.push 'Asistencia'
      columns.push 'Fecha Inicio Real'
      columns.push 'Hora Inicio Real'
      columns.push 'Fecha Fin Real'
      columns.push 'Hora Fin Real'
      characteristics_pub.each do |cha|
        if characteristics_to_show.include?(cha.id)
          columns.push cha.nombre
        end
      end

      if muestra_char_priv
        characteristics_priv.each do |cha|
          if characteristics_to_show.include?(cha.id)
            columns.push cha.nombre
          end
        end

      end

      csv << columns

      user_chars = Hash.new

      user_prev_id = nil
      program_courses_check = nil

      user_courses.each_with_index do |uc, index|

        while !enrollments.empty? && enrollments.first.user_id != uc.user.id && enrollments.first.user.full_name < uc.user.full_name

          enrollment = enrollments.first
          user = enrollment.user

          show_register = true

          filtered_chars.each do |key, value|
            if user.user_characteristics.where(:characteristic_id => key, :valor => value).count == 0
              show_register = false
            end
          end

          user_chars[user.id] = user.user_characteristics unless user_chars[user.id]

          program_courses.each do |pc|

            if show_register && pc.program_id == enrollment.program_id

              columns = Array.new
              columns.push index+1
              columns.push '="'+user.codigo+'"'
              columns.push user.apellidos
              columns.push user.nombre
              columns.push user.email
              columns.push ''
              columns.push pc.course.nombre
              columns.push pc.program.nombre

              dedicacion = pc.course.dedicacion_estimada*60

              dedicacion_h, dedicacion_m = dedicacion.divmod 60

              dedicacion_h = dedicacion_h < 10 ? '0'+dedicacion_h.to_s : dedicacion_h.to_s
              dedicacion_m = dedicacion_m < 10 ? '0'+dedicacion_m.to_i.to_s : dedicacion_m.to_i.to_s


              columns.push dedicacion_h+':'+dedicacion_m
              columns.push ' '
              columns.push ' '
              columns.push ' '
              columns.push ' '
              columns.push 'No iniciado'

              if max_num_evals > 1

                tmp_num_eval = 1

                pc.course.evaluations.each do |eval|

                  tmp_num_eval += 1

                  columns.push ' '
                  columns.push ' '
                  columns.push ' '
                  columns.push ' '

                end

                if tmp_num_eval <= max_num_evals

                  (tmp_num_eval..max_num_evals).each do |rest_eval|
                    columns.push ' '
                    columns.push ' '
                    columns.push ' '
                    columns.push ' '
                  end

                end

              end

              5.times { columns.push ' ' }

              characteristics_pub.each do |cha|
                if characteristics_to_show.include?(cha.id)
                  user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                  columns.push user_cha ? user_cha.valor : '-'
                end
              end

              if muestra_char_priv

                characteristics_priv.each do |cha|
                  if characteristics_to_show.include?(cha.id)
                    user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                    columns.push user_cha ? user_cha.valor : '-'
                  end
                end

              end

              csv << columns

            end

          end

          enrollments.delete_at 0

        end

        enrollments.delete_at 0 if !enrollments.empty? && enrollments.first.user_id == uc.user.id

        user = uc.user

        show_register = true

        if filtered_chars
          filtered_chars.each do |key, value|
            if user.user_characteristics.where(:characteristic_id => key, :valor => value).count == 0
              show_register = false
            end
          end
        end

        if show_register
          program_course = uc.program_course

          user_chars[user.id] = user.user_characteristics unless user_chars[user.id]
          columns = Array.new
          columns.push index+1
          columns.push '="'+user.codigo+'"'
          columns.push user.apellidos
          columns.push user.nombre
          columns.push user.email
          columns.push uc.numero_oportunidad
          columns.push uc.course.nombre
          columns.push program_course.program.nombre

          dedicacion = uc.course.dedicacion_estimada*60

          dedicacion_h, dedicacion_m = dedicacion.divmod 60

          dedicacion_h = dedicacion_h < 10 ? '0'+dedicacion_h.to_s : dedicacion_h.to_s
          dedicacion_m = dedicacion_m < 10 ? '0'+dedicacion_m.to_i.to_s : dedicacion_m.to_i.to_s

          columns.push dedicacion_h+':'+dedicacion_m
          columns.push uc.desde ? localize(uc.desde, format: :day_month_year) : '-'
          columns.push uc.desde ? localize(uc.desde, format: :time) : '-'
          columns.push uc.hasta ? localize(uc.hasta, format: :day_month_year) : '-'
          columns.push uc.hasta ? localize(uc.hasta, format: :time) : '-'


          if !uc.iniciado
            columns.push 'No iniciado'
          elsif uc.finalizado
            columns.push 'Terminado'
          else
            columns.push 'Iniciado'
          end

          tmp_num_eval = 1

          uc.course.evaluations.each do |eval|

            tmp_num_eval += 1

            uc_eval = UserCourseEvaluation.where('user_course_id = ? AND evaluation_id = ?', uc.id, eval.id).first

            if uc_eval && uc_eval.nota
              columns.push uc_eval.user_course_evaluation_questions.where('respondida = ? AND correcta = ?',true,true).size
              columns.push uc_eval.user_course_evaluation_questions.where('respondida = ? AND correcta = ?',true,false).size
              columns.push uc_eval.user_course_evaluation_questions.where('respondida = ?',false).size
              columns.push uc_eval.nota
            else
              columns.push '-'
              columns.push '-'
              columns.push '-'
              columns.push '-'
            end

          end

          if tmp_num_eval <= max_num_evals

            (tmp_num_eval..max_num_evals).each do |rest_eval|
              columns.push ' '
              columns.push ' '
              columns.push ' '
              columns.push ' '
            end

          end


          columns.push uc.nota ? uc.nota : '-'


          if uc.aprobado
            columns.push 'aprobado'
          elsif uc.finalizado
            columns.push 'reprobado'
          else
            columns.push '-'
          end

          columns.push uc.porcentaje_avance.to_i.to_s
          columns.push uc.inicio ? localize(uc.inicio, format: :day_month_year) : '-'
          columns.push uc.inicio ? localize(uc.inicio, format: :time) : '-'
          columns.push uc.fin ? localize(uc.fin, format: :day_month_year) : '-'
          columns.push uc.fin ? localize(uc.fin, format: :time) : '-'

          characteristics_pub.each do |cha|
            if characteristics_to_show.include?(cha.id)
              user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
              columns.push user_cha ? user_cha.valor : '-'
            end
          end

          if muestra_char_priv

            characteristics_priv.each do |cha|
              if characteristics_to_show.include?(cha.id)
                user_cha = user_chars[user.id].find { |user_cha| user_cha.characteristic_id == cha.id }
                columns.push user_cha ? user_cha.valor : '-'
              end
            end

          end

          csv << columns
        end


      end

    end


    return '/tmp/'+file_csv_name


  end

end