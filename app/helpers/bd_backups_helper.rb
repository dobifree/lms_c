module BdBackupsHelper

  def icon_bd_backups_module_html_safe
    icon_bd_backups_module.html_safe
  end

  def icon_bd_backups_module
    '<i class="' + icon_bd_backups_module_class + '"></i>'
  end

  def icon_bd_backups_module_class
    'fa fa-database'
  end

end
