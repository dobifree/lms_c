module LicManagerHelper

  def its_safe_approvers(temp_file)
    should_save = true
    to_save = []
    @excel_errors = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-']
    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1]
      user_cod = row[0].to_s
      user = User.where(codigo: user_cod, activo: true).first
      user_id = user ? user.id : nil
      user_cod_2 = row[1].to_s
      user_2 = User.where(codigo: user_cod_2, activo: true).first
      user_id_2 = user_2 ? user_2.id : nil
      if !user_cod || user_cod.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Aprobador no puede ser vacío', row[0], row[1]]
      end
      if !user_cod_2 || user_cod_2.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Usuario a aprobar no puede ser vacío', row[0], row[1]]
      end
      unless user_id
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Aprobador no existe en la plataforma o no está activo', row[0], row[1]]
      end
      unless user_id_2
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Usuario a aprobar no existe en la plataforma o no está activo', row[0], row[1]]
      end
      if should_save
        to_save << [user_id, user_id_2]
      end
    end
    if should_save
      to_save.each do |users|
        approver = LicApprover.where(user_id: users[0]).first_or_initialize
        approver.active = true
        approver.registered_at = lms_time
        approver.registered_by_user_id = user_connected.id
        approver.save
        approvee = LicUserToApprove.where(user_id: users[1], lic_approver_id: approver.id).first_or_initialize
        approvee.active = true
        approvee.registered_at = lms_time
        approvee.registered_by_user_id = user_connected.id
        approvee.save
      end
    end
    return should_save
  end

end
