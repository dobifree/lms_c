module ElecProcessCategoriesHelper

  def link_to_remove_elec_category_char(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_elec_category_char(this)")
  end

  def link_to_add_elec_category_char(name, f)
    category_char = ElecCategoryCharacteristic.new
    fields = f.fields_for(:elec_category_characteristics, category_char, :child_index => "new_elec_category_char") do |builder|
      render('elec_process_categories/characteristics/form', :f => builder)
    end
    link_to_function(name, "add_elec_category_char(this, \"#{escape_javascript(fields)}\")")
  end
  
end
