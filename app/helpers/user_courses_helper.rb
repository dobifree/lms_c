module UserCoursesHelper

  def verify_access_to_unit(user_course, unit)

    acceso_permitido = true
    motivos = []

    if unit.despues_de_unit

      user_course_unit = user_course.user_course_units.find_by_unit_id(unit.despues_de_unit.id)

      unless user_course_unit && user_course_unit.finalizada
        acceso_permitido = false
        motivos += [['u',unit.despues_de_unit.numero]]
      end

    end

    unit.despues_de_units.each do |despues_unit|

      user_course_unit = user_course.user_course_units.find_by_unit_id(despues_unit.id)
      unless user_course_unit && user_course_unit.finalizada
        acceso_permitido = false
        motivos += [['u',despues_unit.numero]] unless motivos.include? ['u',despues_unit.numero]
      end

    end

    if unit.despues_de_evaluation

      user_course_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(unit.despues_de_evaluation.id)

      unless user_course_evaluation && user_course_evaluation.finalizada
        acceso_permitido = false
        motivos += [['e',unit.despues_de_evaluation.numero]]
      end

    end

    unit.despues_de_evaluations.each do |despues_evaluation|

      user_course_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(despues_evaluation.id)
      unless user_course_evaluation && user_course_evaluation.finalizada
        acceso_permitido = false
        motivos += [['e',despues_evaluation.numero]] unless motivos.include? ['e',despues_evaluation.numero]
      end

    end

    if unit.despues_de_poll

      user_course_poll = user_course.user_course_polls.find_by_poll_id(unit.despues_de_poll.id)

      unless user_course_poll && user_course_poll.finalizada
        acceso_permitido = false
        motivos += [['p',unit.despues_de_poll.numero]]
      end

    end

    unit.despues_de_polls.each do |despues_poll|

      user_course_poll = user_course.user_course_polls.find_by_poll_id(despues_poll.id)
      unless user_course_poll && user_course_poll.finalizada
        acceso_permitido = false
        motivos += [['p',despues_poll.numero]] unless motivos.include? ['p',despues_poll.numero]
      end

    end

    if $mobile_client

      if unit.master_unit.content_type == 0
        #flash

        acceso_permitido = false
        motivos += [['f','flash']]

      end

    end

    return acceso_permitido, motivos

  end


  def verify_access_to_evaluation(user_course, evaluation)

    acceso_permitido = true
    motivos = []

    if evaluation.despues_de_unit

      user_course_unit = user_course.user_course_units.find_by_unit_id(evaluation.despues_de_unit.id)

      unless user_course_unit && user_course_unit.finalizada
        acceso_permitido = false
        motivos += [['u',evaluation.despues_de_unit.numero]]
      end

    end

    evaluation.despues_de_units.each do |despues_unit|

      user_course_unit = user_course.user_course_units.find_by_unit_id(despues_unit.id)
      unless user_course_unit && user_course_unit.finalizada
        acceso_permitido = false
        motivos += [['u',despues_unit.numero]] unless motivos.include? ['u',despues_unit.numero]
      end

    end

    if evaluation.despues_de_evaluation

      user_course_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(evaluation.despues_de_evaluation.id)

      unless user_course_evaluation && user_course_evaluation.finalizada
        acceso_permitido = false
        motivos += [['e',evaluation.despues_de_evaluation.numero]]
      end

    end

    evaluation.despues_de_evaluations.each do |despues_evaluation|

      user_course_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(despues_evaluation.id)
      unless user_course_evaluation && user_course_evaluation.finalizada
        acceso_permitido = false
        motivos += [['e',despues_evaluation.numero]] unless motivos.include? ['e',despues_evaluation.numero]
      end

    end

    if evaluation.despues_de_poll

      user_course_poll = user_course.user_course_polls.find_by_poll_id(evaluation.despues_de_poll.id)

      unless user_course_poll && user_course_poll.finalizada
        acceso_permitido = false
        motivos += [['p',evaluation.despues_de_poll.numero]]
      end

    end

    evaluation.despues_de_polls.each do |despues_poll|

      user_course_poll = user_course.user_course_polls.find_by_poll_id(despues_poll.id)
      unless user_course_poll && user_course_poll.finalizada
        acceso_permitido = false
        motivos += [['p',despues_poll.numero]] unless motivos.include? ['p',despues_poll.numero]
      end

    end

    return acceso_permitido, motivos

  end

  def verify_access_to_poll(user_course, poll)

    acceso_permitido = true
    motivos = []

    if poll.despues_de_unit

      user_course_unit = user_course.user_course_units.find_by_unit_id(poll.despues_de_unit.id)

      unless user_course_unit && user_course_unit.finalizada
        acceso_permitido = false
        motivos += [['u',poll.despues_de_unit.numero]]
      end

    end

    poll.despues_de_units.each do |despues_unit|

      user_course_unit = user_course.user_course_units.find_by_unit_id(despues_unit.id)
      unless user_course_unit && user_course_unit.finalizada
        acceso_permitido = false
        motivos += [['u',despues_unit.numero]] unless motivos.include? ['u',despues_unit.numero]
      end

    end

    if poll.despues_de_evaluation

      user_course_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(poll.despues_de_evaluation.id)

      unless user_course_evaluation && user_course_evaluation.finalizada
        acceso_permitido = false
        motivos += [['e',poll.despues_de_evaluation.numero]]
      end

    end

    poll.despues_de_evaluations.each do |despues_evaluation|

      user_course_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(despues_evaluation.id)
      unless user_course_evaluation && user_course_evaluation.finalizada
        acceso_permitido = false
        motivos += [['e',despues_evaluation.numero]] unless motivos.include? ['e',despues_evaluation.numero]
      end

    end

    if poll.despues_de_poll

      user_course_poll = user_course.user_course_polls.find_by_poll_id(poll.despues_de_poll.id)

      unless user_course_poll && user_course_poll.finalizada
        acceso_permitido = false
        motivos += [['p',poll.despues_de_poll.numero]]
      end

    end

    poll.despues_de_polls.each do |despues_poll|

      user_course_poll = user_course.user_course_polls.find_by_poll_id(despues_poll.id)
      unless user_course_poll && user_course_poll.finalizada
        acceso_permitido = false
        motivos += [['p',despues_poll.numero]] unless motivos.include? ['p',despues_poll.numero]
      end

    end

    return acceso_permitido, motivos

  end

  def user_course_status(enrollment, program_course, fecha_hoy = nil)

    #estado_curso: nil / :no_iniciado / :en_progreso / :aprobado / :reprobado / :plazo_vencido
    #estado_boton: nil / :aun_no_iniciar / :iniciar / :continuar / :resultados

    fecha_hoy = lms_time unless fecha_hoy

    user_courses = enrollment.user_courses.where('program_course_id = ? AND anulado = ? ', program_course.id, false).order('id DESC')

    if user_courses.size > 0

      user_course = user_courses.first
      estado_curso = user_course.estado fecha_hoy

      estado_boton = case estado_curso
        when :no_iniciado
          fecha_hoy < user_course.desde ? :aun_no_iniciar : :iniciar
        when :en_progreso
          :continuar
        when :aprobado
          :resultados
        else #:reprobado, :plazo_vencido
          if !enrollment.program.especifico && user_course.numero_oportunidad < user_course.course.numero_oportunidades
            :iniciar
          elsif user_course.iniciado
            :resultados
          end
      end

    elsif !enrollment.program.especifico
        estado_curso = :no_iniciado
        estado_boton = :iniciar
    end

    return estado_curso, estado_boton, user_course, user_courses

  end

  def user_course_status__(enrollment, program_course)

    #estado_curso: no existe = nil, :no_iniciado / :en_progreso / :aprobado / :reprobado / :plazo_vencido / :anulado
    #estado_boton: no existe = nil, :aun_no_iniciar, :iniciar, :continuar, :resultados

    estado_curso = nil
    estado_boton = nil
    estado_reprobado = false
    estado_reprobado_nota = nil
    user_course = nil
    user_courses = nil
    oportunidad_disponible = nil
    requiere_otra_oportunidad = nil

    user_courses = enrollment.user_courses.where('program_course_id = ? AND anulado = ? ', program_course.id, false).reorder('hasta DESC, numero_oportunidad DESC')

    if user_courses.size > 0

      user_course = user_courses.first

      estado_curso = user_course.estado lms_time

      user_courses.each do |uc|

        if uc.iniciado && uc.finalizado && !uc.aprobado

          estado_reprobado = true
          estado_reprobado_nota = uc.nota
          break

        end

      end

      max_op = user_course.numero_oportunidad

      oportunidad_disponible = true if max_op < user_course.course.numero_oportunidades && estado_curso != :en_progreso && estado_curso != :aprobado

      if estado_curso == :no_iniciado
        requiere_otra_oportunidad = false

        if enrollment.program.especifico
          if lms_time < user_course.desde
            estado_boton = :aun_no_iniciar
          else
            estado_boton = :iniciar
          end
        end

      elsif estado_curso == :en_progreso
        estado_boton = :continuar
        requiere_otra_oportunidad = false
      elsif estado_curso == :aprobado
        estado_boton = :resultados
        requiere_otra_oportunidad = false
      elsif estado_curso == :reprobado
        requiere_otra_oportunidad = true

        if enrollment.program.especifico
          estado_boton = :resultados
        else
          if oportunidad_disponible
            estado_boton = :iniciar
          else
            estado_boton = :resultados
          end
        end

      elsif estado_curso == :plazo_vencido
        requiere_otra_oportunidad = true

        if enrollment.program.especifico
          if user_course.iniciado
            estado_boton = :resultados
          else
            estado_boton = nil
          end
        else
          if oportunidad_disponible
            estado_boton = :iniciar
          else
            if user_course.iniciado
              estado_boton = :resultados
            else
              estado_boton = nil
            end
          end
        end

      end

    else
      unless enrollment.program.especifico
        estado_curso = :no_iniciado
        estado_boton = :iniciar
        oportunidad_disponible = true
      end
    end


    return estado_curso, estado_boton, estado_reprobado, estado_reprobado_nota, user_course, user_courses, oportunidad_disponible, requiere_otra_oportunidad

  end

  def user_course_status_(enrollment, program_course)

    #estado_curso: no existe = nil, :no_iniciado / :en_progreso / :aprobado / :reprobado / :plazo_vencido / :anulado
    #estado_boton: no existe = nil, :iniciar, :continuar, :resultados

    estado_curso = nil
    estado_boton = nil

    estado_reprobado_nota = nil
    user_course = nil
    user_courses = nil
    fecha_tope = nil
    oportunidad_disponible = nil
    requiere_otra_oportunidad = nil

    if enrollment.program.id == program_course.program.id

      if enrollment.program.especifico

        user_courses = enrollment.user_courses.where('program_course_id = ? AND anulado = ? ', program_course.id, false).reorder('hasta DESC, numero_oportunidad DESC')

        if user_courses.length > 0

          user_course = user_courses.first

          estado_reprobado = false
          estado_reprobado_nota = 0
          estado_iniciado = false

          user_courses.each do |uc|

            estado_iniciado = true if uc.iniciado

            if uc.iniciado && uc.finalizado && !uc.aprobado

              estado_reprobado = true
              estado_reprobado_nota = uc.nota
              break

            end

          end

          if user_course.iniciado

            if user_course.finalizado

               if user_course.aprobado
                 estado_curso = :aprobado
                 estado_boton = :resultados
                 requiere_otra_oportunidad = false
               else
                 estado_curso = :reprobado
                 estado_boton = :resultados
                 requiere_otra_oportunidad = true
               end

            else

              fecha_tope = user_course.hasta

              fecha_now = lms_time

              if user_course.limite_tiempo
                fecha_tope = user_course.fin
                fecha_now = lms_time
              end

              if fecha_now > fecha_tope

                if estado_reprobado
                  #Reprobado
                  estado_curso = :reprobado
                else
                  #No terminado
                  estado_curso = :plazo_vencido
                end

                requiere_otra_oportunidad = true
                estado_boton = :resultados
              else
                #En progreso
                estado_curso = :en_progreso
                estado_boton = :continuar
                requiere_otra_oportunidad = false
              end

            end

          else

            if estado_reprobado
              #Reprobado
              estado_curso = :reprobado

            else
              if estado_iniciado
                #No terminado
                estado_curso = :plazo_vencido
              else
                #No iniciado
                estado_curso = :no_iniciado
              end
            end

            if lms_time > user_course.hasta
              requiere_otra_oportunidad = true
              if estado_curso == :reprobado || estado_curso == :plazo_vencido
                estado_boton = :resultados
              end
            else
              requiere_otra_oportunidad = false
              estado_boton = :iniciar
            end


          end

        end

      else

        user_courses = enrollment.user_courses.where('program_course_id = ? AND anulado = ? ', program_course.id, false).reorder('numero_oportunidad DESC')

        if user_courses.length > 0

          user_course = user_courses.first

          estado_reprobado = false
          estado_reprobado_nota = 0

          user_courses.each do |uc|

            if uc.iniciado && uc.finalizado && !uc.aprobado
              estado_reprobado = true
              estado_reprobado_nota = uc.nota
              break
            end

          end

          if user_course.iniciado

            if user_course.finalizado

              if user_course.aprobado
                estado_curso = :aprobado
              else
                estado_curso = :reprobado
              end

              estado_boton = :resultados

            else

              if user_course.limite_tiempo && lms_time > user_course.fin

                if estado_reprobado
                  #Reprobado
                  estado_curso = :reprobado
                else
                  #No terminado
                  estado_curso = :plazo_vencido
                end

                estado_boton = :resultados

              else
                #En progreso
                estado_curso = :en_progreso
                estado_boton = :continuar
                #fecha_tope = user_course.fin if user_course.limite_tiempo

              end

            end

          else

            #No iniciado
            estado_curso = :no_iniciado
            estado_boton = :iniciar
          end

          max_op = user_course.numero_oportunidad

          if max_op < user_course.course.numero_oportunidades && estado_curso != :en_progreso && estado_curso != :aprobado

            oportunidad_disponible = true

          end

        else
          estado_curso = :no_iniciado
          oportunidad_disponible = true
          estado_boton = :iniciar
        end

      end

    end

    return estado_curso, estado_reprobado_nota, user_course, user_courses, fecha_tope, oportunidad_disponible, requiere_otra_oportunidad, estado_boton

  end



end
