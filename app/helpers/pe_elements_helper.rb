module PeElementsHelper

  def link_to_add_rank_non_discrete_model()

    form = render partial: 'pe_elements/rank_non_discrete_model',
                  locals: {
                      pe_question_rank_model: nil,
                      goal: '',
                      percentage: ''
                  }

    link_to_function('<i class="fa fa-plus" aria-hidden="true"></i>'.html_safe, "add_rank_non_discrete_model(\"#{escape_javascript(form)}\")")

  end


  def link_to_add_rank_discrete_model()

    form = render partial: 'pe_elements/rank_discrete_model',
                  locals: {
                      pe_question_rank_model: nil,
                      goal: '',
                      percentage: ''
                  }

    link_to_function('<i class="fa fa-plus" aria-hidden="true"></i>'.html_safe, "add_rank_discrete_model(\"#{escape_javascript(form)}\")")

  end

end
