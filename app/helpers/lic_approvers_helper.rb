module LicApproversHelper
  def complete_change_status(request_id, params)
    request = LicRequest.find(request_id)
    updated_request = request.dup
    updated_request.assign_attributes(params)
    updated_request.registered_at = lms_time
    updated_request.registered_by_user_id = user_connected.id
    updated_request.prev_lic_request_id = request.id
    return updated_request
  end

  def set_items_values(prev_request_id, request_id)
    prev_request = LicRequest.find(prev_request_id)
    request = LicRequest.find(request_id)

    prev_request.lic_item_values.each do |item_value|
      new_item_value = item_value.dup
      new_item_value.registered_at = lms_time
      new_item_value.registered_by_user_id = user_connected.id
      new_item_value.lic_request_id = request.id
      new_item_value.save
    end
  end

  def deactivate_request(request_id)
    request = LicRequest.find(request_id)
    request.active = false
    request.deactivated_at = lms_time
    request.deactivated_by_user_id = user_connected.id
    return request
  end
end
