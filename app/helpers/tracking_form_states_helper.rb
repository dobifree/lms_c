module TrackingFormStatesHelper

  def link_to_remove_state_notification(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_state_notification(this); return false")
  end

  def link_to_add_state_notification(name, f)
    notification = TrackingStateNotification.new
    fields = f.fields_for(:tracking_state_notifications, notification, :child_index => "new_state_notification") do |builder|
      render('tracking_form_states/notifications/form', :f => builder)
    end
    link_to(name, '#', onclick: "add_state_notification(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
