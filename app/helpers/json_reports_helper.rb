module JsonReportsHelper

  def active_user_characteristics(user)

    codigo = user.codigo ? user.codigo : nil
    apellidos = user.apellidos ? user.apellidos : nil
    nombre = user.nombre ? user.nombre : nil
    email = user.email ? user.email : nil
    employee = user.employee ? 'Empleado' : 'Externo'
    activo = user.activo ? 'Activo' : 'Inactivo'
    foto = user.foto ? 'Si' : 'No'
    usuario = { codigo: codigo , apellidos: apellidos, nombre: nombre, email: email, employee: employee, activo: activo, foto: foto }

    anexo = characteristic_value_json(user, 14) ? characteristic_value_json(user, 14) : nil
    cargo = characteristic_value_json(user, 2) ? characteristic_value_json(user, 2) : nil
    empresa = characteristic_value_json(user, 2) ? characteristic_value_json(user, 2) : nil
    fecha_nacimiento = characteristic_value_json(user, 26) ? characteristic_value_json(user, 26) : nil
    fecha_ingreso = characteristic_value_json(user, 3) ? characteristic_value_json(user, 3) : nil
    direccion_oficina = characteristic_value_json(user, 10) ? characteristic_value_json(user, 10) : nil
    comuna_oficina = characteristic_value_json(user, 13) ? characteristic_value_json(user, 13) : nil
    region_oficina = characteristic_value_json(user, 11) ? characteristic_value_json(user, 11) : nil
    pais_oficina = characteristic_value_json(user, 45) ? characteristic_value_json(user, 45) : nil
    rut_jefe_directo = characteristic_value_json(user, 8) ? characteristic_value_json(user, 8) : nil

    datos_características = {anexo: anexo, cargo: cargo, empresa: empresa, fecha_nacimiento: fecha_nacimiento, fecha_ingreso: fecha_ingreso, direccion_oficina: direccion_oficina, comuna_oficina: comuna_oficina,
                             region_oficina: region_oficina, pais_oficina: pais_oficina, rut_jefe_directo: rut_jefe_directo}


    user_group = BpGroupsUser.active?(lms_time, user.id)

    bp_group_name = user_group && user_group.bp_group.active?(lms_time) ? user_group.bp_group.name : nil
    bp_group_description = user_group && user_group.bp_group.active?(lms_time) ? user_group.bp_group.description : nil

    otros = {bp_group_name: bp_group_name, bp_group_description: bp_group_description}


    datos_usuario = {datos_personales: usuario, datos_característica: datos_características, otros: otros}

      return datos_usuario
  end

  private

  def characteristic_value_json(user, characteristic_id)

    characteristic_types = Array.new
    characteristics = Array.new

    CharacteristicType.all.each do |characteristic_type|
      ec = characteristic_type.excel_report_available_characteristics
      characteristics += ec
      if ec.size > 0
        characteristic_types.push characteristic_type
      end
    end

    characteristics += Characteristic.excel_report_available_characteristics
    characteristic_search = ''
    characteristics.each do |characteristic|
      if characteristic.id == characteristic_id
        characteristic_search = user.excel_formatted_characteristic_value(characteristic)
      end
    end
    characteristic_search
  end

end