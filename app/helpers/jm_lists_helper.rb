module JmListsHelper
  def link_to_remove_jm_options(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_option(this)")
  end

  def link_to_add_jm_options(name, f)
    option = JmOption.new
    fields = f.fields_for(:jm_options, option, :child_index => "new_option") do |builder|
      render('jm_lists/form_option', :f => builder)
    end
    link_to_function(name, "add_option(this, \"#{escape_javascript(fields)}\")")
  end
end
