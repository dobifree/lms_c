module HeCtModulePrivilagesHelper
  def its_safe( temp_file )
    should_save = true

    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      data = []
      should_save = false
      @error_index << '-'
      @error_content << ['-', '-']
      @error_message << t('views.ben_cellphone_number.error.format_error')
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      data = []
      should_save = false
      @error_index << '-'
      @error_content << ['-', '-']
      @error_message << t('views.ben_cellphone_number.error.formula_error')
    end

    data.each_with_index do |row, index|
      if index > 0
        break if !row[0] && !row[1]

        if !row[0] || row[0] == ''
          should_save = false
          @error_index << (index + 1).to_s
          @error_content << [row[0], row[1]]
          @error_message << 'Registrador no puede ser vacío'
        end

        if !row[1] || row[1] == ''
          should_save = false
          @error_index << (index + 1).to_s
          @error_content << [row[0], row[1]]
          @error_message << 'Usuario a registrar no puede ser vacío'
        end

        recorder_cod = row[0].to_s
        recorder_id = User.where(codigo: recorder_cod, activo: true).first ? User.where(:codigo => recorder_cod, activo:true).first.id : '-1'
        user_to_register_cod = row[1].to_s
        user_to_register_id = User.where(codigo: user_to_register_cod, activo: true).first ? User.where(:codigo => user_to_register_cod, activo: true).first.id : '-1'

        if recorder_id == '-1' && recorder_cod != ''
          @error_index << (index + 1).to_s
          @error_content << [row[0], row[1]]
          @error_message << 'Registrador no existe en la plataforma o no está activo'
          should_save = false
        end

        if user_to_register_id == '-1' && user_to_register_cod != ''
          @error_index << (index + 1).to_s
          @error_content << [row[0], row[1]]
          @error_message << 'Usuario a registrar no existe en la plataforma o no está activo'
          should_save = false
        end

      end
    end

    if should_save
      data.each_with_index do |row, index|
        if index > 0
          break unless row[0] && row[1]
          recorder_cod = row[0].to_s
          recorder_id = User.where(codigo: recorder_cod).first ? User.where(:codigo => recorder_cod).first.id : '-1'
          user_to_register_cod = row[1].to_s
          user_to_register_id = User.where(codigo: user_to_register_cod).first ? User.where(:codigo => user_to_register_cod).first.id : '-1'

          recorder = HeCtModulePrivilage.where(user_id: recorder_id).first_or_create
          recorder.active = true
          recorder.recorder = true
          recorder.deactivated_at = nil
          recorder.deactivated_by_user_id = nil

          recorder.registered_at = lms_time unless recorder.registered_at
          recorder.registered_by_user_id = user_connected.id unless recorder.registered_by_user_id
          recorder.save

          user_to_register = ScUserToRegister.where(user_id: user_to_register_id, he_ct_module_privilage_id: recorder.id).first_or_create
          user_to_register.active = true
          user_to_register.deactivated_at = nil
          user_to_register.deactivated_by_user_id = nil

          user_to_register.registered_at = lms_time unless recorder.registered_at
          user_to_register.registered_by_user_id = user_connected.id unless recorder.registered_by_user_id
          user_to_register.save
        end
      end
    end
    return should_save
  end
end
