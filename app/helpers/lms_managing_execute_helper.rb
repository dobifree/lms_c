module LmsManagingExecuteHelper

  def enrollment_is_ok?

    program_courses = lms_detail_courses_to_enroll

    unless program_courses
      return false
    end

    # se quita validación de usuarios seleccionados para la matrícula para
    # resolver la generación de instancias sin listas y matricular en la asistencia (con pistola)
    # pedido por pacasmayo 20170906

    #unless lms_list_users_to_enroll
      #return false
    #end

    program_courses.each_key do |program_course_id|

      program_course = ProgramCourse.find(program_course_id)
      unless program_courses[program_course_id]['desdehasta']
        return false
      else
        if program_courses[program_course_id]['desdehasta'].blank?
          return false
        else
          desdehasta = program_courses[program_course_id]['desdehasta'].split(' - ')
          desde = DateTime.strptime(desdehasta[0], '%d/%m/%Y %I:%M %p')
          hasta = DateTime.strptime(desdehasta[1], '%d/%m/%Y %I:%M %p')

          if program_courses[program_course_id]['dates_by_element'] == '1'
            elements, elements_ids = program_course.course.ordered_elements
            elements.each_with_index do |element, index_el|
              ei = elements_ids[index_el].gsub('-', '_')
              field_name = 'desdehasta_'+program_course.id.to_s+'_' + ei
              unless program_courses[program_course_id]['desdehasta_elements'][field_name].blank?
                desdehasta2 = program_courses[program_course_id]['desdehasta_elements'][field_name].split(' - ') #params['desdehasta_'+program_course.id.to_s+'_'+tipo+'_'+element.id.to_s].split(' - ')
                desde2 = DateTime.strptime(desdehasta2[0], '%d/%m/%Y %I:%M %p')
                hasta2 = DateTime.strptime(desdehasta2[1], '%d/%m/%Y %I:%M %p')
                if desde > desde2 || hasta < hasta2
                  return false
                end
              else
                return false
              end
            end
          end
        end
      end
    end

  end

end
