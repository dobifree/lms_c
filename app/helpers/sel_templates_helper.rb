module SelTemplatesHelper

  def icon_sel_module_html_safe
    icon_sel_module.html_safe
  end

  def icon_sel_module
    '<i class="' + icon_sel_module_class + '"></i>'
  end

  def icon_sel_module_class
    'fa fa-hand-o-right'
  end

  def is_apply_form_deletable?(applyForm)

    if SelProcess.find_all_by_sel_template_id(applyForm.sel_template_id).count > 0
      return false
    end

    return true
  end

  def is_sel_characteristic_deletable?(charTemplate)

    if SelProcess.find_all_by_sel_template_id(charTemplate.sel_template_id).count > 0
      return false
    end

    return true
  end

end
