module ExcelReportsHelper

  #include ActionView::Helpers::SanitizeHelper

  def letras_excel

    letras = %w(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI AJ AK AL AM AN AO AP AQ AR AS AT AU AV AW AX AY AZ BA BB BC BD BE BF BG BH BI BJ BK BL BM BN BO BP BQ BR BS BT BU BV BW BX BY BZ CA CB CC CD CE CF CG CH CI CJ CK CL CM CN CO CP CQ CR CS CT CU CV CW CX CY CZ)

  end

  def parametros_design_excel

    my_domains = %w(localhost lms lms-dev hochschild euroamerica-dev hochschild-training epacasmayo epacasmayo-training corpvida pacificocrece pacificocrece-training fundacionsofofa inkafarma inkafarma-training)

    main_background_color = {
        'localhost'               => '1a1a1a',
        'lms'                     => '1a1a1a',
        'lms-dev'                 => '1a1a1a',
        'euroamerica-dev'         => '1a1a1a',
        'hochschild'              => 'BE9F56',
        'hochschild-training'     => 'BE9F56',
        'epacasmayo'              => '4F0D08',
        'epacasmayo-training'     => '4F0D08',
        'corpvida'                => '7BC142',
        'pacificocrece'           => '009FCD',
        'pacificocrece-training'  => '009FCD',
        'fundacionsofofa'         => '0E2958',
        'inkafarma-training'      => 'FFED00',
        'inkafarma'               => 'FFED00'
    }

    menu_text_color = {
        'localhost'               => 'ffffff',
        'lms'                     => 'ffffff',
        'lms-dev'                 => 'ffffff',
        'euroamerica-dev'         => 'ffffff',
        'hochschild'              => '000000',
        'hochschild-training'     => '000000',
        'epacasmayo'              => 'ffffff',
        'epacasmayo-training'     => 'ffffff',
        'corpvida'                => 'ffffff',
        'pacificocrece'           => 'ffffff',
        'pacificocrece-training'  => 'ffffff',
        'fundacionsofofa'         => 'ffffff',
        'inkafarma-training'      => 'ffffff',
        'inkafarma'               => 'ffffff'
    }

    secundary_background_color = {
        'localhost'               => 'f1f1f1',
        'lms'                     => 'f1f1f1',
        'lms-dev'                 => 'f1f1f1',
        'euroamerica-dev'         => 'f1f1f1',
        'hochschild'              => 'f1f1f1',
        'hochschild-training'     => 'f1f1f1',
        'epacasmayo'              => 'f1f1f1',
        'epacasmayo-training'     => 'f1f1f1',
        'corpvida'                => 'f1f1f1',
        'pacificocrece'           => 'f1f1f1',
        'pacificocrece-training'  => 'f1f1f1',
        'fundacionsofofa'         => 'f1f1f1',
        'inkafarma-training'      => 'f1f1f1',
        'inkafarma'               => 'f1f1f1'
    }

    return my_domains, main_background_color, menu_text_color, secundary_background_color

  end


  def reporte_curso_excel(nombre_reporte, course, user_courses, company, rango = nil, desde = nil, hasta = nil, show_characteristics_priv = nil)

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => nombre_reporte) do |reporte_excel|

        si_hay_sence = false

        si_hay_sence = true if course && company.modulo_sence && course.sence_code && course.sence_code != ''

        fila_actual = 1

        if course

          fila = Array.new
          filaHeader = Array.new

          fila << 'Curso'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo
          fila << course.nombre
          filaHeader << headerTitleInfoDet
          fila << ''
          filaHeader << headerTitleInfoDet
          fila << ''
          filaHeader << headerTitleInfoDet
          fila << ''
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
          reporte_excel.merge_cells('C'+fila_actual.to_s+':E'+fila_actual.to_s)

          fila_actual += 1

        end

        if rango

          fila = Array.new
          filaHeader = Array.new

          fila << 'Vigencia'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo


          fila << 'Del '+desde+' Al '+hasta
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        if course

          fila = Array.new
          filaHeader = Array.new

          fila << 'Nota Mínima para Aprobar'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo

          fila << course.nota_minima
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'DATOS PERSONALES'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        fila << 'DATOS CONVOCATORIA'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        unless course

          fila << ''
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter

        end

        fila << 'DATOS AVANCE EN CURSO'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        if si_hay_sence

          fila << ''
          filaHeader <<  headerCenter

          fila << ''
          filaHeader <<  headerCenter

        end

        characteristics = Characteristic.find_all_by_publica(true)

        characteristics_porcentaje_sence = Characteristic.find_by_porcentaje_participante_sence(true)

        characteristics_priv = Characteristic.find_all_by_publica(false)

        if characteristics.length > 0

          fila << 'DATOS DEL ALUMNO'
          filaHeader <<  headerCenter

          (1..characteristics.size-1).each do |n|

            fila << ''
            filaHeader <<  headerCenter

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        letra_pos1 = 0
        letra_pos2 = 4

        #datos personales
        reporte_excel.merge_cells(letras[letra_pos1]+fila_actual.to_s+':'+letras[letra_pos2]+fila_actual.to_s)

        letra_pos1 = letra_pos2+1
        letra_pos2 = letra_pos1 + (course ? 3:5)

        #datos convocatoria
        reporte_excel.merge_cells(letras[letra_pos1]+fila_actual.to_s+':'+letras[letra_pos2]+fila_actual.to_s)


        letra_pos1 = letra_pos2 + 1
        letra_pos2 = letra_pos1 + (si_hay_sence ? 7:5)

        #datos avance curso
        reporte_excel.merge_cells(letras[letra_pos1]+fila_actual.to_s+':'+letras[letra_pos2]+fila_actual.to_s)

        if characteristics.length > 0

          letra_pos1 = letra_pos2 + 1
          letra_pos2 = letra_pos1 + characteristics.length-1

          #caracteristicas
          reporte_excel.merge_cells(letras[letra_pos1]+fila_actual.to_s+':'+letras[letra_pos2]+fila_actual.to_s)

        end

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Correo Electrónico'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        unless course

          fila << 'Curso'
          filaHeader <<  headerLeft
          cols_widths << 20

          fila << 'Programa'
          filaHeader <<  headerLeft
          cols_widths << 20

        end

        fila << 'Duración'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fecha Inicio'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fecha Fin'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Nota Final'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Resultado Final'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Inicio Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fin Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        if si_hay_sence

          fila << 'Inscripción Sence'
          filaHeader <<  headerLeft
          cols_widths << 15

          fila << 'Código Sence'
          filaHeader <<  headerLeft
          cols_widths << 15

        end

        characteristics.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end

        if show_characteristics_priv

          characteristics_priv.each do |cha|

            fila << cha.nombre
            filaHeader <<  headerLeft
            cols_widths << 22

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila_actual += 1

        user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          fila << uc.enrollment.user.email
          fileStyle << fieldLeft

          fila << uc.numero_oportunidad
          fileStyle << fieldLeft

          unless course

            fila << uc.course.nombre
            fileStyle << fieldLeft

            fila << uc.program_course.program.nombre
            fileStyle << fieldLeft

          end

          dedicacion = uc.course.dedicacion_estimada*60

          dedicacion_h, dedicacion_m = dedicacion.divmod 60

          dedicacion_h = dedicacion_h < 10 ? '0'+dedicacion_h.to_s : dedicacion_h.to_s
          dedicacion_m = dedicacion_m < 10 ? '0'+dedicacion_m.to_i.to_s : dedicacion_m.to_i.to_s

          fila << dedicacion_h+':'+dedicacion_m
          fileStyle << fieldLeft

          if uc.desde
            fila << (localize  uc.desde, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.hasta
            fila << (localize  uc.hasta, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft


          if !uc.iniciado
            fila << 'No iniciado'
          else

            if uc.finalizado
              fila << 'Terminado'
            else
              fila << 'Iniciado'
            end

          end

          fileStyle << fieldLeft

          if uc.nota
            fila << uc.nota
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.aprobado
            fila << 'aprobado'
          else

            if uc.finalizado

              fila << 'reprobado'

            else

              fila << '-'

            end


          end
          fileStyle << fieldLeft

          fila << uc.porcentaje_avance.to_i.to_s+'%'
          fileStyle << fieldLeft

          if uc.inicio
            fila << (localize  uc.inicio, format: :day_month_year_time_guion)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.fin
            fila << (localize  uc.fin, format: :day_month_year_time_guion)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if si_hay_sence

            if uc.aplica_sence
              fila << 'Sí'
            else
              fila << 'No'
            end
            fileStyle << fieldLeft

            if uc.aplica_sence
              fila << course.sence_code
            else
              fila << '-'
            end
            fileStyle << fieldLeft

          end



          characteristics.each do |cha|

            user_cha = uc.enrollment.user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end

          if show_characteristics_priv

            characteristics_priv.each do |cha|

              user_cha = uc.enrollment.user.characteristic_by_id(cha.id)

              val_cha = '-'
              if user_cha
                val_cha = user_cha.valor
              end

              fila << val_cha
              fileStyle << fieldLeft

            end

          end

          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def reporte_detallado_curso_excel(nombre_reporte, course, user_courses, company, rango = nil, desde = nil, hasta = nil, show_characteristics_priv = nil)

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => nombre_reporte) do |reporte_excel|

        si_hay_sence = false

        si_hay_sence = true if course && company.modulo_sence && course.sence_code && course.sence_code != ''

        fila_actual = 1

        if course

          fila = Array.new
          filaHeader = Array.new

          fila << 'Curso'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo
          fila << course.nombre
          filaHeader << headerTitleInfoDet
          fila << ''
          filaHeader << headerTitleInfoDet
          fila << ''
          filaHeader << headerTitleInfoDet
          fila << ''
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
          reporte_excel.merge_cells('C'+fila_actual.to_s+':E'+fila_actual.to_s)

          fila_actual += 1

        end

        if rango

          fila = Array.new
          filaHeader = Array.new

          fila << 'Vigencia'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo


          fila << 'Del '+desde+' Al '+hasta
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        if course

          fila = Array.new
          filaHeader = Array.new

          fila << 'Nota Mínima para Aprobar'
          filaHeader << headerTitleInfo
          fila << ''
          filaHeader << headerTitleInfo

          fila << course.nota_minima
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'DATOS PERSONALES'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter

        fila << 'DATOS CONVOCATORIA'
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        unless course

          fila << ''
          filaHeader <<  headerCenter
          fila << ''
          filaHeader <<  headerCenter

        end

        fila << 'DATOS AVANCE EN CURSO'
        filaHeader <<  headerCenter

        if course.evaluations.size > 1

          (1..course.evaluations.size).each do |n_eval|

            fila << ''
            filaHeader <<  headerCenter

          end

        end

        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter
        fila << ''
        filaHeader <<  headerCenter



        if si_hay_sence

          fila << ''
          filaHeader <<  headerCenter

          fila << ''
          filaHeader <<  headerCenter

        end

        characteristics = Characteristic.find_all_by_publica(true)

        characteristics_porcentaje_sence = Characteristic.find_by_porcentaje_participante_sence(true)

        characteristics_priv = Characteristic.find_all_by_publica(false)

        if characteristics.length > 0

          fila << 'DATOS DEL ALUMNO'
          filaHeader <<  headerCenter

          (1..characteristics.size-1).each do |n|

            fila << ''
            filaHeader <<  headerCenter


          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        letra_pos1 = 0
        letra_pos2 = 4

        #datos personales
        reporte_excel.merge_cells(letras[letra_pos1]+fila_actual.to_s+':'+letras[letra_pos2]+fila_actual.to_s)

        letra_pos1 = letra_pos2+1
        letra_pos2 = letra_pos1 + (course ? 3:5)

        #datos convocatoria
        reporte_excel.merge_cells(letras[letra_pos1]+fila_actual.to_s+':'+letras[letra_pos2]+fila_actual.to_s)


        letra_pos1 = letra_pos2 + 1
        letra_pos2 = letra_pos1 + (si_hay_sence ? 7:5) + (course.evaluations.size > 1 ? course.evaluations.size : 0)

        #datos avance curso
        reporte_excel.merge_cells(letras[letra_pos1]+fila_actual.to_s+':'+letras[letra_pos2]+fila_actual.to_s)

        if characteristics.length > 0

          letra_pos1 = letra_pos2 + 1
          letra_pos2 = letra_pos1 + characteristics.length-1

          #caracteristicas
          reporte_excel.merge_cells(letras[letra_pos1]+fila_actual.to_s+':'+letras[letra_pos2]+fila_actual.to_s)

        end

        fila_actual += 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        fila << 'Código'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Apellidos'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Nombre'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << 'Correo Electrónico'
        filaHeader <<  headerLeft
        cols_widths << 22

        fila << '#'
        filaHeader <<  headerLeft
        cols_widths << 6

        unless course

          fila << 'Curso'
          filaHeader <<  headerLeft
          cols_widths << 20

          fila << 'Programa'
          filaHeader <<  headerLeft
          cols_widths << 20

        end

        fila << 'Duración'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fecha Inicio'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fecha Fin'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Estado'
        filaHeader <<  headerLeft
        cols_widths << 10

        if course.evaluations.size > 1

          course.evaluations.each do |eval|

            fila << 'Evaluación '+eval.numero.to_s
            filaHeader <<  headerLeft
            cols_widths << 10

          end

        end

        fila << 'Nota Final'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Resultado Final'
        filaHeader <<  headerLeft
        cols_widths << 15

        fila << 'Asistencia'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Inicio Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        fila << 'Fin Real'
        filaHeader <<  headerLeft
        cols_widths << 10

        if si_hay_sence

          fila << 'Inscripción Sence'
          filaHeader <<  headerLeft
          cols_widths << 15

          fila << 'Código Sence'
          filaHeader <<  headerLeft
          cols_widths << 15

        end

        characteristics.each do |cha|

          fila << cha.nombre
          filaHeader <<  headerLeft
          cols_widths << 22

        end

        if show_characteristics_priv

          characteristics_priv.each do |cha|

            fila << cha.nombre
            filaHeader <<  headerLeft
            cols_widths << 22

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila_actual += 1

        user_courses.each_with_index do |uc, index|

          fila = Array.new
          fileStyle = Array.new

          fila << index+1
          fileStyle << fieldLeft

          fila << uc.enrollment.user.codigo
          fileStyle << fieldLeft

          fila << uc.enrollment.user.apellidos
          fileStyle << fieldLeft

          fila << uc.enrollment.user.nombre
          fileStyle << fieldLeft

          fila << uc.enrollment.user.email
          fileStyle << fieldLeft

          fila << uc.numero_oportunidad
          fileStyle << fieldLeft

          unless course

            fila << uc.course.nombre
            fileStyle << fieldLeft

            fila << uc.program_course.program.nombre
            fileStyle << fieldLeft

          end

          dedicacion = uc.course.dedicacion_estimada*60

          dedicacion_h, dedicacion_m = dedicacion.divmod 60

          dedicacion_h = dedicacion_h < 10 ? '0'+dedicacion_h.to_s : dedicacion_h.to_s
          dedicacion_m = dedicacion_m < 10 ? '0'+dedicacion_m.to_i.to_s : dedicacion_m.to_i.to_s

          fila << dedicacion_h+':'+dedicacion_m
          fileStyle << fieldLeft

          if uc.desde
            fila << (localize  uc.desde, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.hasta
            fila << (localize  uc.hasta, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft


          if !uc.iniciado
            fila << 'No iniciado'
          else

            if uc.finalizado
              fila << 'Terminado'
            else
              fila << 'Iniciado'
            end

          end

          fileStyle << fieldLeft

          if course.evaluations.size > 1

            course.evaluations.each do |eval|

              uc_eval = UserCourseEvaluation.where('user_course_id = ? AND evaluation_id = ?', uc.id, eval.id).first

              if uc_eval && uc_eval.nota
                fila << uc_eval.nota
              else
                fila << '-'
              end

              fileStyle << fieldLeft

            end

          end

          if uc.nota
            fila << uc.nota
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.aprobado
            fila << 'aprobado'
          else

            if uc.finalizado

              fila << 'reprobado'

            else

              fila << '-'

            end


          end
          fileStyle << fieldLeft

          fila << uc.porcentaje_avance.to_i.to_s+'%'
          fileStyle << fieldLeft

          if uc.inicio
            fila << (localize  uc.inicio, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if uc.fin
            fila << (localize  uc.fin, format: :day_month_year)
          else
            fila << '-'
          end
          fileStyle << fieldLeft

          if si_hay_sence

            if uc.aplica_sence
              fila << 'Sí'
            else
              fila << 'No'
            end
            fileStyle << fieldLeft

            if uc.aplica_sence
              fila << course.sence_code
            else
              fila << '-'
            end
            fileStyle << fieldLeft

          end

          characteristics.each do |cha|

            user_cha = uc.enrollment.user.characteristic_by_id(cha.id)

            val_cha = '-'
            if user_cha
              val_cha = user_cha.valor
            end

            fila << val_cha
            fileStyle << fieldLeft

          end

          if show_characteristics_priv

            characteristics_priv.each do |cha|

              user_cha = uc.enrollment.user.characteristic_by_id(cha.id)

              val_cha = '-'
              if user_cha
                val_cha = user_cha.valor
              end

              fila << val_cha
              fileStyle << fieldLeft

            end

          end

          reporte_excel.add_row fila, :style => fileStyle, height: 20, :types => [:integer, :string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def reporte_encuesta_satisfaccion_excel(nombre_reporte, program_course, user_courses, company, rango = nil, desde = nil, hasta = nil, desde_human = nil, hasta_human = nil)

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    if program_course.course.dncp && program_course.course.dncp.poll_presencial
      poll_satis = program_course.course.polls.where('tipo_satisfaccion = ?', true).first

      lista_valid_groups = Array.new

      program_course.course.dncp.group_ids.each do |g|
        lista_valid_groups.push g.grupo if desde && hasta && g.desde >= desde && g.desde <= hasta
      end

    end

    letras = letras_excel

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :alignment => { :horizontal => :left, :wrap_text => true, :vertical => :top}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => nombre_reporte) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Curso'
        filaHeader << headerTitleInfo
        fila << program_course.course.nombre
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        #reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        #reporte_excel.merge_cells('B'+fila_actual.to_s+':E'+fila_actual.to_s)

        fila_actual += 1

        if rango

          fila = Array.new
          filaHeader = Array.new

          fila << 'Vigencia'
          filaHeader << headerTitleInfo

          fila << 'Del '+desde_human+' Al '+hasta_human
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          #reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        #reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila

        fila_actual += 1

        poll = program_course.course.polls.where('tipo_satisfaccion = ?', true).first

        if poll && poll.master_poll

          master_questions = poll.master_poll.master_poll_questions

          master_questions.each do |master_question|

            ucpqs = UserCoursePollQuestion.joins(:user_course_poll).where('poll_id = ? AND finalizada = ? AND master_poll_question_id = ? AND user_course_id IN (?)',
                                                                          poll.id, true, master_question.id, user_courses.map { |uc| uc.id  })

            fila = Array.new
            fileStyle = Array.new

            texto_q = master_question.texto.split('<br>')

            texto_q_joined = ''
            alto_fila = 0
            texto_q.each_with_index do |t_q, numero_lineas|

              if numero_lineas == 0

                texto_q_joined = '= "'+ActionView::Base.full_sanitizer.sanitize(t_q)+'" '


              else

                texto_q_joined += '& CHAR(10) & "'+ActionView::Base.full_sanitizer.sanitize(t_q)+'" '

              end
              alto_fila += 20
            end

            fila << texto_q_joined
            fileStyle << fieldLeft

            reporte_excel.add_row fila, :style => fileStyle, height: alto_fila
            reporte_excel.merge_cells('A'+fila_actual.to_s+':C'+fila_actual.to_s)

            fila_actual += 1

            lista_alternativas = master_question.master_poll_alternatives



            lista_alternativas.each_with_index do |alt|

              total_resp_alts = 0

              fila = Array.new
              fileStyle = Array.new

              fila << alt.letra
              fileStyle << fieldLeft

              texto_a = alt.texto.split('<br>')

              texto_a_joined = ''
              alto_fila = 0
              texto_a.each_with_index do |t_a, numero_lineas|

                if numero_lineas == 0

                  texto_a_joined = '= "'+ActionView::Base.full_sanitizer.sanitize(t_a)+'" '

                else

                  texto_a_joined += '& CHAR(10) & "'+ActionView::Base.full_sanitizer.sanitize(t_a)+'" '

                end
                alto_fila += 20
              end

              fila << texto_a_joined
              fileStyle << fieldLeft
=begin
              user_courses.each do |uc|

                uc_poll = uc.user_course_polls.find_by_poll_id(poll.id)

                if uc_poll

                  uc_poll_q = uc_poll.user_course_poll_questions.find_by_master_poll_question_id(master_question.id)

                  if uc_poll_q && uc_poll_q.alternativas

                    uc_poll_q_alts = uc_poll_q.alternativas.split('-')

                    uc_poll_q_alts.each do |uc_poll_q_alt|

                      total_resp_alts += 1 if uc_poll_q_alt == alt.id.to_s

                    end

                  end

                end

              end
=end

              if program_course.course.dncp && program_course.course.dncp.poll_presencial



                poll_summary = ProgramCoursePollSummary.where('program_course_id = ? AND poll_id = ? AND master_poll_question_id = ? AND master_poll_alternative_id = ? AND grupo IN(?) ',
                                                              program_course.id, poll_satis.id, master_question.id, alt.id, lista_valid_groups).first



                total_resp_alts = poll_summary ? poll_summary.numero_respuestas : 0


              else

                ucpqs.each do |uc_poll_q|

                  if uc_poll_q.alternativas

                    uc_poll_q_alts = uc_poll_q.alternativas.split('-')

                    uc_poll_q_alts.each do |uc_poll_q_alt|

                      total_resp_alts += 1 if uc_poll_q_alt == alt.id.to_s

                    end

                  end

                end

              end


              fila << total_resp_alts
              fileStyle << fieldLeft

              reporte_excel.add_row fila, :style => fileStyle, height: alto_fila

              fila_actual += 1

            end

          end


        end



      end

    end

    return reporte

  end

  def reporte_encuesta_satisfaccion_con_nombres_excel(nombre_reporte, program_course, user_courses, company, rango = nil, desde = nil, hasta = nil)

    my_domains, main_background_color, menu_text_color, secundary_background_color = parametros_design_excel

    letras = letras_excel

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style fg_color: menu_text_color[$current_domain],
                                bg_color: main_background_color[$current_domain],
                                :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style bg_color: secundary_background_color[$current_domain],
                               :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style bg_color: secundary_background_color[$current_domain],
                                 :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :alignment => { :horizontal => :left, :wrap_text => true, :vertical => :top}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => nombre_reporte) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new

        fila << 'Curso'
        filaHeader << headerTitleInfo
        fila << program_course.course.nombre
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet
        fila << ''
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        #reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)
        #reporte_excel.merge_cells('B'+fila_actual.to_s+':E'+fila_actual.to_s)

        fila_actual += 1

        if rango

          fila = Array.new
          filaHeader = Array.new

          fila << 'Vigencia'
          filaHeader << headerTitleInfo

          fila << 'Del '+desde+' Al '+hasta
          filaHeader << headerTitleInfoDet

          reporte_excel.add_row fila, :style => filaHeader, height: 20
          #reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

          fila_actual += 1

        end

        fila = Array.new
        filaHeader = Array.new

        fila << 'Fecha de Reporte'
        filaHeader << headerTitleInfo
        fila << (localize  lms_time, format: :day_month_year_time)
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20
        #reporte_excel.merge_cells('A'+fila_actual.to_s+':B'+fila_actual.to_s)

        fila_actual += 1

        fila = Array.new
        fila << ''
        reporte_excel.add_row fila

        fila_actual += 1

        poll = program_course.course.polls.where('tipo_satisfaccion = ?', true).first

        if poll && poll.master_poll

          master_questions = poll.master_poll.master_poll_questions

          master_questions.each do |master_question|

            fila = Array.new
            fileStyle = Array.new

            texto_q = master_question.texto.split('<br>')

            texto_q_joined = ''
            alto_fila = 0
            texto_q.each_with_index do |t_q, numero_lineas|

              if numero_lineas == 0

                texto_q_joined = '= "'+ActionView::Base.full_sanitizer.sanitize(t_q)+'" '


              else

                texto_q_joined += '& CHAR(10) & "'+ActionView::Base.full_sanitizer.sanitize(t_q)+'" '

              end
              alto_fila += 20
            end

            fila << texto_q_joined
            fileStyle << fieldLeft

            reporte_excel.add_row fila, :style => fileStyle, height: alto_fila
            reporte_excel.merge_cells('A'+fila_actual.to_s+':C'+fila_actual.to_s)

            fila_actual += 1

            lista_alternativas = master_question.master_poll_alternatives

            lista_alternativas.each_with_index do |alt|

              total_resp_alts = 0

              fila = Array.new
              fileStyle = Array.new

              fila << alt.letra
              fileStyle << fieldLeft

              texto_a = alt.texto.split('<br>')

              texto_a_joined = ''
              alto_fila = 0
              texto_a.each_with_index do |t_a, numero_lineas|

                if numero_lineas == 0

                  texto_a_joined = '= "'+ActionView::Base.full_sanitizer.sanitize(t_a)+'" '

                else

                  texto_a_joined += '& CHAR(10) & "'+ActionView::Base.full_sanitizer.sanitize(t_a)+'" '

                end
                alto_fila += 20
              end

              fila << texto_a_joined
              fileStyle << fieldLeft

              lista_nombres = ''

              user_courses.each do |uc|

                uc_poll = uc.user_course_polls.find_by_poll_id(poll.id)

                if uc_poll

                  uc_poll_q = uc_poll.user_course_poll_questions.find_by_master_poll_question_id(master_question.id)

                  if uc_poll_q && uc_poll_q.alternativas

                    uc_poll_q_alts = uc_poll_q.alternativas.split('-')

                    uc_poll_q_alts.each do |uc_poll_q_alt|

                      if uc_poll_q_alt == alt.id.to_s
                        total_resp_alts += 1
                        lista_nombres += uc.enrollment.user.apellidos+' '+uc.enrollment.user.nombre+', '
                      end

                    end

                  end

                end

              end

              fila << total_resp_alts
              fileStyle << fieldLeft

              fila << lista_nombres
              fileStyle << fieldLeft

              reporte_excel.add_row fila, :style => fileStyle, height: alto_fila

              fila_actual += 1

            end

          end


        end



      end

    end

    return reporte

  end


end