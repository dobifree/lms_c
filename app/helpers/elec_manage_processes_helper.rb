module ElecManageProcessesHelper

  def authorized_to_manage_elec_processes
    unless ct_module_elec_manager?
      flash[:danger] = t('security.no_admin_privilege')
      redirect_to root_path
    end
  end

  def round_allow_to_toggle_status_winners(round)

    #si ya está finalizada
    if round.done?
      return false
    end

    #si aún no se acaba el plazo para votar (revisar)
=begin
    if round.to_date >= lms_time
      return false
    end
=end

    return true
  end

  def round_allow_to_finish(round)

    #si ya está finalizada
    if round.done?
      return false
    end

    #si aún no se acaba el plazo para votar (revisar)
=begin
    if round.to_date >= lms_time
      return false
    end
=end

    return true
  end

end
