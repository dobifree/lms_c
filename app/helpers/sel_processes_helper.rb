module SelProcessesHelper

  def authorized_to_processes

    unless is_sel_process_manager? || is_sel_process_owner?
      flash[:danger] = t('security.no_admin_privilege')
      redirect_to root_path
    end

  end

  def authorized_to_manage_processes
    unless is_sel_process_manager?
      flash[:danger] = t('security.no_admin_privilege')
      redirect_to root_path
    end
  end

  def is_sel_process_manager?
    if CtModuleManager.where(:ct_module_id => CtModule.find_by_cod('sel'), :user_id => session[:user_connected_id]).count > 0
      true
    else
      false
    end

  end

  def is_sel_process_owner?
    if SelOwner.find_by_user_id(session[:user_connected_id])
      true
    else
      false
    end
  end

  def step_candidate_exonerate_editable?(sel_candidate_step)

    ## Si ya tiene los resultados listos del paso
    if sel_candidate_step.done
      return false
    end

    if SelStepCandidate.joins(:sel_process_step => :sel_step).where(:jm_candidate_id => sel_candidate_step.jm_candidate_id, :done => true, sel_steps: {:step_type => 1}, sel_process_steps: {:sel_process_id => sel_candidate_step.sel_process_step.sel_process_id}).where('sel_steps.position > ?', sel_candidate_step.sel_process_step.sel_step.position).count > 0
      return false
    end

    return true


  end

  def is_candidate_enrollable?(applicant)
    # este método ahora también se utiliza para validar si es 'eliminable'
    SelStepCandidate.joins(:sel_process_step).where(:jm_candidate_id => applicant.jm_candidate_id, sel_process_steps: {:sel_process_id => applicant.sel_process_id}).count == 0
  end

  def next_until_filter_steps(sel_process, last_filter_step_position = nil)
    next_steps_array = []
    initial_position = last_filter_step_position ? last_filter_step_position : -2000000
    final_position = nil
    SelProcessStep.find_all_by_sel_process_id(sel_process.id).each do |sel_process_step|
      sel_step = sel_process_step.sel_step
      if sel_step.position > initial_position && !final_position
        final_position = sel_step.position if sel_step.step_type == 1
        next_steps_array.append(sel_process_step)
      end
    end
    next_steps_array
  end

  def next_step_candidate_until_filter(sel_step_candidate = nil, sel_applicant = nil)
    next_steps_array = []
    initial_position = sel_step_candidate ? sel_step_candidate.sel_process_step.sel_step.position : -2000000
    final_position = nil

    if sel_step_candidate
    sel_step_candidates = SelStepCandidate.joins(:sel_process_step).where(:jm_candidate_id => sel_step_candidate.jm_candidate_id, sel_process_steps: {:sel_process_id => sel_step_candidate.sel_process_step.sel_process_id})
    else
      sel_step_candidates = SelStepCandidate.joins(:sel_process_step).where(:jm_candidate_id => sel_applicant.jm_candidate_id, sel_process_steps: {:sel_process_id => sel_applicant.sel_process_id})
    end

    sel_step_candidates.each do |step_candidate|
      sel_step = step_candidate.sel_process_step.sel_step
      if sel_step.position > initial_position && !final_position
        final_position = sel_step.position if sel_step.step_type == 1
        next_steps_array.append(step_candidate)
      end
    end
    next_steps_array
  end

  def can_pass_filter_step?(sel_step_candidate)
    #si ya fue pasado a los pasos normales siguientes
    if sel_step_candidate.done == true
      return false
    end

    #si aún tiene pasos sin completar en el grupo de pasos normales de este paso de filtro
    if SelStepCandidate.joins(:sel_process_step => :sel_step).where(:done => false, :exonerated => false, :jm_candidate_id => sel_step_candidate.jm_candidate_id, sel_process_steps: {:sel_process_id => sel_step_candidate.sel_process_step.sel_process_id}).where('sel_steps.position < ?', sel_step_candidate.sel_process_step.sel_step.position).count > 0
      return false
    end

    return true

  end

  def can_undo_filter_step(sel_step_candidate)
    # si aun no ha pasado el filtro, no se puede revertir
    if sel_step_candidate.done == false
      return false
    end
    # si ya está seleccionado o contratado
    sel_applicant = SelApplicant.where(:jm_candidate_id => sel_step_candidate.jm_candidate_id, :sel_process_id => sel_step_candidate.sel_process_step.sel_process_id).first
    if sel_applicant.selected? || sel_applicant.hired?
      return false
    end
    # si el siguiente paso de filtro está listo
    sel_filter_step_candidates = SelStepCandidate.joins(:sel_process_step => :sel_step).where(:jm_candidate_id => sel_step_candidate.jm_candidate_id, sel_process_steps: {:sel_process_id => sel_step_candidate.sel_process_step.sel_process_id }, sel_steps:{:step_type => 1})
    sel_filter_step_candidates.each do |step_candidate|
      if step_candidate.sel_process_step.sel_step.position > sel_step_candidate.sel_process_step.sel_step.position && step_candidate.done
        return false
      end
    end

    return true
  end

  def can_undo_filter_step_from_applicant(sel_applicant)
    # si aun no ha pasado el filtro, no se puede revertir
    if SelStepCandidate.joins(:sel_process_step).where(:jm_candidate_id => sel_applicant.jm_candidate_id, sel_process_steps: {:sel_process_id => sel_applicant.sel_process_id}).count == 0
      return false
    end
    # si ya está seleccionado o contratado
    if sel_applicant.selected? || sel_applicant.hired?
      return false
    end
    # si el siguiente paso de filtro está listo
    sel_filter_step_candidates = SelStepCandidate.joins(:sel_process_step => :sel_step).where(:jm_candidate_id => sel_applicant.jm_candidate_id, sel_process_steps: {:sel_process_id => sel_applicant.sel_process_id }, sel_steps:{:step_type => 1})
    sel_filter_step_candidates.each do |step_candidate|
      if step_candidate.done
        return false
      end
    end

    return true
  end

  def pending_steps_results(process_id, user_id)

    SelProcessStep.joins(:sel_process, [:sel_step_candidates => [:sel_attendants, :jm_candidate => :sel_applicants]])
        .where(sel_processes: {:finished => false, :id => process_id},
               sel_step_candidates: {:done => false, :exonerated => false},
               sel_applicants: {:sel_process_id => process_id, :excluded => false},
               sel_attendants: {:user_id => user_id}).
        select('DISTINCT sel_process_steps.*')

  end

  def is_step_candidate_reaperturable?(sel_step_candidate)

    # si no es un paso normal
    if sel_step_candidate.sel_process_step.sel_step.step_type != 0
      return false
    end

    # si no está finalizado o si está exonerado
    if !(sel_step_candidate.done && !sel_step_candidate.exonerated)
      return false
    end

    # si el siguiente paso de filtro está listo
    sel_filter_step_candidates = SelStepCandidate.joins(:sel_process_step => :sel_step).where(:jm_candidate_id => sel_step_candidate.jm_candidate_id, sel_process_steps: {:sel_process_id => sel_step_candidate.sel_process_step.sel_process_id }, sel_steps:{:step_type => 1})
    sel_filter_step_candidates.each do |step_candidate|
      if step_candidate.sel_process_step.sel_step.position > sel_step_candidate.sel_process_step.sel_step.position && step_candidate.done
        return false
      end
    end

    # si ya fue seleccionado o contratado
    sel_applicant = sel_step_candidate.jm_candidate.sel_applicants.where(:sel_process_id => sel_step_candidate.sel_process_step.sel_process_id).first
    if  sel_applicant.selected || sel_applicant.hired
      return false
    end


    return true

  end



  def link_to_remove_appointment(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_appointment(this)")
  end

  def link_to_add_appointment(name, f)
    appointment = SelAppointment.new
    fields = f.fields_for(:sel_appointments, appointment, :child_index => "new_appointment") do |builder|
      render('sel_processes/config_appointments_form', :f => builder)
    end
    link_to_function(name, "add_appointment(this, \"#{escape_javascript(fields)}\")")
  end


  def link_to_remove_process_characteristic(name, f, need_to_confirm)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_process_characteristic(this, " + need_to_confirm.to_s + ")")
  end

  def link_to_add_process_characteristic(name, f)
    sel_process_char = SelProcessCharacteristic.new
    fields = f.fields_for(:sel_process_characteristics, sel_process_char, :child_index => "new_process_characteristic") do |builder|
      render('sel_processes/config_process_characteristic_form', :f => builder)
    end
    link_to_function(name, "add_process_characteristic(this, \"#{escape_javascript(fields)}\")")
  end


  def link_to_remove_process_question(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_process_question(this); return false")
  end

  def link_to_add_process_question(name, f)
    sel_apply_specific_question = SelApplySpecificQuestion.new
    fields = f.fields_for(:sel_apply_specific_questions, sel_apply_specific_question, :child_index => "new_process_question") do |builder|
      render('sel_processes/specific_questions/form', :f => builder)
    end
    link_to(name, '#', onclick: "add_process_question(this, \"#{escape_javascript(fields)}\"); return false")
  end


end
