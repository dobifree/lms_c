module PeFeedbackHelper

  def link_to_add_compound_field(pe_feedback_field, pe_member_feedback, number_of_compound_feedback, feedback_field_lists_array, max_number)

    form = render partial: 'pe_feedback/form_compound_field',
           locals: {
               pe_feedback_field: pe_feedback_field,
               pe_member_feedback: pe_member_feedback,
               number_of_compound_feedback: number_of_compound_feedback,
               feedback_field_lists_array: feedback_field_lists_array
           }


    link_to_function('<i class="fa fa-plus" aria-hidden="true"></i>'.html_safe, "add_compound_field(\"#{escape_javascript(form)}\", #{pe_feedback_field.id}, #{max_number})")
  end

end
