module PeDefinitionByUserValidateHelper

  def xls_for_validation_everybody pe_evaluation, step_number, before_accept

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Validación_'+pe_evaluation.name).slice(0,31)) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Definición realizada por '
        filaHeader <<  headerCenter
        cols_widths << 20

        last_pe_element = nil

        pe_evaluation.pe_elements.each do |pe_element|

          last_pe_element = pe_element

          if pe_element.assessed

            if pe_element.simple?

              fila << pe_element.name
              filaHeader <<  headerCenter
              cols_widths << 20

            else

              pe_element.pe_element_descriptions.each do |pe_element_description|

                fila << pe_element_description.name
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

            fila << 'peso'
            filaHeader <<  headerCenter
            cols_widths << 10

          else

            fila << pe_element.name
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        if last_pe_element && last_pe_element.assessed

          if last_pe_element.assessment_method == 2

            fila << 'tipo de indicador'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'unidad de medida'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'meta'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'validar (si/no)'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'comentario'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        pe_evaluation.pe_definition_by_user_validators_by_validator_ordered_by_user(user_connected, step_number, before_accept).each_with_index do |pe_definition_by_user_validator, index|

          unless pe_definition_by_user_validator.pe_member.removed

            status_validate = pe_evaluation.pe_member_status_to_be_validated(pe_definition_by_user_validator.pe_member, step_number, before_accept)

            if status_validate  == 'ready' || status_validate == 'finished' || status_validate == 'not_validated'

              row_creator = ''
              pe_evaluation.pe_creators_of_pe_member_questions(pe_definition_by_user_validator.pe_member).each do |creator|
                row_creator += creator.apellidos+', '+creator.nombre
              end

              if pe_evaluation.pe_elements.size == 1

                pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_definition_by_user_validator.pe_member).each do |pe_question_1st_level|

                  fila = Array.new
                  fileStyle = Array.new

                  fila << pe_question_1st_level.id
                  fileStyle << fieldLeft

                  fila << pe_definition_by_user_validator.pe_member.user.codigo
                  fileStyle << fieldLeft

                  fila << pe_definition_by_user_validator.pe_member.user.apellidos
                  fileStyle << fieldLeft

                  fila << pe_definition_by_user_validator.pe_member.user.nombre
                  fileStyle << fieldLeft

                  fila << row_creator
                  fileStyle << fieldLeft

                  if pe_question_1st_level.pe_element.kpi_dashboard && pe_question_1st_level.kpi_indicator

                    fila << pe_question_1st_level.kpi_indicator.name
                    fileStyle << fieldLeft

                  else

                    fila << pe_question_1st_level.description
                    fileStyle << fieldLeft

                  end

                  fila << number_with_precision(pe_question_1st_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
                  fileStyle << fieldLeft

                  if pe_question_1st_level.pe_element.kpi_dashboard && pe_question_1st_level.kpi_indicator

                    fila << 'KPI'
                    fileStyle << fieldLeft

                    fila << pe_question_1st_level.kpi_indicator.unit
                    fileStyle << fieldLeft

                    if pe_question_1st_level.kpi_indicator.goal
                      fila << number_with_precision(pe_question_1st_level.kpi_indicator.goal, precision: 6, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                      fileStyle << fieldLeft
                    else
                      fila << ''
                      fileStyle << fieldLeft
                    end

                  else

                    indicator_type = case pe_question_1st_level.indicator_type
                                       when 0
                                         'Indicador directo'
                                       when 1
                                         'Indicador inverso'
                                       when 2
                                         'Indicador por desviación +/-'
                                       when 3
                                         'Indicador por rangos'
                                       when 4
                                         'Indicador discreto'
                                     end

                    fila << indicator_type
                    fileStyle << fieldLeft

                    fila << pe_question_1st_level.indicator_unit
                    fileStyle << fieldLeft

                    if pe_question_1st_level.indicator_type != 4
                      fila << number_with_precision(pe_question_1st_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                      fileStyle << fieldLeft
                    else
                      if pe_question_1st_level.indicator_discrete_goal
                        fila << pe_question_1st_level.indicator_discrete_goal
                        fileStyle << fieldLeft
                      else
                        fila << ''
                        fileStyle << fieldLeft
                      end
                    end

                  end

                  pe_question_validation = pe_question_1st_level.pe_question_validation(step_number, before_accept)

                  if (pe_question_validation && pe_question_validation.validated) || pe_question_validation.nil?

                    fila << 'si'
                    fileStyle << fieldLeft

                    fila << ''
                    fileStyle << fieldLeft

                  elsif pe_question_validation

                    fila << 'no'
                    fileStyle << fieldLeft

                    fila << pe_question_validation.comment
                    fileStyle << fieldLeft

                  else

                    fila << ''
                    fileStyle << fieldLeft

                    fila << ''
                    fileStyle << fieldLeft

                  end

                  reporte_excel.add_row fila, :style => fileStyle, height: 20

                  fila_actual += 1

                end

              elsif pe_evaluation.pe_elements.size == 2

                pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_definition_by_user_validator.pe_member).each do |pe_question_1st_level|

                  pe_evaluation.pe_questions_by_question(pe_question_1st_level).each do |pe_question_2nd_level|

                    #pe_evaluation.pe_questions_by_question(pe_question_2nd_level).each do |pe_question_3rd_level|

                      fila = Array.new
                      fileStyle = Array.new

                      fila << pe_question_2nd_level.id
                      fileStyle << fieldLeft

                      fila << pe_definition_by_user_validator.pe_member.user.codigo
                      fileStyle << fieldLeft

                      fila << pe_definition_by_user_validator.pe_member.user.apellidos
                      fileStyle << fieldLeft

                      fila << pe_definition_by_user_validator.pe_member.user.nombre
                      fileStyle << fieldLeft

                      fila << row_creator
                      fileStyle << fieldLeft

                      fila << pe_question_1st_level.description
                      fileStyle << fieldLeft

                      #fila << pe_question_2nd_level.description
                      #fileStyle << fieldLeft

                      if pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator

                        fila << pe_question_2nd_level.kpi_indicator.name
                        fileStyle << fieldLeft

                      else

                        fila << pe_question_2nd_level.description
                        fileStyle << fieldLeft

                      end

                      fila << number_with_precision(pe_question_2nd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
                      fileStyle << fieldLeft

                      if pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator

                        fila << 'KPI'
                        fileStyle << fieldLeft

                        fila << pe_question_2nd_level.kpi_indicator.unit
                        fileStyle << fieldLeft

                        if pe_question_2nd_level.kpi_indicator.goal
                          fila << number_with_precision(pe_question_2nd_level.kpi_indicator.goal, precision: 6, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                          fileStyle << fieldLeft
                        else
                          fila << ''
                          fileStyle << fieldLeft
                        end

                      else

                        indicator_type = case pe_question_2nd_level.indicator_type
                                           when 0
                                             'Indicador directo'
                                           when 1
                                             'Indicador inverso'
                                           when 2
                                             'Indicador por desviación +/-'
                                           when 3
                                             'Indicador por rangos'
                                           when 4
                                             'Indicador discreto'
                                         end

                        fila << indicator_type
                        fileStyle << fieldLeft

                        fila << pe_question_2nd_level.indicator_unit
                        fileStyle << fieldLeft

                        if pe_question_2nd_level.indicator_type != 4
                          fila << number_with_precision(pe_question_2nd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                          fileStyle << fieldLeft
                        else
                          if pe_question_2nd_level.indicator_discrete_goal
                            fila << pe_question_2nd_level.indicator_discrete_goal
                            fileStyle << fieldLeft
                          else
                            fila << ''
                            fileStyle << fieldLeft
                          end
                        end

                      end

                      pe_question_validation = pe_question_2nd_level.pe_question_validation(step_number, before_accept)

                      if (pe_question_validation && pe_question_validation.validated) || pe_question_validation.nil?

                        fila << 'si'
                        fileStyle << fieldLeft

                        fila << ''
                        fileStyle << fieldLeft

                      elsif pe_question_validation

                        fila << 'no'
                        fileStyle << fieldLeft

                        fila << pe_question_validation.comment
                        fileStyle << fieldLeft

                      else

                        fila << ''
                        fileStyle << fieldLeft

                        fila << ''
                        fileStyle << fieldLeft

                      end



                      reporte_excel.add_row fila, :style => fileStyle, height: 20

                      fila_actual += 1

                    #end

                  end

                end

              end

            end

          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end


  def xls_for_validation_pending pe_evaluation, step_number, before_accept

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Validación_'+pe_evaluation.name).slice(0,31)) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << '#'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Definición realizada por '
        filaHeader <<  headerCenter
        cols_widths << 20

        last_pe_element = nil

        pe_evaluation.pe_elements.each do |pe_element|

          last_pe_element = pe_element

          if pe_element.assessed

            if pe_element.simple?

              fila << pe_element.name
              filaHeader <<  headerCenter
              cols_widths << 20

            else

              pe_element.pe_element_descriptions.each do |pe_element_description|

                fila << pe_element_description.name
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

            fila << 'peso'
            filaHeader <<  headerCenter
            cols_widths << 10

          else

            fila << pe_element.name
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        if last_pe_element && last_pe_element.assessed

          if last_pe_element.assessment_method == 2

            fila << 'tipo de indicador'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'unidad de medida'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'meta'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'validar (si/no)'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'comentario'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        pe_evaluation.pe_definition_by_user_validators_by_validator_ordered_by_user(user_connected, step_number, before_accept).each_with_index do |pe_definition_by_user_validator, index|

          unless pe_definition_by_user_validator.pe_member.removed

            status_validate = pe_evaluation.pe_member_status_to_be_validated(pe_definition_by_user_validator.pe_member, step_number, before_accept)

            if status_validate == 'ready'

              row_creator = ''
              pe_evaluation.pe_creators_of_pe_member_questions(pe_definition_by_user_validator.pe_member).each do |creator|
                row_creator += creator.apellidos+', '+creator.nombre
              end

              if pe_evaluation.pe_elements.size == 1

                pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_definition_by_user_validator.pe_member).each do |pe_question_1st_level|

                  fila = Array.new
                  fileStyle = Array.new

                  fila << pe_question_1st_level.id
                  fileStyle << fieldLeft

                  fila << pe_definition_by_user_validator.pe_member.user.codigo
                  fileStyle << fieldLeft

                  fila << pe_definition_by_user_validator.pe_member.user.apellidos
                  fileStyle << fieldLeft

                  fila << pe_definition_by_user_validator.pe_member.user.nombre
                  fileStyle << fieldLeft

                  fila << row_creator
                  fileStyle << fieldLeft

                  if pe_question_1st_level.pe_element.kpi_dashboard && pe_question_1st_level.kpi_indicator

                    fila << pe_question_1st_level.kpi_indicator.name
                    fileStyle << fieldLeft

                  else

                    fila << pe_question_1st_level.description
                    fileStyle << fieldLeft

                  end

                  fila << number_with_precision(pe_question_1st_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
                  fileStyle << fieldLeft

                  if pe_question_1st_level.pe_element.kpi_dashboard && pe_question_1st_level.kpi_indicator

                    fila << 'KPI'
                    fileStyle << fieldLeft

                    fila << pe_question_1st_level.kpi_indicator.unit
                    fileStyle << fieldLeft

                    if pe_question_1st_level.kpi_indicator.goal
                      fila << number_with_precision(pe_question_1st_level.kpi_indicator.goal, precision: 6, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                      fileStyle << fieldLeft
                    else
                      fila << ''
                      fileStyle << fieldLeft
                    end

                  else

                    indicator_type = case pe_question_1st_level.indicator_type
                                       when 0
                                         'Indicador directo'
                                       when 1
                                         'Indicador inverso'
                                       when 2
                                         'Indicador por desviación +/-'
                                       when 3
                                         'Indicador por rangos'
                                       when 4
                                         'Indicador discreto'
                                     end

                    fila << indicator_type
                    fileStyle << fieldLeft

                    fila << pe_question_1st_level.indicator_unit
                    fileStyle << fieldLeft

                    if pe_question_1st_level.indicator_type != 4
                      fila << number_with_precision(pe_question_1st_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                      fileStyle << fieldLeft
                    else
                      if pe_question_1st_level.indicator_discrete_goal
                        fila << pe_question_1st_level.indicator_discrete_goal
                        fileStyle << fieldLeft
                      else
                        fila << ''
                        fileStyle << fieldLeft
                      end
                    end

                  end

                  pe_question_validation = pe_question_1st_level.pe_question_validation(step_number, before_accept)

                  if (pe_question_validation && pe_question_validation.validated) || pe_question_validation.nil?

                    fila << 'si'
                    fileStyle << fieldLeft

                    fila << ''
                    fileStyle << fieldLeft

                  elsif pe_question_validation

                    fila << 'no'
                    fileStyle << fieldLeft

                    fila << pe_question_validation.comment
                    fileStyle << fieldLeft

                  else

                    fila << ''
                    fileStyle << fieldLeft

                    fila << ''
                    fileStyle << fieldLeft

                  end

                  reporte_excel.add_row fila, :style => fileStyle, height: 20

                  fila_actual += 1

                end


              elsif pe_evaluation.pe_elements.size == 2

                pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_definition_by_user_validator.pe_member).each do |pe_question_1st_level|

                  pe_evaluation.pe_questions_by_question(pe_question_1st_level).each do |pe_question_2nd_level|

                    #pe_evaluation.pe_questions_by_question(pe_question_2nd_level).each do |pe_question_3rd_level|

                      fila = Array.new
                      fileStyle = Array.new

                      fila << pe_question_2nd_level.id
                      fileStyle << fieldLeft

                      fila << pe_definition_by_user_validator.pe_member.user.codigo
                      fileStyle << fieldLeft

                      fila << pe_definition_by_user_validator.pe_member.user.apellidos
                      fileStyle << fieldLeft

                      fila << pe_definition_by_user_validator.pe_member.user.nombre
                      fileStyle << fieldLeft

                      fila << row_creator
                      fileStyle << fieldLeft

                      fila << pe_question_1st_level.description
                      fileStyle << fieldLeft

                      #fila << pe_question_2nd_level.description
                      #fileStyle << fieldLeft

                      if pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator

                        fila << pe_question_2nd_level.kpi_indicator.name
                        fileStyle << fieldLeft

                      else

                        fila << pe_question_2nd_level.description
                        fileStyle << fieldLeft

                      end

                      fila << number_with_precision(pe_question_2nd_level.weight, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: false)
                      fileStyle << fieldLeft

                      if pe_question_2nd_level.pe_element.kpi_dashboard && pe_question_2nd_level.kpi_indicator

                        fila << 'KPI'
                        fileStyle << fieldLeft

                        fila << pe_question_2nd_level.kpi_indicator.unit
                        fileStyle << fieldLeft

                        if pe_question_2nd_level.kpi_indicator.goal
                          fila << number_with_precision(pe_question_2nd_level.kpi_indicator.goal, precision: 6, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                          fileStyle << fieldLeft
                        else
                          fila << ''
                          fileStyle << fieldLeft
                        end

                      else

                        indicator_type = case pe_question_2nd_level.indicator_type
                                           when 0
                                             'Indicador directo'
                                           when 1
                                             'Indicador inverso'
                                           when 2
                                             'Indicador por desviación +/-'
                                           when 3
                                             'Indicador por rangos'
                                           when 4
                                             'Indicador discreto'
                                         end

                        fila << indicator_type
                        fileStyle << fieldLeft

                        fila << pe_question_2nd_level.indicator_unit
                        fileStyle << fieldLeft

                        if pe_question_2nd_level.indicator_type != 4
                          fila << number_with_precision(pe_question_2nd_level.indicator_goal, precision: 2, separator: user_connected_locale_separator, delimiter: user_connected_locale_delimiter, strip_insignificant_zeros: true)
                          fileStyle << fieldLeft
                        else
                          if pe_question_2nd_level.indicator_discrete_goal
                            fila << pe_question_2nd_level.indicator_discrete_goal
                            fileStyle << fieldLeft
                          else
                            fila << ''
                            fileStyle << fieldLeft
                          end
                        end

                      end

                      pe_question_validation = pe_question_2nd_level.pe_question_validation(step_number, before_accept)

                      if (pe_question_validation && pe_question_validation.validated) || pe_question_validation.nil?

                        fila << 'si'
                        fileStyle << fieldLeft

                        fila << ''
                        fileStyle << fieldLeft

                      elsif pe_question_validation

                        fila << 'no'
                        fileStyle << fieldLeft

                        fila << pe_question_validation.comment
                        fileStyle << fieldLeft

                      else

                        fila << ''
                        fileStyle << fieldLeft

                        fila << ''
                        fileStyle << fieldLeft

                      end



                      reporte_excel.add_row fila, :style => fileStyle, height: 20

                      fila_actual += 1

                    #end

                  end

                end

              end

            end

          end

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end


  def save_validations(archivo_temporal, pe_evaluation, step_number, before_accept)

    if before_accept

      val_def_by_manager = pe_evaluation.def_by_users_validated_before_accepted_manager

    else

      val_def_by_manager = pe_evaluation.def_by_users_validated_after_accepted_manager

    end

    pe_members_ready_to_validate = Array.new

    pe_evaluation.pe_definition_by_user_validators_by_validator_ordered_by_user(user_connected, step_number, before_accept).each_with_index do |pe_definition_by_user_validator, index|

      status_validate = pe_evaluation.pe_member_status_to_be_validated(pe_definition_by_user_validator.pe_member, step_number, before_accept)

      if status_validate == 'ready'

        pe_members_ready_to_validate.push pe_definition_by_user_validator.pe_member.user.codigo

        if pe_evaluation.pe_elements.size == 1

        elsif pe_evaluation.pe_elements.size == 2

          pe_evaluation.pe_questions_only_by_pe_member_1st_level(pe_definition_by_user_validator.pe_member).each do |pe_question_1st_level|

            pe_question_1st_level.pe_question_validations.where('step_number = ? AND before_accept = ?', step_number, before_accept).destroy_all

            pqv = pe_question_1st_level.pe_question_validations.build
            pqv.step_number = step_number
            pqv.before_accept = before_accept
            pqv.validated = true
            pqv.pe_evaluation = pe_evaluation
            pqv.save

          end

        end

      end

    end

    pos_answer = if pe_evaluation.pe_elements.size == 1
                  10
                 elsif pe_evaluation.pe_elements.size == 2
                  11
                 end


    rejected_users = Array.new
    accepted_users = Array.new

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        accepted_users.push fila[1]

        pe_member_temp = pe_evaluation.pe_process.pe_member_by_user_code fila[1]

        if pe_member_temp

          pe_question = pe_evaluation.pe_questions.where('pe_questions.id = ? AND pe_member_evaluated_id = ?', fila[0], pe_member_temp.id).first

          if pe_question

            pe_question.pe_question_validations.where('step_number = ? AND before_accept = ?', step_number, before_accept).destroy_all

            if fila[pos_answer] == 'no' && ((val_def_by_manager && pe_question.entered_by_manager) || !pe_question.entered_by_manager)

              pqv = pe_question.pe_question_validations.build
              pqv.step_number = step_number
              pqv.before_accept = before_accept
              pqv.validated = false
              pqv.comment = fila[pos_answer+1]
              pqv.pe_evaluation = pe_evaluation
              if pqv.save

                unless pqv.validated

                  rejected_users.push fila[1]

                  pe_question.confirmed = false
                  pe_question.accepted = false
                  pe_question.ready = false
                  pe_question.save

                  pe_question.pe_question_validations.each do |pe_question_validation|

                    pe_question_validation.validated = false
                    pe_question_validation.attended = false
                    pe_question_validation.save

                  end

                end



              end

            elsif fila[pos_answer] == 'si' || (!val_def_by_manager && pe_question.entered_by_manager)

              pqv = pe_question.pe_question_validations.build
              pqv.step_number = step_number
              pqv.before_accept = before_accept
              pqv.validated = true
              pqv.pe_evaluation = pe_evaluation
              pqv.save

              pe_question.confirmed = true
              pe_question.save

            end

          end

        else

          accepted_users.pop

        end

      end

    end

    rejected_users.uniq!
    accepted_users.uniq!

    accepted_users = accepted_users - rejected_users

    pe_process_notification_def_val = @pe_process.pe_process_notification_def_val(@before_accept, @step_number)

    menu_name = 'Evaluación de desempeño'

    ct_menus_cod.each_with_index do |ct_menu_cod, index_m|

      if ct_menu_cod == 'pe'
        menu_name = ct_menus_user_menu_alias[index_m]
        break
      end
    end


    accepted_users.each do |accepted_user|

      pe_member_evaluated = pe_evaluation.pe_process.pe_member_by_user_code accepted_user

      creators = pe_evaluation.pe_creators_of_pe_member_questions(pe_member_evaluated)

      pe_member_evaluator_boss = pe_member_evaluated.pe_member_evaluator_boss

      creators.push(pe_member_evaluator_boss.user) if pe_member_evaluator_boss

      creators.uniq!

      if pe_process_notification_def_val

        if pe_process_notification_def_val.send_email_when_accept

          begin
            PeMailer.step_def_by_users_validate_definition_accept(pe_evaluation.pe_process, @company, menu_name, pe_evaluation, pe_member_evaluated.user, creators, pe_process_notification_def_val, user_connected).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'pe'
            lm.step = 'def_by_user:def_by_user:accept'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

      end

      if @before_accept && @step_number == pe_evaluation.num_steps_definition_by_users_validated_before_accepted && pe_evaluation.send_email_to_accept_definition

        creators.each do |creator|

          begin
            PeMailer.step_def_by_users_accept_definition(pe_evaluation.pe_process, @company, menu_name, pe_evaluation, pe_member_evaluated.user, creator, user_connected).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'pe'
            lm.step = 'def_by_user:def_by_user:accept:ask_to_confirm'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

      end

    end

    rejected_users.each do |rejected_user|

      pe_member_evaluated = pe_evaluation.pe_process.pe_member_by_user_code rejected_user

      pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).each do |pe_question|
        pe_question.confirmed = false
        pe_question.save
      end

      if pe_process_notification_def_val

        if pe_process_notification_def_val.send_email_when_reject

          creators = pe_evaluation.pe_creators_of_pe_member_questions(pe_member_evaluated)

          pe_member_evaluator_boss = pe_member_evaluated.pe_member_evaluator_boss

          creators.push(pe_member_evaluator_boss.user) if pe_member_evaluator_boss

          creators.uniq!

          begin
            PeMailer.step_def_by_users_validate_definition_reject(pe_evaluation.pe_process, @company, menu_name, pe_evaluation, pe_member_evaluated.user, creators, pe_process_notification_def_val, user_connected).deliver
          rescue Exception => e
            lm = LogMailer.new
            lm.module = 'pe'
            lm.step = 'def_by_user:def_by_user:reject'
            lm.description = e.message+' '+e.backtrace.inspect
            lm.registered_at = lms_time
            lm.save
          end

        end

      end

    end

  end



end
