module BenCellphoneNumbersHelper

  def its_safe( temp_file )
    should_save = true

    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      # puts 'ver excepción: '
      # puts e.inspect
      data = []
      should_save = false
      @error_index << '-'
      @error_content << ['-', '-', '-', '-']
      @error_message << t('views.ben_cellphone_number.error.format_error')
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      # puts 'ver excepción: '
      # puts e.inspect
      data = []
      should_save = false
      @error_index << '-'
      @error_content << ['-', '-', '-', '-']
      @error_message << t('views.ben_cellphone_number.error.formula_error')
    end
    benefits = []

    data.each_with_index do |row, index|
      if index > 0
        user_cod = row[0].to_s
        user_id = User.where(codigo: user_cod).first ? User.where(:codigo => user_cod).first.id : '-1'
        number = row[3].to_s
        subsidy = row[4]

        active = row[5].to_s
        nemo_plan = row[6]

        if index == 1
          active = active.to_s
          unless active == '1' || active == '0'
            @error_index << '-'
            @error_content << ['-', '-', '-', '-']
            @error_message << t('views.ben_cellphone_number.error.formula_error')
            should_save = false
            break
          end
        end

        break unless row[0] or row[3] or row[4] or row[5]

        ben_cellphone_number = BenCellphoneNumber.where(:number =>  number).first_or_initialize
        ben_cellphone_number.user_id = user_id
        ben_cellphone_number.number = number
        ben_cellphone_number.subsidy = subsidy
        ben_cellphone_number.active = active
        ben_cellphone_number.nemo_plan = nemo_plan

        unless active == '1' || active == '0'
          @error_index << index+1
          @error_content << [user_cod, number, subsidy, active]
          @error_message << 'Activo debe ser 0 o 1'
          should_save = false
        end

        unless ben_cellphone_number.valid?
          ben_cellphone_number.errors.full_messages.each do |error|
            @error_index << index + 1
            @error_content << [user_cod, number, subsidy, active]
            @error_message << error
          end
          should_save = false
        else
          benefits << ben_cellphone_number
        end

        data.each_with_index do |aux_row, aux_index|
          break unless aux_row[0] or aux_row[3] or aux_row[4] or aux_row[5]
          aux_number = aux_row[3].to_s
          if aux_number and aux_number == number and index != aux_index
            @error_index << index + 1
            @error_content << [user_cod, number, subsidy, active]
            @error_message << t('views.ben_cellphone_number.error.duplicated_number')+(aux_index + 1).to_s
            should_save = false
          end
        end

      end
    end

    if should_save
      benefits.each do |benefit|
        benefit.save
        reprocess_number benefit.id
      end
    end

    return should_save
  end

  def updated_number(ben_number_id)
    ben_number = BenCellphoneNumber.find(ben_number_id)
    user_processes = ben_number.ben_user_cellphone_processes
    user_processes.each do |user_process|
      user_process.destroy unless user_process.ben_cellphone_process.charged_to_payroll
    end

    if ben_number.active?
      BenCellphoneProcess.where(:charged_to_payroll => false).each do |ben_process|
        total_bill = 0
        equipment_bill = 0
        plan_bill = 0
        user_process = BenUserCellphoneProcess.new(:user_id => ben_number.user.id,
                                                   :ben_cellphone_number_id => ben_number.id,
                                                   :ben_cellphone_process_id => ben_process.id,
                                                   :configured_subsidy => ben_number.subsidy)

        BenCellphoneBillItem.where(:cellphone_number => ben_number.number, :ben_cellphone_process_id => ben_process.id).each do |bill_item|
          unless bill_item.ben_user_cellphone_process
            unless bill_item.not_charge_to_payroll
              unless bill_item.ben_cellphone_number
                bill_item.ben_cellphone_number_id = ben_number_id
              end
              if bill_item.ben_cellphone_number.active? and bill_item.ben_cellphone_number.user.activo
                bill_item.ben_user_cellphone_process = user_process
                bill_item.save
                total_bill += bill_item.net_with_taxes
                equipment_bill += bill_item.net_with_taxes if bill_item.bill_type == 1
                plan_bill += bill_item.net_with_taxes if bill_item.bill_type == 0
              end
            end

          end
        end

        user_process.equipment_bill = equipment_bill
        user_process.plan_bill = plan_bill
        user_process.total_bill = total_bill
        user_process.applied_subsidy = ben_number.subsidy > user_process.total_bill ? user_process.total_bill : ben_number.subsidy
        user_process.total_charged = total_bill - user_process.applied_subsidy
        user_process.save if user_process.ben_cellphone_bill_items.size > 0
      end
    end
  end
end
