module BpAdminConditionVacationsHelper
  def verify_to_delete_condition_vacation(condition_vacation_id)
    condition_vacation = BpConditionVacation.find(condition_vacation_id)
    found = VacRuleRecord.where(bp_condition_vacation_id: condition_vacation.id).all
    found.size == 0
  end
end
