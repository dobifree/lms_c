module TrackingFormGroupsHelper

  def link_to_remove_form_question(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_form_question(this); return false")
  end

  def link_to_add_form_question(name, f)
    question = TrackingFormQuestion.new
    fields = f.fields_for(:tracking_form_questions, question, :child_index => "new_form_question") do |builder|
      render('tracking_form_groups/question_form', :f => builder)
    end
    link_to(name, '#', onclick: "add_form_question(this, \"#{escape_javascript(fields)}\"); return false")
  end

  def link_to_remove_form_item(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_form_item(this); return false")
  end

  def link_to_add_form_item(name, f)
    item = TrackingFormItem.new
    fields = f.fields_for(:tracking_form_items, item, :child_index => "new_form_item") do |builder|
      render('tracking_form_groups/item_form', :f => builder)
    end
    link_to(name, '#', onclick: "add_form_item(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
