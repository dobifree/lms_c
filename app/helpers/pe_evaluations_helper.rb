module PeEvaluationsHelper

  def xls_for_questions_update_groups(pe_evaluation)

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_'+pe_evaluation.name).slice(0,31)) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        last_pe_element = nil

        pe_evaluation.pe_elements.each do |pe_element|

          last_pe_element = pe_element

          if pe_element.assessed

            if pe_element.simple?

              fila << pe_element.name
              filaHeader <<  headerCenter
              cols_widths << 20

            else

              pe_element.pe_element_descriptions.each do |pe_element_description|

                fila << pe_element_description.name
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

            fila << 'peso'
            filaHeader <<  headerCenter
            cols_widths << 10

            fila << 'tiene comentario (0000)'
            filaHeader <<  headerCenter
            cols_widths << 20

          else

            fila << pe_element.name
            filaHeader <<  headerCenter
            cols_widths << 20

          end

          if pe_element.allow_note

            fila << 'nota'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        if last_pe_element && last_pe_element.assessed

          if last_pe_element.assessment_method == 1

            fila << 'alternativa 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'valor alt. 1'
            filaHeader <<  headerCenter
            cols_widths << 10

            fila << 'alternativa 2'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'valor alt. 2'
            filaHeader <<  headerCenter
            cols_widths << 10

          elsif last_pe_element.assessment_method == 2

            fila << 'tipo de indicador (directo/inverso/desviación/rangos/discreto/especial)'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'unidad de medida'
            filaHeader <<  headerCenter
            cols_widths << 20

            if last_pe_element.available_indicator_types.include? '5'

              fila << 'potencia'
              filaHeader <<  headerCenter
              cols_widths << 20

              fila << 'apalancamiento'
              filaHeader <<  headerCenter
              cols_widths << 20

            end

            fila << 'meta ind. 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << '% logro ind. 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'meta ind. 2'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << '% logro ind. 2'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def xls_for_questions_update_pe_rels(pe_evaluation)

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_'+pe_evaluation.name).slice(0,31)) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        last_pe_element = nil

        pe_evaluation.pe_elements.each do |pe_element|

          last_pe_element = pe_element

          if pe_element.assessed

            if pe_element.simple?

              fila << pe_element.name
              filaHeader <<  headerCenter
              cols_widths << 20

            else

              pe_element.pe_element_descriptions.each do |pe_element_description|

                fila << pe_element_description.name
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

            fila << 'peso'
            filaHeader <<  headerCenter
            cols_widths << 10

            fila << 'tiene comentario (0000)'
            filaHeader <<  headerCenter
            cols_widths << 20

          else

            fila << pe_element.name
            filaHeader <<  headerCenter
            cols_widths << 20

          end

          if pe_element.allow_note

            fila << 'nota'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        if last_pe_element && last_pe_element.assessed

          if last_pe_element.assessment_method == 1

            fila << 'alternativa 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'valor alt. 1'
            filaHeader <<  headerCenter
            cols_widths << 10

            fila << 'alternativa 2'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'valor alt. 2'
            filaHeader <<  headerCenter
            cols_widths << 10

          elsif last_pe_element.assessment_method == 2

            fila << 'tipo de indicador (directo/inverso/desviación/rangos/discreto/especial)'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'unidad de medida'
            filaHeader <<  headerCenter
            cols_widths << 20

            if last_pe_element.available_indicator_types.include? '5'

              fila << 'potencia'
              filaHeader <<  headerCenter
              cols_widths << 20

              fila << 'apalancamiento'
              filaHeader <<  headerCenter
              cols_widths << 20

            end

            fila << 'meta ind. 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << '% logro ind. 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'meta ind. 2'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << '% logro ind. 2'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def xls_for_questions_update_pe_members(pe_evaluation)

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_'+pe_evaluation.name).slice(0,31)) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        if pe_evaluation.entered_by_manager_questions

          fila << 'Registro por gestor (0/1)'
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        fila << 'Evaluado'
        filaHeader <<  headerCenter
        cols_widths << 20

        unless pe_evaluation.pe_process.of_persons?

          fila << 'Área Evaluada'
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        fila << 'Evaluador'
        filaHeader <<  headerCenter
        cols_widths << 20

        unless pe_evaluation.pe_process.of_persons?

          fila << 'Área Evaluadora'
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        last_pe_element = nil

        pe_evaluation.pe_elements.each do |pe_element|

          last_pe_element = pe_element

          if pe_element.assessed

            if pe_element.simple?

              fila << pe_element.name
              filaHeader <<  headerCenter
              cols_widths << 20

            else

              pe_element.pe_element_descriptions.each do |pe_element_description|

                fila << pe_element_description.name
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

            fila << 'peso'
            filaHeader <<  headerCenter
            cols_widths << 10

            fila << 'tiene comentario (0000)'
            filaHeader <<  headerCenter
            cols_widths << 20

          else

            fila << pe_element.name
            filaHeader <<  headerCenter
            cols_widths << 20

          end

          if pe_element.allow_note

            fila << 'nota'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        if last_pe_element && last_pe_element.assessed

          if last_pe_element.assessment_method == 1

            fila << 'alternativa 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'valor alt. 1'
            filaHeader <<  headerCenter
            cols_widths << 10

            fila << 'alternativa 2'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'valor alt. 2'
            filaHeader <<  headerCenter
            cols_widths << 10

          elsif last_pe_element.assessment_method == 2

            fila << 'tipo de indicador (directo/inverso/desviación/rangos/discreto/especial)'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'unidad de medida'
            filaHeader <<  headerCenter
            cols_widths << 20

            if last_pe_element.available_indicator_types.include? '5'

              fila << 'potencia'
              filaHeader <<  headerCenter
              cols_widths << 20

              fila << 'apalancamiento'
              filaHeader <<  headerCenter
              cols_widths << 20

            end

            fila << 'meta ind. 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << '% logro ind. 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'meta ind. 2'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << '% logro ind. 2'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def xls_for_questions_update_pe_member(pe_evaluation)

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_'+pe_evaluation.name).slice(0,31)) do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        if pe_evaluation.entered_by_manager_questions

          fila << 'Registro por gestor (0/1)'
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        fila << 'Evaluado'
        filaHeader <<  headerCenter
        cols_widths << 20

        unless pe_evaluation.pe_process.of_persons?

          fila << 'Área Evaluada'
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        last_pe_element = nil

        pe_evaluation.pe_elements.each do |pe_element|

          last_pe_element = pe_element

          if pe_element.assessed

            if pe_element.simple?

              fila << pe_element.name
              filaHeader <<  headerCenter
              cols_widths << 20

            else

              pe_element.pe_element_descriptions.each do |pe_element_description|

                fila << pe_element_description.name
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

            fila << 'peso'
            filaHeader <<  headerCenter
            cols_widths << 10

            fila << 'tiene comentario (0000)'
            filaHeader <<  headerCenter
            cols_widths << 20

          else

            fila << pe_element.name
            filaHeader <<  headerCenter
            cols_widths << 20

          end

          if pe_element.allow_note

            fila << 'nota'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        if last_pe_element && last_pe_element.assessed

          if last_pe_element.assessment_method == 1

            fila << 'alternativa 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'valor alt. 1'
            filaHeader <<  headerCenter
            cols_widths << 10

            fila << 'alternativa 2'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'valor alt. 2'
            filaHeader <<  headerCenter
            cols_widths << 10

          elsif last_pe_element.assessment_method == 2

            fila << 'tipo de indicador (directo/inverso/desviación/rangos/discreto/especial)'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'unidad de medida'
            filaHeader <<  headerCenter
            cols_widths << 20

            if last_pe_element.available_indicator_types.include? '5'

              fila << 'potencia'
              filaHeader <<  headerCenter
              cols_widths << 20

              fila << 'apalancamiento'
              filaHeader <<  headerCenter
              cols_widths << 20

            end

            fila << 'meta ind. 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << '% logro ind. 1'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << 'meta ind. 2'
            filaHeader <<  headerCenter
            cols_widths << 20

            fila << '% logro ind. 2'
            filaHeader <<  headerCenter
            cols_widths << 20

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def check_data(archivo_temporal, hr_evaluation_type, hr_process_level_id)


    @lineas_error = []
    @lineas_error_detalle = []
    @lineas_error_messages = []
    num_linea = 1

    archivo = RubyXL::Parser.parse archivo_temporal

    if archivo.worksheets[0].sheet_name == 'preguntas'

      hr_process_level_id = nil if hr_process_level_id == ''

      data = archivo.worksheets[0].extract_data

      n_elementos = hr_evaluation_type.hr_evaluation_type_elements.length

      data.each_with_index do |fila, index|

        if index > 0 && fila[0]

          #0 texto
          #1 peso
          #2 comentario
          #3 discreto_para_reportes

          #4 valor 1
          #5 texto 1
          #6 valor 2
          #7 texto 2
          #8 valor 3
          #9 texto 3

          #@lineas_error.push index+1

          hr_process_evaluation_q = nil

          hr_process_evaluation_q_padre = nil

          (0..n_elementos-1).each do |n_elemento|


            pos_texto = 0 + 4*n_elemento
            pos_peso = 1 + 4*n_elemento
            pos_comentario = 2 + 4*n_elemento
            pos_discreto = 3 + 4*n_elemento

            hr_process_evaluation_q = @hr_process_evaluation.question(hr_process_evaluation_q_padre.nil? ? nil : hr_process_evaluation_q_padre.id ,hr_process_level_id, CGI.unescapeHTML(fila[pos_texto].to_s))


            unless hr_process_evaluation_q

              hr_process_evaluation_q = HrProcessEvaluationQ.new

              hr_process_evaluation_q.texto = CGI.unescapeHTML fila[pos_texto].to_s
              hr_process_evaluation_q.peso = fila[pos_peso]
              hr_process_evaluation_q.comentario = fila[pos_comentario] =='no' ? false : true
              hr_process_evaluation_q.discrete_display_in_reports = fila[pos_discreto] =='discreto' ? true : false
              hr_process_evaluation_q.hr_process_evaluation = @hr_process_evaluation
              hr_process_evaluation_q.hr_evaluation_type_element = hr_evaluation_type.hr_evaluation_type_elements.find_by_nivel(n_elemento+1)
              hr_process_evaluation_q.hr_process_level_id = hr_process_level_id
              hr_process_evaluation_q.hr_process_evaluation_q = hr_process_evaluation_q_padre


              unless hr_process_evaluation_q.valid?

                hr_process_evaluation_q.save

                @lineas_error.push index+1
                @lineas_error_messages.push hr_process_evaluation_q.errors.full_messages

              end


            end

            hr_process_evaluation_q_padre = hr_process_evaluation_q



          end

          pos_valores = 4*(n_elementos)

          check_valor = true

          hr_process_evaluation_a = nil

          while fila[pos_valores]
            #puts fila[pos_valores]

            if check_valor

              hr_process_evaluation_a = HrProcessEvaluationA.new
              hr_process_evaluation_a.valor = fila[pos_valores]

              check_valor = false

            else

              hr_process_evaluation_a.texto = CGI.unescapeHTML fila[pos_valores].to_s
              check_valor = true

              unless hr_process_evaluation_a.valid?

                hr_process_evaluation_a.save

                @lineas_error.push index+1
                @lineas_error_messages.push hr_process_evaluation_a.errors.full_messages

              end



            end

            pos_valores += 1

          end

        end

      end

    else

      @lineas_error.push 'hoja'
      @lineas_error_messages.push ['Nombre de hoja incorrecto']


    end



  end

  def save_questions(archivo_temporal, pe_evaluation, reemplazar_todo, reemplazar_todo_excel, reemplazar_solo_entered_by_manager, marcar_ready, marcar_confirmadas, marcar_aceptadas, groups_or_members, pe_members_or_pe_member, pe_group_ids, pe_rel_ids)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    if reemplazar_todo == '1'

      if reemplazar_solo_entered_by_manager == '1'
        pe_evaluation.pe_questions.where('entered_by_manager = ?', 1).reorder('pe_questions.id DESC').each do |pe_question|

          if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

            pe_question.destroy

          end

        end
      else
        pe_evaluation.pe_questions.reorder('pe_questions.id DESC').each do |pe_question|

          if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

            pe_question.destroy

          end

        end
      end



    elsif reemplazar_todo_excel == '1'

      if groups_or_members == '1'

        pe_group_ids.each do |pe_group_id|
          if reemplazar_solo_entered_by_manager == '1'
            pe_evaluation.pe_questions.where('pe_group_id = ? AND entered_by_manager = ?', pe_group_id, 1).reorder('pe_questions.id DESC').each do |pe_question|

              if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                pe_question.destroy

              end

            end
          else
            pe_evaluation.pe_questions.where('pe_group_id = ?', pe_group_id).reorder('pe_questions.id DESC').each do |pe_question|

              if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                pe_question.destroy

              end

            end
          end
        end

        pe_rel_ids.each do |pe_rel_id|
          if reemplazar_solo_entered_by_manager == '1'
            pe_evaluation.pe_questions.where('pe_rel_id = ? AND entered_by_manager = ?', pe_rel_id, 1).reorder('pe_questions.id DESC').each do |pe_question|

              if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                pe_question.destroy

              end

            end
          else
            pe_evaluation.pe_questions.where('pe_rel_id = ?', pe_rel_id).reorder('pe_questions.id DESC').each do |pe_question|

              if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                pe_question.destroy

              end

            end
          end

        end

      else

        inicio_pos = pe_evaluation.entered_by_manager_questions ? 1 : 0

        data.each_with_index do |fila, index|

          if index > 0 && fila[0]

            if pe_members_or_pe_member == '1'

              if pe_evaluation.pe_process.of_persons?

                user_code_evaluated = fila[inicio_pos].to_s
                user_code_evaluator = fila[inicio_pos+1].to_s

                user_evaluator = User.find_by_codigo(user_code_evaluator)
                user_evaluated = User.find_by_codigo(user_code_evaluated)

                if user_evaluator && user_evaluated

                  pe_member_evaluator = pe_evaluation.pe_process.evaluator_members.where('user_id = ?', user_evaluator.id).first

                  pe_member_evaluated = pe_evaluation.pe_process.evaluated_members.where('user_id = ?', user_evaluated.id).first

                  if pe_member_evaluator && pe_member_evaluated

                    if reemplazar_solo_entered_by_manager == '1'

                      pe_evaluation.pe_questions.where('pe_member_evaluator_id = ? AND pe_member_evaluated_id = ? AND entered_by_manager = ?', pe_member_evaluator.id, pe_member_evaluated.id, 1).reorder('pe_questions.id DESC').each do |pe_question|

                        if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                          pe_question.destroy

                        end

                      end

                    else

                      pe_evaluation.pe_questions.where('pe_member_evaluator_id = ? AND pe_member_evaluated_id = ?', pe_member_evaluator.id, pe_member_evaluated.id).reorder('pe_questions.id DESC').each do |pe_question|

                        if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                          pe_question.destroy

                        end

                      end

                    end

                  end

                end

              else

                user_code_evaluated = fila[inicio_pos].to_s
                area_evaluated_name = fila[inicio_pos+1].to_s

                user_code_evaluator = fila[inicio_pos+2].to_s
                area_evaluator_name = fila[inicio_pos+3].to_s

                user_evaluator = User.find_by_codigo(user_code_evaluator)
                user_evaluated = User.find_by_codigo(user_code_evaluated)

                area_evaluated = pe_evaluation.pe_process.pe_areas.where('name = ?', area_evaluated_name).first
                area_evaluator = pe_evaluation.pe_process.pe_areas.where('name = ?', area_evaluator_name).first

                if user_evaluator && user_evaluated && area_evaluated && area_evaluator

                  pe_member_evaluated = pe_evaluation.pe_process.evaluated_members.where('user_id = ? and pe_area_id = ?', user_evaluated.id, area_evaluated.id).first

                  pe_member_evaluator = pe_evaluation.pe_process.evaluator_members.where('user_id = ? and pe_area_id = ?', user_evaluator.id, area_evaluator.id).first

                  if pe_member_evaluator && pe_member_evaluated

                    if reemplazar_solo_entered_by_manager == '1'

                      pe_evaluation.pe_questions.where('pe_member_evaluator_id = ? AND pe_member_evaluated_id = ? AND entered_by_manager = ?', pe_member_evaluator.id, pe_member_evaluated.id, 1).reorder('pe_questions.id DESC').each do |pe_question|

                        if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                          pe_question.destroy

                        end

                      end

                    else

                      pe_evaluation.pe_questions.where('pe_member_evaluator_id = ? AND pe_member_evaluated_id = ?', pe_member_evaluator.id, pe_member_evaluated.id).reorder('pe_questions.id DESC').each do |pe_question|

                        if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                          pe_question.destroy

                        end

                      end

                    end

                  end

                end

              end


            else

              if pe_evaluation.pe_process.of_persons?

                user_code_evaluated = fila[inicio_pos].to_s

                user_evaluated = User.find_by_codigo(user_code_evaluated)

                if user_evaluated

                  pe_member_evaluated = pe_evaluation.pe_process.evaluated_members.where('user_id = ?', user_evaluated.id).first

                  if pe_member_evaluated

                    if reemplazar_solo_entered_by_manager == '1'

                      pe_evaluation.pe_questions.where('pe_member_evaluator_id IS NULL AND pe_member_evaluated_id = ? AND entered_by_manager = ?', pe_member_evaluated.id, 1).reorder('pe_questions.id DESC').each do |pe_question|

                        if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                          pe_question.destroy

                        end

                      end

                    else

                      pe_evaluation.pe_questions.where('pe_member_evaluator_id IS NULL AND pe_member_evaluated_id = ?', pe_member_evaluated.id).reorder('pe_questions.id DESC').reorder('pe_questions.id DESC').each do |pe_question|

                        if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                          pe_question.destroy

                        end

                      end

                    end

                  end

                end

              else

                user_code_evaluated = fila[inicio_pos].to_s
                area_evaluated_name = fila[inicio_pos+1].to_s

                user_evaluated = User.find_by_codigo(user_code_evaluated)
                area_evaluated = pe_evaluation.pe_process.pe_areas.where('name = ?', area_evaluated_name).first

                if user_evaluated && area_evaluated

                  pe_member_evaluated = pe_evaluation.pe_process.evaluated_members.where('user_id = ? and pe_area_id = ?', user_evaluated.id, area_evaluated.id).first

                  if pe_member_evaluated

                    if reemplazar_solo_entered_by_manager == '1'

                      pe_evaluation.pe_questions.where('pe_member_evaluator_id IS NULL AND pe_member_evaluated_id = ? AND entered_by_manager = ?', pe_member_evaluated.id, 1).reorder('pe_questions.id DESC').each do |pe_question|

                        if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                          pe_question.destroy

                        end

                      end

                    else

                      pe_evaluation.pe_questions.where('pe_member_evaluator_id IS NULL AND pe_member_evaluated_id = ?', pe_member_evaluated.id).reorder('pe_questions.id DESC').each do |pe_question|

                        if pe_evaluation.pe_questions.where('pe_question_id = ?', pe_question.id).size == 0

                          pe_question.destroy

                        end

                      end

                    end

                  end

                end

              end

            end

          end

        end

      end

    end


    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        if groups_or_members == '1'

          if pe_group_ids

            pe_group_ids.each do |pe_group_id|

              unless pe_group_id.blank?

                current_pos = pe_evaluation.entered_by_manager_questions ? 1 : 0

                save_question(index+1, pe_evaluation, current_pos, fila, marcar_ready, marcar_confirmadas, marcar_aceptadas, groups_or_members, pe_members_or_pe_member, pe_group_id)

              end

            end

          end

          if pe_rel_ids

            pe_rel_ids.each do |pe_rel_id|

              unless pe_rel_id.blank?

                current_pos = pe_evaluation.entered_by_manager_questions ? 1 : 0

                save_question_2(index+1, pe_evaluation, current_pos, fila, marcar_ready, marcar_confirmadas, marcar_aceptadas, groups_or_members, pe_members_or_pe_member, pe_rel_id)

              end

            end

          end

        else

          if pe_members_or_pe_member == '1'

            if pe_evaluation.pe_process.of_persons?

              if pe_evaluation.entered_by_manager_questions
                user_code_evaluated = fila[1].to_s
                user_code_evaluator = fila[2].to_s
              else
                user_code_evaluated = fila[0].to_s
                user_code_evaluator = fila[1].to_s
              end

              user_evaluator = User.find_by_codigo(user_code_evaluator)
              user_evaluated = User.find_by_codigo(user_code_evaluated)

              if user_evaluator && user_evaluated

                pe_member_evaluator = pe_evaluation.pe_process.evaluator_members.where('user_id = ?', user_evaluator.id).first

                pe_member_evaluated = pe_evaluation.pe_process.evaluated_members.where('user_id = ?', user_evaluated.id).first

                if pe_member_evaluator && pe_member_evaluated

                  current_pos = pe_evaluation.entered_by_manager_questions ? 3 : 2

                  save_question(index+1, pe_evaluation, current_pos, fila, marcar_ready, marcar_confirmadas, marcar_aceptadas,groups_or_members, pe_members_or_pe_member, pe_member_evaluator, pe_member_evaluated)

                end

              end

            else

              if pe_evaluation.entered_by_manager_questions
                user_code_evaluated = fila[1].to_s
                area_evaluated_name = fila[2].to_s

                user_code_evaluator = fila[3].to_s
                area_evaluator_name = fila[4].to_s
              else
                user_code_evaluated = fila[0].to_s
                area_evaluated_name = fila[1].to_s

                user_code_evaluator = fila[2].to_s
                area_evaluator_name = fila[3].to_s
              end



              user_evaluator = User.find_by_codigo(user_code_evaluator)
              user_evaluated = User.find_by_codigo(user_code_evaluated)

              area_evaluated = pe_evaluation.pe_process.pe_areas.where('name = ?', area_evaluated_name).first
              area_evaluator = pe_evaluation.pe_process.pe_areas.where('name = ?', area_evaluator_name).first

              if user_evaluator && user_evaluated && area_evaluated && area_evaluator

                pe_member_evaluated = pe_evaluation.pe_process.evaluated_members.where('user_id = ? and pe_area_id = ?', user_evaluated.id, area_evaluated.id).first

                pe_member_evaluator = pe_evaluation.pe_process.evaluator_members.where('user_id = ? and pe_area_id = ?', user_evaluator.id, area_evaluator.id).first

                if pe_member_evaluator && pe_member_evaluated

                  current_pos = pe_evaluation.entered_by_manager_questions ? 5 : 4

                  save_question(index+1, pe_evaluation, current_pos, fila, marcar_ready, marcar_confirmadas, marcar_aceptadas, groups_or_members, pe_members_or_pe_member, pe_member_evaluator, pe_member_evaluated)

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('user: '+user_code_evaluated+' area: '+area_evaluated_name+'user: '+user_code_evaluator+' area: '+area_evaluator_name).force_encoding('UTF-8')
                  @lineas_error_messages.push ['No existen o el usuario o el área registrados en el proceso']

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('user: '+user_code_evaluated+' area: '+area_evaluated_name+'user: '+user_code_evaluator+' area: '+area_evaluator_name).force_encoding('UTF-8')
                @lineas_error_messages.push ['No existen o el usuario o el área']

              end

            end

          else

            if pe_evaluation.pe_process.of_persons?

              if pe_evaluation.entered_by_manager_questions
                user_code_evaluated = fila[1].to_s
              else
                user_code_evaluated = fila[0].to_s
              end

              user_evaluated = User.find_by_codigo(user_code_evaluated)

              if user_evaluated

                pe_member_evaluated = pe_evaluation.pe_process.evaluated_members.where('user_id = ?', user_evaluated.id).first

                if pe_member_evaluated

                  current_pos = pe_evaluation.entered_by_manager_questions ? 2 : 1

                  save_question(index+1, pe_evaluation, current_pos, fila, marcar_ready, marcar_confirmadas, marcar_aceptadas, groups_or_members, pe_members_or_pe_member, pe_member_evaluated)

                end

              end

            else

              if pe_evaluation.entered_by_manager_questions
                user_code_evaluated = fila[1].to_s
                area_evaluated_name = fila[2].to_s
              else
                user_code_evaluated = fila[0].to_s
                area_evaluated_name = fila[1].to_s
              end

              user_evaluated = User.find_by_codigo(user_code_evaluated)
              area_evaluated = pe_evaluation.pe_process.pe_areas.where('name = ?', area_evaluated_name).first

              if user_evaluated && area_evaluated

                pe_member_evaluated = pe_evaluation.pe_process.evaluated_members.where('user_id = ? and pe_area_id = ?', user_evaluated.id, area_evaluated.id).first

                if pe_member_evaluated

                  current_pos = pe_evaluation.entered_by_manager_questions ? 3 : 2

                  save_question(index+1, pe_evaluation, current_pos, fila, marcar_ready, marcar_confirmadas, marcar_aceptadas, groups_or_members, pe_members_or_pe_member, pe_member_evaluated)

                end

              end

            end

          end

        end

      end

    end

  end

  def set_definition_ready(archivo_temporal, pe_evaluation)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        user_code_evaluated = fila[0].to_s

        pe_process = pe_evaluation.pe_process

        pe_member_evaluated = pe_process.pe_member_by_user_code user_code_evaluated

        if pe_member_evaluated

          pe_evaluation.pe_questions_only_by_pe_member_all_levels(pe_member_evaluated).each do |pe_question|

            pe_question.confirmed = true
            pe_question.accepted = true
            pe_question.validated = true
            pe_question.ready = true
            pe_question.save

            pe_evaluation.num_steps_definition_by_users_validated_before_accepted.times do |n|

              pe_question.pe_question_validations.where('step_number = ? AND before_accept = ?', n+1, true).destroy_all

              pe_question_validation = pe_question.pe_question_validations.build
              pe_question_validation.step_number = n+1
              pe_question_validation.before_accept = true
              pe_question_validation.validated = true
              pe_question_validation.pe_evaluation = pe_evaluation
              pe_question_validation.save

            end

            pe_evaluation.num_steps_definition_by_users_validated_after_accepted.times do |n|

              pe_question.pe_question_validations.where('step_number = ? AND before_accept = ?', n+1, false).destroy_all

              pe_question_validation = pe_question.pe_question_validations.build
              pe_question_validation.step_number = n+1
              pe_question_validation.before_accept = false
              pe_question_validation.validated = true
              pe_question_validation.pe_evaluation = pe_evaluation
              pe_question_validation.save

            end


          end

        end

      end

    end

  end

  def set_informative_evaluation(pe_evaluation)

    s = 0

    pe_evaluation.pe_elements.each do |pe_element|

      if pe_element.assessed

        pe_evaluation.pe_questions_only_by_element_ready(pe_element.id).each do |pe_question|

          s += pe_question.weight

        end

        if s == 0

          pe_evaluation.informative = true
          pe_evaluation.save

        end

        break

      end

    end

  end

  def set_informative_questions(pe_evaluation)

    pe_evaluation.pe_elements.reverse_each do |pe_element|

      pe_evaluation.pe_questions_only_by_element_ready(pe_element.id).each do |pe_question|

        if pe_element.assessment_method == 0
          informative = true
          pe_question.pe_questions.each do |pe_child_question|
            informative = false unless pe_child_question.informative
          end

          pe_question.informative = informative
          pe_question.save

        elsif pe_element.assessment_method == 1
          sum_values = 0
          pe_question.pe_alternatives.each do |pe_alternative|
            sum_values += pe_alternative.value
          end

          if sum_values == 0
            pe_question.informative = true
            pe_question.save
          end

        end

      end

    end

  end

  private

  def save_question(num_fila, pe_evaluation, current_pos, fila, marcar_ready, marcar_confirmadas, marcar_aceptadas, groups_or_members, pe_members_or_pe_member, pe_assoc_1 = nil, pe_assoc_2 = nil)

    pe_questions_parent_id = nil
    last_pe_question = nil

    pe_evaluation.pe_elements.each_with_index do |pe_element, index_e|

      q_name = fila[current_pos].to_s

      if pe_element.assessed && !pe_element.simple?

        q_name = ''

        pe_element.pe_element_descriptions.each_with_index do |pe_element_description, index_e_d|

          q_name += fila[current_pos+index_e_d].to_s

        end

      end

      if groups_or_members == '1'

        if pe_questions_parent_id

          pe_question = pe_evaluation.pe_questions.where('pe_group_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id = ? AND pe_questions.description = ?', pe_assoc_1, pe_element.id, pe_questions_parent_id, CGI.unescapeHTML(format_text_to_html(q_name))).first

        else

          pe_question = pe_evaluation.pe_questions.where('pe_group_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND pe_questions.description = ?', pe_assoc_1, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first

        end

      else

        if pe_members_or_pe_member == '1'

          if pe_questions_parent_id

            pe_question = pe_evaluation.pe_questions.where('pe_member_evaluator_id = ? AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id = ? AND pe_questions.description = ?', pe_assoc_1, pe_assoc_2, pe_element.id, pe_questions_parent_id, CGI.unescapeHTML(format_text_to_html(q_name))).first

          else

            pe_question = pe_evaluation.pe_questions.where('pe_member_evaluator_id = ? AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND pe_questions.description = ?', pe_assoc_1, pe_assoc_2, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first

          end

        else

          if pe_questions_parent_id

            pe_question = pe_evaluation.pe_questions.where('pe_member_evaluator_id IS NULL AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id = ? AND pe_questions.description = ?', pe_assoc_1, pe_element.id, pe_questions_parent_id, CGI.unescapeHTML(format_text_to_html(q_name))).first

          else

            pe_question = pe_evaluation.pe_questions.where('pe_member_evaluator_id IS NULL AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND pe_questions.description = ?', pe_assoc_1, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first

          end

        end

      end

      unless pe_question

        pe_question = pe_evaluation.pe_questions.build

        pe_question.description = CGI.unescapeHTML(format_text_to_html(q_name))

        if pe_element.pe_question_models.size > 0
          pe_question_model = pe_element.pe_question_models.where('description = ?', pe_question.description).first
          pe_question.pe_question_model = pe_question_model if pe_question_model
        end

        if groups_or_members == '1'

          pe_question.pe_group_id = pe_assoc_1

        else

          if pe_members_or_pe_member == '1'

            pe_question.pe_member_evaluator = pe_assoc_1
            pe_question.pe_member_evaluated = pe_assoc_2

          else

            pe_question.pe_member_evaluated = pe_assoc_1

          end

        end

        pe_question.pe_element_id = pe_element.id
        pe_question.pe_question_id = pe_questions_parent_id

        if pe_element.assessed

          if pe_element.simple

            pe_question.weight = fila[current_pos+1]

            has_comment = fila[current_pos+2].to_s

            pe_question.has_comment = true if has_comment[0] == '1'
            pe_question.has_comment_compulsory = true if has_comment[1] == '1'
            pe_question.has_comment_2 = true if has_comment[2] == '1'
            pe_question.has_comment_2_compulsory = true if has_comment[3] == '1'

            pe_question.note = CGI.unescapeHTML(format_text_to_html(fila[current_pos+3].to_s)) if pe_element.allow_note && !fila[current_pos+3].blank?

          else

            pe_question.weight = fila[current_pos+pe_element.pe_element_descriptions.size]

            has_comment = fila[current_pos+pe_element.pe_element_descriptions.size+1].to_s

            pe_question.has_comment = true if has_comment[0] == '1'
            pe_question.has_comment_compulsory = true if has_comment[1] == '1'
            pe_question.has_comment_2 = true if has_comment[2] == '1'
            pe_question.has_comment_2_compulsory = true if has_comment[3] == '1'

            pe_question.note = CGI.unescapeHTML(format_text_to_html(fila[current_pos+pe_element.pe_element_descriptions.size+2])) if pe_element.allow_note && !fila[current_pos+pe_element.pe_element_descriptions.size+2].blank?

          end

        else

          pe_question.note = CGI.unescapeHTML(format_text_to_html(fila[current_pos+1])) if pe_element.allow_note && !fila[current_pos+1].blank?

          #pe_question.weight = 1

        end

      end

      count_allow_comment = pe_element.allow_note ? 1 : 0

      if pe_element.assessed && pe_element.assessment_method == 2

        #indicadores

        if pe_element.simple

          indicator_type = nil

          indicator_type = case fila[current_pos+3+count_allow_comment].downcase.strip
            when 'directo'
              0
            when 'inverso'
              1
            when 'desviación'
              2
            when 'rangos'
              3
            when 'discreto'
              4
            when 'especial'
              5
          end

          pe_question.indicator_type = indicator_type
          pe_question.indicator_unit = fila[current_pos+4+count_allow_comment]

          if indicator_type == 5
            pe_question.power = fila[current_pos+5+count_allow_comment]
            pe_question.leverage = fila[current_pos+6+count_allow_comment]
            pe_question.indicator_goal = fila[current_pos+7+count_allow_comment]

          else
            pe_question.indicator_goal = fila[current_pos+5+count_allow_comment] if indicator_type == 0 || indicator_type == 1 || indicator_type == 2
          end

        else

          indicator_type = nil

          indicator_type = case fila[current_pos+pe_element.pe_element_descriptions.size+2+count_allow_comment].downcase.strip
                             when 'directo'
                               0
                             when 'inverso'
                               1
                             when 'desviación'
                               2
                             when 'rangos'
                               3
                             when 'discreto'
                               4
                             when 'especial'
                               5
                           end

          pe_question.indicator_type = indicator_type
          pe_question.indicator_unit = fila[current_pos+pe_element.pe_element_descriptions.size+3+count_allow_comment]

          if indicator_type == 5
            pe_question.power = fila[current_pos+pe_element.pe_element_descriptions.size+4+count_allow_comment]
            pe_question.leverage = fila[current_pos+pe_element.pe_element_descriptions.size+5+count_allow_comment]
            pe_question.indicator_goal = fila[current_pos+pe_element.pe_element_descriptions.size+6+count_allow_comment]
          else
            pe_question.indicator_goal = fila[current_pos+pe_element.pe_element_descriptions.size+4+count_allow_comment] if indicator_type == 0 || indicator_type == 1 || indicator_type == 2
          end



        end

      end

      if pe_evaluation.allow_definition_by_users


        unless marcar_ready

          pe_question.confirmed = false
          pe_question.accepted = false
          pe_question.validated = false
          pe_question.ready = false

          if marcar_confirmadas && marcar_confirmadas == '1'
            pe_question.confirmed = true

            if pe_evaluation.allow_definition_by_users_accepted

              if marcar_aceptadas && marcar_aceptadas == '1'
                pe_question.accepted = true
              end

            end

          end

        end

      end

      pe_question.entered_by_manager = fila[0] if pe_evaluation.entered_by_manager_questions

      if pe_question.save

        if groups_or_members == '1'

        else

          if pe_members_or_pe_member == '1'

            if pe_questions_parent_id

              #pe_question_same = pe_evaluation.pe_questions.where('pe_questions.id <> ? AND pe_member_evaluator_id IS NOT NULL AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id = ? AND pe_questions.description = ?', pe_question.id, pe_assoc_2, pe_element.id, pe_questions_parent_id, CGI.unescapeHTML(format_text_to_html(q_name))).first
              pe_question_same = pe_evaluation.pe_questions.where('pe_questions.id <> ? AND pe_member_evaluator_id IS NOT NULL AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_questions.description = ?', pe_question.id, pe_assoc_2, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first
            else

              #pe_question_same = pe_evaluation.pe_questions.where('pe_questions.id <> ? AND pe_member_evaluator_id IS NOT NULL AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND pe_questions.description = ?', pe_question.id, pe_assoc_2, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first
              pe_question_same = pe_evaluation.pe_questions.where('pe_questions.id <> ? AND pe_member_evaluator_id IS NOT NULL AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_questions.description = ?', pe_question.id, pe_assoc_2, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first

            end

            if pe_question_same

              unless pe_question_same.questions_grouped_id

                pe_question_same.questions_grouped_id = pe_question_same.id
                pe_question_same.save

              end

              pe_question.questions_grouped_id = pe_question_same.questions_grouped_id
              pe_question.save

            end

          end

        end


        if marcar_ready

          if groups_or_members == '1'

          else

            if pe_members_or_pe_member == '1'

            else

              (1..pe_evaluation.num_steps_definition_by_users_validated_before_accepted).each do |num_step_val_before|

                pe_question_validation = pe_question.pe_question_validations.where('step_number = ? and before_accept = ?', num_step_val_before, true).first

                unless pe_question_validation

                  pe_question_validation = pe_question.pe_question_validations.build
                  pe_question_validation.step_number = num_step_val_before
                  pe_question_validation.before_accept = true
                  pe_question_validation.validated = true
                  pe_question_validation.pe_evaluation = pe_evaluation
                  pe_question_validation.save

                end

              end

              (1..pe_evaluation.num_steps_definition_by_users_validated_after_accepted).each do |num_step_val_after|

                pe_question_validation = pe_question.pe_question_validations.where('step_number = ? and before_accept = ?', num_step_val_after, false).first

                unless pe_question_validation

                  pe_question_validation = pe_question.pe_question_validations.build
                  pe_question_validation.step_number = num_step_val_after
                  pe_question_validation.before_accept = false
                  pe_question_validation.validated = true
                  pe_question_validation.pe_evaluation = pe_evaluation
                  pe_question_validation.save

                end

              end

            end

          end

        end


        unless pe_element.simple

          pe_question.pe_question_descriptions.destroy_all

          pe_element.pe_element_descriptions.each_with_index do |pe_element_description, index_e_d|

            pe_question_description = pe_question.pe_question_descriptions.build

            pe_question_description.description = CGI.unescapeHTML(format_text_to_html(fila[current_pos+index_e_d].to_s))

            pe_question_description.pe_element_description = pe_element_description

            pe_question_description.save

          end


        end

        pe_questions_parent_id = pe_question.id
        last_pe_question = pe_question

      else

        @lineas_error.push num_fila
        @lineas_error_detalle.push (pe_question.description).force_encoding('UTF-8')
        @lineas_error_messages.push [pe_question.errors.full_messages]

      end

      if pe_element.assessed

        if pe_element.simple?

          current_pos += 3

        else

          current_pos += pe_element.pe_element_descriptions.size + 2

        end

        if pe_element.assessment_method == 2
          #indicadores
          current_pos += 2
          if pe_element.available_indicator_types.include? '5'
            current_pos += 2
          end
        end

      else

        current_pos += 1

      end

      current_pos += count_allow_comment

    end

    begin_alt_ind = true

    if last_pe_question

      if last_pe_question.pe_element.assessment_method == 1

        #alternativas

        while fila[current_pos]

          if begin_alt_ind

            pe_alternative = last_pe_question.pe_alternatives.where('description = ?', CGI.unescapeHTML(fila[current_pos].to_s)).first

            unless pe_alternative

              pe_alternative = last_pe_question.pe_alternatives.build
              pe_alternative.description = CGI.unescapeHTML fila[current_pos].to_s

            end

            begin_alt_ind = false

          else

            temp_value = fila[current_pos].to_s.split(':')

            pe_alternative.value = temp_value[0].to_i

            pe_alternative.fixed = true if temp_value[1]=='fijo'

            pe_alternative.save

            begin_alt_ind = true

          end

          current_pos += 1

        end

      elsif last_pe_question.pe_element.assessment_method == 2

        #indicadores

        last_pe_question.pe_question_ranks.destroy_all

        if last_pe_question.indicator_type == 0 || last_pe_question.indicator_type == 1 || last_pe_question.indicator_type == 2

          last_pe_question.indicator_goal = fila[current_pos]

          last_pe_question.save

        else

          while fila[current_pos]

            if begin_alt_ind

              pe_question_rank = last_pe_question.pe_question_ranks.build
              if last_pe_question.indicator_type == 3
                pe_question_rank.goal = fila[current_pos]
              elsif last_pe_question.indicator_type == 4
                pe_question_rank.goal = 0
                pe_question_rank.discrete_goal = fila[current_pos].to_s
              end

              begin_alt_ind = false

            else

              pe_question_rank.percentage = fila[current_pos]

              pe_question_rank.save

              begin_alt_ind = true

            end

            current_pos += 1

          end

          last_pe_question.reload

          last_pe_question.set_goal_rank
          last_pe_question.save

          pe_question_rank_models = last_pe_question.indicator_type == 3 ? last_pe_question.pe_element.pe_question_rank_models_non_discrete : last_pe_question.pe_element.pe_question_rank_models_discrete
          pe_question_ranks = last_pe_question.pe_question_ranks

          pe_question_ranks.each_with_index do |pe_question_rank, index|

            if pe_question_rank_models[index]
              pe_question_rank.pe_question_rank_model = pe_question_rank_models[index]
              pe_question_rank.save
            end

          end


        end

      end

    end

  end

  def save_question_2(num_fila, pe_evaluation, current_pos, fila, marcar_ready, marcar_confirmadas, marcar_aceptadas, marcar_entered_by_manager, groups_or_members, pe_members_or_pe_member, pe_assoc_1 = nil, pe_assoc_2 = nil)

    pe_questions_parent_id = nil
    last_pe_question = nil

    pe_evaluation.pe_elements.each_with_index do |pe_element, index_e|

      q_name = fila[current_pos].to_s

      if pe_element.assessed && !pe_element.simple?

        q_name = ''

        pe_element.pe_element_descriptions.each_with_index do |pe_element_description, index_e_d|

          q_name += fila[current_pos+index_e_d].to_s

        end

      end

      if groups_or_members == '1'

        if pe_questions_parent_id

          pe_question = pe_evaluation.pe_questions.where('pe_rel_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id = ? AND pe_questions.description = ?', pe_assoc_1, pe_element.id, pe_questions_parent_id, CGI.unescapeHTML(format_text_to_html(q_name))).first

        else

          pe_question = pe_evaluation.pe_questions.where('pe_rel_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND pe_questions.description = ?', pe_assoc_1, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first

        end

      else

        if pe_members_or_pe_member == '1'

          if pe_questions_parent_id

            pe_question = pe_evaluation.pe_questions.where('pe_member_evaluator_id = ? AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id = ? AND pe_questions.description = ?', pe_assoc_1, pe_assoc_2, pe_element.id, pe_questions_parent_id, CGI.unescapeHTML(format_text_to_html(q_name))).first

          else

            pe_question = pe_evaluation.pe_questions.where('pe_member_evaluator_id = ? AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND description = ?', pe_assoc_1, pe_assoc_2, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first

          end

        else

          if pe_questions_parent_id

            pe_question = pe_evaluation.pe_questions.where('pe_member_evaluator_id IS NULL AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id = ? AND description = ?', pe_assoc_1, pe_element.id, pe_questions_parent_id, CGI.unescapeHTML(format_text_to_html(q_name))).first

          else

            pe_question = pe_evaluation.pe_questions.where('pe_member_evaluator_id IS NULL AND pe_member_evaluated_id = ? AND pe_questions.pe_element_id = ? AND pe_question_id IS NULL AND description = ?', pe_assoc_1, pe_element.id, CGI.unescapeHTML(format_text_to_html(q_name))).first

          end

        end

      end

      unless pe_question

        pe_question = pe_evaluation.pe_questions.build

        pe_question.description = CGI.unescapeHTML(format_text_to_html(q_name))

        if pe_element.pe_question_models.size > 0
          pe_question_model = pe_element.pe_question_models.where('description = ?', pe_question.description).first
          pe_question.pe_question_model = pe_question_model if pe_question_model
        end

        if groups_or_members == '1'

          pe_question.pe_rel_id = pe_assoc_1

        else

          if pe_members_or_pe_member == '1'

            pe_question.pe_member_evaluator = pe_assoc_1
            pe_question.pe_member_evaluated = pe_assoc_2

          else

            pe_question.pe_member_evaluated = pe_assoc_1

          end

        end

        pe_question.pe_element_id = pe_element.id
        pe_question.pe_question_id = pe_questions_parent_id

        if pe_element.assessed

          if pe_element.simple

            pe_question.weight = fila[current_pos+1]


            has_comment = fila[current_pos+2].to_s

            pe_question.has_comment = true if has_comment[0] == '1'
            pe_question.has_comment_compulsory = true if has_comment[1] == '1'
            pe_question.has_comment_2 = true if has_comment[2] == '1'
            pe_question.has_comment_2_compulsory = true if has_comment[3] == '1'

            pe_question.note = CGI.unescapeHTML(format_text_to_html(fila[current_pos+3])) if pe_element.allow_note && !fila[current_pos+3].blank?

          else

            pe_question.weight = fila[current_pos+pe_element.pe_element_descriptions.size]

            has_comment = fila[current_pos+pe_element.pe_element_descriptions.size+1].to_s

            pe_question.has_comment = true if has_comment[0] == '1'
            pe_question.has_comment_compulsory = true if has_comment[1] == '1'
            pe_question.has_comment_2 = true if has_comment[2] == '1'
            pe_question.has_comment_2_compulsory = true if has_comment[3] == '1'

            pe_question.note = CGI.unescapeHTML(format_text_to_html(fila[current_pos+pe_element.pe_element_descriptions.size+2])) if pe_element.allow_note && !fila[current_pos+pe_element.pe_element_descriptions.size+2].blank?

          end

        else

          #pe_question.weight = 1
          pe_question.note = CGI.unescapeHTML(format_text_to_html(fila[current_pos+1])) if pe_element.allow_note && !fila[current_pos+1].blank?
        end

      end

      count_allow_comment = pe_element.allow_note ? 1 : 0

      if pe_element.assessed && pe_element.assessment_method == 2

        #indicadores

        if pe_element.simple

          indicator_type = nil

          indicator_type = case fila[current_pos+3+count_allow_comment].downcase.strip
                             when 'directo'
                               0
                             when 'inverso'
                               1
                             when 'desviación'
                               2
                             when 'rangos'
                               3
                             when 'discreto'
                               4
                           end

          pe_question.indicator_type = indicator_type
          pe_question.indicator_unit = fila[current_pos+4+count_allow_comment]


        else

          indicator_type = nil

          indicator_type = case fila[current_pos+pe_element.pe_element_descriptions.size+2].downcase.strip
                             when 'directo'
                               0
                             when 'inverso'
                               1
                             when 'desviación'
                               2
                             when 'rangos'
                               3
                             when 'discreto'
                               4
                           end

          pe_question.indicator_type = indicator_type
          pe_question.indicator_unit = fila[current_pos+pe_element.pe_element_descriptions.size+3+count_allow_comment]

        end

      end

      if pe_evaluation.allow_definition_by_users

        unless marcar_ready

          pe_question.confirmed = false
          pe_question.accepted = false
          pe_question.validated = false
          pe_question.ready = false

          if marcar_confirmadas && marcar_confirmadas == '1'
            pe_question.confirmed = true

            if pe_evaluation.allow_definition_by_users_accepted

              if marcar_aceptadas && marcar_aceptadas == '1'
                pe_question.accepted = true
              end

            end

          end

        end

      end

      pe_question.entered_by_manager = marcar_entered_by_manager

      if pe_question.save

        unless pe_element.simple

          pe_question.pe_question_descriptions.destroy_all

          pe_element.pe_element_descriptions.each_with_index do |pe_element_description, index_e_d|

            pe_question_description = pe_question.pe_question_descriptions.build

            pe_question_description.description = CGI.unescapeHTML(format_text_to_html(fila[current_pos+index_e_d].to_s))

            pe_question_description.pe_element_description = pe_element_description

            pe_question_description.save

          end


        end

        pe_questions_parent_id = pe_question.id
        last_pe_question = pe_question

      else

      end

      if pe_element.assessed

        if pe_element.simple?

          current_pos += 3

        else

          current_pos +=  pe_element.pe_element_descriptions.size + 2

        end

        if pe_element.assessment_method == 2
          #indicadores
          current_pos += 2
        end

      else

        current_pos += 1

      end

      current_pos += count_allow_comment

    end

    begin_alt_ind = true

    if last_pe_question

      if last_pe_question.pe_element.assessment_method == 1

        #alternativas

        while fila[current_pos]

          if begin_alt_ind

            pe_alternative = last_pe_question.pe_alternatives.where('description = ?', CGI.unescapeHTML(fila[current_pos].to_s)).first

            unless pe_alternative

              pe_alternative = last_pe_question.pe_alternatives.build
              pe_alternative.description = CGI.unescapeHTML fila[current_pos].to_s

            end

            begin_alt_ind = false

          else

            temp_value = fila[current_pos].to_s.split(':')

            pe_alternative.value = temp_value[0].to_i

            pe_alternative.fixed = true if temp_value[1]=='fijo'

            pe_alternative.save

            begin_alt_ind = true

          end

          current_pos += 1

        end

      elsif last_pe_question.pe_element.assessment_method == 2

        #indicadores

        last_pe_question.pe_question_ranks.destroy_all

        if last_pe_question.indicator_type == 0 || last_pe_question.indicator_type == 1 || last_pe_question.indicator_type == 2

          last_pe_question.indicator_goal = fila[current_pos]

          last_pe_question.save

        else

          while fila[current_pos]

            if begin_alt_ind

              pe_question_rank = last_pe_question.pe_question_ranks.build
              if last_pe_question.indicator_type == 3
                pe_question_rank.goal = fila[current_pos]
              elsif last_pe_question.indicator_type == 4
                pe_question_rank.goal = 0
                pe_question_rank.discrete_goal = fila[current_pos].to_s
              end

              begin_alt_ind = false

            else

              pe_question_rank.percentage = fila[current_pos]

              pe_question_rank.save

              begin_alt_ind = true

            end

            current_pos += 1

          end

          last_pe_question.reload

          last_pe_question.set_goal_rank
          last_pe_question.save

        end

      end

    end

  end
end
