module SelResultsHelper

  def list_candidates_pending_results_by_step(sel_step_id, user_id)
    sel_process_step = SelProcessStep.find(sel_step_id)
    SelStepCandidate.joins(:sel_attendants, :jm_candidate => :sel_applicants).
        where(:sel_process_step_id => sel_step_id, :done => false, :exonerated => false, sel_attendants: {:user_id => user_id}, :sel_applicants => {:sel_process_id => sel_process_step.sel_process_id, :excluded => false})
  end

  def list_candidates_assigned_by_step_ordered_by_name(sel_step_id, user_id)
    sel_process_step = SelProcessStep.find(sel_step_id)
    SelStepCandidate.joins(:sel_attendants, :jm_candidate => :sel_applicants).
        where(:sel_process_step_id => sel_step_id, sel_attendants: {:user_id => user_id}, sel_applicants: {sel_process_id: sel_process_step.sel_process_id,excluded: false}).reorder('surname, name')
  end
end
