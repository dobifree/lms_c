module BpFormsHelper
  def link_to_remove_item(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_item(this); return false")
  end

  def link_to_add_item(name, f)
    item = BpItem.new
    fields = f.fields_for(:bp_items, item, :child_index => "new_item") do |builder|
      render partial: 'bp_forms/item_row_form',
             locals: {:item => item,
                      :item_form => builder,
                      :just_added => true,
                      :editable => true, #editable,
                      :item_types_for_select => BpItem.select_available_data_types}
    end
    link_to(name, '#', onclick: "add_item(this, \"#{escape_javascript(fields)}\"); return false")
  end
end
