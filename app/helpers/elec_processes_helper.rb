module ElecProcessesHelper

  def icon_elec_module_html_safe
    icon_elec_module.html_safe
  end

  def icon_elec_module
    '<i class="' + icon_elec_module_class + '"></i>'
  end

  def icon_elec_module_class
    'fa fa-trophy'
  end

  # definiciones de posibilidad de votar

  def elec_can_user_vote_in_category?(round, category)

    #validando que la ronda esté aún abierta
    if round.done?
      return false
    end
    #####################################################

    # validando en número máximo de votos
    if round.elec_votes.where(:user_id => user_connected.id, :elec_process_category_id => category.id).count >= category.max_votes
      return false
    end
    #####################################################

    # validando los filtros de características de votantes
    counter = 0
    if category.elec_category_characteristics.where(:filter_voter => true).each do |filter_voter|
      if value = UserCharacteristic.where(:user_id => user_connected.id, :characteristic_id => filter_voter.characteristic_id).first
        #TODO: validar forma de comparación para las mayúsculas y las tildes
        if value.formatted_value.upcase == filter_voter.match_value.upcase
          counter = counter + 1
        end
      end
    end.empty?
      counter = 1
    end

    if counter == 0
      return false
    end
    #####################################################


    return true

  end

  def elec_can_user_view_my_voters?(round, category)

    if round.done?
      if !round.elec_process_round.show_final_my_voters?
        return false
      end
    else
      if !round.elec_process_round.show_live_my_voters?
        return false
      end
    end


    return true
  end

  def elec_can_user_view_my_voters_detail?(round, category)


      if !round.elec_process_round.show_my_voters_detail?
        return false
      end

      if elec_my_voters_count(round, category) == 0
        return false
      end



    return true
  end

  def elec_my_voters_count(round, category)
    ElecVote.where(:elec_round_id => round.id ,:candidate_user_id => user_connected.id, :elec_process_category_id => category.id).count
  end

end
