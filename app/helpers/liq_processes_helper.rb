module LiqProcessesHelper

  def icon_liq_module_html_safe
    icon_liq_module.html_safe
  end

  def icon_liq_module
    '<i class="' + icon_liq_module_class + '"></i>'
  end

  def icon_liq_module_class
    'fa fa-money'
  end
end
