module BpAdminSeasonsHelper

  def active_season_periods (season_id)
    season = BpSeason.find(season_id)
    season_periods = season.bp_season_periods
    season_periods_1 = []

    season_periods.each do |season_period|
      if season_period.until
        season_periods_1 << season_period if season_period.until > lms_time
      else
        season_periods_1 << season_period
      end
    end

    return season_periods_1
  end

  def active_seasons
    seasons = BpSeason.where('until is null').all

    BpSeason.where('until is not null').all.each do |season|
      seasons << season if season.until > lms_time
    end

    return seasons
  end

end
