module PasswordPoliciesHelper

  def icon_password_policy_module_html_safe
    icon_password_policy_module.html_safe
  end

  def icon_password_policy_module
    '<i class="' + icon_password_policy_module_class + '"></i>'
  end

  def icon_password_policy_module_class
    'fa fa-key'
  end


  def link_to_remove_password_rule(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_password_rule(this); return false")
  end

  def link_to_add_password_rule(name, f)
    rule = PasswordRule.new
    fields = f.fields_for(:password_rules, rule, :child_index => "new_rule") do |builder|
      render('password_policies/password_rules/form', :f => builder)
    end
    link_to(name, '#', onclick: "add_password_rule(this, \"#{escape_javascript(fields)}\"); return false")
  end




end
