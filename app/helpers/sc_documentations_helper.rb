module ScDocumentationsHelper
  def update_active_doc(user_id)
    documents = ScDocumentation.where(user_id: user_id, deleted: false).all
    return unless documents
    documents = documents.sort_by{ |t| [t.sc_period.begin, t.begin] }.reverse
    documents.each_with_index do |doc, index|
      if index.zero?
        doc.active = true
        doc.deactivated_by_user_id = nil
        doc.deactivated_at = nil
      else
        if doc.active
          doc.active = false
          doc.deactivated_by_user_id = user_connected.id
          doc.deactivated_at = lms_time
        end
      end
      doc.save
    end
  end
end
