module BpAdminGroupsHelper
  def get_users_from_group(group_id)
    users = []
    group_users = BpGroupsUser.where(bp_group_id: group_id).all
    group_users.each { |group_user| users << group_user.user unless users.include?(group_user.user) }
    return users
  end

  def before_group_user(group_id, user_id)
    active = []
    groups_user = group_id ? BpGroupsUser.where('until is not null and user_id = ? and bp_group_id = ?', user_id, group_id) : BpGroupsUser.where('until is not null and user_id = ?', user_id)
    groups_user.each { |var| active << var if lms_time > var.until }
    return active
  end

  def now_group_user(group_id, user_id)
    active = []
    groups_user = group_id ? BpGroupsUser.where(bp_group_id: group_id, user_id: user_id) : BpGroupsUser.where(user_id: user_id)
    groups_user.each do |var|
      active << var if var.until && var.since < lms_time && var.until > lms_time
      active << var if !var.until && lms_time > var.since
    end
    return active
  end

  def later_group_user(group_id, user_id)
    active = []
    groups_user = group_id ? BpGroupsUser.where(bp_group_id: group_id, user_id: user_id) : BpGroupsUser.where(user_id: user_id)
    groups_user.each { |var| active << var if lms_time < var.since }
    return active
  end

  def before_groups
    active = []
    groups = BpGroup.where('until is not null')
    groups.each { |group| active << group if lms_time > group.until }
    return active
  end

  def now_groups
    active = []
    groups = BpGroup.all
    groups.each do |var|
      active << var if var.until && var.since < lms_time && var.until > lms_time
      active << var if !var.until && lms_time > var.since
    end
    return active
  end

  def later_groups
    active = []
    groups = BpGroup.all
    groups.each { |var| active << var if lms_time < var.since }
    return active
  end

  def active_group_user_by_user(group_id, user_id, since_dt, until_dt)
    active_groups_users = []
    groups_users = BpGroupsUser.where('until is null and user_id = ?', user_id)
    groups_users.each {|group_user| active_groups_users << group_user if group_user.since < until_dt} if groups_users && groups_users.size > 0

    groups_users = BpGroupsUser.where('since is not null and until is not null and user_id = ?', user_id)
    groups_users.each {|group_user| active_groups_users << group_user if group_user.since < until_dt && group_user.until > since_dt}

    active_groups_users.delete_if {|active_group_user| active_group_user.bp_group_id != group_id} if group_id
    return active_groups_users
  end

  def overlap_assoc?(since, until_, user_id)
    groups_user = BpGroupsUser.where(user_id: user_id)
    overlap_groups_user = []
    groups_user.each do |group_user|
      if group_user.until
        overlap_groups_user << group_user.since < until_ && group_user.until > since
      else
        overlap_groups_user << group_user unless until_ > group_user.since
      end
    end
    return overlap_groups_user
  end

  def its_safe(temp_file, group_id, close_open_assoc, close_with_excel, specified_datetime)
    should_save = true
    group_users = []
    assocs = []
    @excel_errors = []
    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-', '-', '-', '-', '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-', '-', '-', '-', '-', '-']
    end


    data.each_with_index do |row, index|
      this_should_save = true
      next if index.zero?

      user_cod = row[0].to_s
      user = User.where(:codigo => user_cod).first ? User.where(:codigo => user_cod).first : nil
      since_date = row[3].to_s
      since_time = row[4].to_s
      until_date = row[5].to_s
      until_time = row[6].to_s

      break unless row[0] or row[3] or row[4] or row[5] or row[6]
      since_dt = since_date + ' ' + since_time
      until_dt = until_date + ' ' + until_time

      unless since_dt.to_datetime
        @excel_errors << [index + 1, 'Fecha u hora de Desde no tiene formato adecuado', row[0].to_s, row[1].to_s, row[2].to_s, row[3].to_s, row[4].to_s, row[5].to_s, row[6].to_s]
        this_should_save = false
      end

      if until_dt.size.zero? && until_time.size.zero? && !until_dt.to_datetime
        @excel_errors << [index + 1, 'Fecha u hora de Hasta no tiene formato adecuado', row[0].to_s, row[1].to_s, row[2].to_s, row[3].to_s, row[4].to_s, row[5].to_s, row[6].to_s]
        this_should_save = false
      end

      unless user
        @excel_errors << [index + 1, 'No se encuentra usuario especificado', row[0].to_s, row[1].to_s, row[2].to_s, row[3].to_s, row[4].to_s, row[5].to_s, row[6].to_s]
        this_should_save = false
      end

      data.each_with_index do |aux_row, aux_index|
        break unless aux_row[0] or aux_row[3] or aux_row[4] or aux_row[5] or aux_row[6]
        aux_cod = aux_row[0].to_s
        if aux_cod and aux_cod == user_cod and index != aux_index
          @excel_errors << [index + 1, 'Usuario repetido en: ' + (aux_index + 1).to_s, row[0].to_s, row[1].to_s, row[2].to_s, row[3].to_s, row[4].to_s, row[5].to_s, row[6].to_s]
          this_should_save = false
        end
      end

      if this_should_save
        if close_open_assoc
          assoc = BpGroupsUser.where('until is null and user_id = ?', user.id).first
          if assoc
            assocs << assoc
            if close_with_excel
              if assoc.bp_group.until && since_dt > assoc.bp_group.until
                assoc.until = assoc.bp_group.until.to_datetime - 1.minute
              else
                assoc.until = since_dt.to_datetime - 1.minute
              end
            else
              assoc.until = specified_datetime.to_datetime - 1.minute
            end
            unless assoc.save
              assoc.errors.full_messages.each { |error| @excel_errors << [index + 1, 'No se puede cerrar asociación previa. '+error, row[0].to_s, row[1].to_s, row[2].to_s, row[3].to_s, row[4].to_s, row[5].to_s, row[6].to_s] }
            end
          end
        end

        group_user = BpGroupsUser.new(:bp_group_id => group_id, :user_id => user.id, :since => since_dt, :until => until_dt, :registered_at => lms_time, :registered_by_admin_id => admin_connected.id)
        if group_user.valid?
          group_users << group_user
        else
          this_should_save = false
          group_user.errors.full_messages.each { |error| @excel_errors << [index + 1, error, row[0].to_s, row[1].to_s, row[2].to_s, row[3].to_s, row[4].to_s, row[5].to_s, row[6].to_s] }
        end
      end

      should_save = false if this_should_save == false
    end

    if should_save
      group_users.each do |group_user|
        group_user.save
      end
    else
      assocs.each { |assoc| assoc.update_attributes(until: nil) }
    end
    return should_save
  end

  def its_safe_to_destroy(temp_file, group_id)
    should_save = true
    group_users = []
    assocs = []
    @excel_errors = []
    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-', '-']
    end

    data.each_with_index do |row, index|
      should_save = true
      next if index.zero?

      break unless row[0]
      user_cod = row[0].to_s
      user = User.where(:codigo => user_cod).first ? User.where(:codigo => user_cod).first : nil

      unless user
        @excel_errors << [index + 1, 'No se encuentra usuario especificado', row[0].to_s, row[1].to_s, row[2].to_s]
        should_save = false
      end

      data.each_with_index do |aux_row, aux_index|
        break unless aux_row[0]
        aux_cod = aux_row[0].to_s
        if aux_cod && aux_cod == user_cod && index != aux_index
          @excel_errors << [index + 1, 'Usuario repetido en: ' + (aux_index + 1).to_s, row[0].to_s, row[1].to_s, row[2].to_s]
          should_save = false
        end
      end

      if true #buscar group_user y saber si tiene relaciones que lo complican
        gus = BpGroupsUser.where(bp_group_id: group_id, user_id: user.id)
        gus.each { |group_user| group_users << group_user }
      end

    end

    group_users.each { |group_user| group_user.destroy } if should_save
    return should_save
  end

end
