module ScholarshipListsHelper

  def link_to_remove_scholarship_list_item(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_scholarship_list_item(this)")
  end

  def link_to_add_scholarship_list_item(name, f)
    scholarship_list_item = ScholarshipListItem.new
    fields = f.fields_for(:scholarship_list_items, scholarship_list_item, :child_index => "new_scholarship_list_item") do |builder|
      render('scholarship_lists/list_items/form', :f => builder)
    end
    link_to(name, '#', onclick: "add_scholarship_list_item(this, \"#{escape_javascript(fields)}\")")
  end

end
