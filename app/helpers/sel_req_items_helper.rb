module SelReqItemsHelper

  def link_to_remove_item_options(name, f)
    f.hidden_field(:_destroy) +
        if f.object.new_record? || !f.object.sel_requirement_values.any?
          link_to(name, '#', onclick: "remove_option(this,0); return false")
        else
          link_to(name, '#', onclick: "remove_option(this,1); return false")
        end

  end

  def link_to_add_item_options(name, f)
    option = SelReqOption.new
    fields = f.fields_for(:sel_req_options, option, :child_index => "new_option") do |builder|
      render('sel_req_items/form_option', :f => builder)
    end
    link_to_function(name, "add_option(this, \"#{escape_javascript(fields)}\")")
  end

end
