module CoursesHelper

  def order_course_elements(course, public = false)

    @elements = []
    @elements_ids = []

    units = course.units
    evaluations = course.evaluations
    polls = course.polls

    total = units.length + evaluations.length + polls.length

    total = units.last.orden if !units.empty? && total < units.last.orden
    total = evaluations.last.orden if !evaluations.empty? && total < evaluations.last.orden
    total = polls.last.orden if !polls.empty? && total < polls.last.orden

    (1..total).each do |pos|

      units.each do |unit|
        if unit.orden == pos
          @elements.push unit
          @elements_ids.push 'u-'+unit.id.to_s
        end
      end

      evaluations.each do |evaluation|
        if evaluation.orden == pos
          @elements.push evaluation
          @elements_ids.push 'e-'+evaluation.id.to_s
        end
      end

      polls.each do |poll|
        if poll.orden == pos
          @elements.push poll
          @elements_ids.push 'p-'+poll.id.to_s
        end
      end

    end

    if public

      @elements.each_with_index do |element, index|

        ei = @elements_ids[index].split('-')

        if ei[0] == 'p' && course.dncp && course.dncp.poll_presencial
          @elements.delete_at(index)
          @elements_ids.delete_at(index)
        end

      end

    end

  end

  def number_obligatory_course_elements(course)

    return course.units.count + course.evaluations.count + course.polls.where('obligatoria = true ').count

  end

  def order_course_elements_automatic


    @elements = []
    @elements_ids = []

    course.units.each_with_index do |unit, index|

      if index == 0
        @elements.push unit
        @elements_ids.push 'u-'+unit.id.to_s
      else

        position = 1;

        i1 = @elements.map.with_index{|e, i| i if e.id == unit.despues_de_unit_id }
        i1 = i1.last if i1
        i2 = @elements.map.with_index{|e, i| i if e.antes_de_unit_id == unit.id }
        i2 = i2.last if i2

        if i1 || i2

          if i1
            position = i1
            position = i2 if i2 && i2 > i1
          else
            position = i2
          end

          if unit.antes_de_unit_id.nil?

            position += 1

          else

            i1 = @elements.find_index { |e| e.id == unit.antes_de_unit_id }

            if i1 && i1 >= position

              position = i1

            else
              #fuente de error pide estar antes de un elemento que esta antes del despues
            end

          end

          @elements.insert position, unit
          @elements_ids.insert position, 'u-'+unit.id.to_s

        else

          i1 = @elements.find_index { |e| e.id == unit.antes_de_unit_id }
          i2 = @elements.find_index { |e| e.despues_de_unit_id == unit.id }

          if i1 || i2

            if i1
              position = i1
              position = i2 if i2 && i2 < i1
            else
              position = i2
            end

            @elements.insert position, unit
            @elements_ids.insert position, 'u-'+unit.id.to_s
          else
            @elements.push unit
            @elements_ids.push 'u-'+unit.id.to_s
          end

        end

      end

    end


    course.evaluations.each_with_index do |evaluation, index|

      position = 1;


      i1 = @elements.map.with_index do |e, i|
        pos = nil
        if @elements_ids[i].split('-').first == 'u'
          pos = i if e.id == evaluation.despues_de_unit_id
        elsif @elements_ids[i].split('-').first == 'e'
          pos = i if e.id == evaluation.despues_de_evaluation_id
        end
        pos
      end.compact
      #@er = i1
      i1 = i1.last if i1

      i2 = @elements.map.with_index { |e, i| i if e.antes_de_evaluation_id == evaluation.id }
      i2 = i2.last if i2


      if i1 || i2

        if i1
          position = i1
          position = i2 if i2 && i2 > i1
        else
          position = i2
        end

        if evaluation.antes_de_unit_id.nil? && evaluation.antes_de_evaluation_id.nil?

          position += 1

        else

          i1_u = @elements.find_index { |e| e.id == evaluation.antes_de_unit_id && @elements_ids[@elements.index(e)].split('-').first == 'u'}
          i1_e = @elements.find_index { |e| e.id == evaluation.antes_de_evaluation_id && @elements_ids[@elements.index(e)].split('-').first == 'e'}

          if i1_u && i1_u >= position

            position = i1_u

          end

          if i1_e && i1_e >= position
            position = i1_e

          end

          if i1_u && i1_e
            if i1_u<position && i1_e<position

              #fuente de error pide estar antes de un elemento que esta antes del despues
            end
          end

        end

        @elements.insert position, evaluation
        @elements_ids.insert position, 'e-'+evaluation.id.to_s

      else

        i1_u = @elements.find_index { |e| e.id == evaluation.antes_de_unit_id && @elements_ids[@elements.index(e)].split('-').first == 'u'}
        i1_e = @elements.find_index { |e| e.id == evaluation.antes_de_evaluation_id && @elements_ids[@elements.index(e)].split('-').first == 'e'}

        i1 = nil

        if i1_u
          i1 = i1_u
          i1 = i1_e if i1_e && i1_e < i1_u
        else
          i1 = i1_e
        end

        i2 = @elements.find_index { |e| e.despues_de_evaluation_id == evaluation.id }

        if i1 || i2

          if i1
            position = i1
            position = i2 if i2 && i2 < i1
          else
            position = i2
          end

          @elements.insert position, evaluation
          @elements_ids.insert position, 'e-'+evaluation.id.to_s
        else
          @elements.push evaluation
          @elements_ids.push 'e-'+evaluation.id.to_s
        end

      end

    end

    course.polls.each_with_index do |poll, index|

      position = 1;

      i1 = @elements.map.with_index do |e, i|
        pos = nil
        if @elements_ids[i].split('-').first == 'u'
          pos = i if e.id == poll.despues_de_unit_id
        elsif @elements_ids[i].split('-').first == 'e'
          pos = i if e.id == poll.despues_de_evaluation_id
        elsif @elements_ids[i].split('-').first == 'p'
          pos = i if e.id == poll.despues_de_poll_id
        end
        pos
      end.compact

      i1 = i1.last if i1

      i2 = @elements.map.with_index { |e, i| i if e.antes_de_poll_id == poll.id }
      i2 = i2.last if i2


      if i1 || i2

        if i1
          position = i1
          position = i2 if i2 && i2 > i1
        else
          position = i2
        end

        if poll.antes_de_unit_id.nil? && poll.antes_de_evaluation_id.nil? && poll.antes_de_poll_id.nil?

          position += 1

        else

          i1_u = @elements.find_index { |e| e.id == poll.antes_de_unit_id && @elements_ids[@elements.index(e)].split('-').first == 'u'}
          i1_e = @elements.find_index { |e| e.id == poll.antes_de_evaluation_id && @elements_ids[@elements.index(e)].split('-').first == 'e'}
          i1_p = @elements.find_index { |e| e.id == poll.antes_de_poll_id && @elements_ids[@elements.index(e)].split('-').first == 'p'}

          if i1_u && i1_u >= position

            position = i1_u

          end

          if i1_e && i1_e >= position
            position = i1_e

          end

          if i1_p && i1_p >= position
            position = i1_p

          end

          if i1_u && i1_e && i1_p
            if i1_u<position && i1_e<position && i1_p<position

              #fuente de error pide estar antes de un elemento que esta antes del despues
            end
          end

        end

        @elements.insert position, poll
        @elements_ids.insert position, 'p-'+poll.id.to_s

      else

        i1_u = @elements.find_index { |e| e.id == poll.antes_de_unit_id && @elements_ids[@elements.index(e)].split('-').first == 'u'}
        i1_e = @elements.find_index { |e| e.id == poll.antes_de_evaluation_id && @elements_ids[@elements.index(e)].split('-').first == 'e'}
        i1_e = @elements.find_index { |e| e.id == poll.antes_de_poll_id && @elements_ids[@elements.index(e)].split('-').first == 'p'}

        i1 = [i1_u,i1_e,i1_p].compact.min

        #if i1_u
        #  i1 = i1_u
        #  i1 = i1_e if i1_e && i1_e < i1_u
        #else
        #  i1 = i1_e
        #end

        i2 = @elements.find_index { |e| e.despues_de_poll_id == poll.id }

        if i1 || i2

          if i1
            position = i1
            position = i2 if i2 && i2 < i1
          else
            position = i2
          end

          @elements.insert position, poll
          @elements_ids.insert position, 'p-'+poll.id.to_s
        else
          @elements.push poll
          @elements_ids.push 'p-'+poll.id.to_s
        end

      end



    end



  end

end
