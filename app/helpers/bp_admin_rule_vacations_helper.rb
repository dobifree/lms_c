module BpAdminRuleVacationsHelper
  include BpAdminConditionVacationsHelper

  def verify_to_delete_rules_vacation(rule_vacation_id)
    rule_vacation = BpRuleVacation.find(rule_vacation_id)
    rule_vacation.bp_condition_vacations.each do |condition_vacation|
      answer = verify_to_delete_condition_vacation(condition_vacation.id)
      next if answer
      return false
    end
    true
  end
end
