module SelVacantFlowsHelper

  def link_to_remove_approver_char(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_approver_char(this); return false")
  end

  def link_to_add_approver_char(name, f)
    approver_char = SelFlowApproverChar.new
    fields = f.fields_for(:sel_flow_approver_chars, approver_char, :child_index => "new_approver_char") do |builder|
      render('sel_vacant_flows/approvers/char_approver_form', :f => builder)
    end
    link_to(name, '#', onclick: "add_approver_char(this, \"#{escape_javascript(fields)}\"); return false")
  end



end
