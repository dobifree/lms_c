module TrackingPeriodListsHelper

  def link_to_remove_period_item(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_period_item(this); return false")
  end

  def link_to_add_period_item(name, f)
    item = TrackingPeriodItem.new
    fields = f.fields_for(:tracking_period_items, item, :child_index => "new_item") do |builder|
      render('tracking_period_lists/form_tracking_period_item', :f => builder)
    end
    link_to(name, '#', onclick: "add_period_item(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
