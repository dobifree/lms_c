module UsersHelper

  def search_normal_users(last_name, first_name)

    users = nil

    unless last_name.blank? && first_name.blank?

      users = User.where(
          'apellidos LIKE ? AND nombre LIKE ? AND activo = ?',
          '%'+last_name+'%', '%'+first_name+'%', true).order('apellidos, nombre')

    end

    return users

  end

  def search_active_users(code, last_name, first_name)

    users = nil

    unless code.blank? && last_name.blank? && first_name.blank?

      users = User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo = ?', '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%', true).order('apellidos, nombre')

    end

    return users

  end

  def search_and_list_active_users(code, last_name, first_name)

    users = User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo = ?', '%'+code+'%', '%'+last_name+'%', '%'+first_name+'%', true).order('apellidos, nombre')

    return users

  end

  def search_and_list_normal_users(last_name, first_name)

    users = nil

    unless last_name.nil? && first_name.nil?

      users = User.where(
          'apellidos LIKE ? AND nombre LIKE ? AND activo = ?',
          '%'+last_name+'%', '%'+first_name+'%', true).order('apellidos, nombre')

    end

    return users

  end

  def search_users(codigo, last_name, first_name)

    users = {}

    unless codigo.blank? && last_name.blank? && first_name.blank?

      users = User.where(
          'codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ?',
          '%'+codigo+'%', '%'+last_name+'%', '%'+first_name+'%').order('apellidos, nombre')

    end

    return users

  end

  def search_employees(codigo, last_name, first_name)

    users = {}

    unless codigo.blank? && last_name.blank? && first_name.blank?

      users = User.where(
          'employee = ? AND codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ?',
          true, '%'+codigo+'%', '%'+last_name+'%', '%'+first_name+'%').order('apellidos, nombre')

    end

    return users

  end

  def search_active_employees(last_name, first_name)

    users = {}

    unless last_name.blank? && first_name.blank?

      users = User.where(
          'employee = ? AND activo = ? AND apellidos LIKE ? AND nombre LIKE ?',
          true, true, '%'+last_name+'%', '%'+first_name+'%').order('apellidos, nombre')

    end

    return users

  end

  def search_users_with_chars(user_data, char_data)

    if user_data
      user_data.delete_if { |field, value| value == '' }
    end

    if char_data
      char_data.delete_if { |char_id, value| value == [''] }
      char_data.each_key { |key| char_data[key.to_s.to_sym].slice! 0 }
    end


    pivot_users = User.where('true')

    if user_data
      user_data.each_key do |field|
        pivot_users = pivot_users.where('' + field + ' LIKE ?', '%' + user_data[field] + '%')
      end
    end

    if char_data && !char_data.empty?
      pivot_users = pivot_users.joins(:user_characteristics)
      char_data.each_key do |char_id|
        pivot_users = pivot_users.where('user_characteristics.characteristic_id = ? AND user_characteristics.valor IN (?)', char_id, char_data[char_id])
      end
    end

    return pivot_users
  end

  def xls_for_carga_usuarios()

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_usuarios')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'cod usuario'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'email'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'password'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'empleado'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'activo'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'inicio-características'
        filaHeader <<  headerCenter
        cols_widths << 20

        CharacteristicType.all.each do |characteristic_type|

          characteristic_type.characteristics.each do |characteristic|

            unless characteristic.data_type_id == 6 || characteristic.data_type_id == 8 || characteristic.data_type_id == 9 || characteristic.data_type_id == 10 || characteristic.data_type_id == 12 || characteristic.register_characteristic

              fila << characteristic.nombre
              filaHeader <<  headerCenter
              cols_widths << 20

              if characteristic.data_type_id == 11

                characteristic.elements_characteristics.reorder('register_position').each do |elements_characteristic|

                  fila << elements_characteristic.nombre
                  filaHeader <<  headerCenter
                  cols_widths << 20

                end

              end

            end

          end

        end

        Characteristic.not_grouped_public_characteristics.each do |characteristic|

          unless characteristic.data_type_id == 6 || characteristic.data_type_id == 8 || characteristic.data_type_id == 9 || characteristic.data_type_id == 10 || characteristic.data_type_id == 12 || characteristic.register_characteristic

            fila << characteristic.nombre
            filaHeader <<  headerCenter
            cols_widths << 20

            if characteristic.data_type_id == 11

              characteristic.elements_characteristics.reorder('register_position').each do |elements_characteristic|

                fila << elements_characteristic.nombre
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

          end

        end

        Characteristic.not_grouped_private_characteristics.each do |characteristic|

          unless characteristic.data_type_id == 6 || characteristic.data_type_id == 8 || characteristic.data_type_id == 9 || characteristic.data_type_id == 10 || characteristic.data_type_id == 12 || characteristic.register_characteristic

            fila << characteristic.nombre
            filaHeader <<  headerCenter
            cols_widths << 20

            if characteristic.data_type_id == 11

              characteristic.elements_characteristics.reorder('register_position').each do |elements_characteristic|

                fila << elements_characteristic.nombre
                filaHeader <<  headerCenter
                cols_widths << 20

              end

            end

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def carga_usuario_from_excel(archivo_temporal, from_date)

   company = @company

    campos = Array.new
    characteristics = Array.new
    characteristic_names = Array.new

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index == 0

        in_fields = true

        fila.each do |celda|

          if celda == 'inicio-características'
            in_fields = false
          elsif in_fields
            campos.push celda
          else
            unless celda.blank?
              characteristic = Characteristic.find_by_nombre celda
              characteristics.push characteristic ? characteristic : nil
              characteristic_names.push celda
            else
              break
            end

          end

        end

      elsif index > 0 && fila[0]

        new_user = false

        fila[0] = fila[0].to_s

        user = User.find_by_codigo fila[0]

        unless user

          new_user = true

          user = User.new
          user.codigo = fila[0]

        end

        current_col_pos = 0

        campos.each do |campo|

          unless fila[current_col_pos].blank?

            if campo == 'apellidos'

              user.apellidos = fila[current_col_pos]

            elsif campo == 'nombre'

              user.nombre = fila[current_col_pos]

            elsif campo == 'email'

              user.email = fila[current_col_pos] == '*.-' ? nil : fila[current_col_pos]

            elsif campo == 'password'

              user.password = fila[current_col_pos]

            elsif campo == 'empleado'

              if fila[current_col_pos].to_i == 1
                user.employee = true
              elsif fila[current_col_pos].to_i == 0
                user.employee = false
              end

            elsif campo == 'activo'

              if fila[current_col_pos].to_i == 1
                user.activo = true
              elsif fila[current_col_pos].to_i == 0
                user.activo = false
              end

            end

          end

          current_col_pos += 1
        end



        if user.password.blank? && new_user

          if company.locale == 'es_PE'
            user.password = user.codigo.slice(user.codigo.size-4,4)
          else
            user.password = user.codigo.slice(0,4)
          end

        end

        if user.save

          current_col_pos += 1
          #para pasar la columna inicio-características

          uc_register = nil

          characteristics.each_with_index do |characteristic, index_c|

            if characteristic

              unless characteristic.data_type_id == 6
                #sino es de tipo file

                unless characteristic.data_type_id == 8
                  #sino es de tipo boss_code

                  unless characteristic.data_type_id == 9
                    #sino es de tipo boss_full_name

                    unless characteristic.data_type_id == 10
                      #sino es de tipo boss_characteristic

                      unless characteristic.data_type_id == 12
                        #sino es de tipo user_code

                        if characteristic.data_type_id == 11

                          #register

                          error, uc_register = user.add_user_characteristic(characteristic, '-', from_date, nil, nil, admin_connected, admin_connected, company)

                        else

                          unless fila[current_col_pos].blank?

                            new_value = nil

                            error = false

                            unless fila[current_col_pos] == '*.-'

                              if characteristic.data_type_id == 5

                                characteristic_value = characteristic.characteristic_values.where('value_string = ?', fila[current_col_pos].to_s).first
                                if characteristic_value
                                  new_value = characteristic_value.id
                                else
                                  error = true

                                  @lineas_error.push index+1
                                  @lineas_error_detalle.push ('codigo: '+fila[0])
                                  @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' de tipo list no tiene entre sus posibles valores '+fila[current_col_pos].to_s]

                                end

                              elsif characteristic.data_type_id == 14

                                characteristic_value = characteristic.characteristic_values.where('value_string = ?', fila[current_col_pos].to_s).first
                                if characteristic_value
                                  new_value = characteristic_value.id
                                else
                                  error = true

                                  @lineas_error.push index+1
                                  @lineas_error_detalle.push ('codigo: '+fila[0])
                                  @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' de tipo list no tiene entre sus posibles valores '+fila[current_col_pos].to_s]

                                end

                              else

                                new_value = fila[current_col_pos]

                              end

                            end

                            unless error

                              if characteristic.register_characteristic

                                error, user_characteristic = user.add_user_characteristic(characteristic, new_value, from_date, nil, nil, admin_connected, admin_connected, company)

                              else

                                error, user_characteristic = user.update_user_characteristic(characteristic, new_value, from_date, nil, nil, admin_connected, admin_connected, company)

                              end

                              if error

                                @lineas_error.push index+1
                                @lineas_error_detalle.push ('codigo: '+fila[0])
                                @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' no fue guardada. '+(user_characteristic ? user_characteristic.errors.full_messages.to_s : '' )]

                              elsif characteristic.register_characteristic

                                user_characteristic.register_user_characteristic = uc_register
                                user_characteristic.save

                              end

                            end

                          end

                        end

                      else

                        @lineas_error.push index+1
                        @lineas_error_detalle.push ('codigo: '+fila[0])
                        @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' de tipo user_code no puede ser actualizada']

                      end

                    else

                      @lineas_error.push index+1
                      @lineas_error_detalle.push ('codigo: '+fila[0])
                      @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' de tipo boss_characteristic no puede ser actualizada']

                    end

                  else

                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('codigo: '+fila[0])
                    @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' de tipo boss_full_name no puede ser actualizada']

                  end

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('codigo: '+fila[0])
                  @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' de tipo boss_code no puede ser actualizada']

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo: '+fila[0])
                @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' de tipo file no puede ser actualizada']

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('codigo: '+fila[0])
              @lineas_error_messages.push ['característica: '+characteristic_names[index_c]+' no existe']

            end

            current_col_pos += 1
          end

        else

          @lineas_error.push index+1
          @lineas_error_detalle.push ('codigo: '+fila[0])
          @lineas_error_messages.push ['No se pudo guardar el usuario: '+user.errors.full_messages.to_s]

        end

      end

    end

  end

end
