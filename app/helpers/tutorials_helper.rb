module TutorialsHelper

  def icon_tutorial_module_html_safe
    icon_tutorial_module.html_safe
  end

  def icon_tutorial_module
    '<i class="' + icon_tutorial_module_class + '"></i>'
  end

  def icon_tutorial_module_class
    'fa fa-leanpub'
  end
end
