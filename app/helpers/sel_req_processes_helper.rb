module SelReqProcessesHelper

  def link_to_remove_sel_req_approver(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_sel_req_approver(this); return false")
  end

  def link_to_add_sel_req_approver(name, f)
    sel_req_approver = SelReqApprover.new
    fields = f.fields_for(:sel_req_approvers, sel_req_approver, :child_index => "new_sel_req_approver") do |builder|
      render('sel_req_processes/sel_req_approvers/form', :f => builder)
    end
    link_to(name, '#', onclick: "add_sel_req_approver(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
