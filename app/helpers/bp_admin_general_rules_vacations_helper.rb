module BpAdminGeneralRulesVacationsHelper

  def its_safe_progressive_days(temp_file)
    should_save = true
    progressive_days = []
    @excel_errors = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-', '-', '-', '-', '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-', '-', '-', '-', '-', '-']

    end

    data.each_with_index do |row, index|
      next if index.zero?

     break if !row[0] && !row[1]

      if !row[0] || row[0].to_s.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Año no puede ser vacío',row[0], row[1]]
      end

      if !row[1] || row[1].to_s.empty?
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Día no puede ser vacía',row[0], row[1]]
      end

      begin
        year = row[0].to_i
      rescue Exception => e
        begin_date = nil
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Año no tiene formato adecuado',row[0], row[1]]
      end

      begin
        days = row[1].to_i
      rescue Exception => e
        begin_date = nil
        should_save = false
        @excel_errors << [(index + 1).to_s, 'Día no tiene formato adecuado',row[0], row[1]]
      end

      if should_save
        progressive_day = BpProgressiveDay.new()
        progressive_day.year = year ? year : nil
        progressive_day.days = days ? days : nil
        progressive_day.bp_general_rules_vacation_id = @bp_general_rules_vacation.id
        progressive_day.registered_by_admin_id = admin_connected.id
        progressive_day.registered_at = lms_time

        if progressive_day.valid?
          progressive_days << progressive_day
        else
          should_save = false
          errors = ''
          progressive_day.errors.full_messages.each { |error| errors += error.to_s+'. ' }
          @excel_errors << [(index + 1).to_s, errors,row[0], row[1]]
        end
      end
    end

    if should_save
      progressive_days.each do |progressive_day|
        progressive_day.save
      end
    end

    return should_save
  end


end
