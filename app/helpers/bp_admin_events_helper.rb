module BpAdminEventsHelper
  def active_event_files (event_id)
    event = BpEvent.find(event_id)
    event_files = event.bp_event_files
    event_files_1 = []

    event_files.each do |event_file|
      if event_file.until
        event_files_1 << event_file if event_file.until > lms_time
      else
        event_files_1 << event_file
      end
    end

    return event_files_1
  end

  def before_events
    active = []
    events = BpEvent.where('until is not null')
    events.each { |var| active << var if lms_time > var.until }
    return active
  end

  def now_events
    active = []
    events = BpEvent.all
    events.each do |var|
      active << var if var.until && var.since < lms_time && var.until > lms_time
      active << var if !var.until && lms_time > var.since
    end
    return active
  end

  def later_events
    active = []
    events = BpEvent.all
    events.each { |var| active << var if lms_time < var.since }
    return active
  end

end
