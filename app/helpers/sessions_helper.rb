module SessionsHelper

  def create_session(user, bd)

    session[:user_connected_id] = user.id
    session[:user_connected_is_admin] = false

    company = @company
    #session[:company] = company

    #c = GeoIP.new("#{Rails.root}/public/GeoLiteCity.dat").city(request.remote_ip)
    #session[:user_connected_time_zone] = c ? c.timezone : 'America/Lima'

    #session[:user_connected_time_zone] = company.time_zone

    session[:user_connected_locale] = company.locale

    if company.locale == 'es_PE'
      session[:user_connected_locale_separator] = '.'
      session[:user_connected_locale_delimiter] = ','
    elsif company.locale == 'es_CL'
      session[:user_connected_locale_separator] = ','
      session[:user_connected_locale_delimiter] = '.'
    end

    session[:user_connected_bd] = bd

    #session[:company_codigo] = company.codigo

    #session[:alias_username] = company.alias_username ? company.alias_username : 'Código de usuario'

    session[:last_seen] = Time.now

=begin
    if company.alias_user

      t = company.alias_user.split(',')
      session[:alias_user] = t[0].strip
      session[:alias_users] = t[1].strip

    else
      session[:alias_user] = 'Usuario'
      session[:alias_users] = 'Usuarios'

    end
=end

    self.user_connected = user

    session['ct_menus_cod'] = Array.new
    session['ct_menus_user_menu_alias'] = Array.new

    CtMenu.all.each do |ct_menu|

      session['ct_menus_cod'].push ct_menu.cod
      session['ct_menus_user_menu_alias'].push ct_menu.user_menu_alias

    end

    session[:displayed_banners] = false

  end

  def create_session_admin(admin)

    session[:admin_connected_id] = admin.id

    session[:user_connected_is_admin] = true

    company = @company
    #session[:company] = company

    #c = GeoIP.new("#{Rails.root}/public/GeoLiteCity.dat").city(request.remote_ip)
    #session[:user_connected_time_zone] = c ? c.timezone : 'America/Lima'

    #session[:user_connected_time_zone] = company.time_zone

    #session[:user_connected_locale] = company.locale

    if company.locale == 'es_PE'
      session[:user_connected_locale_separator] = '.'
      session[:user_connected_locale_delimiter] = ','
    elsif company.locale == 'es_CL'
      session[:user_connected_locale_separator] = ','
      session[:user_connected_locale_delimiter] = '.'
    end

    session['ct_menus_cod'] = Array.new
    session['ct_menus_user_menu_alias'] = Array.new

    CtMenu.all.each do |ct_menu|

      session['ct_menus_cod'].push ct_menu.cod
      session['ct_menus_user_menu_alias'].push ct_menu.user_menu_alias

    end

    #session[:company_codigo] = company.codigo

    #session[:alias_username] = company.alias_username ? company.alias_username : 'Código de usuario'

    self.admin_connected = admin

  end

  def user_connected_time_zone
    @company.time_zone
  end

  def user_connected_locale
    @company.locale
  end

  def user_connected_locale_separator
    session[:user_connected_locale_separator]
  end

  def user_connected_locale_delimiter
    session[:user_connected_locale_delimiter]
  end

  def user_connected=(user)
    @user_connected = user
  end

  def admin_connected=(admin)
    @admin_connected = admin
  end

  def user_connected
    @user_connected ||= User.find(session[:user_connected_id])
  end

  def admin_connected
    @admin_connected ||= Admin.find(session[:admin_connected_id])
  end

  def company_codigo
    @company.codigo
  end

  def alias_username
    @company.alias_username ? @company.alias_username : 'Código de usuario'
  end

  def alias_user
    @company.alias_user ? @company.alias_user.split(',')[0].strip : 'Usuario'
    #session[:alias_user]
  end

  def alias_users
    @company.alias_user ? @company.alias_user.split(',')[1].strip : 'Usuarios'
    #session[:alias_users]
  end

  def set_displayed_banners
    session[:displayed_banners] = true
  end

  def displayed_banners
    session[:displayed_banners]
  end

  def logged_in?

    if session[:user_connected_id]

      #puts Time.now.to_i - session[:last_seen].to_i
      #puts (Time.now - session[:last_seen]).to_i
      #puts '---'

      # TODO: colocar este parámetro en el company

      session[:time_session_in_minutes] = 1440
      session[:time_session_in_minutes] = 60 if $current_domain == 'misecurity-training' || $current_domain == 'misecurity'

      time_session_in_minutes = 1440
      time_session_in_minutes = 60 if Rails.env.production? && ($current_domain == 'misecurity-training' || $current_domain == 'misecurity')

      if (Time.now.to_i - session[:last_seen].to_i) > time_session_in_minutes * 60
        sign_out
        false
      else
        session[:last_seen] = Time.now
        true
      end

    elsif session[:admin_connected_id]
      true
    else
      false
    end

  end

  def normal_user_logged_in?
    !session[:user_connected_id].nil?
  end

  def admin_logged_in?
    session[:user_connected_is_admin]
  end

  def user_connected_bd?
    session[:user_connected_bd]
  end

  def authenticate_user

    unless logged_in?
      @redirect_to_after_success_login_path = request.path_info if request.get?
      session[:redirect_post_login] = @redirect_to_after_success_login_path ? @redirect_to_after_success_login_path : root_path
      if @company.active_directory
        set_custom_http_auth_configuration
        render 'static_pages/loading_sesion_data_from_external_provider'
      else
        #unless session[:user_connected_id] || session[:admin_connected_id]
        redirect_to root_path
      end
    end

  end

  def authenticate_user_admin
    unless admin_logged_in?
      flash[:danger] = t('security.no_admin_privilege')
      redirect_to(root_path)
    end
  end

  def authenticate_user_god
    unless admin_logged_in? && [1, 4].include?(admin_connected.id)
      flash[:danger] = t('security.no_admin_privilege')
      redirect_to(root_path)
    end
  end

  def set_custom_http_auth_configuration
    setting_config = File.join(Rails.root, 'config', 'custom_http_auth.yml')
    raise "#{setting_config} is missing!" unless File.exists? setting_config
    @custom_http_auth = YAML.load_file(setting_config)[@company.connection_string] || {}
  end

  def decryp_user_from_custom_http_auth(data, cipher)
    # Proceso de desencriptación
    unescaped = CGI.unescape(data) # Se le quita el urlencode
    decoded = Base64.decode64(unescaped) # Se descodifica de base64
    cipher.iv = decoded[0..15] # Se carga el IV. Este corresponde a los primeros 16 caracteres de la data recibida
    decrypted = cipher.update(decoded[16..decoded.length - 1]) # Se hace el primer paso de desencriptación
    decrypted << cipher.final # Se finaliza la desencriptación
    # Se considera que esté dentro de 1 minuto la solicitud
    timestamp = decrypted[-10..(decrypted.length - 1)].to_i
    if ((timestamp - 30)..(timestamp + 30)).include?(Time.now.to_i)
      # Se retorna el dato del usuariodesencriptado
      return decrypted[0..(decrypted.length - 11)]
    else
      puts "Error de coincidencia de timestamp. Hora local: " + Time.now.to_i.to_s + " - timestamp: " + timestamp.to_s
      raise "Error de coincidencia de timestamp. Hora local: " + Time.now.to_i.to_s + " - timestamp: " + timestamp.to_s
    end
  end

  def sign_out
    self.user_connected = nil
    self.admin_connected = nil
    reset_session
  end

  def count_rec_pass_attempt
    if session[:rec_pass_attempt].nil?
      session[:rec_pass_attempt] = 1
    else
      session[:rec_pass_attempt] += 1
    end
  end

  def num_rec_pass_attemps
    session[:rec_pass_attempt]
  end

  def clean_rec_pass_attempt
    session.delete(:rec_pass_attempt)
  end

  def count_login_attempt
    if session[:login_attempt].nil?
      session[:login_attempt] = 1
    else
      session[:login_attempt] += 1
    end
  end

  def num_login_attempts
    session[:login_attempt]
  end

  def clean_login_attempt
    session.delete(:login_attempt)
  end

  def prepare_data

    $ct_modules = Hash.new
    CtModule.all.each do |ct_module|
      $ct_modules[ct_module.cod] = ct_module.active
    end
  end

  def ct_module_user_menu_alias(cod)

    session[cod + '_user_menu_alias']

  end

  def ct_menus_cod
    session['ct_menus_cod']
  end

  def ct_menus_user_menu_alias
    session['ct_menus_user_menu_alias']
  end

  def create_jm_session(candidate)
    company = @company
    session[:candidate_connected_id] = candidate.id
    if company.locale == 'es_PE'
      session[:user_connected_locale_separator] = '.'
      session[:user_connected_locale_delimiter] = ','
    elsif company.locale == 'es_CL'
      session[:user_connected_locale_separator] = ','
      session[:user_connected_locale_delimiter] = '.'
    end
  end

  def time_session_in_minutes
    session[:time_session_in_minutes]
  end

  def integrate_session_user_job_market
    if session[:user_connected_id] #&& (session[:candidate_connected_id].nil? ? true : JmCandidate.find(session[:candidate_connected_id]).user_id == session[:user_connected_id])
      if !JmCandidate.find_by_user_id(user_connected.id).nil?
        candidate = JmCandidate.find_by_user_id(user_connected.id)
        create_jm_session(candidate)
        #flash.now[:success] = 'Autenticado'
      else

        new_candidate = JmCandidate.new()
        new_candidate.email = ''
        new_candidate.name = user_connected.nombre.upcase
        new_candidate.surname = user_connected.apellidos.upcase
        new_candidate.password = SecureRandom.hex(10).to_s
        new_candidate.user_id = user_connected.id
        new_candidate.internal_user = true

        if new_candidate.save
          create_jm_session(new_candidate)
          #flash.now[:success] = 'Autenticado.'
        else
          flash.now[:danger] = 'Hubo un error al crear el usuario, favor intente nuevamente.'
        end
      end
    end
  end

  def lms_add_users_to_enroll_list(users_array)
    session['lms_list_users_to_enroll'] = Array.new unless session['lms_list_users_to_enroll']
    added_users = []

    users_array.each do |user_id|
      session['lms_list_users_to_enroll'].push(user_id) unless session['lms_list_users_to_enroll'].include?(user_id)
      added_users.push(user_id)
    end
    return added_users
  end

  def lms_list_users_to_enroll
    if session['lms_list_users_to_enroll'] && session['lms_list_users_to_enroll'].count > 0
      session['lms_list_users_to_enroll']
    end
  end

  def lms_remove_user_from_enroll_list(user_id)
    session['lms_list_users_to_enroll'].delete(user_id)
  end

  def lms_add_courses_to_enroll_list(courses_array)
    session['lms_list_courses_to_enroll'] = {} unless session['lms_list_courses_to_enroll']
    added_courses = []

    courses_array.each do |program_course_id|
      session['lms_list_courses_to_enroll'][program_course_id] = {} unless session['lms_list_courses_to_enroll'].key?(program_course_id)
      added_courses.push(program_course_id)
    end
    return added_courses
  end

  def lms_list_courses_to_enroll
    if session['lms_list_courses_to_enroll'] && session['lms_list_courses_to_enroll'].count > 0
      session['lms_list_courses_to_enroll'].keys
    end

  end

  def lms_remove_course_from_enroll_list(program_course_id)
    session['lms_list_courses_to_enroll'].delete(program_course_id)
  end

  def lms_add_dates_to_course(program_course_id, desdehasta, dates_by_element, desdehasta_elements)

    #session['lms_date_courses_to_enroll'] = {} unless session['lms_date_courses_to_enroll']
    #session['lms_date_courses_to_enroll'][program_course_id] = {} unless session['lms_date_courses_to_enroll'][program_course_id]

    session['lms_list_courses_to_enroll'][program_course_id]['desdehasta'] = desdehasta
    session['lms_list_courses_to_enroll'][program_course_id]['dates_by_element'] = dates_by_element
    session['lms_list_courses_to_enroll'][program_course_id]['desdehasta_elements'] = desdehasta_elements
  end

  def lms_detail_courses_to_enroll
    session['lms_list_courses_to_enroll']
  end

  def lms_remove_enrollment_data
    session.delete('lms_list_courses_to_enroll')
    session.delete('lms_list_users_to_enroll')
  end


  def lms_add_users_to_enroll_from_instances_list(users_array, instance_id)
    session['lms_list_users_to_enroll_' + instance_id.to_s] = Array.new unless session['lms_list_users_to_enroll_' + instance_id.to_s]
    added_users = []

    users_array.each do |user_id|
      session['lms_list_users_to_enroll_' + instance_id.to_s].push(user_id) unless session['lms_list_users_to_enroll_' + instance_id.to_s].include?(user_id)
      added_users.push(user_id)
    end
    return added_users
  end

  def lms_list_users_to_enroll_from_instances(instance_id)
    if session['lms_list_users_to_enroll_' + instance_id.to_s] && session['lms_list_users_to_enroll_' + instance_id.to_s].count > 0
      session['lms_list_users_to_enroll_' + instance_id.to_s]
    end
  end

  def lms_remove_user_from_enroll_from_instances_list(user_id, instance_id)
    session['lms_list_users_to_enroll_' + instance_id.to_s].delete(user_id)
  end

  def lms_remove_enrollment_from_instances_data(instance_id)
    session.delete('lms_list_users_to_enroll_' + instance_id.to_s)
  end

end
