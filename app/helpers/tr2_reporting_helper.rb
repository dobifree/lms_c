module Tr2ReportingHelper

  def get_user_characterisctic_by_name(user, name)

    c = Characteristic.where('nombre = ?', name).first

    if c
      uc = UserCharacteristic.where('user_id = ? AND characteristic_id = ?',user.id, c.id).first
      if uc
        return uc.valor
      end
    end

  end

  def get_user_uc_characterisctic_by_name(user_course, name)

    uc = UcCharacteristic.where('name = ?', name).first

    if uc
      uc_ucc = user_course.user_course_uc_characteristics.where('uc_characteristic_id = ? ', uc.id).first
      if uc_ucc
        return uc_ucc.value
      end
    end

  end

  def letras_excel

    letras = %w(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI AJ AK AL AM AN AO AP AQ AR AS AT AU AV AW AX AY AZ BA BB BC BD BE BF BG BH BI BJ BK BL BM BN BO BP BQ BR BS BT BU BV BW BX BY BZ CA CB CC CD CE CF CG CH CI CJ CK CL CM CN CO CP CQ CR CS CT CU CV CW CX CY CZ)

  end

  def xls_rep_fact(program_course_instance)

    letras = letras_excel

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      currency = s.add_style(:sz => 9,
                             :format_code=>"$ #,##0;[Red]$ -#,##0",
                                     :border=>{ :style => :thin, color: '00' })

      currency_bold = s.add_style(:b => true,
                                  :sz => 9,
                                  :format_code=>"$ #,##0;[Red]$ -#,##0",
                             :border=>{ :style => :thin, color: '00' })

      wb.add_worksheet(:name => 'Reporte Facturación') do |reporte_excel|

        num_inscritos_totales = 0
        num_inscritos_con_asistencia = 0
        num_inscritos_sin_asistencias = 0
        num_inscritos_con_asistencia_mayor_0 = 0
        num_no_inscritos = 0

        @program_course_instance.user_courses.each_with_index do |uc, index|

          if get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').blank?
            num_no_inscritos+=1
          else
            num_inscritos_totales+=1
          end
        end

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Curso'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.program_course.course.nombre
        filaHeader << headerTitleInfoDet

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Código Sence'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.program_course.course.sence_code
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Fecha inicio'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << localize(program_course_instance.from_date, format: :day_month_year_guion)
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Fecha término'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << localize(program_course_instance.to_date, format: :day_month_year_guion)
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Duracion'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.sence_hours
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Número participantes inscritos'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << num_inscritos_totales
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Valor hora Sence'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.sence_hour_value
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Valor hora venta'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.sale_hour_value
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Lugar realización'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.place
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'N° Orden de compra OTIC'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.purchase_order_number
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Cliente'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.program_instance.sk_client
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'Proyecto/Programa'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << program_course_instance.program_course.program.nombre
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new

        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << 'No inscritos'
        filaHeader << headerTitleInfo
        fila << ''
        filaHeader << headerTitleInfo
        fila << num_no_inscritos
        filaHeader << headerTitleInfoDet
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new
        fila << ''
        filaHeader << headerTitleInfo
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        fila = Array.new
        filaHeader = Array.new
        fila << ''
        filaHeader << headerTitleInfo
        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.merge_cells('A1:C13')

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'NOMBRES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO PATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO MATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PORCENTAJE DE FRANQUICIA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PORCENTAJE DE ASISTENCIA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CONDICIÓN (INSCRITO / NO INSCRITO)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RESULTADO NOTA FINAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'SITUACIÓN FINAL (APROBADO/PARTICIPADO)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FACTURACIÓN VENTA SENCE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FACTURACIÓN VENTA CLIENTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FACTURACIÓN VENTA TOTAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 40

        sub_total_fact_sense = 0
        sub_total_fact_cliente = 0
        total_venta = 0

        @program_course_instance.user_courses.each_with_index do |uc, index|

          fila = Array.new
          filaHeader = Array.new
          cols_widths = Array.new

          fila << uc.user.codigo
          filaHeader <<  fieldLeft
          cols_widths << 20

          fila << uc.user.nombre
          filaHeader <<  fieldLeft
          cols_widths << 20

          fila << get_user_characterisctic_by_name(uc.user, 'APELLIDO PATERNO')
          filaHeader <<  fieldLeft
          cols_widths << 20

          fila << get_user_characterisctic_by_name(uc.user, 'APELLIDO MATERNO')
          filaHeader <<  fieldLeft
          cols_widths << 20

          fila << get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA')
          filaHeader <<  fieldLeft
          cols_widths << 20

          fila << uc.porcentaje_avance.to_s+'%'
          filaHeader <<  fieldLeft
          cols_widths << 20

          if get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').blank?
            fila << 'NO INSCRITO'
            estado_inscrito = 'no_inscrito'
          else
            fila << 'INSCRITO'

            inscrito = 'inscrito_asistio'
            inscrito = 'inscrito_no_asistio' if uc.porcentaje_avance < 75

            num_inscritos_con_asistencia_mayor_0 += 1 if uc.porcentaje_avance > 0

          end

          if uc.porcentaje_avance < 75
            no_cobrar_no_asistio_75 = true
          end

          filaHeader <<  fieldLeft
          cols_widths << 20

          fila << uc.nota.to_s+'%'
          filaHeader <<  fieldLeft
          cols_widths << 20

          if uc.aprobado
            fila << 'APROBADO'
          else
            fila << 'REPROBADO'
          end
          filaHeader <<  fieldLeft
          cols_widths << 20

          if inscrito == 'inscrito_asistio'

            sence_hour_value = @program_course_instance.sence_hour_value
            sence_hour_value = sence_hour_value*1.2 if @program_course_instance.sence_cbipartito == 'SI'

            sence_hours = @program_course_instance.sence_hours

            vf = sence_hour_value*sence_hours
            vf = vf*get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').to_f/100 unless get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').blank?

            sub_total_fact_sense += vf

            fila << vf
            filaHeader <<  currency
            cols_widths << 20

            sale_hour_value = program_course_instance.sale_hour_value.to_f
            venta_total = program_course_instance.sence_hours*sale_hour_value

            vc = venta_total - vf

            sub_total_fact_cliente += vc

            fila << vc
            filaHeader <<  currency
            cols_widths << 20

            fila << venta_total
            filaHeader <<  currency
            cols_widths << 20

            total_venta += venta_total

          elsif inscrito == 'inscrito_no_asistio' || inscrito == 'no_inscrito'

            fila << 0
            filaHeader <<  currency
            cols_widths << 20

            sale_hour_value = @program_course_instance.sale_hour_value.to_f
            sence_hours = @program_course_instance.sence_hours.to_f
            vc = sale_hour_value*sence_hours

            vc = 0 if inscrito == 'inscrito_no_asistio' && uc.porcentaje_avance == 0

            vc = 0 if no_cobrar_no_asistio_75

            fila << vc
            filaHeader <<  currency
            cols_widths << 20

            sub_total_fact_cliente += vc

            fila << vc
            filaHeader <<  currency
            cols_widths << 20

            total_venta += vc
          end

          reporte_excel.add_row fila, :style => filaHeader, height: 20

          reporte_excel.column_info.each_with_index do |col, index|
            col.width = cols_widths[index]
          end

        end

        fila = Array.new
        filaHeader = Array.new

        fila << 'Observaciones'
        filaHeader <<  headerCenter

        fila << 'Número mínimo de participantes'
        filaHeader <<  headerCenter

        fila << @program_course_instance.sence_minimum_students_number
        filaHeader <<  headerCenter

        (1..5).each do |col|
          fila << ''
          filaHeader <<  headerCenter
        end

        fila << 'SUBTOTAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << sub_total_fact_sense
        filaHeader <<  currency_bold
        cols_widths << 20

        fila << sub_total_fact_cliente
        filaHeader <<  currency_bold
        cols_widths << 20

        fila << total_venta
        filaHeader <<  currency_bold
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

######

        fila = Array.new
        filaHeader = Array.new

        (1..8).each do |col|
          fila << ''
          filaHeader <<  headerCenter
        end

        fila << 'COBRO POR MÍNIMO DE PARTICIPANTES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << ''
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << ''
        filaHeader <<  headerCenter
        cols_widths << 20

        cob_por_min_p = (@program_course_instance.sence_minimum_students_number-num_inscritos_con_asistencia_mayor_0)*@program_course_instance.sence_hours*@program_course_instance.sale_hour_value

        cob_por_min_p = 0 if cob_por_min_p < 0

        fila << cob_por_min_p
        filaHeader <<  currency_bold
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        ######

        fila = Array.new
        filaHeader = Array.new

        (1..8).each do |col|
          fila << ''
          filaHeader <<  headerCenter
        end

        fila << 'TOTAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << ''
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << ''
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << total_venta+cob_por_min_p
        filaHeader <<  currency_bold
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

      end

    end

    return reporte

  end


  def xls_rep_carga_otic(program_course_instance)

    letras = letras_excel

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      wb.add_worksheet(:name => 'Reporte Carga OTIC') do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'CODIGO SENCE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CURSO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'HORAS'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'OTEC'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DIRECCION OTEC'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FONO OTEC'
        filaHeader <<  headerCenter
        cols_widths << 20
        fila << 'FECHA INICIO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FECHA TERMINO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'REGION'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CBIPARTITO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VALOR PARTICIPANTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'LUGAR'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'TIPO CONTRATO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DNC'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'COMENTARIO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'HORARIOS'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RUT'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DIGITO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'NOMBRES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO PATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO MATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FECHA NACIMIENTO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'SEXO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'COMUNA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CODIGO ESCOLARIDAD'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CODIGO NIVEL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FRANQUICIA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VIATICO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'MOVILIZACION'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'EMAIL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'SEGURO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RUT CIA SEGURO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'MONTO SEGURO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'POLIZA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RUT EMPRESA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'SUCURSAL EMPRESA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'NACIONALIDAD'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DIRECCION PARTICULAR'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CFT'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

        @program_course_instance.user_courses.each_with_index do |uc, index|

          fila = Array.new
          filaHeader = Array.new

          fila << @program_course_instance.program_course.course.sence_code
          filaHeader <<  fieldLeft

          fila << @program_course_instance.program_course.course.nombre
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_hours
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_otec_name
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_otec_address
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_otec_telephone
          filaHeader <<  fieldLeft

          fila << localize(program_course_instance.from_date, format: :day_month_year_guion)
          filaHeader <<  fieldLeft

          fila << localize(program_course_instance.to_date, format: :day_month_year_guion)
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_region
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_cbipartito
          filaHeader <<  fieldLeft

          fila << (@program_course_instance.sence_hours*@program_course_instance.sale_hour_value)
          filaHeader <<  fieldLeft

          fila << @program_course_instance.place
          filaHeader <<  fieldLeft

          #fila << @program_course_instance.contract_type
          fila << get_user_characterisctic_by_name(uc.user, 'TIPO CONTRATO')
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_dnc
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_comment
          filaHeader <<  fieldLeft

          fila << @program_course_instance.sence_schedule
          filaHeader <<  fieldLeft

          fila << uc.user.codigo
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'DÍGITO')
          filaHeader <<  fieldLeft

          fila << uc.user.nombre
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'APELLIDO PATERNO')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'APELLIDO MATERNO')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'FECHA NACIMIENTO')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'SEXO')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'COMUNA')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'CÓDIGO ESCOLARIDAD')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'CÓDIGO NIVEL')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'VIATICO')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'MOVILIZACIÓN')
          filaHeader <<  fieldLeft

          fila << uc.user.email
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'SEGURO')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'RUT CIA SEGURO')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'MONTO SEGURO')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'POLIZA')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'RUT EMPRESA')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'SUCURSAL EMPRESA')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'NACIONALIDAD')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(uc.user, 'DIRECCIÓN PARTICULAR')
          filaHeader <<  fieldLeft

          fila << get_user_uc_characterisctic_by_name(uc, 'CFT')
          filaHeader <<  fieldLeft

          reporte_excel.add_row fila, :style => filaHeader, height: 40

        end


      end

    end

    return reporte

  end

  def csv_rep_carga_otic(program_course_instance)

    file_csv_name = SecureRandom.hex(5)+'_sk.csv'

    CSV.open('/tmp/'+file_csv_name, 'wb', {:col_sep => "\t", :encoding=> 'UTF-16LE'}) do |csv|

      columns = Array.new
      columns.push "\uFEFFCODIGO SENCE"
      columns.push 'CURSO'
      columns.push 'HORAS'
      columns.push 'OTEC'
      columns.push 'DIRECCION OTEC'
      columns.push 'FONO OTEC'
      columns.push 'FECHA INICIO'
      columns.push 'FECHA TERMINO'
      columns.push 'REGION'
      columns.push 'CBIPARTITO'
      columns.push 'VALOR PARTICIPANTE'
      columns.push 'LUGAR'
      columns.push 'TIPO CONTRATO'
      columns.push 'DNC'
      columns.push 'COMENTARIO'
      columns.push 'HORARIOS'
      columns.push 'RUT'
      columns.push 'DIGITO'
      columns.push 'NOMBRES'
      columns.push 'APELLIDO PATERNO'
      columns.push 'APELLIDO MATERNO'
      columns.push 'FECHA NACIMIENTO'
      columns.push 'SEXO'
      columns.push 'COMUNA'
      columns.push 'CODIGO ESCOLARIDAD'
      columns.push 'CODIGO NIVEL'
      columns.push 'FRANQUICIA'
      columns.push 'VIATICO'
      columns.push 'MOVILIZACION'
      columns.push 'EMAIL'
      columns.push 'SEGURO'
      columns.push 'RUT CIA SEGURO'
      columns.push 'MONTO SEGURO'
      columns.push 'POLIZA'
      columns.push 'RUT EMPRESA'
      columns.push 'SUCURSAL EMPRESA'
      columns.push 'NACIONALIDAD'
      columns.push 'DIRECCION PARTICULAR'
      columns.push 'CFT'

      csv << columns



      @program_course_instance.user_courses.each_with_index do |uc, index|

        columns = Array.new

        columns.push @program_course_instance.program_course.course.sence_code
        columns.push @program_course_instance.program_course.course.nombre
        columns.push @program_course_instance.sence_hours
        columns.push @program_course_instance.sence_otec_name
        columns.push @program_course_instance.sence_otec_address
        columns.push @program_course_instance.sence_otec_telephone
        columns.push localize(program_course_instance.from_date, format: :day_month_year_guion)
        columns.push localize(program_course_instance.to_date, format: :day_month_year_guion)
        columns.push @program_course_instance.sence_region
        columns.push @program_course_instance.sence_cbipartito
        columns.push (@program_course_instance.sence_hours*@program_course_instance.sale_hour_value).to_i
        columns.push @program_course_instance.place
        #columns.push @program_course_instance.contract_type
        columns.push get_user_characterisctic_by_name(uc.user, 'TIPO CONTRATO')
        columns.push @program_course_instance.sence_dnc
        columns.push @program_course_instance.sence_comment
        columns.push @program_course_instance.sence_schedule
        columns.push uc.user.codigo
        columns.push get_user_characterisctic_by_name(uc.user, 'DÍGITO')
        columns.push uc.user.nombre
        columns.push get_user_characterisctic_by_name(uc.user, 'APELLIDO PATERNO')
        columns.push get_user_characterisctic_by_name(uc.user, 'APELLIDO MATERNO')
        columns.push get_user_characterisctic_by_name(uc.user, 'FECHA NACIMIENTO')
        columns.push get_user_characterisctic_by_name(uc.user, 'SEXO')
        columns.push get_user_characterisctic_by_name(uc.user, 'COMUNA')
        columns.push get_user_characterisctic_by_name(uc.user, 'CÓDIGO ESCOLARIDAD')
        columns.push get_user_characterisctic_by_name(uc.user, 'CÓDIGO NIVEL')
        columns.push get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA')
        columns.push get_user_uc_characterisctic_by_name(uc, 'VIATICO')
        columns.push get_user_uc_characterisctic_by_name(uc, 'MOVILIZACIÓN')
        columns.push uc.user.email
        columns.push get_user_uc_characterisctic_by_name(uc, 'SEGURO')
        columns.push get_user_uc_characterisctic_by_name(uc, 'RUT CIA SEGURO')
        columns.push get_user_uc_characterisctic_by_name(uc, 'MONTO SEGURO')
        columns.push get_user_uc_characterisctic_by_name(uc, 'POLIZA')
        columns.push get_user_uc_characterisctic_by_name(uc, 'RUT EMPRESA')
        columns.push get_user_uc_characterisctic_by_name(uc, 'SUCURSAL EMPRESA')
        columns.push get_user_characterisctic_by_name(uc.user, 'NACIONALIDAD')
        columns.push get_user_characterisctic_by_name(uc.user, 'DIRECCIÓN PARTICULAR')
        columns.push get_user_uc_characterisctic_by_name(uc, 'CFT')

        csv << columns

      end

    end

    return '/tmp/'+file_csv_name

  end

  def xls_rep_consolidado_global(program_instance)

    h2_num_participantes_inscritos = 0

    h2_lista_participantes_inscritos = Array.new

    h2_num_asistencia_programada = 0
    h2_num_oyentes = 0
    h2_num_asistencia_real = 0
    h2_num_participantes_capacitados = 0

    h2_venta_programada = 0
    h2_venta_sence = 0
    h2_venta_cliente = 0
    h2_venta_total = 0

    h2_horas_capacitacion_programada = 0
    h2_horas_asistidas = 0

    h2_duracion_total_de_cursos = 0

    h2_num_inscritos_con_asistencia_mayor_0 = 0
    h2_cobro_minimo_por_pariticipantes = 0

    h2_place = ''

    letras = letras_excel

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      currency = s.add_style(:sz => 9,
                             :format_code=>"$ #,##0;[Red]$ -#,##0 ",
                             :border=>{ :style => :thin, color: '00' })

      wb.add_worksheet(:name => 'Reporte Consolidado Global') do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'PROGRAMA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CURSO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CODIGO SENCE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'SENCENET'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'ORDEN DE COMPRA OTIC'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DURACIÓN'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FECHA INICIO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FECHA TERMINO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'MES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CLIENTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RUT CLIENTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'OBRA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'REGIÓN'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CIUDAD'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'COMITÉ BIPARTITO (SI = 20% ADICIONAL SOBRE VALOR HORA SENCE)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'LUGAR(ES) DE REALIZACIÓN'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RUT PARTICIPANTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DV'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'NOMBRES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO PATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO MATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FECHA DE NACIMIENTO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FRANQUICIA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VALOR HORA SENCE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VALOR HORA VENTA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CÓDIGO DE ESCOLARIDAD'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CÓDIGO DE NIVEL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'ASISTENCIA REAL EN %'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PARTICIPANTES CON FRANQUICIA 100%'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PARTICIPANTES CON FRANQUICIA 50%'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PARTICIPANTES CON FRANQUICIA 15%'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PARTICIPANTES SIN FRANQUICIA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RESULTADO NOTA FINAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'SITUACIÓN FINAL (APROBADO/PARTICIPADO)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'TOTAL HORAS DE CAPACITACIÓN(ASISTIDAS)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA SENCE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA CLIENTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA TOTAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA PROGRAMADA SENCE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA PROGRAMADA CLIENTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA PROGRAMADA TOTAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FINANCIAMIENTO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CERTIFICACIÓN EXTERNA SI/NO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PROYECTO (CLASIFICACIÓN INTERNA)'
        filaHeader <<  headerCenter
        cols_widths << 20



        reporte_excel.add_row fila, :style => filaHeader, height: 40

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

        month_names = %w(Enero Febrero Marzo Abril Mayo Junio Julio Agosto Septiembre Octubre Noviembre Diciembre)

        program_instance.program_course_instances.each_with_index do |program_course_instance, num_course|

          if num_course == 0

            h2_place += program_course_instance.place

          else

            h2_place += ' | '+program_course_instance.place

          end

          h2_duracion_total_de_cursos += program_course_instance.sence_hours

          program_course_instance.user_courses.each_with_index do |uc, index|

            fila = Array.new
            filaHeader = Array.new

            fila << program_instance.name
            filaHeader <<  fieldLeft

            fila << program_course_instance.program_course.course.nombre
            filaHeader <<  fieldLeft

            fila << program_course_instance.program_course.course.sence_code
            filaHeader <<  fieldLeft

            fila << program_course_instance.sencenet
            filaHeader <<  fieldLeft

            fila << program_course_instance.purchase_order_number
            filaHeader <<  fieldLeft



            fila << program_course_instance.sence_hours
            filaHeader <<  fieldLeft

            fila << localize(program_course_instance.from_date, format: :day_month_year_guion)
            filaHeader <<  fieldLeft

            fila << localize(program_course_instance.to_date, format: :day_month_year_guion)
            filaHeader <<  fieldLeft

            fila << month_names[program_course_instance.to_date.strftime('%m').to_i-1]
            filaHeader <<  fieldLeft

            fila << program_instance.sk_client
            filaHeader <<  fieldLeft

            fila << program_instance.sk_rut_client
            filaHeader <<  fieldLeft

            fila << program_course_instance.obra
            filaHeader <<  fieldLeft

            fila << program_course_instance.sence_region
            filaHeader <<  fieldLeft

            fila << program_course_instance.city
            filaHeader <<  fieldLeft

            fila << program_course_instance.sence_cbipartito
            filaHeader <<  fieldLeft

            fila << program_course_instance.place
            filaHeader <<  fieldLeft


            fila << uc.user.codigo
            filaHeader <<  fieldLeft

            fila << get_user_characterisctic_by_name(uc.user, 'DÍGITO')
            filaHeader <<  fieldLeft

            fila << uc.user.nombre
            filaHeader <<  fieldLeft

            fila << get_user_characterisctic_by_name(uc.user, 'APELLIDO PATERNO')
            filaHeader <<  fieldLeft

            fila << get_user_characterisctic_by_name(uc.user, 'APELLIDO MATERNO')
            filaHeader <<  fieldLeft

            fila << get_user_characterisctic_by_name(uc.user, 'FECHA NACIMIENTO')
            filaHeader <<  fieldLeft



            fila << get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA')
            filaHeader <<  fieldLeft

            fila << program_course_instance.sence_hour_value
            filaHeader <<  currency

            fila << program_course_instance.sale_hour_value
            filaHeader <<  currency

            fila << get_user_characterisctic_by_name(uc.user, 'CÓDIGO ESCOLARIDAD')
            filaHeader <<  fieldLeft

            fila << get_user_characterisctic_by_name(uc.user, 'CÓDIGO NIVEL')
            filaHeader <<  fieldLeft

            fila << uc.porcentaje_avance
            filaHeader <<  fieldLeft


            tmp = get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA') == '100' ? '1' : 0
            fila << tmp
            filaHeader <<  fieldLeft

            tmp = get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA') == '50' ? '1' : 0
            fila << tmp
            filaHeader <<  fieldLeft

            tmp = get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA') == '15' ? '1' : 0
            fila << tmp
            filaHeader <<  fieldLeft

            tmp = get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').blank? ? '1' : 0
            fila << tmp
            filaHeader <<  fieldLeft

            fila << uc.nota
            filaHeader <<  fieldLeft

            ap = uc.aprobado ? 'APROBADO' : 'PARTICIPADO'

            fila << ap
            filaHeader <<  fieldLeft

            num_horas_avance = program_course_instance.sence_hours
            num_horas_avance = 0 if uc.porcentaje_avance < 75

            if uc.porcentaje_avance < 75

              no_asistio_75_no_cobrar = true

            end

            fila << num_horas_avance
            filaHeader <<  fieldLeft



            unless h2_lista_participantes_inscritos.include? uc.enrollment.user.codigo

              h2_num_participantes_inscritos += 1

              h2_lista_participantes_inscritos.push uc.enrollment.user.codigo

            end

            h2_horas_capacitacion_programada += program_course_instance.sence_hours

            if get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').blank?

              estado_inscrito = 'no_inscrito'

              h2_num_oyentes += 1

              if uc.porcentaje_avance >= 75
                h2_num_asistencia_real += 1
                #h2_horas_asistidas += program_course_instance.sence_hours*uc.porcentaje_avance
              end

            else
              inscrito = 'inscrito_asistio'
              inscrito = 'inscrito_no_asistio' if uc.porcentaje_avance < 75

              h2_num_asistencia_programada += 1

              if uc.porcentaje_avance >= 75
                h2_num_asistencia_real += 1
                h2_num_participantes_capacitados += 1
                h2_horas_asistidas += program_course_instance.sence_hours #*uc.porcentaje_avance, piden que no se multiplique por el porcentaje

              end

              h2_num_inscritos_con_asistencia_mayor_0 += 1 if uc.porcentaje_avance > 0

            end

            h2_venta_programada += program_course_instance.sale_hour_value.to_f*program_course_instance.sence_hours

            if inscrito == 'inscrito_asistio'

              sence_hour_value = program_course_instance.sence_hour_value.to_f

              sence_hour_value = sence_hour_value*1.2 if program_course_instance.sence_cbipartito == 'SI'

              sence_hours = program_course_instance.sence_hours

              vf = sence_hour_value*sence_hours

              vf = vf*get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').to_f/100 unless get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').blank?

              fila << vf
              filaHeader <<  currency

              h2_venta_sence += vf

              sale_hour_value = program_course_instance.sale_hour_value.to_f
              venta_total = program_course_instance.sence_hours*sale_hour_value

              vc = venta_total - vf

              fila << vc
              filaHeader <<  currency

              h2_venta_cliente += vc

              fila << venta_total
              filaHeader <<  currency

              h2_venta_total += venta_total

            elsif inscrito == 'inscrito_no_asistio' || inscrito == 'no_inscrito'

              fila << 0
              filaHeader <<  currency

              sale_hour_value = program_course_instance.sale_hour_value.to_f
              sence_hours = program_course_instance.sence_hours.to_f
              vc = sale_hour_value*sence_hours

              vc = 0 if no_asistio_75_no_cobrar

              fila << vc
              filaHeader <<  currency

              h2_venta_cliente += vc

              fila << vc
              filaHeader <<  currency

              h2_venta_total += vc

            end

            if inscrito == 'inscrito_asistio' || inscrito == 'inscrito_no_asistio'

              sence_hour_value = program_course_instance.sence_hour_value.to_f

              sence_hour_value = sence_hour_value*1.2 if program_course_instance.sence_cbipartito == 'SI'

              sence_hours = program_course_instance.sence_hours

              vf = sence_hour_value*sence_hours

              vf = vf*get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').to_f/100 unless get_user_uc_characterisctic_by_name(uc, 'FRANQUICIA').blank?

              fila << vf
              filaHeader <<  currency



              sale_hour_value = program_course_instance.sale_hour_value.to_f
              venta_total = program_course_instance.sence_hours*sale_hour_value

              vc = venta_total - vf

              fila << vc
              filaHeader <<  currency

              fila << venta_total
              filaHeader <<  currency

            elsif inscrito == 'no_inscrito'

              fila << 0
              filaHeader <<  currency

              sale_hour_value = program_course_instance.sale_hour_value.to_f
              sence_hours = program_course_instance.sence_hours.to_f
              vc = sale_hour_value*sence_hours

              fila << vc
              filaHeader <<  currency

              fila << vc
              filaHeader <<  currency



            end

            fila << program_course_instance.sk_financing
            filaHeader <<  fieldLeft

            ec = program_course_instance.sk_external_certification ? 'SI' : 'NO'

            fila << ec
            filaHeader <<  fieldLeft

            fila << program_course_instance.proyecto_ci
            filaHeader <<  fieldLeft



            reporte_excel.add_row fila, :style => filaHeader, height: 20

          end

          h2_cobro_minimo_por_pariticipantes += (program_course_instance.sence_minimum_students_number-h2_num_inscritos_con_asistencia_mayor_0)*program_course_instance.sence_hours*program_course_instance.sale_hour_value

          h2_venta_total += h2_cobro_minimo_por_pariticipantes

          h2_num_inscritos_con_asistencia_mayor_0 = 0

        end



      end

      wb.add_worksheet(:name => 'Producción') do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'PROGRAMA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CANTIDAD DE CURSOS'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'ALIAS (CÓDIGO O CORRELATIVO)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DURACIÓN'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FECHA DE INICIO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FECHA DE TÉRMINO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'MES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CLIENTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RUT CLIENTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'OBRA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'REGIÓN'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CIUDAD'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'COMITÉ BIPARTITO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'LUGAR DE REALIZACIÓN'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PARTICIPANTES INSCRITOS'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA PROGRAMADA TOTAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'ASISTENCIA REAL (NÚMERO DE PARTICIPANTES)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'OYENTES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'TOTAL HORAS DE CAPACITACIÓN PROGRAMADAS PROGRAMA/CURSO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'TOTAL DE HORAS ASISTIDAS'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA SENCE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA CLIENTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA POR MINIMO PARTICIPANTES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'VENTA TOTAL'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'CANTIDAD DE PARTICIPANTES CAPACITADOS'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'FINANCIAMIENTO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'PROYECTO (CLASIFICACIÓN INTERNA)'
        filaHeader <<  headerCenter
        cols_widths << 20


        reporte_excel.add_row fila, :style => filaHeader, height: 40

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

        fila = Array.new
        filaHeader = Array.new

        fila << program_instance.name
        filaHeader <<  fieldLeft

        fila << program_instance.program_course_instances.size
        filaHeader <<  fieldLeft

        fila << ''
        filaHeader <<  fieldLeft

        fila << h2_duracion_total_de_cursos
        filaHeader <<  fieldLeft

        fila << localize(program_instance.from_date, format: :day_month_year)
        filaHeader <<  fieldLeft

        fila << localize(program_instance.to_date, format: :day_month_year)
        filaHeader <<  fieldLeft

        meses = t('date.month_names')

        fila << meses[program_instance.to_date.to_s.split('-')[1].to_i].upcase
        filaHeader <<  fieldLeft

        fila << program_instance.sk_client
        filaHeader <<  fieldLeft

        fila << program_instance.sk_rut_client
        filaHeader <<  fieldLeft

        program_course_instance = program_instance.program_course_instances.first

        fila << program_course_instance.obra
        filaHeader <<  fieldLeft

        fila << program_course_instance.sence_region
        filaHeader <<  fieldLeft

        fila << program_course_instance.city
        filaHeader <<  fieldLeft

        fila << program_course_instance.sence_cbipartito
        filaHeader <<  fieldLeft




        fila << h2_place
        filaHeader <<  fieldLeft

        fila << h2_num_participantes_inscritos
        filaHeader <<  fieldLeft

        fila << h2_venta_programada
        filaHeader <<  currency

        fila << h2_num_asistencia_real/program_instance.program_course_instances.size
        filaHeader <<  fieldLeft

        fila << h2_num_oyentes
        filaHeader <<  fieldLeft

        fila << h2_horas_capacitacion_programada
        filaHeader <<  fieldLeft

        fila << h2_horas_asistidas
        filaHeader <<  fieldLeft

        fila << h2_venta_sence
        filaHeader <<  currency

        fila << h2_venta_cliente
        filaHeader <<  currency

        fila << h2_cobro_minimo_por_pariticipantes
        filaHeader <<  currency

        fila << h2_venta_total
        filaHeader <<  currency

        fila << h2_num_participantes_capacitados
        filaHeader <<  fieldLeft

        fila << program_course_instance.sk_financing
        filaHeader <<  fieldLeft

        fila << program_course_instance.proyecto_ci
        filaHeader <<  fieldLeft


        reporte_excel.add_row fila, :style => filaHeader, height: 20



      end

    end

    return reporte

  end


  def xls_rep_resultados_finales(program_instance)



    letras = letras_excel

    cols_widths = Array.new

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|

      headerTitle = s.add_style :b => true,
                                :sz => 9,
                                :border => { :style => :thin, color: '00' },
                                :alignment => { :horizontal => :left, vertical: :center, :wrap_text => true}

      headerTitleInfo = s.add_style :b => true,
                                    :sz => 9,
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      headerTitleInfoDet = s.add_style :b => false,
                                       :sz => 9,
                                       :alignment => { :horizontal => :left, :wrap_text => true}

      headerLeft = s.add_style :b => true,
                               :sz => 9,
                               :border => { :style => :thin, color: '00' },
                               :alignment => { :horizontal => :left, :wrap_text => true}

      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      fieldLeft = s.add_style :sz => 9,
                              :border => { :style => :thin, color: '00' },
                              :alignment => { :horizontal => :left, :wrap_text => true}


      fieldLeftAprob = s.add_style fg_color: '0000ff',
                                   :sz => 9,
                                   :border => { :style => :thin, color: '00' },
                                   :alignment => { :horizontal => :left, :wrap_text => true}

      fieldLeftReprob = s.add_style fg_color: 'ff0000',
                                    :sz => 9,
                                    :border => { :style => :thin, color: '00' },
                                    :alignment => { :horizontal => :left, :wrap_text => true}

      currency = s.add_style(:sz => 9,
                             :format_code=>"$ #,##0;[Red]$ -#,##0 ",
                             :border=>{ :style => :thin, color: '00' })

      wb.add_worksheet(:name => 'Reporte Resultados Finales') do |reporte_excel|

        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << ''
        filaHeader <<  headerTitleInfo
        cols_widths << 20

        fila << ''
        filaHeader <<  headerTitleInfo
        cols_widths << 20

        fila << ''
        filaHeader <<  headerTitleInfo
        cols_widths << 20

        fila << ''
        filaHeader <<  headerTitleInfo
        cols_widths << 20

        fila << ''
        filaHeader <<  headerTitleInfo
        cols_widths << 20

        fila << ''
        filaHeader <<  headerTitleInfo
        cols_widths << 20

        fila << ''
        filaHeader <<  headerTitleInfo
        cols_widths << 20

        fila << ''
        filaHeader <<  headerTitleInfo
        cols_widths << 20

        num_courses = 0

        program_instance.program_course_instances.each do |program_course_instance|

          fila << program_course_instance.program_course.course.nombre.upcase
          filaHeader <<  headerCenter
          cols_widths << 60

          fila << ''
          filaHeader <<  headerCenter
          cols_widths << 20

          fila << ''
          filaHeader <<  headerCenter
          cols_widths << 20

          num_courses += 1

        end


        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

        (0..num_courses-1).each do |num_course|

          reporte_excel.merge_cells(letras[(num_course*3)+8]+'1:'+letras[(num_course*3)+10]+'1')

        end



        fila_actual = 1

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'PROGRAMA'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'RUT PARTICIPANTE'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'DV'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'NOMBRES'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO PATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'APELLIDO MATERNO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'ESTADO'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << ''
        filaHeader <<  headerCenter
        cols_widths << 20

        program_instance.program_course_instances.each do |program_course_instance|

          fila << 'nota'
          filaHeader <<  headerCenter
          cols_widths << 20

          fila << 'asistencia'
          filaHeader <<  headerCenter
          cols_widths << 20

          fila << 'estado'
          filaHeader <<  headerCenter
          cols_widths << 20


        end


        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end



        program_instance.enrollments_ordered_by_name.each do |enrollment|

          fila = Array.new
          filaHeader = Array.new

          fila << program_instance.name
          filaHeader <<  fieldLeft

          fila << enrollment.user.codigo
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(enrollment.user, 'DÍGITO')
          filaHeader <<  fieldLeft

          fila << enrollment.user.nombre
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(enrollment.user, 'APELLIDO PATERNO')
          filaHeader <<  fieldLeft

          fila << get_user_characterisctic_by_name(enrollment.user, 'APELLIDO MATERNO')
          filaHeader <<  fieldLeft

          p_finalizado = true

          program_instance.program_course_instances.each_with_index do |program_course_instance, num_course|

            uc = program_course_instance.user_courses.where('enrollment_id = ?', enrollment.id).first

            if uc && !uc.finalizado
              p_finalizado = false
            end

          end

          if p_finalizado

            if enrollment.aprobado
              fila << 'APROBADO'
              filaHeader <<  fieldLeft
            else
              fila << 'REPROBADO'
              filaHeader <<  fieldLeft
            end

          else

            fila << ''
            filaHeader <<  fieldLeft

          end

          fila << ''
          filaHeader <<  fieldLeft

          program_instance.program_course_instances.each_with_index do |program_course_instance, num_course|

            uc = program_course_instance.user_courses.where('enrollment_id = ?', enrollment.id).first

            if uc && uc.finalizado
              fila << uc.nota
              filaHeader <<  fieldLeft

              fila << uc.porcentaje_avance.to_s+'%'
              filaHeader <<  fieldLeft


              if uc.aprobado
                fila << 'aprobado'
                filaHeader <<  fieldLeft
              else
                fila << 'reprobado'
                filaHeader <<  fieldLeft
              end

            else
              fila << ''
              filaHeader <<  fieldLeft

              fila << ''
              filaHeader <<  fieldLeft

              fila << ''
              filaHeader <<  fieldLeft
            end


          end

          reporte_excel.add_row fila, :style => filaHeader, height: 20

        end



      end


    end

    return reporte

  end

end
