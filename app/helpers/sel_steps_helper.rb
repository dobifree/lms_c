module SelStepsHelper

  def link_to_remove_stepChars(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_stepChar(this)")
  end

  def link_to_add_stepChars(name, f)
    stepChar = SelStepCharacteristic.new
    fields = f.fields_for(:sel_step_characteristics, stepChar, :child_index => "new_step") do |builder|
      render('sel_steps/selChar_form', :f => builder)
    end
    link_to_function(name, "add_stepChar(this, \"#{escape_javascript(fields)}\")")
  end

end
