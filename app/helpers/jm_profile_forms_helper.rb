module JmProfileFormsHelper

  def link_to_remove_profile_char(name, f)
    f.hidden_field(:_destroy, class: 'profile_char_destroy_field') +
        link_to(name, '#', onclick: "remove_profile_char(this); return false")
  end

  def link_to_add_profile_char(name, f)
    char = JmProfileChar.new()
    fields = f.fields_for(:jm_profile_chars, char, :child_index => "new_profile_char") do |builder|
      render :partial => 'jm_profile_forms/profile_char_form', :locals => {:f_char => builder}
    end
    link_to(name, '#', onclick: "add_profile_char(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
