module BlogFormsHelper

  def icon_blog_module_html_safe
    icon_blog_module.html_safe
  end

  def icon_blog_module
    '<i class="' + icon_blog_module_class + '"></i>'
  end

  def icon_blog_module_class
    'fa fa-list-ol'
  end


  def link_to_remove_blog_form_item(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_item(this); return false")
  end

  def link_to_add_blog_form_item(name, f)
    item = BlogFormItem.new
    fields = f.fields_for(:blog_form_items, item, :child_index => "new_item") do |builder|
      render('blog_forms/items/form', :f => builder)
    end
    link_to(name, '#', onclick: "add_item(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
