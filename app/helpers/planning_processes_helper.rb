module PlanningProcessesHelper

  def can_show_planning_process_menu?

    can = false

    can = user_connected.jefe_capacitacion?

    return can if can

    can = can_show_planning_process_training_analyst_menu?

    return can if can

    can = can_show_planning_process_area_manager_menu?

    return can if can

    can = can_show_planning_process_area_superintendent_menu?

    return can if can

    can = can_show_planning_process_unit_manager_menu?

    return can if can

    can = true if user_connected.program_course_managers.count > 0

    return can if can

    return can

  end

  def can_show_planning_process_training_analyst_menu?(user = nil)

    can = false

    user = user_connected unless user

    user.training_analysts.each do |ta|
      if ta.planning_process_company_unit.planning_process.abierto?
        can = true
        break
      end
    end

    return can

  end

  def can_show_planning_process_area_manager_menu?(user = nil)

    can = false

    user = user_connected unless user

    user.company_unit_area_managers.each do |am|
      if am.planning_process_company_unit.planning_process.abierto?
        can = true
        break
      end
    end

    return can

  end

  def can_show_planning_process_area_superintendent_menu?(user = nil)

    can = false

    user = user_connected unless user

    user.area_superintendents.each do |as|
      if as.planning_process_company_unit.planning_process.abierto?
        can = true
        break
      end
    end

    return can

  end

  def can_show_planning_process_unit_manager_menu?(user = nil)

    can = false

    user = user_connected unless user

    user.company_unit_managers.each do |um|
      if um.planning_process_company_unit.planning_process.abierto?
        can = true
        break
      end
    end

    return can

  end

  def pending_training_analyst_activities(user = nil)

    cua_pending = Array.new
    pp_pending = Array.new
    estado_pending = Array.new

    user = user_connected unless user

    user.training_analysts.each do |ta|

      if ta.planning_process_company_unit.planning_process.abierto?

        ta.planning_process_company_unit.company_unit.company_unit_areas.each_with_index do |cua|

          dncs = ta.planning_process_company_unit.dncs_area cua

          estado_enviado = false
          existen_solicitudes_de_correciones = false

          dncs.each_with_index do |dnc|

            estado_enviado = true if dnc.enviado_aprobar_area
            existen_solicitudes_de_correciones = true if dnc.requiere_modificaciones_area

          end

          if !estado_enviado || existen_solicitudes_de_correciones
            cua_pending.push cua
            pp_pending.push ta.planning_process_company_unit.planning_process

            if !estado_enviado
              estado_pending.push :registrar
            else
              estado_pending.push :corregir
            end

          end

        end

      end

    end

    return cua_pending, pp_pending, estado_pending

  end

  def pending_area_manager_activities(user = nil)

    cua_pending = Array.new
    pp_pending = Array.new
    estado_pending = Array.new

    user = user_connected unless user

    user.company_unit_area_managers.each do |am|

      if am.planning_process_company_unit.planning_process.abierto?

        dncs = am.planning_process_company_unit.dncs_area am.company_unit_area

        estado_enviado = false
        existen_solicitudes_de_correciones = false

        dncs.each_with_index do |dnc|

          estado_enviado = true if dnc.enviado_aprobar_area
          existen_solicitudes_de_correciones = true if dnc.requiere_modificaciones_area

        end

        if !estado_enviado || existen_solicitudes_de_correciones
          cua_pending.push am.company_unit_area
          pp_pending.push am.planning_process_company_unit.planning_process

          if !estado_enviado
            estado_pending.push :registrar
          else
            estado_pending.push :corregir
          end
        end

      end

    end

    return cua_pending, pp_pending, estado_pending

  end

  def pending_area_superintendent_activities(user = nil)

    cua_pending = Array.new
    pp_pending = Array.new
    estado_pending = Array.new

    user = user_connected unless user

    user.area_superintendents.each do |as|

      if as.planning_process_company_unit.planning_process.abierto?

        dncs = as.planning_process_company_unit.dncs_area as.company_unit_area

        estado_enviado = false
        existen_solicitudes_de_correciones = false
        estado_aprobado = false
        estado_aprobado_unidad = false

        dncs.each_with_index do |dnc|

          estado_enviado = true if dnc.enviado_aprobar_area
          existen_solicitudes_de_correciones = true if dnc.requiere_modificaciones_area
          estado_aprobado = true if dnc.aprobado_area
          estado_aprobado_unidad = true if dnc.aprobado_unidad

        end

        if estado_enviado && !existen_solicitudes_de_correciones && !estado_aprobado && !estado_aprobado_unidad
          cua_pending.push as.company_unit_area
          pp_pending.push as.planning_process_company_unit.planning_process

          if as.planning_process_company_unit.area_corrections(as.company_unit_area).length == 0

            estado_pending.push :aprobar

          elsif as.planning_process_company_unit.area_corrections_revisado(as.company_unit_area, false).length == 0

            estado_pending.push :aprobar

          else

            estado_pending.push :corregir

          end

        end

      end

    end

    return cua_pending, pp_pending, estado_pending

  end

  def pending_unit_manager_activities(user = nil)

    cua_pending = Array.new
    pp_pending = Array.new

    user = user_connected unless user

    user.company_unit_managers.each do |um|

      um.planning_process_company_unit.company_unit.company_unit_areas.each_with_index do |cua|

        if um.planning_process_company_unit.planning_process.abierto?

          dncs = um.planning_process_company_unit.dncs_area cua

          estado_enviado = false
          existen_solicitudes_de_correciones = false
          estado_aprobado = false
          estado_aprobado_unidad = false

          dncs.each_with_index do |dnc|

            estado_enviado = true if dnc.enviado_aprobar_area
            existen_solicitudes_de_correciones = true if dnc.requiere_modificaciones_area
            estado_aprobado = true if dnc.aprobado_area
            estado_aprobado_unidad = true if dnc.aprobado_unidad

          end

          if estado_enviado && !existen_solicitudes_de_correciones && estado_aprobado && !estado_aprobado_unidad
            cua_pending.push cua
            pp_pending.push um.planning_process_company_unit.planning_process
          end

        end

      end

    end

    return cua_pending, pp_pending

  end

end
