module BpItemsHelper
  def link_to_remove_item_option(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_item_option(this); return false")
  end

  def link_to_add_item_option(name, f)
    item_option = BpItemOption.new
    fields = f.fields_for(:bp_item_options, item_option, :child_index => "new_option") do |builder|
      render partial: 'bp_forms/item_option_row_form',
             locals: {:item_option => item_option,
                      :item_option_form => builder,
                      :just_added => true,
                      :editable => true }
    end
    link_to(name, '#', onclick: "add_item_option(this, \"#{escape_javascript(fields)}\"); return false")
  end
end
