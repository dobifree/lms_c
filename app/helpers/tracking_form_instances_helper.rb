module TrackingFormInstancesHelper
  require 'json'

  def link_to_remove_answer(name, f)
    f.hidden_field(:_destroy, :class => 'remove_hidden_field') + link_to(name, '#', onclick: "remove_answer(this); return false")
  end


  def tracking_prepare_data_to_show(tracking_form_instance)

    @tracking_form_instance = tracking_form_instance

    @fillable = false
    @traceable = tracking_form_instance.tracking_form.is_traceable?
    @submitable = false

  end

  def tracking_prepare_data_to_edit(tracking_form_instance)

    @tracking_form_instance = tracking_form_instance

    @data_sources = prepare_data_of_nested_lists(tracking_form_instance)

    index = 0
    @incrementer = -> { index += 1 }

    @builder = []
    @filler = []
    @counterpart = []

    if @current_action = tracking_form_instance.current_action(user_connected)
      @new_log = tracking_form_instance.tracking_form_logs.build
      @fillable = (@current_action.fill_form? && !tracking_form_instance.filled?) ? true : false
      @traceable = tracking_form_instance.tracking_form.is_traceable?
    end

  end

  def tracking_prepare_data_to_track(tracking_form_instance)

    @tracking_form_instance = tracking_form_instance

    @fillable = false
    @traceable = tracking_form_instance.tracking_form.is_traceable?
    @submitable = false

    #TODO: estas 5 variables están calculándose de manera redundante en la función @tracking_form_instance.data_for_tracking_form(user_connected)
    am_i_an_attendant = (tracking_form_instance.tracking_participant.attendant_user_id == user_connected.id)
    am_i_a_subject = (tracking_form_instance.tracking_participant.subject_user_id == user_connected.id)
    traceable_by_attendant = tracking_form_instance.tracking_form.traceable_by_attendant?
    traceable_to_subject = tracking_form_instance.tracking_form.traceable_by_subject?
    @can_fill_milestones = (am_i_an_attendant && traceable_by_attendant) || (am_i_a_subject && traceable_to_subject)
    # fin del TO-DO

    @builder, @filler, @counterpart = [], [], []

    if @traceable && !@read_only

      @builder, @filler, @counterpart = @tracking_form_instance.data_for_tracking_form(user_connected)

      # solo es submitable si hay algo para llenar, editar o validar
      #@submitable = (@builder.any? || @filler.any? || @counterpart.any?)

    end

  end

  def tracking_view_to_show(t_f_instance)

    if !@read_only && t_f_instance && t_f_instance.current_action(user_connected)
      @view = 'edit'
    elsif t_f_instance
      @view = 'show'
    end

=begin
    if t_f_instance && t_f_instance.done?
      if t_f_instance.tracking_form.is_traceable?
        @view = @tracking_pill ? 'edit_track' : 'show'
      else
        @view = 'show'
      end
    elsif t_f_instance && t_f_instance.current_action(user_connected)
      @view = 'edit'
    elsif t_f_instance
      @view = 'show'
    end
=end
  end


  def tracking_clean_render_variables()
    @fillable = nil
    @traceable = nil
    @submitable = nil
    @builder = nil
    @filler = nil
    @counterpart = nil
    @can_fill_milestones = nil
  end

  def prepare_data_of_nested_lists(tracking_form_instance)

    ds = {}
    tracking_form_instance.tracking_form.tracking_form_questions.each do |question|
      question.tracking_form_items.where('tracking_list_id IS NOT NULL').each do |form_item|
        ds[form_item.tracking_list_id] = {}
        values_hash = []
        form_item.tracking_list.tracking_list_items.where(:active => true).each do |list_item|
          item_row = {:cod => list_item.id, :value => list_item.name}
          item_row.merge!(tracking_add_father_list_data(list_item))
          values_hash.append(item_row)
        end
        ds[form_item.tracking_list_id] = values_hash
      end
    end

    return ds

  end

  def tracking_add_father_list_data(tracking_list_item)
    row = {}
    if tracking_list_item.tracking_list_item_father_id
      row['cod_' + tracking_list_item.tracking_list.tracking_list_father_id.to_s] = tracking_list_item.tracking_list_item_father_id
      row.merge!(tracking_add_father_list_data(tracking_list_item.tracking_list_item_father))
    end

    return row
  end

  def tracking_is_able_to_open_for_redefinition(tracking_form_instance, user)

    unless tracking_form_instance.tracking_participant.tracking_process.accepts_redefinitions?
      return false
    end

    # si no tiene ya alguna redefinición abierta
    unless tracking_form_instance.tracking_redefinitions.where(:finished => false).count == 0
      return false
    end

    unless tracking_form_instance.done? && tracking_form_instance.tracking_participant.complete?
      return false
    end

    filling_action = tracking_form_instance.tracking_form.tracking_form_actions.where(:fill_form => true).first
    # solo para los que pueden 'llenar' el formulario
    unless (filling_action.able_to_attendant && tracking_form_instance.tracking_participant.attendant_user_id == user.id) ||
        (filling_action.able_to_subject && tracking_form_instance.tracking_participant.subject_user_id == user.id)
      return false
    end

    return true
  end

  def tracking_is_able_to_close_redefinition(tracking_form_instance, user)

    unless tracking_form_instance.tracking_participant.in_redefinition?
      return false
    end

    # si hay alguna redefinición abierta, no se puede cerrar el participant
    unless tracking_form_instance.tracking_participant.tracking_form_instances.joins(:tracking_redefinitions).where(tracking_redefinitions: {:finished => false}).count == 0
      return false
    end

    last_redefined_instance = tracking_form_instance.tracking_participant.tracking_form_instances.joins(:tracking_redefinitions).where(tracking_redefinitions: {:finished => true}).order('tracking_redefinitions.id DESC').last
    next_to_last_refedined_instance = last_redefined_instance.tracking_participant.tracking_form_instances.joins(:tracking_form).order('tracking_forms.position ASC').where('tracking_forms.position > ?', last_redefined_instance.tracking_form.position).first

    # solo está disponible para el formulario siguiente
    unless next_to_last_refedined_instance.id == tracking_form_instance.id
      return false
    end

    filling_action = tracking_form_instance.tracking_form.tracking_form_actions.where(:fill_form => true).first
    # solo para los que pueden 'llenar' el formulario
    unless (filling_action.able_to_attendant && tracking_form_instance.tracking_participant.attendant_user_id == user.id) ||
        (filling_action.able_to_subject && tracking_form_instance.tracking_participant.subject_user_id == user.id)
      return false
    end

    return true
  end

  def tracking_is_able_to_rollback_redefinition(tracking_form_instance, user)

    unless tracking_form_instance.tracking_redefinitions.where(:finished => false).count > 0 && tracking_form_instance.tracking_participant.in_redefinition?
      return false
    end

    filling_action = tracking_form_instance.tracking_form.tracking_form_actions.where(:fill_form => true).first
    # solo para los que pueden 'llenar' el formulario
    unless (filling_action.able_to_attendant && tracking_form_instance.tracking_participant.attendant_user_id == user.id) ||
        (filling_action.able_to_subject && tracking_form_instance.tracking_participant.subject_user_id == user.id)
      return false
    end

    return true
  end

  def calculate_pill_to_open(tracking_participant)

    if tracking_participant.in_redefinition?
      last_redefined_instance = tracking_participant.tracking_form_instances.joins(:tracking_redefinitions).order('tracking_redefinitions.id DESC').last
      if last_redefined_instance.tracking_redefinitions.last.finished?
        next_to_last_redefined_instance = tracking_participant.tracking_form_instances.joins(:tracking_form).order('tracking_forms.position ASC').where('tracking_forms.position > ?', last_redefined_instance.tracking_form.position).first
        return next_to_last_redefined_instance
      else
        return last_redefined_instance
      end
    else
      unless tracking_participant.traceable?
        return tracking_participant.tracking_form_instances.joins(:tracking_form).order('tracking_forms.position ASC').last
      end
    end

    return nil

  end


end
