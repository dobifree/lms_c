module ScSchedulesHelper
  include CalendarDatesModule

  def link_to_remove_block(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_block(this); return false")
  end

  def link_to_delete_block(name, block)
    block = ScBlock.find(block.id)
    block.active = false
    block.deactivated_at = lms_time
    block.deactivated_by_user_id = user_connected.id
    block.save
    link_to(name, '#', onclick: "delete_block(this); return false")
  end

  def link_to_add_block(name, f)
    block = ScBlock.new
    fields = f.fields_for(:sc_blocks, block, :child_index => "block") do |builder|
      render partial: 'sc_block_row_form',
             locals: { :block_form => builder,
                       :block => block,
                       :just_added => true,
                       :editable => true }
    end
    link_to(name, '#', onclick: "add_block(this, \"#{escape_javascript(fields)}\"); return false")
  end

  def link_to_remove_condition(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_condition(this); return false")
  end

  def link_to_add_condition(name, f)
    condition = ScCharacteristicCondition.new
    fields = f.fields_for(:sc_characteristic_conditions, condition, :child_index => "condition") do |builder|
      render partial: 'sc_condition_row_form',
             locals: { :condition_form => builder,
                       :condition => condition,
                       :just_added => true,
                       :editable => true,
                       :conditions_for_select => @conditions_for_select }
    end
    link_to(name, '#', onclick: "add_condition(this, \"#{escape_javascript(fields)}\"); return false")
  end

  def link_to_remove_schedule_user(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_schedule_user(this); return false")
  end

  def calculate_he(block, process_record)
    if process_record.user.he_able
      holiday = holiday_name_for(process_record.user_id, process_record.begin.to_date)
      if holiday
        process_record.he = (process_record.end - process_record.begin)/60
        process_record.save
      else
        if block && !(process_record.begin_sc || process_record.end_sc)
          params = block.sc_he_block_params.where(:active => true).first
          if params
            process_record.he = 0
            entry_he = 0
            if params.entry_active
              block_begin_day = process_record.begin.strftime('%d').to_i if block.day == process_record.begin.wday
              block_begin_day = process_record.end.strftime('%d').to_i if block.day == process_record.end.wday
              block_begin = block.begin_time.change(year: process_record.begin.strftime('%Y').to_i, month: process_record.begin.strftime('%m').to_i, day: block_begin_day)
              entry_he = (block_begin - process_record.begin)/60

              if entry_he > 0
                if params.entry_min
                  if params.entry_min <= entry_he
                    unless params.entry_include_min
                      entry_he -= params.entry_min
                      entry_he = 0 if entry_he < 0
                    end
                  else
                    entry_he = 0
                  end
                end
                if params.entry_max && entry_he > params.entry_max
                  entry_he = params.entry_max
                end
              end
            end

            exit_he = 0
            if params.exit_active
              block_end_day = process_record.begin.strftime('%d').to_i if block.day == process_record.begin.wday
              block_end_day = process_record.end.strftime('%d').to_i if block.day == process_record.end.wday
              block_end_day += 1 if block.next_day
              block_end = block.end_time.change(year: process_record.end.strftime('%Y').to_i, month: process_record.end.strftime('%m').to_i, day: block_end_day)
              exit_he = (process_record.end - block_end)/60

              if exit_he > 0
                if params.exit_min
                  if params.exit_min <= exit_he
                    unless params.exit_include_min
                      exit_he -= params.exit_min
                      exit_he = 0 if exit_he < 0
                    end
                  else
                    exit_he = 0
                  end
                end
                if params.exit_max && exit_he > params.exit_max
                  exit_he = params.exit_max
                end
              end
            end

            process_record.he = 0
            process_record.he += entry_he if entry_he > 0
            process_record.he += exit_he if exit_he > 0
            process_record.save
          else
            block_begin_day = process_record.begin.strftime('%d').to_i if block.day == process_record.begin.wday
            block_begin_day = process_record.end.strftime('%d').to_i if block.day == process_record.end.wday
            block_begin = block.begin_time.change(year: process_record.begin.strftime('%Y').to_i, month: process_record.begin.strftime('%m').to_i, day: block_begin_day)
            entry_he = (block_begin - process_record.begin)/60
            block_end_day = process_record.begin.strftime('%d').to_i if block.day == process_record.begin.wday
            block_end_day = process_record.end.strftime('%d').to_i if block.day == process_record.end.wday
            block_end_day += 1 if block.next_day
            block_end = block.end_time.change(year: process_record.end.strftime('%Y').to_i, month: process_record.end.strftime('%m').to_i, day: block_end_day)
            exit_he = (process_record.end - block_end)/60

            process_record.he = 0
            process_record.he += entry_he if entry_he > 0
            process_record.he += exit_he if exit_he > 0
            process_record.save
          end
        else
          if process_record
            if process_record.begin_sc && process_record.end_sc
              if process_record.begin_sc < process_record.end && process_record.end_sc > process_record.begin
                entry_he = (process_record.begin_sc - process_record.begin)/60
                exit_he = (process_record.end - process_record.end_sc)/60

                process_record.he = 0
                process_record.he += entry_he if entry_he > 0
                process_record.he += exit_he if exit_he > 0
              else
                process_record.he = (process_record.end_sc - process_record.begin_sc)/60
              end
            else
              process_record.he = (process_record.end - process_record.begin)/60
            end
            process_record.save
          end
        end
      end
    end
  end

  def get_schedule_by_record record
    sc_schedule_users = ScScheduleUser.where(:active => true, :user_id => record.user_id)
    schedule_user_ind = nil
    theres_time_period_assoc = false
    theres_ind_time_assoc = false
    schedule_user_time_period = []
    sc_schedule_users.each do |sc_schedule_user|
      if sc_schedule_user.active_since
        if sc_schedule_user.active_since < record.end && sc_schedule_user.active_end > record.begin
          theres_time_period_assoc = true
          schedule_user_time_period << sc_schedule_user
        end
      else
        theres_ind_time_assoc = true
        schedule_user_ind = sc_schedule_user
      end
    end
    if theres_time_period_assoc
      if schedule_user_time_period.size > 1
        schedule_user_time_period.sort_by {|schedule_user| schedule_user_time_period.active_since}
        return schedule_user_time_period.first
      else
        return schedule_user_time_period.first
      end
    else
      if theres_ind_time_assoc
        return schedule_user_ind
      else
        return nil
      end
    end
  end

  def get_schedule_block_by_record( schedule_user, record)
    blocks = []
    schedule_blocks = schedule_user ? schedule_user.sc_schedule.sc_blocks.where(:active => true) : []
    schedule_blocks.each do |block|
      next_day = block.next_day ? block.day+1 : block.day

      if record.end.wday == record.begin.wday
        if block.begin_formatted <= record.end_formatted && block.day <= record.end.wday
          if block.end_formatted >= record.begin_formatted && next_day >= record.begin.wday
            blocks << block if block.workable
          end
        end
      else
        if block.day == record.begin.wday
          if block.end_formatted >= record.begin_formatted
            blocks << block if block.workable
          end
        else
          if block.day == record.end.wday
            if block.begin_formatted <= record.end_formatted
              blocks << block if block.workable
            end
          end
        end
      end
    end
    blocks.sort_by {|block| block.begin_time}
    blocks.each do |block|
      sc_record_block = ScRecordBlock.where(:sc_record_id => record.id, :sc_block_id => block.id, :active => true).first
      if sc_record_block
        sc_record_block.active = false
        sc_record_block.deactivated_at = lms_time
        sc_record_block.deactivated_by_user_id = user_connected.id
      end
      ScRecordBlock.create(:sc_block_id => block.id,
                            :sc_record_id => record.id)
    end
    return blocks[0]
  end

  def complete_create_record(record_params, user_id, process_id)
    new_record = ScRecord.new(record_params)
    privilege_user = HeCtModulePrivilage.where(active: true, user_id: user_connected.id).first
    recorder = ScUserToRegister.where(active: true, user_id: user_id, he_ct_module_privilage_id: privilege_user.id).first
    if recorder
      new_record.sc_user_to_register_id = recorder.id
    else
      new_record.he_ct_module_privilege_id = privilege_user.id
    end
    new_record.user_id = user_id
    new_record.sc_process_id = process_id
    new_record.registered_at = lms_time
    new_record.registered_by_user = user_connected
    new_record.status = 0
    new_record.effective_time = ((new_record.end - new_record.begin)/60) if new_record.end &&  new_record.begin #minutes if
    new_record.he = 0
    return new_record
  end

end
