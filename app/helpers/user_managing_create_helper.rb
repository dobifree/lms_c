module UserManagingCreateHelper


  def generate_xlsx_create(company)

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato Alta Usuarios')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        if company.second_last_name

          fila << 'Apellido paterno'
          filaHeader <<  headerCenter
          cols_widths << 20

          fila << 'Apellido materno'
          filaHeader <<  headerCenter
          cols_widths << 20

        else

          fila << 'apellidos'
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        fila << 'Email'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Password'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Empleado'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Fecha alta'
        filaHeader <<  headerCenter
        cols_widths << 20

        characteristics = Characteristic.where('required_new = ?', true).reorder('required_new_position')

        characteristics.each do |characteristic|

          next if characteristic.data_type_id == 11 || characteristic.data_type_id == 6

          fila << characteristic.nombre
          filaHeader <<  headerCenter
          cols_widths << 20

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def carga_usuario_from_excel(archivo_temporal, company)

    pos_char = 7
    pos_char += 1 if company.second_last_name

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      next if index == 0

      errors = false

      @char_errors = Array.new

      Characteristic.where('required_new = ?', true).each do |characteristic|



      end

    end

  end

end
