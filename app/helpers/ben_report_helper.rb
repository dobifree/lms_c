module BenReportHelper

  def bills_by_self_value(process_id, report_id)
    bills_by_self_value = []
    bills = BenCellphoneBillItem.where(ben_cellphone_process_id: process_id)
    users_id = users_id_match_self_value(report_id)
    bills.each do |bill|
      if bill.ben_user_cellphone_process
        bill_user_id = bill.ben_user_cellphone_process.user.id
      else
        if bill.ben_cellphone_number && bill.ben_cellphone_number.user
          bill_user_id = bill.ben_cellphone_number.user.id
        end
      end
      next unless bill_user_id
      next unless users_id.include?(bill_user_id)
      bills_by_self_value << bill
    end
    return bills_by_self_value
  end

  def bills_by_user_connected(process_id, report_id)
    #Obtiene todos los bill items del proceso especificado, y que cumplan con las condiciones del reporte que tienen que ver con el usuario conectado. Por ejemplo: gerencia
    bills = BenCellphoneBillItem.where(ben_cellphone_process_id: process_id)
    users_id = users_id_match_user_connected(report_id)
    bills_match_by_user_connected = []
    bills.each do |bill|
      if bill.ben_user_cellphone_process
        bill_user_id = bill.ben_user_cellphone_process.user.id
      else
        if bill.ben_cellphone_number && bill.ben_cellphone_number.user
          bill_user_id = bill.ben_cellphone_number.user.id
        end
      end
      next unless bill_user_id
      next unless users_id.include?(bill_user_id)
      uc = UserCharacteristic.where(user_id: bill_user_id, characteristic_id: 5).first
      bills_match_by_user_connected << bill
    end
    return bills_match_by_user_connected
  end

  def users_id_match_user_connected(report_id)
    report = BenCustomReport.find(report_id)
    conditions = report.ben_custom_report_characteristics.where(relation_type: 2)
    users_id = []
    conditions.each_with_index do |condition, index|
      if index.zero?
        users_id += condition.match_users_by_user_connected(user_connected.id)
      else
        users_id_aux = condition.match_users_by_user_connected(user_connected.id)
        users_id.delete_if { |user_id| !users_id_aux.include?(user_id)}
      end
    end
    return users_id
  end

  def users_id_match_self_value(report_id)
    users = users_id_match_self_value_unique(report_id)
    users1 = users_id_match_self_value_repeated(report_id)
    users1.each { |user_id| users << user_id unless users.include?(user_id) }
    return users
  end

  def users_id_match_self_value_unique(report_id)
    #Devuelve los id de usuarios que que cumplan condiciones del reporte especificado.
    # Donde las condiciones a cumplir tienen que ver con un valor específico y no del usuario conectado
    # Donde las condiciones a cumplir no se repiten,
    # en cuyo caso, la condición para unirse al arreglo es un O, no un Y
    report = BenCustomReport.find(report_id)
    conditions = report.ben_custom_report_characteristics.where(relation_type: 1)
    users_id = []
    conditions_1 = unique_char_on_conditions(conditions)
    conditions_1 = BenCustomReportCharacteristic.where(ben_cellphone_characteristic_id: conditions_1, ben_custom_report_id: report_id, relation_type: 1)
    conditions_1.each_with_index do |condition, index|
      if index.zero?
        users_id += condition.match_users_by_self_value
      else
        users_id_aux = condition.match_users_by_self_value
        users_id.delete_if { |user_id| !users_id_aux.include?(user_id)}
      end
    end
    return users_id
  end

  def users_id_match_self_value_repeated(report_id)
    report = BenCustomReport.find(report_id)
    conditions = report.ben_custom_report_characteristics.where(relation_type: 1)
    users_id = []
    repeated_characteristics_id = repeated_char_on_conditions(conditions)

    repeated_characteristics_id.each do |characteristic_id|
      conditions = conditions_by_char(characteristic_id, report_id)
      conditions.each do |condition|
        users_id_aux = condition.match_users_by_self_value
        users_id_aux.each { |user_id_aux| users_id << user_id_aux unless users_id.include?(user_id_aux)}
      end
    end
    return users_id
  end

  def conditions_by_char(characteristic_id, report_id)
    report = BenCustomReport.find(report_id)
    conditions = report.ben_custom_report_characteristics.where(relation_type: 1)
    conditions_by_char = []
    conditions.each { |condition| conditions_by_char << condition if condition.characteristic_id == characteristic_id }
    return conditions_by_char
  end

  def users_id_unique_conditions(conditions)
    conditions_1 = unique_char_on_conditions(conditions)
    conditions.each_with_index do |condition, index|
      if index.zero?
        users_id += condition.match_users_by_user_connected(user_connected.id)
      else
        users_id_aux = condition.match_users_by_user_connected(user_connected.id)
        users_id.delete_if { |user_id| !users_id_aux.include?(user_id)}
      end
    end
    return users_id
  end

  def repeated_char_on_conditions(conditions)
    #Devuelve un array con los id de las condiciones
    repeated_char_on_conditions_id = []
    conditions.each do |condition|
      times_found = 0
      conditions.each do |condition_1|
        next unless condition_1.characteristic_id == condition.characteristic_id
        times_found += 1
      end
      next if times_found < 2
      repeated_char_on_conditions_id << condition.characteristic_id
    end
    return repeated_char_on_conditions_id
  end

  def unique_char_on_conditions(conditions)
    unique_char_on_conditions_id = []
    conditions.each do |condition|
      times_found = 0
      conditions.each do |condition_1|
        next unless condition_1.characteristic_id == condition.characteristic_id
        times_found += 1
      end
      next unless times_found == 1
      unique_char_on_conditions_id << condition.characteristic_id
    end
    return unique_char_on_conditions_id
  end

  def repeated_conditions(conditions)
    repeated_conditions_id = []
    repeated_char_on_conditions_id = repeated_char_on_conditions(conditions)
    conditions.each { |condition| repeated_conditions_id << condition.id if repeated_char_on_conditions_id.include?(condition.characteristic_id)}
    return repeated_conditions_id
  end

  def unique_conditions(conditions)
    unique_conditions_id = []
    unique_char_on_conditions_id = unique_char_on_conditions(conditions)
    conditions.each { |condition| unique_conditions_id << condition.id if unique_char_on_conditions_id.include?(condition.characteristic_id)}
    return unique_conditions_id
  end

  def filter_bills_attributes(bills, report_id)
    attributes_filters = BenCustomReportAttributeFilter.where(ben_custom_report_id: report_id).all
    if attributes_filters.size > 0
      bills_array = []
      bills.each_with_index do |bill, index|
        sent_to_array = true
        attributes_filters.each do |filter|
          case filter.name
            when 'Numero de celular'
              sent_to_array = false unless bill.cellphone_number == filter.value
            when 'Folio'
              sent_to_array = false unless bill.bill_folio == filter.value
            when 'Descripción'
              sent_to_array = false unless bill.description == filter.value
            when 'Valor neto'
              sent_to_array = false unless bill.net_value == filter.value
            when 'Descuento Movistar'
              sent_to_array = false unless bill.discount == filter.value
            when 'Neto total'
              sent_to_array = false unless bill.net_total == filter.value
            when 'Neto con IVA'
              sent_to_array = false unless bill.net_with_taxes == filter.value
            when 'Tipo de factura'
              sent_to_array = false unless bill.bill_type == filter.value
            when 'Cargar a Payroll'
              sent_to_array = false unless bill.not_charge_to_payroll != filter.value
            when 'Registrado manualmente'
              sent_to_array = false unless bill.registered_manually == filter.value
            when '¿Por qué el registro manual?'
              sent_to_array = false unless bill.registered_manually_description == filter.value
            when 'Errores del registro'
              sent_to_array = false unless bill.bill_item_errors == filter.value
            when 'Mes del proceso'
              sent_to_array = false unless bill.ben_cellphone_process.month == filter.value
            when 'Año del proceso'
              sent_to_array = false unless bill.ben_cellphone_process.year == filter.value
            when 'RUT de compañia'
              sent_to_array = false unless bill.company_rut == filter.value
          end
        end
        bills_array << bill if sent_to_array
      end
      return bills_array
    else
      return bills
    end
  end

  def link_to_remove_condition_report(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_condition(this); return false")
  end

  def link_to_add_condition_report(name, f)
    condition = BenCustomReportCharacteristic.new
    fields = f.fields_for(:ben_custom_report_characteristics, condition, :child_index => "new_condition") do |builder|
      render partial: 'ben_report_condition',
             locals: { condition_form: builder,
                       condition: condition,
                       just_added: true,
                       editable: true }
    end
    link_to(name, '#', onclick: "add_condition(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
