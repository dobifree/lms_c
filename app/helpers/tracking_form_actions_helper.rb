module TrackingFormActionsHelper

  def link_to_remove_form_notification(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_form_notification(this); return false")
  end

  def link_to_add_form_notification(name, f)
    question = TrackingFormNotification.new
    fields = f.fields_for(:tracking_form_notifications, question, :child_index => "new_form_notification") do |builder|
      render('tracking_form_actions/notifications/form', :f => builder)
    end
    link_to(name, '#', onclick: "add_form_notification(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
