module TrackingProcessesHelper


  def icon_tracking_module_html_safe
    icon_tracking_module.html_safe
  end

  def icon_tracking_module
    '<i class="' + icon_tracking_module_class + '"></i>'
  end

  def icon_tracking_module_class
    'fa fa-compass'
  end

  def link_to_remove_tracking_process_char(name, f)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_tracking_process_char(this); return false")
  end

  def link_to_add_tracking_process_char(name, f)
    char = TrackingProcessChar.new
    fields = f.fields_for(:tracking_process_chars, char, :child_index => "new_tracking_process_char") do |builder|
      render('tracking_processes/tracking_process_char_form', :f_char => builder)
    end
    link_to(name, '#', onclick: "add_tracking_process_char(this, \"#{escape_javascript(fields)}\"); return false")
  end

end
