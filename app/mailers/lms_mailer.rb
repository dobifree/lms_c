class LmsMailer < ActionMailer::Base


  #default from: 'tutor@exa.pe'
  #default from: 'capacitacion@exa.pe'
  default from: 'Notificaciones EXA <notificaciones@exa.pe>'



  def recuperar_pass(user, company)

    @company = company

    @user = user
    mail(from: @company.notifications_email, to: user.email, subject: t('rec_pass.subject')) if user.email
  end

  def jm_recuperar_pass(candidate, company)
    @company = company
    @candidate = candidate

    @ct_module = CtModule.where(cod: 'jm').first

    mail(from: @company.notifications_email, to: candidate.email, subject: @ct_module.alias.blank? ? @ct_module.name : @ct_module.alias + ' - Generación de contraseña') if candidate.email
  end

  def jm_switch_email(candidate, company)
    @company = company
    @candidate = candidate
    mail(from: @company.notifications_email, to: candidate.email_to_switch, subject: t('rec_pass.new_email_subject')) if candidate.email
  end

  def new_support_ticket(support_ticket, company)

    @company = company

    if @company.email_mesa_ayuda

      @support_ticket = support_ticket

      mail(from: @company.notifications_email, to: @company.email_mesa_ayuda, subject: 'Mesa de Ayuda: '+@company.nombre) if @company.email_mesa_ayuda

    end

  end

  def answer_support_ticket(support_ticket, company)

    @company = company

    if support_ticket.user.email

      @support_ticket = support_ticket

      mail(from: @company.notifications_email, to: support_ticket.user.email, subject: 'Mesa de Ayuda') if support_ticket.user.email

    end

  end

  def pe_def_by_users_send_email_to_accept_definition(user, pe_evaluation, company)

    @company = company
    @pe_evaluation = pe_evaluation

    sender = pe_evaluation.pe_process.mailer_name.blank? ? 'Notificaciones EXA' : pe_evaluation.pe_process.mailer_name

    subject = pe_evaluation.email_to_accept_definition_subject.blank? ? '' : pe_evaluation.email_to_accept_definition_subject

    mail(from: @company.notifications_email, to: user.email, from: sender+ '<'+@company.notifications_email+'>', subject: subject) if user.email

  end

  ## copiado
  def pe_step_validation_send_when_accept(user_evaluator, user_evaluated, pe_process, user_validator, company)

    @company = company

    sender = pe_process.mailer_name.blank? ? 'Notificaciones EXA' : pe_process.mailer_name

    subject = 'Notificación de resultado de validación'

    @user_evaluated = user_evaluated
    @user_evaluator = user_evaluator
    @user_validator = user_validator

    @pe_process = pe_process

    mail(from: @company.notifications_email, to: user_evaluator.email, from: sender+ '<'+@company.notifications_email+'>', subject: subject) if user_evaluator.email

  end

  ## copiado
  def pe_step_validation_send_when_reject(user_evaluator, user_evaluated, pe_process, user_validator, company)

    @company = company

    sender = pe_process.mailer_name.blank? ? 'Notificaciones EXA' : pe_process.mailer_name

    subject = 'Notificación de resultado de validación'

    @user_evaluated = user_evaluated
    @user_evaluator = user_evaluator
    @user_validator = user_validator

    @pe_process = pe_process

    mail(from: @company.notifications_email, to: user_evaluator.email, from: sender+ '<'+@company.notifications_email+'>', subject: subject) if user_evaluator.email

  end


  ## copiado
  def ask_pe_member_to_accept_feedback(pe_member, pe_process, menu_name, company)

    @pe_process = pe_process
    @company = company
    @menu_name = menu_name

    sender = pe_process.mailer_name.blank? ? 'Notificaciones EXA' : pe_process.mailer_name

    mail(from: @company.notifications_email, to: pe_member.user.email, from: sender+ '<'+@company.notifications_email+'>', subject: 'Retroalimentación por confirmar') if pe_member.user.email

  end

  def planning_process_send_to_approve_area(planning_process_company_unit, company_unit_area, company)

    @company = company

    @planning_process_company_unit = planning_process_company_unit
    @company_unit_area = company_unit_area

    @planning_process_company_unit.area_superintendents.each do |as|

      if as.company_unit_area_id == @company_unit_area.id

        mail(from: @company.notifications_email, to: as.user.email, subject: 'Planificación Unidad '+@planning_process_company_unit.company_unit.nombre+' - Área '+@company_unit_area.nombre) if as.user.email

      end

    end


  end

  def planning_process_request_dnc_area_correction(planning_process_company_unit, company_unit_area, company)

    @company = company

    @planning_process_company_unit = planning_process_company_unit
    @company_unit_area = company_unit_area

    @planning_process_company_unit.training_analysts.each do |ta|

      mail(from: @company.notifications_email, to: ta.user.email, subject: 'Planificación Unidad '+@planning_process_company_unit.company_unit.nombre+' - Área '+@company_unit_area.nombre) if ta.user.email

    end

    @planning_process_company_unit.company_unit_area_managers.each do |am|

      if am.company_unit_area_id == @company_unit_area.id

        mail(from: @company.notifications_email, to: am.user.email, subject: 'Planificación Unidad '+@planning_process_company_unit.company_unit.nombre+' - Área '+@company_unit_area.nombre) if am.user.email

      end

    end

  end

  def planning_process_approve_area(planning_process_company_unit, company_unit_area, company)

    @company = company

    @planning_process_company_unit = planning_process_company_unit
    @company_unit_area = company_unit_area

    @planning_process_company_unit.company_unit_managers.each do |um|

      mail(from: @company.notifications_email, to: um.user.email, subject: 'Planificación Unidad '+@planning_process_company_unit.company_unit.nombre+' - Área '+@company_unit_area.nombre) if um.user.email

    end

  end

  def planning_process_request_area_correction(planning_process_company_unit, company_unit_area, company)

    @company = company

    @planning_process_company_unit = planning_process_company_unit
    @company_unit_area = company_unit_area

    @planning_process_company_unit.area_superintendents.each do |as|

      if as.company_unit_area_id == @company_unit_area.id

        mail(from: @company.notifications_email, to: as.user.email, subject: 'Planificación Unidad '+@planning_process_company_unit.company_unit.nombre+' - Área '+@company_unit_area.nombre) if as.user.email

      end

    end

  end

  def planning_process_approve_unit(planning_process_company_unit, company_unit_area, company)

    @company = company

    @planning_process_company_unit = planning_process_company_unit
    @company_unit_area = company_unit_area

    @planning_process_company_unit.training_analysts.each do |um|

      mail(from: @company.notifications_email, to: um.user.email, subject: 'Planificación Unidad '+@planning_process_company_unit.company_unit.nombre+' - Área '+@company_unit_area.nombre) if um.user.email

    end

  end

  def informar_jefe_autoeval_terminada(jefe, colaborador, hr_process, company)

    @jefe = jefe
    @colaborador = colaborador
    @hr_process = hr_process
    @company = company

    mail(from: @company.notifications_email, to: jefe.email, subject: 'Autoevaluación finalizada') if jefe.email

  end

  def informar_valuador_feedback_confirmado(colaborador, hr_process, company)

    @hr_process = hr_process
    @company = company

    mail(from: @company.notifications_email, to: colaborador.email, subject: 'Retroalimentación por confirmar') if colaborador.email

  end


  def correo_prima(codigo, nombre, correo, area, servicio, seccion, horas)

    @codigo = codigo
    @nombre = nombre
    @correo = correo
    @area = area
    @servicio = servicio
    @seccion = seccion
    @horas = horas

    if File.exist? '/home/ubuntu/correo_prima/'+codigo+'.pdf'
      attachments['Reporte2013.pdf'] = File.read('/home/ubuntu/correo_prima/'+codigo+'.pdf')
    end

    mail(from: 'TUTOR EXA PERU <tutor@exa.pe>', to: correo, subject: 'Conoce tus resultados de capacitación en el 2013') if correo


  end

  def log_sync_security(sync_type, data_syncs_logs, executed_at,company)
    @sync_type = sync_type
    @data_syncs_logs = data_syncs_logs
    @executed_at = executed_at

    mail(from: company.notifications_email, to: 'alejo.ruiz.robles@gmail.com', subject: 'Sincronziación')

  end

end
