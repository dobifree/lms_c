class PeMailer < ActionMailer::Base

  default from: 'Notificaciones EXA <notificaciones@exa.pe>'


  def step_query_definitions_accept(pe_process, company, menu_name, member_evaluated)

    #se le envía al evaluado para avisarle que confirmó la definición

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process.pe_process_notification.step_query_definitions_accept_subject ? pe_process.pe_process_notification.step_query_definitions_accept_subject : ''

    body = pe_process.pe_process_notification.step_query_definitions_accept_body ? pe_process.pe_process_notification.step_query_definitions_accept_body.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, nil, nil, nil

    mail(from: sender, to: member_evaluated.email, subject: subject)

  end

  def step_query_definitions_accept_cc(pe_process, company, menu_name, member_evaluated, mail_user_cc)

    #se le envía al evaluado para avisarle que confirmó la definición

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process.pe_process_notification.step_query_definitions_accept_subject ? pe_process.pe_process_notification.step_query_definitions_accept_subject : ''

    body = pe_process.pe_process_notification.step_query_definitions_accept_body ? pe_process.pe_process_notification.step_query_definitions_accept_body.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, nil, nil, nil

    mail(from: sender, to: mail_user_cc, subject: subject)

  end

  def step_query_definitions_accept_to_boss(pe_process, company, menu_name, member_evaluated, boss)

    #se le envía al evaluado para avisarle que confirmó la definición

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process.pe_process_notification.step_query_definitions_accept_subject_to_boss ? pe_process.pe_process_notification.step_query_definitions_accept_subject_to_boss : ''

    body = pe_process.pe_process_notification.step_query_definitions_accept_body_to_boss ? pe_process.pe_process_notification.step_query_definitions_accept_body_to_boss.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, boss, nil, nil

    mail(from: sender, to: boss.email, subject: subject)

  end

  def step_def_by_users_validate_before(pe_process, company, menu_name, pe_evaluation, member_evaluated, member_evaluator, validator)

    #se envía al validador cuando se finaliza definición

    sender = PeMailer.define_sender pe_process, company

    pe_process_notification_def = pe_evaluation.pe_process_notification_def

    subject = pe_process_notification_def.to_validator_when_defined_subject ? pe_process_notification_def.to_validator_when_defined_subject : ''

    body = pe_process_notification_def.to_validator_when_defined_body ? pe_process_notification_def.to_validator_when_defined_body.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, member_evaluator, validator

    mail(from: sender, to: validator.email, subject: subject)

  end

  def step_def_by_users_accept_definition(pe_process, company, menu_name, pe_evaluation, member_evaluated, member_evaluator, validator = nil)

    #se envía al evaluado para que confirme las preguntas (objetivos) que le han definido

    sender = PeMailer.define_sender pe_process, company

    subject = pe_evaluation.email_to_accept_definition_subject ? pe_evaluation.email_to_accept_definition_subject : ''

    body = pe_evaluation.email_to_accept_definition_content ? pe_evaluation.email_to_accept_definition_content.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, member_evaluator, validator

    mail(from: sender, to: member_evaluated.email, subject: subject)

  end

  def step_def_by_users_after_accept_definition(pe_process, company, menu_name, pe_evaluation, member_evaluated, creators)

    #se envía al evaluador para avisarle que el evaluador ha aceptado la definición

    sender = PeMailer.define_sender pe_process, company

    subject = pe_evaluation.email_after_accept_definition_subject ? pe_evaluation.email_after_accept_definition_subject : ''

    body = pe_evaluation.email_after_accept_definition_content ? pe_evaluation.email_after_accept_definition_content.dup : ''

    creators.each do |creator|

      @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, creator

      mail(from: sender, to: creator.email, subject: subject)

    end

  end

  def step_def_by_users_validate_definition_accept(pe_process, company, menu_name, pe_evaluation, member_evaluated, creators, pe_process_notification_def_val, validator)

    #se envía al usuario que define indicando que su definición ha sido aceptada

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process_notification_def_val.accepted_subject ? pe_process_notification_def_val.accepted_subject : ''

    body = pe_process_notification_def_val.accepted_body ? pe_process_notification_def_val.accepted_body.dup : ''

    creators.each do |creator|

      @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, creator, validator

      mail(from: sender, to: creator.email, subject: subject)

    end

    unless pe_process_notification_def_val.before_accepted

      if pe_process.has_step_definition_by_users_accepted && pe_evaluation.allow_definition_by_users_accepted

        creators.each do |creator|

          @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, creator, validator

          mail(from: sender, to: member_evaluated.email, subject: subject)

        end

      end

    end

  end

  def step_def_by_users_validate_definition_reject(pe_process, company, menu_name, pe_evaluation, member_evaluated, creators, pe_process_notification_def_val, validator)

    #se envía al usuario que define indicando que su definición ha sido rechazada

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process_notification_def_val.rejected_subject ? pe_process_notification_def_val.rejected_subject : ''

    body = pe_process_notification_def_val.rejected_body ? pe_process_notification_def_val.rejected_body.dup : ''

    creators.each do |creator|

      @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, creator, validator

      mail(from: sender, to: creator.email, subject: subject)

    end

    unless pe_process_notification_def_val.before_accepted

      if pe_process.has_step_definition_by_users_accepted && pe_evaluation.allow_definition_by_users_accepted

        creators.each do |creator|

          @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, creator, validator

          mail(from: sender, to: member_evaluated.email, subject: subject)

        end

      end

    end

  end

  def step_assessment_to_validator(pe_process, company, menu_name, member_evaluated, member_evaluator, validator)

    #se envía al validador para avisrle que el evaluador ha finalizado su evaluación

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process.pe_process_notification.step_assessment_to_validator_subject ? pe_process.pe_process_notification.step_assessment_to_validator_subject : ''

    body = pe_process.pe_process_notification.step_assessment_to_validator_body ? pe_process.pe_process_notification.step_assessment_to_validator_body.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, member_evaluator, validator

    mail(from: sender, to: validator.email, subject: subject)

  end

  def step_validation_accepted(pe_process, company, menu_name, member_evaluated, member_evaluator, validator)

    #se envía al evaluador para avisarle que el validador ha validado su evaluación

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process.pe_process_notification.step_validation_accepted_subject ? pe_process.pe_process_notification.step_validation_accepted_subject : ''

    body = pe_process.pe_process_notification.step_validation_accepted_body ? pe_process.pe_process_notification.step_validation_accepted_body.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, member_evaluator, validator

    mail(from: sender, to: member_evaluator.email, subject: subject)

  end

  def step_validation_rejected(pe_process, company, menu_name, member_evaluated, member_evaluator, validator)

    #se envía al evaluador para avisarle que el validador ha observado su evaluación

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process.pe_process_notification.step_validation_rejected_subject ? pe_process.pe_process_notification.step_validation_rejected_subject : ''

    body = pe_process.pe_process_notification.step_validation_rejected_body ? pe_process.pe_process_notification.step_validation_rejected_body.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, member_evaluator, validator

    mail(from: sender, to: member_evaluator.email, subject: subject)

  end

  def step_feedback_accept_ask_to_accept(pe_process, company, menu_name, member_evaluated, feedback_provider)

    #se le envía al evaluado para que confirme el feedback que ha recibido

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process.pe_process_notification.step_feedback_accepted_ask_to_accept_subject ? pe_process.pe_process_notification.step_feedback_accepted_ask_to_accept_subject : ''

    body = pe_process.pe_process_notification.step_feedback_accepted_ask_to_accept_body ? pe_process.pe_process_notification.step_feedback_accepted_ask_to_accept_body.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, nil, nil, feedback_provider

    mail(from: sender, to: member_evaluated.email, subject: subject)

  end

  def step_feedback_accept_to_feedback_provider(pe_process, company, menu_name, member_evaluated, feedback_provider)

    #se le envía al retroalimentador indicando que el evaluado ha aceptado el feedback recibido

    sender = PeMailer.define_sender pe_process, company

    subject = pe_process.pe_process_notification.step_feedback_accepted_to_boss_subject ? pe_process.pe_process_notification.step_feedback_accepted_to_boss_subject : ''

    body = pe_process.pe_process_notification.step_feedback_accepted_to_boss_body ? pe_process.pe_process_notification.step_feedback_accepted_to_boss_body.dup : ''

    @body = PeMailer.replace_variables body, pe_process, company, menu_name, member_evaluated, nil, nil, feedback_provider

    mail(from: sender, to: feedback_provider.email, subject: subject)

  end

  def self.replace_variables(body, process, company, menu_name = nil, member_evaluated = nil, member_evaluator = nil, validator = nil, feedback_provider = nil)

    if process
      body.gsub! '%{process_name}', process.name
      body.gsub! '%{process_period}', process.period
      body.gsub!('%{process_mailer_name}', process.mailer_name) if process.mailer_name
    end

    body.gsub! '%{email_support}', company.email_mesa_ayuda if company

    body.gsub! '%{menu_name}', menu_name if menu_name

    if member_evaluated
      body.gsub! '%{evaluated_name}', member_evaluated.nombre
      body.gsub! '%{evaluated_lastname}', member_evaluated.apellidos
    end

    if member_evaluator
      body.gsub! '%{evaluator_name}', member_evaluator.nombre
      body.gsub! '%{evaluator_lastname}', member_evaluator.apellidos
    end

    if validator
      body.gsub! '%{validator_name}', validator.nombre
      body.gsub! '%{validator_lastname}', validator.apellidos
    end

    if feedback_provider
      body.gsub! '%{feedback_provider_name}', feedback_provider.nombre
      body.gsub! '%{feedback_provider_lastname}', feedback_provider.apellidos
    end

    body.gsub! '%{link_platform}', "<a href='#{$full_current_protocol_domain}' target='_blank'>#{$full_current_protocol_domain}</a>"

    return body

  end

  def self.define_sender(pe_process, company)
    (pe_process.mailer_name.blank? ? 'Notificaciones EXA' : pe_process.mailer_name) + ' <'+company.notifications_email+'>'
  end


end
