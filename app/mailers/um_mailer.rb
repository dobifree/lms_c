class UmMailer < ActionMailer::Base
  default from: 'Notificaciones EXA <notificaciones@exa.pe>'

  def create_user_security(user, company)

    @company = company

    @user = user

    to = @company.email_altas

    mail(from: @company.notifications_email, to: to, subject: 'Alta')
  end

  def delete_user_security(user, company)

    @company = company

    @user = user

    to = @company.email_bajas

    mail(from: @company.notifications_email, to: to, subject: 'Baja')
  end

end
