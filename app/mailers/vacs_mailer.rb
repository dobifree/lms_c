class VacsMailer < ActionMailer::Base
  include VacationsModule
  include LicensesModule
  include VacRecordsRegisterHelper

  default from: 'Notificaciones EXA <notificaciones@exa.pe>'

  def approved_request(request, company)
    @company = company
    sender = define_sender(@company)
    @request = request
    @back_to_work = back_to_work_request(@request.id)
    send_email_to( @request.user,  @request.user.email, 'Tus vacaciones fueron aprobadas', sender)
  end

  def rejected_request(request, company)
    @company = company
    sender = define_sender(@company)
    @request = request
    @back_to_work = back_to_work_request(@request.id)
    send_email_to( @request.user,  @request.user.email, 'Tus vacaciones no fueron aprobadas', sender)
  end

  def new_pending_request(request, company, approver_id)
    @company = company
    sender = define_sender(@company)
    @request = request
    @back_to_work = back_to_work_request(@request.id)
    approver = VacApprover.find(approver_id)
    @approver = approver
    @restrictive_warnings = gimme_restrictive_warnings(@request.user_id, @request.begin, @request.end,0, 0, @request.id)
    send_email_to( approver.user,  approver.user.email, 'Ingreso de Vacaciones '+@request.user.full_name, sender)
  end

  def new_pending_requester(request, company)
    @company = company
    sender = define_sender(@company)
    @request = request
    @back_to_work = back_to_work_request(@request.id)
    @restrictive_warnings = gimme_restrictive_warnings(@request.user_id, @request.begin, @request.end, 0, 0, @request.id)
    send_email_to(@request.user, @request.user.email, 'Tu solicitud de vacaciones fue ingresada exitosamente', sender)
  end

  def new_lic_pending_request(request, company, approver_id)
    @company = company
    sender = define_sender(@company)
    @request = request
    approver = LicApprover.find(approver_id)
    @approver = approver
    send_email_to(approver.user, approver.user.email, 'Nueva solicitud de beneficio pendiente', sender)
  end

  def new_lic_pending_requester(request, company)
    @company = company
    sender = define_sender(@company)
    @request = request
    send_email_to(@request.user, @request.user.email, 'Tu solicitud de beneficio fue ingresada exitosamente', sender)
  end

  def approved_lic_request(request, company)
    @company = company
    sender = define_sender(@company)
    @request = request
    send_email_to( @request.user,  @request.user.email, 'Tu solicitud de beneficio fue aprobada', sender)
  end

  def rejected_lic_request(request, company)
    @company = company
    sender = define_sender(@company)
    @request = request
    send_email_to(@request.user,  @request.user.email, 'Tu solicitud de beneficio no fue aprobada', sender)
  end


  private

  def define_sender(company)
    'Notificaciones EXA' + ' <'+ company.notifications_email+'>'
  end

  def send_email_to(user, to, subject, sender)
    if (to =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i).nil? || to.nil?
      mail(from: sender, to: default_emails, subject: '[Email inválido para: '+user.codigo.to_s+'] '+subject.to_s)
    else
      mail(from: sender, to: to, subject: subject)
    end
  end

  def default_emails
    ['dino@capitalteam.pe', 'cesar@capitalteam.pe', 'alejandro@capitalteam.pe']
  end
end