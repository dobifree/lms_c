class TrackingMailer < ActionMailer::Base
  default from: 'Notificaciones EXA <notificaciones@exa.pe>'


  def state_selected(tracking_process, company, to, cc, subject, body)
    sender = define_sender(tracking_process, company)
    @body = body
    mail(from: sender, to: to, cc: cc, subject: subject)
  end

  def action_done(tracking_process, company, to, cc, subject, body)
    sender = define_sender(tracking_process, company)
    @body = body
    mail(from: sender, to: to, cc: cc, subject: subject)
  end

  def action_rejected(tracking_process, company, to, cc, subject, body)
    sender = define_sender(tracking_process, company)
    @body = body
    mail(from: sender, to: to, cc: cc, subject: subject)
  end

  def define_sender(tracking_process, company)
    (tracking_process.mailer_name.blank? ? 'Notificaciones EXA' : tracking_process.mailer_name) + ' <'+ company.notifications_email+'>'
  end

end
