class SelMailer < ActionMailer::Base
  default from: 'Notificaciones EXA <notificaciones@exa.pe>'

  def invitation_to_process(jm_message, company)
    @company = company
    sender = define_sender(@company)
    @body = jm_message.body
    mail(from: sender, to: jm_message.jm_candidate.email, subject: jm_message.subject)
  end

  def sel_notification(notification, company)
    @company = company
    sender = define_sender(@company)
    @body = notification.body
    mail(from: sender, to: notification.to, cc: notification.cc, subject: notification.subject)
  end

  private
  def define_sender(company)
    'Notificaciones EXA' + ' <'+ company.notifications_email+'>'
  end

end