module MedlicModule

  def is_there_a_license(date_to_e, user_id)
    return [] unless active_module?
    MedlicLicense.license_for(date_to_e, user_id)
  end

  def be_license(begin_date, end_date, user_id)
    return [] unless active_module?
    MedlicLicense.be_license(begin_date,end_date , user_id)
  end

  def get_medlic_process; @process = MedlicProcess.find(params[:medlic_process_id]) end

  def active_module
    return if active_module?
    flash[:danger] = t('views.medlic_module.flash_messages.no_privilages_danger')
    redirect_to root_path
  end

  def medlic_manager
    return if medlic_manager?(user_connected.id)
    flash[:danger] = t('views.medlic_module.flash_messages.no_privilages_danger')
    redirect_to root_path
  end

  def medlic_manager?(user_id)
    return false unless active_module?
    ct_module = CtModule.where('cod = ? AND active = ?', 'medlic', true).first
    ct_module.ct_module_managers.where('user_id = ?', user_id).count == 1
  end

  def active_module?
    ct_module = CtModule.where('cod = ? AND active = ?', 'medlic', true).first
    !ct_module.nil?
  end

  def set_registered(obj)
    obj.registered_at = lms_time
    obj.registered_by_user_id = user_connected.id
    obj
  end

  def deactivate(obj)
    obj.active = false
    obj.deactivated_at = lms_time
    obj.deactivated_by_user_id = user_connected.id
    obj
  end

  def verify_excel(params)
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
    return error_message unless params
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
    return error_message unless File.extname(params['datafile'].original_filename) == '.xlsx'
    ''
  end

  def get_temporal_file(params)
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal
    path = File.join(directorio_temporal, nombre_temporal)
    File.open(path, 'wb') { |f| f.write(params['datafile'].read) }
    archivo_temporal
  end

  def set_licenses(report)
    licenses = get_licenses_for(report)
    licenses.each { |license| MedlicReportedLicense.create(medlic_license_id: license.id,
                                                           medlic_report_id: report.id,
                                                           registered_at:lms_time,
                                                           registered_by_user_id: user_connected.id) }
  end

  def get_licenses_for(report)
    if report.partial
      licenses = report.medlic_process.pending_paid_license
      licenses = report.medlic_process.la_licenses_reported_partially(report.id) unless licenses.size > 0
    else
      licenses = report.medlic_process.medlic_licenses
    end

    licenses.each do |license|
      next if license.sent_to_payroll
      license.sent_to_payroll = true
      license.sent_to_payroll_at = lms_time
      license.sent_to_payroll_by_user_id = user_connected.id
      license.save
    end

    licenses
  end

  def its_safe_licenses(temp_file, process)
    should_save = true
    @excel_errors = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']
    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1] && !row[2] && !row[3] && !row[4] && !row[5] && !row[6] && !row[7] && !row[8] && !row[9] && !row[10] && !row[11] && !row[12]
      user_cod = row[0].to_s.delete('-').delete(' ').to_s
      user = User.where(codigo: user_cod).first

      unless user
        should_save = false
        @excel_errors << [index + 1, 'No se encuentra usuario', row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12]]
      end

      if should_save
        cc = row[2]
        cc_description = row[3]
        license_number = row[4]
        continuous = row[5].to_s == 'S'
        days = row[6].to_i
        date_begin = Date.new(1899, 12, 30) + row[7].to_f
        date_end = Date.new(1899, 12, 30) + row[8].to_f
        absence_type = row[9].to_s
        specialty = row[10].to_s
        company = row[11].to_s
        status = row[12].to_s

        license = MedlicLicense.where(user_id: user.id, date_begin: date_begin).first
        unless license
          license = MedlicLicense.new
          license.user_id = user.id
          license.medlic_process_id = process.id
          license.cc = cc
          license.cc_description = cc_description
          license.company = company
          license.license_number = license_number
          license.continuous = continuous
          license.days = days
          license.date_begin = date_begin
          license.date_end = date_end
          license.absence_type = absence_type
          license.specialty = specialty
          license.user_status = status
          license = set_registered(license)
          license.save

          pivot = (license.date_begin.year.to_s + '-01-01').to_date
          loop do
            days = 0
            year_days = MedlicYearDaysLicense.where(user_id: user.id, year: pivot.year).first_or_create

            begin_lics = license.date_begin.to_date
            end_lics = license.date_end.to_date
            begin_date = (pivot.year.to_s + '-01-01').to_date
            end_date = (pivot.year.to_s + '-12-31').to_date

            begin_period = begin_date.to_date
            end_period = end_date.to_date

            max_begin_date = begin_period > begin_lics ? begin_period : begin_lics
            min_end_date = end_period < end_lics ? end_period : end_lics
            days += (min_end_date - max_begin_date).to_i + 1

            year_days.days += days
            year_days.save

            pivot += 1.year
            break if pivot > license.date_end
          end
        end
      end
    end
    should_save
  end

  def save_array_objs(array)
    array.each { |obj| obj.save }
  end

end