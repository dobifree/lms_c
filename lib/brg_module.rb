module BrgModule

  def verify_active_bonus
    @bonus = BrgBonus.find(params[:brg_bonus_id])
    return if @bonus.active
    flash[:danger] = 'No puede modificar bono inactivo'
    redirect_to(root_path)
  end

  def number_as_percentage(num)
    return '' unless num
    (((num.to_d*100).to_d.truncate(2)).to_s + ' %')
  end

  def reprocess_this(process)
    process.brg_bonuses.active.each { |bon| deactivate(bon).save }
    create_bonus_for_active_groups(process.id)
  end

  def get_process
    @process = BrgProcess.find(params[:brg_process_id])
  end

  def get_process_for_bonus
    @process = BrgProcess.find(BrgBonus.find(params[:brg_bonus_id]).brg_process_id)
  end

  def get_manager
    @manager = BrgManager.active.by_user(user_connected.id).first
    return if @manager
    flash[:danger] = t('views.brg_module.flash_messages.no_privilages_danger')
    redirect_to(root_path)
  end

  def get_group
    @group = BrgGroup.find(params[:brg_group_id])
  end

  def get_group_user
    @group_user = BrgGroupUser.find(params[:brg_group_user_id])
  end

  def get_bonus
    @bonus = BrgBonus.find(params[:brg_bonus_id])
  end

  def generate_brg_lvl_percentages
    lvl_percentages = []
    JjCharacteristic.brc_characteristic_values.each { |val|
      lvl_percentages << BrgLvlPercentage.new(registered_at: lms_time,
                                              registered_by_user_id: user_connected.id,
                                              jj_characteristic_value_id:val.id) }
    lvl_percentages
  end

  def generate_brg_char_percentages
    char_percentages = []
    characteristic = Characteristic.get_characteristic_company
    values = characteristic ? characteristic.characteristic_values.where(active: true).all.sort_by(&:value_string) : []
    values.each {|val|
      char_percentages << BrgCharPercentage.new(registered_at: lms_time,
                                               registered_by_user_id: user_connected.id,
                                               characteristic_value_id: val.id)}
    char_percentages
  end

  def bpc_conditional_delete(pc_id)
    pc = BrgPercentageConversion.find(pc_id)
    bps = BrgBonusPercentage.by_pc_id(pc_id).all
    if bps.size > 0
      deactivate(pc).save
    else
      pc.destroy
    end
  end

  def active_module
    return if active_module?
    flash[:danger] = t('views.brg_module.flash_messages.no_privilages_danger')
    redirect_to(root_path)
  end

  def brg_manager
    return if brg_manager?(user_connected.id)
    flash[:danger] = t('views.brg_module.flash_messages.no_privilages_danger')
    redirect_to(root_path)
  end

  def change_pay(bonus_id, paid_description, new_value)
    bonus = BrgBonus.find(bonus_id)
    new_bonus = set_registered(bonus.dup)
    deactivate(bonus).save
    new_bonus.prev_bonus_id = bonus.id
    new_bonus.paid = new_value
    new_bonus.paid_at = lms_time
    new_bonus.paid_description = paid_description
    new_bonus.save
    new_bonus
  end

  def im_next_manager_of(bonus_id)
    bonus = BrgBonus.find(bonus_id)
    managers = bonus.next_managers
    if managers.size > 0
      managers.each do |manager|
        return true if manager.user_id == user_connected.id
      end
    end
    false
  end

  def verify_open_process_process
    process = BrgProcess.find(params[:brg_process_id])
    return unless process.closed?
    flash[:danger] = t('views.brg_module.flash_messages.closed_process')
    redirect_to brg_processes_manage_path(process)
  end

  def verify_im_next_manager
    return if im_next_manager_of(params[:brg_bonus_id])
    flash[:danger] = t('views.brg_module.flash_messages.no_privilages_danger')
    redirect_to(root_path)
  end

  def brg_manager?(user_id)
    return false unless active_module?
    ct_module = CtModule.where('cod = ? AND active = ?', 'brg', true).first
    ct_module.ct_module_managers.where('user_id = ?', user_id).count == 1
  end

  def active_module?
    ct_module = CtModule.where('cod = ? AND active = ?', 'brg', true).first
    !ct_module.nil?
  end

  def set_bonuses(report)
    bonuses = get_bonuses_for(report)
    bonuses.each { |bonus| BrgReportedBonus.create(brg_bonus_id: bonus.id, brg_report_id: report.id) }
  end

  def get_bonuses_for(report)
    if report.partial_report
      bonuses = report.brg_process.pending_paid_bonus
      bonuses = report.brg_process.latest_report_partially_bonus unless bonuses.size > 0
    else
      bonuses = report.brg_process.no_pending_manager_bonuses
    end

    bonuses.each do |bonus|
      next if bonus.paid
      bonus.paid = true
      bonus.paid_at = lms_time
      bonus.paid_by_user_id = user_connected.id
      bonus.paid_description = 'Reporte completo generado por: ' + user_connected.codigo + ' - ' + user_connected.comma_full_name unless report.partial_report
      bonus.paid_description = 'Reporte parcial generado por: ' + user_connected.codigo + ' - ' + user_connected.comma_full_name if report.partial_report
      bonus.save
    end

    bonuses
  end

  def set_approve_bonus(bonus)
    bonus.ready_to_go = true
    bonus.ready_to_go_at = lms_time
    bonus
  end

  def set_registered(obj)
    obj.registered_at = lms_time
    obj.registered_by_user_id = user_connected.id
    obj
  end

  def set_actual_value(bonus)
    return bonus unless bonus.registered_value
    bonus.actual_value = bonus.registered_value
    bonus
  end

  def deactivate(obj)
    obj.active = false
    obj.deactivated_at = lms_time
    obj.deactivated_by_user_id = user_connected.id
    obj
  end

  def verify_excel(params)
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_no_file')
    return error_message unless params
    error_message = t('activerecord.success.model.ben_cellphone_number.massive_update_not_xlsx')
    return error_message unless File.extname(params['datafile'].original_filename) == '.xlsx'
    ''
  end

  def get_temporal_file(params)
    directorio_temporal = '/tmp/'
    nombre_temporal = SecureRandom.hex(5) + '.xlsx'
    archivo_temporal = directorio_temporal + nombre_temporal
    path = File.join(directorio_temporal, nombre_temporal)
    File.open(path, 'wb') { |f| f.write(params['datafile'].read) }
    archivo_temporal
  end

  def its_safe_groups(temp_file, process_id)
    should_save = true
    to_save = []
    @excel_errors = []
    all_groups = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-']
    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1] && !row[2]
      group_cod = row[0].to_s
      name = row[1].to_s
      description = row[2].to_s
      if (!group_cod || group_cod.empty?) && name
        group_cod = (BrgGroup.last_id.to_i+index+1).to_s+name
      end

      group = BrgGroup.new(group_cod: group_cod, name: name, description: description, brg_process_id: process_id)
      group = set_registered(group)

      all_groups << group

      if group.valid?
        to_save << group
      else
        should_save = false
        group.errors.full_messages.each do |error|
          @excel_errors << [(index + 1).to_s, error, group_cod, row[1], row[2]]
        end
      end
    end

    all_groups.each_with_index do |group, index|
      all_groups.each_with_index do |aux_group, aux_index|
        next unless aux_group.group_cod == group.group_cod && index != aux_index
        @excel_errors << [(index + 1).to_s, 'Código duplicado en línea: ' + (aux_index + 1).to_s, group.group_cod, group.name, group.description]
        should_save = false
      end
    end

    to_save.each { |group| group.save } if should_save
    should_save
  end

  def its_safe_group_users(temp_file, process_id)
    should_save = true
    to_save = []
    @excel_errors = []
    all_relations = []
    excel_values = []
    process = BrgProcess.find(process_id)

    begin
      file = RubyXL::Parser.parse temp_file
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-']
    end

    fixed_comments = []
    bonus_targets = []
    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1]
      user_cod = row[0].to_s
      group_cod = row[1].to_s
      fixed_comments << row[3].to_s
      bonus_targets << row[4].to_s

      raw_information = []
      raw_information << row[0].to_s
      raw_information << row[1].to_s
      raw_information << row[2].to_s
      raw_information << row[3].to_s
      raw_information << row[4].to_s

      user_id = user_cod && !user_cod.empty? ? User.where(codigo: user_cod).first : nil
      group_id = group_cod && !group_cod.empty? ? BrgGroup.where(group_cod: group_cod, brg_process_id: process_id).first : nil

      user_id = user_id.id if user_id
      group_id = group_id.id if group_id

      group_user = BrgGroupUser.new(user_id: user_id, brg_group_id: group_id)
      group_user = set_registered(group_user)

      all_relations << group_user

      if group_user.valid?
        to_save << group_user
        excel_values << (row[2].to_s.blank? ? '' : row[2].to_i)
      else
        should_save = false
        group_user.errors.full_messages.each do |error|
          @excel_errors << [(index + 1).to_s, error] + raw_information
        end
      end
    end

    all_relations.each_with_index do |assoc, index|
      next unless assoc.user
      all_relations.each_with_index do |aux_assoc, aux_index|
        next unless aux_assoc.user
        next unless assoc.user_id == aux_assoc.user_id && index != aux_index
        @excel_errors << ([(index + 1).to_s, 'Código de usuario duplicado en línea: ' + (aux_index + 1).to_s] + raw_information)
        should_save = false
      end
    end

    if should_save
      process = BrgProcess.find(process_id)
      to_save.each_with_index do |group_user, index|
        group_user.save

        unless fixed_comments[index].blank?
          brg_group_user_comment = BrgGroupUserComment.new(brg_group_user_id: group_user.id, comment: fixed_comments[index])
          brg_group_user_comment = set_registered(brg_group_user_comment)
          brg_group_user_comment.save
        end

        value_array = (excel_values[index].blank? ? set_auto_value(process_id, group_user.user_id) : [excel_values[index].to_i, 'Valor cargado por gestor', nil])

        begin_period = lms_time.change(year: process.reference_year, month:01, day: 01, hour:00, min: 00, sec: 00)
        end_period = lms_time.change(year: process.reference_year, month:12, day: 31, hour:23, min:59, sec: 59)

        filter = (excel_values[index].blank? ? (apply_medlic_filter_for(begin_period, end_period, group_user.user_id) * apply_entry_for(process.reference_year,  group_user.user_id)) : 1)

        bonus = BrgBonus.new(brg_process_id: process_id,
                             brg_group_user_id: group_user.id,
                             brg_lvl_percentage_id: value_array[2] ? value_array[2].id : nil,
                             actual_value: value_array[0] * filter,
                             registered_value: value_array[0],
                             registered_description: value_array[1])
        bonus.first_value = bonus.actual_value
        bonus.bonus_target = bonus_targets[index].to_d if process.private?
        bonus = set_registered(bonus)
        bonus.save
      end
    end
    should_save
  end

  def apply_medlic_filter_for(begin_date, end_date, user_id)
    # [01/Enero - 31/Diciembre] -> Período de comparación.
    # Días de licencias médicas <= 90, bono completo.
    # Días de licencias médicas > 90, proporcional de días trabajados con una base de 365 días al año.
    # days = licenses_days_for_btw_faster(begin_date, end_date, user_id)
    days = MedlicYearDaysLicense.where(year: begin_date.to_date.year, user_id: user_id).pluck(:days).first
    days = days ? days.to_f : 0
    return 1 if days <= 90
    ((365-days.to_f)/365).to_f
  end

  def licenses_days_for_btw(begin_date, end_date, user_id)
    days = 0
    licenses = MedlicLicense.be_license(begin_date, end_date, user_id)
    licenses.each { |license| days += license.days }
    days
  end

  def licenses_days_for_btw_faster(begin_date, end_date, user_id)
    begin_date = begin_date.to_date
    end_date = end_date.to_date
    licenses = MedlicLicense.by_user(user_id).where("date_begin <= '"+end_date.strftime('%Y-%m-%d')+"'", "date_end => '"+begin_date.strftime('%Y-%m-%d')+"'")
    pivot = begin_date
    days = 0

    loop do
      licenses.each do |license|
        max_begin_date = begin_date > license.date_begin ? begin_date : license.date_begin
        min_end_date = end_date < license.date_end ? end_date : license.date_end
        next unless max_begin_date <= pivot && pivot <= min_end_date
        days += 1
      end
      pivot += 1.day
      break if pivot > end_date.to_date
    end
    days
  end

  def apply_entry_for(year_to_evaluate, user_id)
    # Si ingresó a trabajar el mismo año del proceso
    # [01/Enero - 31/Marzo] => Bono completo.
    # [01/Abril - 30/Junio] => Proporcional de días trabajados en base a un año de 365 días. ¿Toma algún criterio?
    # [01/Julio - Adelante] => No cobra bono.
    user = User.find(user_id)
    work_entry = user.user_characteristic(Characteristic.get_characteristic_from_date_work).value

    first_date = Date.new(year_to_evaluate, 03, 31)
    second_date = Date.new(year_to_evaluate, 07, 01)

    return 1 if work_entry <= first_date
    return 0 if work_entry >= second_date
    ((Date.new(year_to_evaluate, 12, 31) - work_entry)/365.to_f)
  end

  def its_safe_managers(temp_file, process_id)
    should_save = true
    to_save = []
    @excel_errors = []
    all_objs = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-']
    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1]
      user_cod = row[0].to_s
      description = row[1].to_s

      user_id = user_cod && !user_cod.empty? ? User.where(codigo: user_cod).first : nil
      user_id = user_id.id if user_id

      manager = BrgManager.new(user_id: user_id, description: description, brg_process_id: process_id)
      manager = set_registered(manager)

      all_objs << manager

      if manager.valid?
        to_save << manager
      else
        should_save = false
        manager.errors.full_messages.each do |error|
          @excel_errors << [(index + 1).to_s, error, row[0], row[1], row[2]]
        end
      end
    end

    to_save.each { |group| group.save } if should_save
    should_save
  end

  def its_safe_process_users temp_file, process_id
    should_save = true
    to_save = []
    @excel_errors = []
    all_objs = []
    process = BrgProcess.find(process_id)
    private = process.private?

    begin
      file = RubyXL::Parser.parse temp_file
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-', '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-', '-', '-']
    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1] && !row[2] && !row[3]
      user_cod = row[0].to_s
      new_value = row[3].to_d
      fixed_comment = row[4]

      information_row = []
      information_row << row[0]
      information_row << row[1]
      information_row << row[2]
      information_row << row[3]
      information_row << row[4]
      information_row << row[5] if private

      prev_bonus = nil
      this_should_save = true
      group_user = nil

      user_id = user_cod && !user_cod.empty? ? User.where(codigo: user_cod).first : nil
      user_id = user_id.id if user_id

      if user_id
        prev_bonus = BrgBonus.active.process(process_id).of_user(user_id).first
        group_user = BrgGroupUser.by_process(process_id).by_user(user_id).first
      else
        @excel_errors << [(index + 1).to_s, 'Usuario no encontrado'] + information_row
        should_save = false
        this_should_save = false
      end

      unless prev_bonus
        @excel_errors << [(index + 1).to_s, 'Usuario sin bono activo registrado'] + information_row
        should_save = false
        this_should_save = false
      end

      if user_id && !group_user
        @excel_errors << [(index + 1).to_s, 'Usuario sin grupo activo registrado dentro del proceso'] + information_row
        should_save = false
        this_should_save = false
      end

      if this_should_save && prev_bonus && new_value && group_user && new_value != prev_bonus.actual_value
        bonus = BrgBonus.new(brg_process_id: process_id,
                             brg_group_user_id: group_user.id,
                             brg_lvl_percentage_id: nil,
                             actual_value: new_value,
                             registered_value: new_value,
                             registered_description: 'Valor cargado por gestor: '+user_connected.comma_full_name)
        bonus.first_value = bonus.actual_value
        bonus.bonus_target = row[5].to_d if private
        bonus = set_registered(bonus)
        if bonus.save
          prev_bonus = deactivate(prev_bonus)
          prev_bonus.save

          unless fixed_comment.blank?
            brg_group_user_comment = BrgGroupUserComment.new(brg_group_user_id: group_user.id, comment: fixed_comment)
            brg_group_user_comment = set_registered(brg_group_user_comment)
            brg_group_user_comment.save
          end

        else
          should_save = false
          bonus.errors.full_messages.each do |error|
            @excel_errors << [(index + 1).to_s, error] + information_row
          end
        end
      end
    end
    should_save
  end

  def its_safe_groups_assoc(temp_file, process_id)
    should_save = true
    to_save = []
    @excel_errors = []
    all_objs = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-']
    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1]
      group_cod = row[0].to_s
      user_cod = row[1].to_s

      user_id = user_cod && !user_cod.empty? ? User.where(codigo: user_cod).first : nil
      user_id = user_id.id if user_id

      manager_id = BrgManager.where(user_id: user_id, active: true, brg_process_id: process_id).first
      manager_id = manager_id.id if manager_id

      group_id = group_cod && !group_cod.empty? ? BrgGroup.where(group_cod: group_cod, brg_process_id: process_id).first : nil
      group_id = group_id.id if group_id

      unless manager_id
        @excel_errors << [(index + 1).to_s, t('views.brg_module.errors.manager_no_registered'), row[0], row[1]]
      end

      unless group_id
        @excel_errors << [(index + 1).to_s, t('views.brg_module.errors.group_no_evists'), row[0], row[1]]
      end

      relation = BrgManageRelation.new(group_to_manage_id: group_id, manager_id: manager_id)
      relation = set_registered(relation)

      all_objs << relation

      if relation.valid?
        to_save << relation
      else
        should_save = false
        relation.errors.full_messages.each do |error|
          @excel_errors << [(index + 1).to_s, error, row[0], row[1]]
        end
      end
    end

    to_save.each { |rel| rel.save } if should_save
    should_save
  end

  def its_safe_managees_assoc(temp_file, process_id)
    should_save = true
    to_save = []
    @excel_errors = []
    all_objs = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue Exception => e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-']
    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1]
      user_cod = row[0].to_s
      user_cod_1 = row[1].to_s

      user_id = user_cod && !user_cod.empty? ? User.where(codigo: user_cod).first : nil
      user_id = user_id.id if user_id

      manager_id = BrgManager.where(user_id: user_id, active: true, brg_process_id: process_id).first
      manager_id = manager_id.id if manager_id

      user_id_1 = user_cod_1 && !user_cod_1.empty? ? User.where(codigo: user_cod_1).first : nil
      user_id_1 = user_id_1.id if user_id_1

      manager_id_1 = BrgManager.where(user_id: user_id_1, active: true, brg_process_id: process_id).first
      manager_id_1 = manager_id_1.id if manager_id_1

      unless manager_id_1
        @excel_errors << [(index + 1).to_s, t('views.brg_module.errors.manager_no_registered'), row[0], row[1]]
      end

      unless manager_id
        @excel_errors << [(index + 1).to_s, t('views.brg_module.errors.group_no_evists'), row[0], row[1]]
      end

      relation = BrgManageRelation.new(managee_id: manager_id, manager_id: manager_id_1 )
      relation = set_registered(relation)

      all_objs << relation

      if relation.valid?
        to_save << relation
      else
        should_save = false
        relation.errors.full_messages.each do |error|
          @excel_errors << [(index + 1).to_s, error, row[0], row[1]]
        end
      end
    end

    to_save.each { |rel| rel.save } if should_save
    should_save
  end

  def its_safe_percentage_conversion(temp_file, process_id)
    should_save = true
    to_save = []
    @excel_errors = []
    all_objs = []

    begin
      file = RubyXL::Parser.parse temp_file
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.format_error'), '-', '-']
    end

    begin
      file = RubyXL::Parser.parse temp_file
      data = file.worksheets[0].extract_data
    rescue e
      data = []
      should_save = false
      @excel_errors << ['-', t('views.ben_cellphone_number.error.formula_error'), '-', '-']
    end

    data.each_with_index do |row, index|
      next if index.zero?
      break if !row[0] && !row[1]&& !row[2]&& !row[3]&& !row[4]

      pc = BrgPercentageConversion.new(before: row[0],
                                       after: row[1],
                                       type_1: row[2],
                                       individual: row[3],
                                       group: row[4],
                                       brg_process_id: process_id)
      pc = set_registered(pc)
      all_objs << pc

      if pc.valid?
        to_save << pc
      else
        should_save = false
        pc.errors.full_messages.each do |error|
          @excel_errors << [(index + 1).to_s, error, row[0], row[1], row[2], row[3], row[4]]
        end
      end
    end

    to_save.each { |rel| rel.save } if should_save
    should_save
  end

  def gen_bonus_for_active_groups(process_id)
    process = BrgProcess.find(process_id)
    all_bonus = []
    group_users = BrgGroupUser.by_process(process_id).active.active_groups
    group_users.each do | gu |
      value_array = set_auto_value(process_id, gu.user_id)

      begin_period = lms_time.change(year: process.reference_year, month:01, day: 01, hour:00, min: 00, sec: 00)
      end_period = lms_time.change(year: process.reference_year, month:12, day: 31, hour:23, min:59, sec: 59)

      filter = apply_medlic_filter_for(begin_period, end_period, gu.user_id) * apply_entry_for(process.reference_year,  gu.user_id)
      bonus = BrgBonus.new(brg_process_id: process_id,
                           brg_group_user_id: gu.id,
                           brg_lvl_percentage_id: value_array[2] ? value_array[2].id : nil,
                           actual_value: value_array[0].to_d * filter,
                           registered_value: value_array[0],
                           registered_description: value_array[1])
      bonus.first_value = bonus.actual_value
      bonus = set_registered(bonus)
      all_bonus << bonus
    end
    all_bonus
  end

  def reprocess_this_bonus(id)
    bonus = BrgBonus.find(id)
    return nil unless bonus.brg_process.active
    return nil if bonus.brg_process.closed?
    return nil unless bonus.active

    value_array = set_auto_value(bonus.brg_process_id, bonus.user_id)

    begin_period = lms_time.change(year: bonus.brg_process.reference_year, month: 01, day: 01, hour: 00, min: 00, sec: 00)
    end_period = lms_time.change(year: bonus.brg_process.reference_year, month: 12, day: 31, hour: 23, min: 59, sec: 59)

    medlic_filter = apply_medlic_filter_for(begin_period, end_period, bonus.user_id)
    entry_filter = apply_entry_for(bonus.brg_process.reference_year, bonus.user_id)

    filter = medlic_filter * entry_filter

    new_bonus = bonus.dup
    new_bonus.brg_lvl_percentage_id = value_array[2] ? value_array[2].id : nil
    new_bonus.actual_value = value_array[0].to_d * filter
    new_bonus.registered_value = value_array[0].to_d * filter
    new_bonus.registered_description = 'Bono reprocesado por: ' + user_connected.comma_full_name
    new_bonus = set_registered(new_bonus)

    return new_bonus unless new_bonus.valid?
    new_bonus.save
    bonus = deactivate(bonus)
    bonus.save
    new_bonus
  end


  def save_array_objs(array)
    array.each { |obj| obj.save }
  end

  def create_bonus_for_active_groups(process_id)
    save_array_objs(gen_bonus_for_active_groups(process_id))
  end

  def set_auto_value(process_id, user_id)
    process = BrgProcess.find(process_id)
    company_result = process.company_result_for(user_id)
    quali_result = process.quali_result_for_user(user_id)
    quanti_result = process.quanti_result_for_user(user_id)
    lvl_percentage = process.lvl_percentage_for(user_id)

    if company_result && quali_result && quanti_result && lvl_percentage
      indv_val = (quali_result * process.quali_percentage + quanti_result * process.quanti_percentage) * lvl_percentage.pond_individual
      company_val = company_result * lvl_percentage.pond_company
      value = (indv_val + company_val) * lvl_percentage.max_prt_bonus
      value = get_bonus_amount_for(process, user_id) * value
      [value, t('views.brg_module.messages.auto_value_description'), lvl_percentage]
    else
      [0, t('views.brg_module.messages.auto_value_description_gotten_nil'), lvl_percentage]
    end
  end

  def best_auto_value(process_id, user_id)
    process = BrgProcess.find(process_id)
    company_result = 1
    quali_result = 1
    quanti_result = 1
    lvl_percentage = process.lvl_percentage_for(user_id)
    if company_result && quali_result && quanti_result && lvl_percentage
      indv_val = (quali_result * process.quali_percentage + quanti_result * process.quanti_percentage) * lvl_percentage.pond_individual
      company_val = company_result * lvl_percentage.pond_company
      value = (indv_val + company_val) * lvl_percentage.max_prt_bonus
      value = get_bonus_amount_for(process, user_id) * value
      [value, t('views.brg_module.messages.auto_value_description'), lvl_percentage]
    else
      [0, t('views.brg_module.messages.auto_value_description_gotten_nil'), lvl_percentage]
    end
  end

  def get_bonus_amount_for(brg_process, user_id)
    subaso = 0
    user = User.find(user_id)
    liq_process = LiqProcess.liq_process_by_year_month brg_process.reference_year,12
    subaso = liq_process.get_subaso_brc_user(user) if liq_process
    subaso ? subaso : 0
  end

end