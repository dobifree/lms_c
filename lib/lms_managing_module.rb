module LmsManagingModule

  def massive_charge_programs_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        fila[0] = fila[0].to_s

        program = Program.find_by_codigo fila[0]

        unless program

          program = Program.new
          program.codigo = fila[0]
          program.color = '000000'

        end

        program.nombre = fila[1]
        program.descripcion = fila[2]

        unless program.save

          @lineas_error.push index+1
          @lineas_error_detalle.push ('codigo: '+fila[0])
          @lineas_error_messages.push ['No se pudo guardar el programa: '+program.errors.full_messages.to_s]

        end

      end

    end

  end

  def massive_charge_programs_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_programas')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'Código'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Descripción'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def massive_charge_courses_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        fila[0] = fila[0].to_s

        course = Course.find_by_codigo fila[0]

        unless course

          course = Course.new
          course.codigo = fila[0]
          course.color = '000000'

        end

        course.nombre = fila[1]
        course.descripcion = fila[2]

        unless course.save

          @lineas_error.push index+1
          @lineas_error_detalle.push ('codigo: '+fila[0])
          @lineas_error_messages.push ['No se pudo guardar el coursea: '+course.errors.full_messages.to_s]

        end

      end

    end

  end

  def massive_charge_courses_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_courseas')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'Código'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Descripción'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end
  
end