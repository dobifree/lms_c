module VacsRuleRecordCrudModule
  include VacRecordsRegisterHelper

  def save_benefits_for(condition_vacations, request, who)
    case who
    when 'manager'
      manager_assoc_benefit(condition_vacations, request)
    when 'register'
      register_assoc_benefit(condition_vacations, request)
    else
      register_assoc_benefit(condition_vacations, request)
    end
  end

  private

  def manager_assoc_benefit(condition_vacations, request)
    condition_vacations.each do |condition_vacation|
      vac_rule_record = VacRuleRecord.new(vac_request_id: request.id, bp_condition_vacation_id: condition_vacation.id)
      vac_rule_record = set_registered(vac_rule_record)
      vac_rule_record = set_dates_for(vac_rule_record, request)
      vac_rule_record.save
    end
    update_end_vacations(request.id)
  end

  def register_assoc_benefit(benefit_ids, request)
  end

  def set_registered(obj)
    obj.registered_at = lms_time
    obj.registered_by_user = user_connected
    obj
  end

  def set_dates_for(vac_rule_record, request)
    return vac_rule_record unless vac_rule_record.bp_condition_vacation.bonus_days
    vac_rule_record.days_begin = go_back_vacations_date(request.id)
    vac_rule_record.days_end = vacations_end(vac_rule_record.bp_condition_vacation.bonus_only_laborable_day,
                                             vac_rule_record.days_begin,
                                             vac_rule_record.bp_condition_vacation.bonus_days,
                                             request.user_id)
    vac_rule_record
  end
end