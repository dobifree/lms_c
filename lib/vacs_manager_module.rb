module VacsManagerModule
  def verify_i_am_manager
    return if am_i_manager?
    flash[:danger] = t('security.no_access_to_manage_module')
    redirect_to root_path
  end

  def am_i_manager?
    ct_module = CtModule.where('cod = ? AND active = ?', 'vacs', true).first
    return false unless ct_module
    return false unless ct_module.has_managers?
    return false unless ct_module.ct_module_managers.where(user_id: user_connected.id).first
    true
  end
end