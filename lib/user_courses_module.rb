module UserCoursesModule

  def finish_course(user_course)
    recien_finalizado = false
    fue_aprobado = false
    tiene_oportunidades = false

    estado = user_course.estado lms_time

    if !user_course.finalizado && estado == :en_progreso

      units = user_course.course.units
      evaluations = user_course.course.evaluations
      valid_evaluations = user_course.course.evaluations.where('weight > 0')
      polls = user_course.course.polls.where('obligatoria = ? and virtual = ?', true, true)

      todo_terminado_units = true
      todo_terminado_evals = true
      todo_terminado_polls = true

      units.each do |unit|
        uc_unit = user_course.user_course_units.find_by_unit_id(unit.id)
        todo_terminado_units = false unless uc_unit && uc_unit.finalizada
        break unless todo_terminado_units
      end

      evaluations.each do |evaluation|
        uc_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(evaluation.id)
        todo_terminado_evals = false unless uc_evaluation && uc_evaluation.finalizada
        break unless todo_terminado_evals
      end

      polls.each do |poll|
        uc_poll = user_course.user_course_polls.find_by_poll_id(poll.id)
        todo_terminado_polls = false unless uc_poll && uc_poll.finalizada
        break unless todo_terminado_polls
      end

      todo_terminado_units = true if user_course.asistencia_unidades >= user_course.course.asistencia_minima

      if todo_terminado_units && todo_terminado_evals && todo_terminado_polls

        nota = nil

        if valid_evaluations.length > 0
          suma = 0
          suma_pesos = 0
          valid_evaluations.each do |evaluation|
            uc_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(evaluation.id)
            suma += uc_evaluation.nota*evaluation.weight
            suma_pesos += evaluation.weight
          end
          nota = suma*1.0/suma_pesos*1.0
        end

        user_course.fin = lms_time
        user_course.finalizado = true
        user_course.nota = nota

        user_course.aprobado = true

        if nota && nota < user_course.course.nota_minima
          user_course.aprobado = false
        end

        if user_course.course.dncp

          user_course.asistencia = true unless user_course.course.dncp.asistencia_presencial

        else
          user_course.asistencia = true
        end

        if user_course.save

          recien_finalizado = true
          fue_aprobado = user_course.aprobado

          if user_course.program_course.program.especifico

            unless user_course.aprobado

              if user_course.course.numero_oportunidades - user_course.numero_oportunidad > 0

                new_user_course = UserCourse.new
                new_user_course.enrollment = user_course.enrollment
                new_user_course.program_course = user_course.program_course
                new_user_course.course = user_course.course

                new_user_course.desde = lms_time
                new_user_course.hasta = user_course.hasta

                new_user_course.numero_oportunidad = user_course.numero_oportunidad+1

                new_user_course.aplica_sence = user_course.aplica_sence

                new_user_course.save

                tiene_oportunidades = true

              end

            end
          else
            unless user_course.aprobado

              if user_course.course.numero_oportunidades - user_course.numero_oportunidad > 0
                tiene_oportunidades = true
              end

            end
          end

        end

      end

    end

    return recien_finalizado, fue_aprobado, tiene_oportunidades

  end

  def update_percentage_advance(user_course)

    numero_elementos = user_course.course.units.count + user_course.course.evaluations.count + user_course.course.polls.where('obligatoria = true ').count

    numero_unidades = user_course.user_course_units.where('finalizada = true').count
    numero_evaluaciones = user_course.user_course_evaluations.where('finalizada = true').count
    numero_encuestas = user_course.user_course_polls.joins(:poll).where('obligatoria = true AND finalizada = true').count

    porcentaje_avance = 100

    if numero_elementos > 0

      porcentaje_avance = (numero_unidades + numero_evaluaciones +numero_encuestas)*100.0 / numero_elementos.to_f

    end

    numero_unidades_total = user_course.course.units.count

    asistencia_unidades = 100

    if numero_unidades_total > 0

      if user_course.program_course_instance && user_course.program_course_instance.program_course_instance_units.first

        t_t = 0
        t_a = 0

        user_course.program_course_instance.program_course_instance_units.each do |pciu|
          t_t += pciu.to_date - pciu.from_date
          ucu = user_course.user_course_units.where('finalizada = ? AND unit_id = ?', true, pciu.unit_id).first
          t_a += pciu.to_date - pciu.from_date if ucu
        end

        asistencia_unidades = t_a*100.0/t_t.to_f

      else

        asistencia_unidades = numero_unidades*100.0/numero_unidades_total.to_f

      end

    end

    user_course.update_attributes(:porcentaje_avance => porcentaje_avance, :asistencia_unidades => asistencia_unidades)

  end

end