module ScheduleDateModule

  def assoc_indef_schedule_to (user_id, schedule_id, deactivate_prev)
    #USER_ID : ID DE USUARIO AL QUE SE ASIGNARÁ HORARIO (OBLIGATORIO)
    #SCHEDULE_ID: ID DE HORARIO A ASIGNAR (OBLIGATORIO)
    #DEACTIVATE_PREV: (BOOLEAN) SI ENCUENTRA YA UN HORARIO INDEFINIDO ACTIVO PARA ESE USUARIO, ENTONCES LO DESACTIVA PARA GUARDAR EL NUEVO (TRUE) O NO (FALSE). (OBLIGATORIO)
    #DEVUELVE UN ARRAY CON ERRORES. SI ESTÁ VACÍO TODO SE EJECUTÓ CORRECTAMENTE

    user = User.find(user_id)
    schedule = ScSchedule.find(schedule_id)
    prev_schedule_user = ScScheduleUser.where('user_id = ? and active = true and active_since is null', user_id).first

    if deactivate_prev && prev_schedule_user
      prev_schedule_user.active = false
      prev_schedule_user.deactivated_manually = true
      prev_schedule_user.deactivated_at = lms_time
      prev_schedule_user.deactivated_by_user_id = user_connected.id
      prev_schedule_user.save
    end

    schedule_user = ScScheduleUser.where('user_id = ? and sc_schedule_id = ? and active_since is null', user_id, schedule_id).first
    schedule_user = ScScheduleUser.new(user_id: user.id, sc_schedule_id: schedule.id, active: true) unless schedule_user

    schedule_user.he_able = user.he_able
    schedule_user.registered_at = lms_time
    schedule_user.registered_by_user = user_connected
    schedule_user.active = true
    schedule_user.registered_manually = true
    if schedule_user.save
      []
      else
        if deactivate_prev && prev_schedule_user
          prev_schedule_user.active = true
          prev_schedule_user.deactivated_manually = nil
          prev_schedule_user.deactivated_at = nil
          prev_schedule_user.deactivated_by_user_id = nil
          prev_schedule_user.save
        end
        schedule_user.errors.full_messages
    end
  end

  def assoc_period_schedule_to(user_id, schedule_id, begin_period, end_period)
    desde = begin_period
    hasta = end_period
    user = User.find(user_id)

    schedule_user = ScScheduleUser.new(user_id: user.id,
                                       sc_schedule_id: schedule_id,
                                       active_since: desde,
                                       active_end: hasta,
                                       he_able: user.he_able)
    schedule_user.active = true
    schedule_user.registered_by_user = user_connected
    schedule_user.registered_at = lms_time

    schedule_user.save ? [] : schedule_user.errors.full_messages
  end

  def not_laborable_by_sc?(lab_date, user_id)
    !is_there_a_block?(schedule_of(user_id, lab_date), lab_date)
  end

  def laborable_by_sc?(date_to_eval, user_id)
    is_there_a_block?(schedule_of(user_id, date_to_eval), date_to_eval)
  end

  def sc_for(user_id, date_to_eval)

  end

  def schedule_of(user_id, date)
    schedules_user = ScScheduleUser.where(active: true, user_id: user_id)
    period_time_sche = []
    indef_sche = nil
    date = date.to_date

    schedules_user.each do |schedule_user|
      if schedule_user.active_since && schedule_user.active_since.to_date <= date && schedule_user.active_end.to_date >= date
        period_time_sche << schedule_user
      else
        indef_sche = schedule_user
      end
    end

    if period_time_sche.size > 0
      period_time_sche.sort_by!(&:active_since)
      return period_time_sche.first
    else
      return indef_sche
    end
  end

  def is_there_a_block?(schedule_user, date)
    date_begin = date.to_datetime.change(hour:00, min:00, sec: 00)
    begin_formatted = date_begin.strftime('%H:%M')
    date_end = date.to_datetime.change(hour:23, min:59, sec: 59)
    end_formatted = date_end.strftime('%H:%M')

    blocks = []
    schedule_blocks = schedule_user ? schedule_user.sc_schedule.sc_blocks.where(:active => true) : []
    schedule_blocks.each do |block|
      next_day = block.next_day ? block.day+1 : block.day
        if block.begin_formatted <= end_formatted && block.day <= date_end.wday
          if block.end_formatted >= begin_formatted && next_day >= date_begin.wday
            blocks << block if block.workable
          end
        end
    end

    blocks.size > 0
  end

  def blocks_for(user_id, date)
    schedule_user = schedule_of(user_id, date)
    date_begin = date.to_datetime.change(hour:00, min:00, sec: 00)
    begin_formatted = date_begin.strftime('%H:%M')
    date_end = date.to_datetime.change(hour:23, min:59, sec: 59)
    end_formatted = date_end.strftime('%H:%M')

    blocks = []
    schedule_blocks = schedule_user ? schedule_user.sc_schedule.sc_blocks.where(:active => true) : []
    schedule_blocks.each do |block|
      next_day = block.next_day ? block.day+1 : block.day
      if block.begin_formatted <= end_formatted && block.day <= date_end.wday
        if block.end_formatted >= begin_formatted && next_day >= date_begin.wday
          blocks << block if block.workable
        end
      end
    end
    return blocks
  end

  def next_day_with_sc_block(user_id, date_to_eval)
    #Siguiente día con un bloque laborable
    #inclusive
    #Máximo 1 año de revisión. Si hay más de un 1 año de no feríados continuos, devolverá el día 366.
    pivot = date_to_eval.to_date
    date_end = date_to_eval.to_date + 365.days
    loop do
      return pivot if blocks_for(user_id, pivot).size > 0
      pivot += 1.day
      break if pivot > date_end
    end
    pivot
  end

  def prev_day_with_sc_block(user_id, date_to_eval)
    #Anterior día con un bloque laborable
    #inclusive
    #Máximo 1 año de revisión. Si hay más de un 1 año de no feríados continuos, devolverá el día 366.
    pivot = date_to_eval.to_date
    date_end = date_to_eval.to_date - 365.days
    loop do
      return pivot if blocks_for(user_id, pivot).size > 0
      pivot -= 1.day
      break if pivot < date_end
    end
    pivot
  end

  def next_day_without_sc_block(user_id, date_to_eval)
    #Siguiente día con un bloque laborable
    #inclusive
    #Máximo 1 año de revisión. Si hay más de un 1 año de no feríados continuos, devolverá el día 366.
    pivot = date_to_eval.to_date
    date_end = date_to_eval.to_date + 365.days
    loop do
      return pivot unless blocks_for(user_id, pivot).size > 0
      pivot += 1.day
      break if pivot > date_end
    end
    pivot
  end

  def prev_day_without_sc_block(user_id, date_to_eval)
    #Anterior día con un bloque laborable
    #inclusive
    #Máximo 1 año de revisión. Si hay más de un 1 año de no feríados continuos, devolverá el día 366.
    pivot = date_to_eval.to_date
    date_end = date_to_eval.to_date - 365.days
    loop do
      return pivot unless blocks_for(user_id, pivot).size > 0
      pivot -= 1.day
      break if pivot < date_end
    end
    pivot
  end

end