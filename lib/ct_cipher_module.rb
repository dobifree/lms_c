module CtCipherModule

  require 'openssl'

  def encrypt_string(s, company)

    cipher = OpenSSL::Cipher::AES256.new :CBC
    cipher.encrypt
    cipher.iv = company.iv
    cipher.key = 'ThisPasswordIsReallyHardToGuess!'
    cipher_text = cipher.update(s) + cipher.final
    return cipher_text

  end

  def decrypt_string(s, company)

    decipher = OpenSSL::Cipher::AES256.new :CBC
    decipher.decrypt
    decipher.iv = company.iv
    decipher.key = 'ThisPasswordIsReallyHardToGuess!'
    plain_text = decipher.update(s) + decipher.final

    return plain_text

  end

  def encrypt_int(i, company)

    cipher = OpenSSL::Cipher::AES256.new :CBC
    cipher.encrypt
    cipher.iv = company.iv
    cipher.key = 'ThisPasswordIsReallyHardToGuess!'
    cipher_text = cipher.update(i.to_s) + cipher.final
    return cipher_text

  end

  def decrypt_int(s, company)

    decipher = OpenSSL::Cipher::AES256.new :CBC
    decipher.decrypt
    decipher.iv = company.iv
    decipher.key = 'ThisPasswordIsReallyHardToGuess!'
    plain_text = decipher.update(s) + decipher.final

    return plain_text.to_i

  end

  def encrypt_float(f, company)

    cipher = OpenSSL::Cipher::AES256.new :CBC
    cipher.encrypt
    cipher.iv = company.iv
    cipher.key = 'ThisPasswordIsReallyHardToGuess!'
    cipher_text = cipher.update(f.to_s) + cipher.final
    return cipher_text

  end

  def decrypt_float(s, company)

    decipher = OpenSSL::Cipher::AES256.new :CBC
    decipher.decrypt
    decipher.iv = company.iv
    decipher.key = 'ThisPasswordIsReallyHardToGuess!'
    plain_text = decipher.update(s) + decipher.final

    return plain_text.to_f

  end

end