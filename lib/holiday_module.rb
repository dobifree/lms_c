module HolidayModule

  def user_by_id; @user = User.find(params[:user_id])end
  def get_holiday_by_id; @holiday = Holiday.find(params[:holiday_id]) end
  def get_assoc; @assoc = HolidayUser.find(params[:holiday_user_id]) end

  def char_options
    Characteristic.where(publica: true, data_type_id: [0,1,2,3,4,5]).map {|char| [char.nombre, char.id]}
  end

  def verify_active_assoc_chain
    if verify_active_assoc?(params[:holiday_user_id])
      assoc = HolidayUser.find(params[:holiday_user_id])
      return if verify_active_holiday?(assoc.holiday.id)
      flash[:danger] = t('views.holiday_module.flash_messages.holiday_deactivated')
      redirect_to holiday_index_path(t('views.holiday_module.tabs.manager_index.global'))
    else
      flash[:danger] = t('views.holiday_module.flash_messages.asign_deactivated')
      redirect_to holiday_index_path(t('views.holiday_module.tabs.manager_index.customized'))
    end
  end

  def verify_active_holiday
    return if verify_active_holiday?(params[:holiday_id])
    flash[:danger] = t('views.holiday_module.flash_messages.holiday_deactivated')
    redirect_to holiday_index_path(t('views.holiday_module.tabs.manager_index.global'))
  end

  def verify_active_holiday?(id); Holiday.find(id).active? end
  def verify_active_assoc?(id); HolidayUser.find(id).active? end

  def active_module
    return if active_module?
    flash[:danger] = t('views.holiday_module.flash_messages.no_privilages_danger')
    redirect_to root_path
  end

  def holiday_manager
    return if holiday_manager?(user_connected.id)
    flash[:danger] = t('views.holiday_module.flash_messages.no_privilages_danger')
    redirect_to root_path
  end

  def holiday_manager?(user_id)
    return false unless active_module?
    ct_module = CtModule.where('cod = ? AND active = ?', 'holidays', true).first
    ct_module.ct_module_managers.where('user_id = ?', user_id).count == 1
  end

  def active_module?
    ct_module = CtModule.where('cod = ? AND active = ?', 'holidays', true).first
    !ct_module.nil?
  end

  def set_registered(obj)
    obj.registered_at = lms_time
    obj.registered_by_user_id = user_connected.id
    obj
  end

  def deactivate(obj)
    obj.active = false
    obj.deactivated_at = lms_time
    obj.deactivated_by_user_id = user_connected.id
    obj
  end

  def deactivate_assocs(holiday, save, manually)
    deactivated = []
    holiday.holiday_users.active.each { |hol_user| deactivated << deactivate(hol_user).deactivated_manually = manually }
    deactivated.each { |obj| obj.save } if save
    dectivated
  end

  def save_array_objs(array); array.each { |obj| obj.save } end

  def assign_holiday(holiday, user, manual)
    hol_user = HolidayUser.where(holiday_id: holiday.id, user_id: user.id, manually_assoc: manual).first_or_initialize
    set_registered(hol_user) unless hol_user.id
  end

end