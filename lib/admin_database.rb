class AdminDatabase < ActiveRecord::Base
  self.abstract_class = true
  establish_connection :production_ct_admin
end