module BpModule
  def active_module
    return if active_module?
    flash[:danger] = t('views.holiday_module.flash_messages.no_privilages_danger')
    redirect_to root_path
  end

  def active_module?
    ct_module = CtModule.where('cod = ? AND active = ?', 'ben_params', true).first
    !ct_module.nil?
  end

  def ben_params_manager
    return if ben_params_manager?(user_connected.id)
    flash[:danger] = t('views.holiday_module.flash_messages.no_privilages_danger')
    redirect_to root_path
  end

  def ben_params_manager?(user_id)
    return false unless active_module?
    ct_module = CtModule.where('cod = ? AND active = ?', 'ben_params', true).first
    ct_module.ct_module_managers.where('user_id = ?', user_id).count == 1
  end

  def set_user_registered(obj)
    obj.registered_at = lms_time
    obj.registered_by_user_id = user_connected.id
    obj
  end

  def deactivate_user(obj)
    obj.active = false
    obj.deactivated_at = lms_time
    obj.deactivated_by_user_id = user_connected.id
    obj
  end

  def new_group_user(registered_by_user, since_date, until_date, group_id, user_id)
    assoc = BpGroupsUser.new(since: since_date,
                             until: until_date,
                             bp_group_id: group_id,
                             user_id: user_id)
    assoc.registered_at = (Time.now - 3.hours)
    assoc.registered_by_user_id = registered_by_user.id
    assoc.save
    assoc
  end

  def set_until_for_at(registered_by_user, active_this_datetime, user_id, new_until_datetime)
    group_user = BpGroupsUser.active?(active_this_datetime, user_id)
    return nil unless group_user
    return nil if group_user.until
    group_user.until = new_until_datetime
    group_user.deactivated_at = (Time.now - 3.hours)
    group_user.deactivated_by_user_id = registered_by_user.id
    group_user.save
    group_user
  end

  def update_this_now(registered_by_user, active_this_datetime, prev_until_datetime, until_updated, user_id, group_id)
    #condiciones de trabajo:
    # 1. Se debe encontrar un grupo para el usuario para la fecha indicada (active_this_date), sino: new_group_user
    # 2. La asociación grupo-usuario no debe tener especificado el fin de la misma (until), sino: new_group_user
    # 3. Prev_until_datetime es la fecha until que se seteará, si es nil, se usará lms_time
    # 4. until_updated es la fecha until de la nueva asociación. NO OBLIGATORIO.
    group_user = BpGroupsUser.active?(active_this_datetime, user_id)
    return nil unless group_user
    this_datetime = prev_until_datetime ? prev_until_datetime.to_datetime : (Time.now - 3.hours)
    group_user = set_until_for_at(registered_by_user, active_this_datetime, user_id, this_datetime)
    return nil unless group_user
    return group_user if group_user.errors.size > 0
    new_group_user(registered_by_user, this_datetime + 1.second, until_updated, group_id, user_id)
  end

  def create_or_update_group_assoc(registered_by_user, since_this_datetime, user_id, group_id)
    group_user = BpGroupsUser.active?(since_this_datetime, user_id)
    if group_user
      update_this_now(registered_by_user, since_this_datetime, (since_this_datetime.to_datetime - 1.second), nil, user_id, group_id)
    else
      new_group_user(registered_by_user, since_this_datetime, nil, group_id, user_id)
    end
  end

end