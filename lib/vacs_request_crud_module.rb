module VacsRequestCrudModule

  def create_request(request_params, user_id, who, send_email)
    case who
    when 'manager'
      manager_create_request(request_params, user_id, send_email)
    when 'register'
      register_create_request(request_params)
    else
      register_create_request(request_params)
    end
  end

  private

  def manager_create_request(request_params, user_id, send_email)
    request = VacRequest.new(request_params)
    request.user_id = user_id
    request = set_registered(request)
    request.save
    send_emails(request) if send_email && request.errors.empty?
    request
  end

  def send_emails(request)
    approvers = VacApprover.approvers_of(request.user_id)
    approvers.each {|approver| VacsMailer.new_pending_request(request, @company, approver.id).deliver if request.pending?}
    VacsMailer.new_pending_requester(request, @company).deliver if request.pending?
  end

  def register_create_request(request_params)
  end

  def set_registered(obj)
    obj.registered_at = lms_time
    obj.registered_by_user = user_connected
    obj
  end

end