module UsersModule


  def list_active_users
    User.where('activo = ?',true)
  end


  def search_active_users(code, last_name, first_name)

    users = nil

    unless code.blank? && last_name.blank? && first_name.blank?

      users = User.where('codigo LIKE ? AND apellidos LIKE ? AND nombre LIKE ? AND activo = ?', '%' + code + '%', '%' + last_name + '%', '%' + first_name + '%', true).order('apellidos, nombre')

    end

    return users

  end

  # Validates if the string has the rut/run syntax and
  # calculates/validate the digit
  # @return [true, false]
  def rut_valid?(string_rut)
    results = Array.new
    rut = string_rut[0, string_rut.length - 1].to_i
    dv = string_rut[-1, 1].to_s.downcase
    numerical_serie = 2
    while rut > 0
      results.push (rut % 10) * numerical_serie
      rut /= 10
      numerical_serie += 1
      numerical_serie = 2 if numerical_serie > 7
    end

    digit = 11 - (results.inject(:+) % 11)

    if digit == 10
      digit = 'k'
    elsif digit == 11
      digit = '0'
    else
      digit = digit.to_s
    end
    if digit == dv
      return true
    else
      return false
    end
  end

  def identifica_hie_r_(j,v)
    r = nil
    j.each_with_index do |n, i|
      next unless n.include? '.'+v
      r = n.split('.')[0]
      puts n
      j.delete_at(i)
      break
    end
    return j,r
  end

  def identifica_hie_r(j,v,b)
    j,v = identifica_r_(j,v) while v && v!=b
    v==b
  end

end