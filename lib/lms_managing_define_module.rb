module LmsManagingDefineModule

  def massive_charge_programs_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        fila[0] = fila[0].to_s.strip #código programa

        program = Program.find_by_codigo fila[0]

        unless program

          program = Program.new
          program.codigo = fila[0]
          program.color = '000000'
          program.managed_by_manager = true

        end

        if program.managed_by_manager

          program.nombre = fila[1].to_s.strip unless fila[1].blank?
          program.descripcion = fila[2].to_s.strip unless fila[2].blank?

          unless program.save

            @lineas_error.push index+1
            @lineas_error_detalle.push ('codigo: '+fila[0])
            @lineas_error_messages.push ['No se pudo guardar el programa: '+program.errors.full_messages.to_s]

          end

        else

          @lineas_error.push index+1
          @lineas_error_detalle.push ('codigo: '+fila[0])
          @lineas_error_messages.push ['No puede modificar el programa']

        end

      end

    end

  end

  def massive_charge_programs_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_programas')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'Código'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Descripción'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def massive_charge_courses_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[1]

        fila[1] = fila[1].to_s.strip #código curso

        course = Course.find_by_codigo fila[1]

        unless course

          course = Course.new
          course.codigo = fila[1]
          course.managed_by_manager = true

        end

        if course.managed_by_manager

          course.nombre = fila[2].to_s.strip unless fila[2].blank? #nombre

          current_pos = 3

          if TrainingType.all.size > 0

            if fila[current_pos].blank?
              training_type = nil
            else
              fila[current_pos] = fila[current_pos].to_s.strip
              training_type = TrainingType.find_by_nombre(fila[current_pos])
              unless training_type
                @lineas_error.push index+1
                @lineas_error_detalle.push ( @company.training_type_alias+': '+fila[current_pos])
                @lineas_error_messages.push ['No existe: '+fila[current_pos]]
              end
            end

            course.training_type = training_type

            current_pos +=1

          end

          course.descripcion = fila[current_pos].to_s.strip unless fila[current_pos].blank? #descripción
          current_pos+=1

          course.objetivos = fila[current_pos].to_s.strip unless fila[current_pos].blank? #objetivos
          current_pos+=1

          course.nota_minima = fila[current_pos].to_i unless fila[current_pos].blank? #nota mínima
          current_pos+=1

          course.calculate_percentage = fila[current_pos] == '%' ? true : false #calculo nota en porcentaje
          current_pos+=1

          course.muestra_porcentajes = fila[current_pos] == '%' ? true : false #muestra notas en porcentaje
          current_pos+=1

          course.dedicacion_estimada = fila[current_pos].to_f unless fila[current_pos].blank? #dedicación estimada en horas
          current_pos+=1

          course.unit_alias = fila[current_pos].blank? ? 'unidad' : fila[current_pos].to_s.strip #alias de la unidad

          if course.save

            #asignar a un programa

            if fila[0]

              fila[0] = fila[0].to_s.strip #código programa

              program = Program.find_by_codigo fila[0]

              if program

                level = program.levels.first

                unless level
                  level = program.levels.build
                  level.nombre 'Único'
                  level.orden = 1
                  level = nil unless level.save
                end

                if level

                  program_course = program.program_course(course, level)

                  unless program_course

                    program_course = program.program_courses.build

                    program_course.course = course
                    program_course.level = level

                    unless program_course.save

                      @lineas_error.push index+1
                      @lineas_error_detalle.push ('codigo del programa: '+fila[0]+', código del curso: '+fila[1])
                      @lineas_error_messages.push ['No se pudo asignar el curso al programa']

                    end

                  end

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('codigo del programa: '+fila[0]+', código del curso: '+fila[1])
                  @lineas_error_messages.push ['No se pudo asignar el curso al programa']

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('codigo del programa: '+fila[0])
                @lineas_error_messages.push ['No existe el programa']

              end

            end

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('codigo del curso: '+fila[1])
            @lineas_error_messages.push ['No se pudo guardar el curso: '+course.errors.full_messages.to_s]

          end

        else

          @lineas_error.push index+1
          @lineas_error_detalle.push ('codigo del curso: '+fila[1])
          @lineas_error_messages.push ['No puede modificar el curso']

        end

      end

    end

  end

  def massive_charge_courses_excel_format

   company = @company

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_courseas')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << 'Código del programa'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Código del curso'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20

        if TrainingType.all.size > 0
          fila << company.training_type_alias
          filaHeader <<  headerCenter
          cols_widths << 20
        end

        fila << 'Descripción'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Objetivos'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Nota mínima'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Cálculo de nota (%/puntos)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Muestra resultado (%/puntos)'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Dedicación estimada en horas'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Alias de unidades'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end
  
end