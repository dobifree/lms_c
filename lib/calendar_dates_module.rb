module CalendarDatesModule

  def not_laborable?(lab_date)
    calendar_date = Holiday.where(hol_date: lab_date,
                                  hol_type: Holiday.global_type,
                                  active: true).first
    return calendar_date != nil
  end

  def laborable_by_holiday?(date_to_eval, user_id)
    return false if all_types_holiday_for?(user_id, date_to_eval)
    true
  end

  def not_laborable_by_holiday?(date_to_eval, user_id)
    return true if all_types_holiday_for?(user_id, date_to_eval)
    false
  end

  def global_holiday_for?(date_to_eval)
    Holiday.active.by_date(date_to_eval.to_date).global.first
  end

  def char_holiday_for?(date_to_eval, user_id)
    user = User.find(user_id)
    hols = Holiday.active.by_date(date_to_eval.to_date).by_customized

    return false unless hols.size > 0
    hols.each do |hol|
      conditions = hol.active_chars
      return false unless conditions.size > 0
      conditions.each do |condition|
        return false unless match_with?(user, match_values_for(conditions, condition.characteristic), condition.characteristic)
      end
    end
    true
  end

  def match_values_for(conditions, char)
    values = []
    conditions.each do |condition|
      next unless char.id == condition.characteristic_id
      values << condition.match_value
    end
    values
  end

  def match_with?(user, match_values, char)
    match_values.include?(user.user_characteristic(char).value)
  end

  def user_holiday_for?(date_to_eval, user_id)
    hols = Holiday.active.by_date(date_to_eval.to_date).by_customized
    return false unless hols.size > 0
    hols.each do |hol|
      assocs = hol.active_users_assoc
      return false unless assocs.size > 0
      assocs.each do |assoc|
        next unless assoc.user_id == user_id
        return true
      end
    end
    true
  end

  def all_types_holiday_for?(user_id, date_to_eval)
    return true unless global_holiday_for?(date_to_eval).nil?
    return true if char_holiday_for?(date_to_eval, user_id)
    return true if user_holiday_for?(date_to_eval, user_id)
    nil
  end

  def holiday_name_for(user_id, date_to_eval)
    global = global_holiday_for(date_to_eval)
    return global if global
    by_user = user_holiday_for(date_to_eval, user_id)
    return by_user if by_user
    by_char = char_holiday_for(date_to_eval, user_id)
    return by_char if by_char
    nil
  end

  def global_holiday_for(date_to_eval)
    Holiday.active.by_date(date_to_eval.to_date).global.first
  end

  def user_holiday_for(date_to_eval, user_id)
    hols = Holiday.active.by_date(date_to_eval.to_date).by_customized
    return nil unless hols.size > 0
    hols.each do |hol|
      assocs = hol.active_users_assoc
      return nil unless assocs.size > 0
      assocs.each do |assoc|
        next unless assoc.user_id.to_s == user_id.to_s
        return hol
      end
    end
    nil
  end

  def char_holiday_for(date_to_eval, user_id)
    user = User.find(user_id)
    hols = Holiday.active.by_date(date_to_eval.to_date).by_customized

    return nil unless hols.size > 0
    hols.each do |hol|
      conditions = hol.active_chars
      return nil unless conditions.size > 0
      conditions.each do |condition|
        return nil unless match_with?(user, match_values_for(conditions, condition.characteristic), condition.characteristic)
      end
      return hol
    end
    nil
  end


  def holiday_for(user_id, date)
    all_types_holiday_for?(user_id, date)
  end

  def holidays_in(user_id, date_begin, date_end)
    #Feriados en el rango pasado. Inclusive.
    # Devuelve Array
    holidays = []
    pivot = date_begin.to_date
    date_end = date_end.to_date

    loop do
      holidays << pivot if all_types_holiday_for?(user_id, pivot)
      pivot += 1.day
      break if pivot > date_end
    end
    holidays
  end

  def next_workable_day(user_id, date)
    #Siguiente día no feŕiado
    #inclusive
    # Máximo de 1 año de revisión. Si hay más de un 1 año de feríados continuos, devolverá el día 366
    pivot = date.to_date
    date_end = date.to_date + 365.days
    loop do
      return pivot unless all_types_holiday_for?(user_id, pivot)
      pivot += 1.day
      break if pivot > date_end
    end
    pivot
  end

  def prev_workable_day(user_id, date)
    #Previo día no feŕiado
    #inclusive
    # Máximo de 1 año de revisión. Si hay más de un 1 año de feríados continuos, devolverá el día 366
    pivot = date.to_date
    date_end = date.to_date - 365.days
    loop do
      return pivot unless all_types_holiday_for?(user_id, pivot)
      pivot -= 1.day
      break if pivot < date_end
    end
    pivot
  end

  def next_holiday(user_id, date)
    #Siguiente día feŕiado
    #inclusive
    #Máximo 1 año de revisión. Si hay más de un 1 año de no feríados continuos, devolverá el día 366.
    pivot = date.to_date
    date_end = date.to_date + 365.days
    loop do
      return pivot if all_types_holiday_for?(user_id, pivot)
      pivot += 1.day
      break if pivot > date_end
    end
    pivot
  end

  def prev_holiday(user_id, date)
    #Anterior día feŕiado
    #inclusive
    #Máximo 1 año de revisión. Si hay más de un 1 año de no feríados continuos, devolverá el día 366.
    pivot = date.to_date
    date_end = date.to_date - 365.days
    loop do
      return pivot if all_types_holiday_for?(user_id, pivot)
      pivot -= 1.day
      break if pivot < date_end
    end
    pivot
  end


end