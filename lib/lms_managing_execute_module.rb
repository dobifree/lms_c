module LmsManagingExecuteModule

  def massive_charge_enrollments_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[0] && fila[1] && fila[2] && fila[3] && fila[4]

        fila[0] = fila[0].to_s.strip #código usuario

        user = User.find_by_codigo fila[0]

        if user

          fila[1] = fila[1].to_s.strip #código del programa

          program = Program.find_by_codigo fila[1]

          if program

            fila[2] = fila[2].to_s.strip #código del curso

            course = Course.find_by_codigo fila[2]

            if course

              program_course = program.program_course(course, program.levels.first)

              if program_course

                from_date = fila[3]
                to_date = fila[4]

                if from_date.is_a?(DateTime) && to_date.is_a?(DateTime)

                  if from_date < to_date

                    program_course_instance = program_course.program_course_instance_by_dates(from_date, to_date)

                    unless program_course_instance

                      program_course_instance = program_course.program_course_instances.build

                      program_course_instance.from_date = from_date
                      program_course_instance.to_date = to_date

                      program_course_instance = nil unless program_course_instance.save

                    end

                    if program_course_instance

                      enrollment = program.enrollment user

                      unless enrollment

                        enrollment = program.enrollments.build
                        enrollment.user = user
                        enrollment.level = program.levels.first

                        enrollment = nil unless enrollment.save

                      end

                      if enrollment

                        user_course = program_course_instance.user_courses.where('enrollment_id = ? AND desde = ? AND hasta = ?', enrollment.id, from_date, to_date).first

                        unless user_course

                          user_course = program_course_instance.user_courses.build

                          user_course.enrollment = enrollment
                          user_course.program_course = program_course
                          user_course.course = course
                          user_course.numero_oportunidad = 1
                          user_course.desde = from_date
                          user_course.hasta = to_date

                        end

                        unless user_course.save

                          @lineas_error.push index+1
                          @lineas_error_detalle.push ('Usuario: '+fila[0]+' - Programa: '+fila[1]+' - Curso: '+fila[2])
                          @lineas_error_messages.push ['Se produjo un error al realizar la matrícula']

                        end

                      else

                        @lineas_error.push index+1
                        @lineas_error_detalle.push ('Usuario: '+fila[0]+' - Programa: '+fila[1]+' - Curso: '+fila[2])
                        @lineas_error_messages.push ['Se produjo un error al realizar la matrícula']

                      end

                    else

                      @lineas_error.push index+1
                      @lineas_error_detalle.push ('Programa: '+fila[1]+' - Curso: '+fila[2])
                      @lineas_error_messages.push ['Existe un error en las fechas y horas de inicio y/o fin']

                    end

                  else

                    @lineas_error.push index+1
                    @lineas_error_detalle.push ('Programa: '+fila[1]+' - Curso: '+fila[2])
                    @lineas_error_messages.push ['La fecha y hora de fin no puede ser menor que la fecha y hora de inicio']


                  end

                else

                  @lineas_error.push index+1
                  @lineas_error_detalle.push ('Inicio: '+fila[3].to_s+' - Fin: '+fila[4].to_s)
                  @lineas_error_messages.push ['No se encuentran en formato fecha']

                end

              else

                @lineas_error.push index+1
                @lineas_error_detalle.push ('Programa: '+fila[1]+' - Curso: '+fila[2])
                @lineas_error_messages.push ['No existe el curso en el programa']

              end

            else

              @lineas_error.push index+1
              @lineas_error_detalle.push ('Curso: '+fila[2])
              @lineas_error_messages.push ['No existe']

            end

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push ('Programa: '+fila[1])
            @lineas_error_messages.push ['No existe']

          end

        else

          @lineas_error.push index+1
          @lineas_error_detalle.push ('Usuario: '+fila[0])
          @lineas_error_messages.push ['No existe']

        end

      end

    end

  end

  def massive_charge_enrollments_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_matriculas')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << @company.alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Código del programa'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Código del curso'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Fecha y hora de inicio'
        filaHeader <<  headerCenter
        cols_widths << 20

        fila << 'Fecha y hora de fin'
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def massive_charge_enrollments_from_instances_from_excel(archivo_temporal, program_course_instance)

    users_array = Array.new
    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        fila[0] = fila[0].to_s.strip #código usuario

        user = User.find_by_codigo fila[0]

        if user
          users_array.append(user.id)
        else

          @lineas_error.push index+1
          @lineas_error_detalle.push ('Usuario: '+fila[0])
          @lineas_error_messages.push ['No existe']

        end

      end

    end

    lms_add_users_to_enroll_from_instances_list(users_array, program_course_instance.id)

  end

  def massive_charge_enrollments_from_instances_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      wb.add_worksheet(:name => ('Formato_carga_matriculas')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new

        fila << @company.alias_username
        filaHeader <<  headerCenter
        cols_widths << 20

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def update_progress(user_course)

    numero_elementos = user_course.course.units.count + user_course.course.evaluations.count + user_course.course.polls.where('obligatoria = true ').count

    numero_unidades = user_course.user_course_units.where('finalizada = true').count
    numero_evaluaciones = user_course.user_course_evaluations.where('finalizada = true').count
    numero_encuestas = user_course.user_course_polls.joins(:poll).where('obligatoria = true AND finalizada = true').count

    porcentaje_avance = 100

    if numero_elementos > 0

      porcentaje_avance = (numero_unidades + numero_evaluaciones +numero_encuestas)*100.0 / numero_elementos.to_f

    end

    numero_unidades_total = user_course.course.units.count

    asistencia_unidades = 100

    if numero_unidades_total > 0

      if user_course.program_course_instance && user_course.program_course_instance.program_course_instance_units.first

        t_t = 0
        t_a = 0

        user_course.program_course_instance.program_course_instance_units.each do |pciu|
          t_t += pciu.to_date - pciu.from_date
          ucu = user_course.user_course_units.where('finalizada = ? AND unit_id = ?', true, pciu.unit_id).first
          t_a += pciu.to_date - pciu.from_date if ucu
        end

        asistencia_unidades = t_a*100.0/t_t.to_f

      else

        asistencia_unidades = numero_unidades*100.0/numero_unidades_total.to_f

      end

    end

    user_course.update_attributes(:porcentaje_avance => porcentaje_avance, :asistencia_unidades => asistencia_unidades)

  end

  def update_user_course_status(user_course)

    units = user_course.course.units
    evaluations = user_course.course.evaluations
    valid_evaluations = user_course.course.evaluations.where('weight > 0')
    polls = user_course.course.polls.where('obligatoria = ? and virtual = ?', true, true)

    todo_terminado_units = true
    todo_terminado_evals = true
    todo_terminado_polls = true
    algo_terminado = false

    units.each do |unit|
      uc_unit = user_course.user_course_units.find_by_unit_id(unit.id)
      algo_terminado = true if uc_unit && uc_unit.finalizada
      todo_terminado_units = false unless uc_unit && uc_unit.finalizada
    end

    evaluations.each do |evaluation|
      uc_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(evaluation.id)
      algo_terminado = true if uc_evaluation && uc_evaluation.finalizada
      todo_terminado_evals = false unless uc_evaluation && uc_evaluation.finalizada
    end

    polls.each do |poll|
      uc_poll = user_course.user_course_polls.find_by_poll_id(poll.id)
      algo_terminado = true if uc_poll && uc_poll.finalizada
      todo_terminado_polls = false unless uc_poll && uc_poll.finalizada
    end

    nota = nil

    if valid_evaluations.length > 0
      suma = 0
      suma_pesos = 0
      valid_evaluations.each do |evaluation|
        uc_evaluation = user_course.user_course_evaluations.find_by_evaluation_id(evaluation.id)
        if uc_evaluation && uc_evaluation.nota
          suma += uc_evaluation.nota*evaluation.weight
          suma_pesos += evaluation.weight
        end
      end
      nota = suma*1.0/suma_pesos*1.0 if suma_pesos > 0
    end

    user_course.nota = nota

    if algo_terminado

      unless user_course.iniciado

        user_course.iniciado = true
        user_course.inicio = user_course.program_course_instance.from_date

      end

    end

    todo_terminado_units = true if user_course.asistencia_unidades >= user_course.course.asistencia_minima

    if todo_terminado_units && todo_terminado_evals && todo_terminado_polls

      unless user_course.iniciado

        user_course.iniciado = true
        user_course.inicio = user_course.program_course_instance.from_date

      end

      user_course.fin = lms_time
      user_course.finalizado = true


      user_course.aprobado = true

      user_course.aprobado = false if nota && nota < user_course.course.nota_minima

      user_course.asistencia = true

    else

      user_course.fin = nil
      user_course.finalizado = false

      user_course.aprobado = false

      user_course.asistencia = false

    end

    user_course.save

  end

  def massive_charge_results_excel_format

    reporte = Axlsx::Package.new
    wb = reporte.workbook

    wb.use_shared_strings = true

    wb.styles do |s|


      headerCenter = s.add_style :b => true,
                                 :sz => 9,
                                 :border => { :style => :thin, color: '00' },
                                 :alignment => { :horizontal => :center, :wrap_text => true}

      normal_cell = s.add_style :b => false,
                                 :sz => 9

      wb.add_worksheet(:name => ('Formato_registro_resultados')) do |reporte_excel|

        fila = Array.new
        filaHeader = Array.new
        cols_widths = Array.new
        types = Array.new

        fila << @company.alias_username
        filaHeader <<  headerCenter
        cols_widths << 20
        types << :string

        fila << 'Apellidos'
        filaHeader <<  headerCenter
        cols_widths << 20
        types << nil

        fila << 'Nombre'
        filaHeader <<  headerCenter
        cols_widths << 20
        types << nil

        if @course.control_total_asistencia?
          fila << 'Asistencia'
          filaHeader <<  headerCenter
          cols_widths << 20
          types << nil

        end

        @elements.each_with_index do |element, index|

          unless element.virtual

            ei = @elements_ids[index].split('-')
            tipo = ei[0]
            id =  ei[1]

            el = nil

            el = @course.units.find(id) if tipo=='u'
            el = @course.evaluations.find(id) if tipo=='e'
            el = @course.polls.find(id) if tipo=='p'

            if tipo == 'u' || tipo == 'e'

              if tipo=='u' && !@course.control_total_asistencia?
                fila << @course.unit_alias.capitalize+' '+el.numero.to_s
                filaHeader <<  headerCenter
                cols_widths << 20
                types << nil
              end

              if tipo=='e'
                fila << 'Evaluación '+el.numero.to_s
                filaHeader <<  headerCenter
                cols_widths << 20
                types << nil
              end

            end

          end

        end

        reporte_excel.add_row fila, :style => filaHeader, height: 20

        @program_course_instance.matriculas_vigentes.each_with_index do |uc, index|

          fila = Array.new
          filaHeader = Array.new

          fila << uc.enrollment.user.codigo
          filaHeader <<  normal_cell

          fila << uc.enrollment.user.apellidos
          filaHeader <<  normal_cell

          fila << uc.enrollment.user.nombre
          filaHeader <<  normal_cell

          if @course.control_total_asistencia?
            fila << (uc.asistencia ? 1 : 0)
            filaHeader <<  normal_cell
          end

          @elements.each_with_index do |element, index|

            unless element.virtual

              ei = @elements_ids[index].split('-')
              tipo = ei[0]
              id =  ei[1]

              if tipo == 'u' || tipo == 'e'

                if tipo == 'u' && !@course.control_total_asistencia?

                  user_course_unit = uc.user_course_units.where('unit_id = ?', element.id).first

                  estado = if user_course_unit && user_course_unit.finalizada
                                  1
                                elsif user_course_unit && user_course_unit.inicio
                                  0
                                else
                                  nil
                           end

                  fila << estado
                  filaHeader <<  normal_cell
                  cols_widths << 20


                elsif tipo == 'e'

                  user_course_evaluation = uc.user_course_evaluations.where('evaluation_id = ?', element.id).first

                  estado = if user_course_evaluation && user_course_evaluation.finalizada
                                        user_course_evaluation.nota
                                      else
                                         nil
                                      end

                  fila << estado
                  filaHeader <<  normal_cell
                  cols_widths << 20

                end



              end

            end

          end

          reporte_excel.add_row fila, :style => normal_cell, height: 20, :types => [:string]

        end

        reporte_excel.column_info.each_with_index do |col, index|
          col.width = cols_widths[index]
        end

      end

    end

    return reporte

  end

  def massive_charge_results_from_excel(archivo_temporal)

    archivo = RubyXL::Parser.parse archivo_temporal

    data = archivo.worksheets[0].extract_data

    data.each_with_index do |fila, index|

      if index > 0 && fila[0]

        fila[0] = fila[0].to_s.strip #código alumno

        user = User.find_by_codigo fila[0]

        if user

          user_course = @program_course_instance.user_course_by_user_read_only_false user

          if user_course

            pos = 3

            if @course.control_total_asistencia?

              asistencia_total = fila[pos]

              pos += 1

            end

            @elements.each_with_index do |element, index|

              unless element.virtual

                ei = @elements_ids[index].split('-')
                tipo = ei[0]
                id =  ei[1]

                if tipo == 'u' || tipo == 'e'

                  if tipo == 'u'

                    user_course_unit = user_course.user_course_units.where('unit_id = ?',element.id).first

                    unless user_course_unit
                      user_course_unit = user_course.user_course_units.build
                      user_course_unit.unit = element
                      user_course_unit.inicio = lms_time
                    end

                    if (fila[pos] == '1' || fila[pos] == 1) || (asistencia_total == 1 && @course.control_total_asistencia?)
                      user_course_unit.inicio = lms_time
                      user_course_unit.fin = lms_time
                      user_course_unit.finalizada = true
                    elsif (fila[pos] == '0' || fila[pos] == 0) || (asistencia_total == 0 && @course.control_total_asistencia?)
                      user_course_unit.inicio = lms_time
                      user_course_unit.fin = nil
                      user_course_unit.finalizada = false
                    else
                      user_course_unit.inicio = nil
                      user_course_unit.fin = nil
                      user_course_unit.finalizada = false
                    end

                    user_course_unit.save

                    pos += 1 unless @course.control_total_asistencia?

                  elsif tipo == 'e'

                    user_course_evaluation = user_course.user_course_evaluations.where('evaluation_id = ?',element.id).first

                    unless user_course_evaluation
                      user_course_evaluation = user_course.user_course_evaluations.build
                      user_course_evaluation.evaluation = element
                    end

                    if fila[pos].blank?

                      user_course_evaluation.inicio = nil
                      user_course_evaluation.fin = nil
                      user_course_evaluation.ultimo_acceso = nil

                      user_course_evaluation.finalizada = false
                      user_course_evaluation.nota = nil
                      user_course_evaluation.aprobada = nil

                    else

                      user_course_evaluation.inicio = lms_time
                      user_course_evaluation.fin = lms_time
                      user_course_evaluation.ultimo_acceso = lms_time

                      user_course_evaluation.finalizada = true
                      user_course_evaluation.nota = fila[pos]

                      if user_course_evaluation.nota >= user_course_evaluation.evaluation.course.nota_minima
                        user_course_evaluation.aprobada = true
                      else
                        user_course_evaluation.aprobada = false
                      end

                    end

                    user_course_evaluation.save

                    pos += 1

                  end

                end

              end

            end

            update_progress user_course

            update_user_course_status user_course

          else

            @lineas_error.push index+1
            @lineas_error_detalle.push (@company.alias_username+': '+fila[0])
            @lineas_error_messages.push ['El usuario no está matriculado en la instancia']

          end

        else

          @lineas_error.push index+1
          @lineas_error_detalle.push (@company.alias_username+': '+fila[0])
          @lineas_error_messages.push ['El usuario no existe']

        end

      end

    end

  end

end