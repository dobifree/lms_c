module VacationsModule
  include CalendarDatesModule
  include ScheduleDateModule
  include MedlicModule
  include LicensesModule


  def verified_closed_request(request, request_id)
    return true unless request.closed?
    back_to = request_id ? true : back_to_work_request(request_id).to_date <= lms_time.to_date
    return true if request.closable? && back_to
    false
  end

  def verify_conflict_benefits(request)
    return '' if request.registered_manually || request.historical || !request.active
    return '' if request.status == VacRequest.draft || request.status == VacRequest.rejected
    requests = LicRequest.where(active: true).joins(:lic_event).where(lic_events: { active: true, user_id: request.user_id})
    message = ''
    begin_date = request.begin.to_date
    end_date = request.end.to_date

    requests.each do |this_request|
      next unless (this_request.begin < end_date && this_request.end > begin_date)
      message += this_request.begin.strftime('%d/%m/%Y')+' - '+this_request.end.strftime('%d/%m/%Y')+' ('+this_request.status_message+'). '
    end

    message
  end

  def scheme_calculus_mod(user_id, date_end)
    vac_accum = VacAccumulated.user_latest(user_id)
    rules = BpGeneralRulesVacation.user_active(date_end.to_datetime, user_id)
    return nil unless rules
    return nil unless vac_accum
    date_end = date_end.to_date

    legal_last_accu = VacAccumulated.user_latest_legal(user_id)
    legal_last_accu = VacAccumulated.user_first(user_id) unless legal_last_accu

    prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
    prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated

    legal_future = future_days(legal_last_accu.up_to_date, date_end, user_id) + vac_accum.accumulated_real_days
    progressive = next_progressive_days(prev_auto_accumulated.up_to_date, date_end, user_id)

    requests = VacRequest.requests_by_status_for(user_id, VacRequest.approved)
    requests.each do |request|
      back_to_work_date = back_to_work_request(request.id).to_date
      if date_end >= back_to_work_date
        progressive -= request.days_progressive if request.days_progressive
        legal_future -= request.days if request.days
      else
        next unless request.begin <= date_end
        this_legal = request.days ? request.days : 0
        this_progressive = request.days_progressive ? request.days_progressive : 0

        days_in = vacations_days(rules.only_laborable_day, request.begin, date_end, user_id)

        days_in = days_in > this_legal + this_progressive ? this_legal + this_progressive : days_in

        prog_to_rest = this_progressive < days_in ? this_progressive : days_in
        days_in -= prog_to_rest
        legal_to_rest = this_legal <  days_in ? this_legal : days_in

        progressive -= prog_to_rest
        legal_future -= legal_to_rest
      end
    end
    [legal_future, progressive]
  end

  def available_progressive_for(user_id)
    user = User.find(user_id)
    return nil unless user
    user_certificate = Characteristic.get_characteristic_from_date_r_progresivas
    return nil unless user_certificate
    user_certificate = user.user_characteristic(user_certificate)
    return nil unless user_certificate
    progressive_day_date = Characteristic.get_characteristic_from_date_progresivas
    return nil unless progressive_day_date
    progressive_day_date = user.user_characteristic(progressive_day_date)
    return nil unless progressive_day_date
    general_rules = BpGeneralRulesVacation.user_active(lms_time, user_id)
    return nil unless general_rules
    days_progressives = BpProgressiveDay.where(active: true, bp_general_rules_vacation_id: general_rules.id).first
    return nil unless days_progressives
    days_progressives
  end

  def back_to_work_request(request_id)
    request = VacRequest.find(request_id)
    date_end = request.end
    request.vac_rule_records.where(active: true, paid: [nil, true]).each do |benefit|
      benefit = benefit.bp_condition_vacation
      next unless benefit.bonus_days
      date_end = next_laborable_day?(date_end, request.user_id)
      date_end = vacations_end(true, date_end, benefit.bonus_days, request.user_id)
    end
    next_laborable_day?(date_end, request.user_id)
  end

  def set_benefit_for(request_id, benefit_id, must_be_in)
    go_to = false
    must_be_in.each { |this| go_to = true if this.to_i == (benefit_id.to_i) }
    return nil unless go_to
    benefit = rule_record_complete_create(benefit_id, request_id)
    benefit.save
    update_end_vacations(request_id)
  end

  def request_satify_this_benefits(request, gimme_ids)
    rules_vacations = BpRuleVacation.active_rules_vacations( request.begin ? request.begin.to_datetime : lms_time, request.end ? request.end.to_datetime : lms_time, request.user_id)
    satisfy_this = []
    actual =  VacAccumulated.user_latest(request.user_id)

    actual_days = actual ? actual.accumulated_real_days : 0

    legal_last_accu = VacAccumulated.user_latest_legal(request.user_id)
    legal_last_accu = VacAccumulated.user_first(request.user_id) unless legal_last_accu

    prev_auto_accumulated = VacAccumulated.user_latest_prog(request.user_id)
    prev_auto_accumulated = VacAccumulated.user_first(request.user_id) unless prev_auto_accumulated

    update_days = future_days(legal_last_accu ? legal_last_accu.up_to_date : lms_time, request.begin, request.user_id)

    next_vac = (actual_days.to_f - request.days.to_i + update_days.to_f - VacRequest.sum_days_by_status(request.user_id, VacRequest.status_in_process).to_i)

    next_pro_days = next_progressive_days(prev_auto_accumulated.up_to_date, request.begin, request.user_id) - VacRequest.sum_progressive_days_by_status(request.user_id, VacRequest.status_in_process).to_i
    next_pro_days += request.days_progressive if request && request.days_progressive && (request.pending? || request.approved?)
    next_pro_days -= request.days_progressive if request.days_progressive

    rules_vacations.each do |rule_vacation|
      benefit = rule_vacation.evaluate_benefits(lms_time, request.begin, request.end, request.days, request.days_progressive, anniversary(request.user_id) , request.user_id, next_vac, next_pro_days, request)
      next unless benefit
      satisfy_this << (gimme_ids ? benefit.id : benefit)
    end

    satisfy_this
  end

  def not_laborable_days(begin_date, end_date, user_id)
    begin_date = begin_date.to_date
    end_date = end_date.to_date

    pivot = begin_date
    not_laborable_days = 0
    loop do
      non_laborable_day = false
      non_laborable_day = true if satisfy_conflict_periods(pivot, pivot, user_id, nil).size > 0
      non_laborable_day = true if not_laborable_by_sc?(pivot, user_id)
      non_laborable_day = true if not_laborable_by_holiday?(pivot, user_id)
      non_laborable_day = true if is_there_a_license(pivot, user_id).size > 0
      not_laborable_days += 1 if non_laborable_day
      pivot += 1.day
      break if pivot > end_date
    end
    not_laborable_days
  end

  def vacations_end(only_laborable, begin_date, days, user_id)
    begin_date = begin_date.to_date
    days = days.to_i
    end_date = (begin_date + days.days - 1.day)
    return end_date unless only_laborable
    non_laborable_days = not_laborable_days(begin_date, end_date, user_id)
    non_laborable_days.times do
      end_date = next_laborable_day?(end_date, user_id)
    end
    end_date
  end

  def vacations_days(only_laborable, begin_date, end_date, user_id)
    begin_date = begin_date.to_date
    end_date = end_date.to_date
    not_laborable_days = not_laborable_days(begin_date, end_date, user_id)
    days_btw = (end_date - begin_date).to_i
    only_laborable_days = (days_btw - not_laborable_days + 1)
    if begin_date == end_date
      only_laborable_days = next_laborable_day?(begin_date - 1.day, user_id) == begin_date ? 1 : 0
    end
    only_laborable_days = only_laborable_days < 0 ? 0 : only_laborable_days
    only_laborable ? only_laborable_days : days_btw
  end

  def next_laborable_day?(date, user_id)
    laborable = false
    date = date.to_date + 1.day
    loop do
      laborable = true unless (satisfy_conflict_periods(date, date, user_id, nil).size > 0 || not_laborable_by_sc?(date, user_id) || not_laborable_by_holiday?(date, user_id) || (is_there_a_license(date, user_id).size > 0))
      laborable = true unless schedule_of(user_id, date)
      return date if laborable
      date += 1.day
    end
  end

  def anniversary(user_id)
    user = User.find(user_id)
    return nil unless user
    ann_char = Characteristic.get_characteristic_from_date_work
    return nil unless ann_char
    uc = user.user_characteristic(ann_char)
    return nil unless uc
    uc.value
  end

  def vacations_days_btwn(date_1, date_2, user_id)
    date_1 = date_1.to_date
    date_2 = date_2.to_date
    aux = date_1
    date_1 = date_1 < date_2 ? date_1 : date_2
    date_2 = date_2 > date_1 ? date_2 : aux
    days_btw = (date_2.to_date - date_1.to_date + 1)
    days_not_laborable = not_laborable_days(date_1, date_2, user_id)
    days_btw - days_not_laborable
  end

  def go_back_vacations_date(vac_request_id)
    request = VacRequest.find(vac_request_id)
    benefits = request.vac_rule_records.where(active: true, refund_pending: false)
    go_back_date = next_laborable_day?(request.end, request.user_id)

    benefits.each do |benefit|
      next unless benefit.bp_condition_vacation.bonus_days
      go_back_date = vacations_end(benefit.bp_condition_vacation.bonus_only_laborable_day, go_back_date, benefit.bp_condition_vacation.bonus_days, request.user_id)
      go_back_date = next_laborable_day?(go_back_date, request.user_id)
    end

    go_back_date
  end

  def future_days(date_since, date_end, user_id)
    date_since = date_since.to_date
    date_end = date_end.to_date
    rules = BpGeneralRulesVacation.user_active(date_since.to_datetime, user_id)
    return 0 unless rules
    monthly_amount = (rules.legal_days.to_f / 12.to_f)
    month_days = Time.days_in_month(date_since.month, date_since.year)
    daily_amount = monthly_amount / month_days
    sum_1 = (month_days - date_since.day)
    sum_1 *= daily_amount

    sum_2 = ((date_end.month - date_since.month - 1) + 12 * (date_end.year - date_since.year)) * monthly_amount

    daily_amount = monthly_amount / month_days
    sum_3 = (date_end.day).to_i * daily_amount

    (sum_1 + sum_2 + sum_3)
  end

  def next_vac_accumulated_1(user_id, begin_vacations, vacs_days)
    rules = BpGeneralRulesVacation.user_active(begin_vacations.to_datetime, user_id)
    next_vac = nil
    if rules
      actual = VacAccumulated.user_latest(user_id)
      actual_days = actual ? actual.accumulated_real_days : 0
      legal_last_accu = VacAccumulated.user_latest_legal(user_id)
      legal_last_accu = VacAccumulated.user_first(user_id) unless legal_last_accu

      update_days = future_days(legal_last_accu ? legal_last_accu.up_to_date : lms_time, begin_vacations, user_id)
      next_vac = (actual_days.to_f - vacs_days.to_i + update_days.to_f - VacRequest.sum_days_by_status(user_id, VacRequest.status_in_process).to_i)
    end
    next_vac
  end

  def next_progressive_days(date_begin, date_end, user_id)
    date_begin = date_begin.to_date
    date_end = date_end.to_date
    days = 0
    prev_accumulated = VacAccumulated.user_latest(user_id)
    prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
    prev_auto_accumulated = prev_auto_accumulated ? prev_auto_accumulated : VacAccumulated.user_first(user_id)

    user = User.find(user_id)

    user_certificate_char = Characteristic.get_characteristic_from_date_r_progresivas
    prog_char = Characteristic.get_characteristic_from_date_progresivas
    general_rules = BpGeneralRulesVacation.user_active(date_begin.to_datetime, user.id)

    return days unless user_certificate_char
    return days unless prog_char
    return days unless general_rules

    user_certificate = user.user_characteristic(user_certificate_char)
    progressive_day_date = user.user_characteristic(prog_char)
    progressive_day_date = progressive_day_date ? progressive_day_date.value : nil

    return days unless progressive_day_date
    return days unless user_certificate

    if prev_accumulated && prev_accumulated.up_to_date < date_end && prev_auto_accumulated
      if user_certificate && general_rules && progressive_day_date
        accumulate_progresive = general_rules.accumulative_progressive?

        if accumulate_progresive
          pivot = progressive_day_date.change(year: date_begin.year)

          loop do
            if pivot > prev_auto_accumulated.up_to_date && pivot <= date_end

              this_year = pivot.year - progressive_day_date.year
              this_year += 1 if pivot.month > progressive_day_date.month
              this_year += 1 if (pivot.month == progressive_day_date.month && pivot.day > progressive_day_date.day)

              days_progressive = BpProgressiveDay.where(active: true, bp_general_rules_vacation_id: general_rules.id, year: this_year).first
              days += days_progressive.days if days_progressive
            end
            pivot += 1.year
            break if pivot > date_end
          end

          days += prev_accumulated.accumulated_progressive_days
        else
          pivot = progressive_day_date.change(year: (date_end + 1.year).year)

          loop do
            pivot -= 1.year
            break if pivot <= date_end
          end

          if pivot > prev_auto_accumulated.up_to_date
            this_year = pivot.year - progressive_day_date.year
            this_year += 1 if pivot.month > progressive_day_date.month
            this_year += 1 if (pivot.month == progressive_day_date.month && pivot.day > progressive_day_date.day)

            days_progressive = BpProgressiveDay.where(active: true, bp_general_rules_vacation_id: general_rules.id, year: this_year).first
            days = days_progressive.days if days_progressive
            days += prev_accumulated.accumulated_progressive_days if prev_accumulated.accumulated_progressive_days < 0
          else
            days = prev_accumulated.accumulated_progressive_days
          end

        end
      end

      days
    else
      prev_accumulated ?  prev_accumulated.accumulated_progressive_days : 0
    end

  end

  def update_legal_accumulated(user_id, this_date)
    return nil unless user_id && this_date
    user = User.find(user_id)
    ann = user.from_date_inicio_laboral
    return nil unless ann

    general_rules = BpGeneralRulesVacation.user_active(this_date.to_datetime, user_id)
    prev_accumulated = VacAccumulated.user_latest(user_id)
    first_accumulated = VacAccumulated.user_first(user_id)
    ann = ann.change(year: this_date.year)

    return nil unless first_accumulated
    return nil unless user
    return nil unless prev_accumulated
    return nil unless ann.to_date == this_date.to_date
    return nil unless general_rules

    prev_auto_accumulated = VacAccumulated.user_latest_legal(user_id)
    prev_auto_accumulated = first_accumulated unless prev_auto_accumulated

    return nil unless prev_auto_accumulated.up_to_date < this_date
    to_rest = general_rules.accumulative_legal? ? 0 : prev_accumulated.accumulated_real_days
    to_sum = general_rules.legal_days

    if first_accumulated.id == prev_auto_accumulated.id
      proy =  future_days(first_accumulated.up_to_date, this_date, user_id)
      to_sum = proy if proy < to_sum
    end

    if to_rest > 0
      manual_request = VacRequest.new
      manual_request.begin = this_date
      manual_request.days_manually = to_rest.to_d
      manual_request.registered_manually = true
      manual_request.registered_manually_description = 'Actualización por aniversario'
      manual_request.status = VacRequest.closed
      manual_request.user_id = user_id
      manual_request.registered_manually = true
      manual_request.registered_by_user_id = 9309
      manual_request.registered_at = lms_time
      manual_request.auto_update_legal = true
      manual_request.save
    end

    manual_request = VacRequest.new
    manual_request.begin = this_date
    manual_request.days_manually = (to_sum.to_d)*-1
    manual_request.registered_manually = true
    manual_request.registered_manually_description = 'Actualización por aniversario'
    manual_request.status = VacRequest.closed
    manual_request.user_id = user_id
    manual_request.registered_manually = true
    manual_request.registered_by_user_id = 9309
    manual_request.registered_at = lms_time
    manual_request.auto_update_legal = true
    manual_request.save

    update_accumulated_since(this_date.to_date, user_id, 'Actualización por aniversario')
  end

  def update_prog_accumulated(this_date, user_id)
    return nil unless user_id
    return nil unless this_date

    user = User.find(user_id)
    ann = user.from_date_inicio_laboral
    ann = ann.change(year: this_date.year)
    return nil unless user
    return nil unless ann.to_date == this_date.to_date

    general_rules = BpGeneralRulesVacation.user_active(this_date.to_datetime, user_id)
    return nil unless general_rules

    prev_accumulated = VacAccumulated.user_latest(user_id)
    return nil unless prev_accumulated

    user_certificate = user.secu_from_date_certificado_progresivas

    progressive_day_date = user.secu_from_date_inicio_progresivas
    return nil unless progressive_day_date

    prev_auto_accumulated = VacAccumulated.user_latest_prog(user_id)
    prev_auto_accumulated = VacAccumulated.user_first(user_id) unless prev_auto_accumulated

    return nil unless prev_auto_accumulated.up_to_date < this_date.to_date

    to_rest = general_rules.accumulative_progressive? ? 0 : prev_accumulated.accumulated_progressive_days
    progressive_year = progressive_day_date.change(year: this_date.year)

    this_year = progressive_year.year - progressive_day_date.year
    this_year += 1 if progressive_year.month > progressive_day_date.month
    this_year += 1 if (progressive_year.month == progressive_day_date.month && progressive_year.day > progressive_day_date.day)

    days_progressive = BpProgressiveDay.where(active: true, bp_general_rules_vacation_id: general_rules.id, year: this_year).first
    return nil unless days_progressive

    registered_description = 'Actualización por aniversario.'
    registered_description += ' Sin reconocimiento de días progresivos' unless user_certificate

    if to_rest > 0
      manual_request = VacRequest.new
      manual_request.begin = this_date
      manual_request.days_progressive = to_rest.to_i
      manual_request.registered_manually = true
      manual_request.registered_manually_description = registered_description
      manual_request.status = VacRequest.closed
      manual_request.user_id = user_id
      manual_request.registered_manually = true
      manual_request.registered_by_user_id = 9309
      manual_request.registered_at = lms_time
      manual_request.auto_update_prog = true
      manual_request.save
    end

    manual_request = VacRequest.new
    manual_request.begin = this_date
    manual_request.days_progressive = user_certificate ? ((days_progressive.days.to_i)*-1) : 0
    manual_request.registered_manually = true
    manual_request.registered_manually_description = registered_description
    manual_request.status = VacRequest.closed
    manual_request.user_id = user_id
    manual_request.registered_manually = true
    manual_request.registered_by_user_id = 9309
    manual_request.registered_at = lms_time
    manual_request.auto_update_prog = true
    manual_request.save
    update_accumulated_since(this_date.to_date, user_id, registered_description)
  end

end