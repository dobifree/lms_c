module LicensesModule
  include CalendarDatesModule
  include ScheduleDateModule

  def active_event
    event = LicEvent.find(params[:lic_event_id])
    return if event.active
    flash[:danger] = 'Evento inactivo'
    redirect_to root_path
  end

  def active_event_by_request
    request = LicRequest.find(params[:lic_request_id])
    event =  LicEvent.find(request.lic_event_id)
    return if event.active
    flash[:danger] = 'Evento inactivo'
    redirect_to root_path
  end

  def verify_register_or_validator
    group = BpGroup.user_active_group(lms_time, user_connected.id)
    validator = LicApprover.where(active: true, user_id: user_connected.id).first
    return if (group && group.any_bonus?(lms_time)) || (validator && validator.any_approvee?)
    flash[:danger] = 'No dispone de privilegios necesarios para ingresar a este módulo'
    redirect_to root_path
  end

  def verify_access_module
    ct_module = CtModule.where('cod = ? AND active = ?', 'licenses', true).first
    return if ct_module
    flash[:danger] = 'No dispone de privilegios necesarios para ingresar a este módulo'
    redirect_to root_path
  end

  def verify_active_request
    request = LicRequest.find(params[:lic_request_id])
    return if request.active
    flash[:danger] = 'No dispone de privilegios necesarios para gestionar sobre solicitud no activa'
    redirect_to root_path
  end

  def verify_active_event
    event = LicEvent.find(params[:lic_event_id])
    return if event.active
    flash[:danger] = 'No dispone de privilegios necesarios para gestionar evento no activo'
    redirect_to root_path
  end

  def available_register_to_edit_event

  end

  def available_register_to_edit
    return unless params[:lic_request_id]
    request = LicRequest.find(params[:lic_request_id])
    return if request && request.available_register_to_edit
    flash[:danger] = t('views.lic_registers.flash_messages.danger_not_available_to_register')
    redirect_to lic_register_index_path
  end

  def satisfy_conflict_periods(begin_date, end_date, user_id, request_id)
    licenses = conflict_licenses_periods(begin_date, end_date, user_id, request_id)
    message = ''
    licenses.each do |request|
      message += request.begin.strftime('%d/%m/%Y') + ' - ' + request.end.strftime('%d/%m/%Y') + ' (' + request.status_message + '). '
    end
    message = 'Conflicto con beneficio: ' + message if message.size > 0
    message.size > 0 ? message : ''
  end

  def conflict_licenses_periods(begin_date, end_date, user_id, request_id)
    requests = LicRequest.where(status: LicRequest.in_process_status, active: true).joins(:lic_event).where(lic_events: {active: true, user_id: user_id})
    licenses = []
    begin_date = begin_date.to_date
    end_date = end_date.to_date
    requests.each do |request|

      next if (request_id && request.id == request_id.to_i)
      next unless request.begin && request.end
      next unless (request.begin < end_date && request.end > begin_date)
      licenses << request
    end
    licenses
  end

  def satisfy_license_begin(user_id, license_id, request_begin, event_date)
    return nil unless license_id || request_begin || event_date

    license = BpLicense.find(license_id)
    messages = []
    return messages unless license.whenever

    event_date = event_date.to_date
    request_begin = request_begin.to_date

    if license.days_limit_only_laborable
      min_request_begin = license.days_since ? prev_days_laborable_license(license.only_laborable_days,user_id, event_date, license.days_since) : nil
      max_request_begin = license.days_until ? next_days_laborable_license(license.only_laborable_days, ser_id, event_date, license.days_until) : nil
    else
      min_request_begin = license.days_since ? (event_date + license.days_since.days) : nil
      max_request_begin = license.days_until ? (event_date + license.days_until.days) : nil
    end

    messages << 'Día mínimo de inicio de permiso es: ' + min_request_begin.strftime('%d/%m/%Y') if min_request_begin && (request_begin < min_request_begin)
    messages << 'Día máximo de inicio de permiso es: ' + max_request_begin.strftime('%d/%m/%Y') if max_request_begin && (request_begin > max_request_begin)
    messages
  end

  def laborable_by_license?(user_id, date_to_eval)
    return nil unless user_id || date_to_eval
    eval_1 = not_laborable_by_holiday?(date_to_eval, user_id)
    return false if eval_1
    eval_2 = not_laborable_by_sc?(date_to_eval, user_id)
    return false if eval_2
    true
  end

  def prev_days_laborable_license(only_laborable, user_id, date_to_eval, days)
    return nil unless user_id || date_to_eval || days
    pivot = date_to_eval.to_date
    days = days.to_i.abs

    loop do
      return pivot if days.zero?
      days -= 1 if !only_laborable || (laborable_by_license?(user_id, pivot) && only_laborable)
      pivot -= 1.day
    end
  end

  def next_days_laborable_license(only_laborable, user_id, date_to_eval, days)
    return nil unless user_id || date_to_eval || days
    pivot = date_to_eval.to_date
    days = days.to_i

    loop do
      return pivot if days.zero?
      days -= 1 if !only_laborable || (laborable_by_license?(user_id, pivot) && only_laborable)
      pivot += 1.day
    end
  end

  def license_end(only_laborable, begin_date, days, user_id)
    begin_date = begin_date.to_date
    days = days.to_i
    end_date = (begin_date + days.days - 1.day)
    return end_date unless only_laborable
    pivot_date = begin_date
    index = 0
    loop do
      if laborable_by_license?(user_id, pivot_date)
        end_date += 1.day
        days -= 1
      end
      pivot_date += 1.day
      index += 1
      break if days <= 0 || index == 150
    end
    end_date
  end

end