# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20190221101349) do

  create_table "admins", :force => true do |t|
    t.string   "username"
    t.string   "password_digest"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "tabla_borrada",   :default => true
  end

  create_table "area_superintendents", :force => true do |t|
    t.integer  "planning_process_company_unit_id"
    t.integer  "company_unit_area_id"
    t.integer  "user_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "area_superintendents", ["company_unit_area_id"], :name => "index_area_superintendents_on_company_unit_area_id"
  add_index "area_superintendents", ["planning_process_company_unit_id"], :name => "index_area_superintendents_on_planning_process_company_unit_id"
  add_index "area_superintendents", ["user_id"], :name => "index_area_superintendents_on_user_id"

  create_table "banner_characteristics", :force => true do |t|
    t.integer  "banner_id"
    t.integer  "characteristic_id"
    t.text     "match_value"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "banner_characteristics", ["banner_id"], :name => "index_banner_characteristics_on_banner_id"
  add_index "banner_characteristics", ["characteristic_id"], :name => "index_banner_characteristics_on_characteristic_id"

  create_table "banners", :force => true do |t|
    t.string   "name"
    t.string   "crypted_name"
    t.integer  "display_method"
    t.integer  "display_content"
    t.boolean  "active"
    t.boolean  "complement_visible"
    t.text     "link"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "location",           :default => 0
    t.integer  "position"
    t.boolean  "open_at_login",      :default => false
  end

  create_table "ben_cellphone_bill_items", :force => true do |t|
    t.string   "bill_folio"
    t.string   "description"
    t.string   "cellphone_number"
    t.string   "company_rut"
    t.decimal  "net_value",                         :precision => 12, :scale => 4
    t.decimal  "discount",                          :precision => 12, :scale => 4
    t.decimal  "net_total",                         :precision => 12, :scale => 4
    t.decimal  "net_with_taxes",                    :precision => 12, :scale => 4
    t.integer  "ben_cellphone_process_id"
    t.integer  "ben_cellphone_number_id"
    t.datetime "created_at",                                                                          :null => false
    t.datetime "updated_at",                                                                          :null => false
    t.integer  "bill_type"
    t.boolean  "not_charge_to_payroll",                                            :default => false
    t.datetime "not_charge_to_payroll_at"
    t.integer  "not_charge_to_payroll_by_user_id"
    t.string   "not_charge_to_payroll_description"
    t.boolean  "registered_manually",                                              :default => false
    t.datetime "registered_manually_at"
    t.integer  "registered_manually_by_user_id"
    t.string   "registered_manually_description"
    t.integer  "ben_user_cellphone_process_id"
    t.string   "bill_item_errors"
  end

  add_index "ben_cellphone_bill_items", ["ben_cellphone_number_id"], :name => "ben_cellphone_bill_items_cn_id"
  add_index "ben_cellphone_bill_items", ["ben_cellphone_process_id"], :name => "ben_cellphone_bill_items_cp_id"
  add_index "ben_cellphone_bill_items", ["ben_user_cellphone_process_id"], :name => "ben_user_cellphone_processes_bi_id"
  add_index "ben_cellphone_bill_items", ["not_charge_to_payroll_by_user_id"], :name => "ben_cellphone_bill_items_nctp_user_id"
  add_index "ben_cellphone_bill_items", ["registered_manually_by_user_id"], :name => "ben_cellphone_bill_items_rm_user_id"

  create_table "ben_cellphone_characteristics", :force => true do |t|
    t.integer  "characteristic_id"
    t.boolean  "active_search_people", :default => false
    t.integer  "pos_search_people"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.boolean  "active_reporting",     :default => false
  end

  add_index "ben_cellphone_characteristics", ["characteristic_id"], :name => "index_ben_cellphone_characteristics_on_characteristic_id"

  create_table "ben_cellphone_numbers", :force => true do |t|
    t.integer  "user_id"
    t.string   "number"
    t.string   "nemo_plan"
    t.boolean  "active"
    t.decimal  "subsidy",    :precision => 12, :scale => 4
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "ben_cellphone_numbers", ["user_id"], :name => "index_ben_cellphone_numbers_on_user_id"

  create_table "ben_cellphone_processes", :force => true do |t|
    t.integer  "month"
    t.integer  "year"
    t.boolean  "charged_to_payroll",                                 :default => false
    t.datetime "charged_to_payroll_at"
    t.integer  "charged_to_payroll_by_user_id"
    t.datetime "created_at",                                                            :null => false
    t.datetime "updated_at",                                                            :null => false
    t.datetime "generated_registered_not_manually_excel_at"
    t.datetime "generated_registered_manually_excel_at"
    t.integer  "generated_registered_not_manually_excel_by_user_id"
    t.integer  "generated_registered_manually_excel_by_user_id"
  end

  add_index "ben_cellphone_processes", ["charged_to_payroll_by_user_id"], :name => "ben_cellphone_processes_ctp_user_id"
  add_index "ben_cellphone_processes", ["generated_registered_manually_excel_by_user_id"], :name => "ben_user_cellphone_grm_id"
  add_index "ben_cellphone_processes", ["generated_registered_not_manually_excel_by_user_id"], :name => "ben_cellphone_processes_grnm_id"

  create_table "ben_custom_report_attribute_filters", :force => true do |t|
    t.integer  "ben_custom_report_id"
    t.string   "name"
    t.string   "match_value_string"
    t.text     "match_value_text"
    t.date     "match_value_date"
    t.integer  "match_value_int"
    t.float    "match_value_float"
    t.integer  "match_value_char_id"
    t.decimal  "match_value_decimal",  :precision => 12, :scale => 4
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.boolean  "match_value_boolean"
  end

  add_index "ben_custom_report_attribute_filters", ["ben_custom_report_id"], :name => "custom_report_attr_report_id"

  create_table "ben_custom_report_attributes", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.boolean  "active",               :default => false
    t.integer  "ben_custom_report_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
  end

  create_table "ben_custom_report_characteristics", :force => true do |t|
    t.integer  "ben_custom_report_id"
    t.integer  "ben_cellphone_characteristic_id"
    t.integer  "position"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "relation_type"
    t.string   "match_value_string"
    t.text     "match_value_text"
    t.date     "match_value_date"
    t.integer  "match_value_int"
    t.float    "match_value_float"
    t.integer  "match_value_char_id"
  end

  add_index "ben_custom_report_characteristics", ["ben_cellphone_characteristic_id"], :name => "ben_report_charac_cell_id"
  add_index "ben_custom_report_characteristics", ["ben_custom_report_id"], :name => "ben_report_charac_rep_id"

  create_table "ben_custom_reports", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ben_user_cellphone_processes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "ben_cellphone_number_id"
    t.integer  "ben_cellphone_process_id"
    t.decimal  "total_bill",                :precision => 12, :scale => 4
    t.decimal  "configured_subsidy",        :precision => 12, :scale => 4
    t.decimal  "applied_subsidy",           :precision => 12, :scale => 4
    t.decimal  "total_charged",             :precision => 12, :scale => 4
    t.datetime "created_at",                                                                :null => false
    t.datetime "updated_at",                                                                :null => false
    t.decimal  "registered_manually_total", :precision => 12, :scale => 4, :default => 0.0
    t.decimal  "plan_bill",                 :precision => 12, :scale => 4
    t.decimal  "equipment_bill",            :precision => 12, :scale => 4
  end

  add_index "ben_user_cellphone_processes", ["ben_cellphone_number_id"], :name => "index_ben_user_cellphone_processes_on_ben_cellphone_number_id"
  add_index "ben_user_cellphone_processes", ["ben_cellphone_process_id"], :name => "index_ben_user_cellphone_processes_on_ben_cellphone_process_id"
  add_index "ben_user_cellphone_processes", ["user_id"], :name => "index_ben_user_cellphone_processes_on_user_id"

  create_table "ben_user_custom_reports", :force => true do |t|
    t.integer  "ben_custom_report_id"
    t.integer  "user_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "ben_user_custom_reports", ["ben_custom_report_id"], :name => "ben_custom_report_user"
  add_index "ben_user_custom_reports", ["user_id"], :name => "index_ben_user_custom_reports_on_user_id"

  create_table "blog_entries", :force => true do |t|
    t.integer  "user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "blog_form_id"
  end

  add_index "blog_entries", ["blog_form_id"], :name => "index_blog_entries_on_blog_form_id"
  add_index "blog_entries", ["registered_by_user_id"], :name => "index_blog_entries_on_registered_by_user_id"
  add_index "blog_entries", ["user_id"], :name => "index_blog_entries_on_user_id"

  create_table "blog_entry_items", :force => true do |t|
    t.integer  "blog_entry_id"
    t.integer  "blog_form_item_id"
    t.text     "value"
    t.integer  "blog_list_item_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "blog_entry_items", ["blog_entry_id"], :name => "index_blog_entry_items_on_blog_entry_id"
  add_index "blog_entry_items", ["blog_form_item_id"], :name => "index_blog_entry_items_on_blog_form_item_id"
  add_index "blog_entry_items", ["blog_list_item_id"], :name => "index_blog_entry_items_on_blog_list_item_id"

  create_table "blog_form_items", :force => true do |t|
    t.integer  "blog_form_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.integer  "item_type"
    t.integer  "blog_list_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "mandatory",    :default => false
  end

  add_index "blog_form_items", ["blog_form_id"], :name => "index_blog_form_items_on_blog_form_id"
  add_index "blog_form_items", ["blog_list_id"], :name => "index_blog_form_items_on_blog_list_id"

  create_table "blog_form_permissions", :force => true do |t|
    t.integer  "blog_form_id"
    t.integer  "perm_type"
    t.boolean  "employee"
    t.boolean  "boss"
    t.boolean  "superior"
    t.boolean  "others"
    t.boolean  "active"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "blog_form_permissions", ["blog_form_id"], :name => "index_blog_form_permissions_on_blog_form_id"

  create_table "blog_forms", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "blog_list_items", :force => true do |t|
    t.integer  "blog_list_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "blog_list_items", ["blog_list_id"], :name => "index_blog_list_items_on_blog_list_id"

  create_table "blog_lists", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "bp_condition_vacations", :force => true do |t|
    t.integer  "bp_season_id"
    t.integer  "bp_rule_vacation_id"
    t.integer  "priority"
    t.integer  "bonus_days"
    t.float    "bonus_money"
    t.string   "bonus_currency"
    t.integer  "days_since"
    t.integer  "days_until"
    t.integer  "antiquity"
    t.datetime "since"
    t.datetime "until"
    t.integer  "period_times"
    t.boolean  "period_anniversary",           :default => true
    t.date     "period_begin_1"
    t.date     "period_begin_2"
    t.boolean  "period_condition_begin",       :default => true
    t.integer  "period_condition_min_days"
    t.float    "period_condition_min_percent"
    t.boolean  "season_condition_begin",       :default => true
    t.integer  "season_condition_min_days"
    t.float    "season_condition_min_percent"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.boolean  "bonus_only_laborable_day",     :default => true
    t.boolean  "refundable",                   :default => true
    t.integer  "days_max_accumulated"
    t.integer  "days_min_accumulated"
    t.integer  "period_times_type",            :default => 0
    t.boolean  "available",                    :default => true
    t.integer  "prev_cond_vac_id"
    t.integer  "next_cond_vac_id"
    t.datetime "unavailable_at"
    t.integer  "unavailable_by_admin_id"
    t.integer  "accumulated_type",             :default => 0
    t.boolean  "indef_contract_required",      :default => false
  end

  add_index "bp_condition_vacations", ["bp_rule_vacation_id"], :name => "bp_cond_vac_rule_vac"
  add_index "bp_condition_vacations", ["bp_season_id"], :name => "index_bp_condition_vacations_on_bp_season_id"
  add_index "bp_condition_vacations", ["deactivated_by_admin_id"], :name => "bp_cond_vac_deac"
  add_index "bp_condition_vacations", ["next_cond_vac_id"], :name => "index_bp_condition_vacations_on_next_cond_vac_id"
  add_index "bp_condition_vacations", ["prev_cond_vac_id"], :name => "index_bp_condition_vacations_on_prev_cond_vac_id"
  add_index "bp_condition_vacations", ["registered_by_admin_id"], :name => "bp_cond_vac_regs"
  add_index "bp_condition_vacations", ["unavailable_by_admin_id"], :name => "index_bp_condition_vacations_on_unavailable_by_admin_id"

  create_table "bp_event_files", :force => true do |t|
    t.integer  "bp_event_id"
    t.string   "name"
    t.text     "description"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.datetime "since"
    t.datetime "until"
  end

  add_index "bp_event_files", ["bp_event_id"], :name => "index_bp_event_files_on_bp_event_id"
  add_index "bp_event_files", ["deactivated_by_admin_id"], :name => "bp_event_files_deac_adm_id"
  add_index "bp_event_files", ["registered_by_admin_id"], :name => "bp_event_files_regs_adm_id"

  create_table "bp_events", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.datetime "since"
    t.datetime "until"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.string   "event_date_name",         :default => "Fecha de evento"
    t.integer  "position",                :default => 1
    t.boolean  "available",               :default => true
    t.datetime "unavailable_at"
    t.integer  "unavailable_by_admin_id"
  end

  add_index "bp_events", ["deactivated_by_admin_id"], :name => "index_bp_events_on_deactivated_by_admin_id"
  add_index "bp_events", ["registered_by_admin_id"], :name => "index_bp_events_on_registered_by_admin_id"
  add_index "bp_events", ["unavailable_by_admin_id"], :name => "index_bp_events_on_unavailable_by_admin_id"

  create_table "bp_forms", :force => true do |t|
    t.integer  "bp_event_id"
    t.string   "name"
    t.text     "description"
    t.boolean  "active",                  :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "bp_forms", ["bp_event_id"], :name => "index_bp_forms_on_bp_event_id"
  add_index "bp_forms", ["deactivated_by_admin_id"], :name => "bp_form_deac_adm_id"
  add_index "bp_forms", ["registered_by_admin_id"], :name => "bp_form_regis_adm_id"

  create_table "bp_general_rules_vacations", :force => true do |t|
    t.integer  "legal_days"
    t.integer  "progressive_days_step"
    t.integer  "progressive_max_days"
    t.integer  "request_min_days"
    t.integer  "request_max_days"
    t.integer  "accumulated_max_days"
    t.integer  "accumulated_max_real_days"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.integer  "bp_group_id"
    t.datetime "since"
    t.datetime "until"
    t.boolean  "only_laborable_day",        :default => true
    t.integer  "user_id"
    t.integer  "accumulated_type",          :default => 2
    t.text     "description"
  end

  add_index "bp_general_rules_vacations", ["bp_group_id"], :name => "index_bp_general_rules_vacations_on_bp_group_id"
  add_index "bp_general_rules_vacations", ["deactivated_by_admin_id"], :name => "bp_general_rules_vacations_deac_adm_id"
  add_index "bp_general_rules_vacations", ["registered_by_admin_id"], :name => "bp_general_rules_vacations_regis_adm_id"
  add_index "bp_general_rules_vacations", ["user_id"], :name => "index_bp_general_rules_vacations_on_user_id"

  create_table "bp_groups", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.datetime "since"
    t.datetime "until"
  end

  add_index "bp_groups", ["deactivated_by_admin_id"], :name => "index_bp_groups_on_deactivated_by_admin_id"
  add_index "bp_groups", ["registered_by_admin_id"], :name => "index_bp_groups_on_registered_by_admin_id"

  create_table "bp_groups_users", :force => true do |t|
    t.integer  "user_id"
    t.integer  "bp_group_id"
    t.datetime "since"
    t.datetime "until"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "registered_by_user_id"
    t.integer  "deactivated_by_user_id"
  end

  add_index "bp_groups_users", ["bp_group_id"], :name => "index_bp_groups_users_on_bp_group_id"
  add_index "bp_groups_users", ["deactivated_by_admin_id"], :name => "index_bp_groups_users_on_deactivated_by_admin_id"
  add_index "bp_groups_users", ["deactivated_by_user_id"], :name => "index_bp_groups_users_on_deactivated_by_user_id"
  add_index "bp_groups_users", ["registered_by_admin_id"], :name => "index_bp_groups_users_on_registered_by_admin_id"
  add_index "bp_groups_users", ["registered_by_user_id"], :name => "index_bp_groups_users_on_registered_by_user_id"
  add_index "bp_groups_users", ["user_id"], :name => "index_bp_groups_users_on_user_id"

  create_table "bp_item_options", :force => true do |t|
    t.integer  "bp_item_id"
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.boolean  "active",                  :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.integer  "prev_item_option_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "bp_item_options", ["bp_item_id"], :name => "index_bp_item_options_on_bp_item_id"
  add_index "bp_item_options", ["deactivated_by_admin_id"], :name => "bp_item_opt_deac_adm_id"
  add_index "bp_item_options", ["prev_item_option_id"], :name => "index_bp_item_options_on_prev_item_option_id"
  add_index "bp_item_options", ["registered_by_admin_id"], :name => "bp_item_opt_regis_adm_id"

  create_table "bp_items", :force => true do |t|
    t.integer  "bp_form_id"
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.integer  "item_type"
    t.boolean  "mandatory",               :default => true
    t.boolean  "active",                  :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.integer  "prev_item_id"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.integer  "next_item_id"
    t.boolean  "regularizable",           :default => false
  end

  add_index "bp_items", ["bp_form_id"], :name => "index_bp_items_on_bp_form_id"
  add_index "bp_items", ["deactivated_by_admin_id"], :name => "bp_item_deac_adm_id"
  add_index "bp_items", ["next_item_id"], :name => "index_bp_items_on_next_item_id"
  add_index "bp_items", ["prev_item_id"], :name => "index_bp_items_on_prev_item_id"
  add_index "bp_items", ["registered_by_admin_id"], :name => "bp_item_regis_adm_id"

  create_table "bp_licenses", :force => true do |t|
    t.integer  "bp_group_id"
    t.integer  "bp_event_id"
    t.string   "name"
    t.text     "description"
    t.integer  "days"
    t.decimal  "money",                        :precision => 12, :scale => 4
    t.string   "currency"
    t.integer  "antiquity"
    t.integer  "days_limit"
    t.boolean  "days_limit_only_laborable",                                   :default => true
    t.integer  "days_since"
    t.boolean  "only_laborable_days",                                         :default => true
    t.integer  "period_times"
    t.boolean  "period_anniversary",                                          :default => true
    t.datetime "period_begin_1"
    t.datetime "period_begin_2"
    t.boolean  "period_condition_begin",                                      :default => true
    t.integer  "period_condition_min_days"
    t.float    "period_condition_min_percent"
    t.integer  "bp_season_id"
    t.boolean  "season_condition_begin",                                      :default => true
    t.integer  "season_condition_min_days"
    t.float    "season_condition_min_percent"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "created_at",                                                                     :null => false
    t.datetime "updated_at",                                                                     :null => false
    t.datetime "since"
    t.datetime "until"
    t.boolean  "days_only_laborable_day",                                     :default => true
    t.boolean  "refundable",                                                  :default => true
    t.boolean  "partially_taken",                                             :default => false
    t.integer  "min_by_request"
    t.integer  "max_by_request"
    t.boolean  "whenever",                                                    :default => true
    t.integer  "days_until"
    t.integer  "hours"
    t.boolean  "with_period",                                                 :default => true
    t.boolean  "with_event",                                                  :default => true
    t.integer  "period_times_type",                                           :default => 0
    t.boolean  "available",                                                   :default => true
    t.integer  "prev_license_id"
    t.integer  "next_license_id"
    t.datetime "unavailable_at"
    t.integer  "unavailable_by_admin_id"
    t.boolean  "indef_contract_required",                                     :default => false
    t.boolean  "need_approval",                                               :default => true
  end

  add_index "bp_licenses", ["bp_event_id"], :name => "index_bp_licenses_on_bp_event_id"
  add_index "bp_licenses", ["bp_group_id"], :name => "index_bp_licenses_on_bp_group_id"
  add_index "bp_licenses", ["bp_season_id"], :name => "index_bp_licenses_on_bp_season_id"
  add_index "bp_licenses", ["deactivated_by_admin_id"], :name => "index_bp_licenses_on_deactivated_by_admin_id"
  add_index "bp_licenses", ["next_license_id"], :name => "index_bp_licenses_on_next_license_id"
  add_index "bp_licenses", ["prev_license_id"], :name => "index_bp_licenses_on_prev_license_id"
  add_index "bp_licenses", ["registered_by_admin_id"], :name => "index_bp_licenses_on_registered_by_admin_id"
  add_index "bp_licenses", ["unavailable_by_admin_id"], :name => "index_bp_licenses_on_unavailable_by_admin_id"

  create_table "bp_progressive_days", :force => true do |t|
    t.integer  "bp_general_rules_vacation_id"
    t.integer  "year"
    t.integer  "days"
    t.boolean  "active",                       :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  add_index "bp_progressive_days", ["bp_general_rules_vacation_id"], :name => "index_bp_progressive_days_on_bp_general_rules_vacation_id"
  add_index "bp_progressive_days", ["deactivated_by_admin_id"], :name => "bp_progress_deact_admin_id"
  add_index "bp_progressive_days", ["registered_by_admin_id"], :name => "bp_progress_regis_admin_id"

  create_table "bp_rule_vacations", :force => true do |t|
    t.integer  "bp_group_id"
    t.string   "name"
    t.text     "description"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.datetime "since"
    t.datetime "until"
    t.boolean  "available",               :default => true
    t.datetime "unavailable_at"
    t.integer  "unavailable_by_admin_id"
  end

  add_index "bp_rule_vacations", ["bp_group_id"], :name => "index_bp_rule_vacations_on_bp_group_id"
  add_index "bp_rule_vacations", ["deactivated_by_admin_id"], :name => "index_bp_rule_vacations_on_deactivated_by_admin_id"
  add_index "bp_rule_vacations", ["registered_by_admin_id"], :name => "index_bp_rule_vacations_on_registered_by_admin_id"
  add_index "bp_rule_vacations", ["unavailable_by_admin_id"], :name => "index_bp_rule_vacations_on_unavailable_by_admin_id"

  create_table "bp_rules_warnings", :force => true do |t|
    t.integer  "bp_general_rules_vacation_id"
    t.string   "name"
    t.text     "description"
    t.boolean  "restrictive",                  :default => false
    t.boolean  "active",                       :default => true
    t.integer  "min_accumulated_days"
    t.integer  "max_accumulated_days"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.integer  "accumulated_type",             :default => 0
  end

  add_index "bp_rules_warnings", ["bp_general_rules_vacation_id"], :name => "bp_warnings_general_id"
  add_index "bp_rules_warnings", ["deactivated_by_admin_id"], :name => "bp_warnings_deact_admin_id"
  add_index "bp_rules_warnings", ["deactivated_by_admin_id"], :name => "bp_warnings_deact_user_id"
  add_index "bp_rules_warnings", ["registered_by_admin_id"], :name => "bp_warnings_regis_admin_id"
  add_index "bp_rules_warnings", ["registered_by_admin_id"], :name => "bp_warnings_regis_user_id"

  create_table "bp_season_periods", :force => true do |t|
    t.integer  "bp_season_id"
    t.date     "begin"
    t.date     "end"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.datetime "since"
    t.datetime "until"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
  end

  add_index "bp_season_periods", ["bp_season_id"], :name => "index_bp_season_periods_on_bp_season_id"
  add_index "bp_season_periods", ["deactivated_by_admin_id"], :name => "bp_season_periods_deac_adm_id"
  add_index "bp_season_periods", ["registered_by_admin_id"], :name => "index_bp_season_periods_on_registered_by_admin_id"

  create_table "bp_seasons", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.datetime "since"
    t.datetime "until"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_admin_id"
    t.datetime "registered_at"
    t.integer  "registered_by_admin_id"
  end

  add_index "bp_seasons", ["deactivated_by_admin_id"], :name => "index_bp_seasons_on_deactivated_by_admin_id"
  add_index "bp_seasons", ["registered_by_admin_id"], :name => "index_bp_seasons_on_registered_by_admin_id"

  create_table "bp_warning_exceptions", :force => true do |t|
    t.integer  "bp_groups_user_id"
    t.integer  "max_usages"
    t.datetime "active_since"
    t.datetime "active_until"
    t.boolean  "available",               :default => true
    t.datetime "unavailable_at"
    t.integer  "unavailable_by_user_id"
    t.text     "unavailable_description"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.text     "registered_description"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "bp_warning_exceptions", ["bp_groups_user_id"], :name => "index_bp_warning_exceptions_on_bp_groups_user_id"
  add_index "bp_warning_exceptions", ["registered_by_user_id"], :name => "index_bp_warning_exceptions_on_registered_by_user_id"
  add_index "bp_warning_exceptions", ["unavailable_by_user_id"], :name => "index_bp_warning_exceptions_on_unavailable_by_user_id"

  create_table "brc_apv_institutions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "brc_bonus_items", :force => true do |t|
    t.integer  "brc_process_id"
    t.integer  "brc_member_id"
    t.integer  "brc_category_id"
    t.boolean  "p_a",                                            :default => true
    t.float    "percentage"
    t.decimal  "amount",          :precision => 20, :scale => 4, :default => 0.0
    t.boolean  "rejected",                                       :default => false
    t.text     "val_comment"
    t.datetime "created_at",                                                        :null => false
    t.datetime "updated_at",                                                        :null => false
    t.integer  "secu_cohade_id"
    t.decimal  "amount_c",        :precision => 20, :scale => 4, :default => 0.0
    t.boolean  "validated",                                      :default => false
  end

  add_index "brc_bonus_items", ["brc_category_id"], :name => "index_brc_bonus_items_on_brc_category_id"
  add_index "brc_bonus_items", ["brc_member_id"], :name => "index_brc_bonus_items_on_brc_member_id"
  add_index "brc_bonus_items", ["brc_process_id"], :name => "index_brc_bonus_items_on_brc_process_id"
  add_index "brc_bonus_items", ["secu_cohade_id"], :name => "index_brc_bonus_items_on_secu_cohade_id"

  create_table "brc_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "brc_definer_companies", :force => true do |t|
    t.integer  "brc_definer_id"
    t.integer  "characteristic_value_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "brc_definer_companies", ["brc_definer_id"], :name => "index_brc_definer_companies_on_brc_definer_id"
  add_index "brc_definer_companies", ["characteristic_value_id"], :name => "index_brc_definer_companies_on_characteristic_value_id"

  create_table "brc_definers", :force => true do |t|
    t.integer  "brc_process_id"
    t.integer  "user_id"
    t.integer  "user_validate_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "brc_definers", ["brc_process_id"], :name => "index_brc_definers_on_brc_process_id"
  add_index "brc_definers", ["user_id"], :name => "index_brc_definers_on_user_id"
  add_index "brc_definers", ["user_validate_id"], :name => "index_brc_definers_on_user_validate_id"

  create_table "brc_mails", :force => true do |t|
    t.string   "def_begin_subject"
    t.text     "def_begin_body"
    t.string   "def_rej_subject"
    t.text     "def_rej_body"
    t.string   "val_begin_subject"
    t.text     "val_begin_body"
    t.string   "def_remind_subject"
    t.text     "def_remind_body"
    t.string   "val_remind_subject"
    t.text     "val_remind_body"
    t.string   "member_begin_subject"
    t.text     "member_begin_body"
    t.text     "member_begin_extra1_body"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "brc_member_files", :force => true do |t|
    t.integer  "brc_member_id"
    t.string   "name"
    t.string   "file_name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "brc_member_files", ["brc_member_id"], :name => "index_brc_member_files_on_brc_member_id"

  create_table "brc_members", :force => true do |t|
    t.integer  "brc_process_id"
    t.integer  "user_id"
    t.datetime "created_at",                                                                :null => false
    t.datetime "updated_at",                                                                :null => false
    t.integer  "user_define_id"
    t.integer  "user_validate_id"
    t.boolean  "status_def",                                             :default => false
    t.boolean  "status_val",                                             :default => false
    t.boolean  "status_bon_cal",                                         :default => false
    t.decimal  "bonus_total",             :precision => 20, :scale => 4, :default => 0.0
    t.decimal  "bonus_liquido",           :precision => 20, :scale => 4, :default => 0.0
    t.decimal  "bonus_retencion",         :precision => 20, :scale => 4, :default => 0.0
    t.float    "bonus_p_retencion",                                      :default => 0.0
    t.boolean  "status_choose",                                          :default => false
    t.decimal  "bonus_apv",               :precision => 20, :scale => 4, :default => 0.0
    t.decimal  "bonus_convenio",          :precision => 20, :scale => 4, :default => 0.0
    t.boolean  "status_rej",                                             :default => false
    t.boolean  "bonus_cc",                                               :default => true
    t.integer  "brc_apv_institution_id"
    t.integer  "brc_con_institution_id"
    t.decimal  "bonus_con_uf",            :precision => 20, :scale => 4, :default => 0.0
    t.boolean  "choose_by_own",                                          :default => false
    t.datetime "choose_date"
    t.boolean  "format_rejected",                                        :default => false
    t.string   "format_rejected_comment"
  end

  add_index "brc_members", ["brc_apv_institution_id"], :name => "index_brc_members_on_brc_apv_institution_id"
  add_index "brc_members", ["brc_process_id"], :name => "index_brc_members_on_brc_process_id"
  add_index "brc_members", ["user_define_id"], :name => "index_brc_members_on_user_define_id"
  add_index "brc_members", ["user_id"], :name => "index_brc_members_on_user_id"
  add_index "brc_members", ["user_validate_id"], :name => "index_brc_members_on_user_validate_id"

  create_table "brc_process_categ_vals", :force => true do |t|
    t.integer  "brc_process_id"
    t.integer  "brc_category_id"
    t.integer  "characteristic_value_id"
    t.text     "comment_val"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "brc_process_categ_vals", ["brc_category_id"], :name => "index_brc_process_categ_vals_on_brc_category_id"
  add_index "brc_process_categ_vals", ["brc_process_id"], :name => "index_brc_process_categ_vals_on_brc_process_id"
  add_index "brc_process_categ_vals", ["characteristic_value_id"], :name => "index_brc_process_categ_vals_on_characteristic_value_id"

  create_table "brc_processes", :force => true do |t|
    t.string   "period"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.boolean  "active",         :default => true
    t.integer  "year"
    t.float    "uf_value"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "choose_date"
    t.integer  "liq_process_id"
    t.datetime "def_date"
    t.datetime "val_date"
    t.integer  "secu_cohade_id"
    t.string   "alias",          :default => "Bono Rol Comercial"
  end

  add_index "brc_processes", ["liq_process_id"], :name => "index_brc_processes_on_liq_process_id"
  add_index "brc_processes", ["secu_cohade_id"], :name => "index_brc_processes_on_secu_cohade_id"

  create_table "brg_bonus", :force => true do |t|
    t.integer  "prev_bonus_id"
    t.integer  "brg_process_id"
    t.integer  "brg_group_user_id"
    t.decimal  "actual_value",           :precision => 20, :scale => 5
    t.decimal  "registered_value",       :precision => 20, :scale => 5
    t.boolean  "active",                                                :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.text     "registered_description"
    t.boolean  "paid"
    t.datetime "paid_at"
    t.integer  "paid_by_user_id"
    t.text     "paid_description"
    t.datetime "created_at",                                                               :null => false
    t.datetime "updated_at",                                                               :null => false
    t.integer  "brg_lvl_percentage_id"
    t.boolean  "ready_to_go",                                           :default => false
    t.datetime "ready_to_go_at"
    t.decimal  "income_times",           :precision => 12, :scale => 4
    t.decimal  "bonus_target",           :precision => 20, :scale => 5, :default => 0.0
    t.decimal  "first_value",            :precision => 20, :scale => 5, :default => 0.0
  end

  add_index "brg_bonus", ["brg_group_user_id"], :name => "index_brg_bonus_on_brg_group_user_id"
  add_index "brg_bonus", ["brg_lvl_percentage_id"], :name => "index_brg_bonus_on_brg_lvl_percentage_id"
  add_index "brg_bonus", ["brg_process_id"], :name => "index_brg_bonus_on_brg_process_id"
  add_index "brg_bonus", ["deactivated_by_user_id"], :name => "index_brg_bonus_on_deactivated_by_user_id"
  add_index "brg_bonus", ["paid_by_user_id"], :name => "index_brg_bonus_on_paid_by_user_id"
  add_index "brg_bonus", ["prev_bonus_id"], :name => "index_brg_bonus_on_prev_bonus_id"
  add_index "brg_bonus", ["registered_by_user_id"], :name => "index_brg_bonus_on_registered_by_user_id"

  create_table "brg_bonus_percentages", :force => true do |t|
    t.integer  "brg_bonus_id"
    t.integer  "brg_percentage_conversion_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "brg_bonus_percentages", ["brg_bonus_id"], :name => "index_brg_bonus_percentages_on_brg_bonus_id"
  add_index "brg_bonus_percentages", ["brg_percentage_conversion_id"], :name => "index_brg_bonus_percentages_on_brg_percentage_conversion_id"

  create_table "brg_char_percentages", :force => true do |t|
    t.integer  "brg_process_id"
    t.string   "cod"
    t.float    "percentage"
    t.boolean  "active",                  :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "characteristic_value_id"
  end

  add_index "brg_char_percentages", ["brg_process_id"], :name => "index_brg_char_percentages_on_brg_process_id"
  add_index "brg_char_percentages", ["characteristic_value_id"], :name => "index_brg_char_percentages_on_characteristic_value_id"
  add_index "brg_char_percentages", ["deactivated_by_user_id"], :name => "index_brg_char_percentages_on_deactivated_by_user_id"
  add_index "brg_char_percentages", ["registered_by_user_id"], :name => "index_brg_char_percentages_on_registered_by_user_id"

  create_table "brg_group_user_comments", :force => true do |t|
    t.integer  "brg_group_user_id"
    t.text     "comment"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.boolean  "active",                :default => true
    t.integer  "prev_comment_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
  end

  add_index "brg_group_user_comments", ["brg_group_user_id"], :name => "index_brg_group_user_comments_on_brg_group_user_id"
  add_index "brg_group_user_comments", ["prev_comment_id"], :name => "index_brg_group_user_comments_on_prev_comment_id"

  create_table "brg_group_users", :force => true do |t|
    t.integer  "user_id"
    t.integer  "brg_group_id"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "brg_group_users", ["brg_group_id"], :name => "index_brg_group_users_on_brg_group_id"
  add_index "brg_group_users", ["deactivated_by_user_id"], :name => "index_brg_group_users_on_deactivated_by_user_id"
  add_index "brg_group_users", ["registered_by_user_id"], :name => "index_brg_group_users_on_registered_by_user_id"
  add_index "brg_group_users", ["user_id"], :name => "index_brg_group_users_on_user_id"

  create_table "brg_groups", :force => true do |t|
    t.string   "group_cod"
    t.string   "name"
    t.text     "description"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "brg_process_id"
  end

  add_index "brg_groups", ["brg_process_id"], :name => "index_brg_groups_on_brg_process_id"
  add_index "brg_groups", ["deactivated_by_user_id"], :name => "index_brg_groups_on_deactivated_by_user_id"
  add_index "brg_groups", ["registered_by_user_id"], :name => "index_brg_groups_on_registered_by_user_id"

  create_table "brg_lvl_percentages", :force => true do |t|
    t.integer  "jj_characteristic_value_id"
    t.float    "max_prt_bonus"
    t.float    "pond_company"
    t.float    "pond_individual"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "brg_process_id"
    t.float    "income_times_target"
  end

  add_index "brg_lvl_percentages", ["brg_process_id"], :name => "index_brg_lvl_percentages_on_brg_process_id"
  add_index "brg_lvl_percentages", ["jj_characteristic_value_id"], :name => "index_brg_lvl_percentages_on_jj_characteristic_value_id"
  add_index "brg_lvl_percentages", ["registered_by_user_id"], :name => "index_brg_lvl_percentages_on_registered_by_user_id"

  create_table "brg_manage_relations", :force => true do |t|
    t.integer  "manager_id"
    t.integer  "managee_id"
    t.integer  "group_to_manage_id"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "brg_manage_relations", ["deactivated_by_user_id"], :name => "index_brg_manage_relations_on_deactivated_by_user_id"
  add_index "brg_manage_relations", ["group_to_manage_id"], :name => "index_brg_manage_relations_on_group_to_manage_id"
  add_index "brg_manage_relations", ["managee_id"], :name => "index_brg_manage_relations_on_managee_id"
  add_index "brg_manage_relations", ["manager_id"], :name => "index_brg_manage_relations_on_manager_id"
  add_index "brg_manage_relations", ["registered_by_user_id"], :name => "index_brg_manage_relations_on_registered_by_user_id"

  create_table "brg_managers", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.integer  "level"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "brg_process_id"
  end

  add_index "brg_managers", ["brg_process_id"], :name => "index_brg_managers_on_brg_process_id"
  add_index "brg_managers", ["deactivated_by_user_id"], :name => "index_brg_managers_on_deactivated_by_user_id"
  add_index "brg_managers", ["registered_by_user_id"], :name => "index_brg_managers_on_registered_by_user_id"
  add_index "brg_managers", ["user_id"], :name => "index_brg_managers_on_user_id"

  create_table "brg_percentage_conversions", :force => true do |t|
    t.float    "before"
    t.float    "after"
    t.integer  "type_1"
    t.boolean  "individual",             :default => false
    t.boolean  "group",                  :default => false
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "brg_process_id"
  end

  add_index "brg_percentage_conversions", ["brg_process_id"], :name => "brg_per_conv_procss_id"
  add_index "brg_percentage_conversions", ["deactivated_by_user_id"], :name => "index_brg_percentage_conversions_on_deactivated_by_user_id"
  add_index "brg_percentage_conversions", ["registered_by_user_id"], :name => "index_brg_percentage_conversions_on_registered_by_user_id"

  create_table "brg_processes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "period_begin"
    t.datetime "period_end"
    t.datetime "due_date"
    t.integer  "status",                 :default => 1
    t.text     "status_description"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.integer  "pe_process_id"
    t.integer  "quali_evaluation_id"
    t.integer  "quanti_evaluation_id"
    t.float    "quali_percentage"
    t.float    "quanti_percentage"
    t.integer  "reference_year"
    t.string   "alias",                  :default => "Bono Rol General"
  end

  add_index "brg_processes", ["deactivated_by_user_id"], :name => "index_brg_processes_on_deactivated_by_user_id"
  add_index "brg_processes", ["pe_process_id"], :name => "index_brg_processes_on_pe_process_id"
  add_index "brg_processes", ["quali_evaluation_id"], :name => "index_brg_processes_on_quali_evaluation_id"
  add_index "brg_processes", ["quanti_evaluation_id"], :name => "index_brg_processes_on_quanti_evaluation_id"
  add_index "brg_processes", ["registered_by_user_id"], :name => "index_brg_processes_on_registered_by_user_id"

  create_table "brg_reported_bonus", :force => true do |t|
    t.integer  "brg_bonus_id"
    t.integer  "brg_report_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "brg_reported_bonus", ["brg_bonus_id"], :name => "index_brg_reported_bonus_on_brg_bonus_id"
  add_index "brg_reported_bonus", ["brg_report_id"], :name => "index_brg_reported_bonus_on_brg_report_id"

  create_table "brg_reports", :force => true do |t|
    t.integer  "brg_process_id"
    t.boolean  "partial_report",         :default => false
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "brg_reports", ["brg_process_id"], :name => "index_brg_reports_on_brg_process_id"
  add_index "brg_reports", ["deactivated_by_user_id"], :name => "index_brg_reports_on_deactivated_by_user_id"
  add_index "brg_reports", ["registered_by_user_id"], :name => "index_brg_reports_on_registered_by_user_id"

  create_table "cc_characteristics", :force => true do |t|
    t.integer  "characteristic_id"
    t.integer  "position"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "cc_characteristics", ["characteristic_id"], :name => "index_cc_characteristics_on_characteristic_id"

  create_table "characteristic_types", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "characteristic_value_parents", :force => true do |t|
    t.integer  "characteristic_value_id"
    t.integer  "characteristic_parent_id"
    t.integer  "characteristic_value_parent_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "characteristic_value_parents", ["characteristic_parent_id"], :name => "cvp_characteristic_parent_id"
  add_index "characteristic_value_parents", ["characteristic_value_id"], :name => "cvp_characteristic_value_id"
  add_index "characteristic_value_parents", ["characteristic_value_parent_id"], :name => "cvp_characteristic_value_parent_id"

  create_table "characteristic_values", :force => true do |t|
    t.integer  "characteristic_id"
    t.string   "value_string"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.integer  "position",          :default => 1
    t.boolean  "active",            :default => true
    t.string   "value_string_2"
  end

  add_index "characteristic_values", ["characteristic_id"], :name => "index_characteristic_values_on_characteristic_id"

  create_table "characteristics", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.boolean  "publica",                        :default => false
    t.boolean  "mini_ficha",                     :default => false
    t.integer  "orden_mini_ficha"
    t.boolean  "planning_process"
    t.integer  "orden_planning_process"
    t.boolean  "porcentaje_participante_sence",  :default => false
    t.boolean  "basic_report",                   :default => false
    t.integer  "orden_basic_report"
    t.boolean  "hr_process_assessment"
    t.integer  "orden_hr_process_assessment"
    t.integer  "position"
    t.boolean  "filter_assessment_report",       :default => false
    t.integer  "order_filter_assessment_report"
    t.integer  "characteristic_type_id"
    t.boolean  "pe_rep_consolidate_results"
    t.integer  "data_type_id",                   :default => 0
    t.boolean  "updated_by_manager",             :default => false
    t.boolean  "updated_by_user",                :default => false
    t.integer  "boss_characteristic_id"
    t.integer  "register_characteristic_id"
    t.integer  "parent_characteristic_id"
    t.integer  "register_position"
    t.boolean  "restricted_to_managers",         :default => false
    t.boolean  "has_history",                    :default => true
    t.boolean  "is_company",                     :default => false
    t.boolean  "encrypted",                      :default => false
    t.boolean  "subase",                         :default => false
    t.boolean  "subaso",                         :default => false
    t.boolean  "query_by_boss",                  :default => true
    t.boolean  "query_by_user",                  :default => true
    t.boolean  "query_by_others",                :default => true
    t.boolean  "currency_subaso",                :default => false
    t.boolean  "from_date_work",                 :default => false
    t.boolean  "from_date_progresivas",          :default => false
    t.boolean  "from_date_r_progresivas",        :default => false
    t.boolean  "rol_security"
    t.boolean  "has_children"
    t.boolean  "required_new"
    t.integer  "characteristic_value_id"
    t.boolean  "char_value_selected",            :default => true
    t.boolean  "contrato_plazo_secu",            :default => false
    t.boolean  "tipo_contrato_secu",             :default => false
    t.integer  "required_new_position"
    t.boolean  "required_new_c",                 :default => false
  end

  add_index "characteristics", ["boss_characteristic_id"], :name => "index_characteristics_on_boss_characteristic_id"
  add_index "characteristics", ["characteristic_type_id"], :name => "index_characteristics_on_characteristic_type_id"
  add_index "characteristics", ["characteristic_value_id"], :name => "index_characteristics_on_characteristic_value_id"
  add_index "characteristics", ["data_type_id"], :name => "index_characteristics_on_data_type_id"
  add_index "characteristics", ["parent_characteristic_id"], :name => "index_characteristics_on_parent_characteristic_id"
  add_index "characteristics", ["register_characteristic_id"], :name => "index_characteristics_on_register_characteristic_id"

  create_table "companies", :force => true do |t|
    t.string   "nombre"
    t.string   "codigo"
    t.datetime "created_at",                                                                    :null => false
    t.datetime "updated_at",                                                                    :null => false
    t.string   "directorio"
    t.string   "email_mesa_ayuda"
    t.boolean  "modulo_sence",                       :default => false
    t.boolean  "show_header_in_login",               :default => true
    t.string   "texto_username_login"
    t.text     "texto_cambio_password"
    t.string   "alias_username"
    t.string   "alias_user"
    t.string   "time_zone",                          :default => "America/Lima"
    t.boolean  "display_code_in_user_profile",       :default => false
    t.string   "header_text_login_box",              :default => "Ingrese sus datos de acceso"
    t.string   "recover_pass_text_login_box",        :default => "¿Olvidó su contraseña?"
    t.string   "user_profile_tab_text_dashboard",    :default => "Perfil de Usuario"
    t.string   "locale",                             :default => "es_PE"
    t.string   "training_type_alias",                :default => "Tipo de capacitación"
    t.boolean  "training_type_user_courses_submenu", :default => false
    t.string   "notifications_email",                :default => "notificaciones@exa.pe"
    t.boolean  "accepts_comments",                   :default => true
    t.boolean  "accepts_values",                     :default => true
    t.boolean  "tabla_borrada",                      :default => true
  end

  create_table "company_unit_area_managers", :force => true do |t|
    t.integer  "planning_process_company_unit_id"
    t.integer  "company_unit_area_id"
    t.integer  "user_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "company_unit_area_managers", ["company_unit_area_id"], :name => "cuam_company_unit_area_id"
  add_index "company_unit_area_managers", ["planning_process_company_unit_id"], :name => "cuam_planning_process_company_unit_id"
  add_index "company_unit_area_managers", ["user_id"], :name => "cuam_user_id"

  create_table "company_unit_areas", :force => true do |t|
    t.string   "nombre"
    t.integer  "company_unit_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "company_unit_areas", ["company_unit_id"], :name => "index_company_unit_areas_on_company_unit_id"

  create_table "company_unit_managers", :force => true do |t|
    t.integer  "planning_process_company_unit_id"
    t.integer  "user_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "company_unit_managers", ["planning_process_company_unit_id"], :name => "index_company_unit_managers_on_planning_process_company_unit_id"
  add_index "company_unit_managers", ["user_id"], :name => "index_company_unit_managers_on_user_id"

  create_table "company_units", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "country_id"
    t.integer  "planning_process_id"
  end

  create_table "countries", :force => true do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "course_certificates", :force => true do |t|
    t.integer  "course_id"
    t.string   "template"
    t.date     "rango_desde"
    t.date     "rango_hasta"
    t.boolean  "nombre"
    t.integer  "nombre_x"
    t.integer  "nombre_y"
    t.boolean  "desde"
    t.integer  "desde_x"
    t.integer  "desde_y"
    t.boolean  "hasta"
    t.integer  "hasta_x"
    t.integer  "hasta_y"
    t.boolean  "inicio"
    t.integer  "inicio_x"
    t.integer  "inicio_y"
    t.boolean  "fin"
    t.integer  "fin_x"
    t.integer  "fin_y"
    t.boolean  "nota"
    t.integer  "nota_x"
    t.integer  "nota_y"
    t.string   "fecha_source"
    t.integer  "fecha_source_delay",  :default => 0
    t.boolean  "fecha"
    t.integer  "fecha_x"
    t.integer  "fecha_y"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.boolean  "nombre_curso"
    t.integer  "nombre_curso_x"
    t.integer  "nombre_curso_y"
    t.string   "nombre_color",        :default => "000000"
    t.string   "nombre_curso_color",  :default => "000000"
    t.string   "fecha_color",         :default => "000000"
    t.string   "desde_color",         :default => "000000"
    t.string   "hasta_color",         :default => "000000"
    t.string   "inicio_color",        :default => "000000"
    t.string   "fin_color",           :default => "000000"
    t.string   "nota_color",          :default => "000000"
    t.string   "nombre_letra",        :default => "Helvetica"
    t.string   "nombre_curso_letra",  :default => "Helvetica"
    t.string   "fecha_letra",         :default => "Helvetica"
    t.string   "desde_letra",         :default => "Helvetica"
    t.string   "hasta_letra",         :default => "Helvetica"
    t.string   "inicio_letra",        :default => "Helvetica"
    t.string   "fin_letra",           :default => "Helvetica"
    t.string   "nota_letra",          :default => "Helvetica"
    t.integer  "nombre_size",         :default => 20
    t.integer  "nombre_curso_size",   :default => 20
    t.integer  "fecha_size",          :default => 20
    t.integer  "desde_size",          :default => 20
    t.integer  "hasta_size",          :default => 20
    t.integer  "inicio_size",         :default => 20
    t.integer  "fin_size",            :default => 20
    t.integer  "nota_size",           :default => 20
    t.string   "desde_formato",       :default => "day_snmonth_year"
    t.string   "hasta_formato",       :default => "day_snmonth_year"
    t.string   "inicio_formato",      :default => "day_snmonth_year"
    t.string   "fin_formato",         :default => "day_snmonth_year"
    t.string   "fecha_formato",       :default => "full_date"
    t.integer  "program_instance_id"
    t.integer  "program_id"
  end

  add_index "course_certificates", ["course_id"], :name => "index_course_certificates_on_course_id"

  create_table "course_comments", :force => true do |t|
    t.integer  "course_id"
    t.integer  "user_id"
    t.datetime "fecha"
    t.text     "comentario"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "course_comments", ["course_id"], :name => "index_course_comments_on_course_id"
  add_index "course_comments", ["user_id"], :name => "index_course_comments_on_user_id"

  create_table "course_files", :force => true do |t|
    t.string   "nombre"
    t.string   "crypted_name"
    t.string   "extension"
    t.text     "descripcion"
    t.integer  "course_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "size"
  end

  add_index "course_files", ["course_id"], :name => "index_course_files_on_course_id"

  create_table "course_training_characteristics", :force => true do |t|
    t.integer  "course_id"
    t.integer  "training_characteristic_id"
    t.string   "value_string"
    t.integer  "value_int"
    t.integer  "training_characteristic_value_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "program_course_instance_id"
  end

  add_index "course_training_characteristics", ["course_id"], :name => "ctc_cid"
  add_index "course_training_characteristics", ["program_course_instance_id"], :name => "course_training_characteristics_pci_id"
  add_index "course_training_characteristics", ["training_characteristic_id"], :name => "ctc_tcid"
  add_index "course_training_characteristics", ["training_characteristic_value_id"], :name => "ctc_tcvid"

  create_table "courses", :force => true do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.string   "nombre_corto"
    t.integer  "numero_oportunidades",          :default => 1
    t.integer  "duracion",                      :default => 0
    t.text     "objetivos"
    t.float    "dedicacion_estimada",           :default => 0.0
    t.integer  "nota_minima"
    t.integer  "dncp_id"
    t.boolean  "clonable",                      :default => false
    t.string   "imagen"
    t.boolean  "eval_presencial",               :default => false
    t.boolean  "poll_presencial",               :default => false
    t.boolean  "asistencia_presencial",         :default => false
    t.string   "sence_code"
    t.float    "sence_hours"
    t.float    "sence_student_value"
    t.boolean  "muestra_porcentajes",           :default => false
    t.boolean  "percentage_minimum_grade",      :default => false
    t.float    "sence_hour_value"
    t.integer  "sence_students_number"
    t.string   "sence_area"
    t.string   "sence_speciality"
    t.string   "sence_group"
    t.string   "sence_training_type"
    t.date     "sence_authorization_date"
    t.date     "sence_end_validity_date"
    t.string   "sence_name"
    t.string   "sence_otec_name"
    t.string   "sence_otec_address"
    t.string   "sence_otec_telephone"
    t.string   "sence_region"
    t.integer  "minimum_assistance",            :default => 0
    t.integer  "sence_maximum_students_number"
    t.integer  "sence_minimum_students_number"
    t.float    "maximum_reference_value"
    t.boolean  "calculate_percentage",          :default => false
    t.string   "unit_alias",                    :default => "unidad"
    t.integer  "first_unit_number",             :default => 1
    t.integer  "training_type_id"
    t.boolean  "managed_by_manager",            :default => false
    t.boolean  "active",                        :default => true
    t.text     "finish_msg_success"
    t.text     "finish_msg_failure"
    t.text     "finish_msg_failure_oportunity"
    t.boolean  "evaluado",                      :default => true
    t.boolean  "control_total_asistencia",      :default => false
    t.boolean  "asistencia_matricula",          :default => false
    t.integer  "training_mode_id"
    t.integer  "asistencia_minima",             :default => 100
  end

  add_index "courses", ["training_mode_id"], :name => "index_courses_on_training_mode_id"
  add_index "courses", ["training_type_id"], :name => "index_courses_on_training_type_id"

  create_table "ct_menus", :force => true do |t|
    t.integer  "position"
    t.string   "cod"
    t.string   "name"
    t.string   "user_menu_alias"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "ct_module_managers", :force => true do |t|
    t.integer  "ct_module_id"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "ct_module_managers", ["ct_module_id"], :name => "index_ct_module_managers_on_ct_module_id"
  add_index "ct_module_managers", ["user_id"], :name => "index_ct_module_managers_on_user_id"

  create_table "ct_modules", :force => true do |t|
    t.string   "name"
    t.boolean  "active"
    t.string   "user_menu_alias"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "cod"
    t.boolean  "has_managers"
    t.string   "alias"
    t.boolean  "create_delete_users", :default => false
  end

  create_table "data_syncs", :force => true do |t|
    t.string   "description"
    t.datetime "executed_at"
    t.boolean  "completed"
    t.text     "sync_errors"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "dnc_area_corrections", :force => true do |t|
    t.text     "correccion"
    t.integer  "dnc_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "revisado",   :default => false
  end

  add_index "dnc_area_corrections", ["dnc_id"], :name => "index_dnc_area_corrections_on_dnc_id"

  create_table "dnc_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dnc_characteristics", :force => true do |t|
    t.integer  "characteristic_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "dnc_characteristics", ["characteristic_id"], :name => "index_dnc_characteristics_on_characteristic_id"

  create_table "dnc_courses", :force => true do |t|
    t.string   "name"
    t.integer  "dnc_program_id"
    t.integer  "dnc_category_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "duration"
  end

  add_index "dnc_courses", ["dnc_category_id"], :name => "index_dnc_courses_on_dnc_category_id"
  add_index "dnc_courses", ["dnc_program_id"], :name => "index_dnc_courses_on_dnc_program_id"

  create_table "dnc_planning_process_goal_ps", :force => true do |t|
    t.integer  "dncp_id"
    t.integer  "planning_process_goal_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "dnc_planning_process_goal_ps", ["dncp_id"], :name => "index_dnc_planning_process_goal_ps_on_dncp_id"
  add_index "dnc_planning_process_goal_ps", ["planning_process_goal_id"], :name => "index_dnc_planning_process_goal_ps_on_planning_process_goal_id"

  create_table "dnc_planning_process_goals", :force => true do |t|
    t.integer  "dnc_id"
    t.integer  "planning_process_goal_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "dnc_planning_process_goals", ["dnc_id"], :name => "index_dnc_planning_process_goals_on_dnc_id"
  add_index "dnc_planning_process_goals", ["planning_process_goal_id"], :name => "index_dnc_planning_process_goals_on_planning_process_goal_id"

  create_table "dnc_process_periods", :force => true do |t|
    t.string   "name"
    t.integer  "dnc_process_id"
    t.date     "from_date"
    t.date     "to_date"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "dnc_process_periods", ["dnc_process_id"], :name => "index_dnc_process_periods_on_dnc_process_id"

  create_table "dnc_processes", :force => true do |t|
    t.string   "name"
    t.string   "period"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dnc_programs", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dnc_providers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dnc_requirements", :force => true do |t|
    t.integer  "user_id"
    t.integer  "dnc_process_id"
    t.integer  "dnc_course_id"
    t.integer  "dnc_process_period_id"
    t.integer  "dnc_provider_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "duration"
  end

  add_index "dnc_requirements", ["dnc_course_id"], :name => "index_dnc_requirements_on_dnc_course_id"
  add_index "dnc_requirements", ["dnc_process_id"], :name => "index_dnc_requirements_on_dnc_process_id"
  add_index "dnc_requirements", ["dnc_process_period_id"], :name => "index_dnc_requirements_on_dnc_process_period_id"
  add_index "dnc_requirements", ["dnc_provider_id"], :name => "index_dnc_requirements_on_dnc_provider_id"
  add_index "dnc_requirements", ["user_id"], :name => "index_dnc_requirements_on_user_id"

  create_table "dnc_student_ps", :force => true do |t|
    t.integer  "dncp_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "dnc_student_ps", ["dncp_id"], :name => "index_dnc_student_ps_on_dncp_id"
  add_index "dnc_student_ps", ["user_id"], :name => "index_dnc_student_ps_on_user_id"

  create_table "dnc_students", :force => true do |t|
    t.integer  "dnc_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "dnc_students", ["dnc_id"], :name => "index_dnc_students_on_dnc_id"
  add_index "dnc_students", ["user_id"], :name => "index_dnc_students_on_user_id"

  create_table "dnc_training_impact_ps", :force => true do |t|
    t.integer  "dncp_id"
    t.integer  "training_impact_category_id"
    t.integer  "training_impact_value_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "dnc_training_impact_ps", ["dncp_id"], :name => "index_dnc_training_impact_ps_on_dncp_id"
  add_index "dnc_training_impact_ps", ["training_impact_category_id"], :name => "index_dnc_training_impact_ps_on_training_impact_category_id"
  add_index "dnc_training_impact_ps", ["training_impact_value_id"], :name => "index_dnc_training_impact_ps_on_training_impact_value_id"

  create_table "dnc_training_impacts", :force => true do |t|
    t.integer  "dnc_id"
    t.integer  "training_impact_category_id"
    t.integer  "training_impact_value_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "dnc_training_impacts", ["dnc_id"], :name => "index_dnc_training_impacts_on_dnc_id"
  add_index "dnc_training_impacts", ["training_impact_category_id"], :name => "index_dnc_training_impacts_on_training_impact_category_id"
  add_index "dnc_training_impacts", ["training_impact_value_id"], :name => "index_dnc_training_impacts_on_training_impact_value_id"

  create_table "dncps", :force => true do |t|
    t.integer  "planning_process_id"
    t.integer  "planning_process_company_unit_id"
    t.integer  "company_unit_area_id"
    t.text     "oportunidad_mejora"
    t.string   "tema_capacitacion"
    t.text     "objetivos_curso"
    t.float    "costo_directo"
    t.float    "costo_indirecto"
    t.integer  "training_mode_id"
    t.integer  "training_type_id"
    t.string   "meses"
    t.integer  "tiempo_capacitacion"
    t.integer  "training_provider_id"
    t.float    "impacto_total"
    t.integer  "dnc_id"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.integer  "nota_minima"
    t.boolean  "eval_presencial",                  :default => true
    t.boolean  "poll_presencial",                  :default => true
    t.boolean  "asistencia_presencial",            :default => true
    t.integer  "course_id"
    t.text     "observaciones"
    t.boolean  "presup_ampliado"
    t.integer  "number_students"
    t.boolean  "eval_eficacia"
    t.integer  "training_program_id"
  end

  add_index "dncps", ["company_unit_area_id"], :name => "index_dncps_on_company_unit_area_id"
  add_index "dncps", ["dnc_id"], :name => "index_dncps_on_dnc_id"
  add_index "dncps", ["planning_process_company_unit_id"], :name => "index_dncps_on_planning_process_company_unit_id"
  add_index "dncps", ["planning_process_id"], :name => "index_dncps_on_planning_process_id"
  add_index "dncps", ["training_mode_id"], :name => "index_dncps_on_training_mode_id"
  add_index "dncps", ["training_program_id"], :name => "index_dncps_on_training_program_id"
  add_index "dncps", ["training_provider_id"], :name => "index_dncps_on_training_provider_id"
  add_index "dncps", ["training_type_id"], :name => "index_dncps_on_training_type_id"

  create_table "dncs", :force => true do |t|
    t.integer  "planning_process_id"
    t.integer  "planning_process_company_unit_id"
    t.integer  "company_unit_area_id"
    t.text     "oportunidad_mejora"
    t.string   "tema_capacitacion"
    t.text     "objetivos_curso"
    t.float    "costo_directo"
    t.float    "costo_indirecto"
    t.integer  "training_mode_id"
    t.integer  "training_type_id"
    t.string   "meses"
    t.integer  "tiempo_capacitacion"
    t.integer  "training_provider_id"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.float    "impacto_total"
    t.boolean  "enviado_aprobar_area",             :default => false
    t.boolean  "requiere_modificaciones_area",     :default => false
    t.boolean  "aprobado_area",                    :default => false
    t.boolean  "aprobado_unidad",                  :default => false
    t.text     "observaciones"
    t.integer  "number_students",                  :default => 0
    t.boolean  "eval_eficacia"
    t.integer  "training_program_id"
  end

  add_index "dncs", ["company_unit_area_id"], :name => "index_dncs_on_company_unit_area_id"
  add_index "dncs", ["planning_process_company_unit_id"], :name => "index_dncs_on_planning_process_company_unit_id"
  add_index "dncs", ["planning_process_id"], :name => "index_dncs_on_planning_process_id"
  add_index "dncs", ["training_mode_id"], :name => "index_dncs_on_training_mode_id"
  add_index "dncs", ["training_program_id"], :name => "index_dncs_on_training_program_id"
  add_index "dncs", ["training_provider_id"], :name => "index_dncs_on_training_provider_id"
  add_index "dncs", ["training_type_id"], :name => "index_dncs_on_training_type_id"

  create_table "elec_category_characteristics", :force => true do |t|
    t.integer  "elec_process_category_id"
    t.integer  "characteristic_id"
    t.string   "match_value"
    t.boolean  "filter_voter"
    t.boolean  "filter_candidate"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "elec_category_characteristics", ["characteristic_id"], :name => "index_elec_category_characteristics_on_characteristic_id"
  add_index "elec_category_characteristics", ["elec_process_category_id"], :name => "index_elec_category_characteristics_on_elec_process_category_id"

  create_table "elec_characteristics", :force => true do |t|
    t.integer  "elec_process_id"
    t.integer  "characteristic_id"
    t.boolean  "used_to_group_by"
    t.boolean  "can_be_used_in_reports"
    t.integer  "can_be_used_in_reports_position"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "elec_characteristics", ["characteristic_id"], :name => "index_elec_characteristics_on_characteristic_id"
  add_index "elec_characteristics", ["elec_process_id"], :name => "index_elec_characteristics_on_elec_process_id"

  create_table "elec_events", :force => true do |t|
    t.integer  "elec_process_id"
    t.integer  "elec_recurrence_id"
    t.boolean  "manual",             :default => true
    t.boolean  "active",             :default => true
    t.datetime "from_date"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.text     "name"
  end

  add_index "elec_events", ["elec_process_id"], :name => "index_elec_events_on_elec_process_id"
  add_index "elec_events", ["elec_recurrence_id"], :name => "index_elec_events_on_elec_recurrence_id"

  create_table "elec_historic_characteristics", :force => true do |t|
    t.integer  "elec_event_id"
    t.integer  "characteristic_id"
    t.integer  "user_id"
    t.string   "value"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "elec_historic_characteristics", ["characteristic_id"], :name => "index_elec_historic_characteristics_on_characteristic_id"
  add_index "elec_historic_characteristics", ["elec_event_id"], :name => "index_elec_historic_characteristics_on_elec_event_id"
  add_index "elec_historic_characteristics", ["user_id"], :name => "index_elec_historic_characteristics_on_user_id"

  create_table "elec_process_categories", :force => true do |t|
    t.integer  "elec_process_id"
    t.string   "name"
    t.text     "description"
    t.integer  "max_votes",       :default => 1
    t.boolean  "self_vote",       :default => true
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "position"
    t.boolean  "ask_for_comment", :default => false
  end

  add_index "elec_process_categories", ["elec_process_id"], :name => "index_elec_process_categories_on_elec_process_id"

  create_table "elec_process_rounds", :force => true do |t|
    t.integer  "elec_process_id"
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.boolean  "mandatory",                         :default => false
    t.integer  "lifespan"
    t.integer  "qty_required",                      :default => 1
    t.boolean  "show_live_results",                 :default => false
    t.boolean  "show_final_results",                :default => false
    t.integer  "threshold_pctg_universe",           :default => 0
    t.integer  "threshold_pctg_voters",             :default => 0
    t.integer  "threshold_qty_voters",              :default => 1
    t.integer  "qty_min_podium",                    :default => 1
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.integer  "show_live_results_extra_time",      :default => 0
    t.integer  "show_final_results_extra_time",     :default => 0
    t.boolean  "show_list_categories_in_dashboard", :default => false
    t.boolean  "show_live_my_voters",               :default => false
    t.integer  "show_live_my_voters_extra_time",    :default => 0
    t.boolean  "show_final_my_voters",              :default => false
    t.integer  "show_final_my_voters_extra_time",   :default => 0
    t.integer  "show_my_voters_since_qty_votes",    :default => 0
    t.boolean  "show_my_voters_detail",             :default => false
    t.boolean  "list_like_minificha",               :default => false
  end

  add_index "elec_process_rounds", ["elec_process_id"], :name => "index_elec_process_rounds_on_elec_process_id"

  create_table "elec_processes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active",      :default => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "elec_round_winners", :force => true do |t|
    t.integer  "elec_round_id"
    t.integer  "elec_process_category_id"
    t.integer  "user_id"
    t.integer  "registered_by_user_id"
    t.datetime "registered_at"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "elec_round_winners", ["elec_process_category_id"], :name => "index_elec_round_winners_on_elec_process_category_id"
  add_index "elec_round_winners", ["elec_round_id"], :name => "index_elec_round_winners_on_elec_round_id"
  add_index "elec_round_winners", ["registered_by_user_id"], :name => "index_elec_round_winners_on_registered_by_user_id"
  add_index "elec_round_winners", ["user_id"], :name => "index_elec_round_winners_on_user_id"

  create_table "elec_rounds", :force => true do |t|
    t.integer  "elec_event_id"
    t.integer  "elec_process_round_id"
    t.datetime "from_date"
    t.datetime "to_date"
    t.boolean  "done",                  :default => false
    t.boolean  "final",                 :default => false
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "done_by_user_id"
    t.datetime "done_at"
  end

  add_index "elec_rounds", ["done_by_user_id"], :name => "index_elec_rounds_on_done_by_user_id"
  add_index "elec_rounds", ["elec_event_id"], :name => "index_elec_rounds_on_elec_event_id"
  add_index "elec_rounds", ["elec_process_round_id"], :name => "index_elec_rounds_on_elec_process_round_id"

  create_table "elec_votes", :force => true do |t|
    t.integer  "elec_round_id"
    t.integer  "elec_process_category_id"
    t.integer  "user_id"
    t.integer  "candidate_user_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.text     "comment"
    t.datetime "registered_at"
  end

  add_index "elec_votes", ["candidate_user_id"], :name => "index_elec_votes_on_candidate_user_id"
  add_index "elec_votes", ["elec_process_category_id"], :name => "index_elec_votes_on_elec_process_category_id"
  add_index "elec_votes", ["elec_round_id"], :name => "index_elec_votes_on_elec_round_id"
  add_index "elec_votes", ["user_id"], :name => "index_elec_votes_on_user_id"

  create_table "enrollments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "program_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "level_id"
    t.integer  "program_instance_id"
    t.boolean  "aprobado"
    t.boolean  "active",              :default => true
  end

  add_index "enrollments", ["active"], :name => "index_enrollments_on_active"
  add_index "enrollments", ["level_id"], :name => "index_enrollments_on_level_id"
  add_index "enrollments", ["program_id"], :name => "index_enrollments_on_program_id"
  add_index "enrollments", ["program_instance_id"], :name => "index_enrollments_on_program_instance_id"
  add_index "enrollments", ["user_id"], :name => "index_enrollments_on_user_id"

  create_table "evaluation_topics", :force => true do |t|
    t.integer  "evaluation_id"
    t.integer  "master_evaluation_topic_id"
    t.integer  "position"
    t.integer  "number_of_questions"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "evaluation_topics", ["evaluation_id"], :name => "index_evaluation_topics_on_evaluation_id"
  add_index "evaluation_topics", ["master_evaluation_topic_id"], :name => "index_evaluation_topics_on_master_evaluation_topic_id"

  create_table "evaluations", :force => true do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.integer  "program_id"
    t.integer  "level_id"
    t.integer  "course_id"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.boolean  "libre",                        :default => true
    t.integer  "numero"
    t.boolean  "activa",                       :default => true
    t.integer  "antes_de_unit_id"
    t.integer  "despues_de_unit_id"
    t.integer  "antes_de_evaluation_id"
    t.integer  "despues_de_evaluation_id"
    t.integer  "antes_de_poll_id"
    t.integer  "despues_de_poll_id"
    t.integer  "orden",                        :default => 1
    t.integer  "master_evaluation_id"
    t.integer  "numero_preguntas"
    t.boolean  "visualizacion_lote"
    t.boolean  "visualizar_resultado_detalle"
    t.boolean  "visualizar_resultado_resumen"
    t.integer  "tiempo"
    t.boolean  "aplicar_tiempo"
    t.integer  "nota_base",                    :default => 0
    t.boolean  "can_be_paused",                :default => false
    t.integer  "weight",                       :default => 1
    t.boolean  "virtual",                      :default => true
  end

  add_index "evaluations", ["course_id"], :name => "index_evaluations_on_course_id"
  add_index "evaluations", ["level_id"], :name => "index_evaluations_on_level_id"
  add_index "evaluations", ["orden"], :name => "index_evaluations_on_orden"
  add_index "evaluations", ["program_id"], :name => "index_evaluations_on_program_id"

  create_table "folder_file_comments", :force => true do |t|
    t.integer  "folder_file_id"
    t.integer  "user_id"
    t.datetime "fecha"
    t.text     "comentario"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "folder_file_comments", ["folder_file_id"], :name => "index_folder_file_comments_on_folder_file_id"
  add_index "folder_file_comments", ["user_id"], :name => "index_folder_file_comments_on_user_id"

  create_table "folder_file_values", :force => true do |t|
    t.integer  "folder_file_id"
    t.integer  "user_id"
    t.datetime "fecha"
    t.integer  "valor"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "folder_file_values", ["folder_file_id"], :name => "index_folder_file_values_on_folder_file_id"
  add_index "folder_file_values", ["user_id"], :name => "index_folder_file_values_on_user_id"

  create_table "folder_files", :force => true do |t|
    t.string   "nombre"
    t.string   "crypted_name"
    t.integer  "folder_id"
    t.integer  "user_id"
    t.boolean  "descargable",           :default => true
    t.string   "extension"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.text     "descripcion"
    t.integer  "size"
    t.float    "valor",                 :default => 0.0
    t.boolean  "oculto",                :default => false
    t.integer  "folder_file_father_id"
    t.boolean  "is_last",               :default => true
    t.string   "download_token"
  end

  add_index "folder_files", ["folder_file_father_id"], :name => "index_folder_files_on_folder_file_father_id"
  add_index "folder_files", ["folder_id"], :name => "index_folder_files_on_folder_id"
  add_index "folder_files", ["user_id"], :name => "index_folder_files_on_user_id"

  create_table "folder_members", :force => true do |t|
    t.integer  "folder_id"
    t.integer  "characteristic_id"
    t.string   "valor"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "folder_members", ["characteristic_id"], :name => "index_folder_members_on_characteristic_id"
  add_index "folder_members", ["folder_id"], :name => "index_folder_members_on_folder_id"

  create_table "folder_owners", :force => true do |t|
    t.integer  "folder_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "folder_owners", ["folder_id"], :name => "index_folder_owners_on_folder_id"
  add_index "folder_owners", ["user_id"], :name => "index_folder_owners_on_user_id"

  create_table "folder_readers", :force => true do |t|
    t.integer  "folder_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "folder_readers", ["folder_id"], :name => "index_folder_readers_on_folder_id"
  add_index "folder_readers", ["user_id"], :name => "index_folder_readers_on_user_id"

  create_table "folders", :force => true do |t|
    t.string   "nombre"
    t.integer  "creador_id"
    t.integer  "padre_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "public",     :default => false
  end

  add_index "folders", ["creador_id"], :name => "index_folders_on_creador_id"
  add_index "folders", ["padre_id"], :name => "index_folders_on_padre_id"

  create_table "he_ct_module_privilages", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "recorder",               :default => false
    t.boolean  "validator",              :default => false
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.boolean  "active",                 :default => true
  end

  add_index "he_ct_module_privilages", ["deactivated_by_user_id"], :name => "index_he_ct_module_privilages_on_deactivated_by_user_id"
  add_index "he_ct_module_privilages", ["registered_by_user_id"], :name => "index_he_ct_module_privilages_on_registered_by_user_id"
  add_index "he_ct_module_privilages", ["user_id"], :name => "index_he_ct_module_privilages_on_user_id"

  create_table "holiday_characteristics", :force => true do |t|
    t.integer  "characteristic_id"
    t.integer  "holiday_id"
    t.integer  "condition_type"
    t.string   "match_value_string"
    t.text     "match_value_text"
    t.date     "match_value_date"
    t.integer  "match_value_int"
    t.float    "match_value_float"
    t.integer  "match_value_char_id"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "prev_hol_char_id"
  end

  add_index "holiday_characteristics", ["characteristic_id"], :name => "index_holiday_characteristics_on_characteristic_id"
  add_index "holiday_characteristics", ["holiday_id"], :name => "index_holiday_characteristics_on_holiday_id"
  add_index "holiday_characteristics", ["prev_hol_char_id"], :name => "index_holiday_characteristics_on_prev_hol_char_id"

  create_table "holiday_users", :force => true do |t|
    t.integer  "holiday_id"
    t.integer  "user_id"
    t.boolean  "manually_assoc",         :default => false
    t.boolean  "active",                 :default => true
    t.boolean  "deactivated_manually",   :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "prev_hol_user_id"
  end

  add_index "holiday_users", ["holiday_id"], :name => "index_holiday_users_on_holiday_id"
  add_index "holiday_users", ["prev_hol_user_id"], :name => "index_holiday_users_on_prev_hol_user_id"
  add_index "holiday_users", ["user_id"], :name => "index_holiday_users_on_user_id"

  create_table "holidays", :force => true do |t|
    t.date     "hol_date"
    t.integer  "hol_type"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.text     "description"
    t.string   "name",                   :default => "Sin nombre"
    t.integer  "prev_holiday_id"
  end

  add_index "holidays", ["deactivated_by_user_id"], :name => "holidays_deact_user_id"
  add_index "holidays", ["prev_holiday_id"], :name => "index_holidays_on_prev_holiday_id"
  add_index "holidays", ["registered_by_user_id"], :name => "holidays_regist_user_id"

  create_table "hr_evaluation_type_elements", :force => true do |t|
    t.string   "nombre"
    t.integer  "nivel"
    t.float    "valor_minimo"
    t.float    "valor_maximo"
    t.boolean  "formula_promedio",            :default => true
    t.integer  "peso_minimo"
    t.integer  "peso_maximo"
    t.integer  "suma_total_pesos"
    t.boolean  "plan_accion",                 :default => false
    t.boolean  "porcentaje_logro",            :default => false
    t.boolean  "indicador_logro",             :default => false
    t.integer  "hr_evaluation_type_id"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.integer  "cantidad_maxima"
    t.boolean  "carga_manual"
    t.boolean  "evaluacion_colaborativa"
    t.boolean  "tiene_clasificacion"
    t.string   "nombre_clasificacion"
    t.boolean  "aplica_evaluacion",           :default => true
    t.boolean  "indicador_logro_descripcion", :default => true
  end

  add_index "hr_evaluation_type_elements", ["hr_evaluation_type_id"], :name => "index_hr_evaluation_type_elements_on_hr_evaluation_type_id"

  create_table "hr_evaluation_types", :force => true do |t|
    t.string   "nombre"
    t.boolean  "activo",        :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.text     "descripcion"
    t.text     "explanation"
    t.boolean  "from_jobs"
    t.boolean  "from_jobs_com"
  end

  create_table "hr_process_assessment_d_cals", :force => true do |t|
    t.integer  "hr_process_assessment_d_id"
    t.integer  "hr_process_calibration_session_id"
    t.integer  "hr_process_calibration_session_m_id"
    t.float    "resultado_porcentaje"
    t.datetime "fecha"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.text     "comentario"
    t.integer  "hr_process_dimension_g_id"
  end

  add_index "hr_process_assessment_d_cals", ["hr_process_assessment_d_id"], :name => "hr_process_assessment_d_cals_pad_id"
  add_index "hr_process_assessment_d_cals", ["hr_process_calibration_session_id"], :name => "hr_process_assessment_d_cals_pcs_id"
  add_index "hr_process_assessment_d_cals", ["hr_process_calibration_session_m_id"], :name => "hr_process_assessment_d_cals_pcsm_id"

  create_table "hr_process_assessment_ds", :force => true do |t|
    t.integer  "hr_process_dimension_id"
    t.integer  "hr_process_user_id"
    t.float    "resultado_porcentaje"
    t.datetime "fecha"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "finalizada",                :default => false
    t.integer  "hr_process_dimension_g_id"
  end

  add_index "hr_process_assessment_ds", ["hr_process_dimension_id"], :name => "index_hr_process_assessment_ds_on_hr_process_dimension_id"
  add_index "hr_process_assessment_ds", ["hr_process_user_id"], :name => "index_hr_process_assessment_ds_on_hr_process_user_id"

  create_table "hr_process_assessment_efs", :force => true do |t|
    t.integer  "hr_process_evaluation_id"
    t.integer  "hr_process_user_id"
    t.float    "resultado_puntos"
    t.float    "resultado_porcentaje"
    t.datetime "fecha"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.boolean  "registered_by_manager",    :default => false
  end

  add_index "hr_process_assessment_efs", ["hr_process_evaluation_id"], :name => "index_hr_process_assessment_efs_on_hr_process_evaluation_id"
  add_index "hr_process_assessment_efs", ["hr_process_user_id"], :name => "index_hr_process_assessment_efs_on_hr_process_user_id"

  create_table "hr_process_assessment_es", :force => true do |t|
    t.integer  "hr_process_evaluation_id"
    t.integer  "hr_process_user_id"
    t.float    "resultado_puntos"
    t.float    "resultado_porcentaje"
    t.datetime "fecha"
    t.integer  "hr_process_user_eval_id"
    t.string   "tipo_rel"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "hr_process_assessment_es", ["hr_process_evaluation_id"], :name => "index_hr_process_assessment_es_on_hr_process_evaluation_id"
  add_index "hr_process_assessment_es", ["hr_process_user_id"], :name => "index_hr_process_assessment_es_on_hr_process_user_id"

  create_table "hr_process_assessment_ls", :force => true do |t|
    t.integer  "hr_process_evaluation_q_id"
    t.integer  "hr_process_evaluation_manual_q_id"
    t.integer  "hr_process_user_id"
    t.integer  "hr_process_user_eval_id"
    t.float    "resultado_puntos"
    t.float    "resultado_porcentaje"
    t.float    "logro_indicador"
    t.datetime "fecha"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "hr_process_assessment_ls", ["hr_process_evaluation_manual_q_id"], :name => "hr_process_assessment_ls_emq_id"
  add_index "hr_process_assessment_ls", ["hr_process_evaluation_q_id"], :name => "hr_process_assessment_ls_eq_id"
  add_index "hr_process_assessment_ls", ["hr_process_user_id"], :name => "hr_process_assessment_ls_pu_id"

  create_table "hr_process_assessment_qs", :force => true do |t|
    t.integer  "hr_process_assessment_e_id"
    t.integer  "hr_process_evaluation_q_id"
    t.integer  "hr_process_user_id"
    t.float    "resultado_puntos"
    t.float    "resultado_porcentaje"
    t.integer  "hr_process_evaluation_a_id"
    t.datetime "fecha"
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.text     "comentario"
    t.integer  "hr_process_evaluation_manual_q_id"
    t.float    "logro_indicador"
    t.string   "logro_indicador_d",                 :default => ""
  end

  add_index "hr_process_assessment_qs", ["hr_process_assessment_e_id"], :name => "index_hr_process_assessment_qs_on_hr_process_assessment_e_id"
  add_index "hr_process_assessment_qs", ["hr_process_evaluation_a_id"], :name => "index_hr_process_assessment_qs_on_hr_process_evaluation_a_id"
  add_index "hr_process_assessment_qs", ["hr_process_evaluation_manual_q_id"], :name => "hr_p_a_q_hr_process_evaluation_manual_q_id"
  add_index "hr_process_assessment_qs", ["hr_process_evaluation_q_id"], :name => "index_hr_process_assessment_qs_on_hr_process_evaluation_q_id"
  add_index "hr_process_assessment_qs", ["hr_process_user_id"], :name => "index_hr_process_assessment_qs_on_hr_process_user_id"

  create_table "hr_process_assessment_qua_cals", :force => true do |t|
    t.integer  "hr_process_calibration_session_id"
    t.integer  "hr_process_calibration_session_m_id"
    t.integer  "hr_process_quadrant_id"
    t.datetime "fecha"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.text     "comentario"
  end

  add_index "hr_process_assessment_qua_cals", ["hr_process_calibration_session_id"], :name => "hr_process_assessment_qua_cals_pcs_id"
  add_index "hr_process_assessment_qua_cals", ["hr_process_calibration_session_m_id"], :name => "hr_process_assessment_qua_cals_pcsm_id"
  add_index "hr_process_assessment_qua_cals", ["hr_process_quadrant_id"], :name => "hr_process_assessment_qua_cals_pq_id"

  create_table "hr_process_assessment_quas", :force => true do |t|
    t.integer  "hr_process_user_id"
    t.integer  "hr_process_quadrant_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.datetime "fecha"
  end

  add_index "hr_process_assessment_quas", ["hr_process_quadrant_id"], :name => "index_hr_process_assessment_quas_on_hr_process_quadrant_id"
  add_index "hr_process_assessment_quas", ["hr_process_user_id"], :name => "index_hr_process_assessment_quas_on_hr_process_user_id"

  create_table "hr_process_calibration_session_cs", :force => true do |t|
    t.integer  "hr_process_calibration_session_id"
    t.integer  "user_id"
    t.boolean  "manager"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "hr_process_calibration_session_cs", ["hr_process_calibration_session_id"], :name => "hr_process_calibration_session_cs_cs_id"
  add_index "hr_process_calibration_session_cs", ["user_id"], :name => "hr_process_calibration_session_cs_u_id"

  create_table "hr_process_calibration_session_ms", :force => true do |t|
    t.integer  "hr_process_calibration_session_id"
    t.integer  "hr_process_user_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "hr_process_calibration_session_ms", ["hr_process_calibration_session_id"], :name => "hr_process_calibration_session_ms_cs_id"
  add_index "hr_process_calibration_session_ms", ["hr_process_user_id"], :name => "hr_process_calibration_session_ms_pu_id"

  create_table "hr_process_calibration_sessions", :force => true do |t|
    t.integer  "hr_process_id"
    t.date     "inicio"
    t.date     "fin"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "abierta",       :default => true
    t.text     "descripcion"
  end

  add_index "hr_process_calibration_sessions", ["hr_process_id"], :name => "index_hr_process_calibration_sessions_on_hr_process_id"

  create_table "hr_process_dimension_es", :force => true do |t|
    t.integer  "orden"
    t.integer  "porcentaje"
    t.integer  "hr_evaluation_type_id"
    t.integer  "hr_process_dimension_id"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "eval_jefe",               :default => false
    t.boolean  "eval_auto",               :default => false
    t.boolean  "eval_par",                :default => false
    t.boolean  "eval_sub",                :default => false
    t.integer  "eval_jefe_peso",          :default => 0
    t.integer  "eval_auto_peso",          :default => 0
    t.integer  "eval_par_peso",           :default => 0
    t.integer  "eval_sub_peso",           :default => 0
    t.boolean  "eval_cli",                :default => false
    t.boolean  "eval_prov",               :default => false
    t.integer  "eval_cli_peso",           :default => 0
    t.integer  "eval_prov_peso",          :default => 0
    t.boolean  "registro_por_gestor",     :default => false
  end

  add_index "hr_process_dimension_es", ["hr_evaluation_type_id"], :name => "index_hr_process_dimension_es_on_hr_evaluation_type_id"
  add_index "hr_process_dimension_es", ["hr_process_dimension_id"], :name => "index_hr_process_dimension_es_on_hr_process_dimension_id"

  create_table "hr_process_dimension_gs", :force => true do |t|
    t.integer  "orden"
    t.integer  "valor_minimo"
    t.integer  "valor_maximo"
    t.integer  "hr_process_dimension_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "nombre"
    t.string   "color_text"
    t.text     "descripcion"
    t.string   "valor_minimo_signo",      :default => ">="
    t.string   "valor_maximo_signo",      :default => "<="
  end

  add_index "hr_process_dimension_gs", ["hr_process_dimension_id"], :name => "index_hr_process_dimension_gs_on_hr_process_dimension_id"

  create_table "hr_process_dimensions", :force => true do |t|
    t.string   "nombre"
    t.integer  "hr_process_template_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "orden"
  end

  add_index "hr_process_dimensions", ["hr_process_template_id"], :name => "index_hr_process_dimensions_on_hr_process_template_id"

  create_table "hr_process_evaluation_as", :force => true do |t|
    t.integer  "valor"
    t.text     "texto"
    t.integer  "hr_process_evaluation_q_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "hr_process_evaluation_as", ["hr_process_evaluation_q_id"], :name => "index_hr_process_evaluation_as_on_hr_process_evaluation_q_id"

  create_table "hr_process_evaluation_manual_qcs", :force => true do |t|
    t.text     "comentario"
    t.datetime "fecha"
    t.integer  "hr_process_evaluation_manual_q_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "hr_process_user_id"
  end

  add_index "hr_process_evaluation_manual_qcs", ["hr_process_evaluation_manual_q_id"], :name => "hr_p_e_mq"
  add_index "hr_process_evaluation_manual_qcs", ["hr_process_user_id"], :name => "hr_pro_eval_manual_qcs_creator_id"

  create_table "hr_process_evaluation_manual_qs", :force => true do |t|
    t.string   "texto"
    t.float    "peso"
    t.integer  "hr_process_evaluation_id"
    t.integer  "hr_evaluation_type_element_id"
    t.integer  "hr_process_evaluation_manual_q_id"
    t.integer  "hr_process_user_id"
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
    t.integer  "hr_process_evaluation_q_classification_id"
    t.string   "descripcion_indicador"
    t.float    "meta_indicador"
    t.string   "unidad_medida_indicador"
    t.boolean  "confirmado"
    t.integer  "tipo_indicador",                            :default => 1
    t.string   "meta_indicador_d",                          :default => ""
  end

  add_index "hr_process_evaluation_manual_qs", ["hr_evaluation_type_element_id"], :name => "hr_p_e_m_q_hr_evaluation_type_element_id"
  add_index "hr_process_evaluation_manual_qs", ["hr_process_evaluation_id"], :name => "hr_p_e_m_q_hr_process_evaluation_id"
  add_index "hr_process_evaluation_manual_qs", ["hr_process_evaluation_q_classification_id"], :name => "hr_p_e_m_q_class_hr_process_evaluation_q_classification_id"
  add_index "hr_process_evaluation_manual_qs", ["hr_process_user_id"], :name => "hr_p_e_m_q_hr_process_user_id"
  add_index "hr_process_evaluation_manual_qs", ["tipo_indicador"], :name => "index_hr_process_evaluation_manual_qs_on_tipo_indicador"

  create_table "hr_process_evaluation_q_classifications", :force => true do |t|
    t.integer  "orden"
    t.text     "nombre"
    t.integer  "hr_process_evaluation_id"
    t.integer  "hr_evaluation_type_element_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "color"
  end

  add_index "hr_process_evaluation_q_classifications", ["hr_evaluation_type_element_id"], :name => "hr_p_e_q_class_hr_evaluation_type_element_id"
  add_index "hr_process_evaluation_q_classifications", ["hr_process_evaluation_id"], :name => "hr_p_e_q_class_hr_process_evaluation_id"

  create_table "hr_process_evaluation_q_schedules", :force => true do |t|
    t.integer  "hr_process_evaluation_id"
    t.integer  "hr_evaluation_type_element_id"
    t.date     "inicio"
    t.date     "fin"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "hr_process_evaluation_q_schedules", ["hr_evaluation_type_element_id"], :name => "hr_process_evaluation_q_schedules_ete_id"
  add_index "hr_process_evaluation_q_schedules", ["hr_process_evaluation_id"], :name => "hr_process_evaluation_q_schedules_pe_id"

  create_table "hr_process_evaluation_qs", :force => true do |t|
    t.text     "texto"
    t.integer  "peso"
    t.integer  "hr_process_evaluation_id"
    t.integer  "hr_process_level_id"
    t.integer  "hr_evaluation_type_element_id"
    t.integer  "hr_process_evaluation_q_id"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.boolean  "comentario",                    :default => false
    t.boolean  "discrete_display_in_reports",   :default => false
  end

  add_index "hr_process_evaluation_qs", ["hr_evaluation_type_element_id"], :name => "index_hr_process_evaluation_qs_on_hr_evaluation_type_element_id"
  add_index "hr_process_evaluation_qs", ["hr_process_evaluation_id"], :name => "index_hr_process_evaluation_qs_on_hr_process_evaluation_id"
  add_index "hr_process_evaluation_qs", ["hr_process_evaluation_q_id"], :name => "index_hr_process_evaluation_qs_on_hr_process_evaluation_q_id"
  add_index "hr_process_evaluation_qs", ["hr_process_level_id"], :name => "index_hr_process_evaluation_qs_on_hr_process_level_id"

  create_table "hr_process_evaluations", :force => true do |t|
    t.integer  "hr_process_id"
    t.integer  "hr_process_dimension_e_id"
    t.datetime "fecha_inicio_eval_jefe"
    t.datetime "fecha_fin_eval_jefe"
    t.datetime "fecha_inicio_eval_auto"
    t.datetime "fecha_fin_eval_auto"
    t.datetime "fecha_inicio_eval_sub"
    t.datetime "fecha_fin_eval_sub"
    t.datetime "fecha_inicio_eval_par"
    t.datetime "fecha_fin_eval_par"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "vigente",                    :default => true
    t.integer  "orden"
    t.integer  "cien_x_cien_puntos"
    t.integer  "visualizacion_alternativas", :default => 1
    t.datetime "fecha_inicio_eval_cli"
    t.datetime "fecha_fin_eval_cli"
    t.datetime "fecha_inicio_eval_prov"
    t.datetime "fecha_fin_eval_prov"
    t.boolean  "show_preg_nivel_1",          :default => true
    t.integer  "tope_no_aplica"
  end

  add_index "hr_process_evaluations", ["hr_process_dimension_e_id"], :name => "index_hr_process_evaluations_on_hr_process_dimension_e_id"
  add_index "hr_process_evaluations", ["hr_process_id"], :name => "index_hr_process_evaluations_on_hr_process_id"

  create_table "hr_process_levels", :force => true do |t|
    t.string   "nombre"
    t.integer  "hr_process_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "hr_process_levels", ["hr_process_id"], :name => "index_hr_process_levels_on_hr_process_id"

  create_table "hr_process_managers", :force => true do |t|
    t.integer  "hr_process_id"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "hr_process_managers", ["hr_process_id"], :name => "index_hr_process_managers_on_hr_process_id"
  add_index "hr_process_managers", ["user_id"], :name => "index_hr_process_managers_on_user_id"

  create_table "hr_process_quadrants", :force => true do |t|
    t.string   "nombre"
    t.integer  "hr_process_template_id"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "color"
    t.string   "color_text"
    t.string   "color_fondo_suave"
    t.string   "descripcion"
    t.text     "descripcion_detalle"
    t.integer  "hr_process_dimension_g_x_id"
    t.integer  "hr_process_dimension_g_y_id"
    t.integer  "orden_grafico",               :default => 1
  end

  add_index "hr_process_quadrants", ["hr_process_template_id"], :name => "index_hr_process_quadrants_on_hr_process_template_id"

  create_table "hr_process_templates", :force => true do |t|
    t.string   "nombre"
    t.string   "descripcion"
    t.boolean  "activo",      :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "hr_process_user_comments", :force => true do |t|
    t.integer  "hr_process_user_id"
    t.text     "comentario"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "hr_process_user_eval_id"
    t.datetime "fecha"
  end

  add_index "hr_process_user_comments", ["hr_process_user_eval_id"], :name => "index_hr_process_user_comments_on_hr_process_user_eval_id"
  add_index "hr_process_user_comments", ["hr_process_user_id"], :name => "index_hr_process_user_comments_on_hr_process_user_id"

  create_table "hr_process_user_rels", :force => true do |t|
    t.integer  "hr_process_user1_id"
    t.string   "tipo"
    t.integer  "hr_process_user2_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "hr_process_id"
    t.boolean  "finalizada",          :default => false
    t.boolean  "no_aplica",           :default => false
  end

  add_index "hr_process_user_rels", ["hr_process_id"], :name => "index_hr_process_user_rels_on_hr_process_id"

  create_table "hr_process_user_tfs", :force => true do |t|
    t.integer  "hr_process_user_id"
    t.text     "what"
    t.text     "how"
    t.text     "when"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "hr_process_user_tfs", ["hr_process_user_id"], :name => "index_hr_process_user_tfs_on_hr_process_user_id"

  create_table "hr_process_users", :force => true do |t|
    t.integer  "hr_process_id"
    t.integer  "user_id"
    t.integer  "hr_process_level_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "finalizado",          :default => false
    t.boolean  "feedback_confirmado"
    t.text     "texto_feedback"
    t.boolean  "finalizada_auto",     :default => false
    t.boolean  "feedback_recibido"
    t.datetime "feedback_date"
    t.text     "tf_1"
    t.text     "tf_2"
    t.text     "tf_4"
  end

  add_index "hr_process_users", ["hr_process_id"], :name => "index_hr_process_users_on_hr_process_id"
  add_index "hr_process_users", ["hr_process_level_id"], :name => "index_hr_process_users_on_hr_process_level_id"
  add_index "hr_process_users", ["user_id"], :name => "index_hr_process_users_on_user_id"

  create_table "hr_processes", :force => true do |t|
    t.string   "nombre"
    t.boolean  "abierto",                               :default => true
    t.datetime "fecha_inicio"
    t.datetime "fecha_fin"
    t.integer  "year"
    t.datetime "created_at",                                                         :null => false
    t.datetime "updated_at",                                                         :null => false
    t.integer  "hr_process_template_id"
    t.boolean  "muestra_cajas"
    t.boolean  "muestra_grupos"
    t.boolean  "tiene_paso_feedback"
    t.boolean  "calib_cuadrante"
    t.boolean  "calib_dimension"
    t.boolean  "muestra_cuadrantes_calib"
    t.boolean  "show_quien_evaluo",                     :default => true
    t.boolean  "jefe_consulta_resultados",              :default => false
    t.boolean  "jefe_ve_autoeval",                      :default => false
    t.boolean  "ae_avisa_jefe_email",                   :default => false
    t.string   "alias_jefe",                            :default => "Supervisor"
    t.string   "alias_jefe_inverso",                    :default => "Colaborador"
    t.string   "alias_jefe_plu",                        :default => "Supervisores"
    t.string   "alias_jefe_inverso_plu",                :default => "Colaboradores"
    t.string   "alias_par",                             :default => "Par"
    t.string   "alias_par_inverso",                     :default => "Par"
    t.string   "alias_par_plu",                         :default => "Pares"
    t.string   "alias_par_inverso_plu",                 :default => "Pares"
    t.string   "alias_sub",                             :default => "Colaborador"
    t.string   "alias_sub_inverso",                     :default => "Supervisor"
    t.string   "alias_sub_plu",                         :default => "Colaboradores"
    t.string   "alias_sub_inverso_plu",                 :default => "Supervisores"
    t.string   "alias_cli",                             :default => "Cliente"
    t.string   "alias_cli_inverso",                     :default => "Proveedor"
    t.string   "alias_cli_plu",                         :default => "Clientes"
    t.string   "alias_cli_inverso_plu",                 :default => "Proveedores"
    t.string   "alias_prov",                            :default => "Proveedor"
    t.string   "alias_prov_inverso",                    :default => "Cliente"
    t.string   "alias_prov_plu",                        :default => "Proveedores"
    t.string   "alias_prov_inverso_plu",                :default => "Clientes"
    t.boolean  "jefe_cons_estado_avance",               :default => false
    t.boolean  "jefe_cons_res_final",                   :default => false
    t.string   "positions_final_results_report"
    t.boolean  "boss_check_results_while_assess"
    t.boolean  "boss_check_final_report_on_feedback"
    t.boolean  "step_feedback_confirmation"
    t.boolean  "step_feedback_confirmation_mail"
    t.boolean  "boss_check_final_results_while_assess"
    t.text     "message_boss_while_assess"
    t.boolean  "jefe_consulta_resultados_rec"
    t.boolean  "show_user_profile"
    t.integer  "tiene_paso_feedback_modo",              :default => 1
    t.string   "label_1_t"
    t.text     "label_1_c"
    t.string   "label_2_t"
    t.text     "label_2_c"
    t.string   "label_3_t"
    t.text     "label_3_c"
  end

  add_index "hr_processes", ["hr_process_template_id"], :name => "index_hr_processes_on_hr_process_template_id"

  create_table "inf_doc_folders", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "informative_documents", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.integer  "inf_doc_folder_id"
    t.boolean  "employee_access",   :default => true
    t.boolean  "boss_access",       :default => true
    t.boolean  "superior_access",   :default => true
  end

  add_index "informative_documents", ["inf_doc_folder_id"], :name => "index_informative_documents_on_inf_doc_folder_id"

  create_table "j_characteristic_types", :force => true do |t|
    t.string   "name"
    t.integer  "position"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "j_characteristics", :force => true do |t|
    t.integer  "characteristic_id"
    t.integer  "position"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "j_characteristics", ["characteristic_id"], :name => "index_j_characteristics_on_characteristic_id"

  create_table "j_cost_centers", :force => true do |t|
    t.string   "name"
    t.decimal  "budget",          :precision => 60, :scale => 4
    t.boolean  "active",                                         :default => true
    t.datetime "created_at",                                                       :null => false
    t.datetime "updated_at",                                                       :null => false
    t.string   "cc_id"
    t.integer  "total_positions",                                :default => 1
  end

  create_table "j_functions", :force => true do |t|
    t.text     "description"
    t.boolean  "main_function"
    t.integer  "j_job_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "j_functions", ["j_job_id"], :name => "index_j_functions_on_j_job_id"

  create_table "j_generic_skills", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "j_job_level_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "j_generic_skills", ["j_job_level_id"], :name => "index_j_generic_skills_on_j_job_level_id"

  create_table "j_job_ccs", :force => true do |t|
    t.integer  "j_job_id"
    t.integer  "j_cost_center_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "j_job_ccs", ["j_cost_center_id"], :name => "index_j_job_ccs_on_j_cost_center_id"
  add_index "j_job_ccs", ["j_job_id"], :name => "index_j_job_ccs_on_j_job_id"

  create_table "j_job_characteristics", :force => true do |t|
    t.integer  "j_job_id"
    t.integer  "characteristic_id"
    t.string   "value_string"
    t.integer  "characteristic_value_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "j_job_characteristics", ["characteristic_id"], :name => "index_j_job_characteristics_on_characteristic_id"
  add_index "j_job_characteristics", ["characteristic_value_id"], :name => "index_j_job_characteristics_on_characteristic_value_id"
  add_index "j_job_characteristics", ["j_job_id"], :name => "index_j_job_characteristics_on_j_job_id"

  create_table "j_job_jj_characteristics", :force => true do |t|
    t.integer  "j_job_id"
    t.integer  "jj_characteristic_id"
    t.integer  "jj_characteristic_value_id"
    t.integer  "register_job_jj_characteristic_id"
    t.string   "value_string"
    t.text     "value_text"
    t.date     "value_date"
    t.integer  "value_int"
    t.float    "value_float"
    t.string   "value_file"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "j_job_jj_characteristics", ["j_job_id"], :name => "index_j_job_jj_characteristics_on_j_job_id"
  add_index "j_job_jj_characteristics", ["jj_characteristic_id"], :name => "index_j_job_jj_characteristics_on_jj_characteristic_id"
  add_index "j_job_jj_characteristics", ["jj_characteristic_value_id"], :name => "index_j_job_jj_characteristics_on_jj_characteristic_value_id"
  add_index "j_job_jj_characteristics", ["register_job_jj_characteristic_id"], :name => "job_char_regs_jj_char_id"

  create_table "j_job_levels", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "j_jobs", :force => true do |t|
    t.string   "name"
    t.string   "mission"
    t.integer  "j_job_level_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "version"
    t.boolean  "current",        :default => true
    t.integer  "j_job_id"
    t.date     "from_date"
    t.date     "to_date"
    t.integer  "j_pay_band_id"
    t.string   "j_code"
    t.boolean  "alert_band"
  end

  add_index "j_jobs", ["j_job_level_id"], :name => "index_j_jobs_on_j_job_level_id"
  add_index "j_jobs", ["j_pay_band_id"], :name => "index_j_jobs_on_j_pay_band_id"

  create_table "j_pay_bands", :force => true do |t|
    t.string   "name"
    t.decimal  "min_value",  :precision => 15, :scale => 4
    t.decimal  "max_value",  :precision => 15, :scale => 4
    t.boolean  "active",                                    :default => true
    t.datetime "created_at",                                                  :null => false
    t.datetime "updated_at",                                                  :null => false
    t.decimal  "q1",         :precision => 15, :scale => 4
    t.decimal  "q2",         :precision => 15, :scale => 4
    t.decimal  "q3",         :precision => 15, :scale => 4
  end

  create_table "jcc_characteristics", :force => true do |t|
    t.integer  "j_cost_center_id"
    t.integer  "characteristic_id"
    t.string   "value_string"
    t.integer  "characteristic_value_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "jcc_characteristics", ["characteristic_id"], :name => "index_jcc_characteristics_on_characteristic_id"
  add_index "jcc_characteristics", ["characteristic_value_id"], :name => "index_jcc_characteristics_on_characteristic_value_id"
  add_index "jcc_characteristics", ["j_cost_center_id"], :name => "index_jcc_characteristics_on_j_cost_center_id"

  create_table "jj_characteristic_values", :force => true do |t|
    t.string   "value_string"
    t.integer  "position"
    t.boolean  "active",               :default => true
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "jj_characteristic_id"
  end

  add_index "jj_characteristic_values", ["jj_characteristic_id"], :name => "index_jj_characteristic_values_on_jj_characteristic_id"

  create_table "jj_characteristics", :force => true do |t|
    t.string   "name"
    t.integer  "data_type_id"
    t.integer  "position"
    t.boolean  "public",                     :default => true
    t.integer  "j_characteristic_type_id"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.integer  "register_characteristic_id"
    t.integer  "register_position"
    t.boolean  "brc",                        :default => false
    t.boolean  "show_list",                  :default => false
    t.boolean  "val_sec_1"
  end

  add_index "jj_characteristics", ["j_characteristic_type_id"], :name => "index_jj_characteristics_on_j_characteristic_type_id"
  add_index "jj_characteristics", ["register_characteristic_id"], :name => "index_jj_characteristics_on_register_characteristic_id"

  create_table "jm_answer_values", :force => true do |t|
    t.integer  "jm_candidate_answer_id"
    t.integer  "jm_subcharacteristic_id"
    t.string   "value_string"
    t.integer  "value_int"
    t.decimal  "value_float",             :precision => 12, :scale => 4
    t.text     "value_text"
    t.date     "value_date"
    t.text     "value_file"
    t.integer  "value_option_id"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.string   "value_file_crypted_name"
  end

  add_index "jm_answer_values", ["jm_candidate_answer_id"], :name => "index_jm_answer_values_on_jm_candidate_answer_id"
  add_index "jm_answer_values", ["jm_subcharacteristic_id"], :name => "index_jm_answer_values_on_jm_subcharacteristic_id"
  add_index "jm_answer_values", ["value_option_id"], :name => "index_jm_answer_values_on_value_option_id"

  create_table "jm_candidate_answers", :force => true do |t|
    t.integer  "jm_candidate_id"
    t.integer  "jm_characteristic_id"
    t.integer  "position"
    t.boolean  "active",                :default => true
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.integer  "prev_answer_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
  end

  add_index "jm_candidate_answers", ["jm_candidate_id"], :name => "index_jm_candidate_answers_on_jm_candidate_id"
  add_index "jm_candidate_answers", ["jm_characteristic_id"], :name => "index_jm_candidate_answers_on_jm_characteristic_id"
  add_index "jm_candidate_answers", ["prev_answer_id"], :name => "index_jm_candidate_answers_on_prev_answer_id"

  create_table "jm_candidate_comments", :force => true do |t|
    t.integer  "jm_candidate_id"
    t.text     "comment"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "jm_candidate_comments", ["jm_candidate_id"], :name => "index_jm_candidate_comments_on_jm_candidate_id"
  add_index "jm_candidate_comments", ["registered_by_user_id"], :name => "index_jm_candidate_comments_on_registered_by_user_id"

  create_table "jm_candidate_photos", :force => true do |t|
    t.integer  "jm_candidate_id"
    t.string   "file_extension"
    t.string   "crypted_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "jm_candidate_photos", ["jm_candidate_id"], :name => "index_jm_candidate_photos_on_jm_candidate_id"

  create_table "jm_candidates", :force => true do |t|
    t.string   "name"
    t.string   "surname"
    t.string   "email"
    t.string   "phone"
    t.string   "password_digest"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "codigo_rec_pass"
    t.datetime "fecha_rec_pass"
    t.boolean  "internal_user",   :default => false
    t.integer  "user_id"
    t.string   "email_to_switch"
  end

  add_index "jm_candidates", ["user_id"], :name => "index_jm_candidates_on_user_id"

  create_table "jm_characteristics", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "max_qty_items",  :default => 1
    t.string   "code"
    t.boolean  "manager_only",   :default => false
    t.boolean  "tabular_layout", :default => false
  end

  create_table "jm_lists", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "jm_messages", :force => true do |t|
    t.integer  "jm_candidate_id"
    t.integer  "sender_user_id"
    t.integer  "sel_process_id"
    t.string   "subject"
    t.text     "body"
    t.boolean  "read"
    t.datetime "read_at"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "jm_messages", ["jm_candidate_id"], :name => "index_jm_messages_on_jm_candidate_id"
  add_index "jm_messages", ["sel_process_id"], :name => "index_jm_messages_on_sel_process_id"
  add_index "jm_messages", ["sender_user_id"], :name => "index_jm_messages_on_sender_user_id"

  create_table "jm_options", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "jm_list_id"
  end

  create_table "jm_profile_char_to_reports", :force => true do |t|
    t.integer  "jm_profile_char_id"
    t.integer  "jm_subcharacteristic_id"
    t.boolean  "use_to_filter_candidates"
    t.integer  "position_filter_candidates"
    t.boolean  "use_to_list_candidates"
    t.integer  "position_list_candidates"
    t.string   "alias"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "jm_profile_char_to_reports", ["jm_profile_char_id"], :name => "index_jm_profile_char_to_reports_on_jm_profile_char_id"
  add_index "jm_profile_char_to_reports", ["jm_subcharacteristic_id"], :name => "index_jm_profile_char_to_reports_on_jm_subcharacteristic_id"

  create_table "jm_profile_chars", :force => true do |t|
    t.integer  "jm_profile_form_id"
    t.integer  "jm_characteristic_id"
    t.integer  "position"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "jm_profile_section_id"
    t.boolean  "mandatory",             :default => false
  end

  add_index "jm_profile_chars", ["jm_characteristic_id"], :name => "index_jm_profile_chars_on_jm_characteristic_id"
  add_index "jm_profile_chars", ["jm_profile_form_id"], :name => "index_jm_profile_chars_on_jm_profile_form_id"
  add_index "jm_profile_chars", ["jm_profile_section_id"], :name => "index_jm_profile_chars_on_jm_profile_section_id"

  create_table "jm_profile_forms", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "layout"
    t.boolean  "active"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "jm_profile_sections", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "jm_subcharacteristics", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "option_type"
    t.integer  "jm_list_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "jm_characteristic_id"
    t.integer  "position"
  end

  add_index "jm_subcharacteristics", ["jm_list_id"], :name => "index_jm_subcharacteristics_on_jm_list_id"

  create_table "kpi_colors", :force => true do |t|
    t.integer  "kpi_dashboard_id"
    t.float    "percentage"
    t.string   "color"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "kpi_set_id"
    t.integer  "kpi_indicator_id"
  end

  add_index "kpi_colors", ["kpi_dashboard_id"], :name => "index_kpi_colors_on_kpi_dashboard_id"
  add_index "kpi_colors", ["kpi_indicator_id"], :name => "index_kpi_colors_on_kpi_indicator_id"
  add_index "kpi_colors", ["kpi_set_id"], :name => "index_kpi_colors_on_kpi_set_id"

  create_table "kpi_dashboard_guests", :force => true do |t|
    t.integer  "kpi_dashboard_id"
    t.integer  "user_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "kpi_dashboard_guests", ["kpi_dashboard_id"], :name => "index_kpi_dashboard_guests_on_kpi_dashboard_id"
  add_index "kpi_dashboard_guests", ["user_id"], :name => "index_kpi_dashboard_guests_on_user_id"

  create_table "kpi_dashboard_managers", :force => true do |t|
    t.integer  "kpi_dashboard_id"
    t.integer  "user_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "kpi_dashboard_managers", ["kpi_dashboard_id"], :name => "index_kpi_dashboard_managers_on_kpi_dashboard_id"
  add_index "kpi_dashboard_managers", ["user_id"], :name => "index_kpi_dashboard_managers_on_user_id"

  create_table "kpi_dashboard_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "position"
  end

  create_table "kpi_dashboards", :force => true do |t|
    t.string   "name"
    t.string   "period"
    t.date     "from"
    t.date     "to"
    t.string   "frequency"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.float    "upper_limit"
    t.float    "lower_limit"
    t.boolean  "csv"
    t.integer  "kpi_dashboard_type_id"
    t.boolean  "def_view_kpis",               :default => true
    t.boolean  "def_view_table",              :default => true
    t.boolean  "def_view_percentage",         :default => true
    t.text     "pt_rows"
    t.text     "pt_cols"
    t.text     "pt_aggregator"
    t.text     "pt_aggregator_field"
    t.boolean  "show_indicator_final_result", :default => false
    t.boolean  "allow_subsets",               :default => false
    t.string   "subset_alias"
    t.boolean  "show_set_final_result",       :default => false
    t.boolean  "show_set_results"
    t.boolean  "kpi_field_annual_goal",       :default => false
    t.boolean  "def_view_goal",               :default => true
    t.boolean  "allow_set_colors",            :default => false
    t.boolean  "allow_indicator_colors",      :default => false
    t.boolean  "w_p",                         :default => true
  end

  add_index "kpi_dashboards", ["kpi_dashboard_type_id"], :name => "index_kpi_dashboards_on_kpi_dashboard_type_id"

  create_table "kpi_indicator_details", :force => true do |t|
    t.integer  "kpi_indicator_id"
    t.date     "measured_at"
    t.float    "goal"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "kpi_indicator_details", ["kpi_indicator_id"], :name => "index_kpi_indicator_details_on_kpi_indicator_id"

  create_table "kpi_indicator_ranks", :force => true do |t|
    t.integer  "kpi_indicator_id"
    t.float    "percentage"
    t.float    "goal"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "kpi_indicator_detail_id"
  end

  add_index "kpi_indicator_ranks", ["kpi_indicator_id"], :name => "index_kpi_indicator_ranks_on_kpi_indicator_id"

  create_table "kpi_indicator_rols", :force => true do |t|
    t.integer  "kpi_indicator_id"
    t.integer  "kpi_rol_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "user_id"
  end

  add_index "kpi_indicator_rols", ["kpi_indicator_id"], :name => "index_kpi_indicator_rols_on_kpi_indicator_id"
  add_index "kpi_indicator_rols", ["kpi_rol_id"], :name => "index_kpi_indicator_rols_on_kpi_rol_id"
  add_index "kpi_indicator_rols", ["user_id"], :name => "index_kpi_indicator_rols_on_user_id"

  create_table "kpi_indicators", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.float    "weight"
    t.integer  "kpi_set_id"
    t.float    "goal"
    t.string   "unit"
    t.integer  "indicator_type"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.float    "value"
    t.float    "percentage"
    t.integer  "final_result_method", :default => 0
    t.integer  "kpi_subset_id"
    t.float    "power",               :default => 1.0
    t.float    "leverage",            :default => 0.0
    t.string   "opf_annual_goal"
  end

  add_index "kpi_indicators", ["kpi_set_id"], :name => "index_kpi_indicators_on_kpi_set_id"
  add_index "kpi_indicators", ["kpi_subset_id"], :name => "index_kpi_indicators_on_kpi_subset_id"

  create_table "kpi_measurements", :force => true do |t|
    t.float    "value"
    t.float    "percentage"
    t.integer  "kpi_indicator_id"
    t.date     "measured_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "kpi_set_id"
    t.integer  "kpi_subset_id"
  end

  add_index "kpi_measurements", ["kpi_indicator_id"], :name => "index_kpi_measurements_on_kpi_indicator_id"
  add_index "kpi_measurements", ["kpi_set_id"], :name => "index_kpi_measurements_on_kpi_set_id"

  create_table "kpi_rols", :force => true do |t|
    t.string   "name"
    t.integer  "kpi_dashboard_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "kpi_rols", ["kpi_dashboard_id"], :name => "index_kpi_rols_on_kpi_dashboard_id"

  create_table "kpi_set_guests", :force => true do |t|
    t.integer  "kpi_set_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "kpi_set_guests", ["kpi_set_id"], :name => "index_kpi_set_guests_on_kpi_set_id"
  add_index "kpi_set_guests", ["user_id"], :name => "index_kpi_set_guests_on_user_id"

  create_table "kpi_sets", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.integer  "kpi_dashboard_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.float    "value"
    t.float    "percentage"
    t.datetime "last_update"
    t.date     "last_measurement_update"
  end

  add_index "kpi_sets", ["kpi_dashboard_id"], :name => "index_kpi_sets_on_kpi_dashboard_id"

  create_table "kpi_subsets", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.integer  "kpi_set_id"
    t.float    "value"
    t.float    "percentage"
    t.datetime "last_update"
    t.date     "last_measurement_update"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "kpi_subsets", ["kpi_set_id"], :name => "index_kpi_subsets_on_kpi_set_id"

  create_table "levels", :force => true do |t|
    t.string   "nombre"
    t.integer  "orden"
    t.integer  "program_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "levels", ["program_id"], :name => "index_levels_on_program_id"

  create_table "lib_characteristics", :force => true do |t|
    t.integer  "characteristic_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "lib_characteristics", ["characteristic_id"], :name => "index_lib_characteristics_on_characteristic_id"

  create_table "lic_approvers", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "lic_approvers", ["deactivated_by_user_id"], :name => "index_lic_approvers_on_deactivated_by_user_id"
  add_index "lic_approvers", ["registered_by_user_id"], :name => "index_lic_approvers_on_registered_by_user_id"
  add_index "lic_approvers", ["user_id"], :name => "index_lic_approvers_on_user_id"

  create_table "lic_events", :force => true do |t|
    t.integer  "user_id"
    t.integer  "bp_license_id"
    t.integer  "bp_form_id"
    t.integer  "prev_lic_event_id"
    t.date     "date_event"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "bp_event_id"
  end

  add_index "lic_events", ["bp_event_id"], :name => "index_lic_events_on_bp_event_id"
  add_index "lic_events", ["bp_license_id"], :name => "index_lic_events_on_bp_licence_id"
  add_index "lic_events", ["deactivated_by_user_id"], :name => "index_lic_events_on_deactivated_by_user_id"
  add_index "lic_events", ["prev_lic_event_id"], :name => "index_lic_events_on_prev_lic_event_id"
  add_index "lic_events", ["registered_by_user_id"], :name => "index_lic_events_on_registered_by_user_id"
  add_index "lic_events", ["user_id"], :name => "index_lic_events_on_user_id"

  create_table "lic_item_values", :force => true do |t|
    t.integer  "bp_item_id"
    t.text     "value"
    t.string   "value_string"
    t.text     "value_text"
    t.integer  "value_int"
    t.float    "value_float"
    t.decimal  "value_decimal",          :precision => 12, :scale => 4
    t.datetime "value_datetime"
    t.time     "value_time"
    t.integer  "value_option_id"
    t.text     "value_file"
    t.boolean  "active",                                                :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                                              :null => false
    t.datetime "updated_at",                                                              :null => false
    t.integer  "lic_request_id"
    t.date     "value_date"
    t.integer  "prev_lic_item_value_id"
    t.integer  "next_lic_item_value_id"
    t.integer  "lic_event_id"
  end

  add_index "lic_item_values", ["bp_item_id"], :name => "index_lic_item_values_on_bp_item_id"
  add_index "lic_item_values", ["deactivated_by_user_id"], :name => "index_lic_item_values_on_deactivated_by_user_id"
  add_index "lic_item_values", ["lic_event_id"], :name => "index_lic_item_values_on_lic_event_id"
  add_index "lic_item_values", ["lic_request_id"], :name => "index_lic_item_values_on_lic_request_id"
  add_index "lic_item_values", ["next_lic_item_value_id"], :name => "next_lic_item_value_id"
  add_index "lic_item_values", ["prev_lic_item_value_id"], :name => "prev_lic_item_value_id"
  add_index "lic_item_values", ["registered_by_user_id"], :name => "index_lic_item_values_on_registered_by_user_id"

  create_table "lic_requests", :force => true do |t|
    t.integer  "user_id"
    t.integer  "bp_license_id"
    t.date     "event_date"
    t.datetime "begin"
    t.datetime "end"
    t.integer  "days"
    t.integer  "hours"
    t.boolean  "paid"
    t.datetime "paid_at"
    t.integer  "paid_by_user_id"
    t.integer  "status"
    t.text     "status_description"
    t.boolean  "active",                                                :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.integer  "prev_lic_request_id"
    t.integer  "next_lic_request_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                                              :null => false
    t.datetime "updated_at",                                                              :null => false
    t.integer  "bp_form_id"
    t.date     "go_back_date"
    t.integer  "lic_event_id"
    t.decimal  "money",                  :precision => 12, :scale => 4
    t.text     "paid_description"
  end

  add_index "lic_requests", ["bp_form_id"], :name => "index_lic_requests_on_bp_form_id"
  add_index "lic_requests", ["bp_license_id"], :name => "index_lic_requests_on_bp_licence_id"
  add_index "lic_requests", ["deactivated_by_user_id"], :name => "index_lic_requests_on_deactivated_by_user_id"
  add_index "lic_requests", ["lic_event_id"], :name => "index_lic_requests_on_lic_event_id"
  add_index "lic_requests", ["next_lic_request_id"], :name => "index_lic_requests_on_next_lic_request_id"
  add_index "lic_requests", ["paid_by_user_id"], :name => "index_lic_requests_on_money_paid_by_user_id"
  add_index "lic_requests", ["prev_lic_request_id"], :name => "index_lic_requests_on_prev_lic_request_id"
  add_index "lic_requests", ["registered_by_user_id"], :name => "index_lic_requests_on_registered_by_user_id"
  add_index "lic_requests", ["user_id"], :name => "index_lic_requests_on_user_id"

  create_table "lic_user_to_approves", :force => true do |t|
    t.integer  "lic_approver_id"
    t.integer  "user_id"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "lic_user_to_approves", ["deactivated_by_user_id"], :name => "index_lic_user_to_approves_on_deactivated_by_user_id"
  add_index "lic_user_to_approves", ["lic_approver_id"], :name => "index_lic_user_to_approves_on_lic_approver_id"
  add_index "lic_user_to_approves", ["registered_by_user_id"], :name => "index_lic_user_to_approves_on_registered_by_user_id"
  add_index "lic_user_to_approves", ["user_id"], :name => "index_lic_user_to_approves_on_user_id"

  create_table "liq_items", :force => true do |t|
    t.integer  "liq_process_id"
    t.integer  "user_id"
    t.integer  "secu_cohade_id"
    t.string   "description"
    t.integer  "monto"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "monto_o"
    t.integer  "cod_empresa_payroll"
    t.string   "cod_cecos"
  end

  add_index "liq_items", ["liq_process_id"], :name => "index_liq_items_on_liq_process_id"
  add_index "liq_items", ["secu_cohade_id"], :name => "index_liq_items_on_secu_cohade_id"
  add_index "liq_items", ["user_id"], :name => "index_liq_items_on_user_id"

  create_table "liq_processes", :force => true do |t|
    t.integer  "year"
    t.integer  "month"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "lms_characteristics", :force => true do |t|
    t.integer  "characteristic_id"
    t.boolean  "active_search_people",  :default => false
    t.integer  "pos_search_people"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.boolean  "active_reporting",      :default => false
    t.integer  "pos_reporting"
    t.integer  "pos_reporting_acta"
    t.boolean  "active_reporting_acta", :default => false
    t.integer  "pos_unit"
    t.boolean  "active_unit",           :default => false
  end

  add_index "lms_characteristics", ["characteristic_id"], :name => "index_lms_characteristics_on_characteristic_id"

  create_table "log_ct_module_ums", :force => true do |t|
    t.string   "task"
    t.datetime "performed_at"
    t.integer  "user_id"
    t.text     "description"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "performed_by_manager_id"
    t.integer  "performed_by_admin_id"
  end

  add_index "log_ct_module_ums", ["performed_by_admin_id"], :name => "index_log_ct_module_ums_on_performed_by_admin_id"
  add_index "log_ct_module_ums", ["performed_by_manager_id"], :name => "index_log_ct_module_ums_on_performed_by_manager_id"
  add_index "log_ct_module_ums", ["user_id"], :name => "index_log_ct_module_ums_on_user_id"

  create_table "log_folder_files", :force => true do |t|
    t.integer  "folder_file_id"
    t.datetime "fecha"
    t.string   "elemento"
    t.string   "accion"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "user_id"
  end

  add_index "log_folder_files", ["folder_file_id"], :name => "index_log_folder_files_on_folder_file_id"

  create_table "log_logins", :force => true do |t|
    t.integer  "user_id"
    t.datetime "fecha"
    t.string   "http_user_agent"
    t.string   "browser"
    t.string   "version"
    t.string   "platform"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "ip"
    t.text     "active_directory_json"
  end

  add_index "log_logins", ["user_id"], :name => "index_log_logins_on_user_id"

  create_table "log_mailers", :force => true do |t|
    t.string   "module"
    t.string   "step"
    t.text     "description"
    t.datetime "registered_at"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "log_systems", :force => true do |t|
    t.integer  "user_id"
    t.text     "description"
    t.datetime "registered_at"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.text     "params"
    t.string   "controller"
    t.string   "action"
    t.text     "request_raw"
    t.text     "session_raw"
    t.string   "server_name"
    t.string   "remote_ip"
    t.string   "remote_host"
    t.string   "path_info"
    t.string   "request_method"
    t.string   "original_url"
    t.string   "url_referer"
    t.boolean  "request_xhr",        :default => false
    t.string   "commit_id"
    t.datetime "commit_deployed_at"
  end

  add_index "log_systems", ["user_id"], :name => "index_log_systems_on_user_id"

  create_table "log_user_courses", :force => true do |t|
    t.integer  "user_course_id"
    t.datetime "fecha"
    t.string   "elemento"
    t.string   "accion"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "log_user_courses", ["user_course_id"], :name => "index_log_user_courses_on_user_course_id"

  create_table "master_evaluation_alternatives", :force => true do |t|
    t.integer  "numero"
    t.text     "texto"
    t.boolean  "correcta"
    t.integer  "master_evaluation_question_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "master_evaluation_alternatives", ["master_evaluation_question_id"], :name => "master_eval_quest_id"

  create_table "master_evaluation_questions", :force => true do |t|
    t.text     "texto"
    t.boolean  "obligatoria"
    t.boolean  "respuesta_unica"
    t.float    "puntaje"
    t.integer  "tiempo"
    t.integer  "master_evaluation_topic_id"
    t.integer  "master_evaluation_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "disponible",                 :default => true
  end

  add_index "master_evaluation_questions", ["master_evaluation_id"], :name => "index_master_evaluation_questions_on_master_evaluation_id"
  add_index "master_evaluation_questions", ["master_evaluation_topic_id"], :name => "index_master_evaluation_questions_on_master_evaluation_topic_id"

  create_table "master_evaluation_topics", :force => true do |t|
    t.integer  "numero"
    t.text     "nombre"
    t.integer  "master_evaluation_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "master_evaluation_topics", ["master_evaluation_id"], :name => "index_master_evaluation_topics_on_master_evaluation_id"

  create_table "master_evaluations", :force => true do |t|
    t.integer  "numero"
    t.string   "nombre"
    t.text     "descripcion"
    t.string   "tags"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "has_topics",  :default => false
  end

  create_table "master_poll_alternative_requirements", :force => true do |t|
    t.integer  "master_poll_alternative_id"
    t.integer  "master_poll_question_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "master_poll_alternative_requirements", ["master_poll_alternative_id"], :name => "mpa_requirements_master_poll_alternative_id"
  add_index "master_poll_alternative_requirements", ["master_poll_question_id"], :name => "mpa_requirements_master_poll_question_id"

  create_table "master_poll_alternatives", :force => true do |t|
    t.integer  "numero"
    t.text     "texto"
    t.integer  "master_poll_question_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "master_poll_alternatives", ["master_poll_question_id"], :name => "index_master_poll_alternatives_on_master_poll_question_id"

  create_table "master_poll_questions", :force => true do |t|
    t.text     "texto"
    t.integer  "master_poll_id"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "respuesta_unica"
    t.integer  "answer_type",             :default => 0
    t.integer  "required_alternative_id"
    t.string   "group"
    t.boolean  "required",                :default => false
  end

  add_index "master_poll_questions", ["master_poll_id"], :name => "index_master_poll_questions_on_master_poll_id"

  create_table "master_polls", :force => true do |t|
    t.integer  "numero"
    t.string   "nombre"
    t.text     "descripcion"
    t.string   "tags"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "plan_anual",  :default => false
    t.boolean  "modo_grid",   :default => false
    t.boolean  "to_managers", :default => false
  end

  create_table "master_units", :force => true do |t|
    t.integer  "numero"
    t.string   "nombre"
    t.text     "descripcion"
    t.string   "tags"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.string   "crypted_name"
    t.integer  "content_type", :default => 0
    t.integer  "height"
  end

  create_table "medlic_licenses", :force => true do |t|
    t.integer  "user_id"
    t.integer  "medlic_process_id"
    t.string   "license_number"
    t.boolean  "continuous"
    t.integer  "days"
    t.date     "date_begin"
    t.date     "date_end"
    t.string   "absence_type"
    t.string   "specialty"
    t.boolean  "partial"
    t.string   "user_status"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "cc"
    t.string   "cc_description"
    t.string   "company"
    t.boolean  "sent_to_payroll",            :default => false
    t.datetime "sent_to_payroll_at"
    t.integer  "sent_to_payroll_by_user_id"
  end

  add_index "medlic_licenses", ["medlic_process_id"], :name => "index_medlic_licenses_on_medlic_process_id"
  add_index "medlic_licenses", ["registered_by_user_id"], :name => "index_medlic_licenses_on_registered_by_user_id"
  add_index "medlic_licenses", ["sent_to_payroll_by_user_id"], :name => "index_medlic_licenses_on_sent_to_payroll_by_user_id"
  add_index "medlic_licenses", ["user_id"], :name => "index_medlic_licenses_on_user_id"

  create_table "medlic_processes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "medlic_processes", ["registered_by_user_id"], :name => "index_medlic_processes_on_registered_by_user_id"

  create_table "medlic_reported_licenses", :force => true do |t|
    t.integer  "medlic_report_id"
    t.integer  "medlic_license_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "medlic_reported_licenses", ["medlic_license_id"], :name => "index_medlic_reported_licenses_on_medlic_license_id"
  add_index "medlic_reported_licenses", ["medlic_report_id"], :name => "index_medlic_reported_licenses_on_medlic_report_id"
  add_index "medlic_reported_licenses", ["registered_by_user_id"], :name => "index_medlic_reported_licenses_on_registered_by_user_id"

  create_table "medlic_reports", :force => true do |t|
    t.boolean  "partial",                :default => false
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.integer  "medlic_process_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "medlic_reports", ["deactivated_by_user_id"], :name => "index_medlic_reports_on_deactivated_by_user_id"
  add_index "medlic_reports", ["medlic_process_id"], :name => "index_medlic_reports_on_medlic_process_id"
  add_index "medlic_reports", ["registered_by_user_id"], :name => "index_medlic_reports_on_registered_by_user_id"

  create_table "medlic_year_days_licenses", :force => true do |t|
    t.integer  "year"
    t.integer  "days",       :default => 0
    t.integer  "user_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "medlic_year_days_licenses", ["user_id"], :name => "index_medlic_year_days_licenses_on_user_id"

  create_table "nodes", :force => true do |t|
    t.text     "nombre"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "node_id"
  end

  add_index "nodes", ["node_id"], :name => "index_nodes_on_node_id"
  add_index "nodes", ["user_id"], :name => "index_nodes_on_user_id"

  create_table "notification_characteristics", :force => true do |t|
    t.integer  "notification_id"
    t.integer  "characteristic_id"
    t.text     "match_value"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "notification_characteristics", ["characteristic_id"], :name => "index_notification_characteristics_on_characteristic_id"
  add_index "notification_characteristics", ["notification_id"], :name => "index_notification_characteristics_on_notification_id"

  create_table "notifications", :force => true do |t|
    t.integer  "orden"
    t.string   "asunto"
    t.text     "texto"
    t.datetime "fecha"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "password_policies", :force => true do |t|
    t.integer  "priority"
    t.string   "name"
    t.boolean  "use_in_exa"
    t.boolean  "use_in_job_market"
    t.boolean  "default"
    t.boolean  "active"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "password_rules", :force => true do |t|
    t.integer  "password_policy_id"
    t.string   "group_name"
    t.integer  "priority"
    t.string   "name"
    t.text     "description"
    t.string   "regular_expression"
    t.boolean  "mandatory"
    t.boolean  "positive_match"
    t.string   "error_match_message"
    t.integer  "strongness"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "password_rules", ["password_policy_id"], :name => "index_password_rules_on_password_policy_id"

  create_table "pe_alternatives", :force => true do |t|
    t.text     "description"
    t.integer  "value"
    t.integer  "pe_question_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "fixed",          :default => false
  end

  add_index "pe_alternatives", ["pe_question_id"], :name => "index_pe_alternatives_on_pe_question_id"

  create_table "pe_areas", :force => true do |t|
    t.string   "name"
    t.integer  "pe_process_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.integer  "pe_box_id"
    t.boolean  "is_evaluated",         :default => false
    t.boolean  "is_evaluator",         :default => false
    t.boolean  "step_assessment",      :default => false
    t.datetime "step_assessment_date"
  end

  add_index "pe_areas", ["pe_box_id"], :name => "index_pe_areas_on_pe_box_id"
  add_index "pe_areas", ["pe_process_id"], :name => "index_pe_areas_on_pe_process_id"

  create_table "pe_assessment_dimensions", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_member_id"
    t.integer  "pe_dimension_id"
    t.integer  "pe_dimension_group_id"
    t.float    "percentage"
    t.datetime "last_update"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "pe_area_id"
  end

  add_index "pe_assessment_dimensions", ["pe_area_id"], :name => "index_pe_assessment_dimensions_on_pe_area_id"
  add_index "pe_assessment_dimensions", ["pe_dimension_group_id"], :name => "index_pe_assessment_dimensions_on_pe_dimension_group_id"
  add_index "pe_assessment_dimensions", ["pe_dimension_id"], :name => "index_pe_assessment_dimensions_on_pe_dimension_id"
  add_index "pe_assessment_dimensions", ["pe_member_id"], :name => "index_pe_assessment_dimensions_on_pe_member_id"
  add_index "pe_assessment_dimensions", ["pe_process_id"], :name => "index_pe_assessment_dimensions_on_pe_process_id"

  create_table "pe_assessment_evaluations", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_member_rel_id"
    t.integer  "pe_evaluation_id"
    t.float    "points"
    t.float    "percentage"
    t.datetime "last_update"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.integer  "pe_dimension_group_id"
    t.boolean  "valid_evaluation",      :default => true
  end

  add_index "pe_assessment_evaluations", ["pe_dimension_group_id"], :name => "index_pe_assessment_evaluations_on_pe_dimension_group_id"
  add_index "pe_assessment_evaluations", ["pe_evaluation_id"], :name => "index_pe_assessment_evaluations_on_pe_evaluation_id"
  add_index "pe_assessment_evaluations", ["pe_member_rel_id"], :name => "index_pe_assessment_evaluations_on_pe_member_rel_id"
  add_index "pe_assessment_evaluations", ["pe_process_id"], :name => "index_pe_assessment_evaluations_on_pe_process_id"

  create_table "pe_assessment_final_evaluations", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_member_id"
    t.integer  "pe_evaluation_id"
    t.float    "points"
    t.float    "percentage"
    t.datetime "last_update"
    t.boolean  "registered_by_manager",          :default => false
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.integer  "pe_dimension_group_id"
    t.integer  "pe_area_id"
    t.float    "original_percentage"
    t.integer  "pe_original_dimension_group_id"
    t.text     "calibration_comment"
  end

  add_index "pe_assessment_final_evaluations", ["pe_area_id"], :name => "index_pe_assessment_final_evaluations_on_pe_area_id"
  add_index "pe_assessment_final_evaluations", ["pe_dimension_group_id"], :name => "index_pe_assessment_final_evaluations_on_pe_dimension_group_id"
  add_index "pe_assessment_final_evaluations", ["pe_evaluation_id"], :name => "index_pe_assessment_final_evaluations_on_pe_evaluation_id"
  add_index "pe_assessment_final_evaluations", ["pe_member_id"], :name => "index_pe_assessment_final_evaluations_on_pe_member_id"
  add_index "pe_assessment_final_evaluations", ["pe_original_dimension_group_id"], :name => "pe_assessment_final_evaluations_orign_dim_group_id"
  add_index "pe_assessment_final_evaluations", ["pe_process_id"], :name => "index_pe_assessment_final_evaluations_on_pe_process_id"

  create_table "pe_assessment_final_questions", :force => true do |t|
    t.integer  "pe_question_id"
    t.decimal  "points",                            :precision => 30, :scale => 6
    t.float    "percentage"
    t.integer  "pe_assessment_final_question_id"
    t.integer  "pe_assessment_final_evaluation_id"
    t.datetime "created_at",                                                       :null => false
    t.datetime "updated_at",                                                       :null => false
  end

  add_index "pe_assessment_final_questions", ["pe_assessment_final_evaluation_id"], :name => "pafq_pe_assessment_final_evaluation_id"
  add_index "pe_assessment_final_questions", ["pe_assessment_final_question_id"], :name => "pafq_pe_assessment_final_question_id"
  add_index "pe_assessment_final_questions", ["pe_question_id"], :name => "pafq_pe_question_id"

  create_table "pe_assessment_groups", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_evaluation_group_id"
    t.integer  "pe_member_id"
    t.float    "percentage"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "pe_assessment_groups", ["pe_evaluation_group_id"], :name => "index_pe_assessment_groups_on_pe_evaluation_group_id"
  add_index "pe_assessment_groups", ["pe_member_id"], :name => "index_pe_assessment_groups_on_pe_member_id"
  add_index "pe_assessment_groups", ["pe_process_id"], :name => "index_pe_assessment_groups_on_pe_process_id"

  create_table "pe_assessment_questions", :force => true do |t|
    t.integer  "pe_assessment_evaluation_id"
    t.integer  "pe_assessment_question_id"
    t.decimal  "points",                         :precision => 30, :scale => 6
    t.float    "percentage"
    t.text     "comment"
    t.integer  "pe_alternative_id"
    t.decimal  "indicator_achievement",          :precision => 30, :scale => 6
    t.datetime "last_update"
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.integer  "pe_question_id"
    t.string   "indicator_discrete_achievement"
    t.text     "comment_2"
  end

  add_index "pe_assessment_questions", ["pe_alternative_id"], :name => "index_pe_assessment_questions_on_pe_alternative_id"
  add_index "pe_assessment_questions", ["pe_assessment_evaluation_id"], :name => "index_pe_assessment_questions_on_pe_assessment_evaluation_id"
  add_index "pe_assessment_questions", ["pe_assessment_question_id"], :name => "index_pe_assessment_questions_on_pe_assessment_question_id"
  add_index "pe_assessment_questions", ["pe_question_id"], :name => "index_pe_assessment_questions_on_pe_question_id"

  create_table "pe_boxes", :force => true do |t|
    t.integer  "position"
    t.string   "short_name"
    t.string   "name"
    t.text     "description"
    t.string   "text_color"
    t.string   "bg_color"
    t.string   "soft_text_color"
    t.string   "soft_bg_color"
    t.integer  "pe_process_id"
    t.integer  "pe_dim_group_x_id"
    t.integer  "pe_dim_group_y_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "pe_boxes", ["pe_dim_group_x_id"], :name => "index_pe_boxes_on_pe_dim_group_x_id"
  add_index "pe_boxes", ["pe_dim_group_y_id"], :name => "index_pe_boxes_on_pe_dim_group_y_id"
  add_index "pe_boxes", ["pe_process_id"], :name => "index_pe_boxes_on_pe_process_id"

  create_table "pe_cal_committees", :force => true do |t|
    t.integer  "pe_cal_session_id"
    t.integer  "pe_process_id"
    t.integer  "user_id"
    t.boolean  "manager",           :default => false
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  add_index "pe_cal_committees", ["pe_cal_session_id"], :name => "index_pe_cal_committees_on_pe_cal_session_id"
  add_index "pe_cal_committees", ["pe_process_id"], :name => "index_pe_cal_committees_on_pe_process_id"
  add_index "pe_cal_committees", ["user_id"], :name => "index_pe_cal_committees_on_user_id"

  create_table "pe_cal_members", :force => true do |t|
    t.integer  "pe_cal_session_id"
    t.integer  "pe_process_id"
    t.integer  "pe_member_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.datetime "step_calibration_date"
  end

  add_index "pe_cal_members", ["pe_cal_session_id"], :name => "index_pe_cal_members_on_pe_cal_session_id"
  add_index "pe_cal_members", ["pe_member_id"], :name => "index_pe_cal_members_on_pe_member_id"
  add_index "pe_cal_members", ["pe_process_id"], :name => "index_pe_cal_members_on_pe_process_id"

  create_table "pe_cal_sessions", :force => true do |t|
    t.integer  "pe_process_id"
    t.text     "description"
    t.datetime "from_date"
    t.datetime "to_date"
    t.boolean  "active",           :default => true
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "pe_evaluation_id"
  end

  add_index "pe_cal_sessions", ["pe_evaluation_id"], :name => "index_pe_cal_sessions_on_pe_evaluation_id"
  add_index "pe_cal_sessions", ["pe_process_id"], :name => "index_pe_cal_sessions_on_pe_process_id"

  create_table "pe_characteristics", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "characteristic_id"
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
    t.integer  "pos_rep_assessment_status"
    t.boolean  "active_rep_assessment_status",          :default => false
    t.integer  "pos_gui_cal_committee"
    t.boolean  "active_gui_cal_committee",              :default => false
    t.integer  "pos_gui_cal_member"
    t.boolean  "active_gui_cal_member",                 :default => false
    t.integer  "pos_gui_feedback"
    t.boolean  "active_gui_feedback",                   :default => false
    t.integer  "pos_rep_calibration_status"
    t.boolean  "active_rep_calibration_status",         :default => false
    t.integer  "pos_rep_feedback_status"
    t.boolean  "active_rep_feedback_status",            :default => false
    t.integer  "pos_rep_consolidated_final_results"
    t.boolean  "active_rep_consolidated_final_results", :default => false
    t.integer  "pos_gui_validation"
    t.boolean  "active_gui_validation",                 :default => false
    t.integer  "pos_rep_detailed_final_results"
    t.boolean  "active_rep_detailed_final_results"
    t.integer  "pos_gui_selection"
    t.boolean  "active_gui_selection",                  :default => false
    t.integer  "pos_reps_configuration"
    t.boolean  "active_reps_configuration",             :default => false
    t.boolean  "active_segmentation"
  end

  add_index "pe_characteristics", ["characteristic_id"], :name => "index_pe_characteristics_on_characteristic_id"
  add_index "pe_characteristics", ["pe_process_id"], :name => "index_pe_characteristics_on_pe_process_id"
  add_index "pe_characteristics", ["pos_gui_cal_committee"], :name => "index_pe_characteristics_on_pos_gui_cal_committee"
  add_index "pe_characteristics", ["pos_gui_cal_member"], :name => "index_pe_characteristics_on_pos_gui_cal_member"
  add_index "pe_characteristics", ["pos_gui_feedback"], :name => "index_pe_characteristics_on_pos_gui_feedback"
  add_index "pe_characteristics", ["pos_gui_validation"], :name => "index_pe_characteristics_on_pos_gui_validation"
  add_index "pe_characteristics", ["pos_rep_assessment_status"], :name => "index_pe_characteristics_on_pos_rep_assessment_status"
  add_index "pe_characteristics", ["pos_rep_calibration_status"], :name => "index_pe_characteristics_on_pos_rep_calibration_status"
  add_index "pe_characteristics", ["pos_rep_consolidated_final_results"], :name => "pe_char_pos_rep_cons_fin_res"
  add_index "pe_characteristics", ["pos_rep_detailed_final_results"], :name => "index_pe_characteristics_on_pos_rep_detailed_final_results"
  add_index "pe_characteristics", ["pos_rep_feedback_status"], :name => "index_pe_characteristics_on_pos_rep_feedback_status"

  create_table "pe_definition_by_user_validators", :force => true do |t|
    t.integer  "pe_member_id"
    t.integer  "pe_evaluation_id"
    t.integer  "step_number"
    t.boolean  "before_accept"
    t.integer  "pe_process_id"
    t.integer  "validator_id"
    t.boolean  "checked",            :default => false
    t.boolean  "validated",          :default => false
    t.text     "validation_comment"
    t.datetime "validated_at"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  add_index "pe_definition_by_user_validators", ["pe_evaluation_id"], :name => "index_pe_definition_by_user_validators_on_pe_evaluation_id"
  add_index "pe_definition_by_user_validators", ["pe_member_id"], :name => "index_pe_definition_by_user_validators_on_pe_member_id"
  add_index "pe_definition_by_user_validators", ["pe_process_id"], :name => "index_pe_definition_by_user_validators_on_pe_process_id"
  add_index "pe_definition_by_user_validators", ["validator_id"], :name => "index_pe_definition_by_user_validators_on_validator_id"

  create_table "pe_dimension_groups", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.string   "min_sign"
    t.float    "min_percentage"
    t.string   "max_sign"
    t.float    "max_percentage"
    t.integer  "pe_dimension_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "text_color"
  end

  add_index "pe_dimension_groups", ["pe_dimension_id"], :name => "index_pe_dimension_groups_on_pe_dimension_id"

  create_table "pe_dimensions", :force => true do |t|
    t.integer  "dimension"
    t.string   "name"
    t.integer  "pe_process_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "pe_dimensions", ["pe_process_id"], :name => "index_pe_dimensions_on_pe_process_id"

  create_table "pe_element_descriptions", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.integer  "pe_element_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "pe_element_descriptions", ["pe_element_id"], :name => "index_pe_element_descriptions_on_pe_element_id"

  create_table "pe_elements", :force => true do |t|
    t.string   "name"
    t.integer  "pe_evaluation_id"
    t.integer  "pe_element_id"
    t.boolean  "assessed",                            :default => true
    t.boolean  "visible",                             :default => true
    t.integer  "assessment_method"
    t.integer  "calculus_method"
    t.integer  "max_number"
    t.integer  "min_number"
    t.integer  "max_weight"
    t.integer  "min_weight"
    t.integer  "weight_sum"
    t.integer  "element_def_by"
    t.string   "element_def_by_rol"
    t.integer  "assessment_method_def_by"
    t.string   "assessment_method_def_by_rol"
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.boolean  "display_name",                        :default => true
    t.integer  "alternatives_display_method"
    t.integer  "position",                            :default => 1
    t.boolean  "simple",                              :default => true
    t.integer  "kpi_dashboard_id"
    t.boolean  "allows_individual_comments",          :default => false
    t.string   "available_indicator_types"
    t.boolean  "display_weight",                      :default => false
    t.integer  "min_numeric_value"
    t.integer  "max_numeric_value"
    t.boolean  "display_result",                      :default => false
    t.string   "plural_name",                         :default => ""
    t.boolean  "allow_individual_comments_with_boss", :default => false
    t.integer  "fix_rank_models",                     :default => 0
    t.boolean  "allow_activities",                    :default => false
    t.boolean  "allow_action_plan",                   :default => false
    t.boolean  "allow_note",                          :default => false
    t.string   "alias_note",                          :default => "Nota"
    t.string   "alias_comment",                       :default => "Comentario"
    t.string   "alias_comment_2",                     :default => "Comentario"
    t.string   "desc_placeholder"
    t.string   "note_placeholder"
    t.string   "default_goal"
    t.string   "default_unit"
    t.boolean  "note_compulsory",                     :default => false
    t.boolean  "cant_change_goal",                    :default => false
    t.boolean  "unit_allow_digits",                   :default => true
  end

  add_index "pe_elements", ["kpi_dashboard_id"], :name => "index_pe_elements_on_kpi_dashboard_id"
  add_index "pe_elements", ["pe_element_id"], :name => "index_pe_elements_on_pe_element_id"
  add_index "pe_elements", ["pe_evaluation_id"], :name => "index_pe_elements_on_pe_evaluation_id"
  add_index "pe_elements", ["position"], :name => "index_pe_elements_on_position"

  create_table "pe_evaluation_assess_rel_messages", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.integer  "pe_rel_id"
    t.text     "message"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_evaluation_assess_rel_messages", ["pe_evaluation_id"], :name => "index_pe_evaluation_assess_rel_messages_on_pe_evaluation_id"
  add_index "pe_evaluation_assess_rel_messages", ["pe_rel_id"], :name => "index_pe_evaluation_assess_rel_messages_on_pe_rel_id"

  create_table "pe_evaluation_def_rel_messages", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.integer  "pe_rel_id"
    t.text     "message"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_evaluation_def_rel_messages", ["pe_evaluation_id"], :name => "index_pe_evaluation_def_rel_messages_on_pe_evaluation_id"
  add_index "pe_evaluation_def_rel_messages", ["pe_rel_id"], :name => "index_pe_evaluation_def_rel_messages_on_pe_rel_id"

  create_table "pe_evaluation_displayed_def_by_users", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.integer  "pe_displayed_evaluation_id"
    t.integer  "position"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "pe_evaluation_displayed_def_by_users", ["pe_displayed_evaluation_id"], :name => "pe_eval_disp_def_by_users_pe_disp_eval_id"
  add_index "pe_evaluation_displayed_def_by_users", ["pe_displayed_evaluation_id"], :name => "pe_eval_disp_def_by_users_pos"
  add_index "pe_evaluation_displayed_def_by_users", ["pe_evaluation_id"], :name => "pe_eval_disp_def_by_users_pe_eval_id"

  create_table "pe_evaluation_displayed_trackings", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.integer  "pe_displayed_evaluation_id"
    t.integer  "position"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "pe_evaluation_displayed_trackings", ["pe_displayed_evaluation_id"], :name => "pe_eval_disp_tracking_pe_disp_eval_id"
  add_index "pe_evaluation_displayed_trackings", ["pe_displayed_evaluation_id"], :name => "pe_eval_disp_tracking_pos"
  add_index "pe_evaluation_displayed_trackings", ["pe_evaluation_id"], :name => "pe_eval_disp_tracking_pe_eval_id"

  create_table "pe_evaluation_groups", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.integer  "pe_process_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "pe_evaluation_groups", ["pe_process_id"], :name => "index_pe_evaluation_groups_on_pe_process_id"

  create_table "pe_evaluation_rel_group2s", :force => true do |t|
    t.integer  "pe_evaluation_rel_id"
    t.integer  "pe_group2_id"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "pe_evaluation_rel_group2s", ["pe_evaluation_rel_id"], :name => "index_pe_evaluation_rel_group2s_on_pe_evaluation_rel_id"
  add_index "pe_evaluation_rel_group2s", ["pe_group2_id"], :name => "index_pe_evaluation_rel_group2s_on_pe_group2_id"

  create_table "pe_evaluation_rels", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.integer  "pe_rel_id"
    t.float    "weight",                    :default => 0.0
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "final_temporal_assessment", :default => false
  end

  add_index "pe_evaluation_rels", ["pe_evaluation_id"], :name => "index_pe_evaluation_rels_on_pe_evaluation_id"
  add_index "pe_evaluation_rels", ["pe_rel_id"], :name => "index_pe_evaluation_rels_on_pe_rel_id"

  create_table "pe_evaluation_track_rel_messages", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.integer  "pe_rel_id"
    t.text     "message"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_evaluation_track_rel_messages", ["pe_evaluation_id"], :name => "index_pe_evaluation_track_rel_messages_on_pe_evaluation_id"
  add_index "pe_evaluation_track_rel_messages", ["pe_rel_id"], :name => "index_pe_evaluation_track_rel_messages_on_pe_rel_id"

  create_table "pe_evaluations", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.integer  "weight"
    t.boolean  "entered_by_manager"
    t.integer  "pe_process_id"
    t.integer  "pe_dimension_id"
    t.datetime "created_at",                                                                                                                             :null => false
    t.datetime "updated_at",                                                                                                                             :null => false
    t.float    "points_to_ohp",                                                 :default => 10.0
    t.float    "max_per_cant_answer",                                           :default => 100.0
    t.float    "max_percentage",                                                :default => 120.0
    t.float    "min_percentage",                                                :default => 0.0
    t.integer  "pe_delegated_evaluation_id"
    t.integer  "assessment_layout",                                             :default => 0
    t.boolean  "allow_tracking",                                                :default => false
    t.integer  "final_evaluation_operation_evaluators",                         :default => 0
    t.integer  "final_evaluation_operation_evaluations",                        :default => 0
    t.boolean  "step_assessment_show_auto_to_boss",                             :default => false
    t.boolean  "allow_definition_by_users",                                     :default => false
    t.boolean  "allow_definition_by_users_accepted",                            :default => false
    t.integer  "num_steps_definition_by_users_validated_before_accepted",       :default => 0
    t.integer  "num_steps_definition_by_users_validated_after_accepted",        :default => 0
    t.string   "alias_steps_definition_by_users_validated_before_accepted"
    t.string   "alias_steps_definition_by_users_validated_after_accepted"
    t.string   "label_not_ready_definition_by_users",                           :default => "not_ready|default"
    t.string   "label_ready_definition_by_users",                               :default => "ready|success"
    t.string   "label_not_ready_definition_by_users_accepted",                  :default => "not_ready|success"
    t.string   "label_ready_definition_by_users_accepted",                      :default => "ready|success"
    t.string   "label_not_ready_definition_by_users_validated_before_accepted", :default => "not_ready|primary"
    t.string   "label_ready_definition_by_users_validated_before_accepted",     :default => "ready|success"
    t.string   "label_rejected_definition_by_users_validated_before_accepted",  :default => "rejected|danger"
    t.string   "label_not_ready_definition_by_users_validated_after_accepted",  :default => "not_ready|primary"
    t.string   "label_ready_definition_by_users_validated_after_accepted",      :default => "ready|success"
    t.string   "label_rejected_definition_by_users_validated_after_accepted",   :default => "rejected|danger"
    t.boolean  "allow_watching_own_tracking",                                   :default => false
    t.boolean  "allow_watching_own_definition",                                 :default => false
    t.boolean  "watch_my_definition_only_when_finished",                        :default => true
    t.boolean  "send_email_to_accept_definition",                               :default => false
    t.string   "email_to_accept_definition_subject"
    t.text     "email_to_accept_definition_content"
    t.boolean  "entered_by_manager_questions",                                  :default => false
    t.integer  "lay_force_nobr_goal_length",                                    :default => 100
    t.string   "definition_by_users_rol"
    t.string   "label_not_initiated_definition_by_users",                       :default => "not_initiated|default"
    t.boolean  "allow_tracking_with_boss",                                      :default => false
    t.boolean  "def_allow_individual_comments_by_boss",                         :default => false
    t.boolean  "def_allow_watch_individual_comments_by_boss",                   :default => false
    t.boolean  "def_allow_individual_comments_with_boss",                       :default => false
    t.string   "definition_by_users_accept_message",                            :default => "Confirmo que conozco y estoy de acuerdo con la definición"
    t.boolean  "track_allow_individual_comments_by_boss",                       :default => false
    t.boolean  "track_allow_watch_individual_comments_by_boss",                 :default => false
    t.boolean  "track_allow_individual_comments_with_boss",                     :default => false
    t.boolean  "assess_allow_individual_comments_by_boss",                      :default => false
    t.boolean  "assess_allow_watch_individual_comments_by_boss",                :default => false
    t.boolean  "assess_allow_individual_comments_with_boss",                    :default => false
    t.boolean  "qdefs_allow_individual_comments_by_boss",                       :default => false
    t.boolean  "qdefs_allow_watch_individual_comments_by_boss",                 :default => false
    t.boolean  "qdefs_allow_individual_comments_with_boss",                     :default => false
    t.boolean  "def_allow_action_plan_by_boss",                                 :default => false
    t.boolean  "def_allow_watch_action_plan_by_boss",                           :default => false
    t.boolean  "def_allow_action_plan_with_boss",                               :default => false
    t.boolean  "informative",                                                   :default => false
    t.boolean  "display_in_qdefs",                                              :default => false
    t.boolean  "display_weight_in_qdefs",                                       :default => true
    t.string   "definition_by_user_finish_message",                             :default => "Confirmo que he finalizado la definición"
    t.string   "definition_by_user_validate_before_message",                    :default => "Confirmo que valido la definición"
    t.string   "definition_by_user_validate_after_message",                     :default => "Confirmo que valido la definición"
    t.string   "definition_by_user_finish_validated_message",                   :default => "Confirmo que he corregido y finalizado la definición"
    t.boolean  "allow_definition_by_managers",                                  :default => false
    t.boolean  "definition_by_users_pdf",                                       :default => false
    t.boolean  "definition_by_users_accept_pdf",                                :default => false
    t.boolean  "allow_calibration",                                             :default => false
    t.boolean  "final_temporal_assessment"
    t.boolean  "display_in_qres",                                               :default => false
    t.boolean  "display_weight_in_qres",                                        :default => false
    t.boolean  "qres_allow_individual_comments_by_boss",                        :default => false
    t.boolean  "qres_allow_watch_individual_comments_by_boss",                  :default => false
    t.boolean  "qres_allow_individual_comments_with_boss",                      :default => false
    t.boolean  "step_assessment_show_auto_comment_to_boss",                     :default => false
    t.integer  "pe_evaluation_group_id"
    t.boolean  "display_weight_in_assessment",                                  :default => false
    t.boolean  "send_email_after_accept_definition",                            :default => false
    t.string   "email_after_accept_definition_subject"
    t.text     "email_after_accept_definition_content"
    t.boolean  "definition_by_users_clone",                                     :default => false
    t.boolean  "def_by_users_validated_before_accepted_manager",                :default => true
    t.boolean  "def_by_users_validated_after_accepted_manager",                 :default => true
    t.boolean  "assess_boss_copy_auto",                                         :default => false
    t.boolean  "assess_allow_files_to_boss",                                    :default => false
    t.string   "cod"
  end

  add_index "pe_evaluations", ["pe_delegated_evaluation_id"], :name => "index_pe_evaluations_on_pe_delegated_evaluation_id"
  add_index "pe_evaluations", ["pe_dimension_id"], :name => "index_pe_evaluations_on_pe_dimension_id"
  add_index "pe_evaluations", ["pe_evaluation_group_id"], :name => "index_pe_evaluations_on_pe_evaluation_group_id"
  add_index "pe_evaluations", ["pe_process_id"], :name => "index_pe_evaluations_on_pe_process_id"

  create_table "pe_feedback_accepted_fields", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "position"
    t.string   "name"
    t.boolean  "simple",                    :default => true
    t.integer  "field_type",                :default => 0
    t.boolean  "required",                  :default => true
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "is_survey",                 :default => false
    t.integer  "pe_feedback_field_list_id"
  end

  add_index "pe_feedback_accepted_fields", ["pe_feedback_field_list_id"], :name => "index_pe_feedback_accepted_fields_on_pe_feedback_field_list_id"
  add_index "pe_feedback_accepted_fields", ["pe_process_id"], :name => "index_pe_feedback_accepted_fields_on_pe_process_id"

  create_table "pe_feedback_compound_fields", :force => true do |t|
    t.integer  "pe_feedback_field_id"
    t.integer  "position"
    t.string   "name"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.integer  "field_type",                     :default => 0
    t.integer  "pe_feedback_field_list_id"
    t.boolean  "required",                       :default => true
    t.integer  "pe_feedback_field_list_item_id"
    t.boolean  "list_item_selected",             :default => true
  end

  add_index "pe_feedback_compound_fields", ["field_type"], :name => "index_pe_feedback_compound_fields_on_field_type"
  add_index "pe_feedback_compound_fields", ["pe_feedback_field_id"], :name => "index_pe_feedback_compound_fields_on_pe_feedback_field_id"
  add_index "pe_feedback_compound_fields", ["pe_feedback_field_list_id"], :name => "index_pe_feedback_compound_fields_on_pe_feedback_field_list_id"
  add_index "pe_feedback_compound_fields", ["pe_feedback_field_list_item_id"], :name => "pe_feedback_cp_list_item_id"

  create_table "pe_feedback_field_list_items", :force => true do |t|
    t.string   "description"
    t.integer  "pe_feedback_field_list_id"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.integer  "position"
    t.integer  "pe_feedback_field_list_item_id"
    t.boolean  "list_item_selected",             :default => true
  end

  add_index "pe_feedback_field_list_items", ["pe_feedback_field_list_id"], :name => "index_pe_feedback_field_list_items_on_pe_feedback_field_list_id"
  add_index "pe_feedback_field_list_items", ["pe_feedback_field_list_item_id"], :name => "pe_f_parent_item_id"

  create_table "pe_feedback_field_lists", :force => true do |t|
    t.integer  "pe_process_id"
    t.string   "name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "pe_feedback_field_lists", ["pe_process_id"], :name => "index_pe_feedback_field_lists_on_pe_process_id"

  create_table "pe_feedback_fields", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "position"
    t.string   "name"
    t.boolean  "simple",                           :default => true
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.integer  "field_type",                       :default => 0
    t.integer  "pe_feedback_field_list_id"
    t.integer  "max_number_of_compound_feedbacks", :default => 3
    t.boolean  "required",                         :default => true
    t.integer  "pe_feedback_field_list_item_id"
    t.boolean  "list_item_selected",               :default => true
  end

  add_index "pe_feedback_fields", ["field_type"], :name => "index_pe_feedback_fields_on_field_type"
  add_index "pe_feedback_fields", ["pe_feedback_field_list_id"], :name => "index_pe_feedback_fields_on_pe_feedback_field_list_id"
  add_index "pe_feedback_fields", ["pe_feedback_field_list_item_id"], :name => "index_pe_feedback_fields_on_pe_feedback_field_list_item_id"
  add_index "pe_feedback_fields", ["pe_process_id"], :name => "index_pe_feedback_fields_on_pe_process_id"

  create_table "pe_group2s", :force => true do |t|
    t.string   "name"
    t.integer  "pe_process_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "pe_group2s", ["pe_process_id"], :name => "index_pe_group2s_on_pe_process_id"

  create_table "pe_groups", :force => true do |t|
    t.string   "name"
    t.integer  "pe_evaluation_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_groups", ["pe_evaluation_id"], :name => "index_pe_groups_on_pe_evaluation_id"

  create_table "pe_managers", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "pe_managers", ["pe_process_id"], :name => "index_pe_managers_on_pe_process_id"
  add_index "pe_managers", ["user_id"], :name => "index_pe_managers_on_user_id"

  create_table "pe_member_accepted_feedbacks", :force => true do |t|
    t.integer  "pe_member_id"
    t.integer  "pe_process_id"
    t.integer  "pe_feedback_accepted_field_id"
    t.text     "comment"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "list_item_id"
  end

  add_index "pe_member_accepted_feedbacks", ["list_item_id"], :name => "index_pe_member_accepted_feedbacks_on_list_item_id"
  add_index "pe_member_accepted_feedbacks", ["pe_feedback_accepted_field_id"], :name => "pe_member_accepted_feedback_field_id"
  add_index "pe_member_accepted_feedbacks", ["pe_member_id"], :name => "index_pe_member_accepted_feedbacks_on_pe_member_id"
  add_index "pe_member_accepted_feedbacks", ["pe_process_id"], :name => "index_pe_member_accepted_feedbacks_on_pe_process_id"

  create_table "pe_member_characteristics", :force => true do |t|
    t.integer  "pe_member_id"
    t.integer  "pe_process_id"
    t.integer  "characteristic_id"
    t.string   "value"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "pe_member_characteristics", ["characteristic_id"], :name => "index_pe_member_characteristics_on_characteristic_id"
  add_index "pe_member_characteristics", ["pe_member_id"], :name => "index_pe_member_characteristics_on_pe_member_id"
  add_index "pe_member_characteristics", ["pe_process_id"], :name => "index_pe_member_characteristics_on_pe_process_id"

  create_table "pe_member_compound_feedbacks", :force => true do |t|
    t.integer  "pe_member_feedback_id"
    t.integer  "pe_feedback_compound_field_id"
    t.text     "comment"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "position"
    t.date     "comment_date"
    t.integer  "list_item_id"
  end

  add_index "pe_member_compound_feedbacks", ["list_item_id"], :name => "index_pe_member_compound_feedbacks_on_list_item_id"
  add_index "pe_member_compound_feedbacks", ["pe_feedback_compound_field_id"], :name => "pe_mem_com_feed_mem_com_feed_id"
  add_index "pe_member_compound_feedbacks", ["pe_member_feedback_id"], :name => "pe_mem_com_feed_mem_feed_id"
  add_index "pe_member_compound_feedbacks", ["position"], :name => "index_pe_member_compound_feedbacks_on_position"

  create_table "pe_member_evaluations", :force => true do |t|
    t.integer  "pe_member_id"
    t.integer  "pe_evaluation_id"
    t.integer  "pe_process_id"
    t.float    "weight"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_member_evaluations", ["pe_evaluation_id"], :name => "index_pe_member_evaluations_on_pe_evaluation_id"
  add_index "pe_member_evaluations", ["pe_member_id"], :name => "index_pe_member_evaluations_on_pe_member_id"
  add_index "pe_member_evaluations", ["pe_process_id"], :name => "index_pe_member_evaluations_on_pe_process_id"

  create_table "pe_member_feedbacks", :force => true do |t|
    t.integer  "pe_member_id"
    t.integer  "pe_process_id"
    t.integer  "pe_feedback_field_id"
    t.text     "comment"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.integer  "number_of_compound_feedbacks"
    t.date     "comment_date"
    t.integer  "list_item_id"
  end

  add_index "pe_member_feedbacks", ["list_item_id"], :name => "index_pe_member_feedbacks_on_list_item_id"
  add_index "pe_member_feedbacks", ["pe_feedback_field_id"], :name => "index_pe_member_feedbacks_on_pe_feedback_field_id"
  add_index "pe_member_feedbacks", ["pe_member_id"], :name => "index_pe_member_feedbacks_on_pe_member_id"
  add_index "pe_member_feedbacks", ["pe_process_id"], :name => "index_pe_member_feedbacks_on_pe_process_id"

  create_table "pe_member_group2s", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_member_id"
    t.integer  "pe_group2_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "pe_member_group2s", ["pe_group2_id"], :name => "index_pe_member_group2s_on_pe_group2_id"
  add_index "pe_member_group2s", ["pe_member_id"], :name => "index_pe_member_group2s_on_pe_member_id"
  add_index "pe_member_group2s", ["pe_process_id"], :name => "index_pe_member_group2s_on_pe_process_id"

  create_table "pe_member_groups", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_member_id"
    t.integer  "pe_evaluation_id"
    t.integer  "pe_group_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_member_groups", ["pe_evaluation_id"], :name => "index_pe_member_groups_on_pe_evaluation_id"
  add_index "pe_member_groups", ["pe_group_id"], :name => "index_pe_member_groups_on_pe_group_id"
  add_index "pe_member_groups", ["pe_member_id"], :name => "index_pe_member_groups_on_pe_member_id"
  add_index "pe_member_groups", ["pe_process_id"], :name => "index_pe_member_groups_on_pe_process_id"

  create_table "pe_member_observers", :force => true do |t|
    t.integer  "pe_member_observed_id"
    t.integer  "observer_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "pe_process_id"
  end

  add_index "pe_member_observers", ["observer_id"], :name => "index_pe_member_observers_on_observer_id"
  add_index "pe_member_observers", ["pe_member_observed_id"], :name => "index_pe_member_observers_on_pe_member_observed_id"
  add_index "pe_member_observers", ["pe_process_id"], :name => "index_pe_member_observers_on_pe_process_id"

  create_table "pe_member_rel_shared_comments", :force => true do |t|
    t.integer  "pe_member_rel_id"
    t.text     "comment"
    t.datetime "registered_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_member_rel_shared_comments", ["pe_member_rel_id"], :name => "index_pe_member_rel_shared_comments_on_pe_member_rel_id"

  create_table "pe_member_rels", :force => true do |t|
    t.integer  "pe_process_id"
    t.boolean  "finished",               :default => false
    t.boolean  "valid_evaluator",        :default => true
    t.integer  "pe_member_evaluator_id"
    t.integer  "pe_rel_id"
    t.integer  "pe_member_evaluated_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.float    "weight",                 :default => 1.0
    t.integer  "validator_id"
    t.boolean  "validated",              :default => false
    t.text     "validation_comment"
    t.datetime "finished_at"
    t.datetime "validated_at"
    t.boolean  "assigned_by_boss",       :default => false
  end

  add_index "pe_member_rels", ["pe_member_evaluated_id"], :name => "index_pe_member_rels_on_pe_member_evaluated_id"
  add_index "pe_member_rels", ["pe_member_evaluator_id"], :name => "index_pe_member_rels_on_pe_member_evaluator_id"
  add_index "pe_member_rels", ["pe_process_id"], :name => "index_pe_member_rels_on_pe_process_id"
  add_index "pe_member_rels", ["pe_rel_id"], :name => "index_pe_member_rels_on_pe_rel_id"
  add_index "pe_member_rels", ["validator_id"], :name => "index_pe_member_rels_on_validator_id"

  create_table "pe_members", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "user_id"
    t.boolean  "is_evaluated",                 :default => false
    t.boolean  "is_evaluator",                 :default => false
    t.boolean  "step_assessment",              :default => false
    t.datetime "step_assessment_date"
    t.boolean  "step_calibration",             :default => false
    t.datetime "step_calibration_date"
    t.boolean  "step_feedback",                :default => false
    t.datetime "step_feedback_date"
    t.boolean  "step_feedback_accepted",       :default => false
    t.datetime "step_feedback_accepted_date"
    t.text     "step_feedback_text"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.integer  "pe_box_id"
    t.integer  "pe_box_before_cal_id"
    t.text     "calibration_comment"
    t.integer  "feedback_provider_id"
    t.integer  "pe_area_id"
    t.boolean  "step_validation",              :default => false
    t.datetime "step_validation_date"
    t.boolean  "require_validation",           :default => false
    t.boolean  "can_select_evaluated",         :default => false
    t.boolean  "can_be_selected_as_evaluated", :default => false
    t.boolean  "removed",                      :default => false
    t.boolean  "step_assign",                  :default => false
    t.datetime "step_assign_date"
  end

  add_index "pe_members", ["feedback_provider_id"], :name => "index_pe_members_on_feedback_provider_id"
  add_index "pe_members", ["pe_area_id"], :name => "index_pe_members_on_pe_area_id"
  add_index "pe_members", ["pe_box_before_cal_id"], :name => "index_pe_members_on_pe_box_before_cal_id"
  add_index "pe_members", ["pe_box_id"], :name => "index_pe_members_on_pe_box_id"
  add_index "pe_members", ["pe_process_id"], :name => "index_pe_members_on_pe_process_id"
  add_index "pe_members", ["user_id"], :name => "index_pe_members_on_user_id"

  create_table "pe_process_notification_def_vals", :force => true do |t|
    t.integer  "pe_process_id"
    t.boolean  "before_accepted",        :default => false
    t.integer  "num_step"
    t.boolean  "send_email_when_accept", :default => false
    t.boolean  "send_email_when_reject", :default => false
    t.string   "accepted_subject"
    t.text     "accepted_body"
    t.string   "rejected_subject"
    t.text     "rejected_body"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "pe_process_notification_def_vals", ["pe_process_id"], :name => "index_pe_process_notification_def_vals_on_pe_process_id"

  create_table "pe_process_notification_defs", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_evaluation_id"
    t.boolean  "send_email_to_validator_when_defined"
    t.string   "to_validator_when_defined_subject"
    t.text     "to_validator_when_defined_body"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  add_index "pe_process_notification_defs", ["pe_evaluation_id"], :name => "index_pe_process_notification_defs_on_pe_evaluation_id"
  add_index "pe_process_notification_defs", ["pe_process_id"], :name => "index_pe_process_notification_defs_on_pe_process_id"

  create_table "pe_process_notifications", :force => true do |t|
    t.integer  "pe_process_id"
    t.string   "step_feedback_accepted_ask_to_accept_subject"
    t.text     "step_feedback_accepted_ask_to_accept_body"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "step_validation_accepted_subject"
    t.text     "step_validation_accepted_body"
    t.string   "step_validation_rejected_subject"
    t.text     "step_validation_rejected_body"
    t.string   "step_assessment_to_validator_subject"
    t.text     "step_assessment_to_validator_body"
    t.string   "step_feedback_accepted_to_boss_subject"
    t.text     "step_feedback_accepted_to_boss_body"
    t.string   "step_query_definitions_accept_subject"
    t.text     "step_query_definitions_accept_body"
    t.string   "step_query_definitions_accept_cc"
    t.string   "step_query_definitions_accept_subject_to_boss"
    t.text     "step_query_definitions_accept_body_to_boss"
  end

  add_index "pe_process_notifications", ["pe_process_id"], :name => "index_pe_process_notifications_on_pe_process_id"

  create_table "pe_process_reports", :force => true do |t|
    t.integer  "pe_process_id"
    t.boolean  "rep_evaluated_evaluators_configuration",     :default => false
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.boolean  "rep_assessment_status",                      :default => false
    t.boolean  "rep_validation_status",                      :default => false
    t.boolean  "rep_calibration_status",                     :default => false
    t.boolean  "rep_feedback_status",                        :default => false
    t.boolean  "rep_assessment_consolidated_results",        :default => false
    t.boolean  "rep_assessment_detailed_results",            :default => false
    t.boolean  "rep_evaluations_configutation",              :default => false
    t.boolean  "rep_general_status",                         :default => false
    t.boolean  "rep_consolidated_feedback",                  :default => false
    t.boolean  "rep_detailed_final_results_evaluations",     :default => false
    t.boolean  "rep_detailed_final_results_questions",       :default => false
    t.boolean  "rep_consolidated_feedback_accepted",         :default => false
    t.boolean  "rep_consolidated_feedback_accepted_survey",  :default => false
    t.boolean  "rep_detailed_auto",                          :default => false
    t.string   "rep_detailed_auto_elements"
    t.text     "rep_detailed_text_1"
    t.text     "rep_detailed_text_2"
    t.boolean  "rep_rol_privado",                            :default => false
    t.boolean  "rep_assessment_detailed_results_i"
    t.boolean  "rep_tracking_status",                        :default => false
    t.boolean  "rep_detailed_final_results_evaluations_cal"
    t.boolean  "rep_assign_status",                          :default => false
    t.boolean  "rep_res_habitat_1",                          :default => false
  end

  add_index "pe_process_reports", ["pe_process_id"], :name => "index_pe_process_reports_on_pe_process_id"

  create_table "pe_process_reports_evaluations", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_evaluation_id"
    t.boolean  "definition_status",                     :default => false
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
    t.boolean  "rep_detailed_final_results_evaluation", :default => false
    t.boolean  "detailed_definition",                   :default => false
    t.boolean  "members_configuration"
    t.boolean  "tracking_status",                       :default => false
    t.boolean  "detailed_definition_i",                 :default => false
  end

  add_index "pe_process_reports_evaluations", ["pe_evaluation_id"], :name => "index_pe_process_reports_evaluations_on_pe_evaluation_id"
  add_index "pe_process_reports_evaluations", ["pe_process_id"], :name => "index_pe_process_reports_evaluations_on_pe_process_id"

  create_table "pe_process_tutorials", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "tutorial_id"
    t.string   "name"
    t.boolean  "active"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "position"
  end

  add_index "pe_process_tutorials", ["pe_process_id"], :name => "index_pe_process_tutorials_on_pe_process_id"
  add_index "pe_process_tutorials", ["tutorial_id"], :name => "index_pe_process_tutorials_on_tutorial_id"

  create_table "pe_processes", :force => true do |t|
    t.string   "name"
    t.string   "period"
    t.datetime "created_at",                                                                                                                                                      :null => false
    t.datetime "updated_at",                                                                                                                                                      :null => false
    t.boolean  "two_dimensions",                                         :default => true
    t.datetime "from_date"
    t.datetime "to_date"
    t.boolean  "active",                                                 :default => false
    t.boolean  "has_step_calibration",                                   :default => false
    t.boolean  "has_step_feedback",                                      :default => false
    t.boolean  "has_step_feedback_accepted",                             :default => false
    t.boolean  "step_feedback_show_names"
    t.boolean  "step_feedback_accepted_send_mail"
    t.string   "rep_detailed_final_results_elements"
    t.boolean  "of_persons",                                             :default => true
    t.boolean  "has_step_definition_by_users",                           :default => false
    t.boolean  "has_step_tracking",                                      :default => false
    t.boolean  "has_step_validation",                                    :default => false
    t.boolean  "has_step_assessment",                                    :default => false
    t.string   "description"
    t.boolean  "rep_detailed_final_results_show_boss"
    t.string   "rep_detailed_final_results_show_feedback"
    t.string   "rep_detailed_final_results_show_calibration"
    t.boolean  "step_assessment_show_rep_dfr_to_boss",                   :default => false
    t.text     "step_assessment_show_rep_dfr_to_boss_text"
    t.boolean  "step_feedback_show_rep_dfr"
    t.text     "step_feedback_show_rep_dfr_text"
    t.boolean  "step_feedback_show_rep_dfr_pdf"
    t.boolean  "step_feedback_show_calibration",                         :default => false
    t.boolean  "rep_consolidated_final_results_col_boss",                :default => false
    t.boolean  "has_step_my_results",                                    :default => false
    t.boolean  "display_results_in_user_profile",                        :default => false
    t.boolean  "display_results_in_my_people",                           :default => false
    t.boolean  "display_user_profile_show_rep_dfr",                      :default => false
    t.boolean  "display_my_people_show_rep_dfr",                         :default => false
    t.boolean  "display_user_profile_show_rep_dfr_pdf",                  :default => false
    t.boolean  "display_my_people_show_rep_dfr_pdf",                     :default => false
    t.boolean  "has_rep_detailed_final_results_only_boss",               :default => false
    t.boolean  "rep_detailed_final_results_only_boss_show_boss",         :default => false
    t.string   "rep_detailed_final_results_only_boss_elements"
    t.string   "rep_detailed_final_results_only_boss_show_calibration"
    t.string   "rep_detailed_final_results_only_boss_show_feedback"
    t.string   "step_definition_by_users_alias",                         :default => "Definir"
    t.string   "step_tracking_alias",                                    :default => "Seguimiento de"
    t.string   "step_assessment_alias",                                  :default => "Evaluar"
    t.string   "step_calibration_alias",                                 :default => "Calibrar"
    t.string   "step_feedback_alias",                                    :default => "Retroalimentar"
    t.string   "step_definition_by_users_img"
    t.string   "step_tracking_img"
    t.string   "step_assessment_img"
    t.boolean  "has_step_definition_by_users_accepted",                  :default => false
    t.boolean  "has_step_definition_by_users_validated",                 :default => false
    t.string   "mailer_name"
    t.boolean  "has_step_my_subs_results",                               :default => false
    t.boolean  "public_pe_members_box",                                  :default => true
    t.boolean  "public_pe_members_dimension_name",                       :default => true
    t.boolean  "step_assessment_show_final_results_to_boss",             :default => false
    t.boolean  "step_assessment_show_final_results_to_boss_bf",          :default => false
    t.boolean  "step_validation_send_email_when_accept",                 :default => false
    t.boolean  "step_validation_send_email_when_reject",                 :default => false
    t.boolean  "step_feedback_accepted_show_rep_dfr"
    t.text     "step_feedback_accepted_show_rep_dfr_text"
    t.boolean  "public_pe_box",                                          :default => true
    t.boolean  "public_pe_dimension_name",                               :default => true
    t.boolean  "manage_dates",                                           :default => false
    t.boolean  "manage_definition",                                      :default => false
    t.boolean  "manage_enter_by_manager",                                :default => false
    t.boolean  "manage_evaluations",                                     :default => false
    t.boolean  "manage_calibration",                                     :default => false
    t.boolean  "manage_feedback",                                        :default => false
    t.boolean  "rep_detailed_final_results_show_feedback_provider"
    t.boolean  "has_step_query_definitions",                             :default => false
    t.string   "step_query_definitions_alias",                           :default => "Consultar definición"
    t.boolean  "rep_detailed_final_results_show_rel_weight",             :default => false
    t.boolean  "has_step_selection",                                     :default => false
    t.string   "step_selection_alias",                                   :default => "Seleccionar"
    t.boolean  "step_assessment_send_email_to_validator",                :default => false
    t.string   "step_assessment_finish_message",                         :default => "Confirmo que he finalizado la evaluación"
    t.string   "step_validation_ok_message",                             :default => "Confirmo que valido la evaluación"
    t.string   "step_validation_no_message",                             :default => "No estoy de acuerdo con la evaluación realizada"
    t.string   "step_feedback_accepted_alias",                           :default => "Confirmar retroalimentación"
    t.boolean  "step_feedback_accepted_mail_to_boss"
    t.string   "step_feedback_finish_message",                           :default => "Confirmo que he finalizado la retroalimentación."
    t.string   "step_feedback_accepted_finish_message",                  :default => "Confirmo que he recibido la retroalimentación y la información proporcionada es correcta."
    t.boolean  "rep_detailed_final_results_show_feedback_date",          :default => false
    t.boolean  "rep_detailed_final_results_show_feedback_accepted_date", :default => false
    t.boolean  "step_assessment_show_rep_dfr_to_boss_pdf",               :default => false
    t.boolean  "step_feed_show_rep_dfr_to_boss",                         :default => false
    t.boolean  "step_feed_show_rep_dfr_to_boss_pdf",                     :default => false
    t.text     "step_feed_show_rep_dfr_to_boss_text"
    t.boolean  "step_feedback_accepted_show_rep_dfr_pdf",                :default => false
    t.string   "step_assessment_final_result_alias",                     :default => "Valoración final"
    t.boolean  "has_step_calibration_evaluation"
    t.string   "step_calibration_evaluation_alias",                      :default => "Calibrar"
    t.boolean  "has_step_query_results",                                 :default => false
    t.string   "step_query_results_alias",                               :default => "Consultar resultados"
    t.boolean  "step_assessment_can_finish",                             :default => true
    t.boolean  "step_assessment_summary_group",                          :default => false
    t.boolean  "step_feedback_summary",                                  :default => true
    t.boolean  "step_query_show_summary",                                :default => false
    t.boolean  "step_query_definitions_accept",                          :default => false
    t.boolean  "step_query_definitions_accept_mail"
    t.boolean  "step_query_definitions_accept_mail_to_boss"
    t.boolean  "step_assessment_show_calculus",                          :default => true
    t.boolean  "step_assessment_uniq"
    t.boolean  "step_feedback_uniq"
    t.boolean  "step_assessment_menu",                                   :default => true
    t.boolean  "step_feedback_menu",                                     :default => true
    t.boolean  "step_assess_uniq_show_temp_res"
    t.boolean  "has_step_assign",                                        :default => false
    t.string   "step_assign_alias",                                      :default => "Asignar evaluadores"
    t.integer  "step_assign_min",                                        :default => 1
    t.integer  "step_assign_max",                                        :default => 1
    t.boolean  "step_validation_uniq"
    t.boolean  "step_validation_menu",                                   :default => true
    t.boolean  "step_assessment_cols_evals",                             :default => true
    t.boolean  "step_assessment_back_list",                              :default => false
    t.boolean  "manage_calibration_e"
    t.boolean  "step_assessment_show_results",                           :default => true
  end

  create_table "pe_question_activities", :force => true do |t|
    t.integer  "position"
    t.integer  "pe_question_activity_field_id"
    t.string   "value_string"
    t.date     "value_date"
    t.integer  "pe_question_activity_field_list_item_id"
    t.integer  "user_creator_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.integer  "pe_question_id"
    t.integer  "pe_question_activity_id"
  end

  add_index "pe_question_activities", ["pe_question_activity_field_id"], :name => "index_pe_question_activities_on_pe_question_activity_field_id"
  add_index "pe_question_activities", ["pe_question_activity_field_list_item_id"], :name => "pe_question_activities_list_item_id"
  add_index "pe_question_activities", ["pe_question_activity_id"], :name => "index_pe_question_activities_on_pe_question_activity_id"
  add_index "pe_question_activities", ["pe_question_id"], :name => "index_pe_question_activities_on_pe_question_id"
  add_index "pe_question_activities", ["user_creator_id"], :name => "index_pe_question_activities_on_user_creator_id"

  create_table "pe_question_activity_field_list_items", :force => true do |t|
    t.integer  "pe_question_activity_field_list_id"
    t.string   "description"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "pe_question_activity_field_list_items", ["pe_question_activity_field_list_id"], :name => "pe_question_activity_field_list_items_list_id"

  create_table "pe_question_activity_field_lists", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.string   "name"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_question_activity_field_lists", ["pe_evaluation_id"], :name => "index_pe_question_activity_field_lists_on_pe_evaluation_id"

  create_table "pe_question_activity_fields", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.integer  "position"
    t.string   "name"
    t.integer  "field_type",                         :default => 0
    t.integer  "pe_question_activity_field_list_id"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.boolean  "required",                           :default => true
  end

  add_index "pe_question_activity_fields", ["pe_evaluation_id"], :name => "index_pe_question_activity_fields_on_pe_evaluation_id"
  add_index "pe_question_activity_fields", ["pe_question_activity_field_list_id"], :name => "pe_question_activity_fields_list_id"

  create_table "pe_question_activity_statuses", :force => true do |t|
    t.integer  "pe_evaluation_id"
    t.integer  "position"
    t.string   "name"
    t.string   "color"
    t.boolean  "completed"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_question_activity_statuses", ["pe_evaluation_id"], :name => "index_pe_question_activity_statuses_on_pe_evaluation_id"

  create_table "pe_question_comments", :force => true do |t|
    t.integer  "pe_question_id"
    t.integer  "pe_member_rel_id"
    t.text     "comment"
    t.datetime "registered_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_question_comments", ["pe_member_rel_id"], :name => "index_pe_question_comments_on_pe_member_rel_id"
  add_index "pe_question_comments", ["pe_question_id"], :name => "index_pe_question_comments_on_pe_question_id"

  create_table "pe_question_descriptions", :force => true do |t|
    t.text     "description"
    t.integer  "pe_question_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "pe_element_description_id"
  end

  add_index "pe_question_descriptions", ["pe_element_description_id"], :name => "index_pe_question_descriptions_on_pe_element_description_id"
  add_index "pe_question_descriptions", ["pe_question_id"], :name => "index_pe_question_descriptions_on_pe_question_id"

  create_table "pe_question_files", :force => true do |t|
    t.string   "description"
    t.string   "file_name"
    t.integer  "pe_question_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.datetime "registered_at"
    t.integer  "pe_member_id"
    t.integer  "user_id"
  end

  add_index "pe_question_files", ["pe_member_id"], :name => "index_pe_question_files_on_pe_member_id"
  add_index "pe_question_files", ["pe_question_id"], :name => "index_pe_question_files_on_pe_question_id"
  add_index "pe_question_files", ["user_id"], :name => "index_pe_question_files_on_user_id"

  create_table "pe_question_models", :force => true do |t|
    t.string   "description"
    t.float    "weight",           :default => 0.0
    t.integer  "pe_evaluation_id"
    t.integer  "pe_element_id"
    t.boolean  "has_comment"
    t.integer  "stored_image_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "min_weight"
    t.integer  "max_weight"
    t.integer  "weight_sum"
    t.integer  "min_number"
    t.integer  "max_number"
  end

  add_index "pe_question_models", ["pe_element_id"], :name => "index_pe_question_models_on_pe_element_id"
  add_index "pe_question_models", ["pe_evaluation_id"], :name => "index_pe_question_models_on_pe_evaluation_id"
  add_index "pe_question_models", ["stored_image_id"], :name => "index_pe_question_models_on_stored_image_id"

  create_table "pe_question_rank_models", :force => true do |t|
    t.float    "percentage"
    t.decimal  "goal",          :precision => 30, :scale => 6
    t.string   "discrete_goal"
    t.integer  "pe_element_id"
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
    t.boolean  "discrete",                                     :default => false
  end

  add_index "pe_question_rank_models", ["pe_element_id"], :name => "index_pe_question_rank_models_on_pe_element_id"

  create_table "pe_question_ranks", :force => true do |t|
    t.float    "percentage"
    t.decimal  "goal",                              :precision => 30, :scale => 6
    t.integer  "hr_process_evaluation_manual_q_id"
    t.datetime "created_at",                                                                       :null => false
    t.datetime "updated_at",                                                                       :null => false
    t.string   "discrete_goal",                                                    :default => ""
    t.integer  "pe_question_id"
    t.integer  "pe_element_id"
    t.integer  "pe_question_rank_model_id"
  end

  add_index "pe_question_ranks", ["hr_process_evaluation_manual_q_id"], :name => "index_pe_question_ranks_on_hr_process_evaluation_manual_q_id"
  add_index "pe_question_ranks", ["pe_element_id"], :name => "index_pe_question_ranks_on_pe_element_id"
  add_index "pe_question_ranks", ["pe_question_id"], :name => "index_pe_question_ranks_on_pe_question_id"
  add_index "pe_question_ranks", ["pe_question_rank_model_id"], :name => "index_pe_question_ranks_on_pe_question_rank_model_id"

  create_table "pe_question_validations", :force => true do |t|
    t.integer  "pe_question_id"
    t.integer  "step_number"
    t.boolean  "before_accept"
    t.boolean  "validated",        :default => false
    t.text     "comment"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.integer  "pe_evaluation_id"
    t.boolean  "attended",         :default => false
  end

  add_index "pe_question_validations", ["pe_evaluation_id"], :name => "index_pe_question_validations_on_pe_evaluation_id"
  add_index "pe_question_validations", ["pe_question_id"], :name => "index_pe_question_validations_on_pe_question_id"

  create_table "pe_questions", :force => true do |t|
    t.text     "description"
    t.float    "weight",                                                        :default => 1.0
    t.integer  "pe_evaluation_id"
    t.integer  "pe_group_id"
    t.integer  "pe_element_id"
    t.integer  "pe_question_id"
    t.boolean  "has_comment"
    t.datetime "created_at",                                                                       :null => false
    t.datetime "updated_at",                                                                       :null => false
    t.integer  "pe_member_evaluator_id"
    t.integer  "pe_member_evaluated_id"
    t.integer  "indicator_type",                                                :default => 0
    t.string   "indicator_unit"
    t.decimal  "indicator_goal",                 :precision => 30, :scale => 6
    t.string   "indicator_discrete_goal"
    t.integer  "kpi_indicator_id"
    t.boolean  "confirmed",                                                     :default => true
    t.integer  "pe_rel_id"
    t.boolean  "informative",                                                   :default => false
    t.boolean  "accepted",                                                      :default => true
    t.boolean  "ready",                                                         :default => true
    t.boolean  "validated",                                                     :default => true
    t.integer  "creator_id"
    t.boolean  "entered_by_manager",                                            :default => false
    t.float    "points"
    t.integer  "pe_alternative_id"
    t.float    "indicator_achievement"
    t.string   "indicator_discrete_achievement"
    t.integer  "stored_image_id"
    t.integer  "pe_question_model_id"
    t.float    "power",                                                         :default => 1.0
    t.float    "leverage",                                                      :default => 0.0
    t.integer  "number_of_activities",                                          :default => 0
    t.text     "comment"
    t.text     "note"
    t.boolean  "has_comment_compulsory",                                        :default => false
    t.integer  "questions_grouped_id"
    t.boolean  "has_comment_2",                                                 :default => false
    t.boolean  "has_comment_2_compulsory",                                      :default => false
  end

  add_index "pe_questions", ["creator_id"], :name => "index_pe_questions_on_creator_id"
  add_index "pe_questions", ["kpi_indicator_id"], :name => "index_pe_questions_on_kpi_indicator_id"
  add_index "pe_questions", ["pe_alternative_id"], :name => "index_pe_questions_on_pe_alternative_id"
  add_index "pe_questions", ["pe_element_id"], :name => "index_pe_questions_on_pe_element_id"
  add_index "pe_questions", ["pe_evaluation_id"], :name => "index_pe_questions_on_pe_evaluation_id"
  add_index "pe_questions", ["pe_group_id"], :name => "index_pe_questions_on_pe_group_id"
  add_index "pe_questions", ["pe_member_evaluated_id"], :name => "index_pe_questions_on_pe_member_evaluated_id"
  add_index "pe_questions", ["pe_member_evaluator_id"], :name => "index_pe_questions_on_pe_member_evaluator_id"
  add_index "pe_questions", ["pe_question_id"], :name => "index_pe_questions_on_pe_question_id"
  add_index "pe_questions", ["pe_question_model_id"], :name => "index_pe_questions_on_pe_question_model_id"
  add_index "pe_questions", ["pe_rel_id"], :name => "index_pe_questions_on_pe_rel_id"
  add_index "pe_questions", ["stored_image_id"], :name => "index_pe_questions_on_stored_image_id"

  create_table "pe_rels", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "rel"
    t.string   "alias"
    t.string   "alias_reverse"
    t.string   "alias_plural"
    t.string   "alias_reverse_plural"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.integer  "position",                            :default => 0
    t.boolean  "allows_shared_comments",              :default => false
    t.boolean  "display_shared_comments_in_feedback", :default => false
    t.boolean  "available_in_selection",              :default => false
    t.integer  "max_number_to_select",                :default => 1
    t.integer  "max_num_times_to_be_selected",        :default => 1
    t.boolean  "available_in_assign",                 :default => false
    t.integer  "max_number_to_assign",                :default => 1
    t.integer  "min_number_to_assign",                :default => 1
    t.boolean  "assign_total_count",                  :default => false
  end

  add_index "pe_rels", ["pe_process_id"], :name => "index_pe_rels_on_pe_process_id"

  create_table "pe_reporter_characteristic_values", :force => true do |t|
    t.integer  "pe_reporter_id"
    t.integer  "pe_characteristic_id"
    t.string   "value"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "pe_reporter_characteristic_values", ["pe_characteristic_id"], :name => "index_pe_reporter_characteristic_values_on_pe_characteristic_id"
  add_index "pe_reporter_characteristic_values", ["pe_reporter_id"], :name => "index_pe_reporter_characteristic_values_on_pe_reporter_id"

  create_table "pe_reporters", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "user_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "general",       :default => true
  end

  add_index "pe_reporters", ["pe_process_id"], :name => "index_pe_reporters_on_pe_process_id"
  add_index "pe_reporters", ["user_id"], :name => "index_pe_reporters_on_user_id"

  create_table "pe_slider_groups", :force => true do |t|
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.string   "text_color"
    t.string   "bg_color"
    t.string   "min_sign"
    t.integer  "min_value"
    t.string   "max_sign"
    t.integer  "max_value"
    t.integer  "pe_evaluation_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "pe_slider_groups", ["pe_evaluation_id"], :name => "index_pe_slider_groups_on_pe_evaluation_id"

  create_table "pe_step_feedback_group2s", :force => true do |t|
    t.integer  "pe_process_id"
    t.integer  "pe_group2_id"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "pe_step_feedback_group2s", ["pe_group2_id"], :name => "index_pe_step_feedback_group2s_on_pe_group2_id"
  add_index "pe_step_feedback_group2s", ["pe_process_id"], :name => "index_pe_step_feedback_group2s_on_pe_process_id"

  create_table "planning_process_company_unit_area_corrections", :force => true do |t|
    t.text     "correccion"
    t.boolean  "revisado",                         :default => false
    t.integer  "planning_process_company_unit_id"
    t.integer  "company_unit_area_id"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

  add_index "planning_process_company_unit_area_corrections", ["company_unit_area_id"], :name => "ppcuac_cuai"
  add_index "planning_process_company_unit_area_corrections", ["planning_process_company_unit_id"], :name => "ppcuac_ppcui"

  create_table "planning_process_company_unit_areas", :force => true do |t|
    t.integer  "planning_process_id"
    t.integer  "planning_process_company_unit_id"
    t.integer  "company_unit_area_id"
    t.string   "objetivos"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "planning_process_company_unit_areas", ["company_unit_area_id"], :name => "ppcua_cuai"
  add_index "planning_process_company_unit_areas", ["planning_process_id"], :name => "ppcua_ppi"

  create_table "planning_process_company_units", :force => true do |t|
    t.integer  "planning_process_id"
    t.integer  "company_unit_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.datetime "fecha_fin"
  end

  add_index "planning_process_company_units", ["company_unit_id"], :name => "index_planning_process_company_units_on_company_unit_id"
  add_index "planning_process_company_units", ["planning_process_id"], :name => "index_planning_process_company_units_on_planning_process_id"

  create_table "planning_process_goals", :force => true do |t|
    t.text     "descripcion"
    t.integer  "planning_process_company_unit_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "numero"
  end

  add_index "planning_process_goals", ["planning_process_company_unit_id"], :name => "index_planning_process_goals_on_planning_process_id"

  create_table "planning_processes", :force => true do |t|
    t.string   "nombre"
    t.boolean  "abierto",    :default => true
    t.datetime "fecha_fin"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.integer  "year"
  end

  create_table "poll_characteristics", :force => true do |t|
    t.boolean  "active_reporting",  :default => false
    t.integer  "pos_reporting"
    t.integer  "characteristic_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  add_index "poll_characteristics", ["characteristic_id"], :name => "index_poll_characteristics_on_characteristic_id"

  create_table "poll_process_characteristics", :force => true do |t|
    t.integer  "poll_process_id"
    t.integer  "characteristic_id"
    t.text     "match_value"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "poll_process_characteristics", ["characteristic_id"], :name => "index_poll_process_characteristics_on_characteristic_id"
  add_index "poll_process_characteristics", ["poll_process_id"], :name => "index_poll_process_characteristics_on_poll_process_id"

  create_table "poll_process_report_chars", :force => true do |t|
    t.integer  "poll_process_id"
    t.integer  "characteristic_id"
    t.integer  "position"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "poll_process_report_chars", ["characteristic_id"], :name => "index_poll_process_report_chars_on_characteristic_id"
  add_index "poll_process_report_chars", ["poll_process_id"], :name => "index_poll_process_report_chars_on_poll_process_id"

  create_table "poll_process_user_answers", :force => true do |t|
    t.integer  "poll_process_user_id"
    t.integer  "master_poll_question_id"
    t.integer  "master_poll_alternative_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.text     "long_answer"
  end

  add_index "poll_process_user_answers", ["master_poll_alternative_id"], :name => "index_poll_process_user_answers_on_master_poll_alternative_id"
  add_index "poll_process_user_answers", ["master_poll_question_id"], :name => "index_poll_process_user_answers_on_master_poll_question_id"
  add_index "poll_process_user_answers", ["poll_process_user_id"], :name => "index_poll_process_user_answers_on_poll_process_user_id"

  create_table "poll_process_users", :force => true do |t|
    t.integer  "poll_process_id"
    t.integer  "user_id"
    t.boolean  "done"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.boolean  "enrolled",        :default => false
  end

  add_index "poll_process_users", ["poll_process_id"], :name => "index_poll_process_users_on_poll_process_id"
  add_index "poll_process_users", ["user_id"], :name => "index_poll_process_users_on_user_id"

  create_table "poll_processes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.datetime "from_date"
    t.datetime "to_date"
    t.integer  "master_poll_id"
    t.boolean  "finished"
    t.datetime "finished_at"
    t.integer  "finished_by_user_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "show_preview_results_user", :default => false
    t.boolean  "to_everyone",               :default => false
    t.boolean  "allow_pause",               :default => false
    t.boolean  "grid_view",                 :default => false
    t.integer  "flow",                      :default => 0
    t.boolean  "show_group_names",          :default => false
  end

  add_index "poll_processes", ["master_poll_id"], :name => "index_poll_processes_on_master_poll_id"

  create_table "polls", :force => true do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.integer  "program_id"
    t.integer  "level_id"
    t.integer  "course_id"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.boolean  "libre",                      :default => true
    t.integer  "numero"
    t.boolean  "activa",                     :default => true
    t.integer  "antes_de_unit_id"
    t.integer  "despues_de_unit_id"
    t.integer  "antes_de_evaluation_id"
    t.integer  "despues_de_evaluation_id"
    t.integer  "antes_de_poll_id"
    t.integer  "despues_de_poll_id"
    t.integer  "orden",                      :default => 1
    t.boolean  "obligatoria",                :default => false
    t.integer  "master_poll_id"
    t.boolean  "tipo_satisfaccion",          :default => false
    t.integer  "program_course_instance_id"
    t.boolean  "virtual",                    :default => true
  end

  add_index "polls", ["course_id"], :name => "index_polls_on_course_id"
  add_index "polls", ["level_id"], :name => "index_polls_on_level_id"
  add_index "polls", ["orden"], :name => "index_polls_on_orden"
  add_index "polls", ["program_id"], :name => "index_polls_on_program_id"

  create_table "program_course_instance_evaluations", :force => true do |t|
    t.integer  "program_course_instance_id"
    t.integer  "evaluation_id"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "program_course_instance_evaluations", ["evaluation_id"], :name => "pcie_e_id"
  add_index "program_course_instance_evaluations", ["program_course_instance_id"], :name => "pcie_pci_id"

  create_table "program_course_instance_managers", :force => true do |t|
    t.integer  "program_course_instance_id"
    t.integer  "user_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "program_course_instance_managers", ["program_course_instance_id"], :name => "pcim_pci"
  add_index "program_course_instance_managers", ["user_id"], :name => "pcim_ui"

  create_table "program_course_instance_polls", :force => true do |t|
    t.integer  "program_course_instance_id"
    t.integer  "poll_id"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "program_course_instance_polls", ["poll_id"], :name => "pcip_p_id"
  add_index "program_course_instance_polls", ["program_course_instance_id"], :name => "pcip_pci_id"

  create_table "program_course_instance_units", :force => true do |t|
    t.integer  "program_course_instance_id"
    t.integer  "unit_id"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "program_course_instance_units", ["program_course_instance_id"], :name => "pciu_pci_id"
  add_index "program_course_instance_units", ["unit_id"], :name => "pciu_u_id"

  create_table "program_course_instances", :force => true do |t|
    t.integer  "program_course_id"
    t.integer  "program_instance_id"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.float    "sence_hour_value"
    t.float    "sence_student_value"
    t.float    "sence_hours"
    t.string   "sence_training_type"
    t.string   "sence_region"
    t.string   "sence_otec_name"
    t.string   "sence_otec_address"
    t.string   "sence_otec_telephone"
    t.string   "place"
    t.string   "contract_type"
    t.integer  "minimum_grade"
    t.integer  "minimum_assistance"
    t.integer  "sence_maximum_students_number"
    t.string   "sence_cbipartito"
    t.string   "sence_dnc"
    t.string   "sence_comment"
    t.string   "sence_schedule"
    t.float    "sale_hour_value"
    t.string   "sencenet"
    t.string   "purchase_order_number"
    t.string   "sk_financing"
    t.boolean  "sk_external_certification"
    t.integer  "sence_minimum_students_number"
    t.string   "city"
    t.string   "obra"
    t.string   "proyecto_ci"
    t.string   "description"
    t.integer  "creator_id"
  end

  add_index "program_course_instances", ["creator_id"], :name => "index_program_course_instances_on_creator_id"
  add_index "program_course_instances", ["program_course_id"], :name => "index_program_course_instances_on_program_course_id"
  add_index "program_course_instances", ["program_instance_id"], :name => "index_program_course_instances_on_program_instance_id"

  create_table "program_course_managers", :force => true do |t|
    t.integer  "program_course_id"
    t.integer  "user_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "program_course_managers", ["program_course_id"], :name => "index_program_course_managers_on_program_course_id"
  add_index "program_course_managers", ["user_id"], :name => "index_program_course_managers_on_user_id"

  create_table "program_course_poll_summaries", :force => true do |t|
    t.integer  "program_course_id"
    t.integer  "poll_id"
    t.integer  "grupo"
    t.integer  "master_poll_question_id"
    t.integer  "master_poll_alternative_id"
    t.integer  "numero_respuestas"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "program_course_instance_id"
  end

  add_index "program_course_poll_summaries", ["master_poll_alternative_id"], :name => "pcps_mpa_id"
  add_index "program_course_poll_summaries", ["master_poll_question_id"], :name => "pcps_mpq_id"
  add_index "program_course_poll_summaries", ["poll_id"], :name => "pcps_p_id"
  add_index "program_course_poll_summaries", ["program_course_id"], :name => "pcps_pc_id"

  create_table "program_courses", :force => true do |t|
    t.integer  "program_id"
    t.integer  "course_id"
    t.integer  "level_id"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.integer  "orden"
    t.boolean  "activo",                      :default => true
    t.boolean  "consultable",                 :default => true
    t.boolean  "hidden_in_user_courses_list", :default => false
  end

  add_index "program_courses", ["course_id"], :name => "index_program_courses_on_course_id"
  add_index "program_courses", ["level_id"], :name => "index_program_courses_on_level_id"
  add_index "program_courses", ["program_id"], :name => "index_program_courses_on_program_id"

  create_table "program_inspectors", :force => true do |t|
    t.integer  "program_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "program_inspectors", ["program_id"], :name => "index_program_inspectors_on_program_id"
  add_index "program_inspectors", ["user_id"], :name => "index_program_inspectors_on_user_id"

  create_table "program_instances", :force => true do |t|
    t.string   "name"
    t.integer  "program_id"
    t.date     "from_date"
    t.date     "to_date"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.float    "minimum_grade"
    t.boolean  "percentage_minimum_grade"
    t.boolean  "active",                   :default => true
    t.integer  "master_poll_id"
    t.integer  "num_passed_courses"
    t.string   "sk_client"
    t.string   "sk_rut_client"
  end

  add_index "program_instances", ["program_id"], :name => "index_program_instances_on_program_id"

  create_table "programs", :force => true do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "nombre_corto"
    t.string   "codigo"
    t.string   "color"
    t.boolean  "especifico",         :default => true
    t.boolean  "plan_anual",         :default => false
    t.boolean  "activo",             :default => true
    t.integer  "orden"
    t.boolean  "managed_by_manager", :default => false
  end

  create_table "queued_reports", :force => true do |t|
    t.text     "description"
    t.string   "code"
    t.string   "name"
    t.boolean  "status"
    t.datetime "from_date"
    t.integer  "created_in"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "extension"
  end

  create_table "sc_blocks", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "day"
    t.integer  "sc_schedule_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.time     "begin_time"
    t.time     "end_time"
    t.boolean  "next_day",       :default => false
    t.datetime "registered_at"
    t.boolean  "active",         :default => true
    t.datetime "deactivated_at"
    t.boolean  "workable",       :default => true
    t.integer  "free_time",      :default => 0
  end

  add_index "sc_blocks", ["sc_schedule_id"], :name => "index_sc_blocks_on_sc_schedule_id"

  create_table "sc_characteristic_conditions", :force => true do |t|
    t.integer  "characteristic_id"
    t.integer  "condition"
    t.string   "match_value_string"
    t.integer  "match_value_int"
    t.float    "match_value_float"
    t.text     "match_value_text"
    t.date     "match_value_date"
    t.integer  "match_value_char_id"
    t.integer  "sc_schedule_id"
    t.datetime "registered_at"
    t.boolean  "active",              :default => false
    t.datetime "deactivated_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "sc_characteristic_conditions", ["characteristic_id"], :name => "index_sc_characteristic_conditions_on_characteristic_id"
  add_index "sc_characteristic_conditions", ["sc_schedule_id"], :name => "index_sc_characteristic_conditions_on_sc_schedule_id"

  create_table "sc_documentations", :force => true do |t|
    t.integer  "user_id"
    t.integer  "sc_period_id"
    t.string   "doc_path"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "name"
    t.text     "description"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.date     "begin"
    t.boolean  "deleted",                :default => false
    t.text     "deleted_description"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
  end

  add_index "sc_documentations", ["deactivated_by_user_id"], :name => "index_sc_documentations_on_deactivated_by_user_id"
  add_index "sc_documentations", ["registered_by_user_id"], :name => "index_sc_documentations_on_registered_by_user_id"
  add_index "sc_documentations", ["sc_period_id"], :name => "index_sc_documentations_on_sc_period_id"
  add_index "sc_documentations", ["user_id"], :name => "index_sc_documentations_on_user_id"

  create_table "sc_he_block_params", :force => true do |t|
    t.integer  "entry_min"
    t.integer  "entry_max"
    t.boolean  "entry_include_min",      :default => true
    t.integer  "exit_min"
    t.integer  "exit_max"
    t.boolean  "exit_include_min",       :default => true
    t.integer  "sc_block_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.boolean  "entry_active",           :default => true
    t.boolean  "exit_active",            :default => true
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
  end

  add_index "sc_he_block_params", ["deactivated_by_user_id"], :name => "index_sc_he_block_params_on_deactivated_by_user_id"
  add_index "sc_he_block_params", ["registered_by_user_id"], :name => "index_sc_he_block_params_on_registered_by_user_id"
  add_index "sc_he_block_params", ["sc_block_id"], :name => "index_sc_he_block_params_on_sc_block_id"

  create_table "sc_log_records", :force => true do |t|
    t.integer  "user_id"
    t.datetime "date"
    t.text     "record_before"
    t.text     "record_after"
    t.string   "action"
    t.integer  "sc_record_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "sc_log_records", ["sc_record_id"], :name => "index_sc_log_records_on_sc_record_id"
  add_index "sc_log_records", ["user_id"], :name => "index_sc_log_records_on_user_id"

  create_table "sc_manual_user_to_registers", :force => true do |t|
    t.integer  "sc_process_id"
    t.integer  "user_id"
    t.integer  "he_ct_module_privilage_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "sc_manual_user_to_registers", ["he_ct_module_privilage_id"], :name => "index_sc_manual_user_to_registers_on_he_ct_module_privilage_id"
  add_index "sc_manual_user_to_registers", ["registered_by_user_id"], :name => "index_sc_manual_user_to_registers_on_registered_by_user_id"
  add_index "sc_manual_user_to_registers", ["sc_process_id"], :name => "index_sc_manual_user_to_registers_on_sc_process_id"
  add_index "sc_manual_user_to_registers", ["user_id"], :name => "index_sc_manual_user_to_registers_on_user_id"

  create_table "sc_periods", :force => true do |t|
    t.datetime "begin"
    t.datetime "end"
    t.text     "description"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "sc_periods", ["deactivated_by_user_id"], :name => "sc_periods_deac_user_id"

  create_table "sc_processes", :force => true do |t|
    t.integer  "month"
    t.integer  "year"
    t.datetime "deadline"
    t.boolean  "closed",            :default => false
    t.datetime "closed_at"
    t.integer  "closed_by_user_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.datetime "records_since"
    t.datetime "records_until"
  end

  add_index "sc_processes", ["closed_by_user_id"], :name => "index_sc_processes_on_closed_by_user_id"

  create_table "sc_record_blocks", :force => true do |t|
    t.integer  "sc_record_id"
    t.integer  "sc_block_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
  end

  add_index "sc_record_blocks", ["deactivated_by_user_id"], :name => "index_sc_record_blocks_on_deactivated_by_user_id"
  add_index "sc_record_blocks", ["sc_block_id"], :name => "index_sc_record_blocks_on_sc_block_id"
  add_index "sc_record_blocks", ["sc_record_id"], :name => "index_sc_record_blocks_on_sc_record_id"

  create_table "sc_records", :force => true do |t|
    t.datetime "begin"
    t.datetime "end"
    t.integer  "user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.integer  "sc_process_id"
    t.integer  "sc_user_process_id"
    t.decimal  "he",                                 :precision => 12, :scale => 4
    t.boolean  "dont_charge_to_payroll",                                            :default => false
    t.datetime "dont_charge_to_payroll_at"
    t.integer  "dont_charge_to_payroll_by_user_id"
    t.text     "dont_charge_to_payroll_description"
    t.integer  "status",                                                            :default => 0
    t.datetime "created_at",                                                                           :null => false
    t.datetime "updated_at",                                                                           :null => false
    t.integer  "effective_time"
    t.datetime "rejected_at"
    t.integer  "rejected_by_user_id"
    t.text     "rejected_description"
    t.integer  "sc_user_to_register_id"
    t.integer  "he_ct_module_privilege_id"
    t.boolean  "sent_to_payroll",                                                   :default => false
    t.datetime "begin_sc"
    t.datetime "end_sc"
  end

  add_index "sc_records", ["dont_charge_to_payroll_by_user_id"], :name => "index_sc_records_on_dont_charge_to_payroll_by_user_id"
  add_index "sc_records", ["he_ct_module_privilege_id"], :name => "index_sc_records_on_he_ct_module_privilege_id"
  add_index "sc_records", ["registered_by_user_id"], :name => "index_sc_records_on_registered_by_user_id"
  add_index "sc_records", ["rejected_by_user_id"], :name => "index_sc_records_on_rejected_by_user_id"
  add_index "sc_records", ["sc_process_id"], :name => "index_sc_records_on_sc_process_id"
  add_index "sc_records", ["sc_user_process_id"], :name => "index_sc_records_on_sc_user_process_id"
  add_index "sc_records", ["sc_user_to_register_id"], :name => "index_sc_records_on_sc_user_to_register_id"
  add_index "sc_records", ["user_id"], :name => "index_sc_records_on_user_id"

  create_table "sc_register_to_validates", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "active",                    :default => true
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.integer  "he_ct_module_privilage_id"
    t.integer  "recorder_id"
  end

  add_index "sc_register_to_validates", ["deactivated_by_user_id"], :name => "index_sc_register_to_validates_on_deactivated_by_user_id"
  add_index "sc_register_to_validates", ["he_ct_module_privilage_id"], :name => "index_sc_register_to_validates_on_he_ct_module_privilage_id"
  add_index "sc_register_to_validates", ["recorder_id"], :name => "index_sc_register_to_validates_on_recorder_id"
  add_index "sc_register_to_validates", ["registered_by_user_id"], :name => "index_sc_register_to_validates_on_registered_by_user_id"
  add_index "sc_register_to_validates", ["user_id"], :name => "index_sc_register_to_validates_on_user_id"

  create_table "sc_reported_records", :force => true do |t|
    t.integer  "sc_reporting_id"
    t.integer  "sc_record_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "sc_reported_records", ["sc_record_id"], :name => "index_sc_reported_records_on_sc_record_id"
  add_index "sc_reported_records", ["sc_reporting_id"], :name => "index_sc_reported_records_on_sc_reporting_id"

  create_table "sc_reportings", :force => true do |t|
    t.integer  "sc_process_id"
    t.datetime "reported_at"
    t.integer  "reported_by_user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.boolean  "partial_report",         :default => false
  end

  add_index "sc_reportings", ["deactivated_by_user_id"], :name => "index_sc_reportings_on_deactivated_by_user_id"
  add_index "sc_reportings", ["reported_by_user_id"], :name => "index_sc_reportings_on_reported_by_user_id"
  add_index "sc_reportings", ["sc_process_id"], :name => "index_sc_reportings_on_sc_process_id"

  create_table "sc_schedule_characteristics", :force => true do |t|
    t.boolean  "active_gui",        :default => false
    t.integer  "pos_gui"
    t.boolean  "active_condition",  :default => false
    t.integer  "pos_condition"
    t.integer  "characteristic_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  add_index "sc_schedule_characteristics", ["characteristic_id"], :name => "index_sc_schedule_characteristics_on_characteristic_id"

  create_table "sc_schedule_users", :force => true do |t|
    t.integer  "user_id"
    t.integer  "sc_schedule_id"
    t.boolean  "he_able",                :default => false
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.boolean  "active",                 :default => false
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "registered_manually",    :default => false
    t.boolean  "deactivated_manually",   :default => false
    t.datetime "active_since"
    t.datetime "active_end"
  end

  add_index "sc_schedule_users", ["deactivated_by_user_id"], :name => "index_sc_schedule_users_on_deactivated_by_user_id"
  add_index "sc_schedule_users", ["registered_by_user_id"], :name => "index_sc_schedule_users_on_registered_by_user_id"
  add_index "sc_schedule_users", ["sc_schedule_id"], :name => "index_sc_schedule_users_on_sc_schedule_id"
  add_index "sc_schedule_users", ["user_id"], :name => "index_sc_schedule_users_on_user_id"

  create_table "sc_schedules", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.datetime "registered_at"
    t.boolean  "active",         :default => true
    t.datetime "deactivated_at"
  end

  create_table "sc_user_processes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "sc_process_id"
    t.decimal  "he_total",                  :precision => 12, :scale => 4, :default => 0.0
    t.datetime "created_at",                                                                :null => false
    t.datetime "updated_at",                                                                :null => false
    t.decimal  "he_send_to_validation",     :precision => 12, :scale => 4, :default => 0.0
    t.decimal  "he_sent_to_validation",     :precision => 12, :scale => 4, :default => 0.0
    t.decimal  "he_approved",               :precision => 12, :scale => 4, :default => 0.0
    t.decimal  "he_rejected",               :precision => 12, :scale => 4, :default => 0.0
    t.decimal  "he_not_charged_to_payroll", :precision => 12, :scale => 4, :default => 0.0
  end

  add_index "sc_user_processes", ["sc_process_id"], :name => "index_sc_user_processes_on_sc_process_id"
  add_index "sc_user_processes", ["user_id"], :name => "index_sc_user_processes_on_user_id"

  create_table "sc_user_to_registers", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "active",                    :default => true
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.integer  "he_ct_module_privilage_id"
  end

  add_index "sc_user_to_registers", ["deactivated_by_user_id"], :name => "index_sc_user_to_registers_on_deactivated_by_user_id"
  add_index "sc_user_to_registers", ["he_ct_module_privilage_id"], :name => "index_sc_user_to_registers_on_he_ct_module_privilage_id"
  add_index "sc_user_to_registers", ["registered_by_user_id"], :name => "index_sc_user_to_registers_on_registered_by_user_id"
  add_index "sc_user_to_registers", ["user_id"], :name => "index_sc_user_to_registers_on_user_id"

  create_table "sc_user_to_validates", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "active",                    :default => true
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.integer  "he_ct_module_privilage_id"
  end

  add_index "sc_user_to_validates", ["deactivated_by_user_id"], :name => "index_sc_user_to_validates_on_deactivated_by_user_id"
  add_index "sc_user_to_validates", ["he_ct_module_privilage_id"], :name => "index_sc_user_to_validates_on_he_ct_module_privilage_id"
  add_index "sc_user_to_validates", ["registered_by_user_id"], :name => "index_sc_user_to_validates_on_registered_by_user_id"
  add_index "sc_user_to_validates", ["user_id"], :name => "index_sc_user_to_validates_on_user_id"

  create_table "scholarship_application_item_ranges", :force => true do |t|
    t.integer  "scholarship_application_item_id"
    t.integer  "scholarship_list_item_id"
    t.integer  "position"
    t.float    "min_val"
    t.integer  "min_sign"
    t.float    "max_val"
    t.integer  "max_sign"
    t.boolean  "is_default"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "scholarship_application_item_ranges", ["scholarship_application_item_id"], :name => "index_schol_app_item_ranges_on_schol_app_item_id"
  add_index "scholarship_application_item_ranges", ["scholarship_list_item_id"], :name => "index_schol_app_item_ranges_on_schol_list_item_id"

  create_table "scholarship_application_items", :force => true do |t|
    t.integer  "scholarship_item_group_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.boolean  "mandatory"
    t.integer  "item_type"
    t.integer  "scholarship_list_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.boolean  "reference_date_today"
    t.date     "reference_date_specific"
    t.integer  "reference_char_id"
  end

  add_index "scholarship_application_items", ["scholarship_item_group_id"], :name => "index_scholarship_application_items_on_scholarship_item_group_id"
  add_index "scholarship_application_items", ["scholarship_list_id"], :name => "index_scholarship_application_items_on_scholarship_list_id"

  create_table "scholarship_application_values", :force => true do |t|
    t.integer  "scholarship_application_id"
    t.integer  "scholarship_application_item_id"
    t.integer  "value_int"
    t.integer  "value_list_item_id"
    t.string   "value_string"
    t.text     "value_text"
    t.date     "value_date"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "scholarship_application_values", ["scholarship_application_id"], :name => "scholarship_app_values_application_id"
  add_index "scholarship_application_values", ["scholarship_application_item_id"], :name => "scholarship_app_values_application_item_id"
  add_index "scholarship_application_values", ["value_list_item_id"], :name => "scholarship_app_values_application_list_item_id"

  create_table "scholarship_applications", :force => true do |t|
    t.integer  "registered_by_user_id"
    t.datetime "registered_at"
    t.boolean  "done"
    t.datetime "done_at"
    t.boolean  "evaluated"
    t.integer  "evaluated_by_user_id"
    t.datetime "evaluated_at"
    t.boolean  "evaluation"
    t.text     "evaluation_comment"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "subject_user_id"
    t.integer  "scholarship_process_id"
  end

  add_index "scholarship_applications", ["evaluated_by_user_id"], :name => "index_scholarship_applications_on_evaluated_by_user_id"
  add_index "scholarship_applications", ["registered_by_user_id"], :name => "index_scholarship_applications_on_registered_by_user_id"
  add_index "scholarship_applications", ["scholarship_process_id"], :name => "index_scholarship_applications_on_scholarship_process_id"
  add_index "scholarship_applications", ["subject_user_id"], :name => "index_scholarship_applications_on_subject_user_id"

  create_table "scholarship_item_groups", :force => true do |t|
    t.integer  "scholarship_process_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "position"
  end

  add_index "scholarship_item_groups", ["scholarship_process_id"], :name => "index_scholarship_item_groups_on_scholarship_process_id"

  create_table "scholarship_list_items", :force => true do |t|
    t.integer  "scholarship_list_id"
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "position"
  end

  add_index "scholarship_list_items", ["scholarship_list_id"], :name => "index_scholarship_list_items_on_scholarship_list_id"

  create_table "scholarship_lists", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "scholarship_processes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.date     "app_from_date"
    t.date     "app_to_date"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.text     "app_form_description"
  end

  create_table "secu_cohades", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.boolean  "available_to_brc", :default => false
  end

  create_table "sel_applicant_attachments", :force => true do |t|
    t.integer  "sel_applicant_id"
    t.string   "description"
    t.string   "crypted_name"
    t.string   "original_filename"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "sel_applicant_attachments", ["registered_by_user_id"], :name => "index_sel_applicant_attachments_on_registered_by_user_id"
  add_index "sel_applicant_attachments", ["sel_applicant_id"], :name => "index_sel_applicant_attachments_on_sel_applicant_id"

  create_table "sel_applicant_records", :force => true do |t|
    t.integer  "sel_applicant_id"
    t.integer  "jm_candidate_answer_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "sel_applicant_records", ["jm_candidate_answer_id"], :name => "index_sel_applicant_records_on_jm_candidate_answer_id"
  add_index "sel_applicant_records", ["sel_applicant_id"], :name => "index_sel_applicant_records_on_sel_applicant_id"

  create_table "sel_applicant_specific_answers", :force => true do |t|
    t.integer  "sel_applicant_id"
    t.integer  "sel_apply_specific_question_id"
    t.text     "answer"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "attachment_code"
    t.string   "attachment_name"
  end

  add_index "sel_applicant_specific_answers", ["registered_by_user_id"], :name => "index_sel_applicant_specific_answers_on_registered_by_user_id"
  add_index "sel_applicant_specific_answers", ["sel_applicant_id"], :name => "index_sel_applicant_specific_answers_on_sel_applicant_id"
  add_index "sel_applicant_specific_answers", ["sel_apply_specific_question_id"], :name => "index_sel_applicant_specific_answers_on_sel_question_id"

  create_table "sel_applicants", :force => true do |t|
    t.integer  "sel_process_id"
    t.integer  "jm_candidate_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "selected",            :default => false
    t.datetime "selected_at"
    t.integer  "selected_by"
    t.boolean  "hired",               :default => false
    t.datetime "hired_at"
    t.integer  "hired_by"
    t.integer  "priority"
    t.text     "comment"
    t.integer  "comment_by_user_id"
    t.datetime "comment_at"
    t.boolean  "excluded",            :default => false
    t.datetime "excluded_at"
    t.integer  "excluded_by_user_id"
  end

  add_index "sel_applicants", ["comment_by_user_id"], :name => "index_sel_applicants_on_comment_by_user_id"
  add_index "sel_applicants", ["excluded_by_user_id"], :name => "index_sel_applicants_on_excluded_by_user_id"
  add_index "sel_applicants", ["jm_candidate_id"], :name => "index_sel_applicants_on_jm_candidate_id"
  add_index "sel_applicants", ["sel_process_id"], :name => "index_sel_applicants_on_sel_process_id"

  create_table "sel_apply_forms", :force => true do |t|
    t.integer  "sel_template_id"
    t.integer  "jm_characteristic_id"
    t.boolean  "mandatory"
    t.integer  "position"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "show_in_list_of_candidates", :default => true
  end

  add_index "sel_apply_forms", ["jm_characteristic_id"], :name => "index_sel_apply_forms_on_jm_characteristic_id"
  add_index "sel_apply_forms", ["sel_template_id"], :name => "index_sel_apply_forms_on_sel_template_id"

  create_table "sel_apply_specific_questions", :force => true do |t|
    t.integer  "sel_process_id"
    t.integer  "position"
    t.string   "question"
    t.text     "description"
    t.boolean  "mandatory"
    t.boolean  "show_in_list_of_candidates"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.boolean  "accepts_attachment",         :default => false
  end

  add_index "sel_apply_specific_questions", ["sel_process_id"], :name => "index_sel_apply_specific_questions_on_sel_process_id"

  create_table "sel_appointments", :force => true do |t|
    t.integer  "sel_process_step_id"
    t.text     "description"
    t.datetime "from_datetime"
    t.datetime "to_datetime"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "sel_appointments", ["registered_by_user_id"], :name => "index_sel_appointments_on_registered_by_user_id"
  add_index "sel_appointments", ["sel_process_step_id"], :name => "index_sel_appointments_on_sel_process_step_id"

  create_table "sel_attendants", :force => true do |t|
    t.integer  "sel_step_candidate_id"
    t.integer  "user_id"
    t.boolean  "role"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.boolean  "automatic_assign",      :default => false
  end

  add_index "sel_attendants", ["sel_step_candidate_id"], :name => "index_sel_attendants_on_sel_step_candidate_id"
  add_index "sel_attendants", ["user_id"], :name => "index_sel_attendants_on_user_id"

  create_table "sel_characteristics", :force => true do |t|
    t.integer  "sel_template_id"
    t.string   "name"
    t.text     "description"
    t.integer  "option_type"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.boolean  "configurable",    :default => false
  end

  add_index "sel_characteristics", ["sel_template_id"], :name => "index_sel_characteristics_on_sel_template_id"

  create_table "sel_claimants", :force => true do |t|
    t.integer  "sel_process_id"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "sel_claimants", ["sel_process_id"], :name => "index_sel_claimants_on_sel_process_id"
  add_index "sel_claimants", ["user_id"], :name => "index_sel_claimants_on_user_id"

  create_table "sel_evaluation_attachments", :force => true do |t|
    t.integer  "sel_step_candidate_id"
    t.string   "description"
    t.string   "crypted_name"
    t.string   "original_filename"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "copied_from_id"
  end

  add_index "sel_evaluation_attachments", ["copied_from_id"], :name => "index_sel_evaluation_attachments_on_copied_from_id"
  add_index "sel_evaluation_attachments", ["registered_by_user_id"], :name => "index_sel_evaluation_attachments_on_registered_by_user_id"
  add_index "sel_evaluation_attachments", ["sel_step_candidate_id"], :name => "index_sel_evaluation_attachments_on_sel_step_candidate_id"

  create_table "sel_flow_approver_chars", :force => true do |t|
    t.integer  "sel_flow_approver_id"
    t.integer  "characteristic_id"
    t.integer  "match_type"
    t.text     "match_value_text"
    t.datetime "match_value_date"
    t.string   "match_value_string"
    t.integer  "match_value_int"
    t.integer  "match_value_char_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.float    "match_value_float"
  end

  add_index "sel_flow_approver_chars", ["characteristic_id"], :name => "index_sel_flow_approver_chars_on_characteristic_id"
  add_index "sel_flow_approver_chars", ["match_value_char_id"], :name => "index_sel_flow_approver_chars_on_match_value_char_id"
  add_index "sel_flow_approver_chars", ["sel_flow_approver_id"], :name => "index_sel_flow_approver_chars_on_sel_flow_approver_id"

  create_table "sel_flow_approvers", :force => true do |t|
    t.integer  "sel_vacant_flow_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.boolean  "mandatory"
    t.float    "weight"
    t.boolean  "conditions_rejection"
    t.integer  "prerequisite_position"
    t.integer  "approver_type"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "sel_flow_approvers", ["sel_vacant_flow_id"], :name => "index_sel_flow_approvers_on_sel_vacant_flow_id"

  create_table "sel_flow_specific_approvers", :force => true do |t|
    t.integer  "sel_flow_approver_id"
    t.integer  "user_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "sel_flow_specific_approvers", ["sel_flow_approver_id"], :name => "index_sel_flow_specific_approvers_on_sel_flow_approver_id"
  add_index "sel_flow_specific_approvers", ["user_id"], :name => "index_sel_flow_specific_approvers_on_user_id"

  create_table "sel_notification_templates", :force => true do |t|
    t.integer  "event_id"
    t.string   "name"
    t.text     "description"
    t.boolean  "active",      :default => false
    t.string   "to"
    t.string   "cc"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "sel_notification_templates", ["event_id"], :name => "index_sel_notification_templates_on_event_id"

  create_table "sel_notifications", :force => true do |t|
    t.integer  "sel_notification_template_id"
    t.string   "to"
    t.string   "cc"
    t.string   "subject"
    t.text     "body"
    t.datetime "triggered_at"
    t.integer  "triggered_by_user_id"
    t.boolean  "sent",                         :default => false
    t.datetime "sent_at"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.integer  "event_id"
    t.integer  "jm_candidate_id"
    t.integer  "sel_process_id"
    t.integer  "sel_req_approver_id"
  end

  add_index "sel_notifications", ["event_id"], :name => "index_sel_notifications_on_event_id"
  add_index "sel_notifications", ["jm_candidate_id"], :name => "index_sel_notifications_on_jm_candidate_id"
  add_index "sel_notifications", ["sel_notification_template_id"], :name => "index_sel_notifications_on_sel_notification_template_id"
  add_index "sel_notifications", ["sel_process_id"], :name => "index_sel_notifications_on_sel_process_id"
  add_index "sel_notifications", ["sel_req_approver_id"], :name => "index_sel_notifications_on_sel_req_approver_id"
  add_index "sel_notifications", ["triggered_by_user_id"], :name => "index_sel_notifications_on_triggered_by_user_id"

  create_table "sel_options", :force => true do |t|
    t.integer  "sel_characteristic_id"
    t.string   "name"
    t.text     "description"
    t.string   "color"
    t.integer  "position"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "min_value"
    t.integer  "max_value"
    t.string   "min_sign"
    t.string   "max_sign"
  end

  add_index "sel_options", ["sel_characteristic_id"], :name => "index_sel_options_on_sel_characteristic_id"

  create_table "sel_owners", :force => true do |t|
    t.integer  "sel_process_id"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "sel_owners", ["sel_process_id"], :name => "index_sel_owners_on_sel_process_id"
  add_index "sel_owners", ["user_id"], :name => "index_sel_owners_on_user_id"

  create_table "sel_process_attendants", :force => true do |t|
    t.integer  "sel_process_step_id"
    t.integer  "user_id"
    t.boolean  "role"
    t.boolean  "automatic_assign",    :default => false
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "sel_process_attendants", ["sel_process_step_id"], :name => "index_sel_process_attendants_on_sel_process_step_id"
  add_index "sel_process_attendants", ["user_id"], :name => "index_sel_process_attendants_on_user_id"

  create_table "sel_process_characteristics", :force => true do |t|
    t.integer  "sel_characteristic_id"
    t.integer  "sel_process_step_id"
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "sel_process_characteristics", ["sel_characteristic_id"], :name => "index_sel_process_characteristics_on_sel_characteristic_id"
  add_index "sel_process_characteristics", ["sel_process_step_id"], :name => "index_sel_process_characteristics_on_sel_process_step_id"

  create_table "sel_process_steps", :force => true do |t|
    t.integer  "sel_process_id"
    t.integer  "sel_step_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "sel_process_steps", ["sel_process_id"], :name => "index_sel_process_steps_on_sel_process_id"
  add_index "sel_process_steps", ["sel_step_id"], :name => "index_sel_process_steps_on_sel_step_id"

  create_table "sel_processes", :force => true do |t|
    t.integer  "sel_template_id"
    t.string   "name"
    t.text     "description"
    t.integer  "qty_required"
    t.integer  "source"
    t.boolean  "apply_available"
    t.date     "from_date"
    t.date     "to_date"
    t.datetime "finished_at"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "file"
    t.string   "claimant_area"
    t.string   "code"
    t.integer  "sel_requirement_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.integer  "finished_by_user_id"
    t.boolean  "finished",              :default => false
    t.boolean  "traceable",             :default => true
  end

  add_index "sel_processes", ["finished_by_user_id"], :name => "index_sel_processes_on_finished_by_user_id"
  add_index "sel_processes", ["registered_by_user_id"], :name => "index_sel_processes_on_registered_by_user_id"
  add_index "sel_processes", ["sel_requirement_id"], :name => "index_sel_processes_on_sel_requirement_id"
  add_index "sel_processes", ["sel_template_id"], :name => "index_sel_processes_on_sel_template_id"

  create_table "sel_req_approver_evals", :force => true do |t|
    t.integer  "sel_req_approver_id"
    t.boolean  "evaluation"
    t.text     "comment"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.datetime "registered_at"
  end

  add_index "sel_req_approver_evals", ["sel_req_approver_id"], :name => "index_sel_req_approver_evals_on_sel_req_approver_id"

  create_table "sel_req_approvers", :force => true do |t|
    t.integer  "sel_req_process_id"
    t.string   "name"
    t.integer  "position"
    t.boolean  "mandatory"
    t.integer  "value"
    t.boolean  "conditions_rejection"
    t.integer  "prerequisite_position"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "user_id"
  end

  add_index "sel_req_approvers", ["sel_req_process_id"], :name => "index_sel_req_approvers_on_sel_req_process_id"
  add_index "sel_req_approvers", ["user_id"], :name => "index_sel_req_approvers_on_user_id"

  create_table "sel_req_items", :force => true do |t|
    t.integer  "sel_req_template_id"
    t.string   "name"
    t.text     "description"
    t.integer  "item_type"
    t.boolean  "mandatory",           :default => false
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "position"
    t.boolean  "select2_list",        :default => false
    t.boolean  "show_on_table",       :default => false
    t.boolean  "active",              :default => true
    t.integer  "characteristic_id"
  end

  add_index "sel_req_items", ["sel_req_template_id"], :name => "index_sel_req_items_on_sel_req_template_id"

  create_table "sel_req_logs", :force => true do |t|
    t.integer  "sel_requirement_id"
    t.integer  "log_state_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.text     "comment"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "sel_req_logs", ["log_state_id"], :name => "index_sel_req_logs_on_log_state_id"
  add_index "sel_req_logs", ["registered_by_user_id"], :name => "index_sel_req_logs_on_registered_by_user_id"
  add_index "sel_req_logs", ["sel_requirement_id"], :name => "index_sel_req_logs_on_sel_requirement_id"

  create_table "sel_req_options", :force => true do |t|
    t.integer  "sel_req_item_id"
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "sel_req_options", ["sel_req_item_id"], :name => "index_sel_req_options_on_sel_req_item_id"

  create_table "sel_req_processes", :force => true do |t|
    t.integer  "sel_requirement_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.boolean  "active"
    t.boolean  "started"
    t.datetime "started_at"
    t.integer  "started_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "sel_req_processes", ["registered_by_user_id"], :name => "index_sel_req_processes_on_registered_by_user_id"
  add_index "sel_req_processes", ["sel_requirement_id"], :name => "index_sel_req_processes_on_sel_requirement_id"
  add_index "sel_req_processes", ["started_by_user_id"], :name => "index_sel_req_processes_on_started_by_user_id"

  create_table "sel_req_templates", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active",      :default => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "sel_requirement_values", :force => true do |t|
    t.integer  "sel_requirement_id"
    t.integer  "sel_req_item_id"
    t.text     "value"
    t.integer  "sel_req_option_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "char_value_string"
    t.text     "char_value_text"
    t.date     "char_value_date"
    t.integer  "char_value_int"
    t.float    "char_value_float"
    t.integer  "char_value_id"
    t.integer  "char_value_jjob_id"
  end

  add_index "sel_requirement_values", ["sel_req_item_id"], :name => "index_sel_requirement_values_on_sel_req_item_id"
  add_index "sel_requirement_values", ["sel_req_option_id"], :name => "index_sel_requirement_values_on_sel_req_option_id"
  add_index "sel_requirement_values", ["sel_requirement_id"], :name => "index_sel_requirement_values_on_sel_requirement_id"

  create_table "sel_requirements", :force => true do |t|
    t.integer  "sel_req_template_id"
    t.integer  "registered_by_user_id"
    t.boolean  "done"
    t.integer  "done_by_user_id"
    t.datetime "done_at"
    t.boolean  "declined"
    t.text     "declined_comment"
    t.integer  "declined_by_user_id"
    t.datetime "declined_at"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.datetime "registered_at"
  end

  add_index "sel_requirements", ["sel_req_template_id"], :name => "index_sel_requirements_on_sel_req_template_id"

  create_table "sel_step_candidates", :force => true do |t|
    t.integer  "jm_candidate_id"
    t.integer  "sel_process_step_id"
    t.boolean  "exonerated",             :default => false
    t.boolean  "done",                   :default => false
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "exonerated_at"
    t.integer  "exonerated_by_user_id"
    t.datetime "done_at"
    t.integer  "done_by_user_id"
    t.integer  "sel_appointment_id"
    t.text     "exonerated_comment"
    t.datetime "final_appointment_date"
    t.integer  "copied_from_id"
  end

  add_index "sel_step_candidates", ["copied_from_id"], :name => "index_sel_step_candidates_on_copied_from_id"
  add_index "sel_step_candidates", ["done_by_user_id"], :name => "index_sel_step_candidates_on_done_by_user_id"
  add_index "sel_step_candidates", ["exonerated_by_user_id"], :name => "index_sel_step_candidates_on_exonerated_by_user_id"
  add_index "sel_step_candidates", ["jm_candidate_id"], :name => "index_sel_step_candidates_on_jm_candidate_id"
  add_index "sel_step_candidates", ["registered_by_user_id"], :name => "index_sel_step_candidates_on_registered_by_user_id"
  add_index "sel_step_candidates", ["sel_appointment_id"], :name => "index_sel_step_candidates_on_sel_appointment_id"
  add_index "sel_step_candidates", ["sel_process_step_id"], :name => "index_sel_step_candidates_on_sel_process_step_id"

  create_table "sel_step_characteristics", :force => true do |t|
    t.integer  "sel_step_id"
    t.integer  "sel_characteristic_id"
    t.integer  "position"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "sel_step_characteristics", ["sel_characteristic_id"], :name => "index_sel_step_characteristics_on_sel_characteristic_id"
  add_index "sel_step_characteristics", ["sel_step_id"], :name => "index_sel_step_characteristics_on_sel_step_id"

  create_table "sel_step_results", :force => true do |t|
    t.integer  "sel_step_characteristic_id"
    t.integer  "sel_option_id"
    t.integer  "sel_step_candidate_id"
    t.integer  "registered_by_user_id"
    t.integer  "non_discrete_result"
    t.text     "comment"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.datetime "registered_at"
    t.integer  "sel_process_characteristic_id"
  end

  add_index "sel_step_results", ["sel_option_id"], :name => "index_sel_step_results_on_sel_option_id"
  add_index "sel_step_results", ["sel_process_characteristic_id"], :name => "index_sel_step_results_on_sel_process_characteristic_id"
  add_index "sel_step_results", ["sel_step_candidate_id"], :name => "index_sel_step_results_on_sel_step_candidate_id"
  add_index "sel_step_results", ["sel_step_characteristic_id"], :name => "index_sel_step_results_on_sel_step_characteristic_id"

  create_table "sel_steps", :force => true do |t|
    t.integer  "sel_template_id"
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.integer  "step_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "sel_steps", ["sel_template_id"], :name => "index_sel_steps_on_sel_template_id"

  create_table "sel_template_attendants", :force => true do |t|
    t.integer  "sel_step_id"
    t.integer  "user_id"
    t.boolean  "role"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "sel_template_attendants", ["sel_step_id"], :name => "index_sel_template_attendants_on_sel_step_id"
  add_index "sel_template_attendants", ["user_id"], :name => "index_sel_template_attendants_on_user_id"

  create_table "sel_templates", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active",            :default => false
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.text     "apply_form_layout"
  end

  create_table "sel_vacant_flows", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.float    "threshold_for_approval"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "stored_images", :force => true do |t|
    t.string   "name"
    t.string   "crypted_name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "support_ticket_types", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "support_tickets", :force => true do |t|
    t.integer  "user_id"
    t.integer  "support_ticket_type_id"
    t.text     "descripcion"
    t.datetime "fecha_registro"
    t.boolean  "atendido",               :default => false
    t.text     "respuesta"
    t.datetime "fecha_respuesta"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "asunto"
    t.string   "curso"
    t.boolean  "envio_email",            :default => false
  end

  add_index "support_tickets", ["support_ticket_type_id"], :name => "index_support_tickets_on_support_ticket_type_id"
  add_index "support_tickets", ["user_id"], :name => "index_support_tickets_on_user_id"

  create_table "tracking_form_actions", :force => true do |t|
    t.integer  "tracking_form_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.string   "label"
    t.boolean  "fill_form"
    t.boolean  "able_to_attendant"
    t.boolean  "able_to_subject"
    t.boolean  "require_comment"
    t.string   "label_comment"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.text     "label_warning"
    t.string   "label_reject"
    t.string   "label_done"
    t.boolean  "can_be_rejected",   :default => true
    t.string   "color_label_done"
  end

  add_index "tracking_form_actions", ["tracking_form_id"], :name => "index_tracking_form_actions_on_tracking_form_id"

  create_table "tracking_form_answers", :force => true do |t|
    t.integer  "tracking_form_instance_id"
    t.integer  "position"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "tracking_form_question_id"
  end

  add_index "tracking_form_answers", ["tracking_form_instance_id"], :name => "index_tracking_form_answers_on_tracking_form_instance_id"
  add_index "tracking_form_answers", ["tracking_form_question_id"], :name => "index_tracking_form_answers_on_tracking_form_question_id"

  create_table "tracking_form_due_dates", :force => true do |t|
    t.integer  "tracking_form_answer_id"
    t.datetime "due_date"
    t.text     "comment"
    t.boolean  "finished",                :default => false
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.integer  "tracking_period_item_id"
  end

  add_index "tracking_form_due_dates", ["tracking_form_answer_id"], :name => "index_tracking_form_due_dates_on_tracking_form_answer_id"
  add_index "tracking_form_due_dates", ["tracking_period_item_id"], :name => "index_tracking_form_due_dates_on_tracking_period_item_id"

  create_table "tracking_form_groups", :force => true do |t|
    t.integer  "tracking_form_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "tracking_form_groups", ["tracking_form_id"], :name => "index_tracking_form_groups_on_tracking_form_id"

  create_table "tracking_form_instances", :force => true do |t|
    t.integer  "tracking_form_id"
    t.integer  "tracking_participant_id"
    t.boolean  "filled"
    t.boolean  "done"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "tracking_form_instances", ["tracking_form_id"], :name => "index_tracking_form_instances_on_tracking_form_id"
  add_index "tracking_form_instances", ["tracking_participant_id"], :name => "index_tracking_form_instances_on_tracking_participant_id"

  create_table "tracking_form_items", :force => true do |t|
    t.integer  "tracking_form_question_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.integer  "item_type"
    t.integer  "tracking_list_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "reportable",                :default => false
  end

  add_index "tracking_form_items", ["tracking_form_question_id"], :name => "index_tracking_form_items_on_tracking_form_question_id"
  add_index "tracking_form_items", ["tracking_list_id"], :name => "index_tracking_form_items_on_tracking_list_id"

  create_table "tracking_form_logs", :force => true do |t|
    t.integer  "tracking_form_instance_id"
    t.integer  "tracking_form_action_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.text     "comment"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "redefinition_group"
  end

  add_index "tracking_form_logs", ["registered_by_user_id"], :name => "index_tracking_form_logs_on_registered_by_user_id"
  add_index "tracking_form_logs", ["tracking_form_action_id"], :name => "index_tracking_form_logs_on_tracking_form_action_id"
  add_index "tracking_form_logs", ["tracking_form_instance_id"], :name => "index_tracking_form_logs_on_tracking_form_instance_id"

  create_table "tracking_form_notifications", :force => true do |t|
    t.integer  "action_type"
    t.integer  "tracking_form_action_id"
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.string   "to"
    t.string   "cc"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "tracking_form_notifications", ["tracking_form_action_id"], :name => "index_tracking_form_notifications_on_tracking_form_action_id"

  create_table "tracking_form_questions", :force => true do |t|
    t.integer  "tracking_form_group_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.integer  "min_qty_answers"
    t.integer  "max_qty_answers"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "traceable",               :default => false
    t.integer  "tracking_period_list_id"
  end

  add_index "tracking_form_questions", ["tracking_form_group_id"], :name => "index_tracking_form_questions_on_tracking_form_group_id"
  add_index "tracking_form_questions", ["tracking_period_list_id"], :name => "index_tracking_form_questions_on_tracking_period_list_id"

  create_table "tracking_form_rejects", :force => true do |t|
    t.integer  "tracking_form_log_id"
    t.text     "comment"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "tracking_form_rejects", ["registered_by_user_id"], :name => "index_tracking_form_rejects_on_registered_by_user_id"
  add_index "tracking_form_rejects", ["tracking_form_log_id"], :name => "index_tracking_form_rejects_on_tracking_form_log_id"

  create_table "tracking_form_states", :force => true do |t|
    t.integer  "tracking_form_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.string   "color"
    t.boolean  "finish_tracking"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "tracking_form_states", ["tracking_form_id"], :name => "index_tracking_form_states_on_tracking_form_id"

  create_table "tracking_form_values", :force => true do |t|
    t.integer  "tracking_form_answer_id"
    t.integer  "tracking_form_item_id"
    t.text     "value"
    t.integer  "tracking_list_item_id"
    t.string   "hashed_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "tracking_form_values", ["tracking_form_answer_id"], :name => "index_tracking_form_values_on_tracking_form_answer_id"
  add_index "tracking_form_values", ["tracking_form_item_id"], :name => "index_tracking_form_values_on_tracking_form_item_id"
  add_index "tracking_form_values", ["tracking_list_item_id"], :name => "index_tracking_form_values_on_tracking_list_item_id"

  create_table "tracking_forms", :force => true do |t|
    t.integer  "tracking_process_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.boolean  "traceable_by_attendant"
    t.boolean  "traceable_by_subject"
    t.boolean  "track_validation_by_counterpart"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.text     "label_unavailable"
  end

  add_index "tracking_forms", ["tracking_process_id"], :name => "index_tracking_forms_on_tracking_process_id"

  create_table "tracking_list_items", :force => true do |t|
    t.integer  "tracking_list_id"
    t.integer  "position"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.boolean  "active",                       :default => true
    t.integer  "tracking_list_item_father_id"
  end

  add_index "tracking_list_items", ["tracking_list_id"], :name => "index_tracking_list_items_on_tracking_list_id"
  add_index "tracking_list_items", ["tracking_list_item_father_id"], :name => "index_tracking_list_items_on_tracking_list_item_father_id"

  create_table "tracking_lists", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "autocomplete",            :default => false
    t.integer  "tracking_list_father_id"
  end

  add_index "tracking_lists", ["tracking_list_father_id"], :name => "index_tracking_lists_on_tracking_list_father_id"

  create_table "tracking_milestones", :force => true do |t|
    t.integer  "tracking_form_due_date_id"
    t.integer  "tracking_form_state_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "fulfilment_date"
    t.text     "comment"
    t.boolean  "validated",                 :default => false
    t.datetime "validated_at"
    t.integer  "validated_by_user_id"
    t.text     "validated_comment"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.text     "replied_comment"
    t.integer  "replied_by_user_id"
    t.datetime "replied_at"
  end

  add_index "tracking_milestones", ["registered_by_user_id"], :name => "index_tracking_milestones_on_registered_by_user_id"
  add_index "tracking_milestones", ["replied_by_user_id"], :name => "index_tracking_milestones_on_replied_by_user_id"
  add_index "tracking_milestones", ["tracking_form_due_date_id"], :name => "index_tracking_milestones_on_tracking_form_due_date_id"
  add_index "tracking_milestones", ["tracking_form_state_id"], :name => "index_tracking_milestones_on_tracking_form_state_id"
  add_index "tracking_milestones", ["validated_by_user_id"], :name => "index_tracking_milestones_on_validated_by_user_id"

  create_table "tracking_participants", :force => true do |t|
    t.integer  "tracking_process_id"
    t.integer  "attendant_user_id"
    t.integer  "subject_user_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "in_redefinition",     :default => false
  end

  add_index "tracking_participants", ["attendant_user_id"], :name => "index_tracking_participants_on_attendant_user_id"
  add_index "tracking_participants", ["subject_user_id"], :name => "index_tracking_participants_on_subject_user_id"
  add_index "tracking_participants", ["tracking_process_id"], :name => "index_tracking_participants_on_tracking_process_id"

  create_table "tracking_period_items", :force => true do |t|
    t.integer  "tracking_period_list_id"
    t.integer  "position"
    t.string   "name"
    t.datetime "from_date"
    t.datetime "to_date"
    t.boolean  "active"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "tracking_period_items", ["tracking_period_list_id"], :name => "index_tracking_period_items_on_tracking_period_list_id"

  create_table "tracking_period_lists", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tracking_process_chars", :force => true do |t|
    t.integer  "tracking_process_id"
    t.integer  "characteristic_id"
    t.integer  "position"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "tracking_process_chars", ["characteristic_id"], :name => "index_tracking_process_chars_on_characteristic_id"
  add_index "tracking_process_chars", ["tracking_process_id"], :name => "index_tracking_process_chars_on_tracking_process_id"

  create_table "tracking_processes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "from_date"
    t.datetime "to_date"
    t.boolean  "active"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "label_due_date"
    t.string   "label_comment_due_date"
    t.boolean  "ask_for_comment_due_date", :default => false
    t.boolean  "accepts_redefinitions",    :default => false
    t.string   "not_started_label"
    t.string   "not_started_color"
    t.string   "in_definition_label"
    t.string   "in_definition_color"
    t.string   "in_redefinition_label"
    t.string   "in_redefinition_color"
    t.string   "log_label"
    t.string   "mailer_name"
    t.string   "label_attendant"
    t.string   "label_subject"
    t.string   "label_attendant_plural"
    t.string   "label_subject_plural"
  end

  create_table "tracking_redefinitions", :force => true do |t|
    t.integer  "tracking_form_instance_id"
    t.integer  "required_by_user_id"
    t.datetime "required_at"
    t.text     "original_data"
    t.text     "final_data"
    t.boolean  "finished",                  :default => false
    t.datetime "finished_at"
    t.boolean  "applied",                   :default => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  add_index "tracking_redefinitions", ["required_by_user_id"], :name => "index_tracking_redefinitions_on_required_by_user_id"
  add_index "tracking_redefinitions", ["tracking_form_instance_id"], :name => "index_tracking_redefinitions_on_tracking_form_instance_id"

  create_table "tracking_state_notifications", :force => true do |t|
    t.integer  "tracking_form_state_id"
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.string   "to"
    t.string   "cc"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "tracking_state_notifications", ["tracking_form_state_id"], :name => "index_tracking_state_notifications_on_tracking_form_state_id"

  create_table "training_analysts", :force => true do |t|
    t.integer  "planning_process_company_unit_id"
    t.integer  "user_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "training_analysts", ["planning_process_company_unit_id"], :name => "index_training_analysts_on_planning_process_company_unit_id"
  add_index "training_analysts", ["user_id"], :name => "index_training_analysts_on_user_id"

  create_table "training_characteristic_values", :force => true do |t|
    t.string   "name"
    t.integer  "position"
    t.string   "value_string"
    t.integer  "training_characteristic_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "training_characteristic_values", ["training_characteristic_id"], :name => "tcv_tcid"

  create_table "training_characteristics", :force => true do |t|
    t.string   "name"
    t.string   "type"
    t.integer  "data_type_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "position"
    t.integer  "position_rep_2"
    t.integer  "position_rep_3"
    t.integer  "position_user_profile"
    t.boolean  "total_cost",            :default => false
    t.boolean  "course_instance",       :default => true
    t.integer  "position_filtro"
    t.integer  "instances_list"
  end

  create_table "training_impact_categories", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.float    "peso"
    t.integer  "orden"
  end

  create_table "training_impact_values", :force => true do |t|
    t.integer  "valor"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "nombre"
  end

  create_table "training_modes", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "training_programs", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "training_providers", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "training_types", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "color"
    t.boolean  "active",     :default => true
  end

  create_table "tutorials", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.string   "crypted_name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "uc_characteristics", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "position"
  end

  create_table "um_characteristics", :force => true do |t|
    t.integer  "characteristic_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "um_characteristics", ["characteristic_id"], :name => "index_um_characteristics_on_characteristic_id"

  create_table "um_custom_report_characteristics", :force => true do |t|
    t.integer  "um_custom_report_id"
    t.integer  "um_characteristic_id"
    t.integer  "position"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "um_custom_report_characteristics", ["um_characteristic_id"], :name => "index_um_custom_report_characteristics_on_um_characteristic_id"
  add_index "um_custom_report_characteristics", ["um_custom_report_id"], :name => "index_um_custom_report_characteristics_on_um_custom_report_id"

  create_table "um_custom_reports", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "units", :force => true do |t|
    t.integer  "numero"
    t.string   "nombre"
    t.text     "descripcion"
    t.integer  "course_id"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "libre",                    :default => true
    t.boolean  "activa",                   :default => true
    t.integer  "antes_de_unit_id"
    t.integer  "despues_de_unit_id"
    t.integer  "antes_de_evaluation_id"
    t.integer  "despues_de_evaluation_id"
    t.integer  "antes_de_poll_id"
    t.integer  "despues_de_poll_id"
    t.integer  "master_unit_id"
    t.integer  "orden",                    :default => 1
    t.boolean  "finaliza_automaticamente", :default => true
    t.boolean  "virtual",                  :default => true
  end

  add_index "units", ["course_id"], :name => "index_units_on_course_id"
  add_index "units", ["orden"], :name => "index_units_on_orden"

  create_table "user_characteristic_records", :force => true do |t|
    t.integer  "user_id"
    t.integer  "characteristic_id"
    t.text     "value"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "value_string"
    t.text     "value_text"
    t.date     "value_date"
    t.integer  "value_int"
    t.float    "value_float"
    t.string   "value_file"
    t.integer  "characteristic_value_id"
    t.integer  "created_by_user_id"
    t.integer  "created_by_admin_id"
    t.integer  "finished_by_user_id"
    t.integer  "finished_by_admin_id"
    t.string   "value_ct_image_url"
    t.binary   "value_encrypted"
  end

  add_index "user_characteristic_records", ["characteristic_id"], :name => "index_user_characteristic_records_on_characteristic_id"
  add_index "user_characteristic_records", ["characteristic_value_id"], :name => "index_user_characteristic_records_on_characteristic_value_id"
  add_index "user_characteristic_records", ["created_by_admin_id"], :name => "index_user_characteristic_records_on_created_by_admin_id"
  add_index "user_characteristic_records", ["created_by_user_id"], :name => "index_user_characteristic_records_on_created_by_user_id"
  add_index "user_characteristic_records", ["finished_by_admin_id"], :name => "index_user_characteristic_records_on_finished_by_admin_id"
  add_index "user_characteristic_records", ["finished_by_user_id"], :name => "index_user_characteristic_records_on_finished_by_user_id"
  add_index "user_characteristic_records", ["user_id"], :name => "index_user_characteristic_records_on_user_id"

  create_table "user_characteristics", :force => true do |t|
    t.integer  "user_id"
    t.integer  "characteristic_id"
    t.text     "valor"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.string   "value_string"
    t.text     "value_text"
    t.date     "value_date"
    t.integer  "value_int"
    t.float    "value_float"
    t.integer  "characteristic_value_id"
    t.string   "value_file"
    t.string   "value_ct_image_url"
    t.integer  "register_user_characteristic_id"
    t.binary   "value_encrypted"
  end

  add_index "user_characteristics", ["characteristic_id"], :name => "index_user_characteristics_on_characteristic_id"
  add_index "user_characteristics", ["characteristic_value_id"], :name => "index_user_characteristics_on_characteristic_value_id"
  add_index "user_characteristics", ["register_user_characteristic_id"], :name => "index_user_characteristics_on_register_user_characteristic_id"
  add_index "user_characteristics", ["user_id"], :name => "index_user_characteristics_on_user_id"

  create_table "user_course_evaluation_questions", :force => true do |t|
    t.integer  "numero"
    t.integer  "user_course_evaluation_id"
    t.integer  "master_evaluation_id"
    t.integer  "master_evaluation_question_id"
    t.boolean  "respondida"
    t.boolean  "correcta"
    t.string   "alternativas"
    t.datetime "fecha_respuesta"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.datetime "fecha_inicio"
  end

  add_index "user_course_evaluation_questions", ["master_evaluation_id"], :name => "u_c_e_q_meval_id"
  add_index "user_course_evaluation_questions", ["master_evaluation_question_id"], :name => "u_c_e_q_mevalq_id"
  add_index "user_course_evaluation_questions", ["user_course_evaluation_id"], :name => "u_c_e_q_uceval_id"

  create_table "user_course_evaluations", :force => true do |t|
    t.integer  "user_course_id"
    t.integer  "evaluation_id"
    t.datetime "inicio"
    t.datetime "fin"
    t.boolean  "finalizada"
    t.datetime "ultimo_acceso"
    t.boolean  "aprobada"
    t.float    "nota"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "paused",         :default => false
    t.datetime "from_date"
    t.datetime "to_date"
  end

  add_index "user_course_evaluations", ["evaluation_id"], :name => "index_user_course_evaluations_on_evaluation_id"
  add_index "user_course_evaluations", ["user_course_id"], :name => "index_user_course_evaluations_on_user_course_id"

  create_table "user_course_poll_questions", :force => true do |t|
    t.integer  "user_course_poll_id"
    t.integer  "master_poll_id"
    t.integer  "master_poll_question_id"
    t.boolean  "respondida"
    t.string   "alternativas"
    t.datetime "fecha_respuesta"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "numero"
    t.text     "texto"
  end

  add_index "user_course_poll_questions", ["master_poll_id"], :name => "index_user_course_poll_questions_on_master_poll_id"
  add_index "user_course_poll_questions", ["master_poll_question_id"], :name => "index_user_course_poll_questions_on_master_poll_question_id"
  add_index "user_course_poll_questions", ["user_course_poll_id"], :name => "index_user_course_poll_questions_on_user_course_poll_id"

  create_table "user_course_polls", :force => true do |t|
    t.integer  "user_course_id"
    t.integer  "poll_id"
    t.datetime "inicio"
    t.boolean  "finalizada"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.datetime "fin"
    t.datetime "ultimo_acceso"
    t.datetime "from_date"
    t.datetime "to_date"
  end

  add_index "user_course_polls", ["poll_id"], :name => "index_user_course_polls_on_poll_id"
  add_index "user_course_polls", ["user_course_id"], :name => "index_user_course_polls_on_user_course_id"

  create_table "user_course_uc_characteristics", :force => true do |t|
    t.integer  "user_course_id"
    t.integer  "uc_characteristic_id"
    t.text     "value"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "user_course_uc_characteristics", ["uc_characteristic_id"], :name => "index_user_course_uc_characteristics_on_uc_characteristic_id"
  add_index "user_course_uc_characteristics", ["user_course_id"], :name => "index_user_course_uc_characteristics_on_user_course_id"

  create_table "user_course_units", :force => true do |t|
    t.integer  "user_course_id"
    t.integer  "unit_id"
    t.datetime "inicio"
    t.datetime "fin"
    t.boolean  "finalizada",     :default => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.datetime "ultimo_acceso"
    t.integer  "milestone",      :default => 0
    t.datetime "from_date"
    t.datetime "to_date"
  end

  add_index "user_course_units", ["unit_id"], :name => "index_user_course_units_on_unit_id"
  add_index "user_course_units", ["user_course_id"], :name => "index_user_course_units_on_user_course_id"

  create_table "user_courses", :force => true do |t|
    t.integer  "enrollment_id"
    t.integer  "program_course_id"
    t.datetime "desde"
    t.datetime "hasta"
    t.boolean  "limite_tiempo"
    t.datetime "inicio"
    t.datetime "fin"
    t.integer  "numero_oportunidad"
    t.boolean  "iniciado",                   :default => false
    t.boolean  "finalizado",                 :default => false
    t.boolean  "aprobado",                   :default => false
    t.float    "nota"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.integer  "course_id"
    t.float    "porcentaje_avance",          :default => 0.0
    t.integer  "dncp_id"
    t.integer  "grupo"
    t.boolean  "asistencia"
    t.boolean  "aplica_sence",               :default => false
    t.boolean  "anulado",                    :default => false
    t.integer  "grupo_f"
    t.integer  "program_course_instance_id"
    t.integer  "user_id"
    t.float    "asistencia_unidades",        :default => 0.0
    t.boolean  "enroll_np",                  :default => false
  end

  add_index "user_courses", ["course_id"], :name => "index_user_courses_on_course_id"
  add_index "user_courses", ["enrollment_id"], :name => "index_user_courses_on_enrollment_id"
  add_index "user_courses", ["program_course_id"], :name => "index_user_courses_on_program_course_id"
  add_index "user_courses", ["program_course_instance_id"], :name => "index_user_courses_on_program_course_instance_id"
  add_index "user_courses", ["user_id"], :name => "index_user_courses_on_user_id"

  create_table "user_ct_module_privileges", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.boolean  "sel_make_requirement",                 :default => false
    t.boolean  "mpi_view_users",                       :default => false
    t.boolean  "mpi_view_users_data",                  :default => false
    t.boolean  "mpi_view_users_training_history",      :default => false
    t.boolean  "mpi_view_users_pe_history",            :default => false
    t.boolean  "mpi_view_users_informative_documents", :default => false
    t.boolean  "um_manage_users",                      :default => false
    t.boolean  "um_manage_users_data",                 :default => false
    t.boolean  "um_manage_users_password",             :default => false
    t.boolean  "sel_approve_requirements",             :default => false
    t.boolean  "um_manage_users_email",                :default => false
    t.boolean  "um_query_reports",                     :default => false
    t.boolean  "lms_define",                           :default => false
    t.boolean  "lms_enroll",                           :default => false
    t.boolean  "lms_manage_enroll",                    :default => false
    t.boolean  "lms_reports",                          :default => false
    t.boolean  "um_create_users",                      :default => false
    t.boolean  "um_delete_users",                      :default => false
    t.boolean  "um_edit_users",                        :default => false
    t.boolean  "um_manage_users_photo",                :default => false
  end

  add_index "user_ct_module_privileges", ["user_id"], :name => "index_user_ct_module_privileges_on_user_id"

  create_table "user_ct_module_privileges_um_custom_reports", :force => true do |t|
    t.integer  "user_ct_module_privileges_id"
    t.integer  "um_custom_report_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "user_ct_module_privileges_um_custom_reports", ["um_custom_report_id"], :name => "index_user_ct_module_privileges_um_custom_reports_report_id"
  add_index "user_ct_module_privileges_um_custom_reports", ["user_ct_module_privileges_id"], :name => "index_user_ct_module_privileges_um_custom_reports_privileges_id"

  create_table "user_delete_reasons", :force => true do |t|
    t.integer  "position"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "user_delete_types", :force => true do |t|
    t.integer  "position"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "user_j_jobs", :force => true do |t|
    t.integer  "user_id"
    t.integer  "j_job_id"
    t.date     "from_date"
    t.date     "to_date"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_j_jobs", ["j_job_id"], :name => "index_user_j_jobs_on_j_job_id"
  add_index "user_j_jobs", ["user_id"], :name => "index_user_j_jobs_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "codigo"
    t.string   "email"
    t.string   "password_digest"
    t.string   "nombre"
    t.string   "apellidos"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "codigo_rec_pass"
    t.string   "fecha_rec_pass"
    t.boolean  "jefe_capacitacion",       :default => false
    t.string   "foto"
    t.boolean  "activo",                  :default => true
    t.boolean  "requiere_cambio_pass",    :default => false
    t.boolean  "jefe_biblioteca",         :default => false
    t.integer  "node_id"
    t.integer  "j_job_id"
    t.boolean  "perpetual_active"
    t.boolean  "employee",                :default => true
    t.text     "private_key"
    t.text     "public_key"
    t.string   "active_directory_id"
    t.boolean  "he_able",                 :default => false
    t.integer  "j_cost_center_id"
    t.date     "delete_date"
    t.integer  "user_delete_reason_id"
    t.text     "delete_comment"
    t.date     "from_date"
    t.string   "last_name_1"
    t.string   "last_name_2"
    t.integer  "user_delete_type_id"
    t.boolean  "show_azure_landing_page", :default => true
    t.boolean  "flexible_sc",             :default => false
    t.boolean  "secu_external"
  end

  add_index "users", ["codigo"], :name => "index_users_on_codigo", :unique => true
  add_index "users", ["j_cost_center_id"], :name => "index_users_on_j_cost_center_id"
  add_index "users", ["node_id"], :name => "index_users_on_node_id"
  add_index "users", ["user_delete_reason_id"], :name => "index_users_on_user_delete_reason_id"
  add_index "users", ["user_delete_type_id"], :name => "index_users_on_user_delete_type_id"

  create_table "vac_accumulateds", :force => true do |t|
    t.integer  "user_id"
    t.integer  "bp_general_rules_vacation_id"
    t.decimal  "accumulated_days",                :precision => 25, :scale => 20
    t.decimal  "accumulated_real_days",           :precision => 25, :scale => 20
    t.integer  "accumulated_progressive_days"
    t.boolean  "active",                                                          :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                                                         :null => false
    t.datetime "updated_at",                                                                         :null => false
    t.text     "deactivated_description"
    t.boolean  "registered_manually"
    t.text     "registered_manually_description"
    t.integer  "vac_request_id"
    t.integer  "vac_record_id"
    t.date     "up_to_date"
    t.integer  "next_vac_accumulated_id"
    t.integer  "prev_vac_accumulated_id"
    t.boolean  "auto_update_legal",                                               :default => false
    t.boolean  "auto_update_prog",                                                :default => false
  end

  add_index "vac_accumulateds", ["bp_general_rules_vacation_id"], :name => "index_vac_accumulateds_on_bp_general_rules_vacation_id"
  add_index "vac_accumulateds", ["deactivated_by_user_id"], :name => "index_vac_accumulateds_on_deactivated_by_user_id"
  add_index "vac_accumulateds", ["next_vac_accumulated_id"], :name => "vac_accum_next_id"
  add_index "vac_accumulateds", ["prev_vac_accumulated_id"], :name => "vac_accum_prev_id"
  add_index "vac_accumulateds", ["registered_by_user_id"], :name => "index_vac_accumulateds_on_registered_by_user_id"
  add_index "vac_accumulateds", ["user_id"], :name => "index_vac_accumulateds_on_user_id"
  add_index "vac_accumulateds", ["vac_record_id"], :name => "index_vac_accumulateds_on_vac_record_id"
  add_index "vac_accumulateds", ["vac_request_id"], :name => "index_vac_accumulateds_on_vac_request_id"

  create_table "vac_approvers", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "active",                 :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "vac_approvers", ["deactivated_by_user_id"], :name => "vac_approvers_deact_user_id"
  add_index "vac_approvers", ["registered_by_user_id"], :name => "vac_approvers_regis_user_id"
  add_index "vac_approvers", ["user_id"], :name => "index_vac_approvers_on_user_id"

  create_table "vac_records", :force => true do |t|
    t.integer  "vac_request_id"
    t.date     "begin"
    t.date     "end"
    t.integer  "days"
    t.integer  "days_progressive"
    t.boolean  "active",                          :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.text     "deactivated_description"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.boolean  "registered_manually",             :default => false
    t.text     "registered_manually_description"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.integer  "user_id"
  end

  add_index "vac_records", ["deactivated_by_user_id"], :name => "vac_record_deact_user_id"
  add_index "vac_records", ["registered_by_user_id"], :name => "vac_record_regis_user_id"
  add_index "vac_records", ["user_id"], :name => "index_vac_records_on_user_id"
  add_index "vac_records", ["vac_request_id"], :name => "index_vac_records_on_vac_request_id"

  create_table "vac_requests", :force => true do |t|
    t.integer  "user_id"
    t.integer  "vac_requests_period_id"
    t.date     "begin"
    t.date     "end"
    t.integer  "days",                                                            :default => 0
    t.integer  "days_progressive",                                                :default => 0
    t.integer  "status",                                                          :default => 0
    t.boolean  "active",                                                          :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.integer  "prev_request_id"
    t.datetime "registered_at"
    t.datetime "created_at",                                                                         :null => false
    t.datetime "updated_at",                                                                         :null => false
    t.integer  "registered_by_user_id"
    t.integer  "status_changed_by_user_id"
    t.text     "status_description"
    t.boolean  "registered_manually",                                             :default => false
    t.text     "registered_manually_description"
    t.integer  "next_request_id"
    t.decimal  "days_manually",                   :precision => 25, :scale => 20
    t.date     "end_vacations"
    t.boolean  "historical",                                                      :default => false
    t.boolean  "auto_update_legal",                                               :default => false
    t.boolean  "auto_update_prog",                                                :default => false
    t.integer  "bp_warning_exception_id"
  end

  add_index "vac_requests", ["bp_warning_exception_id"], :name => "index_vac_requests_on_bp_warning_exception_id"
  add_index "vac_requests", ["deactivated_by_user_id"], :name => "vac_request_deact_user_id"
  add_index "vac_requests", ["next_request_id"], :name => "index_vac_requests_on_next_request_id"
  add_index "vac_requests", ["prev_request_id"], :name => "index_vac_requests_on_prev_request_id"
  add_index "vac_requests", ["registered_by_user_id"], :name => "vac_request_regis_user_id"
  add_index "vac_requests", ["status_changed_by_user_id"], :name => "vac_request_status_chan_user_id"
  add_index "vac_requests", ["user_id"], :name => "index_vac_requests_on_user_id"
  add_index "vac_requests", ["vac_requests_period_id"], :name => "index_vac_requests_on_vac_requests_period_id"

  create_table "vac_requests_periods", :force => true do |t|
    t.datetime "request_since"
    t.datetime "request_until"
    t.datetime "approve_since"
    t.datetime "approve_until"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "vac_requests_periods", ["registered_by_user_id"], :name => "vac_re_periods_regis_user_id"

  create_table "vac_rule_records", :force => true do |t|
    t.integer  "vac_request_id"
    t.integer  "bp_condition_vacation_id"
    t.date     "days_begin"
    t.date     "days_end"
    t.date     "days_real_end"
    t.boolean  "paid"
    t.datetime "paid_at"
    t.integer  "paid_by_user_id"
    t.boolean  "refund_pending",               :default => false
    t.datetime "refund_requested_at"
    t.integer  "refund_requested_by_user_id"
    t.datetime "refund_done_at"
    t.integer  "refund_done_by_user_id"
    t.boolean  "active",                       :default => true
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.boolean  "available",                    :default => true
    t.datetime "available_changed_at"
    t.integer  "available_changed_by_user_id"
    t.text     "paid_description"
  end

  add_index "vac_rule_records", ["available_changed_by_user_id"], :name => "vac_rul_rec_avai_user_id"
  add_index "vac_rule_records", ["bp_condition_vacation_id"], :name => "index_vac_rule_records_on_bp_condition_vacation_id"
  add_index "vac_rule_records", ["deactivated_by_user_id"], :name => "vac_rule_recor_deact_user_id"
  add_index "vac_rule_records", ["paid_by_user_id"], :name => "vac_rule_money_paid_user_id"
  add_index "vac_rule_records", ["refund_done_by_user_id"], :name => "vac_refund_done_user_id"
  add_index "vac_rule_records", ["refund_requested_by_user_id"], :name => "vac_refund_req_user_id"
  add_index "vac_rule_records", ["registered_by_user_id"], :name => "vac_rule_recor_regis_user_id"
  add_index "vac_rule_records", ["vac_request_id"], :name => "index_vac_rule_records_on_vac_request_id"

  create_table "vac_user_to_approves", :force => true do |t|
    t.integer  "user_id"
    t.integer  "vac_approver_id"
    t.boolean  "active",                 :default => false
    t.datetime "deactivated_at"
    t.integer  "deactivated_by_user_id"
    t.datetime "registered_at"
    t.integer  "registered_by_user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "vac_user_to_approves", ["deactivated_by_user_id"], :name => "vac_user_approvee_deact_user_id"
  add_index "vac_user_to_approves", ["registered_by_user_id"], :name => "vac_user_approvee_regis_user_id"
  add_index "vac_user_to_approves", ["user_id"], :name => "index_vac_user_to_approves_on_user_id"
  add_index "vac_user_to_approves", ["vac_approver_id"], :name => "index_vac_user_to_approves_on_vac_approver_id"

end
