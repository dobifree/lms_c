class AddFromJobsToHrEvaluationTypes < ActiveRecord::Migration
  def change
    add_column :hr_evaluation_types, :from_jobs, :boolean
  end
end
