class AddJPayBandIdToJJobs < ActiveRecord::Migration
  def change
    add_column :j_jobs, :j_pay_band_id, :integer
    add_index :j_jobs, :j_pay_band_id
  end
end
