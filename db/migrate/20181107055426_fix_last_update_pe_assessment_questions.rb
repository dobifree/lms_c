class FixLastUpdatePeAssessmentQuestions < ActiveRecord::Migration
  def change
    execute "UPDATE pe_assessment_questions SET last_update = DATE_SUB(updated_at, INTERVAL 3 HOUR) WHERE last_update IS NULL"
  end
end
