class AddMinPercentageToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :min_percentage, :float, default: 0
  end
end
