class AddStepValidationNoMessageToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_validation_no_message, :string, default: 'No estoy de acuerdo con la evaluación realizada'
  end
end
