class CreateUserCtModulePrivilegesUmCustomReports < ActiveRecord::Migration
  def change
    create_table :user_ct_module_privileges_um_custom_reports do |t|
      t.references :user_ct_module_privileges
      t.references :um_custom_report

      t.timestamps
    end
    add_index :user_ct_module_privileges_um_custom_reports, :user_ct_module_privileges_id, name: 'index_user_ct_module_privileges_um_custom_reports_privileges_id'
    add_index :user_ct_module_privileges_um_custom_reports, :um_custom_report_id, name: 'index_user_ct_module_privileges_um_custom_reports_report_id'
  end
end
