class AddExcelIndicatorsToBenCellphoneProcess < ActiveRecord::Migration
  def change
    add_column :ben_cellphone_processes, :generated_registered_not_manually_excel_at, :datetime
    add_column :ben_cellphone_processes, :generated_registered_manually_excel_at, :datetime
    add_column :ben_cellphone_processes, :generated_registered_not_manually_excel_by_user_id, :integer
    add_column :ben_cellphone_processes, :generated_registered_manually_excel_by_user_id, :integer

    add_index :ben_cellphone_processes, :generated_registered_not_manually_excel_by_user_id, :name => 'ben_cellphone_processes_grnm_id'
    add_index :ben_cellphone_processes, :generated_registered_manually_excel_by_user_id, :name => 'ben_user_cellphone_grm_id'
  end
end