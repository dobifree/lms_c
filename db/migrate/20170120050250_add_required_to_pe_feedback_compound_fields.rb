class AddRequiredToPeFeedbackCompoundFields < ActiveRecord::Migration
  def change
    add_column :pe_feedback_compound_fields, :required, :boolean, default: true
  end
end
