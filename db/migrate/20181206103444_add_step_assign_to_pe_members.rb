class AddStepAssignToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :step_assign, :boolean, default: false
  end
end
