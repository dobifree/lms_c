class CreateKpiIndicators < ActiveRecord::Migration
  def change
    create_table :kpi_indicators do |t|
      t.integer :position
      t.string :name
      t.float :weight
      t.references :kpi_set
      t.float :goal
      t.string :unit
      t.integer :indicator_type

      t.timestamps
    end
    add_index :kpi_indicators, :kpi_set_id
  end
end
