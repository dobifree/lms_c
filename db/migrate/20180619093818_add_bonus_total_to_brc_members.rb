class AddBonusTotalToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :bonus_total, :decimal, :precision => 20, :scale => 4, default: 0
  end
end
