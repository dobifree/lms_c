class RenameColumnLicenceToLicenseLicRequest < ActiveRecord::Migration
def change
  rename_column :lic_requests, :bp_licence_id, :bp_license_id
end
end
