class AddEvaluacionColaborativaToHrEvaluationTypeElements < ActiveRecord::Migration
  def change
    add_column :hr_evaluation_type_elements, :evaluacion_colaborativa, :boolean
  end
end
