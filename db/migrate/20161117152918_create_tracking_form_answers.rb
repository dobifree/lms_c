class CreateTrackingFormAnswers < ActiveRecord::Migration
  def change
    create_table :tracking_form_answers do |t|
      t.references :tracking_form_instance
      t.integer :position

      t.timestamps
    end
    add_index :tracking_form_answers, :tracking_form_instance_id
  end
end
