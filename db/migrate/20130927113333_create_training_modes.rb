class CreateTrainingModes < ActiveRecord::Migration
  def change
    create_table :training_modes do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
