class AddMinimumAssistanceToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :minimum_assistance, :integer, default: 0
  end
end
