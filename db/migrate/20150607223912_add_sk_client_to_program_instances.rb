class AddSkClientToProgramInstances < ActiveRecord::Migration
  def change
    add_column :program_instances, :sk_client, :string
  end
end
