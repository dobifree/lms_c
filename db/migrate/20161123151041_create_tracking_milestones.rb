class CreateTrackingMilestones < ActiveRecord::Migration
  def change
    create_table :tracking_milestones do |t|
      t.references :tracking_form_due_date
      t.references :tracking_form_state
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.datetime :fulfilment_date
      t.text :comment
      t.boolean :validated
      t.datetime :validated_at
      t.integer :validated_by_user_id
      t.text :validated_comment

      t.timestamps
    end
    add_index :tracking_milestones, :tracking_form_due_date_id
    add_index :tracking_milestones, :tracking_form_state_id
    add_index :tracking_milestones, :registered_by_user_id
    add_index :tracking_milestones, :validated_by_user_id
  end
end
