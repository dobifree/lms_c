class AddHasStepFeedbackAcceptedToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_feedback_accepted, :boolean, default: false
  end
end
