class ChangeAssessmentValuesToDecimals < ActiveRecord::Migration
  def change
    change_column :pe_questions, :indicator_goal, :decimal, :precision => 30, :scale => 6
    change_column :pe_question_ranks, :goal, :decimal, :precision => 30, :scale => 6
    change_column :pe_question_rank_models, :goal, :decimal, :precision => 30, :scale => 6
    change_column :pe_assessment_questions, :points, :decimal, :precision => 30, :scale => 6
    change_column :pe_assessment_final_questions, :points, :decimal, :precision => 30, :scale => 6
    change_column :pe_assessment_questions, :indicator_achievement, :decimal, :precision => 30, :scale => 6
  end
end
