class ChangeDefaultsAliasCompanies < ActiveRecord::Migration
  def connection
    @connection = Company.connection
  end

  def change
    change_column :companies, :alias_username, :string, :default => 'Código'
    change_column :companies, :alias_user, :string, :default => 'Usuario,Usuarios'
    @connection.execute "update companies set alias_username = 'Código' where alias_username is null"
    @connection.execute "update companies set alias_user = 'Usuario,Usuarios' where alias_user is null"
  end
end
