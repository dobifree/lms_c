class AddAntesDePollIdToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :antes_de_poll_id, :integer
  end
end
