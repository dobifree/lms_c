class AddActiveReportToBenCharacteristic < ActiveRecord::Migration
  def change
    add_column :ben_cellphone_characteristics, :active_reporting, :boolean, default: false
  end
end
