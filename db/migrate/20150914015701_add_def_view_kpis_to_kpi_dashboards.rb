class AddDefViewKpisToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :def_view_kpis, :boolean, default: true
  end
end
