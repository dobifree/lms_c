class AddDespuesDePollIdToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :despues_de_poll_id, :integer
  end
end
