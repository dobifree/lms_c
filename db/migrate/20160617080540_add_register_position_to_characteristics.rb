class AddRegisterPositionToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :register_position, :integer
  end
end
