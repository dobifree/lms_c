class AddShowSetResultsToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :show_set_results, :boolean
  end
end
