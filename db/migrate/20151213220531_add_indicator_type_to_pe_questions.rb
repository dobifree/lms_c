class AddIndicatorTypeToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :indicator_type, :integer, default: 0
  end
end
