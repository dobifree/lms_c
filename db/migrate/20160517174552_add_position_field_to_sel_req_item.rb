class AddPositionFieldToSelReqItem < ActiveRecord::Migration
  def change
    add_column :sel_req_items, :position, :integer
  end
end
