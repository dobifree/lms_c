class AddCanBeSelectedAsEvaluatedToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :can_be_selected_as_evaluated, :boolean, default: false
  end
end
