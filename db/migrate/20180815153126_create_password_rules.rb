class CreatePasswordRules < ActiveRecord::Migration
  def change
    create_table :password_rules do |t|
      t.references :password_policy
      t.string :group_name
      t.integer :priority
      t.string :name
      t.text :description
      t.string :regular_expression
      t.boolean :mandatory
      t.boolean :possitive_match
      t.string :error_match_message
      t.integer :strongness

      t.timestamps
    end
    add_index :password_rules, :password_policy_id
  end
end
