class AddRepCalibrationStatusToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_calibration_status, :boolean, default: false
  end
end
