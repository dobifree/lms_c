class RemoveSkClientFromProgramCourseInstances < ActiveRecord::Migration
  def up
    remove_column :program_course_instances, :sk_client
  end

  def down
    add_column :program_course_instances, :sk_client, :string
  end
end
