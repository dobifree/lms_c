class AddConsultableToProgramCourses < ActiveRecord::Migration
  def change
    add_column :program_courses, :consultable, :boolean, default: true
  end
end
