class AddHasStepMyResultsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_my_results, :boolean, default: false
  end
end
