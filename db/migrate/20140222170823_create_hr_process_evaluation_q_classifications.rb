class CreateHrProcessEvaluationQClassifications < ActiveRecord::Migration
  def change
    create_table :hr_process_evaluation_q_classifications do |t|
      t.integer :orden
      t.text :nombre
      t.references :hr_process_evaluation
      t.references :hr_evaluation_type_element

      t.timestamps
    end
    add_index :hr_process_evaluation_q_classifications, :hr_process_evaluation_id, :name => 'hr_p_e_q_class_hr_process_evaluation_id'
    add_index :hr_process_evaluation_q_classifications, :hr_evaluation_type_element_id, :name => 'hr_p_e_q_class_hr_evaluation_type_element_id'
  end
end
