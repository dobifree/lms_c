class AddAprobadoUnidadToDncs < ActiveRecord::Migration
  def change
    add_column :dncs, :aprobado_unidad, :boolean, default: false
  end
end
