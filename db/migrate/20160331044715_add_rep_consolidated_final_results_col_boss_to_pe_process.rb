class AddRepConsolidatedFinalResultsColBossToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :rep_consolidated_final_results_col_boss, :boolean, default: false
  end
end
