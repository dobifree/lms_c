class AddRequiredAlternativeIdToMasterPollQuestions < ActiveRecord::Migration
  def change
    add_column :master_poll_questions, :required_alternative_id, :integer
  end
end
