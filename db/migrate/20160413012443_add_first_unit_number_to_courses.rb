class AddFirstUnitNumberToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :first_unit_number, :integer, default: 1
  end
end
