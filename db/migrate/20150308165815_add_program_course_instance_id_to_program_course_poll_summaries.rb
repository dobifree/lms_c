class AddProgramCourseInstanceIdToProgramCoursePollSummaries < ActiveRecord::Migration
  def change
    add_column :program_course_poll_summaries, :program_course_instance_id, :integer
  end
end
