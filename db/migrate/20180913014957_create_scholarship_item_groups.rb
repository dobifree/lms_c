class CreateScholarshipItemGroups < ActiveRecord::Migration
  def change
    create_table :scholarship_item_groups do |t|
      t.references :scholarship_process
      t.string :name
      t.text :description

      t.timestamps
    end
    add_index :scholarship_item_groups, :scholarship_process_id
  end
end
