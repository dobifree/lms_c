class AddStatusRejToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :status_rej, :boolean, :default => false
  end
end
