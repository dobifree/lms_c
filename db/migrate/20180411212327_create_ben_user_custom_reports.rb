class CreateBenUserCustomReports < ActiveRecord::Migration
  def change
    create_table :ben_user_custom_reports do |t|
      t.references :ben_custom_report
      t.references :user

      t.timestamps
    end
    add_index :ben_user_custom_reports, :ben_custom_report_id, name: "ben_custom_report_user"
    add_index :ben_user_custom_reports, :user_id
  end
end
