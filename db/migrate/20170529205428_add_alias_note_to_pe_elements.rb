class AddAliasNoteToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :alias_note, :string, default: 'Nota'
  end
end
