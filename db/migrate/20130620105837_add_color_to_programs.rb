class AddColorToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :color, :string
  end
end
