class AddStepAssessmentCanFinishToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_can_finish, :boolean, default: true
  end
end
