class AddActiveRepDetailedFinalResultsToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_rep_detailed_final_results, :boolean
  end
end
