class AddColumnNextItemIdToBpItem < ActiveRecord::Migration
  def change
    add_column :bp_items, :next_item_id, :integer
    add_index :bp_items, :next_item_id
  end
end
