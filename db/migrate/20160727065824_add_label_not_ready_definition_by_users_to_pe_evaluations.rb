class AddLabelNotReadyDefinitionByUsersToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :label_not_ready_definition_by_users, :string, default: 'not_ready|default'
  end
end
