class CreateHolidayCharacteristics < ActiveRecord::Migration
  def change
    create_table :holiday_characteristics do |t|
      t.references :characteristic
      t.references :holiday
      t.integer :condition_type
      t.string :match_value_string
      t.text :match_value_text
      t.date :match_value_date
      t.integer :match_value_int
      t.float :match_value_float
      t.integer :match_value_char_id
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :holiday_characteristics, :characteristic_id
    add_index :holiday_characteristics, :holiday_id
  end
end
