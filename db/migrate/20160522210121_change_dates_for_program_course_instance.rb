class ChangeDatesForProgramCourseInstance < ActiveRecord::Migration
  def change
    change_column :program_course_instances, :from_date, :datetime
    change_column :program_course_instances, :to_date, :datetime
  end
end
