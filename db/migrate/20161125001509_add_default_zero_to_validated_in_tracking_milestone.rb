class AddDefaultZeroToValidatedInTrackingMilestone < ActiveRecord::Migration
  def change
    change_column :tracking_milestones, :validated, :boolean, :default => false
  end
end
