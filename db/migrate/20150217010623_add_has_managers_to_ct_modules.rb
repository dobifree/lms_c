class AddHasManagersToCtModules < ActiveRecord::Migration
  def change
    add_column :ct_modules, :has_managers, :boolean
  end
end
