class CreateTrackingProcessChars < ActiveRecord::Migration
  def change
    create_table :tracking_process_chars do |t|
      t.references :tracking_process
      t.references :characteristic
      t.integer :position

      t.timestamps
    end
    add_index :tracking_process_chars, :tracking_process_id
    add_index :tracking_process_chars, :characteristic_id
  end
end
