class AddAssessBossCopyAutoToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :assess_boss_copy_auto, :boolean, default: false
  end
end
