class CreateUcCharacteristics < ActiveRecord::Migration
  def change
    create_table :uc_characteristics do |t|
      t.string :name

      t.timestamps
    end
  end
end
