class RemoveSelApplicantIdFromSelApplicantValues < ActiveRecord::Migration
  def change
    remove_columns :sel_applicant_values, :sel_applicant_id
    add_column :sel_applicant_values, :sel_applicant_answer_id, :integer
  end
end
