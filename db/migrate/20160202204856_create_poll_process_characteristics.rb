class CreatePollProcessCharacteristics < ActiveRecord::Migration
  def change
    create_table :poll_process_characteristics do |t|
      t.references :poll_process
      t.references :characteristic
      t.text :match_value

      t.timestamps
    end
    add_index :poll_process_characteristics, :poll_process_id
    add_index :poll_process_characteristics, :characteristic_id
  end
end
