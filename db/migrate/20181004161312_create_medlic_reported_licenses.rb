class CreateMedlicReportedLicenses < ActiveRecord::Migration
  def change
    create_table :medlic_reported_licenses do |t|
      t.references :medlic_report
      t.references :medlic_license
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :medlic_reported_licenses, :medlic_report_id
    add_index :medlic_reported_licenses, :medlic_license_id
    add_index :medlic_reported_licenses, :registered_by_user_id
  end
end
