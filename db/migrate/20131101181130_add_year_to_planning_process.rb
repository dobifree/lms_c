class AddYearToPlanningProcess < ActiveRecord::Migration
  def change
    add_column :planning_processes, :year, :integer
  end
end
