class AddHeTotalsToUserProcess < ActiveRecord::Migration
  def change
    add_column :sc_user_processes, :he_send_to_validation, :decimal, precision: 12, scale: 4, default: 0.0
    add_column :sc_user_processes, :he_sent_to_validation, :decimal, precision: 12, scale: 4, default: 0.0
    add_column :sc_user_processes, :he_approved, :decimal, precision: 12, scale: 4, default: 0.0
    add_column :sc_user_processes, :he_rejected, :decimal, precision: 12, scale: 4, default: 0.0
    add_column :sc_user_processes, :he_not_charged_to_payroll, :decimal, precision: 12, scale: 4, default: 0.0
  end
end