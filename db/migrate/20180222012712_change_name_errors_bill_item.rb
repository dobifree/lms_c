class ChangeNameErrorsBillItem < ActiveRecord::Migration
  def change
    rename_column :ben_cellphone_bill_items, :errors, :bill_item_errors
  end
end
