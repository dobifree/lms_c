class AddStepAssessmentSummaryGroupToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_summary_group, :boolean, default: false
  end
end
