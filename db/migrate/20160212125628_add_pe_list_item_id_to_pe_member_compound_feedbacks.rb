class AddPeListItemIdToPeMemberCompoundFeedbacks < ActiveRecord::Migration
  def change
    add_column :pe_member_compound_feedbacks, :list_item_id, :integer
    add_index :pe_member_compound_feedbacks, :list_item_id
  end
end
