class AddNombreColorToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :nombre_color, :string, default: '000000'
  end
end
