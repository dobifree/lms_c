class AddPositionToTrainingCharacteristics < ActiveRecord::Migration
  def change
    add_column :training_characteristics, :position, :integer
  end
end
