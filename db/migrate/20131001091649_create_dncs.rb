class CreateDncs < ActiveRecord::Migration
  def change
    create_table :dncs do |t|
      t.references :planning_process
      t.references :planning_process_company_unit
      t.references :company_unit_area
      t.text :oportunidad_mejora
      t.references :planning_process_goal
      t.string :tema_capacitacion
      t.text :objetivos_curso
      t.float :costo_directo
      t.float :costo_indirecto
      t.references :training_mode
      t.references :training_type
      t.string :meses
      t.integer :tiempo_capacitacion
      t.references :training_provider

      t.timestamps
    end
    add_index :dncs, :planning_process_id
    add_index :dncs, :planning_process_company_unit_id
    add_index :dncs, :company_unit_area_id
    add_index :dncs, :planning_process_goal_id
    add_index :dncs, :training_mode_id
    add_index :dncs, :training_type_id
    add_index :dncs, :training_provider_id
  end
end
