class AddAllowWatchOwnTrackingToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :allow_watching_own_tracking, :boolean, default: false
  end
end
