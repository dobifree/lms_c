class AddUmQueryReportsToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :um_query_reports, :boolean, default: false
  end
end
