class AddValueFileToUserCharacteristicRecords < ActiveRecord::Migration
  def change
    add_column :user_characteristic_records, :value_file, :string
  end
end
