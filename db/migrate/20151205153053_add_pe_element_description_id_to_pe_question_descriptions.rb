class AddPeElementDescriptionIdToPeQuestionDescriptions < ActiveRecord::Migration
  def change
    add_column :pe_question_descriptions, :pe_element_description_id, :integer
    add_index :pe_question_descriptions, :pe_element_description_id
  end
end
