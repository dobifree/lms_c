class AddSelOptionToCtPrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :sel_make_requirement, :boolean, default: false
  end
end
