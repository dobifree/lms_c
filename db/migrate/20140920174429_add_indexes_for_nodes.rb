class AddIndexesForNodes < ActiveRecord::Migration
  def change
    add_index :users, :node_id
    add_index :nodes, :node_id
  end
end
