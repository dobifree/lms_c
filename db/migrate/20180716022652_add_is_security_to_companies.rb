class AddIsSecurityToCompanies < ActiveRecord::Migration

  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :is_security
      add_column :companies, :is_security, :boolean, default: false
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end

end
