class AddAllowTrackingWithBossToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :allow_tracking_with_boss, :boolean, default: false
  end
end
