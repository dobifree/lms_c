class AddStepAssessmentFinishMessageToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_finish_message, :string, default: 'Confirmo que he finalizado la evaluación'
  end
end
