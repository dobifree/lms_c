class CreateVacRecords < ActiveRecord::Migration
  def change
    create_table :vac_records do |t|
      t.references :vac_request
      t.date :begin
      t.date :end
      t.integer :days
      t.integer :days_progressive
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.text :deactivated_description
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.boolean :registered_manually, default: false
      t.text :registered_manually_description

      t.timestamps
    end
    add_index :vac_records, :vac_request_id
    add_index :vac_records, :deactivated_by_user_id, name: 'vac_record_deact_user_id'
    add_index :vac_records, :registered_by_user_id, name: 'vac_record_regis_user_id'
  end
end
