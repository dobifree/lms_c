class CreateSelReqOptions < ActiveRecord::Migration
  def change
    create_table :sel_req_options do |t|
      t.references :sel_req_item
      t.string :name
      t.text :description
      t.integer :position

      t.timestamps
    end
    add_index :sel_req_options, :sel_req_item_id
  end
end
