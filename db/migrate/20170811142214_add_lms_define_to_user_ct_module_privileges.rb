class AddLmsDefineToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :lms_define, :boolean, default: false
  end
end
