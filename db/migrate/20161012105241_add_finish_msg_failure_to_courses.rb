class AddFinishMsgFailureToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :finish_msg_failure, :text
  end
end
