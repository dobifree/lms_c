class CreateVacRequests < ActiveRecord::Migration
  def change
    create_table :vac_requests do |t|
      t.references :user
      t.references :vac_requests_period
      t.references :vac_accumulated
      t.date :begin
      t.date :end
      t.integer :days
      t.integer :days_progressive
      t.integer :status
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.integer :prev_request_id
      t.datetime :registered_at

      t.timestamps
    end
    add_index :vac_requests, :user_id
    add_index :vac_requests, :vac_requests_period_id
    add_index :vac_requests, :vac_accumulated_id
    add_index :vac_requests, :deactivated_by_user_id, name: 'vac_request_deact_user_id'
    add_index :vac_requests, :prev_request_id
  end
end
