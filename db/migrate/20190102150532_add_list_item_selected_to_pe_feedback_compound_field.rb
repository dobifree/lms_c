class AddListItemSelectedToPeFeedbackCompoundField < ActiveRecord::Migration
  def change
    add_column :pe_feedback_compound_fields, :list_item_selected, :boolean, default: true
  end
end
