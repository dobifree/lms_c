class AddRepDetailedFinalResultsShowFeedbackAcceptedDateToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :rep_detailed_final_results_show_feedback_accepted_date, :boolean, default: false
  end
end
