class AddLayoutToSelTemplateApplyForm < ActiveRecord::Migration
  def change
    add_column :sel_templates, :apply_form_layout, :text
  end
end
