class AddActiveRepsConfigurationToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_reps_configuration, :boolean, default: false
  end
end
