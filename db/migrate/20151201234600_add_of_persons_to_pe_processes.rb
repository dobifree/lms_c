class AddOfPersonsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :of_persons, :boolean, default: true
  end
end
