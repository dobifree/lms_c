class AddActiveReportingToLmsCharacteristics < ActiveRecord::Migration
  def change
    add_column :lms_characteristics, :active_reporting, :boolean, :default => false
  end
end
