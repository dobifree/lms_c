class AddColumnsForAccumulatedType < ActiveRecord::Migration
  def change
    add_column :bp_condition_vacations, :accumulated_type, :integer, default: 0
    add_column :bp_rules_warnings, :accumulated_type, :integer, default: 0
  end
end
