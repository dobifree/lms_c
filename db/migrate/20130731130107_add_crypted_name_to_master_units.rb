class AddCryptedNameToMasterUnits < ActiveRecord::Migration
  def change
    add_column :master_units, :crypted_name, :string
  end
end
