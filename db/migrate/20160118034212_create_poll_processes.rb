class CreatePollProcesses < ActiveRecord::Migration
  def change
    create_table :poll_processes do |t|
      t.string :name
      t.text :description
      t.boolean :active
      t.datetime :from_date
      t.datetime :to_date
      t.references :master_poll
      t.boolean :finished
      t.datetime :finished_at
      t.integer :finished_by_user_id

      t.timestamps
    end
    add_index :poll_processes, :master_poll_id
  end
end
