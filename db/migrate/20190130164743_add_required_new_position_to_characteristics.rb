class AddRequiredNewPositionToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :required_new_position, :integer
  end
end
