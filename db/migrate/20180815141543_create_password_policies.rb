class CreatePasswordPolicies < ActiveRecord::Migration
  def change
    create_table :password_policies do |t|
      t.integer :priority
      t.string  :name
      t.boolean :use_in_exa
      t.boolean :use_in_job_market
      t.boolean :default
      t.boolean :active

      t.timestamps
    end
  end
end
