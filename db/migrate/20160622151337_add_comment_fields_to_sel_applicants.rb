class AddCommentFieldsToSelApplicants < ActiveRecord::Migration
  def change
    add_column :sel_applicants, :comment, :text
    add_column :sel_applicants, :comment_by_user_id, :integer
    add_column :sel_applicants, :comment_at, :datetime

    add_index :sel_applicants, :comment_by_user_id
  end
end
