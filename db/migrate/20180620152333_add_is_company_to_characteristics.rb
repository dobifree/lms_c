class AddIsCompanyToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :is_company, :boolean, default: false
  end
end
