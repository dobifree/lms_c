class CreateCtModules < ActiveRecord::Migration
  def change
    create_table :ct_modules do |t|
      t.string :name
      t.boolean :active
      t.string :user_menu_alias

      t.timestamps
    end
  end
end
