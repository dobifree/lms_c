class AddStepFeedbackShowSummaryToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_summary, :boolean, default: true
  end
end
