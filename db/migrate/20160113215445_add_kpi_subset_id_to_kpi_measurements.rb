class AddKpiSubsetIdToKpiMeasurements < ActiveRecord::Migration
  def change
    add_column :kpi_measurements, :kpi_subset_id, :integer
  end
end
