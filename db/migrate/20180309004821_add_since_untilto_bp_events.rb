class AddSinceUntiltoBpEvents < ActiveRecord::Migration

  def change
    add_column :bp_events, :since, :datetime
    add_column :bp_events, :until, :datetime
    add_column :bp_events, :deactivated_at, :datetime
    add_column :bp_events, :deactivated_by_admin_id, :integer
    add_column :bp_events, :registered_at, :datetime
    add_column :bp_events, :registered_by_admin_id, :integer

    add_index :bp_events, :deactivated_by_admin_id
    add_index :bp_events, :registered_by_admin_id

    add_column :bp_event_files, :since, :datetime
    add_column :bp_event_files, :until, :datetime
    remove_column :bp_event_files, :active

  end
end
