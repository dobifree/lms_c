class AddIndicadorLogroDescripcionToHrEvaluationTypeElement < ActiveRecord::Migration
  def change
    add_column :hr_evaluation_type_elements, :indicador_logro_descripcion, :boolean, default: true
  end
end
