class AddStepQueryDefinitionsAcceptBodyToBossToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_query_definitions_accept_body_to_boss, :text
  end
end
