class AddMasterEvaluationIdToEvaluation < ActiveRecord::Migration
  def change
    add_column :evaluations, :master_evaluation_id, :integer
  end
end
