class AddSkRutClientToProgramCourseInstance < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :sk_rut_client, :string
  end
end
