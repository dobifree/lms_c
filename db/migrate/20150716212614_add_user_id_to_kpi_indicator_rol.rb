class AddUserIdToKpiIndicatorRol < ActiveRecord::Migration
  def change
    add_column :kpi_indicator_rols, :user_id, :integer
    add_index :kpi_indicator_rols, :user_id
  end
end
