class CreateScLogRecords < ActiveRecord::Migration
  def change
    create_table :sc_log_records do |t|
      t.references :user
      t.datetime :date
      t.text :record_before
      t.text :record_after
      t.string :action
      t.references :sc_record

      t.timestamps
    end
    add_index :sc_log_records, :user_id
    add_index :sc_log_records, :sc_record_id
  end
end
