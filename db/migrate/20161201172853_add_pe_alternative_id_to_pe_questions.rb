class AddPeAlternativeIdToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :pe_alternative_id, :integer
    add_index :pe_questions, :pe_alternative_id
  end
end
