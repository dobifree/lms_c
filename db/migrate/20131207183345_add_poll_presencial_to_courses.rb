class AddPollPresencialToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :poll_presencial, :boolean, default: false
  end
end
