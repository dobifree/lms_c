class AddUnitAliasToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :unit_alias, :string, default: 'unidad'
  end
end
