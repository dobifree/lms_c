class CreateCourseComments < ActiveRecord::Migration
  def change
    create_table :course_comments do |t|
      t.references :course
      t.references :user
      t.datetime :fecha
      t.text :comentario

      t.timestamps
    end
    add_index :course_comments, :course_id
    add_index :course_comments, :user_id
  end
end
