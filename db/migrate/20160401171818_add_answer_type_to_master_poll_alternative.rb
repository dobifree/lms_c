class AddAnswerTypeToMasterPollAlternative < ActiveRecord::Migration
  def change
    add_column :master_poll_alternatives, :answer_type, :integer, default: 0
  end
end
