class AddColumnHexToScDoc < ActiveRecord::Migration
  def change
    add_column :sc_documentations, :active, :boolean, default: true
    add_column :sc_documentations, :deactivated_at, :datetime
    add_column :sc_documentations, :deactivated_by_user_id, :integer
    add_index :sc_documentations, :deactivated_by_user_id
  end
end
