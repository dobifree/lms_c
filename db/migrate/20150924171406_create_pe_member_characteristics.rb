class CreatePeMemberCharacteristics < ActiveRecord::Migration
  def change
    create_table :pe_member_characteristics do |t|
      t.references :pe_member
      t.references :pe_process
      t.references :characteristic
      t.string :value

      t.timestamps
    end
    add_index :pe_member_characteristics, :pe_member_id
    add_index :pe_member_characteristics, :pe_process_id
    add_index :pe_member_characteristics, :characteristic_id
  end
end
