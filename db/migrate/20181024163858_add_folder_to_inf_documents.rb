class AddFolderToInfDocuments < ActiveRecord::Migration
  def change
    add_column :informative_documents, :inf_doc_folder_id, :integer
    add_index :informative_documents, :inf_doc_folder_id
  end
end