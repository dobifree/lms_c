class AddPublicPeDimensionNameToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :public_pe_dimension_name, :boolean, default: true
  end
end
