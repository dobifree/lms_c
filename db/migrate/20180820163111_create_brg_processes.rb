class CreateBrgProcesses < ActiveRecord::Migration
  def change
    create_table :brg_processes do |t|
      t.string :name
      t.text :description
      t.datetime :period_begin
      t.datetime :period_end
      t.datetime :due_date
      t.integer :status
      t.text :status_description
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :brg_processes, :deactivated_by_user_id
    add_index :brg_processes, :registered_by_user_id
  end
end
