class AddFechaSizeToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :fecha_size, :integer, default: 20
  end
end
