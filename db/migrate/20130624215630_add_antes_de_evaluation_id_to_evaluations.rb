class AddAntesDeEvaluationIdToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :antes_de_evaluation_id, :integer
  end
end
