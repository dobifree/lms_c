class ChangeDefaultActiveToBrgGroupAndGUser < ActiveRecord::Migration
  def change
    change_column :brg_group_users, :active, :boolean, default: true
    change_column :brg_groups, :active, :boolean, default: true
  end
end
