class AddPosUnitToLmsCharacteristics < ActiveRecord::Migration
  def change
    add_column :lms_characteristics, :pos_unit, :integer
  end
end
