class CreateTrackingFormQuestions < ActiveRecord::Migration
  def change
    create_table :tracking_form_questions do |t|
      t.references :tracking_form_group
      t.integer :position
      t.string :name
      t.text :description
      t.integer :min_qty_answers
      t.integer :max_qty_answers

      t.timestamps
    end
    add_index :tracking_form_questions, :tracking_form_group_id
  end
end
