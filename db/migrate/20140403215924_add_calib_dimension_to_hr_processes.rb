class AddCalibDimensionToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :calib_dimension, :boolean
  end
end
