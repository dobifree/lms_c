class AddTrackingFormQuestionIdToTrackingFormAnswer < ActiveRecord::Migration
  def change
    add_column :tracking_form_answers, :tracking_form_question_id, :integer
    add_index :tracking_form_answers, :tracking_form_question_id
  end

end
