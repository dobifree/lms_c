class CreateBpItems < ActiveRecord::Migration
  def change
    create_table :bp_items do |t|
      t.references :bp_form
      t.string :name
      t.text :description
      t.integer :position
      t.integer :item_type
      t.boolean :mandatory, default: true
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id
      t.integer :prev_item_id

      t.timestamps
    end
    add_index :bp_items, :bp_form_id
    add_index :bp_items, :prev_item_id
    add_index :bp_items, :deactivated_by_admin_id, name: 'bp_item_deac_adm_id'
    add_index :bp_items, :registered_by_admin_id, name: 'bp_item_regis_adm_id'
  end
end
