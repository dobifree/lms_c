class AddStepFeedbackAcceptedFinishMessageToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_accepted_finish_message, :string, default: 'Confirmo que he recibido la retroalimentación y la información proporcionada es correcta.'
  end
end
