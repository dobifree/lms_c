class AddRegisToBpSeasonPeriod < ActiveRecord::Migration
  def change
    add_column :bp_season_periods, :registered_at, :datetime
    add_column :bp_season_periods, :registered_by_admin_id, :integer
    add_index :bp_season_periods, :registered_by_admin_id

  end
end
