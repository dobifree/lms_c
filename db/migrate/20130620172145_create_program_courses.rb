class CreateProgramCourses < ActiveRecord::Migration
  def change
    create_table :program_courses do |t|
      t.references :program
      t.references :course
      t.references :level

      t.timestamps
    end
    add_index :program_courses, :program_id
    add_index :program_courses, :course_id
    add_index :program_courses, :level_id
  end
end
