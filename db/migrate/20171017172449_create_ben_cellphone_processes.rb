class CreateBenCellphoneProcesses < ActiveRecord::Migration
  def change
    create_table :ben_cellphone_processes do |t|
      t.integer :month
      t.integer :year
      t.boolean :charged_to_payroll
      t.datetime :charged_to_payroll_at
      t.integer :charged_to_payroll_by_user_id

      t.timestamps
    end
    add_index :ben_cellphone_processes, :charged_to_payroll_by_user_id, :name => 'ben_cellphone_processes_ctp_user_id'
  end
end
