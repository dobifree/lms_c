class AddStepFeedbackConfirmationMailToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :step_feedback_confirmation_mail, :boolean
  end
end
