class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :nombre
      t.string :codigo

      t.timestamps
    end
  end
end
