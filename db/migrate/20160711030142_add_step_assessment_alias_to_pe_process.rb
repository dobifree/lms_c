class AddStepAssessmentAliasToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_alias, :string, default: 'Evaluar'
  end
end
