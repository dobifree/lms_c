class CreateElecProcessRounds < ActiveRecord::Migration
  def change
    create_table :elec_process_rounds do |t|
      t.references :elec_process
      t.string :name
      t.text :description
      t.integer :position
      t.boolean :mandatory
      t.integer :lifespan
      t.integer :qty_required
      t.boolean :show_live_results
      t.boolean :show_final_results
      t.integer :threshold_pctg_universe
      t.integer :threshold_pctg_voters
      t.integer :threshold_qty_voters
      t.integer :qty_min_podium

      t.timestamps
    end

    add_index :elec_process_rounds, :elec_process_id

    change_column :elec_process_rounds, :mandatory, :boolean, :default => false
    change_column :elec_process_rounds, :qty_required, :integer, :default => 1
    change_column :elec_process_rounds, :show_live_results, :boolean, :default => false
    change_column :elec_process_rounds, :show_final_results, :boolean, :default => false
    change_column :elec_process_rounds, :threshold_pctg_universe, :integer, :default => 0
    change_column :elec_process_rounds, :threshold_pctg_voters, :integer, :default => 0
    change_column :elec_process_rounds, :threshold_qty_voters, :integer, :default => 1
    change_column :elec_process_rounds, :qty_min_podium, :integer, :default => 1
  end
end
