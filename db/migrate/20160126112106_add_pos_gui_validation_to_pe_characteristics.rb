class AddPosGuiValidationToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_gui_validation, :integer
    add_index :pe_characteristics, :pos_gui_validation
  end
end
