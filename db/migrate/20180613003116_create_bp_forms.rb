class CreateBpForms < ActiveRecord::Migration
  def change
    create_table :bp_forms do |t|
      t.references :bp_event
      t.string :name
      t.text :description
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id

      t.timestamps
    end
    add_index :bp_forms, :bp_event_id
    add_index :bp_forms, :deactivated_by_admin_id, name: 'bp_form_deac_adm_id'
    add_index :bp_forms, :registered_by_admin_id, name: 'bp_form_regis_adm_id'
  end
end
