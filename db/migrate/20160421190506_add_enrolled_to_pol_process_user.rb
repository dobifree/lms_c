class AddEnrolledToPolProcessUser < ActiveRecord::Migration
  def change
    add_column :poll_process_users, :enrolled, :boolean, default: false
    PollProcessUser.reset_column_information
    PollProcessUser.all.each do |process_user|
      process_user.update_attribute(:enrolled, false)
    end
  end
end
