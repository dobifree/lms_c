class AddUpdatedByUserIdToUserCharacteristicRecords < ActiveRecord::Migration
  def change
    add_column :user_characteristic_records, :updated_by_user_id, :integer
    add_index :user_characteristic_records, :updated_by_user_id
  end
end
