class CreateSelReqLogs < ActiveRecord::Migration
  def change
    create_table :sel_req_logs do |t|
      t.references :sel_requirement
      t.integer :log_state_id
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.text :comment

      t.timestamps
    end
    add_index :sel_req_logs, :sel_requirement_id
    add_index :sel_req_logs, :log_state_id
    add_index :sel_req_logs, :registered_by_user_id
  end
end
