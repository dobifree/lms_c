class AddHastaColorToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :hasta_color, :string, default: '000000'
  end
end
