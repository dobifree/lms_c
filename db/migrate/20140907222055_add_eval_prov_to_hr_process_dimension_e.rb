class AddEvalProvToHrProcessDimensionE < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :eval_prov, :boolean, default: false
  end
end
