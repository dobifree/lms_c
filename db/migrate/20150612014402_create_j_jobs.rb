class CreateJJobs < ActiveRecord::Migration
  def change
    create_table :j_jobs do |t|
      t.string :name
      t.string :mission
      t.references :j_job_level

      t.timestamps
    end
    add_index :j_jobs, :j_job_level_id
  end
end
