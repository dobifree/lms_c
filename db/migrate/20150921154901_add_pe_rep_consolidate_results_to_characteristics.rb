class AddPeRepConsolidateResultsToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :pe_rep_consolidate_results, :boolean
  end
end
