class AddLocationToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :location, :string
  end
end
