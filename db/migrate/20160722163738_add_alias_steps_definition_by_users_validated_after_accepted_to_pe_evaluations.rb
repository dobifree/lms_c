class AddAliasStepsDefinitionByUsersValidatedAfterAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :alias_steps_definition_by_users_validated_after_accepted, :string
  end
end
