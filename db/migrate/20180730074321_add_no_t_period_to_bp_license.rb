class AddNoTPeriodToBpLicense < ActiveRecord::Migration
  def change
    add_column :bp_licenses, :with_period, :boolean, default: true
    change_column :bp_licenses, :hours, :integer
  end
end
