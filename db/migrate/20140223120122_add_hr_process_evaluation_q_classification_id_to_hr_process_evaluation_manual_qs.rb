class AddHrProcessEvaluationQClassificationIdToHrProcessEvaluationManualQs < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluation_manual_qs, :hr_process_evaluation_q_classification_id, :integer

    add_index :hr_process_evaluation_manual_qs, :hr_process_evaluation_q_classification_id, :name => 'hr_p_e_m_q_class_hr_process_evaluation_q_classification_id'

  end
end
