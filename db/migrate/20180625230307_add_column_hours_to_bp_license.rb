class AddColumnHoursToBpLicense < ActiveRecord::Migration
  def change
    add_column :bp_licenses, :hours, :decimal, precision: 12, scale: 4
  end
end
