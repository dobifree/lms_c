class ChangeTypoAtPassworRule < ActiveRecord::Migration
  def change
    rename_column :password_rules, :possitive_match, :positive_match
  end
end
