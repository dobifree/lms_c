class AddDurationToDncCourses < ActiveRecord::Migration
  def change
    add_column :dnc_courses, :duration, :integer
  end
end
