class AddSizeToCourseFile < ActiveRecord::Migration
  def change
    add_column :course_files, :size, :integer
  end
end
