class CreatePollProcessUsers < ActiveRecord::Migration
  def change
    create_table :poll_process_users do |t|
      t.references :poll_process
      t.references :user
      t.boolean :done

      t.timestamps
    end
    add_index :poll_process_users, :poll_process_id
    add_index :poll_process_users, :user_id
  end
end
