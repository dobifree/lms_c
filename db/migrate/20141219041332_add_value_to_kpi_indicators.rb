class AddValueToKpiIndicators < ActiveRecord::Migration
  def change
    add_column :kpi_indicators, :value, :float
  end
end
