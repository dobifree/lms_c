class AddLabelReadyDefinitionByUsersValidatedBeforeAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :label_ready_definition_by_users_validated_before_accepted, :string, default: 'ready|success'
  end
end
