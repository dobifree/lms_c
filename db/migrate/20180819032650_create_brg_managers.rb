class CreateBrgManagers < ActiveRecord::Migration
  def change
    create_table :brg_managers do |t|
      t.references :user
      t.string :name
      t.text :description
      t.integer :level
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :brg_managers, :user_id
    add_index :brg_managers, :deactivated_by_user_id
    add_index :brg_managers, :registered_by_user_id
  end
end
