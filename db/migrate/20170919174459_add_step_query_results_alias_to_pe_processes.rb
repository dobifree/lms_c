class AddStepQueryResultsAliasToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_query_results_alias, :string, default: 'Consultar resultados'
  end
end
