class AddUserIdToLogFolderFiles < ActiveRecord::Migration
  def change
    add_column :log_folder_files, :user_id, :integer
  end
end
