class ChangeDataTypeForDescripcionPoll < ActiveRecord::Migration
  def up
  	change_table :polls do |t|
      t.change :descripcion, :text
    end
  end

  def down
  end
end
