class AddColumnBrgToProcessesDependeciesNew < ActiveRecord::Migration
  def change
    add_column :brg_groups, :brg_process_id, :integer
    add_index :brg_groups, :brg_process_id

    add_column :brg_managers, :brg_process_id, :integer
    add_index :brg_managers, :brg_process_id

    add_column :brg_percentage_conversions, :brg_process_id, :integer
    add_index :brg_percentage_conversions, :brg_process_id, name: 'brg_per_conv_procss_id'

    BrgManageRelation.all.each { |obj| obj.destroy }
    BrgManager.all.each { |obj| obj.destroy }
    BrgReportedBonus.all.each { |obj| obj.destroy }
    BrgBonusPercentage.all.each { |obj| obj.destroy }
    BrgPercentageConversion.all.each { |obj| obj.destroy }
    BrgReport.all.each { |obj| obj.destroy }
    BrgCharPercentage.all.each { |obj| obj.destroy }
    BrgBonus.all.each { |obj| obj.destroy }
    BrgGroupUser.all.each { |obj| obj.destroy }
    BrgGroup.all.each { |obj| obj.destroy }
    BrgLvlPercentage.all.each { |obj| obj.destroy }
    BrgProcess.all.each { |obj| obj.destroy }
  end


end
