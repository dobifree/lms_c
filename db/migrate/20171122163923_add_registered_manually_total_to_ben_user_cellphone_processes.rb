class AddRegisteredManuallyTotalToBenUserCellphoneProcesses < ActiveRecord::Migration
  def change
    add_column :ben_user_cellphone_processes, :registered_manually_total, :decimal, :precision => 12, :scale => 4
  end
end
