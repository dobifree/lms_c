class AddValidationCommentToPeMemberRels < ActiveRecord::Migration
  def change
    add_column :pe_member_rels, :validation_comment, :text
  end
end
