class CreateSecuCohades < ActiveRecord::Migration
  def change
    create_table :secu_cohades do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
