class CreatePeQuestionRankModels < ActiveRecord::Migration
  def change
    create_table :pe_question_rank_models do |t|
      t.float :percentage
      t.float :goal
      t.string :discrete_goal
      t.references :pe_element

      t.timestamps
    end
    add_index :pe_question_rank_models, :pe_element_id
  end
end
