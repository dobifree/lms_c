class AddColumnHistoricalToVacRequest < ActiveRecord::Migration
  def change
    add_column :vac_requests, :historical, :boolean, default: false
  end
end
