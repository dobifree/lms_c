class AddColorToHrProcessEvaluationQClassifications < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluation_q_classifications, :color, :string
  end
end
