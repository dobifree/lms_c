class AddNombreCortoToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :nombre_corto, :string
  end
end
