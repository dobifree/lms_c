class AddPublicPeMembersDimensionNameToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :public_pe_members_dimension_name, :boolean, default: true
  end
end
