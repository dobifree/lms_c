class AddAliasCliInversoToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_cli_inverso, :string, default: 'Proveedor'
  end
end
