class AddUserIdToUserCourses < ActiveRecord::Migration
  def change
    add_column :user_courses, :user_id, :integer
    add_index :user_courses, :user_id
  end
end
