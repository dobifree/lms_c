class AddFormDescriptionToScholarshipProcesses < ActiveRecord::Migration
  def change
    add_column :scholarship_processes, :app_form_description, :text
  end
end
