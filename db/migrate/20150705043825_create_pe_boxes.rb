class CreatePeBoxes < ActiveRecord::Migration
  def change
    create_table :pe_boxes do |t|
      t.integer :position
      t.string :short_name
      t.string :name
      t.text :description
      t.string :text_color
      t.string :bg_color
      t.string :soft_text_color
      t.string :soft_bg_color
      t.references :pe_process
      t.integer :pe_dim_group_x_id
      t.integer :pe_dim_group_y_id

      t.timestamps
    end
    add_index :pe_boxes, :pe_process_id
    add_index :pe_boxes, :pe_dim_group_x_id
    add_index :pe_boxes, :pe_dim_group_y_id
  end
end
