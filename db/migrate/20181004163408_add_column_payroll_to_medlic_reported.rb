class AddColumnPayrollToMedlicReported < ActiveRecord::Migration
  def change
    add_column :medlic_licenses, :sent_to_payroll, :boolean, default: false
    add_column :medlic_licenses, :sent_to_payroll_at, :datetime
    add_column :medlic_licenses, :sent_to_payroll_by_user_id, :integer

    add_index :medlic_licenses, :sent_to_payroll_by_user_id
  end
end
