class AddValidatedToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :validated, :boolean, default: true
  end
end
