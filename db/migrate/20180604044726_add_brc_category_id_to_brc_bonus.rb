class AddBrcCategoryIdToBrcBonus < ActiveRecord::Migration
  def change
    add_column :brc_bonus, :brc_category_id, :integer
    add_index :brc_bonus, :brc_category_id
  end
end
