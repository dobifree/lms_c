class CreateTrainingImpactValues < ActiveRecord::Migration
  def change
    create_table :training_impact_values do |t|
      t.integer :valor

      t.timestamps
    end
  end
end
