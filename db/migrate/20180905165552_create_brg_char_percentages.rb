class CreateBrgCharPercentages < ActiveRecord::Migration
  def change
    create_table :brg_char_percentages do |t|
      t.references :characteristic
      t.references :brg_process
      t.string :cod
      t.float :percentage
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :brg_char_percentages, :characteristic_id
    add_index :brg_char_percentages, :brg_process_id
    add_index :brg_char_percentages, :deactivated_by_user_id
    add_index :brg_char_percentages, :registered_by_user_id
  end
end
