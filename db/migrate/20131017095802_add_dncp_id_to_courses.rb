class AddDncpIdToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :dncp_id, :integer
  end
end
