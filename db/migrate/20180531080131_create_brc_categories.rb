class CreateBrcCategories < ActiveRecord::Migration
  def change
    create_table :brc_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
