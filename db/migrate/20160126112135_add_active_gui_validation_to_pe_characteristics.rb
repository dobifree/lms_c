class AddActiveGuiValidationToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_gui_validation, :boolean, default: false
  end
end
