class CreateBpSeasons < ActiveRecord::Migration
  def change
    create_table :bp_seasons do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
