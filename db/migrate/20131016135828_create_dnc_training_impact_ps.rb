class CreateDncTrainingImpactPs < ActiveRecord::Migration
  def change
    create_table :dnc_training_impact_ps do |t|
      t.references :dncp
      t.references :training_impact_category
      t.references :training_impact_value

      t.timestamps
    end
    add_index :dnc_training_impact_ps, :dncp_id
    add_index :dnc_training_impact_ps, :training_impact_category_id
    add_index :dnc_training_impact_ps, :training_impact_value_id
  end
end
