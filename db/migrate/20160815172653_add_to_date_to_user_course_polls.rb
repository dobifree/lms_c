class AddToDateToUserCoursePolls < ActiveRecord::Migration
  def change
    add_column :user_course_polls, :to_date, :datetime
  end
end
