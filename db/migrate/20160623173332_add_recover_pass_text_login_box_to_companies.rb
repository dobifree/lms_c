class AddRecoverPassTextLoginBoxToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :recover_pass_text_login_box, :string, default: '¿Olvidó su contraseña?'
  end
end
