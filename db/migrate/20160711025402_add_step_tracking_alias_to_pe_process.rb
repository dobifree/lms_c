class AddStepTrackingAliasToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_tracking_alias, :string, default: 'Seguimiento de'
  end
end
