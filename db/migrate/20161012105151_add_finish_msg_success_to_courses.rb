class AddFinishMsgSuccessToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :finish_msg_success, :text
  end
end
