class AddFileNameToSpecificAnswer < ActiveRecord::Migration
  def change
    add_column :sel_applicant_specific_answers, :attachment_name, :string
  end
end
