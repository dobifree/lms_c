class AddEvalPresencialToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :eval_presencial, :boolean, default: false
  end
end
