class CreateDataSyncs < ActiveRecord::Migration
  def change
    create_table :data_syncs do |t|
      t.string :description
      t.datetime :executed_at
      t.boolean :completed
      t.text :sync_errors

      t.timestamps
    end
  end
end
