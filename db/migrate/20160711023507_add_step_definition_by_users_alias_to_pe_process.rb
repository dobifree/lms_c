class AddStepDefinitionByUsersAliasToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_definition_by_users_alias, :string, default: 'Definir'
  end
end
