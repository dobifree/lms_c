class AddDescriptionToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :description, :string
  end
end
