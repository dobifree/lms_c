class AddStepAssessmentToPeAreas < ActiveRecord::Migration
  def change
    add_column :pe_areas, :step_assessment, :boolean, default: false
  end
end
