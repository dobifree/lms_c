class AddDescriptionToProgramCourseInstances < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :description, :string
  end
end
