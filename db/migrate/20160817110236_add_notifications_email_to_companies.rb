class AddNotificationsEmailToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :notifications_email, :string, default: 'notificaciones@exa.pe'
  end
end
