class AddPausedToUserCourseEvaluations < ActiveRecord::Migration
  def change
    add_column :user_course_evaluations, :paused, :boolean, default: false
  end
end
