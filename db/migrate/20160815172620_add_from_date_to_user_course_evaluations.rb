class AddFromDateToUserCourseEvaluations < ActiveRecord::Migration
  def change
    add_column :user_course_evaluations, :from_date, :datetime
  end
end
