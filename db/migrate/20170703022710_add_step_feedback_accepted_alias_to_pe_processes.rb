class AddStepFeedbackAcceptedAliasToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_accepted_alias, :string, default: 'Confirmar retroalimentación'
  end
end
