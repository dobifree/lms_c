class CreateBrcMembers < ActiveRecord::Migration
  def change
    create_table :brc_members do |t|
      t.references :brc_process
      t.references :user

      t.timestamps
    end
    add_index :brc_members, :brc_process_id
    add_index :brc_members, :user_id
  end
end
