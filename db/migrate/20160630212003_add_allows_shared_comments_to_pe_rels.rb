class AddAllowsSharedCommentsToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :allows_shared_comments, :boolean, default: false
  end
end
