class CreateBenUserCellphoneProcesses < ActiveRecord::Migration
  def change
    create_table :ben_user_cellphone_processes do |t|
      t.references :user
      t.references :ben_cellphone_number
      t.references :ben_cellphone_process
      t.float :total_bill
      t.float :configured_subsidy
      t.float :applied_subsidy
      t.float :total_charged

      t.timestamps
    end
    add_index :ben_user_cellphone_processes, :user_id
    add_index :ben_user_cellphone_processes, :ben_cellphone_number_id
    add_index :ben_user_cellphone_processes, :ben_cellphone_process_id
  end
end
