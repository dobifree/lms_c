class CreateLibCharacteristics < ActiveRecord::Migration
  def change
    create_table :lib_characteristics do |t|
      t.references :characteristic

      t.timestamps
    end
    add_index :lib_characteristics, :characteristic_id
  end
end
