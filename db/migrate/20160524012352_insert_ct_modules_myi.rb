class InsertCtModulesMyi < ActiveRecord::Migration
  def change
    execute "insert into ct_modules set cod='mpi', name='Mis colaboradores', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'mpi').count == 0
  end
end
