class AddHrProcessUserIdToHrProcessEvaluationManualQcs < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluation_manual_qcs, :hr_process_user_id, :integer
    add_index :hr_process_evaluation_manual_qcs, :hr_process_user_id, name: 'hr_pro_eval_manual_qcs_creator_id'
  end
end
