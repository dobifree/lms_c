class CreateSelStepCandidates < ActiveRecord::Migration
  def change
    create_table :sel_step_candidates do |t|
      t.references :jm_candidate
      t.references :sel_process_step
      t.boolean :exonerated, :default => false
      t.boolean :done, :default => false

      t.timestamps
    end
    add_index :sel_step_candidates, :jm_candidate_id
    add_index :sel_step_candidates, :sel_process_step_id
  end
end
