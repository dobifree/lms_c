class AddLibreToUnits < ActiveRecord::Migration
  def change
    add_column :units, :libre, :boolean, default: true
  end
end
