class CreateDncps < ActiveRecord::Migration
  def change
    create_table :dncps do |t|
      t.references :planning_process
      t.references :planning_process_company_unit
      t.references :company_unit_area
      t.text :oportunidad_mejora
      t.string :tema_capacitacion
      t.text :objetivos_curso
      t.float :costo_directo
      t.float :costo_indirecto
      t.references :training_mode
      t.references :training_type
      t.string :meses
      t.integer :tiempo_capacitacion
      t.references :training_provider
      t.float :impacto_total
      t.references :dnc

      t.timestamps
    end
    add_index :dncps, :planning_process_id
    add_index :dncps, :planning_process_company_unit_id
    add_index :dncps, :company_unit_area_id
    add_index :dncps, :training_mode_id
    add_index :dncps, :training_type_id
    add_index :dncps, :training_provider_id
    add_index :dncps, :dnc_id
  end
end
