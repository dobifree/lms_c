class AddProgramCourseInstanceIdToCourseTrainingCharacteristics < ActiveRecord::Migration
  def change
    add_column :course_training_characteristics, :program_course_instance_id, :integer
    add_index :course_training_characteristics, :program_course_instance_id, name: 'course_training_characteristics_pci_id'
  end
end
