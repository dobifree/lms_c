class CreateLogMailers < ActiveRecord::Migration
  def change
    create_table :log_mailers do |t|
      t.string :module
      t.string :step
      t.text :description
      t.datetime :registered_at

      t.timestamps
    end
  end
end
