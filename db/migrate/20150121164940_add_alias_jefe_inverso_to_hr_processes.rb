class AddAliasJefeInversoToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_jefe_inverso, :string, default: 'Colaborador'
  end
end
