class CreateHrEvaluationTypes < ActiveRecord::Migration
  def change
    create_table :hr_evaluation_types do |t|
      t.string :nombre
      t.boolean :activo, default: true

      t.timestamps
    end
  end
end
