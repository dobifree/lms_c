class CreateHrProcessEvaluationManualQs < ActiveRecord::Migration
  def change
    create_table :hr_process_evaluation_manual_qs do |t|
      t.string :texto
      t.integer :peso
      t.references :hr_process_evaluation
      t.references :hr_evaluation_type_element
      t.integer :hr_process_evaluation_manual_q_id
      t.references :hr_process_user

      t.timestamps
    end
    add_index :hr_process_evaluation_manual_qs, :hr_process_evaluation_id, :name => 'hr_p_e_m_q_hr_process_evaluation_id'
    add_index :hr_process_evaluation_manual_qs, :hr_evaluation_type_element_id, :name => 'hr_p_e_m_q_hr_evaluation_type_element_id'
    add_index :hr_process_evaluation_manual_qs, :hr_process_user_id, :name => 'hr_p_e_m_q_hr_process_user_id'

  end
end
