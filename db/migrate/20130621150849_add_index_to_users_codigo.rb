class AddIndexToUsersCodigo < ActiveRecord::Migration
  def change
  	add_index :users, :codigo, unique: true 
  end
end
