class AddMinWeightToPeQuestionModel < ActiveRecord::Migration
  def change
    add_column :pe_question_models, :min_weight, :integer
  end
end
