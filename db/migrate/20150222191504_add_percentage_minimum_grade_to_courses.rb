class AddPercentageMinimumGradeToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :percentage_minimum_grade, :boolean, default: false
  end
end
