class AddAliasProvToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_prov, :string, default: 'Proveedor'
  end
end
