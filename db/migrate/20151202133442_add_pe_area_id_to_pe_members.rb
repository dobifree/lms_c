class AddPeAreaIdToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :pe_area_id, :integer
    add_index :pe_members, :pe_area_id
  end
end
