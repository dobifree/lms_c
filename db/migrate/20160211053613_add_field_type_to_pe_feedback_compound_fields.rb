class AddFieldTypeToPeFeedbackCompoundFields < ActiveRecord::Migration
  def change
    add_column :pe_feedback_compound_fields, :field_type, :integer, default: 0
    add_index :pe_feedback_compound_fields, :field_type
  end
end
