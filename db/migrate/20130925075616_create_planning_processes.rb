class CreatePlanningProcesses < ActiveRecord::Migration
  def change
    create_table :planning_processes do |t|
      t.string :nombre
      t.boolean :abierto, default: true
      t.date :fecha_fin

      t.timestamps
    end
  end
end
