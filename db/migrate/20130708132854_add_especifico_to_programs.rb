class AddEspecificoToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :especifico, :boolean, default: false
  end
end
