class CreateHrProcessUserRels < ActiveRecord::Migration
  def change
    create_table :hr_process_user_rels do |t|
      t.integer :hr_process_user1_id
      t.string :tipo
      t.integer :hr_process_user2_id

      t.timestamps
    end
  end
end
