class AddAutoTicketToVacRequestAndAccumulated < ActiveRecord::Migration
  def change
    add_column :vac_requests, :auto_update_legal, :boolean, default: false
    add_column :vac_requests, :auto_update_prog, :boolean, default: false
    add_column :vac_accumulateds, :auto_update_legal, :boolean, default: false
    add_column :vac_accumulateds, :auto_update_prog, :boolean, default: false
  end
end
