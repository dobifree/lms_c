class CreateScCharacteristicConditions < ActiveRecord::Migration
  def change
    create_table :sc_characteristic_conditions do |t|
      t.references :characteristic
      t.integer :condition
      t.string :match_value_string
      t.integer :match_value_int
      t.float :match_value_float
      t.text :match_value_text
      t.date :match_value_date
      t.integer :match_value_char_id
      t.references :sc_schedule
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.boolean :active, :default => false
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id

      t.timestamps
    end
    add_index :sc_characteristic_conditions, :characteristic_id
    add_index :sc_characteristic_conditions, :sc_schedule_id
    add_index :sc_characteristic_conditions, :registered_by_user_id
    add_index :sc_characteristic_conditions, :deactivated_by_user_id
  end
end
