class AddHasStepCalibrationEvaluationToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_calibration_evaluation, :boolean
  end
end
