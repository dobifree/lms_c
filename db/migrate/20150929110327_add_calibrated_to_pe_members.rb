class AddCalibratedToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :calibrated, :boolean, default: false
  end
end
