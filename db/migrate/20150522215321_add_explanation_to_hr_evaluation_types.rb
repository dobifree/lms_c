class AddExplanationToHrEvaluationTypes < ActiveRecord::Migration
  def change
    add_column :hr_evaluation_types, :explanation, :text
  end
end
