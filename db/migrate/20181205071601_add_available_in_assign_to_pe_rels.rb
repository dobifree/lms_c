class AddAvailableInAssignToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :available_in_assign, :boolean, default: false
  end
end
