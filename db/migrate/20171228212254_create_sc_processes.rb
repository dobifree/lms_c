class CreateScProcesses < ActiveRecord::Migration
  def change
    create_table :sc_processes do |t|
      t.integer :month
      t.integer :year
      t.datetime :deadline
      t.boolean :closed, :default => false
      t.datetime :closed_at
      t.integer :closed_by_user_id

      t.timestamps
    end

    add_index :sc_processes, :closed_by_user_id
  end
end
