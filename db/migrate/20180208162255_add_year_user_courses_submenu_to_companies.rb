class AddYearUserCoursesSubmenuToCompanies < ActiveRecord::Migration

  def connection
    @connection = Company.connection
  end

  def change
    add_column(:companies, :year_user_courses_submenu, :boolean, :default => false) unless column_exists?(:companies, :year_user_courses_submenu)
  end
end
