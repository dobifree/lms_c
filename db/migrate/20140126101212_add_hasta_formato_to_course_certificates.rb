class AddHastaFormatoToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :hasta_formato, :string, default: 'day_snmonth_year'
  end
end
