class AddAlternativesDisplayMethodToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :alternatives_display_method, :integer
  end
end
