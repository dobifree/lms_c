class CreateSelVacantFlows < ActiveRecord::Migration
  def change
    create_table :sel_vacant_flows do |t|
      t.string :name
      t.text :description
      t.boolean :active
      t.float :threshold_for_approval

      t.timestamps
    end
  end
end
