class CreatePeProcessNotificationDefs < ActiveRecord::Migration
  def change
    create_table :pe_process_notification_defs do |t|
      t.references :pe_process
      t.references :pe_evaluation
      t.boolean :send_email_to_validator_when_defined
      t.string :to_validator_when_defined_subject
      t.text :to_validator_when_defined_body

      t.timestamps
    end
    add_index :pe_process_notification_defs, :pe_process_id
    add_index :pe_process_notification_defs, :pe_evaluation_id
  end
end
