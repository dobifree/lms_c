class CreateDncProcessPeriods < ActiveRecord::Migration
  def change
    create_table :dnc_process_periods do |t|
      t.string :name
      t.references :dnc_process
      t.date :from_date
      t.date :to_date

      t.timestamps
    end
    add_index :dnc_process_periods, :dnc_process_id
  end
end
