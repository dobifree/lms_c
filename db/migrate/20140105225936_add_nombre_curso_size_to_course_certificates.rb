class AddNombreCursoSizeToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :nombre_curso_size, :integer, default: 20
  end
end
