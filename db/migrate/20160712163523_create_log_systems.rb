class CreateLogSystems < ActiveRecord::Migration
  def change
    create_table :log_systems do |t|
      t.references :user
      t.text :description
      t.datetime :registered_at

      t.timestamps
    end
    add_index :log_systems, :user_id
  end
end
