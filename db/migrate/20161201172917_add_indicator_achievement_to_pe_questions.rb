class AddIndicatorAchievementToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :indicator_achievement, :float
  end
end
