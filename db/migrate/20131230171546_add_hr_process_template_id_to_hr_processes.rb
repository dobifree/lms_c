class AddHrProcessTemplateIdToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :hr_process_template_id, :integer
  end
end
