class CreateSelReqProcesses < ActiveRecord::Migration
  def change
    create_table :sel_req_processes do |t|
      t.references :sel_requirement
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.boolean :active
      t.boolean :started
      t.datetime :started_at
      t.integer :started_by_user_id

      t.timestamps
    end
    add_index :sel_req_processes, :sel_requirement_id
    add_index :sel_req_processes, :registered_by_user_id
    add_index :sel_req_processes, :started_by_user_id
  end
end
