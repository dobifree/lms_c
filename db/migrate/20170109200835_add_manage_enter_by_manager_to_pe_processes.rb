class AddManageEnterByManagerToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :manage_enter_by_manager, :boolean, default: false
  end
end
