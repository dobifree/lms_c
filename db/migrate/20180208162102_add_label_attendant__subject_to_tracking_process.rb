class AddLabelAttendantSubjectToTrackingProcess < ActiveRecord::Migration
  def change
    add_column :tracking_processes, :label_attendant, :string
    add_column :tracking_processes, :label_subject, :string
  end
end
