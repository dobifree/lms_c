class AddValueEncryptedToUserCharacteristicRecords < ActiveRecord::Migration
  def change
    add_column :user_characteristic_records, :value_encrypted, :blob
  end
end
