class RemoveClientFromProgramCourseInstances < ActiveRecord::Migration
  def up
    remove_column :program_course_instances, :client
  end

  def down
    add_column :program_course_instances, :client, :string
  end
end
