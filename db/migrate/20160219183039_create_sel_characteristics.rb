class CreateSelCharacteristics < ActiveRecord::Migration
  def change
    create_table :sel_characteristics do |t|
      t.references :sel_template
      t.string :name
      t.text :description
      t.integer :option_type

      t.timestamps
    end
    add_index :sel_characteristics, :sel_template_id
  end
end
