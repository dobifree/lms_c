class AddMinimumGradeToProgramInstances < ActiveRecord::Migration
  def change
    add_column :program_instances, :minimum_grade, :float
  end
end
