class CreatePeEvaluationGroups < ActiveRecord::Migration
  def change
    create_table :pe_evaluation_groups do |t|
      t.integer :position
      t.string :name
      t.references :pe_process

      t.timestamps
    end
    add_index :pe_evaluation_groups, :pe_process_id
  end
end
