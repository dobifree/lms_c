class AddHastaSizeToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :hasta_size, :integer, default: 20
  end
end
