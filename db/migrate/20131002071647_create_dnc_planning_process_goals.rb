class CreateDncPlanningProcessGoals < ActiveRecord::Migration
  def change
    create_table :dnc_planning_process_goals do |t|
      t.references :dnc
      t.references :planning_process_goal

      t.timestamps
    end
    add_index :dnc_planning_process_goals, :dnc_id
    add_index :dnc_planning_process_goals, :planning_process_goal_id
  end
end
