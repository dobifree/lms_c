class AddFinalizaAutomaticamenteToUnit < ActiveRecord::Migration
  def change
    add_column :units, :finaliza_automaticamente, :boolean, default: true
  end
end
