class AddLabelNotReadyDefinitionByUsersValidatedAfterAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :label_not_ready_definition_by_users_validated_after_accepted, :string, default: 'not_ready|primary'
  end
end
