class AddBonusPRetencionToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :bonus_p_retencion, :float, default: 0
  end
end
