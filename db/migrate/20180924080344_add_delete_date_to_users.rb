class AddDeleteDateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :delete_date, :date
  end
end
