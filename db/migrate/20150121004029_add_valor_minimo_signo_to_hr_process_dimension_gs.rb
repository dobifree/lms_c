class AddValorMinimoSignoToHrProcessDimensionGs < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_gs, :valor_minimo_signo, :string, default: '>='
  end
end
