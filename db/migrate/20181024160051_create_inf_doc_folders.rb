class CreateInfDocFolders < ActiveRecord::Migration
  def change
    create_table :inf_doc_folders do |t|
      t.string :name
      t.text :description
      t.integer :position

      t.timestamps
    end
  end
end
