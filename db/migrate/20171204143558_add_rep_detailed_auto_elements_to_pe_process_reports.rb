class AddRepDetailedAutoElementsToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_detailed_auto_elements, :string
  end
end
