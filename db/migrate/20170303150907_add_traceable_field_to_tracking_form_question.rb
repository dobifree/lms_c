class AddTraceableFieldToTrackingFormQuestion < ActiveRecord::Migration
  def change
    add_column :tracking_form_questions, :traceable, :boolean, :default => false
  end
end
