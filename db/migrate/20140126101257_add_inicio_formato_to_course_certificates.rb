class AddInicioFormatoToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :inicio_formato, :string, default: 'day_snmonth_year'
  end
end
