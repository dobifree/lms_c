class AddFinishedBooleanFieldToSelProcess < ActiveRecord::Migration
  def change
    add_column :sel_processes, :finished, :boolean, :default => false
  end
end
