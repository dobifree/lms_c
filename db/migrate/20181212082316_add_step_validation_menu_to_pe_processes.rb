class AddStepValidationMenuToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_validation_menu, :boolean, default: true
  end
end
