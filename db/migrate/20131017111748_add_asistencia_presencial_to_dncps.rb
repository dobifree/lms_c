class AddAsistenciaPresencialToDncps < ActiveRecord::Migration
  def change
    add_column :dncps, :asistencia_presencial, :boolean, default: true
  end
end
