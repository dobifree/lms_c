class AddReportableToTrackingFormItem < ActiveRecord::Migration
  def change
    add_column :tracking_form_items, :reportable, :boolean, :default => false
  end
end
