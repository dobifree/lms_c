class AddCodeFieldToJmCharacteristics < ActiveRecord::Migration
  def change
    add_column :jm_characteristics, :code, :string
  end
end
