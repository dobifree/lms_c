class CreateKpiSetGuests < ActiveRecord::Migration
  def change
    create_table :kpi_set_guests do |t|
      t.references :kpi_set
      t.references :user

      t.timestamps
    end
    add_index :kpi_set_guests, :kpi_set_id
    add_index :kpi_set_guests, :user_id
  end
end
