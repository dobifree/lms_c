class AddControllerActionFieldsToLogSystems < ActiveRecord::Migration
  def change
    add_column :log_systems, :controller, :string
    add_column :log_systems, :action, :string
  end
end
