class AddFinFormatoToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :fin_formato, :string, default: 'day_snmonth_year'
  end
end
