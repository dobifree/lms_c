class AddFechaColorToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :fecha_color, :string, default: '000000'
  end
end
