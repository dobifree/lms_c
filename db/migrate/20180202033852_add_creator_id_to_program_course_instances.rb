class AddCreatorIdToProgramCourseInstances < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :creator_id, :integer
    add_index :program_course_instances, :creator_id
  end
end
