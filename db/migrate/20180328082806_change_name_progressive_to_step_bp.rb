class ChangeNameProgressiveToStepBp < ActiveRecord::Migration
  def change
    rename_column :bp_general_rules_vacations, :progressive_name_step, :progressive_days_step
  end
end