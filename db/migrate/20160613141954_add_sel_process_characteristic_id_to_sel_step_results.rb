class AddSelProcessCharacteristicIdToSelStepResults < ActiveRecord::Migration
  def change
    add_column :sel_step_results, :sel_process_characteristic_id, :integer
    add_index :sel_step_results, :sel_process_characteristic_id
  end
end
