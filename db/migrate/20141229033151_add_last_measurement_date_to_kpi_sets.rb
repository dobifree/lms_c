class AddLastMeasurementDateToKpiSets < ActiveRecord::Migration
  def change
    add_column :kpi_sets, :last_measurement_update, :date
  end
end
