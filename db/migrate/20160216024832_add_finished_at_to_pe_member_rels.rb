class AddFinishedAtToPeMemberRels < ActiveRecord::Migration
  def change
    add_column :pe_member_rels, :finished_at, :datetime
  end
end
