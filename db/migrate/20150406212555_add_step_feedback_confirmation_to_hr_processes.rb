class AddStepFeedbackConfirmationToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :step_feedback_confirmation, :boolean
  end
end
