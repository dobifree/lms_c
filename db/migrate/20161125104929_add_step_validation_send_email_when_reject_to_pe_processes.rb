class AddStepValidationSendEmailWhenRejectToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_validation_send_email_when_reject, :boolean, default: false
  end
end
