class RenamePointsPeAssessmentFinalQuestions < ActiveRecord::Migration
  def change
    rename_column :pe_assessment_final_questions, :point, :points
  end
end
