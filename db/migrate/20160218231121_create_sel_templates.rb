class CreateSelTemplates < ActiveRecord::Migration
  def change
    create_table :sel_templates do |t|
      t.string :name
      t.text :description
      t.boolean :active, :default => false

      t.timestamps
    end
  end
end
