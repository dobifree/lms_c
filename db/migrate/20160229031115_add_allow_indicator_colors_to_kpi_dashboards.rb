class AddAllowIndicatorColorsToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :allow_indicator_colors, :boolean, default: false
  end
end
