class RenameOrdenPublicaInCharacteristics < ActiveRecord::Migration
  def change
    rename_column :characteristics, :orden_publica, :position
  end
end
