class AddSelRequirementToSelProcess < ActiveRecord::Migration
  def change
    add_column :sel_processes, :sel_requirement_id, :integer

    add_index :sel_processes, :sel_requirement_id
  end
end
