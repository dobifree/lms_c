class AddStepFeedbackAcceptedSendMailToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_accepted_send_mail, :boolean
  end
end
