class AddPlanningProcessToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :planning_process, :boolean
  end
end
