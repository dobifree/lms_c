class CreatePeStepFeedbackGroup2s < ActiveRecord::Migration
  def change
    create_table :pe_step_feedback_group2s do |t|
      t.references :pe_process
      t.references :pe_group2
      t.datetime :from_date
      t.datetime :to_date

      t.timestamps
    end
    add_index :pe_step_feedback_group2s, :pe_process_id
    add_index :pe_step_feedback_group2s, :pe_group2_id
  end
end
