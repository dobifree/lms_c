class AddAllowActivitiesToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :allow_activities, :boolean, default: false
  end
end
