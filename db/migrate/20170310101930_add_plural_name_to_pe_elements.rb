class AddPluralNameToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :plural_name, :string, default: ''
  end
end
