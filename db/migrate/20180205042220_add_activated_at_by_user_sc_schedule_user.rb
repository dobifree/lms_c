class AddActivatedAtByUserScScheduleUser < ActiveRecord::Migration
  def change
    add_column :sc_schedule_users, :activated_at, :datetime
    add_column :sc_schedule_users, :activated_by_user_id, :integer

    add_index :sc_schedule_users, :activated_by_user_id

  end
end
