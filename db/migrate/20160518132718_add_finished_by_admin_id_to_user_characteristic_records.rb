class AddFinishedByAdminIdToUserCharacteristicRecords < ActiveRecord::Migration
  def change
    add_column :user_characteristic_records, :finished_by_admin_id, :integer
    add_index :user_characteristic_records, :finished_by_admin_id
  end
end
