class AddActivaToUnits < ActiveRecord::Migration
  def change
    add_column :units, :activa, :boolean, default: true
  end
end
