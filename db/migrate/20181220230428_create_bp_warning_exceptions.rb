class CreateBpWarningExceptions < ActiveRecord::Migration
  def change
    create_table :bp_warning_exceptions do |t|
      t.references :bp_groups_user
      t.integer :max_usages
      t.datetime :active_since
      t.datetime :active_until
      t.boolean :available, default: true
      t.datetime :unavailable_at
      t.integer :unavailable_by_user_id
      t.text :unavailable_description
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.text :registered_description

      t.timestamps
    end
    add_index :bp_warning_exceptions, :bp_groups_user_id
    add_index :bp_warning_exceptions, :registered_by_user_id
    add_index :bp_warning_exceptions, :unavailable_by_user_id
  end
end
