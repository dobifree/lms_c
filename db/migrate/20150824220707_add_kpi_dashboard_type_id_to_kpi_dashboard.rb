class AddKpiDashboardTypeIdToKpiDashboard < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :kpi_dashboard_type_id, :integer
    add_index :kpi_dashboards, :kpi_dashboard_type_id
  end
end
