class AddCopiedReferenceFieldsToResultsAttachments < ActiveRecord::Migration
  def change
    add_column :sel_step_candidates, :copied_from_id, :integer
    add_column :sel_evaluation_attachments, :copied_from_id, :integer

    add_index :sel_step_candidates, :copied_from_id
    add_index :sel_evaluation_attachments, :copied_from_id
  end
end
