class CreatePeAssessmentGroups < ActiveRecord::Migration
  def change
    create_table :pe_assessment_groups do |t|
      t.references :pe_process
      t.references :pe_evaluation_group
      t.references :pe_member
      t.float :percentage

      t.timestamps
    end
    add_index :pe_assessment_groups, :pe_process_id
    add_index :pe_assessment_groups, :pe_evaluation_group_id
    add_index :pe_assessment_groups, :pe_member_id
  end
end
