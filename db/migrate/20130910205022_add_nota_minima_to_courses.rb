class AddNotaMinimaToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :nota_minima, :integer
  end
end
