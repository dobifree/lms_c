class AddAnswerTypeToMasterPollQuestion < ActiveRecord::Migration
  def change
    add_column :master_poll_questions, :answer_type, :integer, default: 0
  end
end
