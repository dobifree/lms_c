class AddNumPassedCoursesToProgramInstances < ActiveRecord::Migration
  def change
    add_column :program_instances, :num_passed_courses, :integer
  end
end
