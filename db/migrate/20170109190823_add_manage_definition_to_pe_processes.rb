class AddManageDefinitionToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :manage_definition, :boolean, default: false
  end
end
