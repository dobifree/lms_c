class AddCreatorIdToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :creator_id, :integer
    add_index :pe_questions, :creator_id
  end
end
