class CreateHrProcessCalibrationSessionMs < ActiveRecord::Migration
  def change
    create_table :hr_process_calibration_session_ms do |t|
      t.references :hr_process_calibration_session
      t.references :hr_process_user

      t.timestamps
    end
    add_index :hr_process_calibration_session_ms, :hr_process_calibration_session_id, name: 'hr_process_calibration_session_ms_cs_id'
    add_index :hr_process_calibration_session_ms, :hr_process_user_id, name: 'hr_process_calibration_session_ms_pu_id'
  end
end
