class AddReferenceDateToScholarshipAppItem < ActiveRecord::Migration
  def change
    add_column :scholarship_application_items, :reference_date_today, :boolean
    add_column :scholarship_application_items, :reference_date_specific, :date
  end
end
