class AddControlTotalAsistenciaToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :control_total_asistencia, :boolean, default: false
  end
end
