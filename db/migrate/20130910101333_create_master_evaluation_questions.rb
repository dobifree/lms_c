class CreateMasterEvaluationQuestions < ActiveRecord::Migration
  def change
    create_table :master_evaluation_questions do |t|
      t.text :texto
      t.boolean :obligatoria
      t.boolean :respuesta_unica
      t.float :puntaje
      t.integer :tiempo
      t.references :master_evaluation_topic
      t.references :master_evaluation

      t.timestamps
    end
    add_index :master_evaluation_questions, :master_evaluation_topic_id
    add_index :master_evaluation_questions, :master_evaluation_id
  end
end
