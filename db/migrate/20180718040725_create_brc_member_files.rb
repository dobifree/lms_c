class CreateBrcMemberFiles < ActiveRecord::Migration
  def change
    create_table :brc_member_files do |t|
      t.references :brc_member
      t.string :name
      t.string :file_name

      t.timestamps
    end
    add_index :brc_member_files, :brc_member_id
  end
end
