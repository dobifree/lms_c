class AddFeedbackProviderIdToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :feedback_provider_id, :integer
    add_index :pe_members, :feedback_provider_id
    add_index :pe_members, :pe_box_before_cal_id
  end
end
