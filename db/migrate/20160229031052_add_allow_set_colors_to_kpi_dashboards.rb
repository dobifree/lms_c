class AddAllowSetColorsToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :allow_set_colors, :boolean, default: false
  end
end
