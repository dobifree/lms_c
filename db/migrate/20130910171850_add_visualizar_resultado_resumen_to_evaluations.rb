class AddVisualizarResultadoResumenToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :visualizar_resultado_resumen, :boolean
  end
end
