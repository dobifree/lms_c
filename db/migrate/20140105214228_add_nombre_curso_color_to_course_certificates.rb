class AddNombreCursoColorToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :nombre_curso_color, :string, default: '000000'
  end
end
