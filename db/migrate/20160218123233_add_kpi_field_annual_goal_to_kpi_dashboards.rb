class AddKpiFieldAnnualGoalToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :kpi_field_annual_goal, :boolean, default: false
  end
end
