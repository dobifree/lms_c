class CreateKpiRols < ActiveRecord::Migration
  def change
    create_table :kpi_rols do |t|
      t.string :name
      t.references :kpi_dashboard

      t.timestamps
    end
    add_index :kpi_rols, :kpi_dashboard_id
  end
end
