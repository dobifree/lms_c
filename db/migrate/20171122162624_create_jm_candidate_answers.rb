class CreateJmCandidateAnswers < ActiveRecord::Migration
  def change
    create_table :jm_candidate_answers do |t|
      t.references :jm_candidate
      t.references :jm_characteristic
      t.integer :position
      t.boolean :active, :default => true
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.integer :prev_answer_id

      t.timestamps
    end
    add_index :jm_candidate_answers, :jm_candidate_id
    add_index :jm_candidate_answers, :jm_characteristic_id
    add_index :jm_candidate_answers, :prev_answer_id
  end
end
