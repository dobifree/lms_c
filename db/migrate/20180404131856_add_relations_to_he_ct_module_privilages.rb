class AddRelationsToHeCtModulePrivilages < ActiveRecord::Migration
  def change
    add_column :sc_register_to_validates, :recorder_id, :integer
    add_index :sc_register_to_validates, :recorder_id

    add_column :sc_records, :sc_user_to_register_id, :integer
    add_index :sc_records, :sc_user_to_register_id

    add_column :sc_records, :he_ct_module_privilege_id, :integer
    add_index :sc_records, :he_ct_module_privilege_id
  end
end
