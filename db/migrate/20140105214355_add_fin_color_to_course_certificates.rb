class AddFinColorToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :fin_color, :string, default: '000000'
  end
end
