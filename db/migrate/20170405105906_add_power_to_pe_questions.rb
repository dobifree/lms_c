class AddPowerToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :power, :float, default: 1
  end
end
