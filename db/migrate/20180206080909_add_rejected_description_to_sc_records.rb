class AddRejectedDescriptionToScRecords < ActiveRecord::Migration
  def change
    add_column :sc_records, :rejected_description, :text
  end
end
