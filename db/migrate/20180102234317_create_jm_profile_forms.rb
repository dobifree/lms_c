class CreateJmProfileForms < ActiveRecord::Migration
  def change
    create_table :jm_profile_forms do |t|
      t.string :name
      t.text :description
      t.text :layout
      t.boolean :active

      t.timestamps
    end
  end
end
