class RemoveCalibratedFromPeMembers < ActiveRecord::Migration
  def change
    remove_column :pe_members, :calibrated
  end
end
