class AddPercentageToKpiSets < ActiveRecord::Migration
  def change
    add_column :kpi_sets, :percentage, :float
  end
end
