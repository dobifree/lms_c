class AddFixedToPeAlternatives < ActiveRecord::Migration
  def change
    add_column :pe_alternatives, :fixed, :boolean, default: false
  end
end
