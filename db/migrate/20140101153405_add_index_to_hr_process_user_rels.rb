class AddIndexToHrProcessUserRels < ActiveRecord::Migration
  def change
    add_index :hr_process_user_rels, :hr_process_id
  end
end
