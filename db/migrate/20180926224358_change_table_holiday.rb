class ChangeTableHoliday < ActiveRecord::Migration
  def change
    remove_column :holidays, :user_id
    add_column :holidays, :description, :text
    add_column :holidays, :name, :string, default: 'Sin nombre'
  end
end