class AddStepQueryDefinitionsAcceptSubjectToBossToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_query_definitions_accept_subject_to_boss, :string
  end
end
