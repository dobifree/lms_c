class AddLabelAndColorCalculatedStatusToTrackingProcess < ActiveRecord::Migration
  def change
    add_column :tracking_processes, :not_started_label, :string
    add_column :tracking_processes, :not_started_color, :string
    add_column :tracking_processes, :in_definition_label, :string
    add_column :tracking_processes, :in_definition_color, :string
    add_column :tracking_processes, :in_redefinition_label, :string
    add_column :tracking_processes, :in_redefinition_color, :string
  end
end
