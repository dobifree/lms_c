class CreateBpRulesWarnings < ActiveRecord::Migration
  def change
    create_table :bp_rules_warnings do |t|
      t.references :bp_general_rules_vacation
      t.string :name
      t.text :description
      t.boolean :restrictive
      t.boolean :active, default: true
      t.integer :min_accumulated_days
      t.integer :max_accumulated_days
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id

      t.timestamps
    end
    add_index :bp_rules_warnings, :bp_general_rules_vacation_id, name: 'bp_warnings_general_id'
    add_index :bp_rules_warnings, :deactivated_by_user_id, name: 'bp_warnings_deact_user_id'
    add_index :bp_rules_warnings, :registered_by_user_id, name: 'bp_warnings_regis_user_id'
  end
end
