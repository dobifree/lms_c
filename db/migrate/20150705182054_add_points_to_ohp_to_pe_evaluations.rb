class AddPointsToOhpToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :points_to_ohp, :float, default: 10
  end
end
