class CreateJjCharacteristics < ActiveRecord::Migration
  def change
    create_table :jj_characteristics do |t|
      t.string :name
      t.integer :data_type_id
      t.integer :position
      t.boolean :public, :default => true
      t.references :j_characteristic_type

      t.timestamps
    end
    add_index :jj_characteristics, :j_characteristic_type_id
  end
end
