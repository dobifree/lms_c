class AddVigenteToHrProcessEvaluations < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluations, :vigente, :boolean, default: true
  end
end
