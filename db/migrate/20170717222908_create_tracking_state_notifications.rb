class CreateTrackingStateNotifications < ActiveRecord::Migration
  def change
    create_table :tracking_state_notifications do |t|
      t.references :tracking_form_state
      t.string :name
      t.text :description
      t.boolean :active
      t.string :to
      t.string :cc
      t.string :subject
      t.text :body

      t.timestamps
    end
    add_index :tracking_state_notifications, :tracking_form_state_id
  end
end
