class AddStepAssignAliasToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assign_alias, :string, default: 'Asignar evaluadores'
  end
end
