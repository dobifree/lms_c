class AddIndicatorUnitToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :indicator_unit, :string
  end
end
