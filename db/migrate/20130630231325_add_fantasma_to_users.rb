class AddFantasmaToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fantasma, :boolean, default: false
  end
end
