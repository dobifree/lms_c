class AddHasStepTrackingToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_tracking, :boolean, default: false
  end
end
