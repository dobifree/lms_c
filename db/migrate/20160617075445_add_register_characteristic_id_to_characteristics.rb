class AddRegisterCharacteristicIdToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :register_characteristic_id, :integer
    add_index :characteristics, :register_characteristic_id
  end
end
