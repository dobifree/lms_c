class AddEmailToAcceptDefinitionSubjectToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :email_to_accept_definition_subject, :string
  end
end
