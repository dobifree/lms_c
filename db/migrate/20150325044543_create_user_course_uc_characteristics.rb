class CreateUserCourseUcCharacteristics < ActiveRecord::Migration
  def change
    create_table :user_course_uc_characteristics do |t|
      t.references :user_course
      t.references :uc_characteristic
      t.text :value

      t.timestamps
    end
    add_index :user_course_uc_characteristics, :user_course_id
    add_index :user_course_uc_characteristics, :uc_characteristic_id
  end
end
