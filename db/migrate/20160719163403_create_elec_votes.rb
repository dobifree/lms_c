class CreateElecVotes < ActiveRecord::Migration
  def change
    create_table :elec_votes do |t|
      t.references :elec_round
      t.references :elec_process_category
      t.references :user
      t.integer :candidate_user_id

      t.timestamps
    end
    add_index :elec_votes, :elec_round_id
    add_index :elec_votes, :elec_process_category_id
    add_index :elec_votes, :user_id
    add_index :elec_votes, :candidate_user_id
  end
end
