class AddNotePlaceholderToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :note_placeholder, :string
  end
end
