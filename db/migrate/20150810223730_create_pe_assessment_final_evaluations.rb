class CreatePeAssessmentFinalEvaluations < ActiveRecord::Migration
  def change
    create_table :pe_assessment_final_evaluations do |t|
      t.references :pe_process
      t.references :pe_member
      t.references :pe_evaluation
      t.float :points
      t.float :percentage
      t.datetime :last_update
      t.boolean :registered_by_manager, default: false

      t.timestamps
    end
    add_index :pe_assessment_final_evaluations, :pe_process_id
    add_index :pe_assessment_final_evaluations, :pe_member_id
    add_index :pe_assessment_final_evaluations, :pe_evaluation_id
  end
end
