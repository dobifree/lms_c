class CreateBpItemOptions < ActiveRecord::Migration
  def change
    create_table :bp_item_options do |t|
      t.references :bp_item
      t.string :name
      t.text :description
      t.integer :position
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id
      t.integer :prev_item_option_id

      t.timestamps
    end
    add_index :bp_item_options, :bp_item_id
    add_index :bp_item_options, :prev_item_option_id
    add_index :bp_item_options, :deactivated_by_admin_id, name: 'bp_item_opt_deac_adm_id'
    add_index :bp_item_options, :registered_by_admin_id, name: 'bp_item_opt_regis_adm_id'
  end
end
