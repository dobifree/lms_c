class AddInicioColorToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :inicio_color, :string, default: '000000'
  end
end
