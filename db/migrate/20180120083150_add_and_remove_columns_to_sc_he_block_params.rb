class AddAndRemoveColumnsToScHeBlockParams < ActiveRecord::Migration
  def change
    add_column :sc_he_block_params, :registered_at, :datetime
    add_column :sc_he_block_params, :registered_by_user_id, :integer

    remove_column :sc_he_block_params, :entry, :exit

    add_index :sc_he_block_params, :registered_by_user_id
  end
end
