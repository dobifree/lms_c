class AddPorcentajeAvanceToUserCourse < ActiveRecord::Migration
  def change
    add_column :user_courses, :porcentaje_avance, :float, default: 0
  end
end
