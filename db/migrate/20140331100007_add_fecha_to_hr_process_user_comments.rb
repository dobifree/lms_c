class AddFechaToHrProcessUserComments < ActiveRecord::Migration
  def change
    add_column :hr_process_user_comments, :fecha, :datetime
  end
end
