class AddKpiDashboardIdToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :kpi_dashboard_id, :integer
    add_index :pe_elements, :kpi_dashboard_id
  end
end
