class AddCommentDateToPeMemberCompoundFeedbacks < ActiveRecord::Migration
  def change
    add_column :pe_member_compound_feedbacks, :comment_date, :date
  end
end
