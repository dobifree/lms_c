class CreateTrackingPeriodItems < ActiveRecord::Migration
  def change
    create_table :tracking_period_items do |t|
      t.references :tracking_period_list
      t.integer :position
      t.string :name
      t.datetime :from_date
      t.datetime :to_date
      t.boolean :active

      t.timestamps
    end
    add_index :tracking_period_items, :tracking_period_list_id
  end
end
