class AddBpGroupToGeneralRulesVacation < ActiveRecord::Migration
  def change
    add_column :bp_general_rules_vacations, :bp_group_id, :integer
    add_index :bp_general_rules_vacations, :bp_group_id
  end
end
