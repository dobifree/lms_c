class AddTrackerFieldsToSelProcess < ActiveRecord::Migration
  def change
    add_column :sel_processes, :registered_at, :datetime
    add_column :sel_processes, :registered_by_user_id, :integer
    add_column :sel_processes, :finished_by_user_id, :integer

    add_index :sel_processes, :finished_by_user_id
    add_index :sel_processes, :registered_by_user_id
  end
end
