class CreateDncStudents < ActiveRecord::Migration
  def change
    create_table :dnc_students do |t|
      t.references :dnc
      t.references :user

      t.timestamps
    end
    add_index :dnc_students, :dnc_id
    add_index :dnc_students, :user_id
  end
end
