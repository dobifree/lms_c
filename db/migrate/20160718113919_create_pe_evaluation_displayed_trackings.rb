class CreatePeEvaluationDisplayedTrackings < ActiveRecord::Migration
  def change
    create_table :pe_evaluation_displayed_trackings do |t|
      t.references :pe_evaluation
      t.integer :pe_displayed_evaluation_id
      t.integer :position

      t.timestamps
    end
    add_index :pe_evaluation_displayed_trackings, :pe_evaluation_id, name: 'pe_eval_disp_tracking_pe_eval_id'
    add_index :pe_evaluation_displayed_trackings, :pe_displayed_evaluation_id, name: 'pe_eval_disp_tracking_pe_disp_eval_id'
    add_index :pe_evaluation_displayed_trackings, :pe_displayed_evaluation_id, name: 'pe_eval_disp_tracking_pos'
  end
end
