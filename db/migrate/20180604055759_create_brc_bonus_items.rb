class CreateBrcBonusItems < ActiveRecord::Migration
  def change
    create_table :brc_bonus_items do |t|
      t.references :brc_process
      t.references :brc_member
      t.references :brc_category
      t.boolean :p_a, default: true
      t.float :percentage
      t.float :amount
      t.boolean :rejected, default: false
      t.text :val_comment

      t.timestamps
    end
    add_index :brc_bonus_items, :brc_process_id
    add_index :brc_bonus_items, :brc_member_id
    add_index :brc_bonus_items, :brc_category_id
  end
end
