class AddShowPregNivel1ToHrProcessEvaluations < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluations, :show_preg_nivel_1, :boolean, default: true
  end
end
