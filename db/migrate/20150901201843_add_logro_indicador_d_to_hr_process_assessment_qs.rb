class AddLogroIndicadorDToHrProcessAssessmentQs < ActiveRecord::Migration
  def change
    add_column :hr_process_assessment_qs, :logro_indicador_d, :string, default: ''
  end
end
