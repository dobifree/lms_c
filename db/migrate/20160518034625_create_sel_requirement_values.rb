class CreateSelRequirementValues < ActiveRecord::Migration
  def change
    create_table :sel_requirement_values do |t|
      t.references :sel_requirement
      t.references :sel_req_item
      t.text :value
      t.references :sel_req_option

      t.timestamps
    end
    add_index :sel_requirement_values, :sel_requirement_id
    add_index :sel_requirement_values, :sel_req_item_id
    add_index :sel_requirement_values, :sel_req_option_id
  end
end
