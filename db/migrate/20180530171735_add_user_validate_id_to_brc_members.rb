class AddUserValidateIdToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :user_validate_id, :integer
    add_index :brc_members, :user_validate_id
  end
end
