class AddMilestoneToUserCourseUnits < ActiveRecord::Migration
  def change
    add_column :user_course_units, :milestone, :integer, default: 0
  end
end
