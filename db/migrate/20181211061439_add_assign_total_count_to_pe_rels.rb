class AddAssignTotalCountToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :assign_total_count, :boolean, default: false
  end
end
