class AddActivoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :activo, :boolean, default: true
  end
end
