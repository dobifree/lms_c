class AddNombreLetraToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :nombre_letra, :string, default: 'Helvetica'
  end
end
