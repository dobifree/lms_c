class AddShowPreviewResultsUserToPollProcesses < ActiveRecord::Migration
  def change
    add_column :poll_processes, :show_preview_results_user, :boolean, default: false
  end
end
