class AddMaxItemsToJmCharacteristic < ActiveRecord::Migration
  def change
    add_column :jm_characteristics, :max_qty_items, :integer, :default => 1
  end
end
