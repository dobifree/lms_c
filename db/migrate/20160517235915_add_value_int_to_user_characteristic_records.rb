class AddValueIntToUserCharacteristicRecords < ActiveRecord::Migration
  def change
    add_column :user_characteristic_records, :value_int, :int
  end
end
