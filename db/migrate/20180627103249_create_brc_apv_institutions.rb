class CreateBrcApvInstitutions < ActiveRecord::Migration
  def change
    create_table :brc_apv_institutions do |t|
      t.string :name

      t.timestamps
    end
  end
end
