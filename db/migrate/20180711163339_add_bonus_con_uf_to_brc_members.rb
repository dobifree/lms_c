class AddBonusConUfToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :bonus_con_uf, :decimal, :precision => 20, :scale => 4, :default => 0
  end
end
