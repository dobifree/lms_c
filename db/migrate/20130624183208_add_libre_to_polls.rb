class AddLibreToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :libre, :boolean, default: true
  end
end
