class AddVirtualToUnits < ActiveRecord::Migration
  def change
    add_column :units, :virtual, :boolean, default: true
  end
end
