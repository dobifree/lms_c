class AddSelectedFieldsToSellApplicants < ActiveRecord::Migration
  def change
    add_column :sel_applicants, :selected, :boolean, default: false
    add_column :sel_applicants, :selected_at, :datetime
    add_column :sel_applicants, :selected_by, :integer
  end
end
