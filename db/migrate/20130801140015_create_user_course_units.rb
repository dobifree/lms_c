class CreateUserCourseUnits < ActiveRecord::Migration
  def change
    create_table :user_course_units do |t|
      t.references :user_course
      t.references :unit
      t.datetime :inicio
      t.datetime :fin
      t.boolean :finalizada, default: false

      t.timestamps
    end
    add_index :user_course_units, :user_course_id
    add_index :user_course_units, :unit_id
  end
end
