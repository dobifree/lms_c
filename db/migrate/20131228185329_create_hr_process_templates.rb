class CreateHrProcessTemplates < ActiveRecord::Migration
  def change
    create_table :hr_process_templates do |t|
      t.string :nombre
      t.string :descripcion
      t.boolean :activo, default: true

      t.timestamps
    end
  end
end
