class AddProcessToScholarshipApplication < ActiveRecord::Migration
  def change
    add_column :scholarship_applications, :scholarship_process_id, :integer
    add_index :scholarship_applications, :scholarship_process_id
  end
end
