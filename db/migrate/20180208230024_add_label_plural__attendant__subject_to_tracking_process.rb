class AddLabelPluralAttendantSubjectToTrackingProcess < ActiveRecord::Migration
  def change
    add_column :tracking_processes, :label_attendant_plural, :string
    add_column :tracking_processes, :label_subject_plural, :string
  end
end
