class AddAliasParPluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_par_plu, :string, default: 'Pares'
  end
end
