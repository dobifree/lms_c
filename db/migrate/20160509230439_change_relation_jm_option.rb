class ChangeRelationJmOption < ActiveRecord::Migration
  def change
    remove_columns :jm_options, :jm_characteristic_id
    add_column :jm_options, :jm_list_id, :integer
  end
end
