class AddDefaultUnitToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :default_unit, :string
  end
end
