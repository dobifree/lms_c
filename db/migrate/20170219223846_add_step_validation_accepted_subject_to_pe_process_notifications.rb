class AddStepValidationAcceptedSubjectToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_validation_accepted_subject, :string
  end
end
