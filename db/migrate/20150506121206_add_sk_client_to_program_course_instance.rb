class AddSkClientToProgramCourseInstance < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :sk_client, :string
  end
end
