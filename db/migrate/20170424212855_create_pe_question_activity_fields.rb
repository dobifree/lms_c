class CreatePeQuestionActivityFields < ActiveRecord::Migration
  def change
    create_table :pe_question_activity_fields do |t|
      t.references :pe_evaluation
      t.integer :position
      t.string :name
      t.integer :field_type, default: 0
      t.references :pe_question_activity_field_list

      t.timestamps
    end
    add_index :pe_question_activity_fields, :pe_evaluation_id
    add_index :pe_question_activity_fields, :pe_question_activity_field_list_id, name: :pe_question_activity_fields_list_id
  end
end
