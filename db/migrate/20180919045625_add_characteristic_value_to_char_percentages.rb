class AddCharacteristicValueToCharPercentages < ActiveRecord::Migration
  def change
    remove_column :brg_char_percentages, :characteristic_id
    add_column :brg_char_percentages, :characteristic_value_id, :integer
    add_index :brg_char_percentages, :characteristic_value_id
  end
end
