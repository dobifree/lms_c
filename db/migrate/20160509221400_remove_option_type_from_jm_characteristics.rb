class RemoveOptionTypeFromJmCharacteristics < ActiveRecord::Migration
  def change
    remove_columns :jm_characteristics, :option_type
  end
end
