class AddNewFieldsToPollProcesses < ActiveRecord::Migration
  def change
    add_column :poll_processes, :grid_view, :boolean, :default => false
    add_column :poll_processes, :flow, :integer, :default => 0
    add_column :poll_processes, :show_group_names, :boolean, :default => false
  end
end
