class CreateTrackingForms < ActiveRecord::Migration
  def change
    create_table :tracking_forms do |t|
      t.references :tracking_process
      t.integer :position
      t.string :name
      t.text :description
      t.boolean :trackable_by_attendant
      t.boolean :trackable_by_subject
      t.boolean :track_validation_by_counterpart

      t.timestamps
    end
    add_index :tracking_forms, :tracking_process_id
  end
end
