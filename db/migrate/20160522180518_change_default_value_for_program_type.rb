class ChangeDefaultValueForProgramType < ActiveRecord::Migration
  def change
    change_column :programs, :especifico, :boolean, default: true
  end
end
