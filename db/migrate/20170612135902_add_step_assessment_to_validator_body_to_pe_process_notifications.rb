class AddStepAssessmentToValidatorBodyToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_assessment_to_validator_body, :text
  end
end
