class AddHasStepDefinitionByUsersAcceptedToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_definition_by_users_accepted, :boolean, default: false
  end
end
