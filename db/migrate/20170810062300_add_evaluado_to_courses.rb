class AddEvaluadoToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :evaluado, :boolean, default: true
  end
end
