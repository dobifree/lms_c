class ChangeNameDeactAndRegisToBpRulesWarnings < ActiveRecord::Migration

  def change
    rename_column :bp_rules_warnings, :deactivated_by_user_id, :deactivated_by_admin_id
    rename_column :bp_rules_warnings, :registered_by_user_id, :registered_by_admin_id

    add_index :bp_rules_warnings, :deactivated_by_admin_id, name: 'bp_warnings_deact_admin_id'
    add_index :bp_rules_warnings, :registered_by_admin_id, name: 'bp_warnings_regis_admin_id'
  end
end
