class CreateBenCustomReportAttributeFilters < ActiveRecord::Migration
  def change
    create_table :ben_custom_report_attribute_filters do |t|
      t.references :ben_custom_report
      t.string :name
      t.string :match_value_string
      t.text :match_value_text
      t.date :match_value_date
      t.integer :match_value_int
      t.float :match_value_float
      t.integer :match_value_char_id
      t.decimal :match_value_decimal, :precision => 12, :scale => 4

      t.timestamps
    end
    add_index :ben_custom_report_attribute_filters, :ben_custom_report_id, name: 'custom_report_attr_report_id'
  end
end
