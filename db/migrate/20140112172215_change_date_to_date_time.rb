class ChangeDateToDateTime < ActiveRecord::Migration

  def change

    change_column :hr_processes, :fecha_inicio, :datetime
    change_column :hr_processes, :fecha_fin, :datetime
    change_column :hr_process_evaluations, :fecha_inicio_eval_jefe, :datetime
    change_column :hr_process_evaluations, :fecha_fin_eval_jefe, :datetime
    change_column :hr_process_evaluations, :fecha_inicio_eval_auto, :datetime
    change_column :hr_process_evaluations, :fecha_fin_eval_auto, :datetime
    change_column :hr_process_evaluations, :fecha_inicio_eval_sub, :datetime
    change_column :hr_process_evaluations, :fecha_fin_eval_sub, :datetime
    change_column :hr_process_evaluations, :fecha_inicio_eval_par, :datetime
    change_column :hr_process_evaluations, :fecha_fin_eval_par, :datetime
    change_column :planning_processes, :fecha_fin, :datetime
    change_column :planning_process_company_units, :fecha_fin, :datetime

  end

end
