class AddActiveUnitToLmsCharacteristics < ActiveRecord::Migration
  def change
    add_column :lms_characteristics, :active_unit, :boolean, default: false
  end
end
