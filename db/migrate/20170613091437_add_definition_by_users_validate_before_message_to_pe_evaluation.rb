class AddDefinitionByUsersValidateBeforeMessageToPeEvaluation < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :definition_by_user_validate_before_message, :string, default: 'Confirmo que valido la definición'
  end
end
