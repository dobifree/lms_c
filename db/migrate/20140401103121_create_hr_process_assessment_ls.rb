class CreateHrProcessAssessmentLs < ActiveRecord::Migration
  def change
    create_table :hr_process_assessment_ls do |t|
      t.references :hr_process_evaluation_q
      t.references :hr_process_evaluation_manual_q
      t.references :hr_process_user
      t.integer :hr_process_user_eval_id
      t.float :resultado_puntos
      t.float :resultado_porcentaje
      t.float :logro_indicador
      t.datetime :fecha

      t.timestamps
    end
    add_index :hr_process_assessment_ls, :hr_process_evaluation_q_id, name: 'hr_process_assessment_ls_eq_id'
    add_index :hr_process_assessment_ls, :hr_process_evaluation_manual_q_id, name: 'hr_process_assessment_ls_emq_id'
    add_index :hr_process_assessment_ls, :hr_process_user_id, name: 'hr_process_assessment_ls_pu_id'
  end
end
