class AddTipoContratoSecuToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :tipo_contrato_secu, :boolean, default: false
  end
end
