class AddPublicPeBoxToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :public_pe_box, :boolean, default: true
  end
end
