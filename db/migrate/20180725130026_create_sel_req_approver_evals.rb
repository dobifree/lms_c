class CreateSelReqApproverEvals < ActiveRecord::Migration
  def change
    create_table :sel_req_approver_evals do |t|
      t.references :sel_req_approver
      t.boolean :evaluation
      t.text :comment

      t.timestamps
    end
    add_index :sel_req_approver_evals, :sel_req_approver_id
  end
end
