class AddColumnMoneyToLicRequest < ActiveRecord::Migration
  def change
    add_column :lic_requests, :money, :decimal, precision: 12, scale: 4
    change_column :bp_licenses, :money, :decimal, precision: 12, scale: 4
    rename_column :lic_requests, :money_paid, :paid
    rename_column :lic_requests, :money_paid_by_user_id, :paid_by_user_id
    rename_column :lic_requests, :money_paid_at, :paid_at
    change_column_default :lic_requests, :paid, nil
  end
end
