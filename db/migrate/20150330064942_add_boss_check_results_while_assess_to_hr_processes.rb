class AddBossCheckResultsWhileAssessToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :boss_check_results_while_assess, :boolean
  end
end
