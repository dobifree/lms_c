class AddStepAssessmentMenuToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_menu, :boolean, default: true
  end
end
