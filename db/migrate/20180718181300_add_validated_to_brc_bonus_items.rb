class AddValidatedToBrcBonusItems < ActiveRecord::Migration
  def change
    add_column :brc_bonus_items, :validated, :boolean, default: false
  end
end
