class AddNotaBaseToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :nota_base, :integer, default: 0
  end
end
