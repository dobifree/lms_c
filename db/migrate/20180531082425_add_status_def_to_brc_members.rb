class AddStatusDefToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :status_def, :boolean
  end
end
