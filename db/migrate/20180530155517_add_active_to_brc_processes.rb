class AddActiveToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :active, :boolean, default: true
  end
end
