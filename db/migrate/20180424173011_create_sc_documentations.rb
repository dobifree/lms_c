class CreateScDocumentations < ActiveRecord::Migration
  def change
    create_table :sc_documentations do |t|
      t.references :user
      t.references :sc_period
      t.string :doc_path

      t.timestamps
    end
    add_index :sc_documentations, :user_id
    add_index :sc_documentations, :sc_period_id
  end
end
