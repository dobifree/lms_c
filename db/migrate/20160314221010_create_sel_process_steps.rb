class CreateSelProcessSteps < ActiveRecord::Migration
  def change
    create_table :sel_process_steps do |t|
      t.references :sel_process
      t.references :sel_step

      t.timestamps
    end
    add_index :sel_process_steps, :sel_process_id
    add_index :sel_process_steps, :sel_step_id
  end
end
