class AddRegisterUserCharacteristicIdToUserCharacteristics < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :register_user_characteristic_id, :integer
    add_index :user_characteristics, :register_user_characteristic_id
  end
end
