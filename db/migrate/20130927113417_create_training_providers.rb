class CreateTrainingProviders < ActiveRecord::Migration
  def change
    create_table :training_providers do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
