class AddUserDeleteReasonIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :user_delete_reason_id, :integer
    add_index :users, :user_delete_reason_id
  end
end
