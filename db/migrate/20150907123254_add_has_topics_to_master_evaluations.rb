class AddHasTopicsToMasterEvaluations < ActiveRecord::Migration
  def change
    add_column :master_evaluations, :has_topics, :boolean, default: false
  end
end
