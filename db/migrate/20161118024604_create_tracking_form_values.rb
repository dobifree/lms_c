class CreateTrackingFormValues < ActiveRecord::Migration
  def change
    create_table :tracking_form_values do |t|
      t.references :tracking_form_answer
      t.references :tracking_form_item
      t.text :value
      t.references :tracking_list_item
      t.string :hashed_id

      t.timestamps
    end
    add_index :tracking_form_values, :tracking_form_answer_id
    add_index :tracking_form_values, :tracking_form_item_id
    add_index :tracking_form_values, :tracking_list_item_id
  end
end
