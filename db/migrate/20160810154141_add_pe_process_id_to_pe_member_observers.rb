class AddPeProcessIdToPeMemberObservers < ActiveRecord::Migration
  def change
    add_column :pe_member_observers, :pe_process_id, :integer
    add_index :pe_member_observers, :pe_process_id
    add_index :pe_member_observers, :pe_member_observed_id
    add_index :pe_member_observers, :observer_id
  end
end
