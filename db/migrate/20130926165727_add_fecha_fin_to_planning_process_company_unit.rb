class AddFechaFinToPlanningProcessCompanyUnit < ActiveRecord::Migration
  def change
    add_column :planning_process_company_units, :fecha_fin, :date
  end
end
