class AddRolSecurityToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :rol_security, :boolean
  end
end
