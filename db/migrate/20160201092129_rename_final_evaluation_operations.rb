class RenameFinalEvaluationOperations < ActiveRecord::Migration
  def change
    rename_column :pe_evaluations, :final_evaluation_operation, :final_evaluation_operation_evaluators
  end
end
