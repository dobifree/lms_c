class AddFormatRejectedCommentToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :format_rejected_comment, :string
  end
end
