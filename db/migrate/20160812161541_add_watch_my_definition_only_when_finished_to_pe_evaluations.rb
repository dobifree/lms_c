class AddWatchMyDefinitionOnlyWhenFinishedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :watch_my_definition_only_when_finished, :boolean, default: true
  end
end
