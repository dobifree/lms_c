class AddFieldMinMaxToSelOptions < ActiveRecord::Migration
  def change
    add_column :sel_options, :min_value, :integer
    add_column :sel_options, :max_value, :integer
  end
end
