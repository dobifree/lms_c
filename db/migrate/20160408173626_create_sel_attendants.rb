class CreateSelAttendants < ActiveRecord::Migration
  def change
    create_table :sel_attendants do |t|
      t.references :sel_step_candidate
      t.references :user
      t.boolean :role

      t.timestamps
    end
    add_index :sel_attendants, :sel_step_candidate_id
    add_index :sel_attendants, :user_id
  end
end
