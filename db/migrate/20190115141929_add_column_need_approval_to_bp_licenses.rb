class AddColumnNeedApprovalToBpLicenses < ActiveRecord::Migration
  def change
    add_column :bp_licenses, :need_approval, :boolean, default: true
  end
end
