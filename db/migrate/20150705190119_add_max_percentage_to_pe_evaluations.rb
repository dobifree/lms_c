class AddMaxPercentageToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :max_percentage, :float, default: 120
  end
end
