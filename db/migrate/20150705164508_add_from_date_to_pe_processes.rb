class AddFromDateToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :from_date, :datetime
  end
end
