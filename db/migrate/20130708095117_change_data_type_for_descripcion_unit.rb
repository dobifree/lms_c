class ChangeDataTypeForDescripcionUnit < ActiveRecord::Migration
  def up
  	change_table :units do |t|
      t.change :descripcion, :text
    end
  end

  def down
  end
end
