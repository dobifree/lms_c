class AddBlogFormIdToBlogEntry < ActiveRecord::Migration
  def change
    add_column :blog_entries, :blog_form_id, :integer
    add_index :blog_entries, :blog_form_id
  end
end
