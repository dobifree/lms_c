class AddPosRepConsolidatedFinalResultsToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_rep_consolidated_final_results, :integer
    add_index :pe_characteristics, :pos_rep_consolidated_final_results, name: 'pe_char_pos_rep_cons_fin_res'
  end
end
