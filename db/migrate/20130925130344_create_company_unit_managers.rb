class CreateCompanyUnitManagers < ActiveRecord::Migration
  def change
    create_table :company_unit_managers do |t|
      t.references :planning_process_company_unit
      t.references :user

      t.timestamps
    end
    add_index :company_unit_managers, :planning_process_company_unit_id
    add_index :company_unit_managers, :user_id
  end
end
