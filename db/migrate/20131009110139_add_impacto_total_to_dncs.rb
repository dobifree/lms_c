class AddImpactoTotalToDncs < ActiveRecord::Migration
  def change
    add_column :dncs, :impacto_total, :float
  end
end
