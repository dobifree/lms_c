class AddFromDateWorkToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :from_date_work, :boolean, default: false
  end
end
