class AddActiveGuiSelectionToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_gui_selection, :boolean, default: false
  end
end
