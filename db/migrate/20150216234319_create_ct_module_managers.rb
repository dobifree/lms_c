class CreateCtModuleManagers < ActiveRecord::Migration
  def change
    create_table :ct_module_managers do |t|
      t.references :ct_module
      t.references :user

      t.timestamps
    end
    add_index :ct_module_managers, :ct_module_id
    add_index :ct_module_managers, :user_id
  end
end
