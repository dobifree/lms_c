class AddDisplayResultsInMyPeopleToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :display_results_in_my_people, :boolean, default: false
  end
end
