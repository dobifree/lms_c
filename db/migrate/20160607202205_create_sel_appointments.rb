class CreateSelAppointments < ActiveRecord::Migration
  def change
    create_table :sel_appointments do |t|
      t.references :sel_process_step
      t.text :description
      t.datetime :from_datetime
      t.datetime :to_datetime
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :sel_appointments, :sel_process_step_id
    add_index :sel_appointments, :registered_by_user_id
  end
end
