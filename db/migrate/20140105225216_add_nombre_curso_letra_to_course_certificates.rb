class AddNombreCursoLetraToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :nombre_curso_letra, :string, default: 'Helvetica'
  end
end
