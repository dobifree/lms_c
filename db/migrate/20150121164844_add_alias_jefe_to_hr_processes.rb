class AddAliasJefeToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_jefe, :string, default: 'Supervisor'
  end
end
