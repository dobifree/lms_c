class CreateHrProcessEvaluationAs < ActiveRecord::Migration
  def change
    create_table :hr_process_evaluation_as do |t|
      t.integer :valor
      t.text :texto
      t.references :hr_process_evaluation_q

      t.timestamps
    end
    add_index :hr_process_evaluation_as, :hr_process_evaluation_q_id
  end
end
