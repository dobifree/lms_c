class CreatePlanningProcessCompanyUnits < ActiveRecord::Migration
  def change
    create_table :planning_process_company_units do |t|
      t.references :planning_process
      t.references :company_unit

      t.timestamps
    end
    add_index :planning_process_company_units, :planning_process_id
    add_index :planning_process_company_units, :company_unit_id
  end
end
