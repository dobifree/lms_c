class AddStepAssessmentUniqToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_uniq, :boolean
  end
end
