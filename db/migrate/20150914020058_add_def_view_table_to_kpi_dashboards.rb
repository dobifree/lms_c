class AddDefViewTableToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :def_view_table, :boolean, default: true
  end
end
