class AddLicEventIdToLicItemValue < ActiveRecord::Migration
  def change
    add_column :lic_item_values, :lic_event_id, :integer
    add_index :lic_item_values, :lic_event_id
  end
end
