class AddHasStepQueryResultsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_query_results, :boolean, default: false
  end
end
