class AddNumeroOportunidadesToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :numero_oportunidades, :integer, default: 1
  end
end
