class CreatePeEvaluationRels < ActiveRecord::Migration
  def change
    create_table :pe_evaluation_rels do |t|
      t.references :pe_evaluation
      t.references :pe_rel
      t.float :weight, default: 0
      t.datetime :from_date
      t.datetime :to_date

      t.timestamps
    end
    add_index :pe_evaluation_rels, :pe_evaluation_id
    add_index :pe_evaluation_rels, :pe_rel_id
  end
end
