class CreateHrProcesses < ActiveRecord::Migration
  def change
    create_table :hr_processes do |t|
      t.string :nombre
      t.boolean :abierto, default: true
      t.date :fecha_inicio
      t.date :fecha_fin
      t.integer :year

      t.timestamps
    end
  end
end
