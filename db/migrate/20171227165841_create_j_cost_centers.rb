class CreateJCostCenters < ActiveRecord::Migration
  def change
    create_table :j_cost_centers do |t|
      t.string :name
      t.decimal :budget, :precision => 60, :scale => 4
      t.boolean :active, :default => true

      t.timestamps
    end
  end
end
