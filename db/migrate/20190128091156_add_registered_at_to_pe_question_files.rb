class AddRegisteredAtToPeQuestionFiles < ActiveRecord::Migration
  def change
    add_column :pe_question_files, :registered_at, :datetime
  end
end
