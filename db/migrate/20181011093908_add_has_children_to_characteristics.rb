class AddHasChildrenToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :has_children, :boolean
  end
end
