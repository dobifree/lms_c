class CreateProgramCoursePollSummaries < ActiveRecord::Migration
  def change
    create_table :program_course_poll_summaries do |t|
      t.references :program_course
      t.references :poll
      t.integer :grupo
      t.references :master_poll_question
      t.references :master_poll_alternative
      t.integer :numero_respuestas

      t.timestamps
    end
    add_index :program_course_poll_summaries, :program_course_id, name: 'pcps_pc_id'
    add_index :program_course_poll_summaries, :poll_id, name: 'pcps_p_id'
    add_index :program_course_poll_summaries, :master_poll_question_id, name: 'pcps_mpq_id'
    add_index :program_course_poll_summaries, :master_poll_alternative_id, name: 'pcps_mpa_id'
  end
end
