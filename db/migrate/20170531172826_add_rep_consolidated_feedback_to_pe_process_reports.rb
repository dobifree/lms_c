class AddRepConsolidatedFeedbackToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_consolidated_feedback, :boolean, default: false
  end
end
