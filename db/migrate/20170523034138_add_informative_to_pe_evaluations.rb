class AddInformativeToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :informative, :boolean, default: false
  end
end
