class CreateUserDeleteTypes < ActiveRecord::Migration
  def change
    create_table :user_delete_types do |t|
      t.integer :position
      t.string :description

      t.timestamps
    end
  end
end
