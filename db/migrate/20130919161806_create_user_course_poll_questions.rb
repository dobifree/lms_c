class CreateUserCoursePollQuestions < ActiveRecord::Migration
  def change
    create_table :user_course_poll_questions do |t|
      t.references :user_course_poll
      t.references :master_poll
      t.references :master_poll_question
      t.boolean :respondida
      t.string :alternativas
      t.datetime :fecha_respuesta

      t.timestamps
    end
    add_index :user_course_poll_questions, :user_course_poll_id
    add_index :user_course_poll_questions, :master_poll_id
    add_index :user_course_poll_questions, :master_poll_question_id
  end
end
