class CreateBlogFormItems < ActiveRecord::Migration
  def change
    create_table :blog_form_items do |t|
      t.references :blog_form
      t.integer :position
      t.string :name
      t.text :description
      t.integer :item_type
      t.references :blog_list

      t.timestamps
    end
    add_index :blog_form_items, :blog_form_id
    add_index :blog_form_items, :blog_list_id
  end
end
