class CreatePeMemberObservers < ActiveRecord::Migration
  def change
    create_table :pe_member_observers do |t|
      t.integer :pe_member_observed_id
      t.integer :observer_id

      t.timestamps
    end
  end
end
