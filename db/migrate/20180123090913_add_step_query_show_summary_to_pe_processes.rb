class AddStepQueryShowSummaryToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_query_show_summary, :boolean, :default => false
  end
end
