class AddFinalizadaToHrProcessAssessmentDs < ActiveRecord::Migration
  def change
    add_column :hr_process_assessment_ds, :finalizada, :boolean, default: false
  end
end
