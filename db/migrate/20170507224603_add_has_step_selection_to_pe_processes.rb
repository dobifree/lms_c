class AddHasStepSelectionToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_selection, :boolean, default: false
  end
end
