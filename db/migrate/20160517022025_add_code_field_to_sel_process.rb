class AddCodeFieldToSelProcess < ActiveRecord::Migration
  def change
    add_column :sel_processes, :code, :string
  end
end
