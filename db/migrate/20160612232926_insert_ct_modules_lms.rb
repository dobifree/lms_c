class InsertCtModulesLms < ActiveRecord::Migration
  def change
    execute "insert into ct_modules set cod='lms', name='LMS', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'lms').count == 0
  end
end
