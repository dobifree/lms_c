class AddQ3ToJPayBands < ActiveRecord::Migration
  def change
    add_column :j_pay_bands, :q3, :decimal, :precision => 15, :scale => 4
  end
end
