class AddInRedefinitionFieldToTrackingParticipants < ActiveRecord::Migration
  def change
    add_column :tracking_participants, :in_redefinition, :boolean, :default => false
  end
end
