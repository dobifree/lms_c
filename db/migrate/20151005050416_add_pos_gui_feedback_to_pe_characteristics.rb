class AddPosGuiFeedbackToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_gui_feedback, :integer
    add_index :pe_characteristics, :pos_gui_feedback
  end
end
