class AddManagedByManagerToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :managed_by_manager, :boolean, default: false
  end
end
