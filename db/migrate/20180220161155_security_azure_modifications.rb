class SecurityAzureModifications < ActiveRecord::Migration
  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :active_directory
      add_column :companies, :active_directory, :boolean, :default => false
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end

end