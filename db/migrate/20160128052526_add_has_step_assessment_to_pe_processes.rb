class AddHasStepAssessmentToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_assessment, :boolean, default: false
  end
end
