class CreateBdBackups < ActiveRecord::Migration

  def connection
    @connection = BdBackup.connection
  end

  def change
    unless table_exists? :bd_backups
      create_table :bd_backups do |t|
        t.string :file_name
        t.text :description
        t.string :database
        t.text :specific_tables
        t.integer :generated_by_admin_id
        t.datetime :generated_at

        t.timestamps
      end
    end

    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end
end
