class AddPorcentajeParticipanteSenceToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :porcentaje_participante_sence, :boolean, default: false
  end
end
