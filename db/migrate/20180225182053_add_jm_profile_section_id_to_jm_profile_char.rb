class AddJmProfileSectionIdToJmProfileChar < ActiveRecord::Migration
  def change
    add_column :jm_profile_chars, :jm_profile_section_id, :integer
    add_index :jm_profile_chars, :jm_profile_section_id
  end
end
