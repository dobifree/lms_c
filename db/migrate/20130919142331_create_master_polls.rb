class CreateMasterPolls < ActiveRecord::Migration
  def change
    create_table :master_polls do |t|
      t.integer :numero
      t.string :nombre
      t.text :descripcion
      t.string :tags

      t.timestamps
    end
  end
end
