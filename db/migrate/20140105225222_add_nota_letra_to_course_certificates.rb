class AddNotaLetraToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :nota_letra, :string, default: 'Helvetica'
  end
end
