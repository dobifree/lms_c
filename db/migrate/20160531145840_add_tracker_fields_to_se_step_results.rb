class AddTrackerFieldsToSeStepResults < ActiveRecord::Migration
  def change
    add_column :sel_step_results, :registered_at, :datetime
  end
end
