class AddPrevToHolCharacteristic < ActiveRecord::Migration
  def change
    add_column :holiday_characteristics, :prev_hol_char_id, :integer
    add_index :holiday_characteristics, :prev_hol_char_id
  end
end
