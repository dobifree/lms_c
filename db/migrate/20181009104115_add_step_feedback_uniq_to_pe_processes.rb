class AddStepFeedbackUniqToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_uniq, :boolean
  end
end
