class AddSubjectUserTo < ActiveRecord::Migration
  def change
    add_column :scholarship_applications, :subject_user_id, :integer
    add_index :scholarship_applications, :subject_user_id
  end
end
