class AddValidatedToPeMemberRels < ActiveRecord::Migration
  def change
    add_column :pe_member_rels, :validated, :boolean, default: false
  end
end
