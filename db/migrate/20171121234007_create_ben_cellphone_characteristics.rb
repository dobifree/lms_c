class CreateBenCellphoneCharacteristics < ActiveRecord::Migration
  def change
    create_table :ben_cellphone_characteristics do |t|
      t.references :characteristic
      t.boolean :active_search_people, default: false
      t.integer :pos_search_people
      t.boolean :active_reporting, default: false
      t.integer :pos_reporting

      t.timestamps
    end
    add_index :ben_cellphone_characteristics, :characteristic_id
  end
end
