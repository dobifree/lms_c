class AddShowHeaderInLoginToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :show_header_in_login, :boolean, default: true
  end
end
