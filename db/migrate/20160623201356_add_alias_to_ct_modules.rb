class AddAliasToCtModules < ActiveRecord::Migration
  def change
    add_column :ct_modules, :alias, :string
  end
end
