class AddDespueDeEvaluationIdToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :despues_de_evaluation_id, :integer
  end
end
