class CreateInformativeDocuments < ActiveRecord::Migration
  def change
    create_table :informative_documents do |t|
      t.integer :position
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
