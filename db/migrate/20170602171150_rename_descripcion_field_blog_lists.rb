class RenameDescripcionFieldBlogLists < ActiveRecord::Migration
  def change
    rename_column :blog_lists, :descripcion, :description
  end
end
