class AddStepQueryDefinitionsAcceptSubjectToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_query_definitions_accept_subject, :string
  end
end
