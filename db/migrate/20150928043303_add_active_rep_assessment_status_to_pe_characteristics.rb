class AddActiveRepAssessmentStatusToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_rep_assessment_status, :boolean, default: false
  end
end
