class CreateSelProcesses < ActiveRecord::Migration
  def change
    create_table :sel_processes do |t|
      t.references :sel_template
      t.string :name
      t.text :description
      t.integer :qty_required
      t.integer :source
      t.boolean :apply_available
      t.date :from_date
      t.date :to_date
      t.integer :owner_user_id
      t.date :finished_at

      t.timestamps
    end
    add_index :sel_processes, :sel_template_id
  end
end
