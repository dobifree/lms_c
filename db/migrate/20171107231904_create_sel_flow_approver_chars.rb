class CreateSelFlowApproverChars < ActiveRecord::Migration
  def change
    create_table :sel_flow_approver_chars do |t|
      t.references :sel_flow_approver
      t.references :characteristic
      t.integer :match_type
      t.text :match_value_text
      t.datetime :match_value_date
      t.string :match_value_string
      t.integer :match_value_int
      t.integer :match_value_char_id

      t.timestamps
    end
    add_index :sel_flow_approver_chars, :sel_flow_approver_id
    add_index :sel_flow_approver_chars, :characteristic_id
    add_index :sel_flow_approver_chars, :match_value_char_id
  end
end
