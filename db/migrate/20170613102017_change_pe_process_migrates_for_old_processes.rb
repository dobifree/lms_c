class ChangePeProcessMigratesForOldProcesses < ActiveRecord::Migration
  def change
    PeEvaluation.all.each do |pe_evaluation|
      pe_process_notification_def = pe_evaluation.build_pe_process_notification_def
      pe_process_notification_def.pe_process = pe_evaluation.pe_process
      pe_process_notification_def.save
    end
  end
end
