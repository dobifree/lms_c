class AddMandatoryToJmProfileChars < ActiveRecord::Migration
  def change
    add_column :jm_profile_chars, :mandatory, :boolean, default: false
  end
end