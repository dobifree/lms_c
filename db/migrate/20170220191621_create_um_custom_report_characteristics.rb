class CreateUmCustomReportCharacteristics < ActiveRecord::Migration
  def change
    create_table :um_custom_report_characteristics do |t|
      t.references :um_custom_report
      t.references :um_characteristic
      t.integer :position

      t.timestamps
    end
    add_index :um_custom_report_characteristics, :um_custom_report_id
    add_index :um_custom_report_characteristics, :um_characteristic_id
  end
end
