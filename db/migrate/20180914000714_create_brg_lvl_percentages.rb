class CreateBrgLvlPercentages < ActiveRecord::Migration
  def change
    create_table :brg_lvl_percentages do |t|
      t.references :jj_characteristic_value
      t.float :max_prt_bonus
      t.float :pond_company
      t.float :pond_individual
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :brg_lvl_percentages, :jj_characteristic_value_id
    add_index :brg_lvl_percentages, :registered_by_user_id
  end
end
