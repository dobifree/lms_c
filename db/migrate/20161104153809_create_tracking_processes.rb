class CreateTrackingProcesses < ActiveRecord::Migration
  def change
    create_table :tracking_processes do |t|
      t.string :name
      t.text :description
      t.datetime :from_date
      t.datetime :to_date
      t.boolean :active

      t.timestamps
    end
  end
end
