class CreateDncCourses < ActiveRecord::Migration
  def change
    create_table :dnc_courses do |t|
      t.string :name
      t.references :dnc_program
      t.references :dnc_category

      t.timestamps
    end
    add_index :dnc_courses, :dnc_program_id
    add_index :dnc_courses, :dnc_category_id
  end
end
