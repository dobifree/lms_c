class CreateProgramInspectors < ActiveRecord::Migration
  def change
    create_table :program_inspectors do |t|
      t.references :program
      t.references :user

      t.timestamps
    end
    add_index :program_inspectors, :program_id
    add_index :program_inspectors, :user_id
  end
end
