class CreateHrProcessEvaluationQSchedules < ActiveRecord::Migration
  def change
    create_table :hr_process_evaluation_q_schedules do |t|
      t.references :hr_process_evaluation
      t.references :hr_evaluation_type_element
      t.date :inicio
      t.date :fin

      t.timestamps
    end
    add_index :hr_process_evaluation_q_schedules, :hr_process_evaluation_id, name: 'hr_process_evaluation_q_schedules_pe_id'
    add_index :hr_process_evaluation_q_schedules, :hr_evaluation_type_element_id, name: 'hr_process_evaluation_q_schedules_ete_id'
  end
end
