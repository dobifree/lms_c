class AddListLikeMinifichaFieldToElecProcessRound < ActiveRecord::Migration
  def change
    add_column :elec_process_rounds, :list_like_minificha, :boolean, default: false
  end
end
