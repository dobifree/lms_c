class CreateSelFlowSpecificApprovers < ActiveRecord::Migration
  def change
    create_table :sel_flow_specific_approvers do |t|
      t.references :sel_flow_approver
      t.references :user

      t.timestamps
    end
    add_index :sel_flow_specific_approvers, :sel_flow_approver_id
    add_index :sel_flow_specific_approvers, :user_id
  end
end
