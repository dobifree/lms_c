class CreateDncAreaCorrections < ActiveRecord::Migration
  def change
    create_table :dnc_area_corrections do |t|
      t.text :correccion
      t.references :dnc

      t.timestamps
    end
    add_index :dnc_area_corrections, :dnc_id
  end
end
