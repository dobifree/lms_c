class CreateSelReqItems < ActiveRecord::Migration
  def change
    create_table :sel_req_items do |t|
      t.references :sel_req_template
      t.string :name
      t.text :description
      t.integer :item_type
      t.boolean :mandatory, default: false

      t.timestamps
    end
    add_index :sel_req_items, :sel_req_template_id
  end
end
