class CreateScholarshipApplicationValues < ActiveRecord::Migration
  def change
    create_table :scholarship_application_values do |t|
      t.references :scholarship_application
      t.references :scholarship_application_item
      t.integer :value_int
      t.integer :value_list_item_id
      t.string :value_string
      t.text :value_text
      t.date :value_date

      t.timestamps
    end
    add_index :scholarship_application_values, :scholarship_application_id, name: 'scholarship_app_values_application_id'
    add_index :scholarship_application_values, :scholarship_application_item_id, name: 'scholarship_app_values_application_item_id'
    add_index :scholarship_application_values, :value_list_item_id, name: 'scholarship_app_values_application_list_item_id'
  end
end
