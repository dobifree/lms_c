class AddPeQuestionRankModelIdToPeQuestionRanks < ActiveRecord::Migration
  def change
    add_column :pe_question_ranks, :pe_question_rank_model_id, :integer
    add_index :pe_question_ranks, :pe_question_rank_model_id
  end
end
