class AddMasterUnitToUnits < ActiveRecord::Migration
  def change
    add_column :units, :master_unit_id, :integer
  end
end
