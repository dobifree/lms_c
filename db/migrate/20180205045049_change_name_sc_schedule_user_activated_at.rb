class ChangeNameScScheduleUserActivatedAt < ActiveRecord::Migration
  def change
    rename_column :sc_schedule_users, :activated_at, :active_since
    remove_column :sc_schedule_users, :activated_by_user_id
  end
end
