class CreateMasterPollQuestions < ActiveRecord::Migration
  def change
    create_table :master_poll_questions do |t|
      t.text :texto
      t.references :master_poll

      t.timestamps
    end
    add_index :master_poll_questions, :master_poll_id
  end
end
