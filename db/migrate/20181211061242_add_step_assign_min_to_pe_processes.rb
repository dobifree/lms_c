class AddStepAssignMinToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assign_min, :integer, default: 1
  end
end
