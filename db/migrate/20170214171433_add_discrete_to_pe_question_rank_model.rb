class AddDiscreteToPeQuestionRankModel < ActiveRecord::Migration
  def change
    add_column :pe_question_rank_models, :discrete, :boolean, default: false
  end
end
