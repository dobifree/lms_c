class CreateJmProfileSections < ActiveRecord::Migration
  def change
    create_table :jm_profile_sections do |t|
      t.integer :position
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
