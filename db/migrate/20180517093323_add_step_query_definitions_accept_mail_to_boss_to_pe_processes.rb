class AddStepQueryDefinitionsAcceptMailToBossToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_query_definitions_accept_mail_to_boss, :boolean
  end
end
