class AddPowerToKpiIndicators < ActiveRecord::Migration
  def change
    add_column :kpi_indicators, :power, :float, default: 1
  end
end
