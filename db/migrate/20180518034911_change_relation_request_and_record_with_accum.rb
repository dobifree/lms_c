class ChangeRelationRequestAndRecordWithAccum < ActiveRecord::Migration
  def change
    remove_column :vac_requests, :vac_accumulated_id
    add_column :vac_accumulateds, :vac_request_id, :integer
    add_column :vac_accumulateds, :vac_record_id, :integer

    add_index :vac_accumulateds, :vac_request_id
    add_index :vac_accumulateds, :vac_record_id
  end
end
