class FixNameColumnsTrackableToTraceable < ActiveRecord::Migration
  def change
    rename_column :tracking_forms, :trackable_by_attendant, :traceable_by_attendant
    rename_column :tracking_forms, :trackable_by_subject, :traceable_by_subject
  end
end
