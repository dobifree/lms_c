class AddContentTypeToMasterUnits < ActiveRecord::Migration
  def change
    add_column :master_units, :content_type, :integer, default: 0
  end
end
