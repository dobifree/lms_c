class AddColumnDeleteToScDocuments < ActiveRecord::Migration
  def change
    add_column :sc_documentations, :deleted, :boolean, default: false
    add_column :sc_documentations, :deleted_description, :text
  end
end
