class AddMandatoryFieldToBlogFormItem < ActiveRecord::Migration
  def change
    add_column :blog_form_items, :mandatory, :boolean, :default => false
  end
end
