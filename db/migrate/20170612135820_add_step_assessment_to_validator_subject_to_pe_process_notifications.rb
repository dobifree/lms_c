class AddStepAssessmentToValidatorSubjectToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_assessment_to_validator_subject, :string
  end
end
