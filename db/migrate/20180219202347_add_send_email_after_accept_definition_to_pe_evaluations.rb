class AddSendEmailAfterAcceptDefinitionToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :send_email_after_accept_definition, :boolean, default: false
  end
end
