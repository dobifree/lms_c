class CreateJjCharacteristicValues < ActiveRecord::Migration
  def change
    create_table :jj_characteristic_values do |t|
      t.references :jj_characteristic
      t.string :value_string
      t.integer :position
      t.boolean :active, :default => true

      t.timestamps
    end
    add_index :jj_characteristic_values, :jj_characteristic_id
  end
end
