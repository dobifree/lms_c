class AddIpToLogLogins < ActiveRecord::Migration
  def change
    add_column :log_logins, :ip, :string
  end
end
