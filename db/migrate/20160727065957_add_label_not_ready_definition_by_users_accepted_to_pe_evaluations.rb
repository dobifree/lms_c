class AddLabelNotReadyDefinitionByUsersAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :label_not_ready_definition_by_users_accepted, :string, default: 'not_ready|success'
  end
end
