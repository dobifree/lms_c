class CreatePollProcessReportChars < ActiveRecord::Migration
  def change
    create_table :poll_process_report_chars do |t|
      t.references :poll_process
      t.references :characteristic
      t.integer :position

      t.timestamps
    end
    add_index :poll_process_report_chars, :poll_process_id
    add_index :poll_process_report_chars, :characteristic_id
  end
end
