class AddAllowDefinitionByManagersToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :allow_definition_by_managers, :boolean, default: false
  end
end
