class CleanLogSystems < ActiveRecord::Migration
  def change
    execute "delete from log_systems"
    execute "truncate log_systems"
  end
end
