class ChangeDefaultRegisteredManuallyTotal < ActiveRecord::Migration
  def change
    change_column_default( :ben_user_cellphone_processes, :registered_manually_total, 0)
  end
end
