class CreateSelOptions < ActiveRecord::Migration
  def change
    create_table :sel_options do |t|
      t.references :sel_characteristic
      t.string :name
      t.text :description
      t.string :color
      t.integer :position

      t.timestamps
    end
    add_index :sel_options, :sel_characteristic_id
  end
end
