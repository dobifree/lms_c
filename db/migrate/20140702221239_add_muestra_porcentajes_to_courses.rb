class AddMuestraPorcentajesToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :muestra_porcentajes, :boolean, default: false
  end
end
