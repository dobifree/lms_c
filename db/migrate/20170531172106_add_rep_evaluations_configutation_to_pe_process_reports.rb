class AddRepEvaluationsConfigutationToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_evaluations_configutation, :boolean, default: false
  end
end
