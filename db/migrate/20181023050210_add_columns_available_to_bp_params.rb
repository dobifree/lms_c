class AddColumnsAvailableToBpParams < ActiveRecord::Migration
  def change
    add_column :bp_condition_vacations, :unavailable_at, :datetime
    add_column :bp_condition_vacations, :unavailable_by_admin_id, :integer

    add_column :bp_licenses, :unavailable_at, :datetime
    add_column :bp_licenses, :unavailable_by_admin_id, :integer

    add_index :bp_condition_vacations, :unavailable_by_admin_id
    add_index :bp_licenses, :unavailable_by_admin_id

  end
end
