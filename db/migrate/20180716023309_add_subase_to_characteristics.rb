class AddSubaseToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :subase, :boolean, default: false
  end
end
