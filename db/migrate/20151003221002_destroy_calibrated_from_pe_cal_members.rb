class DestroyCalibratedFromPeCalMembers < ActiveRecord::Migration
  def change
    remove_column :pe_cal_members, :calibrated
  end
end
