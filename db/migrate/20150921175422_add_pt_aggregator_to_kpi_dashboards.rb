class AddPtAggregatorToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :pt_aggregator, :text
  end
end
