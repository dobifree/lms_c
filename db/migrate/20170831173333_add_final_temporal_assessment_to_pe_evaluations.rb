class AddFinalTemporalAssessmentToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :final_temporal_assessment, :boolean
  end
end
