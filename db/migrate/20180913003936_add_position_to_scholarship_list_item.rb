class AddPositionToScholarshipListItem < ActiveRecord::Migration
  def change
    add_column :scholarship_list_items, :position, :integer
  end
end
