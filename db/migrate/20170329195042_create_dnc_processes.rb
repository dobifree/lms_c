class CreateDncProcesses < ActiveRecord::Migration
  def change
    create_table :dnc_processes do |t|
      t.string :name
      t.string :period

      t.timestamps
    end
  end
end
