class AddBonusLiquidoToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :bonus_liquido, :decimal, :precision => 20, :scale => 4, default: 0
  end
end
