class AddActivoToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :activo, :boolean, default: true
  end
end
