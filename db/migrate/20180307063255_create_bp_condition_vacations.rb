class CreateBpConditionVacations < ActiveRecord::Migration
  def change
    create_table :bp_condition_vacations do |t|
      t.references :bp_season
      t.references :bp_rule_vacation
      t.integer :priority
      t.integer :bonus_days
      t.float :bonus_money
      t.string :bonus_currency
      t.integer :days_since
      t.integer :days_until
      t.integer :antiquity
      t.date :since
      t.date :until
      t.integer :period_times
      t.boolean :period_anniversary, :default => true
      t.date :period_begin_1
      t.date :period_begin_2
      t.boolean :period_condition_begin, :default => true
      t.integer :period_condition_min_days
      t.float :period_condition_min_percent
      t.boolean :season_condition_begin, :default => true
      t.integer :season_condition_min_days
      t.float :season_condition_min_percent
      t.boolean :active, :default => true
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id

      t.timestamps
    end
    add_index :bp_condition_vacations, :bp_season_id
    add_index :bp_condition_vacations, :bp_rule_vacation_id, :name => 'bp_cond_vac_rule_vac'
    add_index :bp_condition_vacations, :deactivated_by_admin_id, :name => 'bp_cond_vac_deac'
    add_index :bp_condition_vacations, :registered_by_admin_id, :name => 'bp_cond_vac_regs'
  end
end
