class SetDescargableTrueFolderFile < ActiveRecord::Migration
  def change
    change_column :folder_files, :descargable, :boolean, default: true
  end
end
