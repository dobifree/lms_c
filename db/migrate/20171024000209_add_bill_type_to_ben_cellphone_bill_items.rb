class AddBillTypeToBenCellphoneBillItems < ActiveRecord::Migration
  def change
    add_column :ben_cellphone_bill_items, :bill_type, :integer
    add_column :ben_cellphone_bill_items, :not_charge_to_payroll, :boolean
    add_column :ben_cellphone_bill_items, :not_charge_to_payroll_at, :datetime
    add_column :ben_cellphone_bill_items, :not_charge_to_payroll_by_user_id, :integer
    add_column :ben_cellphone_bill_items, :not_charge_to_payroll_description, :string
    add_column :ben_cellphone_bill_items, :registered_manually, :boolean
    add_column :ben_cellphone_bill_items, :registered_manually_at, :datetime
    add_column :ben_cellphone_bill_items, :registered_manually_by_user_id, :integer
    add_column :ben_cellphone_bill_items, :registered_manually_description, :string

    add_index :ben_cellphone_bill_items, :not_charge_to_payroll_by_user_id, :name => 'ben_cellphone_bill_items_nctp_user_id'
    add_index :ben_cellphone_bill_items, :registered_manually_by_user_id, :name => 'ben_cellphone_bill_items_rm_user_id'
  end
end
