class AddRepDetailedAutoToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_detailed_auto, :boolean, default: false
  end
end
