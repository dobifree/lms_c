class AddCommentFieldToSelStepCandidate < ActiveRecord::Migration
  def change
    add_column :sel_step_candidates, :exonerated_comment, :text
  end
end
