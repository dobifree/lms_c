class CreateUserCoursePolls < ActiveRecord::Migration
  def change
    create_table :user_course_polls do |t|
      t.references :user_course
      t.references :poll
      t.datetime :fecha
      t.boolean :finalizada

      t.timestamps
    end
    add_index :user_course_polls, :user_course_id
    add_index :user_course_polls, :poll_id
  end
end
