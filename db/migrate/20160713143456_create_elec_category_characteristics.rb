class CreateElecCategoryCharacteristics < ActiveRecord::Migration
  def change
    create_table :elec_category_characteristics do |t|
      t.references :elec_process_category
      t.references :characteristic
      t.string :match_value
      t.boolean :filter_voter
      t.boolean :filter_candidate

      t.timestamps
    end
    add_index :elec_category_characteristics, :elec_process_category_id
    add_index :elec_category_characteristics, :characteristic_id
  end
end
