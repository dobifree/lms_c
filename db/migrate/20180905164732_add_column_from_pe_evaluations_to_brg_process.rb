class AddColumnFromPeEvaluationsToBrgProcess < ActiveRecord::Migration
  def change
    add_column :brg_processes, :quali_evaluation_id, :integer
    add_column :brg_processes, :quanti_evaluation_id, :integer
    add_column :brg_processes, :quali_percentage, :float
    add_column :brg_processes, :quanti_percentage, :float

    add_index :brg_processes, :quali_evaluation_id
    add_index :brg_processes, :quanti_evaluation_id
  end
end
