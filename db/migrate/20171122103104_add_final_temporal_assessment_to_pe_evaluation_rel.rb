class AddFinalTemporalAssessmentToPeEvaluationRel < ActiveRecord::Migration
  def change
    add_column :pe_evaluation_rels, :final_temporal_assessment, :boolean, default: false
  end
end
