class CreatePeProcessReports < ActiveRecord::Migration
  def change
    create_table :pe_process_reports do |t|
      t.references :pe_process
      t.boolean :rep_evaluated_evaluators_configuration, default: false

      t.timestamps
    end
    add_index :pe_process_reports, :pe_process_id
  end
end
