class CreateScholarshipApplications < ActiveRecord::Migration
  def change
    create_table :scholarship_applications do |t|
      t.integer :registered_by_user_id
      t.datetime :registered_at
      t.boolean :done
      t.datetime :done_at
      t.boolean :evaluated
      t.integer :evaluated_by_user_id
      t.datetime :evaluated_at
      t.boolean :evaluation
      t.text :evaluation_comment

      t.timestamps
    end

    add_index :scholarship_applications, :registered_by_user_id
    add_index :scholarship_applications, :evaluated_by_user_id
  end
end
