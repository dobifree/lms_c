class AddTextColorToPeDimensionGroups < ActiveRecord::Migration
  def change
    add_column :pe_dimension_groups, :text_color, :string
  end
end
