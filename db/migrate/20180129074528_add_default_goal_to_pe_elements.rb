class AddDefaultGoalToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :default_goal, :string
  end
end
