class AddTrainingProgramIdToDncps < ActiveRecord::Migration
  def change
    add_column :dncps, :training_program_id, :integer
    add_index :dncps, :training_program_id
  end
end
