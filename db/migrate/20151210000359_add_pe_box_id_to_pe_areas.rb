class AddPeBoxIdToPeAreas < ActiveRecord::Migration
  def change
    add_column :pe_areas, :pe_box_id, :integer
    add_index :pe_areas, :pe_box_id
  end
end
