class AddDefViewPercentageToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :def_view_percentage, :boolean, default: true
  end
end
