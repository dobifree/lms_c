class CreatePeCalCommittees < ActiveRecord::Migration
  def change
    create_table :pe_cal_committees do |t|
      t.references :pe_cal_session
      t.references :pe_process
      t.references :user
      t.boolean :manager, default: false

      t.timestamps
    end
    add_index :pe_cal_committees, :pe_cal_session_id
    add_index :pe_cal_committees, :pe_process_id
    add_index :pe_cal_committees, :user_id
  end
end
