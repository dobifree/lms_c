class AddValDateToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :val_date, :datetime
  end
end
