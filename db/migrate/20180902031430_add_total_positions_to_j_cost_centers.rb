class AddTotalPositionsToJCostCenters < ActiveRecord::Migration
  def change
    add_column :j_cost_centers, :total_positions, :integer, default: 1
  end
end
