class AddRepDetailedFinalResultsShowFeedbackProviderToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :rep_detailed_final_results_show_feedback_provider, :boolean
  end
end
