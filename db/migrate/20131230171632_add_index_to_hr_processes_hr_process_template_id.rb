class AddIndexToHrProcessesHrProcessTemplateId < ActiveRecord::Migration
  def change
    add_index :hr_processes, :hr_process_template_id
  end
end
