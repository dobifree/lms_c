class AddEmailAfterAcceptDefinitionContentToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :email_after_accept_definition_content, :text
  end
end
