class CreateKpiSets < ActiveRecord::Migration
  def change
    create_table :kpi_sets do |t|
      t.integer :position
      t.string :name
      t.references :kpi_dashboard

      t.timestamps
    end
    add_index :kpi_sets, :kpi_dashboard_id
  end
end
