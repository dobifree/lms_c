class RemoveOwnerUserIdFromSelProcesses < ActiveRecord::Migration
  def up
    remove_columns :sel_processes, :owner_user_id
  end
end
