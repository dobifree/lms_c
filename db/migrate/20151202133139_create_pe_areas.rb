class CreatePeAreas < ActiveRecord::Migration
  def change
    create_table :pe_areas do |t|
      t.string :name
      t.references :pe_process

      t.timestamps
    end
    add_index :pe_areas, :pe_process_id
  end
end
