class AddIndicatorDiscreteAchievementToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :indicator_discrete_achievement, :string
  end
end
