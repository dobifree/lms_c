class CreateSelNotifications < ActiveRecord::Migration
  def change
    create_table :sel_notifications do |t|
      t.references :sel_notification_template
      t.string :to
      t.string :cc
      t.string :subject
      t.text :body
      t.datetime :triggered_at
      t.integer :triggered_by_user_id
      t.boolean :sent, default: false
      t.datetime :sent_at

      t.timestamps
    end
    add_index :sel_notifications, :sel_notification_template_id
    add_index :sel_notifications, :triggered_by_user_id
  end
end
