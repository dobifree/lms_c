class AddManageCalibrationEToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :manage_calibration_e, :boolean
  end
end
