class ChangeColumnRejectedDescriptionToVacRequest < ActiveRecord::Migration
  def change
    rename_column :vac_requests, :rejected_description, :status_description
  end
end
