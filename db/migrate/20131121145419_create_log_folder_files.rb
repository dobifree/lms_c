class CreateLogFolderFiles < ActiveRecord::Migration
  def change
    create_table :log_folder_files do |t|
      t.references :folder_file
      t.datetime :fecha
      t.string :elemento
      t.string :accion

      t.timestamps
    end
    add_index :log_folder_files, :folder_file_id
  end
end
