class AddColumntoScBlockAndScRecord < ActiveRecord::Migration
  def change
    add_column :sc_records, :effective_time, :integer
    add_column :sc_blocks, :free_time, :integer, :default => 0
  end
end
