class AddMinNumberToPeQuestionModel < ActiveRecord::Migration
  def change
    add_column :pe_question_models, :min_number, :integer
  end
end
