class AddSecondLastNameToCompanies < ActiveRecord::Migration
  def connection
    @connection = Company.connection
  end

  def change
    add_column(:companies, :second_last_name, :boolean, :default => false) unless column_exists?(:companies, :second_last_name)
  end

end
