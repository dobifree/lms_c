class CreateQueuedReports < ActiveRecord::Migration
  def change
    create_table :queued_reports do |t|
      t.text :description
      t.string :code
      t.string :name
      t.boolean :status
      t.datetime :from_date
      t.integer :created_in

      t.timestamps
    end
  end
end
