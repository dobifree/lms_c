class AddHrProcessDimensionGYIdToHrProcessQuadrant < ActiveRecord::Migration
  def change
    add_column :hr_process_quadrants, :hr_process_dimension_g_y_id, :integer
  end
end
