class AddUmManageUsersPhotoToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :um_manage_users_photo, :boolean, default: false
  end
end
