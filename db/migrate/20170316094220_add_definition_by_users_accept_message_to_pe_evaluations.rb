class AddDefinitionByUsersAcceptMessageToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :definition_by_users_accept_message, :string, default: 'Confirmo que conozco y estoy de acuerdo con la definición'
  end
end
