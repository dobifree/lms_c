class CreateElecProcessCategories < ActiveRecord::Migration
  def change
    create_table :elec_process_categories do |t|
      t.references :elec_process
      t.string :name
      t.text :description
      t.integer :max_votes
      t.boolean :self_vote

      t.timestamps
    end
    add_index :elec_process_categories, :elec_process_id
    change_column :elec_process_categories, :max_votes, :integer, :default => 1
    change_column :elec_process_categories, :self_vote, :boolean, :default => true
  end
end
