class AddAliasCliInversoPluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_cli_inverso_plu, :string, default: 'Proveedores'
  end
end
