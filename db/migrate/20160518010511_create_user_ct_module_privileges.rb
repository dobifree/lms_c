class CreateUserCtModulePrivileges < ActiveRecord::Migration
  def change
    create_table :user_ct_module_privileges do |t|
      t.references :user

      t.timestamps
    end
    add_index :user_ct_module_privileges, :user_id
  end
end
