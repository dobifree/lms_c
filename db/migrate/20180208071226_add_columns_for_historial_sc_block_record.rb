class AddColumnsForHistorialScBlockRecord < ActiveRecord::Migration
  def change
    add_column :sc_record_blocks, :active, :boolean, :default => true
    add_column :sc_record_blocks, :deactivated_at, :datetime
    add_column :sc_record_blocks, :deactivated_by_user_id, :integer

    add_index :sc_record_blocks, :deactivated_by_user_id
  end
end
