class AddVirtualToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :virtual, :boolean, default: true
  end
end
