class AddKpiIndicatorIdToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :kpi_indicator_id, :integer
    add_index :pe_questions, :kpi_indicator_id
  end
end
