class AddAliasParInversoToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_par_inverso, :string, default: 'Par'
  end
end
