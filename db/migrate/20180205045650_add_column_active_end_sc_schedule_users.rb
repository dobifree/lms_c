class AddColumnActiveEndScScheduleUsers < ActiveRecord::Migration
  def change
    add_column :sc_schedule_users, :active_end, :datetime
  end
end
