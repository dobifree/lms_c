class CreateCharacteristicValueParents < ActiveRecord::Migration
  def change
    create_table :characteristic_value_parents do |t|
      t.references :characteristic_value
      t.integer :characteristic_parent_id
      t.integer :characteristic_value_parent_id

      t.timestamps
    end
    add_index :characteristic_value_parents, :characteristic_value_id, name: 'cvp_characteristic_value_id'
    add_index :characteristic_value_parents, :characteristic_parent_id, name: 'cvp_characteristic_parent_id'
    add_index :characteristic_value_parents, :characteristic_value_parent_id, name: 'cvp_characteristic_value_parent_id'
  end
end
