class CreateBrcProcesses < ActiveRecord::Migration
  def change
    create_table :brc_processes do |t|
      t.string :name
      t.string :period

      t.timestamps
    end
  end
end
