class AddPeQuestionActivityIdToPeQuestionActivities < ActiveRecord::Migration
  def change
    add_column :pe_question_activities, :pe_question_activity_id, :integer
    add_index :pe_question_activities, :pe_question_activity_id
  end
end
