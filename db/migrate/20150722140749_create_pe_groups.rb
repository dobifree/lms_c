class CreatePeGroups < ActiveRecord::Migration
  def change
    create_table :pe_groups do |t|
      t.string :name
      t.references :pe_evaluation

      t.timestamps
    end
    add_index :pe_groups, :pe_evaluation_id
  end
end
