class AddKpiIndicatorIdToKpiColors < ActiveRecord::Migration
  def change
    add_column :kpi_colors, :kpi_indicator_id, :integer
    add_index :kpi_colors, :kpi_indicator_id
  end
end
