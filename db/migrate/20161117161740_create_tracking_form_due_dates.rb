class CreateTrackingFormDueDates < ActiveRecord::Migration
  def change
    create_table :tracking_form_due_dates do |t|
      t.references :tracking_form_answer
      t.datetime :due_date
      t.text :comment
      t.boolean :finished

      t.timestamps
    end
    add_index :tracking_form_due_dates, :tracking_form_answer_id
    change_column :tracking_form_due_dates, :finished, :boolean, :default => false
  end
end
