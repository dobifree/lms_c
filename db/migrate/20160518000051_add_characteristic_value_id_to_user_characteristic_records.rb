class AddCharacteristicValueIdToUserCharacteristicRecords < ActiveRecord::Migration
  def change
    add_column :user_characteristic_records, :characteristic_value_id, :integer
    add_index :user_characteristic_records, :characteristic_value_id
  end
end
