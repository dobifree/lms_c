class AddTotalsToBenUserCellphoneProcess < ActiveRecord::Migration
  def change
    add_column :ben_user_cellphone_processes, :plan_bill,:decimal, :precision => 12, :scale => 4
    add_column :ben_user_cellphone_processes, :equipment_bill, :decimal, :precision => 12, :scale => 4
  end
end
