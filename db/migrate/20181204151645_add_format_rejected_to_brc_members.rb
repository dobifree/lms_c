class AddFormatRejectedToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :format_rejected, :boolean, default: false
  end
end
