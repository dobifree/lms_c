class RemoveFantasmaFromUsers < ActiveRecord::Migration

  def change
    remove_column :users, :fantasma
  end

end
