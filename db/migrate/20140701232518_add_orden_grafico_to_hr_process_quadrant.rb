class AddOrdenGraficoToHrProcessQuadrant < ActiveRecord::Migration
  def change
    add_column :hr_process_quadrants, :orden_grafico, :integer, default: 1
  end
end
