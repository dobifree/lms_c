class AddFinalizadaToHrProcessUserRels < ActiveRecord::Migration
  def change
    add_column :hr_process_user_rels, :finalizada, :boolean, default: false
  end
end
