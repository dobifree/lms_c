class CreateMedlicYearDaysLicenses < ActiveRecord::Migration
  def change
    create_table :medlic_year_days_licenses do |t|
      t.integer :year
      t.integer :days, default: 0
      t.references :user

      t.timestamps
    end
    add_index :medlic_year_days_licenses, :user_id
=begin
    year = 2000
    loop do

      begin_date = (year.to_s+'-01-01').to_date
      end_date =  (year.to_s+'-12-31').to_date

      User.all.each do |user|
        licenses = MedlicLicense.by_user(user.id).where("date_begin <= '" + begin_date.to_s + "'", "date_end => '" + end_date.to_s + "'")
        next unless licenses.size > 0
        pivot = begin_date
        days = 0

        loop do
          licenses.each do |license|
            max_begin_date = begin_date > license.date_begin ? begin_date : license.date_begin
            min_end_date = end_date < license.date_end ? end_date : license.date_end
            next unless max_begin_date <= pivot && pivot <= min_end_date
            days += 1
          end
          pivot += 1.day
          break if pivot > end_date.to_date
        end

        new_obj = MedlicYearDaysLicense.new()
        new_obj.user_id = user.id
        new_obj.year = year
        new_obj.days = days
        new_obj.save
      end

      year += 1
      break if year > 2018
    end
=end
  end
end
