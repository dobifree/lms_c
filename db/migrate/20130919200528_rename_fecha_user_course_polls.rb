class RenameFechaUserCoursePolls < ActiveRecord::Migration
  def change
    rename_column :user_course_polls, :fecha, :inicio
  end
end
