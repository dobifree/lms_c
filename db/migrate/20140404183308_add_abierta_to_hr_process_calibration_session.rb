class AddAbiertaToHrProcessCalibrationSession < ActiveRecord::Migration
  def change
    add_column :hr_process_calibration_sessions, :abierta, :boolean, default: true
  end
end
