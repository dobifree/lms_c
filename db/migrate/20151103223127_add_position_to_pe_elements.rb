class AddPositionToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :position, :integer, default: 1
    add_index :pe_elements, :position
  end
end
