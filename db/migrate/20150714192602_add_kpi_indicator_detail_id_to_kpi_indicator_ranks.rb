class AddKpiIndicatorDetailIdToKpiIndicatorRanks < ActiveRecord::Migration
  def change
    add_column :kpi_indicator_ranks, :kpi_indicator_detail_id, :integer
  end
end
