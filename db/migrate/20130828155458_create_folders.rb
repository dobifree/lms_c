class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.string :nombre
      t.integer :creador_id
      t.integer :padre_id

      t.timestamps
    end
  end
end
