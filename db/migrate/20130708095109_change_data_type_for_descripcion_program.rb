class ChangeDataTypeForDescripcionProgram < ActiveRecord::Migration
  def up
  	change_table :programs do |t|
      t.change :descripcion, :text
    end
  end

  def down
  end
end
