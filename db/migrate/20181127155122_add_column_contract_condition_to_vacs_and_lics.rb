class AddColumnContractConditionToVacsAndLics < ActiveRecord::Migration
  def change
    add_column :bp_condition_vacations, :indef_contract_required, :boolean, default: false
    add_column :bp_licenses, :indef_contract_required, :boolean, default: false
  end
end
