class CreatePlanningProcessGoals < ActiveRecord::Migration
  def change
    create_table :planning_process_goals do |t|
      t.text :descripcion
      t.references :planning_process

      t.timestamps
    end
    add_index :planning_process_goals, :planning_process_id
  end
end
