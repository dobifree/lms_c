class CreateSupportTickets < ActiveRecord::Migration
  def change
    create_table :support_tickets do |t|
      t.references :user
      t.references :support_ticket_type
      t.text :descripcion
      t.datetime :fecha_registro
      t.boolean :atendido
      t.text :respuesta
      t.datetime :fecha_respuesta

      t.timestamps
    end
    add_index :support_tickets, :user_id
    add_index :support_tickets, :support_ticket_type_id
  end
end
