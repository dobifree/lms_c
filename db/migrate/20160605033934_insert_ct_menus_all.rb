class InsertCtMenusAll < ActiveRecord::Migration

  def change
    execute "insert into ct_menus set position = 1, cod='home', name='Inicio', user_menu_alias='Inicio', created_at = NOW(), updated_at = NOW()"
    execute "insert into ct_menus set position = 2, cod='tr', name='Capacitación', user_menu_alias='Cursos', created_at = NOW(), updated_at = NOW()"
    execute "insert into ct_menus set position = 3, cod='lib', name='Biblioteca', user_menu_alias='Biblioteca', created_at = NOW(), updated_at = NOW()"
    execute "insert into ct_menus set position = 4, cod='hd', name='Mesa de ayuda', user_menu_alias='Mesa de ayuda', created_at = NOW(), updated_at = NOW()"
    execute "insert into ct_menus set position = 5, cod='sel', name='Selección', user_menu_alias='Selección', created_at = NOW(), updated_at = NOW()"
    execute "insert into ct_menus set position = 6, cod='tm', name='Gestión de la capacitación', user_menu_alias='Gestión de la capacitación', created_at = NOW(), updated_at = NOW()"
    execute "insert into ct_menus set position = 7, cod='pe', name='Evaluación de desempeño', user_menu_alias='Evaluación de desempeño', created_at = NOW(), updated_at = NOW()"
    execute "insert into ct_menus set position = 8, cod='kpi', name='KPIs', user_menu_alias='KPI Dashboards', created_at = NOW(), updated_at = NOW()"
    execute "insert into ct_menus set position = 9, cod='up', name='Ficha de usuario', user_menu_alias='Ficha de usuario', created_at = NOW(), updated_at = NOW()"
  end

end
