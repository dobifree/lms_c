class CreateBpGeneralRulesVacations < ActiveRecord::Migration
  def change
    create_table :bp_general_rules_vacations do |t|
      t.boolean :active, :default => true
      t.integer :legal_days
      t.string :progressive_days
      t.integer :progressive_max_days
      t.integer :request_min_days
      t.integer :request_max_days
      t.integer :accumulated_max_days
      t.integer :accumulated_max_real_days
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id

      t.timestamps
    end
    add_index :bp_general_rules_vacations, :deactivated_by_admin_id, :name => 'bp_general_rules_vacations_deac_adm_id'
    add_index :bp_general_rules_vacations, :registered_by_admin_id, :name => 'bp_general_rules_vacations_regis_adm_id'
  end
end
