class CreateUserCourseEvaluations < ActiveRecord::Migration
  def change
    create_table :user_course_evaluations do |t|
      t.references :user_course
      t.references :evaluation
      t.datetime :inicio
      t.datetime :fin
      t.boolean :finalizada
      t.datetime :ultimo_acceso
      t.boolean :aprobada
      t.float :nota

      t.timestamps
    end
    add_index :user_course_evaluations, :user_course_id
    add_index :user_course_evaluations, :evaluation_id
  end
end
