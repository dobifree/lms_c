class RemoveTableLicForm < ActiveRecord::Migration
  def change
    remove_column :lic_item_values, :lic_form_id

    add_column :lic_item_values, :lic_request_id, :integer
    add_index :lic_item_values, :lic_request_id

    add_column :lic_requests, :bp_form_id, :integer
    add_index :lic_requests, :bp_form_id
  end
end
