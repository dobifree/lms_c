class CreateKpiColors < ActiveRecord::Migration
  def change
    create_table :kpi_colors do |t|
      t.references :kpi_dashboard
      t.float :percentage
      t.string :color

      t.timestamps
    end
    add_index :kpi_colors, :kpi_dashboard_id
  end
end
