class CreatePeEvaluationAssessRelMessages < ActiveRecord::Migration
  def change
    create_table :pe_evaluation_assess_rel_messages do |t|
      t.references :pe_evaluation
      t.references :pe_rel
      t.text :message

      t.timestamps
    end
    add_index :pe_evaluation_assess_rel_messages, :pe_evaluation_id
    add_index :pe_evaluation_assess_rel_messages, :pe_rel_id
  end
end
