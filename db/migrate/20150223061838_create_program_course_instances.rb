class CreateProgramCourseInstances < ActiveRecord::Migration
  def change
    create_table :program_course_instances do |t|
      t.references :program_course
      t.references :program_instance
      t.date :from_date
      t.date :to_date

      t.timestamps
    end
    add_index :program_course_instances, :program_course_id
    add_index :program_course_instances, :program_instance_id
  end
end
