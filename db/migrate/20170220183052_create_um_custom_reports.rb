class CreateUmCustomReports < ActiveRecord::Migration
  def change
    create_table :um_custom_reports do |t|
      t.string :name

      t.timestamps
    end
  end
end
