class AddRegisteredAtToSelRequirement < ActiveRecord::Migration
  def change
    add_column :sel_requirements, :registered_at, :datetime
  end
end
