class AddFromDateProgresivasToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :from_date_progresivas, :boolean, default: false
  end
end
