class AddContratoPlazoSecuToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :contrato_plazo_secu, :boolean, default: false
  end
end
