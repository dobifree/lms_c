class CreateScReportings < ActiveRecord::Migration
  def change
    create_table :sc_reportings do |t|
      t.references :sc_process
      t.datetime :reported_at
      t.integer :reported_by_user_id
      t.references :sc_record

      t.timestamps
    end
    add_index :sc_reportings, :sc_process_id
    add_index :sc_reportings, :sc_record_id
    add_index :sc_reportings, :reported_by_user_id
  end
end
