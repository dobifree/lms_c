class AddDespueDeEvaluationIdToUnits < ActiveRecord::Migration
  def change
    add_column :units, :despues_de_evaluation_id, :integer
  end
end
