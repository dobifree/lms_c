class AddCurrencySubasoToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :currency_subaso, :boolean, default: false
  end
end
