class CreateDncRequirements < ActiveRecord::Migration
  def change
    create_table :dnc_requirements do |t|
      t.references :user
      t.references :dnc_process
      t.references :dnc_course
      t.references :dnc_process_period
      t.references :dnc_provider

      t.timestamps
    end
    add_index :dnc_requirements, :user_id
    add_index :dnc_requirements, :dnc_process_id
    add_index :dnc_requirements, :dnc_course_id
    add_index :dnc_requirements, :dnc_process_period_id
    add_index :dnc_requirements, :dnc_provider_id
  end
end
