class AddRequireValidationToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :require_validation, :boolean, default: false
  end
end
