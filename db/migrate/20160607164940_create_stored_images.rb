class CreateStoredImages < ActiveRecord::Migration
  def change
    create_table :stored_images do |t|
      t.string :name
      t.string :crypted_name

      t.timestamps
    end
  end
end
