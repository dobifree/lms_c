class AddTieneClasificacionToHrEvaluationTypeElement < ActiveRecord::Migration
  def change
    add_column :hr_evaluation_type_elements, :tiene_clasificacion, :boolean
  end
end
