class CreateSelApplicants < ActiveRecord::Migration
  def change
    create_table :sel_applicants do |t|
      t.references :sel_process
      t.references :jm_candidate

      t.timestamps
    end
    add_index :sel_applicants, :sel_process_id
    add_index :sel_applicants, :jm_candidate_id
  end
end
