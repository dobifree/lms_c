class AddSentToPayrollRecord < ActiveRecord::Migration
  def change
    add_column :sc_records, :sent_to_payroll, :boolean, default: false
    add_column :sc_reportings, :active, :boolean, default: true
    add_column :sc_reportings, :deactivated_at, :datetime
    add_column :sc_reportings, :deactivated_by_user_id, :integer
    add_index :sc_reportings, :deactivated_by_user_id
    remove_column :sc_reportings, :sc_record_id
  end
end
