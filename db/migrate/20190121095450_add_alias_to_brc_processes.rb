class AddAliasToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :alias, :string, default: 'Bono Rol Comercial'
  end
end
