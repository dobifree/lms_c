class AddListItemSelectedToPeFeedbackFields < ActiveRecord::Migration
  def change
    add_column :pe_feedback_fields, :list_item_selected, :boolean, default: true
  end
end
