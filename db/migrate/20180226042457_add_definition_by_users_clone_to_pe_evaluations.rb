class AddDefinitionByUsersCloneToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :definition_by_users_clone, :boolean, :default => false
  end
end
