class AddAvailableToVacRuleRecord < ActiveRecord::Migration
  def change
    add_column :vac_rule_records, :available, :boolean, default: true
    add_column :vac_rule_records, :available_changed_at, :datetime
    add_column :vac_rule_records, :available_changed_by_user_id, :integer

    add_index :vac_rule_records, :available_changed_by_user_id, name: 'vac_rul_rec_avai_user_id'
  end
end
