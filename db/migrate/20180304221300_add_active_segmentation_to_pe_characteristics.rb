class AddActiveSegmentationToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_segmentation, :boolean
  end
end
