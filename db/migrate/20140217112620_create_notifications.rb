class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :orden
      t.string :asunto
      t.text :texto
      t.datetime :fecha

      t.timestamps
    end
  end
end
