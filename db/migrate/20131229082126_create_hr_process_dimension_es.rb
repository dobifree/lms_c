class CreateHrProcessDimensionEs < ActiveRecord::Migration
  def change
    create_table :hr_process_dimension_es do |t|
      t.integer :orden
      t.integer :porcentaje
      t.references :hr_evaluation_type
      t.references :hr_process_dimension

      t.timestamps
    end
    add_index :hr_process_dimension_es, :hr_evaluation_type_id
    add_index :hr_process_dimension_es, :hr_process_dimension_id
  end
end
