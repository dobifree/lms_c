class AddNameFieldToElecEvent < ActiveRecord::Migration
  def change
    add_column :elec_events, :name, :text
  end
end
