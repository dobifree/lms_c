class AddSencenetToProgramCourseInstance < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :sencenet, :string
  end
end
