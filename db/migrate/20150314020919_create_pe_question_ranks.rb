class CreatePeQuestionRanks < ActiveRecord::Migration
  def change
    create_table :pe_question_ranks do |t|
      t.float :percentage
      t.float :goal
      t.references :hr_process_evaluation_manual_q

      t.timestamps
    end
    add_index :pe_question_ranks, :hr_process_evaluation_manual_q_id
  end
end
