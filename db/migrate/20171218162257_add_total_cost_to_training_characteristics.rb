class AddTotalCostToTrainingCharacteristics < ActiveRecord::Migration
  def change
    add_column :training_characteristics, :total_cost, :boolean, default: false
  end
end
