class CreateScholarshipApplicationItemRanges < ActiveRecord::Migration
  def change
    create_table :scholarship_application_item_ranges do |t|
      t.references :scholarship_application_item
      t.references :scholarship_list_item
      t.integer :position
      t.float :min_val
      t.integer :min_sign
      t.float :max_val
      t.integer :max_sign
      t.boolean :is_default

      t.timestamps
    end
    add_index :scholarship_application_item_ranges, :scholarship_application_item_id, name: :index_schol_app_item_ranges_on_schol_app_item_id
    add_index :scholarship_application_item_ranges, :scholarship_list_item_id, name: :index_schol_app_item_ranges_on_schol_list_item_id
  end
end
