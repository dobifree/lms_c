class CreatePeGroup2s < ActiveRecord::Migration
  def change
    create_table :pe_group2s do |t|
      t.string :name
      t.references :pe_process

      t.timestamps
    end
    add_index :pe_group2s, :pe_process_id
  end
end
