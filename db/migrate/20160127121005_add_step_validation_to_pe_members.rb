class AddStepValidationToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :step_validation, :boolean, default: false
  end
end
