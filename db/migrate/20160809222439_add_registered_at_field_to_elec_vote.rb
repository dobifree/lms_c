class AddRegisteredAtFieldToElecVote < ActiveRecord::Migration
  def change
    add_column :elec_votes, :registered_at, :datetime
  end
end
