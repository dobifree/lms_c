class CreateJJobLevels < ActiveRecord::Migration
  def change
    create_table :j_job_levels do |t|
      t.string :name

      t.timestamps
    end
  end
end
