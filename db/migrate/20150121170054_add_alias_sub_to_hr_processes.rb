class AddAliasSubToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_sub, :string, default: 'Colaborador'
  end
end
