class AddBitacoraLabelToTrackingProcess < ActiveRecord::Migration
  def change
    add_column :tracking_processes, :log_label, :string
  end
end
