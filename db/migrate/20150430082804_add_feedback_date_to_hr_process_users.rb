class AddFeedbackDateToHrProcessUsers < ActiveRecord::Migration
  def change
    add_column :hr_process_users, :feedback_date, :datetime
  end
end
