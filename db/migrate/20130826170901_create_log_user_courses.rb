class CreateLogUserCourses < ActiveRecord::Migration
  def change
    create_table :log_user_courses do |t|
      t.references :user_course
      t.datetime :fecha
      t.string :elemento
      t.string :accion

      t.timestamps
    end
    add_index :log_user_courses, :user_course_id
  end
end
