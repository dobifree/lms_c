class CreatePeProcessMigratesForOldProcesses < ActiveRecord::Migration
  def change
    PeProcess.all.each do |pe_process|
      pe_process_notification = pe_process.build_pe_process_notification
      pe_process_notification.save
    end
  end
end
