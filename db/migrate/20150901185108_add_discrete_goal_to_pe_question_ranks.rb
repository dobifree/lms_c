class AddDiscreteGoalToPeQuestionRanks < ActiveRecord::Migration
  def change
    add_column :pe_question_ranks, :discrete_goal, :string, default: ''
  end
end
