class AddPeAreaIdToPeAssessmentFinalEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_assessment_final_evaluations, :pe_area_id, :integer
    add_index :pe_assessment_final_evaluations, :pe_area_id
  end
end
