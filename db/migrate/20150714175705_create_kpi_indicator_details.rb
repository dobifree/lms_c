class CreateKpiIndicatorDetails < ActiveRecord::Migration
  def change
    create_table :kpi_indicator_details do |t|
      t.references :kpi_indicator
      t.date :measured_at
      t.float :goal

      t.timestamps
    end
    add_index :kpi_indicator_details, :kpi_indicator_id
  end
end
