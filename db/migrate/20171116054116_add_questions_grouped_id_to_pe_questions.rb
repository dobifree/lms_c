class AddQuestionsGroupedIdToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :questions_grouped_id, :integer
  end
end
