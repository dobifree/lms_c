class AddCommentFieldToElecVotes < ActiveRecord::Migration
  def change
    add_column :elec_votes, :comment, :text
  end
end
