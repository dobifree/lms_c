class AddStepAssessmentShowAutoToBossToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :step_assessment_show_auto_to_boss, :boolean, default: false
  end
end
