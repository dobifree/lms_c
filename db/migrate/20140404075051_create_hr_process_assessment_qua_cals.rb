class CreateHrProcessAssessmentQuaCals < ActiveRecord::Migration
  def change
    create_table :hr_process_assessment_qua_cals do |t|
      t.references :hr_process_calibration_session
      t.references :hr_process_calibration_session_m
      t.references :hr_process_quadrant
      t.datetime :fecha

      t.timestamps
    end
    add_index :hr_process_assessment_qua_cals, :hr_process_calibration_session_id, name: 'hr_process_assessment_qua_cals_pcs_id'
    add_index :hr_process_assessment_qua_cals, :hr_process_calibration_session_m_id, name: 'hr_process_assessment_qua_cals_pcsm_id'
    add_index :hr_process_assessment_qua_cals, :hr_process_quadrant_id, name: 'hr_process_assessment_qua_cals_pq_id'
  end
end
