class AddSkRutClientToProgramInstances < ActiveRecord::Migration
  def change
    add_column :program_instances, :sk_rut_client, :string
  end
end
