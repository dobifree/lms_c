class AddSkFinancingToProgramCourseInstance < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :sk_financing, :string
  end
end
