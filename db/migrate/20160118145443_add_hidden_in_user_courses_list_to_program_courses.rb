class AddHiddenInUserCoursesListToProgramCourses < ActiveRecord::Migration
  def change
    add_column :program_courses, :hidden_in_user_courses_list, :boolean, default: false
  end
end
