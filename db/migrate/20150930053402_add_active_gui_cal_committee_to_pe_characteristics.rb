class AddActiveGuiCalCommitteeToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_gui_cal_committee, :boolean, default: false
  end
end
