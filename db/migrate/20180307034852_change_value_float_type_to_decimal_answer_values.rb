class ChangeValueFloatTypeToDecimalAnswerValues < ActiveRecord::Migration
  def change
    change_column :jm_answer_values, :value_float, :decimal, :precision => 12, :scale => 4
  end

end
