class CreateJCharacteristics < ActiveRecord::Migration
  def change
    create_table :j_characteristics do |t|
      t.references :characteristic
      t.integer :position

      t.timestamps
    end
    add_index :j_characteristics, :characteristic_id
  end
end
