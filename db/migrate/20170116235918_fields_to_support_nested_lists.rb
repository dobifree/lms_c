class FieldsToSupportNestedLists < ActiveRecord::Migration
  def change
    add_column :tracking_lists, :tracking_list_father_id, :integer
    add_column :tracking_list_items, :tracking_list_item_father_id, :integer

    add_index :tracking_lists, :tracking_list_father_id
    add_index :tracking_list_items, :tracking_list_item_father_id
  end
end
