class AddCityToProgramCourseInstance < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :city, :string
  end
end
