class CreateProgramCourseInstanceEvaluations < ActiveRecord::Migration
  def change
    create_table :program_course_instance_evaluations do |t|
      t.references :program_course_instance
      t.references :evaluation
      t.datetime :from_date
      t.datetime :to_date

      t.timestamps
    end
    add_index :program_course_instance_evaluations, :program_course_instance_id, name: 'pcie_pci_id'
    add_index :program_course_instance_evaluations, :evaluation_id, name: 'pcie_e_id'
  end
end
