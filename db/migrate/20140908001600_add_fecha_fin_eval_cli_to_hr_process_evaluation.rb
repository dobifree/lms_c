class AddFechaFinEvalCliToHrProcessEvaluation < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluations, :fecha_fin_eval_cli, :datetime
  end
end
