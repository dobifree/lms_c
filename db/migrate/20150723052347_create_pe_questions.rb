class CreatePeQuestions < ActiveRecord::Migration
  def change
    create_table :pe_questions do |t|
      t.text :description
      t.integer :weight
      t.references :pe_evaluation
      t.references :pe_group
      t.references :pe_element
      t.integer :pe_question_id
      t.boolean :has_comment

      t.timestamps
    end
    add_index :pe_questions, :pe_evaluation_id
    add_index :pe_questions, :pe_group_id
    add_index :pe_questions, :pe_element_id
    add_index :pe_questions, :pe_question_id
  end
end
