class AddColumnsForPerTypeConfigToVacsLics < ActiveRecord::Migration
  def change
    add_column :bp_licenses, :period_times_type, :integer, default: 0
    add_column :bp_licenses, :available, :boolean, default: true
    add_column :bp_licenses, :prev_license_id, :integer
    add_column :bp_licenses, :next_license_id, :integer


    add_column :bp_condition_vacations, :period_times_type, :integer, default: 0
    add_column :bp_condition_vacations, :available, :boolean, default: true
    add_column :bp_condition_vacations, :prev_cond_vac_id, :integer
    add_column :bp_condition_vacations, :next_cond_vac_id, :integer

    add_index :bp_licenses, :prev_license_id
    add_index :bp_licenses, :next_license_id
    add_index :bp_condition_vacations, :prev_cond_vac_id
    add_index :bp_condition_vacations, :next_cond_vac_id

  end

end
