class AddMinNumericValueToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :min_numeric_value, :integer
  end
end
