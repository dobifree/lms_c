class CreatePeQuestionActivityFieldLists < ActiveRecord::Migration
  def change
    create_table :pe_question_activity_field_lists do |t|
      t.references :pe_evaluation
      t.string :name

      t.timestamps
    end
    add_index :pe_question_activity_field_lists, :pe_evaluation_id
  end
end
