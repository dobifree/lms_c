class AddLowerLimitToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :lower_limit, :float
  end
end
