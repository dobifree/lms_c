class CreatePeQuestionDescriptions < ActiveRecord::Migration
  def change
    create_table :pe_question_descriptions do |t|
      t.text :description
      t.references :pe_question

      t.timestamps
    end
    add_index :pe_question_descriptions, :pe_question_id
  end
end
