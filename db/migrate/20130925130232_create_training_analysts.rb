class CreateTrainingAnalysts < ActiveRecord::Migration
  def change
    create_table :training_analysts do |t|
      t.references :planning_process_company_unit
      t.references :user

      t.timestamps
    end
    add_index :training_analysts, :planning_process_company_unit_id
    add_index :training_analysts, :user_id
  end
end
