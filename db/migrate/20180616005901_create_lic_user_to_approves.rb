class CreateLicUserToApproves < ActiveRecord::Migration
  def change
    create_table :lic_user_to_approves do |t|
      t.references :lic_approver
      t.references :user
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :lic_user_to_approves, :lic_approver_id
    add_index :lic_user_to_approves, :user_id
    add_index :lic_user_to_approves, :deactivated_by_user_id
    add_index :lic_user_to_approves, :registered_by_user_id
  end
end
