class AddColumnPartialReportToScReporting < ActiveRecord::Migration
  def change
    add_column :sc_reportings, :partial_report, :boolean, default: false
  end
end
