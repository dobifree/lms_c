class AddPositionToKpiDashboardTypes < ActiveRecord::Migration
  def change
    add_column :kpi_dashboard_types, :position, :integer
  end
end
