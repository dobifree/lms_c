class CreateHrProcessDimensions < ActiveRecord::Migration
  def change
    create_table :hr_process_dimensions do |t|
      t.string :nombre
      t.references :hr_process_template

      t.timestamps
    end
    add_index :hr_process_dimensions, :hr_process_template_id
  end
end
