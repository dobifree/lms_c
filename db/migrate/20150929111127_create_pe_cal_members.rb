class CreatePeCalMembers < ActiveRecord::Migration
  def change
    create_table :pe_cal_members do |t|
      t.references :pe_cal_session
      t.references :pe_process
      t.references :pe_member
      t.boolean :calibrated, default: false

      t.timestamps
    end
    add_index :pe_cal_members, :pe_cal_session_id
    add_index :pe_cal_members, :pe_process_id
    add_index :pe_cal_members, :pe_member_id
  end
end
