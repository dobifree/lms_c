class AddPerformedByManagerIdToLogCtModuleUms < ActiveRecord::Migration
  def change
    add_column :log_ct_module_ums, :performed_by_manager_id, :integer
    add_index :log_ct_module_ums, :performed_by_manager_id
  end
end
