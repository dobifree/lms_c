class AddRequiredNewCToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :required_new_c, :boolean, default: false
  end
end
