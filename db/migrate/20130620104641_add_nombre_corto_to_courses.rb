class AddNombreCortoToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :nombre_corto, :string
  end
end
