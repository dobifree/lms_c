class CreatePeDimensions < ActiveRecord::Migration
  def change
    create_table :pe_dimensions do |t|
      t.integer :dimension
      t.string :name
      t.references :pe_process

      t.timestamps
    end
    add_index :pe_dimensions, :pe_process_id
  end
end
