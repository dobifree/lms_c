class CreateScholarshipLists < ActiveRecord::Migration
  def change
    create_table :scholarship_lists do |t|
      t.string :name
      t.text :description
      t.boolean :active

      t.timestamps
    end
  end
end
