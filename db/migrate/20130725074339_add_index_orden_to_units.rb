class AddIndexOrdenToUnits < ActiveRecord::Migration
  def change
    add_index :units, :orden
  end
end
