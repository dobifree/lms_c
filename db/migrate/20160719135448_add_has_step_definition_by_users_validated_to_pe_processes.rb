class AddHasStepDefinitionByUsersValidatedToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_definition_by_users_validated, :boolean, default: false
  end
end
