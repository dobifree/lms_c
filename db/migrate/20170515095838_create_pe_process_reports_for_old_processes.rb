class CreatePeProcessReportsForOldProcesses < ActiveRecord::Migration
  def change
    PeProcess.all.each do |pe_process|
      pe_process_reports = pe_process.build_pe_process_reports
      pe_process_reports.save
    end
  end
end
