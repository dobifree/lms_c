class AddAliasUnitToCompanies < ActiveRecord::Migration

  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :alias_unit
      add_column :companies, :alias_unit, :string, default: 'Unidad,Unidades'
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end
end
