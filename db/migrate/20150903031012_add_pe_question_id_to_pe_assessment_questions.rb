class AddPeQuestionIdToPeAssessmentQuestions < ActiveRecord::Migration
  def change
    add_column :pe_assessment_questions, :pe_question_id, :integer
    add_index :pe_assessment_questions, :pe_question_id
  end
end
