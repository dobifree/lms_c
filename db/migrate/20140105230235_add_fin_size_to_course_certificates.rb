class AddFinSizeToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :fin_size, :integer, default: 20
  end
end
