class CreatePeProcessNotificationDefVals < ActiveRecord::Migration
  def change
    create_table :pe_process_notification_def_vals do |t|
      t.references :pe_process
      t.boolean :before_accepted, default: false
      t.integer :num_step
      t.boolean :send_email_when_accept, default: false
      t.boolean :send_email_when_reject, default: false
      t.string :accepted_subject
      t.text :accepted_body
      t.string :rejected_subject
      t.text :rejected_body

      t.timestamps
    end
    add_index :pe_process_notification_def_vals, :pe_process_id
  end
end
