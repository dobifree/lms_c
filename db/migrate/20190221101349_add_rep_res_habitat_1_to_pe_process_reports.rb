class AddRepResHabitat1ToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_res_habitat_1, :boolean, default: false
  end
end
