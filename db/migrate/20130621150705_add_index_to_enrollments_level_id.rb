class AddIndexToEnrollmentsLevelId < ActiveRecord::Migration
  def change
  	add_index :enrollments, :level_id
  end
end
