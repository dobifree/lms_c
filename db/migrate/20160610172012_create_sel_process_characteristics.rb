class CreateSelProcessCharacteristics < ActiveRecord::Migration
  def change
    create_table :sel_process_characteristics do |t|
      t.references :sel_characteristic
      t.references :sel_process_step
      t.string :name
      t.text :description
      t.integer :position
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :sel_process_characteristics, :sel_characteristic_id
    add_index :sel_process_characteristics, :sel_process_step_id
  end
end
