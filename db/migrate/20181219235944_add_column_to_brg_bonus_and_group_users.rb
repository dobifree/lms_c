class AddColumnToBrgBonusAndGroupUsers < ActiveRecord::Migration
  def change
    add_column :brg_bonus, :income_times, :decimal, precision: 12, scale: 4
    change_column_default :brg_processes, :status, 1
  end
end
