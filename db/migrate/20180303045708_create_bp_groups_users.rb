class CreateBpGroupsUsers < ActiveRecord::Migration
  def change
    create_table :bp_groups_users do |t|
      t.references :user
      t.references :bp_group
      t.boolean :active, :default => true
      t.datetime :since
      t.datetime :until
      t.datetime :registered_at
      t.integer :registered_by_admin_id
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id

      t.timestamps
    end
    add_index :bp_groups_users, :user_id
    add_index :bp_groups_users, :bp_group_id
  end
end
