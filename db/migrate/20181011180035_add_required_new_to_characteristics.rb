class AddRequiredNewToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :required_new, :boolean
  end
end
