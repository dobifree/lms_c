class AddFinalEvaluationOperationEvaluationsToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :final_evaluation_operation_evaluations, :integer, default: 0
  end
end
