class AddAskForCommentFieldToElecProcessCategories < ActiveRecord::Migration
  def change
    add_column :elec_process_categories, :ask_for_comment, :boolean, :default => false
  end
end
