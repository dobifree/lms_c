class CreateBpEventFiles < ActiveRecord::Migration
  def change
    create_table :bp_event_files do |t|
      t.references :bp_event
      t.string :name
      t.text :description
      t.boolean :active, :default => true
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id

      t.timestamps
    end
    add_index :bp_event_files, :bp_event_id
    add_index :bp_event_files, :deactivated_by_admin_id, :name => 'bp_event_files_deac_adm_id'
    add_index :bp_event_files, :registered_by_admin_id, :name => 'bp_event_files_regs_adm_id'

  end
end
