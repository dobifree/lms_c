class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.references :user
      t.references :program

      t.timestamps
    end
    add_index :enrollments, :user_id
    add_index :enrollments, :program_id
  end
end
