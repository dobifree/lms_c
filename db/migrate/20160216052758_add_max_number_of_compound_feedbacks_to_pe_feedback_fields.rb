class AddMaxNumberOfCompoundFeedbacksToPeFeedbackFields < ActiveRecord::Migration
  def change
    add_column :pe_feedback_fields, :max_number_of_compound_feedbacks, :integer, default: 3
  end
end
