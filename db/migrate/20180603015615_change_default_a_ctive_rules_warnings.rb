class ChangeDefaultACtiveRulesWarnings < ActiveRecord::Migration
  def change
    change_column_default :bp_rules_warnings, :restrictive, false
  end
end
