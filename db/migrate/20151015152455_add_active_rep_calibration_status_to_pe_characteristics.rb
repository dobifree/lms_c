class AddActiveRepCalibrationStatusToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_rep_calibration_status, :boolean, default: false
  end
end
