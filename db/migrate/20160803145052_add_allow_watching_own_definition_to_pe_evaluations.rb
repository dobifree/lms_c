class AddAllowWatchingOwnDefinitionToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :allow_watching_own_definition, :boolean, default: false
  end
end
