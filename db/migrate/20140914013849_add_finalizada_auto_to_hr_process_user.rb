class AddFinalizadaAutoToHrProcessUser < ActiveRecord::Migration
  def change
    add_column :hr_process_users, :finalizada_auto, :boolean, default: false
  end
end
