class CreateHrProcessUserTfs < ActiveRecord::Migration
  def change
    create_table :hr_process_user_tfs do |t|
      t.references :hr_process_user
      t.text :what
      t.text :how
      t.text :when

      t.timestamps
    end
    add_index :hr_process_user_tfs, :hr_process_user_id
  end
end
