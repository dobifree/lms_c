class CreateJmProfileCharToReports < ActiveRecord::Migration
  def change
    create_table :jm_profile_char_to_reports do |t|
      t.references :jm_profile_char
      t.references :jm_subcharacteristic
      t.boolean :use_to_filter_candidates
      t.integer :position_filter_candidates
      t.boolean :use_to_list_candidates
      t.integer :position_list_candidates
      t.string :alias

      t.timestamps
    end
    add_index :jm_profile_char_to_reports, :jm_profile_char_id
    add_index :jm_profile_char_to_reports, :jm_subcharacteristic_id
  end
end
