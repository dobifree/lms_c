class CreatePeCharacteristics < ActiveRecord::Migration
  def change
    create_table :pe_characteristics do |t|
      t.references :pe_process
      t.references :characteristic

      t.timestamps
    end
    add_index :pe_characteristics, :pe_process_id
    add_index :pe_characteristics, :characteristic_id
  end
end
