class AddAlertBandToJJobs < ActiveRecord::Migration
  def change
    add_column :j_jobs, :alert_band, :boolean
  end
end
