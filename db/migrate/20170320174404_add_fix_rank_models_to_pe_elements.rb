class AddFixRankModelsToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :fix_rank_models, :integer
  end
end
