class CreateProgramInstances < ActiveRecord::Migration
  def change
    create_table :program_instances do |t|
      t.string :name
      t.references :program
      t.date :from_date
      t.date :to_date

      t.timestamps
    end
    add_index :program_instances, :program_id
  end
end
