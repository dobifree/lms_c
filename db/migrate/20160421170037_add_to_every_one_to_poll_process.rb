class AddToEveryOneToPollProcess < ActiveRecord::Migration
  def change
    add_column :poll_processes, :to_everyone, :boolean, default: false
    PollProcess.reset_column_information
    PollProcess.all.each do |process|
      process.update_attribute(:to_everyone, process.characteristics.count == 0 ? true : false)
    end
  end
end
