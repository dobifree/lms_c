class ChangeDefaultActiveBpSeasonPeriod < ActiveRecord::Migration
def change
  change_column_default :bp_season_periods, :active, true
end
end
