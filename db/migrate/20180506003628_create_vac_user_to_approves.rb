class CreateVacUserToApproves < ActiveRecord::Migration
  def change
    create_table :vac_user_to_approves do |t|
      t.references :user
      t.references :vac_approver
      t.boolean :active, default: false
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :vac_user_to_approves, :user_id
    add_index :vac_user_to_approves, :vac_approver_id
    add_index :vac_user_to_approves, :registered_by_user_id, name: 'vac_user_approvee_regis_user_id'
    add_index :vac_user_to_approves, :deactivated_by_user_id, name: 'vac_user_approvee_deact_user_id'
  end
end
