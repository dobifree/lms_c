class ChangeRelationApplicantValues < ActiveRecord::Migration
  def change
    remove_columns :sel_applicant_values, :sel_apply_form_id
    add_column :sel_applicant_values, :jm_subcharacteristic_id, :integer
  end
end
