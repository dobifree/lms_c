class AddAsistenciaPresencialToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :asistencia_presencial, :boolean, default: false
  end
end
