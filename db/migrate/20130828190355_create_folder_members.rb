class CreateFolderMembers < ActiveRecord::Migration
  def change
    create_table :folder_members do |t|
      t.references :folder
      t.references :characteristic
      t.string :valor

      t.timestamps
    end
    add_index :folder_members, :folder_id
    add_index :folder_members, :characteristic_id
  end
end
