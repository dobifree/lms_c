class AddRecordsSinceUntilToScProcess < ActiveRecord::Migration
  def change
    add_column :sc_processes, :records_since, :datetime
    add_column :sc_processes, :records_until, :datetime
  end
end
