class ChangePecentagesPeDimensionGroups < ActiveRecord::Migration
  def change
    change_column :pe_dimension_groups, :min_percentage, :float
    change_column :pe_dimension_groups, :max_percentage, :float
  end
end
