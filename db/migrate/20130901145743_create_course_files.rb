class CreateCourseFiles < ActiveRecord::Migration
  def change
    create_table :course_files do |t|
      t.string :nombre
      t.string :crypted_name
      t.string :extension
      t.text :descripcion
      t.references :course

      t.timestamps
    end
    add_index :course_files, :course_id
  end
end
