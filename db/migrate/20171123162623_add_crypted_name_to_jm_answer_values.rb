class AddCryptedNameToJmAnswerValues < ActiveRecord::Migration
  def change
    add_column :jm_answer_values, :value_file_crypted_name, :string
  end
end
