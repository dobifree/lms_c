class AddMinNumberToAssignToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :min_number_to_assign, :integer, default: 1
  end
end
