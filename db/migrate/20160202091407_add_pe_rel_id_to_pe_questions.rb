class AddPeRelIdToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :pe_rel_id, :integer
    add_index :pe_questions, :pe_rel_id
  end
end
