class CreateBrcDefinerCompanies < ActiveRecord::Migration
  def change
    create_table :brc_definer_companies do |t|
      t.references :brc_definer
      t.references :characteristic_value

      t.timestamps
    end
    add_index :brc_definer_companies, :brc_definer_id
    add_index :brc_definer_companies, :characteristic_value_id
  end
end
