class CreateBlogEntryItems < ActiveRecord::Migration
  def change
    create_table :blog_entry_items do |t|
      t.references :blog_entry
      t.references :blog_form_item
      t.text :value
      t.references :blog_list_item

      t.timestamps
    end
    add_index :blog_entry_items, :blog_entry_id
    add_index :blog_entry_items, :blog_form_item_id
    add_index :blog_entry_items, :blog_list_item_id
  end
end
