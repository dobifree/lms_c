class CreateJmAnswerValues < ActiveRecord::Migration
  def change
    create_table :jm_answer_values do |t|
      t.references :jm_candidate_answer
      t.references :jm_subcharacteristic
      t.string :value_string
      t.integer :value_int
      t.float :value_float
      t.text :value_text
      t.date :value_date
      t.text :value_file
      t.integer :value_option_id

      t.timestamps
    end
    add_index :jm_answer_values, :jm_candidate_answer_id
    add_index :jm_answer_values, :jm_subcharacteristic_id
    add_index :jm_answer_values, :value_option_id
  end
end
