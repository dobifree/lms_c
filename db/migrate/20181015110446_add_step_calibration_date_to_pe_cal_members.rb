class AddStepCalibrationDateToPeCalMembers < ActiveRecord::Migration
  def change
    add_column :pe_cal_members, :step_calibration_date, :datetime
  end
end
