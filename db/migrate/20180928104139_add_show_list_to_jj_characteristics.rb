class AddShowListToJjCharacteristics < ActiveRecord::Migration
  def change
    add_column :jj_characteristics, :show_list, :boolean, default: false
  end
end
