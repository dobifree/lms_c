class AddUpdatedByManagerToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :updated_by_manager, :boolean, default: false
  end
end
