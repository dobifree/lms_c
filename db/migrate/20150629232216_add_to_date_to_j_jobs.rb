class AddToDateToJJobs < ActiveRecord::Migration
  def change
    add_column :j_jobs, :to_date, :date
  end
end
