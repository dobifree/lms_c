class AddDisplayUserProfileShowRepDfrToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :display_user_profile_show_rep_dfr, :boolean, default: false
  end
end
