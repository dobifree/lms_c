class AddIndexToMasterEvaluationAlternativesMasterEvaluationQuestionId < ActiveRecord::Migration
  def change
    add_index :master_evaluation_alternatives, :master_evaluation_question_id, name: 'master_eval_quest_id'
  end
end
