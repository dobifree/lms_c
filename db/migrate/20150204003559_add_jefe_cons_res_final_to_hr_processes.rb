class AddJefeConsResFinalToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :jefe_cons_res_final, :boolean, default: false
  end
end
