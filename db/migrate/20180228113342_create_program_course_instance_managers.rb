class CreateProgramCourseInstanceManagers < ActiveRecord::Migration
  def change
    create_table :program_course_instance_managers do |t|
      t.references :program_course_instance
      t.references :user

      t.timestamps
    end
    add_index :program_course_instance_managers, :program_course_instance_id, :name => 'pcim_pci'
    add_index :program_course_instance_managers, :user_id, :name => 'pcim_ui'
  end
end
