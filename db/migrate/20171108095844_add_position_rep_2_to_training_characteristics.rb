class AddPositionRep2ToTrainingCharacteristics < ActiveRecord::Migration
  def change
    add_column :training_characteristics, :position_rep_2, :integer
  end
end
