class CreateHrProcessCalibrationSessions < ActiveRecord::Migration
  def change
    create_table :hr_process_calibration_sessions do |t|
      t.references :hr_process
      t.date :inicio
      t.date :fin

      t.timestamps
    end
    add_index :hr_process_calibration_sessions, :hr_process_id
  end
end
