class AddCanBePausedToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :can_be_paused, :boolean, default: false
  end
end
