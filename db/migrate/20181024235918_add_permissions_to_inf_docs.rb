class AddPermissionsToInfDocs < ActiveRecord::Migration
  def change
    add_column :informative_documents, :employee_access, :boolean, default: true
    add_column :informative_documents, :boss_access, :boolean, default: true
    add_column :informative_documents, :superior_access, :boolean, default: true
  end
end
