class CreateLmsCharacteristics < ActiveRecord::Migration
  def change
    create_table :lms_characteristics do |t|
      t.references :characteristic
      t.boolean :active_search_people, default: false
      t.integer :pos_search_people

      t.timestamps
    end
    add_index :lms_characteristics, :characteristic_id
  end
end
