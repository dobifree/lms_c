class RemoveToEveryoneFieldFromElecProcesses < ActiveRecord::Migration
  def change
    remove_columns :elec_processes, :to_everyone
    remove_columns :elec_processes, :rounds
    remove_columns :elec_processes, :max_votes
    remove_columns :elec_processes, :self_vote
    remove_columns :elec_processes, :recurrent
  end
end
