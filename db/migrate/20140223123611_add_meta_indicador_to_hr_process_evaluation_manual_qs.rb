class AddMetaIndicadorToHrProcessEvaluationManualQs < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluation_manual_qs, :meta_indicador, :integer
  end
end
