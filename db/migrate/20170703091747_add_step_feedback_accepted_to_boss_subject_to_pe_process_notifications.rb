class AddStepFeedbackAcceptedToBossSubjectToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_feedback_accepted_to_boss_subject, :string
  end
end
