class CreateEvaluationTopics < ActiveRecord::Migration
  def change
    create_table :evaluation_topics do |t|
      t.references :evaluation
      t.references :master_evaluation_topic
      t.integer :position
      t.integer :number_of_questions

      t.timestamps
    end
    add_index :evaluation_topics, :evaluation_id
    add_index :evaluation_topics, :master_evaluation_topic_id
  end
end
