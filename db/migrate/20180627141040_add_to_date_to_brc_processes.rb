class AddToDateToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :to_date, :datetime
  end
end
