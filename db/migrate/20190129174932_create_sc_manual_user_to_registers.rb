class CreateScManualUserToRegisters < ActiveRecord::Migration
  def change
    create_table :sc_manual_user_to_registers do |t|
      t.references :sc_process
      t.references :user
      t.references :he_ct_module_privilage
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :sc_manual_user_to_registers, :sc_process_id
    add_index :sc_manual_user_to_registers, :user_id
    add_index :sc_manual_user_to_registers, :he_ct_module_privilage_id
    add_index :sc_manual_user_to_registers, :registered_by_user_id
  end
end
