class CreateSelOwners < ActiveRecord::Migration
  def change
    create_table :sel_owners do |t|
      t.references :sel_process
      t.references :user

      t.timestamps
    end
    add_index :sel_owners, :sel_process_id
    add_index :sel_owners, :user_id
  end
end
