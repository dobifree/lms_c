class AddRepDetailedFinalResultsQuestionsToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_detailed_final_results_questions, :boolean, default: false
  end
end
