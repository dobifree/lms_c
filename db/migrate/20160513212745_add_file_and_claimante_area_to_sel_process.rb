class AddFileAndClaimanteAreaToSelProcess < ActiveRecord::Migration
  def change
    add_column :sel_processes, :file, :string
    add_column :sel_processes, :claimant_area, :string
  end
end
