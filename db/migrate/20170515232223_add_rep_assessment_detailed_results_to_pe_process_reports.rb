class AddRepAssessmentDetailedResultsToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_assessment_detailed_results, :boolean, default: false
  end
end
