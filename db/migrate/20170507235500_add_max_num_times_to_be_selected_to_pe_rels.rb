class AddMaxNumTimesToBeSelectedToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :max_num_times_to_be_selected, :integer, default: 1
  end
end
