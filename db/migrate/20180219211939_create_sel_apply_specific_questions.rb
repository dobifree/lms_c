class CreateSelApplySpecificQuestions < ActiveRecord::Migration
  def change
    create_table :sel_apply_specific_questions do |t|
      t.references :sel_process
      t.integer :position
      t.string :question
      t.text :description
      t.boolean :mandatory
      t.boolean :show_in_list_of_candidates

      t.timestamps
    end
    add_index :sel_apply_specific_questions, :sel_process_id
  end
end
