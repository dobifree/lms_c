class AddJCostCenterIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :j_cost_center_id, :integer
    add_index :users, :j_cost_center_id
  end
end
