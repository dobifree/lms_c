class AddTipoIndicadorToHrProcessEvaluationManualQs < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluation_manual_qs, :tipo_indicador, :integer, default: 1
    add_index :hr_process_evaluation_manual_qs, :tipo_indicador
  end
end
