class AddAliasSubPluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_sub_plu, :string, default: 'Colaboradores'
  end
end
