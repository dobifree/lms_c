class AddOnlyManagerBooleanToJmChar < ActiveRecord::Migration
  def change
    add_column :jm_characteristics, :manager_only, :boolean, :default => false
  end
end
