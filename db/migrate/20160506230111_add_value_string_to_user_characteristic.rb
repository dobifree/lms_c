class AddValueStringToUserCharacteristic < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :value_string, :string
  end
end
