class CreateUserCharacteristics < ActiveRecord::Migration
  def change
    create_table :user_characteristics do |t|
      t.references :user
      t.references :characteristic
      t.text :valor

      t.timestamps
    end
    add_index :user_characteristics, :user_id
    add_index :user_characteristics, :characteristic_id
  end
end
