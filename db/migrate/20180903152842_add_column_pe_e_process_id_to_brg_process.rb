class AddColumnPeEProcessIdToBrgProcess < ActiveRecord::Migration
  def change
    add_column :brg_processes, :pe_process_id, :integer
    add_index :brg_processes, :pe_process_id
  end
end
