class AddRegisteredByManagerToHrProcessAssessmentEfs < ActiveRecord::Migration
  def change
    add_column :hr_process_assessment_efs, :registered_by_manager, :boolean, default: false
  end
end
