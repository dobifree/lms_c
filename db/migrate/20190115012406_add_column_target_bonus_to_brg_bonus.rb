class AddColumnTargetBonusToBrgBonus < ActiveRecord::Migration
  def change
    add_column :brg_bonus, :bonus_target, :decimal,precision: 20, scale: 5, default: 0
  end
end
