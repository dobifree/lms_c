class AddStepFeedbackAliasToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_alias, :string, default: 'Retroalimentar'
  end
end
