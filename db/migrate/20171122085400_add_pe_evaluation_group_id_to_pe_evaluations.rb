class AddPeEvaluationGroupIdToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :pe_evaluation_group_id, :integer
    add_index :pe_evaluations, :pe_evaluation_group_id
  end
end
