class AddNumStepsDefinitionByUsersValidatedBeforeAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :num_steps_definition_by_users_validated_before_accepted, :integer, default: 0
  end
end
