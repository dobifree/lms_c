class AddAutocompleteToTrackingList < ActiveRecord::Migration
  def change
    add_column :tracking_lists, :autocomplete, :boolean, :default => false
  end
end
