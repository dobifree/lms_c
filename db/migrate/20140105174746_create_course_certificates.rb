class CreateCourseCertificates < ActiveRecord::Migration
  def change
    create_table :course_certificates do |t|
      t.references :course
      t.string :template
      t.date :rango_desde
      t.date :rango_hasta
      t.boolean :nombre
      t.integer :nombre_x
      t.integer :nombre_y
      t.boolean :desde
      t.integer :desde_x
      t.integer :desde_y
      t.boolean :hasta
      t.integer :hasta_x
      t.integer :hasta_y
      t.boolean :inicio
      t.integer :inicio_x
      t.integer :inicio_y
      t.boolean :fin
      t.integer :fin_x
      t.integer :fin_y
      t.boolean :nota
      t.integer :nota_x
      t.integer :nota_y
      t.string :fecha_source
      t.integer :fecha_source_delay, default: 0
      t.boolean :fecha
      t.integer :fecha_x
      t.integer :fecha_y

      t.timestamps
    end
    add_index :course_certificates, :course_id
  end
end
