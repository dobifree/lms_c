class CreateMedlicProcesses < ActiveRecord::Migration
  def change
    create_table :medlic_processes do |t|
      t.string :name
      t.text :description
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end

    add_index :medlic_processes, :registered_by_user_id
  end
end
