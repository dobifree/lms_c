class CreateSelSteps < ActiveRecord::Migration
  def change
    create_table :sel_steps do |t|
      t.references :sel_template
      t.string :name
      t.text :description
      t.integer :position
      t.integer :step_type

      t.timestamps
    end
    add_index :sel_steps, :sel_template_id
  end
end
