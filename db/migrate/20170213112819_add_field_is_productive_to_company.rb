class AddFieldIsProductiveToCompany < ActiveRecord::Migration

  def connection
    @connection = Company.connection
  end

  def change

    unless column_exists? :companies, :is_productive
      add_column :companies, :is_productive, :boolean, :default => true
    end

    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end


end
