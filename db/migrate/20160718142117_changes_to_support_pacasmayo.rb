class ChangesToSupportPacasmayo < ActiveRecord::Migration
  def change
    add_column :elec_process_rounds, :show_live_results_extra_time, :integer, :default => 0
    add_column :elec_process_rounds, :show_final_results_extra_time, :integer, :default => 0
    add_column :elec_process_rounds, :show_list_categories_in_dashboard, :boolean, :default => false
    add_column :elec_process_rounds, :show_live_my_voters, :boolean, :default => false
    add_column :elec_process_rounds, :show_live_my_voters_extra_time, :integer, :default => 0
    add_column :elec_process_rounds, :show_final_my_voters, :boolean, :default => false
    add_column :elec_process_rounds, :show_final_my_voters_extra_time, :integer, :default => 0
    add_column :elec_process_rounds, :show_my_voters_since_qty_votes, :integer, :default => 0
    add_column :elec_process_rounds, :show_my_voters_detail, :boolean, :default => false
  end

end
