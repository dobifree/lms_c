class CreateDncCharacteristics < ActiveRecord::Migration
  def change
    create_table :dnc_characteristics do |t|
      t.references :characteristic

      t.timestamps
    end
    add_index :dnc_characteristics, :characteristic_id
  end
end
