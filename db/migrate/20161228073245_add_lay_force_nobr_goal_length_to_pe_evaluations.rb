class AddLayForceNobrGoalLengthToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :lay_force_nobr_goal_length, :integer, default: 100
  end
end
