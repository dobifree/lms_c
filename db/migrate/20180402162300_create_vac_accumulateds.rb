class CreateVacAccumulateds < ActiveRecord::Migration
  def change
    create_table :vac_accumulateds do |t|
      t.references :user
      t.references :bp_general_rules_vacation
      t.integer :accumulated_days
      t.integer :accumulated_real_days
      t.integer :accumulated_progressive_days
      t.integer :taken_days
      t.integer :taken_real_days
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :vac_accumulateds, :user_id
    add_index :vac_accumulateds, :bp_general_rules_vacation_id
    add_index :vac_accumulateds, :deactivated_by_user_id
    add_index :vac_accumulateds, :registered_by_user_id

  end
end
