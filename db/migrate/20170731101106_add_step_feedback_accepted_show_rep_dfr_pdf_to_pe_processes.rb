class AddStepFeedbackAcceptedShowRepDfrPdfToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_accepted_show_rep_dfr_pdf, :boolean, default: false
  end
end
