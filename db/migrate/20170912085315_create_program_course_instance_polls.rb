class CreateProgramCourseInstancePolls < ActiveRecord::Migration
  def change
    create_table :program_course_instance_polls do |t|
      t.references :program_course_instance
      t.references :poll
      t.datetime :from_date
      t.datetime :to_date

      t.timestamps
    end
    add_index :program_course_instance_polls, :program_course_instance_id, name: 'pcip_pci_id'
    add_index :program_course_instance_polls, :poll_id, name: 'pcip_p_id'
  end
end
