class AddPeQuestionIdToPeQuestionActivities < ActiveRecord::Migration
  def change
    add_column :pe_question_activities, :pe_question_id, :integer
    add_index :pe_question_activities, :pe_question_id
  end
end
