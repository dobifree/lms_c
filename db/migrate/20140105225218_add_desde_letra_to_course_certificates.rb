class AddDesdeLetraToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :desde_letra, :string, default: 'Helvetica'
  end
end
