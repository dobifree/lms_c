class AddInternalUserFieldToJmCandidate < ActiveRecord::Migration
  def change
    add_column :jm_candidates, :internal_user, :boolean, default: false
    add_column :jm_candidates, :user_id, :integer

    add_index :jm_candidates, :user_id
  end
end
