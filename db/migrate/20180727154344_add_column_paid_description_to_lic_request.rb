class AddColumnPaidDescriptionToLicRequest < ActiveRecord::Migration
  def change
    add_column :lic_requests, :paid_description, :text
  end
end
