class AddComentarioToHrProcessEvaluationQs < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluation_qs, :comentario, :boolean, default: false
  end
end
