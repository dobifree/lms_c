class AddQueryByBossToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :query_by_boss, :boolean, default: true
  end
end
