class AddWarningLabelToTrackingFormAction < ActiveRecord::Migration
  def change
    add_column :tracking_form_actions, :label_warning, :text
  end
end
