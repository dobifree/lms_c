class AddRepDetailedFinalResultsOnlyBossElementsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :rep_detailed_final_results_only_boss_elements, :string
  end
end
