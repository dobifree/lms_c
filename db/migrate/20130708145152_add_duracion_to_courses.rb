class AddDuracionToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :duracion, :integer, default: 0
  end
end
