class FixColumnNameSurnameToJmCandidates < ActiveRecord::Migration
  def change
    rename_column :jm_candidates, :surename, :surname
  end
end
