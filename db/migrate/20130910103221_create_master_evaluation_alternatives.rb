class CreateMasterEvaluationAlternatives < ActiveRecord::Migration
  def change
    create_table :master_evaluation_alternatives do |t|
      t.integer :numero
      t.text :texto
      t.boolean :correcta
      t.references :master_evaluation_question

      t.timestamps
    end
    #add_index :master_evaluation_alternatives, :master_evaluation_question_id
  end
end
