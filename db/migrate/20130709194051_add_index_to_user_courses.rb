class AddIndexToUserCourses < ActiveRecord::Migration
  def change
  	add_index :user_courses, :course_id
  end
end
