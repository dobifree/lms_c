class AddWeightSumToPeQuestionModel < ActiveRecord::Migration
  def change
    add_column :pe_question_models, :weight_sum, :integer
  end
end
