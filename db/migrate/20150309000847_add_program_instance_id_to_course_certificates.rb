class AddProgramInstanceIdToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :program_instance_id, :integer
  end
end
