class AddLeverageToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :leverage, :float, default: 0
  end
end
