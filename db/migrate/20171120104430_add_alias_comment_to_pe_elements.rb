class AddAliasCommentToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :alias_comment, :string, default: 'Comentario'
  end
end
