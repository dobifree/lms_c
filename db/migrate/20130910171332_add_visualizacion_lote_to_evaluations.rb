class AddVisualizacionLoteToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :visualizacion_lote, :boolean
  end
end
