class AddToDateToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :to_date, :datetime
  end
end
