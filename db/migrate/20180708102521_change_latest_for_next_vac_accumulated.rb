class ChangeLatestForNextVacAccumulated < ActiveRecord::Migration
  def change
    remove_column :vac_accumulateds, :latest
    add_column :vac_accumulateds, :next_vac_accumulated_id, :integer
    add_column :vac_accumulateds, :prev_vac_accumulated_id, :integer

    add_index :vac_accumulateds, :next_vac_accumulated_id, name: 'vac_accum_next_id'
    add_index :vac_accumulateds, :prev_vac_accumulated_id, name: 'vac_accum_prev_id'
  end
end
