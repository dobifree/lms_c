class CreatePeProcessReportsEvaluations < ActiveRecord::Migration
  def change
    create_table :pe_process_reports_evaluations do |t|
      t.references :pe_process
      t.references :pe_evaluation
      t.boolean :definition_status, default: false

      t.timestamps
    end
    add_index :pe_process_reports_evaluations, :pe_process_id
    add_index :pe_process_reports_evaluations, :pe_evaluation_id
  end
end
