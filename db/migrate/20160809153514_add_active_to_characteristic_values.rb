class AddActiveToCharacteristicValues < ActiveRecord::Migration
  def change
    add_column :characteristic_values, :active, :boolean, default: true
  end
end
