class AddFromDateRProgresivasToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :from_date_r_progresivas, :boolean, default: false
  end
end
