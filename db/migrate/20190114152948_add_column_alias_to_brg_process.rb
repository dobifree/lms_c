class AddColumnAliasToBrgProcess < ActiveRecord::Migration
  def change
    add_column :brg_processes, :alias, :string, default: 'Bono Rol General'
  end
end
