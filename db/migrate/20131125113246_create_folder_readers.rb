class CreateFolderReaders < ActiveRecord::Migration
  def change
    create_table :folder_readers do |t|
      t.references :folder
      t.references :user

      t.timestamps
    end
    add_index :folder_readers, :folder_id
    add_index :folder_readers, :user_id
  end
end
