class AddManageDatesToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :manage_dates, :boolean, default: false
  end
end
