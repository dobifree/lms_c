class AddHasStepAssignToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_assign, :boolean, default: false
  end
end
