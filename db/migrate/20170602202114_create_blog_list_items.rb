class CreateBlogListItems < ActiveRecord::Migration
  def change
    create_table :blog_list_items do |t|
      t.references :blog_list
      t.integer :position
      t.string :name
      t.text :description
      t.boolean :active

      t.timestamps
    end
    add_index :blog_list_items, :blog_list_id
  end
end
