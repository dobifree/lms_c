class AddOcultoToFolderFiles < ActiveRecord::Migration
  def change
    add_column :folder_files, :oculto, :boolean, default: false
  end
end
