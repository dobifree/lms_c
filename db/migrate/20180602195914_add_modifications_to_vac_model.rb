class AddModificationsToVacModel < ActiveRecord::Migration
  def change
    add_column :vac_requests, :registered_manually, :boolean, default: false
    add_column :vac_requests, :registered_manually_description, :text

    add_column :bp_condition_vacations, :days_max_accumulated, :integer
    add_column :bp_condition_vacations, :days_min_accumulated, :integer

  end
end
