class AddLabelNotReadyDefinitionByUsersValidatedBeforeAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :label_not_ready_definition_by_users_validated_before_accepted, :string, default: 'not_ready|primary'
  end
end
