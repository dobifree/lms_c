class AddHasStepMySubsResultsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_my_subs_results, :boolean, default: false
  end
end
