class AddEmailAfterAcceptDefinitionSubjectToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :email_after_accept_definition_subject, :string
  end
end
