class AddColorToTrainingTypes < ActiveRecord::Migration
  def change
    add_column :training_types, :color, :string
  end
end
