class AddActiveGuiCalMemberToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_gui_cal_member, :boolean, default: false
  end
end
