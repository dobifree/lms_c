class AddMaxPerCantAnswerToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :max_per_cant_answer, :float, default: 100
  end
end
