class AddUfValueToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :uf_value, :float
  end
end
