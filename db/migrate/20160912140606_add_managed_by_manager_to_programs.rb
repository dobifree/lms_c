class AddManagedByManagerToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :managed_by_manager, :boolean, default: false
  end
end
