class RenameRankKpiIndicatorRanks < ActiveRecord::Migration
  def change
    rename_column :kpi_indicator_ranks, :rank, :percentage
  end
end
