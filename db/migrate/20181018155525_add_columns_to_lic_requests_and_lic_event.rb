class AddColumnsToLicRequestsAndLicEvent < ActiveRecord::Migration
  def change
    add_column :lic_events, :bp_event_id, :integer
    add_index :lic_events, :bp_event_id
    LicEvent.all.each { | event | event.update_attributes(bp_event_id: event.bp_license.bp_event_id) }
    LicRequest.all.each { |request| request.update_attributes(bp_license_id: request.lic_event.bp_license_id) }
  end
end
