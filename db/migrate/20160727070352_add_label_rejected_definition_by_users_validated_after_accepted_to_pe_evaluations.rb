class AddLabelRejectedDefinitionByUsersValidatedAfterAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :label_rejected_definition_by_users_validated_after_accepted, :string, default: 'rejected|danger'
  end
end
