class AddActivaToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :activa, :boolean, default: true
  end
end
