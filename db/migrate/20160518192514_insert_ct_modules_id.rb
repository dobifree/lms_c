class InsertCtModulesId < ActiveRecord::Migration
  def change
    execute "insert into ct_modules set cod='id', name='Documentos Informativos', active=false, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'id').count == 0
  end
end
