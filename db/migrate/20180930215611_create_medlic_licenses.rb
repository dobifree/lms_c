class CreateMedlicLicenses < ActiveRecord::Migration
  def change
    create_table :medlic_licenses do |t|
      t.references :user
      t.references :medlic_process
      t.string :license_number
      t.boolean :continuous
      t.integer :days
      t.date :date_begin
      t.date :date_end
      t.string :absence_type
      t.string :specialty
      t.boolean :partial
      t.string :user_status
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :medlic_licenses, :user_id
    add_index :medlic_licenses, :medlic_process_id
    add_index :medlic_licenses, :registered_by_user_id
  end
end
