class CreateTrackingPeriodLists < ActiveRecord::Migration
  def change
    create_table :tracking_period_lists do |t|
      t.string :name
      t.string :code
      t.text :description
      t.boolean :active

      t.timestamps
    end
  end
end
