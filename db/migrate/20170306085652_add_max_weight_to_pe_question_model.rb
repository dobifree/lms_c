class AddMaxWeightToPeQuestionModel < ActiveRecord::Migration
  def change
    add_column :pe_question_models, :max_weight, :integer
  end
end
