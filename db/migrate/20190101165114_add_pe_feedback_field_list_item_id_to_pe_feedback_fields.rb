class AddPeFeedbackFieldListItemIdToPeFeedbackFields < ActiveRecord::Migration
  def change
    add_column :pe_feedback_fields, :pe_feedback_field_list_item_id, :integer
    add_index :pe_feedback_fields, :pe_feedback_field_list_item_id
  end
end
