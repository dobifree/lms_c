class AddStepCalibrationAliasToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_calibration_alias, :string, default: 'Calibrar'
  end
end
