class CreateJGenericSkills < ActiveRecord::Migration
  def change
    create_table :j_generic_skills do |t|
      t.string :name
      t.text :description
      t.references :j_job_level

      t.timestamps
    end
    add_index :j_generic_skills, :j_job_level_id
  end
end
