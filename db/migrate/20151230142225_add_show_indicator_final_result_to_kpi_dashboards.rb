class AddShowIndicatorFinalResultToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :show_indicator_final_result, :boolean, default: false
  end
end
