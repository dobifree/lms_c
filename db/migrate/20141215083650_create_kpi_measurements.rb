class CreateKpiMeasurements < ActiveRecord::Migration
  def change
    create_table :kpi_measurements do |t|
      t.float :value
      t.float :percentage
      t.references :kpi_indicator
      t.date :measured_at

      t.timestamps
    end
    add_index :kpi_measurements, :kpi_indicator_id
  end
end
