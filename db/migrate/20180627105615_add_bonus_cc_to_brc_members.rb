class AddBonusCcToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :bonus_cc, :boolean, default: true
  end
end
