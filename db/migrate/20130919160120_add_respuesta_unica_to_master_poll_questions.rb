class AddRespuestaUnicaToMasterPollQuestions < ActiveRecord::Migration
  def change
    add_column :master_poll_questions, :respuesta_unica, :boolean
  end
end
