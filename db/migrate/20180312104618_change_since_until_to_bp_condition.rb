class ChangeSinceUntilToBpCondition < ActiveRecord::Migration

  def change
    change_column :bp_condition_vacations, :since, :datetime
    change_column :bp_condition_vacations, :until, :datetime
  end
end
