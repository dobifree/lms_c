class AddColumnRegisManToVacACcumu < ActiveRecord::Migration
  def change
    add_column :vac_accumulateds, :registered_manually, :boolean
    add_column :vac_accumulateds, :registered_manually_description, :text
  end
end
