class CreateBrgPercentageConversions < ActiveRecord::Migration
  def change
    create_table :brg_percentage_conversions do |t|
      t.float :before
      t.float :after
      t.integer :type
      t.boolean :individual, default: false
      t.boolean :group, default: false
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end

    add_index :brg_percentage_conversions, :deactivated_by_user_id
    add_index :brg_percentage_conversions, :registered_by_user_id
  end
end
