class CreatePeProcessNotifications < ActiveRecord::Migration
  def change
    create_table :pe_process_notifications do |t|
      t.references :pe_process
      t.string :step_feedback_accepted_subject
      t.text :step_feedback_accepted_body

      t.timestamps
    end
    add_index :pe_process_notifications, :pe_process_id
  end
end
