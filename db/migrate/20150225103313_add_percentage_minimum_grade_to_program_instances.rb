class AddPercentageMinimumGradeToProgramInstances < ActiveRecord::Migration
  def change
    add_column :program_instances, :percentage_minimum_grade, :boolean
  end
end
