class AddStepAssessmentBackListToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_back_list, :boolean, default: false
  end
end
