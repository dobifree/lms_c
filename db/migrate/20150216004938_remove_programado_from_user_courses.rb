class RemoveProgramadoFromUserCourses < ActiveRecord::Migration

  def change
    remove_column :user_courses, :programado
  end

end
