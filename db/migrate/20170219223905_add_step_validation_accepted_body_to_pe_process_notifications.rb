class AddStepValidationAcceptedBodyToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_validation_accepted_body, :text
  end
end
