class AddAsistenciaToUserCourses < ActiveRecord::Migration
  def change
    add_column :user_courses, :asistencia, :boolean
  end
end
