class AddToDateToUserCourseEvaluations < ActiveRecord::Migration
  def change
    add_column :user_course_evaluations, :to_date, :datetime
  end
end
