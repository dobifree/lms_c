class AddBossCheckFinalReportOnFeedbackToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :boss_check_final_report_on_feedback, :boolean
  end
end
