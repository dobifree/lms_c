class AddStepQueryDefinitionsAliasToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_query_definitions_alias, :string, default: 'Consultar definición'
  end
end
