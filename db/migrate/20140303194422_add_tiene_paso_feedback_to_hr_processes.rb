class AddTienePasoFeedbackToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :tiene_paso_feedback, :boolean
  end
end
