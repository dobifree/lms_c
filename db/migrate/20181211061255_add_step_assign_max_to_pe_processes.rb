class AddStepAssignMaxToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assign_max, :integer, default: 1
  end
end
