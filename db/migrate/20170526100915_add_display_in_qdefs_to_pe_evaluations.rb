class AddDisplayInQdefsToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :display_in_qdefs, :boolean, default: false
  end
end
