class AddPtRowsToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :pt_rows, :text
  end
end
