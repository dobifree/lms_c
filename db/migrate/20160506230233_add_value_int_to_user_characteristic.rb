class AddValueIntToUserCharacteristic < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :value_int, :int
  end
end
