class AddTabularLayoutToJmChar < ActiveRecord::Migration
  def change
    add_column :jm_characteristics, :tabular_layout, :boolean, :default => false

    JmCharacteristic.find_each do |jm_char|
      jm_char.tabular_layout = !jm_char.simple?
      jm_char.save!
    end

    remove_columns :jm_characteristics, :simple

  end
end
