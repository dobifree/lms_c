class AddRepGeneralStatusToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_general_status, :boolean, default: false
  end
end
