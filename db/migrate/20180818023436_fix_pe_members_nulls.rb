class FixPeMembersNulls < ActiveRecord::Migration
  def change
    change_column_default :pe_members, :step_assessment, false
    change_column_default :pe_members, :step_calibration, false
    change_column_default :pe_members, :step_feedback, false
    change_column_default :pe_members, :step_feedback_accepted, false
  end
end
