class AddIndicatorDiscreteGoalToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :indicator_discrete_goal, :string
  end
end
