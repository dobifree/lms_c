class AddEvalParToHrProcessDimensionEs < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :eval_par, :boolean, default: false
  end
end
