class AddMinimumAssistanceToProgramCourseInstances < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :minimum_assistance, :integer
  end
end
