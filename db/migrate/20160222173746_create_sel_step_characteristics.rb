class CreateSelStepCharacteristics < ActiveRecord::Migration
  def change
    create_table :sel_step_characteristics do |t|
      t.references :sel_step
      t.references :sel_characteristic
      t.integer :position

      t.timestamps
    end
    add_index :sel_step_characteristics, :sel_step_id
    add_index :sel_step_characteristics, :sel_characteristic_id
  end
end
