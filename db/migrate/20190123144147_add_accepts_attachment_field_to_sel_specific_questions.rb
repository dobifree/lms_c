class AddAcceptsAttachmentFieldToSelSpecificQuestions < ActiveRecord::Migration
  def change
    add_column :sel_apply_specific_questions, :accepts_attachment, :boolean, default: false
  end
end
