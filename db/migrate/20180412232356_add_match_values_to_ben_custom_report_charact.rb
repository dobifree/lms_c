class AddMatchValuesToBenCustomReportCharact < ActiveRecord::Migration
  def change
    add_column :ben_custom_report_characteristics, :match_value_string, :string
    add_column :ben_custom_report_characteristics, :match_value_text, :text
    add_column :ben_custom_report_characteristics, :match_value_date, :date
    add_column :ben_custom_report_characteristics, :match_value_int, :integer
    add_column :ben_custom_report_characteristics, :match_value_float, :float
    add_column :ben_custom_report_characteristics, :match_value_char_id, :integer

    add_column :ben_custom_reports, :bill_folio, :boolean, default: false
    add_column :ben_custom_reports, :description, :boolean, default: false
    add_column :ben_custom_reports, :cellphone_number, :boolean, default: false
    add_column :ben_custom_reports, :company_rut, :boolean, default: false
    add_column :ben_custom_reports, :net_value, :boolean, default: false
    add_column :ben_custom_reports, :discount, :boolean, default: false
    add_column :ben_custom_reports, :net_total, :boolean, default: false
    add_column :ben_custom_reports, :net_with_taxes, :boolean, default: false
    add_column :ben_custom_reports, :bill_type, :boolean, default: false
    add_column :ben_custom_reports, :not_charge_to_payroll, :boolean, default: false
    add_column :ben_custom_reports, :registered_manually, :boolean, default: false
    add_column :ben_custom_reports, :registered_manually_description, :boolean, default: false
    add_column :ben_custom_reports, :bill_item_errors, :boolean, default: false
    add_column :ben_custom_reports, :month_process, :boolean, default: false
    add_column :ben_custom_reports, :year_process, :boolean, default: false

    add_column :ben_custom_reports, :bill_folio_position, :integer
    add_column :ben_custom_reports, :description_position, :integer
    add_column :ben_custom_reports, :cellphone_number_position, :integer
    add_column :ben_custom_reports, :company_rut_position, :integer
    add_column :ben_custom_reports, :net_value_position, :integer
    add_column :ben_custom_reports, :discount_position, :integer
    add_column :ben_custom_reports, :net_total_position, :integer
    add_column :ben_custom_reports, :net_with_taxes_position, :integer
    add_column :ben_custom_reports, :bill_type_position, :integer
    add_column :ben_custom_reports, :not_charge_to_payroll_position, :integer
    add_column :ben_custom_reports, :registered_manually_position, :integer
    add_column :ben_custom_reports, :registered_manually_description_position, :integer
    add_column :ben_custom_reports, :bill_item_errors_position, :integer
    add_column :ben_custom_reports, :month_process_position, :integer
    add_column :ben_custom_reports, :year_process_position, :integer

  end

end
