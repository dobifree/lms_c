class AddPublicaToCharacteristic < ActiveRecord::Migration
  def change
    add_column :characteristics, :publica, :boolean, default: false
  end
end
