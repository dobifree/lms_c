class AddDefAllowWatchActionPlanByBossToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :def_allow_watch_action_plan_by_boss, :boolean, default: false
  end
end
