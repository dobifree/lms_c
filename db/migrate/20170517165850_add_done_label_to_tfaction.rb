class AddDoneLabelToTfaction < ActiveRecord::Migration
  def change
    add_column :tracking_form_actions, :label_done, :string
  end
end
