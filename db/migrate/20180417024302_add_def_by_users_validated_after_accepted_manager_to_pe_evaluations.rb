class AddDefByUsersValidatedAfterAcceptedManagerToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :def_by_users_validated_after_accepted_manager, :boolean, default: true
  end
end
