class CreateBrcDefiners < ActiveRecord::Migration
  def change
    create_table :brc_definers do |t|
      t.references :brc_process
      t.references :user
      t.integer :user_validate_id

      t.timestamps
    end
    add_index :brc_definers, :brc_process_id
    add_index :brc_definers, :user_id
    add_index :brc_definers, :user_validate_id
  end
end
