class CreateTrackingFormGroups < ActiveRecord::Migration
  def change
    create_table :tracking_form_groups do |t|
      t.references :tracking_form
      t.integer :position
      t.string :name
      t.text :description

      t.timestamps
    end
    add_index :tracking_form_groups, :tracking_form_id
  end
end
