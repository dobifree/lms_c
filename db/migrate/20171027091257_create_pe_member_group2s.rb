class CreatePeMemberGroup2s < ActiveRecord::Migration
  def change
    create_table :pe_member_group2s do |t|
      t.references :pe_process
      t.references :pe_member
      t.references :pe_group2

      t.timestamps
    end
    add_index :pe_member_group2s, :pe_process_id
    add_index :pe_member_group2s, :pe_member_id
    add_index :pe_member_group2s, :pe_group2_id
  end
end
