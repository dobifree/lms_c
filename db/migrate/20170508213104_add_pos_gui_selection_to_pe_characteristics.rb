class AddPosGuiSelectionToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_gui_selection, :integer
  end
end
