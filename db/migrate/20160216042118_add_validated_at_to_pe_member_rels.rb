class AddValidatedAtToPeMemberRels < ActiveRecord::Migration
  def change
    add_column :pe_member_rels, :validated_at, :datetime
  end
end
