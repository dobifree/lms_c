class AddNombreClasificacionToHrEvaluationTypeElement < ActiveRecord::Migration
  def change
    add_column :hr_evaluation_type_elements, :nombre_clasificacion, :string
  end
end
