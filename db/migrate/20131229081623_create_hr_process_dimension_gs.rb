class CreateHrProcessDimensionGs < ActiveRecord::Migration
  def change
    create_table :hr_process_dimension_gs do |t|
      t.integer :orden
      t.integer :valor_minimo
      t.integer :valor_maximo
      t.references :hr_process_dimension

      t.timestamps
    end
    add_index :hr_process_dimension_gs, :hr_process_dimension_id
  end
end
