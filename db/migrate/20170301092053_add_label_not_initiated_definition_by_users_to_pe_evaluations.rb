class AddLabelNotInitiatedDefinitionByUsersToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :label_not_initiated_definition_by_users, :string, default: 'not_initiated|default'
  end
end
