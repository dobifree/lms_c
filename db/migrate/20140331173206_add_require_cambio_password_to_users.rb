class AddRequireCambioPasswordToUsers < ActiveRecord::Migration
  def change
    add_column :users, :requiere_cambio_pass, :boolean, default: false
  end
end
