class AddKpiSetIdToKpiMeasurements < ActiveRecord::Migration
  def change
    add_column :kpi_measurements, :kpi_set_id, :integer
    add_index :kpi_measurements, :kpi_set_id
  end
end
