class AddQueryByUserToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :query_by_user, :boolean, default: true
  end
end
