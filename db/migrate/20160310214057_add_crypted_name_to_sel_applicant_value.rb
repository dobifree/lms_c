class AddCryptedNameToSelApplicantValue < ActiveRecord::Migration
  def change
    add_column :sel_applicant_values, :crypted_name, :string
  end
end
