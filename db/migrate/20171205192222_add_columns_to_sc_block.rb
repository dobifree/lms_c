class AddColumnsToScBlock < ActiveRecord::Migration
  def change
    add_column :sc_blocks, :begin_time, :time
    add_column :sc_blocks, :end_time, :time
    add_column :sc_blocks, :next_day, :boolean, :default => false
    add_column :sc_blocks, :registered_at, :datetime
    add_column :sc_blocks, :registered_by_user_id, :integer
    add_column :sc_blocks, :active, :boolean, :default => true
    add_column :sc_blocks, :deactivated_at, :datetime
    add_column :sc_blocks, :deactivated_by_user_id, :integer
    add_column :sc_blocks, :workable, :boolean, :default => true

    add_index :sc_blocks, :deactivated_by_user_id
    add_index :sc_blocks, :registered_by_user_id


    add_column :sc_schedules, :registered_at, :datetime
    add_column :sc_schedules, :registered_by_user_id, :integer
    add_column :sc_schedules, :active, :boolean, :default => true
    add_column :sc_schedules, :deactivated_at, :datetime
    add_column :sc_schedules, :deactivated_by_user_id, :integer

    add_index :sc_schedules, :deactivated_by_user_id
    add_index :sc_schedules, :registered_by_user_id

  end
end
