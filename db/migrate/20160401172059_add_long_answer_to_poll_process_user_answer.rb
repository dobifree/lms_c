class AddLongAnswerToPollProcessUserAnswer < ActiveRecord::Migration
  def change
    add_column :poll_process_user_answers, :long_answer, :text
  end
end
