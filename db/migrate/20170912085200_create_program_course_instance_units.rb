class CreateProgramCourseInstanceUnits < ActiveRecord::Migration
  def change
    create_table :program_course_instance_units do |t|
      t.references :program_course_instance
      t.references :unit
      t.datetime :from_date
      t.datetime :to_date

      t.timestamps
    end
    add_index :program_course_instance_units, :program_course_instance_id, name: 'pciu_pci_id'
    add_index :program_course_instance_units, :unit_id, name: 'pciu_u_id'
  end
end
