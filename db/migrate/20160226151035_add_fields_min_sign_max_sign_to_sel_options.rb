class AddFieldsMinSignMaxSignToSelOptions < ActiveRecord::Migration
  def change
    add_column :sel_options, :min_sign, :string
    add_column :sel_options, :max_sign, :string
  end
end
