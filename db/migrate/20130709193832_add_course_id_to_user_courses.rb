class AddCourseIdToUserCourses < ActiveRecord::Migration
  def change
    add_column :user_courses, :course_id, :integer
  end
end
