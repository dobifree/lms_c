class AddStepFeedbackAcceptedShowRepDfrTextToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_accepted_show_rep_dfr_text, :text
  end
end
