class AddMontoOToLiqItems < ActiveRecord::Migration
  def change
    add_column :liq_items, :monto_o, :integer
    add_column :liq_items, :cod_empresa_payroll, :integer
    add_column :liq_items, :cod_cecos, :string
  end
end
