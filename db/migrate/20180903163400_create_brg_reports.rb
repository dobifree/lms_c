class CreateBrgReports < ActiveRecord::Migration
  def change
    create_table :brg_reports do |t|
      t.references :brg_process
      t.boolean :partial_report, default: false
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :brg_reports, :brg_process_id
    add_index :brg_reports, :deactivated_by_user_id
    add_index :brg_reports, :registered_by_user_id
  end
end
