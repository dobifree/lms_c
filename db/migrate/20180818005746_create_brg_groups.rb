class CreateBrgGroups < ActiveRecord::Migration
  def change
    create_table :brg_groups do |t|
      t.string :group_cod
      t.string :name
      t.text :description
      t.boolean :active
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :brg_groups, :deactivated_by_user_id
    add_index :brg_groups, :registered_by_user_id
  end
end
