class AddHasStepQueryDefinitionsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_query_definitions, :boolean, default: false
  end
end
