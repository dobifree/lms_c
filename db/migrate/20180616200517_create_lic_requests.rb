class CreateLicRequests < ActiveRecord::Migration
  def change
    create_table :lic_requests do |t|
      t.references :user
      t.references :bp_licence
      t.date :event_date
      t.datetime :begin
      t.datetime :end
      t.integer :days
      t.integer :hours
      t.boolean :money_paid, default: false
      t.datetime :money_paid_at
      t.integer :money_paid_by_user_id
      t.integer :status
      t.text :status_description
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.integer :prev_lic_request_id
      t.integer :next_lic_request_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :lic_requests, :user_id
    add_index :lic_requests, :bp_licence_id
    add_index :lic_requests, :deactivated_by_user_id
    add_index :lic_requests, :registered_by_user_id
    add_index :lic_requests, :money_paid_by_user_id
    add_index :lic_requests, :prev_lic_request_id
    add_index :lic_requests, :next_lic_request_id


  end
end
