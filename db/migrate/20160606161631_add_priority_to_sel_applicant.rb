class AddPriorityToSelApplicant < ActiveRecord::Migration
  def change
    add_column :sel_applicants, :priority, :integer
  end
end
