class AddColumnBonusTargetToLvlPercentages < ActiveRecord::Migration
  def change
    add_column :brg_lvl_percentages, :income_times_target, :float
    lvls = BrgLvlPercentage.all
    lvls.each { |lvl_per| lvl_per.income_times_target = 1.0; lvl_per.save }
  end
end
