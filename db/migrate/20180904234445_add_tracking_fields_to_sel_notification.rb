class AddTrackingFieldsToSelNotification < ActiveRecord::Migration
  def change
    add_column :sel_notifications, :event_id, :integer
    add_column :sel_notifications, :jm_candidate_id, :integer
    add_column :sel_notifications, :sel_process_id, :integer
    add_column :sel_notifications, :sel_req_approver_id, :integer
    add_index :sel_notifications, :event_id
    add_index :sel_notifications, :jm_candidate_id
    add_index :sel_notifications, :sel_process_id
    add_index :sel_notifications, :sel_req_approver_id
  end
end
