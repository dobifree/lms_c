class ChangeColumnVacAccumulatedToBigDecimal < ActiveRecord::Migration
  def change
    change_column :vac_accumulateds, :accumulated_days, :decimal, :precision => 25, :scale => 20
    change_column :vac_accumulateds, :accumulated_real_days, :decimal, :precision => 25, :scale => 20
  end
end
