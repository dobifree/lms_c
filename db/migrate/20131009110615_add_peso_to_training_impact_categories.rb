class AddPesoToTrainingImpactCategories < ActiveRecord::Migration
  def change
    add_column :training_impact_categories, :peso, :float
  end
end
