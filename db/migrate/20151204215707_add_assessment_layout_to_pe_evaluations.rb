class AddAssessmentLayoutToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :assessment_layout, :integer, default: 0
  end
end
