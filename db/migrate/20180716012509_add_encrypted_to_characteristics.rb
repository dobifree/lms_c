class AddEncryptedToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :encrypted, :boolean, default: false
  end
end
