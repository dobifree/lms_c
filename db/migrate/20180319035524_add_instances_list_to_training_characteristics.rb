class AddInstancesListToTrainingCharacteristics < ActiveRecord::Migration
  def change
    add_column :training_characteristics, :instances_list, :integer
  end
end
