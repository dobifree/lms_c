class AddShowontableToSelReqItem < ActiveRecord::Migration
  def change
    add_column :sel_req_items, :show_on_table, :boolean, default: false
  end
end
