class AddPosReportingActaToLmsCharacteristics < ActiveRecord::Migration
  def change
    add_column :lms_characteristics, :pos_reporting_acta, :integer
  end
end
