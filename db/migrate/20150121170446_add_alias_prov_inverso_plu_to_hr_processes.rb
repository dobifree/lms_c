class AddAliasProvInversoPluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_prov_inverso_plu, :string, default: 'Clientes'
  end
end
