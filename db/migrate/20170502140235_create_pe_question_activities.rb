class CreatePeQuestionActivities < ActiveRecord::Migration
  def change
    create_table :pe_question_activities do |t|
      t.integer :position
      t.references :pe_question_activity_field
      t.string :value_string
      t.date :value_date
      t.references :pe_question_activity_field_list_item
      t.integer :user_creator_id

      t.timestamps
    end
    add_index :pe_question_activities, :pe_question_activity_field_id
    add_index :pe_question_activities, :pe_question_activity_field_list_item_id, name: :pe_question_activities_list_item_id
    add_index :pe_question_activities, :user_creator_id
  end
end
