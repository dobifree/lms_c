class AddBrgProcessIdToBrgLvlPercentage < ActiveRecord::Migration
  def change
    add_column :brg_lvl_percentages, :brg_process_id, :integer
    add_index :brg_lvl_percentages, :brg_process_id
  end
end
