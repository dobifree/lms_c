class AddAllowsIndividualCommentsToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :allows_individual_comments, :boolean, default: false
  end
end
