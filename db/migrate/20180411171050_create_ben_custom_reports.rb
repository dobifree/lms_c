class CreateBenCustomReports < ActiveRecord::Migration
  def change
    create_table :ben_custom_reports do |t|
      t.string :name

      t.timestamps
    end
  end
end
