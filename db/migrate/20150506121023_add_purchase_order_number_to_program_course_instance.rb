class AddPurchaseOrderNumberToProgramCourseInstance < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :purchase_order_number, :string
  end
end
