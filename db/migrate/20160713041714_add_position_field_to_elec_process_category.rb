class AddPositionFieldToElecProcessCategory < ActiveRecord::Migration
  def change
    add_column :elec_process_categories, :position, :integer
  end
end
