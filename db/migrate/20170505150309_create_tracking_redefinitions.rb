class CreateTrackingRedefinitions < ActiveRecord::Migration
  def change
    create_table :tracking_redefinitions do |t|
      t.references :tracking_form_instance
      t.integer :required_by_user_id
      t.datetime :required_at
      t.text :original_data
      t.text :final_data
      t.boolean :finished
      t.datetime :finished_at
      t.boolean :applied

      t.timestamps
    end
    add_index :tracking_redefinitions, :tracking_form_instance_id
    add_index :tracking_redefinitions, :required_by_user_id
    change_column_default :tracking_redefinitions, :finished, false
    change_column_default :tracking_redefinitions, :applied, false
  end
end
