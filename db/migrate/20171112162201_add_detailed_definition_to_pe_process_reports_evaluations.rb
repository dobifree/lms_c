class AddDetailedDefinitionToPeProcessReportsEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_process_reports_evaluations, :detailed_definition, :boolean, default: false
  end
end
