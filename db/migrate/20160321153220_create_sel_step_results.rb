class CreateSelStepResults < ActiveRecord::Migration
  def change
    create_table :sel_step_results do |t|
      t.references :sel_step_characteristic
      t.references :sel_option
      t.references :sel_step_candidate
      t.integer :registered_by_user_id
      t.integer :non_discrete_result
      t.text :comment

      t.timestamps
    end
    add_index :sel_step_results, :sel_step_characteristic_id
    add_index :sel_step_results, :sel_option_id
    add_index :sel_step_results, :sel_step_candidate_id
  end
end
