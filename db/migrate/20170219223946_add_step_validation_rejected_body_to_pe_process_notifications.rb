class AddStepValidationRejectedBodyToPeProcessNotifications < ActiveRecord::Migration
  def change
    add_column :pe_process_notifications, :step_validation_rejected_body, :text
  end
end
