class AddSelAppointmentIdToSelStepCandidate < ActiveRecord::Migration
  def change
    add_column :sel_step_candidates, :sel_appointment_id, :integer
    add_index :sel_step_candidates, :sel_appointment_id
  end
end
