class AddRegularizableAttrToBpItem < ActiveRecord::Migration
  def change
    add_column :bp_items, :regularizable, :boolean, default: false
  end
end
