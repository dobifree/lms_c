class AddIndicatorGoalToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :indicator_goal, :float
  end
end
