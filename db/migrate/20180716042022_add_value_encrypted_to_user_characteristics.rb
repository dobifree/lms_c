class AddValueEncryptedToUserCharacteristics < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :value_encrypted, :blob
  end
end
