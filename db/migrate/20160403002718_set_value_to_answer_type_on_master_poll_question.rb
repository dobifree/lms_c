class SetValueToAnswerTypeOnMasterPollQuestion < ActiveRecord::Migration
  def up
    execute <<-SQL
      update master_poll_questions set answer_type = NOT respuesta_unica
    SQL
  end

  def down
    execute <<-SQL
      update master_poll_questions set answer_type = 0
    SQL
  end
end
