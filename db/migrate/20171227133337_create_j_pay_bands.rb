class CreateJPayBands < ActiveRecord::Migration
  def change
    create_table :j_pay_bands do |t|
      t.string :name
      t.decimal :min_value, :precision => 15, :scale => 4
      t.decimal :max_value, :precision => 15, :scale => 4
      t.boolean :active, :default => true

      t.timestamps
    end
  end
end
