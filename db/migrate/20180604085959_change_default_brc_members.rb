class ChangeDefaultBrcMembers < ActiveRecord::Migration
  def change
    change_column :brc_members, :status_def, :boolean, :default => false
    change_column :brc_members, :status_val, :boolean, :default => false
  end
end
