class AddEnteredByManagerQuestionsToPeEvaluation < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :entered_by_manager_questions, :boolean, default: false
  end
end
