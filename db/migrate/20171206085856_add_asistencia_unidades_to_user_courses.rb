class AddAsistenciaUnidadesToUserCourses < ActiveRecord::Migration
  def change
    add_column :user_courses, :asistencia_unidades, :float, :default => 0
  end
end
