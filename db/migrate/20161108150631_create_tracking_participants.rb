class CreateTrackingParticipants < ActiveRecord::Migration
  def change
    create_table :tracking_participants do |t|
      t.references :tracking_process
      t.integer :attendant_user_id
      t.integer :subject_user_id

      t.timestamps
    end
    add_index :tracking_participants, :tracking_process_id
    add_index :tracking_participants, :attendant_user_id
    add_index :tracking_participants, :subject_user_id
  end
end
