class MoverTablaAdminsNuevaBd < ActiveRecord::Migration
  def connection
    @connection = Admin.connection
  end

  def change

    # esto se ejecuta en la bd de administración
    # como se va a ejecutar para cada plataforma, solo es necesario que se ejecute para la primera
    unless table_exists? :admins
      create_table :admins do |t|
        t.string :username
        t.string :password_digest

        t.timestamps
      end

      # se crean los administradores

      @connection.execute("INSERT INTO `admins` VALUES
                    (1,'alejo','$2a$10$qT5YhnT1sSkoGnz.nY7bLOwMWVpxf8/NtAYlnEMYAnQHEWT.w8WSG','2015-02-16 07:39:43','2015-02-16 07:39:43'),
                    (2,'rrabines','$2a$10$76ocZZ/MCA4rrMLOv.IroeWfq2aLNBpvS.Oi6MjqZY/OBGCQgigJS','2015-02-16 07:39:44','2015-02-16 07:39:44'),
                    (3,'adminCL','$2a$10$WEwJGreb7YApc1zQfl1E8OoH/DjjBRCZ1Tt3Tv7GMrGB4370ibvx6','2015-02-16 07:44:45','2015-02-16 07:44:45'),
                    (4,'corrego','$2a$10$FoiwlHnm.lu9cr8CalGUkO6ZWz0QL6NYTYp6ThLAUD6mkXeXlGiem','2016-01-13 19:36:00','2016-02-10 20:12:29');")

      #Admin.create({:username => 'alejo', :password_digest => '$2a$10$qT5YhnT1sSkoGnz.nY7bLOwMWVpxf8/NtAYlnEMYAnQHEWT.w8WSG'}, :without_protection => true)
      #Admin.create({:username => 'rrabines', :password_digest => '$2a$10$76ocZZ/MCA4rrMLOv.IroeWfq2aLNBpvS.Oi6MjqZY/OBGCQgigJS'}, :without_protection => true)
      #Admin.create({:username => 'adminCL', :password_digest => '$2a$10$WEwJGreb7YApc1zQfl1E8OoH/DjjBRCZ1Tt3Tv7GMrGB4370ibvx6'}, :without_protection => true)
      #Admin.create({:username => 'corrego', :password_digest => '$2a$10$FoiwlHnm.lu9cr8CalGUkO6ZWz0QL6NYTYp6ThLAUD6mkXeXlGiem'}, :without_protection => true)
    end

    # esto se ejecuta en la bd de la 'company' específica
    # se borra la tabla admins que ya fue creada en la bd de administración
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection

    #drop_table :admins

    # se actualizan las referencias a la tabla de admins, para evitar problemas, se asignan todos al usuario rrabines
    PollProcess.where(:finished_by_user_id => [1,2,3,4,5,6,7,8]).update_all(:finished_by_user_id => 2)
    UserCharacteristicRecord.where(:created_by_admin_id => [1,2,3,4,5,6,7,8]).update_all(:created_by_admin_id => 2)
    UserCharacteristicRecord.where(:finished_by_admin_id => [1,2,3,4,5,6,7,8]).update_all(:finished_by_admin_id => 2)

    @connection.add_column :admins, :tabla_borrada, :boolean, :default => true

  end
end
