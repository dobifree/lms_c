class RemoveColumnFormReportFolioStf < ActiveRecord::Migration
  def change
    remove_column :ben_custom_reports, :bill_folio
    remove_column :ben_custom_reports, :description
    remove_column :ben_custom_reports, :cellphone_number
    remove_column :ben_custom_reports, :company_rut
    remove_column :ben_custom_reports, :net_value
    remove_column :ben_custom_reports, :discount
    remove_column :ben_custom_reports, :net_total
    remove_column :ben_custom_reports, :net_with_taxes
    remove_column :ben_custom_reports, :bill_type
    remove_column :ben_custom_reports, :not_charge_to_payroll
    remove_column :ben_custom_reports, :registered_manually
    remove_column :ben_custom_reports, :registered_manually_description
    remove_column :ben_custom_reports, :bill_item_errors
    remove_column :ben_custom_reports, :month_process
    remove_column :ben_custom_reports, :year_process
    remove_column :ben_custom_reports, :bill_folio_position
    remove_column :ben_custom_reports, :description_position
    remove_column :ben_custom_reports, :cellphone_number_position
    remove_column :ben_custom_reports, :company_rut_position
    remove_column :ben_custom_reports, :net_value_position
    remove_column :ben_custom_reports, :discount_position
    remove_column :ben_custom_reports, :net_total_position
    remove_column :ben_custom_reports, :net_with_taxes_position
    remove_column :ben_custom_reports, :bill_type_position
    remove_column :ben_custom_reports, :not_charge_to_payroll_position
    remove_column :ben_custom_reports, :registered_manually_position
    remove_column :ben_custom_reports, :registered_manually_description_position
    remove_column :ben_custom_reports, :bill_item_errors_position
    remove_column :ben_custom_reports, :month_process_position
    remove_column :ben_custom_reports, :year_process_position
  end
end
