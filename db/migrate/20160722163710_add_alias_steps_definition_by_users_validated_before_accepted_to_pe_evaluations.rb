class AddAliasStepsDefinitionByUsersValidatedBeforeAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :alias_steps_definition_by_users_validated_before_accepted, :string
  end
end
