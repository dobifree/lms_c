class CreateLogLogins < ActiveRecord::Migration
  def change
    create_table :log_logins do |t|
      t.references :user
      t.datetime :fecha
      t.string :http_user_agent
      t.string :browser
      t.string :version
      t.string :platform

      t.timestamps
    end
    add_index :log_logins, :user_id
  end
end
