class AddParentCharacteristicIdToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :parent_characteristic_id, :integer
    add_index :characteristics, :parent_characteristic_id
  end
end
