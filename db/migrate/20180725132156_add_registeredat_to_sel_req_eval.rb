class AddRegisteredatToSelReqEval < ActiveRecord::Migration
  def change
    add_column :sel_req_approver_evals, :registered_at, :datetime
  end
end
