class CreateLiqProcesses < ActiveRecord::Migration
  def change
    create_table :liq_processes do |t|
      t.integer :year
      t.integer :month

      t.timestamps
    end
  end
end
