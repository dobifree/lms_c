class AddColumnUserIdToVacRecords < ActiveRecord::Migration
  def change
    add_column :vac_records, :user_id, :integer
    add_index :vac_records, :user_id
  end
end
