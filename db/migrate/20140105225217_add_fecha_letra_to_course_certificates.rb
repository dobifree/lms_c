class AddFechaLetraToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :fecha_letra, :string, default: 'Helvetica'
  end
end
