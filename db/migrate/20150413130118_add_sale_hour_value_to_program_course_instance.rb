class AddSaleHourValueToProgramCourseInstance < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :sale_hour_value, :float
  end
end
