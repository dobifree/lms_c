class CreateBenCustomReportCharacteristics < ActiveRecord::Migration
  def change
    create_table :ben_custom_report_characteristics do |t|
      t.references :ben_custom_report
      t.references :ben_cellphone_characteristic
      t.integer :position

      t.timestamps
    end
    add_index :ben_custom_report_characteristics, :ben_custom_report_id, name: "ben_report_charac_rep_id"
    add_index :ben_custom_report_characteristics, :ben_cellphone_characteristic_id, name: "ben_report_charac_cell_id"
  end
end
