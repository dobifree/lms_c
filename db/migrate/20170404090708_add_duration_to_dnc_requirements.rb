class AddDurationToDncRequirements < ActiveRecord::Migration
  def change
    add_column :dnc_requirements, :duration, :integer
  end
end
