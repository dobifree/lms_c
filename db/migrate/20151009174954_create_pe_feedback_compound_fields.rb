class CreatePeFeedbackCompoundFields < ActiveRecord::Migration
  def change
    create_table :pe_feedback_compound_fields do |t|
      t.references :pe_feedback_field
      t.integer :position
      t.string :name

      t.timestamps
    end
    add_index :pe_feedback_compound_fields, :pe_feedback_field_id
  end
end
