class AddPlanAnualToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :plan_anual, :boolean, default: false
  end
end
