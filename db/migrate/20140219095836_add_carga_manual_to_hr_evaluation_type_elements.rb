class AddCargaManualToHrEvaluationTypeElements < ActiveRecord::Migration
  def change
    add_column :hr_evaluation_type_elements, :carga_manual, :boolean
  end
end
