class AddFromDateToUserCoursePolls < ActiveRecord::Migration
  def change
    add_column :user_course_polls, :from_date, :datetime
  end
end
