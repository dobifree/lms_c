class AddAliasCliToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_cli, :string, default: 'Cliente'
  end
end
