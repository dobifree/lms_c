class AddColumnRegisteredAtToVacRequest < ActiveRecord::Migration
  def change
    add_column :vac_requests, :registered_by_user_id, :integer
    add_index :vac_requests, :registered_by_user_id, name: 'vac_request_regis_user_id'
  end
end
