class AddGroupToMasterPoll < ActiveRecord::Migration
  def change
    add_column :master_poll_questions, :group, :string
  end
end
