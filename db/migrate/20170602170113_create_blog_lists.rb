class CreateBlogLists < ActiveRecord::Migration
  def change
    create_table :blog_lists do |t|
      t.string :name
      t.text :descripcion
      t.boolean :active

      t.timestamps
    end
  end
end
