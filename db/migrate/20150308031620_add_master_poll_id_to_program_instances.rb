class AddMasterPollIdToProgramInstances < ActiveRecord::Migration
  def change
    add_column :program_instances, :master_poll_id, :integer
  end
end
