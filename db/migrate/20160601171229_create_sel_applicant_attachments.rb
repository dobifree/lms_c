class CreateSelApplicantAttachments < ActiveRecord::Migration
  def change
    create_table :sel_applicant_attachments do |t|
      t.references :sel_applicant
      t.string :description
      t.string :crypted_name
      t.string :original_filename
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :sel_applicant_attachments, :sel_applicant_id
    add_index :sel_applicant_attachments, :registered_by_user_id
  end
end
