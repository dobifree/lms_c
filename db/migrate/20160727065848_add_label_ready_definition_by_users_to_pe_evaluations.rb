class AddLabelReadyDefinitionByUsersToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :label_ready_definition_by_users, :string, default: 'ready|success'
  end
end
