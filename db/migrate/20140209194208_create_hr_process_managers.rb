class CreateHrProcessManagers < ActiveRecord::Migration
  def change
    create_table :hr_process_managers do |t|
      t.references :hr_process
      t.references :user

      t.timestamps
    end
    add_index :hr_process_managers, :hr_process_id
    add_index :hr_process_managers, :user_id
  end
end
