class AddConfirmedToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :confirmed, :boolean, default: true
  end
end
