class AddIndexOrdenToEvaluations < ActiveRecord::Migration
  def change
    add_index :evaluations, :orden
  end
end
