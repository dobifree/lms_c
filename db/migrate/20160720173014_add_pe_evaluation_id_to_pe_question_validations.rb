class AddPeEvaluationIdToPeQuestionValidations < ActiveRecord::Migration
  def change
    add_column :pe_question_validations, :pe_evaluation_id, :integer
    add_index :pe_question_validations, :pe_evaluation_id
  end
end
