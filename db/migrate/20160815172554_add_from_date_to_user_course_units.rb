class AddFromDateToUserCourseUnits < ActiveRecord::Migration
  def change
    add_column :user_course_units, :from_date, :datetime
  end
end
