class AddDiscreteDisplayInReportsToHrProcessEvaluationQs < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluation_qs, :discrete_display_in_reports, :boolean, default: false
  end
end
