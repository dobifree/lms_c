class AddObservacionesToDncs < ActiveRecord::Migration
  def change
    add_column :dncs, :observaciones, :text
  end
end
