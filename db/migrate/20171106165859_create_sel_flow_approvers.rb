class CreateSelFlowApprovers < ActiveRecord::Migration
  def change
    create_table :sel_flow_approvers do |t|
      t.references :sel_vacant_flow
      t.integer :position
      t.string :name
      t.text :description
      t.boolean :mandatory
      t.float :weight
      t.boolean :conditions_rejection
      t.integer :prerequisite_position
      t.integer :approver_type

      t.timestamps
    end
    add_index :sel_flow_approvers, :sel_vacant_flow_id
  end
end
