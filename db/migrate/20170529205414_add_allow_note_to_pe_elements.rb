class AddAllowNoteToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :allow_note, :boolean, default: false
  end
end
