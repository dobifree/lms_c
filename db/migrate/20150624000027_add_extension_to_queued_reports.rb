class AddExtensionToQueuedReports < ActiveRecord::Migration
  def change
    add_column :queued_reports, :extension, :string
  end
end
