class AddWeightToPeMemberRels < ActiveRecord::Migration
  def change
    add_column :pe_member_rels, :weight, :float, default: 1
  end
end
