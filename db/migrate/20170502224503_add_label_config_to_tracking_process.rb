class AddLabelConfigToTrackingProcess < ActiveRecord::Migration
  def change
    add_column :tracking_processes, :label_due_date, :string
    add_column :tracking_processes, :label_comment_due_date, :string
    add_column :tracking_processes, :ask_for_comment_due_date, :boolean, :default => false
  end
end
