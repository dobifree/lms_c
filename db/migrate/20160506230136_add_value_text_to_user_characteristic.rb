class AddValueTextToUserCharacteristic < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :value_text, :text
  end
end
