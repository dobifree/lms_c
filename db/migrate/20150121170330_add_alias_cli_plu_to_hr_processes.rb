class AddAliasCliPluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_cli_plu, :string, default: 'Clientes'
  end
end
