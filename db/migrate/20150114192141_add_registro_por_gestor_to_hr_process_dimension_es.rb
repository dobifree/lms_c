class AddRegistroPorGestorToHrProcessDimensionEs < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :registro_por_gestor, :boolean, default: false
  end
end
