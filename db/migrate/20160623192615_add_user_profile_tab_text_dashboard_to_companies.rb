class AddUserProfileTabTextDashboardToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :user_profile_tab_text_dashboard, :string, default: 'Perfil de Usuario'
  end
end
