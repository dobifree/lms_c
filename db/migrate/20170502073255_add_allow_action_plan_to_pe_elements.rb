class AddAllowActionPlanToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :allow_action_plan, :boolean, default: false
  end
end
