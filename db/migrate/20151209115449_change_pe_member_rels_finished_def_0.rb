class ChangePeMemberRelsFinishedDef0 < ActiveRecord::Migration
  def change
    change_column :pe_member_rels, :finished, :boolean, default: 0
  end
end
