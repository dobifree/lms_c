class AddRegisterIdJToJcharacteristics < ActiveRecord::Migration
  def change
    add_column :jj_characteristics, :register_characteristic_id, :integer
    add_index :jj_characteristics, :register_characteristic_id
  end
end
