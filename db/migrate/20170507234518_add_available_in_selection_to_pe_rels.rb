class AddAvailableInSelectionToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :available_in_selection, :boolean, default: false
  end
end
