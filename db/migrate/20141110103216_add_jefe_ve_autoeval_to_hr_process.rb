class AddJefeVeAutoevalToHrProcess < ActiveRecord::Migration
  def change
    add_column :hr_processes, :jefe_ve_autoeval, :boolean, default: false
  end
end
