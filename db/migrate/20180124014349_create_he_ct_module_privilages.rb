class CreateHeCtModulePrivilages < ActiveRecord::Migration
  def change
    create_table :he_ct_module_privilages do |t|
      t.references :user
      t.boolean :recorder, :default => false
      t.boolean :validator, :default => false

      t.timestamps
    end
    add_index :he_ct_module_privilages, :user_id
  end
end
