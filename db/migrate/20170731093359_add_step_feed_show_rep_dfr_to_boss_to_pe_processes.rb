class AddStepFeedShowRepDfrToBossToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feed_show_rep_dfr_to_boss, :boolean, default: false
  end
end
