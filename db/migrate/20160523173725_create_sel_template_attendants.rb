class CreateSelTemplateAttendants < ActiveRecord::Migration
  def change
    create_table :sel_template_attendants do |t|
      t.references :sel_step
      t.references :user
      t.boolean :role

      t.timestamps
    end
    add_index :sel_template_attendants, :sel_step_id
    add_index :sel_template_attendants, :user_id
  end
end
