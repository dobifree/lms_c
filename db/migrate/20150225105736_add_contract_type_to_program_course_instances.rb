class AddContractTypeToProgramCourseInstances < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :contract_type, :string
  end
end
