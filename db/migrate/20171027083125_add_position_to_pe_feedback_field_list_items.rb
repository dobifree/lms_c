class AddPositionToPeFeedbackFieldListItems < ActiveRecord::Migration
  def change
    add_column :pe_feedback_field_list_items, :position, :integer
  end
end
