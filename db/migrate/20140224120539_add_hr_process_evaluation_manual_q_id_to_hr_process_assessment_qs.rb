class AddHrProcessEvaluationManualQIdToHrProcessAssessmentQs < ActiveRecord::Migration
  def change
    add_column :hr_process_assessment_qs, :hr_process_evaluation_manual_q_id, :integer
    add_index :hr_process_assessment_qs, :hr_process_evaluation_manual_q_id, :name => 'hr_p_a_q_hr_process_evaluation_manual_q_id'

  end
end
