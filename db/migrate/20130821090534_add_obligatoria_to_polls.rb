class AddObligatoriaToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :obligatoria, :boolean, default: false
  end
end
