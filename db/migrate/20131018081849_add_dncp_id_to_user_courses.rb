class AddDncpIdToUserCourses < ActiveRecord::Migration
  def change
    add_column :user_courses, :dncp_id, :integer
  end
end
