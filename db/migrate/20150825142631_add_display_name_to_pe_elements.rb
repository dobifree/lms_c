class AddDisplayNameToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :display_name, :boolean, default: true
  end
end
