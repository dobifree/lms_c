class CreateMasterEvaluations < ActiveRecord::Migration
  def change
    create_table :master_evaluations do |t|
      t.integer :numero
      t.string :nombre
      t.text :descripcion
      t.string :tags

      t.timestamps
    end
  end
end
