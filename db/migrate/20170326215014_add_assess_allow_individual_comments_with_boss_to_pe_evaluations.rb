class AddAssessAllowIndividualCommentsWithBossToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :assess_allow_individual_comments_with_boss, :boolean, default: false
  end
end
