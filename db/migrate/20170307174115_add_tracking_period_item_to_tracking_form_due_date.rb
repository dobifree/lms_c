class AddTrackingPeriodItemToTrackingFormDueDate < ActiveRecord::Migration
  def change
    add_column :tracking_form_due_dates, :tracking_period_item_id, :integer
    add_index :tracking_form_due_dates, :tracking_period_item_id

    add_column :tracking_form_questions, :tracking_period_list_id, :integer
    add_index :tracking_form_questions, :tracking_period_list_id
  end
end
