class AddEvalSubToHrProcessDimensionEs < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :eval_sub, :boolean, default: false
  end
end
