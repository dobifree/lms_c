class AddNoteCompulsoryToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :note_compulsory, :boolean, :default => false
  end
end
