class AddStepFeedbackMenuToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_menu, :boolean, default: true
  end
end
