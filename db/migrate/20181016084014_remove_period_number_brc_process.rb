class RemovePeriodNumberBrcProcess < ActiveRecord::Migration
  def change
    remove_column :brc_processes, :period_number
  end
end
