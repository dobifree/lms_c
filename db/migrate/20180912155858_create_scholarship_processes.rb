class CreateScholarshipProcesses < ActiveRecord::Migration
  def change
    create_table :scholarship_processes do |t|
      t.string :name
      t.text :description
      t.boolean :active
      t.date :app_from_date
      t.date :app_to_date

      t.timestamps
    end
  end
end
