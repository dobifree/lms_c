class AddShowUserProfileToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :show_user_profile, :boolean
  end
end
