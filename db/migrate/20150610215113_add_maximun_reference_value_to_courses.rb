class AddMaximunReferenceValueToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :maximum_reference_value, :float
  end
end
