class ChangeFinishedAtToDatetimeFieldAtSelProcess < ActiveRecord::Migration
  def change
    change_column :sel_processes, :finished_at, :datetime
  end
end
