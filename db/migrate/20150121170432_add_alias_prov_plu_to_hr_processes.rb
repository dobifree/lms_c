class AddAliasProvPluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_prov_plu, :string, default: 'Proveedores'
  end
end
