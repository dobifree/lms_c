class CreatePollProcessUserAnswers < ActiveRecord::Migration
  def change
    create_table :poll_process_user_answers do |t|
      t.references :poll_process_user
      t.references :master_poll_question
      t.references :master_poll_alternative

      t.timestamps
    end
    add_index :poll_process_user_answers, :poll_process_user_id
    add_index :poll_process_user_answers, :master_poll_question_id
    add_index :poll_process_user_answers, :master_poll_alternative_id
  end
end
