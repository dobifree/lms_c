class CreateCharacteristicValues < ActiveRecord::Migration
  def change
    create_table :characteristic_values do |t|
      t.references :characteristic
      t.string :value_string

      t.timestamps
    end
    add_index :characteristic_values, :characteristic_id
  end
end
