class AddAllowDefinitionByUsersToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :allow_definition_by_users, :boolean, default: false
  end
end
