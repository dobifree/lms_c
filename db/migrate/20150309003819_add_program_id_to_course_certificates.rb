class AddProgramIdToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :program_id, :integer
  end
end
