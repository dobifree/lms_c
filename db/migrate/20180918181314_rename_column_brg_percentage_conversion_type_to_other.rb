class RenameColumnBrgPercentageConversionTypeToOther < ActiveRecord::Migration
  def change
    rename_column :brg_percentage_conversions, :type, :type_1
  end
end
