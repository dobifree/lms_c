class AddRequiredToPeQuestionActivityFields < ActiveRecord::Migration
  def change
    add_column :pe_question_activity_fields, :required, :boolean, default: true
  end
end
