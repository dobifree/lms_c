class AddColumnNextRequestToVacRequest < ActiveRecord::Migration
  def change
    add_column :vac_requests, :next_request_id, :integer
    add_index :vac_requests, :next_request_id
  end
end
