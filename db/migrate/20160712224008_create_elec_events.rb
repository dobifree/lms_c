class CreateElecEvents < ActiveRecord::Migration
  def change
    create_table :elec_events do |t|
      t.references :elec_process
      t.references :elec_recurrence
      t.boolean :manual
      t.boolean :active
      t.datetime :from_date

      t.timestamps
    end
    add_index :elec_events, :elec_process_id
    add_index :elec_events, :elec_recurrence_id
  end
end
