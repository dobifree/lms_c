class ChangeLocationDefaultFromBanners < ActiveRecord::Migration
  def change
    change_column_default :banners, :location, 0
  end
end
