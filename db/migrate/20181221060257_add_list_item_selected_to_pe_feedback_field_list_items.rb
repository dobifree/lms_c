class AddListItemSelectedToPeFeedbackFieldListItems < ActiveRecord::Migration
  def change
    add_column :pe_feedback_field_list_items, :list_item_selected, :boolean, default: true
  end
end
