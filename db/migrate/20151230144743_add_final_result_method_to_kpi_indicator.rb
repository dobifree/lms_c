class AddFinalResultMethodToKpiIndicator < ActiveRecord::Migration
  def change
    add_column :kpi_indicators, :final_result_method, :integer, default: 0
  end
end
