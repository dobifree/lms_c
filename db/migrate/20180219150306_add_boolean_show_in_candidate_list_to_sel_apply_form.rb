class AddBooleanShowInCandidateListToSelApplyForm < ActiveRecord::Migration
  def change
    add_column :sel_apply_forms, :show_in_list_of_candidates, :boolean, :default => true
  end
end
