class CreateDncStudentPs < ActiveRecord::Migration
  def change
    create_table :dnc_student_ps do |t|
      t.references :dncp
      t.references :user

      t.timestamps
    end
    add_index :dnc_student_ps, :dncp_id
    add_index :dnc_student_ps, :user_id
  end
end
