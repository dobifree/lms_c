class AddNumberOfActivitiesToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :number_of_activities, :integer, default: 0
  end
end
