class AddStoredImageIdToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :stored_image_id, :integer
    add_index :pe_questions, :stored_image_id
  end
end
