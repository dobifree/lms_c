class CreateDncProviders < ActiveRecord::Migration
  def change
    create_table :dnc_providers do |t|
      t.string :name

      t.timestamps
    end
  end
end
