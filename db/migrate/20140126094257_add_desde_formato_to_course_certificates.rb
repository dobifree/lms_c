class AddDesdeFormatoToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :desde_formato, :string, default: 'day_snmonth_year'
  end
end
