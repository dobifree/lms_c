class ChangeNameValueHourToValueTimeLicItemValue < ActiveRecord::Migration
  def change
    rename_column :lic_item_values, :value_hour, :value_time
  end
end
