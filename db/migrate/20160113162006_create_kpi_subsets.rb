class CreateKpiSubsets < ActiveRecord::Migration
  def change
    create_table :kpi_subsets do |t|
      t.integer :position
      t.string :name
      t.references :kpi_set
      t.float :value
      t.float :percentage
      t.datetime :last_update
      t.date :last_measurement_update

      t.timestamps
    end
    add_index :kpi_subsets, :kpi_set_id
  end
end
