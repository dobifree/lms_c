class AddAliasSubInversoToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_sub_inverso, :string, default: 'Supervisor'
  end
end
