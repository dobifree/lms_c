class AddDefinitionByUsersFinishMessageToPeEvaluation < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :definition_by_user_finish_message, :string, default: 'Confirmo que he finalizado la definición'
  end
end
