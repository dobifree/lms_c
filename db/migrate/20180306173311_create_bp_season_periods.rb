class CreateBpSeasonPeriods < ActiveRecord::Migration
  def change
    create_table :bp_season_periods do |t|
      t.references :bp_season
      t.date :begin
      t.date :end
      t.boolean :active
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id

      t.timestamps
    end
    add_index :bp_season_periods, :bp_season_id
    add_index :bp_season_periods, :deactivated_by_admin_id, :name => 'bp_season_periods_deac_adm_id'
  end
end
