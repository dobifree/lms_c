class CreateBrgReportedBonus < ActiveRecord::Migration
  def change
    create_table :brg_reported_bonus do |t|
      t.references :brg_bonus
      t.references :brg_report

      t.timestamps
    end
    add_index :brg_reported_bonus, :brg_bonus_id
    add_index :brg_reported_bonus, :brg_report_id
  end
end
