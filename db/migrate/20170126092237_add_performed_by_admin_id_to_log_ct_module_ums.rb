class AddPerformedByAdminIdToLogCtModuleUms < ActiveRecord::Migration
  def change
    add_column :log_ct_module_ums, :performed_by_admin_id, :integer
    add_index :log_ct_module_ums, :performed_by_admin_id
  end
end
