class AddPublicPeMembersBoxToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :public_pe_members_box, :boolean, default: true
  end
end
