class AddNotaSizeToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :nota_size, :integer, default: 20
  end
end
