class CreateKpiIndicatorRols < ActiveRecord::Migration
  def change
    create_table :kpi_indicator_rols do |t|
      t.references :kpi_indicator
      t.references :kpi_rol

      t.timestamps
    end
    add_index :kpi_indicator_rols, :kpi_indicator_id
    add_index :kpi_indicator_rols, :kpi_rol_id
  end
end
