class AddUpdatedByUserToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :updated_by_user, :boolean, default: false
  end
end
