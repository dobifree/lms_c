class AddMpiViewUsersPeHistoryToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :mpi_view_users_pe_history, :boolean, default: false
  end
end
