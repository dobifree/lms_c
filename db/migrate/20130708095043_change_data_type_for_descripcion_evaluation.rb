class ChangeDataTypeForDescripcionEvaluation < ActiveRecord::Migration
  def up
  	change_table :evaluations do |t|
      t.change :descripcion, :text
    end
  end

  def down
  end
end
