class CreateFolderOwners < ActiveRecord::Migration
  def change
    create_table :folder_owners do |t|
      t.references :folder
      t.references :user

      t.timestamps
    end
    add_index :folder_owners, :folder_id
    add_index :folder_owners, :user_id
  end
end
