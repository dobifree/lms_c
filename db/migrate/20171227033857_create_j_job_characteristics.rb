class CreateJJobCharacteristics < ActiveRecord::Migration
  def change
    create_table :j_job_characteristics do |t|
      t.references :j_job
      t.references :characteristic
      t.string :value_string
      t.references :characteristic_value

      t.timestamps
    end
    add_index :j_job_characteristics, :j_job_id
    add_index :j_job_characteristics, :characteristic_id
    add_index :j_job_characteristics, :characteristic_value_id
  end
end
