class AddHasRepDetailedFinalResultsOnlyBossToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_rep_detailed_final_results_only_boss, :boolean, default: false
  end
end
