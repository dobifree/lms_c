class AddColumnsToVacRequestAndAccumulated < ActiveRecord::Migration
  def change
    remove_column :vac_accumulateds, :taken_days
    remove_column :vac_accumulateds, :taken_real_days
    add_column :vac_accumulateds, :up_to_date, :date
    add_column :vac_requests, :days_manually, :decimal, precision: 25, scale: 20
  end
end
