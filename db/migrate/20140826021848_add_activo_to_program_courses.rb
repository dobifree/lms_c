class AddActivoToProgramCourses < ActiveRecord::Migration
  def change
    add_column :program_courses, :activo, :boolean, default: true
  end
end
