class CreateJmCandidates < ActiveRecord::Migration
  def change
    create_table :jm_candidates do |t|
      t.string :name
      t.string :surename
      t.string :email
      t.string :phone
      t.string :password_digest

      t.timestamps
    end
  end
end
