class AddPerpertualActiveToUsers < ActiveRecord::Migration
  def change
    add_column :users, :perpetual_active, :boolean
  end
end
