class AddValidatorIdToPeMemberRels < ActiveRecord::Migration
  def change
    add_column :pe_member_rels, :validator_id, :integer
    add_index :pe_member_rels, :validator_id
  end
end
