class AddStepAssessmentDateToPeAreas < ActiveRecord::Migration
  def change
    add_column :pe_areas, :step_assessment_date, :datetime
  end
end
