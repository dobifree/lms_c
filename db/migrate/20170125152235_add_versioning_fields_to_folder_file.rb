class AddVersioningFieldsToFolderFile < ActiveRecord::Migration
  def change
    add_column :folder_files, :folder_file_father_id, :integer
    add_column :folder_files, :is_last, :boolean, :default => true

    add_index :folder_files, :folder_file_father_id

  end
end
