class ChangeDefaultValueForVacDaysProgAndLegal < ActiveRecord::Migration
  def change
    change_column_default :vac_requests, :days, 0
    change_column_default :vac_requests, :days_progressive, 0
  end
end
