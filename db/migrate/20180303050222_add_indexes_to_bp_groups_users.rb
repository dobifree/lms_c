class AddIndexesToBpGroupsUsers < ActiveRecord::Migration
  def change
    add_index :bp_groups_users, :registered_by_admin_id
    add_index :bp_groups_users, :deactivated_by_admin_id
  end
end
