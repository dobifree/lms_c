class CreatePeMemberEvaluations < ActiveRecord::Migration
  def change
    create_table :pe_member_evaluations do |t|
      t.references :pe_member
      t.references :pe_evaluation
      t.references :pe_process
      t.float :weight

      t.timestamps
    end
    add_index :pe_member_evaluations, :pe_member_id
    add_index :pe_member_evaluations, :pe_evaluation_id
    add_index :pe_member_evaluations, :pe_process_id
  end
end
