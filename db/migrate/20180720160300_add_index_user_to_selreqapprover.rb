class AddIndexUserToSelreqapprover < ActiveRecord::Migration
  def change
    add_index :sel_req_approvers, :user_id
  end
end
