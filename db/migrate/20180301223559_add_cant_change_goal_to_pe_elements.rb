class AddCantChangeGoalToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :cant_change_goal, :boolean, :default => false
  end
end
