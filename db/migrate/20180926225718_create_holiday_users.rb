class CreateHolidayUsers < ActiveRecord::Migration
  def change
    create_table :holiday_users do |t|
      t.references :holiday
      t.references :user
      t.boolean :manually_assoc, default: false
      t.boolean :active, default: true
      t.boolean :deactivated_manually, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :holiday_users, :holiday_id
    add_index :holiday_users, :user_id
  end
end
