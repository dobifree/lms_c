class CreateBrgGroupUserComments < ActiveRecord::Migration
  def change
    create_table :brg_group_user_comments do |t|
      t.references :brg_group_user
      t.text :comment
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.boolean :active, default: true
      t.integer :prev_comment_id
      t.timestamps
    end
    add_index :brg_group_user_comments, :brg_group_user_id
    add_index :brg_group_user_comments, :prev_comment_id
  end
end
