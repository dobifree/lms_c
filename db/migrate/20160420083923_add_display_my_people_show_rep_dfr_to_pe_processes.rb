class AddDisplayMyPeopleShowRepDfrToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :display_my_people_show_rep_dfr, :boolean, default: false
  end
end
