class AddMasterPollIdToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :master_poll_id, :integer
  end
end
