class AddJefeConsultaResultadosToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :jefe_consulta_resultados, :boolean, default: false
  end
end
