class AddPositionToJmSubcharacteristic < ActiveRecord::Migration
  def change
    add_column :jm_subcharacteristics, :position, :integer
  end
end
