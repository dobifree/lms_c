class AddManageEvaluationsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :manage_evaluations, :boolean, default: false
  end
end
