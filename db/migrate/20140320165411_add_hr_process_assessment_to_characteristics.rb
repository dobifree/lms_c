class AddHrProcessAssessmentToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :hr_process_assessment, :boolean
  end
end
