class InsertCtModulesKpis < ActiveRecord::Migration
  def change
    execute "insert into ct_modules set cod='kpis', name='KPIs', active=true, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'kpis').count == 0
  end
end
