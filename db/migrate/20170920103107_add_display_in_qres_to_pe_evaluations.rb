class AddDisplayInQresToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :display_in_qres, :boolean, default: false
  end
end
