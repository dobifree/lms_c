class AddAllowCalibrationToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :allow_calibration, :boolean, default: false
  end
end
