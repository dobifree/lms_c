class AddRepDetailedFinalResultsEvaluationsToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_detailed_final_results_evaluations, :boolean, default: false
  end
end
