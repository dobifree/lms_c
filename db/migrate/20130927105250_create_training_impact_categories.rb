class CreateTrainingImpactCategories < ActiveRecord::Migration
  def change
    create_table :training_impact_categories do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
