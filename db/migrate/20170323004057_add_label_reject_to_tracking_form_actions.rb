class AddLabelRejectToTrackingFormActions < ActiveRecord::Migration
  def change
    add_column :tracking_form_actions, :label_reject, :string
  end
end
