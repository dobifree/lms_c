class CreateBrcMails < ActiveRecord::Migration
  def change
    create_table :brc_mails do |t|
      t.string :def_begin_subject
      t.text :def_begin_body
      t.string :def_rej_subject
      t.text :def_rej_body
      t.string :val_begin_subject
      t.text :val_begin_body
      t.string :def_remind_subject
      t.text :def_remind_body
      t.string :val_remind_subject
      t.text :val_remind_body
      t.string :member_begin_subject
      t.text :member_begin_body
      t.text :member_begin_extra1_body

      t.timestamps
    end
  end
end
