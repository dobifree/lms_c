class AddAcceptsRedefinitionsToTrackingProcess < ActiveRecord::Migration
  def change
    add_column :tracking_processes, :accepts_redefinitions, :boolean, :default => false
  end
end
