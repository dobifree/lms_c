class AddFinalEvaluationOperationToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :final_evaluation_operation, :integer, default: 0
  end
end
