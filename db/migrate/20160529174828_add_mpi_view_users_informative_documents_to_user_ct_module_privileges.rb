class AddMpiViewUsersInformativeDocumentsToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :mpi_view_users_informative_documents, :boolean, default: false
  end
end
