class AddPositionToCharacteristicValues < ActiveRecord::Migration
  def change
    add_column :characteristic_values, :position, :integer, default: 1
  end
end
