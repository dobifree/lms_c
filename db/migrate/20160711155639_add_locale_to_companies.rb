class AddLocaleToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :locale, :string, default: 'es_PE'
  end
end
