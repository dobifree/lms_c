class AddUserIdToPeQuestionFiles < ActiveRecord::Migration
  def change
    add_column :pe_question_files, :user_id, :integer
    add_index :pe_question_files, :user_id
  end
end
