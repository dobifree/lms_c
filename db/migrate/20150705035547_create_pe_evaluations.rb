class CreatePeEvaluations < ActiveRecord::Migration
  def change
    create_table :pe_evaluations do |t|
      t.integer :position
      t.string :name
      t.text :description
      t.integer :weight
      t.boolean :entered_by_manager
      t.references :pe_process
      t.references :pe_dimension

      t.timestamps
    end
    add_index :pe_evaluations, :pe_process_id
    add_index :pe_evaluations, :pe_dimension_id
  end
end
