class AddRepDetailedFinalResultsOnlyBossShowBossToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :rep_detailed_final_results_only_boss_show_boss, :boolean, default: false
  end
end
