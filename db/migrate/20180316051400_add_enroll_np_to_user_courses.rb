class AddEnrollNpToUserCourses < ActiveRecord::Migration
  def change
    add_column :user_courses, :enroll_np, :boolean, default: false
  end
end
