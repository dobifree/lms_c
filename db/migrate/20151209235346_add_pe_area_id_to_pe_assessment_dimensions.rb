class AddPeAreaIdToPeAssessmentDimensions < ActiveRecord::Migration
  def change
    add_column :pe_assessment_dimensions, :pe_area_id, :integer
    add_index :pe_assessment_dimensions, :pe_area_id
  end
end
