class ChangePesosInHrProcessDimensionEs < ActiveRecord::Migration
  def change
    change_column :hr_process_dimension_es, :eval_jefe_peso, :integer, default: 0
    change_column :hr_process_dimension_es, :eval_auto_peso, :integer, default: 0
    change_column :hr_process_dimension_es, :eval_par_peso, :integer, default: 0
    change_column :hr_process_dimension_es, :eval_sub_peso, :integer, default: 0
  end
end
