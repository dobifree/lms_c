class AddPeEvaluationIdToPeCalSessions < ActiveRecord::Migration
  def change
    add_column :pe_cal_sessions, :pe_evaluation_id, :integer
    add_index :pe_cal_sessions, :pe_evaluation_id
  end
end
