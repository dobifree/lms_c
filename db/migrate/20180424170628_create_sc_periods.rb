class CreateScPeriods < ActiveRecord::Migration
  def change
    create_table :sc_periods do |t|
      t.datetime :begin
      t.datetime :end
      t.text :description
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id

      t.timestamps
    end
    add_index :sc_periods, :deactivated_by_user_id, name: 'sc_periods_deac_user_id'
  end
end
