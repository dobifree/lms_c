class AddStepCalibrationEvaluationAliasToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_calibration_evaluation_alias, :string, default: 'Calibrar'
  end
end
