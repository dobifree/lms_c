class MoverCompaniesAAdminbd < ActiveRecord::Migration

  def connection
    @connection = Company.connection
  end

  def change
    unless table_exists? :companies
      create_table :companies do |t|
        t.string :nombre
        t.string :codigo
        t.string :url
        t.string :connection_string
        t.boolean :active, :default => false
        t.string :directorio
        t.string :email_mesa_ayuda
        t.boolean :modulo_sence, :default => false
        t.boolean :show_header_in_login, :default => true
        t.string :texto_username_login
        t.text :texto_cambio_password
        t.string :alias_username
        t.string :alias_user
        t.string :time_zone, :default => 'America/Lima'
        t.boolean :display_code_in_user_profile, :default => false
        t.string :header_text_login_box, :default => 'Ingrese sus datos de acceso'
        t.string :recover_pass_text_login_box, :default => '¿Olvidó su contraseña?'
        t.string :user_profile_tab_text_dashboard, :default => 'Perfil de Usuario'
        t.string :locale, :default => 'es_PE'
        t.string :training_type_alias, :default => 'Tipo de capacitación'
        t.boolean :training_type_user_courses_submenu, :default => false
        t.string :notifications_email, :default => 'notificaciones@exa.pe'
        t.boolean :accepts_comments, :default => true
        t.boolean :accepts_values, :default => true

        t.timestamps
      end

      #inserts


    end

    #company = Company.new()

    # esto se ejecuta en la bd de la 'company' específica
    # se borra la tabla companies que ya fue creada en la bd de administración

    old_connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
    #drop_table :admins

    old_company = Company.find_by_sql('select * from ' + old_connection.current_database + '.companies').first

    #@connection = Company.connection

    old_company.connection_string = "#{Rails.env}"

    current_domain = old_company.connection_string.split('ction_')[1].nil? ? old_company.connection_string : old_company.connection_string.split('ction_')[1]
    current_country_sufix = old_company.locale.split('_')[1].downcase


    old_company.url = 'https://' + current_domain + '.exa.' + current_country_sufix
    old_company.active = true


    insert "Insert into companies (nombre, codigo, url, connection_string, active, directorio,
            email_mesa_ayuda, modulo_sence, show_header_in_login, texto_username_login, texto_cambio_password,
            alias_username, alias_user, time_zone, display_code_in_user_profile, header_text_login_box,
            recover_pass_text_login_box, user_profile_tab_text_dashboard, locale, training_type_alias,
            training_type_user_courses_submenu, notifications_email, accepts_comments, accepts_values,
            created_at, updated_at) VALUES (
            '#{old_company.nombre}',
            '#{old_company.codigo}',
            '#{old_company.url}',
            '#{old_company.connection_string}',
            #{old_company.active},
            '#{old_company.directorio}',
            '#{old_company.email_mesa_ayuda}',
            #{old_company.modulo_sence},
            #{old_company.show_header_in_login},
            '#{old_company.texto_username_login}',
            '#{old_company.texto_cambio_password}',
            '#{old_company.alias_username}',
            '#{old_company.alias_user}',
            '#{old_company.time_zone}',
            #{old_company.display_code_in_user_profile},
            '#{old_company.header_text_login_box}',
            '#{old_company.recover_pass_text_login_box}',
            '#{old_company.user_profile_tab_text_dashboard}',
            '#{old_company.locale}',
            '#{old_company.training_type_alias}',
            #{old_company.training_type_user_courses_submenu},
            '#{old_company.notifications_email}',
            #{old_company.accepts_comments},
            #{old_company.accepts_values},
            '#{old_company.created_at.strftime('%Y-%m-%d %H:%M:%S')}',
            '#{old_company.updated_at.strftime('%Y-%m-%d %H:%M:%S')}'
            )"


    @connection = old_connection

    @connection.add_column :companies, :tabla_borrada, :boolean, :default => true

  end


end
