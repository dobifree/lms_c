class CreateScScheduleCharacteristics < ActiveRecord::Migration
  def change
    create_table :sc_schedule_characteristics do |t|
      t.boolean :active_gui, :default => false
      t.integer :pos_gui
      t.boolean :active_condition, :default => false
      t.integer :pos_condition
      t.references :characteristic

      t.timestamps
    end
    add_index :sc_schedule_characteristics, :characteristic_id
  end
end
