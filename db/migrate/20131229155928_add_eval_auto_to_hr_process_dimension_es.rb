class AddEvalAutoToHrProcessDimensionEs < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :eval_auto, :boolean, default: false
  end
end
