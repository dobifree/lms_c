class CreateUmCharacteristics < ActiveRecord::Migration
  def change
    create_table :um_characteristics do |t|
      t.references :characteristic

      t.timestamps
    end
    add_index :um_characteristics, :characteristic_id
  end
end
