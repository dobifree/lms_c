class AddPositionToScholarshipItemGroups < ActiveRecord::Migration
  def change
    add_column :scholarship_item_groups, :position, :integer
  end
end
