class AddDespuesDeUnitIdToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :despues_de_unit_id, :integer
  end
end
