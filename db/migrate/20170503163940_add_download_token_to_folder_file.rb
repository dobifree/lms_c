class AddDownloadTokenToFolderFile < ActiveRecord::Migration
  def change
    add_column :folder_files, :download_token, :string
  end
end
