class AddEnviadoAprobarAreaToDncs < ActiveRecord::Migration
  def change
    add_column :dncs, :enviado_aprovar_area, :boolean, default: false
  end
end
