class CreateMasterEvaluationTopics < ActiveRecord::Migration
  def change
    create_table :master_evaluation_topics do |t|
      t.integer :numero
      t.string :nombre
      t.references :master_evaluation

      t.timestamps
    end
    add_index :master_evaluation_topics, :master_evaluation_id
  end
end
