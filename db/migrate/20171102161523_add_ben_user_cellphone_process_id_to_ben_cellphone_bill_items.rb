class AddBenUserCellphoneProcessIdToBenCellphoneBillItems < ActiveRecord::Migration
  def change
    add_column :ben_cellphone_bill_items, :ben_user_cellphone_process_id, :integer
    add_index :ben_cellphone_bill_items, :ben_user_cellphone_process_id, :name => 'ben_user_cellphone_processes_bi_id'
  end
end
