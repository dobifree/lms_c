class AddActiveToTrackingListItem < ActiveRecord::Migration
  def change
    add_column :tracking_list_items, :active, :boolean, :default => true
  end
end
