class AddActiveReportingActaToLmsCharacteristics < ActiveRecord::Migration
  def change
    add_column :lms_characteristics, :active_reporting_acta, :boolean, :default => false
  end
end
