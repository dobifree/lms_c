class AddDefaultValuesToElecProcesses < ActiveRecord::Migration
  def change
    change_column :elec_processes, :active, :boolean, :default => false
    change_column :elec_processes, :to_everyone, :boolean, :default => false
    change_column :elec_processes, :self_vote, :boolean, :default => false
    change_column :elec_processes, :recurrent, :boolean, :default => false

    change_column :elec_processes, :max_votes, :integer, :default => 1
    change_column :elec_processes, :rounds, :integer, :default => 1
  end
end
