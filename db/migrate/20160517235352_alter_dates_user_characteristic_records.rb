class AlterDatesUserCharacteristicRecords < ActiveRecord::Migration

  def change

    change_column :user_characteristic_records, :from_date, :datetime
    change_column :user_characteristic_records, :to_date, :datetime

  end

end
