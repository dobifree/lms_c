class CreatePeQuestionActivityFieldListItems < ActiveRecord::Migration
  def change
    create_table :pe_question_activity_field_list_items do |t|
      t.references :pe_question_activity_field_list
      t.string :description

      t.timestamps
    end
    add_index :pe_question_activity_field_list_items, :pe_question_activity_field_list_id, name: :pe_question_activity_field_list_items_list_id
  end
end
