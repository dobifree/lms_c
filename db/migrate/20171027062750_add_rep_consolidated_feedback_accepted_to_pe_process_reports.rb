class AddRepConsolidatedFeedbackAcceptedToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_consolidated_feedback_accepted, :boolean, :default => false
  end
end
