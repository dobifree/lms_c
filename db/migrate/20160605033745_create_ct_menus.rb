class CreateCtMenus < ActiveRecord::Migration
  def change
    create_table :ct_menus do |t|
      t.integer :position
      t.string :cod
      t.string :name
      t.string :user_menu_alias

      t.timestamps
    end
  end
end
