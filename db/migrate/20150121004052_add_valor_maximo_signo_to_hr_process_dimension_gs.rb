class AddValorMaximoSignoToHrProcessDimensionGs < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_gs, :valor_maximo_signo, :string, default: '<='
  end
end
