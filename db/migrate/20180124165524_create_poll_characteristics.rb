class CreatePollCharacteristics < ActiveRecord::Migration
  def change
    create_table :poll_characteristics do |t|
      t.boolean :active_reporting, :default => false
      t.integer :pos_reporting
      t.references :characteristic

      t.timestamps
    end
    add_index :poll_characteristics, :characteristic_id
  end
end
