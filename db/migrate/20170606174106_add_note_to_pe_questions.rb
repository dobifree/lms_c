class AddNoteToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :note, :text
  end
end
