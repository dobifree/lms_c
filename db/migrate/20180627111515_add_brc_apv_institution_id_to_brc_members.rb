class AddBrcApvInstitutionIdToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :brc_apv_institution_id, :integer
    add_index :brc_members, :brc_apv_institution_id
  end
end
