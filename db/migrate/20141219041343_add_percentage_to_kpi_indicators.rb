class AddPercentageToKpiIndicators < ActiveRecord::Migration
  def change
    add_column :kpi_indicators, :percentage, :float
  end
end
