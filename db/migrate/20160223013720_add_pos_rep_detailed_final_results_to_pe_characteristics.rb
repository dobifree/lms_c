class AddPosRepDetailedFinalResultsToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_rep_detailed_final_results, :integer
    add_index :pe_characteristics, :pos_rep_detailed_final_results
  end
end
