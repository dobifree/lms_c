class AddPlaceToProgramCourseInstances < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :place, :string
  end
end
