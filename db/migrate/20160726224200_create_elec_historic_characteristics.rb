class CreateElecHistoricCharacteristics < ActiveRecord::Migration
  def change
    create_table :elec_historic_characteristics do |t|
      t.references :elec_event
      t.references :characteristic
      t.references :user
      t.string :value

      t.timestamps
    end
    add_index :elec_historic_characteristics, :elec_event_id
    add_index :elec_historic_characteristics, :characteristic_id
    add_index :elec_historic_characteristics, :user_id
  end
end
