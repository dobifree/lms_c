class AddFinLetraToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :fin_letra, :string, default: 'Helvetica'
  end
end
