class AddStepSelectionAliasToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_selection_alias, :string
  end
end
