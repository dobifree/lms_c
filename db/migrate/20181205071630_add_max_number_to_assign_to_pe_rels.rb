class AddMaxNumberToAssignToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :max_number_to_assign, :integer, default: 1
  end
end
