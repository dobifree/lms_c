class AddRepValidationStatusToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_validation_status, :boolean, default: false
  end
end
