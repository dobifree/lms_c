class AddAllowTrackingToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :allow_tracking, :boolean, default: false
  end
end
