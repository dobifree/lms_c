class AddUltimoAccesoToUserCourseUnits < ActiveRecord::Migration
  def change
    add_column :user_course_units, :ultimo_acceso, :datetime
  end
end
