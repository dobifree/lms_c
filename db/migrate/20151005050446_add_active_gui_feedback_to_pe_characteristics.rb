class AddActiveGuiFeedbackToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_gui_feedback, :boolean, default: false
  end
end
