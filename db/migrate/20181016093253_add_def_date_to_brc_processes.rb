class AddDefDateToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :def_date, :datetime
  end
end
