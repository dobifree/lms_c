class CreatePeAssessmentQuestions < ActiveRecord::Migration
  def change
    create_table :pe_assessment_questions do |t|
      t.references :pe_assessment_evaluation
      t.integer :pe_assessment_question_id
      t.float :points
      t.float :percentage
      t.text :comment
      t.references :pe_alternative
      t.float :indicator_achievement
      t.datetime :last_update

      t.timestamps
    end
    add_index :pe_assessment_questions, :pe_assessment_evaluation_id
    add_index :pe_assessment_questions, :pe_alternative_id
    add_index :pe_assessment_questions, :pe_assessment_question_id
  end
end
