class AddManageFeedbackToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :manage_feedback, :boolean, default: false
  end
end
