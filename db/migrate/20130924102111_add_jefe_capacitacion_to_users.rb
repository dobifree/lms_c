class AddJefeCapacitacionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :jefe_capacitacion, :boolean, default: false
  end
end
