class CreateBrgGroupUsers < ActiveRecord::Migration
  def change
    create_table :brg_group_users do |t|
      t.references :user
      t.references :brg_group
      t.boolean :active
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :brg_group_users, :user_id
    add_index :brg_group_users, :brg_group_id
    add_index :brg_group_users, :deactivated_by_user_id
    add_index :brg_group_users, :registered_by_user_id
  end
end
