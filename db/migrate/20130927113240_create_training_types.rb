class CreateTrainingTypes < ActiveRecord::Migration
  def change
    create_table :training_types do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
