class AddLicEventIdToLicRequest < ActiveRecord::Migration
  def change
    add_column :lic_requests, :lic_event_id, :integer
    add_index :lic_requests, :lic_event_id
  end
end
