class CreateCompanyUnits < ActiveRecord::Migration
  def change
    create_table :company_units do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
