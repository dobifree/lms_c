class AddLeverageToKpiIndicators < ActiveRecord::Migration
  def change
    add_column :kpi_indicators, :leverage, :float, default: 0
  end
end
