class AddAllowSubsetsToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :allow_subsets, :boolean, default: false
  end
end
