class AddStepAssessmentShowFinalResultsToBossToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_show_final_results_to_boss, :boolean, default: false
  end
end
