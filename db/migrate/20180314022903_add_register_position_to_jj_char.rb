class AddRegisterPositionToJjChar < ActiveRecord::Migration
  def change
    add_column :jj_characteristics, :register_position, :integer
  end
end
