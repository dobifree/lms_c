class AddEnvioEmailToSupportTickets < ActiveRecord::Migration
  def change
    add_column :support_tickets, :envio_email, :boolean, default: false
  end
end
