class AddColumnToScHeBlockParams < ActiveRecord::Migration
  def change
    add_column :sc_he_block_params, :entry_active, :boolean, :default => true
    add_column :sc_he_block_params, :exit_active, :boolean, :default => true
  end
end
