class ChangeWeightPeQuestions < ActiveRecord::Migration
  def change
    change_column :pe_questions, :weight, :float, default: 1
  end
end
