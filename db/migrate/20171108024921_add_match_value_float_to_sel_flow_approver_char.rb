class AddMatchValueFloatToSelFlowApproverChar < ActiveRecord::Migration
  def change
    add_column :sel_flow_approver_chars, :match_value_float, :float
  end
end
