class AddCharacteristicTypeIdToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :characteristic_type_id, :integer
    add_index :characteristics, :characteristic_type_id
  end
end
