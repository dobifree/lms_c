class AddNotaColorToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :nota_color, :string, default: '000000'
  end
end
