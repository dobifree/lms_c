class AddFilterAssessmentReportToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :filter_assessment_report, :boolean, default: false
  end
end
