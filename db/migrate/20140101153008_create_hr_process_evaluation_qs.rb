class CreateHrProcessEvaluationQs < ActiveRecord::Migration
  def change
    create_table :hr_process_evaluation_qs do |t|
      t.text :texto
      t.integer :peso
      t.references :hr_process_evaluation
      t.references :hr_process_level
      t.references :hr_evaluation_type_element
      t.integer :hr_process_evaluation_q_id

      t.timestamps
    end
    add_index :hr_process_evaluation_qs, :hr_process_evaluation_id
    add_index :hr_process_evaluation_qs, :hr_process_level_id
    add_index :hr_process_evaluation_qs, :hr_evaluation_type_element_id
    add_index :hr_process_evaluation_qs, :hr_process_evaluation_q_id
  end
end
