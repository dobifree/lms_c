class AddDefByUsersValidatedBeforeAcceptedManagerToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :def_by_users_validated_before_accepted_manager, :boolean, default: true
  end
end
