class AddAccumToBpGeneralRules < ActiveRecord::Migration
  def change
   add_column :bp_general_rules_vacations, :accumulated_type, :integer, default: 2
  end
end