class CreateSelApplicantValues < ActiveRecord::Migration
  def change
    create_table :sel_applicant_values do |t|
      t.references :sel_applicant
      t.references :sel_apply_form
      t.references :jm_option
      t.text :value

      t.timestamps
    end
    add_index :sel_applicant_values, :sel_applicant_id
    add_index :sel_applicant_values, :sel_apply_form_id
    add_index :sel_applicant_values, :jm_option_id
  end
end
