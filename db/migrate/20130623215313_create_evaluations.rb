class CreateEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.string :nombre
      t.string :descripcion
      t.references :program
      t.references :level
      t.references :course

      t.timestamps
    end
    add_index :evaluations, :program_id
    add_index :evaluations, :level_id
    add_index :evaluations, :course_id
  end
end
