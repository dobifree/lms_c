class AddFlexibleScToUser < ActiveRecord::Migration
  def change
    add_column :users, :flexible_sc, :boolean, default: false
  end
end
