class CreateBrcProcessCategVals < ActiveRecord::Migration
  def change
    create_table :brc_process_categ_vals do |t|
      t.references :brc_process
      t.references :brc_category
      t.references :characteristic_value
      t.text :comment_val

      t.timestamps
    end
    add_index :brc_process_categ_vals, :brc_process_id
    add_index :brc_process_categ_vals, :brc_category_id
    add_index :brc_process_categ_vals, :characteristic_value_id
  end
end
