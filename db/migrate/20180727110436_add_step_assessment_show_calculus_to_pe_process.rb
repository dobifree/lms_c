class AddStepAssessmentShowCalculusToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_show_calculus, :boolean, default: true
  end
end
