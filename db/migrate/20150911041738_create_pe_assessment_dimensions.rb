class CreatePeAssessmentDimensions < ActiveRecord::Migration
  def change
    create_table :pe_assessment_dimensions do |t|
      t.references :pe_process
      t.references :pe_member
      t.references :pe_dimension
      t.references :pe_dimension_group
      t.float :percentage
      t.datetime :last_update

      t.timestamps
    end
    add_index :pe_assessment_dimensions, :pe_process_id
    add_index :pe_assessment_dimensions, :pe_member_id
    add_index :pe_assessment_dimensions, :pe_dimension_id
    add_index :pe_assessment_dimensions, :pe_dimension_group_id
  end
end
