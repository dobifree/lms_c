class CreateHrProcessCalibrationSessionCs < ActiveRecord::Migration
  def change
    create_table :hr_process_calibration_session_cs do |t|
      t.references :hr_process_calibration_session
      t.references :user
      t.boolean :manager

      t.timestamps
    end
    add_index :hr_process_calibration_session_cs, :hr_process_calibration_session_id, name: 'hr_process_calibration_session_cs_cs_id'
    add_index :hr_process_calibration_session_cs, :user_id, name: 'hr_process_calibration_session_cs_u_id'
  end
end
