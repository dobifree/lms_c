class AddDisplayWeightInQdefsToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :display_weight_in_qdefs, :boolean, default: true
  end
end
