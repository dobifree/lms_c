class AddRepTrackingStatusToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_tracking_status, :boolean, default: false
  end
end
