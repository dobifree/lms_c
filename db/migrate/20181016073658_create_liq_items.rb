class CreateLiqItems < ActiveRecord::Migration
  def change
    create_table :liq_items do |t|
      t.references :liq_process
      t.references :user
      t.references :secu_cohade
      t.string :description
      t.integer :monto

      t.timestamps
    end
    add_index :liq_items, :liq_process_id
    add_index :liq_items, :user_id
    add_index :liq_items, :secu_cohade_id
  end
end
