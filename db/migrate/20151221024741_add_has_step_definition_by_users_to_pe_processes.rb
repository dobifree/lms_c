class AddHasStepDefinitionByUsersToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_definition_by_users, :boolean, default: false
  end
end
