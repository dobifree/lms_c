class CreateSelClaimants < ActiveRecord::Migration
  def change
    create_table :sel_claimants do |t|
      t.references :sel_process
      t.references :user

      t.timestamps
    end
    add_index :sel_claimants, :sel_process_id
    add_index :sel_claimants, :user_id
  end
end
