class AddPeriodNumberToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :period_number, :integer
  end
end
