class CreateJmMessages < ActiveRecord::Migration
  def change
    create_table :jm_messages do |t|
      t.references :jm_candidate
      t.integer :sender_user_id
      t.references :sel_process
      t.string :subject
      t.text :body
      t.boolean :read
      t.datetime :read_at

      t.timestamps
    end
    add_index :jm_messages, :jm_candidate_id
    add_index :jm_messages, :sender_user_id
    add_index :jm_messages, :sel_process_id
  end
end
