class AddTipoSatisfaccionToPoll < ActiveRecord::Migration
  def change
    add_column :polls, :tipo_satisfaccion, :boolean, default: false
  end
end
