class CreateScholarshipListItems < ActiveRecord::Migration
  def change
    create_table :scholarship_list_items do |t|
      t.references :scholarship_list
      t.string :name
      t.text :description
      t.boolean :active

      t.timestamps
    end
    add_index :scholarship_list_items, :scholarship_list_id
  end
end
