class AddTrackerFieldsToSelStepCandidate < ActiveRecord::Migration
  def change
    add_column :sel_step_candidates, :registered_at, :datetime
    add_column :sel_step_candidates, :registered_by_user_id, :integer
    add_column :sel_step_candidates, :exonerated_at, :datetime
    add_column :sel_step_candidates, :exonerated_by_user_id, :integer
    add_column :sel_step_candidates, :done_at, :datetime
    add_column :sel_step_candidates, :done_by_user_id, :integer

    add_index :sel_step_candidates, :registered_by_user_id
    add_index :sel_step_candidates, :exonerated_by_user_id
    add_index :sel_step_candidates, :done_by_user_id
  end
end
