class AddFieldsResetPasswordToJmCandidates < ActiveRecord::Migration
  def change
    add_column :jm_candidates, :codigo_rec_pass, :string
    add_column :jm_candidates, :fecha_rec_pass, :datetime
  end
end
