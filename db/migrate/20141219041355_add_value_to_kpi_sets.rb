class AddValueToKpiSets < ActiveRecord::Migration
  def change
    add_column :kpi_sets, :value, :float
  end
end
