class AddIvToCompanies < ActiveRecord::Migration
  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :iv
      add_column :companies, :iv, :blob
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end


end
