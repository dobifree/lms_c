class AddBonusApvToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :bonus_apv, :decimal, :precision => 20, :scale => 4, default: 0
  end
end
