class ChangeLocationTypeFromBanners < ActiveRecord::Migration
  def change
    change_column :banners, :location, :integer
  end
end
