class AddIsEvaluatedToPeAreas < ActiveRecord::Migration
  def change
    add_column :pe_areas, :is_evaluated, :boolean, default: false
  end
end
