class AddQresAllowIndividualCommentsWithBossToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :qres_allow_individual_comments_with_boss, :boolean, default: false
  end
end
