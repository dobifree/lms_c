class ChangeFixQuestionRamkModel < ActiveRecord::Migration
  def change
    change_column :pe_elements, :fix_rank_models, :integer, default: 0
    execute "update pe_elements set fix_rank_models = 0 where fix_rank_models IS NULL"
  end
end
