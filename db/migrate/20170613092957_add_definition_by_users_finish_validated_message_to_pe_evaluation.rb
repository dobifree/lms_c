class AddDefinitionByUsersFinishValidatedMessageToPeEvaluation < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :definition_by_user_finish_validated_message, :string, default: 'Confirmo que he corregido y finalizado la definición'
  end
end
