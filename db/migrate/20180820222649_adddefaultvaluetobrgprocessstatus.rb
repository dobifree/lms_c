class Adddefaultvaluetobrgprocessstatus < ActiveRecord::Migration
  def change
    change_column :brg_processes, :status, :integer, default: 0
  end
end
