class CreatePeDimensionGroups < ActiveRecord::Migration
  def change
    create_table :pe_dimension_groups do |t|
      t.integer :position
      t.string :name
      t.text :description
      t.string :min_sign
      t.integer :min_percentage
      t.string :max_sign
      t.integer :max_percentage
      t.references :pe_dimension

      t.timestamps
    end
    add_index :pe_dimension_groups, :pe_dimension_id
  end
end
