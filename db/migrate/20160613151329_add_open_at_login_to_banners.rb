class AddOpenAtLoginToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :open_at_login, :boolean, default: false
  end
end
