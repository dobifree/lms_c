class CreateSelApplicantSpecificAnswers < ActiveRecord::Migration
  def change
    create_table :sel_applicant_specific_answers do |t|
      t.references :sel_applicant
      t.references :sel_apply_specific_question
      t.text :answer
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :sel_applicant_specific_answers, :sel_applicant_id
    add_index :sel_applicant_specific_answers, :sel_apply_specific_question_id, :name => 'index_sel_applicant_specific_answers_on_sel_question_id'
    add_index :sel_applicant_specific_answers, :registered_by_user_id
  end
end
