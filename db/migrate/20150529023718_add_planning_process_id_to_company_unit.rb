class AddPlanningProcessIdToCompanyUnit < ActiveRecord::Migration
  def change
    add_column :company_units, :planning_process_id, :integer
  end
end
