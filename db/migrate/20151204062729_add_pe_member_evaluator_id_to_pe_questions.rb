class AddPeMemberEvaluatorIdToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :pe_member_evaluator_id, :integer
    add_index :pe_questions, :pe_member_evaluator_id
  end
end
