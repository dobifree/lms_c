class AddJCostCenterIdToJJobs < ActiveRecord::Migration
  def change
    add_column :j_jobs, :j_cost_center_id, :integer
    add_index :j_jobs, :j_cost_center_id
  end
end
