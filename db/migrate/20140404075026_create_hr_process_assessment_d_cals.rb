class CreateHrProcessAssessmentDCals < ActiveRecord::Migration
  def change
    create_table :hr_process_assessment_d_cals do |t|
      t.references :hr_process_assessment_d
      t.references :hr_process_calibration_session
      t.references :hr_process_calibration_session_m
      t.float :resultado_porcentaje
      t.datetime :fecha

      t.timestamps
    end
    add_index :hr_process_assessment_d_cals, :hr_process_assessment_d_id, name: 'hr_process_assessment_d_cals_pad_id'
    add_index :hr_process_assessment_d_cals, :hr_process_calibration_session_id, name: 'hr_process_assessment_d_cals_pcs_id'
    add_index :hr_process_assessment_d_cals, :hr_process_calibration_session_m_id, name: 'hr_process_assessment_d_cals_pcsm_id'
  end
end
