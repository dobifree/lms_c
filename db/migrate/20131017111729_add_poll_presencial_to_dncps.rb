class AddPollPresencialToDncps < ActiveRecord::Migration
  def change
    add_column :dncps, :poll_presencial, :boolean, default: true
  end
end
