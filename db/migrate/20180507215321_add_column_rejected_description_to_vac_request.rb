class AddColumnRejectedDescriptionToVacRequest < ActiveRecord::Migration
  def change
    add_column :vac_requests, :rejected_description, :text
  end
end
