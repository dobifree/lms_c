class AddVisualizacionAlternativasToHrProcessEvaluations < ActiveRecord::Migration
  def change
    add_column :hr_process_evaluations, :visualizacion_alternativas, :integer, default: 1
  end
end
