class AddActivaToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :activa, :boolean, default: true
  end
end
