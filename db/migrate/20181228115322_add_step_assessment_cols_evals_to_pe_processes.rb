class AddStepAssessmentColsEvalsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_cols_evals, :boolean, default: true
  end
end
