class AddValueFloatToUserCharacteristic < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :value_float, :float
  end
end
