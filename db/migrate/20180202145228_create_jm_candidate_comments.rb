class CreateJmCandidateComments < ActiveRecord::Migration
  def change
    create_table :jm_candidate_comments do |t|
      t.references :jm_candidate
      t.text :comment
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :jm_candidate_comments, :jm_candidate_id
    add_index :jm_candidate_comments, :registered_by_user_id
  end
end
