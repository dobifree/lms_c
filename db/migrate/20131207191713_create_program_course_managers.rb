class CreateProgramCourseManagers < ActiveRecord::Migration
  def change
    create_table :program_course_managers do |t|
      t.references :program_course
      t.references :user

      t.timestamps
    end
    add_index :program_course_managers, :program_course_id
    add_index :program_course_managers, :user_id
  end
end
