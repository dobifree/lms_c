class CreateBlogFormPermissions < ActiveRecord::Migration
  def change
    create_table :blog_form_permissions do |t|
      t.references :blog_form
      t.integer :perm_type
      t.boolean :employee
      t.boolean :boss
      t.boolean :superior
      t.boolean :others
      t.boolean :active

      t.timestamps
    end
    add_index :blog_form_permissions, :blog_form_id
  end
end
