class CreatePeProcesses < ActiveRecord::Migration
  def change
    create_table :pe_processes do |t|
      t.string :name
      t.string :period

      t.timestamps
    end
  end
end
