class AddDisplayResultsInUserProfileToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :display_results_in_user_profile, :boolean, default: false
  end
end
