class AddHrProcessUserEvalIdToHrProcessUserComments < ActiveRecord::Migration
  def change
    add_column :hr_process_user_comments, :hr_process_user_eval_id, :integer
    add_index :hr_process_user_comments, :hr_process_user_eval_id
  end
end
