class CreateScReportedRecords < ActiveRecord::Migration
  def change
    create_table :sc_reported_records do |t|
      t.references :sc_reporting
      t.references :sc_record

      t.timestamps
    end
    add_index :sc_reported_records, :sc_reporting_id
    add_index :sc_reported_records, :sc_record_id
  end
end
