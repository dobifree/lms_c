class CorrecJjCharacteristicValues < ActiveRecord::Migration
  def change
    remove_column :jj_characteristic_values, :Jj_characteristic_id

    add_column :jj_characteristic_values, :jj_characteristic_id, :integer
    add_index :jj_characteristic_values, :jj_characteristic_id

  end
end
