class AddSecuCohadeIdToBrcBonusItems < ActiveRecord::Migration
  def change
    add_column :brc_bonus_items, :secu_cohade_id, :integer
    add_index :brc_bonus_items, :secu_cohade_id
  end
end
