class AddQ1ToJPayBands < ActiveRecord::Migration
  def change
    add_column :j_pay_bands, :q1, :decimal, :precision => 15, :scale => 4
  end
end
