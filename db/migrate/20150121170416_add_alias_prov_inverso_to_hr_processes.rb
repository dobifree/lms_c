class AddAliasProvInversoToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_prov_inverso, :string, default: 'Cliente'
  end
end
