class AddRedefinitionGroupToTrackingFormLogs < ActiveRecord::Migration
  def change
    add_column :tracking_form_logs, :redefinition_group, :integer
  end
end
