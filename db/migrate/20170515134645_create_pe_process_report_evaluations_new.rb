class CreatePeProcessReportEvaluationsNew < ActiveRecord::Migration
  def change
    PeProcess.all.each do |pe_process|
      pe_process.pe_evaluations.each do |pe_evaluation|
        pe_process_reports_evaluation = pe_evaluation.build_pe_process_reports_evaluation
        pe_process_reports_evaluation.pe_process = pe_process
        pe_process_reports_evaluation.save
      end
    end
  end
end
