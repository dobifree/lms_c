class CreateTrackingFormLogs < ActiveRecord::Migration
  def change
    create_table :tracking_form_logs do |t|
      t.references :tracking_form_instance
      t.references :tracking_form_action
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.text :comment

      t.timestamps
    end
    add_index :tracking_form_logs, :tracking_form_instance_id
    add_index :tracking_form_logs, :tracking_form_action_id
    add_index :tracking_form_logs, :registered_by_user_id
  end
end
