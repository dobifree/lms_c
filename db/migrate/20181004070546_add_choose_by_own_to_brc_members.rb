class AddChooseByOwnToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :choose_by_own, :boolean, default: false
  end
end
