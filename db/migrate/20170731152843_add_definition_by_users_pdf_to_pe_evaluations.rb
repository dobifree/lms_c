class AddDefinitionByUsersPdfToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :definition_by_users_pdf, :boolean, default: false
  end
end
