class AddTrackingFieldsToElecRound < ActiveRecord::Migration
  def change
    add_column :elec_rounds, :done_by_user_id, :integer
    add_column :elec_rounds, :done_at, :datetime

    add_index :elec_rounds, :done_by_user_id
  end
end
