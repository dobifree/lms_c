class CreatePlanningProcessCompanyUnitAreas < ActiveRecord::Migration
  def change
    create_table :planning_process_company_unit_areas do |t|
      t.references :planning_process
      t.references :planning_process_company_unit
      t.references :company_unit_area
      t.string :objetivos

      t.timestamps
    end
    add_index :planning_process_company_unit_areas, :planning_process_id, name: 'ppcua_ppi'
    add_index :planning_process_company_unit_areas, :company_unit_area_id, name: 'ppcua_cuai'
  end
end
