class AddActiveToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :active, :boolean, default: true
    add_index :enrollments, :active
  end
end
