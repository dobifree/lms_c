class AddColumnsBackToWorkToVacRequest < ActiveRecord::Migration
  def change
    add_column :vac_requests, :end_vacations, :date
    add_column :vac_rule_records, :paid_description, :text
  end
end
