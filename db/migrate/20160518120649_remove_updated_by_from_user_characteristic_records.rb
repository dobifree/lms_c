class RemoveUpdatedByFromUserCharacteristicRecords < ActiveRecord::Migration
  def change
    remove_columns :user_characteristic_records, :updated_by_admin_id
    remove_columns :user_characteristic_records, :updated_by_user_id
  end
end
