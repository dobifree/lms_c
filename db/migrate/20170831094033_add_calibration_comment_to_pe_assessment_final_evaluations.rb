class AddCalibrationCommentToPeAssessmentFinalEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_assessment_final_evaluations, :calibration_comment, :text
  end
end
