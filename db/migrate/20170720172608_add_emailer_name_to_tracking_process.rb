class AddEmailerNameToTrackingProcess < ActiveRecord::Migration
  def change
    add_column :tracking_processes, :mailer_name, :string
  end
end
