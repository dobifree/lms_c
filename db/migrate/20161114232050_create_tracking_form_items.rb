class CreateTrackingFormItems < ActiveRecord::Migration
  def change
    create_table :tracking_form_items do |t|
      t.references :tracking_form_question
      t.integer :position
      t.string :name
      t.text :description
      t.integer :item_type
      t.references :tracking_list

      t.timestamps
    end
    add_index :tracking_form_items, :tracking_form_question_id
    add_index :tracking_form_items, :tracking_list_id
  end
end
