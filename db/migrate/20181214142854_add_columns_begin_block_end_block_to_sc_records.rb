class AddColumnsBeginBlockEndBlockToScRecords < ActiveRecord::Migration
  def change
    add_column :sc_records, :begin_sc, :datetime
    add_column :sc_records, :end_sc, :datetime
  end
end
