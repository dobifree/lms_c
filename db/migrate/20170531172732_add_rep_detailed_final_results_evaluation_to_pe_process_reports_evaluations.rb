class AddRepDetailedFinalResultsEvaluationToPeProcessReportsEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_process_reports_evaluations, :rep_detailed_final_results_evaluation, :boolean, default: false
  end
end
