class CreatePeMemberFeedbacks < ActiveRecord::Migration
  def change
    create_table :pe_member_feedbacks do |t|
      t.references :pe_member
      t.references :pe_process
      t.references :pe_feedback_field
      t.text :comment

      t.timestamps
    end
    add_index :pe_member_feedbacks, :pe_member_id
    add_index :pe_member_feedbacks, :pe_process_id
    add_index :pe_member_feedbacks, :pe_feedback_field_id
  end
end
