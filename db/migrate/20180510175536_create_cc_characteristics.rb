class CreateCcCharacteristics < ActiveRecord::Migration
  def change
    create_table :cc_characteristics do |t|
      t.references :characteristic
      t.integer :position

      t.timestamps
    end
    add_index :cc_characteristics, :characteristic_id
  end
end
