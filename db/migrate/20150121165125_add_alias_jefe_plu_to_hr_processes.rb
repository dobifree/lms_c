class AddAliasJefePluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_jefe_plu, :string, default: 'Supervisores'
  end
end
