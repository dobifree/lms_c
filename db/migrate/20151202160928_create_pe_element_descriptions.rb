class CreatePeElementDescriptions < ActiveRecord::Migration
  def change
    create_table :pe_element_descriptions do |t|
      t.integer :position
      t.string :name
      t.references :pe_element

      t.timestamps
    end
    add_index :pe_element_descriptions, :pe_element_id
  end
end
