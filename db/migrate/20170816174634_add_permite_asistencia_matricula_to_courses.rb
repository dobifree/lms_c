class AddPermiteAsistenciaMatriculaToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :asistencia_matricula, :boolean, default: false
  end
end
