class CreateLevels < ActiveRecord::Migration
  def change
    create_table :levels do |t|
      t.string :nombre
      t.integer :orden
      t.references :program

      t.timestamps
    end
    add_index :levels, :program_id
  end
end
