class DeleteColumnBenCellphoneCharacteristics < ActiveRecord::Migration
  def change
    remove_column :ben_cellphone_characteristics, :active_reporting
    remove_column :ben_cellphone_characteristics, :pos_reporting
  end
end
