class RenamePlanningProcessIdPlanningProcessGoal < ActiveRecord::Migration
  def change
    rename_column :planning_process_goals, :planning_process_id, :planning_process_company_unit_id
  end
end
