class AddCourseInstanceToTrainingCharacteristics < ActiveRecord::Migration
  def change
    add_column :training_characteristics, :course_instance, :boolean, default: true
  end
end
