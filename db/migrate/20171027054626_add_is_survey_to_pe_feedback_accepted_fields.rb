class AddIsSurveyToPeFeedbackAcceptedFields < ActiveRecord::Migration
  def change
    add_column :pe_feedback_accepted_fields, :is_survey, :boolean, default: false
  end
end
