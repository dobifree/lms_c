class CreatePeFeedbackFieldLists < ActiveRecord::Migration
  def change
    create_table :pe_feedback_field_lists do |t|
      t.references :pe_process
      t.string :name

      t.timestamps
    end
    add_index :pe_feedback_field_lists, :pe_process_id
  end
end
