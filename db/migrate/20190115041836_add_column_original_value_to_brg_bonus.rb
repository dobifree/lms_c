class AddColumnOriginalValueToBrgBonus < ActiveRecord::Migration
  def change
    add_column :brg_bonus, :first_value, :decimal,precision: 20, scale: 5, default: 0
  end
end
