class AddColumnMatchBooleanBenCustomReportAttributeFilter < ActiveRecord::Migration
  def change
    add_column :ben_custom_report_attribute_filters, :match_value_boolean, :boolean
  end
end
