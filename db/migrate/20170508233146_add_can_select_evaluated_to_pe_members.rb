class AddCanSelectEvaluatedToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :can_select_evaluated, :boolean, default: false
  end
end
