class AddYearToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :year, :int
  end
end
