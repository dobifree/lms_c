class CreateHrProcessLevels < ActiveRecord::Migration
  def change
    create_table :hr_process_levels do |t|
      t.string :nombre
      t.references :hr_process

      t.timestamps
    end
    add_index :hr_process_levels, :hr_process_id
  end
end
