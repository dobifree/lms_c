class CreateBenCustomReportAttributes < ActiveRecord::Migration
  def change
    create_table :ben_custom_report_attributes do |t|
      t.integer :position
      t.string :name
      t.boolean :active, default: false
      t.references :ben_custom_report
      t.timestamps
    end
  end
end
