class AddStepFeedbackAcceptedMailToBossToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_accepted_mail_to_boss, :boolean
  end
end
