class CreateScRecords < ActiveRecord::Migration
  def change
    create_table :sc_records do |t|
      t.datetime :begin
      t.datetime :end
      t.references :user
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.references :sc_process
      t.references :sc_user_process
      t.decimal :he, :precision => 12, :scale => 4
      t.boolean :dont_charge_to_payroll, :default => false
      t.datetime :dont_charge_to_payroll_at
      t.integer :dont_charge_to_payroll_by_user_id
      t.text :dont_charge_to_payroll_description
      t.integer :status

      t.timestamps
    end
    add_index :sc_records, :user_id
    add_index :sc_records, :sc_process_id
    add_index :sc_records, :sc_user_process_id
    add_index :sc_records, :registered_by_user_id
    add_index :sc_records, :dont_charge_to_payroll_by_user_id
  end
end
