class CreatePeEvaluationDisplayedDefByUsers < ActiveRecord::Migration
  def change
    create_table :pe_evaluation_displayed_def_by_users do |t|
      t.references :pe_evaluation
      t.integer :pe_displayed_evaluation_id
      t.integer :position

      t.timestamps
    end
    add_index :pe_evaluation_displayed_def_by_users, :pe_evaluation_id, name: 'pe_eval_disp_def_by_users_pe_eval_id'
    add_index :pe_evaluation_displayed_def_by_users, :pe_displayed_evaluation_id, name: 'pe_eval_disp_def_by_users_pe_disp_eval_id'
    add_index :pe_evaluation_displayed_def_by_users, :pe_displayed_evaluation_id, name: 'pe_eval_disp_def_by_users_pos'
  end
end
