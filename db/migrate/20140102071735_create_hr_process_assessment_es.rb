class CreateHrProcessAssessmentEs < ActiveRecord::Migration
  def change
    create_table :hr_process_assessment_es do |t|
      t.references :hr_process_evaluation
      t.references :hr_process_user
      t.float :resultado_puntos
      t.float :resultado_porcentaje
      t.datetime :fecha
      t.integer :hr_process_user_eval_id
      t.string :tipo_rel

      t.timestamps
    end
    add_index :hr_process_assessment_es, :hr_process_evaluation_id
    add_index :hr_process_assessment_es, :hr_process_user_id
  end
end
