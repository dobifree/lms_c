class InsertCtModulesPe < ActiveRecord::Migration

  def change
    execute "insert into ct_modules set cod='pe', name='Evaluación de Desempeño', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'pe').count == 0
  end

end
