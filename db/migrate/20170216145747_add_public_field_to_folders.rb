class AddPublicFieldToFolders < ActiveRecord::Migration
  def change
    add_column :folders, :public, :boolean, :default => false
  end
end
