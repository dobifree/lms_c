class AddColumnDescriptionToBpGeneralRules < ActiveRecord::Migration
  def change
    add_column :bp_general_rules_vacations, :description, :text
  end
end
