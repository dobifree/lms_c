class CreateSelApplyForms < ActiveRecord::Migration
  def change
    create_table :sel_apply_forms do |t|
      t.references :sel_template
      t.references :jm_characteristic
      t.boolean :mandatory
      t.integer :position

      t.timestamps
    end
    add_index :sel_apply_forms, :sel_template_id
    add_index :sel_apply_forms, :jm_characteristic_id
  end
end
