class AddStatusChangedByToVacRequest < ActiveRecord::Migration
  def change
    add_column :vac_requests, :status_changed_by_user_id, :integer
    add_index :vac_requests, :status_changed_by_user_id, name: 'vac_request_status_chan_user_id'
  end
end
