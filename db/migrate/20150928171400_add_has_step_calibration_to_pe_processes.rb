class AddHasStepCalibrationToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_calibration, :boolean, default: false
  end
end
