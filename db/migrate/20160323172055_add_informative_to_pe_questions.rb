class AddInformativeToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :informative, :boolean, default: false
  end
end
