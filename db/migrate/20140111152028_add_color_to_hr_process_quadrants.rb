class AddColorToHrProcessQuadrants < ActiveRecord::Migration
  def change
    add_column :hr_process_quadrants, :color, :string
  end
end
