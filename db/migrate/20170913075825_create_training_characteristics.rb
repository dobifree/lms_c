class CreateTrainingCharacteristics < ActiveRecord::Migration
  def change
    create_table :training_characteristics do |t|
      t.string :name
      t.string :type
      t.integer :data_type_id

      t.timestamps
    end
  end
end
