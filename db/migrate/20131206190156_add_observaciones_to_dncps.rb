class AddObservacionesToDncps < ActiveRecord::Migration
  def change
    add_column :dncps, :observaciones, :text
  end
end
