class AddStatusBonCalToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :status_bon_cal, :boolean, default: false
  end
end
