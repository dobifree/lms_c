class CreateSelApplicantAnswers < ActiveRecord::Migration
  def change
    create_table :sel_applicant_answers do |t|
      t.references :sel_applicant
      t.references :jm_characteristic
      t.integer :group

      t.timestamps
    end
    add_index :sel_applicant_answers, :sel_applicant_id
    add_index :sel_applicant_answers, :jm_characteristic_id
  end
end
