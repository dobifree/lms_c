class AddColumnsAvailableToBpRuleVacations < ActiveRecord::Migration
  def change
    add_column :bp_rule_vacations, :available, :boolean, default: true
    add_column :bp_rule_vacations, :unavailable_at, :datetime
    add_column :bp_rule_vacations, :unavailable_by_admin_id, :integer

    add_index :bp_rule_vacations, :unavailable_by_admin_id
  end
end
