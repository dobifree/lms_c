class CreateElecRoundWinners < ActiveRecord::Migration
  def change
    create_table :elec_round_winners do |t|
      t.references :elec_round
      t.references :elec_process_category
      t.references :user
      t.integer :registered_by_user_id
      t.datetime :registered_at

      t.timestamps
    end
    add_index :elec_round_winners, :elec_round_id
    add_index :elec_round_winners, :elec_process_category_id
    add_index :elec_round_winners, :user_id
    add_index :elec_round_winners, :registered_by_user_id
  end
end
