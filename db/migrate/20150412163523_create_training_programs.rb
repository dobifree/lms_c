class CreateTrainingPrograms < ActiveRecord::Migration
  def change
    create_table :training_programs do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
