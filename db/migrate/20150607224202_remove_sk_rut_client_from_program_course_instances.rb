class RemoveSkRutClientFromProgramCourseInstances < ActiveRecord::Migration
  def up
    remove_column :program_course_instances, :sk_rut_client
  end

  def down
    add_column :program_course_instances, :sk_rut_client, :string
  end
end
