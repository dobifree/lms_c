class CreatePeMemberGroups < ActiveRecord::Migration
  def change
    create_table :pe_member_groups do |t|
      t.references :pe_process
      t.references :pe_member
      t.references :pe_evaluation
      t.references :pe_group

      t.timestamps
    end
    add_index :pe_member_groups, :pe_process_id
    add_index :pe_member_groups, :pe_member_id
    add_index :pe_member_groups, :pe_evaluation_id
    add_index :pe_member_groups, :pe_group_id
  end
end
