class ChangeNameToColumnBpLicenceIdOnLicEVent < ActiveRecord::Migration
  def change
    rename_column :lic_events, :bp_licence_id, :bp_license_id
  end
end
