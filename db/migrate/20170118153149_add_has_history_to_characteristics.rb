class AddHasHistoryToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :has_history, :boolean, default: true
  end
end
