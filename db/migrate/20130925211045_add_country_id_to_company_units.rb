class AddCountryIdToCompanyUnits < ActiveRecord::Migration
  def change
    add_column :company_units, :country_id, :integer
  end
end
