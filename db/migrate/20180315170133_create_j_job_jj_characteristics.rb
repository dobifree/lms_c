class CreateJJobJjCharacteristics < ActiveRecord::Migration
  def change
    create_table :j_job_jj_characteristics do |t|
      t.references :j_job
      t.references :jj_characteristic
      t.references :jj_characteristic_value
      t.references :register_job_jj_characteristic
      t.string :value_string
      t.text :value_text
      t.date :value_date
      t.integer :value_int
      t.float :value_float
      t.string :value_file
      t.integer :jj_characteristic_value_id
      t.integer :register_job_jj_characteristic_id

      t.timestamps
    end
    add_index :j_job_jj_characteristics, :j_job_id
    add_index :j_job_jj_characteristics, :jj_characteristic_id
    add_index :j_job_jj_characteristics, :jj_characteristic_value_id
    add_index :j_job_jj_characteristics, :register_job_jj_characteristic_id, name: 'job_char_regs_jj_char_id'
  end
end
