class AddMinimumGradeToProgramCourseInstances < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :minimum_grade, :integer
  end
end
