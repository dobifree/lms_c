class AddDedicacionEstimadaToCourse < ActiveRecord::Migration
  def change
    add_column :courses, :dedicacion_estimada, :float, default: 0
  end
end
