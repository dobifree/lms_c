class RemoveColumnsByUserForSchedules < ActiveRecord::Migration
  def change
    remove_column :sc_schedules, :registered_by_user_id
    remove_column :sc_blocks, :registered_by_user_id
    remove_column :sc_characteristic_conditions, :registered_by_user_id
    remove_column :sc_schedules, :deactivated_by_user_id
    remove_column :sc_blocks, :deactivated_by_user_id
    remove_column :sc_characteristic_conditions, :deactivated_by_user_id
  end
end
