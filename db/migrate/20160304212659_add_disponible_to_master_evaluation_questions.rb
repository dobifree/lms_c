class AddDisponibleToMasterEvaluationQuestions < ActiveRecord::Migration
  def change
    add_column :master_evaluation_questions, :disponible, :boolean, default: true
  end
end
