class InsertSelModulesSelJm < ActiveRecord::Migration

  def change
    execute "insert into ct_modules set cod='sel', name='Selección', active=true, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'sel').count == 0
    execute "insert into ct_modules set cod='jm', name='Job Market', active=true, has_managers = false, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'jm').count == 0
  end

end
