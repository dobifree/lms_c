class AddValueString2ToCharacteristicValues < ActiveRecord::Migration
  def change
    add_column :characteristic_values, :value_string_2, :string
  end
end
