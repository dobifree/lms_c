class AddCalibrationCommentToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :calibration_comment, :text
  end
end
