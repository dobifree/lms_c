class AddInicioSizeToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :inicio_size, :integer, default: 20
  end
end
