class AddPosGuiCalMemberToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_gui_cal_member, :integer
    add_index :pe_characteristics, :pos_gui_cal_member
  end
end
