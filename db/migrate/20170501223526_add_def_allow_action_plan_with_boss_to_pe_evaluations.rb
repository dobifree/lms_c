class AddDefAllowActionPlanWithBossToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :def_allow_action_plan_with_boss, :boolean, default: false
  end
end
