class RemovePlanningProcessGoalFromDnc < ActiveRecord::Migration
  def change
    remove_column :dncs, :planning_process_goal_id
  end
end
