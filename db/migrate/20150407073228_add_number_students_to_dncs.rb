class AddNumberStudentsToDncs < ActiveRecord::Migration
  def change
    add_column :dncs, :number_students, :integer, default: 0
  end
end
