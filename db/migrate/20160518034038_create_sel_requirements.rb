class CreateSelRequirements < ActiveRecord::Migration
  def change
    create_table :sel_requirements do |t|
      t.references :sel_req_template
      t.integer :registered_by_user_id
      t.boolean :done
      t.integer :done_by_user_id
      t.datetime :done_at
      t.boolean :declined
      t.text :declined_comment
      t.integer :declined_by_user_id
      t.datetime :declined_at

      t.timestamps
    end
    add_index :sel_requirements, :sel_req_template_id
  end
end
