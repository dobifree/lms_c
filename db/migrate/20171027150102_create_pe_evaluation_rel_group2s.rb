class CreatePeEvaluationRelGroup2s < ActiveRecord::Migration
  def change
    create_table :pe_evaluation_rel_group2s do |t|
      t.references :pe_evaluation_rel
      t.references :pe_group2
      t.datetime :from_date
      t.datetime :to_date

      t.timestamps
    end
    add_index :pe_evaluation_rel_group2s, :pe_evaluation_rel_id
    add_index :pe_evaluation_rel_group2s, :pe_group2_id
  end
end
