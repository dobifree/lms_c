class CreateElecRounds < ActiveRecord::Migration
  def change
    create_table :elec_rounds do |t|
      t.references :elec_event
      t.references :elec_process_round
      t.datetime :from_date
      t.datetime :to_date
      t.boolean :done
      t.boolean :final

      t.timestamps
    end
    add_index :elec_rounds, :elec_event_id
    add_index :elec_rounds, :elec_process_round_id
    change_column :elec_rounds, :done, :boolean, :default => false
    change_column :elec_rounds, :final, :boolean, :default => false
  end
end
