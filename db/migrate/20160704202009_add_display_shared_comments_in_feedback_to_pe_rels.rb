class AddDisplaySharedCommentsInFeedbackToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :display_shared_comments_in_feedback, :boolean, default: false
  end
end
