class AddUpperCourseNameToCompanies < ActiveRecord::Migration

  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :upper_course_name
      add_column :companies, :upper_course_name, :boolean, default: false
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end
end
