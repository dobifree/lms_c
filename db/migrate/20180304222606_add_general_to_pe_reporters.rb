class AddGeneralToPeReporters < ActiveRecord::Migration
  def change
    add_column :pe_reporters, :general, :boolean, default: true
  end
end
