class AddEmailToAcceptDefinitionContentToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :email_to_accept_definition_content, :text
  end
end
