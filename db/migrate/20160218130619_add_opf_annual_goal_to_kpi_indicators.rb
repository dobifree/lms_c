class AddOpfAnnualGoalToKpiIndicators < ActiveRecord::Migration
  def change
    add_column :kpi_indicators, :opf_annual_goal, :string
  end
end
