class AddPeBoxIdToPeMember < ActiveRecord::Migration
  def change
    add_column :pe_members, :pe_box_id, :integer
    add_index :pe_members, :pe_box_id
  end
end
