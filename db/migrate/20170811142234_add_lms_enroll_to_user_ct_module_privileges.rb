class AddLmsEnrollToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :lms_enroll, :boolean, default: false
  end
end
