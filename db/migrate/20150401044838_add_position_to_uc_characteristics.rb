class AddPositionToUcCharacteristics < ActiveRecord::Migration
  def change
    add_column :uc_characteristics, :position, :integer
  end
end
