class AddbrgLvlPercentageIdToBrgBonus < ActiveRecord::Migration

  def change
    add_column :brg_bonus, :brg_lvl_percentage_id, :integer
    add_index :brg_bonus, :brg_lvl_percentage_id
  end
end
