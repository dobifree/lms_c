class CreateFolderFileValues < ActiveRecord::Migration
  def change
    create_table :folder_file_values do |t|
      t.references :folder_file
      t.references :user
      t.datetime :fecha
      t.integer :valor

      t.timestamps
    end
    add_index :folder_file_values, :folder_file_id
    add_index :folder_file_values, :user_id
  end
end
