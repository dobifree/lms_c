class AddTrainingModeIdToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :training_mode_id, :integer
    add_index :courses, :training_mode_id
  end
end
