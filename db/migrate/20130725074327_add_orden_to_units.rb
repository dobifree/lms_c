class AddOrdenToUnits < ActiveRecord::Migration
  def change
    add_column :units, :orden, :integer, default: 1
  end
end
