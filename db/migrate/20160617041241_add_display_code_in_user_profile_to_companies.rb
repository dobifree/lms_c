class AddDisplayCodeInUserProfileToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :display_code_in_user_profile, :boolean, default: false
  end
end
