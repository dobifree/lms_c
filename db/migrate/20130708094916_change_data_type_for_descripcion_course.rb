class ChangeDataTypeForDescripcionCourse < ActiveRecord::Migration
  def up
  	change_table :courses do |t|
      t.change :descripcion, :text
    end
  end

  def down
  end
end
