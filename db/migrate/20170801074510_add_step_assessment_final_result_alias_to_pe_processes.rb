class AddStepAssessmentFinalResultAliasToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_final_result_alias, :string, default: 'Valoración final'
  end
end
