class CreateTrackingFormNotifications < ActiveRecord::Migration
  def change
    create_table :tracking_form_notifications do |t|
      t.integer :action_type
      t.references :tracking_form_action
      t.string :name
      t.text :description
      t.boolean :active
      t.string :to
      t.string :cc
      t.string :subject
      t.text :body

      t.timestamps
    end
    add_index :tracking_form_notifications, :tracking_form_action_id
  end
end
