class CreateMedlicReports < ActiveRecord::Migration
  def change
    create_table :medlic_reports do |t|
      t.boolean :partial, default: false
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.references :medlic_process

      t.timestamps
    end
    add_index :medlic_reports, :medlic_process_id
    add_index :medlic_reports, :registered_by_user_id
    add_index :medlic_reports, :deactivated_by_user_id
  end
end
