class AddQueryByOthersToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :query_by_others, :boolean, default: true
  end
end
