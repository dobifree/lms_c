class ChangeDefaultBillItem < ActiveRecord::Migration
  def change
    change_column_default(:ben_cellphone_bill_items, :not_charge_to_payroll, false)
    change_column_default(:ben_cellphone_bill_items, :registered_manually, false)
    change_column_default(:ben_cellphone_processes, :charged_to_payroll, false)
  end
end