class AddAllowDefinitionByUsersAcceptedToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :allow_definition_by_users_accepted, :boolean, default: false
  end
end
