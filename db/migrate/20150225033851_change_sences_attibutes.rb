class ChangeSencesAttibutes < ActiveRecord::Migration

  def change
    rename_column :courses, :codigo_sence, :sence_code
    rename_column :courses, :numero_horas_sence, :sence_hours
    rename_column :courses, :valor_participante_sence, :sence_student_value
  end

end
