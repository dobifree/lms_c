class AddModuloSenceToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :modulo_sence, :boolean, default: false
  end
end
