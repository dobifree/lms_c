class AddValidEvaluationToPeAssessmentEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_assessment_evaluations, :valid_evaluation, :boolean, default: true
  end
end
