class AddColumnBrgWarningExceptionIdToVacRequests < ActiveRecord::Migration
  def change
    add_column :vac_requests, :bp_warning_exception_id, :integer
    add_index :vac_requests, :bp_warning_exception_id
  end
end
