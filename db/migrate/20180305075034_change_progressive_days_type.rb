class ChangeProgressiveDaysType < ActiveRecord::Migration
def change
  change_column :bp_general_rules_vacations, :progressive_days, :integer
end
end
