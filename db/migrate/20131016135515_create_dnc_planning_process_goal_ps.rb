class CreateDncPlanningProcessGoalPs < ActiveRecord::Migration
  def change
    create_table :dnc_planning_process_goal_ps do |t|
      t.references :dncp
      t.references :planning_process_goal

      t.timestamps
    end
    add_index :dnc_planning_process_goal_ps, :dncp_id
    add_index :dnc_planning_process_goal_ps, :planning_process_goal_id
  end
end
