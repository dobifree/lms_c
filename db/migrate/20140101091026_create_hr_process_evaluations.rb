class CreateHrProcessEvaluations < ActiveRecord::Migration
  def change
    create_table :hr_process_evaluations do |t|
      t.references :hr_process
      t.references :hr_process_dimension_e
      t.date :fecha_inicio_eval_jefe
      t.date :fecha_fin_eval_jefe
      t.date :fecha_inicio_eval_auto
      t.date :fecha_fin_eval_auto
      t.date :fecha_inicio_eval_sub
      t.date :fecha_fin_eval_sub
      t.date :fecha_inicio_eval_par
      t.date :fecha_fin_eval_par

      t.timestamps
    end
    add_index :hr_process_evaluations, :hr_process_id
    add_index :hr_process_evaluations, :hr_process_dimension_e_id
  end
end
