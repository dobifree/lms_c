class AddDisplayWeightToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :display_weight, :boolean, default: false
  end
end
