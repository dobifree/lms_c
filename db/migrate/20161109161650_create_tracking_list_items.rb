class CreateTrackingListItems < ActiveRecord::Migration
  def change
    create_table :tracking_list_items do |t|
      t.references :tracking_list
      t.integer :position
      t.string :name
      t.text :description

      t.timestamps
    end
    add_index :tracking_list_items, :tracking_list_id
  end
end
