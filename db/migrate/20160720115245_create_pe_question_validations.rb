class CreatePeQuestionValidations < ActiveRecord::Migration
  def change
    create_table :pe_question_validations do |t|
      t.references :pe_question
      t.integer :step_number
      t.boolean :before_confirmation
      t.boolean :validated, default: false
      t.text :comment

      t.timestamps
    end
    add_index :pe_question_validations, :pe_question_id
  end
end
