class AddHasStepFeedbackToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_feedback, :boolean, default: false
  end
end
