class AddAliasComment2ToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :alias_comment_2, :string, default: 'Comentario'
  end
end
