class AddLiqProcessIdToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :liq_process_id, :integer
    add_index :brc_processes, :liq_process_id
  end
end
