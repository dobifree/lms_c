class AddStepAssessmentSendEmailToValidatorToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_send_email_to_validator, :boolean,default: false
  end
end
