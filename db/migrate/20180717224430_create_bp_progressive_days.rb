class CreateBpProgressiveDays < ActiveRecord::Migration
  def change
    create_table :bp_progressive_days do |t|
      t.references :bp_general_rules_vacation
      t.integer :year
      t.integer :days
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id

      t.timestamps
    end
    add_index :bp_progressive_days, :bp_general_rules_vacation_id
    add_index :bp_progressive_days, :registered_by_admin_id, name: 'bp_progress_regis_admin_id'
    add_index :bp_progressive_days, :deactivated_by_admin_id, name: 'bp_progress_deact_admin_id'
  end
end