class AddRepDetailedFinalResultsShowRelWeightToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :rep_detailed_final_results_show_rel_weight, :boolean, default: false
  end
end
