class CreateUserCharacteristicRecords < ActiveRecord::Migration
  def change
    create_table :user_characteristic_records do |t|
      t.references :user
      t.references :characteristic
      t.text :value
      t.date :from_date
      t.date :to_date

      t.timestamps
    end
    add_index :user_characteristic_records, :user_id
    add_index :user_characteristic_records, :characteristic_id
  end
end
