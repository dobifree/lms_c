class AddCharacteristicValueIdToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :characteristic_value_id, :integer
    add_index :characteristics, :characteristic_value_id
  end
end
