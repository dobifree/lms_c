class AddPosRepCalibrationStatusToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_rep_calibration_status, :integer
    add_index :pe_characteristics, :pos_rep_calibration_status
  end
end
