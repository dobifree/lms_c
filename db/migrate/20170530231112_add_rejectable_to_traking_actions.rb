class AddRejectableToTrakingActions < ActiveRecord::Migration
  def change
    add_column :tracking_form_actions, :can_be_rejected, :boolean, :default => true
  end
end
