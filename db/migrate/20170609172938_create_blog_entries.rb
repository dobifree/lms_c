class CreateBlogEntries < ActiveRecord::Migration
  def change
    create_table :blog_entries do |t|
      t.references :user
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :blog_entries, :user_id
    add_index :blog_entries, :registered_by_user_id
  end
end
