class AddDisplayWeightInQresToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :display_weight_in_qres, :boolean, default: false
  end
end
