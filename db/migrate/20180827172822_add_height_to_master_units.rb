class AddHeightToMasterUnits < ActiveRecord::Migration
  def change
    add_column :master_units, :height, :int
  end
end
