class CreateScRecordBlocks < ActiveRecord::Migration
  def change
    create_table :sc_record_blocks do |t|
      t.references :sc_record
      t.references :sc_block

      t.timestamps
    end
    add_index :sc_record_blocks, :sc_record_id
    add_index :sc_record_blocks, :sc_block_id
  end
end
