class AddDisplayWeightInAssessmentToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :display_weight_in_assessment, :boolean, default: false
  end
end
