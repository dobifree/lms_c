class AddAttendedToPeQuestionValidations < ActiveRecord::Migration
  def change
    add_column :pe_question_validations, :attended, :boolean, default: false
  end
end
