class AddSizeToFolderFile < ActiveRecord::Migration
  def change
    add_column :folder_files, :size, :integer
  end
end
