class CreateJccCharacteristics < ActiveRecord::Migration
  def change
    create_table :jcc_characteristics do |t|
      t.references :j_cost_center
      t.references :characteristic
      t.string :value_string
      t.references :characteristic_value

      t.timestamps
    end
    add_index :jcc_characteristics, :j_cost_center_id
    add_index :jcc_characteristics, :characteristic_id
    add_index :jcc_characteristics, :characteristic_value_id
  end
end
