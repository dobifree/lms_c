class CreatePeMemberRels < ActiveRecord::Migration
  def change
    create_table :pe_member_rels do |t|
      t.references :pe_process
      t.boolean :finished, default: 0
      t.boolean :valid_evaluator, default: true
      t.integer :pe_member_evaluator_id
      t.references :pe_rel
      t.integer :pe_member_evaluated_id

      t.timestamps
    end
    add_index :pe_member_rels, :pe_process_id
    add_index :pe_member_rels, :pe_rel_id
    add_index :pe_member_rels, :pe_member_evaluator_id
    add_index :pe_member_rels, :pe_member_evaluated_id
  end
end
