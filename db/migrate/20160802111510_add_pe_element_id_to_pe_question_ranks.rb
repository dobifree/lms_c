class AddPeElementIdToPeQuestionRanks < ActiveRecord::Migration
  def change
    add_column :pe_question_ranks, :pe_element_id, :integer
    add_index :pe_question_ranks, :pe_element_id
  end
end
