class AddClientToProgramCourseInstances < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :client, :string
  end
end
