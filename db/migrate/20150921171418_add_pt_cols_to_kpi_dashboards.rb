class AddPtColsToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :pt_cols, :text
  end
end
