class AddAliasParToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_par, :string, default: 'Par'
  end
end
