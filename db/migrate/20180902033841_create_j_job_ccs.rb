class CreateJJobCcs < ActiveRecord::Migration
  def change
    create_table :j_job_ccs do |t|
      t.references :j_job
      t.references :j_cost_center

      t.timestamps
    end
    add_index :j_job_ccs, :j_job_id
    add_index :j_job_ccs, :j_cost_center_id
  end
end
