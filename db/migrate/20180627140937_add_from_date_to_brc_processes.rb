class AddFromDateToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :from_date, :datetime
  end
end
