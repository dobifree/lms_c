class AddInicioLetraToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :inicio_letra, :string, default: 'Helvetica'
  end
end
