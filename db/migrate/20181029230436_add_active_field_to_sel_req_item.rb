class AddActiveFieldToSelReqItem < ActiveRecord::Migration
  def change
    add_column :sel_req_items, :active, :boolean, default: true
  end
end
