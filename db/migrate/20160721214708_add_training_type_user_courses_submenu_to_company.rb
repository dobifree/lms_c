class AddTrainingTypeUserCoursesSubmenuToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :training_type_user_courses_submenu, :boolean, default: false
  end
end
