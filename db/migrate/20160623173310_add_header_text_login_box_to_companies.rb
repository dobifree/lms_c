class AddHeaderTextLoginBoxToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :header_text_login_box, :string, default: 'Ingrese sus datos de acceso'
  end
end
