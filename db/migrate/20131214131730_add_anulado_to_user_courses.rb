class AddAnuladoToUserCourses < ActiveRecord::Migration
  def change
    add_column :user_courses, :anulado, :boolean, default: false
  end
end
