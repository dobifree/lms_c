class CreateJmSubcharacteristics < ActiveRecord::Migration
  def change
    create_table :jm_subcharacteristics do |t|
      t.string :name
      t.text :description
      t.integer :option_type
      t.references :jm_list

      t.timestamps
    end
    add_index :jm_subcharacteristics, :jm_list_id
  end
end
