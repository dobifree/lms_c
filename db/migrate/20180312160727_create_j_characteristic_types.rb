class CreateJCharacteristicTypes < ActiveRecord::Migration
  def change
    create_table :j_characteristic_types do |t|
      t.string :name
      t.integer :position

      t.timestamps
    end
  end
end
