class AddRemovedToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :removed, :boolean, :default => false
  end
end
