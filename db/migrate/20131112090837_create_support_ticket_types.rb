class CreateSupportTicketTypes < ActiveRecord::Migration
  def change
    create_table :support_ticket_types do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
