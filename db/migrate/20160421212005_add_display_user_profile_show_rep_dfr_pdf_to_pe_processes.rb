class AddDisplayUserProfileShowRepDfrPdfToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :display_user_profile_show_rep_dfr_pdf, :boolean, default: false
  end
end
