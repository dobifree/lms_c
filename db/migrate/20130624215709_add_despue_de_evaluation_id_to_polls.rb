class AddDespueDeEvaluationIdToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :despues_de_evaluation_id, :integer
  end
end
