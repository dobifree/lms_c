class CreateSelEvaluationAttachments < ActiveRecord::Migration
  def change
    create_table :sel_evaluation_attachments do |t|
      t.references :sel_step_candidate
      t.string :description
      t.string :crypted_name
      t.string :original_filename
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :sel_evaluation_attachments, :sel_step_candidate_id
    add_index :sel_evaluation_attachments, :registered_by_user_id
  end
end
