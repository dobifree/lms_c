class CreateScScheduleUsers < ActiveRecord::Migration
  def change
    create_table :sc_schedule_users do |t|
      t.references :user
      t.references :sc_schedule
      t.boolean :he_able, :default => false
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.boolean :active, :default => false
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id

      t.timestamps
    end
    add_index :sc_schedule_users, :user_id
    add_index :sc_schedule_users, :sc_schedule_id
    add_index :sc_schedule_users, :deactivated_by_user_id
    add_index :sc_schedule_users, :registered_by_user_id
  end
end
