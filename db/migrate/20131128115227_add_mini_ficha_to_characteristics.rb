class AddMiniFichaToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :mini_ficha, :boolean, default: false
  end
end
