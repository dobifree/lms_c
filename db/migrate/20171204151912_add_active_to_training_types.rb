class AddActiveToTrainingTypes < ActiveRecord::Migration
  def change
    add_column :training_types, :active, :boolean, default: true
  end
end
