class CreatePeFeedbackFieldListItems < ActiveRecord::Migration
  def change
    create_table :pe_feedback_field_list_items do |t|
      t.string :description
      t.references :pe_feedback_field_list

      t.timestamps
    end
    add_index :pe_feedback_field_list_items, :pe_feedback_field_list_id
  end
end
