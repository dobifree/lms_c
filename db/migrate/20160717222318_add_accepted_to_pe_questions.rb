class AddAcceptedToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :accepted, :boolean, default: true
  end
end
