class AddDefaultStatusToVacRequests < ActiveRecord::Migration
  def change
    change_column_default :vac_requests, :status, 0
  end
end
