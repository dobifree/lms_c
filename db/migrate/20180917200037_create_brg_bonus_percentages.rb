class CreateBrgBonusPercentages < ActiveRecord::Migration
  def change
    create_table :brg_bonus_percentages do |t|
      t.references :brg_bonus
      t.references :brg_percentage_conversion

      t.timestamps
    end
    add_index :brg_bonus_percentages, :brg_bonus_id
    add_index :brg_bonus_percentages, :brg_percentage_conversion_id
  end
end
