class CreateLicApprovers < ActiveRecord::Migration
  def change
    create_table :lic_approvers do |t|
      t.references :user
      t.boolean :active, default:true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :lic_approvers, :user_id
    add_index :lic_approvers, :deactivated_by_user_id
    add_index :lic_approvers, :registered_by_user_id
  end
end
