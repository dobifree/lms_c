class AddIndicatorDiscreteAchievementToPeAssessmentQuestions < ActiveRecord::Migration
  def change
    add_column :pe_assessment_questions, :indicator_discrete_achievement, :string
  end
end
