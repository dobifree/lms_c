class AddTrainingTypeIdToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :training_type_id, :integer
    add_index :courses, :training_type_id
  end
end
