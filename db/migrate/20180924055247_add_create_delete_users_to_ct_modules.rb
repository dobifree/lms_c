class AddCreateDeleteUsersToCtModules < ActiveRecord::Migration
  def change
    add_column :ct_modules, :create_delete_users, :boolean, default: false
  end
end
