class CreateBlogForms < ActiveRecord::Migration
  def change
    create_table :blog_forms do |t|
      t.string :name
      t.text :description
      t.boolean :active

      t.timestamps
    end
  end
end
