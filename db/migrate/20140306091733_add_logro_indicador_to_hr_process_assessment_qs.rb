class AddLogroIndicadorToHrProcessAssessmentQs < ActiveRecord::Migration
  def change
    add_column :hr_process_assessment_qs, :logro_indicador, :float
  end
end
