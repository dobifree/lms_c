class CreatePeReporters < ActiveRecord::Migration
  def change
    create_table :pe_reporters do |t|
      t.references :pe_process
      t.references :user

      t.timestamps
    end
    add_index :pe_reporters, :pe_process_id
    add_index :pe_reporters, :user_id
  end
end
