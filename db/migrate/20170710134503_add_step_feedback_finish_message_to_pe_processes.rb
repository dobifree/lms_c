class AddStepFeedbackFinishMessageToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_finish_message, :string, default: 'Confirmo que he finalizado la retroalimentación.'
  end
end
