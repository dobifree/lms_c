class AddStepValidationOkMessageToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_validation_ok_message, :string, default: 'Confirmo que valido la evaluación'
  end
end
