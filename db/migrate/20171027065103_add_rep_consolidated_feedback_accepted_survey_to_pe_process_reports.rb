class AddRepConsolidatedFeedbackAcceptedSurveyToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_consolidated_feedback_accepted_survey, :boolean, :default => false
  end
end
