class AddEvalPresencialToDncps < ActiveRecord::Migration
  def change
    add_column :dncps, :eval_presencial, :boolean, default: true
  end
end
