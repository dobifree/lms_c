class AddDefaultsToElecEvent < ActiveRecord::Migration
  def change
    change_column :elec_events, :active, :boolean, :default => true
    change_column :elec_events, :manual, :boolean, :default => true
  end
end
