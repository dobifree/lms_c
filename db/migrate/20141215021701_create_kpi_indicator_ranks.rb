class CreateKpiIndicatorRanks < ActiveRecord::Migration
  def change
    create_table :kpi_indicator_ranks do |t|
      t.references :kpi_indicator
      t.float :rank
      t.float :goal

      t.timestamps
    end
    add_index :kpi_indicator_ranks, :kpi_indicator_id
  end
end
