class ChangeDefaultPeProcessStepSelection < ActiveRecord::Migration
  def change
    change_column :pe_processes, :step_selection_alias, :string, default: 'Seleccionar'
  end
end
