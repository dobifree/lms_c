class AddHeAbleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :he_able, :boolean, :default => false
  end
end
