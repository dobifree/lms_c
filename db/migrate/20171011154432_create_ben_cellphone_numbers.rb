class CreateBenCellphoneNumbers < ActiveRecord::Migration
  def change
    create_table :ben_cellphone_numbers do |t|
      t.references :user
      t.string :number
      t.string :nemo_plan
      t.boolean :active
      t.float :subsidy

      t.timestamps
    end
    add_index :ben_cellphone_numbers, :user_id
  end
end
