class AddAliasJefeInversoPluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_jefe_inverso_plu, :string, default: 'Colaboradores'
  end
end
