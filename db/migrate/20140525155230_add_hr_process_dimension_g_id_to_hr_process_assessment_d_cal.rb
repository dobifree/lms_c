class AddHrProcessDimensionGIdToHrProcessAssessmentDCal < ActiveRecord::Migration
  def change
    add_column :hr_process_assessment_d_cals, :hr_process_dimension_g_id, :integer
  end
end
