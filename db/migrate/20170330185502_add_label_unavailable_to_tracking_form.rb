class AddLabelUnavailableToTrackingForm < ActiveRecord::Migration
  def change
    add_column :tracking_forms, :label_unavailable, :text
  end
end
