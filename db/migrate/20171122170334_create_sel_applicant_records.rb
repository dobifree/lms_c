class CreateSelApplicantRecords < ActiveRecord::Migration
  def change
    create_table :sel_applicant_records do |t|
      t.references :sel_applicant
      t.references :jm_candidate_answer

      t.timestamps
    end
    add_index :sel_applicant_records, :sel_applicant_id
    add_index :sel_applicant_records, :jm_candidate_answer_id
  end
end
