class CreatePeQuestionActivityStatuses < ActiveRecord::Migration
  def change
    create_table :pe_question_activity_statuses do |t|
      t.references :pe_evaluation
      t.integer :position
      t.string :name
      t.string :color
      t.boolean :completed

      t.timestamps
    end
    add_index :pe_question_activity_statuses, :pe_evaluation_id
  end
end
