class AddSubsetAliasToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :subset_alias, :string
  end
end
