class AddColumnLabelTagForEventDateNameToBpEvent < ActiveRecord::Migration
  def change
    add_column :bp_events, :event_date_name, :string, default: 'Fecha de evento'
  end
end
