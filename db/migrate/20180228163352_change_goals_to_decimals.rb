class ChangeGoalsToDecimals < ActiveRecord::Migration
  def change
    change_column :pe_questions, :indicator_goal, :decimal, :precision => 20, :scale => 4
    change_column :pe_question_ranks, :goal, :decimal, :precision => 20, :scale => 4
    change_column :pe_question_rank_models, :goal, :decimal, :precision => 20, :scale => 4
  end
end
