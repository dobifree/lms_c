class AddBrcToJjCharacteristics < ActiveRecord::Migration
  def change
    add_column :jj_characteristics, :brc, :boolean, default: false
  end
end
