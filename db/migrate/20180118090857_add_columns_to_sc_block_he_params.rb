class AddColumnsToScBlockHeParams < ActiveRecord::Migration
  def change
    add_column :sc_he_block_params, :active, :boolean, default: true
    add_column :sc_he_block_params, :deactivated_at, :datetime
    add_column :sc_he_block_params, :deactivated_by_user_id, :integer

    add_index :sc_he_block_params, :deactivated_by_user_id
  end
end
