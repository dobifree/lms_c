class AddTraceableToSelProcess < ActiveRecord::Migration
  def change
    add_column :sel_processes, :traceable, :boolean, :default => true
  end
end
