class AddCurrentToJJobs < ActiveRecord::Migration
  def change
    add_column :j_jobs, :current, :boolean, default: true
  end
end
