class CreateTrackingFormStates < ActiveRecord::Migration
  def change
    create_table :tracking_form_states do |t|
      t.references :tracking_form
      t.integer :position
      t.string :name
      t.text :description
      t.string :color
      t.boolean :finish_tracking

      t.timestamps
    end
    add_index :tracking_form_states, :tracking_form_id
  end
end
