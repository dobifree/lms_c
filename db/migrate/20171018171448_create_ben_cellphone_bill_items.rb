class CreateBenCellphoneBillItems < ActiveRecord::Migration
  def change
    create_table :ben_cellphone_bill_items do |t|
      t.string :bill_folio
      t.string :description
      t.string :cellphone_number
      t.string :company_rut
      t.float :net_value
      t.float :discount
      t.float :net_total
      t.float :net_with_taxes
      t.integer :ben_cellphone_process_id
      t.integer :ben_cellphone_number_id

      t.timestamps
    end

    add_index :ben_cellphone_bill_items, :ben_cellphone_process_id, :name => 'ben_cellphone_bill_items_cp_id'
    add_index :ben_cellphone_bill_items, :ben_cellphone_number_id, :name => 'ben_cellphone_bill_items_cn_id'
  end
end
