class AddColumnsToBpPeriods < ActiveRecord::Migration
  def change
    add_column :bp_seasons, :since, :datetime
    add_column :bp_seasons, :until, :datetime
    add_column :bp_seasons, :deactivated_at, :datetime
    add_column :bp_seasons, :deactivated_by_admin_id, :integer
    add_column :bp_seasons, :registered_at, :datetime
    add_column :bp_seasons, :registered_by_admin_id, :integer

    add_index :bp_seasons, :deactivated_by_admin_id
    add_index :bp_seasons, :registered_by_admin_id

    add_column :bp_season_periods, :since, :datetime
    add_column :bp_season_periods, :until, :datetime
    remove_column :bp_season_periods, :active

  end
end
