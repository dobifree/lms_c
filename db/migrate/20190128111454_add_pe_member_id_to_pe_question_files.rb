class AddPeMemberIdToPeQuestionFiles < ActiveRecord::Migration
  def change
    add_column :pe_question_files, :pe_member_id, :integer
    add_index :pe_question_files, :pe_member_id
  end
end
