class AddSelApproveRequirementsToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :sel_approve_requirements, :boolean, :default => false
  end
end
