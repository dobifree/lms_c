class AddStepQueryDefinitionsAcceptMailToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_query_definitions_accept_mail, :boolean
  end
end
