class CreateScholarshipApplicationItems < ActiveRecord::Migration
  def change
    create_table :scholarship_application_items do |t|
      t.references :scholarship_item_group
      t.integer :position
      t.string :name
      t.text :description
      t.boolean :mandatory
      t.integer :item_type
      t.references :scholarship_list

      t.timestamps
    end
    add_index :scholarship_application_items, :scholarship_item_group_id
    add_index :scholarship_application_items, :scholarship_list_id
  end
end
