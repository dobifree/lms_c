class CreateScUserProcesses < ActiveRecord::Migration
  def change
    create_table :sc_user_processes do |t|
      t.references :user
      t.references :sc_process
      t.decimal :he_total, :precision => 12, :scale => 4

      t.timestamps
    end
    add_index :sc_user_processes, :user_id
    add_index :sc_user_processes, :sc_process_id
  end
end
