class CreateBrgBonus < ActiveRecord::Migration
  def change
    create_table :brg_bonus do |t|
      t.integer :prev_bonus_id
      t.references :brg_process
      t.references :brg_group_user
      t.decimal :actual_value, :precision => 20, :scale => 5
      t.decimal :registered_value, :precision => 20, :scale => 5
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.text :registered_description
      t.boolean :paid
      t.datetime :paid_at
      t.integer :paid_by_user_id
      t.text :paid_description

      t.timestamps
    end
    add_index :brg_bonus, :brg_process_id
    add_index :brg_bonus, :brg_group_user_id
    add_index :brg_bonus, :deactivated_by_user_id
    add_index :brg_bonus, :registered_by_user_id
    add_index :brg_bonus, :prev_bonus_id
    add_index :brg_bonus, :paid_by_user_id
  end
end