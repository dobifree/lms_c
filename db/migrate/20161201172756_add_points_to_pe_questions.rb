class AddPointsToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :points, :float
  end
end
