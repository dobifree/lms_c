class AddFieldAutomaticAssignToSelAttendants < ActiveRecord::Migration
  def change
    add_column :sel_attendants, :automatic_assign, :boolean, default: false
  end
end
