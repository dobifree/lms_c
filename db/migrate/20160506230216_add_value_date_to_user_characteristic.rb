class AddValueDateToUserCharacteristic < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :value_date, :date
  end
end
