class AddRepAssignStatusToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_assign_status, :boolean, default: false
  end
end
