class AddPeFeedbackFieldListItemIdToPeFeedbackFieldListItems < ActiveRecord::Migration
  def change
    add_column :pe_feedback_field_list_items, :pe_feedback_field_list_item_id, :integer
    add_index :pe_feedback_field_list_items, :pe_feedback_field_list_item_id, name: 'pe_f_parent_item_id'
  end
end
