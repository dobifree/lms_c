class ChangeBillItemTypes < ActiveRecord::Migration
  def change
    change_column :ben_cellphone_bill_items, :net_value, :decimal, :precision => 12, :scale => 4
    change_column :ben_cellphone_bill_items, :discount, :decimal, :precision => 12, :scale => 4
    change_column :ben_cellphone_bill_items, :net_total, :decimal, :precision => 12, :scale => 4
    change_column :ben_cellphone_bill_items, :net_with_taxes, :decimal, :precision => 12, :scale => 4

    change_column :ben_cellphone_numbers, :subsidy, :decimal, :precision => 12, :scale => 4

    change_column :ben_user_cellphone_processes, :total_bill, :decimal, :precision => 12, :scale => 4
    change_column :ben_user_cellphone_processes, :configured_subsidy, :decimal, :precision => 12, :scale => 4
    change_column :ben_user_cellphone_processes, :applied_subsidy, :decimal, :precision => 12, :scale => 4
    change_column :ben_user_cellphone_processes, :total_charged, :decimal, :precision => 12, :scale => 4

  end
end
