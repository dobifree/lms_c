class AddJmCharacteristicToJmSubcharacteristic < ActiveRecord::Migration
  def change
    add_column :jm_subcharacteristics, :jm_characteristic_id, :integer
  end
end
