class AddLastUpdateToKpiSets < ActiveRecord::Migration
  def change
    add_column :kpi_sets, :last_update, :datetime
  end
end
