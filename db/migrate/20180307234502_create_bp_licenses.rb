class CreateBpLicenses < ActiveRecord::Migration
  def change
    create_table :bp_licenses do |t|
      t.references :bp_group
      t.references :bp_event
      t.string :name
      t.text :description
      t.integer :days
      t.float :money
      t.string :currency
      t.boolean :straight_time, :default => true
      t.integer :antiquity
      t.integer :days_limit
      t.boolean :days_limit_only_laborable, :default => true
      t.integer :days_between_days
      t.integer :days_since
      t.boolean :only_laborable_days, :default => true
      t.integer :period_times
      t.boolean :period_anniversary, :default => true
      t.datetime :period_begin_1
      t.datetime :period_begin_2
      t.boolean :period_condition_begin, :default => true
      t.integer :period_condition_min_days
      t.float :period_condition_min_percent
      t.references :bp_season
      t.boolean :season_condition_begin, :default => true
      t.integer :season_condition_min_days
      t.float :season_condition_min_percent
      t.boolean :active, :default => true
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id

      t.timestamps
    end
    add_index :bp_licenses, :bp_group_id
    add_index :bp_licenses, :bp_event_id
    add_index :bp_licenses, :bp_season_id
    add_index :bp_licenses, :deactivated_by_admin_id
    add_index :bp_licenses, :registered_by_admin_id

  end
end
