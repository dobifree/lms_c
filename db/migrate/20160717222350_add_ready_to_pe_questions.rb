class AddReadyToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :ready, :boolean, default: true
  end
end
