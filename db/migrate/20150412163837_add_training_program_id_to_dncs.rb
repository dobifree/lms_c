class AddTrainingProgramIdToDncs < ActiveRecord::Migration
  def change
    add_column :dncs, :training_program_id, :integer
    add_index :dncs, :training_program_id
  end
end
