class CreateScBlocks < ActiveRecord::Migration
  def change
    create_table :sc_blocks do |t|
      t.string :name
      t.text :description
      t.integer :day
      t.references :sc_schedule

      t.timestamps
    end
    add_index :sc_blocks, :sc_schedule_id
  end
end
