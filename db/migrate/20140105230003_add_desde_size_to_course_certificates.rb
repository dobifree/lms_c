class AddDesdeSizeToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :desde_size, :integer, default: 20
  end
end
