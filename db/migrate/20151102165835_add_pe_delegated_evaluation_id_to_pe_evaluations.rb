class AddPeDelegatedEvaluationIdToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :pe_delegated_evaluation_id, :integer
    add_index :pe_evaluations, :pe_delegated_evaluation_id
  end
end
