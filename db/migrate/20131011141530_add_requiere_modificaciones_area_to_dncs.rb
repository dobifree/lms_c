class AddRequiereModificacionesAreaToDncs < ActiveRecord::Migration
  def change
    add_column :dncs, :requiere_modificaciones_area, :boolean, default: false
  end
end
