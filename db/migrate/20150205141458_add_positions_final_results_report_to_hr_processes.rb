class AddPositionsFinalResultsReportToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :positions_final_results_report, :string
  end
end
