class CreateCourseTrainingCharacteristics < ActiveRecord::Migration
  def change
    create_table :course_training_characteristics do |t|
      t.references :course
      t.references :training_characteristic
      t.string :value_string
      t.integer :value_int
      t.references :training_characteristic_value

      t.timestamps
    end
    add_index :course_training_characteristics, :course_id, name: 'ctc_cid'
    add_index :course_training_characteristics, :training_characteristic_id, name: 'ctc_tcid'
    add_index :course_training_characteristics, :training_characteristic_value_id, name: 'ctc_tcvid'
  end
end
