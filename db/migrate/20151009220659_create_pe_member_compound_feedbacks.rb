class CreatePeMemberCompoundFeedbacks < ActiveRecord::Migration
  def change
    create_table :pe_member_compound_feedbacks do |t|
      t.references :pe_member_feedback
      t.references :pe_feedback_compound_field
      t.text :comment

      t.timestamps
    end
    add_index :pe_member_compound_feedbacks, :pe_member_feedback_id, name: 'pe_mem_com_feed_mem_feed_id'
    add_index :pe_member_compound_feedbacks, :pe_feedback_compound_field_id, name: 'pe_mem_com_feed_mem_com_feed_id'
  end
end
