class CreateScUserToValidates < ActiveRecord::Migration
  def change
    create_table :sc_user_to_validates do |t|
      t.references :user
      t.boolean :active, :default => true
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id

      t.timestamps
    end
    add_index :sc_user_to_validates, :user_id
    add_index :sc_user_to_validates, :registered_by_user_id
    add_index :sc_user_to_validates, :deactivated_by_user_id
  end
end
