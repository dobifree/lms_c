class CreateBpRuleVacations < ActiveRecord::Migration
  def change
    create_table :bp_rule_vacations do |t|
      t.references :bp_group
      t.string :name
      t.text :description
      t.boolean :active, :default => true
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id
      t.datetime :registered_at
      t.integer :registered_by_admin_id

      t.timestamps
    end
    add_index :bp_rule_vacations, :bp_group_id
    add_index :bp_rule_vacations, :registered_by_admin_id
    add_index :bp_rule_vacations, :deactivated_by_admin_id

  end
end
