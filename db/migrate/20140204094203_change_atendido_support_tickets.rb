class ChangeAtendidoSupportTickets < ActiveRecord::Migration
  def change
    change_column :support_tickets, :atendido, :boolean, default: false
  end
end
