class AddColumnPosToBpEvents < ActiveRecord::Migration
  def change
    add_column :bp_events, :position, :integer, default: 1
    add_column :bp_events, :available, :boolean, default: true
    add_column :bp_events, :unavailable_at, :datetime
    add_column :bp_events, :unavailable_by_admin_id, :integer
    add_index :bp_events, :unavailable_by_admin_id
  end
end
