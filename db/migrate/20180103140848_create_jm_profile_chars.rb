class CreateJmProfileChars < ActiveRecord::Migration
  def change
    create_table :jm_profile_chars do |t|
      t.references :jm_profile_form
      t.references :jm_characteristic
      t.integer :position

      t.timestamps
    end
    add_index :jm_profile_chars, :jm_profile_form_id
    add_index :jm_profile_chars, :jm_characteristic_id
  end
end
