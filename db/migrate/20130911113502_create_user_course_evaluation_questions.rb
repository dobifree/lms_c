class CreateUserCourseEvaluationQuestions < ActiveRecord::Migration
  def change
    create_table :user_course_evaluation_questions do |t|
      t.integer :numero
      t.references :user_course_evaluation
      t.references :master_evaluation
      t.references :master_evaluation_question
      t.boolean :respondida
      t.boolean :correcta
      t.string :alternativas
      t.datetime :fecha_respuesta

      t.timestamps
    end
    add_index :user_course_evaluation_questions, :user_course_evaluation_id, name: 'u_c_e_q_uceval_id'
    add_index :user_course_evaluation_questions, :master_evaluation_id, name: 'u_c_e_q_meval_id'
    add_index :user_course_evaluation_questions, :master_evaluation_question_id, name: 'u_c_e_q_mevalq_id'
  end
end
