class CreatePeFeedbackFields < ActiveRecord::Migration
  def change
    create_table :pe_feedback_fields do |t|
      t.references :pe_process
      t.integer :position
      t.string :name
      t.boolean :simple, default: true
      t.string :compound_names

      t.timestamps
    end
    add_index :pe_feedback_fields, :pe_process_id
  end
end
