class AddPtAggregatorFieldToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :pt_aggregator_field, :text
  end
end
