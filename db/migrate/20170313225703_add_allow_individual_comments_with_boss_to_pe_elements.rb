class AddAllowIndividualCommentsWithBossToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :allow_individual_comments_with_boss, :boolean, default: false
  end
end
