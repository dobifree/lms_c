class CreateLicEvents < ActiveRecord::Migration
  def change
    create_table :lic_events do |t|
      t.references :user
      t.references :bp_licence
      t.references :bp_form
      t.integer :prev_lic_event_id
      t.date :date_event
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id
      t.timestamps
    end
    add_index :lic_events, :user_id
    add_index :lic_events, :bp_licence_id
    add_index :lic_events, :deactivated_by_user_id
    add_index :lic_events, :registered_by_user_id
    add_index :lic_events, :prev_lic_event_id
  end
end
