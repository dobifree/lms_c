class CreatePeManagers < ActiveRecord::Migration
  def change
    create_table :pe_managers do |t|
      t.references :pe_process
      t.references :user

      t.timestamps
    end
    add_index :pe_managers, :pe_process_id
    add_index :pe_managers, :user_id
  end
end
