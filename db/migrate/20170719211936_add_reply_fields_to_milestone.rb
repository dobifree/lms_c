class AddReplyFieldsToMilestone < ActiveRecord::Migration
  def change
    add_column :tracking_milestones, :replied_comment, :text
    add_column :tracking_milestones, :replied_by_user_id, :integer
    add_column :tracking_milestones, :replied_at, :datetime
    add_index :tracking_milestones, :replied_by_user_id
  end
end
