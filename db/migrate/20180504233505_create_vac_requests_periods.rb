class CreateVacRequestsPeriods < ActiveRecord::Migration
  def change
    create_table :vac_requests_periods do |t|
      t.datetime :request_since
      t.datetime :request_until
      t.datetime :approve_since
      t.datetime :approve_until
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end

    add_index :vac_requests_periods, :registered_by_user_id, name: 'vac_re_periods_regis_user_id'
  end
end
