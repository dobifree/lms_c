class CreateHrProcessEvaluationManualQcs < ActiveRecord::Migration
  def change
    create_table :hr_process_evaluation_manual_qcs do |t|
      t.text :comentario
      t.datetime :fecha
      t.references :hr_process_evaluation_manual_q

      t.timestamps
    end
    add_index :hr_process_evaluation_manual_qcs, :hr_process_evaluation_manual_q_id, name: 'hr_p_e_mq'
  end
end
