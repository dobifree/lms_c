class AddFechaFormatoToCourseCertificate < ActiveRecord::Migration
  def change
    add_column :course_certificates, :fecha_formato, :string, default: 'full_date'
  end
end
