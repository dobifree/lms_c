class AddPeQuestionModelIdToPeQuestion < ActiveRecord::Migration
  def change
    add_column :pe_questions, :pe_question_model_id, :integer
    add_index :pe_questions, :pe_question_model_id
  end
end
