class AddColumnsRegisteredAtRegisteredByToScDocumentation < ActiveRecord::Migration
  def change
    add_column :sc_documentations, :registered_at, :datetime
    add_column :sc_documentations, :registered_by_user_id, :integer
    add_index :sc_documentations, :registered_by_user_id
  end
end
