class AddAeAvisaJefeEmailToHrProcess < ActiveRecord::Migration
  def change
    add_column :hr_processes, :ae_avisa_jefe_email, :boolean, default: false
  end
end
