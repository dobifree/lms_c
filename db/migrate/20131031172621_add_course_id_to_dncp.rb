class AddCourseIdToDncp < ActiveRecord::Migration
  def change
    add_column :dncps, :course_id, :integer
  end
end
