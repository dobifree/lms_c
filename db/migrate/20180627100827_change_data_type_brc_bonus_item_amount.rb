class ChangeDataTypeBrcBonusItemAmount < ActiveRecord::Migration
  def change
    change_column :brc_bonus_items, :amount, :decimal, :precision => 20, :scale => 4, default: 0
  end
end
