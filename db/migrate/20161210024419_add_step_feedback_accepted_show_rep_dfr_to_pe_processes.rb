class AddStepFeedbackAcceptedShowRepDfrToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_accepted_show_rep_dfr, :boolean
  end
end
