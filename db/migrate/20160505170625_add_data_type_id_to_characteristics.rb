class AddDataTypeIdToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :data_type_id, :integer, default: 0
    add_index :characteristics, :data_type_id
  end
end
