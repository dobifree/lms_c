class AddPositionToPeRels < ActiveRecord::Migration
  def change
    add_column :pe_rels, :position, :integer, default: 0
  end
end
