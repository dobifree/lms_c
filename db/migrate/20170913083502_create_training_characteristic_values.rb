class CreateTrainingCharacteristicValues < ActiveRecord::Migration
  def change
    create_table :training_characteristic_values do |t|
      t.string :name
      t.integer :position
      t.string :value_string
      t.references :training_characteristic

      t.timestamps
    end
    add_index :training_characteristic_values, :training_characteristic_id, name: 'tcv_tcid'
  end
end
