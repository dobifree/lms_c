class CreatePeQuestionComments < ActiveRecord::Migration
  def change
    create_table :pe_question_comments do |t|
      t.references :pe_question
      t.references :pe_member_rel
      t.text :comment
      t.datetime :registered_at

      t.timestamps
    end
    add_index :pe_question_comments, :pe_question_id
    add_index :pe_question_comments, :pe_member_rel_id
  end
end
