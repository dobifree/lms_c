class AddPosReportingToLmsCharacteristics < ActiveRecord::Migration
  def change
    add_column :lms_characteristics, :pos_reporting, :integer
  end
end
