class AddLibreToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :libre, :boolean, default: true
  end
end
