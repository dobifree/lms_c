class AddDetailedDefinitionIToPeProcessReportsEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_process_reports_evaluations, :detailed_definition_i, :boolean, default: false
  end
end
