class CreateJmOptions < ActiveRecord::Migration
  def change
    create_table :jm_options do |t|
      t.references :jm_characteristic
      t.string :name
      t.text :description
      t.integer :position

      t.timestamps
    end
    add_index :jm_options, :jm_characteristic_id
  end
end
