class AddNumberOfCompoundFeedbacksToPeMemberFeedbacks < ActiveRecord::Migration
  def change
    add_column :pe_member_feedbacks, :number_of_compound_feedbacks, :integer
  end
end
