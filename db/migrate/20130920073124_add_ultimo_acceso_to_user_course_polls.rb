class AddUltimoAccesoToUserCoursePolls < ActiveRecord::Migration
  def change
    add_column :user_course_polls, :ultimo_acceso, :datetime
  end
end
