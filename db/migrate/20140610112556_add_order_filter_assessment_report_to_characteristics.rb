class AddOrderFilterAssessmentReportToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :order_filter_assessment_report, :integer
  end
end
