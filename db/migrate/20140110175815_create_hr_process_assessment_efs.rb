class CreateHrProcessAssessmentEfs < ActiveRecord::Migration
  def change
    create_table :hr_process_assessment_efs do |t|
      t.references :hr_process_evaluation
      t.references :hr_process_user
      t.float :resultado_puntos
      t.float :resultado_porcentaje
      t.datetime :fecha

      t.timestamps
    end
    add_index :hr_process_assessment_efs, :hr_process_evaluation_id
    add_index :hr_process_assessment_efs, :hr_process_user_id
  end
end
