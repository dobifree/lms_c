class AddCalculatePercentageToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :calculate_percentage, :boolean, default: false
  end
end
