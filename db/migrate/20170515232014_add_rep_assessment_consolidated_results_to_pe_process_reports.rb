class AddRepAssessmentConsolidatedResultsToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_assessment_consolidated_results, :boolean, default: false
  end
end
