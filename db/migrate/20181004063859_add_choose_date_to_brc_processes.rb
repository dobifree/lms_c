class AddChooseDateToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :choose_date, :datetime
  end
end
