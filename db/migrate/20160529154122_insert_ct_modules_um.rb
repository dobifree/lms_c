class InsertCtModulesUm < ActiveRecord::Migration
  def change
    execute "insert into ct_modules set cod='um', name='Gestión de usuarios', active=true, has_managers = true, created_at = NOW(), updated_at = NOW()" if CtModule.where('cod = ?', 'um').count == 0
  end
end
