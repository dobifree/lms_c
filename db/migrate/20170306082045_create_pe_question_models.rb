class CreatePeQuestionModels < ActiveRecord::Migration
  def change
    create_table :pe_question_models do |t|
      t.string :description
      t.float :weight, default: 0
      t.references :pe_evaluation
      t.references :pe_element
      t.boolean :has_comment
      t.references :stored_image

      t.timestamps
    end
    add_index :pe_question_models, :pe_evaluation_id
    add_index :pe_question_models, :pe_element_id
    add_index :pe_question_models, :stored_image_id
  end
end
