class AddValueFloatToUserCharacteristicRecords < ActiveRecord::Migration
  def change
    add_column :user_characteristic_records, :value_float, :float
  end
end
