class CreateVacRuleRecords < ActiveRecord::Migration
  def change
    create_table :vac_rule_records do |t|
      t.references :vac_request
      t.references :bp_condition_vacation
      t.date :days_begin
      t.date :days_end
      t.date :days_real_end
      t.boolean :money_paid, default: false
      t.datetime :money_paid_at
      t.integer :money_paid_by_user_id
      t.boolean :refund_pending, default: false
      t.datetime :refund_requested_at
      t.integer :refund_requested_by_user_id
      t.datetime :refund_done_at
      t.integer :refund_done_by_user_id
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :vac_rule_records, :vac_request_id
    add_index :vac_rule_records, :bp_condition_vacation_id
    add_index :vac_rule_records, :money_paid_by_user_id, name: 'vac_rule_money_paid_user_id'
    add_index :vac_rule_records, :refund_requested_by_user_id, name: 'vac_refund_req_user_id'
    add_index :vac_rule_records, :refund_done_by_user_id, name: 'vac_refund_done_user_id'
    add_index :vac_rule_records, :deactivated_by_user_id, name: 'vac_rule_recor_deact_user_id'
    add_index :vac_rule_records, :registered_by_user_id, name: 'vac_rule_recor_regis_user_id'
  end
end
