class PopulateHistorySelReqLogs < ActiveRecord::Migration
  def change
    SelRequirement.all.each do |sel_req|
      first_log = sel_req.sel_req_logs.new
      first_log.log_state_id = 1 # enviado
      first_log.registered_at = sel_req.registered_at
      first_log.registered_by_user_id = sel_req.registered_by_user_id
      first_log.save
      next unless sel_req.sel_process
      second_log = sel_req.sel_req_logs.new
      second_log.log_state_id = 2 # aprobado
      second_log.registered_at = sel_req.registered_at
      second_log.registered_by_user_id = sel_req.registered_by_user_id
      second_log.comment = '## Aprobación automática, sin flujo de aprobación. ##'
      second_log.save
    end
  end
end
