class AddPeDimensionGroupIdToPeAssessmentFinalEvaluation < ActiveRecord::Migration
  def change
    add_column :pe_assessment_final_evaluations, :pe_dimension_group_id, :integer
    add_index :pe_assessment_final_evaluations, :pe_dimension_group_id
  end
end
