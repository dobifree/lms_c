class AddColumnChangesToUserAndCompanyForAzureLandscape < ActiveRecord::Migration
  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :active_directory_auto_redirection
      add_column :companies, :active_directory_auto_redirection, :boolean, default: true
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end
end
