class AddOriginalPercentageToPeAssessmentFinalEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_assessment_final_evaluations, :original_percentage, :float
  end
end
