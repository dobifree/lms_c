class AddHastaLetraToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :hasta_letra, :string, default: 'Helvetica'
  end
end
