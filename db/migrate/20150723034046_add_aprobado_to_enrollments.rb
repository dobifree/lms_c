class AddAprobadoToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :aprobado, :boolean
  end
end
