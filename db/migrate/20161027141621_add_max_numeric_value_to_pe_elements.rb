class AddMaxNumericValueToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :max_numeric_value, :integer
  end
end
