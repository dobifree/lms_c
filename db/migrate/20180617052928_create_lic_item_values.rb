class CreateLicItemValues < ActiveRecord::Migration
  def change
    create_table :lic_item_values do |t|
      t.references :lic_form
      t.references :bp_item
      t.text :value
      t.string :value_string
      t.text :value_text
      t.integer :value_int
      t.float :value_float
      t.decimal :value_decimal, :precision => 12, :scale => 4
      t.datetime :value_datetime
      t.time :value_hour
      t.integer :value_option_id
      t.text :value_file
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :lic_item_values, :lic_form_id
    add_index :lic_item_values, :bp_item_id
    add_index :lic_item_values, :deactivated_by_user_id
    add_index :lic_item_values, :registered_by_user_id
  end
end
