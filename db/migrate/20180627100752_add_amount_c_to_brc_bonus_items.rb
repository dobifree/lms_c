class AddAmountCToBrcBonusItems < ActiveRecord::Migration
  def change
    add_column :brc_bonus_items, :amount_c, :decimal, :precision => 20, :scale => 4, :default => 0
  end
end
