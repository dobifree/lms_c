class AddDescPlaceholderToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :desc_placeholder, :string
  end
end
