class AddDefViewGoalToKpiDashboard < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :def_view_goal, :boolean, default: true
  end
end
