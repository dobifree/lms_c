class AddCharValueSelectedToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :char_value_selected, :boolean, default: true
  end
end
