class CreateHrProcessAssessmentQs < ActiveRecord::Migration
  def change
    create_table :hr_process_assessment_qs do |t|
      t.references :hr_process_assessment_e
      t.references :hr_process_evaluation_q
      t.references :hr_process_user
      t.float :resultado_puntos
      t.float :resultado_porcentaje
      t.references :hr_process_evaluation_a
      t.datetime :fecha

      t.timestamps
    end
    add_index :hr_process_assessment_qs, :hr_process_assessment_e_id
    add_index :hr_process_assessment_qs, :hr_process_evaluation_q_id
    add_index :hr_process_assessment_qs, :hr_process_user_id
    add_index :hr_process_assessment_qs, :hr_process_evaluation_a_id
  end
end
