class CreateNodes < ActiveRecord::Migration
  def change
    create_table :nodes do |t|
      t.text :nombre
      t.references :user

      t.timestamps
    end
    add_index :nodes, :user_id
  end
end
