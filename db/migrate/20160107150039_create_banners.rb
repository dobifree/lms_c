class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :name
      t.string :crypted_name
      t.integer :display_method
      t.integer :display_content
      t.boolean :active
      t.boolean :complement_visible
      t.text :link

      t.timestamps
    end
  end
end
