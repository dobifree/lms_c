class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.string :nombre
      t.string :descripcion

      t.timestamps
    end
  end
end
