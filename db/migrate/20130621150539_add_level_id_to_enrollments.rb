class AddLevelIdToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :level_id, :integer
  end
end
