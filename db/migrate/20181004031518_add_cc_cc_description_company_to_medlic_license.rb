class AddCcCcDescriptionCompanyToMedlicLicense < ActiveRecord::Migration
  def change
    add_column :medlic_licenses, :cc, :string
    add_column :medlic_licenses, :cc_description, :string
    add_column :medlic_licenses, :company, :string
  end
end
