class AddOrdenToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :orden, :integer, default: 1
  end
end
