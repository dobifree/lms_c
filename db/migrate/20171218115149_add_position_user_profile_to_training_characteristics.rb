class AddPositionUserProfileToTrainingCharacteristics < ActiveRecord::Migration
  def change
    add_column :training_characteristics, :position_user_profile, :integer
  end
end
