class AddPeOriginalDimensionGroupIdToPeAssessmentFinalEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_assessment_final_evaluations, :pe_original_dimension_group_id, :integer
    add_index :pe_assessment_final_evaluations, :pe_original_dimension_group_id, name: 'pe_assessment_final_evaluations_orign_dim_group_id'
  end
end
