class AddRejectedAttributesToScRecord < ActiveRecord::Migration
  def change
    add_column :sc_records, :rejected_at, :datetime
    add_column :sc_records, :rejected_by_user_id, :integer

    add_index :sc_records, :rejected_by_user_id
  end
end
