class AddColumnsLaboralAndRefundableToBp < ActiveRecord::Migration
  def change
    add_column :bp_condition_vacations, :bonus_only_laborable_day, :boolean, default: true
    add_column :bp_condition_vacations, :refundable, :boolean, default: true

    add_column :bp_licenses, :days_only_laborable_day, :boolean, default: true
    add_column :bp_licenses, :refundable, :boolean, default: true

    rename_column :bp_general_rules_vacations, :progressive_days, :progressive_name_step
    add_column :bp_general_rules_vacations, :only_laborable_day, :boolean, default: true
  end
end
