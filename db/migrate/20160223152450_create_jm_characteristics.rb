class CreateJmCharacteristics < ActiveRecord::Migration
  def change
    create_table :jm_characteristics do |t|
      t.string :name
      t.text :description
      t.integer :option_type

      t.timestamps
    end
  end
end
