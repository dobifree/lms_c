class AddStepFeedbackShowCalibrationToPeProcess < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_show_calibration, :boolean, default: false
  end
end
