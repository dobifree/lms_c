class AddOrdenToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :orden, :integer, default: 1
  end
end
