class AddAvailableIndicatorTypesToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :available_indicator_types, :string
  end
end
