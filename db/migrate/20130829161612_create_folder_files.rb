class CreateFolderFiles < ActiveRecord::Migration
  def change
    create_table :folder_files do |t|
      t.string :nombre
      t.string :crypted_name
      t.references :folder
      t.references :user
      t.boolean :descargable
      t.string :extension

      t.timestamps
    end
    add_index :folder_files, :folder_id
    add_index :folder_files, :user_id
  end
end
