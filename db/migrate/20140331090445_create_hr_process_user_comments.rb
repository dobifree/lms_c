class CreateHrProcessUserComments < ActiveRecord::Migration
  def change
    create_table :hr_process_user_comments do |t|
      t.references :hr_process_user
      t.text :comentario

      t.timestamps
    end
    add_index :hr_process_user_comments, :hr_process_user_id
  end
end
