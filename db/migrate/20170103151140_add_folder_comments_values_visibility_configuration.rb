class AddFolderCommentsValuesVisibilityConfiguration < ActiveRecord::Migration
  def change
    add_column :companies, :accepts_comments, :boolean, :default => true
    add_column :companies, :accepts_values, :boolean, :default => true
  end
end
