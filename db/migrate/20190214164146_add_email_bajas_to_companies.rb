class AddEmailBajasToCompanies < ActiveRecord::Migration
  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :email_bajas
      add_column :companies, :email_bajas, :string
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end

end
