class CreatePeFeedbackAcceptedFields < ActiveRecord::Migration
  def change
    create_table :pe_feedback_accepted_fields do |t|
      t.references :pe_process
      t.integer :position
      t.string :name
      t.boolean :simple, default: true
      t.integer :field_type, default: 0
      t.boolean :required, default: true

      t.timestamps
    end
    add_index :pe_feedback_accepted_fields, :pe_process_id
  end
end
