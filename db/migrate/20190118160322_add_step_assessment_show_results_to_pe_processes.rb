class AddStepAssessmentShowResultsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_assessment_show_results, :boolean, default: true
  end
end
