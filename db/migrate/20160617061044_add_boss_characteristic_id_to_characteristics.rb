class AddBossCharacteristicIdToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :boss_characteristic_id, :integer
    add_index :characteristics, :boss_characteristic_id
  end
end
