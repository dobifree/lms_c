class CreatePeElements < ActiveRecord::Migration
  def change
    create_table :pe_elements do |t|
      t.string :name
      t.references :pe_evaluation
      t.integer :pe_element_id
      t.boolean :assessed, default: true
      t.boolean :visible, default: true
      t.integer :assessment_method
      t.integer :calculus_method
      t.integer :max_number
      t.integer :min_number
      t.integer :max_weight
      t.integer :min_weight
      t.integer :weight_sum
      t.integer :element_def_by
      t.string :element_def_by_rol
      t.integer :assessment_method_def_by
      t.string :assessment_method_def_by_rol

      t.timestamps
    end
    add_index :pe_elements, :pe_evaluation_id
    add_index :pe_elements, :pe_element_id
  end
end
