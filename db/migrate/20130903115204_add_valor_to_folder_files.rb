class AddValorToFolderFiles < ActiveRecord::Migration
  def change
    add_column :folder_files, :valor, :float, default: 0
  end
end
