class AddNumeroPreguntasToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :numero_preguntas, :integer
  end
end
