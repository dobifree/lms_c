class AddAttachmentCodeToSelApplySpecificAnswers < ActiveRecord::Migration
  def change
    add_column :sel_applicant_specific_answers, :attachment_code, :string
  end
end
