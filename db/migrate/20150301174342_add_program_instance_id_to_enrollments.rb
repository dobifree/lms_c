class AddProgramInstanceIdToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :program_instance_id, :integer
    add_index :enrollments, :program_instance_id
  end
end
