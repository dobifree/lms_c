class CreateHrProcessAssessmentQuas < ActiveRecord::Migration
  def change
    create_table :hr_process_assessment_quas do |t|
      t.references :hr_process_user
      t.references :hr_process_quadrant

      t.timestamps
    end
    add_index :hr_process_assessment_quas, :hr_process_user_id
    add_index :hr_process_assessment_quas, :hr_process_quadrant_id
  end
end
