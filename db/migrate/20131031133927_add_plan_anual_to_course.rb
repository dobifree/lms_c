class AddPlanAnualToCourse < ActiveRecord::Migration
  def change
    add_column :courses, :plan_anual, :boolean, default: false
  end
end
