class AddEvalCliPesoToHrProcessDimensionE < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :eval_cli_peso, :integer, default: 0
  end
end
