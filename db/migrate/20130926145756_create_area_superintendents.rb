class CreateAreaSuperintendents < ActiveRecord::Migration
  def change
    create_table :area_superintendents do |t|
      t.references :planning_process_company_unit
      t.references :company_unit_area
      t.references :user

      t.timestamps
    end
    add_index :area_superintendents, :planning_process_company_unit_id
    add_index :area_superintendents, :company_unit_area_id
    add_index :area_superintendents, :user_id
  end
end
