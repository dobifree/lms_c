class AddHasComment2ToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :has_comment_2, :boolean, default: false
  end
end
