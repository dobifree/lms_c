class CreatePlanningProcessCompanyUnitAreaCorrections < ActiveRecord::Migration
  def change
    create_table :planning_process_company_unit_area_corrections do |t|
      t.text :correccion
      t.boolean :revisado, default: false
      t.references :planning_process_company_unit
      t.references :company_unit_area

      t.timestamps
    end
    add_index :planning_process_company_unit_area_corrections, :planning_process_company_unit_id, name: 'ppcuac_ppcui'
    add_index :planning_process_company_unit_area_corrections, :company_unit_area_id, name: 'ppcuac_cuai'
  end
end
