class CreatePeAlternatives < ActiveRecord::Migration
  def change
    create_table :pe_alternatives do |t|
      t.text :description
      t.integer :value
      t.references :pe_question

      t.timestamps
    end
    add_index :pe_alternatives, :pe_question_id
  end
end
