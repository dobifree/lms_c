class AddRegisteredAndDeactivatedColumnsToHeCtModulePrivilages < ActiveRecord::Migration
  def change
    add_column :he_ct_module_privilages, :registered_at, :datetime
    add_column :he_ct_module_privilages, :registered_by_user_id, :integer
    add_column :he_ct_module_privilages, :deactivated_at, :datetime
    add_column :he_ct_module_privilages, :deactivated_by_user_id, :integer
    add_column :he_ct_module_privilages, :active, :boolean, :deafult => true

    add_index :he_ct_module_privilages, :registered_by_user_id
    add_index :he_ct_module_privilages, :deactivated_by_user_id
  end
end
