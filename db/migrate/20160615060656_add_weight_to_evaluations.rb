class AddWeightToEvaluations < ActiveRecord::Migration
  def change
    add_column :evaluations, :weight, :integer, default: 1
  end
end
