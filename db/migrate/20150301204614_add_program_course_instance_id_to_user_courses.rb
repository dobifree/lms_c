class AddProgramCourseInstanceIdToUserCourses < ActiveRecord::Migration
  def change
    add_column :user_courses, :program_course_instance_id, :integer
    add_index :user_courses, :program_course_instance_id
  end
end
