class AddFieldsToLogSystems < ActiveRecord::Migration
  def change
    add_column :log_systems, :request_raw, :text
    add_column :log_systems, :session_raw, :text
    add_column :log_systems, :server_name, :string
    add_column :log_systems, :remote_ip, :string
    add_column :log_systems, :remote_host, :string
    add_column :log_systems, :path_info, :string
    add_column :log_systems, :request_method, :string
    add_column :log_systems, :original_url, :string
    add_column :log_systems, :url_referer, :string
    add_column :log_systems, :request_xhr, :boolean, default: false
    add_column :log_systems, :commit_id, :string
    add_column :log_systems, :commit_deployed_at, :datetime
  end
end
