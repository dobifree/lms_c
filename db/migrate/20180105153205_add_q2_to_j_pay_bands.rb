class AddQ2ToJPayBands < ActiveRecord::Migration
  def change
    add_column :j_pay_bands, :q2, :decimal, :precision => 15, :scale => 4
  end
end
