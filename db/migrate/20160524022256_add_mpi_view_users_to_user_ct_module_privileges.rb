class AddMpiViewUsersToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :mpi_view_users, :boolean, default: false
  end
end
