class AddAllowPauseToPollProcess < ActiveRecord::Migration
  def change
    add_column :poll_processes, :allow_pause, :boolean, :default => false
  end
end
