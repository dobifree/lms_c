class AddConfigurableFieldToSelCharacteristic < ActiveRecord::Migration
  def change
    add_column :sel_characteristics, :configurable, :boolean, :default => false
  end
end
