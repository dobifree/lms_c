class CreateSelNotificationTemplates < ActiveRecord::Migration
  def change
    create_table :sel_notification_templates do |t|
      t.integer :event_id
      t.string :name
      t.text :description
      t.boolean :active, default: false
      t.string :to
      t.string :cc
      t.string :subject
      t.text :body

      t.timestamps
    end

    add_index :sel_notification_templates, :event_id
  end
end
