class CreateBrgManageRelations < ActiveRecord::Migration
  def change
    create_table :brg_manage_relations do |t|
      t.integer :manager_id
      t.integer :managee_id
      t.integer :group_to_manage_id
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :brg_manage_relations, :manager_id
    add_index :brg_manage_relations, :managee_id
    add_index :brg_manage_relations, :group_to_manage_id
    add_index :brg_manage_relations, :deactivated_by_user_id
    add_index :brg_manage_relations, :registered_by_user_id
  end
end