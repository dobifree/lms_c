class CreatePeRels < ActiveRecord::Migration
  def change
    create_table :pe_rels do |t|
      t.references :pe_process
      t.integer :rel
      t.string :alias
      t.string :alias_reverse
      t.string :alias_plural
      t.string :alias_reverse_plural

      t.timestamps
    end
    add_index :pe_rels, :pe_process_id
  end
end
