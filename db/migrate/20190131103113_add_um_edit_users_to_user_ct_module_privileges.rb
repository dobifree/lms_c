class AddUmEditUsersToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :um_edit_users, :boolean, default: false
  end
end
