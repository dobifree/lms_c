class AddHeCtModulePrivilagesIdsToRegistersValidators < ActiveRecord::Migration
  def change
    add_column :sc_user_to_registers,:he_ct_module_privilage_id, :integer
    add_column :sc_user_to_validates,:he_ct_module_privilage_id, :integer

    add_index :sc_user_to_registers, :he_ct_module_privilage_id
    add_index :sc_user_to_validates, :he_ct_module_privilage_id
  end
end
