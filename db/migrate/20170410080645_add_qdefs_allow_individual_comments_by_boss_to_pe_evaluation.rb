class AddQdefsAllowIndividualCommentsByBossToPeEvaluation < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :qdefs_allow_individual_comments_by_boss, :boolean, default: false
  end
end
