class CreateTrackingFormRejects < ActiveRecord::Migration
  def change
    create_table :tracking_form_rejects do |t|
      t.references :tracking_form_log
      t.text :comment
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :tracking_form_rejects, :tracking_form_log_id
    add_index :tracking_form_rejects, :registered_by_user_id
  end
end
