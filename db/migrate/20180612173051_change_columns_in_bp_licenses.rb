class ChangeColumnsInBpLicenses < ActiveRecord::Migration
  def change
    remove_column :bp_licenses, :straight_time
    remove_column :bp_licenses, :days_between_days
    add_column :bp_licenses, :partially_taken, :boolean, default: false
    add_column :bp_licenses, :min_by_request, :integer
    add_column :bp_licenses, :max_by_request, :integer
    add_column :bp_licenses, :whenever, :boolean, default: true
    add_column :bp_licenses, :days_until, :integer
  end
end
