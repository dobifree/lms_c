class AddFechaInicioToUserCourseEvaluationQuestions < ActiveRecord::Migration
  def change
    add_column :user_course_evaluation_questions, :fecha_inicio, :datetime
  end
end
