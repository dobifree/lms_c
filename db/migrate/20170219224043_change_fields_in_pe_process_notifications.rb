class ChangeFieldsInPeProcessNotifications < ActiveRecord::Migration
  def change
    rename_column :pe_process_notifications, :step_feedback_accepted_subject, :step_feedback_accepted_ask_to_accept_subject
    rename_column :pe_process_notifications, :step_feedback_accepted_body, :step_feedback_accepted_ask_to_accept_body
  end
end
