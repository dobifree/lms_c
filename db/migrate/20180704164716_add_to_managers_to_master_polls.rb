class AddToManagersToMasterPolls < ActiveRecord::Migration
  def change
    add_column :master_polls, :to_managers, :boolean, :default => false
  end
end
