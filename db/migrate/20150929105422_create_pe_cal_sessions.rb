class CreatePeCalSessions < ActiveRecord::Migration
  def change
    create_table :pe_cal_sessions do |t|
      t.references :pe_process
      t.text :description
      t.datetime :from_date
      t.datetime :to_date
      t.boolean :active, default: true

      t.timestamps
    end
    add_index :pe_cal_sessions, :pe_process_id
  end
end
