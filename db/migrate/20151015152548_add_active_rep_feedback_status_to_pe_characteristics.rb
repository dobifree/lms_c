class AddActiveRepFeedbackStatusToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_rep_feedback_status, :boolean, default: false
  end
end
