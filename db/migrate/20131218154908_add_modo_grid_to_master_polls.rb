class AddModoGridToMasterPolls < ActiveRecord::Migration
  def change
    add_column :master_polls, :modo_grid, :boolean, default: false
  end
end
