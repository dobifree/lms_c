class CreateLogCtModuleUms < ActiveRecord::Migration
  def change
    create_table :log_ct_module_ums do |t|
      t.string :task
      t.datetime :performed_at
      t.references :user
      t.text :description

      t.timestamps
    end
    add_index :log_ct_module_ums, :user_id
  end
end
