class AddAprobadoAreaToDncs < ActiveRecord::Migration
  def change
    add_column :dncs, :aprobado_area, :boolean, default: false
  end
end
