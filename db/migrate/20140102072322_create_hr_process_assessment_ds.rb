class CreateHrProcessAssessmentDs < ActiveRecord::Migration
  def change
    create_table :hr_process_assessment_ds do |t|
      t.references :hr_process_dimension
      t.references :hr_process_user
      t.float :resultado_porcentaje
      t.datetime :fecha

      t.timestamps
    end
    add_index :hr_process_assessment_ds, :hr_process_dimension_id
    add_index :hr_process_assessment_ds, :hr_process_user_id
  end
end
