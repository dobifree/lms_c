class AddProgramCourseInstanceIdToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :program_course_instance_id, :integer
  end
end
