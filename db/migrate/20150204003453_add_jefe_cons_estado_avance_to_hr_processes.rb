class AddJefeConsEstadoAvanceToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :jefe_cons_estado_avance, :boolean, default: false
  end
end
