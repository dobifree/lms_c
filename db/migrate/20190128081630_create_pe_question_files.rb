class CreatePeQuestionFiles < ActiveRecord::Migration
  def change
    create_table :pe_question_files do |t|
      t.string :description
      t.string :file_name
      t.references :pe_question

      t.timestamps
    end
    add_index :pe_question_files, :pe_question_id
  end
end
