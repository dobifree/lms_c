class CreateUserDeleteReasons < ActiveRecord::Migration
  def change
    create_table :user_delete_reasons do |t|
      t.integer :position
      t.string :description

      t.timestamps
    end
  end
end
