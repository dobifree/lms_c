class CreateKpiDashboardGuests < ActiveRecord::Migration
  def change
    create_table :kpi_dashboard_guests do |t|
      t.references :kpi_dashboard
      t.references :user

      t.timestamps
    end
    add_index :kpi_dashboard_guests, :kpi_dashboard_id
    add_index :kpi_dashboard_guests, :user_id
  end
end
