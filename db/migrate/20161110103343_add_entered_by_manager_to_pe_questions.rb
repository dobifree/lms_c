class AddEnteredByManagerToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :entered_by_manager, :boolean, default: false
  end
end
