class AddSelect2listBooleanToSelReqItems < ActiveRecord::Migration
  def change
    add_column :sel_req_items, :select2_list, :boolean, :default => false
  end
end
