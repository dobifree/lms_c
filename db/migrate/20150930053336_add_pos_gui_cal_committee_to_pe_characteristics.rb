class AddPosGuiCalCommitteeToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_gui_cal_committee, :integer
    add_index :pe_characteristics, :pos_gui_cal_committee
  end
end
