class CreateCompanyUnitAreaManagers < ActiveRecord::Migration
  def change
    create_table :company_unit_area_managers do |t|
      t.references :planning_process_company_unit
      t.references :company_unit_area
      t.references :user

      t.timestamps
    end
    add_index :company_unit_area_managers, :planning_process_company_unit_id, name: 'cuam_planning_process_company_unit_id'
    add_index :company_unit_area_managers, :company_unit_area_id, name: 'cuam_company_unit_area_id'
    add_index :company_unit_area_managers, :user_id, name: 'cuam_user_id'
  end
end
