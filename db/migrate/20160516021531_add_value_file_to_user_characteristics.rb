class AddValueFileToUserCharacteristics < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :value_file, :string
  end
end
