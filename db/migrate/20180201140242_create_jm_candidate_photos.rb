class CreateJmCandidatePhotos < ActiveRecord::Migration
  def change
    create_table :jm_candidate_photos do |t|
      t.references :jm_candidate
      t.string :file_extension
      t.string :crypted_name

      t.timestamps
    end
    add_index :jm_candidate_photos, :jm_candidate_id
  end
end
