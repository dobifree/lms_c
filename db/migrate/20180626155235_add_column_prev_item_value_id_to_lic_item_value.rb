class AddColumnPrevItemValueIdToLicItemValue < ActiveRecord::Migration
  def change
    add_column :lic_item_values, :prev_lic_item_value_id, :integer
    add_column :lic_item_values, :next_lic_item_value_id, :integer

    add_index :lic_item_values, :prev_lic_item_value_id, name: 'prev_lic_item_value_id'
    add_index :lic_item_values, :next_lic_item_value_id, name: 'next_lic_item_value_id'
  end
end
