class AddShowQuienEvaluoToHrProcess < ActiveRecord::Migration
  def change
    add_column :hr_processes, :show_quien_evaluo, :boolean, default: true
  end
end
