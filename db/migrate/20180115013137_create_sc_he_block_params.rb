class CreateScHeBlockParams < ActiveRecord::Migration
  def change
    create_table :sc_he_block_params do |t|
      t.integer :entry
      t.integer :entry_min
      t.integer :entry_max
      t.boolean :entry_include_min, :default => true
      t.integer :exit
      t.integer :exit_min
      t.integer :exit_max
      t.boolean :exit_include_min, :default => true
      t.references :sc_block

      t.timestamps
    end
    add_index :sc_he_block_params, :sc_block_id
  end
end
