class RemoveRecurrenceConfigFieldFromElecProcess < ActiveRecord::Migration
  def change
    remove_columns :elec_processes, :recurrence_pattern
    remove_columns :elec_processes, :recurrence_period_name
    remove_columns :elec_processes, :recurrence_period_correction_factor
  end

end
