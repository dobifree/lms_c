class AddUpperLimitToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :upper_limit, :float
  end
end
