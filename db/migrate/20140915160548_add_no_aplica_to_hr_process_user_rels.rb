class AddNoAplicaToHrProcessUserRels < ActiveRecord::Migration
  def change
    add_column :hr_process_user_rels, :no_aplica, :boolean, default: false
  end
end
