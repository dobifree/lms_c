class AddExtraInfoJobs < ActiveRecord::Migration
  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :jobs_extra_info
      add_column :companies, :jobs_extra_info, :boolean, default: true
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end
end
