class CreateElecCharacteristics < ActiveRecord::Migration
  def change
    create_table :elec_characteristics do |t|
      t.references :elec_process
      t.references :characteristic
      t.boolean :used_to_group_by
      t.boolean :can_be_used_in_reports
      t.integer :can_be_used_in_reports_position

      t.timestamps
    end
    add_index :elec_characteristics, :elec_process_id
    add_index :elec_characteristics, :characteristic_id
  end
end
