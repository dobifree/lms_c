class AddEvalJefeToHrProcessDimensionEs < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :eval_jefe, :boolean, default: false
  end
end
