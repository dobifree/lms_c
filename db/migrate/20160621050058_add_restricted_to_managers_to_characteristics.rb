class AddRestrictedToManagersToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :restricted_to_managers, :boolean, default: false
  end
end
