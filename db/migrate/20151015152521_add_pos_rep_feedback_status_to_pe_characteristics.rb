class AddPosRepFeedbackStatusToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_rep_feedback_status, :integer
    add_index :pe_characteristics, :pos_rep_feedback_status
  end
end
