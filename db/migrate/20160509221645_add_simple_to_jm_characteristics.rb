class AddSimpleToJmCharacteristics < ActiveRecord::Migration
  def change
    add_column :jm_characteristics, :simple, :boolean, default: true
  end
end
