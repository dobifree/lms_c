class AddAplicaEvaluacionToHrEvaluationTypeElements < ActiveRecord::Migration
  def change
    add_column :hr_evaluation_type_elements, :aplica_evaluacion, :boolean, default: true
  end
end
