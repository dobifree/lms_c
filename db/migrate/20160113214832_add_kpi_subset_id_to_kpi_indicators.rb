class AddKpiSubsetIdToKpiIndicators < ActiveRecord::Migration
  def change
    add_column :kpi_indicators, :kpi_subset_id, :integer
    add_index :kpi_indicators, :kpi_subset_id
  end
end
