class AddUserDefineIdToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :user_define_id, :integer
    add_index :brc_members, :user_define_id
  end
end
