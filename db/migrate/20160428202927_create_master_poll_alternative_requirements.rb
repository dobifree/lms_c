class CreateMasterPollAlternativeRequirements < ActiveRecord::Migration
  def change
    create_table :master_poll_alternative_requirements do |t|
      t.references :master_poll_alternative
      t.references :master_poll_question

      t.timestamps
    end
    add_index :master_poll_alternative_requirements, :master_poll_alternative_id, :name => 'mpa_requirements_master_poll_alternative_id'
    add_index :master_poll_alternative_requirements, :master_poll_question_id, :name => 'mpa_requirements_master_poll_question_id'
  end
end
