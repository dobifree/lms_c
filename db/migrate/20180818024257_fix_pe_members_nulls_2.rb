class FixPeMembersNulls2 < ActiveRecord::Migration
  def change
    ActiveRecord::Base.connection.execute "update pe_members set step_assessment = 0 where step_assessment IS NULL"
    ActiveRecord::Base.connection.execute "update pe_members set step_calibration = 0 where step_calibration IS NULL"
    ActiveRecord::Base.connection.execute "update pe_members set step_feedback = 0 where step_feedback IS NULL"
    ActiveRecord::Base.connection.execute "update pe_members set step_feedback_accepted = 0 where step_feedback_accepted IS NULL"
  end
end
