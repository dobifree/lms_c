class AddReadyToGoToBrgBonus < ActiveRecord::Migration
  def change
    add_column :brg_bonus, :ready_to_go, :boolean, default: false
    add_column :brg_bonus, :ready_to_go_at, :datetime
  end
end
