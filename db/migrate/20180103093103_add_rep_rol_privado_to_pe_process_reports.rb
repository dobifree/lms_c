class AddRepRolPrivadoToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_rol_privado, :boolean, default: false
  end
end
