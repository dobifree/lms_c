class CreateScSchedules < ActiveRecord::Migration
  def change
    create_table :sc_schedules do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
