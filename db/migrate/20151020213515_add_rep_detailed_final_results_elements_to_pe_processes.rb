class AddRepDetailedFinalResultsElementsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :rep_detailed_final_results_elements, :string
  end
end
