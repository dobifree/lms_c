class AddPeListItemIdToPeMemberFeedbacks < ActiveRecord::Migration
  def change
    add_column :pe_member_feedbacks, :list_item_id, :integer
    add_index :pe_member_feedbacks, :list_item_id
  end
end
