class CreateElecProcesses < ActiveRecord::Migration
  def change
    create_table :elec_processes do |t|
      t.string :name
      t.text :description
      t.boolean :active
      t.boolean :to_everyone
      t.integer :max_votes
      t.boolean :self_vote
      t.integer :rounds
      t.boolean :recurrent
      t.string :recurrence_pattern
      t.string :recurrence_period_name
      t.integer :recurrence_period_correction_factor

      t.timestamps
    end
  end
end
