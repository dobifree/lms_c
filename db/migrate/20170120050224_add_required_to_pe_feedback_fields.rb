class AddRequiredToPeFeedbackFields < ActiveRecord::Migration
  def change
    add_column :pe_feedback_fields, :required, :boolean, default: true
  end
end
