class AddColumnValueDateToLicItemValue < ActiveRecord::Migration
  def change
    add_column :lic_item_values, :value_date, :date
  end
end
