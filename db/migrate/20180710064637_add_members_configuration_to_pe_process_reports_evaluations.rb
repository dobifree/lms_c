class AddMembersConfigurationToPeProcessReportsEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_process_reports_evaluations, :members_configuration, :boolean
  end
end
