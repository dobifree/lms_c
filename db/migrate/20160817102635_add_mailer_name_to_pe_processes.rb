class AddMailerNameToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :mailer_name, :string
  end
end
