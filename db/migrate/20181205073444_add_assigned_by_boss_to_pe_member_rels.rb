class AddAssignedByBossToPeMemberRels < ActiveRecord::Migration
  def change
    add_column :pe_member_rels, :assigned_by_boss, :boolean, default: false
  end
end
