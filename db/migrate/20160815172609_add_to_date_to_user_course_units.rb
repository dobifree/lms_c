class AddToDateToUserCourseUnits < ActiveRecord::Migration
  def change
    add_column :user_course_units, :to_date, :datetime
  end
end
