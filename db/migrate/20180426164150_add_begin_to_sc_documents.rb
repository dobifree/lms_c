class AddBeginToScDocuments < ActiveRecord::Migration
  def change
    add_column :sc_documentations, :begin, :date
  end
end
