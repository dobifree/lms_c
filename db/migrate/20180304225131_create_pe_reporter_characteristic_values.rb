class CreatePeReporterCharacteristicValues < ActiveRecord::Migration
  def change
    create_table :pe_reporter_characteristic_values do |t|
      t.references :pe_reporter
      t.references :pe_characteristic
      t.string :value

      t.timestamps
    end
    add_index :pe_reporter_characteristic_values, :pe_reporter_id
    add_index :pe_reporter_characteristic_values, :pe_characteristic_id
  end
end
