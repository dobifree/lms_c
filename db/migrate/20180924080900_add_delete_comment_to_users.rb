class AddDeleteCommentToUsers < ActiveRecord::Migration
  def change
    add_column :users, :delete_comment, :text
  end
end
