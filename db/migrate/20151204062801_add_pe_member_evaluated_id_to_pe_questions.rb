class AddPeMemberEvaluatedIdToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :pe_member_evaluated_id, :integer
    add_index :pe_questions, :pe_member_evaluated_id
  end
end
