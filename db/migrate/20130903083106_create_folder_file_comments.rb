class CreateFolderFileComments < ActiveRecord::Migration
  def change
    create_table :folder_file_comments do |t|
      t.references :folder_file
      t.references :user
      t.datetime :fecha
      t.text :comentario

      t.timestamps
    end
    add_index :folder_file_comments, :folder_file_id
    add_index :folder_file_comments, :user_id
  end
end
