class CreateUserJJobs < ActiveRecord::Migration
  def change
    create_table :user_j_jobs do |t|
      t.references :user
      t.references :j_job
      t.date :from_date
      t.date :to_date

      t.timestamps
    end
    add_index :user_j_jobs, :user_id
    add_index :user_j_jobs, :j_job_id
  end
end
