class CreateKpiDashboardManagers < ActiveRecord::Migration
  def change
    create_table :kpi_dashboard_managers do |t|
      t.references :kpi_dashboard
      t.references :user

      t.timestamps
    end
    add_index :kpi_dashboard_managers, :kpi_dashboard_id
    add_index :kpi_dashboard_managers, :user_id
  end
end
