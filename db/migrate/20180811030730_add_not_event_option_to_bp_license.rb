class AddNotEventOptionToBpLicense < ActiveRecord::Migration
  def change
    add_column :bp_licenses, :with_event, :boolean, default: true
  end
end
