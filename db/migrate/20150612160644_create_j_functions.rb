class CreateJFunctions < ActiveRecord::Migration
  def change
    create_table :j_functions do |t|
      t.text :description
      t.boolean :main_function
      t.references :j_job

      t.timestamps
    end
    add_index :j_functions, :j_job_id
  end
end
