class CreateKpiDashboards < ActiveRecord::Migration
  def change
    create_table :kpi_dashboards do |t|
      t.string :name
      t.string :period
      t.date :from
      t.date :to
      t.string :frequency

      t.timestamps
    end
  end
end
