class CreateHolidays < ActiveRecord::Migration
  def change
    create_table :holidays do |t|
      t.references :user
      t.date :hol_date
      t.integer :hol_type
      t.boolean :active, default: true
      t.datetime :deactivated_at
      t.integer :deactivated_by_user_id
      t.datetime :registered_at
      t.integer :registered_by_user_id

      t.timestamps
    end
    add_index :holidays, :user_id
    add_index :holidays, :deactivated_by_user_id, name: 'holidays_deact_user_id'
    add_index :holidays, :registered_by_user_id, name: 'holidays_regist_user_id'
  end
end
