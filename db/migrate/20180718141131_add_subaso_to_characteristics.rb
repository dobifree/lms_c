class AddSubasoToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :subaso, :boolean, default: false
  end
end
