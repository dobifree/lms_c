class AddJefeBibliotecaToUsers < ActiveRecord::Migration
  def change
    add_column :users, :jefe_biblioteca, :boolean, default: false
  end
end
