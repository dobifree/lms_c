class AddPrevAccumAndNExt < ActiveRecord::Migration
  def change
    add_column :vac_accumulateds, :latest, :boolean, default: false
  end
end