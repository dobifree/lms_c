class AddRevisadoToDncAreaCorrections < ActiveRecord::Migration
  def change
    add_column :dnc_area_corrections, :revisado, :boolean, default: false
  end
end
