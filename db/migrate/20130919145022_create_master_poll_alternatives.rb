class CreateMasterPollAlternatives < ActiveRecord::Migration
  def change
    create_table :master_poll_alternatives do |t|
      t.integer :numero
      t.text :texto
      t.references :master_poll_question

      t.timestamps
    end
    add_index :master_poll_alternatives, :master_poll_question_id
  end
end
