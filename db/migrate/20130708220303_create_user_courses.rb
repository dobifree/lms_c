class CreateUserCourses < ActiveRecord::Migration
  def change
    create_table :user_courses do |t|
      t.references :enrollment
      t.references :program_course
      t.boolean :programado
      t.datetime :desde
      t.datetime :hasta
      t.boolean :limite_tiempo
      t.datetime :inicio
      t.datetime :fin
      t.integer :numero_oportunidad
      t.boolean :iniciado, default: false
      t.boolean :finalizado, default: false
      t.boolean :aprobado, default: false
      t.float :nota

      t.timestamps
    end
    add_index :user_courses, :enrollment_id
    add_index :user_courses, :program_course_id
  end
end
