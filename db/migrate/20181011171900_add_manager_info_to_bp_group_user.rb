class AddManagerInfoToBpGroupUser < ActiveRecord::Migration
  def change
    add_column :bp_groups_users, :registered_by_user_id, :integer
    add_column :bp_groups_users, :deactivated_by_user_id, :integer

    add_index :bp_groups_users, :deactivated_by_user_id
    add_index :bp_groups_users, :registered_by_user_id
  end
end
