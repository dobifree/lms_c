class AddColumnShowAzureRedirectionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :show_azure_landing_page, :boolean, default: true
  end
end
