class AddReferenceDateCharIdToScholarshipAppItem < ActiveRecord::Migration
  def change
    add_column :scholarship_application_items, :reference_char_id, :integer
  end
end
