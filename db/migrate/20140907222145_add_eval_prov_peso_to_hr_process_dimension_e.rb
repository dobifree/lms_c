class AddEvalProvPesoToHrProcessDimensionE < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :eval_prov_peso, :integer, default: 0
  end
end
