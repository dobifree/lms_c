class AddSkExternalCertificationToProgramCourseInstance < ActiveRecord::Migration
  def change
    add_column :program_course_instances, :sk_external_certification, :boolean
  end
end
