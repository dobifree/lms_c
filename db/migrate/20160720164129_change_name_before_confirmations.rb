class ChangeNameBeforeConfirmations < ActiveRecord::Migration
  def change
    rename_column :pe_question_validations, :before_confirmation, :before_accept
    rename_column :pe_definition_by_user_validators, :before_confirmation, :before_accept
  end
end
