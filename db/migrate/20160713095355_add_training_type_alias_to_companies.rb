class AddTrainingTypeAliasToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :training_type_alias, :string, default: 'Tipo de capacitación'
  end
end
