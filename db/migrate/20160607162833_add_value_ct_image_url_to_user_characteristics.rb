class AddValueCtImageUrlToUserCharacteristics < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :value_ct_image_url, :string
  end
end
