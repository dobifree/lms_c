class AddActiveRepConsolidatedFinalResultsToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :active_rep_consolidated_final_results, :boolean, default: false
  end
end
