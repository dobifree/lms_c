class AddAliasSubInversoPluToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :alias_sub_inverso_plu, :string, default: 'Supervisores'
  end
end
