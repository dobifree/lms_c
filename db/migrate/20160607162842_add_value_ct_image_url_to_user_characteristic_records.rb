class AddValueCtImageUrlToUserCharacteristicRecords < ActiveRecord::Migration
  def change
    add_column :user_characteristic_records, :value_ct_image_url, :string
  end
end
