class CreatePeDefinitionByUserValidators < ActiveRecord::Migration
  def change
    create_table :pe_definition_by_user_validators do |t|
      t.references :pe_member
      t.references :pe_evaluation
      t.integer :step_number
      t.boolean :before_confirmation
      t.references :pe_process
      t.integer :validator_id
      t.boolean :checked, default: false
      t.boolean :validated, default: false
      t.text :validation_comment
      t.datetime :validated_at

      t.timestamps
    end
    add_index :pe_definition_by_user_validators, :pe_member_id
    add_index :pe_definition_by_user_validators, :pe_evaluation_id
    add_index :pe_definition_by_user_validators, :pe_process_id
    add_index :pe_definition_by_user_validators, :validator_id
  end
end
