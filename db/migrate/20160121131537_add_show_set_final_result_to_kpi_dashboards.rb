class AddShowSetFinalResultToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :show_set_final_result, :boolean, default: false
  end
end
