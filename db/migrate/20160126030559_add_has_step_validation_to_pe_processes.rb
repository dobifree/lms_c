class AddHasStepValidationToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :has_step_validation, :boolean, default: false
  end
end
