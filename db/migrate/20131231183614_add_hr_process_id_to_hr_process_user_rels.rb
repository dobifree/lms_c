class AddHrProcessIdToHrProcessUserRels < ActiveRecord::Migration
  def change
    add_column :hr_process_user_rels, :hr_process_id, :integer
  end
end
