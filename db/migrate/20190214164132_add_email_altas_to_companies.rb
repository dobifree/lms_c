class AddEmailAltasToCompanies < ActiveRecord::Migration
  def connection
    @connection = Company.connection
  end

  def change
    unless column_exists? :companies, :email_altas
      add_column :companies, :email_altas, :string
    end
    @connection = ActiveRecord::Base.establish_connection("#{Rails.env}").connection
  end

end
