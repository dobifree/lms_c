class CreateSelProcessAttendants < ActiveRecord::Migration
  def change
    create_table :sel_process_attendants do |t|
      t.references :sel_process_step
      t.references :user
      t.boolean :role
      t.boolean :automatic_assign, default: false

      t.timestamps
    end
    add_index :sel_process_attendants, :sel_process_step_id
    add_index :sel_process_attendants, :user_id
  end
end
