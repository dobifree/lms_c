class CreateBannerCharacteristics < ActiveRecord::Migration
  def change
    create_table :banner_characteristics do |t|
      t.references :banner
      t.references :characteristic
      t.text :match_value

      t.timestamps
    end
    add_index :banner_characteristics, :banner_id
    add_index :banner_characteristics, :characteristic_id
  end
end
