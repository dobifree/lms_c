class AddCharacteristicIdToSelReqItem < ActiveRecord::Migration
  def change
    add_column :sel_req_items, :characteristic_id, :integer
  end
end
