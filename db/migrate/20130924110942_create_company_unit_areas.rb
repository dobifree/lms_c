class CreateCompanyUnitAreas < ActiveRecord::Migration
  def change
    create_table :company_unit_areas do |t|
      t.string :nombre
      t.references :company_unit

      t.timestamps
    end
    add_index :company_unit_areas, :company_unit_id
  end
end
