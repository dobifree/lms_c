class AddDefaultValueScHeValue < ActiveRecord::Migration
  def change
    change_column_default :sc_user_processes, :he_total, 0
    change_column_default :sc_records, :status, 0
    change_column_default :he_ct_module_privilages, :active, true
  end
end

