class AddStatusChooseToBrcMembers < ActiveRecord::Migration
  def change
    add_column :brc_members, :status_choose, :boolean, default: false
  end
end
