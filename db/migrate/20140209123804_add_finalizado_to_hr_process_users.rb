class AddFinalizadoToHrProcessUsers < ActiveRecord::Migration
  def change
    add_column :hr_process_users, :finalizado, :boolean, default: false
  end
end
