class AddUnitAllowDigitsToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :unit_allow_digits, :boolean, default: true
  end
end
