class AddCommentDateToPeMemberFeedbacks < ActiveRecord::Migration
  def change
    add_column :pe_member_feedbacks, :comment_date, :date
  end
end
