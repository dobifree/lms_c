class AddAsistenciaMinimaToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :asistencia_minima, :integer, :default => 100
  end
end
