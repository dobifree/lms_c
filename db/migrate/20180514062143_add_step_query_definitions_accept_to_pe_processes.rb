class AddStepQueryDefinitionsAcceptToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_query_definitions_accept, :boolean, default: false
  end
end
