class AddTotalPositionsToJJobs < ActiveRecord::Migration
  def change
    add_column :j_jobs, :total_positions, :integer, :default => 1
  end
end
