class AddStepValidationUniqToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_validation_uniq, :boolean
  end
end
