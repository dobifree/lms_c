class ChangeRelationGroupGenRulesVacConfig < ActiveRecord::Migration
  def change
    add_column :bp_general_rules_vacations, :user_id, :integer
    add_index :bp_general_rules_vacations, :user_id
  end
end
