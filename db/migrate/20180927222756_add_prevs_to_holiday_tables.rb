class AddPrevsToHolidayTables < ActiveRecord::Migration
  def change
    add_column :holidays, :prev_holiday_id, :integer
    add_index :holidays, :prev_holiday_id

    add_column :holiday_users, :prev_hol_user_id, :integer
    add_index :holiday_users, :prev_hol_user_id
  end
end
