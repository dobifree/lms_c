class AddMaxNumberToPeQuestionModel < ActiveRecord::Migration
  def change
    add_column :pe_question_models, :max_number, :integer
  end
end
