class CreateBpEvents < ActiveRecord::Migration
  def change
    create_table :bp_events do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
