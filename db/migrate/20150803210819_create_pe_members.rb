class CreatePeMembers < ActiveRecord::Migration
  def change
    create_table :pe_members do |t|
      t.references :pe_process
      t.references :user
      t.boolean :is_evaluated, default: false
      t.boolean :is_evaluator, default: false
      t.boolean :step_assessment, default: false
      t.datetime :step_assessment_date
      t.boolean :step_calibration, default: false
      t.datetime :step_calibration_date
      t.boolean :step_feedback, default: false
      t.datetime :step_feedback_date
      t.boolean :step_feedback_accepted, default: false
      t.datetime :step_feedback_accepted_date
      t.text :step_feedback_text

      t.timestamps
    end
    add_index :pe_members, :pe_process_id
    add_index :pe_members, :user_id
  end
end
