class CreateNotificationCharacteristics < ActiveRecord::Migration
  def change
    create_table :notification_characteristics do |t|
      t.references :notification
      t.references :characteristic
      t.text :match_value

      t.timestamps
    end
    add_index :notification_characteristics, :notification_id
    add_index :notification_characteristics, :characteristic_id
  end
end
