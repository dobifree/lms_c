class SecurityAddActiveDirectoryId < ActiveRecord::Migration
  def change
    add_column :users, :active_directory_id, :string
    add_column :log_logins, :active_directory_json, :text
  end
end
