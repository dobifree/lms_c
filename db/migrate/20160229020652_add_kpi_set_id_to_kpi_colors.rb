class AddKpiSetIdToKpiColors < ActiveRecord::Migration
  def change
    add_column :kpi_colors, :kpi_set_id, :integer
    add_index :kpi_colors, :kpi_set_id
  end
end
