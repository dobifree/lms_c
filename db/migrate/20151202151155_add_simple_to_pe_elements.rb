class AddSimpleToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :simple, :boolean, default: true
  end
end
