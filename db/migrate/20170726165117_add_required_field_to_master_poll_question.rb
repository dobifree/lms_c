class AddRequiredFieldToMasterPollQuestion < ActiveRecord::Migration
  def change
    add_column :master_poll_questions, :required, :boolean, :default => false
  end
end
