class CreatePeProcessTutorials < ActiveRecord::Migration
  def change
    create_table :pe_process_tutorials do |t|
      t.references :pe_process
      t.references :tutorial
      t.string :name
      t.boolean :active

      t.timestamps
    end
    add_index :pe_process_tutorials, :pe_process_id
    add_index :pe_process_tutorials, :tutorial_id
  end
end
