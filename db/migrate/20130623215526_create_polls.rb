class CreatePolls < ActiveRecord::Migration
  def change
    create_table :polls do |t|
      t.string :nombre
      t.string :descripcion
      t.references :program
      t.references :level
      t.references :course

      t.timestamps
    end
    add_index :polls, :program_id
    add_index :polls, :level_id
    add_index :polls, :course_id
  end
end
