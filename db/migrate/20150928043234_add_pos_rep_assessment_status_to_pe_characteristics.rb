class AddPosRepAssessmentStatusToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_rep_assessment_status, :integer
    add_index :pe_characteristics, :pos_rep_assessment_status
  end
end
