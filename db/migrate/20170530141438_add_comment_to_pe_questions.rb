class AddCommentToPeQuestions < ActiveRecord::Migration
  def change
    add_column :pe_questions, :comment, :text
  end
end
