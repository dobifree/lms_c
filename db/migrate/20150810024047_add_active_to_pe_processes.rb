class AddActiveToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :active, :boolean, default: false
  end
end
