class AddBasicReportToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :basic_report, :boolean, default: false
  end
end
