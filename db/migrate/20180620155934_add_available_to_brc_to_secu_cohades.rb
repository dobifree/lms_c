class AddAvailableToBrcToSecuCohades < ActiveRecord::Migration
  def change
    add_column :secu_cohades, :available_to_brc, :boolean, default: false
  end
end
