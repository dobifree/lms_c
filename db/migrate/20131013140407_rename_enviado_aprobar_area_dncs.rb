class RenameEnviadoAprobarAreaDncs < ActiveRecord::Migration
  def change
    rename_column :dncs, :enviado_aprovar_area, :enviado_aprobar_area
  end
end
