class AddPosRepsConfigurationToPeCharacteristics < ActiveRecord::Migration
  def change
    add_column :pe_characteristics, :pos_reps_configuration, :integer
  end
end
