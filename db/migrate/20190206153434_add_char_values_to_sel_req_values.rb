class AddCharValuesToSelReqValues < ActiveRecord::Migration
  def change
    add_column :sel_requirement_values, :char_value_string, :string
    add_column :sel_requirement_values, :char_value_text, :text
    add_column :sel_requirement_values, :char_value_date, :date
    add_column :sel_requirement_values, :char_value_int, :integer
    add_column :sel_requirement_values, :char_value_float, :float
    add_column :sel_requirement_values, :char_value_id, :integer
    add_column :sel_requirement_values, :char_value_jjob_id, :integer
  end
end
