class AddTwoDimensionsToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :two_dimensions, :boolean, default: true
  end
end
