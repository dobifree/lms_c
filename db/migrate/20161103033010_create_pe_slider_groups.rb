class CreatePeSliderGroups < ActiveRecord::Migration
  def change
    create_table :pe_slider_groups do |t|
      t.integer :position
      t.string :name
      t.text :description
      t.string :text_color
      t.string :bg_color
      t.string :min_sign
      t.integer :min_value
      t.string :max_sign
      t.integer :max_value
      t.references :pe_evaluation

      t.timestamps
    end
    add_index :pe_slider_groups, :pe_evaluation_id
  end
end
