class ChangeColumnsPaidVacRuleRecord < ActiveRecord::Migration
  def change
    rename_column :vac_rule_records, :money_paid, :paid
    rename_column :vac_rule_records, :money_paid_at, :paid_at
    rename_column :vac_rule_records, :money_paid_by_user_id, :paid_by_user_id

    change_column_default :vac_rule_records, :paid, nil
  end
end
