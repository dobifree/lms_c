class AddTrackingStatusToPeProcessReportsEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_process_reports_evaluations, :tracking_status, :boolean, default: false
  end
end
