class AddPositionToPeMemberCompoundFeedbacks < ActiveRecord::Migration
  def change
    add_column :pe_member_compound_feedbacks, :position, :integer
    add_index :pe_member_compound_feedbacks, :position
  end
end
