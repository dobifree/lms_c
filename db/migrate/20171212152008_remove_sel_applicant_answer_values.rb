class RemoveSelApplicantAnswerValues < ActiveRecord::Migration
  def change
    drop_table :sel_applicant_answers
    drop_table :sel_applicant_values
  end
end
