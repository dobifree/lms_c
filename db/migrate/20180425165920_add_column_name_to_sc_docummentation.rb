class AddColumnNameToScDocummentation < ActiveRecord::Migration
  def change
    add_column :sc_documentations, :name, :string
    add_column :sc_documentations, :description, :text
  end
end
