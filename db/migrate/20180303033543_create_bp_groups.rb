class CreateBpGroups < ActiveRecord::Migration
  def change
    create_table :bp_groups do |t|
      t.string :name
      t.text :description
      t.boolean :active, :default => true
      t.datetime :registered_at
      t.integer :registered_by_admin_id
      t.datetime :deactivated_at
      t.integer :deactivated_by_admin_id

      t.timestamps
    end
    add_index :bp_groups, :registered_by_admin_id
    add_index :bp_groups, :deactivated_by_admin_id
  end
end
