class AddColumnsForPercentagesToBrgProcessAndBrgBonuses < ActiveRecord::Migration
  def change
    add_column :brg_processes, :brg_percentage_conversion_id, :integer
    add_index :brg_processes, :brg_percentage_conversion_id
  end
end
