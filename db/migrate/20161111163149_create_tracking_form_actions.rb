class CreateTrackingFormActions < ActiveRecord::Migration
  def change
    create_table :tracking_form_actions do |t|
      t.references :tracking_form
      t.integer :position
      t.string :name
      t.text :description
      t.string :label
      t.boolean :fill_form
      t.boolean :able_to_attendant
      t.boolean :able_to_subject
      t.boolean :require_comment
      t.string :label_comment

      t.timestamps
    end
    add_index :tracking_form_actions, :tracking_form_id
  end
end
