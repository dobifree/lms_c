class AddRegisteredManuallyToScScheduleUsers < ActiveRecord::Migration
  def change
    add_column :sc_schedule_users, :registered_manually, :boolean, :default => false
    add_column :sc_schedule_users, :deactivated_manually, :boolean, :default => false
  end
end
