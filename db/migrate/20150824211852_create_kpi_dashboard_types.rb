class CreateKpiDashboardTypes < ActiveRecord::Migration
  def change
    create_table :kpi_dashboard_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
