class CreateJmLists < ActiveRecord::Migration
  def change
    create_table :jm_lists do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
