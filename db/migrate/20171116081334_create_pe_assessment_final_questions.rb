class CreatePeAssessmentFinalQuestions < ActiveRecord::Migration
  def change
    create_table :pe_assessment_final_questions do |t|
      t.references :pe_question
      t.float :point
      t.float :percentage
      t.integer :pe_assessment_final_question_id
      t.references :pe_assessment_final_evaluation

      t.timestamps
    end
    add_index :pe_assessment_final_questions, :pe_question_id, :name => 'pafq_pe_question_id'
    add_index :pe_assessment_final_questions, :pe_assessment_final_evaluation_id, :name => 'pafq_pe_assessment_final_evaluation_id'
    add_index :pe_assessment_final_questions, :pe_assessment_final_question_id, :name => 'pafq_pe_assessment_final_question_id'
  end
end
