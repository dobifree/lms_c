class CreateHrEvaluationTypeElements < ActiveRecord::Migration
  def change
    create_table :hr_evaluation_type_elements do |t|
      t.string :nombre
      t.integer :nivel
      t.float :valor_minimo
      t.float :valor_maximo
      t.boolean :formula_promedio, default: true
      t.integer :peso_minimo
      t.integer :peso_maximo
      t.integer :suma_total_pesos
      t.boolean :plan_accion, default: false
      t.boolean :porcentaje_logro, default: false
      t.boolean :indicador_logro, default: false
      t.references :hr_evaluation_type

      t.timestamps
    end
    add_index :hr_evaluation_type_elements, :hr_evaluation_type_id
  end
end
