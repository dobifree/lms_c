class AddSecuCohadeIdToBrcProcesses < ActiveRecord::Migration
  def change
    add_column :brc_processes, :secu_cohade_id, :integer
    add_index :brc_processes, :secu_cohade_id
  end
end
