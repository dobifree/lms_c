class AddFieldsToSupportNoContinua < ActiveRecord::Migration
  def change
    add_column :sel_applicants, :excluded, :boolean, default: false
    add_column :sel_applicants, :excluded_at, :datetime
    add_column :sel_applicants, :excluded_by_user_id, :integer
    add_index :sel_applicants, :excluded_by_user_id
  end
end
