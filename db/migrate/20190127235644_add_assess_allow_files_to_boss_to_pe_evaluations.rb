class AddAssessAllowFilesToBossToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :assess_allow_files_to_boss, :boolean, default: false
  end
end
