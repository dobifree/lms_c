class AddTienePasoFeedbackModoToHrProcesses < ActiveRecord::Migration
  def change
    add_column :hr_processes, :tiene_paso_feedback_modo, :integer, default: 1
  end
end
