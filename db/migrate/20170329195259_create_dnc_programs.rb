class CreateDncPrograms < ActiveRecord::Migration
  def change
    create_table :dnc_programs do |t|
      t.string :name

      t.timestamps
    end
  end
end
