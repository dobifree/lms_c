class AddIsEvaluatorToPeAreas < ActiveRecord::Migration
  def change
    add_column :pe_areas, :is_evaluator, :boolean, default: false
  end
end
