class AddEvalCliToHrProcessDimensionE < ActiveRecord::Migration
  def change
    add_column :hr_process_dimension_es, :eval_cli, :boolean, default: false
  end
end
