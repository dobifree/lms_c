class CreateHrProcessUsers < ActiveRecord::Migration
  def change
    create_table :hr_process_users do |t|
      t.references :hr_process
      t.references :user
      t.references :hr_process_level

      t.timestamps
    end
    add_index :hr_process_users, :hr_process_id
    add_index :hr_process_users, :user_id
    add_index :hr_process_users, :hr_process_level_id
  end
end
