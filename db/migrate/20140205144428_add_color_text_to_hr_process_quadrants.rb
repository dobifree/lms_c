class AddColorTextToHrProcessQuadrants < ActiveRecord::Migration
  def change
    add_column :hr_process_quadrants, :color_text, :string
  end
end
