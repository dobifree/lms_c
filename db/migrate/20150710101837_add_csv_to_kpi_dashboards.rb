class AddCsvToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :csv, :boolean
  end
end
