class CreatePeMemberRelSharedComments < ActiveRecord::Migration
  def change
    create_table :pe_member_rel_shared_comments do |t|
      t.references :pe_member_rel
      t.text :comment
      t.datetime :registered_at

      t.timestamps
    end
    add_index :pe_member_rel_shared_comments, :pe_member_rel_id
  end
end
