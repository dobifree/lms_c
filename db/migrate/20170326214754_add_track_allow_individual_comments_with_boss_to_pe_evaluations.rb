class AddTrackAllowIndividualCommentsWithBossToPeEvaluations < ActiveRecord::Migration
  def change
    add_column :pe_evaluations, :track_allow_individual_comments_with_boss, :boolean, default: false
  end
end
