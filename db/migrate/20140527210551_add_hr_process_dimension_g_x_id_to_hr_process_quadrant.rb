class AddHrProcessDimensionGXIdToHrProcessQuadrant < ActiveRecord::Migration
  def change
    add_column :hr_process_quadrants, :hr_process_dimension_g_x_id, :integer
  end
end
