class AddMpiViewUsersTrainingHistoryToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :mpi_view_users_training_history, :boolean, default: false
  end
end
