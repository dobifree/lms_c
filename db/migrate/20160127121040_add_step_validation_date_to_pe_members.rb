class AddStepValidationDateToPeMembers < ActiveRecord::Migration
  def change
    add_column :pe_members, :step_validation_date, :datetime
  end
end
