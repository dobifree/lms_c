class AddLmsReportsToUserCtModulePrivileges < ActiveRecord::Migration
  def change
    add_column :user_ct_module_privileges, :lms_reports, :boolean, default: false
  end
end
