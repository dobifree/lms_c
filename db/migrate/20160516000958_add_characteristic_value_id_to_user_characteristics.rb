class AddCharacteristicValueIdToUserCharacteristics < ActiveRecord::Migration
  def change
    add_column :user_characteristics, :characteristic_value_id, :integer
    add_index :user_characteristics, :characteristic_value_id
  end
end
