class AddActiveToProgramInstances < ActiveRecord::Migration
  def change
    add_column :program_instances, :active, :boolean, default: true
  end
end
