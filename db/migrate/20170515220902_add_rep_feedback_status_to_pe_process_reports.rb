class AddRepFeedbackStatusToPeProcessReports < ActiveRecord::Migration
  def change
    add_column :pe_process_reports, :rep_feedback_status, :boolean, default: false
  end
end
