class AddWPToKpiDashboards < ActiveRecord::Migration
  def change
    add_column :kpi_dashboards, :w_p, :boolean, default: true
  end
end
