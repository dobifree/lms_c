class AddPositionRep3ToTrainingCharacteristics < ActiveRecord::Migration
  def change
    add_column :training_characteristics, :position_rep_3, :integer
  end
end
