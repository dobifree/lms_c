class CreateTrackingFormInstances < ActiveRecord::Migration
  def change
    create_table :tracking_form_instances do |t|
      t.references :tracking_form
      t.references :tracking_participant
      t.boolean :filled
      t.boolean :done

      t.timestamps
    end
    add_index :tracking_form_instances, :tracking_form_id
    add_index :tracking_form_instances, :tracking_participant_id
  end
end
