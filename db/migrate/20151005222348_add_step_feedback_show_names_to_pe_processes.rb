class AddStepFeedbackShowNamesToPeProcesses < ActiveRecord::Migration
  def change
    add_column :pe_processes, :step_feedback_show_names, :boolean
  end
end
