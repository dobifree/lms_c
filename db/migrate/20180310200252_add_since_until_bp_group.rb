class AddSinceUntilBpGroup < ActiveRecord::Migration
  def change
    add_column :bp_groups, :since, :datetime
    add_column :bp_groups, :until, :datetime
    add_column :bp_general_rules_vacations, :since, :datetime
    add_column :bp_general_rules_vacations, :until, :datetime
    add_column :bp_rule_vacations, :since, :datetime
    add_column :bp_rule_vacations, :until, :datetime
    add_column :bp_licenses, :since, :datetime
    add_column :bp_licenses, :until, :datetime

    remove_column :bp_groups, :active
    remove_column :bp_groups_users, :active
    remove_column :bp_general_rules_vacations, :active
    remove_column :bp_rule_vacations, :active
    remove_column :bp_condition_vacations, :active
    remove_column :bp_licenses, :active
  end
end
