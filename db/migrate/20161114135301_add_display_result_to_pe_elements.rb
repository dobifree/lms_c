class AddDisplayResultToPeElements < ActiveRecord::Migration
  def change
    add_column :pe_elements, :display_result, :boolean, default: false
  end
end
