class AddParamsToLogSystem < ActiveRecord::Migration
  def change
    add_column :log_systems, :params, :text
  end
end
