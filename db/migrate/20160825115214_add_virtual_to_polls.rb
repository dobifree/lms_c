class AddVirtualToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :virtual, :boolean, default: true
  end
end
