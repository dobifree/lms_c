class AddPlanAnualToMasterPolls < ActiveRecord::Migration
  def change
    add_column :master_polls, :plan_anual, :boolean, default: false
  end
end
