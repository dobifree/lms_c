class CreateDncTrainingImpacts < ActiveRecord::Migration
  def change
    create_table :dnc_training_impacts do |t|
      t.references :dnc
      t.references :training_impact_category
      t.references :training_impact_value

      t.timestamps
    end
    add_index :dnc_training_impacts, :dnc_id
    add_index :dnc_training_impacts, :training_impact_category_id
    add_index :dnc_training_impacts, :training_impact_value_id
  end
end
