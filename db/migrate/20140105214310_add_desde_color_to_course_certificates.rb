class AddDesdeColorToCourseCertificates < ActiveRecord::Migration
  def change
    add_column :course_certificates, :desde_color, :string, default: '000000'
  end
end
