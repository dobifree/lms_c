class AddFinalAppointmentDateToSelStepCandidate < ActiveRecord::Migration
  def change
    add_column :sel_step_candidates, :final_appointment_date, :datetime
  end
end
