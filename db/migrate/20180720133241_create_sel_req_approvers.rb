class CreateSelReqApprovers < ActiveRecord::Migration
  def change
    create_table :sel_req_approvers do |t|
      t.references :sel_req_process
      t.string :name
      t.integer :position
      t.boolean :mandatory
      t.integer :value
      t.boolean :conditions_rejection
      t.integer :prerequisite_position

      t.timestamps
    end
    add_index :sel_req_approvers, :sel_req_process_id
  end
end
