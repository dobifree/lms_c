class RemoveAnswerTypeOfMasterPollAlternative < ActiveRecord::Migration
  def change
   remove_column :master_poll_alternatives, :answer_type
  end
end
