LMSSys::Application.routes.draw do


  get "json_reports/index"
  match 'json_reports/index', to: 'json_reports#index', via: :get, as: 'json_reports_index'
  match 'json_reports/show/:user_id', to: 'json_reports#show', via: :get, as: 'json_reports_show'

  match 'user_sync_photo/update_photo', to: 'user_sync_photo#update_photo', via: :post, as: 'user_sync_photo_update'
  resources :bd_backups, only: [:index, :new, :create, :destroy]
  match 'bd_backups/:database/show_tables', to: 'bd_backups#get_tables_from_database', via: :post, as: 'bd_backups_get_tables_from_database'

  resources :brc_mails


  match 'single_sign_on', to: 'static_pages#single_sign_on', via: :post, as: 'single_sign_on'
  resources :user_delete_types


  resources :liq_items


  resources :liq_processes
  match 'liq_processes/:id/load_data_from_payroll', to: 'liq_processes#load_data_from_payroll', via: :post, as: 'liq_process_load_data_from_payroll'
  match 'liq_processes/:id/detail', to: 'liq_processes#show_detail', via: :get, as: 'liq_process_show_detail'
  match 'liq_processes/:id/clean_items', to: 'liq_processes#clean_items', via: :get, as: 'liq_process_clean_items'

  resources :user_delete_reasons


  resources :scholarship_applications, except: [:new, :index]
  match 'scholarship_applications/go/:scholarship_process_id/:subject_user_id', to: 'scholarship_applications#go_to', via: :get, as: 'scholarship_application_go_to'


  resources :scholarship_lists


  resources :scholarship_processes
  match 'scholarship_processes/:id/edit_groups', to: 'scholarship_processes#edit_groups', via: :get, as: 'scholarship_process_edit_groups'
  match 'scholarship_processes/:id/update_groups', to: 'scholarship_processes#update_groups', via: :put, as: 'scholarship_process_update_groups'
  match 'scholarship_manager_processes/', to: 'scholarship_manager_processes#index', via: :get, as: 'scholarship_manager_processes'
  match 'scholarship_manager_processes/:id', to: 'scholarship_manager_processes#show', via: :get, as: 'scholarship_manager_processes_show'
  match 'scholarship_manager_processes/application/:scholarship_application_id', to: 'scholarship_manager_processes#show_application', via: :get, as: 'scholarship_manager_processes_show_application'
  match 'scholarship_manager_processes/application/:scholarship_application_id/evaluate', to: 'scholarship_manager_processes#evaluate_application', via: :post, as: 'scholarship_manager_processes_evaluate_application'
  match 'scholarship_manager_processes/:id/applications/report', to: 'scholarship_manager_processes#applications_list_report', via: :get, as: 'scholarship_manager_processes_applications_list_report'
  match 'scholarship_manager_processes/application/:scholarship_application_id/revert', to: 'scholarship_manager_processes#revert_evaluation', via: :get, as: 'scholarship_manager_processes_revert_evaluation'
  match 'scholarship_manager_processes/application/:scholarship_application_id/delete', to: 'scholarship_manager_processes#delete_application', via: :get, as: 'scholarship_manager_processes_delete_application'
  match 'scholarship_manager_processes/:id/massive_evaluation', to: 'scholarship_manager_processes#massive_evaluation', via: :post, as: 'scholarship_manager_processes_massive_evaluation'
  resources :sel_notification_templates


  resources :password_policies


  match 'migra_security/p4', to: 'migra_security#paso_4', via: :get, as: 'migra_security_p4'
  match 'migra_security/p5', to: 'migra_security#paso_5', via: :get, as: 'migra_security_p5'
  match 'migra_security/p6', to: 'migra_security#paso_6', via: :get, as: 'migra_security_p6'
  match 'migra_security/p7', to: 'migra_security#paso_7', via: :get, as: 'migra_security_p7'
  match 'migra_security/p8', to: 'migra_security#paso_8', via: :get, as: 'migra_security_p8'
  match 'migra_security/p9', to: 'migra_security#paso_9', via: :get, as: 'migra_security_p9'

  match 'temporal_processing/puestos_visanet', to: 'temporal_processing#puestos_visanet', via: :get, as: 'temporal_processing_puestos_visanet'

  resources :brc_apv_institutions


  resources :secu_cohades




  #brc_define
  match 'brc_define/list_processes', to: 'brc_define#list_processes', via: :get, as: 'brc_define_list_processes'
  match 'brc_define/list_people/:brc_process_id', to: 'brc_define#list_people', via: :get, as: 'brc_define_list_people'

  match 'brc_define/massive_assign_members/:brc_process_id', to: 'brc_define#massive_assign_members', via: :get, as: 'brc_define_massive_assign_members'
  match 'brc_define/massive_assign_members/:brc_process_id', to: 'brc_define#massive_assign_members', via: :post, as: 'brc_define_massive_assign_members'
  match 'brc_define/download_massive_assign_members_excel_format', to: 'brc_define#download_massive_assign_members_excel_format', via: :get, as: 'brc_define_download_massive_assign_members_excel_format'
  match 'brc_define/download_categories_excel', to: 'brc_define#download_categories_excel', via: :get, as: 'brc_define_download_categories_excel'

  match 'brc_define/remove_members/:brc_process_id', to: 'brc_define#remove_members', via: :post, as: 'brc_define_remove_members'

  match 'brc_define/show/:brc_member_id', to: 'brc_define#show', via: :get, as: 'brc_define_show'
  match 'brc_define/new/:brc_member_id', to: 'brc_define#new', via: :get, as: 'brc_define_new'
  match 'brc_define/create/:brc_member_id', to: 'brc_define#create', via: :post, as: 'brc_define_create'
  match 'brc_define/edit/:brc_bonus_item_id', to: 'brc_define#edit', via: :get, as: 'brc_define_edit'
  match 'brc_define/update/:brc_bonus_item_id', to: 'brc_define#update', via: :put, as: 'brc_define_update'
  match 'brc_define/delete/:brc_bonus_item_id', to: 'brc_define#delete', via: :get, as: 'brc_define_delete'
  match 'brc_define/finish/:brc_member_id', to: 'brc_define#finish', via: :post, as: 'brc_define_finish'

  match 'brc_define/massive_finish/:brc_process_id', to: 'brc_define#massive_finish', via: :post, as: 'brc_define_massive_finish'

  match 'brc_define/download_defined_bonus/:brc_process_id', to: 'brc_define#download_defined_bonus', via: :get, as: 'brc_define_download_defined_bonus'

  #brc_validate
  match 'brc_validate/list_processes', to: 'brc_validate#list_processes', via: :get, as: 'brc_validate_list_processes'
  match 'brc_validate/list_people/:brc_process_id', to: 'brc_validate#list_people', via: :get, as: 'brc_validate_list_people'
  match 'brc_validate/show/:brc_member_id', to: 'brc_validate#show', via: :get, as: 'brc_validate_show'
  match 'brc_validate/finish/:brc_member_id', to: 'brc_validate#finish', via: :post, as: 'brc_validate_finish'


  match 'brc_validate/massive_validate/:brc_process_id', to: 'brc_validate#massive_validate', via: :get, as: 'brc_validate_massive_validate'
  match 'brc_validate/massive_validate/:brc_process_id', to: 'brc_validate#massive_validate', via: :post, as: 'brc_validate_massive_validate'
  match 'brc_validate/download_massive_validate_excel_format/:brc_process_id', to: 'brc_validate#download_massive_validate_excel_format', via: :get, as: 'brc_validate_download_massive_validate_excel_format'

  match 'brc_validate/massive_validate_categ/:brc_process_id/:cv_id/:brc_category_id', to: 'brc_validate#massive_validate_categ', via: :post, as: 'brc_validate_massive_validate_categ'
  match 'brc_validate/validate_item/:brc_process_id/:cv_id/:brc_category_id/:brc_bonus_item_id', to: 'brc_validate#validate_item', via: :post, as: 'brc_validate_validate_item'

  resources :brc_categories

  #brc_managing
  match 'brc/massive_assign_members/:brc_process_id', to: 'brc_managing#massive_assign_members', via: :get, as: 'brc_managing_massive_assign_members'
  match 'brc/massive_assign_members/:brc_process_id', to: 'brc_managing#massive_assign_members', via: :post, as: 'brc_managing_massive_assign_members'
  match 'brc/download_massive_assign_members_excel_format', to: 'brc_managing#download_massive_assign_members_excel_format', via: :get, as: 'brc_managing_download_massive_assign_members_excel_format'

  match 'brc/massive_assign_definers/:brc_process_id', to: 'brc_managing#massive_assign_definers', via: :get, as: 'brc_managing_massive_assign_definers'
  match 'brc/massive_assign_definers/:brc_process_id', to: 'brc_managing#massive_assign_definers', via: :post, as: 'brc_managing_massive_assign_definers'
  match 'brc/download_massive_assign_definers_excel_format', to: 'brc_managing#download_massive_assign_definers_excel_format', via: :get, as: 'brc_managing_download_massive_assign_definers_excel_format'

  match 'brc/download_bonus_defined_excel_format/:brc_process_id', to: 'brc_managing#download_bonus_defined_excel_format', via: :get, as: 'brc_managing_download_bonus_defined_excel_format'
  match 'brc/massive_charge_bonus_calculated/:brc_process_id', to: 'brc_managing#massive_charge_bonus_calculated', via: :get, as: 'brc_managing_massive_charge_bonus_calculated'
  match 'brc/massive_charge_bonus_calculated/:brc_process_id', to: 'brc_managing#massive_charge_bonus_calculated', via: :post, as: 'brc_managing_massive_charge_bonus_calculated'
  match 'brc/download_massive_charge_bonus_calculated_excel_format', to: 'brc_managing#download_massive_charge_bonus_calculated_excel_format', via: :get, as: 'brc_managing_download_massive_charge_bonus_calculated_excel_format'

  match 'brc/download_bonus_chosen_excel_format/:brc_process_id', to: 'brc_managing#download_bonus_chosen_excel_format', via: :get, as: 'brc_managing_download_bonus_chosen_excel_format'
  match 'brc/download_bonus_chosen_excel_format_2/:brc_process_id', to: 'brc_managing#download_bonus_chosen_excel_format_2', via: :get, as: 'brc_managing_download_bonus_chosen_excel_format_2'

  match 'brc/massive_choose_list/:brc_process_id', to: 'brc_managing#massive_choose_list', via: :get, as: 'brc_managing_massive_choose_list'
  match 'brc/massive_choose/:brc_process_id', to: 'brc_managing#massive_choose', via: :post, as: 'brc_managing_massive_choose'

  #brc_choose
  match 'brc_choose/list_processes', to: 'brc_choose#list_processes', via: :get, as: 'brc_choose_list_processes'
  match 'brc_choose/show/:brc_process_id', to: 'brc_choose#show', via: :get, as: 'brc_choose_show'
  match 'brc_choose/choose/:brc_process_id', to: 'brc_choose#choose', via: :post, as: 'brc_choose_choose'

  match 'brc_choose/upload_file/:brc_process_id', to: 'brc_choose#upload_file', via: :get, as: 'brc_choose_upload_file'
  match 'brc_choose/upload_file/:brc_process_id', to: 'brc_choose#upload_file', via: :post, as: 'brc_choose_upload_file'

  match 'brc_choose/delete_file/:brc_process_id/:brc_member_file_id', to: 'brc_choose#delete_file', via: :get, as: 'brc_choose_delete_file'
  match 'brc_choose/download_file/:brc_process_id/:brc_member_file_id', to: 'brc_choose#download_file', via: :get, as: 'brc_choose_download_file'

  resources :brc_processes
  match 'brc_process/download_file/:brc_process_id/:brc_member_file_id', to: 'brc_processes#download_file', via: :get, as: 'brc_processes_download_file'
  match 'brc_process/download_files/:brc_process_id', to: 'brc_processes#download_files', via: :get, as: 'brc_processes_download_files'
  match 'brc_process/reject_format/:brc_member_id', to: 'brc_processes#reject_format', via: :post, as: 'brc_processes_reject_format'
  match 'brc_process/remove_reject_format/:brc_member_id', to: 'brc_processes#remove_reject_format', via: :post, as: 'brc_processes_remove_reject_format'

  resources :cc_characteristics


  resources :jcc_characteristics


  resources :sc_documentations


  resources :sc_periods





  resources :vac_requests_periods


  resources :jm_profile_sections, except: [:show]


  # This is where we send people to authenticate with OmniAuth.
  get '/auth/azureactivedirectory', as: :azure_ad

  # This is where we are redirected if OmniAuth successfully authenicates the user.
  match '/auth/:provider/callback', to: 'sessions#create_from_azure', via: [:get, :post]

  # This is where we are redirected if we acquire authorization separately from OmniAuth.
  match '/authorize', to: 'sessions#create_from_azure_0', via: [:get, :post]

  #This is where we are redirected if OmniAuth fails to authenticate the user. user
  match '/auth/:provider/failure', to: redirect('/'), via: [:get, :post]


  resources :jm_profile_forms
  match 'jm_profile_forms/:id/manage_chars', to: 'jm_profile_forms#manage_chars', via: :get, as: 'jm_profile_forms_manage_chars'
  match 'jm_profile_forms/:id/preview', to: 'jm_profile_forms#preview_form', via: [:get, :post], as: 'jm_profile_forms_preview_form'
  match 'jm_profile_forms/:id/edit_visibility_chars', to: 'jm_profile_forms#edit_visibility_chars', via: :get, as: 'jm_profile_forms_edit_visibility_chars'

  resources :j_cost_centers
  resources :j_pay_bands
  resources :j_characteristics

  #MEDLIC_PROCESSES
  match 'medlic_processes/index', to: 'medlic_processes#index', via: :get, as: 'medlic_processes_index'
  match 'medlic_processes/new', to: 'medlic_processes#new', via: :get, as: 'medlic_processes_new'
  match 'medlic_processes/create', to: 'medlic_processes#create', via: :post, as: 'medlic_processes_create'

  match 'medlic_processes/manage/:medlic_process_id', to: 'medlic_processes#manage', via: :get, as: 'medlic_process_manage'
  match 'medlic_processes/upload_licenses/:medlic_process_id', to: 'medlic_processes#upload_licenses', via: :post, as: 'medlic_process_upload_licenses'

  match 'medlic_processes/delete/:medlic_process_id', to: 'medlic_processes#delete', via: :post, as: 'medlic_processes_delete'


  match 'medlic_processes/edit/:medlic_process_id', to: 'medlic_processes#edit', via: :get, as: 'medlic_processes_edit'
  match 'medlic_processes/update/:medlic_process_id', to: 'medlic_processes#update', via: :post, as: 'medlic_processes_update'

  #MEDLIC_REPORTS
  match 'medlic_reports/new_simple_report/for_process/:medlic_process_id/:partial_report', to: 'medlic_reports#new_simple_report', via: :post, as: 'medlic_reports_new_simple_report'


  #BRG_PERCENTAGE_CONVERSIONS
  match 'brg_percentage_conversions/new/:brg_process_id', to: 'brg_percentage_conversions#new', via: :get, as: 'brg_percentage_conversions_new'
  match 'brg_percentage_conversions/create/:brg_process_id', to: 'brg_percentage_conversions#create', via: :post, as: 'brg_percentage_conversions_create'
  match 'brg_percentage_conversions/edit/:brg_percentage_conversion_id', to: 'brg_percentage_conversions#edit', via: :get, as: 'brg_percentage_conversions_edit'
  match 'brg_percentage_conversions/update/:brg_percentage_conversion_id', to: 'brg_percentage_conversions#update', via: :post, as: 'brg_percentage_conversions_update'
  match 'brg_percentage_conversions/deactivate_pc/:brg_percentage_conversion_id', to: 'brg_percentage_conversions#deactivate_pc', via: :post, as: 'brg_percentage_conversions_deactivate'
  match 'brg_percentage_conversions/deactivate_pc/by_ajax/:brg_percentage_conversion_id', to: 'brg_percentage_conversions#deactivate_pc_ajax', via: :post, as: 'brg_percentage_conversions_deactivate_ajax'

  match 'brg_percentage_conversions/upload_excel/:brg_process_id', to: 'brg_percentage_conversions#massive_upload', via: :get, as: 'brg_percentage_conversions_massive_upload'
  match 'brg_percentage_conversions/create/upload_excel/:brg_process_id', to: 'brg_percentage_conversions#massive_upload_2', via: :post, as: 'brg_percentage_conversions_massive_upload_2'

  #BRG_REPORTS
  match 'brg_reports/new_simple_report/for_process/:brg_process_id/:partial_report', to: 'brg_reports#new_simple_report', via: :post, as: 'brg_reports_new_simple_report'

  #BRG_GROUPS
  match 'brg_groups/index/:tab', to: 'brg_groups#index', via: :get, as: 'brg_groups_index'
  match 'brg_groups/new/:brg_process_id', to: 'brg_groups#new', via: :get, as: 'brg_groups_new'
  match 'brg_groups/create/:brg_process_id', to: 'brg_groups#create', via: :post, as: 'brg_groups_create'
  match 'brg_groups/edit/:brg_group_id', to: 'brg_groups#edit', via: :get, as: 'brg_groups_edit'
  match 'brg_groups/manage/:brg_group_id', to: 'brg_groups#manage', via: :get, as: 'brg_groups_manage'
  match 'brg_groups/update/:brg_group_id', to: 'brg_groups#update', via: :post, as: 'brg_groups_update'
  match 'brg_groups/deactivate_group/:brg_group_id', to: 'brg_groups#deactivate_group', via: :post, as: 'brg_groups_deactivate_group'
  match 'brg_groups/deactivate_group/:brg_group_id', to: 'brg_groups#deactivate_group_ajax', via: :post, as: 'brg_groups_deactivate_group_ajax'

  match 'brg_groups/upload_excel/groups/:brg_process_id', to: 'brg_groups#groups_massive_upload', via: :get, as: 'brg_groups_groups_massive_upload'
  match 'brg_groups/create/upload_excel/groups/:brg_process_id', to: 'brg_groups#groups_massive_upload_2', via: :post, as: 'brg_groups_groups_massive_upload_2'

  match 'brg_groups/upload_excel/groups/users/:brg_process_id', to: 'brg_groups#group_users_massive_upload', via: :get, as: 'brg_groups_group_users_massive_upload'
  match 'brg_groups/create/upload_excel/groups/users/:brg_process_id', to: 'brg_groups#group_users_massive_upload_2', via: :post, as: 'brg_groups_group_users_massive_upload_2'

  match 'brg_groups/:brg_group_id/search_people_for/assoc_user/:brg_process_id', to: 'brg_groups#search_user_for_assign', via: [:get, :post], as: 'brg_groups_search_user_for_assign'

  match 'brg_groups/:brg_group_id/assoc_user/:user_id/create/:brg_process_id', to: 'brg_groups#group_user_create', via: :post, as: 'brg_groups_group_user_create'
  match 'brg_groups/deactivate_group_user/:brg_group_user_id', to: 'brg_groups#group_user_deactivate', via: :post, as: 'brg_groups_group_user_deactivate'


  #BRG_MANAGER
  match 'brg_managers/search_people_for/new_manager/:brg_process_id', to: 'brg_managers#new_step_0', via: [:get, :post], as: 'brg_managers_new_step_0'
  match 'brg_managers/new/:user_id/:brg_process_id', to: 'brg_managers#new', via: [:get, :post], as: 'brg_managers_new'
  match 'brg_managers/create/:brg_process_id', to: 'brg_managers#create', via: :post, as: 'brg_managers_create'
  match 'brg_managers/edit/:brg_manager_id', to: 'brg_managers#edit', via: :get, as: 'brg_managers_edit'
  match 'brg_managers/manage/:brg_manager_id/:tab', to: 'brg_managers#manage', via: :get, as: 'brg_managers_manage'
  match 'brg_managers/update/:brg_manager_id', to: 'brg_managers#update', via: :post, as: 'brg_managers_update'
  match 'brg_managers/deactivate_relation/:brg_manage_relation_id/:tab', to: 'brg_managers#deactivate_relation', via: :post, as: 'brg_managers_deactivate_relation'

  match 'brg_managers/deactivate_manager/:brg_manager_id', to: 'brg_managers#deactivate_manager', via: :post, as: 'brg_managers_deactivate_manager'
  match 'brg_managers/deactivate_manager/:brg_manager_id', to: 'brg_managers#deactivate_manager_ajax', via: :post, as: 'brg_managers_deactivate_manager_ajax'

  match 'brg_managers/upload_excel/managers/:brg_process_id', to: 'brg_managers#managers_massive_upload', via: :get, as: 'brg_managers_managers_massive_upload'
  match 'brg_managers/create/upload_excel/managers/:brg_process_id', to: 'brg_managers#managers_massive_upload_2', via: :post, as: 'brg_managers_managers_massive_upload_2'

  match 'brg_managers/search_people_for/assoc_managee_to/:brg_manager_id', to: 'brg_managers#assoc_manager', via: [:get, :post], as: 'brg_managers_assoc_manager'
  match 'brg_managers/assoc_manager/:user_id/to/:brg_manager_id/create', to: 'brg_managers#assoc_manager_2', via: :post, as: 'brg_managers_assoc_manager_2'

  match 'brg_managers/assoc_group/to/:brg_manager_id', to: 'brg_managers#assoc_group', via: :get, as: 'brg_managers_assoc_group'
  match 'brg_managers/assoc_group/:brg_group_id/to/:brg_manager_id/create', to: 'brg_managers#assoc_group_2', via: :post, as: 'brg_managers_assoc_group_2'


  match 'brg_managers/upload_excel/assoc_group/:brg_process_id', to: 'brg_managers#assoc_group_massive_upload', via: :get, as: 'brg_managers_assoc_group_massive_upload'
  match 'brg_managers/create/upload_excel/assoc_groups/:brg_process_id', to: 'brg_managers#assoc_group_massive_upload_2', via: :post, as: 'brg_managers_assoc_group_massive_upload_2'
  match 'brg_managers/deactivate_relation/:brg_manage_relation_id', to: 'brg_managers#deactivate_relation_ajax', via: :post, as: 'brg_managers_deactivate_relation_ajax'

  match 'brg_managers/upload_excel/assoc_managees/:brg_process_id', to: 'brg_managers#assoc_managee_massive_upload', via: :get, as: 'brg_managers_assoc_managee_massive_upload'
  match 'brg_managers/create/upload_excel/assoc_managees/:brg_process_id', to: 'brg_managers#assoc_managee_massive_upload_2', via: :post, as: 'brg_managers_assoc_managee_massive_upload_2'


  #BRG_PROCESS
  match 'brg_processes/new', to: 'brg_processes#new', via: :get, as: 'brg_processes_new'
  match 'brg_processes/create', to: 'brg_processes#create', via: :post, as: 'brg_processes_create'
  match 'brg_processes/edit/:brg_process_id', to: 'brg_processes#edit', via: :get, as: 'brg_processes_edit'

  match 'brg_processes/super_edit/:brg_process_id/:super_edition', to: 'brg_processes#edit', via: :get, as: 'brg_processes_super_edit'
  match 'brg_processes/super_update/:brg_process_id/:super_update', to: 'brg_processes#update', via: :post, as: 'brg_processes_super_update'

  match 'brg_processes/manage/:brg_process_id', to: 'brg_processes#manage', via: :get, as: 'brg_processes_manage'
  match 'brg_processes/update/:brg_process_id', to: 'brg_processes#update', via: :post, as: 'brg_processes_update'

  match 'brg_processes/index_for_manager', to: 'brg_processes#index_for_manager', via: :get, as: 'brg_processes_index_for_manager'

  match 'brg_processes/index_for_manager/step/0', to: 'brg_processes#index_for_manager_0', via: :get, as: 'brg_processes_index_for_manager_0'
  match 'brg_processes/index_for_manager/step/0/:as_brg_rp_manager', to: 'brg_processes#index_for_manager_0', via: :get, as: 'brg_processes_index_for_manager_0_privates'

  match 'brg_processes/process/:brg_process_id/gimme/bonus_datatable/index_1', to: 'brg_processes#gimme_bonus_datatable_index_1', via: :post, as: 'brg_processes_gimme_bonus_datatable_index_1'
  match 'brg_processes/index_for/:brg_process_id/process', to: 'brg_processes#index_for_manager_1', via: :get, as: 'brg_processes_index_for_manager_1'
  match 'brg_processes/sum_table_for/:brg_process_id/by_manager', to: 'brg_processes#gimme_sum_table', via: :get, as: 'brg_processes_gimme_sum_table'

  match 'brg_processes/update/bonus/:brg_bonus_id', to: 'brg_processes#update_bonus', via: :post, as: 'brg_processes_update_bonus'

  match 'brg_processes/get/bonus_show_modal/:brg_bonus_id/:index', to: 'brg_processes#bonus_show_modal', via: :get, as: 'brg_processes_bonus_show_modal'
  match 'brg_processes/update/bonus/:brg_bonus_id/by_ajax/:index', to: 'brg_processes#update_bonus_by_ajax', via: :post, as: 'brg_processes_update_bonus_by_ajax'
  match 'brg_processes/approve_bonus/:brg_bonus_id/ajax_call/:index', to: 'brg_processes#approve_bonus_ajax', via: :post, as: 'brg_processes_approve_bonus_ajax'

  match 'brg_processes/approve_bonus/:brg_bonus_id', to: 'brg_processes#approve_bonus', via: :post, as: 'brg_processes_approve_bonus'

  match 'brg_processes/manage_users/:brg_process_id', to: 'brg_processes#manage_users', via: :get, as: 'brg_processes_manage_users'
  match 'brg_processes/change_paid/:brg_bonus_id', to: 'brg_processes#change_paid_ajax', via: :post, as: 'brg_processes_change_paid_ajax'

  match 'brg_processes/set_evaluations/ajax_call', to: 'brg_processes#set_evaluations', via: :post, as: 'brg_processes_set_evaluations_ajax'

  match 'brg_processes/process_status_switch/:brg_process_id', to: 'brg_processes#process_status_switch', via: :post, as: 'brg_processes_process_status_switch'

  match 'brg_processes/reprocess_this_bonus/:brg_bonus_id', to: 'brg_processes#reprocess_bonus', via: :get, as: 'brg_processes_reprocess_bonus'

  #BRG_USERS
  match 'brg_users/massive_upload/:brg_process_id', to: 'brg_users#massive_upload', via: :get, as: 'brg_users_massive_upload'
  match 'brg_users/massive_upload/step_2/:brg_process_id', to: 'brg_users#massive_upload_2', via: :post, as: 'brg_users_massive_upload_2'


  #LIC_APPROVERS
  match 'lic_approvers/index', to: 'lic_approvers#index', via: :get, as: 'lic_approvers_index'
  match 'lic_approvers/index/:as_manager', to: 'lic_approvers#index', via: :get, as: 'lic_approvers_index_as_manager'

  match 'lic_approvers/manage/request/:lic_event_id', to: 'lic_approvers#manage', via: :get, as: 'lic_approvers_manage'
  match 'lic_approvers/edit/:lic_request_id/change_status/:status', to: 'lic_approvers#change_status', via: :post, as: 'lic_approvers_change_status'
  match 'lic_approvers/edit/:lic_request_id/change_status/:status/modal', to: 'lic_approvers#change_status_modal', via: :get, as: 'lic_approvers_change_status_modal'

  #LIC_REGISTERS
  match 'lic_register/index', to: 'lic_registers#index', via: :get, as: 'lic_register_index'
  match 'lic_register/request_new/:lic_event_id/:bp_license_id', to: 'lic_registers#request_new', via: :get, as: 'lic_register_request_new'
  match 'lic_register/request_create/:lic_event_id', to: 'lic_registers#request_create', via: :post, as: 'lic_register_request_create'
  match 'lic_register/request/:lic_request_id/manage', to: 'lic_registers#request_manage', via: :get, as: 'lic_register_request_manage'
  match 'lic_register/request/:lic_request_id/edit', to: 'lic_registers#request_edit', via: :get, as: 'lic_register_request_edit'
  match 'lic_register/request/:lic_request_id/update', to: 'lic_registers#request_update', via: :post, as: 'lic_register_request_update'
  match 'lic_register/request/:lic_request_id/deactivate', to: 'lic_registers#request_deactivate', via: :get, as: 'lic_register_request_deactivate'

  match 'lic_register/get/request_show_modal/:lic_event_id', to: 'lic_registers#request_show_modal', via: :get, as: 'lic_register_request_show_modal'


  match 'lic_register/download/file/:value_file', to: 'lic_registers#download_file', via: :get, as: 'lic_register_download_file'

  match 'lic_register/only_event_new/:bp_event_id', to: 'lic_registers#only_event_new', via: :get, as: 'lic_register_only_event_new'
  match 'lic_register/only_event_create/:bp_event_id', to: 'lic_registers#only_event_create', via: :post, as: 'lic_register_only_event_create'

  match 'lic_register/event_new/:bp_license_id', to: 'lic_registers#event_new', via: :get, as: 'lic_register_event_new'
  match 'lic_register/event_create/:bp_license_id', to: 'lic_registers#event_create', via: :post, as: 'lic_register_event_create'
  match 'lic_register/event_manage/:lic_event_id', to: 'lic_registers#event_manage', via: :get, as: 'lic_register_event_manage'
  match 'lic_register/event_manage/:lic_event_id/:open_detail', to: 'lic_registers#event_manage', via: :get, as: 'lic_register_event_manage_open_detail_option'

  match 'lic_register/verify_events_date/:bp_event_id/:user_id', to: 'lic_registers#verify_events_date', via: :post, as: 'lic_register_event_verify_events_date'

  match 'lic_register/event_edit/:lic_event_id', to: 'lic_registers#event_edit', via: :get, as: 'lic_register_event_edit'
  match 'lic_register/event_update/:lic_event_id', to: 'lic_registers#event_update', via: :post, as: 'lic_register_event_update'
  match 'lic_register/verify_licenses_conditions/:user_id/:bp_license_id', to: 'lic_registers#verify_licenses_conditions', via: :post, as: 'lic_register_verify_licenses_conditions'

  match 'lic_register/get_end/:bp_license_id/:user_id', to: 'lic_registers#get_end', via: :post, as: 'lic_register_get_end'
  match 'lic_register/get_go_back/:bp_license_id/:user_id', to: 'lic_registers#get_go_back', via: :post, as: 'lic_register_get_go_back'
  match 'lic_register/get_available/:bp_license_id/:user_id', to: 'lic_registers#get_available', via: :post, as: 'lic_register_get_available'
  match 'lic_register/get_period/:bp_license_id/:user_id', to: 'lic_registers#get_request_period', via: :post, as: 'lic_register_get_period'
  match 'lic_register/get_period/:bp_license_id/:user_id', to: 'lic_registers#get_request_period', via: :post, as: 'lic_register_get_period'

  #LIC_MANAGER
  match 'lic_manager/index', to:'lic_manager#index', via: :get, as: 'lic_manager_index'

  match 'lic_manager/upload_excel/validators', to: 'lic_manager#massive_upload_validators', via: :get, as: 'lic_manager_massive_upload_validators'
  match 'lic_manager/create/upload_excel/validators', to: 'lic_manager#massive_upload_validators_2', via: :post, as: 'lic_manager_massive_upload_validators_2'
  match 'lic_manager/deactivate/approvee/:lic_user_to_approve_id', to: 'lic_manager#approvee_delete', via: :get, as: 'lic_manager_approvee_delete'
  match 'lic_manager/get/request_historical_info/show_modal/:lic_request_id', to: 'lic_manager#show_request_historical_info', via: :get, as: 'lic_manager_show_request_historical_info'
  match 'lic_manager/edit/:lic_event_id', to: 'lic_manager#event_edit', via: :get, as: 'lic_manager_event_edit'
  match 'lic_manager/event_update/:lic_event_id', to: 'lic_manager#event_update', via: :post, as: 'lic_manager_event_update'

  #Approver
  match 'lic_manager/new/search_people', to: 'lic_manager#approver_new', via: [:get, :post], as: 'lic_managers_approver_new'
  match 'lic_manager/create_approver/user/:user_id', to: 'lic_manager#approver_create', via: [:post], as: 'lic_managers_approver_create'
  match 'lic_manager/deactivate/approver/:lic_approver_id', to:'lic_manager#approver_deactivate', via: :get, as: 'lic_managers_approver_deactivate'
  match 'lic_manager/manage/approver/:lic_approver_id', to:'lic_manager#approver_manage', via: :get, as: 'lic_managers_approver_manage'

  #Approvee
  match 'lic_manager/new_approvee/search_people_for/:lic_approver_id', to: 'lic_manager#approvee_new', via: [:get, :post], as: 'lic_managers_aprovee_new'
  match 'lic_manager/create/approvee/user/:user_id/for_approver/:lic_approver_id', to: 'lic_manager#approvee_create', via: [:post], as: 'lic_managers_approvee_create'
  match 'lic_manager/deactivate/approvee/:lic_user_to_approve_id', to:'lic_manager#approvee_deactivate', via: :get, as: 'lic_managers_approvee_deactivate'

  #Events
  match 'lic_manager/event_manage/:lic_event_id', to: 'lic_manager#event_manage', via: :get, as: 'lic_managers_event_manage'
  match 'lic_manager/edit/:lic_request_id/change_status/:status', to: 'lic_manager#change_status', via: :post, as: 'lic_managers_change_status'
  match 'lic_manager/edit/:lic_request_id/change_status/:status/modal', to: 'lic_manager#change_status_modal', via: :get, as: 'lic_managers_change_status_modal'

  match 'lic_manager/edit/:lic_request_id/change_paid/:paid', to: 'lic_manager#change_paid', via: :post, as: 'lic_managers_change_paid'
  match 'lic_manager/edit/:lic_request_id/change_paid/:paid/modal', to: 'lic_manager#change_paid_modal', via: :get, as: 'lic_managers_change_paid_modal'

  match 'lic_manager/request/:lic_request_id/edit', to: 'lic_manager#request_edit', via: :get, as: 'lic_managers_request_edit'
  match 'lic_manager/request/:lic_request_id/update', to: 'lic_manager#request_update', via: :post, as: 'lic_managers_change_request_update'


  #VAC_ACCUMULATEDS
  resources :vac_accumulateds
  match 'vac_accumulateds/index/all', to: 'vac_accumulateds#index', via: :get, as: 'vac_accumulateds_index'
  match 'vac_accumulateds/new/search_people', to: 'vac_accumulateds#search_people', via: [:get, :post], as: 'vac_accumulateds_new_search_people'
  match 'vac_accumulateds/new/form/:user_id', to: 'vac_accumulateds#new', via: [:get], as: 'vac_accumulateds_new'
  match 'vac_accumulateds/create/', to: 'vac_accumulateds#create', via: :post, as: 'vac_accumulateds_create'
  match 'vac_accumulateds/modal/balance_detail/:user_id', to: 'vac_accumulateds#balance_detail_modal', via: :get, as: 'vac_accumulateds_balance_detail_modal'

  #VAC_APPROVERS
  resources :vac_approvers
  match 'vac_approvers/new/search_people', to: 'vac_approvers#new', via: [:get, :post], as: 'vac_approvers_new_search_people'
  match 'vac_approvers/create_approver/user/:user_id', to: 'vac_approvers#create', via: [:post], as: 'vac_approvers_create'
  match 'vac_approvers/deactivate/approver/:vac_approver_id', to:'vac_approvers#deactivate', via: :get, as: 'vac_approvers_deactivate'

  #VAC_USER_TO_APPROVE
  match 'vac_approvers/new_approvee/search_people_for/:vac_approver_id', to: 'vac_approvers#aprovee_new', via: [:get, :post], as: 'vac_approvers_new_approvee_search_people'
  match 'vac_approvers/create/approvee/user/:user_id/for_approver/:vac_approver_id', to: 'vac_approvers#approvee_create', via: [:post], as: 'vac_approvers_create_approvee'
  match 'vac_approvers/deactivate/approvee/:vac_user_to_approve_id', to:'vac_approvers#approvee_deactivate', via: :get, as: 'vac_approvers_deactivate_approvee'

  #VAC_REQUEST vac_records_register
  match 'vac_records_register/scheme_calculus', to: 'vac_records_register#scheme_calculus', via: :post, as: 'vac_records_register_scheme_calculus'
  match 'vac_records_register/index', to: 'vac_records_register#index', via: :get, as: 'vac_records_register_index'
  match 'vac_records_register/new_request', to: 'vac_records_register#register', via: [:get, :post], as: 'vac_records_register_register_new'
  match 'vac_records_register/valid/create_request', to: 'vac_records_register#valid_create_request', via: [:post], as: 'vac_records_register_valid_create_request'
  match 'vac_records_register/valid/update_request/:vac_request_id', to: 'vac_records_register#valid_create_request', via: [:post], as: 'vac_records_register_valid_update_request'

  match 'vac_records_register/list/benfits/from/request', to: 'vac_records_register#request_benefits', via: :post, as: 'vac_records_register_request_benefits'

  match 'vac_records_register/create/request', to: 'vac_records_register#create', via: :post, as: 'vac_records_register_register_create'
  match 'vac_records_register/take/benefits', to: 'vac_records_register#take_benefits', via: :post, as: 'vac_records_register_take_benefits'

  match 'vac_records_register/edit/request/:vac_request_id', to: 'vac_records_register#edit', via: :get, as: 'vac_records_register_register_edit'
  match 'vac_records_register/show/request/:vac_request_id', to: 'vac_records_register#show', via: :get, as: 'vac_records_register_register_show'
  match 'vac_records_register/delete/request/:vac_request_id', to: 'vac_records_register#deactivate', via: :get, as: 'vac_records_register_register_deactivate'
  match 'vac_records_register/delete/taken_benefit/:vac_rule_record_id', to: 'vac_records_register#deactivate_rule_record', via: :get, as: 'vac_records_register_deactivate_rule_record'

  match 'vac_records_register/update/request/:vac_request_id', to: 'vac_records_register#update', via: [:put, :post], as: 'vac_records_register_register_update'

  match 'vac_records_register/scheme/days/:user_id', to: 'vac_records_register#scheme_days', via: :get, as: 'vac_records_register_scheme_days'

  #VAC_REQUEST vac_records_validation
  match 'vac_records_validation/index/:as_manager', to: 'vac_records_validation#index', via: :get, as: 'vac_records_validation_index_as_manager'
  match 'vac_records_validation/index', to: 'vac_records_validation#index', via: :get, as: 'vac_records_validation_index'
  match 'vac_records_validation/manage/request/:vac_request_id', to: 'vac_records_validation#manage', via: :get, as: 'vac_records_validation_manage'
  match 'vac_records_validation/edit/request/:vac_request_id', to: 'vac_records_validation#edit', via: :get, as: 'vac_records_validation_edit'
  match 'vac_records_validation/update/request/:vac_request_id', to: 'vac_records_validation#update', via: [:put, :post], as: 'vac_records_validation_update'
  match 'vac_records_validation/request/:vac_request_id/status/:vac_request_status', to: 'vac_records_validation#change_status', via: :post, as: 'vac_records_validation_change_status'
  match 'vac_records_validation/reject/request/:vac_request_id/modal/:vac_request_status', to: 'vac_records_validation#status_description_modal', via: :get, as: 'vac_records_validation_status_description_modal'

  match 'vac_records_validation/approved_requests', to: 'vac_records_validation#approved_requests', via: :get, as: 'vac_records_validation_approved_requests'

  #VAC_FORM
  match 'vac_form/set/days', to: 'vac_form#set_days', via: :post, as: 'vac_form_set_days'
  match 'vac_form/set/end', to: 'vac_form#set_end', via: :post, as: 'vac_form_set_end'
  match 'vac_form/set/progressive_days/:user_id', to: 'vac_form#progressive_days_available?', via: :post, as: 'vac_form_progressive_days_available'
  match 'vac_form/set/warnings/:user_id', to: 'vac_form#set_warnings', via: :post, as: 'vac_form_set_warnings'
  match 'vac_form/restrictive/warnings/:user_id', to: 'vac_form#restrictive_warnings', via: :post, as: 'vac_form_restrictive_warnings'
  match 'vac_form/get/vacations_detail/:user_id', to: 'vac_form#vacs_detail', via: :post, as: 'vac_form_vac_detail'
  match 'vac_form/get/vacations_detail/:user_id/modal', to: 'vac_form#vacs_detail_as_modal', via: :post, as: 'vac_form_vacs_detail_as_modal'

  match 'vac_form/get/next_laborable_day', to: 'vac_form#next_laborable_day', via: :post, as: 'vac_form_next_laborable_day'

  match 'vac_form/get/next_vac_accumulated/:user_id', to: 'vac_form#next_vac_accumulated', via: :post, as: 'vac_form_next_vac_accumulated'

  match 'vac_form/get/set_days_only_vac', to: 'vac_form#set_days_only_vac', via: :post, as: 'vac_form_set_days_only_vac'
  match 'vac_form/get/set_benefits', to: 'vac_form#set_benefits', via: :post, as: 'vac_form_set_benefits'
  match 'vac_form/get/set_benefits/as_manager', to: 'vac_form#set_benefits_as_manager', via: :post, as: 'vac_form_set_benefits_as_manager'
  match 'vac_form/get/set_benefits/as_manager/:user_id', to: 'vac_form#set_benefits_as_manager', via: :post, as: 'vac_form_set_benefits_as_manager_user_id'

  match 'vac_form/get/set_next_days/:user_id', to: 'vac_form#set_next_days_record_form', via: :post, as: 'vac_form_set_next_days_record_form'
  match 'vac_form/get/set_next_days_progressive/:user_id', to: 'vac_form#set_next_days_progressive_record_form', via: :post, as: 'vac_form_set_next_days_progressive_record_form'
  match 'vac_form/get/sum_or_rest_back_to_work/:user_id', to: 'vac_form#sum_or_rest_back_to_work', via: :post, as: 'vac_form_sum_or_rest_back_to_work'

  match 'vac_form/get/request_show_modal/:vac_request_id', to: 'vac_form#request_show_modal', via: :get, as: 'vac_form_request_show_modal'

  match 'vac_form/get/model_errors_request/', to: 'vac_form#model_errors_request', via: :post, as: 'vac_form_model_errors_request'
  match 'vac_form/get/model_errors_request/request/:vac_request_id', to: 'vac_form#model_errors_request', via: :post, as: 'vac_form_model_errors_request_request'


  #VAC_MANAGER_REQUESTS
  match 'vac_manager_requests/new/:user_id/:back_to_validation', to:'vac_manager_requests#new', via: :get, as: 'vac_manager_requests_new'
  match 'vac_manager_requests/create/:user_id/:back_to_validation', to:'vac_manager_requests#create', via: :post, as: 'vac_manager_requests_create'

  #VAC_WARNING_EXCEPTION

  match 'vac_manager_by_user/find_user_information/from/:user_id', to: 'vac_warning_exceptions#find_user_information', via: :post, as: 'vac_warning_exceptions_index_find_user_information'
  match 'vac_manager_by_user/index', to:'vac_warning_exceptions#index', via: :get, as: 'vac_warning_exceptions_index'
  match 'vac_manager_by_user/found/:user_id', to:'vac_warning_exceptions#index', via: :get, as: 'vac_manager_by_user_found'
  match 'vac_manager_by_user/match_user', to: 'vac_warning_exceptions#match_user', via: :post, as: 'vac_warning_exceptions_match_user'
  match 'vac_manager_by_user/create/exception/for/:user_id', to: 'vac_warning_exceptions#create', via: :post, as: 'vac_warning_exceptions_create'
  match 'vac_manager_by_user/delete/exception/:bp_warning_exception_id', to: 'vac_warning_exceptions#delete', via: :post, as: 'vac_warning_exceptions_delete'

  #VAC_MANAGER
  match 'vac_manager/deactivated_description_modal/:vac_request_id', to: 'vac_manager#deactivated_description_modal', via: :get, as: 'vac_manager_deactivated_description_modal'
  match 'vac_manager/deactivate_request_manager/:vac_request_id', to: 'vac_manager#deactivate_request_manager', via: :post, as: 'vac_manager_deactivate_request_manager'
  match 'vac_manager/scheme/days/:user_id', to: 'vac_manager#scheme_days', via: :get, as: 'vac_manager_scheme_days'
  match 'vac_manager/scheme_calculus/for/:user_id', to: 'vac_manager#scheme_calculus', via: :post, as: 'vac_manager_scheme_calculus'
  match 'vac_manager/update_legal_and_prog', to:'vac_manager#update_legal_and_prog', via: [:post], as: 'vac_manager_update_legal_and_prog'

  match 'vac_manager/manager/manual_requests_simple_list/:user_id', to: 'vac_manager#manual_requests_simple_list', via: :post, as: 'vac_manager_manual_requests_simple_list'

  match 'vac_manager/manager/historical_deletable_from/:user_id', to: 'vac_manager#historical_deletable_from', via: :post, as: 'vac_manager_historical_deletable_from'
  match 'vac_manager/find_users_search/for_manual_requests/:action_to_do', to:'vac_manager#find_users_search_generic', via: [:post], as: 'vac_manager_find_users_search_generic'

  match 'vac_manager/users_search/paid/or/not/benefits', to:'vac_manager#users_search_unpaid_benefits', via: [:post], as: 'vac_manager_users_search_unpaid_benefits'
  match 'vac_manager/users_search/paid/or/not/benefits/:user_id', to:'vac_manager#unpaid_benefits_for', via: [:post], as: 'vac_manager_unpaid_benefits_for'


  match 'vac_manager/users_search/unpaid/benefits', to:'vac_manager#users_search_paid_yn_benefits', via: [:post], as: 'vac_manager_users_search_paid_yn_benefits'
  match 'vac_manager/users_search/unpaid/benefits/:user_id', to:'vac_manager#paid_yn_benefits_for', via: [:post], as: 'vac_manager_paid_yn_benefits_for'

  match 'vac_manager/manager/manual_requests_from/from/:user_id', to: 'vac_manager#manual_requests_from', via: :post, as: 'vac_manager_manual_requests_from'
  match 'vac_manager/find_users_search/for_manual_requests', to:'vac_manager#find_users_search_manual_req', via: [:post], as: 'vac_manager_find_users_search_manual_req'


  match 'vac_manager/manager/find_historical_requests/from/:user_id', to: 'vac_manager#historical_requests_from', via: :post, as: 'vac_manager_historical_requests_from'
  match 'vac_manager/find_users_search', to:'vac_manager#find_users_search', via: [:post], as: 'vac_manager_find_users_search'

  match 'vac_manager/index', to:'vac_manager#index', via: :get, as: 'vac_manager_index'
  match 'vac_manager/show/record/:vac_request_id', to:'vac_manager#show_manual_record', via: :get, as: 'vac_manager_show_manual_record'
  match 'vac_manager/new/record/search_people', to:'vac_manager#new_manual_record', via: [:get, :post], as: 'vac_manager_new_record_search_people'
  match 'vac_manager/new/record/:user_id/step2', to:'vac_manager#new_manual_record_step_2', via: [:post], as: 'vac_manager_new_manual_record_step_2'
  match 'vac_manager/manage/user/:user_id', to: 'vac_manager#user_manage', via: :get, as: 'vac_manager_user_manage'
  match 'vac_manager/create/record/:user_id', to:'vac_manager#create_manual_record', via: [:post], as: 'vac_manager_create_manual_record'
  match 'vac_manager/upload_excel/requests', to:'vac_manager#massive_upload_requests', via: :get, as: 'vac_manager_massive_upload_requests'
  match 'vac_manager/create/upload_excel/requests', to:'vac_manager#massive_create_upload_requests', via: :post, as: 'vac_manager_massive_create_upload_requests'

  match 'vac_manager/massive/historical_upload/requests', to:'vac_manager#massive_historical_upload', via: :get, as: 'vac_manager_massive_historical_upload'
  match 'vac_manager/create/historical_upload/requests/2', to:'vac_manager#massive_historical_upload_2', via: :post, as: 'vac_manager_massive_historical_upload_2'


  match 'vac_manager/request/:vac_request_id/manage/:back_to_validation', to:'vac_manager#request_manage', via: :get, as: 'vac_manager_request_manage_validation'
  match 'vac_manager/request/:vac_request_id/manage', to:'vac_manager#request_manage', via: :get, as: 'vac_manager_request_manage'
  match 'vac_manager/request/:vac_request_id/manager_edit', to:'vac_manager#request_edit', via: :get, as: 'vac_manager_request_edit'
  match 'vac_manager/request/:vac_request_id/request_update', to:'vac_manager#request_update', via: :post, as: 'vac_manager_request_update'

  match 'vac_manager/take/benefits/as/manager', to: 'vac_manager#take_benefits', via: :post, as: 'vac_manager_take_benefits'

  match 'vac_manager/benefit/:vac_rule_record_id/paid_description_modal/:paid', to:'vac_manager#paid_description_modal', via: :get, as: 'vac_manager_paid_description_modal'
  match 'vac_manager/benefit/:vac_rule_record_id/change_paid/:paid', to:'vac_manager#benefit_change_paid', via: :post, as: 'vac_manager_benefit_change_paid'


  match 'vac_manager/massive/close_requests', to:'vac_manager#massive_close_requests', via: :post, as: 'vac_manager_massive_close_requests'


  match 'vac_manager/upload_excel/validators', to:'vac_manager#massive_upload_validators', via: :get, as: 'vac_manager_massive_upload_validators'
  match 'vac_manager/create/upload_excel/validators', to:'vac_manager#massive_upload_validators_2', via: :post, as: 'vac_manager_massive_upload_validators_2'
  match 'vac_manager/deactivate/approvee/:vac_user_to_approve_id',to:'vac_manager#approvee_delete', via: :get, as: 'vac_manager_approvee_delete'

  match 'vac_manager/get/request_historical_info/show_modal/:vac_request_id', to: 'vac_manager#show_request_historical_info', via: :get, as: 'vac_manager_show_request_historical_info'

  match 'vac_manager/benefits_report/index', to: 'vac_manager#benefits_report', via: :get, as: 'vac_manager_benefits_report'
  match 'vac_manager/benefits_report/generate', to: 'vac_manager#benefits_report_generate', via: :post, as: 'vac_manager_benefits_report_generate'
  match 'vac_manager/benefits_report/generate/bonus_specific', to: 'vac_manager#benefits_report_generate_for_bonus', via: :post, as: 'vac_manager_benefits_report_generate_for_bonus'
  match 'vac_manager/benefits_report/generate/requests', to: 'vac_manager#benefits_report_generate_requests', via: :post, as: 'vac_manager_benefits_report_generate_requests'


  match 'vac_manager/auto_close_seven_days', to: 'vac_manager#auto_close_seven_days', via: :get, as: 'vac_manager_auto_close_seven_days'

  match 'vac_manager/reject/request/:vac_request_id/modal/:vac_request_status', to: 'vac_manager#status_description_modal', via: :get, as: 'vac_manager_status_description_modal'
  match 'vac_manager/request/:vac_request_id/status/:vac_request_status', to: 'vac_manager#change_status', via: :post, as: 'vac_manager_change_status'
  match 'vac_manager/accumulated_info_for/:vac_request_id', to: 'vac_manager#accumulated_info_for', via: :get, as: 'vac_manager_accumulated_info_for'
  match 'vac_manager/delete_historical_request/:vac_request_id', to: 'vac_manager#delete_historical_request', via: :post, as: 'vac_manager_delete_historical_request'
  match 'vac_manager/delete_historical_request_ajax/:vac_request_id', to: 'vac_manager#delete_historical_request_ajax', via: :post, as: 'vac_manager_delete_historical_request_ajax'

  match 'vac_manager/scheme_report/generate', to: 'vac_manager#scheme_report', via: :post, as: 'vac_manager_scheme_report'

  #BP_FORM
  resources :bp_forms
  match 'bp_forms/event/:bp_event_id/new/form', to: 'bp_forms#new', via: :get, as: 'bp_form_new'
  match 'bp_forms/event/:bp_event_id/create/form', to: 'bp_forms#create', via: :post, as: 'bp_form_create'
  match 'bp_forms/edit/form/:id', to: 'bp_forms#edit', via: :get, as: 'bp_form_edit'
  match 'bp_forms/event/:bp_event_id/update/form', to: 'bp_forms#update', via: :post, as: 'bp_form_update'

  #BP_ITEM
  match 'bp_items/item/:id/delete', to: 'bp_items#delete', via: :post, as: 'bp_item_delete'
  match 'bp_items/item/:id/editable', to: 'bp_items#editable_item', via: :post, as: 'bp_item_editable_item'
  match 'bp_items/item/:id/non/editable', to: 'bp_items#non_editable', via: :post, as: 'bp_item_non_editable'
  match 'bp_items/item/:id/save', to: 'bp_items#save', via: :post, as: 'bp_item_save'

  #BP_ITEM_OPTION

  match 'bp_item_options/option/:id/delete', to: 'bp_item_options#delete', via: :post, as: 'bp_item_option_delete'
  match 'bp_item_options/option/:id/editable', to: 'bp_item_options#editable_item_option', via: :post, as: 'bp_item_option_editable_item_option'
  match 'bp_item_options/option/:id/non/editable', to: 'bp_item_options#non_editable', via: :post, as: 'bp_item_option_non_editable'
  match 'bp_item_options/option/:id/save', to: 'bp_item_options#save', via: :post, as: 'bp_item_option_save'

  #BP_ADMIN_EVENT
  match 'bp_admin_events/index', to: 'bp_admin_events#bp_admin_events', via: :get, as: 'bp_admin_events'
  match 'bp_admin_events/new', to: 'bp_admin_events#bp_admin_event_new', via: :get, as: 'bp_admin_event_new'
  match 'bp_admin_events/create', to: 'bp_admin_events#bp_admin_event_create', via: :post, as: 'bp_admin_event_create'
  match 'bp_admin_events/edit/:bp_event_id', to: 'bp_admin_events#bp_admin_event_edit', via: :get, as: 'bp_admin_event_edit'
  match 'bp_admin_events/update/:bp_event_id', to: 'bp_admin_events#bp_admin_event_update', via: :post, as: 'bp_admin_event_update'
  match 'bp_admin_events/deactivate/:bp_event_id', to: 'bp_admin_events#bp_admin_event_deactivate', via: :post, as: 'bp_admin_event_deactivate'
  match 'bp_admin_events/manage/:bp_event_id', to: 'bp_admin_events#bp_admin_event_manage', via: :get, as: 'bp_admin_event_manage'

  match 'bp_admin_event_files/event/:bp_event_id/file/new', to: 'bp_admin_events#bp_event_file_new', via: :get, as: 'bp_admin_event_file_new'
  match 'bp_admin_event_files/event/:bp_event_id/file/create', to: 'bp_admin_events#bp_event_file_create', via: :post, as: 'bp_admin_event_file_create'
  match 'bp_admin_event_files/event/:bp_event_id/file/:bp_event_file_id/edit', to: 'bp_admin_events#bp_event_file_edit', via: :get, as: 'bp_admin_event_file_edit'
  match 'bp_admin_event_files/update/:bp_event_file_id', to: 'bp_admin_events#bp_event_file_update', via: :post, as: 'bp_admin_event_file_update'
  match 'bp_admin_event_files/deactivate/:bp_event_file_id', to: 'bp_admin_events#bp_event_file_deactivate', via: :post, as: 'bp_admin_event_file_deactivate'

  #BP_ADMIN_SEASON
  match 'bp_admin_seasons/index', to: 'bp_admin_seasons#bp_admin_seasons', via: :get, as: 'bp_admin_seasons'
  match 'bp_admin_seasons/new', to: 'bp_admin_seasons#bp_admin_season_new', via: :get, as: 'bp_admin_season_new'
  match 'bp_admin_seasons/create', to: 'bp_admin_seasons#bp_admin_season_create', via: :post, as: 'bp_admin_season_create'
  match 'bp_admin_seasons/edit/:bp_season_id', to: 'bp_admin_seasons#bp_admin_season_edit', via: :get, as: 'bp_admin_season_edit'
  match 'bp_admin_seasons/update/:bp_season_id', to: 'bp_admin_seasons#bp_admin_season_update', via: :post, as: 'bp_admin_season_update'
  match 'bp_admin_seasons/deactivate/:bp_season_id', to: 'bp_admin_seasons#bp_admin_season_deactivate', via: :post, as: 'bp_admin_season_deactivate'
  match 'bp_admin_seasons/manage/:bp_season_id', to: 'bp_admin_seasons#bp_admin_season_manage', via: :get, as: 'bp_admin_season_manage'

  match 'bp_admin_season_periods/season/:bp_season_id/file/new', to: 'bp_admin_seasons#bp_season_period_new', via: :get, as: 'bp_admin_season_period_new'
  match 'bp_admin_season_periods/season/:bp_season_id/file/create', to: 'bp_admin_seasons#bp_season_period_create', via: :post, as: 'bp_admin_season_period_create'
  match 'bp_admin_season_periods/season/:bp_season_id/file/:bp_season_period_id/edit', to: 'bp_admin_seasons#bp_season_period_edit', via: :get, as: 'bp_admin_season_period_edit'
  match 'bp_admin_season_periods/update/:bp_season_period_id', to: 'bp_admin_seasons#bp_season_period_update', via: :post, as: 'bp_admin_season_period_update'
  match 'bp_admin_season_periods/deactivate/:bp_season_period_id', to: 'bp_admin_seasons#bp_season_period_deactivate', via: :post, as: 'bp_admin_season_period_deactivate'

  #BP_ADMIN_GROUP
  match 'bp_admin_groups/index', to: 'bp_admin_groups#bp_group_index', via: :get, as: 'bp_admin_group_index'
  match 'bp_admin_groups/new', to: 'bp_admin_groups#bp_group_new', via: :get, as: 'bp_admin_group_new'
  match 'bp_admin_groups/create', to: 'bp_admin_groups#bp_group_create', via: :post, as: 'bp_admin_group_create'
  match 'bp_admin_groups/:bp_group_id/edit', to: 'bp_admin_groups#bp_group_edit', via: :get, as: 'bp_admin_group_edit'
  match 'bp_admin_groups/:bp_group_id/:update', to: 'bp_admin_groups#bp_group_update', via: :post, as: 'bp_admin_group_update'
  match 'bp_admin_groups/:bp_group_id/manage', to: 'bp_admin_groups#bp_group_manage', via: :get, as: 'bp_admin_group_manage'

  #BP_ADMIN_GROUP_USER
  match 'bp_admin_group_users/:bp_group_id/new', to: 'bp_admin_groups#bp_group_user_new_search_people', via: [:get, :post], as: 'bp_admin_group_user_new'
  match 'bp_admin_group_users/:bp_group_id/user/:user_id/create', to: 'bp_admin_groups#bp_group_user_create', via: :post, as: 'bp_admin_group_user_create'
  match 'bp_admin_group_users/:bp_group_user_id/edit', to: 'bp_admin_groups#bp_group_user_edit', via: :get, as: 'bp_admin_group_user_edit'
  match 'bp_admin_group_users/:bp_group_user_id/update', to: 'bp_admin_groups#bp_group_user_update', via: :post, as: 'bp_admin_group_user_update'
  match 'bp_admin_group_users/:bp_group_id/excel_upload', to: 'bp_admin_groups#bp_group_user_excel_upload', via: :get, as: 'bp_admin_group_user_excel_upload'
  match 'bp_admin_group_users/:bp_group_id/excel_create', to: 'bp_admin_groups#bp_group_user_excel_create', via: :post, as: 'bp_admin_group_user_excel_create'
  match 'bp_admin_group_users/:bp_group_id/excel_delete', to: 'bp_admin_groups#bp_group_user_excel_delete', via: :get, as: 'bp_admin_group_user_excel_delete'
  match 'bp_admin_group_users/:bp_group_id/excel_destroy', to: 'bp_admin_groups#bp_group_user_excel_destroy', via: :post, as: 'bp_admin_group_user_excel_destroy'


  #BP_ADMIN_GENERAL_RULES_VACATION
  match 'bp_admin_general_rules_vacations/get/description/from/:bp_general_rule_id', to: 'bp_admin_general_rules_vacations#general_rule_description_modal', via: :get, as: 'bp_admin_general_rules_vacations_general_rule_description_modal'

  match 'bp_admin_general_rules_vacations/group/:bp_group_id/new', to: 'bp_admin_general_rules_vacations#new', via: :get, as: 'bp_admin_general_rules_vacation_new'
  match 'bp_admin_general_rules_vacations/group/:bp_group_id/create', to: 'bp_admin_general_rules_vacations#create', via: :post, as: 'bp_admin_general_rules_vacation_create'
  match 'bp_admin_general_rules_vacations/group/:bp_group_id/general_rules_vacations/:bp_general_rules_vacation_id/edit', to: 'bp_admin_general_rules_vacations#edit', via: :get, as: 'bp_admin_general_rules_vacation_edit'
  match 'bp_admin_general_rules_vacations/group/:bp_group_id/general_rules_vacations/:bp_general_rules_vacation_id/update', to: 'bp_admin_general_rules_vacations#update', via: :post, as: 'bp_admin_general_rules_vacation_update'

  match 'bp_admin_general_rules_vacations/index_by_user', to: 'bp_admin_general_rules_vacations#index_by_user', via: :get, as: 'bp_admin_general_rules_vacation_index_by_user'
  match 'bp_admin_general_rules_vacations/index_manage/user/:user_id', to: 'bp_admin_general_rules_vacations#index_by_user_manage', via: :get, as: 'bp_admin_general_rules_vacation_index_by_user_manage'
  match 'bp_admin_general_rules_vacations/new/specific_user/:user_id', to: 'bp_admin_general_rules_vacations#new_by_user', via: :get, as: 'bp_admin_general_rules_vacation_new_by_user'
  match 'bp_admin_general_rules_vacations/user/:user_id/create', to: 'bp_admin_general_rules_vacations#create_by_user', via: :post, as: 'bp_admin_general_rules_vacation_create_by_user'
  match 'bp_admin_general_rules_vacations/edit/:bp_general_rules_vacation_id', to: 'bp_admin_general_rules_vacations#edit_by_user', via: :get, as: 'bp_admin_general_rules_vacation_edit_by_user'
  match 'bp_admin_general_rules_vacations/update/:bp_general_rules_vacation_id/by_user', to: 'bp_admin_general_rules_vacations#update_by_user', via: :post, as: 'bp_admin_general_rules_vacation_update_by_user'

  match 'bp_admin_general_rules_vacations/new_warning/:bp_general_rules_vacation_id', to: 'bp_admin_general_rules_vacations#new_warning', via: :get, as: 'bp_admin_general_rules_vacation_new_warning'
  match 'bp_admin_general_rules_vacations/create_warning/:bp_general_rules_vacation_id', to: 'bp_admin_general_rules_vacations#create_warning', via: :post, as: 'bp_admin_general_rules_vacation_create_warning'
  match 'bp_admin_general_rules_vacations/edit_warning/:bp_rules_warning_id', to: 'bp_admin_general_rules_vacations#edit_warning', via: :get, as: 'bp_admin_general_rules_vacation_edit_warning'
  match 'bp_admin_general_rules_vacations/update_waning/:bp_rules_warning_id', to: 'bp_admin_general_rules_vacations#update_waning', via: :post, as: 'bp_admin_general_rules_vacation_update_waning'
  match 'bp_admin_general_rules_vacations/deactivate_warning/:bp_rules_warning_id', to: 'bp_admin_general_rules_vacations#deactivate_warning', via: :post, as: 'bp_admin_general_rules_vacation_deactivate_warning'

  match 'bp_admin_general_rules_vacations/new_progressive_day/:bp_general_rules_vacation_id', to: 'bp_admin_general_rules_vacations#new_progressive_day', via: :get, as: 'bp_admin_general_rules_vacation_new_progressive_day'
  match 'bp_admin_general_rules_vacations/create_progressive_day/:bp_general_rules_vacation_id', to: 'bp_admin_general_rules_vacations#create_progressive_day', via: :post, as: 'bp_admin_general_rules_vacation_create_progressive_day'
  match 'bp_admin_general_rules_vacations/edit_progressive_day/:bp_progressive_day_id', to: 'bp_admin_general_rules_vacations#edit_progressive_day', via: :get, as: 'bp_admin_general_rules_vacation_edit_progressive_day'
  match 'bp_admin_general_rules_vacations/update_progressive_day/:bp_progressive_day_id', to: 'bp_admin_general_rules_vacations#update_progressive_day', via: :post, as: 'bp_admin_general_rules_vacation_update_progressive_day'
  match 'bp_admin_general_rules_vacations/deactivate_progressive_day/:bp_progressive_day_id', to: 'bp_admin_general_rules_vacations#deactivate_progressive_day', via: :post, as: 'bp_admin_general_rules_vacation_deactivate_progressive_day'

  match 'bp_admin_general_rules_vacations/upload_excel/progressive_days/:bp_general_rules_vacation_id', to:'bp_admin_general_rules_vacations#massive_upload_progressive_days', via: :get, as: 'bp_admin_general_rules_vacation_massive_upload_progressive_days'
  match 'bp_admin_general_rules_vacations/create/upload_excel/progressive_days/:bp_general_rules_vacation_id', to:'bp_admin_general_rules_vacations#massive_create_upload_progressive_days', via: :post, as: 'bp_admin_general_rules_vacation_massive_create_upload_progressive_days'

  #BP_ADMIN_RULE_VACATION
  match 'bp_admin_rule_vacations/group/:bp_group_id/new', to: 'bp_admin_rule_vacations#new', via: :get, as: 'bp_admin_rule_vacation_new'
  match 'bp_admin_rule_vacations/group/:bp_group_id/create', to: 'bp_admin_rule_vacations#create', via: :post, as: 'bp_admin_rule_vacation_create'
  match 'bp_admin_rule_vacations/:bp_rule_vacation_id/edit', to: 'bp_admin_rule_vacations#edit', via: :get, as: 'bp_admin_rule_vacation_edit'
  match 'bp_admin_rule_vacations/group/:bp_group_id/:bp_rule_vacation_id/update', to: 'bp_admin_rule_vacations#update', via: :post, as: 'bp_admin_rule_vacation_update'
  match 'bp_admin_rule_vacations/group/:bp_group_id/:bp_rule_vacation_id/manage', to: 'bp_admin_rule_vacations#manage', via: :get, as: 'bp_admin_rule_vacation_manage'
  match 'bp_admin_rule_vacations/:bp_rule_vacation_id/deactivate', to: 'bp_admin_rule_vacations#deactivate_rules', via: :get, as: 'bp_admin_rule_vacation_deactivate_rules'
  match 'bp_admin_rule_vacations/rule_vacations/show_modal/:user_id', to: 'bp_admin_rule_vacations#rule_vacations_show_modal', via: :get, as: 'bp_admin_rule_vacation_rule_vacations_show_modal'


  #BP_ADMIN_CONDITION_VACATION
  match 'bp_admin_condition_vacations/rule_vacation/:bp_rule_vacation_id/new', to: 'bp_admin_condition_vacations#new', via: :get, as: 'bp_admin_condition_vacation_new'
  match 'bp_admin_condition_vacations/rule_vacation/:bp_rule_vacation_id/create', to: 'bp_admin_condition_vacations#create', via: :post, as: 'bp_admin_condition_vacation_create'
  match 'bp_admin_condition_vacations/rule_vacation/:bp_rule_vacation_id/condition/:bp_condition_vacation_id/edit', to: 'bp_admin_condition_vacations#edit', via: :get, as: 'bp_admin_condition_vacation_edit'
  match 'bp_admin_condition_vacations/rule_vacation/:bp_rule_vacation_id/condition/:bp_condition_vacation_id/update', to: 'bp_admin_condition_vacations#update', via: :post, as: 'bp_admin_condition_vacation_update'
  match 'bp_admin_condition_vacations/:bp_condition_vacation_id/deactivate_condition', to: 'bp_admin_condition_vacations#deactivate_condition', via: :get, as: 'bp_admin_condition_vacation_deactivate_condition'

  match 'bp_admin_condition_vacations/new_version/:bp_condition_vacation_id/:bp_rule_vacation_id', to: 'bp_admin_condition_vacations#new_version', via: :get, as: 'bp_admin_condition_vacations_new_version'
  match 'bp_admin_condition_vacations/create_version/:bp_condition_vacation_id/:bp_rule_vacation_id', to: 'bp_admin_condition_vacations#create_version', via: :post, as: 'bp_admin_condition_vacations_create_version'

  #BP_ADMIN_LICENSE
  match 'bp_admin_licenses/group/:bp_group_id/new', to: 'bp_admin_licenses#new', via: :get, as: 'bp_admin_licenses_new'
  match 'bp_admin_licenses/group/:bp_group_id/create', to: 'bp_admin_licenses#create', via: :post, as: 'bp_admin_licenses_create'
  match 'bp_admin_licenses/group/:bp_group_id/license/:bp_license_id/edit', to: 'bp_admin_licenses#edit', via: :get, as: 'bp_admin_licenses_edit'
  match 'bp_admin_licenses/group/:bp_group_id/license/:bp_license_id/update', to: 'bp_admin_licenses#update', via: :post, as: 'bp_admin_licenses_update'
  match 'bp_admin_licenses/license/:bp_license_id/deactivate', to: 'bp_admin_licenses#deactivate', via: :get, as: 'bp_admin_licenses_deactivate'

  match 'bp_admin_licenses/get/license_show_modal/:bp_license_id', to: 'bp_admin_licenses#license_info_modal', via: :get, as: 'bp_admin_license_info_modal'

  match 'bp_admin_licenses/new_version/:bp_license_id', to: 'bp_admin_licenses#new_version', via: :get, as: 'bp_admin_licenses_new_version'
  match 'bp_admin_licenses/create_version/:bp_license_id', to: 'bp_admin_licenses#create_version', via: :post, as: 'bp_admin_licenses_create_version'

  #PARAMETRIZACIÓN DE BENEFICIOS DE VACACIONES Y PERMISOS
  resources :bp_groups
  #BP_GROUPS GRUPOS - SINDICATOS
  match 'bp_groups/:bp_group_id/deactivate', to: 'bp_groups#bp_groups_deactivate', via: :post, as: 'bp_groups_deactivate'
  match 'bp_groups/:bp_group_id/activate', to: 'bp_groups#bp_groups_activate', via: :post, as: 'bp_groups_activate'

  #BP_GROUPS_USERS ASOCIACIONES DE USUARIOS CON GRUPOS
  match 'bp_groups_users/:bp_group_id/index', to: 'bp_groups#groups_users', via: :get, as: 'bp_groups_users'
  match 'bp_groups_users/:bp_group_id/new_search_people', to: 'bp_groups#groups_users_new_search_people', via: :get, as: 'groups_users_new_search_people'
  match 'bp_groups_users/:bp_group_id/new_search_people', to: 'bp_groups#groups_users_new_search_people', via: :post, as: 'groups_users_new_search_people'
  match 'bp_groups_users/:bp_group_id/user/:user_id', to: 'bp_groups#groups_users_create', via: :get, as: 'groups_users_create'
  match 'bp_groups_users/:bp_group_id/user/:user_id/period_time', to: 'bp_groups#groups_users_create_period_time', via: :post, as: 'bp_groups_users_create_period_time'
  match 'bp_groups_users/:bp_group_user_id/deactivate', to: 'bp_groups#groups_users_deactivate', via: :post, as: 'bp_groups_users_deactivate'
  match 'bp_groups_users/:bp_group_user_id/edit', to: 'bp_groups#groups_users_edit', via: :get, as: 'bp_groups_users_edit'
  match 'bp_groups_users/:bp_group_user_id/update', to: 'bp_groups#groups_users_update', via: :post, as: 'bp_groups_users_update'

  #BP_GENERAL_RULES_VACATIONS REGLAS GENERALES DE VACACIONES
  resources :bp_general_rules_vacations


  #BP_RULE_VACATIONS REGLAS ESPECÍFICAS DE VACACIONES: BONOS DE VACACIONES
  resources :bp_rule_vacations
  match 'bp_rule_vacations/:bp_rule_vacation_id/deactivate', to: 'bp_rule_vacations#deactivate', via: :post, as: 'bp_rule_vacations_deactivate'

  # BP_SEASONS  : TEMPORADAS Y SUS FECHAS
  resources :bp_seasons

  #BP_SEASONS_PERIODS
  match 'bp_season_periods/new', to: 'bp_seasons#bp_season_period_new', via: :get, as: 'bp_season_period_new'
  match 'bp_season_periods/create', to: 'bp_seasons#bp_season_period_create', via: :post, as: 'bp_season_period_create'
  match 'bp_season_periods/:bp_season_period_id/deactivate', to: 'bp_seasons#bp_season_period_deactivate', via: :post, as: 'bp_season_period_deactivate'

  #BP_EVENTS
  resources :bp_events

  #BP_EVENTS_FILES
  match 'bp_event_files/event/:bp_event_id', to: 'bp_events#bp_event_files', via: :get, as: 'bp_event_files'
  match 'bp_event_files/new/event/:bp_event_id', to: 'bp_events#bp_event_file_new', via: :get, as: 'bp_event_file_new'
  match 'bp_event_files/create/event/:bp_event_id', to: 'bp_events#bp_event_file_create', via: :post, as: 'bp_event_file_create'
  match 'bp_event_files/edit/:bp_event_file_id', to: 'bp_events#bp_event_file_edit', via: :get, as: 'bp_event_file_edit'
  match 'bp_event_files/update/:bp_event_file_id', to: 'bp_events#bp_event_file_update', via: :post, as: 'bp_event_file_update'
  match 'bp_event_files/deactivate/:bp_event_file_id', to: 'bp_events#bp_event_file_deactivate', via: :post, as: 'bp_event_file_deactivate'


  #BP_CONDITION_VACATIONS

  resources :bp_condition_vacations
  match 'bp_condition_vacations/deactivate/:bp_condition_vacation_id', to: 'bp_condition_vacations#deactivate', via: :post, as: 'bp_condition_vacation_deactivate'

  #BP_LICENSES
  resources :bp_licenses
  match 'bp_licenses/deactivate/:bp_license_id', to: 'bp_licenses#deactivate', via: :post, as: 'bp_license_deactivate'


  resources :sc_processes


  resources :ben_cellphone_characteristics
  match 'poll_characteristics/index', to: 'poll_characteristics#index', via: :get, as: 'poll_characteristics_index'
  match 'poll_characteristics/:id/edit', to: 'poll_characteristics#edit', via: :get, as: 'poll_characteristics_edit'
  match 'poll_characteristics/:id/update', to: 'poll_characteristics#update', via: :post, as: 'poll_characteristics_update'


  #match 'fix_brenny', to: 'pe_questions#destroy_pe_questions_massive', via: :get, as: 'fix_brenny'

  resources :sel_vacant_flows
  match 'sel_vacant_flows/:id/approvers/new', to: 'sel_vacant_flows#new_approver', via: :get, as: 'sel_vacant_flows_new_approver'
  match 'sel_vacant_flows/:id/approvers/create', to: 'sel_vacant_flows#create_approver', via: :post, as: 'sel_vacant_flows_create_approver'
  match 'sel_vacant_flows/:id/approvers/:sel_flow_approver_id/edit', to: 'sel_vacant_flows#edit_approver', via: :get, as: 'sel_vacant_flows_edit_approver'
  match 'sel_vacant_flows/:id/approvers/:sel_flow_approver_id/update', to: 'sel_vacant_flows#update_approver', via: :put, as: 'sel_vacant_flows_update_approver'
  match 'sel_vacant_flows/:id/approvers/:sel_flow_approver_id/destroy', to: 'sel_vacant_flows#destroy_approver', via: :delete, as: 'sel_vacant_flows_destroy_approver'
  match 'sel_vacant_flows/load_characteristic_values', to: 'sel_vacant_flows#load_characteristic_values', via: :post, as: 'sel_vacant_flows_load_characteristic_values'
  match 'sel_vacant_flows/load_user_characteristic_values', to: 'sel_vacant_flows#load_user_characteristic_values', via: :post, as: 'sel_vacant_flows_load_user_characteristic_values'


  resources :training_characteristics


  resources :blog_entries, only: [:destroy]
  match 'blog_entries/:blog_form_id/:user_id/new', to: 'blog_entries#new', via: :get, as: 'new_blog_entry'
  match 'blog_entries/:blog_form_id/new', to: 'blog_entries#new', via: :get, as: 'new_my_blog_entry'
  match 'blog_entries/:blog_form_id/:user_id/', to: 'blog_entries#create', via: :post, as: 'create_blog_entry'
  match 'blog_entries/:blog_form_id', to: 'blog_entries#create', via: :post, as: 'create_my_blog_entry'

  resources :blog_lists

  resources :blog_forms
  match 'blog_forms/:id/edit_permissions', to: 'blog_forms#show_config_permissions', via: :get, as: 'blog_forms_show_config_permissions'
  match 'blog_forms/:id/config_permissions', to: 'blog_forms#config_permissions', via: :put, as: 'blog_forms_config_permissions'

  resources :tutorials
  match 'tutorials/:id/upload', to: 'tutorials#upload_content', via: :post, as: 'tutorials_upload_content'
  match 'tutorials/:id/form_upload', to: 'tutorials#form_upload_content', via: :get, as: 'tutorials_form_upload_content'

  resources :dnc_characteristics


  resources :dnc_requirements


  resources :dnc_process_periods


  resources :dnc_courses


  resources :dnc_providers


  resources :dnc_categories


  resources :dnc_programs


  resources :dnc_processes
  match 'dnc_processes/massive_upload_form/:id', to: 'dnc_processes#massive_upload_form', via: :get, as: 'dnc_processes_massive_upload_form'
  match 'dnc_processes/massive_upload/:id', to: 'dnc_processes#massive_upload', via: :post, as: 'dnc_processes_massive_upload'


  resources :lib_characteristics, only: [:index, :new, :create, :destroy]


  resources :tracking_period_lists


  # sc_document
  match 'sc_documentations/index/manage', to: 'sc_documentations#index_manage', via: :get, as: 'sc_documentation_index_manage'
  match 'sc_documentations/new/:user_id', to: 'sc_documentations#new', via: :get, as: 'sc_documentation_new'
  match 'sc_documentations/search_people/:sc_period_id', to: 'sc_documentations#search_people', via: [:get, :post], as: 'sc_documentation_search_people'
  match 'sc_documentations/download_file/:sc_documentation_id/:doc_path', to: 'sc_documentations#download_file', via: :get, as: 'sc_documentation_download_file'
  match 'sc_documentations/delete/:sc_documentation_id', to: 'sc_documentations#delete', via: :get, as: 'sc_documentation_delete'
  match 'sc_documentations/delete_modal/:sc_documentation_id', to: 'sc_documentations#delete_modal', via: :get, as: 'sc_documentation_delete_modal'

  match 'sc_documentations/historical_documentation/:user_id', to: 'sc_documentations#historical_documentation', via: :get, as: 'sc_documentation_historical_documentation'
  match 'sc_documentations/edit_doc/:sc_documentation_id', to: 'sc_documentations#edit', via: :get, as: 'sc_documentations_edit_doc'
  match 'sc_documentations/ajax/users/:all_users/:manager_list', to: 'sc_documentations#users_to_show', via: :get, as: 'sc_documentations_users_to_show'


  #schedules_process Close  process

  match 'sc_processes/back_to_edit_process/:sc_process_id', to: 'sc_processes_closes#back_to_edit_process', via: :get, as: 'back_to_edit_process'

  match 'sc_processes/:sc_process_id/close_process/:tab', to: 'sc_processes_closes#close_process', via: :get, as: 'close_process'
  match 'sc_processes/:sc_process_id/action_close_process', to: 'sc_processes_closes#action_close_process', via: :get, as: 'action_close_process'
  match 'sc_processes/:sc_process_id/action_open_process', to: 'sc_processes_closes#action_open_process', via: :get, as: 'action_open_process'

  match 'sc_processes/attendances_detail/:sc_user_process_id', to: 'sc_processes_closes#attendances_detail', via: :get, as: 'attendances_detail'

  match 'sc_processes/:sc_process_id/dont_charge_to_payroll/sc_record/:sc_record_id', to: 'sc_processes_closes#dont_charge_to_payroll_record_show_modal', via: :get, as: 'dont_charge_to_payroll_record_show_modal'
  match 'sc_processes/:sc_process_id/dont_charge_to_payroll/update/sc_record/:sc_record_id', to: 'sc_processes_closes#dont_charge_to_payroll_update_record', via: :post, as: 'dont_charge_to_payroll_update_record'
  match 'sc_processes/:sc_process_id/refresh/records_list/:editable/close_process/:success_message', to: 'sc_processes_closes#show_to_close_records_list', via: :get, as: 'show_to_close_records_list'
  match 'sc_processes/:sc_process_id/charge_to_payroll/update/sc_record/:sc_record_id', to: 'sc_processes_closes#charge_to_payroll_update_record', via: :get, as: 'charge_to_payroll_update_record'
  match 'sc_processes/refresh_user_process_row/:sc_user_process_id', to: 'sc_processes_closes#refresh_user_process_row', via: :get, as: 'refresh_user_process_row'



  #SC Schedule Characteristics
  match 'sc_schedule_characteristics/index', to: 'sc_schedule_characteristics#index', via: :get, as: 'sc_schedule_characteristics_index'
  match 'sc_schedule_characteristics/:id/edit', to: 'sc_schedule_characteristics#edit', via: :get, as: 'sc_schedule_characteristics_edit'
  match 'sc_schedule_characteristics/:id/update', to: 'sc_schedule_characteristics#update', via: :post, as: 'sc_schedule_characteristics_update'

  #he_ct_module_privilages SC HE CT Module Privileges
  resources :he_ct_module_privilages
  match 'he_ct_module_privilages/index/:tab', to: 'he_ct_module_privilages#index', via: :get, as: 'he_ct_module_privilages_index'
  match 'he_ct_module_privilages/schedules/:sc_schedule_id/assign_users', to: 'he_ct_module_privilages#schedule_users', via: :get, as: 'schedule_users'
  match 'he_ct_module_privilages/schedules/:sc_schedule_id/search_people', to: 'he_ct_module_privilages#schedule_search_people_create_schedule_user', via: :get, as: 'schedule_search_people_create_schedule_user'
  match 'he_ct_module_privilages/schedules/:sc_schedule_id/search_people', to: 'he_ct_module_privilages#schedule_search_people_create_schedule_user', via: :post, as: 'schedule_search_people_create_schedule_user'
  match 'he_ct_module_privilages/schedules/:sc_schedule_id/refresh_table', to: 'he_ct_module_privilages#schedule_refresh_users', via: :get, as: 'schedule_refresh_users'
  match 'he_ct_module_privilages/schedules/users/:sc_schedule_user_id', to: 'he_ct_module_privilages#schedule_delete_users', via: :get, as: 'schedule_delete_users'
  match 'he_ct_module_privilages/schedules/:sc_schedule_id/users/:user_id/create_manually', to: 'he_ct_module_privilages#schedule_users_create_manually', via: :get, as: 'schedule_users_create_manually'
  match 'he_ct_module_privilages/schedules/:sc_schedule_id/user/:user_id/assign_user_with_period', to: 'he_ct_module_privilages#schedule_users_assign_user_with_period', via: :post, as: 'schedule_users_assign_user_with_period'
  match 'he_ct_module_privilages/schedules/users/:sc_schedule_user_id/deactivate_time_period', to: 'he_ct_module_privilages#deactivate_time_period', via: :get, as: 'schedule_dactivate_time_period_user'
  match 'he_ct_module_privilages/schedules/:sc_schedule_id/refresh_time_period_table', to: 'he_ct_module_privilages#schedule_refresh_users_time_period', via: :get, as: 'schedule_refresh_users_time_period'

  match 'he_ct_module_privilages/recorder/excel/', to:'he_ct_module_privilages#recorders_excel_upload', via: :get, as: 'he_ct_module_privilages_recorders_excel_upload'
  match 'he_ct_module_privilages/recorder/excel/upload', to:'he_ct_module_privilages#recorders_excel_upload_step_2', via: :post, as: 'he_ct_module_privilages_recorders_excel_upload_step_2'

  match 'he_ct_module_privilages/get/blocks_detail/from/:sc_schedule_id', to: 'he_ct_module_privilages#blocks_detail', via: :get, as: 'he_ct_module_privilages_blocks_detail'


  #CREATE PROCESS SC PRIVILAGES
  match 'he_ct_module_privilages/users_privilages/search_people', to: 'he_ct_module_privilages#privilages_search_people_new', via: :get, as: 'privilages_search_people_new'
  match 'he_ct_module_privilages/users_privilages/search_people', to: 'he_ct_module_privilages#privilages_search_people_new', via: :post, as: 'privilages_search_people_new'
  match 'he_ct_module_privilages/users_privilages/new/:user_id', to: 'he_ct_module_privilages#new', via: :get, as: 'privilages_new'
  match 'he_ct_module_privilages/users_privilages/create/:user_id', to: 'he_ct_module_privilages#create', via: :post, as: 'privilages_create'
  #MANAGMENT PROCESS SC PRIVILAGES
  match 'he_ct_module_privilages/users_privilages/:he_ct_module_privilages_id/privilages_management/:tab', to: 'he_ct_module_privilages#privilages_management', via: :get, as: 'privilages_management'
  match 'he_ct_module_privilages/users_privilages/:he_ct_module_privilages_id/edit', to: 'he_ct_module_privilages#edit', via: :get, as: 'privilages_management_edit'
  match 'he_ct_module_privilages/users_privilages/:he_ct_module_privilages_id/update', to: 'he_ct_module_privilages#update', via: :post, as: 'privilages_management_update'
  #PEOPLE_TO_RECORD
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/people_to_record/search_people', to: 'he_ct_module_privilages#people_to_record_search_new', via: :get, as: 'people_to_record_search_new'
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/people_to_record/search_people', to: 'he_ct_module_privilages#people_to_record_search_new', via: :post, as: 'people_to_record_search_new'
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/people_to_record/:user_id/new', to: 'he_ct_module_privilages#people_to_record_new', via: :get, as: 'people_to_record_new'
  match 'he_ct_module_privilages/people_to_record/:sc_user_to_register_id/delete/:go_to', to: 'he_ct_module_privilages#people_to_record_delete', via: :get, as: 'people_to_record_delete'
  #PEOPLE TO VALIDATE
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/people_to_validate/search_people', to: 'he_ct_module_privilages#people_to_validate_search_new', via: :get, as: 'people_to_validate_search_new'
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/people_to_validate/search_people', to: 'he_ct_module_privilages#people_to_validate_search_new', via: :post, as: 'people_to_validate_search_new'
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/people_to_validate/:user_id/new', to: 'he_ct_module_privilages#people_to_validate_new', via: :get, as: 'people_to_validate_new'
  match 'he_ct_module_privilages/people_to_validate/:sc_user_to_validate_id/delete', to: 'he_ct_module_privilages#people_to_validate_delete', via: :get, as: 'people_to_validate_delete'
  #RECORDERS TO VALIDATE
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/recorders_to_validate/search_people', to: 'he_ct_module_privilages#recorders_to_validate_search_new', via: :get, as: 'recorders_to_validate_search_new'
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/recorders_to_validate/search_people', to: 'he_ct_module_privilages#recorders_to_validate_search_new', via: :post, as: 'recorders_to_validate_search_new'
  match 'he_ct_module_privilages/:he_ct_module_privilage_id/recorders_to_validate/:user_id/new', to: 'he_ct_module_privilages#recorders_to_validate_new', via: :get, as: 'recorders_to_validate_new'
  match 'he_ct_module_privilages/recorders_to_validate/:sc_user_to_validate_id/delete', to: 'he_ct_module_privilages#recorders_to_validate_delete', via: :get, as: 'recorders_to_validate_delete'
  #USERS TO REGISTERS
  match 'he_ct_module_privilages/user_to_register_managment/:user_id/:tab', to: 'he_ct_module_privilages#user_to_register_managment', via: :get, as: 'user_to_register_managment'
  match 'he_ct_module_privilages/user_to_register_toggle_he_able/:user_id', to: 'he_ct_module_privilages#user_to_register_toggle_he_able', via: :get, as: 'user_to_register_toggle_he_able'
  match 'he_ct_module_privilages/user_to_register_toggle_flexible_sc/:user_id', to: 'he_ct_module_privilages#user_to_register_toggle_flexible_sc', via: :get, as: 'user_to_register_toggle_flexible_sc'

  match 'he_ct_module_privilages/user_to_register/:user_id/user_to_register_search_recorders', to: 'he_ct_module_privilages#user_to_register_search_recorders', via: :get, as: 'user_to_register_search_recorders'
  match 'he_ct_module_privilages/user_to_register/:user_id/user_to_register_search_recorders', to: 'he_ct_module_privilages#user_to_register_search_recorders', via: :post, as: 'user_to_register_search_recorders'
  match 'he_ct_module_privilages/user_to_register/:user_id/user_to_register_vinculate_recorder/:he_ct_module_privilage_id', to: 'he_ct_module_privilages#user_to_register_vinculate_recorder', via: :get, as: 'user_to_register_vinculate_recorder'

  #Ben_cellphone.processes.1
  match 'ben_cellphone_processes/show/bills_detail/:ben_user_cellphone_process_id', to: 'ben_cellphone_processes#bills_detail', via: :get, as: 'bills_detail'
  match 'ben_cellphone_processes/show/not_charge_to_payroll_modal/:ben_cellphone_bill_item_id', to: 'ben_cellphone_processes#not_charge_to_payroll_modal', via: :get, as: 'not_charge_to_payroll_modal'
  match 'ben_cellphone_processes/show/not_charge_to_payroll_modal/step_2/:ben_cellphone_bill_item_id', to: 'ben_cellphone_processes#not_charge_to_payroll_modal_step2', via: :post, as: 'not_charge_to_payroll_modal_step2'
  match 'ben_cellphone_processes/show/charge_to_pauroll/:ben_cellphone_bill_item_id', to: 'ben_cellphone_processes#charge_to_payroll', via: :get, as: 'charge_to_payroll'
  match 'ben_cellphone_processes/show/add/bill_item_modal/:ben_user_cellphone_process_id', to: 'ben_cellphone_processes#add_bill_item_modal', via: :get, as: 'add_bill_item_modal'
  match 'ben_cellphone_processes/show/add/bill_item_modal/step_2/update/:ben_user_cellphone_process_id', to: 'ben_cellphone_processes#add_bill_item_modal_step2', via: :post, as: 'add_bill_item_modal_step2'
  match 'ben_cellphone_processes/update_to_payroll/:ben_cellphone_process_id', to: 'ben_cellphone_processes#update_messages_to_payroll', via: :get, as: 'update_messages_to_payroll'
  match 'ben_cellphone_processes/show/:ben_cellphone_process_id/:tab', to: 'ben_cellphone_processes#show', via: :get, as: 'show_ben_cellphone_process'
  match 'ben_cellphone_processes/validate_bill_items/:ben_cellphone_process_id', to: 'ben_cellphone_processes#to_validation', via: :post, as: 'ben_cellphone_processes_validation'
  match 'ben_cellphone_processes/update/:ben_cellphone_process_id', to: 'ben_cellphone_processes#update', via: :put, as: 'update_cellphone_process'
  match 'ben_cellphone_processes/edit/:ben_cellphone_process_id', to: 'ben_cellphone_processes#edit', via: :get, as: 'edit_cellphone_process'

  #BEN REPORT
  match 'ben_reports/index', to: 'ben_report#index', via: :get, as: 'ben_custom_reports_index'
  match 'ben_reports/new', to: 'ben_report#new', via: :get, as: 'ben_custom_reports_new'
  match 'ben_reports/create', to: 'ben_report#create', via: :post, as: 'ben_custom_reports_create'
  match 'ben_reports/load_characteristics_values_list', to: 'ben_report#load_characteristics_values_list', via: :post, as: 'load_characteristics_values_list_from_ben_report'
  match 'ben_reports/edit/:ben_custom_report_id', to: 'ben_report#edit', via: :get, as: 'ben_custom_reports_edit'
  match 'ben_reports/update/:ben_custom_report_id', to: 'ben_report#update', via: :post, as: 'ben_custom_reports_update'
  match 'ben_reports/delete/:ben_custom_report_id', to: 'ben_report#delete', via: :get, as: 'ben_custom_reports_delete'
  match 'ben_reports/generate_custom_report/:ben_custom_report_id/users/:only_active/process/:ben_cellphone_process_id', to: 'ben_report#gen_custom_report', via: :get, as: 'ben_custom_report_gen'
  match 'ben_reports/list_reports', to: 'ben_report#list_reports', via: :get, as: 'ben_custom_reports_list_reports'


  #BEN_USER_CUSTOM_REPORTS
  match 'ben_user_custom_report/index', to:'ben_user_custom_report#index', via: :get, as:'ben_user_custom_report_index'
  match 'ben_user_custom_report/index', to:'ben_user_custom_report#index', via: :post, as:'ben_user_custom_report_index'
  match 'ben_user_custom_report/edit_user_privileges/report/:ben_custom_report_id/user/:user_id', to:'ben_user_custom_report#edit_user_privileges', via: :get, as:'ben_user_custom_report_edit'


  #schedules_process Register attendance
  match 'sc_processes/:sc_process_id/register_attendance/:tab', to: 'sc_processes_registers#register_attendance', via: :get, as: 'register_attendance'
  match 'sc_processes/send_single_record/:sc_record_id/to_validation', to: 'sc_processes_registers#send_single_record_to_validation', via: :get, as: 'send_single_record_to_validation'


  match 'sc_processes/:sc_process_id/register_attendance_of/:user_id', to: 'sc_processes_registers#new_record', via: :get, as: 'new_record'
  match 'sc_processes/:sc_process_id/register_attendance_of/:user_id/create_record', to: 'sc_processes_registers#create_record', via: :post, as: 'create_record'
  match 'sc_processes/:sc_process_id/update_attendance_of/:user_id/update_record/:sc_record_id/:whoami', to: 'sc_processes_registers#update_record', via: :post, as: 'recorder_update_record'
  match 'sc_processes/:sc_process_id/new_record_of/:user_id/new_record_form', to: 'sc_processes_registers#new_record_form', via: :get, as: 'new_record_form'


  match 'sc_processes/delete_sc_record/:sc_record_id/:tab', to: 'sc_processes_registers#delete_record', via: :get, as: 'delete_record'
  match 'sc_processes/make_it_editable/:sc_record_id', to: 'sc_processes_registers#make_it_editable', via: :get, as: 'make_it_editable'
  match 'sc_processes/:sc_process_id/refresh/falta_enviar_a_validar_datatable', to: 'sc_processes_registers#refresh_falta_enviar_a_validar_datatable', via: :get, as: 'refresh_falta_enviar_a_validar_datatable'

  match 'sc_processes/:sc_process_id/send_records_to_validation', to: 'sc_processes_registers#send_records_to_validation', via: :get, as: 'send_records_to_validation'
  match 'sc_processes/:sc_process_id/search_people_register_record', to: 'sc_processes_registers#search_people_register_record', via: :get, as: 'search_people_register_record'
  match 'sc_processes/:sc_process_id/search_people_register_record', to: 'sc_processes_registers#search_people_register_record', via: :post, as: 'search_people_register_record'
  match 'sc_processes/:sc_process_id/go_to_register_attendances_by_date', to: 'sc_processes_registers#go_to_register_attendances_by_date', via: :post, as: 'go_to_register_attendances_by_date'

  match 'sc_processes/add_user_to_session/:user_id/to_process/:sc_process_id', to: 'sc_processes_registers#add_user_to_session', via: :get, as: 'add_user_to_session'
  match 'sc_processes/record/:sc_record_id/get_non_editable_form/:whoami', to: 'sc_processes_registers#get_non_editable_form', via: :get, as: 'get_non_editable_sc_record_form'
  match 'sc_processes/record/:sc_record_id/get_editable_form/:whoami', to: 'sc_processes_registers#get_editable_form', via: :get, as: 'get_editable_sc_record_form'
  match 'sc_processes/record/:sc_record_id/get_deleted_form/:whoami', to: 'sc_processes_registers#get_deleted_form', via: :get, as: 'get_deleted_sc_record_form'



  match 'sc_processes/datatable/falta_por_validar/:sc_process_id', to: 'sc_processes_registers#falta_por_validar_attendances', via: :get, as: 'sc_falta_por_validar_attendances'
  match 'sc_processes/datatable/falta_enviar_a_validacion/:sc_process_id', to: 'sc_processes_registers#falta_enviar_a_validacion_attendances', via: :get, as: 'sc_falta_enviar_a_validacion'
  #schedules_process Validate attendance
  match 'sc_processes/:sc_process_id/validate_attendance', to: 'sc_processes_validates#validate_attendance', via: :get, as: 'validate_attendance'
  match 'sc_processes/update_record/:sc_record_id/status/:status/:index', to: 'sc_processes_validates#update_status', via: :get, as: 'update_status'
  match 'sc_processes/:sc_process_id/refresh_validation_table', to: 'sc_processes_validates#refresh_validation_table', via: :get, as: 'refresh_validation_table'
  match 'sc_processes/reject_attendance/:sc_record_id/description', to: 'sc_processes_validates#reject_attendance', via: :post, as: 'reject_attendance'
  match 'sc_processes/approve_attendance/:sc_record_id', to: 'sc_processes_validates#approve_attendance', via: :get, as: 'approve_attendance'
  match 'sc_processes/:sc_process_id/show_reject_attendance_modal', to: 'sc_processes_validates#show_reject_attendance_modal', via: :get, as: 'show_reject_attendance_modal'
  match 'sc_processes/back_to_edit_record/:sc_record_id', to: 'sc_processes_validates#back_to_edit', via: :get, as: 'back_to_edit_sc_record'



  #schedules process
  match 'sc_processes/new/record/:sc_process_id', to: 'sc_processes#new_record', via: :get, as: 'new_record'
  match 'sc_processes/new_process/annually', to: 'sc_processes#new_annually', via: :get, as: 'new_annually'
  match 'sc_processes/create_process/annually', to: 'sc_processes#create_annually', via: :post, as: 'create_annually'
  match 'sc_processes/refresh/records_list/:sc_process_id', to: 'sc_processes#show_records_list', via: :get, as: 'show_records_list'
  # match 'sc_processes/create/record/:sc_process_id/:user_id', to: 'sc_processes#create_record', via: :post, as: 'create_record'
  match 'sc_processes/to_validation/records_from/:sc_process_id', to: 'sc_processes#records_to_validation', via: :get, as: 'records_to_validation'
  match 'sc_processes/update_process/:sc_process_id', to: 'sc_processes#update', via: :post, as: 'update_schedule_process'
  match 'sc_processes/edit_process/:sc_process_id', to: 'sc_processes#edit', via: :get, as: 'edit_schedule_process'

  #SC PROCESSES EXCEL
  match 'sc_processes/:sc_process_id/sc_process_he_report', to: 'sc_processes#sc_process_he_report', as:'sc_process_he_report'
  match 'sc_processes/:sc_process_id/sc_process_he_report_partial', to: 'sc_processes#sc_process_he_report_partial', as:'sc_process_he_report_partial'
  match 'sc_processes/validation_report/:sc_process_id', to: 'sc_processes#validation_report', via: :get, as: 'sc_process_validation_report'


  #HOLIDAYS
  match 'holidays/index/all/:tab', to: 'holidays#index', as: 'holiday_index'
  match 'holidays/new/global_date', to: 'holidays#new_global_date', as: 'holiday_new_global_date'
  match 'holidays/create/global_date', to: 'holidays#create_global_date', via: :post, as: 'holiday_create_global_date'
  match 'holidays/delete/:holiday_id', to: 'holidays#delete_global_date', as: 'holiday_delete_global_date'

  match 'holidays/manage/holiday/:holiday_id', to: 'holidays#manage', via: :get, as: 'holiday_manage'
  match 'holidays/edit/holiday/:holiday_id', to: 'holidays#edit', via: :get, as: 'holiday_edit'
  match 'holidays/update/holiday/:holiday_id', to: 'holidays#update', via: :post, as: 'holiday_update'


  #HOLIDAYS_USERS
  # match 'holiday_users/search_people_for/new_holiday', to: 'holiday_users#new_users', via: [:get, :post], as: 'holiday_users_new_users'
  # match 'holiday_users/new_holiday/users_type/:user_id', to: 'holiday_users#new_users_step2', via: :post, as: 'holiday_users_new_users_step2'
  # match 'holiday_users/create_holiday/users_type/:user_id', to: 'holiday_users#create_users_type', via: :post, as: 'holiday_users_create_users_type'

  match 'holiday_users/delete_assoc/:holiday_user_id', to: 'holiday_users#delete_assoc', as: 'holiday_users_delete_assoc'

  match 'holiday_users/search_people_for/assoc_new_user/:holiday_id', to: 'holiday_users#assoc_new_user', via: [:get, :post], as: 'holiday_users_assoc_new_user'
  match 'holiday_users/assoc_new_user/:user_id/step2/for_holiday/:holiday_id', to: 'holiday_users#assoc_new_user_step2', via: :post, as: 'holiday_users_assoc_new_user_step2'

  #HOLIDAYS_CHARACTERISTICS
  match 'holiday_characteristics/new/characteristics', to: 'holiday_characteristics#new', via: :get, as: 'holiday_characteristics_new_characteristics_date'
  match 'holiday_characteristics/create/characteristics', to: 'holiday_characteristics#create', via: :post, as: 'holiday_characteristics_create_characteristics_date'

  match 'holiday_characteristics/new/hol_condition/:holiday_id', to: 'holiday_characteristics#new_hol_condition', via: :post, as: 'holiday_characteristics_new_hol_condition'
  match 'holiday_characteristics/create/hol_condition/:holiday_id', to: 'holiday_characteristics#create_hol_condition', via: :post, as: 'holiday_characteristics_create_hol_condition'

  match 'holiday_characteristics/edit/:holiday_characteristic_id', to: 'holiday_characteristics#edit_hol_condition', via: :post, as: 'holiday_characteristics_edit_hol_condition'
  match 'holiday_characteristics/update/:holiday_characteristic_id', to: 'holiday_characteristics#update_hol_condition', via: :post, as: 'holiday_characteristics_update_hol_condition'
  match 'holiday_characteristics/delete/:holiday_characteristic_id', to: 'holiday_characteristics#delete_hol_condition', via: :post, as: 'holiday_characteristics_delete_hol_condition'


  match 'holiday_characteristics/match_form', to: 'holiday_characteristics#match_form', via: :post, as: 'holiday_characteristics_match_form'
  match 'holiday_characteristics/row_show', to: 'holiday_characteristics#row_show', via: :post, as: 'holiday_characteristics_row_show'


  #Schedules
  resources :sc_schedules
  match 'sc_schedules/schedule_users/:sc_schedule_id/search_people', to: 'sc_schedules#search_people_create_schedule_user', via: :get, as: 'search_people_create_schedule_user'
  match 'sc_schedules/schedule_users/:sc_schedule_id/search_people', to: 'sc_schedules#search_people_create_schedule_user', via: :post, as: 'search_people_create_schedule_user'
  # match 'sc_schedules/schedule_users/create_manually/:sc_schedule_id/:user_id', to:'sc_schedules#schedule_users_create_manually', via: :get, as: 'schedule_users_create_manually'

  match 'sc_schedules/block/schedule/:sc_schedule_id/block_new_modal', to: 'sc_schedules#block_new_modal', via: :get, as: 'block_new_modal'
  match 'sc_schedules/block/schedule/:sc_schedule_id/block_create', to: 'sc_schedules#block_create', via: :post, as: 'block_create'
  match 'sc_schedules/block/:sc_block_id/edit', to: 'sc_schedules#block_edit', via: :get, as: 'block_edit'
  match 'sc_schedules/block/:sc_block_id/update', to: 'sc_schedules#block_update', via: :post, as: 'block_update'
  match 'sc_schedules/block/:sc_block_id/deactivate', to: 'sc_schedules#block_deactivate', via: :post, as: 'block_deactivate'


  # match 'sc_schedules/block/:sc_block_id/show_edit_block_modal', to 'sc_schedules#show_edit_block_modal', via: :post, as: 'show_edit_block_modal'
  # match 'sc_schedules/block/:sc_block_id/update_block', to 'sc_schedules#update_block_from_modal', via: :post, as: 'update_block_from_modal'
  # match 'sc_schedules/block/schedule/:sc_schedule_id/show_new_block_modal', to 'sc_schedules#show_new_block_modal', via: :post, as: 'show_edit_block_modal'


  match 'sc_schedules/create/schedule', to: 'sc_schedules#create_schedule', via: :post, as: 'create_schedule'
  match 'sc_schedules/update/schedule/:sc_schedule_id', to: 'sc_schedules#update_schedule', via: :post, as: 'update_schedule'
  match 'sc_schedules/edit/schedule/:sc_schedule_id', to: 'sc_schedules#edit_schedule', via: :get, as: 'edit_schedule'


  match 'sc_schedules/refresh/blocks/:sc_schedule_id', to: 'sc_schedules#refresh_blocks', via: :get, as: 'refresh_blocks'
  match 'sc_schedules/refresh/conditions/:sc_schedule_id', to: 'sc_schedules#refresh_conditions', via: :get, as: 'refresh_conditions'
  # match 'sc_schedules/refresh/schedule_users/:sc_schedule_id', to:'sc_schedules#refresh_schedule_users', via: :get, as: 'refresh_schedule_users'
  # match 'sc_schedules/delete/schedule_users/:sc_schedule_user_id', to:'sc_schedules#delete_schedule_users', via: :get, as: 'delete_schedule_users'


  match 'sc_schedules/condition/create', to: 'sc_schedules#create_condition', via: :post, as: 'sc_schedules_create_condition'
  match 'sc_schedules/manage/:schedule_id/:pill', to: 'sc_schedules#manage_schedule', via: :get, as: 'manage_schedule'
  match 'sc_schedules/condition/update/:sc_characteristic_condition_id', to: 'sc_schedules#update_condition', via: :post, as: 'sc_schedules_update_condition'
  match 'sc_schedules/condition/delete/:sc_characteristic_condition_id', to: 'sc_schedules#delete_condition', via: :get, as: 'sc_schedules_delete_condition'
  match 'sc_schedules/condition/create', to: 'sc_schedules#create_condition', via: :post, as: 'sc_schedules_create_condition'
  match 'sc_schedules/block/download_excel/only_headers', to: 'sc_schedules#blocks_download_excel_headers', as: 'sc_schedule_blocks_download_excel_headers'
  match 'sc_schedules/block/massive_upload/:sc_schedule_id', to: 'sc_schedules#blocks_massive_upload', as: 'sc_schedule_blocks_massive_upload'
  match 'sc_schedules/block/create', to: 'sc_schedules#create_block', via: :post, as: 'sc_schedules_create_block'
  match 'sc_schedules/block/update/:sc_block_id', to: 'sc_schedules#update_block', via: :post, as: 'sc_schedules_update_block'

  match 'sc_schedules/match_users/schedule_users/:sc_schedule_id', to: 'he_ct_module_privilages#schedule_users_match_by_conditions', as: 'schedule_users_match_by_conditions'

  match 'sc_schedules/block/:block_id', to: 'sc_schedules#new_block', via: :post, as: 'sc_schedules_new_block'
  match 'sc_schedules/block/delete/:block_id', to: 'sc_schedules#delete_block', via: :get, as: 'sc_schedules_delete_block'

  match 'sc_schedules/load_characteristics_values_list', to: 'sc_schedules#load_characteristics_values_list', via: :post, as: 'load_characteristics_values_list'
  #match 'sc_schedules/massive_schedules_user_upload', to:'sc_schedules#massive_schedules_user_upload', via: :post, as:'massive_schedules_user_upload'

  #Ben_Cellphone_processes

  resources :ben_cellphone_processes
  match 'ben_cellphone_processes_complete_excel/:ben_cellphone_process_id', to: 'ben_cellphone_processes#complete_excel', as: 'ben_cellphone_processes_complete_excel'


  match 'ben_cellphone_processes/upload_plan/:ben_cellphone_process_id', to: 'ben_cellphone_processes#upload_plan', via: :post, as: 'ben_cellphone_processes_plan'
  match 'ben_cellphone_processes/upload_equipment_bill/:ben_cellphone_process_id', to: 'ben_cellphone_processes#upload_equipment_bill', via: :post, as: 'ben_cellphone_processes_equipment_bill'
  match 'ben_cellphone_processes/new_bill_item/:ben_cellphone_process_id/:ben_user_cellphone_process_id', to: 'ben_cellphone_processes#add_bill_item', via: :get, as: 'ben_cellphone_processes_add_bill_item'
  match 'ben_cellphone_processes/new_bill_item/step2/:ben_cellphone_process_id/:ben_user_cellphone_process_id', to: 'ben_cellphone_processes#add_bill_item2', via: :post, as: 'ben_cellphone_processes_add_bill_item2'
  match 'ben_cellphone_processes/change_charge_to_payroll/:ben_user_cellphone_process_id/:ben_cellphone_bill_item_id', to: 'ben_cellphone_processes#charge_or_not_to_payroll', via: :post, as: 'ben_cellphone_processes_charge_or_not_to_payroll'
  match 'ben_cellphone_processes/change_charge_to_payroll/:ben_cellphone_bill_item_id', to: 'ben_cellphone_processes#charge_or_not_to_payroll', via: :get, as: 'ben_cellphone_processes_charge_or_not_to_payroll_no_match'
  match 'ben_cellphone_processes/not_charge_to_payroll/bill_item/:ben_cellphone_bill_item_id', to: 'ben_cellphone_processes#not_charge_to_payroll', via: :post, as: 'ben_cellphone_processes_not_charge_to_payroll'
  match 'ben_cellphone_processes/delete_process_data/:ben_cellphone_process_id', to: 'ben_cellphone_processes#delete_data', via: :get, as: 'ben_cellphone_processes_delete_data'
  match 'ben_cellphone_processes/excel/errors_list/:ben_cellphone_process_id', to: 'ben_cellphone_processes#no_matched_bills_errors_excel', as: 'ben_cellphone_no_matched_errors'


  match 'ben_cellphone_processes/resolve_error/:ben_cellphone_process_id/:ben_cellphone_bill_item_id/:no_match_error', to: 'ben_cellphone_processes#no_match_errors', via: :get, as: 'ben_cellphone_processes_no_match_errors'
  match 'ben_cellphone_processes/change_process_status/:ben_cellphone_process_id', to: 'ben_cellphone_processes#close_open_process', via: :get, as: 'ben_cellphone_processes_close_open_process'
  match 'ben_cellphone_processes/charge_to_payroll/:ben_cellphone_process_id', to: 'ben_cellphone_processes#charge_to_payroll_excel', as: 'ben_cellphone_processes_charge_to_payroll_excel'
  match 'ben_cellphone_processes/show/validates/:ben_cellphone_bill_item_id/:ben_user_cellphone_process_id', to: 'ben_cellphone_processes#show', as: 'ben_cellphone_process_just_added_bill_item'
  match 'ben_cellphone_processes/show/:ben_cellphone_bill_item_id', to: 'ben_cellphone_processes#show', as: 'ben_cellphone_process_just_added_bill_item2'
  match 'ben_cellphone_processes/show/:ben_cellphone_process_id/:payroll', to: 'ben_cellphone_processes#show', as: 'ben_cellphone_process_show_payroll'
  match 'ben_cellphone_processes/registered_manually/to_payroll/:ben_cellphone_process_id', to: 'ben_cellphone_processes#registered_manually_excel', as: 'ben_cellphone_processes_registered_manually_excel'


  #match 'ben_cellphone_processes/new_benefit/:bill_item_id', to: 'ben_cellphone_numbers#new', via: :post, as:'ben_cellphone_processes_new_benefit'
  #match 'ben_cellphone_processes/activate_benefit/:bill_item_id', to: 'ben_cellphone_numbers#edit', via: :post, as:'ben_cellphone_processes_activate_benefit'


  #ben_cellphone_number
  resources :ben_cellphone_numbers

  match 'ben_cellphone_numbers/download_excel/only_headers', to: 'ben_cellphone_numbers#download_excel_headers', as: 'ben_cellphone_numbers_download_excel_headers' #Del link de subir archivos a descargar cabeceras del excel de beneficios
  match 'ben_cellphone_numbers/:from_to/select_user', to: 'ben_cellphone_numbers#search', via: :post, as: 'ben_cellphone_numbers_create_benefit_user_search' #Crear beneficio en index
  match 'ben_cellphone_numbers/:from_to/select_user/:ben_cellphone_number_id', to: 'ben_cellphone_numbers#search', via: :post, as: 'ben_cellphone_numbers_search_user' #form search.html.erb
  match 'ben_cellphone_number/create_benefit/step2/:user_id', to: 'ben_cellphone_numbers#new', via: :post, as: 'new_ben_cellphone_numbers_create_benefit_user_id' #Link en search: create benefit
  match 'ben_cellphone_number/modify_benefit/:ben_cellphone_number_id/change_user/:user_id', to: 'ben_cellphone_numbers#show', via: :post, as: 'ben_cellphone_numbers_modify_user_show' #Link en search: change user
  match 'ben_cellphone_numbers/create/step2/:user_id', to: 'ben_cellphone_numbers#create', via: :post, as: 'create_ben_cellphone_number_user_cod' #_form va a create
  match 'ben_cellphone_numbers/:from_to/:ben_cellphone_number_id', to: 'ben_cellphone_numbers#search', via: :post, as: 'ben_cellphone_numbers_search_change_user' #_show va a search: change user
  match 'ben_cellphone_numbers/form_for/update_benefit/:ben_cellphone_number_id/:user_id', to: 'ben_cellphone_numbers#update', via: :post, as: 'ben_cellphone_numbers_update_benefit' #_form_update a update (form nuevo)
  match 'ben_cellphone_numbers/form_for/create_benefit/:user_id', to: 'ben_cellphone_numbers#create', via: :post, as: 'ben_cellphone_numbers_create_benefit' #_form_create a create (form nuevo)

  match 'ben_cellphone_number_complete_excel', to: 'ben_cellphone_numbers#complete_excel'
  match 'ben_cellphone_number_excel', to: 'ben_cellphone_numbers#export_to_excel'
  match 'ben_massive_update', to: 'ben_cellphone_numbers#ben_massive_update'
  match 'ben_massive_update_aux', to: 'ben_cellphone_numbers#ben_massive_update_aux'

  #data_sync
  match 'data_sync/read_users/:diff_days', to: 'data_syncs#sync_read_users', via: :get, as: 'data_sync_read_users'
  match 'data_sync/read_nodes/:diff_days', to: 'data_syncs#sync_read_nodes', via: :get, as: 'data_sync_read_nodes'
  match 'data_sync/read_users_photos/:diff_days', to: 'data_syncs#sync_read_users_photos', via: :get, as: 'data_sync_read_users_photos'

  match 'data_sync/generate_active_users_register_characteristic', to: 'data_syncs#sync_generate_active_users_register_characteristic', via: :get, as: 'data_sync_generate_active_users_register_characteristic'
  match 'data_sync/generate_active_users_training', to: 'data_syncs#sync_generate_active_users_training', via: :get, as: 'data_sync_generate_active_users_training'


  #match 'users/reps/rep_active_users_register_sync_csv/:characteristic_id', to: 'users#rep_active_users_register_sync_csv', via: :get, as: 'users_rep_active_users_register_sync_csv'
  #match 'users/reps/rep_active_users_training_sync_csv', to: 'users#rep_active_users_training_sync_csv', via: :get, as: 'users_rep_active_users_training_sync_csv'

  resources :tracking_form_instances, only: [:update, :show]
  match 'tracking_form_instances/f/:tracking_form_id/p/:tracking_participant_id', to: 'tracking_form_instances#create_from_admin', via: :post, as: 'new_tracking_form_instance_admin'
  match 'tracking_form_instances/:id/mfe', to: 'tracking_form_instances#make_form_editable', via: :get, as: 'tracking_form_instance_make_form_editable'
  match 'tracking_processes/:tracking_process_id/index', to: 'tracking_form_instances#index', via: :get, as: 'tracking_form_instances'
  match 'tracking_milestone/:tracking_milestone_id/delete', to: 'tracking_form_instances#destroy_milestone', via: :post, as: 'tracking_form_instance_destroy_milestone'
  match 'tracking/:tracking_participant_id', to: 'tracking_form_instances#show', via: :get, as: 'tracking_participant_track'
  match 'tracking_dd/:tracking_form_due_date_id/add_milestone', to: 'tracking_form_instances#add_milestone', via: :put, as: 'tracking_form_dd_add_milestone'
  match 'tracking_form_instances/:id/otredef', to: 'tracking_form_instances#open_instance_for_redefinition', via: :get, as: 'tracking_form_instance_open_instance_for_redefinition'
  match 'tracking_form_instances/:id/rolredef', to: 'tracking_form_instances#rollback_redefinition', via: :get, as: 'tracking_form_instance_rollback_redefinition'
  match 'tracking_form_instances/:id/cloredef', to: 'tracking_form_instances#close_redefinition', via: :get, as: 'tracking_form_instance_close_redefinition'
  match 'tracking_form_instances/:id/modal_view/:modal', to: 'tracking_form_instances#show', via: :post, as: 'tracking_form_instance_show_in_modal'
  match 'tracking/:tracking_participant_id/modal_view/:modal', to: 'tracking_form_instances#show', via: :post, as: 'tracking_participant_track_in_modal'

  resources :tracking_form_groups, except: [:index, :new, :show]
  match 'tracking_form_groups/f/:tracking_form_id', to: 'tracking_form_groups#index', via: :post, as: 'tracking_form_groups_list'
  match 'tracking_form_groups/f/:tracking_form_id/new', to: 'tracking_form_groups#new', via: :get, as: 'new_tracking_form_group'


  resources :tracking_form_states, except: [:index, :new, :show]
  match 'tracking_form_states/f/:tracking_form_id', to: 'tracking_form_states#index', via: :post, as: 'tracking_form_states_list'
  match 'tracking_form_states/f/:tracking_form_id/new', to: 'tracking_form_states#new', via: :get, as: 'new_tracking_form_state'
  match 'tracking_form_states/:id/manage_notifications', to: 'tracking_form_states#manage_notifications', via: :get, as: 'tracking_form_state_manage_notifications'


  resources :tracking_form_actions, except: [:index, :new, :show]
  match 'tracking_form_actions/f/:tracking_form_id', to: 'tracking_form_actions#index', via: :post, as: 'tracking_form_actions_list'
  match 'tracking_form_actions/f/:tracking_form_id/new', to: 'tracking_form_actions#new', via: :get, as: 'new_tracking_form_action'
  match 'tracking_form_actions/:id/manage_notifications', to: 'tracking_form_actions#manage_notifications', via: :get, as: 'tracking_form_action_manage_notifications'


  resources :tracking_forms, except: [:index, :new]
  match 'tracking_forms/p/:tracking_process_id', to: 'tracking_forms#index', via: :post, as: 'tracking_forms_list'
  match 'tracking_forms/p/:tracking_process_id/new', to: 'tracking_forms#new', via: :get, as: 'new_tracking_form'
  match 'tracking_forms/:id/p/:pill_open', to: 'tracking_forms#show', via: :get, as: 'tracking_form_pill_open'


  resources :tracking_lists


  resources :tracking_processes
  match 'tracking_processes/:id/show_load_participants_csv', to: 'tracking_processes#show_upload_participants_csv', via: :post, as: 'tracking_process_show_load_participants_csv'
  match 'tracking_processes/:id/upload_participants_csv', to: 'tracking_processes#upload_participants_csv', via: :post, as: 'tracking_process_upload_participants_csv'
  match 'tracking_processes/:id/list_config_report', to: 'tracking_processes#list_config_report', via: :post, as: 'tracking_process_list_config_report'
  match 'tracking_processes/:id/show_config_report', to: 'tracking_processes#show_config_report', via: :get, as: 'tracking_process_show_config_report'
  match 'tracking_processes/:id/toggle_config_report/:tracking_form_item_id', to: 'tracking_processes#toggle_config_report', via: :get, as: 'tracking_process_toggle_config_report'
  match 'tracking_processes/:id/show_config_chars', to: 'tracking_processes#show_config_chars', via: :get, as: 'tracking_process_show_config_chars'
  match 'tracking_processes/:id/config_chars', to: 'tracking_processes#config_chars', via: :put, as: 'tracking_process_config_chars'

  match 'tracking_processes/:id/clone_form', to: 'tracking_processes#show_clone_form', via: :get, as: 'tracking_process_show_clone_form'
  match 'tracking_processes/:id/clone_form/p/:pill_open', to: 'tracking_processes#show_clone_form', via: :get, as: 'tracking_process_show_clone_form_pill_open'
  match 'tracking_processes/:id/clone/structure_form', to: 'tracking_processes#show_clone_structure', via: :get, as: 'tracking_process_show_clone_structure'
  match 'tracking_processes/:id/clone/data_form', to: 'tracking_processes#show_clone_data', via: :get, as: 'tracking_process_show_clone_data'
  match 'tracking_processes/:id/clone/confirmation_form', to: 'tracking_processes#show_clone_confirmation', via: :get, as: 'tracking_process_show_clone_confirmation'
  match 'tracking_processes/:id/clone/save_structure', to: 'tracking_processes#save_clone_structure', via: :post, as: 'tracking_process_save_clone_structure'
  match 'tracking_processes/:id/clone/get_options_for_form/:form_id', to: 'tracking_processes#get_options_for_form', via: :post, as: 'tracking_process_get_options_for_form'
  match 'tracking_processes/:id/clone/save_data', to: 'tracking_processes#save_clone_data', via: :post, as: 'tracking_process_save_clone_data'
  match 'tracking_processes/:id/clone/execute_clone', to: 'tracking_processes#clone_process', via: :post, as: 'tracking_process_clone_process'

  match 'tracking_processes/:id/p/:pill_open', to: 'tracking_processes#show', via: :get, as: 'tracking_process_pill_open'
  match 'tracking_processes/:id/participants/:tracking_participant_id', to: 'tracking_processes#remove_couple_participants', via: :delete, as: 'tracking_process_remove_participant'
  match 'tracking_processes/:id/participants/:tracking_participant_id/reset', to: 'tracking_processes#reset_couple_participants', via: :get, as: 'tracking_process_reset_participant'
  match 'tracking_processes/m/list', to: 'tracking_processes#index_manager', via: :get, as: 'tracking_processes_index_manager'
  match 'tracking_processes/:id/subject_answers_report', to: 'tracking_processes#subject_answers_report', via: :get, as: 'tracking_processes_subject_answers_report'


  resources :tracking_participants, except: [:index, :new, :create]
  match 'tracking_participants/p/:tracking_process_id/new', to: 'tracking_participants#new', via: :get, as: 'new_tracking_participant'
  match 'tracking_participants/p/:tracking_process_id', to: 'tracking_participants#create', via: :post, as: 'create_tracking_participant'
  match 'tracking_participants/p/:tracking_process_id/', to: 'tracking_participants#index', via: :get, as: 'tracking_participants'
  match 'tracking_participants/p/:tracking_process_id/status', to: 'tracking_participants#report_status', via: :get, as: 'tracking_managing_status'
  match 'tracking_participants/:id/reset', to: 'tracking_participants#reset', via: :get, as: 'tracking_participants_reset'


  resources :pe_member_observers


  resources :stored_images


  resources :sel_req_processes, except: [:new, :create]
  match 'sel_requirements/:id/create_process', to: 'sel_requirements#generate_approval_process', via: :get, as: 'sel_requirements_generate_approval_process'
  match 'sel_req_processes/:id/start', to: 'sel_req_processes#start', via: :get, as: 'sel_req_process_start'
  match 'sel_req_processes/eval/to_eval', to: 'sel_req_processes#index_for_approver', via: :get, as: 'sel_req_index_for_approver'
  match 'sel_req_processes/:id/eval', to: 'sel_req_processes#show_for_approver', via: :get, as: 'sel_req_show_for_approver'
  match 'sel_req_processes/:id/app/:sel_req_approver_id/eval', to: 'sel_req_processes#evaluate', via: :post, as: 'sel_req_approver_eval'
  match 'sel_req_processes/:id/app/:sel_req_approver_id/notify_assign', to: 'sel_req_processes#notify_assign_approver', via: :get, as: 'sel_req_approver_notify_assign'
  match 'sel_req_processes/:id/notify_all_assign', to: 'sel_req_processes#notify_assign_approver', via: :get, as: 'sel_req_process_notify_all_assign'
  match 'sel_req_processes/:id/edit/flow/:sel_vacant_flow_id', to: 'sel_req_processes#edit', via: :get, as: 'sel_req_process_edit_with_template_flow'
  match 'sel_req_processes/:id/close', to: 'sel_req_processes#close', via: :get, as: 'sel_req_process_close'
  match 'sel_req_processes/:id/restart', to: 'sel_req_processes#restart', via: :get, as: 'sel_req_process_restart'

  resources :sel_requirements, only: [:new, :create, :index, :edit, :update]
  match 'sel_requirements/:id/delete', to: 'sel_requirements#delete', via: :delete, as: 'sel_requirements_delete'
  match 'sel_requirements/:id/reject', to: 'sel_requirements#reject', via: :post, as: 'sel_requirements_reject'
  match 'sel_requirements/mine', to: 'sel_requirements#mine', via: :get, as: 'sel_requirements_mine'
  match 'sel_requirements/unattended', to: 'sel_requirements#unattended', via: :get, as: 'sel_requirements_unattended'
  match 'sel_requirements/dl/:sel_requirement_value_id', to: 'sel_requirements#download_file', via: :get, as: 'sel_requirements_download'

  resources :sel_req_templates
  match 'sel_req_templates/:id/change_status', to: 'sel_req_templates#update_active_value', via: :get, as: 'sel_requirements_update_active_value'

  resources :sel_permissions
  match 'sel_permissions/massive/update', to: 'sel_permissions#massive_update', via: :get, as: 'sel_permissions_massive_update'
  match 'sel_permissions/massive/upload', to: 'sel_permissions#massive_upload', via: :post, as: 'sel_permissions_massive_upload'
  resources :sel_req_items
  match '/sel_req_item/add/:sel_req_template_id', to: 'sel_req_items#new', via: :get, as: 'add_item_to_req_template'


  resources :jm_lists


  resources :pe_reporters

  # jm_my_profile
  match 'job_market/my_profile', to: 'jm_my_profiles#show', via: :get, as: 'jm_my_profile'
  match 'job_market/my_profile/s/:section_id', to: 'jm_my_profiles#show', via: :get, as: 'jm_my_profile_section_open'
  match 'job_market/my_profile/edit', to: 'jm_my_profiles#edit', via: :get, as: 'edit_jm_my_profile'
  match 'job_market/my_profile/edit/s/:section_id', to: 'jm_my_profiles#edit', via: :get, as: 'edit_jm_my_profile_section_open'
  match 'job_market/my_profile/save', to: 'jm_my_profiles#save_profile', via: :post, as: 'jm_my_profile_save_profile'

  resources :jm_candidates, except: [:destroy, :index, :show, :edit]
  match 'job_market/register', to: 'jm_candidates#new', via: :get, as: 'jm_register'
  match 'job_market/register', to: 'jm_candidates#new', via: :post, as: 'jm_register_with_email'
  match 'job_market/login', to: 'jm_sessions#new', via: :get, as: 'jm_login'
  match 'job_market/login/create', to: 'jm_sessions#create', via: :post, as: 'jm_login_validation'
  match 'job_market/logout', to: 'jm_sessions#destroy', via: :get, as: 'jm_logout'
  match 'job_market/sign_in_user', to: 'jm_sessions#sign_in_user', via: :get, as: 'jm_sign_in_user'
  match 'job_market/terms_conditions', to: 'jm_sessions#download_terms_conditions', via: :get, as: 'jm_download_terms_conditions'
  match 'job_market/my_profile/basic_edit', to: 'jm_candidates#edit', via: :get, as: 'edit_jm_candidate'
  match 'job_market/my_profile/edit_email', to: 'jm_candidates#edit_email', via: :get, as: 'jm_candidate_edit_email'
  match 'job_market/my_profile/update_email_to_switch', to: 'jm_candidates#update_email_to_switch', via: :put, as: 'jm_candidate_update_email_to_switch'
  match 'job_market/my_profile/validate_email', to: 'jm_candidates#show_validate_email_to_switch', via: :get, as: 'jm_candidate_show_validate_email_to_switch'
  match 'job_market/my_profile/update_email', to: 'jm_candidates#update_email', via: :post, as: 'jm_candidate_update_email'

  match 'sel_candidates', to: 'candidates#index', via: :get, as: 'sel_candidates'
  match 'sel_candidates/:jm_candidate_id/', to: 'candidates#show', via: :get, as: 'sel_candidate'
  match 'sel_candidates/:jm_candidate_id/s/:section_id', to: 'candidates#show', via: :get, as: 'sel_candidate_section_open'
  match 'sel_candidates/:jm_candidate_id/process/:sel_process_id/', to: 'candidates#show', via: :get, as: 'sel_candidate_from_process'
  match 'sel_candidates/:jm_candidate_id/process/:sel_process_id/s/:section_id', to: 'candidates#show', via: :get, as: 'sel_candidate_from_process_section_open'
  match 'sel_candidates/:jm_candidate_id/edit', to: 'candidates#edit', via: :get, as: 'edit_sel_candidate'
  match 'sel_candidates/:jm_candidate_id/edit/s/:section_id', to: 'candidates#edit', via: :get, as: 'edit_sel_candidate_section_open'
  match 'sel_candidates/:jm_candidate_id/process/:sel_process_id/edit', to: 'candidates#edit', via: :get, as: 'edit_sel_candidate_from_process'
  match 'sel_candidates/:jm_candidate_id/process/:sel_process_id/edit/s/:section_id', to: 'candidates#edit', via: :get, as: 'edit_sel_candidate_from_process_section_open'
  match 'sel_candidates/:jm_candidate_id/show_process_to_apply', to: 'candidates#show_processes_to_apply', via: :get, as: 'sel_candidate_show_process_to_apply'
  match 'sel_candidates/:jm_candidate_id/update', to: 'candidates#update', via: :put, as: 'update_sel_candidate'
  match 'sel_candidates/:jm_candidate_id/process/:sel_process_id/update', to: 'candidates#update', via: :put, as: 'update_sel_candidate_from_process'
  match 'sel_candidates', to: 'candidates#index', via: :post, as: 'filter_sel_candidate'
  match 'sel_candidate/new', to: 'candidates#new', via: :get, as: 'new_sel_candidates'
  match 'sel_candidate/create', to: 'candidates#create', via: :post, as: 'create_sel_candidates'
  match 'sel_candidate/:jm_candidate_id/edit_basic', to: 'candidates#edit_basic_data', via: :get, as: 'edit_basic_data_sel_candidates'
  match 'sel_candidate/:jm_candidate_id/update_basic', to: 'candidates#update_basic_data', via: :put, as: 'update_basic_data_sel_candidates'
  match 'sel_candidate/:jm_candidate_id/add_comment', to: 'candidates#add_candidate_comment', via: :post, as: 'add_candidate_comment_sel_candidate'
  match 'sel_candidate/:jm_candidate_id/show_invite_form/:sel_process_id', to: 'candidates#show_invitation_form', via: :get, as: 'show_invitation_form_sel_candidate'
  match 'sel_candidate/:jm_candidate_id/invite/:sel_process_id', to: 'candidates#create_invitation', via: :post, as: 'invite_sel_candidate'

  match 'job_market/recuperar_pass1', to: 'static_pages#jm_recuperar_pass_paso1', via: :post, as: 'jm_recuperar_pass_paso1'
  match 'job_market/recuperar_pass2', to: 'static_pages#jm_recuperar_pass_paso2', via: :post, as: 'jm_recuperar_pass_paso2'
  match 'job_market/generate_pass/:jm_candidate_id', to: 'static_pages#jm_recuperar_pass_paso3', via: :get, as: 'jm_recuperar_pass_paso3'
  match 'job_market/regenerate_validation_code/:jm_candidate_id', to: 'static_pages#jm_regenerate_validation_code', via: :get, as: 'jm_regenerate_validation_code'
  match 'job_market/recuperar_pass4', to: 'static_pages#jm_recuperar_pass_paso4', via: :post, as: 'jm_recuperar_pass_paso4'

  resources :sel_processes
  match '/sel_process/step/:sel_process_step_id', to: 'sel_processes#show_detail_process_step', via: :post, as: 'sel_process_detail_step'
  match '/sel_process/search_users', to: 'sel_processes#search_users', via: :post, as: 'sel_process_search_users'
  match '/sel_process/:id/show_assign_users', to: 'sel_processes#show_assign_attendants',  via: :get, as: 'sel_process_show_assign_users'
  match '/sel_process/assign_attendants', to: 'sel_processes#assign_attendants',  via: :post, as: 'assign_attendants'
  match '/sel_processes/:id/s/:sel_step_id', to: 'sel_processes#show',  via: :get, as: 'sel_process_show_open_step'
  #match '/sel_processes/exonerate', to: 'sel_processes#exonerate_step_candidate',  via: :post, as: 'sel_process_exonerate'
  match '/sel_processes/exec_step_filter', to: 'sel_processes#execute_step_filter',  via: :post, as: 'sel_process_exec_step_filter'
  match '/sel_processes/:id/selection_step', to: 'sel_processes#show_detail_selection_step',  via: :post, as: 'sel_process_detail_selection_step'
  match '/sel_processes/:id/select', to: 'sel_processes#select_candidates',  via: :post, as: 'sel_process_select_candidates'
  match '/sel_processes/:id/validation_step', to: 'sel_processes#show_detail_validation_step',  via: :post, as: 'sel_process_detail_validation_step'
  match '/sel_processes/:id/validate', to: 'sel_processes#hire_candidates',  via: :post, as: 'sel_process_hire_candidates'
  match '/sel_processes/:id/applicants', to: 'sel_processes#show_detail_applicants_list',  via: :post, as: 'sel_process_detail_applicants_list'
  match '/sel_processes/:id/enroll', to: 'sel_processes#enroll_applicants_to_first_steps',  via: :post, as: 'sel_process_enroll_applicants'
  match '/sel_processes/new/:sel_requirement_id', to: 'sel_processes#new',  via: :get, as: 'new_sel_process_from_requirement'
  match '/sel_processes/:id/dl_file', to: 'sel_processes#download_file', via: :get, as: 'sel_sel_processes_download_file'

  match 'sel_processes/:id/attendants', to: 'sel_processes#show_assign_step_attendants', via: :get, as: 'sel_processes_show_assign_step_attendants'
  match 'sel_processes/:id/assign_attendants', to: 'sel_processes#assign_step_attendants', via: :post, as: 'sel_processes_assign_step_attendants'

  match 'sel_processes/:sel_applicant_id/undo_validation', to: 'sel_processes#undo_validation', via: :get, as: 'sel_processes_undo_validation'
  match 'sel_processes/:sel_applicant_id/undo_selection', to: 'sel_processes#undo_selection', via: :get, as: 'sel_processes_undo_selection'
  match 'sel_processes/:sel_step_candidate_id/undo_steps_from_filter', to: 'sel_processes#undo_steps_until_next_filter', via: :get, as: 'sel_processes_undo_steps_until_next_filter_from_filter'
  match 'sel_processes/:sel_applicant_id/undo_steps_from_applicant', to: 'sel_processes#undo_steps_until_next_filter', via: :get, as: 'sel_processes_undo_steps_until_next_filter_from_applicants'
  match 'sel_processes/:sel_applicant_id/remove', to: 'sel_processes#remove_applicant', via: :get, as: 'sel_processes_remove_applicant'
  match 'sel_processes/:sel_applicant_id/exclude', to: 'sel_processes#exclude_applicant', via: :get, as: 'sel_processes_exclude_applicant'
  match 'sel_processes/:sel_applicant_id/undo_exclude', to: 'sel_processes#undo_exclude_applicant', via: :get, as: 'sel_processes_undo_exclude_applicant'
  match 'sel_processes/:sel_applicant_id/notifiy_exclusion', to: 'sel_processes#notify_exclude_applicant', via: :get, as: 'sel_processes_notify_exclude_applicant'

  match 'sel_processes/:id/search_candidate', to: 'sel_processes#show_add_candidate_to_process', via: :get, as: 'sel_processes_show_add_candidate_to_process'
  match 'sel_processes/:id/search_internal_candidate', to: 'sel_processes#show_add_candidate_to_process', via: :post, as: 'sel_processes_show_add_internal_candidate_to_process'
  match 'sel_processes/:id/validate_email', to: 'sel_processes#validate_if_email_registered', via: :post, as: 'sel_processes_validate_if_email_registered'

  match 'sel_processes/:id/form_new_candidate/:email', to: 'sel_processes#new_candidate', via: :get, as: 'sel_processes_new_candidate', :constraints => { :email => /.+@.+\..*/ }
  match 'sel_processes/:id/create_candidate', to: 'sel_processes#create_candidate', via: :post, as: 'sel_processes_create_candidate'
  match 'sel_processes/:id/create_internal_candidate/:user_id', to: 'sel_processes#create_internal_candidate', via: :get, as: 'sel_processes_create_internal_candidate'

  match 'sel_processes/:id/apply_form/:jm_candidate_id', to: 'sel_processes#apply_form_candidate', via: :get, as: 'sel_processes_apply_form_candidate'
  match 'sel_processes/:id/enroll_candidate/:jm_candidate_id', to: 'sel_processes#enroll_candidate', via: :post, as: 'sel_processes_enroll_candidate'

  match 'sel_applicant/:sel_applicant_id/attach/new', to: 'sel_processes#show_attach_file_to_applicant', via: :get, as: 'sel_processes_show_attach_file_to_applicant'
  match 'sel_applicant/:sel_applicant_id/attach/add', to: 'sel_processes#attach_file_to_applicant', via: :post, as: 'sel_processes_attach_file_to_applicant'
  match 'sel_applicant/attach/:attachment_crypted_name/dl', to: 'sel_processes#download_attach_file_from_applicant', via: :get, as: 'sel_processes_download_attach_file_from_applicant'
  match 'sel_applicant/attach/:attachment_crypted_name/delete', to: 'sel_processes#delete_attach_file_from_applicant', via: :post, as: 'sel_processes_delete_attach_file_from_applicant'
  match 'sel_applicant/:sel_applicant_id/char/:jm_characteristic_id/sync', to: 'sel_processes#sync_applicant_char_answers', via: :get, as: 'sel_process_sync_applicant_char_answers'
  match 'sel_applicant/:sel_applicant_id/specific_answers_attach/:attachment_code/dl', to: 'sel_processes#download_specific_answer_attach_file_from_applicant', via: :get, as: 'sel_processes_download_specific_answer_attach_file_from_applicant'

  match 'sel_processes/:id/set_priority', to: 'sel_processes#show_set_priority', via: :get, as: 'sel_processes_show_set_priority'
  match 'sel_processes/:id/set_priority', to: 'sel_processes#set_priority', via: :post, as: 'sel_processes_set_priority'

  match 'sel_processes/:id/show_particular_agenda/:sel_step_candidate_id', to: 'sel_processes#show_config_particular_agenda', via: :post, as: 'sel_process_show_config_particular_agenda'
  match 'sel_processes/:id/config_particular_agenda/:sel_step_candidate_id', to: 'sel_processes#config_particular_agenda', via: :post, as: 'sel_process_config_particular_agenda'

  match 'sel_process_steps/:sel_process_step_id/appointments', to: 'sel_processes#show_config_appointments', via: :get, as: 'sel_processes_show_config_appointments'
  match 'sel_process_steps/:sel_process_step_id/appointments', to: 'sel_processes#config_appointments', via: :put, as: 'sel_processes_config_appointments'

  match 'sel_process_steps/:sel_process_step_id/show_set_appointments', to: 'sel_processes#show_assign_appointments', via: :get, as: 'sel_processes_show_set_appointments'
  match 'sel_process_steps/:sel_process_step_id/set_appointments', to: 'sel_processes#assign_appointments', via: :post, as: 'sel_processes_set_appointments'

  match 'sel_process_steps/:sel_process_step_id/show_config_characteristics/:sel_characteristic_id', to: 'sel_processes#show_config_characteristics', via: :get, as: 'sel_processes_show_config_characteristics'
  match 'sel_process_steps/:sel_process_step_id/config_characteristics/:sel_characteristic_id', to: 'sel_processes#config_characteristics', via: :put, as: 'sel_processes_config_characteristics'

  match 'sel_process_steps/:sel_step_candidate_id/exonerate', to: 'sel_processes#exonerate_step_candidate', via: :post, as: 'sel_processes_exonerate'
  match 'sel_process_steps/:sel_step_candidate_id/undo_exonerate', to: 'sel_processes#undo_exonerate_step_candidate', via: :get, as: 'sel_processes_undo_exonerate'

  match 'sel_step_candidate/:sel_step_candidate_id/reopen', to: 'sel_processes#undo_set_results_done', via: :get, as: 'sel_processes_undo_set_results_done'
  match 'sel_step_candidate/:sel_step_candidate_id/files', to: 'sel_processes#list_evaluation_attachments', via: :post, as: 'sel_processes_list_evaluation_attachments'


  match 'sel_applicants/:sel_applicant_id/show_set_comment', to: 'sel_processes#show_set_comment_sel_applicant', via: :post, as: 'sel_processes_show_set_comment_sel_applicant'
  match 'sel_applicants/:sel_applicant_id/set_comment', to: 'sel_processes#set_comment_sel_applicant', via: :post, as: 'sel_processes_set_comment_sel_applicant'
  match 'sel_applicants/:sel_applicant_id/delete_comment', to: 'sel_processes#delete_comment_sel_applicant', via: :post, as: 'sel_processes_delete_comment_sel_applicant'

  match 'sel_processes/:id/benchmark', to: 'sel_processes#benchmark_sel_applicants', via: :get, as: 'sel_processes_benchmark_sel_applicants'
  match 'sel_processes/:id/calendar', to: 'sel_processes#list_configured_events', via: :get, as: 'sel_processes_list_configured_events'

  ## sel_process_trackers
  resources :sel_process_trackers, only: [:index]
  match '/sel_process_trackers/:id', to: 'sel_process_trackers#show',  via: :get, as: 'sel_process_trackers_show'
  match '/sel_process_trackers/:id/applicants', to: 'sel_process_trackers#show_detail_applicants_list',  via: :post, as: 'sel_process_trackers_detail_applicants_list'
  match '/sel_process_trackers/step/:sel_process_step_id', to: 'sel_process_trackers#show_detail_process_step', via: :post, as: 'sel_process_trackers_detail_step'
  match '/sel_process_trackers/:id/selection_step', to: 'sel_process_trackers#show_detail_selection_step',  via: :post, as: 'sel_process_trackers_detail_selection_step'
  match '/sel_process_trackers/:id/validation_step', to: 'sel_process_trackers#show_detail_validation_step',  via: :post, as: 'sel_process_trackers_detail_validation_step'
  match '/sel_process_trackers/:id/s/:sel_step_id', to: 'sel_process_trackers#show',  via: :get, as: 'sel_process_trackers_show_open_step'

  match 'sel_applicant_trackers/:sel_applicant_id/attach_list', to: 'sel_process_trackers#show_attach_file_to_applicant', via: :get, as: 'sel_process_trackers_show_attach_file_to_applicant'

  match '/sel_results/assigned', to: 'sel_results#list_pending_processes',  via: :get, as: 'sel_results_list_assigned_processes'
  match '/sel_results/:sel_process_id', to: 'sel_results#index',  via: :get, as: 'sel_list_pending_results'
  match '/sel_results/input/:sel_step_candidate_id', to: 'sel_results#show_set_results',  via: :get, as: 'sel_input_results'
  match '/sel_results/input/:sel_step_candidate_id/clone_from/:prev_sel_step_candidate_id', to: 'sel_results#show_set_results',  via: :get, as: 'sel_input_results_with_prev_data'
  match '/sel_results/input/:sel_step_candidate_id/p/:from_process', to: 'sel_results#show_set_results',  via: :get, as: 'sel_input_results_from_process'
  match '/sel_results/set_results', to: 'sel_results#set_results',  via: :post, as: 'sel_set_results'
  #match '/sel_results/:jm_answer_value_id/:crypted_name/dl', to: 'sel_results#download_file',  via: :get, as: 'sel_results_download_file'
  match '/jm_candidates/:jm_answer_value_id/:crypted_name/dl', to: 'sel_results#download_file',  via: :get, as: 'sel_results_download_file'

  match 'sel_results/:sel_step_candidate_id/attach/new', to: 'sel_results#show_attach_file_to_step_candidate', via: :get, as: 'sel_results_show_attach_file_to_step_candidate'
  match 'sel_results/:sel_step_candidate_id/attach/add', to: 'sel_results#attach_file_to_step_candidate', via: :post, as: 'sel_results_attach_file_to_step_candidate'
  match 'sel_results/attach/:attachment_crypted_name/dl', to: 'sel_results#download_attach_file_from_step_candidate', via: :get, as: 'sel_results_download_attach_file_from_step_candidate'
  match 'sel_results/attach/:attachment_crypted_name/delete', to: 'sel_results#delete_attach_file_from_step_candidate', via: :post, as: 'sel_results_delete_attach_file_from_step_candidate'


  resources :jm_characteristics
  match 'jm_characteristics/:id/toggle_manager_only', to: 'jm_characteristics#toggle_manager_only', via: :get, as: 'jm_characteristics_toggle_manager_only'

  #job market
  match 'job_market', to: 'jm_process#list_job_offers', via: :get, as: 'list_jm_offers'
  match 'job_market/:sel_process_id', to: 'jm_process#show_job_offer', via: :get, as: 'show_job_offer'
  match 'job_market/:sel_process_id/form_apply', to: 'jm_process#show_apply_form', via: :get, as: 'jm_application_form'
  match 'job_market/:sel_process_id/apply', to: 'jm_process#apply_job_offer', via: :post, as: 'apply_job_offer'


  resources :sel_apply_forms
  match 'sel_apply_forms/add/:template_id', to: 'sel_apply_forms#new', via: :get, as: 'add_sel_apply_form_to_template'

  resources :sel_steps
  match 'sel_steps/add/:template_id', to: 'sel_steps#new', via: :get, as: 'add_step_to_template'


  resources :sel_characteristics
  match 'sel_characteristics/add/:template_id', to: 'sel_characteristics#new', via: :get, as: 'add_characteristic_to_template'


  resources :sel_templates
  match 'sel_characteristics_list/:template_id', to: 'sel_templates#characteristics_list', via: :post, as: 'sel_characteristics_list'
  match 'sel_steps_list/:template_id', to: 'sel_templates#steps_list', via: :post, as: 'sel_steps_list'
  match 'sel_apply_forms_list/:template_id', to: 'sel_templates#apply_forms_list', via: :post, as: 'jm_apply_forms_list'
  match 'sel_templates/:id/p/:pill_name', to: 'sel_templates#show', via: :get, as: 'sel_templates_show_open_pill'

  # sel_manage_template
  resources :sel_manage_templates, only: [:index]
  match 'sel_manage_templates/:id/attendants', to: 'sel_manage_templates#show_assign_attendants', via: :get, as: 'sel_manage_templates_show_assign_attendants'
  match 'sel_manage_templates/:id/assign_attendants', to: 'sel_manage_templates#assign_attendants', via: :post, as: 'sel_manage_templates_assign_attendants'


  resources :elec_processes
  match 'elec_processes/:id/p/:pill_name', to: 'elec_processes#show', via: :get, as: 'elec_processes_show_with_open_pill'

  resources :elec_process_rounds, except: [:index, :new, :edit, :show]
  match 'elec_process_rounds/:elec_process_id/detail', to: 'elec_process_rounds#index', via: :post, as: 'elec_process_rounds_detail'
  match 'elec_processes/:elec_process_id/rounds/new', to: 'elec_process_rounds#new', via: :get, as: 'new_elec_process_round'
  match 'elec_process_rounds/:id/edit', to: 'elec_process_rounds#edit', via: :get, as: 'edit_elec_process_round'

  resources :elec_manage_processes, only: [:index]
  match 'elec_manage_processes/:id/:what_to_show', to: 'elec_manage_processes#show', via: :get, as: 'elec_manage_processes_show'
  match 'elec_manage_processes/:id/:what_to_show/r/:elec_round_id', to: 'elec_manage_processes#show', via: :get, as: 'elec_manage_processes_show_round_open'
  match 'elec_manage_processes/:id/:what_to_show/rounds_list', to: 'elec_manage_processes#show_rounds_list', via: :post, as: 'elec_manage_processes_show_rounds_list'
  match 'elec_manage_processes/r/:elec_round_id/detail', to: 'elec_manage_processes#show_round_detail', via: :post, as: 'elec_manage_processes_show_round_detail'
  match 'elec_manage_processes/r/:elec_round_id/c/:elec_process_category_id/detail', to: 'elec_manage_processes#show_category_detail', via: :post, as: 'elec_manage_processes_show_category_detail'
  match 'elec_manage_processes/r/:elec_round_id/c/:elec_process_category_id/u/:user_id/toggle', to: 'elec_manage_processes#toggle_winner_status_candidate', via: :post, as: 'elec_manage_processes_toggle_winner_status_candidate'
  match 'elec_manage_processes/r/:elec_round_id/finish', to: 'elec_manage_processes#finish_round', via: :get, as: 'elec_manage_processes_finish_round'
  match 'elec_manage_processes/r/:elec_round_id/export', to: 'elec_manage_processes#export_round_results', via: :get, as: 'elec_manage_processes_export_round_results'


  resources :elec_events, except: [:index, :new, :edit, :show]
  match 'elec_events/:elec_process_id/detail', to: 'elec_events#index', via: :post, as: 'elec_events_detail'
  match 'elec_processes/:elec_process_id/schedule/new', to: 'elec_events#new', via: :get, as: 'new_elec_event'
  match 'elec_events/:id/edit', to: 'elec_events#edit', via: :get, as: 'edit_elec_event'


  resources :elec_characteristics, except: [:index, :new, :edit, :show]
  match 'elec_characteristics/:elec_process_id/detail', to: 'elec_characteristics#index', via: :post, as: 'elec_characteristics_detail'
  match 'elec_processes/:elec_process_id/characteristics/new', to: 'elec_characteristics#new', via: :get, as: 'new_elec_characteristic'
  match 'elec_characteristics/:id/edit', to: 'elec_characteristics#edit', via: :get, as: 'edit_elec_characteristic'


  resources :elec_process_categories, except: [:index, :new, :edit, :show]
  match 'elec_process_categories/:elec_process_id/detail', to: 'elec_process_categories#index', via: :post, as: 'elec_process_categories_detail'
  match 'elec_processes/:elec_process_id/categories/new', to: 'elec_process_categories#new', via: :get, as: 'new_elec_process_category'
  match 'elec_process_categories/:id/edit', to: 'elec_process_categories#edit', via: :get, as: 'edit_elec_process_category'
  match 'elec_process_categories/load_characteristic_values', to: 'elec_process_categories#load_characteristic_values', via: :post, as: 'elec_process_category_load_characteristic_values'


  # elec_round_exec
  match 'elec_round_exec/:id', to: 'elec_round_exec#show', via: :get, as: 'elec_round_exec'
  match 'elec_round_exec/:id/c/:elec_process_category_id', to: 'elec_round_exec#show', via: :get, as: 'elec_round_exec_open_category'
  match 'elec_round_exec/:id/category_detail/:elec_process_category_id', to: 'elec_round_exec#show_detail_category', via: :post, as: 'elec_round_exec_show_detail_category'
  match 'elec_round_exec/:id/vote/:elec_process_category_id', to: 'elec_round_exec#vote', via: :post, as: 'elec_round_exec_vote'
  match 'elec_round_exec/load_user_mini_file', to: 'elec_round_exec#load_user_mini_file', via: :post, as: 'elec_round_exec_load_user_mini_file'
  match 'elec_round_exec/:id/my_voters_detail/:elec_process_category_id', to: 'elec_round_exec#view_my_voters_detail', via: :get, as: 'elec_round_exec_show_my_voters_detail'

  resources :kpi_dashboard_types

  resources :uc_characteristics

  resources :program_course_instances

  resources :program_instances

  #match '/', to: 'static_pages#home'
  root to: 'static_pages#home'
  match '/save_show_azure/ajax_call/for/:user_id', to: 'static_pages#save_show_azure_ajax_call', via: :get, as: 'static_pages_save_show_azure_ajax_call'
  match '/login_standard/:p', to: 'static_pages#home', via: :get, as: 'login_standard'
  match '/logout/ad', to: 'static_pages#logout_ad', via: :get, as: 'logout_ad'
  match '/landing_azure/ad', to: 'static_pages#landing_azure', via: :get, as: 'static_pages_landing_azure'
  match '/landing_azure/ad/:close_parent', to: 'static_pages#landing_azure', via: :get, as: 'static_pages_landing_azure_close_parent'

  match '/landing_azure/ad/step/2', to: 'static_pages#landing_azure_2', via: :get, as: 'static_pages_landing_azure_2'
  match 'error_auth/ad', to: 'static_pages#error_external_authentication', via: :get, as: 'error_auth_azure'


  match 'ficha_usuario', to: 'static_pages#user_profile', via: :get, as: 'user_profile'
  match 'ficha_usuario_new', to: 'static_pages#user_profile_new', via: :get, as: 'user_profile_new'

  match 'recuperar_pass1', to: 'static_pages#recuperar_pass_paso1', via: :get, as: 'recuperar_pass_paso1'
  match 'recuperar_pass2', to: 'static_pages#recuperar_pass_paso2', via: :post, as: 'recuperar_pass_paso2'
  match 'recuperar_pass3/:codigo_rec_pass', to: 'static_pages#recuperar_pass_paso3', via: :get, as: 'recuperar_pass_paso3'
  match 'recuperar_pass4', to: 'static_pages#recuperar_pass_paso4', via: :post, as: 'recuperar_pass_paso4'
  match 'ejecuta_patch', to: 'static_pages#ejecuta_patch', via: :get, as: 'ejecuta_patch'

  #match 'ficha_usuario/pe_rep_detailed_final_results/:pe_process_id', to: 'static_pages#pe_rep_detailed_final_results', via: :get, as: 'user_profile_pe_rep_detailed_final_results'
  #match 'ficha_usuario/pe_rep_detailed_final_results_pdf/:pe_process_id', to: 'static_pages#pe_rep_detailed_final_results_pdf', via: :get, as: 'user_profile_pe_rep_detailed_final_results_pdf'

  match 'Ateneus', to: 'static_pages#redirect_from_ateneus', via: :get, as: 'redirect_from_ateneus'
  match 'ateneus', to: 'static_pages#redirect_from_ateneus', via: :get, as: 'redirect_from_ateneus'

  match 'download_informative_document/:id', to: 'static_pages#download_informative_document', via: :get, as: 'download_informative_document'

  match 'get_lms_server_time', to: 'static_pages#get_lms_server_time', via: :get, as: 'get_lms_server_time'
  match 'test', to: 'static_pages#test', via: :get, as: 'test'
  match 'sww', to: 'static_pages#sww', via: :get, as: 'sww'
  match 'smart_admin_login', to: 'static_pages#create_session_admin_from_other_company', via: :post, as: 'smart_admin_login'
  match 'validation_smart_admin_login', to: 'static_pages#ask_for_validation_admin_logged_from_other_company', via: :get, as: 'validate_smart_admin_login'
  match 'load_smart_admin_login/:caller_company_id', to: 'static_pages#loading_sesion_data_from_other_company', via: :get, as: 'load_smart_admin_login'

  match 'insert_base_data', to: 'static_pages#insert_base_data', via: :get, as: 'insert_base_data'

  match 'ct_statistics', to: 'ct_statistics#show', via: :get, as: 'ct_statistics_show'
  match 'ct_statistics_registro_accesos', to: 'ct_statistics#registro_accesos', via: :get, as: 'ct_statistics_registro_accesos'
  match 'ct_statistics_navegadores', to: 'ct_statistics#navegadores', via: :get, as: 'ct_statistics_navegadores'
  match 'ct_statistics_navegadoresglobal', to: 'ct_statistics#navegadoresglobal', via: :get, as: 'ct_statistics_navegadoresglobal'

  resources :sessions, only: [:create, :destroy]
  match 'logout', to: 'sessions#destroy', via: :get

  resources :companies, only: [:new, :create, :index, :edit, :update]
  match 'company/edit_emails', to: 'companies#edit_emails', via: :get, as: 'companies_edit_emails'
  match 'company/edit_design_paramenters', to: 'companies#edit_design', via: :get, as: 'companies_edit_design'

  match 'company/edit_header_img', to: 'companies#edit_header_img', via: :get, as: 'companies_edit_header_img'
  match 'company/edit_header_img', to: 'companies#edit_header_img', via: :post, as: 'companies_update_header_img'

  match 'company/edit_login_img/:res', to: 'companies#edit_login_img', via: :get, as: 'companies_edit_login_img'
  match 'company/edit_login_img/:res', to: 'companies#edit_login_img', via: :post, as: 'companies_update_login_img'

  match 'company/delete_login_img/:res', to: 'companies#delete_login_img', via: :get, as: 'companies_delete_login_img'

  match 'company/edit_login_css', to: 'companies#edit_login_css', via: :get, as: 'companies_edit_login_css'
  match 'company/update_login_css', to: 'companies#update_login_css', via: :post, as: 'companies_update_login_css'


  resources :users, only: [:new, :create, :index, :search, :show, :edit, :update, :edit_pass, :update_pass]
  match 'cambiar_password', to: 'users#edit_pass', via: :get, as: 'user_change_pass'
  match 'cambiar_password', to: 'users#update_pass', via: :put, as: 'user_change_pass'

  match 'users_edit_requiere_cambio_password', to: 'users#edit_requiere_cambio_password', via: :get, as: 'user_edit_requiere_cambio_password'
  match 'users_update_requiere_cambio_password', to: 'users#update_requiere_cambio_password', via: :post, as: 'user_update_requiere_cambio_password'

  match 'users_edit_requiere_cambio_password_search', to: 'users#edit_requiere_cambio_password_search', via: :get, as: 'user_edit_requiere_cambio_password_search'
  match 'users_edit_requiere_cambio_password_search', to: 'users#edit_requiere_cambio_password_search', via: :post, as: 'user_edit_requiere_cambio_password_search'
  match 'users_update_requiere_cambio_password_search', to: 'users#update_requiere_cambio_password_search', via: :post, as: 'user_update_requiere_cambio_password_search'

  match 'users/search_users', to: 'users#search_users', via: :post, as: 'users_search_users'
  match 'users_carga_masiva', to: 'users#carga_masiva', via: :get, as: 'users_carga_masiva'
  match 'users_carga_masiva', to: 'users#carga_masiva_upload', via: :post, as: 'users_carga_masiva'
  match 'users_update_masivo', to: 'users#update_masivo', via: :get, as: 'users_update_masivo'
  match 'users_update_masivo', to: 'users#update_masivo_upload', via: :post, as: 'users_update_masivo'
  match 'users_carga_masiva_fotos', to: 'users#carga_masiva_fotos', via: :get, as: 'users_carga_masiva_fotos'
  match 'users_carga_masiva_fotos', to: 'users#carga_masiva_fotos_upload', via: :post, as: 'users_carga_masiva_fotos'
  match 'actualizar_foto/:id', to: 'users#edit_foto', via: :get, as: 'users_edit_foto'
  match 'actualizar_foto/:id', to: 'users#upload_foto', via: :post, as: 'users_upload_foto'
  match 'actualizar_foto_by_user', to: 'users#edit_foto_by_user', via: :get, as: 'users_edit_foto_by_user'
  match 'actualizar_foto_by_user', to: 'users#upload_foto_by_user', via: :post, as: 'users_upload_foto_by_user'

  match 'users_carga_masiva_excel', to: 'users#carga_masiva_excel', via: :get, as: 'users_carga_masiva_excel'
  match 'users_carga_masiva_excel', to: 'users#carga_masiva_excel', via: :post, as: 'users_carga_masiva_excel'

  match 'users_download_xls_for_carga_masiva', to: 'users#download_xls_for_carga_masiva', via: :get, as: 'users_download_xls_for_carga_masiva'

  match 'users/reportes/lista_usuarios_csv', to: 'users#lista_usuarios_csv', via: :get, as: 'users_lista_usuarios_csv'
  match 'users/reportes/lista_usuarios_xlsx', to: 'users#lista_usuarios_xlsx', via: :get, as: 'users_lista_usuarios_xlsx'
  match 'users/reportes/lista_usuarios', to: 'users#lista_usuarios', via: :get, as: 'users_lista_usuarios'
  match 'users/reps/rep_logins', to: 'users#rep_logins', via: :get, as: 'users_rep_logins'
  match 'users/reps/rep_last_logins', to: 'users#rep_last_logins', via: :get, as: 'users_rep_last_logins'
  match 'users/reps/rep_users', to: 'users#rep_users', via: :get, as: 'users_rep_users'
  match 'users/reps/rep_active_users', to: 'users#rep_active_users', via: :get, as: 'users_rep_active_users'
  match 'users/reps/rep_active_users_update_date', to: 'users#rep_active_users_update_date', via: :get, as: 'users_rep_active_users_update_date'
  match 'users/reps/rep_active_users_register/:characteristic_id', to: 'users#rep_active_users_register', via: :get, as: 'users_rep_active_users_register'

  match 'users/reps/rep_active_users_register_sync/:characteristic_id', to: 'users#rep_active_users_register_sync', via: :get, as: 'users_rep_active_users_register_sync'
  match 'users/reps/rep_active_users_training_sync', to: 'users#rep_active_users_training_sync', via: :get, as: 'users_rep_active_users_training_sync'

  match 'users/reps/rep_active_users_register_sync_csv/:characteristic_id', to: 'users#rep_active_users_register_sync_csv', via: :get, as: 'users_rep_active_users_register_sync_csv'
  match 'users/reps/rep_active_users_training_sync_csv', to: 'users#rep_active_users_training_sync_csv', via: :get, as: 'users_rep_active_users_training_sync_csv'


  match 'users/edit_characteristics/:id', to: 'users#edit_characteristics', via: :get, as: 'edit_characteristics_user'
  match 'users/update_characteristics/:id', to: 'users#update_characteristics', via: :put, as: 'update_characteristics_user'

  #UserProfile
  match 'user_profile/edit_photo/user_connected', to: 'user_profile#edit_photo', via: :get, as: 'user_profile_edit_photo'
  match 'user_profile/upload_photo/user_connected', to: 'user_profile#upload_photo', via: :post, as: 'user_profile_upload_photo'

  match 'user_profile', to: 'user_profile#show_profile', via: :get, as: 'show_user_profile'
  match 'user_profile/:ct_id', to: 'user_profile#show_profile', via: :get, as: 'show_user_profile_after_update'
  match 'user_profile/download_characteristic_file/:characteristic_id', to: 'user_profile#download_characteristic_file', via: :get, as: 'user_profile_download_characteristic_file'
  match 'user_profile/download_user_characteristic_file/:user_characteristic_id', to: 'user_profile#download_user_characteristic_file', via: :get, as: 'user_profile_download_user_characteristic_file'

  match 'user_profile/edit_characteristics', to: 'user_profile#edit_characteristics', via: :get, as: 'user_profile_edit_characteristics_nc'
  match 'user_profile/update_characteristics', to: 'user_profile#update_characteristics', via: :post, as: 'user_profile_update_characteristics_nc'

  match 'user_profile/edit_characteristics/:characteristic_type_id', to: 'user_profile#edit_characteristics', via: :get, as: 'user_profile_edit_characteristics'
  match 'user_profile/update_characteristics/:characteristic_type_id', to: 'user_profile#update_characteristics', via: :post, as: 'user_profile_update_characteristics'

  match 'user_profile/download_informative_document/:document_id', to: 'user_profile#download_informative_document', via: :get, as: 'user_profile_download_informative_document'

  match 'user_profile/pe_rep_detailed_final_results/:pe_process_id', to: 'user_profile#pe_rep_detailed_final_results', via: :get, as: 'user_profile_pe_rep_detailed_final_results'
  match 'user_profile/pe_rep_detailed_final_results_pdf/:pe_process_id', to: 'user_profile#pe_rep_detailed_final_results_pdf', via: :get, as: 'user_profile_pe_rep_detailed_final_results_pdf'

  resources :admins

  resources :nodes
  match 'nodes_carga_masiva', to: 'nodes#carga_masiva', via: :get, as: 'nodes_carga_masiva'
  match 'nodes_carga_masiva', to: 'nodes#carga_masiva_upload', via: :post, as: 'nodes_carga_masiva'

  match 'nodes_carga_masiva_nodes', to: 'nodes#carga_masiva_nodes', via: :get, as: 'nodes_carga_masiva_nodes'
  match 'nodes_carga_masiva_nodes', to: 'nodes#carga_masiva_nodes_upload', via: :post, as: 'nodes_carga_masiva_nodes'

  match 'nodes/node_search_to_link/:id', to: 'nodes#show', via: :post, as: 'node_search_to_link'

  match 'nodes/unlink/:user_id', to: 'nodes#unlink', via: :get, as: 'nodes_unlink'
  match 'nodes/link/:node_id/:user_id', to: 'nodes#link', via: :get, as: 'nodes_link'

  match 'nodes_download_hierarchy', to: 'nodes#download_hierarchy', via: :get, as: 'nodes_download_hierarchy'

  #user_managing_characteristics
  resources :um_characteristics
  #resources :um_custom_reports

  #user_managing_reports_controller
  match 'user_managing_reports/rep/active_employees_characteristics', to: 'user_managing_reports#rep_active_employees_characteristics', via: :get, as: 'user_managing_reports_rep_active_employees_characteristics'
  match 'user_managing_reports/rep/active_employees_photos', to: 'user_managing_reports#rep_active_employees_photos', via: :get, as: 'user_managing_reports_rep_active_employees_photos'

  #user_managing_controller
  match 'user_managing/search', to: 'user_managing#search', via: :get, as: 'user_managing_search'
  match 'user_managing/search', to: 'user_managing#search', via: :post, as: 'user_managing_search'
  match 'user_managing/user_profile/:user_id', to: 'user_managing#user_profile', via: :get, as: 'user_managing_user_profile'
  match 'user_managing/user_profile/:user_id/:ct_id', to: 'user_managing#user_profile', via: :get, as: 'user_managing_user_profile_after_update'

  match 'user_managing/reports', to: 'user_managing#reports', via: :get, as: 'user_managing_reports'

  match 'user_managing/new_custom_report', to: 'user_managing#new_custom_report', via: :get, as: 'user_managing_new_custom_report'
  match 'user_managing/create_custom_report', to: 'user_managing#create_custom_report', via: :post, as: 'user_managing_create_custom_report'

  match 'user_managing/edit_custom_report/:id', to: 'user_managing#edit_custom_report', via: :get, as: 'user_managing_edit_custom_report'
  match 'user_managing/update_custom_report/:id', to: 'user_managing#update_custom_report', via: :post, as: 'user_managing_update_custom_report'

  match 'user_managing/delete_custom_report/:id', to: 'user_managing#delete_custom_report', via: :delete, as: 'user_managing_delete_custom_report'

  match 'user_managing/query_custom_reports', to: 'user_managing#query_custom_reports', via: :get, as: 'user_managing_query_custom_reports'
  match 'user_managing/query_custom_report/:id/:active', to: 'user_managing#query_custom_report', via: :get, as: 'user_managing_query_custom_report'

  match 'user_managing/rep_active_employees_characteristics', to: 'user_managing#rep_active_employees_characteristics', via: :get, as: 'user_managing_rep_active_employees_characteristics'
  match 'user_managing/rep_employees_characteristics', to: 'user_managing#rep_employees_characteristics', via: :get, as: 'user_managing_rep_employees_characteristics'

  match 'user_managing/rep_active_employees_characteristic_register/:characteristic_id', to: 'user_managing#rep_active_employees_characteristic_register', via: :get, as: 'user_managing_rep_active_employees_characteristic_register'
  match 'user_managing/rep_employees_characteristic_register/:characteristic_id', to: 'user_managing#rep_employees_characteristic_register', via: :get, as: 'user_managing_rep_employees_characteristic_register'

  match 'user_managing/rep_active_employees_characteristic_history', to: 'user_managing#rep_active_employees_characteristic_history', via: :get, as: 'user_managing_rep_active_employees_characteristic_history'
  match 'user_managing/rep_employees_characteristic_history', to: 'user_managing#rep_employees_characteristic_history', via: :get, as: 'user_managing_rep_employees_characteristic_history'

  match 'user_managing/rep_active_user_characteristics', to: 'user_managing#rep_active_user_characteristics', via: :get, as: 'user_managing_rep_active_user_characteristics'
  match 'user_managing/rep_active_user_characteristic_register/:characteristic_id', to: 'user_managing#rep_active_user_characteristic_register', via: :get, as: 'user_managing_rep_active_user_characteristic_register'

  match 'user_managing/download_characteristic_file/:user_id/:characteristic_id', to: 'user_managing#download_characteristic_file', via: :get, as: 'user_managing_download_characteristic_file'
  match 'user_managing/download_user_characteristic_file/:user_id/:user_characteristic_id', to: 'user_managing#download_user_characteristic_file', via: :get, as: 'user_managing_download_user_characteristic_file'

  match 'user_managing/reports_privileges_search', to: 'user_managing#reports_privileges_search', via: :get, as: 'user_managing_reports_privileges_search'
  match 'user_managing/reports_privileges_search', to: 'user_managing#reports_privileges_search', via: :post, as: 'user_managing_reports_privileges_search'
  match 'user_managing/reports_privileges_edit/:user_id', to: 'user_managing#reports_privileges_edit', via: :get, as: 'user_managing_reports_privileges_edit'
  match 'user_managing/reports_privileges_update/:user_id', to: 'user_managing#reports_privileges_update', via: :post, as: 'user_managing_reports_privileges_update'

  match 'user_managing/mpi_privileges_search', to: 'user_managing#mpi_privileges_search', via: :get, as: 'user_managing_mpi_privileges_search'
  match 'user_managing/mpi_privileges_search', to: 'user_managing#mpi_privileges_search', via: :post, as: 'user_managing_mpi_privileges_search'
  match 'user_managing/mpi_privileges_edit/:user_id', to: 'user_managing#mpi_privileges_edit', via: :get, as: 'user_managing_mpi_privileges_edit'
  match 'user_managing/mpi_privileges_update/:user_id', to: 'user_managing#mpi_privileges_update', via: :post, as: 'user_managing_mpi_privileges_update'

  match 'user_managing/um_privileges_search', to: 'user_managing#um_privileges_search', via: :get, as: 'user_managing_um_privileges_search'
  match 'user_managing/um_privileges_search', to: 'user_managing#um_privileges_search', via: :post, as: 'user_managing_um_privileges_search'
  match 'user_managing/um_privileges_edit/:user_id', to: 'user_managing#um_privileges_edit', via: :get, as: 'user_managing_um_privileges_edit'
  match 'user_managing/um_privileges_update/:user_id', to: 'user_managing#um_privileges_update', via: :post, as: 'user_managing_um_privileges_update'

  match 'user_managing/search_for_users_manager', to: 'user_managing#search_for_users_manager', via: :get, as: 'user_managing_search_for_users_manager'
  match 'user_managing/search_for_users_manager', to: 'user_managing#search_for_users_manager', via: :post, as: 'user_managing_search_for_users_manager'
  match 'user_managing/user_profile_for_users_manager/:user_id', to: 'user_managing#user_profile_for_users_manager', via: :get, as: 'user_managing_user_profile_for_users_manager'
  match 'user_managing/user_profile_for_users_manager/:user_id/:ct_id', to: 'user_managing#user_profile_for_users_manager', via: :get, as: 'user_managing_user_profile_for_users_manager_after_update'

  match 'user_managing/edit_characteristics/:user_id', to: 'user_managing#edit_characteristics', via: :get, as: 'user_managing_edit_characteristics_nc'
  match 'user_managing/update_characteristics/:user_id', to: 'user_managing#update_characteristics', via: :post, as: 'user_managing_update_characteristics_nc'

  match 'user_managing/edit_characteristics/:user_id/:characteristic_type_id', to: 'user_managing#edit_characteristics', via: :get, as: 'user_managing_edit_characteristics'
  match 'user_managing/update_characteristics/:user_id/:characteristic_type_id', to: 'user_managing#update_characteristics', via: :post, as: 'user_managing_update_characteristics'

  match 'user_managing/edit_characteristic_history/:user_id/:characteristic_id', to: 'user_managing#edit_characteristic_history', via: :get, as: 'user_managing_edit_characteristic_history'
  match 'user_managing/edit_characteristic_record/:user_characteristic_record_id', to: 'user_managing#edit_user_characteristic_record', via: :get, as: 'user_managing_edit_characteristic_record'
  match 'user_managing/update_characteristic_record/:user_characteristic_record_id', to: 'user_managing#update_user_characteristic_record', via: :post, as: 'user_managing_update_characteristic_record'
  match 'user_managing/delete_characteristic_record/:user_characteristic_record_id', to: 'user_managing#delete_user_characteristic_record', via: :get, as: 'user_managing_delete_characteristic_record'

  match 'user_managing/edit/:user_id', to: 'user_managing#edit', via: :get, as: 'user_managing_edit'
  match 'user_managing/update/:user_id', to: 'user_managing#update', via: :post, as: 'user_managing_update'

  match 'user_managing/edit_password/:user_id', to: 'user_managing#edit_password', via: :get, as: 'user_managing_edit_password'
  match 'user_managing/update_password/:user_id', to: 'user_managing#update_password', via: :post, as: 'user_managing_update_password'

  match 'user_managing/edit_photo/:user_id', to: 'user_managing#edit_photo', via: :get, as: 'user_managing_edit_photo'
  match 'user_managing/update_photo/:user_id', to: 'user_managing#update_photo', via: :post, as: 'user_managing_update_photo'

  match 'user_managing/edit_email/:user_id', to: 'user_managing#edit_email', via: :get, as: 'user_managing_edit_email'
  match 'user_managing/update_email/:user_id', to: 'user_managing#update_email', via: :post, as: 'user_managing_update_email'

  match 'user_managing/delete/:user_id', to: 'user_managing#delete_form', via: :get, as: 'user_managing_delete'
  match 'user_managing/delete/:user_id', to: 'user_managing#delete', via: :post, as: 'user_managing_delete'


  #user_managing_create
  match 'user_managing/new', to: 'user_managing_create#new', via: :get, as: 'user_managing_new'
  match 'user_managing/create', to: 'user_managing_create#create', via: :post, as: 'user_managing_create'
  match 'user_managing/new_from_excel', to: 'user_managing_create#new_from_excel', via: :get, as: 'user_managing_new_from_excel'
  match 'user_managing/create_from_excel', to: 'user_managing_create#create_from_excel', via: :post, as: 'user_managing_create_from_excel'
  match 'user_managing/new_from_excel_format', to: 'user_managing_create#new_from_excel_format', via: :get, as: 'user_managing_new_from_excel_format'

  #lms_managing_define_controller
  match 'lms_managing_define/define', to: 'lms_managing_define#define', via: :get, as: 'lms_managing_define'

  match 'lms_managing_define/massive_charge_programs', to: 'lms_managing_define#massive_charge_programs', via: :get, as: 'lms_managing_define_massive_charge_programs'
  match 'lms_managing_define/massive_charge_programs', to: 'lms_managing_define#massive_charge_programs', via: :post, as: 'lms_managing_define_massive_charge_programs'
  match 'lms_managing_define/massive_charge_programs_download_format', to: 'lms_managing_define#massive_charge_programs_download_format', via: :get, as: 'lms_managing_define_massive_charge_programs_download_format'

  match 'lms_managing_define/massive_charge_courses', to: 'lms_managing_define#massive_charge_courses', via: :get, as: 'lms_managing_define_massive_charge_courses'
  match 'lms_managing_define/massive_charge_courses', to: 'lms_managing_define#massive_charge_courses', via: :post, as: 'lms_managing_define_massive_charge_courses'
  match 'lms_managing_define/massive_charge_courses_download_format', to: 'lms_managing_define#massive_charge_courses_download_format', via: :get, as: 'lms_managing_define_massive_charge_courses_download_format'

  match 'lms_managing_define/new_program', to: 'lms_managing_define#new_program', via: :get, as: 'lms_managing_define_new_program'
  match 'lms_managing_define/create_program', to: 'lms_managing_define#create_program', via: :post, as: 'lms_managing_define_create_program'
  match 'lms_managing_define/edit_program/:program_id', to: 'lms_managing_define#edit_program', via: :get, as: 'lms_managing_define_edit_program'
  match 'lms_managing_define/update_program/:program_id', to: 'lms_managing_define#update_program', via: :post, as: 'lms_managing_define_update_program'
  match 'lms_managing_define/show_program/:program_id', to: 'lms_managing_define#show_program', via: :get, as: 'lms_managing_define_show_program'

  match 'lms_managing_define/new_course', to: 'lms_managing_define#new_course', via: :get, as: 'lms_managing_define_new_course'
  match 'lms_managing_define/create_course', to: 'lms_managing_define#create_course', via: :post, as: 'lms_managing_define_create_course'
  match 'lms_managing_define/edit_course/:course_id', to: 'lms_managing_define#edit_course', via: :get, as: 'lms_managing_define_edit_course'
  match 'lms_managing_define/update_course/:course_id', to: 'lms_managing_define#update_course', via: :post, as: 'lms_managing_define_update_course'
  match 'lms_managing_define/show_course/:course_id', to: 'lms_managing_define#show_course', via: :get, as: 'lms_managing_define_show_course'
  match 'lms_managing_define/edit_course_image/:course_id', to: 'lms_managing_define#edit_course_image', via: :get, as: 'lms_managing_define_edit_course_image'
  match 'lms_managing_define/update_course_image/:course_id', to: 'lms_managing_define#update_course_image', via: :post, as: 'lms_managing_define_update_course_image'

  match 'lms_managing_define/new_unit/:course_id', to: 'lms_managing_define#new_unit', via: :get, as: 'lms_managing_define_new_unit'
  match 'lms_managing_define/create_unit/:course_id', to: 'lms_managing_define#create_unit', via: :post, as: 'lms_managing_define_create_unit'
  match 'lms_managing_define/edit_unit/:unit_id', to: 'lms_managing_define#edit_unit', via: :get, as: 'lms_managing_define_edit_unit'
  match 'lms_managing_define/update_unit/:unit_id', to: 'lms_managing_define#update_unit', via: :post, as: 'lms_managing_define_update_unit'

  match 'lms_managing_define/new_evaluation/:course_id', to: 'lms_managing_define#new_evaluation', via: :get, as: 'lms_managing_define_new_evaluation'
  match 'lms_managing_define/create_evaluation/:course_id', to: 'lms_managing_define#create_evaluation', via: :post, as: 'lms_managing_define_create_evaluation'
  match 'lms_managing_define/edit_evaluation/:evaluation_id', to: 'lms_managing_define#edit_evaluation', via: :get, as: 'lms_managing_define_edit_evaluation'
  match 'lms_managing_define/update_evaluation/:evaluation_id', to: 'lms_managing_define#update_evaluation', via: :post, as: 'lms_managing_define_update_evaluation'

  match 'lms_managing_define/new_poll/:course_id', to: 'lms_managing_define#new_poll', via: :get, as: 'lms_managing_define_new_poll'
  match 'lms_managing_define/create_poll/:course_id', to: 'lms_managing_define#create_poll', via: :post, as: 'lms_managing_define_create_poll'
  match 'lms_managing_define/edit_poll/:poll_id', to: 'lms_managing_define#edit_poll', via: :get, as: 'lms_managing_define_edit_poll'
  match 'lms_managing_define/update_poll/:poll_id', to: 'lms_managing_define#update_poll', via: :post, as: 'lms_managing_define_update_poll'

  match 'lms_managing_define/show_study_plan/:program_id', to: 'lms_managing_define#show_study_plan', via: :get, as: 'lms_managing_define_show_study_plan'
  match 'lms_managing_define/add_study_plan/:program_id', to: 'lms_managing_define#add_study_plan', via: :get, as: 'lms_managing_define_add_study_plan'
  match 'lms_managing_define/create_study_plan/:program_id', to: 'lms_managing_define#create_study_plan', via: :post, as: 'lms_managing_define_create_study_plan'

  #lms_managing_execute_controller

  match 'lms_managing_execute/massive_charge_enrollments', to: 'lms_managing_execute#massive_charge_enrollments', via: :get, as: 'lms_managing_execute_massive_charge_enrollments'
  match 'lms_managing_execute/massive_charge_enrollments', to: 'lms_managing_execute#massive_charge_enrollments', via: :post, as: 'lms_managing_execute_massive_charge_enrollments'
  match 'lms_managing_execute/massive_charge_enrollments_download_format', to: 'lms_managing_execute#massive_charge_enrollments_download_format', via: :get, as: 'lms_managing_execute_massive_charge_enrollments_download_format'

  match 'lms_managing_execute/enroll', to: 'lms_managing_execute#enroll', via: :get, as: 'lms_managing_show_enroll'
  match 'lms_managing_execute/enroll/:pill_open', to: 'lms_managing_execute#enroll', via: :get, as: 'lms_managing_show_enroll_pill_open'
  match 'lms_managing_execute/enroll_people', to: 'lms_managing_execute#enroll_1_people', via: :post, as: 'lms_managing_show_list_to_enroll_users'
  match 'lms_managing_execute/remove_user/:user_id', to: 'lms_managing_execute#remove_user_from_list', via: :post, as: 'lms_managing_remove_user_from_list'
  match 'lms_managing_execute/enroll_found_people', to: 'lms_managing_execute#enroll_1_people', via: :post, as: 'lms_managing_show_list_found_users'
  match 'lms_managing_execute/enroll_courses', to: 'lms_managing_execute#enroll_2_courses', via: :post, as: 'lms_managing_show_list_to_enroll_courses'
  match 'lms_managing_execute/remove_course/:program_course_id', to: 'lms_managing_execute#remove_course_from_list', via: :post, as: 'lms_managing_remove_course_from_list'
  match 'lms_managing_execute/enroll_dates', to: 'lms_managing_execute#enroll_3_dates', via: :post, as: 'lms_managing_show_enroll_dates'
  match 'lms_managing_execute/save_dates', to: 'lms_managing_execute#save_dates_to_course', via: :post, as:'lms_managing_save_dates_to_course'
  match 'lms_managing_execute/enroll_confirm', to: 'lms_managing_execute#enroll_4_confirm', via: :post, as: 'lms_managing_show_enroll_confirmation'
  match 'lms_managing_execute/save_enroll', to: 'lms_managing_execute#enroll_5_enroll', via: :post, as: 'lms_managing_execute_save_enroll'

  match 'lms_managing_execute/pci/list_courses', to: 'lms_managing_execute#pci_list_courses', via: :get, as: 'lms_managing_execute_pci_list_courses'
  match 'lms_managing_execute/pci/:program_course_id', to: 'lms_managing_execute#pci_program_course', via: :get, as: 'lms_managing_execute_pci_program_course'
  match 'lms_managing_execute/pci/:program_course_id/new', to: 'lms_managing_execute#program_course_instance_new', via: :get, as: 'lms_managing_execute_pci_new'
  match 'lms_managing_execute/pci/:program_course_id/create', to: 'lms_managing_execute#program_course_instance_create', via: :post, as: 'lms_managing_execute_pci_create'
  match 'lms_managing_execute/pci/:program_course_instance_id/destroy', to: 'lms_managing_execute#program_course_instance_destroy', via: :delete, as: 'lms_managing_execute_pci_destroy'
  match 'lms_managing_execute/pci/:program_course_instance_id/edit_dates', to: 'lms_managing_execute#program_course_instance_edit_dates', via: :get, as: 'lms_managing_execute_pci_edit_dates'
  match 'lms_managing_execute/pci/:program_course_instance_id/update_dates', to: 'lms_managing_execute#program_course_instance_update_dates', via: :post, as: 'lms_managing_execute_pci_update_dates'

  match 'lms_managing_execute/pci/:program_course_instance_id/search_add_manager', to: 'lms_managing_execute#program_course_instance_search_add_manager', via: :get, as: 'lms_managing_execute_pci_search_add_manager'
  match 'lms_managing_execute/pci/:program_course_instance_id/search_add_manager', to: 'lms_managing_execute#program_course_instance_search_add_manager', via: :post, as: 'lms_managing_execute_pci_search_add_manager'
  match 'lms_managing_execute/pci/:program_course_instance_id/add_manager/:user_id', to: 'lms_managing_execute#program_course_instance_add_manager', via: :get, as: 'lms_managing_execute_pci_add_manager'
  match 'lms_managing_execute/pci/:program_course_instance_id/remove_manager/:user_id', to: 'lms_managing_execute#program_course_instance_remove_manager', via: :get, as: 'lms_managing_execute_pci_remove_manager'

  match 'lms_managing_execute/enrollment/list_courses', to: 'lms_managing_execute#enrollment_list_courses', via: :get, as: 'lms_managing_execute_enrollment_list_courses'
  match 'lms_managing_execute/enrollment/:program_course_id', to: 'lms_managing_execute#enrollment_program_course', via: :get, as: 'lms_managing_execute_enrollment_program_course'

  match 'lms_managing_execute/enrollment/:program_course_instance_id/enroll', to: 'lms_managing_execute#enrollment_enroll', via: :get, as: 'lms_managing_execute_enrollment_enroll'

  match 'lms_managing_execute/enrollment/:program_course_instance_id/delete_enrollments', to: 'lms_managing_execute#enrollment_delete_enrollments', via: :get, as: 'lms_managing_execute_enrollment_delete_enrollments'
  match 'lms_managing_execute/enrollment/:program_course_instance_id/destroy_enrollments', to: 'lms_managing_execute#enrollment_destroy_enrollments', via: :post, as: 'lms_managing_execute_enrollment_destroy_enrollments'
  match 'lms_managing_execute/enrollment/:program_course_instance_id/list', to: 'lms_managing_execute#enrollment_list', via: :get, as: 'lms_managing_execute_enrollment_list'


  match 'lms_managing_execute/enrollment/:program_course_instance_id/enroll/:pill_open', to: 'lms_managing_execute#enrollment_enroll', via: :get, as: 'lms_managing_execute_enrollment_enroll_pill_open'
  match 'lms_managing_execute/pci/:program_course_instance_id/enroll_people', to: 'lms_managing_execute#enroll_from_instances_1_people', via: :post, as: 'lms_managing_show_list_to_enroll_from_instances_users'
  match 'lms_managing_execute/pci/:program_course_instance_id/enroll_confirm', to: 'lms_managing_execute#enroll_from_instances_2_confirm', via: :post, as: 'lms_managing_show_enroll_from_instances_confirmation'
  match 'lms_managing_execute/pci/:program_course_instance_id/save_enroll', to: 'lms_managing_execute#enroll_from_instances_3_enroll', via: :post, as: 'lms_managing_execute_save_enroll_from_instances'
  match 'lms_managing_execute/pci/:program_course_instance_id/delete_people/:user_id', to: 'lms_managing_execute#remove_user_from_instance_list', via: :post, as: 'lms_managing_remove_user_from_instance_list'
  match 'lms_managing_execute/pci/:program_course_instance_id/enroll_found_people', to: 'lms_managing_execute#enroll_from_instances_1_people', via: :post, as: 'lms_managing_show_list_found_users_from_instance'
  match 'lms_managing_execute/pci/:program_course_instance_id/massive_charge_enrollments', to: 'lms_managing_execute#massive_charge_enrollments_from_instances', via: :get, as: 'lms_managing_execute_massive_charge_enrollments_from_instances_form'
  match 'lms_managing_execute/pci/:program_course_instance_id/massive_charge_enrollments', to: 'lms_managing_execute#massive_charge_enrollments_from_instances', via: :post, as: 'lms_managing_execute_massive_charge_enrollments_from_instances'
  match 'lms_managing_execute/pci/:program_course_instance_id/massive_charge_enrollments_download_format', to: 'lms_managing_execute#massive_charge_enrollments_from_instances_download_format', via: :get, as: 'lms_managing_execute_massive_charge_enrollments_from_instances_download_format'



  match 'lms_managing_execute/results/list_courses', to: 'lms_managing_execute#results_list_courses', via: :get, as: 'lms_managing_execute_results_list_courses'
  match 'lms_managing_execute/results/:program_course_id', to: 'lms_managing_execute#results_program_course', via: :get, as: 'lms_managing_execute_results_program_course'

  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_massive_charge', to: 'lms_managing_execute#program_course_instance_register_results_massive_charge', via: :get, as: 'lms_managing_execute_pci_register_results_massive_charge'
  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_massive_charge', to: 'lms_managing_execute#program_course_instance_register_results_massive_charge', via: :post, as: 'lms_managing_execute_pci_register_results_massive_charge'
  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_download_format', to: 'lms_managing_execute#program_course_instance_register_results_download_format', via: :get, as: 'lms_managing_execute_pci_register_results_download_format'


  match 'lms_managing_execute/pci/:program_course_instance_id/register_results', to: 'lms_managing_execute#program_course_instance_register_results', via: :get, as: 'lms_managing_execute_pci_register_results'

  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_unit/:unit_id', to: 'lms_managing_execute#program_course_instance_register_results_unit', via: :get, as: 'lms_managing_execute_pci_register_results_unit'
  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_unit_register/:unit_id', to: 'lms_managing_execute#program_course_instance_register_results_unit_register', via: :post, as: 'lms_managing_execute_pci_register_results_unit_register'

  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_evaluation/:evaluation_id', to: 'lms_managing_execute#program_course_instance_register_results_evaluation', via: :get, as: 'lms_managing_execute_pci_register_results_evaluation'
  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_evaluation_register/:evaluation_id', to: 'lms_managing_execute#program_course_instance_register_results_evaluation_register', via: :post, as: 'lms_managing_execute_pci_register_results_evaluation_register'

  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_poll/:poll_id', to: 'lms_managing_execute#program_course_instance_register_results_poll', via: :get, as: 'lms_managing_execute_pci_register_results_poll'
  match 'lms_managing_execute/pci/:program_course_instance_id/register_results_poll_register/:poll_id', to: 'lms_managing_execute#program_course_instance_register_results_poll_register', via: :post, as: 'lms_managing_execute_pci_register_results_poll_register'

  match 'lms_managing_execute/pci/:program_course_instance_id/register_attendance', to: 'lms_managing_execute#program_course_instance_register_attendance', via: :get, as: 'lms_managing_execute_pci_register_attendance'
  match 'lms_managing_execute/pci/:program_course_instance_id/register_attendance_register', to: 'lms_managing_execute#program_course_instance_register_attendance_register', via: :post, as: 'lms_managing_execute_pci_register_attendance_register'

  match 'lms_managing_execute/pci/:program_course_instance_id/register_attendance2', to: 'lms_managing_execute#program_course_instance_register_attendance2', via: :get, as: 'lms_managing_execute_pci_register_attendance2'
  match 'lms_managing_execute/pci/:program_course_instance_id/register_attendance_register2', to: 'lms_managing_execute#program_course_instance_register_attendance_register2', via: :post, as: 'lms_managing_execute_pci_register_attendance_register2'

  match 'lms_managing_execute/pci/:program_course_instance_id/u/:unit_id/register_attendance2', to: 'lms_managing_execute#program_course_instance_unit_register_attendance2', via: :get, as: 'lms_managing_execute_pci_unit_register_attendance2'
  match 'lms_managing_execute/pci/:program_course_instance_id/u/:unit_id/register_attendance_register2', to: 'lms_managing_execute#program_course_instance_unit_register_attendance_register2', via: :post, as: 'lms_managing_execute_pci_unit_register_attendance_register2'

  #lms_reporting_controller
  match 'lms_reporting/r2_0', to: 'lms_reporting#generate_r2_0', via: :post, as: 'lms_reporting_generate_r2_0'
  match 'lms_reporting/r2_1/:program_course_id', to: 'lms_reporting#generate_r2_1', via: :get, as: 'lms_reporting_generate_r2_1'
  match 'lms_reporting/r3_1/:program_course_instance_id', to: 'lms_reporting#generate_r3_1', via: :get, as: 'lms_reporting_generate_r3_1'

  match 'lms_reporting/courses', to: 'lms_reporting#courses', via: :get, as: 'lms_reporting_courses'
  match 'lms_reporting/course/:program_course_id', to: 'lms_reporting#program_course', via: :get, as: 'lms_reporting_program_course'
  match 'lms_reporting/r1/:program_course_instance_id', to: 'lms_reporting#generate_r1', via: :get, as: 'lms_reporting_generate_r1'
  match 'lms_reporting/r2/:program_course_id', to: 'lms_reporting#generate_r2', via: :get, as: 'lms_reporting_generate_r2'
  match 'lms_reporting/r3/:program_course_instance_id', to: 'lms_reporting#generate_r3', via: :get, as: 'lms_reporting_generate_r3'

  match 'lms_reporting/pool_program_course/:program_course_id', to: 'lms_reporting#generate_pool_program_course', via: :get, as: 'lms_reporting_generate_pool_program_course'

  #lms_managing_privileges
  match 'lms_managing_privileges/search', to: 'lms_managing_privileges#search', via: :get, as: 'lms_managing_privileges_search'
  match 'lms_managing_privileges/search', to: 'lms_managing_privileges#search', via: :post, as: 'lms_managing_privileges_search'
  match 'lms_managing_privileges/edit/:user_id', to: 'lms_managing_privileges#edit', via: :get, as: 'lms_managing_privileges_edit'
  match 'lms_managing_privileges/update/:user_id', to: 'lms_managing_privileges#update', via: :post, as: 'lms_managing_privileges_update'

  #lms_characteristics
  resources :lms_characteristics, only: [:index, :edit, :update]

  #lms_admin_enrollments
  match 'lms_admin_enrollments/', to: 'lms_admin_enrollments#index', via: :get, as:'lms_admin_enrollments'
  match 'lms_admin_enrollments/show_list_to_enroll_users', to: 'lms_admin_enrollments#show_list_to_enroll_users', via: :post, as:'lms_show_list_to_enroll_users'
  match 'lms_admin_enrollments/add_users_to_list', to: 'lms_admin_enrollments#add_users_to_list', via: :post, as:'lms_add_users_to_list'
  match 'lms_admin_enrollments/remove_user_from_list/:user_id', to: 'lms_admin_enrollments#remove_user_from_list', via: :post, as:'lms_remove_user_from_list'
  match 'lms_admin_enrollments/show_list_to_enroll_courses', to: 'lms_admin_enrollments#show_list_to_enroll_courses', via: :post, as:'lms_show_list_to_enroll_courses'
  match 'lms_admin_enrollments/add_list_to_enroll_courses', to: 'lms_admin_enrollments#add_courses_to_list', via: :post, as:'lms_add_list_to_enroll_courses'
  match 'lms_admin_enrollments/remove_course_from_list/:program_course_id', to: 'lms_admin_enrollments#remove_course_from_list', via: :post, as:'lms_remove_course_from_list'
  match 'lms_admin_enrollments/save_dates', to: 'lms_admin_enrollments#save_dates_to_course', via: :post, as:'lms_save_dates_to_course'
  match 'lms_admin_enrollments/show_enroll_confirmation', to: 'lms_admin_enrollments#show_enroll_confirmation', via: :post, as:'lms_show_enroll_confirmation'
  match 'lms_admin_enrollments/confirm_enroll', to: 'lms_admin_enrollments#confirm_enroll', via: :post, as:'lms_confirm_enroll'

  #user_hound

  match 'user_hound/', to: 'user_hound#main', via: :get, as: 'user_hound'
  match 'user_hound/show_select2_finder', to: 'user_hound#show_select2_finder', via: :post, as: 'user_hound_show_select2_finder'
  match 'user_hound/show_basic_finder', to: 'user_hound#show_basic_finder', via: :post, as: 'user_hound_show_basic_finder'
  match 'user_hound/find_by_basic_data', to: 'user_hound#find_by_basic_data', via: :post, as: 'user_hound_find_by_basic_data'
  match 'user_hound/show_chars_finder', to: 'user_hound#show_chars_finder', via: :post, as: 'user_hound_show_chars_finder'
  match 'user_hound/find_by_chars', to: 'user_hound#find_by_chars', via: :post, as: 'user_hound_find_by_chars'

  #my_people_information_controller
  match 'mis_colaboradores', to: 'my_people_information#index', via: :get, as: 'my_people_index'
  match 'mis_colaboradores2', to: 'my_people_information#index2', via: :get, as: 'my_people_index2'

  match 'mis_colaboradores/render_graph_row/:user_id/:row', to: 'my_people_information#render_graph_row', via: :get, as: 'my_people_render_graph_row'
  match 'mis_colaboradores/render_graph_row_selected/:user_id/:row', to: 'my_people_information#render_graph_row_selected', via: :get, as: 'my_people_render_graph_row_selected'

  match 'mis_colaboradores3/:user_id', to: 'my_people_information#index3', via: :get, as: 'my_people_index3'
  match 'mis_colaboradores3/render_graph_row/:user_id/:row', to: 'my_people_information#render_graph_row3', via: :get, as: 'my_people_render_graph_row3'
  match 'mis_colaboradores3/render_graph_row_selected/:user_id/:row', to: 'my_people_information#render_graph_row_selected3', via: :get, as: 'my_people_render_graph_row_selected3'

  match 'mis_colaboradores/search', to: 'my_people_information#search', via: :get, as: 'my_people_information_search'
  match 'mis_colaboradores/search', to: 'my_people_information#search', via: :post, as: 'my_people_information_search'

  match 'mi_colabor/:user_id', to: 'my_people_information#user_profile', via: :get, as: 'my_people_mi_colaborador'
  match 'mis_colaboradores/pe_processes', to: 'my_people_information#list_pe_processes', via: :get, as: 'my_people_list_pe_processes'
  match 'mis_colaboradores/pe_process/:hr_process_id', to: 'my_people_information#list_pe_members', via: :get, as: 'my_people_list_pe_members'
  match 'mi_colabor/pe/:hr_process_user_id', to: 'my_people_information#show_pe_member', via: :get, as: 'my_people_show_pe_member'
  match 'download_informative_document/:id/:user_id', to: 'my_people_information#download_informative_document', via: :get, as: 'download_informative_document_my_people'

  match 'mis_colaboradores/download_characteristic_file/:user_id/:characteristic_id', to: 'my_people_information#download_characteristic_file', via: :get, as: 'my_people_information_download_characteristic_file'
  match 'mis_colaboradores/download_user_characteristic_file/:user_id/:user_characteristic_id', to: 'my_people_information#download_user_characteristic_file', via: :get, as: 'my_people_information_download_user_characteristic_file'

  match 'mis_colaboradores/pe_process2/:pe_process_id', to: 'my_people_information#list_pe_members2', via: :get, as: 'my_people_list_pe_members2'
  match 'mis_colaboradores/pe_rep_detailed_final_results/:pe_process_id/:user_id', to: 'my_people_information#pe_rep_detailed_final_results', via: :get, as: 'my_people_information_pe_rep_detailed_final_results'
  match 'mis_colaboradores/pe_rep_detailed_final_results_pdf/:pe_process_id/:user_id', to: 'my_people_information#pe_rep_detailed_final_results_pdf', via: :get, as: 'my_people_information_pe_rep_detailed_final_results_pdf'

  match 'mis_colaboradores/pe_rep_detailed_final_results_from_pe/:current_pe_process_id/:pe_process_id/:user_id', to: 'my_people_information#pe_rep_detailed_final_results_from_pe', via: :get, as: 'my_people_information_pe_rep_detailed_final_results_from_pe'
  match 'mis_colaboradores/pe_rep_detailed_final_results_from_pe_pdf/:current_pe_process_id/:pe_process_id/:user_id', to: 'my_people_information#pe_rep_detailed_final_results_from_pe_pdf', via: :get, as: 'my_people_information_pe_rep_detailed_final_results_from_pe_pdf'

  match 'mis_colaboradores/dncs', to: 'my_people_information#dncs', via: :get, as: 'my_people_dncs'

  resources :programs
  match 'programs/:id/delete', to: 'programs#delete', via: :get, as: 'delete_program'

  match 'programs/:id/search_program_inspector', to: 'programs#search_program_inspector', via: :get, as: 'program_search_program_inspectors'
  match 'programs/:id/search_program_inspector', to: 'programs#search_program_inspector', via: :post, as: 'program_search_program_inspectors'

  match 'programs/:id/create_program_inspector/:user_id', to: 'programs#create_program_inspector', via: :get, as: 'create_program_inspector'
  match 'programs/:id/destroy_program_inspector', to: 'programs#destroy_program_inspector', via: :get, as: 'destroy_program_inspector'

  resources :levels, only: [:new, :edit, :delete, :create, :update, :destroy]
  match 'levels/new/:program_id', to: 'levels#new', via: :get, as: 'new_level'
  match 'levels/delete/:id', to: 'levels#delete', via: :get, as: 'delete_level'

  resources :courses
  match 'courses/delete/:id', to: 'courses#delete', via: :get, as: 'delete_course'
  match 'courses/:id/edit_unit_sequence/:unit_id', to: 'courses#edit_unit_sequence', via: :get, as: 'course_edit_unit_sequence'
  match 'courses/update_unit_sequence', to: 'courses#update_unit_sequence', via: :post, as: 'course_update_unit_sequence'
  match 'courses/:id/edit_evaluation_sequence/:evaluation_id', to: 'courses#edit_evaluation_sequence', via: :get, as: 'course_edit_evaluation_sequence'
  match 'courses/update_evaluation_sequence', to: 'courses#update_evaluation_sequence', via: :post, as: 'course_update_evaluation_sequence'
  match 'courses/:id/edit_poll_sequence/:poll_id', to: 'courses#edit_poll_sequence', via: :get, as: 'course_edit_poll_sequence'
  match 'courses/update_poll_sequence', to: 'courses#update_poll_sequence', via: :post, as: 'course_update_poll_sequence'
  match 'courses/:id/preview', to: 'courses#preview', via: :get, as: 'course_preview'
  match 'courses/:id/preview/:unit_id', to: 'courses#preview', via: :get, as: 'course_unit_preview'
  match 'courses/:id/update_poll_obligatory/:poll_id', to: 'courses#update_poll_obligatory', via: :post, as: 'update_poll_obligatory'
  match 'courses/:id/create_file', to: 'courses#create_file', via: :post, as: 'create_course_file'
  match 'courses/:id/destroy_file/:course_file_id', to: 'courses#destroy_file', via: :get, as: 'destroy_course_file'

  match 'courses/:id/clone', to: 'courses#select_clone_course', via: :get, as: 'select_clone_course'
  match 'courses/:id/clone', to: 'courses#clone_course', via: :post, as: 'clone_course'

  match 'courses/:id/upload_imagen', to: 'courses#upload_imagen', via: :post, as: 'course_upload_imagen'


  resources :program_courses, only: [:new, :create, :edit, :update]
  match 'program_courses/study_plan/:program_id', to: 'program_courses#study_plan', via: :get, as: 'study_plan'
  match 'program_courses/new/:program_id', to: 'program_courses#new', via: :get, as: 'new_program_course'

  match 'program_courses/:id/search_course_manager', to: 'program_courses#search_course_manager', via: :get, as: 'program_courses_search_course_manager'
  match 'program_courses/:id/search_course_manager', to: 'program_courses#search_course_manager', via: :post, as: 'program_courses_search_course_manager'

  match 'program_courses/:id/create_course_manager/:user_id', to: 'program_courses#create_course_manager', via: :get, as: 'create_course_manager'
  match 'program_courses/:id/destroy_course_manager/:pcm_id', to: 'program_courses#destroy_course_manager', via: :get, as: 'destroy_course_manager'

  match 'program_courses/migrate_program/:program_course_id', to: 'program_courses#choose_migrate_program', via: :get, as: 'program_courses_migrate_program'
  match 'program_courses/migrate_program/:program_course_id', to: 'program_courses#migrate_program', via: :post, as: 'program_courses_migrate_program'

  match 'program_courses/migrate_program_massive', to: 'program_courses#migrate_program_massive', via: :get, as: 'program_courses_migrate_program_massive'
  match 'program_courses/migrate_program_massive', to: 'program_courses#migrate_program_massive', via: :post, as: 'program_courses_migrate_program_massive'

  #enrollments

  match 'matriculas', to: 'enrollments#index', via: :get, as: 'enrollments'
  match 'matriculas/program/:program_id', to: 'enrollments#ver_programa', via: :get, as: 'enrollments_ver_programa'
  match 'matriculas/program/:program_id/:year/:month', to: 'enrollments#ver_programa', via: :get, as: 'enrollments_ver_programa_f'




  match 'matricular', to: 'enrollments#matricular1', via: :get, as: 'matricular'
  match 'matricular2', to: 'enrollments#matricular2', via: :post, as: 'matricular2'
  match 'matricular3', to: 'enrollments#matricular3', via: :post, as: 'matricular3'

  match 'anular_matricula_masiva', to: 'enrollments#anular_matricula_masiva1', via: :get, as: 'anular_matricula_masiva'
  match 'anular_matricula_masiva2', to: 'enrollments#anular_matricula_masiva2', via: :post, as: 'anular_matricula_masiva2'
  match 'anular_matricula_masiva3', to: 'enrollments#anular_matricula_masiva3', via: :post, as: 'anular_matricula_masiva3'

  match 'migracion_masiva_ateneus', to: 'enrollments#migracion_masiva_ateneus', via: :get, as: 'migracion_masiva_ateneus'
  match 'migracion_masiva_ateneus', to: 'enrollments#migracion_masiva_ateneus_upload', via: :post, as: 'migracion_masiva_ateneus'
  match 'migration_training_history_format', to: 'enrollments#migration_training_history_format', via: :get, as: 'migration_training_history_format'


  match 'matricula/consultar_grupo_f/:program_course_id/grupo/:grupo_f', to: 'enrollments#consultar_resultados_grupo_f', via: :get, as: 'enrollments_consultar_resultados_grupo_f'

  match 'matricula/registrar_asist_eval/:program_course_id/grupo/:grupo', to: 'enrollments#register_asist_eval', via: :get, as: 'enrollments_register_asist_eval'
  match 'matricula/registrar_asist_eval/:program_course_id/grupo/:grupo', to: 'enrollments#update_asist_eval', via: :post, as: 'enrollments_update_asist_eval'

  match 'matricula/registrar_encuesta/:program_course_id/grupo/:grupo', to: 'enrollments#register_poll', via: :get, as: 'enrollments_register_poll'
  match 'matricula/registrar_encuesta/:program_course_id/grupo/:grupo', to: 'enrollments#update_poll', via: :post, as: 'enrollments_update_poll'

  match 'matricula/define_aplica_sence/:program_course_id/grupo/:grupo', to: 'enrollments#define_aplica_sence', via: :get, as: 'enrollments_define_aplica_sence'
  match 'matricula/define_aplica_sence/:program_course_id/grupo/:grupo', to: 'enrollments#update_aplica_sence', via: :post, as: 'enrollments_update_aplica_sence'

  match 'matricula/anula_matricula/:program_course_id/grupo/:grupo', to: 'enrollments#define_anula_matricula', via: :get, as: 'enrollments_define_anula_matricula'
  match 'matricula/anula_matricula/:program_course_id/grupo/:grupo', to: 'enrollments#update_anula_matricula', via: :post, as: 'enrollments_update_anula_matricula'

  match 'matricula/anula_matricula_grupo_f/:program_course_id/grupo/:grupo_f', to: 'enrollments#define_anula_matricula_grupo_f', via: :get, as: 'enrollments_define_anula_matricula_grupo_f'
  match 'matricula/anula_matricula_grupo_f/:program_course_id/grupo/:grupo_f', to: 'enrollments#update_anula_matricula_grupo_f', via: :post, as: 'enrollments_update_anula_matricula_grupo_f'

  match 'matricula/edit_desde_hasta/:program_course_id/grupo/:grupo', to: 'enrollments#edit_desde_hasta', via: :get, as: 'enrollments_edit_desde_hasta'
  match 'matricula/update_desde_hasta/:program_course_id/grupo/:grupo', to: 'enrollments#update_desde_hasta', via: :post, as: 'enrollments_update_desde_hasta'

  match 'matricula/edit_hasta/:program_course_id/grupo/:grupo_f', to: 'enrollments#edit_hasta', via: :get, as: 'enrollments_edit_hasta'
  match 'matricula/update_hasta/:program_course_id/grupo/:grupo_f', to: 'enrollments#update_hasta', via: :post, as: 'enrollments_update_hasta'

  match 'reportes_certificados', to: 'enrollments#reportes', via: :get, as: 'reportes_certificados'


  match 'matricula/empty_students_list_xlsx/:program_course_id/grupo/:grupo', to: 'enrollments#empty_students_list_xlsx', via: :get, as: 'enrollments_empty_students_list_xlsx'
  match 'matricula/acta_students_xlsx/:program_course_id/grupo/:grupo', to: 'enrollments#acta_students_xlsx', via: :get, as: 'enrollments_acta_students_xlsx'

  match 'matricula/reporte_curso_excel_grupo/:program_course_id/grupo/:grupo', to: 'enrollments#reporte_curso_excel_grupo', via: :get, as: 'enrollments_reporte_curso_excel_grupo'
  match 'matricula/reporte_curso_excel_grupo_f/:program_course_id/grupo/:grupo_f', to: 'enrollments#reporte_curso_excel_grupo_f', via: :get, as: 'enrollments_reporte_curso_excel_grupo_f'
  match 'matricula/reporte_curso_excel_rango/:program_course_id', to: 'enrollments#reporte_curso_excel_rango', via: :get, as: 'enrollments_reporte_curso_excel_rango'
  match 'matricula/reporte_cursos_excel_rango', to: 'enrollments#reporte_cursos_excel_rango', via: :post, as: 'enrollments_reporte_cursos_excel_rango'
  match 'matricula/reporte_cursos_excel_rango_solo_finalizados', to: 'enrollments#reporte_cursos_excel_rango_solo_finalizados', via: :post, as: 'enrollments_reporte_cursos_excel_rango_solo_finalizados'


  match 'matricula/reporte_detallado_por_curso_excel', to: 'enrollments#reporte_detallado_por_curso_excel', via: :post, as: 'enrollments_reporte_detallado_por_curso_excel'


  match 'matricula/reporte_reporte_por_programa_xlsx', to: 'enrollments#reporte_por_programa_xlsx', via: :post, as: 'enrollments_reporte_por_programa_xlsx'

  match 'matricula/reporte_encuesta_satisfaccion_xlsx', to: 'enrollments#reporte_encuesta_satisfaccion_xlsx', via: :post, as: 'enrollments_reporte_encuesta_satisfaccion_xlsx'
  match 'matricula/reporte_encuesta_satisfaccion_con_nombres_xlsx', to: 'enrollments#reporte_encuesta_satisfaccion_con_nombres_xlsx', via: :post, as: 'enrollments_reporte_encuesta_satisfaccion_con_nombres_xlsx'


  match 'matricula/certificados_curso/', to: 'enrollments#certificados_curso', via: :post, as: 'enrollments_certificados_curso'

  resources :units, only: [:new, :edit, :delete, :create, :update, :destroy]
  match 'units/new/:course_id', to: 'units#new', via: :get, as: 'new_unit'
  match 'units/:id/delete', to: 'units#delete', via: :get, as: 'delete_unit'
  match 'units/:id/edit_master_unit', to: 'units#edit_master_unit', via: :get, as: 'edit_unit_master_unit'
  match 'units/update_master_unit', to: 'units#update_master_unit', via: :post, as: 'update_unit_master_unit'
  match 'units/:id/update_finaliza_automaticamente', to: 'units#update_finaliza_automaticamente', via: :post, as: 'update_unit_finaliza_automaticamente'

  resources :master_units
  match 'master_units/:id/delete', to: 'master_units#delete', via: :get, as: 'delete_master_unit'
  match 'master_units/:id/upload_file', to: 'master_units#upload_file_form', via: :get, as: 'upload_file_master_unit'
  match 'master_units/:id/upload_file', to: 'master_units#upload_file', via: :post, as: 'upload_file_master_unit'
  match 'master_units/:id/preview', to: 'master_units#preview', via: :get, as: 'master_unit_preview'

  resources :master_evaluations
  match 'master_evaluations/:id/delete', to: 'master_evaluations#delete', via: :get, as: 'delete_master_evaluation'
  match 'master_evaluations/:id/upload_file', to: 'master_evaluations#upload_file_form', via: :get, as: 'upload_file_master_evaluation'
  match 'master_evaluations/:id/upload_file', to: 'master_evaluations#upload_file', via: :post, as: 'upload_file_master_evaluation'
  match 'master_evaluations/:id/edit_question/:master_evaluation_question_id', to: 'master_evaluations#edit_question', via: :get, as: 'master_evaluation_edit_question'
  match 'master_evaluations/:id/update_question/:master_evaluation_question_id', to: 'master_evaluations#update_question', via: :post, as: 'master_evaluation_update_question'

  resources :master_polls
  match 'master_polls/:id/delete', to: 'master_polls#delete', via: :get, as: 'delete_master_poll'
  match 'master_polls/:id/upload_file', to: 'master_polls#upload_file_form', via: :get, as: 'upload_file_master_poll'
  match 'master_polls/:id/upload_file', to: 'master_polls#upload_file', via: :post, as: 'upload_file_master_poll'

  resources :evaluations, only: [:new, :edit, :delete, :create, :update, :destroy]
  match 'evaluations/new/:course_id', to: 'evaluations#new', via: :get, as: 'new_evaluation'
  match 'evaluations/delete/:id', to: 'evaluations#delete', via: :get, as: 'delete_evaluation'
  match 'evaluations/:id/edit_master_evaluation', to: 'evaluations#edit_master_evaluation', via: :get, as: 'edit_evaluation_master_evaluation'
  match 'evaluations/update_master_evaluation', to: 'evaluations#update_master_evaluation', via: :post, as: 'update_evaluation_master_evaluation'

  resources :polls, only: [:new, :edit, :delete, :create, :update, :destroy]
  match 'polls/new/:course_id', to: 'polls#new', via: :get, as: 'new_poll'
  match 'polls/delete/:id', to: 'polls#delete', via: :get, as: 'delete_poll'
  match 'polls/:id/edit_master_poll', to: 'polls#edit_master_poll', via: :get, as: 'edit_poll_master_poll'
  match 'polls/update_master_poll', to: 'polls#update_master_poll', via: :post, as: 'update_poll_master_poll'

  resources :user_courses
  match 'cursos', to: 'user_courses#index', via: :get, as: 'user_courses'
  match 'cursos/:program_id', to: 'user_courses#index', via: :get, as: 'user_courses_from'
  match 'curso/:id', to: 'user_courses#show', via: :get, as: 'user_course'
  match 'curso/:id/update', to: 'user_courses#update', via: :put, as: 'user_course_update'
  match 'curso/:id/update_automatic', to: 'user_courses#update_automatic', via: :get, as: 'user_course_update_automatic'
  match 'curso/create', to: 'user_courses#create', via: :post, as: 'user_course_create'
  match 'curso/:id/u/:unit_id', to: 'user_courses#show', via: :get, as: 'user_course_unit'
  match 'curso/:id/u/:unit_id/fin', to: 'user_courses#finish_unit', via: :get, as: 'user_course_finish_unit'
  match 'curso/:id/set_milestone/:unit_id/:milestone', to: 'user_courses#set_milestone', via: :get, as: 'user_course_set_milestone'
  match 'curso/:id/get_milestone/:unit_id', to: 'user_courses#get_milestone', via: :get, as: 'user_course_get_milestone'
  match 'curso/:id/publicar_comentario', to: 'user_courses#create_comment', via: :post, as: 'user_course_create_comment'
  match 'curso/:id/descargar_archivo/:course_file_id', to: 'user_courses#download_file', via: :get, as: 'download_course_file'
  match 'curso/:id/e/:evaluation_id', to: 'user_courses#show', via: :get, as: 'user_course_evaluation'
  match 'curso/:id/e/:evaluation_id/start', to: 'user_courses#start_evaluation', via: :post, as: 'start_user_course_evaluation'
  match 'curso/:id/e/:evaluation_id/set_clock', to: 'user_courses#set_clock', via: :get, as: 'set_clock_user_course_evaluation'
  match 'curso/:id/e/:evaluation_id/answer_evaluation', to: 'user_courses#answer_evaluation', via: :post, as: 'answer_user_course_evaluation'
  match 'curso/:id/e/:evaluation_id/unpause_evaluation', to: 'user_courses#unpause_evaluation', via: :post, as: 'unpause_user_course_evaluation'
  match 'curso/:id/p/:poll_id', to: 'user_courses#show', via: :get, as: 'user_course_poll'
  match 'curso/:id/p/:poll_id/start', to: 'user_courses#start_poll', via: :post, as: 'start_user_course_poll'
  match 'curso/:id/p/:poll_id/answer_evaluation', to: 'user_courses#answer_poll', via: :post, as: 'answer_user_course_poll'
  match 'curso/:id/certificado', to: 'user_courses#certificado', via: :get, as: 'user_course_certificado'

  match 'curso/:program_course_id/:enrollment_id/ok_abierto', to: 'user_courses#confirmacion_iniciar_abierto_mobile', via: :get, as: 'user_course_confirmacion_abierto'
  match 'curso/:id/ok_especifico', to: 'user_courses#confirmacion_iniciar_especifico_mobile', via: :get, as: 'user_course_confirmacion_especifico'

  #J_CHARACTERISTIC_TYPE
  match 'j_characteristic_types/index', to: 'j_characteristic_types#index', via: :get, as: 'j_characteristic_type_index'
  match 'j_characteristic_types/new', to: 'j_characteristic_types#new', via: :get, as: 'j_characteristic_type_new'
  match 'j_characteristic_types/create', to: 'j_characteristic_types#create', via: :post, as: 'j_characteristic_type_create'
  match 'j_characteristic_types/:j_characteristic_type_id/edit', to: 'j_characteristic_types#edit', via: :get, as: 'j_characteristic_type_edit'
  match 'j_characteristic_types/:j_characteristic_type_id/update', to: 'j_characteristic_types#update', via: :post, as: 'j_characteristic_type_update'
  match 'j_characteristic_types/:j_characteristic_type_id/delete', to: 'j_characteristic_types#delete', via: :get, as: 'j_characteristic_type_delete'
  match 'j_characteristic_types/:j_characteristic_type_id/destroy', to: 'j_characteristic_types#destroy', via: :post, as: 'j_characteristic_type_destroy'


  #JJ_CHARACTERISTICS
  match 'jj_characteristics/index', to: 'jj_characteristics#index', via: :get, as: 'jj_characteristics_index'
  match 'jj_characteristics/new', to: 'jj_characteristics#new', via: :get, as: 'jj_characteristic_new'
  match 'jj_characteristics/create', to: 'jj_characteristics#create', via: :post, as: 'jj_characteristic_create'

  match 'jj_characteristics/config/:jj_characteristic_id', to: 'jj_characteristics#show', via: :get, as: 'jj_characteristic_config'
  match 'jj_characteristics/edit/:jj_characteristic_id', to: 'jj_characteristics#edit', via: :get, as: 'jj_characteristic_edit'
  match 'jj_characteristics/update/:jj_characteristic_id', to: 'jj_characteristics#update', via: :post, as: 'jj_characteristic_update'

  match 'jj_characteristics/delete/:jj_characteristic_id', to: 'jj_characteristics#delete', via: :get, as: 'jj_characteristic_delete'
  match 'jj_characteristics/destroy/:jj_characteristic_id', to: 'jj_characteristics#destroy', via: :post, as: 'jj_characteristic_destroy'


  match 'jj_characteristics/characteristic/:jj_characteristic_id/create_value', to: 'jj_characteristics#create_value', via: :post, as: 'jj_characteristic_create_value'
  match 'jj_characteristics/edit_value/:jj_characteristic_value_id', to: 'jj_characteristics#edit_value', via: :get, as: 'jj_characteristic_edit_value'
  match 'jj_characteristics/update_value/:jj_characteristic_value_id', to: 'jj_characteristics#update_value', via: :post, as: 'jj_characteristic_update_value'





  resources :characteristics
  match 'characteristics/delete/:id', to: 'characteristics#delete', via: :get, as: 'delete_characteristic'
  match 'characteristics/create_value/:characteristic_id', to: 'characteristics#create_value', via: :post, as: 'characteristics_create_value'
  match 'characteristics/edit_value/:characteristic_value_id', to: 'characteristics#edit_value', via: :get, as: 'characteristics_edit_value'
  match 'characteristics/update_value/:characteristic_value_id', to: 'characteristics#update_value', via: :post, as: 'characteristics_update_value'
  match 'characteristics/destroy_value/:characteristic_value_id', to: 'characteristics#destroy_value', via: :delete, as: 'characteristics_destroy_value'


  match 'characteristicas/uso_antiguo', to: 'characteristics#index2', via: :get, as: 'characteristics_uso_antiguo'
  match 'characteristicas/edit_antiguo/:id', to: 'characteristics#edit_antiguo', via: :get, as: 'edit_characteristic_antiguo'
  match 'characteristicas/update_antiguo/:id', to: 'characteristics#update_antiguo', via: :post, as: 'update_characteristic_antiguo'

  resources :characteristic_types
  match 'characteristic_types/delete/:id', to: 'characteristic_types#delete', via: :get, as: 'delete_characteristic_type'

  resources :folders
  match 'biblioteca', to: 'folders#index', via: :get, as: 'folders'
  match 'biblioteca/:id', to: 'folders#index', via: :get, as: 'folder'
  match 'biblioteca/:id/masive_charge', to: 'folders#load_massive_folder_charge', via: :get, as: 'folder_massive_charge'
  match 'biblioteca/r/masive_charge', to: 'folders#load_massive_folder_charge', via: :get, as: 'folder_root_massive_charge'

  match 'buscar_en_biblioteca', to: 'folders#search', via: :post, as: 'folder_search'

  match 'biblioteca_nuevo_directorio', to: 'folders#new', via: :get, as: 'new_folder'
  match 'biblioteca/:id/nuevo_directorio', to: 'folders#new', via: :get, as: 'new_subfolder'
  match 'biblioteca_nuevo_directorio', to: 'folders#create', via: :post, as: 'create_folder'
  match 'biblioteca/:id/renombrar', to: 'folders#edit', via: :get, as: 'edit_folder'
  match 'biblioteca/:id/renombrar', to: 'folders#update', via: :post, as: 'update_folder'
  match 'biblioteca/:id/eliminar', to: 'folders#destroy', via: :delete, as: 'destroy_folder'

  match 'biblioteca/:id/new_member', to: 'folders#new_member', via: :get, as: 'new_folder_member'
  match 'biblioteca/:id/create_member', to: 'folders#create_member', via: :post, as: 'create_folder_member'
  match 'biblioteca/:id/permiso_lectura', to: 'folders#edit_members', via: :get, as: 'edit_folder_members'
  match 'biblioteca/:id/permiso_lectura', to: 'folders#update_members', via: :post, as: 'update_folder_members'
  match 'biblioteca/:id/destroy_member/:characteristic_id/:folder_member_id', to: 'folders#destroy_member', via: :get, as: 'destroy_folder_member'

  match 'biblioteca/:id/new_owner', to: 'folders#new_owner', via: :get, as: 'new_folder_owner'
  match 'biblioteca/:id/new_owner', to: 'folders#new_owner', via: :post, as: 'new_folder_owner'
  match 'biblioteca/:id/create_owner', to: 'folders#create_owner', via: :post, as: 'create_folder_owner'
  match 'biblioteca/:id/destroy_owner/:user_id', to: 'folders#destroy_owner', via: :get, as: 'destroy_folder_owner'

  match 'biblioteca/:id/new_reader', to: 'folders#new_reader', via: :get, as: 'new_folder_reader'
  match 'biblioteca/:id/new_reader', to: 'folders#new_reader', via: :post, as: 'new_folder_reader'
  match 'biblioteca/:id/create_reader', to: 'folders#create_reader', via: :post, as: 'create_folder_reader'
  match 'biblioteca/:id/destroy_reader/:user_id', to: 'folders#destroy_reader', via: :get, as: 'destroy_folder_reader'
  match 'biblioteca/:id/make_public', to: 'folders#make_public', via: :get, as: 'folder_make_public'
  match 'biblioteca/:id/make_not_public', to: 'folders#make_not_public', via: :get, as: 'folder_make_not_public'


  match 'biblioteca/:id/nuevo_archivo', to: 'folders#new_file', via: :get, as: 'new_folder_file'
  match 'biblioteca/:id/nuevo_archivo', to: 'folders#create_file', via: :post, as: 'create_folder_file'
  match 'biblioteca/:id/archivo/:folder_file_id/editar_archivo', to: 'folders#edit_file', via: :get, as: 'edit_folder_file'
  match 'biblioteca/:id/archivo/:folder_file_id/editar_archivo', to: 'folders#update_file', via: :post, as: 'update_folder_file'

  match 'biblioteca/:id/archivo/:folder_file_id', to: 'folders#show_file', via: :get, as: 'show_folder_file'
  match 'biblioteca/:id/descargar_archivo/:folder_file_id', to: 'folders#download_file', via: :get, as: 'download_folder_file'

  match 'biblioteca/:id/archivo/:folder_file_id/publicar_comentario', to: 'folders#create_folder_file_comment', via: :post, as: 'folder_file_create_comment'
  match 'biblioteca/:id/archivo/:folder_file_id/valorar', to: 'folders#create_folder_file_value', via: :post, as: 'folder_file_create_value'

  match 'biblioteca/:id/unpublish/:folder_file_id', to: 'folders#hidden_file', via: :get, as: 'hidden_folder_file'
  match 'biblioteca/:id/mostrar/:folder_file_id', to: 'folders#unhidden_file', via: :get, as: 'unhidden_folder_file'
  match 'biblioteca/:id/eliminar/:folder_file_id', to: 'folders#destroy_folder_file', via: :get, as: 'destroy_folder_file'
  match 'biblioteca/:id/reemplazar/:folder_file_father_id', to: 'folders#replace_file', via: :get, as: 'replace_folder_file'

  match 'biblioteca_reporte', to: 'folders#reporte_uso', via: :get, as: 'folder_files_reporte_uso'

  match 'biblioteca/dex/:download_token', to: 'folders#download_from_token', via: :get, as: 'biblioteca_download_from_token'




  # folder_managing
  match 'biblioteca_m', to: 'folder_managing#index', via: :get, as: 'folder_managing_folders'
  match 'biblioteca_m/:id', to: 'folder_managing#index', via: :get, as: 'folder_managing_folder'

  match 'buscar_en_biblioteca_m', to: 'folder_managing#search', via: :post, as: 'folder_managing_folder_search'

  match 'biblioteca_m_nuevo_directorio', to: 'folder_managing#new', via: :get, as: 'folder_managing_new_folder'
  match 'biblioteca_m/:id/nuevo_directorio', to: 'folder_managing#new', via: :get, as: 'folder_managing_new_subfolder'
  match 'biblioteca_m_nuevo_directorio', to: 'folder_managing#create', via: :post, as: 'folder_managing_create_folder'
  match 'biblioteca_m/:id/renombrar', to: 'folder_managing#edit', via: :get, as: 'folder_managing_edit_folder'
  match 'biblioteca_m/:id/renombrar', to: 'folder_managing#update', via: :post, as: 'folder_managing_update_folder'
  match 'biblioteca_m/:id/eliminar', to: 'folder_managing#destroy', via: :delete, as: 'folder_managing_destroy_folder'

  match 'biblioteca_m/:id/new_member', to: 'folder_managing#new_member', via: :get, as: 'folder_managing_new_folder_member'
  match 'biblioteca_m/:id/create_member', to: 'folder_managing#create_member', via: :post, as: 'folder_managing_create_folder_member'
  match 'biblioteca_m/:id/permiso_lectura', to: 'folder_managing#edit_members', via: :get, as: 'folder_managing_edit_folder_members'
  match 'biblioteca_m/:id/permiso_lectura', to: 'folder_managing#update_members', via: :post, as: 'folder_managing_update_folder_members'
  match 'biblioteca_m/:id/destroy_member/:characteristic_id/:valor', to: 'folder_managing#destroy_member', via: :get, as: 'folder_managing_destroy_folder_member'

  match 'biblioteca_m/:id/new_owner', to: 'folder_managing#new_owner', via: :get, as: 'folder_managing_new_folder_owner'
  match 'biblioteca_m/:id/new_owner', to: 'folder_managing#new_owner', via: :post, as: 'folder_managing_new_folder_owner'
  match 'biblioteca_m/:id/create_owner', to: 'folder_managing#create_owner', via: :post, as: 'folder_managing_create_folder_owner'
  match 'biblioteca_m/:id/destroy_owner/:user_id', to: 'folder_managing#destroy_owner', via: :get, as: 'folder_managing_destroy_folder_owner'

  match 'biblioteca_m/:id/new_reader', to: 'folder_managing#new_reader', via: :get, as: 'folder_managing_new_folder_reader'
  match 'biblioteca_m/:id/new_reader', to: 'folder_managing#new_reader', via: :post, as: 'folder_managing_new_folder_reader'
  match 'biblioteca_m/:id/create_reader', to: 'folder_managing#create_reader', via: :post, as: 'folder_managing_create_folder_reader'
  match 'biblioteca_m/:id/destroy_reader/:user_id', to: 'folder_managing#destroy_reader', via: :get, as: 'folder_managing_destroy_folder_reader'
  match 'biblioteca_m/:id/make_public', to: 'folder_managing#make_public', via: :get, as: 'folder_managing_make_public'
  match 'biblioteca_m/:id/make_not_public', to: 'folder_managing#make_not_public', via: :get, as: 'folder_managing_make_not_public'

  match 'biblioteca_m/:id/nuevo_archivo', to: 'folder_managing#new_file', via: :get, as: 'folder_managing_new_folder_file'
  match 'biblioteca_m/:id/nuevo_archivo', to: 'folder_managing#create_file', via: :post, as: 'folder_managing_create_folder_file'
  match 'biblioteca_m/:id/archivo/:folder_file_id/editar_archivo', to: 'folder_managing#edit_file', via: :get, as: 'folder_managing_edit_folder_file'
  match 'biblioteca_m/:id/archivo/:folder_file_id/editar_archivo', to: 'folder_managing#update_file', via: :post, as: 'folder_managing_update_folder_file'

  match 'biblioteca_m/:id/archivo/:folder_file_id', to: 'folder_managing#show_file', via: :get, as: 'folder_managing_show_folder_file'
  match 'biblioteca_m/:id/descargar_archivo/:folder_file_id', to: 'folder_managing#download_file', via: :get, as: 'folder_managing_download_folder_file'

  match 'biblioteca_m/:id/archivo/:folder_file_id/publicar_comentario', to: 'folder_managing#create_folder_file_comment', via: :post, as: 'folder_managing_folder_file_create_comment'
  match 'biblioteca_m/:id/archivo/:folder_file_id/valorar', to: 'folder_managing#create_folder_file_value', via: :post, as: 'folder_managing_folder_file_create_value'

  match 'biblioteca_m/:id/unpublish/:folder_file_id', to: 'folder_managing#hidden_file', via: :get, as: 'folder_managing_hidden_folder_file'
  match 'biblioteca_m/:id/mostrar/:folder_file_id', to: 'folder_managing#unhidden_file', via: :get, as: 'folder_managing_unhidden_folder_file'
  match 'biblioteca_m/:id/eliminar/:folder_file_id', to: 'folder_managing#destroy_folder_file', via: :get, as: 'folder_managing_destroy_folder_file'
  match 'biblioteca_m/:id/reemplazar/:folder_file_father_id', to: 'folder_managing#replace_file', via: :get, as: 'folder_managing_replace_folder_file'

  match 'biblioteca_m_reporte', to: 'folder_managing#reporte_uso', via: :get, as: 'folder_managing_folder_files_reporte_uso'



  resources :company_units
  match 'company_units/:id/delete', to: 'company_units#delete', via: :get, as: 'delete_company_unit'

  resources :company_unit_areas, only: [:new, :edit, :delete, :create, :update, :destroy]
  match 'company_unit_areas/new/:company_unit_id', to: 'company_unit_areas#new', via: :get, as: 'new_company_unit_area'
  match 'company_unit_areas/:id/delete', to: 'company_unit_areas#delete', via: :get, as: 'delete_company_unit_area'

  resources :planning_processes
  match 'planning_processes/:id/delete', to: 'planning_processes#delete', via: :get, as: 'delete_planning_process'
  match 'planning_processes/:id/new_unit', to: 'planning_processes#new_unit', via: :get, as: 'new_unit_planning_process'
  match 'planning_processes/:id/create_unit', to: 'planning_processes#create_unit', via: :post, as: 'create_unit_planning_process'

  match 'planning_processes/:id/search_analyst_manager/:planning_process_company_unit_id', to: 'planning_processes#search_analyst_manager', via: :get, as: 'planning_process_search_analyst_manager'
  match 'planning_processes/:id/search_analyst_manager/:planning_process_company_unit_id', to: 'planning_processes#search_analyst_manager', via: :post, as: 'planning_process_search_analyst_manager'
  match 'planning_processes/:id/create_training_analyst/:planning_process_company_unit_id/:user_id', to: 'planning_processes#create_training_analyst', via: :get, as: 'planning_process_create_training_analyst'
  match 'planning_processes/:id/destroy_training_analyst/:planning_process_company_unit_id/:user_id', to: 'planning_processes#destroy_training_analyst', via: :get, as: 'planning_process_destroy_training_analyst'
  match 'planning_processes/:id/create_company_unit_manager/:planning_process_company_unit_id/:user_id', to: 'planning_processes#create_company_unit_manager', via: :get, as: 'planning_process_create_company_unit_manager'
  match 'planning_processes/:id/destroy_company_unit_manager/:planning_process_company_unit_id/:user_id', to: 'planning_processes#destroy_company_unit_manager', via: :get, as: 'planning_process_destroy_company_unit_manager'

  match 'planning_processes/:id/unit/:planning_process_company_unit_id/new_goal', to: 'planning_processes#new_goal', via: :get, as: 'new_goal_planning_process'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/create_goal', to: 'planning_processes#create_goal', via: :post, as: 'create_goal_planning_process'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/edit_goal/:planning_process_goal_id', to: 'planning_processes#edit_goal', via: :get, as: 'edit_goal_planning_process'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/update_goal/:planning_process_goal_id', to: 'planning_processes#update_goal', via: :post, as: 'update_goal_planning_process'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/delete_goal/:planning_process_goal_id', to: 'planning_processes#destroy_goal', via: :get, as: 'delete_goal_planning_process'

  match 'planning_processes/:id/unit/:planning_process_company_unit_id', to: 'planning_processes#show_unit', via: :get, as: 'show_planning_process_unit'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/edit_fecha_fin', to: 'planning_processes#edit_fecha_fin_unit', via: :get, as: 'edit_fecha_fin_planning_process_unit'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/update_fecha_fin', to: 'planning_processes#update_fecha_fin_unit', via: :post, as: 'update_fecha_fin_planning_process_unit'

  match 'planning_processes/:id/unit/:planning_process_company_unit_id/search_manager_superintendent/:company_unit_area_id', to: 'planning_processes#search_manager_superintendent', via: :get, as: 'planning_process_search_manager_superintendent'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/search_manager_superintendent/:company_unit_area_id', to: 'planning_processes#search_manager_superintendent', via: :post, as: 'planning_process_search_manager_superintendent'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/create_area_manager/:company_unit_area_id/:user_id', to: 'planning_processes#create_area_manager', via: :get, as: 'planning_process_create_area_manager'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/create_area_superintendet/:company_unit_area_id/:user_id', to: 'planning_processes#create_area_superintendent', via: :get, as: 'planning_process_create_area_superintendet'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/destroy_area_manager/:company_unit_area_id/:user_id', to: 'planning_processes#destroy_area_manager', via: :get, as: 'planning_process_destroy_area_manager'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/destroy_area_superintendet/:company_unit_area_id/:user_id', to: 'planning_processes#destroy_area_superintendent', via: :get, as: 'planning_process_destroy_area_superintendet'

  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/show_dnc', to: 'planning_processes#show_dnc', via: :get, as: 'planning_process_show_dnc'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/create_dnc', to: 'planning_processes#create_dnc', via: :post, as: 'create_dnc'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/update_dnc/:dnc_id', to: 'planning_processes#update_dnc', via: :post, as: 'update_dnc'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/destroy_dnc/:dnc_id', to: 'planning_processes#destroy_dnc', via: :get, as: 'destroy_dnc'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/dnc_xlsx', to: 'planning_processes#dnc_xlsx', via: :get, as: 'dnc_xlsx'

  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/define_area_goals', to: 'planning_processes#define_area_goals', via: :get, as: 'define_area_goals'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/update_area_goals', to: 'planning_processes#update_area_goals', via: :post, as: 'update_area_goals'

  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/dnc_manage_students/:dnc_id', to: 'planning_processes#dnc_manage_students', via: :get, as: 'planning_process_dnc_manage_students'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/dnc_manage_students/:dnc_id', to: 'planning_processes#dnc_manage_students', via: :post, as: 'planning_process_dnc_manage_students'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/create_dnc_student/:dnc_id', to: 'planning_processes#create_dnc_student', via: :post, as: 'planning_process_create_dnc_student'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/destroy_dnc_student/:dnc_id/student/:user_id', to: 'planning_processes#destroy_dnc_student', via: :get, as: 'planning_process_destroy_dnc_student'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/create_dnc_student_xls/:dnc_id', to: 'planning_processes#create_dnc_student_xls', via: :post, as: 'planning_process_create_dnc_student_xls'

  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/send_to_approve_area', to: 'planning_processes#send_to_approve_area', via: :post, as: 'send_to_approve_area'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/request_dnc_area_correction/:dnc_id', to: 'planning_processes#request_dnc_area_correction', via: :post, as: 'request_dnc_area_correction'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/destroy_dnc_area_correction/:dnc_id/correccion/:dnc_area_correction_id', to: 'planning_processes#destroy_dnc_area_correction', via: :get, as: 'destroy_dnc_area_correction'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/approve_area', to: 'planning_processes#approve_area', via: :post, as: 'approve_area'

  match 'planning_processes/:id/unit/:planning_process_company_unit_id/unit_manager', to: 'planning_processes#unit_manager', via: :get, as: 'unit_manager'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/show_pace', to: 'planning_processes#show_pace', via: :get, as: 'planning_process_show_pace'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/approve_unit', to: 'planning_processes#approve_unit', via: :post, as: 'approve_unit'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/request_area_correction', to: 'planning_processes#request_area_correction', via: :post, as: 'request_area_correction'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/destroy_area_correction/:ppcu_area_correction_id', to: 'planning_processes#destroy_area_correction', via: :get, as: 'destroy_area_correction'
  match 'planning_processes/:id/unit/:planning_process_company_unit_id/area/:company_unit_area_id/pace_xlsx', to: 'planning_processes#pace_xlsx', via: :get, as: 'pace_xlsx'


  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id', to: 'planning_processes#programming_unit', via: :get, as: 'programming_process_unit'
  #match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id', to: 'planning_processes#programming_unit_pace', via: :get, as: 'programming_process_unit_pace'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/mes/:mes_actual', to: 'planning_processes#programming_unit_pace', via: :get, as: 'programming_process_unit_pace'
  match 'planning_processes/:id/programming_unit_indicators/:planning_process_company_unit_id/area/:company_unit_area_id', to: 'planning_processes#programming_unit_pace_indicators', via: :get, as: 'programming_process_unit_pace_indicators'
  match 'planning_processes/:id/programming_unit_indicators1/:planning_process_company_unit_id/area/:company_unit_area_id', to: 'planning_processes#programming_unit_pace_indicators1', via: :get, as: 'programming_process_unit_pace_indicators1'
  match 'planning_processes/:id/programming_unit_indicators2/:planning_process_company_unit_id/area/:company_unit_area_id', to: 'planning_processes#programming_unit_pace_indicators2', via: :get, as: 'programming_process_unit_pace_indicators2'

  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/mes/:mes_actual/create_dncp/', to: 'planning_processes#create_dncp', via: :post, as: 'create_dncp'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/update_dncp/:dncp_id', to: 'planning_processes#update_dncp', via: :post, as: 'update_dncp'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/manage_students/:dncp_id', to: 'planning_processes#programming_manage_students', via: :get, as: 'programming_manage_students'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/manage_students/:dncp_id', to: 'planning_processes#programming_manage_students', via: :post, as: 'programming_manage_students'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/create_student/:dncp_id', to: 'planning_processes#programming_create_student', via: :post, as: 'programming_create_student'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/destroy_student/:dncp_id/student/:user_id', to: 'planning_processes#programming_destroy_student', via: :get, as: 'programming_destroy_student'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/create_student_xls/:dncp_id', to: 'planning_processes#programming_create_student_xls', via: :post, as: 'programming_create_student_xls'

  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/prepare_course/:dncp_id', to: 'planning_processes#programming_prepare_course', via: :get, as: 'programming_prepare_course'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/prrogram_course/:dncp_id', to: 'planning_processes#programming_program_course', via: :post, as: 'programming_program_course'

  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/course_evaluacion/:dncp_id/grupo/:grupo', to: 'planning_processes#programming_course_evaluation', via: :get, as: 'programming_course_evaluation'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/course_evaluacion/:dncp_id/grupo/:grupo/xls', to: 'planning_processes#programming_course_evaluation_xls', via: :get, as: 'programming_course_evaluation_xls'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/course_evaluacion/:dncp_id/grupo/:grupo', to: 'planning_processes#programming_course_evaluate', via: :post, as: 'programming_course_evaluate'
  match 'planning_processes/:id/programming_unit_xls/:planning_process_company_unit_id/area/:company_unit_area_id/course_evaluacion/:dncp_id/grupo/:grupo', to: 'planning_processes#programming_course_evaluation_from_xls', via: :post, as: 'programming_course_evaluation_from_xls'

  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/edit_course_group/:dncp_id/grupo/:grupo', to: 'planning_processes#edit_programming_course_group', via: :get, as: 'edit_programming_course_group'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/update_course_group/:dncp_id/grupo/:grupo', to: 'planning_processes#update_programming_course_group', via: :post, as: 'update_programming_course_group'

  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/course_poll/:dncp_id/grupo/:grupo', to: 'planning_processes#programming_course_poll', via: :get, as: 'programming_course_poll'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/course_poll/:dncp_id/grupo/:grupo', to: 'planning_processes#programming_course_polling', via: :post, as: 'programming_course_poll'

  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/empty_students_list_xlsx/:dncp_id/grupo/:grupo', to: 'planning_processes#empty_students_list_xlsx', via: :get, as: 'empty_students_list_xlsx'
  match 'planning_processes/:id/programming_unit/:planning_process_company_unit_id/area/:company_unit_area_id/acta_students_xlsx/:dncp_id/grupo/:grupo', to: 'planning_processes#acta_students_xlsx', via: :get, as: 'acta_students_xlsx'

  match 'planning_processe/migracion_masiva_excel', to: 'planning_processes#migracion_masiva_excel', via: :get, as: 'planning_processes_migracion_masiva_excel'
  match 'planning_processe/migracion_masiva_excel', to: 'planning_processes#migracion_masiva_excel_upload', via: :post, as: 'planning_processes_migracion_masiva_excel'


  # :planning_programming_indicators

  match 'indicadores_capacitacion_p', to: 'planning_programming_indicators#listar_procesos', via: :get, as: 'planning_programming_indicators_listar_procesos'
  match 'indicadores_capacitacion_p/proceso/:planning_process_id', to: 'planning_programming_indicators#ver_proceso', via: :get, as: 'planning_programming_indicators_ver_proceso'

  match 'indicadores_capacitacion_p/proceso/:planning_process_id/u/:planning_process_company_unit_id', to: 'planning_programming_indicators#ver_unidad', via: :get, as: 'planning_programming_indicators_ver_unidad'

  match 'indicadores_capacitacion_p/proceso/:planning_process_id/u/:planning_process_company_unit_id/mes/:mes_actual', to: 'planning_programming_indicators#ver_unidad', via: :get, as: 'planning_programming_indicators_ver_unidad_mes'
  match 'indicadores_capacitacion_p/proceso/:planning_process_id/u/:planning_process_company_unit_id/:resumen', to: 'planning_programming_indicators#ver_unidad', via: :get, as: 'planning_programming_indicators_ver_unidad_total'

  match 'indicadores_capacitacion_p/proceso/:planning_process_id/u/:planning_process_company_unit_id', to: 'planning_programming_indicators#ver_unidad', via: :post, as: 'planning_programming_indicators_ver_unidad_area'
  match 'indicadores_capacitacion_p/proceso/:planning_process_id/u/:planning_process_company_unit_id/mes/:mes_actual/cua/:company_unit_area_id', to: 'planning_programming_indicators#ver_unidad', via: :get, as: 'planning_programming_indicators_ver_unidad_area_mes_filtro'
  match 'indicadores_capacitacion_p/proceso/:planning_process_id/u/:planning_process_company_unit_id/mes/:mes_actual', to: 'planning_programming_indicators#ver_unidad', via: :post, as: 'planning_programming_indicators_ver_unidad_area_mes'
  match 'indicadores_capacitacion_p/proceso/:planning_process_id/u/:planning_process_company_unit_id/cua/:company_unit_area_id/:resumen', to: 'planning_programming_indicators#ver_unidad', via: :get, as: 'planning_programming_indicators_ver_unidad_area_total'

  match 'indicadores_capacitacion_p/proceso/:planning_process_id/consolidado', to: 'planning_programming_indicators#ver_consolidado', via: :get, as: 'planning_programming_indicators_ver_consolidado'
  match 'indicadores_capacitacion_p/proceso/:planning_process_id/consolidado/mes/:mes_actual', to: 'planning_programming_indicators#ver_consolidado', via: :get, as: 'planning_programming_indicators_ver_consolidado_mes'
  match 'indicadores_capacitacion_p/proceso/:planning_process_id/consolidado/:resumen', to: 'planning_programming_indicators#ver_consolidado', via: :get, as: 'planning_programming_indicators_ver_consolidado_total'


  resources :training_providers
  match 'training_providers/:id/delete', to: 'training_providers#delete', via: :get, as: 'delete_training_provider'

  resources :training_programs
  match 'training_programs/:id/delete', to: 'training_programs#delete', via: :get, as: 'delete_training_program'

  resources :training_modes
  match 'training_modes/:id/delete', to: 'training_modes#delete', via: :get, as: 'delete_training_mode'

  resources :training_types
  match 'training_types/:id/delete', to: 'training_types#delete', via: :get, as: 'delete_training_type'

  resources :training_impact_categories
  match 'training_impact_categories/:id/delete', to: 'training_impact_categories#delete', via: :get, as: 'delete_training_impact_category'

  resources :training_impact_values
  match 'training_impact_values/:id/delete', to: 'training_impact_values#delete', via: :get, as: 'delete_training_impact_value'

  resources :countries
  match 'countries/:id/delete', to: 'countries#delete', via: :get, as: 'delete_country'


  match 'user_information/buscar', to: 'user_information#search', via: :get, as: 'user_information_search'
  match 'user_information/buscar', to: 'user_information#search', via: :post, as: 'user_information_search'
  match 'user_information/user_profile/:id', to: 'user_information#user_profile', via: :get, as: 'user_information_user_profile'

  resources :support_ticket_types
  match 'support_ticket_types/:id/delete', to: 'support_ticket_types#delete', via: :get, as: 'delete_support_ticket_type'


  resources :support_tickets
  match 'mesa_ayuda', to: 'support_tickets#index_user', via: :get, as: 'support_tickets_index_user'
  match 'mesa_ayuda/nuevo', to: 'support_tickets#create_support_ticket_user', via: :post, as: 'create_support_ticket_user'
  match 'support_tickets_atendidos', to: 'support_tickets#index_atendidos', via: :get, as: 'support_tickets_atendidos'
  match 'support_tickets_search', to: 'support_tickets#search', via: :get, as: 'support_tickets_search'
  match 'support_tickets_search', to: 'support_tickets#search', via: :post, as: 'support_tickets_search'

  match 'support_tickets_xls_report', to: 'support_tickets#xls_report', via: :post, as: 'support_tickets_xls_report'


  # course_managers
  match 'gestionar_cursos/:id', to: 'course_managers#groups_list', via: :get, as: 'course_manager_groups_list'

  match 'gestionar_cursos/:id/registrar_asist_eval/:program_course_id/grupo/:grupo', to: 'course_managers#register_asist_eval', via: :get, as: 'course_managers_register_asist_eval'
  match 'gestionar_cursos/:id/registrar_asist_eval/:program_course_id/grupo/:grupo', to: 'course_managers#update_asist_eval', via: :post, as: 'course_managers_update_asist_eval'

  match 'gestionar_cursos/:id/registrar_encuesta/:program_course_id/grupo/:grupo', to: 'course_managers#register_poll', via: :get, as: 'course_managers_register_poll'
  match 'gestionar_cursos/:id/registrar_encuesta/:program_course_id/grupo/:grupo', to: 'course_managers#update_poll', via: :post, as: 'course_managers_update_poll'

  match 'gestionar_cursos/:id/empty_students_list_xlsx/:program_course_id/grupo/:grupo', to: 'course_managers#empty_students_list_xlsx', via: :get, as: 'course_managers_empty_students_list_xlsx'
  match 'gestionar_cursos/:id/acta_students_xlsx/:program_course_id/grupo/:grupo', to: 'course_managers#acta_students_xlsx', via: :get, as: 'course_managers_acta_students_xlsx'

  #human resources

  resources :hr_evaluation_types
  match 'hr_evaluation_types/:id/delete', to: 'hr_evaluation_types#delete', via: :get, as: 'delete_hr_evaluation_type'

  resources :hr_evaluation_type_elements, only: [:new, :create, :edit, :update, :destroy]
  match 'hr_evaluation_type_elements/new/:hr_evaluation_type_id', to: 'hr_evaluation_type_elements#new', via: :get, as: 'new_hr_evaluation_type_element'
  match 'hr_evaluation_type_elements/:id/delete', to: 'hr_evaluation_type_elements#delete', via: :get, as: 'delete_hr_evaluation_type_element'

  resources :hr_process_templates
  match 'hr_process_templates/:id/delete', to: 'hr_process_templates#delete', via: :get, as: 'delete_hr_process_template'
  match 'hr_process_templates/:id/create_quadrant/1d/:hr_process_dimension_g_x_id', to: 'hr_process_templates#create_quadrant', via: :post, as: 'create_hr_process_quadrant_1d'
  match 'hr_process_templates/:id/create_quadrant/2d/:hr_process_dimension_g_x_id/:hr_process_dimension_g_y_id', to: 'hr_process_templates#create_quadrant', via: :post, as: 'create_hr_process_quadrant_2d'
  match 'hr_process_templates/:id/update_quadrant/:hr_process_quadrant_id', to: 'hr_process_templates#update_quadrant', via: :post, as: 'update_hr_process_quadrant'


  resources :hr_process_dimensions, only: [:new, :create, :edit, :update, :destroy]
  match 'hr_process_dimensions/new/:hr_process_template_id', to: 'hr_process_dimensions#new', via: :get, as: 'new_hr_process_dimension'
  match 'hr_process_dimensions/:id/delete', to: 'hr_process_dimensions#delete', via: :get, as: 'delete_hr_process_dimension'

  resources :hr_process_dimension_gs, only: [:new, :create, :edit, :update, :destroy]
  match 'hr_process_dimension_gs/new/:hr_process_dimension_id', to: 'hr_process_dimension_gs#new', via: :get, as: 'new_hr_process_dimension_g'
  match 'hr_process_dimension_gs/:id/delete', to: 'hr_process_dimension_gs#delete', via: :get, as: 'delete_hr_process_dimension_g'

  resources :hr_process_dimension_es, only: [:new, :create, :edit, :update, :destroy]
  match 'hr_process_dimension_es/new/:hr_process_dimension_id', to: 'hr_process_dimension_es#new', via: :get, as: 'new_hr_process_dimension_e'
  match 'hr_process_dimension_es/:id/delete', to: 'hr_process_dimension_es#delete', via: :get, as: 'delete_hr_process_dimension_e'

  resources :hr_processes
  match 'hr_processes/:id/delete', to: 'hr_processes#delete', via: :get, as: 'delete_hr_process'
  match 'hr_processes/:id/edit_template', to: 'hr_processes#edit_template', via: :get, as: 'edit_template_hr_process'
  match 'hr_processes/:id/update_template', to: 'hr_processes#update_template', via: :post, as: 'update_template_hr_process'

  resources :hr_process_levels, only: [:new, :create, :edit, :update, :destroy]
  match 'hr_process_levels/new/:hr_process_id', to: 'hr_process_levels#new', via: :get, as: 'new_hr_process_level'
  match 'hr_process_levels/:id/delete', to: 'hr_process_levels#delete', via: :get, as: 'delete_hr_process_level'

  match 'hr_process_levels/new_fj/:hr_process_id', to: 'hr_process_levels#new_from_j', via: :get, as: 'new_hr_process_level_from_j'

  resources :hr_process_users, only: [:index, :destroy]
  match 'hr_process_users/process/:hr_process_id', to: 'hr_process_users#index', via: :get, as: 'hr_process_users'
  match 'hr_process_users/process/:hr_process_id/carga_participantes', to: 'hr_process_users#carga_participantes', via: :post, as: 'carga_participantes_hr_process'
  match 'hr_process_users/process/:hr_process_id/carga_relaciones', to: 'hr_process_users#carga_relaciones', via: :post, as: 'carga_relaciones_hr_process'

  resources :hr_process_managers, only: [:index, :destroy]
  match 'hr_process_managers/process/:hr_process_id', to: 'hr_process_managers#index', via: :get, as: 'hr_process_managers'
  match 'hr_process_managers/process/:hr_process_id/create_manager/:user_id', to: 'hr_process_managers#create', via: :get, as: 'hr_process_managers_create'

  match 'hr_process_managers/:hr_process_id/search_manager', to: 'hr_process_managers#search_manager', via: :get, as: 'hr_process_managers_search_manager'
  match 'hr_process_managers/:hr_process_id/search_manager', to: 'hr_process_managers#search_manager', via: :post, as: 'hr_process_managers_search_manager'



  resources :hr_process_evaluations, only: [:edit, :update]

  resources :hr_process_evaluation_qs, only: [:index, :show, :edit, :update, :destroy]
  match 'hr_process_evaluation_qs/process/:hr_process_evaluation_id', to: 'hr_process_evaluation_qs#index', via: :get, as: 'hr_process_evaluation_qs'
  match 'hr_process_evaluation_qs/process/:hr_process_evaluation_id/carga_preguntas', to: 'hr_process_evaluation_qs#carga_preguntas', via: :post, as: 'carga_preguntas_hr_process_evaluation_qs'
  match 'hr_process_evaluation_qs/process/:hr_process_evaluation_id/carga_preguntas_fj', to: 'hr_process_evaluation_qs#carga_preguntas_fj', via: :post, as: 'carga_preguntas_hr_process_evaluation_qs_fj'
  match 'hr_process_evaluation_qs/process/:hr_process_evaluation_id/carga_preguntas_fj2', to: 'hr_process_evaluation_qs#carga_preguntas_fj2', via: :post, as: 'carga_preguntas_hr_process_evaluation_qs_fj2'

  match 'manual_hr_process_evaluation_qs/process/:hr_process_evaluation_id', to: 'hr_process_evaluation_qs#index_manual', via: :get, as: 'hr_process_evaluation_qs_manual'
  match 'manual_hr_process_evaluation_qs/process/:hr_process_evaluation_id/carga_preguntas', to: 'hr_process_evaluation_qs#carga_preguntas_manual', via: :post, as: 'carga_preguntas_hr_process_evaluation_qs_manual'

  resources :hr_process_evaluation_q_classifications

  match 'hr_process_evaluation_q_classifications/new/:hr_process_evaluation_id', to: 'hr_process_evaluation_q_classifications#new', via: :get, as: 'new_hr_process_evaluation_q_classification'

  resources :hr_process_evaluation_q_schedules
  match 'hr_process_evaluation_q_schedules/new/:hr_process_evaluation_id', to: 'hr_process_evaluation_q_schedules#new', via: :get, as: 'new_hr_process_evaluation_q_schedule'

  #process_assessment

  match 'ed/listar_procesos', to: 'hr_process_assessments#listar_procesos', via: :get, as: 'hr_process_assessment_listar_procesos'
  match 'ed/listar_procesos/:autoeval', to: 'hr_process_assessments#listar_procesos', via: :get, as: 'hr_process_assessment_listar_procesos_autoeval'

  match 'ed/ver_evaluar/:hr_process_id', to: 'hr_process_assessments#ver_proceso_evaluar', via: :get, as: 'hr_process_assessment_ver_proceso_evaluar'
  match 'ed/ver_feedback/:hr_process_id', to: 'hr_process_assessments#ver_proceso_feedback', via: :get, as: 'hr_process_assessment_ver_proceso_feedback'

  #match 'ed/ver_resultados/:hr_process_id', to: 'hr_process_assessments#ver_proceso_resultados', via: :get, as: 'hr_process_assessment_ver_proceso_resultados'

  match 'ed/ver_calibrar/:hr_process_id', to: 'hr_process_assessments#ver_sesiones_calibracion', via: :get, as: 'hr_process_assessment_ver_sesiones_calibracion'
  match 'ed/ver_calibrar/:hr_process_id/sesion_calibracion/:hr_process_calibration_session_id', to: 'hr_process_assessments#ver_proceso_calibrar', via: :get, as: 'hr_process_assessment_ver_proceso_calibrar'
  match 'ed/ver_calibrar/:hr_process_id/sesion_calibracion/:hr_process_calibration_session_id', to: 'hr_process_assessments#ver_proceso_calibrar', via: :post, as: 'hr_process_assessment_ver_proceso_calibrar'
  match 'ed/calibrar/:hr_process_id/sesion_calibracion/:hr_process_calibration_session_id/m/:hr_process_calibration_session_m_id', to: 'hr_process_assessments#make_calibrar', via: :post, as: 'hr_process_assessment_make_calibrar'


  match 'ed/definir/:hr_process_id/e/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id', to: 'hr_process_assessments#carga_manual', via: :get, as: 'hr_process_assessment_carga_manual'
  match 'ed/logro/:hr_process_id/e/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id', to: 'hr_process_assessments#porcentaje_logro', via: :get, as: 'hr_process_assessment_porcentaje_logro'
  match 'ed/logro_usuario/:hr_process_id/ver/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id/u/:hr_process_user_id', to: 'hr_process_assessments#porcentaje_logro_usuario', via: :get, as: 'hr_process_assessment_porcentaje_logro_usuario'

  match 'ed/create_comment/p/:hr_process_id/pe/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id/u/:hr_process_user_id/f/:from', to: 'hr_process_assessments#create_comment', via: :post, as: 'hr_process_assessment_create_comment'
  match 'ed/destroy_comment/:id/p/:hr_process_id/pe/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id/u/:hr_process_user_id/f/:from', to: 'hr_process_assessments#destroy_comment', via: :get, as: 'hr_process_assessment_destroy_comment'

  match 'ed/create_comment/p/:hr_process_id/u/:hr_process_user_id/f/:from', to: 'hr_process_assessments#create_comment', via: :post, as: 'hr_process_assessment_create_comment_eval_feedback'
  match 'ed/destroy_comment/:id/p/:hr_process_id/u/:hr_process_user_id/f/:from', to: 'hr_process_assessments#destroy_comment', via: :get, as: 'hr_process_assessment_destroy_comment_eval_feedback'

  match 'ed/ver_proceso/:hr_process_id/:autoeval', to: 'hr_process_assessments#ver_proceso', via: :get, as: 'hr_process_assessment_ver_proceso_autoeval'

  match 'ed/evaluar/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#evaluar', via: :get, as: 'hr_process_assessment_evaluar'
  match 'ed/responder_eval/:hr_process_id/e/:hr_process_evaluation_id/u/:hr_process_user_id', to: 'hr_process_assessments#responder_eval', via: :post, as: 'hr_process_assessment_responder_eval'
  match 'ed/responder_responder_porcentaje_logro/:hr_process_id/e/:hr_process_evaluation_id/u/:hr_process_user_id', to: 'hr_process_assessments#responder_porcentaje_logro', via: :post, as: 'hr_process_assessment_responder_responder_porcentaje_logro'
  match 'ed/confirmar_eval/:hr_process_id/u/:hr_process_user_id/rel/:hr_process_user_rel_id', to: 'hr_process_assessments#confirmar_eval', via: :post, as: 'hr_process_assessment_confirmar_eval'
  #match 'ed/estado_avance/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#estado_avance_para_jefe', via: :get, as: 'hr_process_assessment_estado_avance_para_jefe'
  #match 'ed/resulato_final/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#resultado_final_para_jefe', via: :get, as: 'hr_process_assessment_resultado_final_para_jefe'
  match 'ed/resultado_r/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#resultado_para_reporte', via: :get, as: 'hr_process_assessment_resultado_para_reporte'

  match 'ed/feedback/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#feedback', via: :get, as: 'hr_process_assessment_feedback'
  match 'ed/confirmar_feedback/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#confirmar_feedback', via: :post, as: 'hr_process_assessment_confirmar_feedback'



  match 'ed/listar_procesos_gestionar', to: 'hr_process_assessments#listar_procesos_gestionar', via: :get, as: 'hr_process_assessment_listar_procesos_gestionar'
  match 'ed/autorizar_modificaciones/:hr_process_id', to: 'hr_process_assessments#autorizar_modificaciones_1', via: :get, as: 'hr_process_assessment_autorizar_modificaciones_1'
  match 'ed/autorizar_modificaciones2/:hr_process_id/:hr_process_user_id/:hr_process_user_rel_id', to: 'hr_process_assessments#autorizar_modificaciones_2', via: :get, as: 'hr_process_assessment_autorizar_modificaciones_2'
  match 'ed/autorizar_modificaciones_auto/:hr_process_id/:hr_process_user_id', to: 'hr_process_assessments#autorizar_modificaciones_auto', via: :get, as: 'hr_process_assessment_autorizar_modificaciones_auto'


  match 'ed/gestionar_evaluadores/:hr_process_id', to: 'hr_process_assessments#gestionar_evaluadores', via: :get, as: 'hr_process_assessment_gestionar_evaluadores'
  match 'ed/gestionar_evaluadores_1/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#gestionar_evaluadores1', via: :get, as: 'hr_process_assessment_gestionar_evaluadores_1'
  match 'ed/gestionar_evaluadores_1/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#gestionar_evaluadores1', via: :post, as: 'hr_process_assessment_gestionar_evaluadores_1_search'
  match 'ed/gestionar_evaluadores_add/:hr_process_id/u/:hr_process_user_id/ue/:user_id', to: 'hr_process_assessments#gestionar_evaluadores_add', via: :post, as: 'hr_process_assessment_gestionar_evaluadores_add'
  match 'ed/gestionar_evaluadores_delete/:hr_process_id/:hr_process_user_id/:hr_process_user_rel_id', to: 'hr_process_assessments#eliminar_evaluador', via: :get, as: 'hr_process_assessment_eliminar_evaluador'

  match 'ed/gestionar_responsables/:hr_process_id', to: 'hr_process_assessments#gestionar_responsables', via: :get, as: 'hr_process_assessment_gestionar_responsables'
  match 'ed/gestionar_responsables_1/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#gestionar_responsables1', via: :get, as: 'hr_process_assessment_gestionar_responsables_1'
  match 'ed/gestionar_responsables_1/:hr_process_id/u/:hr_process_user_id', to: 'hr_process_assessments#gestionar_responsables1', via: :post, as: 'hr_process_assessment_gestionar_responsables_1_search'
  match 'ed/gestionar_responsables_add/:hr_process_id/u/:hr_process_user_id/ue/:user_id', to: 'hr_process_assessments#gestionar_responsables_add', via: :post, as: 'hr_process_assessment_gestionar_responsables_add'
  match 'ed/gestionar_responsables_delete/:hr_process_id/:hr_process_user_id/:hr_process_user_rel_id', to: 'hr_process_assessments#eliminar_responsable', via: :get, as: 'hr_process_assessment_eliminar_responsable'

  match 'ed/gestionar_evaluado/finalizar_evaluacion/:hr_process_id/:hr_process_user_id', to: 'hr_process_assessments#finalizar_evaluado', via: :get, as: 'hr_process_assessment_finalizar_evaluado'

  match 'ed/listar_procesos_reportes', to: 'hr_process_assessments#listar_procesos_reportes', via: :get, as: 'hr_process_assessment_listar_procesos_reportes'
  match 'ed/reportes/proceso/:hr_process_id', to: 'hr_process_assessments#ver_proceso_lista_reportes', via: :get, as: 'hr_process_assessment_ver_proceso_lista_reportes'
  match 'ed/reportes/reporte_estado_general/:hr_process_id', to: 'hr_process_assessments#reporte_estado_general', via: :get, as: 'hr_process_assessment_reporte_estado_general'

  match 'ed/reportes/filtrar/:hr_process_id', to: 'hr_process_assessments#reporte_con_filtros_ver_filtros', via: :get, as: 'hr_process_assessment_reporte_con_filtros_ver_filtros'
  match 'ed/reportes/reporte_con_filtros/:hr_process_id', to: 'hr_process_assessments#reporte_con_filtros', via: :post, as: 'hr_process_assessment_reporte_con_filtros'

  match 'ed/reportes/por_persona_lista/:hr_process_id', to: 'hr_process_assessments#reporte_por_evaluado_lista', via: :get, as: 'hr_process_assessment_reporte_por_evaluado_lista'
  #match 'ed/reportes/por_persona/:hr_process_id/:hr_process_user_id/:cs_id', to: 'hr_process_assessments#reporte_por_evaluado', via: :get, as: 'hr_process_assessment_reporte_por_evaluado'
  #match 'ed/reportes/por_persona_pdf/:hr_process_id/:hr_process_user_id/:cs_id', to: 'hr_process_assessments#reporte_por_evaluado_pdf', via: :get, as: 'hr_process_assessment_reporte_por_evaluado_pdf'

  match 'ed/add_manual_q_comment/:hr_process_id/e/:hr_process_evaluation_id/u/:hr_process_user_id/mq/:hr_process_manual_q_id', to: 'hr_process_assessments#add_manual_q_comment', via: :post, as: 'hr_process_assessment_add_manual_q_comment'
  match 'ed/el_manual_q_comment/:hr_process_id/e/:hr_process_evaluation_id/u/:hr_process_user_id/mq/:hr_process_manual_q_id/c/:hr_process_manual_qc_id', to: 'hr_process_assessments#destroy_manual_q_comment', via: :get, as: 'hr_process_assessment_eliminar_manual_q_comment'

  #pe_uniq
  match 'pe/uniq', to: 'pe_uniq#index', via: :get, as: 'pe_uniq'
  match 'pe/uniq_assess', to: 'pe_uniq#index_assess', via: :get, as: 'pe_uniq_assess'

  #pe_managing

  match 'pe/managing', to: 'pe_managing#processes_list', via: :get, as: 'pe_managing_processes_list'

  match 'pe/managing/edit_dates/:hr_process_id', to: 'pe_managing#edit_dates', via: :get, as: 'pe_managing_edit_dates'
  match 'pe/managing/update_dates/:hr_process_id', to: 'pe_managing#update_dates', via: :post, as: 'pe_managing_update_dates'

  match 'pe/register_results/1/:hr_process_id', to: 'pe_managing#register_results_1', via: :get, as: 'pe_managing_register_results_1'
  match 'pe/register_results/2/:hr_process_evaluation_id', to: 'pe_managing#register_results_2', via: :get, as: 'pe_managing_register_results_2'
  match 'pe/register_results/3/:hr_process_evaluation_id', to: 'pe_managing#register_results_3', via: :post, as: 'pe_managing_register_results_3'

  #resources :pe_member_rel_shared_comments

  match 'pe/rel_shared_comment/create/:pe_evaluation_id/:pe_member_rel_id/:f', to: 'pe_member_rel_shared_comments#create', via: :post, as: 'pe_member_rel_shared_comments_create_from_definition_by_user'
  match 'pe/rel_shared_comment/destroy/:pe_evaluation_id/:pe_member_rel_shared_comment_id/:f', to: 'pe_member_rel_shared_comments#destroy', via: :get, as: 'pe_member_rel_shared_comments_destroy_from_definition_by_user'

  match 'pe/rel_shared_comment/create/:pe_evaluation_id/:pe_member_rel_id/:f', to: 'pe_member_rel_shared_comments#create', via: :post, as: 'pe_member_rel_shared_comments_create_from_tracking'
  match 'pe/rel_shared_comment/destroy/:pe_evaluation_id/:pe_member_rel_shared_comment_id/:f', to: 'pe_member_rel_shared_comments#destroy', via: :get, as: 'pe_member_rel_shared_comments_destroy_from_tracking'

  match 'pe/rel_shared_comment/create/:pe_member_rel_id/:f', to: 'pe_member_rel_shared_comments#create', via: :post, as: 'pe_member_rel_shared_comments_create_from_assessment'
  match 'pe/rel_shared_comment/destroy/:pe_member_rel_shared_comment_id/:f', to: 'pe_member_rel_shared_comments#destroy', via: :get, as: 'pe_member_rel_shared_comments_destroy_from_assessment'

  #resources :pe_question_comments

  match 'pe/pe_question_comment/create/:pe_question_id/:pe_member_rel_id/:f', to: 'pe_question_comments#create', via: :post, as: 'pe_question_comments_create_from_definition_by_user'
  match 'pe/pe_question_comment/destroy/:pe_question_comment_id/:f', to: 'pe_question_comments#destroy', via: :get, as: 'pe_question_comments_destroy_from_definition_by_user'

  match 'pe/pe_question_comment/create/:pe_question_id/:pe_member_rel_id/:f', to: 'pe_question_comments#create', via: :post, as: 'pe_question_comments_create_from_tracking'
  match 'pe/pe_question_comment/destroy/:pe_question_comment_id/:f', to: 'pe_question_comments#destroy', via: :get, as: 'pe_question_comments_destroy_from_tracking'

  match 'pe/pe_question_comment/create/:pe_question_id/:pe_member_rel_id/:f', to: 'pe_question_comments#create', via: :post, as: 'pe_question_comments_create_from_assessment'
  match 'pe/pe_question_comment/destroy/:pe_question_comment_id/:f', to: 'pe_question_comments#destroy', via: :get, as: 'pe_question_comments_destroy_from_assessment'

  match 'pe/pe_question_comment/create/:pe_question_id/:pe_member_rel_id/:f/:from_another_pe_evaluation_id', to: 'pe_question_comments#create', via: :post, as: 'pe_question_comments_create_from'
  match 'pe/pe_question_comment/destroy/:pe_question_comment_id/:f/:from_another_pe_evaluation_id', to: 'pe_question_comments#destroy', via: :get, as: 'pe_question_comments_destroy_from'

  #resources :pe_question_files

  match 'pe/pe_question_file/create/:pe_question_id/:pe_member_rel_id', to: 'pe_question_files#create', via: :post, as: 'pe_question_files_create_from_assessment'
  match 'pe/pe_question_file/destroy/:pe_question_file_id/:pe_member_rel_id', to: 'pe_question_files#destroy', via: :get, as: 'pe_question_files_destroy_from_assessment'
  match 'pe/pe_question_file/download/:pe_question_file_id/:pe_member_rel_id', to: 'pe_question_files#download', via: :get, as: 'pe_question_files_download_from_assessment'

  #pe_manual_definition

  match 'pe/mqd/pl/:hr_process_id/:hr_process_evaluation_id/:hr_evaluation_type_element_id', to: 'pe_manual_definition#people_list', via: :get, as: 'pe_manual_definition_people_list'
  match 'pe/mqd/show/:hr_process_evaluation_id/:hr_process_user_id', to: 'pe_manual_definition#show', via: :get, as: 'pe_manual_definition_show'

  match 'pe/mqd/new/:hr_process_evaluation_id/:hr_process_user_id/:hr_evaluation_type_element_id', to: 'pe_manual_definition#new', via: :get, as: 'pe_manual_definition_new'
  match 'pe/mqd/new/:hr_process_evaluation_id/:hr_process_user_id/:hr_evaluation_type_element_id/:hr_process_evaluation_manual_q_id', to: 'pe_manual_definition#new', via: :get, as: 'pe_manual_definition_new_l2'
  match 'pe/mqd/create', to: 'pe_manual_definition#create', via: :post, as: 'pe_manual_definition_create'

  match 'pe/mqd/edit/:hr_process_evaluation_manual_q_id', to: 'pe_manual_definition#edit', via: :get, as: 'pe_manual_definition_edit'
  match 'pe/mqd/update/:hr_process_evaluation_manual_q_id', to: 'pe_manual_definition#update', via: :post, as: 'pe_manual_definition_update'

  match 'pe/mqd/destroy/:hr_process_evaluation_manual_q_id', to: 'pe_manual_definition#destroy', via: :delete, as: 'pe_manual_definition_destroy'

  match 'pe/mqd/confirm/:hr_process_evaluation_id/:hr_process_user_id', to: 'pe_manual_definition#confirm', via: :post, as: 'pe_manual_definition_confirm'

  #pe_manual_tracking

  match 'pe/mqt/pl/:hr_process_id/:hr_process_evaluation_id/:hr_evaluation_type_element_id', to: 'pe_manual_tracking#people_list', via: :get, as: 'pe_manual_tracking_people_list'
  match 'pe/mqt/show/:hr_process_evaluation_id/:hr_process_user_id', to: 'pe_manual_tracking#show', via: :get, as: 'pe_manual_tracking_show'

  match 'pe/mqt/track/:hr_process_evaluation_id/:hr_process_user_id', to: 'pe_manual_tracking#track', via: :post, as: 'pe_manual_tracking_track'

  match 'pe/mqt/create_manual_q_comment/:hr_process_evaluation_manual_q_id', to: 'pe_manual_tracking#create_manual_q_comment', via: :post, as: 'pe_manual_tracking_create_manual_q_comment'
  match 'pe/mqt/destroy_manual_q_comment/:hr_process_evaluation_manual_qc_id', to: 'pe_manual_tracking#destroy_manual_q_comment', via: :get, as: 'pe_manual_tracking_destroy_manual_q_comment'

  #pe_assessment
  match 'pe/assess/pl/:hr_process_id', to: 'pe_assessment#people_list', via: :get, as: 'pe_assessment_people_list'
  match 'pe/assess/show/:hr_process_user_id', to: 'pe_assessment#show', via: :get, as: 'pe_assessment_show'
  match 'pe/assess/assess/:hr_process_evaluation_id/:hr_process_user_id', to: 'pe_assessment#assess', via: :post, as: 'pe_assessment_assess'
  match 'pe/assess/confirm/:hr_process_user_id', to: 'pe_assessment#confirm', via: :post, as: 'pe_assessment_confirm'


  match 'pe/assess/create_manual_q_comment/:hr_process_evaluation_manual_q_id', to: 'pe_assessment#create_manual_q_comment', via: :post, as: 'pe_manual_tracking_create_manual_q_comment_assess'
  match 'pe/assess/destroy_manual_q_comment/:hr_process_evaluation_manual_qc_id', to: 'pe_assessment#destroy_manual_q_comment', via: :get, as: 'pe_manual_tracking_destroy_manual_q_comment_assess'

  #pe_reporting

  match 'pe/reporting/lp', to: 'pe_reporting#list_processes', via: :get, as: 'pe_reporting_list_processes'
  match 'pe/reporting/lr/:hr_process_id', to: 'pe_reporting#list_reports', via: :get, as: 'pe_reporting_list_reports'

  match 'pe/reporting/ls/:hr_process_id', to: 'pe_reporting#list_subs', via: :get, as: 'pe_reporting_list_subs'

  match 'pe/reporting/r_state_of_evals/:hr_process_id', to: 'pe_reporting#state_of_evals', via: :get, as: 'pe_reporting_state_of_evals'
  match 'pe/reporting/r_state_of_evals_det/:hr_process_user_id', to: 'pe_reporting#state_of_evals_det', via: :get, as: 'pe_reporting_state_of_evals_det'

  match 'pe/reporting/r_state_of_feedback/:hr_process_id', to: 'pe_reporting#state_of_feedback', via: :get, as: 'pe_reporting_state_of_feedback'

  match 'pe/reporting/r_frc_f/:hr_process_id', to: 'pe_reporting#final_results_consolidate_filter', via: :get, as: 'pe_reporting_final_results_consolidate_filter'
  match 'pe/reporting/r_frc/:hr_process_id', to: 'pe_reporting#final_results_consolidate', via: :post, as: 'pe_reporting_final_results_consolidate'

  match 'pe/reporting/r_frc_f_comp/:hr_process_id', to: 'pe_reporting#final_results_consolidate_filter_comp', via: :get, as: 'pe_reporting_final_results_consolidate_filter_comp'
  match 'pe/reporting/r_frc_comp/:hr_process_id', to: 'pe_reporting#final_results_consolidate_comp', via: :post, as: 'pe_reporting_final_results_consolidate_comp'

  match 'pe/reporting/r_frc_f_comp_2/:hr_process_id', to: 'pe_reporting#final_results_consolidate_filter_comp_2', via: :get, as: 'pe_reporting_final_results_consolidate_filter_comp_2'
  match 'pe/reporting/r_frc_comp_2/:hr_process_id', to: 'pe_reporting#final_results_consolidate_comp_2', via: :post, as: 'pe_reporting_final_results_consolidate_comp_2'


  match 'pe/reporting/r_fr_list_people/:hr_process_id', to: 'pe_reporting#final_results_list_people', via: :get, as: 'pe_reporting_final_results_list_people'
  match 'pe/reporting/r_fr/:hr_process_id/u/:hr_process_user_id/:cs_id', to: 'pe_reporting#final_results', via: :get, as: 'pe_reporting_final_results'

  match 'pe/reporting/r_frfb_pdf/:hr_process_id/u/:hr_process_user_id', to: 'pe_reporting#final_results_for_boss_pdf', via: :get, as: 'pe_reporting_final_results_for_boss_pdf'
  match 'pe/reporting/r_fr_pdf/:hr_process_id/:hr_process_user_id/:cs_id', to: 'pe_reporting#final_results_pdf', via: :get, as: 'pe_reporting_final_results_pdf'

  match 'pe/reporting/r_spfb/:hr_process_id/u/:hr_process_user_id', to: 'pe_reporting#state_of_progress_for_boss', via: :get, as: 'pe_reporting_state_of_progress_for_boss'
  match 'pe/reporting/r_frfb/:hr_process_id/u/:hr_process_user_id', to: 'pe_reporting#final_results_for_boss', via: :get, as: 'pe_reporting_final_results_for_boss'

  match 'pe/reporting/r_frfb_node/:hr_process_user_id', to: 'pe_reporting#final_results_for_boss_node', via: :get, as: 'pe_reporting_final_results_for_boss_node'
  match 'pe/reporting/r_frfb_pdf_node/:hr_process_user_id', to: 'pe_reporting#final_results_for_boss_pdf_node', via: :get, as: 'pe_reporting_final_results_for_boss_pdf_node'
  match 'pe/reporting/r_frfb_pdf_node_f/:hr_process_user_id', to: 'pe_reporting#final_results_for_boss_pdf_node_f', via: :get, as: 'pe_reporting_final_results_for_boss_pdf_node_f'
  match 'pe/reporting/r_frfb_himself_pdf_node/:hr_process_user_id', to: 'pe_reporting#final_results_for_himself_pdf_node', via: :get, as: 'pe_reporting_final_results_for_himself_pdf_node'

  match 'pe/reporting/final_rep_only_boss_pdf/:hr_process_user_id', to: 'pe_reporting#final_rep_only_boss_pdf', via: :get, as: 'pe_reporting_final_rep_only_boss_pdf'

  #pe_reporting_habitat

  match 'pe/reporting_habitat/bpl/:pe_member_id/:r', to: 'pe_reporting_habitat#generate_rep_habitat_bpl', via: :get, as: 'pe_reporting_habitat_bpl'

  #pe_feedback

  match 'pe/feedback/pl/:hr_process_id', to: 'pe_feedback#people_list', via: :get, as: 'pe_feedback_people_list'
  match 'pe/feedback/fb/:hr_process_user_id', to: 'pe_feedback#new_feedback', via: :get, as: 'pe_feedback_new_feedback'
  match 'pe/feedback/fb/:hr_process_user_id', to: 'pe_feedback#create_feedback', via: :post, as: 'pe_feedback_create_feedback'
  match 'pe/feedback/final_rep_pdf/:hr_process_user_id', to: 'pe_feedback#final_rep_pdf', via: :get, as: 'pe_feedback_final_rep_pdf'
  match 'pe/feedback/final_rep_only_boss_pdf/:hr_process_user_id', to: 'pe_feedback#final_rep_only_boss_pdf', via: :get, as: 'pe_feedback_final_rep_only_boss_pdf'

  #pe_feedback_confirmation
  match 'pe/feedback_c/show/:hr_process_id', to: 'pe_feedback_confirmation#show', via: :get, as: 'pe_feedback_confirmation_show'
  match 'pe/feedback_c/confirm/:hr_process_id', to: 'pe_feedback_confirmation#confirm', via: :post, as: 'pe_feedback_confirmation_confirm'

  resources :hr_process_evaluation_manual_qs
  match 'ed/proceso/:hr_process_id/nuevo/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id', to: 'hr_process_evaluation_manual_qs#new', via: :get, as: 'new_hr_process_evaluation_manual_q'
  match 'ed/proceso/:hr_process_id/nuevo/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id/u/:hr_process_user_id', to: 'hr_process_evaluation_manual_qs#new', via: :get, as: 'new_hr_process_evaluation_manual_q_u'
  match 'ed/proceso/:hr_process_id/nuevo/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id/u/:hr_process_user_id/mq_id/:hr_process_evaluation_manual_q_id', to: 'hr_process_evaluation_manual_qs#new', via: :get, as: 'new_hr_process_evaluation_manual_q_u_mq'

  match 'ed/proceso/:hr_process_id/ver/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id/u/:hr_process_user_id', to: 'hr_process_evaluation_manual_qs#ver_usuario', via: :get, as: 'hr_process_evaluation_manual_q_ver_usuario'
  match 'ed/proceso/:hr_process_id/confirmar/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id/u/:hr_process_user_id', to: 'hr_process_evaluation_manual_qs#confirmar', via: :post, as: 'hr_process_evaluation_manual_q_confirmar'
  match 'ed/proceso/:hr_process_id/habilitar_definicion/:hr_process_evaluation_id/et/:hr_evaluation_type_element_id/u/:hr_process_user_id', to: 'hr_process_evaluation_manual_qs#habilitar_definicion', via: :post, as: 'hr_process_evaluation_manual_q_habilitar_definicion'

  #pe_my_results
  match 'pe/my_results/:pe_process_id', to: 'pe_my_results#my_results', via: :get, as: 'pe_my_results'

  #pe_my_subs_results
  match 'pe/my_subs_results/:pe_process_id', to: 'pe_my_subs_results#my_subs_results', via: :get, as: 'pe_my_subs_results'
  match 'pe/my_subs_results/detailed_final_results_person/:pe_member_rel_id', to: 'pe_my_subs_results#rep_detailed_final_results_person', via: :get, as: 'pe_my_subs_results_rep_detailed_final_results_person'
  match 'pe/my_subs_results/detailed_final_results_pdf/:pe_member_rel_id', to: 'pe_my_subs_results#rep_detailed_final_results_pdf', via: :get, as: 'pe_my_subs_results_rep_detailed_final_results_pdf'

  resources :hr_process_calibration_sessions, only: [:show, :create, :edit, :update, :destroy]
  match 'ed/sesiones_calibracion/:hr_process_id', to: 'hr_process_calibration_sessions#index', via: :get, as: 'listar_hr_process_calibration_sessions'
  match 'ed/sesiones_calibracion/:hr_process_id/new', to: 'hr_process_calibration_sessions#new', via: :get, as: 'new_hr_process_calibration_session'
  match 'ed/sesiones_calibracion/:hr_process_id/abrir/:hr_process_calibration_session_id', to: 'hr_process_calibration_sessions#abrir_sesion', via: :get, as: 'hr_process_calibration_session_abrir_sesion'
  match 'ed/sesiones_calibracion/:hr_process_id/finalizar/:hr_process_calibration_session_id', to: 'hr_process_calibration_sessions#finalizar_sesion', via: :get, as: 'hr_process_calibration_session_finalizar_sesion'

  resources :hr_process_calibration_session_cs, only: [:destroy]
  match 'ed/comite_sesion_calibracion/:hr_process_calibration_session_id', to: 'hr_process_calibration_session_cs#index', via: :get, as: 'listar_hr_process_calibration_session_cs'
  match 'ed/comite_sesion_calibracion/:hr_process_calibration_session_id/search', to: 'hr_process_calibration_session_cs#search', via: :post, as: 'hr_process_calibration_session_cs_search'
  match 'ed/comite_sesion_calibracion/:hr_process_calibration_session_id/create/:user_id', to: 'hr_process_calibration_session_cs#create', via: :get, as: 'hr_process_calibration_session_cs_create'
  match 'ed/comite_sesion_calibracion/:hr_process_calibration_session_id/update_manager/:hr_process_calibration_session_c_id', to: 'hr_process_calibration_session_cs#update_manager', via: :post, as: 'hr_process_calibration_session_cs_update_manager'

  resources :hr_process_calibration_session_ms, only: [:destroy]
  match 'ed/participantes_sesion_calibracion/:hr_process_calibration_session_id', to: 'hr_process_calibration_session_ms#index', via: :get, as: 'listar_hr_process_calibration_session_ms'
  match 'ed/participantes_sesion_calibracion/:hr_process_calibration_session_id/search', to: 'hr_process_calibration_session_ms#search', via: :post, as: 'hr_process_calibration_session_ms_search'
  match 'ed/participantes_sesion_calibracion/:hr_process_calibration_session_id/create/:hr_process_user_id', to: 'hr_process_calibration_session_ms#create', via: :get, as: 'hr_process_calibration_session_ms_create'
  match 'ed/participantes_sesion_calibracion/:hr_process_calibration_session_id/create_grupo', to: 'hr_process_calibration_session_ms#create_grupo', via: :post, as: 'hr_process_calibration_session_ms_create_grupo'


  resources :course_certificates, only: [:new, :create, :edit, :update, :destroy]
  match 'course_certificates/new/:course_id', to: 'course_certificates#new', via: :get, as: 'new_course_certificate'
  match 'course_certificates/:id/subir_plantilla', to: 'course_certificates#upload_template_form', via: :get, as: 'upload_course_certificate_template'
  match 'course_certificates/:id/subir_plantilla', to: 'course_certificates#upload_template', via: :post, as: 'upload_course_certificate_template'
  match 'course_certificates/:id/descargar_plantilla', to: 'course_certificates#download_template', via: :get, as: 'download_course_certificate_template'
  match 'course_certificates/:id/preview_certificado', to: 'course_certificates#preview_certificado', via: :get, as: 'preview_course_certificate_template'

  resources :program_inspectors, only: [:index]
  #match 'reportes_capacitacion', to: 'program_inspectors#index', via: :get, as: 'program_inspectors'
  #match 'reportes_capacitacion/empty_students_list_xlsx/:program_course_id/grupo/:grupo', to: 'program_inspectors#empty_students_list_xlsx', via: :get, as: 'program_inspectors_empty_students_list_xlsx'
  #match 'reportes_capacitacion/acta_students_xlsx/:program_course_id/grupo/:grupo', to: 'program_inspectors#acta_students_xlsx', via: :get, as: 'program_inspectors_acta_students_xlsx'
  #match 'reportes_capacitacion/reporte_avance_xlsx/:program_course_id/grupo/:grupo', to: 'program_inspectors#reporte_avance_xlsx', via: :get, as: 'program_inspectors_reporte_avance_xlsx'
  #match 'reportes_capacitacion/reporte_curso_xlsx/:program_course_id', to: 'program_inspectors#reporte_curso_xlsx', via: :get, as: 'program_inspectors_reporte_curso_xlsx'
  #match 'reportes_capacitacion/reporte_general_cursos_xlsx/', to: 'program_inspectors#reporte_general_cursos_xlsx', via: :post, as: 'program_inspectors_reporte_general_cursos_xlsx'
  #match 'reportes_capacitacion/certificados_curso/', to: 'program_inspectors#certificados_curso', via: :post, as: 'program_inspectors_certificados_curso'


  #training_reports
  match 'reportes_capacitacion', to: 'training_reports#index', via: :get, as: 'training_reports'
  match 'reportes_capacitacion_dnc_1', to: 'training_reports#index_dnc_1', via: :get, as: 'training_reports_dnc_1'
  match 'reportes_capacitacion_dnc_2/:planning_process_id', to: 'training_reports#index_dnc_2', via: :get, as: 'training_reports_dnc_2'
  match 'reportes_capacitacion_dnc/:planning_process_id/:planning_process_company_unit_id', to: 'training_reports#index_dnc', via: :get, as: 'training_reports_dnc'
  match 'reportes_capacitacion/general', to: 'training_reports#reporte_general', via: :post, as: 'training_reports_reporte_general'
  match 'reportes_capacitacion/det_evals', to: 'training_reports#reporte_det_evals', via: :post, as: 'training_reports_detalle_evals'
  #match 'reportes_capacitacion/reporte_cursos_excel_rango/', to: 'training_reports#reporte_cursos_excel_rango', via: :post, as: 'training_reports_reporte_cursos_excel_rango'
  match 'reportes_capacitacion/certificados_cursos_rango/', to: 'training_reports#certificados_cursos_rango', via: :post, as: 'training_reports_certificados_cursos_rango'
  match 'reportes_capacitacion/enc_satis/', to: 'training_reports#reporte_encuesta_satisfaccion', via: :post, as: 'training_reports_enc_satis'

  match 'indicadores_capacitacion', to: 'training_reports#lista_cursos', via: :get, as: 'training_indicators'
  match 'indicadores_capacitacion_curso/:program_course_id', to: 'training_reports#indicadores_curso', via: :get, as: 'training_indicators_curso'
  match 'indicadores_capacitacion_curso/:program_course_id', to: 'training_reports#indicadores_curso', via: :post, as: 'training_indicators_curso'
  match 'indicadores_capacitacion_cursos_consolidado', to: 'training_reports#indicadores_cursos_consolidado', via: :get, as: 'training_indicators_cursos_consolidado'
  match 'indicadores_capacitacion_cursos_consolidado', to: 'training_reports#indicadores_cursos_consolidado', via: :post, as: 'training_indicators_cursos_consolidado'

  resources :notifications

  resources :banners, except: [:destroy]
  match 'banners/:id/upload_image', to: 'banners#upload_image', via: :post, as:'banners_upload_image'
  match 'banners/:id/upload_image', to: 'banners#upload_image_form', via: :get, as:'banners_upload_image'

  match 'banners/:id/upload_complement', to: 'banners#upload_complement', via: :post, as:'banners_upload_complement'
  match 'banners/:id/upload_complement', to: 'banners#upload_complement_form', via: :get, as:'banners_upload_complement'

  resources :poll_processes
  match 'poll_processes/:id/execute', to: 'poll_processes#execute', via: :get, as:'poll_processes_execute'
  match 'poll_processes/:id/view_report', to:'poll_processes#view_report', via: :get, as:'poll_processes_view_report'
  match 'poll_processes/:id/export_report', to:'poll_processes#export_report', via: :get, as:'poll_processes_export_report'
  match 'poll_processes/:id/export_report_manager', to:'poll_processes#export_report_manager', via: :get, as:'poll_processes_export_report_manager'
  match 'poll_processes/index/manager', to:'poll_processes#index_manager', via: :get, as:'poll_processes_index_manager'
  match 'poll_processes/:id/form_update_to_everyone', to:'poll_processes#form_update_to_everyone', via: :post, as:'poll_processes_form_update_to_everyone'
  match 'poll_processes/:id/update_to_everyone', to:'poll_processes#update_to_everyone', via: :post, as:'poll_processes_update_to_everyone'
  match 'poll_processes/:id/form_set_chars', to:'poll_processes#form_set_chars', via: :get, as:'poll_processes_form_set_chars'
  match 'poll_processes/:id/set_chars', to:'poll_processes#set_characteristics', via: :post, as:'poll_processes_set_chars'
  match 'poll_processes/:id/form_set_users', to:'poll_processes#form_set_users', via: :get, as:'poll_processes_form_set_users'
  match 'poll_processes/:id/form_set_users', to:'poll_processes#form_set_users', via: :post, as:'poll_processes_form_set_users'
  match 'poll_processes/:id/set_users', to:'poll_processes#set_users', via: :post, as:'poll_processes_set_users'
  match 'poll_processes/:id/report_chars/edit', to:'poll_processes#edit_report_chars', via: :get, as:'poll_processes_edit_report_chars'

  resource :poll_process_users#, only: [:create]

  match 'training_reports_for_admin', to: 'training_reports_for_admin#reportes', via: :get, as: 'training_reports_for_admin_reportes'
  match 'training_reports_for_admin/general', to: 'training_reports_for_admin#reporte_general', via: :post, as: 'training_reports_for_admin_reporte_general'
  match 'training_reports_for_admin/general_2', to: 'training_reports_for_admin#reporte_general_2', via: :post, as: 'training_reports_for_admin_reporte_general_2'
  match 'training_reports_for_admin/por_programa', to: 'training_reports_for_admin#reporte_por_programa', via: :post, as: 'training_reports_for_admin_reporte_por_programa'
  match 'training_reports_for_admin/det_evals', to: 'training_reports_for_admin#detalle_evaluaciones', via: :post, as: 'training_reports_for_admin_detalle_evaluaciones'
  match 'training_reports_for_admin/enc_satis', to: 'training_reports_for_admin#reporte_encuesta_satisfaccion', via: :post, as: 'training_reports_for_admin_reporte_encuesta_satisfaccion'

  match 'training_reports_for_admin/general_remote', to: 'training_reports_for_admin#reporte_general_remote', via: :post, as: 'training_reports_for_admin_reporte_general_remote'

  match 'training_reports_for_admin/get_q_r/:code', to: 'training_reports_for_admin#download_reporte_general_remote', via: :post, as: 'training_reports_for_admin_download_reporte_general_remote'

  resources :ct_modules, only: [:index, :edit, :update]
  match 'ct_modules/change_status/:ct_module_id', to: 'ct_modules#change_status', via: :get, as: 'ct_modules_change_status'

  resources :ct_menus, only: [:index, :edit, :update]

  resources :ct_module_managers, only: [:destroy]
  match 'ct_module_managers/search/:ct_module_id', to: 'ct_module_managers#search', via: :get, as: 'ct_module_managers_search'
  match 'ct_module_managers/search/:ct_module_id', to: 'ct_module_managers#search', via: :post, as: 'ct_module_managers_search'
  match 'ct_module_managers/add_manager/:ct_module_id/:user_id', to: 'ct_module_managers#create', via: :get, as: 'ct_module_managers_create'

  resources :kpi_dashboards
  match 'kpi_dashboards/edit_csv/:id', to: 'kpi_dashboards#edit_csv', via: :get, as: 'kpi_dashboard_edit_csv'
  match 'kpi_dashboards/update_csv/:id', to: 'kpi_dashboards#update_csv', via: :post, as: 'kpi_dashboard_update_csv'
  match 'kpi_dashboards/delete/:id', to: 'kpi_dashboards#delete', via: :get, as: 'kpi_dashboard_delete'
  match 'kpi_dashboards/clone_dashboard/:id', to: 'kpi_dashboards#select_dashboard_to_clone', via: :get, as: 'kpi_dashboard_clone_dashboard'
  match 'kpi_dashboards/clone_dashboard/:id', to: 'kpi_dashboards#clone_dashboard', via: :post, as: 'kpi_dashboard_clone_dashboard'

  resources :kpi_dashboard_managers, only: [:destroy]
  match 'kpi_dashboard_managers/new/:kpi_dashboard_id', to: 'kpi_dashboard_managers#new', via: :get, as: 'new_kpi_dashboard_manager'
  match 'kpi_dashboard_managers/new/:kpi_dashboard_id', to: 'kpi_dashboard_managers#new', via: :post, as: 'new_kpi_dashboard_manager'
  #match 'kpi_dashboard_managers/create/:kpi_dashboard_id/:user_id', to: 'kpi_dashboard_managers#create', via: :get, as: 'create_kpi_dashboard_manager'
  match 'kpi_dashboard_managers/create/:kpi_dashboard_id', to: 'kpi_dashboard_managers#create', via: :post, as: 'create_kpi_dashboard_manager'

  resources :kpi_dashboard_guests, only: [:destroy]
  match 'kpi_dashboard_guests/new/:kpi_dashboard_id', to: 'kpi_dashboard_guests#new', via: :get, as: 'new_kpi_dashboard_guest'
  match 'kpi_dashboard_guests/new/:kpi_dashboard_id', to: 'kpi_dashboard_guests#new', via: :post, as: 'new_kpi_dashboard_guest'
  #match 'kpi_dashboard_guests/create/:kpi_dashboard_id/:user_id', to: 'kpi_dashboard_guests#create', via: :get, as: 'create_kpi_dashboard_guest'
  match 'kpi_dashboard_guests/create/:kpi_dashboard_id', to: 'kpi_dashboard_guests#create', via: :post, as: 'create_kpi_dashboard_guest'

  resources :kpi_indicator_rols, only: [:destroy]
  match 'kpi_indicator_rols/new/:kpi_indicator_id', to: 'kpi_indicator_rols#new', via: :get, as: 'new_kpi_indicator_rol'
  match 'kpi_indicator_rols/new/:kpi_indicator_id', to: 'kpi_indicator_rols#new', via: :post, as: 'new_kpi_indicator_rol'
  match 'kpi_indicator_rols/create/:kpi_indicator_id/:user_id', to: 'kpi_indicator_rols#create', via: :post, as: 'create_kpi_indicator_rol'

  resources :kpi_sets, only: [:edit, :create, :update, :destroy]
  match 'kpi_sets/new/:kpi_dashboard_id', to: 'kpi_sets#new', via: :get, as: 'new_kpi_set'

  resources :kpi_subsets, only: [:edit, :create, :update, :destroy]
  match 'kpi_subsets/new/:kpi_dashboard_id', to: 'kpi_subsets#new', via: :get, as: 'new_kpi_subset'

  resources :kpi_colors, only: [:edit, :create, :update, :destroy]
  match 'kpi_colors/new/:kpi_dashboard_id', to: 'kpi_colors#new', via: :get, as: 'new_kpi_color'

  resources :kpi_rols, only: [:edit, :create, :update, :destroy]
  match 'kpi_rols/new/:kpi_dashboard_id', to: 'kpi_rols#new', via: :get, as: 'new_kpi_rol'

  resources :kpi_indicators, only: [:edit, :create, :update, :destroy]
  match 'kpi_indicators/new/:kpi_set_id', to: 'kpi_indicators#new', via: :get, as: 'new_kpi_indicator'
  match 'kpi_indicators/edit_goal/:id', to: 'kpi_indicators#edit_goal', via: :get, as: 'kpi_indicator_edit_goal'
  match 'kpi_indicators/update_goal/:id', to: 'kpi_indicators#update_goal', via: :post, as: 'kpi_indicator_update_goal'
  match 'kpi_indicators/update_value/:id', to: 'kpi_indicators#update_value', via: :post, as: 'kpi_indicator_update_value'

  resources :kpi_indicator_details, only: [:edit, :create, :update]
  match 'kpi_indicator_details/new/:kpi_indicator_id/:date', to: 'kpi_indicator_details#new', via: :get, as: 'new_kpi_indicator_detail'

  #kpi_managing
  match 'kpi/dashboards', to: 'kpi_managing#index', via: :get, as: 'kpi_managing_dashboards'

  match 'kpi/dashboard/:kpi_dashboard_id', to: 'kpi_managing#manage_indicators', via: :get, as: 'kpi_managing_dashboard_manage_indicators'
  match 'kpi/dashboard/:kpi_dashboard_id/g/', to: 'kpi_managing#manage_guests', via: :get, as: 'kpi_managing_dashboard_manage_guests'
  match 'kpi/dashboard/:kpi_dashboard_id/g/', to: 'kpi_managing#manage_guests', via: :post, as: 'kpi_managing_dashboard_manage_guests'
  match 'kpi/dashboard/:kpi_dashboard_id/g/create_g/:user_id', to: 'kpi_managing#create_dashboard_guest', via: :post, as: 'kpi_managing_dashboard_create_kpi_dashboard_guest'
  match 'kpi/dashboard/g/destroy/:id', to: 'kpi_managing#destroy_dashboard_guest', via: :delete, as: 'kpi_managing_dashboard_destroy_kpi_dashboard_guest'
  match 'kpi/dashboard/g_e/destroy/:id', to: 'kpi_managing#destroy_kpi_set_guest', via: :delete, as: 'kpi_managing_dashboard_destroy_kpi_set_guest'

  match 'kpi/dashboard/:kpi_dashboard_id/q/', to: 'kpi_managing#query', via: :get, as: 'kpi_managing_dashboard_query'

  match 'kpi/download_xls_for_update/:kpi_dashboard_id', to: 'kpi_managing#download_xls_for_update', via: :get, as: 'kpi_managing_dashboard_download_xls_for_update'
  match 'kpi/download_xls_query/:kpi_dashboard_id', to: 'kpi_managing#download_xls_query', via: :get, as: 'kpi_managing_dashboard_download_xls_query'
  match 'kpi/upload_xls_for_update/:kpi_dashboard_id', to: 'kpi_managing#upload_xls_for_update_form', via: :get, as: 'kpi_managing_dashboard_upload_xls_for_update'
  match 'kpi/upload_xls_for_update/:kpi_dashboard_id', to: 'kpi_managing#upload_xls_for_update', via: :post, as: 'kpi_managing_dashboard_upload_xls_for_update'

  match 'kpi/update_csv/:kpi_dashboard_id', to: 'kpi_managing#update_csv', via: :post, as: 'kpi_managing_dashboard_update_csv'

  resources :kpi_measurements, only: [:create, :update]

  resources :j_job_levels

  resources :j_jobs

  match 'j_jobs/new_version/:j_job_id', to: 'j_jobs#new_version', via: :get, as: 'j_jobs_new_version'
  match 'j_jobs/create_version', to: 'j_jobs#create_version', via: :post, as: 'j_jobs_create_version'

  match 'j_jobs/show_history/:j_job_id', to: 'j_jobs#show_history', via: :get, as: 'j_jobs_show_history'

  match 'j_jobs/edit_jj_characteristics/:j_job_id/:j_characteristic_type_id', to: 'j_jobs#edit_jj_characteristics', via: :get, as: 'j_jobs_edit_jj_characteristics'
  match 'j_jobs/update_jj_characteristics/:j_job_id/:j_characteristic_type_id', to: 'j_jobs#update_jj_characteristics', via: :post, as: 'j_jobs_update_jj_characteristics'

  match 'j_jobs/download_jj_characteristic_file/:j_job_id/:jj_characteristic_id', to: 'j_jobs#download_jj_characteristic_file', via: :get, as: 'j_jobs_download_jj_characteristic_file'

  resources :j_functions

  match 'j_functions/new/:j_job_id', to: 'j_functions#new', via: :get, as: 'j_functions_new'


  resources :j_generic_skills

  match 'j_generic_skills/new/:j_job_level_id', to: 'j_generic_skills#new', via: :get, as: 'j_generic_skills_new'

  #resources :j_managing

  match 'j_managing/search_users', to: 'j_managing#search_users', via: :get, as: 'j_managing_search_users'
  match 'j_managing/search_users', to: 'j_managing#search_users', via: :post, as: 'j_managing_search_users'

  match 'j_managing/list_jobs', to: 'j_managing#list_jobs', via: :get, as: 'j_managing_list_jobs'
  match 'j_managing/assign_job_search/:j_job_id', to: 'j_managing#assign_job_search', via: :get, as: 'j_managing_assign_job_search'
  match 'j_managing/assign_job_search/:j_job_id', to: 'j_managing#assign_job_search', via: :post, as: 'j_managing_assign_job_search'
  match 'j_managing/manage_user_job_2/:user_id/:j_job_id', to: 'j_managing#manage_user_job_2', via: :get, as: 'j_managing_manager_user_job_2'

  match 'j_managing/manage_user_job/:user_id', to: 'j_managing#manage_user_job', via: :get, as: 'j_managing_manager_user_job'
  match 'j_managing/assign_job/:user_id', to: 'j_managing#assign_job', via: :post, as: 'j_managing_assign_job'

  match 'j_managing/remove_job/:id', to: 'j_managing#remove_job', via: :delete, as: 'j_managing_remove_job'

  match 'j_managing/hierarchy', to: 'j_managing#hierarchy', via: :get, as: 'j_managing_hierarchy'
  match 'j_managing/hierarchy', to: 'j_managing#hierarchy', via: :post, as: 'j_managing_hierarchy'

  match 'j_managing/assign_as_boss_search/:user_id', to: 'j_managing#assign_as_boss_search', via: :get, as: 'j_managing_assign_as_boss_search'
  match 'j_managing/assign_as_boss_search/:user_id', to: 'j_managing#assign_as_boss_search', via: :post, as: 'j_managing_assign_as_boss_search'
  match 'j_managing/assign_as_boss/:user_boss_id/:user_id', to: 'j_managing#assign_as_boss', via: :get, as: 'j_managing_assign_as_boss'

  match 'j_managing/replace_boss_search/:user_id', to: 'j_managing#replace_boss_search', via: :get, as: 'j_managing_replace_boss_search'
  match 'j_managing/replace_boss_search/:user_id', to: 'j_managing#replace_boss_search', via: :post, as: 'j_managing_replace_boss_search'
  match 'j_managing/replace_boss/:user_boss_id/:user_id', to: 'j_managing#replace_boss', via: :get, as: 'j_managing_replace_boss'

  match 'j_managing/assign_as_sub_search/:user_id', to: 'j_managing#assign_as_sub_search', via: :get, as: 'j_managing_assign_as_sub_search'
  match 'j_managing/assign_as_sub_search/:user_id', to: 'j_managing#assign_as_sub_search', via: :post, as: 'j_managing_assign_as_sub_search'
  match 'j_managing/assign_as_sub/:user_sub_id/:user_id', to: 'j_managing#assign_as_sub', via: :get, as: 'j_managing_assign_as_sub'

  match 'j_managing/massive_replace_hierarchy', to: 'j_managing#massive_replace_hierarchy', via: :get, as: 'j_managing_massive_replace_hierarchy'
  match 'j_managing/massive_replace_hierarchy', to: 'j_managing#massive_replace_hierarchy', via: :post, as: 'j_managing_massive_replace_hierarchy'

  match 'j_managing/massive_change_cc', to: 'j_managing#massive_change_cc', via: :get, as: 'j_managing_massive_change_cc'
  match 'j_managing/massive_change_cc', to: 'j_managing#massive_change_cc', via: :post, as: 'j_managing_massive_change_cc'

  match 'j_managing/massive_change_job', to: 'j_managing#massive_change_job', via: :get, as: 'j_managing_massive_change_job'
  match 'j_managing/massive_change_job', to: 'j_managing#massive_change_job', via: :post, as: 'j_managing_massive_change_job'

  #em_managing module

  #employees module

  match 'em/search', to: 'em_managing#search', via: :get, as: 'em_search'
  match 'em/search', to: 'em_managing#search', via: :post, as: 'em_search'

  match 'em/show/:id', to: 'em_managing#show', via: :get, as: 'em_show_employee'

  match 'em/new', to: 'em_managing#new', via: :get, as: 'em_new_employee'
  match 'em/create', to: 'em_managing#create', via: :post, as: 'em_create_employee'

  match 'em/edit/:id', to: 'em_managing#edit', via: :get, as: 'em_edit_employee'
  match 'em/update/:id', to: 'em_managing#update', via: :put, as: 'em_update_employee'

  match 'em/edit_characteristics/:id', to: 'em_managing#edit_characteristics', via: :get, as: 'em_edit_characteristics_employee'
  match 'em/update_characteristics/:id', to: 'em_managing#update_characteristics', via: :put, as: 'em_update_characteristics_employee'

  match 'em/edit_characteristic_records/:id', to: 'em_managing#edit_characteristic_records', via: :get, as: 'em_edit_characteristic_records_employee'
  match 'em/update_characteristic_records/:id', to: 'em_managing#update_characteristic_records', via: :put, as: 'em_update_characteristic_records_employee'

  match 'em/edit_photo/:id', to: 'em_managing#edit_photo', via: :get, as: 'em_edit_employee_photo'
  match 'em/update_photo/:id', to: 'em_managing#update_photo', via: :post, as: 'em_update_employee_photo'

  match 'em/carga_masiva', to: 'em_managing#carga_masiva', via: :get, as: 'em_employees_carga_masiva'
  match 'em/carga_masiva', to: 'em_managing#carga_masiva_upload', via: :post, as: 'em_employees_carga_masiva'

  match 'em/carga_masiva_sk', to: 'em_managing#carga_masiva_sk', via: :get, as: 'em_employees_carga_masiva_sk'
  match 'em/carga_masiva_sk', to: 'em_managing#carga_masiva_upload_sk', via: :post, as: 'em_employees_carga_masiva_sk'

  match 'em/update_masivo', to: 'em_managing#update_masivo', via: :get, as: 'em_employees_update_masivo'
  match 'em/update_masivo', to: 'em_managing#update_masivo_upload', via: :post, as: 'em_employees_update_masivo'

  match 'em/update_masivo_sk', to: 'em_managing#update_masivo_sk', via: :get, as: 'em_employees_update_masivo_sk'
  match 'em/update_masivo_sk', to: 'em_managing#update_masivo_upload_sk', via: :post, as: 'em_employees_update_masivo_sk'

  match 'em/download_xls_format/:tipo', to: 'em_managing#download_xls_for_massive_create_update', via: :get, as: 'em_download_xls_for_massive_create_update'
  match 'em/download_xls_format_sk/:tipo', to: 'em_managing#download_xls_for_massive_create_update_sk', via: :get, as: 'em_download_xls_for_massive_create_update_sk'


  #tc_managing module

  match 'tc/list_programs', to: 'tc_managing#list_programs', via: :get, as: 'tc_list_programs'

  match 'tc/new_program', to: 'tc_managing#new_program', via: :get, as: 'tc_new_program'
  match 'tc/create_program', to: 'tc_managing#create_program', via: :post, as: 'tc_create_program'

  match 'tc/edit_program/:program_id', to: 'tc_managing#edit_program', via: :get, as: 'tc_edit_program'
  match 'tc/update_program/:program_id', to: 'tc_managing#update_program', via: :post, as: 'tc_update_program'

  match 'tc/configure_certificate/:program_id', to: 'tc_managing#configure_certificate', via: :get, as: 'tc_configure_certificate'
  match 'tc/configure_certificate_c/:program_id', to: 'tc_managing#configure_certificate_create', via: :post, as: 'tc_configure_certificate_create'
  match 'tc/configure_certificate_u/:id', to: 'tc_managing#configure_certificate_update', via: :post, as: 'tc_configure_certificate_update'

  match 'tc/configure_certificate_upload_form/:id', to: 'tc_managing#upload_certificate_template_form', via: :get, as: 'tc_configure_certificate_template_form'
  match 'tc/configure_certificate_upload/:id', to: 'tc_managing#upload_certificate_template', via: :post, as: 'tc_configure_certificate_template_upload'

  match 'tc/configure_certificate_download_teamplate/:id', to: 'tc_managing#download_template', via: :get, as: 'tc_configure_certificate_download_teamplate'
  match 'tc/configure_certificate_preview_certificado/:id', to: 'tc_managing#preview_certificado', via: :get, as: 'tc_configure_certificate_preview_certificado'

  match 'tc/delete_program/:program_id', to: 'tc_managing#delete_program', via: :get, as: 'tc_delete_program'
  match 'tc/destroy_program/:program_id', to: 'tc_managing#destroy_program', via: :post, as: 'tc_destroy_program'

  match 'tc/list_courses', to: 'tc_managing#list_courses', via: :get, as: 'tc_list_courses'

  match 'tc/new_course', to: 'tc_managing#new_course', via: :get, as: 'tc_new_course'
  match 'tc/create_course', to: 'tc_managing#create_course', via: :post, as: 'tc_create_course'

  match 'tc/edit_course/:course_id', to: 'tc_managing#edit_course', via: :get, as: 'tc_edit_course'
  match 'tc/update_course/:course_id', to: 'tc_managing#update_course', via: :post, as: 'tc_update_course'

  match 'tc/delete_course/:course_id', to: 'tc_managing#delete_course', via: :get, as: 'tc_delete_course'
  match 'tc/destroy_course/:course_id', to: 'tc_managing#destroy_course', via: :post, as: 'tc_destroy_course'

  match 'tc/study_plan/:program_id', to: 'tc_managing#study_plan', via: :get, as: 'tc_study_plan'

  match 'tc/new_program_course/:program_id', to: 'tc_managing#new_program_course', via: :get, as: 'tc_new_program_course'
  match 'tc/create_program_course/:program_id', to: 'tc_managing#create_program_course', via: :post, as: 'tc_create_program_course'

  match 'tc/delete_program_course/:program_course_id', to: 'tc_managing#delete_program_course', via: :get, as: 'tc_delete_program_course'
  match 'tc/destroy_program_course/:program_course_id', to: 'tc_managing#destroy_program_course', via: :post, as: 'tc_destroy_program_course'

  #tp2_managing module

  match 'tp2/list_program_instances', to: 'tp2_managing#list_program_instances', via: :get, as: 'tp2_list_program_instances'

  match 'tp2/new_program_instance', to: 'tp2_managing#new_program_instance', via: :get, as: 'tp2_new_program_instance'
  match 'tp2/create_program_instance', to: 'tp2_managing#create_program_instance', via: :post, as: 'tp2_create_program_instance'

  match 'tp2/config_program_instance/:program_instance_id', to: 'tp2_managing#config_program_instance', via: :get, as: 'tp2_config_program_instance'

  match 'tp2/edit_program_instance/:program_instance_id', to: 'tp2_managing#edit_program_instance', via: :get, as: 'tp2_edit_program_instance'
  match 'tp2/update_program_instance/:program_instance_id', to: 'tp2_managing#update_program_instance', via: :post, as: 'tp2_update_program_instance'

  match 'tp2/edit_program_course_instance/:program_course_instance_id', to: 'tp2_managing#edit_program_course_instance', via: :get, as: 'tp2_edit_program_course_instance'
  match 'tp2/update_program_course_instance/:program_course_instance_id', to: 'tp2_managing#update_program_course_instance', via: :post, as: 'tp2_update_program_course_instance'

  match 'tp2/delete_program_instance/:program_instance_id', to: 'tp2_managing#delete_program_instance', via: :get, as: 'tp2_delete_program_instance'
  match 'tp2/destroy_program_instance/:program_instance_id', to: 'tp2_managing#destroy_program_instance', via: :post, as: 'tp2_destroy_program_instance'

  match 'tp2/list_program_instances_to_enroll', to: 'tp2_managing#list_program_instances_to_enroll', via: :get, as: 'tp2_list_program_instances_to_enroll'
  match 'tp2/enroll_from_excel/:program_instance_id', to: 'tp2_managing#form_enroll_from_xlsx', via: :get, as: 'tp2_enroll_from_excel'
  match 'tp2/enroll_from_excel/:program_instance_id', to: 'tp2_managing#enroll_from_xlsx', via: :post, as: 'tp2_enroll_from_excel'
  match 'tp2/download_xls_for_enroll', to: 'tp2_managing#download_xls_for_enroll', via: :get, as: 'tp2_download_xls_for_enroll'

  match 'tp2/enroll_from_search_1/:program_instance_id', to: 'tp2_managing#enroll_from_search_1', via: :get, as: 'tp2_enroll_from_search_1'
  match 'tp2/enroll_from_search_2/:program_instance_id', to: 'tp2_managing#enroll_from_search_2', via: :post, as: 'tp2_enroll_from_search_2'
  match 'tp2/enroll_from_search_3/:program_instance_id', to: 'tp2_managing#enroll_from_search_3', via: :post, as: 'tp2_enroll_from_search_3'

  match 'tp2/list_program_instances_to_modify', to: 'tp2_managing#list_program_instances_to_modify', via: :get, as: 'tp2_list_program_instances_to_modify'
  match 'tp2/unenroll/:program_instance_id', to: 'tp2_managing#form_unenroll', via: :get, as: 'tp2_unenroll'
  match 'tp2/unenroll/:program_instance_id', to: 'tp2_managing#unenroll', via: :post, as: 'tp2_unenroll'
  match 'tp2/list_enrollments_to_modify/:program_instance_id', to: 'tp2_managing#list_enrollments_to_modify', via: :get, as: 'tp2_list_enrollments_to_modify'
  match 'tp2/edit_user_course/:user_course_id', to: 'tp2_managing#edit_user_course', via: :get, as: 'tp2_edit_user_course'
  match 'tp2/update_user_course/:user_course_id', to: 'tp2_managing#update_user_course', via: :post, as: 'tp2_update_user_course'

  match 'tp2/list_enrollments/:program_instance_id', to: 'tp2_managing#list_enrollments', via: :get, as: 'tp2_list_enrollments'
  match 'tp2/list_enrollments_full/:program_instance_id', to: 'tp2_managing#list_enrollments_full', via: :get, as: 'tp2_list_enrollments_full'


  match 'tp2/list_instances_to_reopen', to: 'tp2_managing#list_instances_to_reopen', via: :get, as: 'tp2_list_instances_to_reopen'
  match 'tp2/reopen_instance/:program_instance_id', to: 'tp2_managing#reopen_instance', via: :get, as: 'tp2_reopen_instance'
  match 'tp2/reopen_instance/:program_instance_id', to: 'tp2_managing#make_reopen_instance', via: :post, as: 'tp2_reopen_instance'

  #tr2_managing module

  match 'tr2/list_program_instances', to: 'tr2_managing#list_program_instances', via: :get, as: 'tr2_list_program_instances'

  match 'tr2/show_results/:program_instance_id', to: 'tr2_managing#show_results', via: :get, as: 'tr2_show_results'

  match 'tr2/edit_results_from_excel/:program_instance_id', to: 'tr2_managing#edit_results_from_excel', via: :get, as: 'tr2_edit_results_from_excel'
  match 'tr2/update_results_from_excel/:program_course_instance_id', to: 'tr2_managing#update_results_from_excel', via: :post, as: 'tr2_update_results_from_excel'
  match 'tr2/download_excel_to_edit_results/:program_course_instance_id', to: 'tr2_managing#download_excel_to_edit_results', via: :get, as: 'tr2_download_excel_to_edit_results'

  match 'tr2/edit_results/:program_course_instance_id', to: 'tr2_managing#edit_results', via: :get, as: 'tr2_edit_results'
  match 'tr2/update_results/:program_course_instance_id', to: 'tr2_managing#update_results', via: :post, as: 'tr2_update_results'

  match 'tr2/show_results_es/:program_instance_id', to: 'tr2_managing#show_results_es', via: :get, as: 'tr2_show_results_es'
  match 'tr2/edit_results_es/:program_course_instance_id', to: 'tr2_managing#edit_results_es', via: :get, as: 'tr2_edit_results_es'
  match 'tr2/update_results_es/:program_course_instance_id', to: 'tr2_managing#update_results_es', via: :post, as: 'tr2_update_results_es'


  match 'tr2/finish_instance/:program_instance_id', to: 'tr2_managing#finish_instance', via: :get, as: 'tr2_finish_instance'
  match 'tr2/finish_instance/:program_instance_id', to: 'tr2_managing#make_finish_instance', via: :post, as: 'tr2_finish_instance'

  #tr2_reporting module

  match 'tr2/rep/list_program_instances', to: 'tr2_reporting#list_program_instances', via: :get, as: 'tr2_rep_list_program_instances'

  match 'tr2/rep/show_program_results/:program_instance_id', to: 'tr2_reporting#show_program_results', via: :get, as: 'tr2_rep_show_program_results'

  match 'tr2/rep/rep_fact_list_program_instances', to: 'tr2_reporting#rep_fact_list_program_instances', via: :get, as: 'tr2_rep_fact_list_program_instances'
  match 'tr2/rep/rep_fact_list_program_course_instances/:program_instance_id', to: 'tr2_reporting#rep_fact_list_program_course_instances', via: :get, as: 'tr2_rep_fact_list_program_course_instances'
  match 'tr2/rep/rep_fact/:program_course_instance_id', to: 'tr2_reporting#rep_fact', via: :get, as: 'tr2_rep_fact'

  match 'tr2/rep/rep_carga_otic_list_program_instances', to: 'tr2_reporting#rep_carga_otic_list_program_instances', via: :get, as: 'tr2_rep_carga_otic_list_program_instances'
  match 'tr2/rep/rep_carga_otic_list_program_course_instances/:program_instance_id', to: 'tr2_reporting#rep_carga_otic_list_program_course_instances', via: :get, as: 'tr2_rep_carga_otic_list_program_course_instances'
  match 'tr2/rep/rep_carga_otic_xls/:program_course_instance_id', to: 'tr2_reporting#rep_carga_otic_xls', via: :get, as: 'tr2_rep_carga_otic_xls'
  match 'tr2/rep/rep_carga_otic_csv/:program_course_instance_id', to: 'tr2_reporting#rep_carga_otic_csv', via: :get, as: 'tr2_rep_carga_otic_csv'

  match 'tr2/rep/rep_consolidado_global_list_program_instances', to: 'tr2_reporting#rep_consolidado_global_list_program_instances', via: :get, as: 'tr2_rep_consolidado_global_list_program_instances'
  match 'tr2/rep/rep_consolidado_global/:program_instance_id', to: 'tr2_reporting#rep_consolidado_global', via: :get, as: 'tr2_rep_consolidado_global'

  match 'tr2/rep/rep_resultados_finales_list_program_instances', to: 'tr2_reporting#rep_resultados_finales_list_program_instances', via: :get, as: 'tr2_rep_resultados_finales_list_program_instances'
  match 'tr2/rep/rep_resultados_finales/:program_instance_id', to: 'tr2_reporting#rep_resultados_finales', via: :get, as: 'tr2_rep_resultados_finales'
  match 'tr2/rep/rep_certificados/:program_instance_id', to: 'tr2_reporting#rep_certificados', via: :get, as: 'tr2_rep_certificados'

  resources :informative_documents
  match 'informative_documents/f/:inf_doc_folder_id/', to: 'informative_documents#index', via: :get, as: 'informative_documents_by_folder'
  match 'informative_documents/f/:inf_doc_folder_id/new', to: 'informative_documents#new', via: :get, as: 'new_informative_document_by_folder'
  resources :inf_doc_folders, except: [:show, :index]

  match 'informative_documents/edit_files/:id', to: 'informative_documents#edit_files', via: :get, as: 'informative_documents_edit_files'
  match 'informative_documents/update_files/:id', to: 'informative_documents#update_files', via: :post, as: 'informative_documents_update_files'



  resources :pe_processes

  match 'pe_processes/new/clone', to: 'pe_processes#new_clone', via: :get, as: 'pe_process_new_clone'
  match 'pe_processes/create/clone', to: 'pe_processes#create_clone', via: :post, as: 'pe_process_create_clone'

  match 'pe_processes/:id/:tab', to: 'pe_processes#show', via: :get, as: 'pe_process_tab'
  match 'pe_processes/show_partial/:id/:tab', to: 'pe_processes#show_partial', via: :get, as: 'pe_process_show_partial'

  match 'pe_processes/:id/search_members', to: 'pe_processes#search_members', via: :post, as: 'pe_process_search_members'
  match 'pe_processes/:id/search_members_evaluated', to: 'pe_processes#search_members_evaluated', via: :post, as: 'pe_process_search_members_evaluated'
  match 'pe_processes/:id/search_members_evaluator', to: 'pe_processes#search_members_evaluator', via: :post, as: 'pe_process_search_members_evaluator'
  match 'pe_processes/:id/search_assessment_validators', to: 'pe_processes#search_assessment_validators', via: :post, as: 'pe_process_search_assessment_validators'
  match 'pe_processes/:id/search_feedback_providers', to: 'pe_processes#search_feedback_providers', via: :post, as: 'pe_process_search_feedback_providers'
  match 'pe_processes/:id/search_observers', to: 'pe_processes#search_observers', via: :post, as: 'pe_process_search_observers'

  match 'pe_process/edit_steps/:id', to: 'pe_processes#edit_steps', via: :get, as: 'pe_process_edit_steps'
  match 'pe_process/update_steps/:id', to: 'pe_processes#update_steps', via: :post, as: 'pe_process_update_steps'

  match 'pe_process/edit_notifications/:id', to: 'pe_processes#edit_notifications', via: :get, as: 'pe_process_edit_notifications'
  match 'pe_process/update_notifications/:id', to: 'pe_processes#update_notifications', via: :post, as: 'pe_process_update_notifications'

  match 'pe_process/edit_notifications_q_defs/:id', to: 'pe_processes#edit_notifications_q_defs', via: :get, as: 'pe_process_edit_notifications_q_defs'
  match 'pe_process/update_notifications_q_defs/:id', to: 'pe_processes#update_notifications_q_defs', via: :post, as: 'pe_process_update_notifications_q_defs'

  match 'pe_process/edit_notifications_q_defs_boss/:id', to: 'pe_processes#edit_notifications_q_defs_boss', via: :get, as: 'pe_process_edit_notifications_q_defs_boss'
  match 'pe_process/update_notifications_q_defs_boss/:id', to: 'pe_processes#update_notifications_q_defs_boss', via: :post, as: 'pe_process_update_notifications_q_defs_boss'

  match 'pe_process/edit_notifications_validation/:id', to: 'pe_processes#edit_notifications_validation', via: :get, as: 'pe_process_edit_notifications_validation'
  match 'pe_process/update_notifications_validation/:id', to: 'pe_processes#update_notifications_validation', via: :post, as: 'pe_process_update_notifications_validation'

  match 'pe_process/edit_notifications_feedback_accepted/:id', to: 'pe_processes#edit_notifications_feedback_accepted', via: :get, as: 'pe_process_edit_notifications_feedback_accepted'
  match 'pe_process/update_notifications_feedback_accepted/:id', to: 'pe_processes#update_notifications_feedback_accepted', via: :post, as: 'pe_process_update_notifications_feedback_accepted'

  match 'pe_process/edit_notifications_feedback_accepted_to_boss/:id', to: 'pe_processes#edit_notifications_feedback_accepted_to_boss', via: :get, as: 'pe_process_edit_notifications_feedback_accepted_to_boss'
  match 'pe_process/update_notifications_feedback_accepted_to_boss/:id', to: 'pe_processes#update_notifications_feedback_accepted_to_boss', via: :post, as: 'pe_process_update_notifications_feedback_accepted_to_boss'

  match 'pe_process/edit_notifications_def_val/:id/:before_accepted/:num_step', to: 'pe_processes#edit_notifications_def_val', via: :get, as: 'pe_process_edit_notifications_def_val'
  match 'pe_process/edit_notifications_def_val/:id/:before_accepted/:num_step', to: 'pe_processes#update_notifications_def_val', via: :post, as: 'pe_process_update_notifications_def_val'

  match 'pe_process/edit_display_results/:id', to: 'pe_processes#edit_display_results', via: :get, as: 'pe_process_edit_display_results'
  match 'pe_process/update_display_results/:id', to: 'pe_processes#update_display_results', via: :post, as: 'pe_process_update_display_results'

  match 'pe_process/edit_manage_options/:id', to: 'pe_processes#edit_manage_options', via: :get, as: 'pe_process_edit_manage_optionss'
  match 'pe_process/update_manage_options/:id', to: 'pe_processes#update_manage_options', via: :post, as: 'pe_process_update_manage_options'

  match 'pe_process/edit_step_img/:id/:step', to: 'pe_processes#edit_step_img', via: :get, as: 'pe_process_edit_step_img'
  match 'pe_process/update_step_img/:id/:step', to: 'pe_processes#update_step_img', via: :post, as: 'pe_process_update_step_img'
  match 'pe_process/delete_step_img/:id/:step', to: 'pe_processes#delete_step_img', via: :get, as: 'pe_process_delete_step_img'

  match 'pe_process/edit_feedback/:id', to: 'pe_processes#edit_feedback', via: :get, as: 'pe_process_edit_feedback'
  match 'pe_process/update_feedback/:id', to: 'pe_processes#update_feedback', via: :post, as: 'pe_process_update_feedback'

  match 'pe_process/edit_feedback_accepted/:id', to: 'pe_processes#edit_feedback_accepted', via: :get, as: 'pe_process_edit_feedback_accepted'
  match 'pe_process/update_feedback_accepted/:id', to: 'pe_processes#update_feedback_accepted', via: :post, as: 'pe_process_update_feedback_accepted'

  match 'pe_process/edit_rep_detailed_final_results/:id', to: 'pe_processes#edit_rep_detailed_final_results', via: :get, as: 'pe_process_edit_rep_detailed_final_results'
  match 'pe_process/update_rep_detailed_final_results/:id', to: 'pe_processes#update_rep_detailed_final_results', via: :post, as: 'pe_process_update_rep_detailed_final_results'

  match 'pe_process/edit_rep_detailed_final_results_auto/:id', to: 'pe_processes#edit_rep_detailed_final_results_auto', via: :get, as: 'pe_process_edit_rep_detailed_final_results_auto'
  match 'pe_process/update_rep_detailed_final_results_auto/:id', to: 'pe_processes#update_rep_detailed_final_results_auto', via: :post, as: 'pe_process_update_rep_detailed_final_results_auto'

  match 'pe_process/edit_active_reportes/:id', to: 'pe_processes#edit_active_reportes', via: :get, as: 'pe_process_edit_active_reportes'
  match 'pe_process/update_active_reports/:id', to: 'pe_processes#update_active_reports', via: :post, as: 'pe_process_update_active_reports'

  match 'pe_process/re_calculate_assessments/:id', to: 'pe_processes#re_calculate_assessments', via: :get, as: 'pe_process_re_calculate_assessments'

  resources :pe_rels, only: [:new, :create, :edit, :update, :destroy]
  match 'pe_rels/new/:pe_process_id', to: 'pe_rels#new', via: :get, as: 'new_pe_rel'

  resources :pe_dimensions, only: [:edit, :update]

  resources :pe_dimension_groups, only: [:create, :edit, :update, :destroy]
  match 'pe_dimension_groups/new/:pe_process_id', to: 'pe_dimension_groups#new', via: :get, as: 'new_pe_dimension_group'

  resources :pe_evaluation_groups, only: [:create, :edit, :update, :destroy]
  match 'pe_evaluation_groups/new/:pe_process_id', to: 'pe_evaluation_groups#new', via: :get, as: 'new_pe_evaluation_group'

  resources :pe_slider_groups, only: [:create, :edit, :update, :destroy]
  match 'pe_slider_groups/new/:pe_evaluation_id', to: 'pe_slider_groups#new', via: :get, as: 'new_pe_slider_group'

  resources :pe_process_tutorials, only: [:create, :show, :edit, :update, :destroy]
  match 'pe_process_tutorials/new/:pe_process_id', to: 'pe_process_tutorials#new', via: :get, as: 'new_pe_process_tutorial'

  #resources :pe_boxes

  match 'pe_boxes/create_1d/:pe_process_id/:pe_dimension_group_x_id', to: 'pe_boxes#create_1d', via: :post, as: 'pe_boxes_create_1d'
  match 'pe_boxes/update_1d/:id', to: 'pe_boxes#update_1d', via: :post, as: 'pe_boxes_update_1d'

  match 'pe_boxes/create_2d/:pe_process_id/:pe_dimension_group_x_id/:pe_dimension_group_y_id', to: 'pe_boxes#create_2d', via: :post, as: 'pe_boxes_create_2d'
  match 'pe_boxes/update_2d/:id', to: 'pe_boxes#update_2d', via: :post, as: 'pe_boxes_update_2d'


  resources :pe_evaluations, only: [:new, :create, :show, :edit, :update, :destroy]
  match 'pe_evaluations/new/:pe_process_id', to: 'pe_evaluations#new', via: :get, as: 'new_pe_evaluation'
  match 'pe_evaluations/download_xls_questions_update_pe_groups/:id', to: 'pe_evaluations#download_xls_for_questions_update_pe_groups', via: :get, as: 'pe_evaluation_download_xls_for_questions_update_pe_groups'
  match 'pe_evaluations/download_xls_questions_update_pe_members/:id', to: 'pe_evaluations#download_xls_for_questions_update_pe_members', via: :get, as: 'pe_evaluation_download_xls_for_questions_update_pe_members'
  match 'pe_evaluations/download_xls_questions_update_pe_member/:id', to: 'pe_evaluations#download_xls_for_questions_update_pe_member', via: :get, as: 'pe_evaluation_download_xls_for_questions_update_pe_member'
  match 'pe_evaluations/download_xls_questions_update_pe_rels/:id', to: 'pe_evaluations#download_xls_for_questions_update_pe_rels', via: :get, as: 'pe_evaluation_download_xls_for_questions_update_pe_rels'
  match 'pe_evaluations/massive_update_form/:id', to: 'pe_evaluations#massive_update_form', via: :get, as: 'pe_evaluation_massive_update_form'
  match 'pe_evaluations/massive_update/:id', to: 'pe_evaluations#massive_update', via: :post, as: 'pe_evaluation_massive_update'

  match 'pe_evaluations/massive_update_finish_definition_form/:id', to: 'pe_evaluations#massive_update_finish_definition_form', via: :get, as: 'pe_evaluation_massive_update_finish_definition_form'
  match 'pe_evaluations/massive_update_finish_definition/:id', to: 'pe_evaluations#massive_update_finish_definition', via: :post, as: 'pe_evaluation_massive_update_finish_definition'

  match 'pe_evaluations/edit_notifications_definition_by_users_accepted/:id', to: 'pe_evaluations#edit_notifications_definition_by_users_accepted', via: :get, as: 'pe_evaluations_edit_notifications_definition_by_users_accepted'
  match 'pe_evaluations/update_notifications_definition_by_users_accepted/:id', to: 'pe_evaluations#update_notifications_definition_by_users_accepted', via: :post, as: 'pe_evaluations_update_notifications_definition_by_users_accepted'

  match 'pe_evaluations/edit_notifications_def/:id/', to: 'pe_evaluations#edit_notifications_def', via: :get, as: 'pe_evaluation_edit_notifications_def'
  match 'pe_evaluations/update_notifications_def/:id', to: 'pe_evaluations#update_notifications_def', via: :post, as: 'pe_evaluation_update_notifications_def'


  #resources :pe_evaluation_rels

  resources :pe_elements, only: [:create, :show, :edit, :update, :destroy]
  match 'pe_elements/new/:pe_evaluation_id', to: 'pe_elements#new', via: :get, as: 'new_pe_element'

  resources :pe_question_models, only: [:create, :show, :edit, :update, :destroy]
  match 'pe_question_models/new/:pe_element_id', to: 'pe_question_models#new', via: :get, as: 'new_pe_question_model'


  resources :pe_element_descriptions, only: [:create, :edit, :update, :destroy]
  match 'pe_element_descriptions/new/:pe_element_id', to: 'pe_element_descriptions#new', via: :get, as: 'new_pe_element_description'

  resources :pe_groups, only: [:create, :edit, :update, :destroy]
  match 'pe_groups/new/:pe_evaluation_id', to: 'pe_groups#new', via: :get, as: 'new_pe_group'

  resources :pe_member_group2s
  match 'pe_member_group2s/massive_charge/:pe_process_id', to: 'pe_member_group2s#massive_charge', via: :get, as: 'pe_member_group2s_massive_charge'
  match 'pe_member_group2s/massive_charge/:pe_process_id', to: 'pe_member_group2s#massive_charge', via: :post, as: 'pe_member_group2s_massive_charge'

  resources :pe_group2s

  resources :pe_questions, only: [:create, :edit, :update, :destroy]
  match 'pe_questions/show_pe_member/:pe_evaluation_id/:pe_member_id', to: 'pe_questions#show_pe_member', via: :get, as: 'pe_questions_show_pe_member'
  match 'pe_questions/new/:pe_evaluation_id/:pe_question_id', to: 'pe_questions#new', via: :get, as: 'new_pe_question'

  resources :pe_alternatives, only: [:create, :edit, :update, :destroy]
  match 'pe_alternatives/new/:pe_question_id', to: 'pe_alternatives#new', via: :get, as: 'new_pe_alternative'

  resources :pe_question_activity_statuses, only: [:show, :create, :edit, :update, :destroy]
  match 'pe_question_activity_statuses/new/:pe_evaluation_id', to: 'pe_question_activity_statuses#new', via: :get, as: 'new_pe_question_activity_status'


  resources :pe_question_activity_fields, only: [:show, :create, :edit, :update, :destroy]
  match 'pe_question_activity_fields/new/:pe_evaluation_id', to: 'pe_question_activity_fields#new', via: :get, as: 'new_pe_question_activity_field'

  resources :pe_question_activity_field_lists, only: [:show, :create, :edit, :update, :destroy]
  match 'pe_question_activity_field_lists/new/:pe_evaluation_id', to: 'pe_question_activity_field_lists#new', via: :get, as: 'new_pe_question_activity_field_list'

  resources :pe_question_activity_field_list_items, only: [:create, :edit, :update, :destroy]
  match 'pe_question_activity_field_list_items/new/:pe_question_activity_field_list_id', to: 'pe_question_activity_field_list_items#new', via: :get, as: 'new_pe_question_activity_field_list_item'

  resources :pe_question_activities, only: [:create, :edit, :update, :destroy]
  match 'pe_question_activities/list/:pe_question_id', to: 'pe_question_activities#list', via: :get, as: 'pe_question_activities_list'
  match 'pe_question_activities/new/:pe_question_id', to: 'pe_question_activities#new', via: :get, as: 'pe_question_activities_new'

  resources :pe_members
  match 'pe_members/massive_charge/:pe_process_id', to: 'pe_members#massive_charge', via: :get, as: 'pe_members_massive_charge'
  match 'pe_members/massive_charge/:pe_process_id', to: 'pe_members#massive_charge', via: :post, as: 'pe_members_massive_charge'

  match 'pe_members/destroy_evaluated/:pe_member_id', to: 'pe_members#destroy_evaluated', via: :get, as: 'pe_members_destroy_evaluated'
  match 'pe_members/reset_evaluated/:pe_member_id', to: 'pe_members#reset_evaluated', via: :get, as: 'pe_members_reset_evaluated'
  match 'pe_members/remove_evaluated/:pe_member_id', to: 'pe_members#remove_evaluated', via: :get, as: 'pe_members_remove_evaluated'
  match 'pe_members/reactivate_evaluated/:pe_member_id', to: 'pe_members#reactivate_evaluated', via: :get, as: 'pe_members_reactivate_evaluated'

  match 'pe_members/massive_replace_evaluators_form/:pe_process_id', to: 'pe_members#massive_replace_evaluators', via: :get, as: 'pe_members_massive_replace_evaluators_form'
  match 'pe_members/massive_replace_evaluators/:pe_process_id', to: 'pe_members#massive_replace_evaluators', via: :post, as: 'pe_members_massive_replace_evaluators'

  match 'pe_members/massive_destroy_evaluated_form/:pe_process_id', to: 'pe_members#massive_destroy_evaluated', via: :get, as: 'pe_members_massive_destroy_evaluated_form'
  match 'pe_members/massive_destroy_evaluated/:pe_process_id', to: 'pe_members#massive_destroy_evaluated', via: :post, as: 'pe_members_massive_destroy_evaluated'

  match 'pe_members/massive_remove_evaluated_form/:pe_process_id', to: 'pe_members#massive_remove_evaluated', via: :get, as: 'pe_members_massive_remove_evaluated_form'
  match 'pe_members/massive_remove_evaluated/:pe_process_id', to: 'pe_members#massive_remove_evaluated', via: :post, as: 'pe_members_massive_remove_evaluated'

  match 'pe_members/massive_destroy_evaluator_form/:pe_process_id', to: 'pe_members#massive_destroy_evaluator', via: :get, as: 'pe_members_massive_destroy_evaluator_form'
  match 'pe_members/massive_destroy_evaluator/:pe_process_id', to: 'pe_members#massive_destroy_evaluator', via: :post, as: 'pe_members_massive_destroy_evaluator'

  match 'pe_members/show_evaluators/:pe_member_id', to: 'pe_members#show_evaluators', via: :get, as: 'pe_members_show_evaluators'
  match 'pe_members/show_evaluators/:pe_member_id', to: 'pe_members#show_evaluators', via: :post, as: 'pe_members_show_evaluators'

  match 'pe_members/update_weight/:pe_member_rel_id', to: 'pe_members#update_weight', via: :post, as: 'pe_members_update_weight'

  match 'pe_members/open_assessment/:pe_member_rel_id', to: 'pe_members#open_assessment', via: :get, as: 'pe_members_open_assessment'
  match 'pe_members/finish_assessment/:pe_member_rel_id', to: 'pe_members#finish_assessment', via: :get, as: 'pe_members_finish_assessment'

  match 'pe_members/massive_finish_assessment_form/:pe_process_id', to: 'pe_members#massive_finish_assessment', via: :get, as: 'pe_members_massive_finish_assessment_form'
  match 'pe_members/massive_finish_assessment/:pe_process_id', to: 'pe_members#massive_finish_assessment', via: :post, as: 'pe_members_massive_finish_assessment'

  match 'pe_members/massive_open_assessment_form/:pe_process_id', to: 'pe_members#massive_open_assessment', via: :get, as: 'pe_members_massive_open_assessment_form'
  match 'pe_members/massive_open_assessment/:pe_process_id', to: 'pe_members#massive_open_assessment', via: :post, as: 'pe_members_massive_open_assessment'

  match 'pe_members/massive_add_evaluator_form/:pe_process_id', to: 'pe_members#massive_add_evaluator', via: :get, as: 'pe_members_massive_add_evaluator_form'
  match 'pe_members/massive_add_evaluator/:pe_process_id', to: 'pe_members#massive_add_evaluator', via: :post, as: 'pe_members_massive_add_evaluator'

  match 'pe_members/massive_finish_assessment_rel_form/:pe_process_id', to: 'pe_members#massive_finish_assessment_rel', via: :get, as: 'pe_members_massive_finish_assessment_rel_form'
  match 'pe_members/massive_finish_assessment_rel/:pe_process_id', to: 'pe_members#massive_finish_assessment_rel', via: :post, as: 'pe_members_massive_finish_assessment_rel'

  match 'pe_members/open_whole_assessment/:pe_member_id', to: 'pe_members#open_whole_assessment', via: :get, as: 'pe_members_open_whole_assessment'
  match 'pe_members/finish_whole_assessment/:pe_member_id', to: 'pe_members#finish_whole_assessment', via: :get, as: 'pe_members_finish_whole_assessment'

  match 'pe_members/create_evaluator/:pe_member_id', to: 'pe_members#create_evaluator', via: :post, as: 'pe_members_create_evaluator'
  match 'pe_members/destroy_evaluator/:pe_member_rel_id', to: 'pe_members#destroy_evaluator', via: :get, as: 'pe_members_destroy_evaluator'

  match 'pe_members/create_validator', to: 'pe_members#create_validator', via: :post, as: 'pe_members_create_validator'
  match 'pe_members/delete_validator/:pe_member_rel_id', to: 'pe_members#destroy_validator', via: :get, as: 'pe_members_destroy_validator'

  match 'pe_members/feedback/:pe_member_id', to: 'pe_members#manage_feedback', via: :get, as: 'pe_members_manage_feedback'
  match 'pe_members/feedback/:pe_member_id', to: 'pe_members#manage_feedback', via: :post, as: 'pe_members_manage_feedback'

  match 'pe_members/open_feedback/:pe_member_id', to: 'pe_members#open_feedback', via: :get, as: 'pe_members_open_feedback'
  match 'pe_members/finish_feedback/:pe_member_id', to: 'pe_members#finish_feedback', via: :get, as: 'pe_members_finish_feedback'

  match 'pe_members/destroy_feedback_provider/:pe_member_id', to: 'pe_members#detroy_feedback_provider', via: :get, as: 'pe_members_destroy_feedback_provider'
  match 'pe_members/create_feedback_provider/:pe_member_id', to: 'pe_members#create_feedback_provider', via: :post, as: 'pe_members_create_feedback_provider'


  match 'pe_members/edit_evaluation_group/:pe_member_id/:pe_evaluation_id', to: 'pe_members#edit_evaluation_group', via: :get, as: 'pe_members_edit_evaluation_group'
  match 'pe_members/update_evaluation_group/:pe_member_id/:pe_evaluation_id', to: 'pe_members#update_evaluation_group', via: :post, as: 'pe_members_update_evaluation_group'

  match 'pe_members/edit_evaluation_weight/:pe_member_id/:pe_evaluation_id', to: 'pe_members#edit_evaluation_weight', via: :get, as: 'pe_members_edit_evaluation_weight'
  match 'pe_members/update_evaluation_weight/:pe_member_id/:pe_evaluation_id', to: 'pe_members#update_evaluation_weight', via: :post, as: 'pe_members_update_evaluation_weight'

  match 'pe_members/massive_change_evaluation_group_form/:pe_process_id', to: 'pe_members#massive_change_evaluation_group', via: :get, as: 'pe_members_massive_change_evaluation_group_form'
  match 'pe_members/massive_change_evaluation_group/:pe_process_id', to: 'pe_members#massive_change_evaluation_group', via: :post, as: 'pe_members_massive_change_evaluation_group'

  match 'pe_members/massive_change_evaluation_weight_form/:pe_process_id', to: 'pe_members#massive_change_evaluation_weight', via: :get, as: 'pe_members_massive_change_evaluation_weight_form'
  match 'pe_members/massive_change_evaluation_weight/:pe_process_id', to: 'pe_members#massive_change_evaluation_weight', via: :post, as: 'pe_members_massive_change_evaluation_weight'

  match 'pe_members/sync_characteristics/:pe_process_id', to: 'pe_members#sync_characteristics', via: :get, as: 'pe_members_sync_characteristics'
  match 'pe_members/sync_characteristics_user/:pe_member_id', to: 'pe_members#sync_characteristics_user', via: :get, as: 'pe_members_sync_characteristics_user'

  match 'pe_members/feedback_massive_charge/:pe_process_id', to: 'pe_members#feedback_massive_charge', via: :get, as: 'pe_members_feedback_massive_charge'
  match 'pe_members/feedback_massive_charge/:pe_process_id', to: 'pe_members#feedback_massive_charge', via: :post, as: 'pe_members_feedback_massive_charge'

  match 'pe_members/observers_massive_charge/:pe_process_id', to: 'pe_members#observers_massive_charge', via: :get, as: 'pe_members_observers_massive_charge'
  match 'pe_members/observers_massive_charge/:pe_process_id', to: 'pe_members#observers_massive_charge', via: :post, as: 'pe_members_observers_massive_charge'

  match 'pe_members/validation_massive_charge/:pe_process_id', to: 'pe_members#validation_massive_charge', via: :get, as: 'pe_members_validation_massive_charge'
  match 'pe_members/validation_massive_charge/:pe_process_id', to: 'pe_members#validation_massive_charge', via: :post, as: 'pe_members_validation_massive_charge'

  match 'pe_members/validation_massive_finish/:pe_process_id', to: 'pe_members#validation_massive_finish', via: :get, as: 'pe_members_validation_massive_finish'
  match 'pe_members/validation_massive_finish/:pe_process_id', to: 'pe_members#validation_massive_finish', via: :post, as: 'pe_members_validation_massive_finish'

  match 'pe_members/validation_def_by_u_massive_charge/:pe_evaluation_id/:step_number/:before_accept', to: 'pe_members#validation_def_by_u_massive_charge', via: :get, as: 'pe_members_validation_def_by_u_massive_charge'
  match 'pe_members/validation_def_by_u_massive_charge/:pe_evaluation_id/:step_number/:before_accept', to: 'pe_members#validation_def_by_u_massive_charge', via: :post, as: 'pe_members_validation_def_by_u_massive_charge'

  match 'pe_members/massive_finish_def_form/:pe_evaluation_id', to: 'pe_members#massive_finish_def', via: :get, as: 'pe_members_massive_finish_def_form'
  match 'pe_members/massive_finish_def/:pe_evaluation_id', to: 'pe_members#massive_finish_def', via: :post, as: 'pe_members_massive_finish_def'

  #resources :pe_member_rels

  #resources :pe_characteristics
  match 'pe_characteristics/rep/assessment_status/:pe_process_id', to: 'pe_characteristics#edit_rep_assessment_status', via: :get, as: 'pe_characteristics_edit_rep_assessment_status'
  match 'pe_characteristics/rep/assessment_status/:pe_process_id', to: 'pe_characteristics#update_rep_assessment_status', via: :post, as: 'pe_characteristics_update_rep_assessment_status'

  match 'pe_characteristics/rep/calibration_status/:pe_process_id', to: 'pe_characteristics#edit_rep_calibration_status', via: :get, as: 'pe_characteristics_edit_rep_calibration_status'
  match 'pe_characteristics/rep/calibration_status/:pe_process_id', to: 'pe_characteristics#update_rep_calibration_status', via: :post, as: 'pe_characteristics_update_rep_calibration_status'

  match 'pe_characteristics/rep/feedback_status/:pe_process_id', to: 'pe_characteristics#edit_rep_feedback_status', via: :get, as: 'pe_characteristics_edit_rep_feedback_status'
  match 'pe_characteristics/rep/feedback_status/:pe_process_id', to: 'pe_characteristics#update_rep_feedback_status', via: :post, as: 'pe_characteristics_update_rep_feedback_status'

  match 'pe_characteristics/rep/consolidated_final_results/:pe_process_id', to: 'pe_characteristics#edit_rep_consolidated_final_results', via: :get, as: 'pe_characteristics_edit_rep_consolidated_final_results'
  match 'pe_characteristics/rep/consolidated_final_results/:pe_process_id', to: 'pe_characteristics#update_rep_consolidated_final_results', via: :post, as: 'pe_characteristics_update_rep_consolidated_final_results'

  match 'pe_characteristics/rep/detailed_final_results/:pe_process_id', to: 'pe_characteristics#edit_rep_detailed_final_results', via: :get, as: 'pe_characteristics_edit_rep_detailed_final_results'
  match 'pe_characteristics/rep/detailed_final_results/:pe_process_id', to: 'pe_characteristics#update_rep_detailed_final_results', via: :post, as: 'pe_characteristics_update_rep_detailed_final_results'

  match 'pe_characteristics/reps/configuration/:pe_process_id', to: 'pe_characteristics#edit_reps_configuration', via: :get, as: 'pe_characteristics_edit_reps_configuration'
  match 'pe_characteristics/reps/configuration/:pe_process_id', to: 'pe_characteristics#update_reps_configuration', via: :post, as: 'pe_characteristics_update_reps_configuration'

  match 'pe_characteristics/gui/cal_committee/:pe_process_id', to: 'pe_characteristics#edit_gui_cal_committee', via: :get, as: 'pe_characteristics_edit_gui_cal_committee'
  match 'pe_characteristics/gui/cal_committee/:pe_process_id', to: 'pe_characteristics#update_gui_cal_committee', via: :post, as: 'pe_characteristics_update_gui_cal_committee'

  match 'pe_characteristics/gui/cal_member/:pe_process_id', to: 'pe_characteristics#edit_gui_cal_member', via: :get, as: 'pe_characteristics_edit_gui_cal_member'
  match 'pe_characteristics/gui/cal_member/:pe_process_id', to: 'pe_characteristics#update_gui_cal_member', via: :post, as: 'pe_characteristics_update_gui_cal_member'

  match 'pe_characteristics/gui/feedback/:pe_process_id', to: 'pe_characteristics#edit_gui_feedback', via: :get, as: 'pe_characteristics_edit_gui_feedback'
  match 'pe_characteristics/gui/feedback/:pe_process_id', to: 'pe_characteristics#update_gui_feedback', via: :post, as: 'pe_characteristics_update_gui_feedback'

  match 'pe_characteristics/gui/validation/:pe_process_id', to: 'pe_characteristics#edit_gui_validation', via: :get, as: 'pe_characteristics_edit_gui_validation'
  match 'pe_characteristics/gui/validation/:pe_process_id', to: 'pe_characteristics#update_gui_validation', via: :post, as: 'pe_characteristics_update_gui_validation'

  match 'pe_characteristics/gui/selection/:pe_process_id', to: 'pe_characteristics#edit_gui_selection', via: :get, as: 'pe_characteristics_edit_gui_selection'
  match 'pe_characteristics/gui/selection/:pe_process_id', to: 'pe_characteristics#update_gui_selection', via: :post, as: 'pe_characteristics_update_gui_selection'

  #pe_assign
  match 'pe/pe_assign/pl/:pe_process_id', to: 'pe_assign#people_list', via: :get, as: 'pe_assign_people_list'
  match 'pe/pe_assign/show/:pe_member_id', to: 'pe_assign#show', via: :get, as: 'pe_assign_show'
  match 'pe/pe_assign/search/:pe_member_id', to: 'pe_assign#show', via: :post, as: 'pe_assign_search'
  match 'pe/pe_assign/add/:pe_member_id', to: 'pe_assign#add', via: :post, as: 'pe_assign_add'
  match 'pe/pe_assign/remove/:pe_member_rel_id', to: 'pe_assign#remove', via: :get, as: 'pe_assign_remove'
  match 'pe/pe_assign/finish/:pe_member_id', to: 'pe_assign#finish', via: :post, as: 'pe_assign_finish'

  #pe_selection
  match 'pe/pe_selection/pl/:pe_process_id', to: 'pe_selection#people_list', via: :get, as: 'pe_selection_people_list'
  match 'pe/pe_selection/search_form/:pe_process_id', to: 'pe_selection#search', via: :get, as: 'pe_selection_search_form'
  match 'pe/pe_selection/search/:pe_process_id', to: 'pe_selection#search', via: :post, as: 'pe_selection_search'
  match 'pe/pe_selection/add/:pe_process_id', to: 'pe_selection#add', via: :post, as: 'pe_selection_add'
  match 'pe/pe_selection/remove/:pe_member_id', to: 'pe_selection#remove', via: :get, as: 'pe_selection_remove'

  #pe_query_definitions
  match 'pe/query_definitions/pl/:pe_process_id', to: 'pe_query_definitions#people_list', via: :get, as: 'pe_query_definitions_people_list'
  match 'pe/query_definitions/show/:pe_member_rel_id', to: 'pe_query_definitions#show', via: :get, as: 'pe_query_definitions_show'
  match 'pe/query_definitions/accept/:pe_process_id', to: 'pe_query_definitions#accept', via: :post, as: 'pe_query_definitions_accept'

  #pe_query_results
  match 'pe/query_results/pl/:pe_process_id', to: 'pe_query_results#people_list', via: :get, as: 'pe_query_results_people_list'
  match 'pe/query_results/show/:pe_member_rel_id', to: 'pe_query_results#show', via: :get, as: 'pe_query_results_show'

  #pe_definition_by_users

  match 'pe/def_by_user/pl/:pe_evaluation_id', to: 'pe_definition_by_user#people_list', via: :get, as: 'pe_def_by_user_people_list'
  match 'pe/def_by_user/show/:pe_evaluation_id/:pe_member_rel_id', to: 'pe_definition_by_user#show', via: :get, as: 'pe_def_by_user_show'
  match 'pe/def_by_user/show/:pe_evaluation_id/:pe_member_rel_id/:pe_evaluation_focus_id', to: 'pe_definition_by_user#show', via: :get, as: 'pe_def_by_user_show_with_focus'

  match 'pe/def_by_user/show_to_clone/:pe_evaluation_id/:pe_member_rel_id', to: 'pe_definition_by_user#show_to_clone', via: :get, as: 'pe_def_by_user_show_to_clone'
  match 'pe/def_by_user/clone/:pe_evaluation_id/:pe_member_rel_id', to: 'pe_definition_by_user#clone_def', via: :post, as: 'pe_def_by_user_clone_def'

  match 'pe/def_by_user/new/:pe_evaluation_id/:pe_member_rel_id/:pe_question_id', to: 'pe_definition_by_user#new', via: :get, as: 'pe_def_by_user_new'
  match 'pe/def_by_user/create/:pe_evaluation_id/:pe_member_rel_id', to: 'pe_definition_by_user#create', via: :post, as: 'pe_def_by_user_create'

  match 'pe/def_by_user/edit/:pe_question_id/:pe_member_rel_id', to: 'pe_definition_by_user#edit', via: :get, as: 'pe_def_by_user_edit'
  match 'pe/def_by_user/update/:pe_question_id/:pe_member_rel_id', to: 'pe_definition_by_user#update', via: :post, as: 'pe_def_by_user_update'

  match 'pe/def_by_user/delete/:pe_question_id/:pe_member_rel_id', to: 'pe_definition_by_user#destroy', via: :delete, as: 'pe_def_by_user_destroy'

  match 'pe/def_by_user/finish/:pe_evaluation_id/:pe_member_rel_id', to: 'pe_definition_by_user#finish', via: :post, as: 'pe_def_by_user_finish'

  match 'pe/def_by_user/detailed_final_results_pdf/:pe_member_rel_id', to: 'pe_definition_by_user#rep_detailed_final_results_pdf', via: :get, as: 'pe_definition_by_user_rep_detailed_final_results_pdf'
  match 'pe/def_by_user/definition_pdf/:pe_evaluation_id/:pe_member_rel_id', to: 'pe_definition_by_user#rep_definition_pdf', via: :get, as: 'pe_definition_by_user_definition_pdf'

  #pe_definition_by_users_accepted
  match 'pe/def_by_user_accept/:pe_evaluation_id', to: 'pe_definition_by_user_accept#accept_definition', via: :post, as: 'pe_def_by_user_accept_definition'
  match 'pe/def_by_user_accept/definition_pdf/:pe_evaluation_id', to: 'pe_definition_by_user_accept#rep_definition_pdf', via: :get, as: 'pe_definition_by_user_accept_definition_pdf'

  #pe_definition_by_users_validate
  match 'pe/def_by_user_validate/:pe_evaluation_id/:pe_member_evaluated_id/:step_number/:before_accept', to: 'pe_definition_by_user_validate#show', via: :get, as: 'pe_definition_by_user_validate_show'
  match 'pe/def_by_user_to_validate/:pe_evaluation_id/:pe_member_evaluated_id/:step_number/:before_accept', to: 'pe_definition_by_user_validate#validate', via: :post, as: 'pe_definition_by_user_validate_validate'

  match 'pe/def_by_user_validate_download_xls_for_validation_everybody/:pe_evaluation_id/:step_number/:before_accept', to: 'pe_definition_by_user_validate#download_xls_for_validation_everybody', via: :get, as: 'pe_definition_by_user_download_xls_for_validation_everybody'
  match 'pe/def_by_user_validate_download_xls_for_validation_pending/:pe_evaluation_id/:step_number/:before_accept', to: 'pe_definition_by_user_validate#download_xls_for_validation_pending', via: :get, as: 'pe_definition_by_user_download_xls_for_validation_pending'

  match 'pe/def_by_user_validate_validate_with_excel/:pe_evaluation_id/:step_number/:before_accept', to: 'pe_definition_by_user_validate#validate_with_excel', via: :get, as: 'pe_definition_by_user_validate_with_excel'
  match 'pe/def_by_user_validate_validate_with_excel/:pe_evaluation_id/:step_number/:before_accept', to: 'pe_definition_by_user_validate#validate_with_excel', via: :post, as: 'pe_definition_by_user_validate_with_excel'

  #pe_tracking

  match 'pe/tracking/pl/:pe_evaluation_id', to: 'pe_tracking#people_list', via: :get, as: 'pe_tracking_people_list'
  match 'pe/tracking/pl/:pe_evaluation_id/:pe_rel_id', to: 'pe_tracking#people_list', via: :get, as: 'pe_tracking_people_list_pe_rel'
  match 'pe/tracking/show/:pe_evaluation_id/:pe_member_rel_id', to: 'pe_tracking#show', via: :get, as: 'pe_tracking_show'
  match 'pe/tracking/show/:pe_evaluation_id/:pe_member_rel_id/:pe_evaluation_focus_id', to: 'pe_tracking#show', via: :get, as: 'pe_tracking_show_with_focus'
  match 'pe/tracking/track/:pe_evaluation_id/:pe_member_rel_id', to: 'pe_tracking#track', via: :post, as: 'pe_tracking_track'

  #pe_assessment
  match 'pe/assessment/pl/:pe_process_id', to: 'pe_assessment#people_list_2', via: :get, as: 'pe_assessment_people_list_2'
  match 'pe/assessment/pl2/:pe_process_id/:active_pe_rel_id', to: 'pe_assessment#people_list_2', via: :get, as: 'pe_assessment_people_list_2_2'
  match 'pe/assessment/show/:pe_member_rel_id', to: 'pe_assessment#show_2', via: :get, as: 'pe_assessment_show_2'
  match 'pe/assessment/show/:pe_member_rel_id/:from_uniq', to: 'pe_assessment#show_2', via: :get, as: 'pe_assessment_show_2_from_uniq'
  match 'pe/assessment/assess/:pe_member_rel_id/:pe_evaluation_id', to: 'pe_assessment#assess_2', via: :post, as: 'pe_assessment_assess_2'
  match 'pe/assessment/finish_assessment/:pe_member_rel_id', to: 'pe_assessment#finish_assessment', via: :post, as: 'pe_assessment_finish_assessment'
  #match 'pe/assessment/confirm/:hr_process_user_id', to: 'pe_assessment#confirm_2', via: :post, as: 'pe_assessment_confirm_2'

  match 'pe/force_re_calculate_evaluations_alejo', to: 'pe_assessment#force_re_calculate_evaluations', via: :get, as: 'force_re_calculate_evaluations_alejo'

  match 'pe/assessment/detailed_final_results_pdf/:pe_member_rel_id', to: 'pe_assessment#rep_detailed_final_results_pdf', via: :get, as: 'pe_assessment_rep_detailed_final_results_pdf'
  match 'pe/assessment/detailed_final_results_pdf_auto/:pe_member_rel_id', to: 'pe_assessment#rep_detailed_final_results_pdf_auto', via: :get, as: 'pe_assessment_rep_detailed_final_results_pdf_auto'

  #pe_observe
  match 'pe/observe_definition_by_user/:pe_member_evaluated_id/:pe_evaluation_id', to: 'pe_observe#definition_by_user', via: :get, as: 'pe_observe_definition_by_user'
  match 'pe/observe_tracking/:pe_member_evaluated_id/:pe_evaluation_id', to: 'pe_observe#tracking', via: :get, as: 'pe_observe_tracking'
  match 'pe/observe_assessment/:pe_member_rel_id', to: 'pe_observe#assessment', via: :get, as: 'pe_observe_assessment'

  match 'pe/observe_rep_detailed_final_results/:pe_process_report_id/:pe_process_id/:user_id', to: 'pe_observe#pe_rep_detailed_final_results', via: :get, as: 'pe_observe_rep_detailed_final_results'
  match 'pe/observe_rep_detailed_final_results_pdf/:pe_process_report_id/:pe_process_id/:user_id', to: 'pe_observe#pe_rep_detailed_final_results_pdf', via: :get, as: 'pe_observe_rep_detailed_final_results_pdf'
  match 'pe/observe_rep_detailed_final_results_pdf_old/:hr_process_user_id/:pe_process_id/:user_id', to: 'pe_observe#final_results_pdf_node', via: :get, as: 'pe_observe_final_results_pdf_node'

  #pe_validation

  match 'pe/validation/list_people/:pe_process_id', to: 'pe_validation#people_list', via: :get, as: 'pe_validation_people_list'
  match 'pe/validation/show/:pe_member_rel_id', to: 'pe_validation#show', via: :get, as: 'pe_validation_show'
  match 'pe/validation/validate/:pe_member_rel_id', to: 'pe_validation#validate', via: :post, as: 'pe_validation_validate'

  match 'pe/validation/massive_form/:pe_process_id', to: 'pe_validation#massive_form', via: :get, as: 'pe_validation_massive_form'
  match 'pe/validation/massive_download_xls/:pe_process_id', to: 'pe_validation#massive_download_xls', via: :get, as: 'pe_validation_massive_download_xls'
  match 'pe/validation/massive/:pe_process_id', to: 'pe_validation#massive_validation', via: :post, as: 'pe_validation_massive'

  resources :pe_feedback_fields, only: [:create, :edit, :update, :destroy]
  match 'pe_feedback_fields/new/:pe_process_id', to: 'pe_feedback_fields#new', via: :get, as: 'new_pe_feedback_field'

  resources :pe_feedback_field_lists, only: [:show, :create, :edit, :update, :destroy]
  match 'pe_feedback_field_lists/new/:pe_process_id', to: 'pe_feedback_field_lists#new', via: :get, as: 'new_pe_feedback_field_list'
  match 'pe_feedback_field_lists/massive_create_items/:pe_process_id/:pe_feedback_field_list_id', to: 'pe_feedback_field_lists#massive_create_items', via: :post, as: 'massive_create_items_pe_feedback_field_list'

  resources :pe_feedback_field_list_items, only: [:create, :edit, :update, :destroy]
  match 'pe_feedback_field_list_items/new/:pe_feedback_field_list_id', to: 'pe_feedback_field_list_items#new', via: :get, as: 'new_pe_feedback_field_list_item'

  resources :pe_feedback_accepted_fields, only: [:create, :edit, :update, :destroy]
  match 'pe_feedback_accepted_fields/new/:pe_process_id', to: 'pe_feedback_accepted_fields#new', via: :get, as: 'new_pe_feedback_accepted_field'

  #pe_calibration
  match 'pe/calibration/list_sessions/:pe_process_id', to: 'pe_calibration#list_sessions', via: :get, as: 'pe_calibration_list_sessions'
  match 'pe/calibration/session/:pe_cal_session_id', to: 'pe_calibration#show_session', via: :get, as: 'pe_calibration_show_session'
  match 'pe/calibration/active_finish_session/:pe_cal_session_id', to: 'pe_calibration#active_finish_session', via: :get, as: 'pe_calibration_active_finish_session'
  match 'pe/calibration/calibrate/:pe_cal_member_id', to: 'pe_calibration#calibrate', via: :post, as: 'pe_calibration_calibrate'

  #pe_calibration_evaluation
  match 'pe/calibration_evaluations/list_sessions/:pe_evaluation_id', to: 'pe_calibration_evaluation#list_sessions', via: :get, as: 'pe_calibration_evaluations_list_sessions'
  match 'pe/calibration_evaluations/session/:pe_cal_session_id', to: 'pe_calibration_evaluation#show_session', via: :get, as: 'pe_calibration_evaluations_show_session'
  match 'pe/calibration_evaluations/active_finish_session/:pe_cal_session_id', to: 'pe_calibration_evaluation#active_finish_session', via: :get, as: 'pe_calibration_evaluations_active_finish_session'
  match 'pe/calibration_evaluations/calibrate/:pe_cal_session_id', to: 'pe_calibration_evaluation#calibrate', via: :post, as: 'pe_calibration_evaluations_calibrate'


  #pe_feedback

  match 'pe/feedback/list_people/:pe_process_id', to: 'pe_feedback#people_list_2', via: :get, as: 'pe_feedback_people_list_2'
  match 'pe/feedback/new/:pe_member_id', to: 'pe_feedback#new_feedback_2', via: :get, as: 'pe_feedback_new_feedback_2'
  match 'pe/feedback/new/:pe_member_id/:from_uniq', to: 'pe_feedback#new_feedback_2', via: :get, as: 'pe_feedback_new_feedback_2_from_uniq'
  match 'pe/feedback/create/:pe_member_id', to: 'pe_feedback#create_feedback_2', via: :post, as: 'pe_feedback_create_feedback_2'

  match 'pe/feedback/detailed_final_results_pdf/:pe_member_id', to: 'pe_feedback#rep_detailed_final_results_pdf', via: :get, as: 'pe_feedback_rep_detailed_final_results_pdf'


  #pe_feedback_accept

  match 'pe/feedback_accept/show/:pe_process_id', to: 'pe_feedback_accept#show', via: :get, as: 'pe_feedback_accept_show'
  match 'pe/feedback_accept/accept/:pe_process_id', to: 'pe_feedback_accept#accept', via: :post, as: 'pe_feedback_accept_accept'
  match 'pe/feedback_accept/detailed_final_results_pdf/:pe_process_id', to: 'pe_feedback_accept#rep_detailed_final_results_pdf', via: :get, as: 'pe_feedback_accept_rep_detailed_final_results_pdf'

  #pe_managinig

  match 'pe/managing/list_processes', to: 'pe_managing#list_processes_2', via: :get, as: 'pe_managing_list_processes_2'

  match 'pe/managing/edit_dates_2/:pe_process_id', to: 'pe_managing#edit_dates_2', via: :get, as: 'pe_managing_edit_dates_2'
  match 'pe/managing/update_dates_2/:pe_process_id', to: 'pe_managing#update_dates_2', via: :post, as: 'pe_managing_update_dates_2'

  match 'pe/managing/def_by_users/:pe_evaluation_id', to: 'pe_managing#manage_def_by_users', via: :get, as: 'pe_managing_def_by_users'
  match 'pe/managing/def_by_user/:pe_member_id/:pe_evaluation_id', to: 'pe_managing#manage_def_by_user', via: :get, as: 'pe_managing_def_by_user'
  match 'pe/managing/def_by_user/:pe_member_id/:pe_evaluation_id', to: 'pe_managing#manage_def_by_user', via: :post, as: 'pe_managing_def_by_user'
  match 'pe/managing/def_by_user/:pe_evaluation_id/open/:pe_member_rel_id', to: 'pe_managing#open_def_by_user', via: :get, as: 'pe_managing_open_def_by_user'
  match 'pe/managing/def_by_user/:pe_evaluation_id/close/:pe_member_rel_id', to: 'pe_managing#close_def_by_user', via: :get, as: 'pe_managing_close_def_by_user'

  match 'pe/managing/def_by_user/:pe_evaluation_id/open_accept/:pe_member_rel_id', to: 'pe_managing#open_accept_def_by_user', via: :get, as: 'pe_managing_open_accept_def_by_user'
  match 'pe/managing/def_by_user/:pe_evaluation_id/close_accept/:pe_member_rel_id', to: 'pe_managing#close_accept_def_by_user', via: :get, as: 'pe_managing_close_accept_def_by_user'

  match 'pe/managing/def_by_user/:pe_evaluation_id/open_val/:pe_member_rel_id/:num_step/:before', to: 'pe_managing#open_val_def_by_user', via: :get, as: 'pe_managing_open_val_def_by_user'
  match 'pe/managing/def_by_user/:pe_evaluation_id/close_val/:pe_member_rel_id/:num_step/:before', to: 'pe_managing#close_val_def_by_user', via: :get, as: 'pe_managing_close_val_def_by_user'
  match 'pe/managing/def_by_user/:pe_member_id/:pe_evaluation_id/destroy_validator/:num_step/:before', to: 'pe_managing#destroy_validator_def_by_user', via: :get, as: 'pe_managing_destroy_validator_def_by_user'

  match 'pe/managing/def_by_user/:pe_member_id/:pe_evaluation_id/change_boss', to: 'pe_managing#change_boss_def_by_user', via: :post, as: 'pe_managing_change_boss_def_by_user'
  match 'pe/managing/def_by_user/:pe_member_id/:pe_evaluation_id/change_val/:num_step/:before', to: 'pe_managing#change_val_def_by_user', via: :post, as: 'pe_managing_change_val_def_by_user'

  match 'pe/managing/assessments/:pe_process_id', to: 'pe_managing#manage_assessments', via: :get, as: 'pe_managing_manage_assessments'
  match 'pe/managing/assessment/:pe_member_id', to: 'pe_managing#manage_assessment', via: :get, as: 'pe_managing_manage_assessment'
  match 'pe/managing/assessment/:pe_member_id', to: 'pe_managing#manage_assessment', via: :post, as: 'pe_managing_manage_assessment'

  match 'pe/managing/open_assessment/:pe_member_rel_id', to: 'pe_managing#open_assessment', via: :get, as: 'pe_managing_open_assessment'
  match 'pe/managing/finish_assessment/:pe_member_rel_id', to: 'pe_managing#finish_assessment', via: :get, as: 'pe_managing_finish_assessment'

  match 'pe/managing/open_whole_assessment/:pe_member_id', to: 'pe_managing#open_whole_assessment', via: :get, as: 'pe_managing_open_whole_assessment'
  match 'pe/managing/finish_whole_assessment/:pe_member_id', to: 'pe_managing#finish_whole_assessment', via: :get, as: 'pe_managing_finish_whole_assessment'

  match 'pe/managing/create_evaluator/:pe_member_id', to: 'pe_managing#create_evaluator', via: :post, as: 'pe_managing_create_evaluator'
  match 'pe/managing/destroy_evaluator/:pe_member_rel_id', to: 'pe_managing#destroy_evaluator', via: :get, as: 'pe_managing_destroy_evaluator'

  match 'pe/managing/create_validator/:pe_member_rel_id', to: 'pe_managing#create_validator', via: :post, as: 'pe_managing_create_validator'
  match 'pe/managing/delete_validator/:pe_member_rel_id', to: 'pe_managing#destroy_validator', via: :get, as: 'pe_managing_destroy_validator'

  match 'pe/managing/feedbacks/:pe_process_id', to: 'pe_managing#manage_feedbacks', via: :get, as: 'pe_managing_manage_feedbacks'
  match 'pe/managing/feedback/:pe_member_id', to: 'pe_managing#manage_feedback', via: :get, as: 'pe_managing_manage_feedback'
  match 'pe/managing/feedback/:pe_member_id', to: 'pe_managing#manage_feedback', via: :post, as: 'pe_managing_manage_feedback'

  match 'pe/managing/open_feedback/:pe_member_id', to: 'pe_managing#open_feedback', via: :get, as: 'pe_managing_open_feedback'
  match 'pe/managing/finish_feedback/:pe_member_id', to: 'pe_managing#finish_feedback', via: :get, as: 'pe_managing_finish_feedback'

  match 'pe/managing/destroy_feedback_provider/:pe_member_id', to: 'pe_managing#detroy_feedback_provider', via: :get, as: 'pe_managing_destroy_feedback_provider'
  match 'pe/managing/create_feedback_provider/:pe_member_id', to: 'pe_managing#create_feedback_provider', via: :post, as: 'pe_managing_create_feedback_provider'

  match 'pe/managing/register_results/list_evaluations/:pe_process_id', to: 'pe_managing#register_results_list_evaluations', via: :get, as: 'pe_managing_register_results_list_evaluations'
  match 'pe/managing/register_results/show_evaluation/:pe_evaluation_id', to: 'pe_managing#register_results_show_evaluation', via: :get, as: 'pe_managing_register_results_show_evaluation'
  match 'pe/managing/register_results/register_results/:pe_evaluation_id', to: 'pe_managing#register_results', via: :post, as: 'pe_managing_register_results'
  match 'pe/managing/register_results/register_results_xls/:pe_evaluation_id', to: 'pe_managing#register_results_xls', via: :post, as: 'pe_managing_register_results_xls'
  match 'pe/managing/register_results_questions/show_evaluation/:pe_evaluation_id', to: 'pe_managing#register_results_questions_show_evaluation', via: :get, as: 'pe_managing_register_questions_results_show_evaluation'
  match 'pe/managing/register_results_questions/download_evaluation_to_activate/:pe_evaluation_id', to: 'pe_managing#register_results_questions_download_evaluation_to_activate', via: :get, as: 'pe_managing_register_questions_download_evaluation_to_activate'
  match 'pe/managing/register_results_questions/download_evaluation_to_assess/:pe_evaluation_id', to: 'pe_managing#register_results_questions_download_evaluation_to_assess', via: :get, as: 'pe_managing_register_questions_download_evaluation_to_assess'
  match 'pe/managing/register_results/register_results_questions_xls/:pe_evaluation_id', to: 'pe_managing#register_results_questions_xls', via: :post, as: 'pe_managing_register_results_questions_xls'
  match 'pe/managing/register_results/register_results_all_evaluations_questions_xls/:pe_process_id', to: 'pe_managing#register_results_all_evaluations_questions_xls', via: :post, as: 'pe_managing_register_results_all_evaluations_questions_xls'

  match 'pe/managing/register_results/list_people/:pe_process_id', to: 'pe_managing#register_results_list_people', via: :get, as: 'pe_managing_register_results_list_people'
  match 'pe/managing/register_results/show/:pe_member_id', to: 'pe_managing#register_results_show', via: :get, as: 'pe_managing_register_results_show'
  match 'pe/managing/register_results/assess/:pe_member_id/:pe_evaluation_id', to: 'pe_managing#register_results_assess', via: :post, as: 'pe_managing_register_results_assess'

  match 'pe/managing/download_excel_register_results_evaluations/:pe_process_id', to: 'pe_managing#download_excel_register_results_evaluations', via: :get, as: 'pe_managing_download_excel_register_results_evaluations'

  match 'pe/managing/modify_evaluations/list_people/:pe_process_id', to: 'pe_managing#modify_evaluations_list_people', via: :get, as: 'pe_managing_modify_evaluations_list_people'
  match 'pe/managing/modify_evaluations/show/:pe_member_id', to: 'pe_managing#modify_evaluations_show', via: :get, as: 'pe_managing_modify_evaluations_show'

  match 'pe/managing/modify_evaluations/new/:pe_evaluation_id/:pe_member_id/:pe_question_id', to: 'pe_managing#modify_evaluations_new', via: :get, as: 'pe_managing_modify_evaluations_new'
  match 'pe/managing/modify_evaluations/create/:pe_evaluation_id/:pe_member_id', to: 'pe_managing#modify_evaluations_create', via: :post, as: 'pe_managing_modify_evaluations_create'

  match 'pe/managing/modify_evaluations/edit/:pe_question_id', to: 'pe_managing#modify_evaluations_edit', via: :get, as: 'pe_managing_modify_evaluations_edit'
  match 'pe/managing/modify_evaluations/update/:pe_question_id', to: 'pe_managing#modify_evaluations_update', via: :post, as: 'pe_managing_modify_evaluations_update'

  match 'pe/managing/modify_evaluations/destroy/:pe_question_id', to: 'pe_managing#modify_evaluations_destroy', via: :delete, as: 'pe_managing_modify_evaluations_destroy'

  #pe_reporting_tests
  match 'pe_reporting_tests/detailed_final_results_evaluation/:pe_evaluation_id', to: 'pe_reporting_tests#rep_detailed_final_results_evaluation', via: :get, as: 'pe_reporting_tests_rep_detailed_final_results_evaluation'

  #pe_reporting

  match 'pe/reports/list_processes', to: 'pe_reporting#list_processes_2', via: :get, as: 'pe_reporting_list_processes_2'
  match 'pe/reports/list_reports/:pe_process_id', to: 'pe_reporting#list_reports_2', via: :get, as: 'pe_reporting_list_reports_2'

  match 'pe/report/evaluated_members_configuration/:pe_process_id', to: 'pe_reporting#rep_evaluated_members_configuration', via: :get, as: 'pe_reporting_rep_evaluated_members_configuration'
  match 'pe/report/evaluations_configuration/:pe_process_id', to: 'pe_reporting#rep_evaluations_configuration', via: :get, as: 'pe_reporting_rep_evaluations_configuration'

  match 'pe/report/general_status/:pe_process_id', to: 'pe_reporting#rep_general_status', via: :get, as: 'pe_reporting_rep_general_status'
  match 'pe/report/definition_by_user_status/:pe_evaluation_id', to: 'pe_reporting#rep_definition_by_user_status', via: :get, as: 'pe_reporting_rep_definition_by_user_status'
  match 'pe/report/members_configuration_evaluation/:pe_evaluation_id', to: 'pe_reporting#rep_members_configuration_evaluation', via: :get, as: 'pe_reporting_rep_members_configuration_evaluation'
  match 'pe/report/detailed_definition_evaluation/:pe_evaluation_id/:active', to: 'pe_reporting#rep_detailed_definition_evaluation', via: :get, as: 'pe_reporting_rep_detailed_definition_evaluation'

  match 'pe/report/tracking_status/:pe_evaluation_id', to: 'pe_reporting#rep_tracking_status', via: :get, as: 'pe_reporting_rep_tracking_status'
  match 'pe/report/assign_status/:pe_process_id', to: 'pe_reporting#rep_assign_status', via: :get, as: 'pe_reporting_rep_assign_status'
  match 'pe/report/assessment_status/:pe_process_id', to: 'pe_reporting#rep_assessment_status', via: :get, as: 'pe_reporting_rep_assessment_status'
  match 'pe/report/validation_status/:pe_process_id', to: 'pe_reporting#rep_validation_status', via: :get, as: 'pe_reporting_rep_validation_status'
  match 'pe/report/calibration_status/:pe_process_id', to: 'pe_reporting#rep_calibration_status', via: :get, as: 'pe_reporting_rep_calibration_status'
  match 'pe/report/feedback_status/:pe_process_id', to: 'pe_reporting#rep_feedback_status', via: :get, as: 'pe_reporting_rep_feedback_status'

  match 'pe/report/consolidated_final_results_people_filter/:pe_process_id', to: 'pe_reporting#rep_consolidated_final_results_people_filter', via: :get, as: 'pe_reporting_rep_consolidated_final_results_people_filter'
  match 'pe/report/consolidated_final_results_people/:pe_process_id', to: 'pe_reporting#rep_consolidated_final_results_people', via: :post, as: 'pe_reporting_rep_consolidated_final_results_people'

  match 'pe/report/consolidated_final_results_areas/:pe_process_id', to: 'pe_reporting#rep_consolidated_final_results_areas', via: :get, as: 'pe_reporting_rep_consolidated_final_results_areas'

  match 'pe/report/detailed_final_results_people_list/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_people_list', via: :get, as: 'pe_reporting_rep_detailed_final_results_people_list'
  match 'pe/report/detailed_final_results_people_list_hab/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_people_list_hab', via: :get, as: 'pe_reporting_rep_detailed_final_results_people_list_hab'
  match 'pe/report/detailed_final_results_people_list_i/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_people_list_i', via: :get, as: 'pe_reporting_rep_detailed_final_results_people_list_i'
  match 'pe/report/detailed_final_results_person/:pe_member_id', to: 'pe_reporting#rep_detailed_final_results_person', via: :get, as: 'pe_reporting_rep_detailed_final_results_person'
  match 'pe/report/detailed_final_results_person_i/:pe_member_id', to: 'pe_reporting#rep_detailed_final_results_person_i', via: :get, as: 'pe_reporting_rep_detailed_final_results_person_i'




  match 'pe/report/detailed_final_results_area_list/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_area_list', via: :get, as: 'pe_reporting_rep_detailed_final_results_area_list'
  match 'pe/report/detailed_final_results_area/:pe_area_id', to: 'pe_reporting#rep_detailed_final_results_area', via: :get, as: 'pe_reporting_rep_detailed_final_results_area'

  match 'pe/report/detailed_final_results_evaluations/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_evaluations', via: :get, as: 'pe_reporting_rep_detailed_final_results_evaluations'
  match 'pe/report/detailed_final_results_evaluations_cal/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_evaluations_cal', via: :get, as: 'pe_reporting_rep_detailed_final_results_evaluations_cal'
  match 'pe/report/detailed_final_results_questions/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_questions', via: :get, as: 'pe_reporting_rep_detailed_final_results_questions'
  match 'pe/report/detailed_final_results_evaluation/:pe_evaluation_id', to: 'pe_reporting#rep_detailed_final_results_evaluation', via: :get, as: 'pe_reporting_rep_detailed_final_results_evaluation'
  match 'pe/report/detailed_final_results_evaluation_1/:pe_evaluation_id', to: 'pe_reporting#rep_detailed_final_results_evaluation_1', via: :get, as: 'pe_reporting_rep_detailed_final_results_evaluation_1'

  match 'pe/report/detailed_final_results_pdf/:pe_member_id', to: 'pe_reporting#rep_detailed_final_results_pdf', via: :get, as: 'pe_reporting_rep_detailed_final_results_pdf'
  match 'pe/report/detailed_final_results_pdf_only_boss/:pe_member_id', to: 'pe_reporting#rep_detailed_final_results_pdf_only_boss', via: :get, as: 'pe_reporting_rep_detailed_final_results_pdf_only_boss'

  match 'pe/report/consolidated_feedback/:pe_process_id', to: 'pe_reporting#rep_consolidated_feedback', via: :get, as: 'pe_reporting_rep_consolidated_feedback'
  match 'pe/report/consolidated_feedback_cols/:pe_process_id', to: 'pe_reporting#rep_consolidated_feedback_cols', via: :get, as: 'pe_reporting_rep_consolidated_feedback_cols'
  match 'pe/report/consolidated_feedback_accepted/:pe_process_id', to: 'pe_reporting#rep_consolidated_feedback_accepted', via: :get, as: 'pe_reporting_rep_consolidated_feedback_accepted'
  match 'pe/report/consolidated_feedback_accepted_survey/:pe_process_id', to: 'pe_reporting#rep_consolidated_feedback_accepted_survey', via: :get, as: 'pe_reporting_rep_consolidated_feedback_accepted_survey'

  match 'pe/report/detailed_final_results_people_pdf/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_people_pdf', via: :get, as: 'pe_reporting_rep_detailed_final_results_people_pdf'
  match 'pe/report/detailed_final_results_people_for_employees_pdf/:pe_process_id', to: 'pe_reporting#rep_detailed_final_results_people_for_employees_pdf', via: :get, as: 'pe_reporting_rep_detailed_final_results_people_for_employees_pdf'

  match 'pe/report/def_by_user_validate_download_xls_for_validation_everybody_admin/:pe_process_id', to: 'pe_reporting#download_xls_for_validation_everybody_admin', via: :get, as: 'pe_def_by_user_validate_download_xls_for_validation_everybody_admin'

  match 'pe/report/rep_rol_privado/:pe_process_id', to: 'pe_reporting#rep_rol_privado', via: :get, as: 'pe_reporting_rep_rol_privado'

  #pe_reporting_admin

  match 'pe/report_admin/detailed_definition_evaluation_admin/:pe_evaluation_id', to: 'pe_reporting_admin#rep_detailed_definition_evaluation_admin', via: :get, as: 'pe_reporting_admin_rep_detailed_definition_evaluation_admin'

  #resources :pe_member_groups

  resources :pe_managers, only: [:destroy]
  match 'pe_managers/:pe_process_id/search', to: 'pe_managers#search', via: :get, as: 'pe_managers_search'
  match 'pe_managers/:pe_process_id/search', to: 'pe_managers#search', via: :post, as: 'pe_managers_search'
  match 'pe_managers/:pe_process_id/add_manager/:user_id', to: 'pe_managers#add_manager', via: :get, as: 'pe_managers_add_manager'

  resources :pe_reporters, only: [:destroy]
  match 'pe_reporters/:pe_process_id/search', to: 'pe_reporters#search', via: :get, as: 'pe_reporters_search'
  match 'pe_reporters/:pe_process_id/search', to: 'pe_reporters#search', via: :post, as: 'pe_reporters_search'
  match 'pe_reporters/:pe_process_id/add_reporter/:user_id', to: 'pe_reporters#add_reporter', via: :post, as: 'pe_reporters_add_reporter'

  resources :pe_cal_sessions, only: [:create, :edit, :update, :destroy]
  match 'pe_cal_sessions/list/:pe_process_id', to: 'pe_cal_sessions#index', via: :get, as: 'pe_cal_sessions_list'
  match 'pe_cal_session/new/:pe_process_id', to: 'pe_cal_sessions#new', via: :get, as: 'pe_cal_session_new'

  match 'pe_cal_sessions/list_e/:pe_evaluation_id', to: 'pe_cal_sessions#index_e', via: :get, as: 'pe_cal_sessions_list_e'
  match 'pe_cal_session/new_e/:pe_evaluation_id', to: 'pe_cal_sessions#new_e', via: :get, as: 'pe_cal_session_new_e'

  resources :pe_cal_committees, only: [:destroy]

  match 'pe_cal_committees/list/:pe_cal_session_id', to: 'pe_cal_committees#list', via: :get, as: 'pe_cal_committees_list'
  match 'pe_cal_committees/list/:pe_cal_session_id', to: 'pe_cal_committees#list', via: :post, as: 'pe_cal_committees_list'
  match 'pe_cal_committees/create/:pe_cal_session_id', to: 'pe_cal_committees#create', via: :post, as: 'pe_cal_committees_create'
  match 'pe_cal_committees/set_manager/:id', to: 'pe_cal_committees#set_manager', via: :get, as: 'pe_cal_committees_set_manager'
  match 'pe_cal_committees/unset_manager/:id', to: 'pe_cal_committees#unset_manager', via: :get, as: 'pe_cal_committees_unset_manager'

  resources :pe_cal_members, only: [:destroy]

  match 'pe_cal_members/list/:pe_cal_session_id', to: 'pe_cal_members#list', via: :get, as: 'pe_cal_members_list'
  match 'pe_cal_members/list/:pe_cal_session_id', to: 'pe_cal_members#list', via: :post, as: 'pe_cal_members_list'
  match 'pe_cal_members/create/:pe_cal_session_id', to: 'pe_cal_members#create', via: :post, as: 'pe_cal_members_create'

=begin


  match 'users_carga_masiva', to: 'users#carga_masiva', via: :get, as: 'users_carga_masiva'
  match 'users_carga_masiva', to: 'users#carga_masiva_upload', via: :post, as: 'users_carga_masiva'
  match 'users_update_masivo', to: 'users#update_masivo', via: :get, as: 'users_update_masivo'
  match 'users_update_masivo', to: 'users#update_masivo_upload', via: :post, as: 'users_update_masivo'
  match 'users_carga_masiva_fotos', to: 'users#carga_masiva_fotos', via: :get, as: 'users_carga_masiva_fotos'
  match 'users_carga_masiva_fotos', to: 'users#carga_masiva_fotos_upload', via: :post, as: 'users_carga_masiva_fotos'
  match 'actualizar_foto/:id', to: 'users#edit_foto', via: :get, as: 'users_edit_foto'
  match 'actualizar_foto/:id', to: 'users#upload_foto', via: :post, as: 'users_upload_foto'
  match 'actualizar_foto_by_user', to: 'users#edit_foto_by_user', via: :get, as: 'users_edit_foto_by_user'
  match 'actualizar_foto_by_user', to: 'users#upload_foto_by_user', via: :post, as: 'users_upload_foto_by_user'
  match 'users/reportes/lista_usuarios_xlsx', to: 'users#lista_usuarios_xlsx', via: :get, as: 'users_lista_usuarios_xlsx'
  match 'users/reportes/lista_usuarios', to: 'users#lista_usuarios', via: :get, as: 'users_lista_usuarios'
=end

  match 'correo_prima', to: 'correo_prima#formulario', via: :get, as: 'correo_prima_formulario'
  match 'correo_prima', to: 'correo_prima#enviar_correo', via: :post, as: 'correo_prima_enviar'


  match 'log_systems', to: 'log_systems#index', via: :get, as: 'log_systems'
  match 'log_systems', to: 'log_systems#index', via: :post, as: 'log_systems_form'
  match 'log_systems/:id', to: 'log_systems#show', via: :get, as: 'log_system'

  match 'sql_query/box', to: 'sql_query#query_box', via: :get, as: 'sql_query_box'
  match 'sql_query/results', to: 'sql_query#query_results', via: :post, as: 'sql_query_results'

  match 'batch_data_corrector', to: 'batch_data_correctors#cargador_form', via: :get, as: 'batch_data_corrector_form'
  match 'batch_data_corrector', to: 'batch_data_correctors#cargador', via: :post, as: 'batch_data_corrector'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end