run 'cd /srv/www/lms/current/config/ && git checkout -- database.yml'

if !Dir.exists?('/var/www/remote-storage/lms-storage')

  #se protege la clave para que solo sea accesible por el usuario
  run 'chmod 400 /srv/www/lms/current/deploy/storage_server.pem'

  #se monta la la carpeta remota siempre y cuando no esté ya montada. Esto para evitar errores en deploys con la máquina andando (que serán la mayoría)
  run 'sshfs ubuntu@10.0.0.22:storage /var/www/remote-storage/ -o allow_other,IdentityFile=/srv/www/lms/current/deploy/storage_server.pem'
end